﻿//agGrid.initialiseAgGridWithAngular1(angular);

//var app = angular.module('QuickFMS', ["agGrid"]);


app.service('MaploaderService', function ($http, $q, UtilityService) {

    this.bindMap = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetMapItems', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GET_VERTICAL_WISE_ALLOCATIONS = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GET_VERTICAL_WISE_ALLOCATIONS', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.bindMarkers = function (result) {
        var deferred = $q.defer();
        // var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetMarkers', result)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.GradeAllocStatus = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.get(UtilityService.path + '/api/MaploaderAPI/GradeAllocStatus')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getSpaceDaysRestriction = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.get(UtilityService.path + '/api/MaploaderAPI/getSpaceDaysRestriction')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.bindCornerLables = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetCornerLables', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetFlrIdbyLoc = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Space_mapAPI/GetFloorIDBBox?lcm_code=' + GetParameterValues('lcm_code') + '&twr_code=' + GetParameterValues('twr_code') + '&flr_code=' + GetParameterValues('flr_code'))
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsBySPCID = function (spcid) {
        var deferred = $q.defer();
        var dataobj = { flr_code: $('#ddlfloors').val(), CatValue: spcid };
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceDetailsBySPCID', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsByREQID = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceDetailsByREQID', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsBySUBITEM = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceDetailsBySUBITEM', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getVLANS = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MaploaderAPI/getVLANS')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsByVLAN = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceDetailsByVLAN', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.release = function (reqdet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/ReleaseSelectedseat', reqdet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.ActivateSpaces = function (reqdet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/ActivateSpaces', reqdet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getEmpDetails = function (selCostCenters) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetEmpDetails', selCostCenters)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getAllocEmpDetails = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetAllocEmpDetails', selDet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };



    this.allocateSeats = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/AllocateSeats', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.spcAvailabilityByShift = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/SpcAvailabilityByShift', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceFromFloorMaps = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSpaceRecords', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getLegendsSummary = function (dataobj) {
        var deferred = $q.defer();
        /*var dataobj = { flr_code: flrid };*/
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetLegendsSummary', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getallFilterbyItem = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MaploaderAPI/GetallFilterbyItem')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getallFilterbySubItem = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetallFilterbySubItem', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetTotalAreaDetails = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetTotalAreaDetails', selDet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSeatingCapacity = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSeatingCapacity', selDet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getEmpAllocSeat = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetEmpAllocSeat', selDet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.InactiveSeats = function (spcobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/InactiveSpaceSeats', spcobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getVer_And_CC = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/getVer_And_CC', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getMonitor = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetMonitors')
            .then(function (response) {
                //console.log(response.data);
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getBookedSeatDetailsBySpace = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaploaderAPI/GetSeatAllocDetails', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});

app.controller('MaploaderController', function ($scope, $q, MaploaderService, UtilityService, SpaceRequisitionService, $filter, blockUI, $timeout, $ngConfirm) {

    if (window.innerWidth <= 780) {

        document.getElementById("SyestemViewBooking").style.display = "none";
        document.getElementById("Occupied").style.innerWidth = "150px";
        document.getElementById("SU").style.innerWidth = "150px";
    } else {

        document.getElementById("MobileViewBooking").style.display = "none";
    }

    $scope.mobileCC = {};
    $scope.SelLayers = [];
    $scope.Markers = [];
    $scope.marker = {};
    $scope.pageSize = '1000';
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.SpaceAlloc = {};
    $scope.SpaceAlloc.Verticals = [];
    $scope.Verticals = [];
    $scope.CostCenters = [];
    $scope.AurNames = [];
    $scope.Shifts = [];
    $scope.VLANS = [];
    $scope.EmpDetails = [];
    $scope.SpaceAlloc.CostCenters = [];
    $scope.selectedSpaces = [];
    $scope.Markerdata = [];
    $scope.Items = [];
    $scope.SubItems = [];
    $scope.SelectedEmp = [];
    $scope.Maploader = [];
    // $scope.EmpAllocSeats = [];
    //$scope.FilterVar = "EmpSearch";
    $scope.ddldisplay = false;
    $scope.detailsdisplay = false;
    $scope.nrmaldisplay = true;
    $scope.MarkerLblLayer = [];
    $scope.MarkerMeetingLblLayer = [];
    $scope.SpcCountArea = [];
    $scope.Empcount = [];
    $scope.OnEmpAllocSeats = false;
    $scope.inactive = true;
    $scope.LastRefresh = false;
    $scope.hotdeskspecifications = true;
    $('#btnAllocateSeats').attr('disabled', false);
    $scope.FloorID;
    $scope.floorDetails = [];
    $scope.EmpSearchItems = [{ CODE: 'AUR_ID', NAME: 'User ID' },
    { CODE: 'AUR_KNOWN_AS', NAME: 'Name' },
    { CODE: 'AUR_EMAIL', NAME: 'Email ID' },
    { CODE: 'SPC_ID', NAME: 'Space ID' }];
    $scope.ddlItem = "verticalalloc";
    var cnt = 0;
    $scope.Grade_Allow_status = '';
    $scope.GradeDetails = '';
    $scope.selectedSpacesLength = 0;
    $scope.SeatPopUpDetails = '';
    $scope.FlipKartSmallChanges = '';
    $scope.LibertyChanges = '';
    $scope.SeatAllocForSpace = [];

    if (moment().format("HH") < 18) {
        $scope.Maploader.FROM_DATE1 = moment().format('MM/DD/YYYY');
        $scope.Maploader.TO_DATE1 = moment().format('MM/DD/YYYY');
    } else {
        $scope.Maploader.FROM_DATE1 = moment().add(1, 'days').format('MM/DD/YYYY');
        $scope.Maploader.TO_DATE1 = moment().add(1, 'days').format('MM/DD/YYYY');
    }

    $scope.Maploader.Search_SH_CODE = "";

    $scope.Maploader.Search_MO_TYPE = "";

    $scope.Maploader.SH_CODE = "";


    $scope.Monitors = [];

    MaploaderService.getMonitor().then(function (result) {
        $scope.Monitors = result.data;
    })


    //setTimeout(function () {
    //    UtilityService.GetRoleAndReportingManger().then(function (response) {
    //        if (response.data != null) {
    //            $scope.GetRoleAndRM = response.data;

    //            if ($scope.HideDeatilsOnMap.SYSP_VAL1 == 1 && $scope.GetRoleAndRM[0].AUR_ROLE != 1) {
    //                $("#accordion").fadeOut();
    //                $scope.mapPerDetails = false;
    //                $scope.ShiftSelect = "True";
    //            }
    //            else {
    //                $("#accordion").fadeIn();
    //                $scope.mapPerDetails = true;
    //                $scope.ShiftSelect = "False";
    //            }
    //        }
    //    });
    //}, 100);
    var LOADLAYERS;
    var map = L.map('leafletMap');
    $scope.Markerdata = new L.FeatureGroup();

    $scope.MarkerLblLayer = new L.FeatureGroup();
    $scope.MarkerLblLayer_copy = new L.FeatureGroup();

    $scope.MarkerMeetingLblLayer = new L.FeatureGroup();
    $scope.MarkerMeetingLblLayer_copy = new L.FeatureGroup();
    //new layer addition
    $scope.MarkerLblLayerEmpDetails = new L.FeatureGroup();
    $scope.MarkerLblLayerEmpDetails_copy = new L.FeatureGroup();

    $scope.MarkerLblLayerSpace = new L.FeatureGroup();
    $scope.MarkerLblLayerSpace_copy = new L.FeatureGroup();

    //$scope.MarkerLblEmpNames = new L.FeatureGroup();
    //$scope.MarkerLblEmpNames_copy = new L.FeatureGroup();

    map.addLayer($scope.Markerdata);
    //var overlayMaps = {};
    var overlayMaps = {
        "Markers": $scope.Markerdata,
        "Spaceid's & Emp Details": $scope.MarkerLblLayer,
        "Emp Details": $scope.MarkerLblLayerEmpDetails,
        "Spaceid's": $scope.MarkerLblLayerSpace
    };
    L.control.layers({}, overlayMaps).addTo(map);

    var prefval;
    var OccupiedStyle;
    var VacantStyle;
    $scope.GradeAlloc = [];

    UtilityService.getSysPreferences().then(function (response) {

        if (response.data != null) {
            $scope.SysPref = response.data;
        }
        prefval = _.find($scope.SysPref, { SYSP_CODE: "display emp details on map" });
        var sysval = _.find($scope.SysPref, { SYSP_CODE: "Seat Color in red for Vacant" });
        var hotdesk = _.find($scope.SysPref, { SYSP_CODE: "Hot Desk Specifications" });

        $scope.GradeAlloc = _.find($scope.SysPref, { SYSP_CODE: "Grade Wise Allocation" });
        $scope.SeatPopUpDetails = _.find($scope.SysPref, { SYSP_CODE: "Seat details on Map" });
        $scope.FlipKartSmallChanges = _.find($scope.SysPref, { SYSP_CODE: "Flipkart Small Changes" });
        $scope.LibertyChanges = _.find($scope.SysPref, { SYSP_CODE: "Liberty Changes" });
        $scope.HideBlocking = _.find($scope.SysPref, { SYSP_CODE: "Hide Blocking" });
        $scope.VerticalWiseAlloc = _.find($scope.SysPref, { SYSP_CODE: "Booking Based on Vertical" });
        $scope.HideDeatilsOnMap = _.find($scope.SysPref, { SYSP_CODE: "HideDeatilsOnMap" });
        //$scope.MultipleBooking = _.find($scope.SysPref, { SYSP_CODE: "Multiple Booking" });

        $("#accordion").fadeOut();
        UtilityService.GetRoleAndReportingManger().then(function (response) {
            if (response.data != null) {
                $scope.GetRoleAndRM = response.data;

                if ($scope.HideDeatilsOnMap.SYSP_VAL1 == 1 && $scope.GetRoleAndRM[0].AUR_ROLE != 1) {
                    $("#accordion").fadeOut();
                    $scope.mapPerDetails = false;
                    $scope.ShiftSelect = "True";
                }
                else {
                    $("#accordion").fadeIn();
                    $scope.mapPerDetails = true;
                    $scope.ShiftSelect = "False";
                }
            };
        });
        UtilityService.GetRoleAndReportingManger().then(function (response) {
            if (response.data != null) {
                $scope.GetRoleAndRM = response.data;

                if ($scope.HideDeatilsOnMap.SYSP_VAL1 == 1 && $scope.GetRoleAndRM[0].AUR_ROLE != 1) {
                    $("#accordion").fadeOut();
                    $scope.mapPerDetails = false;
                    $scope.VLANSelect = "True";
                }
                else {
                    $("#accordion").fadeIn();
                    $scope.mapPerDetails = true;
                    $scope.VLANSelect = "False";
                }
            };
        });

        if ($scope.VerticalWiseAlloc.SYSP_VAL1 == 1) {
            $scope.UptoVetrialRelease = true;
            $scope.EmployeeRelease = false;
        }
        else {
            $scope.UptoVetrialRelease = false;
            $scope.EmployeeRelease = true;
        }

        if (hotdesk.SYSP_VAL1 == 1) {
            $scope.hotdeskspecifications = false;
        }
        else {
            $scope.hotdeskspecifications = true;
        }
        if (sysval.SYSP_VAL1 == 1) {
            document.getElementById("Occupied").className = "btn btn-success";
            document.getElementById("Vacant").className = "btn btn-danger";
            document.getElementById("occupiedstatus").className = "label label-success pull-right countVal Vacant";
            document.getElementById("Vacantstatus").className = "label label-danger pull-right countVal Occupied";
            OccupiedStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
            VacantStyle = { fillColor: '#E74C3C', opacity: 0.8, fillOpacity: 0.8 };
            $scope.LastRefresh = true;
        }
        else {
            document.getElementById("Occupied").className = "btn btn-danger";
            document.getElementById("Vacant").className = "btn btn-success";
            document.getElementById("occupiedstatus").className = "label label-danger pull-right countVal Occupied";
            document.getElementById("Vacantstatus").className = "label label-success pull-right countVal Vacant";
            OccupiedStyle = { fillColor: '#E74C3C', opacity: 0.8, fillOpacity: 0.8 };
            VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
            $scope.LastRefresh = false;
        }
    });


    MaploaderService.GetFlrIdbyLoc().then(function (response) {
        progress(0, 'Loading...', true);
        $scope.LoadMap(GetParameterValues('flr_code'));
    }, function (error) {
    });

    UtilityService.GetEmployee().then(function (response) {
        if (response.data != null) {
            $scope.Employee = response.data;
        }
    });
    //document.getElementById('SU').className = 'btn btn-info';


    var fullscreen = map.addControl(new L.Control.Fullscreen());


    var AllocateStyle = { fillColor: '#3498DB', opacity: 0.8, fillOpacity: 0.8 };
    var ReservedStyle = { fillColor: '#17202A', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };
    var LeaveStyle = { fillColor: '#800080', opacity: 0.8, fillOpacity: 0.8 };
    var BlockStyle = { fillColor: '#000000', opacity: 0.8, fillOpacity: 0.8 };
    var CstVer = { fillColor: '#c47750', opacity: 0.8, fillOpacity: 0.8 };
    var InactiveStyle = { fillColor: '#a4adbc', opacity: 0.8, fillOpacity: 0.8 };

    //Initialize map

    //This is currently the only way to get access to the drawnItems feature group which
    //is required to implement the edit functionality

    /// Refresh - resets the markers button
    var refresh = L.easyButton('fa fa-refresh', function () {
        $scope.selectedSpaces = [];
        $scope.SelectedType = "";
        $scope.SelectedID = "";
        $scope.LoadDet = [];
        $scope.ddldisplay = false;
        $scope.FilterVar = "";

        $scope.$apply(function () {
            if ($('#divSearch').css('display') != 'none' && $('#accordion').css('display') == 'none') {
                $('#divSearch').hide();
                $('#accordion').show();
            }
        });

        reloadMarkers();
        map.fitBounds($scope.bounds);
        //$scope.$apply(function () {
        $scope.ddlItem = "verticalalloc";
        //});
        //$scope.loadThemes();
        updateSummaryCount();
    }, "Refresh");
    refresh.addTo(map);

    //// Filter button
    var selectmap = L.easyButton('fa fa-filter', function () {
        $scope.$apply(function () {
            if ($('#divSearch').css('display') != 'none' && $('#accordion').css('display') == 'none') {
                $('#divSearch').hide();
            }
            else
                $('#accordion').toggle("slow");

            setTimeout(function () {
                $('#divFilter').toggle("slow");
            }, 100);
        });
    }, "Select Map");
    selectmap.addTo(map);

    ///// View Grid button
    var filter = L.easyButton('fa fa-list-ul', function () {
        $scope.$apply(function () {
            $scope.FilterVar = "spcList";

            $('.slide_content').slideToggle();
        });
    }, "Search Filter");
    filter.addTo(map);

    /// Allocate button
    var allocate = L.easyButton('fa fa-user fa-3x', function () {
        $scope.validate = false;
        progress(0, 'Loading...', true);
        //$scope.enddateselection = '';
        if ($scope.selectedSpaces.length != 0) {

            var role = _.find($scope.GetRoleAndRM);
            if (role.AUR_ROLE == 6) {
                //hide row
                $scope.SearchEmployee = false;
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('AUR_SEARCH', false);
            }
            else {
                $scope.SearchEmployee = true;
            }
            if ($scope.FlipKartSmallChanges.SYSP_VAL1 == 1) {
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('SPC_TYPE_NAME', false);
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('SST_NAME', false);
                //$scope.gridSpaceAllocOptions.columnApi.setColumnVisible('SH_CODE', false);
            }

            if ($scope.HideBlocking.SYSP_VAL1 == 1) {
                if ($scope.HideBlocking.SYSP_VAL2 == 1) {
                    $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('Allocate_Other', false);
                }
                //if (role.AUR_ROLE == 6 || role.AUR_ROLE == 0) {
                //$scope.gridSpaceAllocOptions.columnApi.setColumnVisible('blocked', false);
                // }
            }


            if ($scope.selectedSpaces[0].SPACE_TYPE == 4) {
                $scope.GeneralSeatType = false;
                $scope.HotdeskSeatType = true;
                $('#HotDesking').show();
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('SH_CODE', false);
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('FROM_TIME', true);
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('TO_TIME', true);
                //$scope.gridSpaceAllocOptions.columnApi.setColumnVisible('blocked', false);


            }
            else {
                $scope.HotdeskSeatType = false;
                $scope.GeneralSeatType = true;
                $('#HotDesking').hide();
                $scope.hotdesking = 0;
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('SH_CODE', true);
                //$scope.gridSpaceAllocOptions.columnApi.setColumnVisible('blocked', true);             

                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('FROM_TIME', false);
                $scope.gridSpaceAllocOptions.columnApi.setColumnVisible('TO_TIME', false);
            }




            progress(0, '', false);
            $('#SeatAllocation').modal('show');
            var myInput = $("input[name=data_aur_search]");
            var oldValue = myInput.val();
            myInput.val('');
            myInput.trigger('input');
        }
        else {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select atleast one seat to allocate / release");
        }
    }, "Book / Release seat");
    allocate.addTo(map);

    var search = L.easyButton('fa fa-search', function () {
        $scope.$apply(function () {
            if ($('#divFilter').css('display') != 'none' && $('#accordion').css('display') == 'none') {
                $('#divFilter').hide();
            }
            else
                $('#accordion').toggle("slow");
            setTimeout(function () {
                $('#divSearch').toggle("slow");
            }, 100);
        });
    }, "Search Employee");
    search.addTo(map);

    var inactive = L.easyButton('fa fa-check-square-o', function () {
        $scope.validate = false;
        progress(0, 'Loading...', true);
        var role = _.find($scope.GetRoleAndRM);

        if (role.AUR_ROLE != 1) {
            progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', 'Employee cannot Inactive Spaces');
            return;
        }
        else {
            $scope.GetInactiveSpaces();
        }
    }, "Inactive Spaces");
    inactive.addTo(map);

    //L.easyPrint({
    //    title: 'Print Map',
    //    elementsToHide: '.gitButton, .easy-button-button, .leaflet-control-zoom, .leaflet-control-layers-overlays'

    //}).addTo(map);
    var printcount = 0;
    var print1 = L.easyButton('fa fa-print', function () {
        printcount = printcount + 1;

        progress(0, '', true);
        $timeout(function () {
            $scope.Themes = true;
            $scope.Capacity = true;
            $scope.WorkArea = true;
            $scope.OtherArea = true;
            $("#divSeatStatus").collapse("show");
        }, 500);

        refresh.disable();
        selectmap.disable();
        filter.disable();
        allocate.disable();
        search.disable();
        inactive.disable();
        print1.disable();
        print2.disable();


        if (map.hasLayer($scope.MarkerLblLayer)) {
            map.removeLayer($scope.MarkerLblLayer);
            map.removeLayer($scope.MarkerLblLayerSpace);
            map.removeLayer($scope.MarkerLblLayerEmpDetails);
            map.removeLayer($scope.MarkerLblLayerSpace_copy);
            map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);

            map.addLayer($scope.MarkerLblLayer_copy);
        }
        else if (map.hasLayer($scope.MarkerLblLayerSpace)) {
            map.removeLayer($scope.MarkerLblLayerSpace);
            map.removeLayer($scope.MarkerLblLayer);
            map.removeLayer($scope.MarkerLblLayerEmpDetails);
            map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);
            map.removeLayer($scope.MarkerLblLayer_copy);

            map.addLayer($scope.MarkerLblLayerSpace_copy);
        }
        else if (map.hasLayer($scope.MarkerLblLayerEmpDetails)) {

            map.removeLayer($scope.MarkerLblLayerEmpDetails);
            map.removeLayer($scope.MarkerLblLayer);
            map.removeLayer($scope.MarkerLblLayerSpace);
            map.removeLayer($scope.MarkerLblLayer_copy);
            map.removeLayer($scope.MarkerLblLayerSpace_copy);

            map.addLayer($scope.MarkerLblLayerEmpDetails_copy);
        }
        else {
            map.addLayer($scope.MarkerLblLayerSpace_copy);
        }

        //}, 500);
        $timeout(function () {
            if (cnt == 0 && LOADLAYERS == 1) {
                $scope.LoadMapLayers(GetParameterValues('flr_code'));
                setTimeout(function () {
                    $("#divSeatStatus").collapse("hide");
                    map.removeLayer($scope.MarkerLblLayer_copy);
                    map.removeLayer($scope.MarkerLblLayerSpace_copy);
                    map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);
                    $scope.Themes = false;
                    $scope.Capacity = false;
                    $scope.WorkArea = false;
                    $scope.OtherArea = false;

                    refresh.enable();
                    selectmap.enable();
                    filter.enable();
                    allocate.enable();
                    search.enable();
                    inactive.enable();
                    print1.enable();
                    print2.enable();
                    $scope.LoadMarkerLayers($scope.floorDetails);
                }, 1000);
                cnt = cnt + 1;
            }
            else {
                setTimeout(function () {
                    $scope.MarkerMeetingLblLayer.clearLayers();
                    print('leafletMap')
                }, 1000);
                setTimeout(function () {
                    $("#divSeatStatus").collapse("hide");
                    map.removeLayer($scope.MarkerLblLayer_copy);
                    map.removeLayer($scope.MarkerLblLayerSpace_copy);
                    map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);
                    $scope.Themes = false;
                    $scope.Capacity = false;
                    $scope.WorkArea = false;
                    $scope.OtherArea = false;

                    refresh.enable();
                    selectmap.enable();
                    filter.enable();
                    allocate.enable();
                    search.enable();
                    inactive.enable();
                    print1.enable();
                    print2.enable();
                    $scope.LoadMarkerLayers($scope.floorDetails);
                }, 1000);

            }


        }, 1000);

    }, "Print Map");
    print1.addTo(map);

    //L.easyButton('fa fa-print', function () {
    //    printWithoutHeaderfooter('leafletMap');

    //}, "Print Only Layout").addTo(map);

    var print2 = L.easyButton('fa fa-print', function () {
        printcount = printcount + 1;
        progress(0, '', true);

        $timeout(function () {
            $scope.Themes = true;
            $scope.Capacity = true;
            $scope.WorkArea = true;
            $scope.OtherArea = true;
            $scope.showStatusPanel = false;
        }, 500);
        refresh.disable();
        selectmap.disable();
        filter.disable();
        allocate.disable();
        search.disable();
        inactive.disable();
        print1.disable();
        print2.disable();
        if (map.hasLayer($scope.MarkerLblLayer)) {
            map.removeLayer($scope.MarkerLblLayer);
            map.removeLayer($scope.MarkerLblLayerSpace);
            map.removeLayer($scope.MarkerLblLayerEmpDetails);
            map.removeLayer($scope.MarkerLblLayerSpace_copy);
            map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);

            map.addLayer($scope.MarkerLblLayer_copy);
        }
        else if (map.hasLayer($scope.MarkerLblLayerSpace)) {
            map.removeLayer($scope.MarkerLblLayerSpace);
            map.removeLayer($scope.MarkerLblLayer);
            map.removeLayer($scope.MarkerLblLayerEmpDetails);
            map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);
            map.removeLayer($scope.MarkerLblLayer_copy);

            map.addLayer($scope.MarkerLblLayerSpace_copy);
        }
        else if (map.hasLayer($scope.MarkerLblLayerEmpDetails)) {

            map.removeLayer($scope.MarkerLblLayerEmpDetails);
            map.removeLayer($scope.MarkerLblLayer);
            map.removeLayer($scope.MarkerLblLayerSpace);
            map.removeLayer($scope.MarkerLblLayer_copy);
            map.removeLayer($scope.MarkerLblLayerSpace_copy);

            map.addLayer($scope.MarkerLblLayerEmpDetails_copy);
        }
        else {
            map.addLayer($scope.MarkerLblLayerSpace_copy);
        }
        $timeout(function () {
            if (cnt == 0 && LOADLAYERS == 1) {
                $scope.LoadMapLayers(GetParameterValues('flr_code'));
                setTimeout(function () {
                    $scope.showStatusPanel = true;
                    map.removeLayer($scope.MarkerLblLayer_copy);
                    map.removeLayer($scope.MarkerLblLayerSpace_copy);
                    map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);
                    $scope.Themes = false;
                    $scope.Capacity = false;
                    $scope.WorkArea = false;
                    $scope.OtherArea = false;

                    refresh.enable();
                    selectmap.enable();
                    filter.enable();
                    allocate.enable();
                    search.enable();
                    inactive.enable();
                    print1.enable();
                    print2.enable();
                    $scope.LoadMarkerLayers($scope.floorDetails);
                }, 1000);
                cnt = cnt + 1;
            }
            else {
                setTimeout(function () {
                    $scope.MarkerMeetingLblLayer.clearLayers();
                    print('leafletMap')

                }, 1000);
                setTimeout(function () {
                    $scope.showStatusPanel = true;
                    map.removeLayer($scope.MarkerLblLayer_copy);
                    map.removeLayer($scope.MarkerLblLayerSpace_copy);
                    map.removeLayer($scope.MarkerLblLayerEmpDetails_copy);
                    $scope.Themes = false;
                    $scope.Capacity = false;
                    $scope.WorkArea = false;
                    $scope.OtherArea = false;

                    refresh.enable();
                    selectmap.enable();
                    filter.enable();
                    allocate.enable();
                    search.enable();
                    inactive.enable();
                    print1.enable();
                    print2.enable();
                    $scope.LoadMarkerLayers($scope.floorDetails);
                }, 1000);
            }

        }, 1000);

    }, "Print Only Layout");
    print2.addTo(map);

    $scope.binddata = function (data) {
        var selectedData
        selectedData = _.find($scope.AurNames, { AUR_ID: data.AUR_SEARCH });
        if (selectedData != undefined) {
            data.AUR_ID = selectedData.AUR_ID;
            data.AUR_NAME = selectedData.AUR_NAME;
            data.VERTICAL = selectedData.VER_CODE;
            data.VER_NAME = selectedData.VER_NAME;
            data.Cost_Center_Code = selectedData.Cost_Center_Code;
            data.Cost_Center_Name = selectedData.Cost_Center_Name;
            $scope.gridSpaceAllocOptions.api.refreshView();
            $scope.EmpChange(data, data.AUR_SEARCH);
            $scope.mobileCC = selectedData;
            $scope.selectedSpaces[0].AUR_NAME = selectedData.AUR_NAME;
        }
    }

    $scope.filterdata = function (data) {
    }

    $scope.columDefsAlloc = [
        { headerName: "Select", field: "ticked", width: 50, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-disabled='data.disabled' ng-model='data.ticked' ng-change='chkChanged(data)'>", pinned: 'left' }, // headerCellRenderer: headerCellRendererFunc, 
        { headerName: "Space ID", field: "SPC_NAME", width: 100, cellClass: "grid-align", pinned: 'left' },
        //{ headerName: "Space Type", field: "SPC_TYPE_NAME", width: 110, cellClass: "grid-align" },
        //{ headerName: "Space Sub Type", field: "SST_NAME", width: 110, cellClass: "grid-align" },
        {
            headerName: "Search", cellClass: "grid-align", width: 100, filter: 'set', field: "AUR_SEARCH",
            template: "<input list='employees' data-ng-change='binddata(data)' ng-model='data.AUR_SEARCH' /><datalist  id='employees' ><option ng-repeat='Employee in AurNames  | filter:{ VER_CODE:data.VERTICAL }'  value='{{Employee.AUR_ID}}' text='{{Employee.AUR_NAME}}'>{{Employee.AUR_NAME}} </option> </datalist>"
        },
        {
            /*  headerName: "Employee", cellClass: "grid-align", width: 110, filter: 'set', field: "AUR_NAME", cellRenderer: customEditor*/
            headerName: "Employee", cellClass: "grid-align", width: 110, filter: 'set', field: "AUR_NAME"
        },
        {
            /*  headerName: "Ver", width: 110, cellClass: "grid-align", field: "VER_NAME", cellRenderer: customEditor*/
            headerName: "Ver", width: 110, cellClass: "grid-align", field: "VER_NAME"
        },
        {
            /*   headerName: "Cost", width: 110, cellClass: "grid-align", field: "Cost_Center_Name", cellRenderer: customEditor*/
            headerName: "Cost", width: 110, cellClass: "grid-align", field: "Cost_Center_Name"
        },
        {
            headerName: "From Date  <br />(mm/dd/yyyy)", field: "FROM_DATE", width: 100, cellClass: 'grid-align'
            /*   headerName: "From Date  <br />(mm/dd/yyyy)", field: "FROM_DATE", width: 100, cellClass: 'grid-align', cellRenderer: createFromDatePicker*/
        },
        {
            headerName: "To Date  <br />(mm/dd/yyyy)", field: "TO_DATE", width: 100, cellClass: 'grid-align'
            /*   headerName: "To Date  <br />(mm/dd/yyyy)", field: "TO_DATE", width: 100, cellClass: 'grid-align', cellRenderer: createToDatePicker*/
            //cellRenderer: (invNum) => "<input type='text' id='TO_DATE' data-ng-click ='GetToDate()' value='${invNum.value}'>"
        },
        { headerName: "From Time", field: "FROM_TIME", width: 100, cellClass: 'grid-align', cellRenderer: createFromTimePicker },
        { headerName: "To Time", field: "TO_TIME", width: 100, cellClass: 'grid-align', cellRenderer: createToTimePicker },
        //{

        //    headerName: "Shift Type", field: "SH_CODE", width: 100, cellClass: "grid-align", filter: 'set',
        //    template: "<select data-ng-change='shiftChange(data)' ng-model='data.SH_CODE'><option ng-repeat='Shift in ShiftData' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>"
        //},
        {
            headerName: "Shift Type", field: "SH_CODE", width: 100, cellClass: "grid-align", filter: 'set',
            template: "<select data-ng-change='shiftChange(data)' ng-model='data.SH_CODE'><option value=''>--Select--</option><option ng-repeat='Shift in ShiftData' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>"
        },

        {
            headerName: "VLAN Type", field: "VL_Code", width: 100, cellClass: "grid-align", filter: 'set',
            template: "<select data-ng-change='VLANChange(data)' ng-model='data.VL_Code'><option value=''>--Select--</option><option ng-repeat='VLAN in VLANData' value='{{VLAN.VL_Code}}'>{{VLAN.VL_Name}}</option></select>"
        },
        //{ headerName: "Blocking", field: "blocked", width: 60, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-disabled='data.block' ng-model='data.blocked' ng-change='blkChanged(data)'>" }, // headerCellRenderer: headerCellRendererFunc, 
        {
            headerName: "", field: "Allocate_Other", width: 80, cellClass: "grid-align", filter: 'set',
            template: "<a href='' data-ng-click ='AllocAnother(data)'>Allocate Other</a>"
        }
    ]

    $scope.MObcolumDefsAlloc = [
        { headerName: "Select", field: "ticked", width: 50, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-disabled='data.disabled' ng-model='data.ticked' ng-change='chkChanged(data)'>", pinned: 'left' }, // headerCellRenderer: headerCellRendererFunc, 
        { headerName: "Space ID", field: "SPC_NAME", width: 100, cellClass: "grid-align", pinned: 'left' },
        {
            headerName: "Search", cellClass: "grid-align", width: 100, filter: 'set', field: "AUR_SEARCH",
            template: "<input list='employees' data-ng-change='binddata(data)' ng-model='data.AUR_SEARCH' /><datalist  id='employees' ><option ng-repeat='Employee in AurNames' value='{{Employee.AUR_ID}}' text='{{Employee.AUR_NAME}}'>{{Employee.AUR_NAME}} </option> </datalist>"
        },
        { headerName: "Employee", cellClass: "grid-align", width: 110, filter: 'set', field: "AUR_NAME", cellRenderer: customEditor },
        { headerName: "From Date  <br />(mm/dd/yyyy)", field: "FROM_DATE", width: 100, cellClass: 'grid-align', cellRenderer: createFromDatePicker },
        {
            headerName: "To Date  <br />(mm/dd/yyyy)", field: "TO_DATE", width: 100, cellClass: 'grid-align', cellRenderer: createToDatePicker
            //cellRenderer: (invNum) => "<input type='text' id='TO_DATE' data-ng-click ='GetToDate()' value='${invNum.value}'>"
        },
        { headerName: "From Time", field: "FROM_TIME", width: 100, cellClass: 'grid-align', cellRenderer: createFromTimePicker },
        { headerName: "To Time", field: "TO_TIME", width: 100, cellClass: 'grid-align', cellRenderer: createToTimePicker },
        {
            headerName: "Shift Type", field: "SH_CODE", width: 100, cellClass: "grid-align", filter: 'set',
            template: "<select data-ng-change='shiftChange(data)' ng-model='data.SH_CODE'><option value=''>--Select--</option><option ng-repeat='Shift in Shifts' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>"
        },
        {
            headerName: "VLAN Type", field: "VL_Code", width: 100, cellClass: "grid-align", filter: 'set',
            template: "<select data-ng-change='VLANChange(data)' ng-model='data.VL_Code'><option value=''>--Select--</option><option ng-repeat='VLAN in VLANs' value='{{VLAN.VL_Code}}'>{{VLAN.VL_Name}}</option></select>"
        }
    ]
    text.style.display = "none";
    accordion.style.display = "block";
    $scope.myFunction = function () {
        var checkBox = document.getElementById("myCheck");
        var text = document.getElementById("text");
        if (checkBox.checked == true) {
            text.style.display = "block";
            accordion.style.display = "none";
        } else {

            text.style.display = "none";
            accordion.style.display = "block";
        }
    }

    MaploaderService.GET_VERTICAL_WISE_ALLOCATIONS(GetParameterValues('flr_code')).then(function (response) {

        if (response != null) {
            $scope.VerticalData = response.data;
            $scope.Vertical_Wise_Data = response.data1;
            $scope.Vertical_Wise_Total = response.data2;
        }
    });




    $scope.chkChanged = function (data) {
        if ($scope.GradeAlloc.SYSP_VAL1 == 1 && data.ticked == false && $scope.Eligibility != 'No') {
            $scope.selectedSpacesLength = $scope.selectedSpacesLength - 1;
            if ($scope.Allow_Remaining == undefined) {
                $scope.AllocateEnabledStatus = false;
            }
            else if ($scope.Allow_Remaining >= $scope.selectedSpacesLength) {
                $scope.AllocateEnabledStatus = false;
            }
            else {
                $scope.AllocateEnabledStatus = true;
            }
        } else {
            $scope.selectedSpacesLength = $scope.selectedSpacesLength + 1;
            if ($scope.Allow_Remaining == undefined) {
                $scope.AllocateEnabledStatus = false;
            }
            else if ($scope.Allow_Remaining >= $scope.selectedSpacesLength) {
                $scope.AllocateEnabledStatus = false;
            }
            else {
                $scope.AllocateEnabledStatus = true;
            }
        }
    }
    $scope.GetToDate = function () {


        prefvalue = _.find($scope.SysPref, { SYSP_CODE: "Restrict Allocation to 90 Days" });

        if (prefvalue == undefined) {


            $('#TO_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,


            })

        } else {
            $('#TO_DATE').datepicker('setStartDate', fromdate);
            $('#TO_DATE').datepicker('setEndDate', todate);
            $('#TO_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                startDate: fromdate,
                endDate: todate,


            })
        }
    }

    $scope.columDefSpaceAlloc = [
        { headerName: "Select", field: "ticked", width: 60, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-model='data.ticked' ng-change='ChkSpaceChanged(data)'>", pinned: 'left' }, // headerCellRenderer: headerCellRendererFunc, 
        { headerName: "Space ID", field: "SPACE_ID", width: 350, cellClass: "grid-align", pinned: 'left' },
        { headerName: "Space Type", field: "SPC_TYPE", width: 250, cellClass: "grid-align" },
        { headerName: "Space Sub Type", field: "SPC_SUB_TYPE", width: 239, cellClass: "grid-align" }];
    $scope.selectedActSpaces = [];

    $scope.ChkSpaceChanged = function (data) {
        $scope.selectedActSpaces.push(data);
    }

    $scope.GetInactiveSpaces = function () {

        if ($scope.selectedSpaces.length > 0) {
            $scope.selectedActSpaces = [];
            $('#Active').modal('show');
            angular.forEach($scope.selectedSpaces, function (value, key) {
                var spcdet = {};
                if (value.ticked == true) {
                    spcdet.SPACE_ID = value.SPC_ID;
                    spcdet.SPC_TYPE = value.SPC_TYPE_NAME;
                    spcdet.SPC_SUB_TYPE = value.SST_NAME;
                    spcdet.ticked = value.ticked;
                    $scope.selectedActSpaces.push(spcdet);
                }
            });
            setTimeout(function () {
                $scope.gridSpaceAllocation.api.setRowData([]);
                $scope.gridSpaceAllocation.api.setRowData($scope.selectedActSpaces);
                progress(0, '', false);
            }, 200);
        }
        else {
            var params = {
                flr_code: $scope.SearchSpace.Floor[0].FLR_CODE
            };
            MaploaderService.GetSpaceFromFloorMaps(params).then(function (data) {
                if (data.length != 0) {
                    $scope.gridata = data;
                    $scope.gridSpaceAllocation.api.setRowData([]);
                    $scope.gridSpaceAllocation.api.setRowData($scope.gridata);
                    progress(0, '', false);
                    $('#Active').modal('show');
                }
                else {
                    progress(0, '', false);
                    $('#Active').modal('hide');
                    showNotification('error', 8, 'bottom-right', 'No Inactive Spaces Found For The Selected Floor');
                }
            }, function (error) {
                console.log(error);
            });
        }

    }

    $scope.ActivateSpaces = function () {
        var flrcode = $scope.SearchSpace.Floor[0].FLR_CODE;
        var dataobj = { FLR_CODE: flrcode, SPC_LST: $scope.selectedActSpaces };
        MaploaderService.ActivateSpaces(dataobj).then(function (response) {
            if (response != null) {
                $('#Active').modal('hide');
                setTimeout(function () {
                    $scope.LoadMarkerLayers($scope.SearchSpace.Floor[0].FLR_CODE);
                    $scope.loadThemes();
                }, 500);
                showNotification('success', 8, 'bottom-right', 'Selected Spaces Has Been Active');
            }
        });

    };

    $scope.InactiveSpaces = function () {
        var flrcode = $scope.SearchSpace.Floor[0].FLR_CODE;

        var dataobj = { FLR_CODE: flrcode, SPC_LST: $scope.selectedActSpaces };
        MaploaderService.InactiveSeats(dataobj).then(function (response) {
            if (response != null) {
                angular.forEach($scope.selectedActSpaces, function (value, key) {
                    _.remove($scope.Markers, { SPC_ID: value.SPACE_ID });
                });
                reloadMarkers();
                $scope.selectedActSpaces = [];
                $scope.selectedSpaces = [];
                $('#Active').modal('hide');
                showNotification('success', 8, 'bottom-right', 'Selected Spaces Has Been Inactive');
                setTimeout(function () {
                    $scope.ShowTotalOnMap();
                    $scope.loadThemes();
                });
            }
        });
    };

    function customEditor(params) {

        params.data.SH_CODE = $scope.ShiftData[0].SH_CODE;



        var VLAN = _.find($scope.VLANData, { VL_Code: 'Def-VLAN' });

        params.data.VL_Code = VLAN.VL_Code;

        var editing = false;


        var eCell = document.createElement('div');
        var eLabel = document.createTextNode('Please Select');
        if (params.value)
            eLabel.nodeValue = params.value;
        eCell.appendChild(eLabel);

        var eSelect = document.createElement("select");
        eSelect.setAttribute("width", 50);
        var role = _.find($scope.GetRoleAndRM);
        if (role.AUR_ROLE == 6) {
            editing = true;
        }
        eCell.addEventListener('click', function () {
            if (!editing) {
                progress(0, 'Loading...', true);
                if (params.column.colId === "VER_NAME") {
                    removeOptions(eSelect);
                    var eOption = document.createElement("option");
                    //eSelect.appendChild(eOption);
                    angular.forEach($scope.Verticals, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.VER_CODE);
                        eOption.setAttribute("text", item.VER_NAME);
                        eOption.innerHTML = item.VER_NAME;
                        eSelect.appendChild(eOption);

                    });
                    progress(0, '', false);
                }
                else if (params.column.colId === "Cost_Center_Name") {
                    $scope.getCostcenterByVertical(params.data, eSelect);
                }
                else if (params.column.colId === "AUR_NAME" && params.data.Cost_Center_Code == "" && $scope.FlipKartSmallChanges.SYSP_VAL1 == 0) {
                    removeOptions(eSelect);
                    var eOption = document.createElement("option");
                    angular.forEach($scope.AurNames, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.AUR_ID);
                        eOption.setAttribute("text", item.AUR_NAME);
                        eOption.innerHTML = item.AUR_NAME;
                        eSelect.appendChild(eOption);



                    });
                    progress(0, '', false);

                }
                else {
                    if (params.data.Cost_Center_Code != "" && $scope.FlipKartSmallChanges.SYSP_VAL1 == 0)
                        $scope.cstChange(params.data, eSelect);
                }
                setTimeout(function () {
                    eSelect.value = params.value;

                    eCell.removeChild(eLabel);
                    eCell.appendChild(eSelect);
                    eSelect.focus();
                    editing = true;
                }, 200);
            }
        });

        eSelect.addEventListener('blur', function () {

            if (editing) {

                editing = false;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
            }
        });
        eSelect.addEventListener('change', function () {
            if (editing) {
                editing = false;

                var newValue = eSelect.options[eSelect.selectedIndex].text;
                eLabel.nodeValue = newValue;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
                if (params.column.colId === "VER_NAME") {
                    params.data.VERTICAL = eSelect.value;
                    params.data.VER_NAME = newValue;
                    $scope.getCostcenterByVertical(params.data, eSelect);
                }
                else if (params.column.colId === "Cost_Center_Name") {
                    var prefval = _.find($scope.SysPref, { SYSP_CODE: "LOB Change Alert" })

                    if (prefval.SYSP_VAL1 == 1) {

                        if ((eSelect.value != params.data.Cost_Center_Code) && (params.data.Cost_Center_Code != "" || params.data.Cost_Center_Code == null) && (params.data.SPACE_TYPE != "2")) {

                            showNotification('error', 40, 'bottom-right', 'Please Release,seat allocated to another LOB');

                            $('#btnAllocateSeats').attr('disabled', 'disabled');
                            eLabel.nodeValue = params.data.Cost_Center_Name;
                            eCell.appendChild(eLabel);
                        }
                        else {
                            $('#btnAllocateSeats').attr('disabled', false);
                            params.data.Cost_Center_Code = eSelect.value;
                            params.data.Cost_Center_Name = newValue;
                            $scope.cstChange(params.data, eSelect);
                        }
                    }
                    else {

                        params.data.Cost_Center_Code = eSelect.value;
                        params.data.Cost_Center_Name = newValue;
                        $scope.cstChange(params.data, eSelect);
                    }

                }
                else {
                    params.data.AUR_ID = eSelect.value;
                    params.data.AUR_NAME = newValue;
                    $scope.getVer_And_CC(params);
                    $scope.EmpChange(params.data, params.data.AUR_ID);
                }
            }
        });
        return eCell;
    }

    UtilityService.getVerticals(3).then(function (response) {

        if (response.data != null) {
            $scope.Verticals = response.data;
        }
    });

    UtilityService.getAurNames().then(function (response) {
        if (response.data != null) {
            $scope.AurNames = response.data;
            $scope.AurNames1 = angular.copy(response.data);
        }
        MaploaderService.getSpaceDaysRestriction().then(function (response) {
            if (response != null) {
                var count = response[0].RESTRICT_DAYS_COUNT;
                if (count > 0) {
                    $scope.enddateselection = '+' + (count - 1) + 'd';
                }
                else {
                    $scope.enddateselection = '';
                }
            }
        });
    });



    MaploaderService.getallFilterbyItem().then(function (response) {

        if (response != null) {
            $scope.Items = response;
        }
    });

    $scope.getCostcenterByVertical = function (data, eSelect) {
        $scope.CostCenters = [];
        data.STATUS = 1002;
        if (data.STACHECK != UtilityService.Added)
            data.STACHECK = UtilityService.Modified;
        else {
            data.SSA_SRNREQ_ID = null;
            data.SSAD_SRN_REQ_ID = null;
        }
        ///// removing options from select
        removeOptions(eSelect);
        var eOption = document.createElement("option");
        eSelect.appendChild(eOption);
        UtilityService.getCostcenterByVerticalcode({ VER_CODE: data.VERTICAL }, 2).then(function (response) {
            // $timeout(function () {
            $scope.CostCenters = response.data;


            angular.forEach(response.data, function (item, key) {
                var eOption = document.createElement("option");
                eOption.setAttribute("value", item.Cost_Center_Code);
                eOption.setAttribute("text", item.Cost_Center_Name);
                eOption.innerHTML = item.Cost_Center_Name;
                eSelect.appendChild(eOption);
            });
            progress(0, '', false);
            //  }, 500);
        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }

    $scope.getVer_And_CC = function (data) {

        var params = {
            AUR_ID: data.data.AUR_ID
        }
        MaploaderService.getVer_And_CC(params).then(function (response) {

            data.data.AUR_ID = response[0].AUR_ID;
            data.data.AUR_NAME = response[0].AUR_NAME;

            data.data.VERTICAL = response[0].VER_CODE;
            data.data.VER_NAME = response[0].VER_NAME;
            data.data.Cost_Center_Code = response[0].COST_CODE;
            data.data.Cost_Center_Name = response[0].Cost_Center_Name;
            $scope.gridSpaceAllocOptions.api.refreshView();

        }, function (error) {
            progress(0, '', false);
            console.log(error);

        })


    };



    $scope.EmpChange = function (data, OldVal) {
        //$scope.$apply(function () {
        $scope.OnEmpAllocSeats = true;
        //});
        //$scope.SelectedEmp.push({ AUR_ID: OldVal });
        var valueArr = $scope.gridSpaceAllocOptions.rowData.map(function (item) { return item.AUR_ID });

        var isDuplicate = valueArr.some(function (item, idx) {
            if (item != '')
                return valueArr.indexOf(item) != idx
        });

        //if (isDuplicate == true) {
        //    showNotification('error', 8, 'bottom-right', 'Employee Should Not be Same');
        //}
        //else {
        $scope.SelectedEmp = data;
        data.STATUS = 1004;
        if (data.STACHECK != UtilityService.Added)
            data.STACHECK = UtilityService.Modified;
        for (var i = 0; $scope.EmpDetails != null && i < $scope.EmpDetails.length; i += 1) {
            if ($scope.EmpDetails[i].AUR_ID === data.AUR_ID) {
                $scope.EmpDetails[i].ticked = true;
                data.ticked = true;
            }
        }
        var dataobj = { lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: data.AUR_ID, mode: 1 };
        MaploaderService.getEmpAllocSeat(dataobj).then(function (result) {
            if (result.data.length > 0) {
                //$scope.EmpAllocSeats.push(result.data[0]);
                // $scope.EmpAllocSeats.push(result.data);


                if ($scope.EmpAllocSeats.length > 0) {
                    //$scope.EmpAllocSeats = $scope.EmpAllocSeats.filter(function (value, index, arr) {
                    //    return value == data.aur;
                    //});
                    //$scope.EmpAllocSeats.map(function (v, i) {
                    //    if (v.AUR_ID === data.AUR_ID) {
                    //        $scope.EmpAllocSeats.pop(i);
                    //    }
                    //});
                    $scope.EmpAllocSeats = result.data;;
                }
                else {
                    $scope.EmpAllocSeats = result.data;
                }
            }

            //if ($scope.gridSpaceAllocOptions != undefined && $scope.gridSpaceAllocOptions.rowData.length > 0) {
            //    let filteredData;
            //    filteredData = $scope.gridSpaceAllocOptions.rowData.filter(function (o1) {
            //        //filter out (!) items in arr2
            //        return $scope.EmpAllocSeats.some(function (o2) {
            //            return o1.AUR_ID !== o2.AUR_ID;
            //        });
            //    });
            //    console.log(filteredData);

            //    //angular.forEach($scope.gridSpaceAllocOptions.rowData, function (item, key) {
            //    //    if (item.AUR_ID != data.AUR_ID) {
            //    //        let index = $scope.EmpAllocSeats.findIndex(record => record.AUR_ID === data.AUR_ID);
            //    //        $scope.EmpAllocSeats.slice(index, 1);
            //    //    }
            //    //});
            //}

            //if (window.innerWidth <= 780) {
            //    $scope.EmpAllocSeats = _.remove($scope.EmpAllocSeats, function (n) {
            //        debugger;
            //        return _.find($scope.mobileCC, { AUR_ID: n.AUR_ID });
            //    });
            //}
            //else {
            //    $scope.EmpAllocSeats = _.remove($scope.EmpAllocSeats, function (n) {
            //        debugger;
            //        return _.find($scope.gridSpaceAllocOptions.rowData, { AUR_ID: n.AUR_ID });
            //    });
            //}

            $scope.OnEmpAllocSeats = false;
            //$scope.EmpAllocSeats = _.uniqBy($scope.EmpAllocSeats, 'AUR_ID');
        });
        //}
    }

    $scope.blkChanged = function (data) {
        data.STATUS = 1052;
        if (data.STACHECK != UtilityService.Added)
            data.STACHECK = UtilityService.Modified;
    }

    function removeOptions(selectbox) {
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }

    $scope.cstChange = function (data, eSelect) {
        data.STATUS = 1003;
        if (data.STACHECK != UtilityService.Added)
            data.STACHECK = UtilityService.Modified;
        data.AUR_ID = "";
        data.AUR_NAME = "";
        ///// removing options from select
        removeOptions(eSelect);
        var eOption = document.createElement("option");
        eOption.setAttribute("text", "Please select");
        eSelect.appendChild(eOption);

        var dataobj = {
            lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE,
            Item: data.VERTICAL, subitem: data.Cost_Center_Code
        };
        MaploaderService.getEmpDetails(dataobj).then(function (response) {
            if (response.data != null) {
                if (response.data.length != 0) {
                    $scope.EmpDetails = response.data;
                    angular.forEach($scope.EmpDetails, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.AUR_ID);
                        eOption.setAttribute("text", item.AUR_NAME);
                        eOption.innerHTML = item.AUR_NAME;
                        eSelect.appendChild(eOption);
                        if (data.AUR_ID === item.AUR_ID) {
                            eSelect.value = data.AUR_ID;
                            data.ticked = true;
                            item.ticked = true;
                        }
                        if (_.find($scope.Markers, { AUR_ID: item.AUR_ID }))
                            eOption.disabled = true;
                        progress(0, '', false);
                    });
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'No Employee Found');
                }
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message + $scope.BsmDet.Child);
            }

        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }
    $scope.checkshiftvalue = 0;

    $scope.shiftChange = function (data) {
        if (data.STATUS != 1 && (data.SSA_SRNREQ_ID != "" || data.FROM_DATE != "")) {
            MaploaderService.spcAvailabilityByShift(data).then(function (response) {
                if (response.data == null) {
                    //$scope.checkshiftvalue = 1;
                    //$('#allocatespacebtn').prop("disabled", true);      
                    $scope.AllocateEnabledStatus = true;
                    showNotification('error', 8, 'bottom-right', response.Message);
                }
                else {
                    if ($scope.GradeAlloc.SYSP_VAL1 == 1 && response.data != null) {
                        $scope.GradeAllocStatus();
                    }
                    else {
                        $scope.AllocateEnabledStatus = false;
                        //$('#allocatespacebtn').prop("disabled", false);
                    }
                }
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.FilterSpaces = function () {
        progress(0, 'Loading...', true);
        $scope.LoadMap($scope.SearchSpace.Floor[0].FLR_CODE);
        updateSummaryCount();
        $('#divFilter').toggle("slow");
        setTimeout(function () {
            $('#accordion').toggle("slow");
        }, 100)
        //$('#divFilter').toggle("slow");
        //$('#accordion').toggle("slow");
    }

    function createFromTimePicker(params) {
        var editing = false;
        var newTime;
        newTime = document.createElement('input');
        newTime.setAttribute('ng-model', 'data.FROM_TIME');
        newTime.type = "text";
        newTime.id = 'fromtime' + params.rowIndex;
        newTime.className = "pickDate";
        $(newTime).timepicker(
            {
                timeFormat: 'H:i',
                show2400: true
            }).on("input change", function (e) {
                var date = _.find($scope.SysPref, { SYSP_CODE: "BUSSTIME" });
                var total = parseInt(date.SYSP_VAL2) - parseInt(date.SYSP_VAL1);
                var endTime = moment(e.target.value, 'HH:mm').add(total, 'hours').format('HH:mm');
                var selectedspaceexist = _.find($scope.selectedSpaces,
                    function (x) {
                        if (x.FROM_TIME === e.target.value) {

                            return x
                        }
                    });
                if (selectedspaceexist) {
                    showNotification('error', 8, 'bottom-right', "Time slot not available");
                    e.target.value = "";
                    return;
                }
                params.data.TO_TIME = endTime;
            });
        return newTime;
    }

    function createToTimePicker(params) {
        var editing = false;
        var newTime;
        newTime = document.createElement('input');
        newTime.setAttribute('ng-model', 'data.TO_TIME');
        newTime.type = "text";
        newTime.id = 'totime' + params.rowIndex;
        newTime.className = "pickDate";
        $(newTime).timepicker(
            {
                timeFormat: 'H:i',
                show2400: true
            }).on("input change", function (e) {
                var date = _.find($scope.SysPref, { SYSP_CODE: "BUSSTIME" });
                var total = parseInt(date.SYSP_VAL2) - parseInt(date.SYSP_VAL1);
                var endTime1 = moment(params.data.FROM_TIME, 'HH:mm').add(total, 'hours').format('HH:mm');
                var currentTime = moment(e.target.value, "HH:mm a");
                var startTime = moment(params.data.FROM_TIME, "HH:mm a");
                var endTime = moment(endTime1, "HH:mm a");
                currentTime.toString();
                startTime.toString();
                var c = currentTime.isBetween(startTime, endTime);  // false
                if (!currentTime.isBetween(startTime, endTime)) {
                    showNotification('error', 8, 'bottom-right', "To time should not be less than from time and not exceed business hours");
                }
            });
        return newTime;
    }





    var fromdate, todate;
    function createFromDatePicker(params) {

        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.FROM_DATE');
        newDate.type = "text";
        newDate.readOnly = true;
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        fromdate = params.data.FROM_DATE;
        todate = params.data.TO_DATE;
        $('#TO_DATE').val(params.data.TO_DATE);

        $('#TO_DATE').datepicker({

            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
            startDate: new Date(),


        })



        $(newDate).datepicker({
            //format: 'dd M, yyyy',
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
            startDate: new Date(),
            endDate: $scope.enddateselection



        }).on("input change", function (e) {
            prefvalue = _.find($scope.SysPref, { SYSP_CODE: "Restrict Allocation to 90 Days" });
            //Santharaju Start
            if (prefvalue != undefined && e.target.value != undefined && e.target.value != "" && params.data.FROM_DATE != e.target.value) {
                /*params.data.FROM_DATE = e.target.value;*/
                if ($scope.gridSpaceAllocOptions != undefined && $scope.gridSpaceAllocOptions.rowData.length > 0) {
                    angular.forEach($scope.gridSpaceAllocOptions.rowData, function (item) {
                        if (item.AUR_ID == params.data.AUR_ID && new Date(item.FROM_DATE) <= new Date(e.target.value) && new Date(item.TO_DATE) >= new Date(e.target.value)) {
                            showNotification('error', 8, 'bottom-right', 'Seat already booked within selected date range');
                            params.data.FROM_DATE = fromdate;
                            params.data.TO_DATE = todate;
                            e.target.value = "";
                            return;
                        }
                    });
                }
                if ($scope.EmpAllocSeats.length > 0) {
                    angular.forEach($scope.EmpAllocSeats, function (item) {
                        if (item.AUR_ID == params.data.AUR_ID && new Date(item.FROM_DATE) <= new Date(e.target.value) && new Date(item.TO_DATE) >= new Date(e.target.value)) {
                            showNotification('error', 8, 'bottom-right', 'Seat already booked within selected date range');
                            params.data.FROM_DATE = fromdate;
                            params.data.TO_DATE = todate;
                            e.target.value = "";
                            return;
                        }
                    });
                }
                //Santharaju End
                var checkdate = moment(e.target.value).add(0, 'day').format('MM/DD/YYYY');


                $('#TO_DATE').val(checkdate);
                fromdate = e.target.value;
                todate = checkdate;
                params.data.TO_DATE = checkdate;
                var d = e.target.value;

                $('#TO_DATE').datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                    todayHighlight: true,
                    startDate: d,
                    endDate: checkdate,
                    //maxDate: '10/19/2019',

                })

                $('#TO_DATE').val();

                //$scope.datedisable = true;               
            }
            params.data.FROM_DATE = e.target.value;
        });
        return newDate;
    }


    function createToDatePicker(params) {
        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.TO_DATE');
        //newDate.setAttribute('ng-disabled', 'datedisable==true');
        newDate.type = "text";
        newDate.readOnly = true;
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        prefvalue = _.find($scope.SysPref, { SYSP_CODE: "Restrict Allocation to 90 Days" });
        fromdate = params.data.FROM_DATE;
        todate = params.data.TO_DATE;
        //console.log(prefvalue);
        if (prefvalue != undefined) {
            $scope.datedisable = true;

        }
        $(newDate).datepicker({
            //format: 'dd M, yyyy',
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
            startDate: new Date(),
            endDate: $scope.enddateselection
            //Santharaju Start
        }).on("input change", function (e) {
            if (e.target.value != undefined && e.target.value != "" && params.data.TO_DATE != e.target.value) {
                if ($scope.gridSpaceAllocOptions != undefined && $scope.gridSpaceAllocOptions.rowData.length > 0) {
                    angular.forEach($scope.gridSpaceAllocOptions.rowData, function (item) {
                        if (item.AUR_ID == params.data.AUR_ID && new Date(item.FROM_DATE) <= new Date(e.target.value) && new Date(item.TO_DATE) >= new Date(e.target.value)) {
                            showNotification('error', 8, 'bottom-right', 'Seat already booked within selected date range');
                            e.target.value = "";
                            return;
                        }
                    });
                }
                if ($scope.EmpAllocSeats.length > 0) {
                    angular.forEach($scope.EmpAllocSeats, function (item) {
                        if (item.AUR_ID == params.data.AUR_ID && (
                            new Date(item.FROM_DATE) <= new Date(params.data.FROM_DATE) && new Date(item.TO_DATE) >= new Date(e.target.value) ||
                            new Date(item.FROM_DATE) >= new Date(params.data.FROM_DATE) && new Date(item.TO_DATE) <= new Date(e.target.value))) {
                            showNotification('error', 8, 'bottom-right', 'Seat already booked within selected date range');
                            e.target.value = "";
                            return;
                        }
                    });
                }
            }
            params.data.TO_DATE = e.target.value;
        });
        //Santharaju End
        return newDate;
    }

    $scope.gridSpaceAllocOptions = {
        columnDefs: $scope.columDefsAlloc,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
        headerHeight: 40
    };

    $scope.gridSpaceAllocation = {
        columnDefs: $scope.columDefSpaceAlloc,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
    };
    //Showing Vacant Seats On Map When Click On Seat Status - Vacant
    $scope.ShowVacantOnMap = function () {
        var vacspaces = [];
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            if (marker.STATUS == 1) {
                marker.setStyle(VacantStyle);
                vacspaces.push(marker);
            }
            progress(0, '', false);
        });
        $.each($scope.drawnItems._layers, function (Key, layer) {
            var spcid = _.find(vacspaces, { SPC_ID: layer.options.spaceid });
            if (spcid == undefined)
                layer.setStyle({ fillColor: "#E8E8E8" });
            else {
                layer.setStyle({ fillColor: "#228B22", opacity: 0.65 });
            }
            progress(0, '', false);
        });
    };

    //Total Seats On Map
    $scope.ShowTotalOnMap = function () {
        //$scope.LoadMarkerLayers($scope.SearchSpace.Floor[0].FLR_CODE);
        $scope.Markerdata.clearLayers();
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            switch (marker.STATUS) {
                case 1: marker.setStyle(VacantStyle);
                    break;
                case 1002: marker.setStyle(AllocateStyle);
                    break;
                case 1004: marker.setStyle(OccupiedStyle);
                    break;
                case 1003: marker.setStyle(AllocateStyle);
                    break;
                case 1052: marker.setStyle(BlockStyle);
                    break;
                default: marker.setStyle(VacantStyle);
                    break;
            }
            marker.bindLabel(marker.SPC_DESC, {
                noHide: false,
                direction: 'auto',
                zIndexOffset: 2000
            });//.setOpacity(1);
            $scope.Markerdata.addLayer(marker);
        });
        $scope.loadThemes();
    };

    //Occupied To Employee on Map
    $scope.ShowOccupiedOnMap = function () {
        var occspaces = [];
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            if (marker.STATUS == 1004) {
                marker.setStyle(OccupiedStyle);
                occspaces.push(marker);
            }
            progress(0, '', false);
        });

        //console.log(occspaces);
        $.each($scope.drawnItems._layers, function (Key, layer) {
            var spcid = _.find(occspaces, { SPC_ID: layer.options.spaceid });
            if (spcid == undefined)
                layer.setStyle({ fillColor: "#E8E8E8" });
            else {
                layer.setStyle({ fillColor: "#E74C3C", opacity: 0.65 });
            }
            progress(0, '', false);
        });
    }

    //Vertical Costcenter On Map
    $scope.ShowCostCentre = function () {
        var costcentre = [];
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            if (marker.STATUS == 1003) {
                marker.setStyle(CstVer);
                costcentre.push(marker);
            }
            else if (marker.STATUS == 1002) {
                marker.setStyle(CstVer);
                costcentre.push(marker);
            }
            progress(0, '', false);
        });
        $.each($scope.drawnItems._layers, function (Key, layer) {
            var spcid = _.find(costcentre, { SPC_ID: layer.options.spaceid });
            if (spcid == undefined)
                layer.setStyle({ fillColor: "#E8E8E8" });
            else {
                layer.setStyle({ fillColor: "#3ccae7", opacity: 0.65 });
            }
            progress(0, '', false);
        });
    }
    function detec() {

        if (navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)) {

            $("#modalbooking").css("width", "100%");

        } else {

            $("#modalbooking").css("width", "95%");
        }

    }
    detec();

    $('#SeatAllocation').on('shown.bs.modal', function () {
        $('#btnAllocateSeats').attr('disabled', false);
        $scope.relfrom = {};
        var role = _.find($scope.GetRoleAndRM);
        var emp = _.find($scope.Employee);

        window.addEventListener('resize', function () {
            setTimeout(function () {
                if (window.innerWidth <= 780) {
                    $scope.gridSpaceAllocOptions.api.setColumnDefs($scope.MObcolumDefsAlloc);
                }
                else {
                    $scope.gridSpaceAllocOptions.api.setColumnDefs($scope.columDefsAlloc);
                }
            })
        })

        if (role.AUR_ROLE == 6) {
            angular.forEach($scope.selectedSpaces, function (value, key) {
                value.ticked = false;
                value.disabled = true;
                if (value.AUR_ID == emp.AUR_ID || value.AUR_ID == "") {
                    value.ticked = true;
                    value.disabled = false;
                }
            });
            var count = $scope.selectedSpaces.length;
            $scope.selectedSpaces.count = count;


            angular.forEach($scope.selectedSpaces, function (value, key) {
                value.FROM_DATE = $scope.Maploader.FROM_DATE1;
                value.TO_DATE = $scope.Maploader.TO_DATE1;
            })


            $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);

            $scope.gridSpaceAllocOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridSpaceAllocOptions.columnApi.getColumn("Cost_Center_Name").colDef.headerName = $scope.BsmDet.Child;
            //$scope.gridSpaceAllocOptions.api.refreshView();
            $scope.gridSpaceAllocOptions.api.refreshHeader();
        }
        else {

            //if ($scope.VerticalWiseAlloc.SYSP_VAL1 == 1) {
            //    var dta = angular.copy($scope.AurNames1);
            //    $scope.AurNames = [];
            //    for (var o in dta) {
            //        if (dta[o].VERTICAL == $scope.selectedSpaces[0].VERTICAL) {
            //            $scope.AurNames.push(dta[o]);
            //        }
            //    }
            //}

            angular.forEach($scope.selectedSpaces, function (value, key) {
                value.FROM_DATE = $scope.Maploader.FROM_DATE1;
                value.TO_DATE = $scope.Maploader.TO_DATE1;
            })

            $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);

            $scope.gridSpaceAllocOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridSpaceAllocOptions.columnApi.getColumn("Cost_Center_Name").colDef.headerName = $scope.BsmDet.Child;
            //$scope.gridSpaceAllocOptions.api.refreshView();
            $scope.gridSpaceAllocOptions.api.refreshHeader();
        }

    });

    $scope.ReleaseEmp = function (Relemp) {
        $scope.SelRelEmp = [];
        $scope.SelRelEmp.push(Relemp);
        var role = _.find($scope.GetRoleAndRM);

        var dataobj = { sad: $scope.SelRelEmp, reltype: 1003 };


        _.remove($scope.EmpAllocSeats, function (sel) {
            return sel.SSAD_SRN_REQ_ID === Relemp.SSAD_SRN_REQ_ID
        });

        //pk
        _.remove($scope.SeatAllocForSpace, function (sel) {
            return sel.SSAD_SRN_REQ_ID === Relemp.SSAD_SRN_REQ_ID
        });
       
        MaploaderService.release(dataobj).then(function (response) {

            $scope.releasecount++;
            var result = $filter('filter')($scope.SelRelEmp, { SPC_FLR_ID: $scope.SearchSpace.Floor[0].FLR_CODE });
            for (var a = $scope.SelRelEmp.length - 1; a >= 0; a--) {



                if ($scope.SearchSpace.Floor[0].FLR_CODE == $scope.SelRelEmp[a].FLR_CODE) {
                    var spcid = _.find($scope.Markers, { SPC_ID: $scope.SelRelEmp[a].SPC_ID });
                    var emp = _.find($scope.selectedSpaces, { ticked: false })


                    if (role.AUR_ROLE == 6) {

                        if ($scope.selectedSpaces.length != $scope.SelRelEmp.length) {

                            spcid.STATUS = 1004;
                            spcid.setStyle(OccupiedStyle);
                            spcid.ticked = false;
                            var seatdes = "Seat Id:" + spcid.SPC_NAME + " , Occupied by: " + emp.AUR_ID;
                            spcid.SPC_DESC = seatdes;
                            var formattedString = seatdes.split(",").join("\n");
                            var k = formattedString.split("\n");
                            var id = "";
                            var id1 = "";
                            var text = "";
                            var fulltext = "";
                            var str = " ";
                            fulltext = BindMarkerText(formattedString, spcid);

                            spcid.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: 'my-label',
                                zIndexOffset: 2000
                            });//.setOpacity(1);

                            spcid.STACHECK = UtilityService.Deleted;

                        }
                        else {


                            $scope.selectedSpaces[0].SSAD_SRN_REQ_ID = '';
                            $scope.selectedSpaces[0].SSA_SRNREQ_ID = '';

                            spcid.STATUS = 1;
                            spcid.setStyle(VacantStyle);

                            var seatdese = "Seat Id:" + spcid.SPC_NAME + ",Employee :Vacant";
                            spcid.SPC_DESC = seatdesc;
                            var formattedStringe = seatdese.split(",").join("\n");
                            var ke = formattedStringe.split("\n");
                            var ide = "";
                            var id1e = "";
                            var texte = "";
                            var fulltexte = "";
                            var stre = " ";
                            fulltext = BindMarkerText(formattedStringe, spcid);
                            spcid.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: 'my-label',
                                zIndexOffset: 2000
                            });//.setOpacity(1);

                            spcid.STACHECK = UtilityService.Deleted;
                            spcid.ticked = false;
                            spcid.SSA_SRNREQ_ID = "";
                        }
                    }
                    else {
                        if ($scope.selectedSpaces.length != $scope.SelRelEmp.length) {
                            spcid.STATUS = 1004;
                            spcid.setStyle(OccupiedStyle);

                            var seatdes = "Seat Id:" + spcid.SPC_NAME + " ,Occupied by :" + emp.AUR_ID;
                            spcid.SPC_DESC = seatdesc;
                            var formattedString = seatdes.split(",").join("\n");
                            var k = formattedString.split("\n");
                            var id = "";
                            var id1 = "";
                            var text = "";
                            var fulltext = "";
                            var str = " ";

                            fulltext = BindMarkerText(formattedString, spcid);
                            spcid.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: 'my-label',
                                zIndexOffset: 2000
                            });//.setOpacity(1);

                            spcid.STACHECK = UtilityService.Deleted;
                            spcid.ticked = false;
                        }
                        else {
                            var seatdesc = "Seat Id:" + spcid.SPC_NAME + " ,Allocated to " + $scope.SelRelEmp[a].Cost_Center_Name;
                            spcid.SPC_DESC = seatdesc;
                            var formattedStringc = seatdesc.split(",").join("\n");
                            var kc = formattedStringc.split("\n");
                            var idc = "";
                            var id1c = "";
                            var textc = "";
                            var fulltext = "";
                            var strc = " ";
                            fulltext = BindMarkerText(formattedStringc, spcid);

                            spcid.STATUS = 1003;
                            spcid.setStyle(AllocateStyle);
                            spcid.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: 'my-label',
                                zIndexOffset: 2000
                            });//.setOpacity(1);

                            spcid.STACHECK = UtilityService.Deleted;
                            spcid.ticked = false;
                        }
                    }

                }

                //$('#SeatAllocation').modal('hide');
                //$scope.selectedSpaces = [];

                //setTimeout(function () {
                //    showNotification('success', 8, 'bottom-right', 'Selected Spaces Allocated Successfully');
                //}, 1000);

                $('#btnAllocateSeats').attr('enabled', 'enabled');

                if ($scope.GradeAlloc.SYSP_VAL1 == 1) {
                    $scope.GradeAllocStatus();
                }
                //$scope.DisableAllocSeats = false;
            }
            // reloadMarkers();
        });
        if ($scope.LibertyChanges.SYSP_VAL1 == 1) {
            var dataobj = {
                flr_code: $scope.SelRelEmp[0].FLR_CODE,
                category: $scope.SelRelEmp[0].SPC_ID,
                subitem: $scope.SelRelEmp[0].SSA_SRNREQ_ID ? $scope.SelRelEmp[0].SSA_SRNREQ_ID : ""
            };

            MaploaderService.SeatRestriction(dataobj).then(function (response) {
                if (response != null) {
                    $scope.SeatRestrictionStatus = response[0].Seat_Restr;

                }
            });
        }

    }

    $scope.Proceedalloc = function () {
        var prefval = _.find($scope.SysPref, { SYSP_CODE: "Duplicate allocation" })
        if ((prefval == undefined) || prefval.SYSP_VAL1 == undefined) {
            $scope.EmpAllocSeats = []
            progress(0, '', false);
        }
        else if (prefval.SYSP_VAL1 = 1042 && ($scope.EmpAllocSeats.length >= 1)) {
            showNotification('error', 8, 'bottom-right', 'Please Release Employee');
        }

    }


    $scope.Release = function (reltypecode, reltypename) {
        progress(0, 'Loading...', true);
        $scope.relfrom = {};
        $scope.relfrom.Name = reltypename;
        $scope.relfrom.Value = reltypecode;
        var dataobj = { sad: $filter('filter')($scope.selectedSpaces, function (x) { if (x.ticked == true) return x; }), reltype: reltypecode };


        if (dataobj.sad.length != 0) {
            MaploaderService.release(dataobj).then(function (response) {
                if (response.data != null) {
                    angular.forEach($scope.selectedSpaces, function (value, key) {
                        if (value.ticked == true) {
                            var spcid = _.find($scope.Markers, { SPC_ID: value.SPC_ID });


                            //var spclst = _.filter($scope.selectedSpaces, function (space) { if (space.SPC_ID == value.SPC_ID && space != value) return space });
                            //var length = 0;
                            //if (spclst != undefined)
                            //    length = spclst.length;
                            //if (length > 0)
                            //    updateSpcDetails(spclst, spcid);
                            //else {
                            if (dataobj.sad.length != $scope.selectedSpaces.length) {
                                var emp = _.find($scope.selectedSpaces, { ticked: false })


                                value.AUR_ID = emp.AUR_ID;
                                value.STACHECK = UtilityService.Deleted;
                                spcid.STATUS = 1004;
                                spcid.setStyle(OccupiedStyle);
                                spcid.ticked = false;
                                var seatdes = "Seat Id:" + spcid.SPC_NAME + " :Occupied by " + emp.AUR_ID;
                                var formattedString = seatdes.split(",").join("\n");
                                var k = formattedString.split("\n");
                                var id = "";
                                var id1 = "";
                                var text = "";
                                var fulltext = "";
                                var str = " ";
                                fulltext = BindMarkerText(formattedString, spcid);
                                spcid.bindLabel(fulltext.split(",").join("\n"), {
                                    noHide: false,
                                    direction: 'auto',
                                    zIndexOffset: 2000
                                });//.setOpacity(1);
                                spcid.SPC_DESC = fulltext.split(",").join("\n");
                                spcid.STACHECK = UtilityService.Deleted;
                            }
                            else {

                                switch ($scope.relfrom.Value) {
                                    case 1004:

                                        value.AUR_ID = "";
                                        value.STACHECK = UtilityService.Deleted;
                                        spcid.STATUS = 1003;
                                        spcid.setStyle(AllocateStyle);
                                        spcid.ticked = false;
                                        var seatdesc = "Seat Id:" + spcid.SPC_NAME + " ,Allocated to " + value.Cost_Center_Name + ' /' + value.VERTICAL;
                                        var formattedStringc = seatdesc.split(",").join("\n");
                                        var kc = formattedStringc.split("\n");
                                        var idc = "";
                                        var id1c = "";
                                        var textc = "";
                                        var fulltextc = "";
                                        var strc = " ";
                                        for (i = 0; i < kc.length; i++) {
                                            id1c = kc[i].substr(0, kc[i].indexOf(":"));
                                            idc = kc[i].substr(0, kc[i].indexOf(":")).fontcolor("black");
                                            textc = kc[i].substr(kc[i].indexOf(":") + 1).fontcolor("seagreen");
                                            if (value.STATUS == 1004) {
                                                fulltextc += id1c + "\t" + ":" + textc + ",";
                                            }
                                            else {
                                                fulltextc += idc + " :" + textc + ",";
                                            }
                                        }
                                        spcid.bindLabel(fulltextc.split(",").join("\n"), {
                                            noHide: false,
                                            direction: 'auto',
                                            zIndexOffset: 2000
                                        });//.setOpacity(1);
                                        spcid.SPC_DESC = fulltextc.split(",").join("\n");
                                        spcid.STACHECK = UtilityService.Deleted;
                                        break;
                                    case 1003:


                                        value.Cost_Center_Code = "";
                                        value.SH_CODE = "";
                                        value.AUR_ID = "";
                                        value.STACHECK = UtilityService.Deleted;
                                        spcid.STATUS = 1002;
                                        spcid.ticked = false;
                                        spcid.setStyle(AllocateStyle);
                                        var seatdesv = "Seat Id:" + spcid.SPC_NAME + " :Allocated to " + value.VERTICAL;
                                        var formattedStringv = seatdesv.split(",").join("\n");
                                        var kv = formattedStringv.split("\n");
                                        var idv = "";
                                        var id1v = "";
                                        var textv = "";
                                        var fulltextv = "";
                                        var strv = " ";
                                        for (i = 0; i < kv.length; i++) {
                                            id1v = kv[i].substr(0, kv[i].indexOf(":"));
                                            idv = kv[i].substr(0, kv[i].indexOf(":")).fontcolor("black");
                                            textv = kv[i].substr(kv[i].indexOf(":") + 1).fontcolor("seagreen");
                                            if (value.STATUS == 1004) {
                                                fulltextv += id1v + "\t" + ":" + textv + ",";
                                            }
                                            else {
                                                fulltextv += idv + " :" + textv + ",";
                                            }
                                        }
                                        spcid.bindLabel(fulltextv.split(",").join("\n"), {
                                            noHide: false,
                                            direction: 'auto',
                                            zIndexOffset: 2000
                                        });//.setOpacity(1);
                                        spcid.SPC_DESC = fulltextv.split(",").join("\n");
                                        spcid.STACHECK = UtilityService.Deleted;
                                        spcid.STATUS = 1002;
                                        break;
                                    case 1002:


                                        value.VERTICAL = "";
                                        value.Cost_Center_Code = "";
                                        value.SH_CODE = "";
                                        value.AUR_ID = "";
                                        value.FROM_DATE = "";
                                        value.TO_DATE = "";
                                        value.STATUS = 1;
                                        value.SSA_SRNREQ_ID = "";
                                        spcid.setStyle(VacantStyle);
                                        spcid.ticked = false;
                                        spcid.STATUS = 1;
                                        var seatdese = "Seat Id:" + spcid.SPC_NAME + " Employee :Vacant";
                                        var formattedStringe = seatdese.split(",").join("\n");
                                        var ke = formattedStringe.split("\n");
                                        var ide = "";
                                        var id1e = "";
                                        var texte = "";
                                        var fulltexte = "";
                                        var stre = " ";
                                        for (i = 0; i < ke.length; i++) {
                                            id1e = ke[i].substr(0, ke[i].indexOf(":"));
                                            ide = ke[i].substr(0, ke[i].indexOf(":")).fontcolor("black");
                                            texte = ke[i].substr(ke[i].indexOf(":") + 1).fontcolor("seagreen");
                                            if (value.STATUS == 1004) {
                                                fulltexte += id1e + "\t" + ":" + texte + ",";
                                            }
                                            else {
                                                fulltexte += ide + " :" + texte + ",";
                                            }
                                        }
                                        spcid.bindLabel(fulltexte.split(",").join("\n"), {
                                            noHide: false,
                                            direction: 'auto',
                                            zIndexOffset: 2000
                                        });//.setOpacity(1);
                                        spcid.SSA_SRNREQ_ID = "";
                                        spcid.VERTICAL = "";
                                        spcid.Cost_Center_Code = "";
                                        spcid.SH_CODE = "";
                                        spcid.AUR_ID = "";
                                        spcid.FROM_DATE = "";
                                        spcid.TO_DATE = "";
                                        spcid.STATUS = 1;
                                        spcid.SSA_SRNREQ_ID = "";
                                        spcid.SPC_DESC = fulltexte.split(",").join("\n");
                                        spcid.STACHECK = UtilityService.Deleted;

                                        $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                                            if (layer.options.spaceid == value.SPC_ID) {
                                                layer.options.seattype = 1;
                                                //console.log(layer.options.spaceid);
                                            }
                                        });
                                        $scope.loadThemes();
                                        break;
                                    case 1052:
                                        value.VERTICAL = "";
                                        value.Cost_Center_Code = "";
                                        value.SH_CODE = "";
                                        value.AUR_ID = "";
                                        value.FROM_DATE = "";
                                        value.TO_DATE = "";
                                        value.STATUS = 1;
                                        value.SSA_SRNREQ_ID = "";
                                        spcid.setStyle(VacantStyle);
                                        spcid.STATUS = 1;
                                        spcid.ticked = false;
                                        spcid.bindLabel(fulltexte.split(",").join("\n"), {
                                            noHide: false,
                                            direction: 'auto',
                                            zIndexOffset: 2000
                                        });//.setOpacity(1);
                                        spcid.SSA_SRNREQ_ID = "";
                                        spcid.VERTICAL = "";
                                        spcid.Cost_Center_Code = "";
                                        spcid.SH_CODE = "";
                                        spcid.AUR_ID = "";
                                        spcid.FROM_DATE = "";
                                        spcid.TO_DATE = "";
                                        spcid.STATUS = 1;
                                        spcid.SSA_SRNREQ_ID = "";
                                        spcid.SPC_DESC = fulltexte.split(",").join("\n");
                                        spcid.STACHECK = UtilityService.Deleted;

                                        $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                                            if (layer.options.spaceid == value.SPC_ID) {
                                                layer.options.seattype = 1;
                                                //console.log(layer.options.spaceid);
                                            }
                                        });
                                        $scope.loadThemes();
                                        break;

                                }
                                //}
                            }
                        }

                    });
                    setTimeout(function () {
                        $('#SeatAllocation').modal('hide');
                        $scope.selectedSpaces = [];
                        updateSummaryCount();
                        progress(0, '', false);
                        $scope.gridSpaceAllocOptions.api.refreshView();
                        showNotification('success', 8, 'bottom-right', "Selected Spaces Released From " + $scope.relfrom.Name);
                        if ($scope.GradeAlloc.SYSP_VAL1 == 1) {
                            $scope.GradeAllocStatus();
                        }
                    }, 300);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                }
            }, function (error) {
            });
        }

        else {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "employee cannot release others seat");
        }

    }

    function updateSpcDetails(spclst, spcid) {
        var labelval = "";
        switch ($scope.relfrom.Value) {
            case 1002: spcid.setStyle(VacantStyle);
                spcid.STATUS = 1;
                labelval = spcid.SPC_NAME + " : Vacant";
                spcid.VERTICAL = "";
                spcid.Cost_Center_Name = "";
                spcid.SSA_SRNREQ_ID = "";
                spcid.SSAD_SRN_REQ_ID = "";
                break;
            case 1004: spcid.setStyle(AllocateStyle);
                labelval = spcid.SPC_NAME + " : Allocated to " + spcid.Cost_Center_Name;
                spcid.STATUS = 1003;
                break;
            case 1003: spcid.setStyle(AllocateStyle);
                labelval = spcid.SPC_NAME + " : Allocated to " + spcid.VER_NAME;
                spcid.STATUS = 1002;
                spcid.Cost_Center_Name = "";
                break;
            case 1052: spcid.setStyle(VacantStyle);
                labelval = spcid.SPC_NAME + " : Vacant";
                spcid.VERTICAL = "";
                spcid.Cost_Center_Name = "";
                spcid.SSA_SRNREQ_ID = "";
                spcid.SSAD_SRN_REQ_ID = "";
                break;
            default: spcid.setStyle(VacantStyle);
                break;
        }
        spcid.bindLabel(labelval, {
            noHide: false,
            direction: 'auto',
            zIndexOffset: 2000
        });//.setOpacity(1);
        spcid.SPC_DESC = labelval;
        spcid.SSA_SRNREQ_ID = "";
        spcid.STACHECK = UtilityService.Deleted;
        spcid.ticked = false;

    }

    function BindMarkerText(formattedString, marker) {
        var k = formattedString.split("\n");
        var id = "";
        var id1 = "";
        var text = "";
        var fulltext = "";
        var str = " ";
        fulltext = '<table class="tabledata">'
        for (i = 0; i < k.length; i++) {
            id1 = k[i].substr(0, k[i].indexOf(":"));
            id = k[i].substr(0, k[i].indexOf(":")).fontcolor("black");
            text = k[i].substr(k[i].indexOf(":") + 1).fontcolor("seagreen");

            if ($scope.LibertyChanges.SYSP_VAL1 == 1 && (marker.STATUS == 1004 || marker.STATUS == 1002)) {


                if ((marker.SHIFT_TYPE == 1 && (id1 === 'From Date' || id1 == 'To Date' || id1 == 'Shift' || id1 == 'Extn ')) ||
                    (marker.SHIFT_TYPE == 5 && (id1 == 'Employee' || id1 == 'Division' || id1 == 'Channel' || id1 == 'From Date' || id1 == 'To Date' || id1 == 'Shift' || id1 == 'Extn '))
                    || (marker.SHIFT_TYPE == 4 && (id1 == 'Extn '))) {
                    //no requirement
                }


                else {

                    if (marker.SHIFT_TYPE == 4 && marker.SPC_TYPE_NAME == 'Meeting Room')//&& (id1 == 'Seat Type' || id1 == 'Division' || id1 == 'Channel' || id1 == 'Extn ')) {
                    {
                        if (id1 == 'Seat Type') {
                            text = marker.SPC_TYPE_NAME.fontcolor("seagreen");
                            fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                        }
                        else if (id1 == 'Division' || id1 == 'Channel' || id1 == 'Extn ') {
                            //
                        }
                        else {
                            fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                        }
                    }


                    else {


                        fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';

                    }

                }

            }

            else if ($scope.LibertyChanges.SYSP_VAL1 == 1 && marker.STATUS == 1) {

                if ((marker.SHIFT_TYPE == 5 || marker.SHIFT_TYPE == 6) && (id1 == 'Employee' || id1 == 'Division' || id1 == 'Channel' || id1 == 'From Date' || id1 == 'To Date' || id1 == 'Shift' || id1 == 'Extn ')) {
                    //
                }
                //else if (marker.SHIFT_TYPE == 6 && (id1 == 'Employee' || id1 == 'Division' || id1 == 'Channel' || id1 == 'From Date' || id1 == 'To Date' || id1 == 'Shift' || id1 == 'Extn ')) {
                //    //
                //}
                else {

                    if ($scope.LibertyChanges.SYSP_VAL1 == 1 && marker.SHIFT_TYPE == 4 && marker.SPC_TYPE_NAME == 'Meeting Room' && id1 == 'Seat Type') {
                        text = marker.SPC_TYPE_NAME.fontcolor("seagreen");
                        fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                    }
                    else {
                        fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                    }
                }
            }
            else {
                if ($scope.LibertyChanges.SYSP_VAL1 == 1 && $scope.marker.SHIFT_TYPE == 4 && $scope.marker.SPC_TYPE_NAME == 'Meeting Room' && id1 == 'Seat Type') {
                    text = $scope.marker.SPC_TYPE_NAME.fontcolor("seagreen");
                    fulltext += id1 + "\t" + ":" + text + ",";
                }
                else {

                    if (id1 == 'Employee') {
                        fulltext += '<tr><td> </td></tr> <tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                    }
                    else {
                        fulltext += '<tr><td>' + id1 + '</td><td>   :' + text + '</td></tr>';
                    }

                }

            }
        }

        fulltext += '</table>'
        return fulltext;
    }


    $scope.AllocateSeats = function () {

        progress(0, 'Loading...', true);
        var count = 0;
        var flag = false;
        $scope.FinalSelectedSpaces = [];
        var data = $scope.Shifts.filter(function (item) {
            return !_.find($scope.gridSpaceAllocOptions.rowData, { SPACE_TYPE: item.SH_SEAT_TYPE });
        });

        var check = $scope.gridSpaceAllocOptions.rowData.filter(function (item) {
            return _.find(data, { SH_CODE: item.SH_CODE });
        });

        var data = $scope.VLANS.filter(function (item) {
            return !_.find($scope.gridSpaceAllocOptions.rowData, { SPACE_TYPE: item.VL_Code });
        });

        var check = $scope.gridSpaceAllocOptions.rowData.filter(function (item) {
            return _.find(data, { VL_Code: item.VL_Code });
        });

        var valueArr = $scope.gridSpaceAllocOptions.rowData.map(function (item) { return item.AUR_ID });
        var isDuplicate = valueArr.some(function (item, idx) {
            if (item != '')
                return valueArr.indexOf(item) != idx
        });

        var checkForDuplicate = [];
        angular.forEach($scope.selectedSpaces, function (value, key) {

            if (value.ticked == true) {
                if (checkForDuplicate.length > 0) {
                    var result = _.find(checkForDuplicate, function (check) {
                        return value.SPC_ID == check.SPC_ID && check.FROM_DATE === value.FROM_DATE && check.TO_DATE == value.TO_DATE
                    });

                    if (result) {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', "can't book same sheet with duplicate dates");
                        console.log("duplicate found" + result)
                        return;
                    }
                    else {
                        checkForDuplicate.push(value);
                    }
                }
                else {
                    checkForDuplicate.push(value);
                }
            }

        })



        var count1 = 0;
        if ($scope.selectedSpaces[0].SPACE_TYPE != 4) {

            angular.forEach($scope.selectedSpaces, function (value, key) {

                if (value.ticked == true && flag != true) {
                    if (value.VERTICAL == undefined || value.VERTICAL == "") {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select ' + $scope.BsmDet.Parent);
                        return;
                    }
                    else if (value.AUR_ID == undefined || value.AUR_NAME == undefined || value.AUR_NAME == "") {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select employee');
                        return;
                    }
                    else if (value.Cost_Center_Code == undefined || value.Cost_Center_Code == "" || value.Cost_Center_Name == "") {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select Coscenter');
                        return;
                    }
                    else if (value.FROM_DATE == undefined || value.FROM_DATE == "") {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select From Date');
                        return;
                    }
                    else if (value.TO_DATE == undefined || value.TO_DATE == "") {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select To Date');
                        return;
                    }
                    else if (new Date(value.FROM_DATE) > new Date(value.TO_DATE)) {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select To Date greater than From Date For ' + value.SPC_ID);
                        return;
                    }
                    else if (value.SH_CODE == undefined || value.SH_CODE == '' || value.SH_NAME == '') {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select Shift');
                        return;
                    }
                    else if (value.VL_Code == undefined || value.VL_Code == '' || value.VL_NAME == '') {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select VLAN');
                        return;
                    }
                    else {
                        //Santharaju Start
                        //if ($scope.EmpAllocSeats.length > 0) {
                        //    angular.forEach($scope.EmpAllocSeats, function (item) {
                        //        if (isDateEqual != "true" && (new Date(item.FROM_DATE).getDate() == new Date(value.FROM_DATE).getDate()
                        //            || new Date(item.TO_DATE).getDate() == new Date(value.TO_DATE).getDate())) {
                        //            isDateEqual = "true";
                        //        }
                        //    });
                        //}

                        //praveen start

                        var value_FROM_DATE = moment(new Date(value.FROM_DATE)).format('MM/DD/YYYY');
                        var value_TO_DATE = moment(new Date(value.TO_DATE)).format('MM/DD/YYYY');
                        var isDateEqual = "false";
                        if ($scope.EmpAllocSeats.length > 0) {
                            angular.forEach($scope.EmpAllocSeats, function (item) {
                                var item_FROM_DATE = moment(new Date(item.FROM_DATE)).format('MM/DD/YYYY');
                                var item_TO_DATE = moment(new Date(item.TO_DATE)).format('MM/DD/YYYY');
                                if (isDateEqual != "true" && item.AUR_ID == value.AUR_ID && (value_FROM_DATE == item_FROM_DATE || value_TO_DATE == item_TO_DATE)) {
                                    isDateEqual = "true";
                                }
                            });
                        }

                        if ($scope.SeatAllocForSpace.length > 0) {
                            angular.forEach($scope.SeatAllocForSpace, function (item) {
                                var item_FROM_DATE = moment(new Date(item.FROM_DATE)).format('MM/DD/YYYY');
                                var item_TO_DATE = moment(new Date(item.TO_DATE)).format('MM/DD/YYYY');
                                if (isDateEqual != "true" && item.SPC_ID == value.SPC_ID && (value_FROM_DATE == item_FROM_DATE || value_TO_DATE == item_TO_DATE)) {
                                    isDateEqual = "true";
                                }
                            });
                        }

                        if (isDateEqual == "true") {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', 'Seat already booked within selected date range');
                            flag = true;
                            return;
                        }

                        if ($scope.selectedSpaces.length > 0 && flag != true) {
                            for (var k = 0; k < $scope.selectedSpaces.length; k++) {

                                if (isDateEqual != "true" && k != count1 && $scope.selectedSpaces[k].AUR_ID == value.AUR_ID && (new Date($scope.selectedSpaces[k].FROM_DATE).getDate() == new Date(value.FROM_DATE).getDate()
                                    || new Date($scope.selectedSpaces[k].TO_DATE).getDate() == new Date(value.TO_DATE).getDate())) {
                                    isDateEqual = "true";
                                }
                            }
                        }

                        if (isDateEqual == "true") {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', 'Please change from date and todate');
                            flag = true;
                            return;
                        }
                        //Santharaju End
                        $scope.FinalSelectedSpaces.push(value);
                        count = count + 1;
                    }
                }
                count1++;
            });
        }


        else {
            angular.forEach($scope.selectedSpaces, function (value, key) {
                if (value.ticked == true) {
                    if (value.VERTICAL == undefined || value.VERTICAL == "") {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select ' + $scope.BsmDet.Parent);
                        return;
                    }
                    if (value.FROM_DATE == undefined || value.FROM_DATE == "") {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select From Date');
                        return;
                    }
                    else if (value.TO_DATE == undefined || value.TO_DATE == "") {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select To Date');
                        return;
                    }
                    else if (new Date(value.FROM_DATE) > new Date(value.TO_DATE)) {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select To Date greater than From Date For ' + value.SPC_ID);
                        return;
                    }
                    else if (value.FROM_TIME == undefined || value.FROM_TIME == '') {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select From Time');
                        return;
                    }
                    else if (value.TO_TIME == undefined || value.TO_TIME == '') {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select To Time');
                        return;
                    }
                    else if (value.VL_Code == undefined || value.VL_Code == '' || value.VL_NAME == '') {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select VLAN');
                        return;
                    }
                    else if (value.AUR_NAME == undefined || value.AUR_NAME == "") {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select employee');
                        return;
                    }
                    else if (value.Cost_Center_Code == undefined || value.Cost_Center_Code == "" || value.Cost_Center_Name == "") {
                        progress(0, '', false);
                        flag = true;
                        showNotification('error', 8, 'bottom-right', 'Please Select Coscenter');
                        return;
                    }
                    else {
                        //Santharaju Start
                        var isDateEqual = "false";
                        if ($scope.EmpAllocSeats.length > 0) {
                            angular.forEach($scope.EmpAllocSeats, function (item) {
                                if (isDateEqual != "true" && item.AUR_ID == value.AUR_ID && (new Date(item.FROM_DATE).getDate() == new Date(value.FROM_DATE).getDate()
                                    || new Date(item.TO_DATE).getDate() == new Date(value.TO_DATE).getDate())) {
                                    isDateEqual = "true";
                                }
                            });
                        }
                        if (isDateEqual == "true") {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', 'Seat already booked within selected date range');
                            flag = true;
                            return;
                        }

                        if ($scope.selectedSpaces.length > 0 && flag != true) {
                            for (var k = 0; k < $scope.selectedSpaces.length; k++) {

                                if (isDateEqual != "true" && k != count1 && $scope.selectedSpaces[k].AUR_ID == value.AUR_ID && (new Date($scope.selectedSpaces[k].FROM_DATE).getDate() == new Date(value.FROM_DATE).getDate()
                                    || new Date($scope.selectedSpaces[k].TO_DATE).getDate() == new Date(value.TO_DATE).getDate())) {
                                    isDateEqual = "true";
                                }
                            }
                        }

                        if (isDateEqual == "true") {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', 'Please change from date and todate');
                            flag = true;
                            return;
                        }
                        //Santharaju End
                        $scope.FinalSelectedSpaces.push(value);
                        count = count + 1;
                    }
                }

                count1++;
            });

        }
        if (flag) {
            return;
        }
        if (count == 0) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', 'Please select atleast employee for allocate');
            return;
        }


        //if (check.length > 0) {
        //    progress(0, '', false);
        //    flag = true;
        //    showNotification('error', 8, 'bottom-right', "Please Select Valid Shift");
        //    return;
        //}

        //Swethan start

        //if (isDuplicate == true) {
        //    progress(0, '', false);
        //    flag = true;
        //    showNotification('error', 8, 'bottom-right', 'Employee Should Not be Same');
        //    return;
        //}

        // end start


        //console.log($scope.FinalSelectedSpaces);

        var FinalSpacesList = [];
        for (var i = 0; i < $scope.FinalSelectedSpaces.length; i++) {
            var dateDiff = moment($scope.FinalSelectedSpaces[i].TO_DATE).diff(moment($scope.FinalSelectedSpaces[i].FROM_DATE), 'days');
            if (dateDiff > 0) {
                dateDiff = dateDiff + 1;
            }
            for (var j = 0; j < dateDiff; j++) {
                var space = {};
                angular.copy($scope.FinalSelectedSpaces[i], space);
                space.FROM_DATE = moment($scope.FinalSelectedSpaces[i].FROM_DATE).add(j, 'day').format('MM/DD/YYYY');
                space.TO_DATE = space.FROM_DATE;
                FinalSpacesList.push(space);
            }
        }
        if (FinalSpacesList.length == 0) {
            FinalSpacesList = $scope.FinalSelectedSpaces;
        }



        MaploaderService.allocateSeats(FinalSpacesList).then(function (response) {
            if (response.data != null) {
                angular.forEach(response.data, function (value, key) {
                    var spcid = _.find($scope.Markers, { SPC_ID: value.SPC_ID });
                    spcid.STATUS = value.STATUS;
                    spcid.VERTICAL = value.VERTICAL;
                    spcid.Cost_Center_Name = value.Cost_Center_Name;
                    spcid.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                    spcid.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                    spcid.ticked = false;
                    spcid.SHIFT_TYPE = value.SHIFT_TYPE;
                    spcid.VLAN_TYPE = value.VLAN_TYPE;
                    spcid.SSAD_AUR_ID = value.emp;
                    spcid.SPC_DESC = value.AUR_ID;

                    var formattedString = value.AUR_ID.split(",").join("\n");
                    var k = formattedString.split("\n");
                    var id = "";
                    var id1 = "";
                    var text = "";
                    var fulltext = "";
                    var str = " ";
                    fulltext = BindMarkerText(formattedString, spcid);

                    var labelval = "";
                    switch (value.STATUS) {
                        case 1: spcid.setStyle(VacantStyle);
                            break;
                        case 1002: spcid.setStyle(AllocateStyle);
                            break;
                        case 1004: spcid.setStyle(OccupiedStyle);
                            break;
                        case 1003: spcid.setStyle(AllocateStyle);
                            break;
                        case 1052: spcid.setStyle(BlockStyle);
                            break;
                        default: spcid.setStyle(VacantStyle);
                            break;
                    }


                    spcid.bindLabel(fulltext.split(",").join("\n"), {
                        noHide: false,
                        direction: 'auto',
                        className: "my-label",
                        zIndexOffset: 2000
                    });//.setOpacity(1);



                    $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                        if (layer.options.spaceid == value.SPC_ID) {
                            layer.options.seattype = value.SHIFT_TYPE;
                            //console.log(layer.options.spaceid);
                        }
                    });
                    $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                        if (layer.options.spaceid == value.SPC_ID) {
                            layer.options.Code = value.VLAN_TYPE;
                            //console.log(layer.options.spaceid);
                        }
                    });

                    $scope.loadThemes();

                });
                $('#SeatAllocation').modal('hide');
                $scope.selectedSpaces = [];
                updateSummaryCount();
                $scope.gridSpaceAllocOptions.api.refreshView();
                progress(0, '', false);
                if ($scope.GradeAlloc.SYSP_VAL1 == 1) {
                    $scope.GradeAllocStatus();
                }

                setTimeout(function () {
                    showNotification('success', 8, 'bottom-right', 'Selected Spaces Allocated Successfully');
                }, 1000);



            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        });

    }
    $scope.AllocAnother = function (data) {
        debugger;
        //swethan start
        //if (data.SPACE_TYPE == 1) {
        //    showNotification('error', 8, 'bottom-right', "Please select maximum of 1 person for dedicated");
        //}
        //else {

        //swethan end

        var prefval = _.find($scope.SysPref, { SYSP_CODE: "Duplicate allocation" });
        if ((prefval == undefined) || prefval.SYSP_VAL1 == undefined) {

            var refdata = {};
            angular.copy(data, refdata);

            var role = _.find($scope.GetRoleAndRM);
            //Santharaju Start
            //refdata.FROM_DATE = "";
            //refdata.TO_DATE = "";
            //need to do for all conditon
            refdata.Cost_Center_Code = "";
            refdata.Cost_Center_Name = "";

            if (role != 6) {
                refdata.AUR_ID = "";
                refdata.AUR_SEARCH = "";
            }


            //Santharaju End
            var prefval = _.find($scope.SysPref, { SYSP_CODE: "OVERLOAD" });
            if (prefval.SYSP_VAL1 >= $filter('filter')($scope.selectedSpaces, { SPC_ID: refdata.SPC_ID }).length + 1) {
                if (refdata.AUR_LONG_LEAVE_FROM_DT != '' && refdata.AUR_LONG_LEAVE_TO_DT != '' && refdata.AUR_LONG_LEAVE_FROM_DT != null && refdata.AUR_LONG_LEAVE_TO_DT != null) {
                    refdata.FROM_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_FROM_DT), 'MM/dd/yyyy');
                    refdata.TO_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_TO_DT), 'MM/dd/yyyy');
                }


                if (role.AUR_ROLE == 6) {
                    //swethan start
                    // var emp = _.find($scope.selectedSpaces, function (x) { if (x.AUR_ID == role.AUR_ID) return x });
                    //if (emp != undefined) {
                    //    showNotification('error', 8, 'bottom-right', 'Employee can not allocate for another');
                    //    return;
                    //}
                    //swethan end
                    var ver = _.find($scope.selectedSpaces, function (x) { if (x.VERTICAL == role.VER_CODE) return x });
                    if (ver == undefined) {
                        showNotification('error', 8, 'bottom-right', 'you can not allocate for different verticals in single seat');
                        return;
                    }
                    refdata.AUR_LEAVE_STATUS = "N";
                    refdata.SSAD_SRN_REQ_ID = null;
                    refdata.STACHECK = 16;
                    refdata.AUR_NAME = role.AUR_KNOWN_AS;
                    refdata.AUR_ID = role.AUR_ID;
                    refdata.VER_CODE = role.VER_CODE;
                    refdata.VER_NAME = role.VER_NAME;
                    //refdata.Cost_Center_Code = role.COST_CENTER_CODE;
                    //refdata.Cost_Center_Name = role.COST_CENTER_NAME;
                    /*   refdata.SH_CODE = "";*/

                    //swethan start
                    refdata.STATUS = 1004;
                    //swethan end
                    $scope.selectedSpaces.push(refdata);
                    $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
                    var emp = _.find($scope.Employee);
                    angular.forEach($scope.selectedSpaces, function (value, key) {
                        value.ticked = false;
                        value.disabled = true;
                        if (value.AUR_ID == emp.AUR_ID || value.AUR_ID == "") {
                            value.ticked = true;
                            value.disabled = false;
                        }
                    });
                    var dataobj = { lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: role.AUR_ID, mode: 1 };
                    MaploaderService.getEmpAllocSeat(dataobj).then(function (result) {

                        if (result.data) {
                            $scope.EmpAllocSeats = result.data;
                            progress(0, '', false);
                        }
                    });
                }
                else {
                    refdata.AUR_LEAVE_STATUS = "N";
                    refdata.SSAD_SRN_REQ_ID = null;
                    refdata.STACHECK = 16;
                    refdata.AUR_NAME = null;
                    refdata.AUR_ID = null;
                    /*   refdata.SH_CODE = "";*/
                    $scope.selectedSpaces.push(refdata);
                    $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
                }
            }
            else
                showNotification('error', 8, 'bottom-right', "Please select maximum of " + prefval.SYSP_VAL1 + " Spaces");

        }
        else if (prefval.SYSP_VAL1 = 1042 && ($scope.EmpAllocSeats.length >= 1)) {


            showNotification('error', 8, 'bottom-right', 'first release employee');

        }

        else if (prefval.SYSP_VAL1 = 1042) {


            var refdata = {};
            angular.copy(data, refdata);

            var prefval = _.find($scope.SysPref, { SYSP_CODE: "OVERLOAD" });
            if (prefval.SYSP_VAL1 >= $filter('filter')($scope.selectedSpaces, { SPC_ID: refdata.SPC_ID }).length + 1) {
                if (refdata.AUR_LONG_LEAVE_FROM_DT != '' && refdata.AUR_LONG_LEAVE_TO_DT != '' && refdata.AUR_LONG_LEAVE_FROM_DT != null && refdata.AUR_LONG_LEAVE_TO_DT != null) {
                    refdata.FROM_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_FROM_DT), 'MM/dd/yyyy');
                    refdata.TO_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_TO_DT), 'MM/dd/yyyy');
                }
                var role = _.find($scope.GetRoleAndRM);
                var emp = _.find($scope.Employee);
                if (role.AUR_ROLE == 6) {
                    var ver = _.find($scope.selectedSpaces, function (x) { if (x.VERTICAL == role.VER_CODE) return x });
                    if (ver == undefined) {
                        showNotification('error', 8, 'bottom-right', 'you can not allocate for different verticals in single seat');
                        return;
                    }
                    refdata.AUR_LEAVE_STATUS = "N";
                    refdata.SSAD_SRN_REQ_ID = null;
                    refdata.STACHECK = 16;
                    refdata.AUR_NAME = role.AUR_KNOWN_AS;
                    refdata.AUR_ID = role.AUR_ID;
                    refdata.VER_CODE = role.VER_CODE;
                    refdata.VER_NAME = role.VER_NAME;
                    refdata.Cost_Center_Code = role.COST_CENTER_CODE;
                    refdata.Cost_Center_Name = role.COST_CENTER_NAME;
                    $scope.selectedSpaces.push(refdata);
                    $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
                    MaploaderService.getEmpAllocSeat(dataobj).then(function (result) {
                        if (result.data) {
                            $scope.EmpAllocSeats = result.data;
                            progress(0, '', false);
                        }
                    });
                }
                else {
                    refdata.AUR_LEAVE_STATUS = "N";
                    refdata.SSAD_SRN_REQ_ID = null;
                    refdata.STACHECK = 16;
                    refdata.AUR_NAME = null;
                    refdata.AUR_ID = null;
                    $scope.selectedSpaces.push(refdata);
                    $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
                }

            }
            else
                showNotification('error', 8, 'bottom-right', "Please select maximum of " + prefval.SYSP_VAL1 + " Spaces");

        }

        //swethan start
        // }
        //swethan end
    }

    $scope.AllocateData = function () {
        var role = _.find($scope.GetRoleAndRM);
        $scope.FinalSelectedSpaces = [];
        $scope.selectedSpaces[0].FROM_DATE = $scope.Maploader.FROM_DATE;
        $scope.selectedSpaces[0].TO_DATE = $scope.Maploader.TO_DATE;
        $scope.selectedSpaces[0].SH_CODE = $scope.Maploader.SH_CODE;
        $scope.selectedSpaces[0].STATUS = 1004;

        if (role.AUR_ROLE != 6) {
            $scope.selectedSpaces[0].AUR_ID = $scope.mobileCC.AUR_ID;
            $scope.selectedSpaces[0].Cost_Center_Code = $scope.mobileCC.Cost_Center_Code;
            $scope.selectedSpaces[0].Cost_Center_Name = $scope.mobileCC.Cost_Center_Name;
            $scope.selectedSpaces[0].VERTICAL = $scope.mobileCC.VER_CODE;
            $scope.selectedSpaces[0].VER_NAME = $scope.mobileCC.VER_NAME;
            $scope.selectedSpaces[0].AUR_SEARCH = $scope.mobileCC.AUR_ID;
        }

        if ($scope.selectedSpaces[0].AUR_ID == undefined) {
            showNotification('error', 8, 'bottom-right', 'Please Select employee');
            return;
        }

        if ($scope.selectedSpaces[0].SH_CODE == undefined && $scope.selectedSpaces[0].SPACE_TYPE != 4) {
            showNotification('error', 8, 'bottom-right', 'Please Select Shift');
            return;
        }


        if ($scope.selectedSpaces[0].FROM_DATE == undefined) {
            progress(0, '', false);
            flag = true;
            showNotification('error', 8, 'bottom-right', 'Please Select From Date');
            return;
        }
        else if ($scope.selectedSpaces[0].TO_DATE == undefined) {
            progress(0, '', false);
            flag = true;
            showNotification('error', 8, 'bottom-right', 'Please Select To Date');
            return;
        }
        else if (new Date($scope.selectedSpaces[0].FROM_DATE) > new Date($scope.selectedSpaces[0].TO_DATE)) {
            progress(0, '', false);
            flag = true;
            showNotification('error', 8, 'bottom-right', 'Please Select To Date greater than From Date For ' + $scope.selectedSpaces[0].SPC_ID);
            return;
        }

        MaploaderService.allocateSeats($scope.selectedSpaces).then(function (response) {
            if (response.data != null) {
                angular.forEach(response.data, function (value, key) {
                    var spcid = _.find($scope.Markers, { SPC_ID: value.SPC_ID });
                    spcid.STATUS = value.STATUS;
                    spcid.VERTICAL = value.VERTICAL;
                    spcid.Cost_Center_Name = value.Cost_Center_Name;
                    spcid.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                    spcid.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                    spcid.ticked = false;
                    spcid.SHIFT_TYPE = value.SHIFT_TYPE;
                    spcid.VLAN_TYPE = value.VLAN_TYPE;
                    spcid.SSAD_AUR_ID = value.emp;

                    spcid.SPC_DESC = value.AUR_ID;
                    var formattedString = value.AUR_ID.split(",").join("\n");
                    var k = formattedString.split("\n");
                    var id = "";
                    var id1 = "";
                    var text = "";
                    var fulltext = "";
                    var str = " ";

                    fulltext = BindMarkerText(formattedString, spcid);

                    var labelval = "";
                    switch (value.STATUS) {
                        case 1: spcid.setStyle(VacantStyle);
                            break;
                        case 1002: spcid.setStyle(AllocateStyle);
                            break;
                        case 1004: spcid.setStyle(OccupiedStyle);
                            break;
                        case 1003: spcid.setStyle(AllocateStyle);
                            break;
                        case 1052: spcid.setStyle(BlockStyle);
                            break;
                        default: spcid.setStyle(VacantStyle);
                            break;
                    }


                    spcid.bindLabel(fulltext.split(",").join("\n"), {
                        noHide: false,
                        direction: 'auto',
                        className: "my-label",
                        zIndexOffset: 2000
                    });//.setOpacity(1);



                    $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                        if (layer.options.spaceid == value.SPC_ID) {
                            layer.options.seattype = value.SHIFT_TYPE;

                        }
                    });

                    $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                        if (layer.options.spaceid == value.SPC_ID) {
                            layer.options.Code = value.VLAN_TYPE;
                            //console.log(layer.options.spaceid);
                        }
                    });

                    $scope.loadThemes();

                });
                $('#SeatAllocation').modal('hide');
                $scope.selectedSpaces = [];
                $scope.mobileCC = {};
                updateSummaryCount();
                $scope.gridSpaceAllocOptions.api.refreshView();
                progress(0, '', false);
                if ($scope.GradeAlloc.SYSP_VAL1 == 1) {
                    $scope.GradeAllocStatus();
                }

                setTimeout(function () {
                    showNotification('success', 8, 'bottom-right', 'Selected Spaces Allocated Successfully');
                }, 1000);



            }
            else {
                progress(0, '', false);
                //swethan start
                //showNotification('error', 8, 'bottom-right', response.Message);
                showNotification('error', 8, 'bottom-right', "Something went wrong");
                //swethan end
            }
        });
    }




    ///   Load location data
    $scope.show = function () {
        //UtilityService.getCountires(2).then(function (Countries) {
        //    progress(0, 'Loading...', true);
        //    if (Countries.data != null) {
        //        $scope.Country = Countries.data;
        //    }
        //    UtilityService.getCities(2).then(function (Ctresponse) {
        //        if (Ctresponse.data != null) {
        //            $scope.City = Ctresponse.data;
        //        }

        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Location = response.data;
            }
            UtilityService.getTowers(2).then(function (response) {
                if (response.data != null) {
                    $scope.Tower = response.data;
                }
                UtilityService.getFloors(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Floor = response.data;
                        angular.forEach($scope.Floor, function (Value, index) {
                            if (Value.FLR_CODE == GetParameterValues('flr_code')) {

                                //var cny = _.find($scope.Country, { CNY_CODE: Value.CNY_CODE });
                                //cny.ticked = true;
                                //_.remove($scope.Country, { ticked: Value.ticked = false });
                                //$scope.SearchSpace.Country[0] = cny;

                                //var cty = _.find($scope.City, { CTY_CODE: Value.CTY_CODE });
                                //cty.ticked = true;
                                //_.remove($scope.City, { ticked: Value.ticked = false });
                                //$scope.SearchSpace.City[0] = cty;

                                var twr = _.find($scope.Tower, { TWR_CODE: Value.TWR_CODE } || { LCM_CODE: Value.LCM_CODE });
                                twr.ticked = true;
                                _.remove($scope.Tower, { ticked: Value.ticked = false });
                                $scope.SearchSpace.Tower[0] = twr;

                                var lcm = _.find($scope.Location, { LCM_CODE: Value.LCM_CODE });
                                lcm.ticked = true;
                                _.remove($scope.Location, { ticked: Value.ticked = false });
                                $scope.SearchSpace.Location[0] = lcm;

                                var flr = _.find($scope.Floor, { FLR_CODE: Value.FLR_CODE } || { LCM_CODE: Value.LCM_CODE } || { TWR_CODE: Value.TWR_CODE });
                                flr.ticked = true;
                                _.remove($scope.Floor, { ticked: Value.ticked == false });
                                $scope.SearchSpace.Floor[0] = flr;
                            }
                        });

                        SpaceRequisitionService.getShifts($scope.SearchSpace.Location).then(function (response) {
                            $scope.Shifts = response.data;
                        }, function (error) {
                            console.log(error);
                        });

                        MaploaderService.getVLANS().then(function (response) {
                            var first = "Def-VLAN";
                            response.data.sort(function (x, y) { return x.VL_Code == first ? 1 : y.VL_Code == first ? -1 : 0; });
                            $scope.VLANData = response.data;
                        }, function (error) {
                            console.log(error);
                        });

                        //MaploaderService.GetFlrIdbyLoc().then(function (response) {
                        //    $scope.LoadMap(response[0].FLR_ID);
                        //}, function (error) {
                        //});
                    }
                    UtilityService.getBussHeirarchy().then(function (response) {
                        if (response.data != null) {
                            $scope.BsmDet = response.data;
                        }

                    });
                });

            });

        });
        //    });
        //});
    }



    //UtilityService.getBussHeirarchy().then(function (response) {
    //    if (response.data != null) {
    //        $scope.BsmDet = response.data;
    //    }
    //});


    //To bind map
    $scope.LoadMap = function (flrid) {

        map.scrollWheelZoom.disable();
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });


        $scope.urlRedirection = UtilityService.path + '/SMViews/Map/ScheduleMySeatBookings.aspx?flr_id=' + flrid;

        $scope.Markerdata.clearLayers();
        $scope.MarkerLblLayer.clearLayers();
        $scope.MarkerLblLayerSpace.clearLayers();
        $scope.MarkerLblLayerEmpDetails.clearLayers();
        $scope.MarkerLblLayer_copy.clearLayers();
        $scope.MarkerLblLayerSpace_copy.clearLayers();
        $scope.MarkerLblLayerEmpDetails_copy.clearLayers();
        $scope.MarkerMeetingLblLayer.clearLayers();
        $scope.drawnItems = new L.FeatureGroup();
        $scope.drawnItemscheckWS = new L.FeatureGroup();
        $scope.drawnItemscheck = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        map.addLayer($scope.Markerdata);
        $scope.GradeAllocStatus();
        MaploaderService.bindMap(flrid).then(function (response) {
            if (response.mapDetails != null) {
                LOADLAYERS = response.mapDetails[0].LOADLAYERS;

                angular.forEach(response.mapDetails, function (value, index) {
                    // do something

                    var wkt = new Wkt.Wkt();
                    wkt.read(value.Wkt);

                    var theLayer = wkt.toObject();

                    theLayer.dbId = value.ID;
                    theLayer.options.color = '#000000';
                    //theLayer.options.weight = 1;
                    if (prefval == undefined) {
                        theLayer.options.weight = 1;

                    }
                    else theLayer.options.weight = 0.3;
                    theLayer.options.seattype = value.SEATTYPE;
                    theLayer.options.spacetype = value.layer;
                    theLayer.options.seatstatus = value.STAID;
                    theLayer.options.spacesubtype = value.SPC_SUB_TYPE;
                    theLayer.options.spaceid = value.SPACE_ID;
                    theLayer.options.checked = false;

                    //var col = response.COLOR[value.layer] == undefined ? '#E8E8E8' : response.COLOR[value.layer];
                    if (value.layer == "FRE")
                        theLayer.setStyle({ fillColor: '#FA8072', opacity: 0.5, fillOpacity: 0.5 });
                    else
                        theLayer.setStyle({ fillColor: '#E8E8E8', opacity: 0.5, fillOpacity: 0.5 });
                    $scope.drawnItems.addLayer(theLayer);
                    if (value.SPACE_ID != "" || value.layer == "FLR")
                        $scope.drawnItemscheckWS.addLayer(theLayer);
                    //}
                });


                $scope.bounds = [[response.BBOX[0].MinY, response.BBOX[0].MinX], [response.BBOX[0].MaxY, response.BBOX[0].MaxX]];

                var formattedString = response.BBOX[0].LAYOUTNAME.split("/").join("\n");
                var k = formattedString.split("\n");

                var fulltext = "";

                for (i = 0; i < k.length; i++) {

                    if (i == 0) {
                        fulltext += k[i].bold();
                    }
                    else {
                        fulltext += "/" + k[i];
                    }
                }

                document.getElementById("LAYOUTNAME").innerHTML = fulltext;

                $scope.SU = response.BBOX[0].SU;
                $scope.WorkArea = response.BBOX[0].WorkArea;
                $scope.TotalArea = response.BBOX[0].TotalArea;
                $scope.LastRefreshDate = response.BBOX[0].LastRefreshDate;

                map.fitBounds($scope.bounds);
                map.whenReady(function (e) {
                    setTimeout(function () {
                        $scope.$apply(function () {
                            $scope.ZoomLvl = map.getZoom();
                        });
                    }, 200);
                });

                if (response.mapDetails2 != null)
                    $scope.chairicons = response.mapDetails2;

                $scope.floorDetails = response.FloorDetails;
                if (LOADLAYERS == 1) {
                    $scope.LoadMarkers(response.FloorDetails);
                }
                else
                    $scope.LoadALLMarkers(response.FloorDetails);

            }

        });


    }
    //To bind chair layers
    $scope.LoadMapLayers = function (flrid) {
        map.scrollWheelZoom.disable();
        cnt = cnt + 1;
        angular.forEach($scope.chairicons, function (value, index) {
            // do something

            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);

            var theLayer = wkt.toObject();

            theLayer.dbId = value.ID;
            theLayer.options.color = '#000000';
            //theLayer.options.weight = 1;
            if (prefval == undefined) {
                theLayer.options.weight = 1;

            }
            else theLayer.options.weight = 0.3;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.spacesubtype = value.SPC_SUB_TYPE;
            theLayer.options.spaceid = value.SPACE_ID;
            theLayer.options.checked = false;


            if (value.layer == "FRE")
                theLayer.setStyle({ fillColor: '#FA8072', opacity: 0.5, fillOpacity: 0.5 });
            else
                theLayer.setStyle({ fillColor: '#E8E8E8', opacity: 0.5, fillOpacity: 0.5 });
            $scope.drawnItems.addLayer(theLayer);
            if (value.layer == "CHA" && value.SPACE_ID != '')
                $scope.drawnItemscheck.addLayer(theLayer);
            //}
        });

        $scope.LoadMarkerLayers(GetParameterValues('flr_code'));

    }

    function updateSummaryCount() {
        var themeobj =
        {
            flr_code: GetParameterValues('flr_code'),
            from_date: $scope.Maploader.FROM_DATE1,
            to_date: $scope.Maploader.TO_DATE1,
            sh_code: $scope.Maploader.Search_SH_CODE
        };

        MaploaderService.getLegendsSummary(themeobj).then(function (response) {

            if (response != null) {

                $scope.SeatSummary = response;

            }
        }, function (error) {
        });
    }
    //To assign markers to chairs
    $scope.LoadMarkerLayers = function (flrdata) {
        printcount = printcount + 1;
        $scope.EmpAllocSeats = [];
        $scope.Markers = [];

        angular.forEach($scope.chairlayers, function (value, index) {
            var style = {};
            if (value.SPC_TYPE_STATUS == 2) {

                switch (value.STATUS) {
                    case 1: chairicon = VacantStyle;
                        break;
                    case 1001: chairicon = AllocateStyle;
                        break;
                    case 1002: chairicon = AllocateStyle;
                        break;
                    case 1004: chairicon = OccupiedStyle;
                        break;
                    case 1003: chairicon = AllocateStyle;
                        break;
                    case 1006: chairicon = ReservedStyle;
                        break;
                    case 1052: chairicon = BlockStyle;
                        break;
                    default: chairicon = VacantStyle;
                        break;
                }
                if (value.SPC_STA_ID == '0') {
                    switch (value.SPC_STA_ID) {
                        case 0: chairicon = InactiveStyle;
                            break;
                    }
                }

                if (value.AUR_LEAVE_STATUS == 'Y') {
                    switch (value.AUR_LEAVE_STATUS) {
                        case 'Y': chairicon = LeaveStyle;
                            break;
                    }
                }

                $.each($scope.drawnItemscheck._layers, function (Key, layer) {
                    if (layer.options.spaceid == value.SPC_ID && layer.options.spacetype == 'CHA') {
                        layer.off('click', markerclicked);
                        $scope.marker = layer;
                    }
                });
                //$scope.marker = _.find($scope.drawnItems._layers, { options: { spaceid: value.SPC_ID, spacetype: 'CHA' } });
                $scope.marker.setStyle(chairicon);
                $scope.marker.lat = value.x;
                $scope.marker.lon = value.y;
                $scope.marker.SPC_ID = value.SPC_ID;
                $scope.marker.SPC_NAME = value.SPC_NAME;
                $scope.marker.SPC_DESC = value.SPC_DESC;
                $scope.marker.layer = value.SPC_TYPE_CODE;
                $scope.marker.VERTICAL = value.VERTICAL;
                $scope.marker.VER_NAME = value.VER_NAME;
                $scope.marker.COSTCENTER = value.COSTCENTER;
                $scope.marker.SPC_TYPE_CODE = value.SPC_TYPE_CODE;
                $scope.marker.SPC_TYPE_NAME = value.SPC_TYPE_NAME;
                $scope.marker.SST_CODE = value.SST_CODE;
                $scope.marker.SST_NAME = value.SST_NAME;
                $scope.marker.SHIFT_TYPE = value.SHIFT_TYPE;
                $scope.marker.VLAN_TYPE = value.VLAN_TYPE;
                $scope.marker.SSAD_AUR_ID = value.SSAD_AUR_ID;
                $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                $scope.marker.STACHECK = value.STACHECK;
                $scope.marker.STATUS = value.STATUS;
                $scope.marker.STA_DESC = value.STA_DESC;
                $scope.marker.AUR_LEAVE_STATUS = value.AUR_LEAVE_STATUS;
                $scope.marker.ticked = false;



                var formattedString = value.SPC_DESC.split(",").join("\n");
                var k = formattedString.split("\n");
                var id = "";
                var id1 = "";
                var text = "";
                var fulltext = "";
                var str = " ";

                fulltext = BindMarkerText(formattedString, $scope.marker);


                if ($scope.SeatPopUpDetails.SYSP_VAL1 == 1) {
                    var role = _.find($scope.GetRoleAndRM);
                    if (role.AUR_ROLE == 6) {
                        if (value.SSAD_AUR_ID == UserId || $scope.marker.STATUS == 1) {
                            $scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: "my-label",
                                zIndexOffset: 2000
                            });//.setOpacity(1);
                        }
                    } else {
                        $scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                            noHide: false,
                            direction: 'auto',
                            className: "my-label",
                            zIndexOffset: 2000
                        });
                    }

                } else {
                    $scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                        noHide: false,
                        direction: 'auto',
                        className: "my-label",
                        zIndexOffset: 2000
                    });
                }

                $scope.marker.on('contextmenu', function (e) {
                    var role = _.find($scope.GetRoleAndRM);
                    if (role.AUR_ROLE != 1) {
                        progress(0, 'Loading...', false);
                        showNotification('error', 8, 'bottom-right', 'Employee cannot Inactive Spaces');
                        return;
                    }
                    else {
                        $ngConfirm({
                            icon: 'fa fa-warning',
                            title: 'Confirm!',
                            content: 'Are You Sure You Want To Inactive Seat',
                            closeIcon: true,
                            closeIconClass: 'fa fa-close',
                            buttons: {
                                Confirm: {
                                    text: 'Sure',
                                    btnClass: 'btn-blue',
                                    action: function (button) {
                                        var spcdet = {};
                                        $scope.selectedActSpaces = [];
                                        spcdet.SPACE_ID = value.SPC_ID;
                                        spcdet.SPC_TYPE = value.SPC_TYPE_NAME;
                                        spcdet.SPC_SUB_TYPE = value.SST_NAME;
                                        spcdet.ticked = true;
                                        $scope.selectedActSpaces.push(spcdet);

                                        var dataobj = { FLR_CODE: e.target.SPC_FLR_ID, SPC_LST: $scope.selectedActSpaces };
                                        MaploaderService.InactiveSeats(dataobj).then(function (response) {
                                            _.remove($scope.Markers, { SPC_ID: e.target.SPC_ID });
                                            reloadMarkers();
                                            $ngConfirm({
                                                icon: 'fa fa-check',
                                                title: 'Confirm!',
                                                content: 'Selected Space Has Been Inactive',
                                                buttons: {
                                                    Confirm: {
                                                        text: 'Ok',
                                                        btnClass: 'btn-blue'
                                                    },
                                                }
                                            });
                                        });

                                    }
                                },
                                close: function ($scope, button) {
                                }
                            }
                        });
                    }
                });

                if (value.STATUS != 1006)
                    $scope.marker.on('click', markerclicked);

                $scope.Markers.push($scope.marker);
                $scope.Markerdata.addLayer($scope.marker);
                //console.log($scope.Markers);
            }

        });
        $scope.gridOptions.api.setRowData($scope.Markers);
        $scope.gridOptions.api.refreshHeader();
        progress(0, '', false);
        if (printcount == 2 && LOADLAYERS == 1) {
            $scope.MarkerMeetingLblLayer.clearLayers();
            print('leafletMap');
        }
        if (GetParameterValues('value') == 1) {

            var marker = _.find($scope.Markers, { SPC_ID: GetParameterValues('spc_id') });

            var chairicon = new L.icon({ iconUrl: '/images/marker.png', iconSize: [25, 32] });

            var empMarker = L.marker(L.latLng(marker.lat, marker.lon), {
                icon: chairicon,
                draggable: false
            }).addTo(map);
            if (map.hasLayer($scope.MarkerLblLayerSpace))
                map.removeLayer($scope.MarkerLblLayerSpace);

            setTimeout(function () {
                if (empMarker) { // check
                    map.removeLayer(empMarker); // remove
                    if (!map.hasLayer($scope.MarkerLblLayerSpace)) {
                        map.addLayer($scope.MarkerLblLayerSpace);
                    }
                }
            }, 20000);

            if ($scope.ZoomLvl >= map.getZoom()) {

                //map.fitBounds($scope.bounds);
                setTimeout(function () { map.setView(L.latLng(marker.lat, marker.lon), map.getZoom() + 2); }, 200);
            }
            setTimeout(function () { map.panTo(L.latLng(marker.lat, marker.lon)); }, 400);


            setTimeout(function () {
                $('#Searchdata').selectpicker('refresh');
            }, 200);
        }
        map.scrollWheelZoom.enable();
    };

    //To assign markers to desks
    $scope.LoadMarkers = function (flrdata) {
        $scope.EmpAllocSeats = [];
        $scope.flr_code = flrdata;
        var params = {
            flr_code: flrdata,
            from_date: $scope.Maploader.FROM_DATE1,
            to_date: $scope.Maploader.TO_DATE1,
            sh_code: $scope.Maploader.Search_SH_CODE,
            monitor_type: $scope.Maploader.Search_MO_TYPE
        }

        MaploaderService.bindMarkers(params).then(function (response) {
            if (response != null) {
                $scope.chairlayers = response;
                angular.forEach(response, function (value, index) {
                    var style = {};

                    if (value.SPC_TYPE_STATUS == 2) {

                        $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {
                            if (layer.options.spaceid == value.SPC_ID) {
                                layer.removeEventListener(); //fix double event firing issue
                                $scope.marker = layer;
                            }
                        });

                        //$scope.marker = _.find($scope.drawnItems._layers, { options: { spaceid: value.SPC_ID } });
                        $scope.marker.lat = value.x;
                        $scope.marker.lon = value.y;
                        $scope.marker.SPC_ID = value.SPC_ID;
                        $scope.marker.SPC_NAME = value.SPC_NAME;
                        $scope.marker.SPC_DESC = value.SPC_DESC;
                        $scope.marker.layer = value.SPC_TYPE_CODE;
                        $scope.marker.VERTICAL = value.VERTICAL;
                        $scope.marker.VER_NAME = value.VER_NAME;
                        $scope.marker.COSTCENTER = value.COSTCENTER;
                        $scope.marker.SPC_TYPE_CODE = value.SPC_TYPE_CODE;
                        $scope.marker.SPC_TYPE_NAME = value.SPC_TYPE_NAME;
                        $scope.marker.SST_CODE = value.SST_CODE;
                        $scope.marker.SST_NAME = value.SST_NAME;
                        $scope.marker.SHIFT_TYPE = value.SHIFT_TYPE;
                        $scope.marker.VLAN_TYPE = value.VLAN_TYPE;
                        $scope.marker.SSAD_AUR_ID = value.SSAD_AUR_ID;
                        $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                        $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                        $scope.marker.STACHECK = value.STACHECK;
                        $scope.marker.STATUS = value.STATUS;
                        $scope.marker.STA_DESC = value.STA_DESC;
                        $scope.marker.AUR_LEAVE_STATUS = value.AUR_LEAVE_STATUS;
                        $scope.marker.ticked = false;
                        //.fontcolor("green")

                        //var formattedString = value.SPC_DESC.split(",").join("\n");
                        if (value.SPC_DESC != null && value.SPC_DESC != undefined) {
                            var formattedString = value.SPC_DESC.split(",").join("\n");
                        }
                        else {
                            var formattedString = '';
                        }
                        var k = formattedString.split("\n");
                        var id = "";
                        var id1 = "";
                        var text = "";
                        var fulltext = "";
                        var str = " ";

                        fulltext = BindMarkerText(formattedString, $scope.marker);



                        //$scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                        //    noHide: false,
                        //    direction: 'auto',
                        //    className: "my-label",
                        //    zIndexOffset: 2000
                        //});//.setOpacity(1);

                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_ID_EMP_NAMES,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabel1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_ID_EMP_NAMES,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelspace = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelspace1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelEmp = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.EMP_NAMES,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelEmp1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.EMP_NAMES,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3],

                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });

                        $scope.MarkerLblLayer_copy.addLayer($scope.MarkerLabel1);
                        $scope.MarkerLblLayer.addLayer($scope.MarkerLabel);

                        $scope.MarkerLblLayerSpace.addLayer($scope.MarkerLabelspace);
                        $scope.MarkerLblLayerSpace_copy.addLayer($scope.MarkerLabelspace1);

                        $scope.MarkerLblLayerEmpDetails.addLayer($scope.MarkerLabelEmp);
                        $scope.MarkerLblLayerEmpDetails_copy.addLayer($scope.MarkerLabelEmp1);


                        $scope.Markers.push($scope.marker);

                    }
                    else {

                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabel1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        //$scope.MarkerLabel.bindLabel(value.SPC_DESC, {
                        //    noHide: false,
                        //    direction: 'auto',
                        //    zIndexOffset: 2000
                        //});//.setOpacity(1);
                        $scope.MarkerLblLayer_copy.addLayer($scope.MarkerLabel1);
                        $scope.MarkerLblLayer.addLayer($scope.MarkerLabel);

                        $scope.MarkerMeetingLblLayer_copy.addLayer($scope.MarkerLabel);
                        $scope.MarkerMeetingLblLayer.addLayer($scope.MarkerLabel);

                    }
                });



                $scope.gridOptions.api.setRowData($scope.Markers);
                $scope.gridOptions.api.refreshHeader();
                progress(0, '', false);
                //$scope.gridOptions.api.sizeColumnsToFit();
            }
            else {
                showNotification('error', 8, 'bottom-right', 'No records Found');
            }
            $scope.loadThemes();
            let searchParams = new URLSearchParams(window.location.search);
            if (searchParams.has('spc_id')) {
                space_id = searchParams.get('spc_id');
                var space = {};
                space = { 'Code': space_id, 'SPC_ID': space_id, 'NAME': '' }

                $scope.ShowEmponMap(space);
            }
            $scope.show();
        });

        updateSummaryCount();

    }
    //To assign all markers
    $scope.LoadALLMarkers = function (flrdata) {

        $scope.EmpAllocSeats = [];

        var params = {
            flr_code: flrdata,
            from_date: $scope.Maploader.FROM_DATE1,
            to_date: $scope.Maploader.TO_DATE1,
            sh_code: $scope.Maploader.Search_SH_CODE,
            monitor_type: $scope.Maploader.Search_MO_TYPE
        }

        MaploaderService.bindMarkers(params).then(function (response) {

            if (response != null) {
                angular.forEach(response, function (value, index) {
                    var style = {};
                    if (value.SPC_TYPE_STATUS == 2) {

                        switch (value.STATUS) {
                            case 1: chairicon = VacantStyle;
                                break;
                            case 1001: chairicon = AllocateStyle;
                                break;
                            case 1002: chairicon = AllocateStyle;
                                break;
                            case 1004: chairicon = OccupiedStyle;
                                break;
                            case 1003: chairicon = AllocateStyle;
                                break;
                            case 1006: chairicon = ReservedStyle;
                                break;
                            case 1052: chairicon = BlockStyle;
                                break;
                            default: chairicon = VacantStyle;
                                break;
                        }
                        if (value.SPC_STA_ID == '0') {
                            switch (value.SPC_STA_ID) {
                                case 0: chairicon = InactiveStyle;
                                    break;
                            }
                        }

                        if (value.AUR_LEAVE_STATUS == 'Y') {
                            switch (value.AUR_LEAVE_STATUS) {
                                case 'Y': chairicon = LeaveStyle;
                                    break;
                            }
                        }

                        $.each($scope.drawnItems._layers, function (Key, layer) {
                            if (layer.options.spaceid == value.SPC_ID && layer.options.spacetype == 'CHA') {
                                layer.removeEventListener(); //fix double event firing issue
                                $scope.marker = layer;
                            }
                        });

                        // $scope.marker = _.find($scope.drawnItems._layers, { options: { spaceid: value.SPC_ID, spacetype: 'CHA' } });
                        $scope.marker.setStyle(chairicon);
                        $scope.marker.lat = value.x;
                        $scope.marker.lon = value.y;
                        $scope.marker.SPC_ID = value.SPC_ID;
                        $scope.marker.SPC_NAME = value.SPC_NAME;
                        $scope.marker.SPC_DESC = value.SPC_DESC;
                        $scope.marker.layer = value.SPC_TYPE_CODE;
                        $scope.marker.VERTICAL = value.VERTICAL;
                        $scope.marker.VER_NAME = value.VER_NAME;
                        $scope.marker.COSTCENTER = value.COSTCENTER;
                        $scope.marker.SPC_TYPE_CODE = value.SPC_TYPE_CODE;
                        $scope.marker.SPC_TYPE_NAME = value.SPC_TYPE_NAME;
                        $scope.marker.SST_CODE = value.SST_CODE;
                        $scope.marker.SST_NAME = value.SST_NAME;
                        $scope.marker.SHIFT_TYPE = value.SHIFT_TYPE;
                        $scope.marker.VLAN_TYPE = value.VLAN_TYPE;
                        $scope.marker.SSAD_AUR_ID = value.SSAD_AUR_ID;
                        $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                        $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                        $scope.marker.STACHECK = value.STACHECK;
                        $scope.marker.STATUS = value.STATUS;
                        $scope.marker.STA_DESC = value.STA_DESC;
                        $scope.marker.AUR_LEAVE_STATUS = value.AUR_LEAVE_STATUS;
                        $scope.marker.ticked = false;

                        $scope.marker.on('contextmenu', function (e) {
                            var role = _.find($scope.GetRoleAndRM);
                            if (role.AUR_ROLE != 1) {
                                progress(0, 'Loading...', false);
                                showNotification('error', 8, 'bottom-right', 'Employee cannot Inactive Spaces');
                                return;
                            }
                            else {
                                $ngConfirm({
                                    icon: 'fa fa-warning',
                                    title: 'Confirm!',
                                    content: 'Are You Sure You Want To Inactive Seat',
                                    closeIcon: true,
                                    closeIconClass: 'fa fa-close',
                                    buttons: {
                                        Confirm: {
                                            text: 'Sure',
                                            btnClass: 'btn-blue',
                                            action: function (button) {
                                                var spcdet = {};
                                                $scope.selectedActSpaces = [];
                                                spcdet.SPACE_ID = value.SPC_ID;
                                                spcdet.SPC_TYPE = value.SPC_TYPE_NAME;
                                                spcdet.SPC_SUB_TYPE = value.SST_NAME;
                                                spcdet.ticked = true;
                                                $scope.selectedActSpaces.push(spcdet);

                                                var dataobj = { FLR_CODE: e.target.SPC_FLR_ID, SPC_LST: $scope.selectedActSpaces };
                                                MaploaderService.InactiveSeats(dataobj).then(function (response) {
                                                    _.remove($scope.Markers, { SPC_ID: e.target.SPC_ID });
                                                    reloadMarkers();
                                                    $ngConfirm({
                                                        icon: 'fa fa-check',
                                                        title: 'Confirm!',
                                                        content: 'Selected Space Has Been Inactive',
                                                        buttons: {
                                                            Confirm: {
                                                                text: 'Ok',
                                                                btnClass: 'btn-blue'
                                                            },
                                                        }
                                                    });
                                                });

                                            }
                                        },
                                        close: function ($scope, button) {
                                        }
                                    }
                                });
                            }
                        });

                        if (value.SPC_DESC != null && value.SPC_DESC != undefined) {
                            var formattedString = value.SPC_DESC.split(",").join("\n");
                        }
                        else {
                            var formattedString = '';
                        }
                        //var formattedString = value.SPC_DESC.split(",").join("\n");
                        var k = formattedString.split("\n");
                        var id = "";
                        var id1 = "";
                        var text = "";
                        var fulltext = "";
                        var str = " ";
                        fulltext = BindMarkerText(formattedString, $scope.marker);
                        if ($scope.SeatPopUpDetails.SYSP_VAL1 == 1) {
                            var role = _.find($scope.GetRoleAndRM);
                            if (role.AUR_ROLE == 6) {
                                if (value.SSAD_AUR_ID == UserId || $scope.marker.STATUS == 1) {
                                    $scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                                        noHide: false,
                                        direction: 'auto',
                                        className: "my-label",
                                        zIndexOffset: 2000
                                    });//.setOpacity(1);
                                }
                            } else {
                                $scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                                    noHide: false,
                                    direction: 'auto',
                                    className: "my-label",
                                    zIndexOffset: 2000
                                });
                            }

                        } else {
                            $scope.marker.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: "my-label",
                                zIndexOffset: 2000
                            });
                        }
                        if (value.STATUS != 1006)
                            $scope.marker.on('click', markerclicked);


                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_ID_EMP_NAMES,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabel1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_ID_EMP_NAMES,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelspace = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelspace1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelEmp = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.EMP_NAMES,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabelEmp1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.EMP_NAMES,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });

                        $scope.MarkerLblLayer_copy.addLayer($scope.MarkerLabel1);
                        $scope.MarkerLblLayer.addLayer($scope.MarkerLabel);

                        $scope.MarkerLblLayerSpace.addLayer($scope.MarkerLabelspace);
                        $scope.MarkerLblLayerSpace_copy.addLayer($scope.MarkerLabelspace1);

                        $scope.MarkerLblLayerEmpDetails.addLayer($scope.MarkerLabelEmp);
                        $scope.MarkerLblLayerEmpDetails_copy.addLayer($scope.MarkerLabelEmp1);

                        $scope.Markers.push($scope.marker);
                        $scope.Markerdata.addLayer($scope.marker);
                    }
                    else {

                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [10, 10],
                                labelAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabel1 = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [3, 3],
                                labelAnchor: [3, 3]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });

                        $scope.MarkerLabel.bindLabel(value.SPC_DESC, {
                            noHide: false,
                            direction: 'auto',
                            zIndexOffset: 2000
                        });//.setOpacity(1);
                        $scope.MarkerMeetingLblLayer_copy.addLayer($scope.MarkerLabel1);
                        $scope.MarkerMeetingLblLayer.addLayer($scope.MarkerLabel);

                    }

                });

                $scope.gridOptions.api.setRowData($scope.Markers);
                $scope.gridOptions.api.refreshHeader();

                progress(0, '', false);
                //setTimeout(function () {
                //    $scope.loadThemes();
                //}, 500);
                $scope.show();
                //$scope.gridOptions.api.sizeColumnsToFit();
            } else {
                showNotification('error', 8, 'bottom-right', 'No records Found');
            }
            $scope.loadThemes();
            if (GetParameterValues('value') == 1) {

                var marker = _.find($scope.Markers, { SPC_ID: GetParameterValues('spc_id') });

                var chairicon = new L.icon({ iconUrl: '/images/marker.png', iconSize: [25, 32] });

                var empMarker = L.marker(L.latLng(marker.lat, marker.lon), {
                    icon: chairicon,
                    draggable: false
                }).addTo(map);
                if (map.hasLayer($scope.MarkerLblLayerSpace))
                    map.removeLayer($scope.MarkerLblLayerSpace);

                setTimeout(function () {
                    if (empMarker) { // check
                        map.removeLayer(empMarker); // remove
                        if (!map.hasLayer($scope.MarkerLblLayerSpace)) {
                            map.addLayer($scope.MarkerLblLayerSpace);
                        }
                    }
                }, 20000);

                if ($scope.ZoomLvl >= map.getZoom()) {

                    //map.fitBounds($scope.bounds);
                    setTimeout(function () { map.setView(L.latLng(marker.lat, marker.lon), map.getZoom() + 2); }, 200);
                }
                setTimeout(function () { map.panTo(L.latLng(marker.lat, marker.lon)); }, 400);


                setTimeout(function () {
                    $('#Searchdata').selectpicker('refresh');
                }, 200);
            }
        }, function (error) {
        });
        updateSummaryCount();
    }

    UtilityService.GetRoleByUserId().then(function (response) {
        if (response.data != null) {
            $scope.Role = response.data;
            if ($scope.Role.Rol_id == 'N') {
                $scope.gridOptions.columnApi.setColumnVisible('ticked', false)
            }
            else { $scope.gridOptions.columnApi.setColumnVisible('ticked', true) }
        }
    });

    function markerclicked(e) {

        $scope.ShiftData = $scope.Shifts.filter(
            (item) => item.SH_SEAT_TYPE == this.SHIFT_TYPE
        );


        var IN_ACTIVE = _.find($scope.InactivespaceidList, { IN_SPC_ID: this.SPC_ID })
        if (IN_ACTIVE != undefined) {
            this.ticked = false;
            return;
        }

        var role = _.find($scope.GetRoleAndRM);

        if (((role.AUR_ROLE == 6 && (this.SPC_TYPE_CODE == 'MD' || this.SPC_TYPE_CODE == 'DD')) || (role.AUR_ROLE == 0 && this.SPC_TYPE_CODE == 'DD')
            || (role.AUR_ROLE != 1 && (this.SPC_TYPE_CODE == 'CB'))
        )) {

            $ngConfirm({
                icon: 'fa fa-warning',
                title: 'Allocation Warning',
                content: 'You are not allowed to book this seat',
                closeIcon: true,
                closeIconClass: 'fa fa-close',
                buttons: {
                    close: function ($scope, button) {
                    }
                }
            });
            return;
        }

        if ($scope.VerticalWiseAlloc.SYSP_VAL1 == 1 && this.VERTICAL != "" && this.VERTICAL != role.VER_CODE && role.AUR_ROLE != 1) {
            $ngConfirm({
                icon: 'fa fa-warning',
                title: 'Allocation Warning',
                content: 'You are not allowed to book this seat',
                closeIcon: true,
                closeIconClass: 'fa fa-close',
                buttons: {
                    close: function ($scope, button) {
                    }
                }
            });
        }
        else {

            //if ($scope.selectedSpaces.length != 0 && role.AUR_ROLE == 6 && this.ticked == false) {
            //    showNotification('error', 8, 'bottom-right', 'Employee can not access more than 1 seat');
            //    return;
            //}


            if (this.SSAD_AUR_ID != '' && this.SSAD_AUR_ID != null) {
                sta = this.STATUS;
                aur = this.SSAD_AUR_ID.trim();
            }
            else {

                sta = this.STATUS;
                aur = this.SSAD_AUR_ID;
            }

            //swethan start
            //if (role.AUR_ROLE == 6) {

            //swethan start
            //if (this.STATUS != 1 && this.STATUS != 1003 && this.STATUS != 1002 && this.SHIFT_TYPE == 1 && role.AUR_ID != aur) {
            //    showNotification('error', 8, 'bottom-right', 'Employee can not access a dedicated seat');
            //    return;
            //}
            //swethan end

            //if (this.STATUS != 1 && this.STATUS != 1003 && this.STATUS != 1002 && this.SHIFT_TYPE == 2 && role.AUR_ID != aur) {
            //    $scope.ReleaseEnabledStatus = true;
            //    return;
            //}
            //}

            //swethan end


            //if (role.AUR_ROLE == 6 && this.STATUS != 1 && this.STATUS != 1003 && this.STATUS != 1002 && this.SHIFT_TYPE == 1) {
            //    showNotification('error', 8, 'bottom-right', 'Employee can not access a dedicated seat');
            //    return;
            //}
            if (!this.ticked) {
                if ($scope.Role.Rol_id == 'N') {
                    this.ticked = false;
                }
                else {
                    if ($scope.GradeAlloc.SYSP_VAL1 = 1 && $scope.Allow_Remaining <= $scope.selectedSpaces.length && $scope.Grade_Allow_status == 'Allow' && $scope.Eligibility != 'No') {
                        $scope.AllocateEnabledStatus = true;

                        $ngConfirm({
                            icon: 'fa fa-warning',
                            title: 'Allocation Warning',
                            content: 'You have exceeded maxmimum no of allocations.Current limit is ' + $scope.ORG_ALLOC_COUNT + '. You can allocate another ' + $scope.Allow_Remaining + ' seats only',
                            closeIcon: true,
                            closeIconClass: 'fa fa-close',
                            buttons: {
                                close: function ($scope, button) {
                                }
                            }
                        });
                    }
                    this.setStyle(selctdChrStyle)
                    this.ticked = true;
                    var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, category: this.SPC_ID, subitem: this.SSA_SRNREQ_ID ? this.SSA_SRNREQ_ID : "" };
                    MaploaderService.GetSpaceDetailsByREQID(dataobj).then(function (response) {
                        if (response != null) {
                            var VLAN = _.find($scope.VLANData, { VL_Code: 'Def-VLAN' });
                            var emp = _.find($scope.Employee);
                            angular.forEach(response, function (value, index) {
                                value.FROM_DATE = $scope.Maploader.FROM_DATE1;
                                value.TO_DATE = $scope.Maploader.TO_DATE1;
                                value.SH_CODE = $scope.ShiftData[0].SH_CODE;
                                value.VL_Code = VLAN.VL_Code;

                                if (role.AUR_ROLE == 6) {
                                    value.AUR_LEAVE_STATUS = "N";
                                    value.SSAD_SRN_REQ_ID = null;
                                    value.STACHECK = 16;
                                    value.STATUS = 1004;
                                    value.SSA_SRNREQ_ID = null;
                                    value.AUR_NAME = role.AUR_KNOWN_AS;
                                    value.AUR_ID = role.AUR_ID;
                                    value.VER_CODE = role.VER_CODE;
                                    value.VER_NAME = role.VER_NAME;
                                    value.Cost_Center_Code = role.COST_CENTER_CODE;
                                    value.Cost_Center_Name = role.COST_CENTER_NAME;


                                    value.ticked = false;
                                    value.disabled = true;
                                    if (value.AUR_ID == emp.AUR_ID || value.AUR_ID == "") {
                                        value.ticked = true;
                                        value.disabled = false;
                                    }
                                }
                                else
                                {
                                    value.AUR_NAME = "";
                                    value.AUR_ID = "";
                                    value.Cost_Center_Code = "";
                                    value.Cost_Center_Name = "";
                                    value.SSAD_SRN_REQ_ID = "";
                                    value.SSA_SRNREQ_ID = "";
                                }


                                if ($scope.selectedSpaces.length != 0) {
                                    //fetch all space ids
                                    var spaceids = $scope.selectedSpaces.map(
                                        (item) => item.SPC_ID);
                                    // check if space id not exist then only it adds list
                                    if (!spaceids.includes(value.SPC_ID)) {
                                        $scope.selectedSpaces.push(value);
                                    }
                                }
                                else {
                                    $scope.selectedSpaces.push(value);
                                }
                                $scope.selectedSpacesLength = $scope.selectedSpaces.length;
                                if (role.AUR_ROLE == 6) {
                                    value.block = true;
                                    //var ver = _.find($scope.selectedSpaces, function (x) { if (x.VERTICAL == role.VER_CODE) return x });
                                    //if (ver == undefined) {
                                    //    $scope.selectedSpaces = [];
                                    //    showNotification('error', 8, 'bottom-right', 'you can not allocate for different verticals in single seat');
                                    //    reloadMarkers();
                                    //    return;

                                    //}
                                    //var cost = _.find($scope.selectedSpaces, function (x) { if ((x.STATUS == 1003 || x.STATUS == 1004) && x.Cost_Center_Code == role.COST_CENTER_CODE) return x });
                                    //if (cost == undefined) {
                                    //    $scope.selectedSpaces = [];
                                    //    showNotification('error', 8, 'bottom-right', 'you can not allocate for different costcenter to employee');
                                    //    reloadMarkers();
                                    //    return;

                                    //}

                                    //}
                                }
                            })

                            var data = { lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: role.AUR_ID, mode: 1 };
                            $scope.EmpAllocSeats = {};
                            //if (role.AUR_ROLE == 6 && sta != 1 && sta != 1003 && sta != 1002 && role.AUR_ID != aur) {
                            MaploaderService.getEmpAllocSeat(data).then(function (result) {
                                if (result.data) {
                                    $scope.EmpAllocSeats = result.data;
                                    if (role.AUR_ROLE == 6) {
                                        $scope.ProallocEnabledStatus = true;
                                    }
                                    progress(0, '', false);
                                }
                            });
                        }
                        $scope.Maploader.FROM_DATE = $scope.selectedSpaces[0].FROM_DATE;
                        $scope.Maploader.TO_DATE = $scope.selectedSpaces[0].TO_DATE;

                        $scope.Maploader.FROM_TIME = $scope.selectedSpaces[0].FROM_TIME;
                        $scope.Maploader.TO_TIME = $scope.selectedSpaces[0].TO_TIME;
                        //$scope.Maploader.FROM_DATE = $scope.selectedSpaces[0].FROM_DATE;


                        $scope.EmpAllocSeats = _.remove($scope.EmpAllocSeats, function (n) {
                            return _.find($scope.gridSpaceAllocOptions.rowData, { AUR_ID: n.AUR_ID });
                        });
                        $scope.EmpAllocSeats = _.uniqBy($scope.EmpAllocSeats, 'AUR_ID');
                    }, function (error) {
                    });

                    //pk
                    var searchingSpc_Id = this.SPC_ID;
                    var dataobj = { lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: this.SPC_ID, mode: 1, spcid: this.SPC_ID, from_date: $scope.Maploader.FROM_DATE1, to_date: $scope.Maploader.TO_DATE1 };
                    MaploaderService.getBookedSeatDetailsBySpace(dataobj).then(function (result) {
                        if (result.data.length > 0) {
                            //check if this item is alreay we have
                            angular.forEach(result.data, function (value, index) {
                                var isAlreayAdded = _.some($scope.SeatAllocForSpace, { 'SSA_SRNREQ_ID': value.SSA_SRNREQ_ID });
                                if (!isAlreayAdded) {
                                    $scope.SeatAllocForSpace.push(value);
                                }
                            })
                        }
                        else {

                            console.log("No Booked Seat for" + searchingSpc_Id)
                        }

                    });
                }
            }
            else {
                switch (this.STATUS) {
                    case 1: chairicon = VacantStyle;
                        break;
                    case 1002: chairicon = AllocateStyle;
                        break;
                    case 1004: chairicon = OccupiedStyle;
                        break;
                    case 1003: chairicon = AllocateStyle;
                        break;
                    case 1052: chairicon = BlockStyle;
                        break;

                    default: chairicon = VacantStyle;
                        break;
                }
                _.remove($scope.selectedSpaces, function (space) { return space.SPC_ID == e.target.SPC_ID });

                if ($scope.SeatAllocForSpace.length > 0) {
                    _.remove($scope.SeatAllocForSpace, function (space) { return space.SPC_ID == e.target.SPC_ID });
                }

                $scope.selectedSpacesLength = $scope.selectedSpaces.length;
                if ($scope.GradeAlloc.SYSP_VAL1 = 1 && $scope.Allow_Remaining >= ($scope.selectedSpacesLength) && $scope.Grade_Allow_status == 'Allow' && $scope.Eligibility != 'No') {
                    $scope.AllocateEnabledStatus = false;

                }

                this.setStyle(chairicon)
                this.ticked = false;
            }
            $scope.gridOptions.api.refreshView();
        }
    }
    /****************  Grid Display ***************/

    var columnDefs = [
        { headerName: "Select", field: "ticked", width: 90, template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkedChanged(data)' />", cellClass: 'grid-align', suppressFilter: true },
        //{ headerName: "Space ID", field: "SPC_ID", cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)" },
        { headerName: "Space ID", field: "SPC_NAME", cellClass: 'grid-align', enableSorting: true },
        { headerName: "Space Type", field: "SPC_TYPE_NAME", enableSorting: true },
        { headerName: "Space Sub Type", field: "SST_NAME", enableSorting: true },
        { headerName: "Status", field: "STA_DESC", enableSorting: true }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        showToolPanel: true,
        rowAggregatePanelShow: 'none',
        enableColResize: true,
        enableCellSelection: false,
        suppressRowClickSelection: true,
        onready: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode('Select All');
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = true;
                        value.setStyle(selctdChrStyle)
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = false;
                        value.setStyle(VacantStyle)
                        switch (value.STATUS) {
                            case 1: value.setStyle(VacantStyle);
                                break;
                            case 1002: value.setStyle(AllocateStyle);
                                break;
                            case 1004: value.setStyle(OccupiedStyle);
                                break;
                            case 1003: value.setStyle(AllocateStyle);
                                break;
                            case 1052: value.setStyle(BlockStyle);
                                break;
                            default: value.setStyle(VacantStyle);
                                break;
                        }

                    });
                });
            }
        });
        return eHeader;
    }

    /****************  Filter ***************/

    $("#filtertxt").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    });

    $("#InacFilter").change(function () {
        onSpaceReqFilterChanged($(this).val());
    }).keydown(function () {
        onSpaceReqFilterChanged($(this).val());
    }).keyup(function () {
        onSpaceReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onSpaceReqFilterChanged($(this).val());
    });

    $("#txtCountFilter").change(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).keydown(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).keyup(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).bind('paste', function () {
        onReq_SelSpacesFilterChanged($(this).val());
    });

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    function onSpaceReqFilterChanged(value) {
        $scope.gridSpaceAllocation.api.setQuickFilter(value);
    }

    function onReq_SelSpacesFilterChanged(value) {
        $scope.gridSpaceAllocOptions.api.setQuickFilter(value);
    }


    /****************  Check change event ***************/
    $scope.chkedChanged = function (data) {
        if (data.ticked) {
            if ($scope.Role.Rol_id == 'N') {
                this.ticked = false;
            }
            else {
                data.setStyle(selctdChrStyle)
                data.ticked = true;
                var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, category: data.SPC_ID, subitem: data.SSA_SRNREQ_ID ? data.SSA_SRNREQ_ID : "" };
                MaploaderService.GetSpaceDetailsByREQID(dataobj).then(function (response) {
                    if (response != null) {
                        angular.forEach(response, function (value, index) {
                            $scope.selectedSpaces.push(value);
                        })
                    }
                }, function (error) {
                });
            }
        }
        else {
            switch (data.STATUS) {
                case 1: chairicon = VacantStyle;
                    break;
                case 1002: chairicon = AllocateStyle;
                    break;
                case 1004: chairicon = OccupiedStyle;
                    break;
                case 1003: chairicon = AllocateStyle;
                    break;
                case 1052: chairicon = BlockStyle;
                    break;
                default: chairicon = VacantStyle;
                    break;
            }
            _.remove($scope.selectedSpaces, _.find($scope.selectedSpaces, { SPC_ID: data.SPC_ID }));

            data.setStyle(chairicon)
            data.ticked = false;
        }
    }

    /****************  Filters ***************/

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });

    $scope.CnyChangeAll = function () {
        $scope.SearchSpace.Country = $scope.Country;
        $scope.CnyChanged();
    }
    $scope.CnySelectNone = function () {
        $scope.SearchSpace.Country = [];
        $scope.CnyChanged();
    }
    $scope.CnyChanged = function () {
        if ($scope.SearchSpace.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.SearchSpace.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.SearchSpace.City = $scope.City;
        $scope.CtyChanged();
    }
    $scope.CtySelectNone = function () {
        $scope.SearchSpace.City = [];
        $scope.CtyChanged();
    }
    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.SearchSpace.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.SearchSpace.Location = $scope.Location;
        $scope.LcmChanged();
    }
    $scope.LcmSelectNone = function () {
        $scope.SearchSpace.Location = [];
        $scope.LcmChanged();
    }
    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.SearchSpace.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });
    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.SearchSpace.Tower = $scope.Tower;
        $scope.TwrChanged();
    }
    $scope.TwrSelectNone = function () {
        $scope.SearchSpace.Tower = [];
        $scope.TwrChanged();
    }
    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.SearchSpace.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SearchSpace.Location.push(lcm);
            }
        });
    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.SearchSpace.Floor = $scope.Floor;
        $scope.FlrChanged();
    }
    $scope.FlrSelectNone = function () {
        $scope.SearchSpace.Floor = [];
        $scope.FlrChanged();
    }
    $scope.FlrChanged = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SearchSpace.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SearchSpace.Tower.push(twr);
            }
        });
        $scope.loadThemes();
    }
    $scope.showStatusPanel = true;

    $scope.loadThemes = function () {

        $scope.detailsdisplay = false;
        $scope.nrmaldisplay = true;
        $scope.showStatusPanel = true;
        progress(0, 'Loading...', true);
        $scope.SubItems = [];
        $scope.ddlSubItem = "";
        var item = {};
        $scope.ddlselval = _.find($scope.Items, { CODE: $scope.ddlItem });

        if ($scope.ddlItem) {
            if ($scope.ddlselval != undefined) {
                if ($scope.ddlselval.CHKDDL == 1) {
                    var dataobj = { flr_code: GetParameterValues('flr_code'), Item: $scope.ddlItem, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
                    MaploaderService.getallFilterbySubItem(dataobj).then(function (response) {
                        $scope.ddldisplay = true;
                        $scope.SubItems = response;

                        if ($scope.ddlItem == 'spacesubtype') {
                            setTimeout(function () { $scope.GetLegendCount() }, 200);
                        }

                        progress(0, '', false);
                    }, function (error) {
                        console.log(error);
                    });
                    progress(0, '', false);
                }
                else {
                    $scope.ddldisplay = false;
                    setTimeout(function () { $scope.GetLegendCount() }, 200);
                }
            }
            else {
                $scope.ddldisplay = false;
                setTimeout(function () { $scope.GetLegendCount() }, 200);
            }
        }
        map.scrollWheelZoom.enable();
    }

    $scope.loadSubDetails = function () {
        $scope.showStatusPanel = true;
        progress(0, 'Loading...', true);
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: $scope.ddlItem, subitem: $scope.ddlSubItem, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.GetSpaceDetailsBySUBITEM(dataobj).then(function (response) {
            if (response.Table != null) {
                if ($scope.ddlItem == "login" || $scope.ddlItem == "liveattn") {
                    $.each($scope.drawnItems._layers, function (Key, layer) {
                        var spcid = _.find(response.Table, { SPC_ID: layer.options.spaceid });
                        if (spcid == undefined)
                            layer.setStyle({ fillColor: "#E8E8E8" });
                    });

                    angular.forEach($scope.Markers, function (marker, key) {

                        var spcid = _.find(response.Table, { SPC_ID: marker.SPC_ID });
                        if (spcid != null) {
                            marker.ticked = false;
                            switch (spcid.STATUS) {
                                case 1: marker.setStyle(VacantStyle);
                                    break;
                                case 1002: marker.setStyle(AllocateStyle);
                                    break;
                                case 1004: marker.setStyle(OccupiedStyle);
                                    break;
                                case 1003: marker.setStyle(AllocateStyle);
                                    break;
                                case 1052: marker.setStyle(BlockStyle);
                                    break;
                                default: marker.setStyle(VacantStyle);
                                    break;
                            }
                            marker.bindLabel(spcid.SPC_DESC, {
                                noHide: false,
                                direction: 'auto',
                                zIndexOffset: 2000
                            });//.setOpacity(1);
                        }
                    });
                    $scope.showStatusPanel = false;
                    $scope.SpcCount = response.Table1;

                }
                else {
                    var length = response.Table.length;
                    var i = 0;
                    $.each($scope.drawnItems._layers, function (Key, layer) {

                        var spcid = _.find(response.Table, { SPC_ID: layer.options.spaceid });
                        if (spcid == undefined)
                            layer.setStyle({ fillColor: "#E8E8E8" });
                        else {
                            i = i + 1;
                            if (layer.options.spacetype == 'CHA') {
                                switch (spcid.STATUS) {
                                    case 1: chairicon = VacantStyle;
                                        break;
                                    case 1002: chairicon = AllocateStyle;
                                        break;
                                    case 1004: chairicon = OccupiedStyle;
                                        break;
                                    case 1003: chairicon = AllocateStyle;
                                        break;
                                    case 1052: chairicon = BlockStyle;
                                        break;
                                    default: chairicon = VacantStyle;
                                        break;
                                }

                                layer.setStyle(chairicon);
                            }
                            else {
                                if ($scope.ddlItem == "shiftalloc") {
                                    $scope.SpcCount = response.Table1;
                                }
                                else {
                                    var spcclrobj = _.find($scope.SubItemColordet, { SPC_ID: spcid.SPC_ID });
                                    if (spcclrobj != undefined)
                                        layer.setStyle({ fillColor: spcclrobj.COLOR, opacity: 0.65 });
                                }
                            }

                        }
                        if (length == i) {
                            if ($scope.ZoomLvl >= map.getZoom()) {
                                //map.fitBounds($scope.bounds);
                                setTimeout(function () { map.setView(L.latLng(spcid.x, spcid.y), map.getZoom() + 2); }, 200);
                            }
                            else {
                                setTimeout(function () { map.setView(L.latLng(spcid.x, spcid.y), map.getZoom()); }, 200);
                            }
                        }

                    });




                    if ($scope.ddlItem == "shiftalloc") {
                        $scope.GetLegendCount();
                    }
                    else if ($scope.ddlItem == "liveattn" || $scope.ddlItem == "shift") {
                        $scope.showStatusPanel = false;
                        $scope.SpcCount = response.Table1;
                    }
                }

            }
            progress(0, '', false);
        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }

    $scope.GetLegendCount = function () {

        $scope.SpcCount = [];

        var themeobj =
        {
            Item: $scope.ddlItem,
            flr_code: GetParameterValues('flr_code'),
            subitem: $scope.ddlSubItem,
            from_date: $scope.Maploader.FROM_DATE1,
            to_date: $scope.Maploader.TO_DATE1,
            sh_code: $scope.Maploader.Search_SH_CODE
        };
        if ($scope.ddlItem == 'verticalalloc') {
            $scope.detailsdisplay = true;
            $scope.nrmaldisplay = false;
        }
        else {
            $scope.detailsdisplay = false;
            $scope.nrmaldisplay = true;
        }
        $.post(UtilityService.path + '/api/MaploaderAPI/GetLegendsCount', themeobj, function (result) {
            if ($scope.ddlItem == 'Vacant') {
                $scope.$apply(function () {
                    $scope.SpcCount = result.Table;
                });
                var vacspaces = [];
                angular.forEach($scope.Markers, function (marker, key) {
                    marker.ticked = false;
                    if (marker.STATUS == 1) {
                        marker.setStyle(VacantStyle);
                        vacspaces.push(marker);
                    }
                    progress(0, '', false);
                });
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find(vacspaces, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                    else {
                        layer.setStyle({ fillColor: "#228B22", opacity: 0.65 });
                    }
                    progress(0, '', false);
                });
                reloadMarkers();
            }

            else if ($scope.ddlItem == "entity") {
                if (result.Table) {
                    $scope.$apply(function () {
                        $scope.SpcCount = result.Table;
                        if ($scope.SpcCountArea.length == 0)
                            $scope.SpcCountArea = result.Table;
                    });
                    for (var i = 0; i < result.Table1.length; i++) {
                        $.each($scope.drawnItems._layers, function (Key, layer) {
                            if (layer.options.spaceid == result.Table1[i].SPC_ID && layer.options.spacetype != "CHA") {
                                layer.setStyle({ fillColor: result.Table1[i].COLOR, opacity: 0.65 });
                            }
                        });
                    }

                    progress(0, '', false);
                }
                reloadMarkers();
            }
            else if ($scope.ddlItem == "release") {
                $scope.$apply(function () {
                    $scope.SubItemColordet = result.Table;
                    $scope.SpcCount = result.Table1;
                });
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find($scope.SubItemColordet, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                });

                angular.forEach($scope.Markers, function (marker, key) {
                    marker.ticked = false;
                    switch (marker.STATUS) {
                        case 1: marker.setStyle(VacantStyle);
                            break;
                        case 1002: marker.setStyle(AllocateStyle);
                            break;
                        case 1004: marker.setStyle(OccupiedStyle);
                            break;
                        case 1003: marker.setStyle(AllocateStyle);
                            break;
                        case 1052: marker.setStyle(BlockStyle);
                            break;
                        default: marker.setStyle(VacantStyle);
                            break;
                    }
                });

                angular.forEach($scope.SubItemColordet, function (Value, key) {
                    var clrobj = _.find($scope.SpcCount, { CODE: Value.CODE });
                    if (clrobj != undefined)
                        $.each($scope.drawnItems._layers, function (Key, layer) {
                            if (layer.options.spaceid == Value.SPC_ID && layer.options.spacetype != "CHA") {
                                layer.setStyle({ fillColor: clrobj.COLOR, opacity: 0.65 });
                            }
                        });
                });

                progress(0, '', false);
                reloadMarkers();
            }

            else if ($scope.ddlItem == "Unused") {
                $scope.$apply(function () {
                    $scope.UnusedDetails = result.Table;
                    $scope.SpcCount = result.Table1;
                });
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find($scope.UnusedDetails, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                });

                angular.forEach($scope.Markers, function (marker, key) {
                    marker.ticked = false;
                    var clrobj = _.find($scope.UnusedDetails, { SPC_ID: marker.SPC_ID });
                    if (clrobj != undefined) {
                        marker.setStyle({ fillColor: clrobj.COLOR });
                    }
                });



                progress(0, '', false);
            }
            else if ($scope.ddlItem != "verticalalloc" && $scope.ddlItem != "costcenteralloc" && $scope.ddlItem != "shiftalloc") {

                if (result.Table) {
                    $scope.$apply(function () {
                        $scope.SpcCount = result.Table;
                        if ($scope.SpcCountArea.length == 0)
                            $scope.SpcCountArea = result.Table;
                    });

                    for (var i = 0; i < result.Table.length; i++) {
                        $.each($scope.drawnItemscheckWS._layers, function (Key, layer) {

                            $.each($scope.Markers, function (Key, value) {

                                if (layer.options[$scope.ddlItem] == result.Table[i].CODE && layer.options.spacetype != "CHA" && layer.options.spaceid == value.SPC_ID) {
                                    layer.setStyle({ fillColor: result.Table[i].COLOR, opacity: 0.65 });
                                }
                                if (layer.options[$scope.ddlItem] == result.Table[i].CODE && layer.options.spacetype == "FLR") {
                                    layer.setStyle({ fillColor: result.Table[i].COLOR, opacity: 0.65 });
                                }
                            });
                        });
                    }

                    progress(0, '', false);
                }

                reloadMarkers();
            }

            else {
                $scope.SubItemColordet = result.Table;
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find($scope.SubItemColordet, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                });

                angular.forEach($scope.Markers, function (marker, key) {
                    marker.ticked = false;
                    switch (marker.STATUS) {
                        case 1: marker.setStyle(VacantStyle);
                            break;
                        case 1002: marker.setStyle(AllocateStyle);
                            break;
                        case 1004: marker.setStyle(OccupiedStyle);
                            break;
                        case 1003: marker.setStyle(AllocateStyle);
                            break;
                        case 1052: marker.setStyle(BlockStyle);
                            break;
                        default: marker.setStyle(VacantStyle);
                            break;
                    }
                });

                angular.forEach($scope.SubItemColordet, function (Value, key) {
                    $.each($scope.drawnItems._layers, function (Key, layer) {

                        if (layer.options.spaceid == Value.SPC_ID && layer.options.spacetype == Value.SPC_LAYER && layer.options.spacetype != "CHA") {
                            layer.setStyle({ fillColor: Value.COLOR, opacity: 0.65 });
                            return;
                        }

                    });
                });

                $scope.$apply(function () {
                    $scope.ddldisplay = true;
                    $scope.SpcCount = result.Table1;
                    if ($scope.ddlItem != "shiftalloc") {
                        $scope.SubItems = result.Table1;
                    }
                });

                progress(0, '', false);
                reloadMarkers();
            }

        });

    }

    $scope.SDate = 1;

    $('input[type="radio"]').on('click', function (e) {
        if (this.value == 1) {
            for (var i = 0; i < $scope.selectedSpaces.length; i++) {
                if ($scope.selectedSpaces[i].STACHECK == 16 && ($scope.selectedSpaces[i].FROM_TIME == null || $scope.selectedSpaces[i].FROM_TIME == "")) {
                    $scope.selectedSpaces[i].FROM_DATE = moment().format('MM/DD/YYYY');
                    //$scope.selectedSpaces[i].TO_DATE = moment($scope.selectedSpaces[i].FROM_DATE).add(1, 'day').format('MM/DD/YYYY');
                    $scope.selectedSpaces[i].TO_DATE = moment().format('MM/DD/YYYY');
                }
            }
            $scope.gridSpaceAllocOptions.api.setRowData([]);
            setTimeout(function () {
                $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
            }, 10);
        }
        else if (this.value == 2) {
            for (var i = 0; i < $scope.selectedSpaces.length; i++) {
                if ($scope.selectedSpaces[i].STACHECK == 16 && ($scope.selectedSpaces[i].FROM_TIME == null || $scope.selectedSpaces[i].FROM_TIME == "")) {
                    $scope.selectedSpaces[i].FROM_DATE = moment().format('MM/DD/YYYY');
                    $scope.selectedSpaces[i].TO_DATE = moment($scope.selectedSpaces[i].FROM_DATE).add(1, 'week').format('MM/DD/YYYY');

                }
            }

            $scope.gridSpaceAllocOptions.api.setRowData([]);
            setTimeout(function () {
                $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
            }, 10);

        }
        else if (this.value == 3) {
            for (var i = 0; i < $scope.selectedSpaces.length; i++) {
                if ($scope.selectedSpaces[i].STACHECK == 16 && ($scope.selectedSpaces[i].FROM_TIME == null || $scope.selectedSpaces[i].FROM_TIME == "")) {
                    $scope.selectedSpaces[i].FROM_DATE = moment().format('MM/DD/YYYY');
                    $scope.selectedSpaces[i].TO_DATE = moment($scope.selectedSpaces[i].FROM_DATE).add(1, 'month').format('MM/DD/YYYY');
                }
            }
        }
        $scope.gridSpaceAllocOptions.api.setRowData([]);
        setTimeout(function () {
            $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
        }, 10);
    });

    $scope.FilterData = function (filterdata, type) {
        var filterMarkers = $filter('filter')($scope.Markers, { STATUS: filterdata });
    }

    $('#divSeatStatus').on('shown.bs.collapse', function () {

        progress(0, '', true);
        if ($scope.SeatSummary == undefined) {
            updateSummaryCount();
            progress(0, 'Loading...', false);
        }
        else
            progress(0, 'Loading...', false);
    });

    $('#divSeatcap').on('shown.bs.collapse', function () {
        if ($scope.seatingCapacity == undefined)
            $scope.GetSeatingCapacity();
    });

    $('#divWorkarea').on('shown.bs.collapse', function () {
        if ($scope.SpcCountArea == undefined)
            $scope.GetSeatingArea();
    });

    $('#divotherarea').on('shown.bs.collapse', function () {
        if ($scope.Totalareadet == undefined)
            $scope.GetTotalAreaDetails()
    });

    $scope.SelectAllocEmp = function (SelectedEmp) {
        $scope.LoadDet = [];
        $scope.selecteval = SelectedEmp;
        $scope.SelectedID = "";
        var dataobj = {
            flr_code: $scope.SearchSpace.Floor[0].FLR_CODE,
            Item: SelectedEmp,
            twr_code: $scope.SearchSpace.Tower[0].TWR_CODE,
            lcm_code: $scope.SearchSpace.Location[0].LCM_CODE,
            from_date: $scope.Maploader.FROM_DATE1,
            to_date: $scope.Maploader.TO_DATE1
        };
        MaploaderService.getAllocEmpDetails(dataobj).then(function (response) {
            if (response.data != null) {
                $scope.LoadDet = response.data;
                setTimeout(function () {
                    $('#Searchdata').selectpicker('refresh');
                }, 200);
            }
        });
    }

    $scope.GetTotalAreaDetails = function () {
        $scope.Totalareadet = [];
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.GetTotalAreaDetails(dataobj).then(function (result) {

            if (result.data) {
                $scope.Totalareadet = result.data;
                progress(0, '', false);
            }
        });
    }

    $scope.GetSeatingCapacity = function () {

        progress(0, 'Loading...', false);
        $scope.seatingCapacity = [];
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };

        MaploaderService.GetSeatingCapacity(dataobj).then(function (result) {
            if (result.data) {
                $scope.seatingCapacity = result.data;
                progress(0, '', false);
            }
        });
    }

    $scope.GetSeatingArea = function () {
        progress(0, 'Loading...', false);
        $scope.seatingCapacity = [];
        //var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        //var themeobj = { Item: 'spacetype', flr_code: $scope.SearchSpace.Floor[0].FLR_CODE };
        var themeobj = {
            Item: $scope.ddlItem,
            flr_code: GetParameterValues('flr_code'),
            subitem: $scope.ddlSubItem,
            from_date: $scope.Maploader.FROM_DATE1,
            to_date: $scope.Maploader.TO_DATE1,
            sh_code: $scope.Maploader.Search_SH_CODE
        }
        $.post(UtilityService.path + '/api/MaploaderAPI/GetLegendsCount', themeobj, function (result) {
            $scope.$apply(function () {
                if ($scope.SpcCountArea.length == 0)
                    $scope.SpcCountArea = result.Table;
            });
        });
    }

    $scope.ShowEmponMap = function (SelectedID) {

        //_.forEach($scope.Markers, function (value) {
        //    console.log(value.SPC_ID);
        //})

        var marker = _.find($scope.Markers, { SPC_ID: SelectedID.SPC_ID });

        var chairicon = new L.icon({ iconUrl: '/images/marker.png', iconSize: [25, 32] });

        var empMarker = L.marker(L.latLng(marker.lat, marker.lon), {
            icon: chairicon,
            draggable: false
        }).addTo(map);
        if (map.hasLayer($scope.MarkerLblLayerSpace))
            map.removeLayer($scope.MarkerLblLayerSpace);

        setTimeout(function () {
            if (empMarker) { // check
                map.removeLayer(empMarker); // remove
                if (!map.hasLayer($scope.MarkerLblLayerSpace)) {
                    map.addLayer($scope.MarkerLblLayerSpace);
                }
            }
        }, 20000);

        if ($scope.ZoomLvl >= map.getZoom()) {
            //map.fitBounds($scope.bounds);
            setTimeout(function () { map.setView(L.latLng(marker.lat, marker.lon), map.getZoom() + 2); }, 200);
        }
        setTimeout(function () { map.panTo(L.latLng(marker.lat, marker.lon)); }, 400);

        setTimeout(function () { marker.setStyle(VacantStyle) }, 500);
        setTimeout(function () { marker.setStyle(AllocateStyle) }, 1000);
        setTimeout(function () { marker.setStyle(OccupiedStyle) }, 1500);
        setTimeout(function () { marker.setStyle(VacantStyle) }, 2000);

        setTimeout(function () {
            switch (marker.STATUS) {
                case 1: marker.setStyle(VacantStyle)
                    break;
                case 1002: marker.setStyle(AllocateStyle)
                    break;
                case 1004: marker.setStyle(OccupiedStyle)
                    break;
                case 1003: marker.setStyle(AllocateStyle)
                    break;
                case 1052: marker.setStyle(BlockStyle);
                    break;
                default: marker.setStyle(VacantStyle)
                    break;
            }
        }, 3000);

        var dataobj = { lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: $scope.SelectedID.CODE, mode: 2 };
        MaploaderService.getEmpAllocSeat(dataobj).then(function (result) {
            if (result.data) {
                $scope.EmpDet = result.data[0];
                progress(0, '', false);
            }
        });

        setTimeout(function () {
            $('#Searchdata').selectpicker('refresh');
        }, 200);

    }

    $scope.set_color = function (color) {
        return { 'background-color': color };
    }

    var zoomtreshold = 0;

    map.on('zoomend', function () {

        zoomtreshold = 0;
        map.addLayer($scope.MarkerMeetingLblLayer);

        if ($scope.ZoomLvl >= 14)
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 2;
        else if ($scope.ZoomLvl >= 12)
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 3;
        else if ($scope.ZoomLvl >= 9)
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 2;
        else
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 2;

        if ((map.getZoom() > $scope.ZoomLvl) && cnt == 0 && LOADLAYERS == 1) {
            progress(0, 'Loading...', true);
            setTimeout(function () {
                $scope.LoadMapLayers(GetParameterValues('flr_code'));
            }, 2);
        }

        var prefval = _.find($scope.SysPref, { SYSP_CODE: "Seats enable by role on map" });
        if (prefval == 0 || prefval == undefined) {
            setTimeout(function () {
                if (map.getZoom() < zoomtreshold) {
                    if (map.hasLayer($scope.MarkerLblLayerSpace)) {
                        map.removeLayer($scope.MarkerLblLayerSpace);
                    }
                    if (map.hasLayer($scope.MarkerLblLayer)) {
                        map.removeLayer($scope.MarkerLblLayer);
                    }
                    if (map.hasLayer($scope.MarkerLblLayerEmpDetails)) {
                        map.removeLayer($scope.MarkerLblLayerEmpDetails);
                    }

                }

                if (map.getZoom() >= zoomtreshold) {

                    if (map.hasLayer($scope.MarkerLblLayerSpace)) {
                        //console.log("layer already added");
                    }
                    else if (map.hasLayer($scope.MarkerLblLayer)) {
                        //console.log("layer already added");
                    }
                    else if (map.hasLayer($scope.MarkerLblLayerEmpDetails)) {
                        //console.log("layer already added");
                    }
                    else {
                        map.addLayer($scope.MarkerLblLayerSpace);
                    }
                }
            }, 200);
        }


    });

    $scope.getShiftName = function (shcode) {
        var shift = _.find($scope.Shifts, { SH_CODE: shcode });
        if (shift)
            return shift.SH_NAME;
        return "";
    }

    $scope.getVLANName = function (vlcode) {
        var VLAN = _.find($scope.VLANS, { VL_Code: vlcode });
        if (VLAN)
            return VLAN.VL_NAME;
        return "";
    }

    var classHighlight = 'highlight';
    var $thumbs = $('.thumbnail').click(function (e) {
        e.preventDefault();
        $thumbs.removeClass(classHighlight);
        $(this).addClass(classHighlight);
    });

    function reloadMarkers() {

        $scope.Markerdata.clearLayers();

        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            switch (marker.STATUS) {
                case 1:
                    if (marker.options.spacetype == "CHA")
                        marker.setStyle(VacantStyle);
                    break;
                case 1002:
                    if (marker.options.spacetype == "CHA")
                        marker.setStyle(AllocateStyle);
                    break;
                case 1004:
                    if (marker.options.spacetype == "CHA")
                        marker.setStyle(OccupiedStyle);
                    break;
                case 1003:
                    if (marker.options.spacetype == "CHA")
                        marker.setStyle(AllocateStyle);
                    break;
                case 1052:
                    if (marker.options.spacetype == "CHA")
                        marker.setStyle(BlockStyle);
                    break;
                default: marker.setStyle(VacantStyle);
                    break;
            }
            if (marker.AUR_LEAVE_STATUS == 'Y') {
                switch (marker.AUR_LEAVE_STATUS) {
                    case 'Y': marker.setStyle(LeaveStyle);
                        break;
                }
            }

            var formattedString = marker.SPC_DESC.split(",").join("\n");
            var k = formattedString.split("\n");
            var id = "";
            var id1 = "";
            var text = "";
            var fulltext = "";
            var str = " ";
            fulltext = BindMarkerText(formattedString, marker);
            if ($scope.SeatPopUpDetails.SYSP_VAL1 == 1) {
                var role = _.find($scope.GetRoleAndRM);
                if (role.AUR_ROLE == 6) {
                    if (marker.SSAD_AUR_ID == UserId || marker.STATUS == 1) {

                        if (marker.options.spacetype == "CHA")
                            marker.bindLabel(fulltext.split(",").join("\n"), {
                                noHide: false,
                                direction: 'auto',
                                className: "my-label",
                                zIndexOffset: 2000
                            });//.setOpacity(1);
                    }
                }
                else {
                    if (marker.options.spacetype == "CHA")
                        marker.bindLabel(fulltext.split(",").join("\n"), {
                            noHide: false,
                            direction: 'auto',
                            className: "my-label",
                            zIndexOffset: 2000
                        });
                }

            } else {
                if (marker.options.spacetype == "CHA")
                    marker.bindLabel(fulltext.split(",").join("\n"), {
                        noHide: false,
                        direction: 'auto',
                        className: "my-label",
                        zIndexOffset: 2000
                    });
            }


            if (marker.options.spacetype == "CHA")
                $scope.Markerdata.addLayer(marker);

        });
    }


    $scope.GradeAllocStatus = function () {
        if ($scope.GradeAlloc.SYSP_VAL1 == 1) {

            MaploaderService.GradeAllocStatus().then(function (response) {
                if (response[0].Eligiblity != 'No') {
                    $scope.Grade_Allow_status = response[0].ALLOW_STATUS;
                    $scope.Allow_Remaining = response[0].REMAINING;
                    $scope.ORG_ALLOC_COUNT = response[0].ORG_ALLOC_COUNT;
                    $scope.Eligibility = response[0].Eligiblity;

                    $scope.Details = 'Your total limit is -' + $scope.ORG_ALLOC_COUNT + ',utilized -' + ($scope.ORG_ALLOC_COUNT - $scope.Allow_Remaining) + '  and can allocate more ' + $scope.Allow_Remaining + ' seats';

                    if (response != undefined) {
                        if ($scope.Grade_Allow_status == 'Not Allow') {
                            $scope.AllocateEnabledStatus = true;
                            $ngConfirm({
                                icon: 'fa fa-warning',
                                title: 'Allocation Warning',
                                content: 'You have exceeded maxmimum no of allocations.Current limit is ' + $scope.ORG_ALLOC_COUNT,
                                closeIcon: true,
                                closeIconClass: 'fa fa-close',
                                buttons: {
                                    close: function ($scope, button) {
                                    }
                                }
                            });
                        }
                        else {
                            if ($scope.Allow_Remaining >= $scope.selectedSpacesLength) {
                                $scope.AllocateEnabledStatus = false;
                            }
                            else {
                                $scope.AllocateEnabledStatus = true;
                            }
                            // $scope.AllocateEnabledStatus = false;
                        }
                    }
                    else {
                        console.log("error");
                    }
                } else {
                    $scope.AllocateEnabledStatus = false;
                }
            })

        }
    }


    $scope.Maploader.FROM_DATE = moment().format('MM/DD/YYYY');
    $scope.Maploader.TO_DATE = moment().format('MM/DD/YYYY');

    $scope.SearchSpaces = function () {
        $scope.SeatAllocForSpace = [];
        var frmDate = new Date($scope.Maploader.FROM_DATE1);
        var toDate = new Date($scope.Maploader.TO_DATE1);
        if (frmDate > toDate) {
            showNotification('error', 8, 'bottom-right', 'Please Select To Date greater than From Date For ');
            return;
        }
        $scope.selectedSpaces = [];
        //$scope.Markers = [];
        $scope.LoadMarkers($scope.flr_code);
        $scope.LoadALLMarkers($scope.flr_code);
        updateSummaryCount();
        $scope.GetLegendCount();
        $scope.SelectAllocEmp($scope.selecteval);
    }

});


function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return decodeURIComponent(urlparam[1]);
        }
    }
}



