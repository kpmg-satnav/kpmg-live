﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

<%--     
    <script defer src='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js'></script>
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css' rel='stylesheet' />--%>

    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/main.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/leaflet/Print/easyPrint.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.fullscreen.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/angular-confirm.css" rel="stylesheet" />

    <style>
        #main {
    width: 135% !important;
    height: 500px !important;
   
                }
        .countVal {
            font-size: 13px;
        }

        .highlight {
            background-color: #d9edf7;
        }

        ul {
            cursor: pointer;
        }

        .ag-header {
            background-color: black;
        }

        @media print {
            html, body {
                height: 99%;
            }

            a[href]:after {
                content: none;
            }
        }

        @page {
            size: auto; /* auto is the initial value */
            /*margin: 0mm;*/ /* this affects the margin in the printer settings */
            margin: 0px,0px,0px,10px;
            size: landscape;
        }

        .text-labels {
            font-size: 10px;
            font-weight: 100;
            width: 250px !important;
            /* Use color, background, set margins for offset, etc */
        }

         .text-labels2 {
            font-size: 2px;
            font-weight: 400;
            width: 32px !important;
            /* Use color, background, set margins for offset, etc */
        }

        body {
            margin: 0px;
        }
    </style>
</head>
<body class="amantra" ng-controller="MaploaderController">
    <div class="animsition">
        <div class="widgets">
            <div ba-panel ba-panel-title="Asset Type Master" ba-panel-class="with-scroll">
                <div class="panel">
                    <div class="panel-heading" style="height: 41px;">
                        <h3 class="panel-title">Map Layout -
                        <label ng-bind="LAYOUTNAME"></label>
                        </h3>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <div class="row">
                            <div class="col-md-9">
                                <div id="main">
                                    <div id="leafletMap"></div>
                                    <!--<leaflet defaults="leafletDefaults" center="leafletCenter" bounds="leafletBounds" markers="leafletMarkers" controls="leafletDraw" id="leafletMap"></leaflet>-->

                                </div>
                            </div>
                            <div class="col-md-3" id="divFilter" style="display: none;">

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#divFilter" href="#divAccFilter">Filter</a>
                                        </h4>
                                    </div>
                                    <div id="divAccFilter" class="panel-collapse collapse in">

                                        <div id="Div2" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <form id="Form1" name="frmSearchspc" data-valid-submit="FilterSpaces()" novalidate>
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12 col-xs-12" style="display: none">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CNY_NAME.$invalid}">
                                                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="Country" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                                                    data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Country[0].CNY_NAME" name="CNY_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CNY_NAME.$invalid">Please select country </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12" style="display: none">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid}">
                                                                <label class="control-label">City <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="City" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                                                    data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.City[0].CTY_NAME" name="CTY_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid">Please select city </span>

                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.LCM_NAME.$invalid}">
                                                                <label class="control-label">Location <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="Location" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                                    data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Location[0].LCM_NAME" name="LCM_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.LCM_NAME.$invalid">Please select location </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid}">
                                                                <label class="control-label">Tower <span style="color: red;">**</span></label>
                                                                <div isteven-multi-select data-input-model="Tower" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                                                    data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Tower[0].TWR_NAME" name="TWR_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid">Please select tower </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid}">
                                                                <label class="control-label">Floor <span style="color: red;">**</span></label>
                                                                <div isteven-multi-select data-input-model="Floor" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                                    data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Floor[0].FLR_NAME" name="FLR_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid">Please select floor </span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="">
                                                        <input type="submit" class="btn btn-primary" value="Apply Filter" />
                                                        <%--<button type="button" class="btn btn-default" onclick="$('.slide_content').slideToggle();">Close</button>--%>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-3" id="divSearch" style="display: none;">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#divSearch" href="#divSearchdata">Search Employee</a>
                                        </h4>
                                    </div>
                                    <div id="divSearchdata" class="panel-collapse collapse in">
                                        <div id="Div4" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group row">
                                                            <label class="control-label  col-md-3">Search By<span style="color: red;">*</span></label>
                                                            <div class="col-md-9">
                                                                <select name="SelectedType" data-ng-model="SelectedType" class="form-control" id="ddlvert" required="" data-ng-change="SelectAllocEmp(SelectedType)">
                                                                    <option value="" selected>--Select--</option>
                                                                    <option ng-repeat="emp in EmpSearchItems" value="{{emp.CODE}}">{{emp.NAME}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group row">
                                                            <label class="control-label col-md-3">Value<span style="color: red;">*</span></label>
                                                            <div class="col-md-9">
                                                                <select name="mySelect" id="Searchdata" ng-options="det.NAME for det in LoadDet track by det.CODE"
                                                                    ng-model="SelectedID" class="form-control selectpicker" data-live-search="true"
                                                                    ng-change="ShowEmponMap(SelectedID)" required="">
                                                                </select>
                                                                <%--<select name="SelectedID" data-ng-model="SelectedID" class="form-control selectpicker" data-live-search="true"  id="Searchdata" required=""
                                                                    ng-change="ShowEmponMap(SelectedID)">
                                                                    <option value="" selected>--Select--</option>
                                                                    <option ng-repeat="det in LoadDet" value="{{det.CODE}}">{{det.NAME}}</option>
                                                                </select>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" ng-show="EmpDet">
                                                    <div class="panel-heading">Employee Details</div>
                                                    <div class="panel-body row">
                                                        <label class="control-label col-md-4 text-right">Name:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{EmpDet.AUR_NAME}} / {{EmpDet.AUR_ID}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">Space:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{EmpDet.SPC_NAME}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">{{BsmDet.Parent}}:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{EmpDet.VER_NAME}}/{{EmpDet.VERTICAL}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">{{BsmDet.Child}}:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{EmpDet.Cost_Center_Name}}/{{EmpDet.Cost_Center_Code}}</label>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <label class="control-label col-md-4 text-right">Shift:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{getShiftName(EmpDet.SH_CODE)}}</label>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" id="accordion">
                              <%--  <div class="panel panel-primary panel-map" id="DivSeatStatusPanel" ng-show="showStatusPanel==true">

                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#divSeatStatus">Seat Status</a>
                                        </h4>
                                    </div>
                                    <div id="divSeatStatus" class="panel-collapse collapse ">

                                        <ul class="list-group" id="ulSeatStatus">
                                            <li class="list-group-item thumbnail" data-ng-click="ShowTotalOnMap()">Total Seats<span class="label label-primary pull-right countVal" ng-bind="SeatSummary.TOTAL" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item thumbnail" data-ng-click="ShowVacantOnMap()">Available / Vacant<span class="label label-success pull-right countVal" ng-bind="SeatSummary.VACANT" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item thumbnail" data-ng-click="ShowCostCentre()">{{SeatSummary.ALLOCUNUSED}}
                                                    <span class="label label-info pull-right countVal" ng-bind="SeatSummary.ALLOCATEDVER + SeatSummary.ALLOCATEDCST" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item thumbnail" data-ng-click="ShowOccupiedOnMap()">{{SeatSummary.OCCUP_EMP}}
                                                    <span class="label label-danger pull-right countVal" ng-bind="SeatSummary.OCCUPIED" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item">Overloaded Seats<span class="label label-default pull-right countVal" ng-bind="SeatSummary.OVERLOAD" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item">Inactive/Long Leave Seats<span class="label longleave pull-right countVal" ng-bind="SeatSummary.LEAVE" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item">Blocked Seats<span class="label block pull-right countVal" ng-bind="SeatSummary.BLOCKED" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item">Selected Seats<span class="label selectedseat pull-right countVal">{{selectedSpaces.length}}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>--%>
                                <div class="panel panel-primary panel-map" style="display:none;">
                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Themes</a>

                                        </h4>
                                    </div>

                                    <div id="collapseOne" class="panel-collapse collapse in">

                                        <div id="ThemeAccordian" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="clearfix">
                                                    <label class="control-label row col-md-2">Items: </label>
                                                    <select class="selectpicker-xs col-md-10 pull-right" name="ddlItem" ng-init="ddlItem = 'spacetype'" ng-model="ddlItem" ng-change="loadThemes()">
                                                        <option value="" ng-selected="">--Select--</option>
                                                        <option ng-repeat="item in Items" value="{{item.CODE}}" title="{{item.NAME}}">{{item.NAME}}</option>
                                                    </select>
                                                </div>
                                                <div data-ng-show="ddldisplay">
                                                    <label class="control-label row col-md-2">Sub Items: </label>
                                                    <select class="selectpicker-xs col-md-10 pull-right" name="ddlSubItem" ng-model="ddlSubItem" ng-change="loadSubDetails()">
                                                        <option value="" ng-selected="">--Select--</option>
                                                        <option ng-repeat="item in SubItems" value="{{item.CODE}}" ng-selected="{{item.ticked}}">{{item.NAME}}</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="box-body">
                                                <ul class="list-group" id="ulThemes">
                                                    <%--list-group-item-success--%>
                                                    <li class="list-group-item" ng-class="{'list-group-item-info' : ddlSubItem == item.CODE}" ng-repeat="item in SpcCount">{{item.NAME}}
                                                        <span class="label pull-right countVal" ng-style="set_color(item.COLOR)" ng-bind="item.CNT" ng-init="0"></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

<%--                                <div class="panel panel-primary panel-map">
                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#divSeatcap">Seating Capacity Details</a>

                                        </h4>
                                    </div>

                                    <div id="divSeatcap" class="panel-collapse collapse">
                                        <ul class="list-group" id="ulCapacity">
                                            <li class="list-group-item" ng-repeat="item in seatingCapacity">{{item.SPC_NAME}}
                                      <span class="label pull-right countVal" ng-style="set_color(item.COLOR)" data-ng-bind="item.CAPACITY" ng-init="0"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel panel-primary panel-map">
                                    <div class="panel-heading">

                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#divWorkarea">Work Area Details in sq. ft.</a>
                                        </h4>
                                    </div>

                                    <div id="divWorkarea" class="panel-collapse collapse">
                                        <ul class="list-group" id="ulArea">
                                            <li class="list-group-item" ng-repeat="item in SpcCountArea">{{item.NAME}}
                                <span class="label pull-right countVal" ng-style="set_color(item.COLOR)" ng-bind="item.SUM" ng-init="0"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel panel-primary panel-map">
                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#divotherarea">Other Area Details in sq. ft.</a>
                                        </h4>
                                    </div>
                                    <div id="divotherarea" class="panel-collapse collapse">
                                        <ul class="list-group" id="ulC">
                                            <li class="list-group-item" ng-repeat="item in Totalareadet">{{item.AREA}}
                                <span class="label pull-right label-primary countVal" ng-bind="item.SFT" ng-init="0"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="slide_block">
        <div class=" slide_head clearfix">
            <div class="pull-left Icons_right" ng-show="FilterVar == 'spcList'">
                <input id="filtertxt" placeholder="Filter..." type="text" class="form-control input-xs" />
            </div>

            <div class="pull-right Icons_right">
                <%--<div class="input-group">
                            <input type="text" placeholder="Search" />
                            <span class="input-group-addon">
                                <span class="fa fa-search" onclick="setup('fromdate')"></span>
                            </span>
                        </div>--%>
            </div>
        </div>
        <div class="slide_content">
            <div class="col-md-12" ng-show="FilterVar == 'spcList'">
                <div class="form-group">
                    <div style="height: 230px;">
                        <div data-ag-grid="gridOptions" style="height: 100%;" class="ag-blue"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="SeatAllocation" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Seat Allocation</h4>
                </div>
                <form id="Form2" name="frmSpaceAlloc">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div style="height: 180px" ng-show="selectedSpaces.length != 0">
                                    <input class="form-control" id="txtCountFilter" placeholder="Filter..." type="text" style="width: 25%" />
                                    <div data-ag-grid="gridSpaceAllocOptions" style="height: 90%;" class="ag-blue"></div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer" ng-show="EmpAllocSeats.length> 0">
                            <div class="panel-heading">
                                <h4 class="modal-title" id="H2">Seat(s) allocated to {{SelectedEmp.AUR_NAME}}</h4>
                            </div>
                            <div class="panel-body">
                                <div class="clearfix">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <table id="tblempalloc" width="100%" class="table table-condensed table-bordered table-hover table-striped">
                                            <tr>
                                                <th>Space ID</th>
                                                <th>Location</th>
                                                <th>Tower</th>
                                                <th>Floor</th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Shift</th>
                                                <th></th>
                                            </tr>
                                            <tr ng-repeat="emp in EmpAllocSeats">
                                                <td>{{emp.SPC_NAME}}</td>
                                                <td>{{emp.LOCATION}}</td>
                                                <td>{{emp.TOWER}}</td>
                                                <td>{{emp.FLOOR}}</td>
                                                <td>{{emp.FROM_DATE}}</td>
                                                <td>{{emp.TO_DATE}}</td>
                                                <td>{{emp.SH_CODE}}</td>
                                                <td>
                                                    <input type="button" id="btnRelease" value="Release" ng-click="ReleaseEmp(emp)" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="row"></div>
                                    <div class="box-footer text-right">
                                        <button type="button" class="btn btn-primary" data-ng-hide="ProallocEnabledStatus" ng-click="Proceedalloc(alloc)"<%-- ng-click="EmpAllocSeats = []"--%>>Proceed to allocate additional seat</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-right" ng-show="EmpAllocSeats.length <= 0 && GetRoleAndRM[0].AUR_ROLE!=6">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                            
                            <input type="button" ng-click="AllocateSeats()" class="btn btn-primary" value="Allocate Seats" ng-disabled="OnEmpAllocSeats" />
                            <div class="btn-group dropup">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Release From {{relfrom.Name}}  <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a ng-click="Release(1004,'Employee')">Employee</a></li>
                                    <li><a ng-click="Release(1003,BsmDet.Child)">{{BsmDet.Child}}</a></li>
                                    <li><a ng-click="Release(1002,BsmDet.Parent)">{{BsmDet.Parent}}</a></li>
                                    <li><a ng-click="Release(1052,'Blocked')">Blocked</a></li>
                                    <%--<li role="separator" class="divider"></li>
                                            <li><a href="#" ng-click="relfrom = 'Complete Release'; relfrom = 1001">Complete Release</a></li>--%>
                                </ul>
                            </div>
                        </div>
                        <div class="box-footer text-right" ng-show="EmpAllocSeats.length <= 0 && GetRoleAndRM[0].AUR_ROLE==6">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input type="button" ng-click="AllocateSeats()" class="btn btn-primary" value="Allocate Seats" />
                            <div class="btn-group dropup">
                                <button type="button" class="btn btn-primary" ng-click="Release(1002,BsmDet.Parent)">
                                    Release
                                </button>
                                
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="Active" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H3">Active/Inactive Spaces</h4>
                </div>
                <form id="Form3" name="frmSpaceAlloc">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div style="height: 200px">
                                    <input class="form-control" id="InacFilter" placeholder="Filter..." type="text" style="width: 25%" />
                                    <div data-ag-grid="gridSpaceAllocation" style="height: 80%;" class="ag-blue"></div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-right" ng-show="EmpAllocSeats.length <= 0">
                            <input type="button" class="btn btn-danger" value="Inactive Spaces" data-ng-click="InactiveSpaces()" ng-show="selectedSpaces.length > 0" />
                            <input type="button" class="btn btn-primary" value="Activate Space" data-ng-click="ActivateSpaces()" ng-show="selectedSpaces.length==0" />
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/Leaflet.fullscreen.min.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../BootStrapCSS/leaflet/addons/easy-button.js"></script>
    <script defer src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/Print/leaflet.easyPrint.js"></script>
    <script defer src="../../Scripts/angular-block-ui.min.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/angular-confirm.js"></script>
    <script defer src="../../Scripts/jQuery.print.js"></script>
    <script defer src="../../BlurScripts/BlurJs/DashboardPrint.js"></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI", "cp.ngConfirm"]);
    </script>
    <script defer src="../Utility.js"></script>

    <%--<script defer src="../../BootStrapCSS/Scripts/AllocateSeatToEmployeeVertical.js"></script>--%>
    <script defer src="Kotakmaploader.js"></script>
    <script defer src="../DeptAlloc/Js/SpaceRequisition.js"></script>
    <script defer>
        function slideTogglefn() {
            $('#slide_trigger').click(function () {
                $(this).toggleClass("glyphicon-chevron-down");
                $('.slide_content').slideToggle();
            });
        }
        $(document).ready(function () {
            $('#toggleSlide').click(function () {
                $('#slide_trigger').click();
            });
            slideTogglefn();
        });
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>     
    <script defer src="../../Scripts/moment.min.js"></script>
</body>
</html>

