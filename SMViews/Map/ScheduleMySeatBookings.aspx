﻿<%@ Page Language="C#" AutoEventWireup="true" %>



<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css" />--%>
    <link href="../../assets/js/cdnjstimepicker1.10.0.css" rel="stylesheet" />


    <style>
        .grid-align {
            text-align: center;
        }

        .list-group-item {
            position: relative;
            display: block;
            background-color: #fff;
            border: 1px solid #ddd;
        }

        .column {
            float: left;
            width: 50%;
            padding: 6px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        
    </style>

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                setDate: new Date(),
                startDate: new Date(),
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: '+15d'
            });
        };

    </script>
</head>


<body data-ng-controller="ScheduleMySeatBookingsController" class="amantra">
    <div class="animsition">
        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading" style="height: 41px; background-color: #0e2f5d">
                        <h3 class="panel-title panel-heading-qfms-title" style="color: #fff">Search Availability</h3>
                    </div>

                    <div class="panel-body" style="padding-right: 10px; background-color: #fff">
                        <form role="form" id="Form1" name="frmScheduleMySeatBookings" data-valid-submit="SearchSpaces()" novalidate>

                            <div class="row">
                                <div class="clearfix col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmScheduleMySeatBookings.$submitted && frmScheduleMySeatBookings.LCM_NAME.$invalid}">
                                            <label class="control-label">Location</label>
                                            <div isteven-multi-select data-input-model="Locations" data-output-model="ScheduleMySeatBookings.Locations" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="ScheduleMySeatBookings.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmScheduleMySeatBookings.$submitted && frmScheduleMySeatBookings.LCM_NAME.$invalid" style="color: red">Please select Location</span>

                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Tower</label>
                                            <div isteven-multi-select data-input-model="Towers" data-output-model="ScheduleMySeatBookings.Towers" data-button-label="icon TWR_NAME "
                                                data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="ScheduleMySeatBookings.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmScheduleMySeatBookings.$submitted && frmScheduleMySeatBookings.TWR_NAME.$invalid" style="color: red">Please select Tower</span>



                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Floor</label>
                                            <div isteven-multi-select data-input-model="Floors" data-output-model="ScheduleMySeatBookings.Floors" data-button-label="icon FLR_NAME"
                                                data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="ScheduleMySeatBookings.Floors[0]" name="FLR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmScheduleMySeatBookings.$submitted && frmScheduleMySeatBookings.FLR_NAME.$invalid" style="color: red">Please select Floor</span>



                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="clearfix col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>From Date</label>
                                            <div class="input-group date" id='fromdate'>
                                                <input type="text" class="form-control" data-ng-model="ScheduleMySeatBookings.FROM_DATE" id="Text1" name="FromDate"
                                                    placeholder="mm/dd/yyyy" />

                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                                </span>

                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>To Date</label>
                                            <div class="input-group date" id='todate'>
                                                <input type="text" class="form-control" data-ng-model="ScheduleMySeatBookings.TO_DATE" id="Text2" name="ToDate"
                                                    placeholder="mm/dd/yyyy" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                                </span>
                                            </div>
                                            <%-- <input type="text" data-ng-model="ScheduleMySeatBookings.todate[0]" name="TO_DATE" style="display: none" required="" />
                                              <span class="error" data-ng-show="frmScheduleMySeatBookings.$submitted && frmScheduleMySeatBookings.TO_DATE.$invalid" style="color: red">Please select To date</span>--%>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class=" row clearfix">
                                <div class=" col-md-9 col-sm-12 col-xs-12 box-footer text-right">
                                    <button type="submit" id="btnsubmit"  style="margin-right: 15px" class="btn btn-primary custom-button-color">Search Spaces</button>
                                    <input type="button" id="btnNew" data-ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </form>

                        <form id="Form2" name="ScheduleMySeatBookings" style="background-color: #fff" data-valid-submit="SearchSpaces1()" data-ng-show="Form2Enable">



                            <div class="form-group">
                                <div class="panel-body" style="padding-right: 10px;" data-ng-show="IsMobile" style="padding: inherit">

                                    <div>
                                        <h3 style="font-size: 15px" class="panel-title panel-heading-qfms-title">Available Seats and Schedule</h3>
                                    </div>


                                    <div class="row" style="margin-top: 10px">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': ScheduleMySeatBookings.$submitted && ScheduleMySeatBookings.SPC_ID.$invalid}">
                                                <label class="control-label">Select Space ID's</label>
                                                <div isteven-multi-select data-input-model="SpaceIds" data-output-model="ScheduleMySeatBookings.SpaceId" data-button-label="icon SPC_NAME"
                                                    data-item-label="icon SPC_NAME maker" data-on-item-click="SpaceIdSelected()" data-selection-mode="single"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': ScheduleMySeatBookings.$submitted && ScheduleMySeatBookings.AUR_NAME.$invalid}">
                                                <label class="control-label">Search Employee</label>
                                                <div isteven-multi-select data-input-model="EmployeeDetails" data-output-model="ScheduleMySeatBookings.EmployeeDetails" data-button-label="icon AUR_NAME"
                                                    data-item-label="icon AUR_NAME maker" data-on-item-click="EmployeeIdSelected()" data-selection-mode="single"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': ScheduleMySeatBookings.$submitted && ScheduleMySeatBookings.SH_NAME.$invalid}">
                                                <label class="control-label">Shift <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Shifts" data-output-model="ScheduleMySeatBookings.Shift" data-button-label="icon SH_NAME"
                                                    data-item-label="icon SH_NAME maker"
                                                    data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': ScheduleMySeatBookings.$submitted && ScheduleMySeatBookings.VL_Name.$invalid}">
                                                <label class="control-label">VLAN <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="VLANData" data-output-model="ScheduleMySeatBookings.VLAN" data-button-label="icon VL_Name"
                                                    data-item-label="icon VL_Name maker"
                                                    data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding: inherit" data-ng-show="SelectionType">

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <label class="control-label">
                                                    Daily
                                                <input type="radio" id="setDaily" value="Daily" class="clsRadioButton" name="SchType" data-ng-model="value" data-ng-click="CheckType(value)" />
                                                </label>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <label class="control-label">
                                                    Select Days
                                                <input type="radio" id="setAlterDays" value="AlterDays" class="clsRadioButton" name="SchType" data-ng-model="value" data-ng-click="CheckType(value)" />
                                                </label>
                                            </div>

                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <label class="control-label">
                                                    Weekly
                                                <input type="radio" id="setWeekly" value="Weekly" class="clsRadioButton" name="SchType" data-ng-model="value" data-ng-click="CheckType(value)" />
                                                </label>
                                            </div>



                                            <%--<label class="col-md-2 control-label">
                                                    Monthly
                                                <input type="radio" id="setMonthly" value="Monthly" class="clsRadioButton" name="SchType" data-ng-model="value" data-ng-click="CheckType(value)" />
                                                </label>--%>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12" id="AlterDays" style="margin-top: 8px" data-ng-show="SelectAlterDays">
                                            <div class="form-group">
                                                <label class="control-label">Select Days</label>
                                                <div isteven-multi-select data-input-model="AlterDays" data-output-model="ScheduleMySeatBookings.AlterDays" data-button-label="icon Date"
                                                    data-item-label="icon Date maker"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="ScheduleMySeatBookings.WeekDates[0]" name="Date" style="display: none" />
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12" id="Weekly" style="margin-top: 8px" data-ng-show="SelectWeekly">
                                            <div class="form-group">
                                                <label class="control-label">Select Weekly</label>
                                                <div isteven-multi-select data-input-model="Weekdays" data-output-model="ScheduleMySeatBookings.WeeklyDays" data-button-label="icon Name"
                                                    data-item-label="icon Name maker"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="ScheduleMySeatBookings.WeekDates[0]" name="Date" style="display: none" />
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 box-footer" style="margin-top: 4px">
                                            <input type="button" data-ng-click="Check()" value="ADD" class="btn btn-primary custom-button-color" data-ng-show="Checing" />
                                        </div>
                                    </div>


                                    <div class="row" data-ng-show="Griddata">

                                        <div class="col-md-12 col-sm-12 col-xs-12"  style="margin-top: 8px">

                                            <ul class="list-group" id="ulThemes">
                                                <li class="list-group-item" data-ng-repeat="item in FinalData| filter :{'ticked' : true}">
                                                    <div class="row">
                                                        <div class="column">
                                                            <label class="form-label" style="color: #0e2f5d; font-size: 12px">From Date :</label>
                                                             <br />
                                                            <label class="form-label" style="color: rgb(0,0,0); font-size: 10px" data-ng-bind="item.FROM_DATE"></label>
                                                        </div>


                                                        <div class="column">

                                                            <label class="form-label" style="color: #0e2f5d; font-size: 12px">To Date :</label>
                                                           <br />
                                                            <label class="form-label" style="color: rgb(0,0,0); font-size: 10px" data-ng-bind="item.TO_DATE"></label>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <button type="button" data-ng-click="DeleteFromGrid(item)" class="btn-primary" style="margin-left: 10px;  margin-right: 10px; height: 30px; width: 180px; font-size: 12px; ">DELETE</button>
                                                    </div>

                                                </li>
                                            </ul>


                                        </div>

                                        <div class="box-footer">
                                            <button type="button" data-ng-click="scheduleSeats()" id="btnAllocateSeats" class="btn btn-primary" style="width: 100px" data-ng-show="Griddata">Schedule Seats</button>
                                        </div>
                                    </div>
                                </div>


                                <div class="panel-body" style="padding-right: 10px;" data-ng-show="IsWeb">


                                    <div class="panel-heading">
                                        <h3 class="panel-title panel-heading-qfms-title">Available Seats and Schedule</h3>
                                    </div>

                                    <div style="margin-top: 15px" class="row clearfix col-md-12 col-sm-6 col-xs-12">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': ScheduleMySeatBookings.$submitted && ScheduleMySeatBookings.SPC_ID.$invalid}">
                                                <label class="control-label">Select Space ID's</label>
                                                <div isteven-multi-select data-input-model="SpaceIds" data-output-model="ScheduleMySeatBookings.SpaceId" data-button-label="icon SPC_NAME"
                                                    data-item-label="icon SPC_NAME maker" data-on-item-click="SpaceIdSelected()" data-selection-mode="single"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': ScheduleMySeatBookings.$submitted && ScheduleMySeatBookings.AUR_NAME.$invalid}">
                                                <label class="control-label">Search Employee</label>
                                                <div isteven-multi-select data-input-model="EmployeeDetails" data-output-model="ScheduleMySeatBookings.EmployeeDetails" data-button-label="icon AUR_NAME"
                                                    data-item-label="icon AUR_NAME maker" data-on-item-click="EmployeeIdSelected()" data-selection-mode="single"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': ScheduleMySeatBookings.$submitted && ScheduleMySeatBookings.SH_NAME.$invalid}">
                                                <label class="control-label">Shift <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Shifts" data-output-model="ScheduleMySeatBookings.Shift" data-button-label="icon SH_NAME"
                                                    data-item-label="icon SH_NAME maker"
                                                    data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': ScheduleMySeatBookings.$submitted && ScheduleMySeatBookings.VL_Name.$invalid}">
                                                <label class="control-label">VLAN <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="VLANData" data-output-model="ScheduleMySeatBookings.VLAN" data-button-label="icon VL_Name"
                                                    data-item-label="icon VL_Name maker"
                                                    data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-md-6 col-sm-6 col-xs-12" data-ng-show="SelectionType">
                                            <label class="col-md-3 col-sm-6 col-xs-12 control-label">
                                                Daily
                                                <input type="radio" id="setDaily" value="Daily" class="clsRadioButton" name="SchType" data-ng-model="value" data-ng-click="CheckType(value)" />
                                            </label>
                                            <label class="col-md-3  col-sm-6  col-xs-12 control-label">
                                                Select Days
                                                <input type="radio" id="setAlterDays" value="AlterDays" class="clsRadioButton" name="SchType" data-ng-model="value" data-ng-click="CheckType(value)" />
                                            </label>
                                            <label class="col-md-3 col-sm-6 col-xs-12 control-label">
                                                Weekly
                                                <input type="radio" id="setWeekly" value="Weekly" class="clsRadioButton" name="SchType" data-ng-model="value" data-ng-click="CheckType(value)" />
                                            </label>
                                            <%--<label class="col-md-2 control-label">
                                                    Monthly
                                                <input type="radio" id="setMonthly" value="Monthly" class="clsRadioButton" name="SchType" data-ng-model="value" data-ng-click="CheckType(value)" />
                                                </label>--%>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12" id="AlterDays" data-ng-show="SelectAlterDays">
                                            <div class="form-group">
                                                <label class="control-label">Select Days</label>
                                                <div isteven-multi-select data-input-model="AlterDays" data-output-model="ScheduleMySeatBookings.AlterDays" data-button-label="icon Date"
                                                    data-item-label="icon Date maker"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="ScheduleMySeatBookings.WeekDates[0]" name="Date" style="display: none" />
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12" id="Weekly" data-ng-show="SelectWeekly">
                                            <div class="form-group">
                                                <label class="control-label">Select Weekly</label>
                                                <div isteven-multi-select data-input-model="Weekdays" data-output-model="ScheduleMySeatBookings.WeeklyDays" data-button-label="icon Name"
                                                    data-item-label="icon Name maker"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="ScheduleMySeatBookings.WeekDates[0]" name="Date" style="display: none" />
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12 box-footer">
                                            <input type="button" data-ng-click="Check()" value="ADD" class="btn btn-primary custom-button-color" data-ng-show="Checing" />
                                        </div>
                                    </div>


                                    <div class=" row clearfix" data-ng-show="Griddata">

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div style="height: 325px;" data-ng-show="Checing">
                                                <div data-ag-grid="gridOptions" style="height: 90%; width: auto" class="ag-blue"></div>
                                            </div>
                                        </div>

                                        <div class="box-footer">
                                            <button type="button" data-ng-click="scheduleSeats()" id="btnAllocateSeats" class="btn btn-primary" style="width: 100px" data-ng-show="Griddata">Schedule Seats</button>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../Scripts/moment.min.js"></script>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>--%>
    <script src="../../assets/js/Jquerytimepicker1.10.0.js"></script>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>--%>
    <script defer src="../Utility.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script defer> 
        var aur_id = '<%= Session["UID"] %>';
    </script>
    <script defer src="ScheduleMySeatBookings.js"></script>
    <script src="../DeptAlloc/Js/SpaceRequisition.js"></script>
</body>
</html>


