﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/main.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/leaflet/Print/easyPrint.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/leaflet/leaflet.fullscreen.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/angular-confirm.css" rel="stylesheet" />
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css">--%>
    <link href="../../assets/js/cdnjsselect4.0.10.css" rel="stylesheet" />
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css" />--%>
    <link href="../../assets/js/cdnjstimepicker1.10.0.css" rel="stylesheet" />
    <style>
        .fa-user {
            color: darkblue;
        }

        .tabledata {
            font-weight: bold;
            font-size: 12px;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 35px;
            height: 20px;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 12px;
            width: 12px;
            left: 4.5px;
            bottom: 4.5px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #1C2B36;
            -webkit-transition: .4s;
            transition: .4s;
        }


        input:checked + .slider {
            background-color: #63bbb2;
        }

            input:checked + .slider:before {
                -webkit-transform: translateX(12px);
                -ms-transform: translateX(12px);
                transform: translateX(12px);
            }

        .slider.round:before {
            border-radius: 45%;
        }
        /* Rounded sliders */
        .slider.round {
            border-radius: 15px;
        }
    </style>

    <style>
        .easy-button-button.disabled {
            display: none;
        }

        .countVal {
            font-size: 13px;
        }

        .highlight {
            background-color: #d9edf7;
        }

        ul {
            cursor: pointer;
        }

        .ag-header {
            background-color: black;
        }

        @media print {
            html, body {
                height: 99%;
            }

            .Total {
                background-color: #209e91 !important;
                color: white !important;
            }

            .Vacant {
                background-color: #90b900 !important;
                color: white !important;
            }

            .Allocated {
                background-color: #3498DB !important;
                color: white !important;
            }

            .Occupied {
                background-color: #e85656 !important;
                color: white !important;
            }

            .Overload {
                background-color: #777 !important;
                color: white !important;
            }

            .Inactive {
                background-color: #800080 !important;
                color: white !important;
            }

            .Blocked {
                background-color: #000000 !important;
                color: white !important;
            }

            .Selected {
                background-color: #ebf442 !important;
                color: black !important;
            }

            a[href]:after {
                content: none;
            }
        }

        @page {
            /*size: auto;*/ /* auto is the initial value */
            /*margin: 0mm;*/ /* this affects the margin in the printer settings */
            /*margin: 0px,0px,0px,10px;*/
            /*size: landscape;*/
            size: 25cm 25.7cm;
            margin: 5mm 5mm 5mm 5mm;
        }

        .my-label {
            position: absolute;
            width: 300px;
            font-size: 12px;
        }

        .text-labels {
            font-size: 10px;
            font-weight: 100;
            width: 50px !important;
            word-wrap: break-word;
            -moz-transform: rotate(-1.14rad);
            -webkit-transform: rotate(-1.14rad);
            -o-transform: rotate(-1.14rad);
            -ms-transform: rotate(-1.14rad);
            transform: rotate(-1.14rad);
            /* Use color, background, set margins for offset, etc */
        }

        .text-labels2 {
            font-size: 2px;
            font-weight: 200;
            width: 10px !important;
            word-wrap: break-word;
            position: center;
            /* Use color, background, set margins for offset, etc */
        }

        .select2-dropdown {
            top: 22px !important;
            left: 8px !important;
        }

        body {
            margin: 0px;
        }

        /*.modal .modal-dialog {
            width: 100%;
        }*/
    </style>
    <script defer type="text/javascript">

        function setup(id) {
            $('#' + id).datepicker({
                startDate: new Date(),
                format: 'mm/dd/yyyy',
                endDate: '+15d',
                autoclose: true,
                todayHighlight: true
            });
        };
        function timesetup(id) {
            //alert(1);
            $('#' + id).timepicker({
                'timeFormat': 'H:i',
                'show2400': true,
                'showDuration': true
            });
        };


    </script>
</head>
<body class="amantra" ng-controller="MaploaderController">
    <div class="animsition">
        <div class="widgets">
            <div ba-panel ba-panel-title="Asset Type Master" ba-panel-class="with-scroll">
                <div class="panel">
                    <%--<select id="country">
                     <option value="">Select Country</option>
                     <option value="586">586/Pakistan</option>
                     <option value="682">682/Saudi Arabia</option>
                     <option value="784">784/United Arab Emirates</option>
                     <option value="826">826/United Kingdom</option>
                     <option value="840">840/United States</option>
                    </select>--%>
                    <%--<div class="panel-heading" style="height: 41px;">--%>
                    <div class="row">

                        <span class="btn btn-warning" id="LAYOUTNAME" style="font-weight: bold"></span>


                        <span id="SU" class="btn btn-success" data-ng-show="mapPerDetails">SU(%)
                           <br />
                            <span class="badge" ng-bind="SU"></span>
                        </span>
                        <span class="btn btn-primary">Total
                            <br />
                            <span class="badge" ng-bind="SeatSummary.TOTAL"></span>
                        </span>

                        <span id="Occupied">Booked
                            <br />
                            <span class="badge" ng-bind="SeatSummary.OCCUPIED"></span>
                        </span>
                        <span id="Vacant">Available
                            <br />
                            <span class="badge" ng-bind="SeatSummary.VACANT"></span>
                        </span>
                        <span class="btn btn-info" data-ng-show="mapPerDetails">Total Area(sq.ft)
                            <br />
                            <span class="badge" ng-bind="TotalArea"></span>
                        </span>
                        <span class="btn btn-info" data-ng-show="mapPerDetails">Work Area(sq.ft)
                            <br />
                            <span class="badge" ng-bind="WorkArea">sq.ft</span>
                        </span>
                        <span class="btn btn-warning" ng-show="LastRefresh">LastRefreshDate-
                        <span ng-bind="LastRefreshDate" ng-show="LastRefresh"></span>
                        </span>
                    </div>
                    <br />
                    <div class="row">
                        <div class="clearfix col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>From Date</label>
                                    <div class="input-group date" id='fromdate1'>
                                        <input type="text" class="form-control" data-ng-model="Maploader.FROM_DATE1" name="FromDate"
                                            placeholder="mm/dd/yyyy" onclick="setup('fromdate1')" readonly="true" />

                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('fromdate1')"></i>
                                        </span>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>To Date</label>
                                    <div class="input-group date" id='todate1'>
                                        <input type="text" class="form-control" data-ng-model="Maploader.TO_DATE1" name="ToDate"
                                            placeholder="mm/dd/yyyy" onclick="setup('todate1')" readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('todate1')"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                           <%-- <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <select class="form-control" data-ng-model='Maploader.Search_SH_CODE'>
                                        <option value=''>--Select Shift--</option>
                                        <option data-ng-repeat='Shift in Shifts' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <select class="form-control" data-ng-model='Maploader.Search_MO_TYPE'>
                                        <option value=''>--Select Monitor --</option>
                                        <option data-ng-repeat='Monitor in Monitors' value='{{Monitor.MO_CODE}}'>{{Monitor.MO_NAME}}</option>
                                    </select>

                                </div>
                            </div>--%>

                            <div class="col-md-3 clearfix">
                                <button data-ng-click="SearchSpaces()" id="btnsubmit" class="btn btn-primary custom-button-color">Search Spaces</button>
                            </div>
                        </div>
                    </div>


                    <%-- <div class="align-right">

                        <label class="switch" data-ng-show="false" style="font-size: medium">
                            <input type="checkbox" id="myCheck" ng-click="myFunction()">
                            <span class="slider round"></span>
                        </label>
                    </div>--%>

                    <div class="panel-body" style="padding-right: 10px;">
                        <div class="row">
                            <div class="col-md-9">
                                <div id="main">
                                    <div id="leafletMap"></div>
                                    <!--<leaflet defaults="leafletDefaults" center="leafletCenter" bounds="leafletBounds" markers="leafletMarkers" controls="leafletDraw" id="leafletMap"></leaflet>-->

                                </div>
                            </div>
                            <div class="col-md-3" id="divFilter" style="display: none;">

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#divFilter" href="#divAccFilter">Filter</a>
                                        </h4>
                                    </div>
                                    <div id="divAccFilter" class="panel-collapse collapse in">

                                        <div id="Div2" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <form id="Form1" name="frmSearchspc" novalidate>
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12 col-xs-12" style="display: none">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CNY_NAME.$invalid}">
                                                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="Country" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                                                    data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Country[0].CNY_NAME" name="CNY_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CNY_NAME.$invalid">Please select country </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12" style="display: none">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid}">
                                                                <label class="control-label">City <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="City" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                                                    data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.City[0].CTY_NAME" name="CTY_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid">Please select city </span>

                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.LCM_NAME.$invalid}">
                                                                <label class="control-label">Location <span style="color: red;">*</span></label>
                                                                <div isteven-multi-select data-input-model="Location" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                                    data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Location[0].LCM_NAME" name="LCM_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.LCM_NAME.$invalid">Please select location </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid}">
                                                                <label class="control-label">Tower <span style="color: red;">**</span></label>
                                                                <div isteven-multi-select data-input-model="Tower" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                                                    data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Tower[0].TWR_NAME" name="TWR_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid">Please select tower </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid}">
                                                                <label class="control-label">Floor <span style="color: red;">**</span></label>
                                                                <div isteven-multi-select data-input-model="Floor" data-selection-mode="single" data-helper-elements="filter" data-output-model="SearchSpace.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                                    data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                                                </div>
                                                                <input type="text" data-ng-model="SearchSpace.Floor[0].FLR_NAME" name="FLR_NAME" style="display: none" required="" />
                                                                <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid">Please select floor </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="">
                                                        <input type="submit" class="btn btn-primary" value="Apply Filter" data-ng-click="FilterSpaces()" />
                                                        <%--<button type="button" class="btn btn-default" onclick="$('.slide_content').slideToggle();">Close</button>--%>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-3" id="divSearch" style="display: none;">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#divSearch" href="#divSearchdata">Search Employee</a>
                                        </h4>
                                    </div>
                                    <div id="divSearchdata" class="panel-collapse collapse in">
                                        <div id="Div4" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group row">
                                                            <label class="control-label  col-md-3">Search By<span style="color: red;">*</span></label>
                                                            <div class="col-md-9">
                                                                <select name="SelectedType" data-ng-model="SelectedType" class="form-control" id="ddlvert" required="" data-ng-change="SelectAllocEmp(SelectedType)">
                                                                    <option value="" selected>--Select--</option>
                                                                    <option ng-repeat="emp in EmpSearchItems" value="{{emp.CODE}}">{{emp.NAME}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group row">
                                                            <label class="control-label col-md-3">Value<span style="color: red;">*</span></label>
                                                            <div class="col-md-9">
                                                                <select name="mySelect" id="Searchdata" ng-options="det.NAME for det in LoadDet track by det.CODE"
                                                                    ng-model="SelectedID" class="form-control selectpicker" data-live-search="true"
                                                                    ng-change="ShowEmponMap(SelectedID)" required="">
                                                                </select>
                                                                <%--<select name="SelectedID" data-ng-model="SelectedID" class="form-control selectpicker" data-live-search="true"  id="Searchdata" required=""
                                                                    ng-change="ShowEmponMap(SelectedID)">
                                                                    <option value="" selected>--Select--</option>
                                                                    <option ng-repeat="det in LoadDet" value="{{det.CODE}}">{{det.NAME}}</option>
                                                                </select>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" ng-show="EmpDet">
                                                    <div class="panel-heading">Employee Details</div>
                                                    <div class="panel-body row">
                                                        <label class="control-label col-md-4 text-right">Name:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{EmpDet.AUR_NAME}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">Space:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{EmpDet.SPC_NAME}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">{{BsmDet.Parent}}:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{EmpDet.VER_NAME}}/{{EmpDet.VERTICAL}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">{{BsmDet.Child}}:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{EmpDet.Cost_Center_Name}}/{{EmpDet.Cost_Center_Code}}</label>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <label class="control-label col-md-4 text-right">FromDate:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-6 text-left">{{EmpDet.FROM_DATE}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">To Date:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-6 text-left">{{EmpDet.TO_DATE}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">Shift:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{getShiftName(EmpDet.SH_CODE)}}</label>
                                                        </div>
                                                        <label class="control-label col-md-4 text-right">VLAN:</label>
                                                        <div class="form-group">
                                                            <label class="col-md-8 text-left">{{getVLANName(EmpDet.VL_Code)}}</label>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="width: 200px; height: 100px;" id="text">
                                <div class="col-md-12" style="width: 200px; height: 100px; border: 1px solid #000; border-radius: 25px; background-color: antiquewhite" data-ng-repeat="ver in VerticalData">
                                    <table>
                                        <h5 style="font-weight: bold">{{ver}}</h5>
                                        <thead style="font-weight: bold">
                                            <tr>
                                                <td>Cabin</td>
                                                <td>:</td>
                                                <td data-ng-repeat="vername in Vertical_Wise_Data">
                                                    <span ng-if="ver==vername.VER_NAME&&vername.SPC_LAYER=='CB'">{{vername.CNT}}</span>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>WorkStation</td>
                                                <td>:</td>
                                                <td data-ng-repeat="vername in Vertical_Wise_Data"><span ng-if="ver==vername.VER_NAME&&vername.SPC_LAYER=='WS'">{{vername.CNT}}</span></td>

                                            </tr>
                                            <tr>
                                                <td>Semi Private Desk</td>
                                                <td>:</td>
                                                <td data-ng-repeat="vername in Vertical_Wise_Data"><span ng-if="ver==vername.VER_NAME&&vername.SPC_LAYER=='SPD'">{{vername.CNT}}</span></td>

                                            </tr>
                                            <tr>
                                                <td>Total</td>
                                                <td>:</td>
                                                <td data-ng-repeat="vername in Vertical_Wise_Total"><span ng-if="ver==vername.VER_NAME">{{vername.TOTAL}}</span></td>

                                            </tr>
                                        </thead>
                                    </table>

                                </div>
                                <br />

                            </div>

                            <div class="col-md-3" id="accordion" style="display: none">
                                <div class="panel panel-primary panel-map" id="DivSeatStatusPanel" ng-show="showStatusPanel==true">

                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#divSeatStatus">Seat Status</a>
                                        </h4>
                                    </div>
                                    <div id="divSeatStatus" class="panel-collapse collapse ">

                                        <ul class="list-group" id="ulSeatStatus">
                                            <li class="list-group-item thumbnail" data-ng-click="ShowTotalOnMap()">Total Seats<span class="label label-primary pull-right countVal Total" ng-bind="SeatSummary.TOTAL" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item thumbnail" data-ng-click="ShowVacantOnMap()">Available / Vacant<span id="Vacantstatus" ng-bind="SeatSummary.VACANT" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item thumbnail" data-ng-click="ShowCostCentre()">{{SeatSummary.ALLOCUNUSED}}
                                                    <span class="label label-info pull-right countVal Allocated" ng-bind="SeatSummary.ALLOCATEDVER + SeatSummary.ALLOCATEDCST" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item thumbnail" data-ng-click="ShowOccupiedOnMap()">{{SeatSummary.OCCUP_EMP}}
                                                
                                                    <span id="occupiedstatus" ng-bind="SeatSummary.OCCUPIED" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item">Overloaded Seats<span class="label label-default pull-right countVal Overload" ng-bind="SeatSummary.OVERLOAD" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item">Inactive/Long Leave Seats<span class="label longleave pull-right countVal Inactive" ng-bind="SeatSummary.LEAVE" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item">Blocked Seats<span class="label block pull-right countVal Blocked" ng-bind="SeatSummary.BLOCKED" ng-init="0"></span>
                                            </li>
                                            <li class="list-group-item">Selected Seats<span class="label selectedseat pull-right countVal Selected" style="color: black;">{{selectedSpaces.length}}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel panel-primary panel-map" data-ng-hide="Themes">
                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Filter</a>

                                        </h4>
                                    </div>

                                    <div id="collapseOne" class="panel-collapse collapse in">

                                        <%--  <div id="ThemeAccordian" class="panel-collapse collapse in">--%>
                                        <div class="panel-body">
                                            <div class="clearfix">
                                                <label class="control-label row col-md-2">Items: </label>
                                                <select class="selectpicker-xs col-md-10 pull-right" name="ddlItem" ng-init="ddlItem = 'verticalalloc'" ng-model="ddlItem" ng-change="loadThemes()">
                                                    <option value="" ng-selected="">--Select--</option>
                                                    <option ng-repeat="item in Items" value="{{item.CODE}}" title="{{item.NAME}}">{{item.NAME}}</option>
                                                </select>
                                            </div>
                                            <div data-ng-show="ddldisplay">
                                                <label class="control-label row col-md-2">Sub Items: </label>
                                                <select class="selectpicker-xs col-md-10 pull-right" name="ddlSubItem" ng-model="ddlSubItem" ng-change="loadSubDetails()">
                                                    <option value="" ng-selected="">--Select--</option>
                                                    <option ng-repeat="item in SubItems" value="{{item.CODE}}" ng-selected="{{item.ticked}}">{{item.NAME}}</option>
                                                </select>
                                            </div>

                                            <div class="col-md-12" data-ng-show="detailsdisplay">
                                                <label class="control-label col-md-3" style="color: red">Function</label>
                                                <label class="control-label col-md-3"></label>
                                                <label class="control-label col-md-1" style="color: rgb(32, 158, 145)">Tot</label>
                                                <label class="control-label col-md-1" style="color: rgb(217, 83, 79)">Occ</label>
                                                <label class="control-label col-md-1" style="color: rgb(92, 184, 92)">Vac</label>
                                            </div>
                                        </div>

                                        <div class="box-body">
                                            <ul class="list-group" id="ulThemes" data-ng-show="nrmaldisplay">
                                                <%--list-group-item-success--%>
                                                <li class="list-group-item" ng-class="{'list-group-item-info' : ddlSubItem == item.CODE}" ng-repeat="item in SpcCount">{{item.NAME}}
                                                        <span class="label pull-right countVal" ng-style="set_color(item.COLOR)" ng-bind="item.VAC" ng-init="0"></span>
                                                    <span class="label pull-right countVal" ng-style="set_color(item.COLOR)" ng-bind="item.ALC" ng-init="0"></span>
                                                    <span class="label pull-right countVal" ng-style="set_color(item.COLOR)" ng-bind="item.CNT" ng-init="0"></span>
                                                </li>
                                            </ul>

                                            <ul class="list-group" id="ulThemes" data-ng-show="detailsdisplay">
                                                <li class="list-group-item" ng-class="{'list-group-item-info' : ddlSubItem == item.CODE}" ng-repeat="item in SpcCount" ng-style="set_fontcolor(item.COLOR)">{{item.NAME}}
                                                    <span class="label pull-right countVal" style="color: rgb(92, 184, 92)" ng-bind="item.VAC" ng-init="0"></span>
                                                    <span class="label pull-right countVal" style="color: rgb(217, 83, 79)" ng-bind="item.ALC" ng-init="0"></span>
                                                    <span class="label pull-right countVal" style="color: rgb(32, 158, 145)" ng-bind="item.CNT" ng-init="0"></span>
                                                </li>

                                            </ul>
                                        </div>
                                        <%--</div>--%>
                                    </div>
                                </div>


                                <div class="panel panel-primary panel-map" data-ng-hide="Capacity">
                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#divSeatcap">Seating Capacity Details</a>

                                        </h4>
                                    </div>

                                    <div id="divSeatcap" class="panel-collapse collapse">
                                        <ul class="list-group" id="ulCapacity">
                                            <li class="list-group-item" ng-repeat="item in seatingCapacity">{{item.SPC_NAME}}
                                      <span class="label pull-right countVal" ng-style="set_color(item.COLOR)" data-ng-bind="item.CAPACITY" ng-init="0"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel panel-primary panel-map" data-ng-hide="WorkArea">
                                    <div class="panel-heading">

                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#divWorkarea">Work Area Details in sq. ft.</a>
                                        </h4>
                                    </div>

                                    <div id="divWorkarea" class="panel-collapse collapse">
                                        <ul class="list-group" id="ulArea">
                                            <li class="list-group-item" ng-repeat="item in SpcCountArea">{{item.NAME}}
                                <span class="label pull-right countVal" ng-style="set_color(item.COLOR)" ng-bind="item.SUM" ng-init="0"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="panel panel-primary panel-map" data-ng-hide="OtherArea">
                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-map">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#divotherarea">Other Area Details in sq. ft.</a>
                                        </h4>
                                    </div>
                                    <div id="divotherarea" class="panel-collapse collapse">
                                        <ul class="list-group" id="ulC">
                                            <li class="list-group-item" ng-repeat="item in Totalareadet">{{item.AREA}}
                                <span class="label pull-right label-primary countVal" ng-bind="item.SFT" ng-init="0"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="slide_block">
        <div class=" slide_head clearfix">
            <div class="pull-left Icons_right" ng-show="FilterVar == 'spcList'">
                <input id="filtertxt" placeholder="Filter..." type="text" class="form-control input-xs" />
            </div>

            <div class="pull-right Icons_right">
                <%--<div class="input-group">
                            <input type="text" placeholder="Search" />
                            <span class="input-group-addon">
                                <span class="fa fa-search" onclick="setup('fromdate')"></span>
                            </span>
                        </div>--%>
            </div>
        </div>
        <div class="slide_content">
            <div class="col-md-12" ng-show="FilterVar == 'spcList'">
                <div class="form-group">
                    <div style="height: 230px;">
                        <div data-ag-grid="gridOptions" style="height: 100%;" class="ag-blue"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="SeatAllocation" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" id="modalbooking">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <h4 class="modal-title" id="H1">Seat Allocation</h4>
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <h6 class="modal-title" style="text-align: right;">{{Details}}</h6>
                        </div>
                    </div>
                </div>
                <form id="Form2" name="frmSpaceAlloc">

                    <div class="modal-body" id="SyestemViewBooking">
                        <div class="row" id="HotDesking">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label>
                                    <input type="radio" name="SDate" data-ng-model="SDate" class="date-color" data-ng-value="1" checked />
                                    One Day
                                </label>
                                <label>
                                    <input type="radio" name="SDate" data-ng-model="SDate" class="date-color" data-ng-value="2" />
                                    One Week
                                </label>
                                <label data-ng-show="hotdeskspecifications">
                                    <input type="radio" name="SDate" data-ng-model="SDate" class="date-color" data-ng-value="3" />
                                    One Month
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div style="height: 180px" ng-show="selectedSpaces.length != 0">
                                    <input class="form-control" id="txtCountFilter" placeholder="Filter..." type="text" style="width: 25%" />
                                    <div data-ag-grid="gridSpaceAllocOptions" style="height: 90%;" class="ag-blue"></div>
                                </div>
                            </div>
                        </div>
                        <%--<div class="box-footer text-right" ng-show="EmpAllocSeats.length <= 0 && GetRoleAndRM[0].AUR_ROLE==1">--%>
                        <div class="box-footer text-right" ng-show="GetRoleAndRM[0].AUR_ROLE==1">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            <button type="button" ng-click="AllocateSeats()" id="btnAllocateSeats" data-ng-hide="AllocateEnabledStatus" class="btn btn-primary" style="width: 100px">Book Seats</button>
                            <%--<input type="button" ng-click="AllocateSeats()" class="btn btn-primary" value="Allocate Seats" ng-disabled="OnEmpAllocSeats" />--%>
                            <div class="btn-group dropup">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-ng-show="false" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Release From {{relfrom.Name}}  <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a ng-click="Release(1003,'Employee')">Employee</a></li>
                                    <li><a ng-click="Release(1003,BsmDet.Child)">{{BsmDet.Child}}</a></li>
                                    <li><a ng-click="Release(1002,BsmDet.Parent)">{{BsmDet.Parent}}</a></li>
                                    <li><a ng-click="Release(1052,'Blocked')">Blocked</a></li>
                                    <%--<li role="separator" class="divider"></li>
                                            <li><a href="#" ng-click="relfrom = 'Complete Release'; relfrom = 1001">Complete Release</a></li>--%>
                                </ul>
                            </div>
                        </div>
                        <%-- <div class="box-footer text-right" data-ng-show="EmpAllocSeats.length <= 0 && GetRoleAndRM[0].AUR_ROLE!=1">--%>
                        <div class="box-footer text-right" data-ng-show="GetRoleAndRM[0].AUR_ROLE!=1">

                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <%--<input type="button" data-ng-click="AllocateSeats()" data-ng-hide="AllocateEnabledStatus" class="btn btn-primary" value="Allocate Seats" />--%>
                            <input type="button" data-ng-click="AllocateSeats()" class="btn btn-primary" value="Allocate Seats" />
                            <div class="btn-group dropup">
                                <button type="button" class="btn btn-primary" data-ng-show="false" ng-click="Release(1003,BsmDet.Parent)">
                                    Release
                                </button>
                            </div>
                        </div>
                        <div class="box-footer" ng-show="EmpAllocSeats.length> 0">
                            <div class="panel-heading">
                                <h4 class="modal-title" id="H2">Seat(s) allocated to <%--{{SelectedEmp.AUR_NAME}}--%></h4>
                            </div>
                            <div class="panel-body">
                                <div class="clearfix">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <table id="tblempalloc" width="100%" class="table table-condensed table-bordered table-hover table-striped">
                                            <tr>
                                                <th></th>
                                                <th>Space ID</th>
                                                <th>Employee</th>
                                                <th>Location</th>
                                                <th>Tower</th>
                                                <th>Floor</th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Shift</th>
                                                <th>VLAN</th>
                                            </tr>
                                            <tr ng-repeat="emp in EmpAllocSeats">
                                                <td>
                                                    <input type="button" id="btnRelease" value="Release" ng-click="ReleaseEmp(emp)" ng-show="EmployeeRelease" />
                                                    <input type="button" id="btnRelease1" value="Release" ng-click="ReleaseEmp(emp)" ng-show="UptoVetrialRelease" />
                                                </td>
                                                <td>{{emp.SPC_NAME}}</td>
                                                <td>{{emp.AUR_NAME}}/{{emp.AUR_ID}}</td>
                                                <td>{{emp.LOCATION}}</td>
                                                <td>{{emp.TOWER}}</td>
                                                <td>{{emp.FLOOR}}</td>
                                                <td>{{emp.FROM_DATE}}</td>
                                                <td>{{emp.TO_DATE}}</td>
                                                <td>{{emp.SH_CODE}}</td>
                                                <td>{{emp.VL_Code}}</td>
                                            </tr>
                                            <%-- <tr>
                                                <td colspan="5">
                                                    <a ng-href="{{urlRedirection}}">Schedule seat</a>
                                                </td>
                                            </tr>--%>
                                        </table>
                                    </div>
                                    <div class="row"></div>
                                    <div class="box-footer text-right">
                                        <%-- <button type="button" class="btn btn-primary" data-ng-hide="ProallocEnabledStatus" ng-click="Proceedalloc(alloc)" <%-- ng-click="EmpAllocSeats = []"--%><%--Proceed to allocate additional seat--%>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer" ng-show="SeatAllocForSpace.length> 0 && GetRoleAndRM[0].AUR_ROLE==14">
                            <div class="panel-heading">
                                <h4 class="modal-title" id="H2">Seat(s) allocated to SPACE <%--{{SelectedEmp.AUR_NAME}}--%></h4>
                            </div>
                            <div class="panel-body">
                                <div class="clearfix">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <table id="tblempalloc1" width="100%" class="table table-condensed table-bordered table-hover table-striped">
                                            <tr>
                                                <th></th>
                                                <th>Space ID</th>
                                                <th>Employee</th>
                                                <th>Location</th>
                                                <th>Tower</th>
                                                <th>Floor</th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Shift</th>
                                                <th>VLAN</th>
                                            </tr>
                                            <tr ng-repeat="emp in SeatAllocForSpace">
                                                <td>
                                                    <input type="button" id="btnRelease" value="Release" ng-click="ReleaseEmp(emp)" ng-show="EmployeeRelease" />
                                                    <input type="button" id="btnRelease1" value="Release" ng-click="ReleaseEmp(emp)" ng-show="UptoVetrialRelease" />
                                                </td>
                                                <td>{{emp.SPC_NAME}}</td>
                                                <td>{{emp.AUR_NAME}}/{{emp.AUR_ID}}</td>
                                                <td>{{emp.LOCATION}}</td>
                                                <td>{{emp.TOWER}}</td>
                                                <td>{{emp.FLOOR}}</td>
                                                <td>{{emp.FROM_DATE}}</td>
                                                <td>{{emp.TO_DATE}}</td>
                                                <td>{{emp.SH_CODE}}</td>
                                                <td>{{emp.VL_Code}}</td>
                                            </tr>
                                            <%-- <tr>
                                                <td colspan="5">
                                                    <a ng-href="{{urlRedirection}}">Schedule seat</a>
                                                </td>
                                            </tr>--%>
                                        </table>
                                    </div>
                                    <div class="row"></div>
                                    <div class="box-footer text-right">
                                        <%-- <button type="button" class="btn btn-primary" data-ng-hide="ProallocEnabledStatus" ng-click="Proceedalloc(alloc)" <%-- ng-click="EmpAllocSeats = []"--%><%--Proceed to allocate additional seat--%>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="model-body cls-md-9 cls-sm-9 cls-xs-9" id="MobileViewBooking">
                        <div style="text-align: center; margin-left: 8px; margin-right: 8px">
                            <div class="form-group">
                                <label class="control-label col-md-12 col-sm-12 col-xs-12">Space id</label>
                                <input type="text" class="form-control form-control col-sm-12 col-xs-12" readonly="readonly" value="{{selectedSpaces[0].SPC_NAME}}" />

                            </div>
                            <div class="form-group" data-ng-show="SearchEmployee">
                                <label class="control-label col-md-12 col-sm-12 col-xs-12">Search Employee</label>
                                <input name="data_aur_search" class="form-control" list='employees' data-ng-change='binddata(data)' data-ng-model='data.AUR_SEARCH' /><datalist id='employees'><option data-ng-repeat='Employee in AurNames' value='{{Employee.AUR_ID}}' text='{{Employee.AUR_NAME}}'>{{Employee.AUR_NAME}} </option>
                                </datalist>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12 col-sm-12 col-xs-12">Employee</label>
                                <input type="text" class="form-control form-control col-sm-12 col-xs-12" readonly="readonly" value="{{selectedSpaces[0].AUR_NAME}}" />
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12 col-sm-12 col-xs-12">From date</label>
                               <%-- <div class="input-group date col-md-9 col-sm-9 col-xs-9" id='fromdate'>
                                    <input type="text" class="form-control" data-ng-model="Maploader.FROM_DATE" id="Text1" name="FromDate"
                                        placeholder="mm/dd/yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>--%>
                                <input type="text" class="form-control form-control col-sm-12 col-xs-12" readonly="readonly" data-ng-model="Maploader.FROM_DATE" value="{{Maploader.FROM_DATE1}}" />
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-12 col-sm-12 col-xs-12">To Date</label>
                             <%--   <div class="input-group date col-md-9 col-sm-9 col-xs-9" id='todate'>
                                    <input type="text" class="form-control" data-ng-model="Maploader.TO_DATE" id="Text2" name="ToDate"
                                        placeholder="mm/dd/yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>--%>
                                <input type="text" class="form-control form-control col-sm-12 col-xs-12" readonly="readonly" data-ng-model="Maploader.TO_DATE" value="{{Maploader.TO_DATE1}}"/>
                            </div>
                            <div class="form-group" data-ng-show="GeneralSeatType">
                                <label class="control-label col-md-12 col-sm-12 col-xs-12">Select Shift</label>
                                <select class="form-control" data-ng-init="ShiftData[0].SH_CODE"  data-ng-model='Maploader.SH_CODE'>
                                    <option value=''>--Select--</option>
                                    <option data-ng-repeat='Shift in ShiftData' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option>
                                </select>
                            </div>

                            <div class="form-group" data-ng-show="GeneralSeatType">
                                <label class="control-label col-md-12 col-sm-12 col-xs-12">Select VLAN</label>
                                <select class="form-control" data-ng-model='Maploader.VL_Code'>
                                    <option value=''>--Select--</option>
                                    <option data-ng-repeat='VLAN in VLANData' selected="0" value='{{VLAN.VL_Code}}'>{{VLAN.VL_Name}}</option>
                                </select>
                            </div>
                            <div class="form-group" data-ng-show="HotdeskSeatType">
                                <label class="control-label col-md-12 col-sm-12 col-xs-12">From Time</label>
                                <div class="input-group col-md-8 col-sm-8 col-xs-8">
                                    <input type="text" id="fromtime" name="FromTimeFilter" class="form-control timepicker timecss" data-ng-model="Maploader.FROM_TIME"
                                        required="" data-ng-disabled="statsss == 2" onclick="timesetup('fromtime')" />
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o" onclick="timesetup('fromtime')"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" data-ng-show="HotdeskSeatType">
                                <label class="control-label col-md-12 col-sm-12 col-xs-12">To Time</label>
                                <div class="input-group col-md-8 col-sm-8 col-xs-8">
                                    <input type="text" id="totime" name="FromTimeFilter" class="form-control timepicker timecss" data-ng-model="Maploader.TO_TIME"
                                        required="" data-ng-disabled="statsss == 2" onclick="timesetup('totime')" />
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o" onclick="timesetup('totime')"></i>
                                    </div>
                                </div>
                            </div>


                            <div data-ng-show="EmpAllocSeats.length <=0 && selectedSpaces[0].Status!=1004">
                                <input type="button" class="btn btn-primary" value="Allocate Seats" data-ng-click="AllocateData()" />
                            </div>
                            <br />
                            <div data-ng-show="selectedSpaces[0].Status==1004">
                                <button type="button" class="btn btn-primary" data-ng-click="Release(1003,BsmDet.Child)">Release</button>
                            </div>
                            <br />
                            <div data-ng-show="EmpAllocSeats.length> 0">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <table id="tblempalloc1" width="100%" class="table table-condensed table-bordered table-hover table-striped">
                                        <tr>
                                            <th>Space ID</th>
                                            <th></th>
                                        </tr>
                                        <tr data-ng-repeat="emp in EmpAllocSeats">
                                            <td>{{emp.SPC_NAME}}</td>
                                            <td>
                                                <input type="button" value="Release" data-ng-click="ReleaseEmp(emp)" data-ng-show="EmployeeRelease" />
                                                <input type="button" value="Release" data-ng-click="ReleaseEmp(emp)" data-ng-show="UptoVetrialRelease" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>

                    </div>

                    <div class="modal-footer">
                    </div>
                </form>
            </div>
        </div>

    </div>


    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="Active" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H3">Active/Inactive Spaces</h4>
                </div>
                <form id="Form3" name="frmSpaceAlloc">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div style="height: 200px">
                                    <input class="form-control" id="InacFilter" placeholder="Filter..." type="text" style="width: 25%" />
                                    <div data-ag-grid="gridSpaceAllocation" style="height: 80%;" class="ag-blue"></div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer text-right" ng-show="EmpAllocSeats.length <= 0">
                            <input type="button" class="btn btn-danger" value="Inactive Spaces" data-ng-click="InactiveSpaces()" ng-show="selectedSpaces.length > 0" />
                            <input type="button" class="btn btn-primary" value="Activate Space" data-ng-click="ActivateSpaces()" ng-show="selectedSpaces.length==0" />
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>

    <script defer src="../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/Leaflet.fullscreen.min.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../BootStrapCSS/leaflet/addons/easy-button.js"></script>
    <script defer src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/leaflet/Print/leaflet.easyPrint.js"></script>
    <script defer src="../../Scripts/angular-block-ui.min.js"></script>
    <script defer src="../../BootStrapCSS/Scripts/angular-confirm.js"></script>
    <script defer src="../../Scripts/jQuery.print.js"></script>
    <script defer src="../../BlurScripts/BlurJs/DashboardPrint.js"></script>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>--%>
    <script src="../../assets/js/Jquerytimepicker1.10.0.js"></script>

    <%--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>--%>
    <script src="../../assets/js/cdnjsselect2-4.0.10.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "blockUI", "cp.ngConfirm"]);
    </script>

    <script defer src="../Utility.js"></script>
    <%--<script defer src="../../BootStrapCSS/Scripts/AllocateSeatToEmployeeVertical.js"></script>--%>
    <script defer src="maploader.js"></script>
    <script defer src="../DeptAlloc/Js/SpaceRequisition.min.js"></script>
    <script defer>

        function slideTogglefn() {
            $('#slide_trigger').click(function () {
                $(this).toggleClass("glyphicon-chevron-down");
                $('.slide_content').slideToggle();
            });
        }
        $(document).ready(function () {
            $('#toggleSlide').click(function () {
                $('#slide_trigger').click();
            });
            slideTogglefn();
        });

        //function setup(id) {
        //    $('#' + id).datepicker({
        //        format: 'mm/dd/yyyy',
        //        startDate=new Date(),
        //        endDate: '+15d',
        //        autoclose: true,
        //        todayHighlight: true

        //    });
        //};
        //$("#country").select2({
        //    placeholder: "Select Country",
        //    allowClear: true
        //});

    </script>
    <script>
        var UserId = '<%= Session["UID"] %>';
    </script>

    <script defer src="../../Scripts/moment.min.js"></script>
</body>
</html>

