<%@ Page Language="VB" AutoEventWireup="true" Title="User Floor List" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script defer src="http://code.angularjs.org/1.2.0rc1/angular-animate.min.js"></script>
    <script defer src="http://code.angularjs.org/1.2.0rc1/angular-touch.min.js"></script>
    <script defer src="Responsive.min.js"></script>
    <style>
        .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
             padding-right: 0px !important; 
             padding-left: 0px !important; 
        }

        * {
            margin: 0;
            padding: 0;
        }


        .arrow {
            cursor: pointer;
            display: block;
            height: 64px;
            margin-top: -35px;
            outline: medium none;
            position: absolute;
            top: 50%;
            width: 64px;
            z-index: 5;
        }

            .arrow.prev {
                background-image: url("../../images/prev.png");
                left: -47px;
                opacity: 0.2;
                transition: all 0.2s linear 0s;
                background-size: 42px;
                background-repeat: no-repeat;
            }

            .arrow.next {
                background-image: url(../../images/next.png);
                opacity: 0.2;
                right: -97px;
                transition: all 0.2s linear 0s;
                background-size: 42px;
                background-repeat: no-repeat;
            }

            .arrow.prev:hover {
                opacity: 1;
            }

            .arrow.next:hover {
                opacity: 1;
            }

        .container-fluid {
            padding-right: 0px !important;
            padding-left: 0px !important;
        }

        .center {
            margin-left: auto;
            margin-right: auto;
            width: 10%;
        }


        .slider {
            border: 15px solid #FFFFFF;
            border-radius: 5px;
            height: 430px;
            margin: -36px auto;
            position: relative;
            width: 500px;
            right: 13px;
            -webkit-perspective: 1000px;
            -moz-perspective: 1000px;
            -ms-perspective: 1000px;
            -o-perspective: 1000px;
            perspective: 1000px;
            -webkit-transform-style: preserve-3d;
            -moz-transform-style: preserve-3d;
            -ms-transform-style: preserve-3d;
            -o-transform-style: preserve-3d;
            transform-style: preserve-3d;
        }

        .slide {
            position: absolute;
            top: 0;
            left: 0;
        }

            .slide.ng-hide-add {
                opacity: 1;
                -webkit-transition: 1s linear all;
                -moz-transition: 1s linear all;
                -o-transition: 1s linear all;
                transition: 1s linear all;
                -webkit-transform: rotateX(50deg) rotateY(30deg);
                -moz-transform: rotateX(50deg) rotateY(30deg);
                -ms-transform: rotateX(50deg) rotateY(30deg);
                -o-transform: rotateX(50deg) rotateY(30deg);
                transform: rotateX(50deg) rotateY(30deg);
                -webkit-transform-origin: right top 0;
                -moz-transform-origin: right top 0;
                -ms-transform-origin: right top 0;
                -o-transform-origin: right top 0;
                transform-origin: right top 0;
            }

                .slide.ng-hide-add.ng-hide-add-active {
                    opacity: 0;
                }

            .slide.ng-hide-remove {
                -webkit-transition: 1s linear all;
                -moz-transition: 1s linear all;
                -o-transition: 1s linear all;
                transition: 1s linear all;
                display: block !important;
                opacity: 0;
            }

                .slide, .slide.ng-hide-remove.ng-hide-remove-active {
                    opacity: 1;
                }

        @media (max-width: 900px) {

            .slider {
                height: 312px;
                width: 500px;
            }

                .slider .slide {
                    width: 100%;
                }
        }

        @media (max-width: 550px) {

            .slider {
                height: 188px;
                width: 300px;
            }

                .slider .slide {
                    width: 100%;
                }
        }
    </style>
</head>
<body class="amantra">
    <div class="animsition" data-ng-controller="FloorLstController">
        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25" style="padding: inherit">
                    <div class="panel-heading-qfms">
                        <div class="panel-heading " style="height: 50px; background-color: #0e2f5d">
                            <h3 class="panel-title panel-heading-qfms-title" style="padding-left: 14px; color: #fff">Floor Maps 
						<img class="pull-right center" style="height: 35px;width: 35px;" src="../../assets/images/icons/SpaceMgmt1.png" />

                            </h3>
                        </div>
			
                        <div class="panel-body" style="padding-right: 10px;">
                        <%-- <div class="clearfix">
                                <div class="box-footer text-right">
                                    <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                                </div>
                            </div>--%>
                        <br />

                        <form id="form1">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div style="height: 530px" data-ng-show="IsWeb">
                                        <input id="txtCountFilter" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%" />
                                        <div data-ag-grid="gridOptions" style="height: 90%;" class="ag-blue"></div>
                                    </div>
                                    <div data-ng-show="IsMobile">
                                        <div class="col-md-6" style="padding: inherit">
                                            <input id="txtsearch" class="form-control" data-ng-model="TxtSearch" placeholder="Filter by Floor and Tower..." type="text" />


                                        </div>
                                        <ul class="list-group" id="ulThemes">
                                            <li class="list-group-item" data-ng-class="{list-group-item-info}; font-size:12px" data-ng-repeat="item in data | filter : { FILTERDATA : TxtSearch}">
                                                <img src="../../assets/images/icons/Location.png" width="15" />
                                                <label>Location :</label>
                                                <span class="label pull-right" style="color: rgb(92, 184, 92); font-size: 11px" data-ng-bind="item.LCM_NAME"></span>
                                                <br />
                                                <img src="../../assets/images/icons/tower.png" width="15" />
                                                <label>Tower :</label>
                                                <span class="label pull-right" style="color: rgb(92, 184, 92); font-size: 12px" data-ng-bind="item.TWR_NAME"></span>
                                                <br />
                                                <img src="../../assets/images/icons/floor.png" width="15" />
                                                <label>Floor :</label>
                                                <a class="pull-right btn btn-primary" href="../../SMViews/Map/Maploader.aspx?lcm_code={{item.LCM_CODE}} &twr_code= {{item.TWR_CODE}} &flr_code={{item.FLR_CODE}}">{{item.FLR_NAME}}</a>
                                                <br />
                                                <br />

                                            </li>

                                        </ul>

                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                <div class="container">
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <%--<h4 class="modal-title">Tower Images</h4>--%>
                                </div>
                                <div class="modal-body">
                                    <div class="container slider">

                                        <!-- enumerate all photos -->
                                        <img data-ng-repeat="slideOptions in slideOptionsData" class="slide" data-ng-swipe-right="showPrev()" data-ng-swipe-left="showNext()" data-ng-show="isActive($index)" data-ng-src="{{slideOptions.TWI_UPLOAD_PATH}}" width="500" height="350" />

                                        <!-- prev / next controls -->
                                        <a class="arrow prev" href="#" data-ng-click="showPrev()"></a>
                                        <a class="arrow next" href="#" data-ng-click="showNext()"></a>

                                        <!-- extra navigation controls -->
                                        <%--<ul class="nav">
            <li ng-repeat="slideOptions in slideOptionsData" ng-class="{'active':isActive($index)}">
                <img src="{{slideOptions.TWI_UPLOAD_PATH}}"  ng-click="showPhoto($index);" />
            </li>
        </ul>--%>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <%-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--%>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <%--<script defer src="../Utility.js"></script>--%>
    <script defer src="../Utility.min.js"></script>
    <script src="../../assets/js/scripts.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer>

        app.service('FloorLstService', function ($http, $q, UtilityService) {

            this.getFloorLst = function (result) {
                var deferred = $q.defer();
                return $http.get(UtilityService.path + '/api/MaploaderAPI/GetFloorLst')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };
            this.GetImageList = function (Image) {
                var deferred = $q.defer();
                return $http.get(UtilityService.path + '/api/MaploaderAPI/GetImageList?Image=' + Image + ' ')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

        });
        app.controller('FloorLstController', function ($scope, $q, FloorLstService, $filter, UtilityService) {
            $scope.FloorLst = {};
            $scope.data = [];
            UtilityService.getSysPreferences().then(function (response) {
                if (response.data != null) {
                    $scope.SysPref = response.data;
                    $scope.FlipKartSmallChanges = _.find($scope.SysPref, { SYSP_CODE: "Flipkart Small Changes" });
                }
                return FloorLstService.getFloorLst()
            }).then(function (response) {

                $scope.data = response.data;

                for (var i in $scope.data) {
                    $scope.data[i].FILTERDATA = $scope.data[i].FLR_NAME + ' ' + $scope.data[i].TWR_NAME + ' ' + $scope.data[i].LCM_NAME;
                }

                if (response.data != null) {

                    if ($scope.FlipKartSmallChanges.SYSP_VAL1 == 1) {
                        $scope.gridOptions.columnApi.setColumnVisible('Image', false);
                    }


                    if (window.innerWidth <= 780) {

                        $scope.IsMobile = true;
                        $scope.IsWeb = false;
                    }
                    else {
                        $scope.gridOptions.api.setRowData(response.data);
                        $scope.IsMobile = false;
                        $scope.IsWeb = true;
                    }

                    //$scope.gridOptions.api.sizeColumnsToFit();
                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            });

            $scope.slideOptionsData = [];
            $scope.columDefsAlloc = [
                {
                    headerName: "View Map", cellClass: "grid-align", width: 150, filter: 'set',
                    //template: "<a href='../../SMViews/Map/Maploader.aspx?lcm_code={{data.LCM_CODE}}&twr_code={{data.TWR_CODE}}&flr_code={{data.FLR_CODE}}'>View Map</a>"
                    template: '<a ng-click = "EditFunction(data)">View Map</a>'
                },
                { headerName: "Floor", cellClass: "grid-align", field: "FLR_NAME" },
                { headerName: "Tower/ Building", field: "TWR_NAME", cellClass: "grid-align" },
                { headerName: "Location", field: "LCM_NAME", cellClass: "grid-align" },
                { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', filter: 'set' },

                //{ headerName: "View Image", field: "Image", width: 90, cellRenderer: ViewImageCellRendererFunc, },
            ];

            $scope.EditFunction = function (data) {

                window.open("../../SMViews/Map/Maploader.aspx?lcm_code=" + encodeURIComponent(data.LCM_CODE) + "&twr_code=" + encodeURIComponent(data.TWR_CODE) + "&flr_code=" + encodeURIComponent(data.FLR_CODE), "_self")

            };




            $scope._Index = 0;
            // if a current image is the same as requested image
            $scope.isActive = function (index) {
                return $scope._Index === index;
            };
            function ImageClicked(Image) {
                FloorLstService.GetImageList(Image).then(function (response) {
                    if (response.data != null) {

                        $scope.slideOptionsData = response.data;
                        //_.forEach($scope.slideOptionsData, function (n, key) {
                        //    console.log(n);
                        //        $scope.slideOptionsData.push(n);

                        //});

                    }
                    else
                        showNotification('error', 8, 'bottom-right', response.Message);
                });
            }
            function ViewImageCellRendererFunc(params) {
                params.$scope.ImageClicked = ImageClicked;
                return '<a ng-click="ImageClicked(data.TWR_CODE)" id="lnkView"  data-toggle="modal" data-target="#myModal">View Image</a>';
            }

            $scope.gridOptions = {
                columnDefs: $scope.columDefsAlloc,
                rowData: null,
                cellClass: 'grid-align',
                angularCompileRows: true,
                enableColResize: true,
                enableCellSelection: true,
                enableSorting: true,
                groupHideGroupColumns: true,
            };

            //FloorLstService.getFloorLst().then(function (response) {
            //    alert(1);
            //    if (response.data != null) {
            //        $scope.gridOptions.api.setRowData(response.data);
            //    }
            //    else
            //        showNotification('error', 8, 'bottom-right', response.Message);
            //});

            $("#txtCountFilter").change(function () {
                onReq_SelSpacesFilterChanged($(this).val());
            }).keydown(function () {
                onReq_SelSpacesFilterChanged($(this).val());
            }).keyup(function () {
                onReq_SelSpacesFilterChanged($(this).val());
            }).bind('paste', function () {
                onReq_SelSpacesFilterChanged($(this).val());
            });

            function onReq_SelSpacesFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }

            // initial image index
            $scope._Index = 0;

            // if a current image is the same as requested image
            $scope.isActive = function (index) {
                return $scope._Index === index;
            };

            // show prev image
            $scope.showPrev = function () {
                $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.slideOptionsData.length - 1;
            };

            // show next image
            $scope.showNext = function () {
                $scope._Index = ($scope._Index < $scope.slideOptionsData.length - 1) ? ++$scope._Index : 0;
            };

            // show a certain image
            $scope.showPhoto = function (index) {
                $scope._Index = index;
            };

        });
    </script>
</body>
</html>

