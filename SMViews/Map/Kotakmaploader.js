﻿//agGrid.initialiseAgGridWithAngular1(angular);

//var app = angular.module('QuickFMS', ["agGrid"]);


app.service('MaploaderService', function ($http, $q, UtilityService) {

    this.bindMap = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetMapItems', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.bindMarkers = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetMarkers', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.bindCornerLables = function (result) {
        var deferred = $q.defer();
        var dataobj = { flr_code: result };
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetCornerLables', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.GetFlrIdbyLoc = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Space_mapAPI/GetFloorIDBBox?lcm_code=' + GetParameterValues('lcm_code') + '&twr_code=' + GetParameterValues('twr_code') + '&flr_code=' + GetParameterValues('flr_code'))
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsBySPCID = function (spcid) {
        var deferred = $q.defer();
        var dataobj = { flr_code: $('#ddlfloors').val(), CatValue: spcid };
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetSpaceDetailsBySPCID', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsByREQID = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetSpaceDetailsByREQID', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsBySUBITEM = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetSpaceDetailsBySUBITEM', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceDetailsBySHIFT = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetSpaceDetailsBySHIFT', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.release = function (reqdet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/ReleaseSelectedseat', reqdet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    }

    this.ActivateSpaces = function (reqdet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/ActivateSpaces', reqdet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    }

    this.getEmpDetails = function (selCostCenters) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetEmpDetails', selCostCenters)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getAllocEmpDetails = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetAllocEmpDetails', selDet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.allocateSeats = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/AllocateSeats', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.spcAvailabilityByShift = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/SpcAvailabilityByShift', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpaceFromFloorMaps = function (selSpaces) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetSpaceRecords', selSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getLegendsSummary = function (flrid) {
        var deferred = $q.defer();
        var dataobj = { flr_code: flrid };
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetLegendsSummary', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getallFilterbyItem = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/KotakMaploader/GetallFilterbyItem')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getallFilterbySubItem = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetallFilterbySubItem', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetTotalAreaDetails = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetTotalAreaDetails', selDet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSeatingCapacity = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetSeatingCapacity', selDet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getEmpAllocSeat = function (selDet) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/GetEmpAllocSeat', selDet)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.InactiveSeats = function (spcobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/KotakMaploader/InactiveSpaceSeats', spcobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});

app.controller('MaploaderController', function ($scope, $q, MaploaderService, UtilityService, SpaceRequisitionService, $filter, blockUI, $timeout, $ngConfirm) {
    $scope.SelLayers = [];
    $scope.Markers = [];
    $scope.marker = {};
    $scope.pageSize = '1000';
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.SpaceAlloc = {};
    $scope.SpaceAlloc.Verticals = [];
    $scope.Verticals = [];
    $scope.CostCenters = [];
    $scope.Shifts = [];
    $scope.EmpDetails = [];
    $scope.SpaceAlloc.CostCenters = [];
    $scope.selectedSpaces = [];
    $scope.Markerdata = [];
    $scope.Items = [];
    $scope.SubItems = [];
    $scope.SelectedEmp = [];
    // $scope.EmpAllocSeats = [];
    //$scope.FilterVar = "EmpSearch";
    $scope.ddldisplay = false;
    $scope.MarkerLblLayer = [];
    $scope.MarkerMeetingLblLayer = [];
    $scope.SpcCountArea = [];
    $scope.Empcount = [];
    $scope.OnEmpAllocSeats = false;
    $scope.FloorID;
    $scope.floorDetails = [];
    $scope.kiosk = [];

    $scope.EmpSearchItems = [{ CODE: 'AUR_ID', NAME: 'User ID' },
    { CODE: 'AUR_KNOWN_AS', NAME: 'Name' },
    { CODE: 'AUR_EMAIL', NAME: 'Email ID' },
    { CODE: 'SPC_ID', NAME: 'Space ID' }];

    
    var map = L.map('leafletMap');
    $scope.Markerdata = new L.FeatureGroup();
    $scope.MarkerLblLayer = new L.FeatureGroup();
    $scope.MarkerLblLayer_copy = new L.FeatureGroup();

    $scope.MarkerMeetingLblLayer = new L.FeatureGroup();
    $scope.MarkerMeetingLblLayer_copy = new L.FeatureGroup();
    //new layer addition
    $scope.MarkerLblLayerEmpDetails = new L.FeatureGroup();
    $scope.MarkerLblLayerEmpDetails_copy = new L.FeatureGroup();

    map.addLayer($scope.Markerdata);
    var prefval;
    var overlayMaps = {};
    UtilityService.getSysPreferences().then(function (response) {

        if (response.data != null) {
            $scope.SysPref = response.data;
        }
        prefval = _.find($scope.SysPref, { SYSP_CODE: "display emp details on map" })
    });

  

    setTimeout(function () {
        if (prefval != undefined) {
            if (prefval.SYSP_CODE == 'display emp details on map') {
                var overlayMaps = {
                    "Markers": $scope.Markerdata,
                    "Labels": $scope.MarkerLblLayer,
                    "Emp Details": $scope.MarkerLblLayerEmpDetails
                };
            }
        }
        if (prefval == undefined) {
            var overlayMaps = {
                "Markers": $scope.Markerdata,
                "Labels": $scope.MarkerLblLayer
            };
        }

        L.control.layers({}, overlayMaps).addTo(map);
    }, 2000);

    map.addControl(new L.Control.Fullscreen());
    //map.toggleFullscreen();
    
    
    //L.control.layers({}, overlayMaps).addTo(map);

    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var AllocateStyle = { fillColor: '#3498DB', opacity: 0.8, fillOpacity: 0.8 };
    var ReservedStyle = { fillColor: '#17202A', opacity: 0.8, fillOpacity: 0.8 };
    var OccupiedStyle = { fillColor: '#E74C3C', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };
    var LeaveStyle = { fillColor: '#800080', opacity: 0.8, fillOpacity: 0.8 };
    var BlockStyle = { fillColor: '#000000', opacity: 0.8, fillOpacity: 0.8 };
    var CstVer = { fillColor: '#c47750', opacity: 0.8, fillOpacity: 0.8 };
    var InactiveStyle = { fillColor: '#a4adbc', opacity: 0.8, fillOpacity: 0.8 }
    //Initialize map

    //This is currently the only way to get access to the drawnItems feature group which
    //is required to implement the edit functionality

    /// Refresh - resets the markers button
    L.easyButton('fa fa-refresh', function () {
        $scope.selectedSpaces = [];
        $scope.SelectedType = "";
        $scope.SelectedID = "";
        $scope.LoadDet = [];
        $scope.ddldisplay = false;
        $scope.FilterVar = "";

        $scope.$apply(function () {
            if ($('#divSearch').css('display') != 'none' && $('#accordion').css('display') == 'none') {
                $('#divSearch').hide();
                $('#accordion').show();
            }
        });

        reloadMarkers();
       
            $scope.ShowEmponMap();
      
        map.fitBounds($scope.bounds);
        $scope.$apply(function () {
            $scope.ddlItem = "spacetype";
        });
        $scope.loadThemes();
        updateSummaryCount();
    }, "Refresh").addTo(map);

    //// Filter button
    //L.easyButton('fa fa-filter', function () {
    //    $scope.$apply(function () {
    //        if ($('#divSearch').css('display') != 'none' && $('#accordion').css('display') == 'none') {
    //            $('#divSearch').hide();
    //        }
    //        else
    //            $('#accordion').toggle("slow");

    //        setTimeout(function () {
    //            $('#divFilter').toggle("slow");
    //        }, 100);
    //    });
    //}, "Select Map").addTo(map);

    ///// View Grid button
    //L.easyButton('fa fa-list-ul', function () {
    //    $scope.$apply(function () {
    //        $scope.FilterVar = "spcList";

    //        $('.slide_content').slideToggle();
    //    });
    //}, "Search Filter").addTo(map);

    /// Allocate button
    //L.easyButton('fa fa-user', function () {
    //    $scope.validate = false;
    //    progress(0, 'Loading...', true);

    //    if ($scope.selectedSpaces.length != 0) {
    //        progress(0, '', false);
    //        $('#SeatAllocation').modal('show');
    //    }
    //    else {
    //        progress(0, '', false);
    //        showNotification('error', 8, 'bottom-right', "Please select atleast one seat to allocate / release");
    //    }
    //}, "Allocate / Release").addTo(map);

   

    //L.easyButton('fa fa-search', function () {
    //    $scope.$apply(function () {
    //        if ($('#divFilter').css('display') != 'none' && $('#accordion').css('display') == 'none') {
    //            $('#divFilter').hide();
    //        }
    //        else
    //            $('#accordion').toggle("slow");
    //        setTimeout(function () {
    //            $('#divSearch').toggle("slow");
    //        }, 100);
    //    });
    //}, "Search Employee").addTo(map);

    //L.easyButton('fa fa-check-square-o', function () {
    //    $scope.validate = false;
    //    progress(0, 'Loading...', true);
    //    $scope.GetInactiveSpaces();
    //}, "Inactive Spaces").addTo(map);

    //L.easyPrint({
    //    title: 'Print Map',
    //    elementsToHide: '.gitButton, .easy-button-button, .leaflet-control-zoom, .leaflet-control-layers-overlays'
    //}).addTo(map);

    //L.easyButton('fa fa-print', function () {
    //    printWithoutHeaderfooter('leafletMap');

    //}, "Print Only Layout").addTo(map);

    //L.easyButton('fa fa-print', function () {

    //    if ($scope.floorDetails != null) {
    //        $scope.MarkerLblLayer.clearLayers();
    //        $scope.MarkerMeetingLblLayer.clearLayers();
    //        $scope.MarkerLblLayerEmpDetails.clearLayers();
    //        $scope.LoadMarkers2($scope.floorDetails);
    //        setTimeout(function () { window.print() }, 3600);
    //        //printWithoutHeaderfooter('leafletMap')                
    //    }


    //    if ($scope.floorDetails != null) {
    //        setTimeout(function () {
    //            $scope.MarkerLblLayer.clearLayers();
    //            $scope.MarkerMeetingLblLayer.clearLayers();
    //            $scope.MarkerLblLayerEmpDetails.clearLayers();
    //            //$scope.MarkerLblLayer._layers = angular.copy($scope.MarkerLblLayer_copy._layers);
    //            angular.forEach($scope.MarkerLblLayer_copy._layers, function (value, index) {
    //                $scope.MarkerLblLayer.addLayer(value);
    //            });
    //            //$scope.MarkerMeetingLblLayer._layers = angular.copy($scope.MarkerMeetingLblLayer_copy._layers);
    //            angular.forEach($scope.MarkerMeetingLblLayer_copy._layers, function (value, index) {
    //                $scope.MarkerMeetingLblLayer.addLayer(value);
    //            });
    //            //$scope.MarkerLblLayerEmpDetails._layers = angular.copy($scope.MarkerLblLayerEmpDetails_copy._layers);
    //            angular.forEach($scope.MarkerLblLayerEmpDetails_copy._layers, function (value, index) {
    //                $scope.MarkerLblLayerEmpDetails.addLayer(value);
    //            });
    //            //$scope.LoadMarkers($scope.floorDetails)
    //        }, 4000);
    //    }



    //}, "Print Only Layout").addTo(map);


    $scope.columDefsAlloc = [
        { headerName: "Select", field: "ticked", width: 60, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-disabled='data.disabled' ng-model='data.ticked' ng-change='chkChanged(data)'>", pinned: 'left' }, // headerCellRenderer: headerCellRendererFunc, 
        { headerName: "Space ID", field: "SPC_NAME", width: 150, cellClass: "grid-align", pinned: 'left' },
        { headerName: "Space Type", field: "SPC_TYPE_NAME", width: 110, cellClass: "grid-align" },
        { headerName: "Space Sub Type", field: "SST_NAME", width: 110, cellClass: "grid-align" },
        { headerName: "Ver", width: 110, cellClass: "grid-align", field: "VER_NAME", cellRenderer: customEditor },
        { headerName: "Cost", width: 110, cellClass: "grid-align", field: "Cost_Center_Name", cellRenderer: customEditor },
        { headerName: "From Date", field: "FROM_DATE", width: 120, cellClass: 'grid-align'},
        { headerName: "To Date", field: "TO_DATE", width: 120, cellClass: 'grid-align'},
        //{ headerName: "Shift Type", field: "SH_CODE", width: 110, cellClass: "grid-align", filter: 'set', template: "<select data-ng-change='shiftChange(data)' ng-model='data.SH_CODE'><option value=''>--Select--</option><option ng-repeat='Shift in Shifts' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>" },
        { headerName: "Blocking", field: "blocked", width: 60, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-disabled='data.blocked' ng-model='data.blocked' ng-change='blkChanged(data)'>" }, // headerCellRenderer: headerCellRendererFunc, 
        { headerName: "Employee", cellClass: "grid-align", width: 110, filter: 'set', field: "AUR_NAME", cellRenderer: customEditor },
        {
            headerName: "", width: 80, cellClass: "grid-align", filter: 'set',
            template: "<a href='' data-ng-click ='AllocAnother(data)'>Allocate Other</a>"
        }
    ];

    $scope.chkChanged = function (data) {

    }

    $scope.columDefSpaceAlloc = [
        { headerName: "Select", field: "ticked", width: 60, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-model='data.ticked' ng-change='ChkSpaceChanged(data)'>", pinned: 'left' }, // headerCellRenderer: headerCellRendererFunc, 
        { headerName: "Space ID", field: "SPACE_ID", width: 350, cellClass: "grid-align", pinned: 'left' },
        { headerName: "Space Type", field: "SPC_TYPE", width: 250, cellClass: "grid-align" },
        { headerName: "Space Sub Type", field: "SPC_SUB_TYPE", width: 239, cellClass: "grid-align" }];
    $scope.selectedActSpaces = [];

    $scope.ChkSpaceChanged = function (data) {
        $scope.selectedActSpaces.push(data);
    }

    $scope.GetInactiveSpaces = function () {
        if ($scope.selectedSpaces.length > 0) {
            $scope.selectedActSpaces = [];
            $('#Active').modal('show');
            angular.forEach($scope.selectedSpaces, function (value, key) {
                var spcdet = {};
                if (value.ticked == true) {
                    spcdet.SPACE_ID = value.SPC_ID;
                    spcdet.SPC_TYPE = value.SPC_TYPE_NAME;
                    spcdet.SPC_SUB_TYPE = value.SST_NAME;
                    spcdet.ticked = value.ticked;
                    $scope.selectedActSpaces.push(spcdet);
                }
            });
            setTimeout(function () {
                $scope.gridSpaceAllocation.api.setRowData([]);
                $scope.gridSpaceAllocation.api.setRowData($scope.selectedActSpaces);
                progress(0, '', false);
            }, 200);
        }
        else {
            var params = {
                flr_code: $scope.SearchSpace.Floor[0].FLR_CODE
            };
            MaploaderService.GetSpaceFromFloorMaps(params).then(function (data) {
                if (data.length != 0) {
                    $scope.gridata = data;
                    $scope.gridSpaceAllocation.api.setRowData([]);
                    $scope.gridSpaceAllocation.api.setRowData($scope.gridata);
                    progress(0, '', false);
                    $('#Active').modal('show');
                }
                else {
                    progress(0, '', false);
                    $('#Active').modal('hide');
                    showNotification('error', 8, 'bottom-right', 'No Inactive Spaces Found For The Selected Floor');
                }
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.ActivateSpaces = function () {
        var flrcode = $scope.SearchSpace.Floor[0].FLR_CODE;
        var dataobj = { FLR_CODE: flrcode, SPC_LST: $scope.selectedActSpaces };
        MaploaderService.ActivateSpaces(dataobj).then(function (response) {
            if (response != null) {
                $('#Active').modal('hide');
                setTimeout(function () {
                    $scope.LoadMarkers($scope.SearchSpace.Floor[0].FLR_CODE);
                    $scope.loadThemes();
                }, 500);
                showNotification('success', 8, 'bottom-right', 'Selected Spaces Has Been Active');
            }
        });

    };

    $scope.InactiveSpaces = function () {
        var flrcode = $scope.SearchSpace.Floor[0].FLR_CODE;

        var dataobj = { FLR_CODE: flrcode, SPC_LST: $scope.selectedActSpaces };
        MaploaderService.InactiveSeats(dataobj).then(function (response) {
            if (response != null) {
                angular.forEach($scope.selectedActSpaces, function (value, key) {
                    _.remove($scope.Markers, { SPC_ID: value.SPACE_ID });
                });
                reloadMarkers();
                $scope.selectedActSpaces = [];
                $scope.selectedSpaces = [];
                $('#Active').modal('hide');
                showNotification('success', 8, 'bottom-right', 'Selected Spaces Has Been Inactive');
                setTimeout(function () {
                    $scope.ShowTotalOnMap();
                    $scope.loadThemes();
                });
            }
        });
    };

    function customEditor(params) {
        var editing = true;

        var eCell = document.createElement('span');

        var eLabel = document.createTextNode('Please Select');
        if (params.value)
            eLabel.nodeValue = params.value;
        eCell.appendChild(eLabel);

        var eSelect = document.createElement("select");
        eSelect.setAttribute("width", 50);
        eCell.addEventListener('click', function () {
            if (!editing) {
                progress(0, 'Loading...', true);
                if (params.column.colId === "VER_NAME") {
                    removeOptions(eSelect);
                    var eOption = document.createElement("option");
                    eSelect.appendChild(eOption);
                    angular.forEach($scope.Verticals, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.VER_CODE);
                        eOption.setAttribute("text", item.VER_NAME);
                        eOption.innerHTML = item.VER_NAME;
                        eSelect.appendChild(eOption);
                    });
                    progress(0, '', false);
                }
                else if (params.column.colId === "Cost_Center_Name") {
                    $scope.getCostcenterByVertical(params.data, eSelect);
                }
                else {
                    if (params.data.Cost_Center_Code != "")
                        $scope.cstChange(params.data, eSelect);
                }
                setTimeout(function () {
                    eSelect.value = params.value;
                    eCell.removeChild(eLabel);
                    eCell.appendChild(eSelect);
                    eSelect.focus();
                    editing = true;
                }, 200);
            }
        });

        eSelect.addEventListener('blur', function () {
            if (editing) {
                editing = false;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
            }
        });
        eSelect.addEventListener('change', function () {
            if (editing) {
                editing = false;

                var newValue = eSelect.options[eSelect.selectedIndex].text;
                eLabel.nodeValue = newValue;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
                if (params.column.colId === "VER_NAME") {
                    params.data.VERTICAL = eSelect.value;
                    params.data.VER_NAME = newValue;
                    $scope.getCostcenterByVertical(params.data, eSelect);
                }
                else if (params.column.colId === "Cost_Center_Name") {
                    params.data.Cost_Center_Code = eSelect.value;
                    params.data.Cost_Center_Name = newValue;
                    $scope.cstChange(params.data, eSelect);
                }
                else {
                    params.data.AUR_ID = eSelect.value;
                    params.data.AUR_NAME = newValue;
                    $scope.EmpChange(params.data, params.data.AUR_ID);
                }
            }
        });
        return eCell;
    }

    UtilityService.getVerticals(3).then(function (response) {
        if (response.data != null) {
            $scope.Verticals = response.data;
        }
    });


    //UtilityService.getSysPreferences().then(function (response) {

    //    if (response.data != null) {
    //        $scope.SysPref = response.data;
    //    }
    //});

    MaploaderService.getallFilterbyItem().then(function (response) {

        if (response != null) {
            $scope.Items = response;
        }
    });

    $scope.getCostcenterByVertical = function (data, eSelect) {
        $scope.CostCenters = [];
        data.STATUS = 1002;
        if (data.STACHECK != UtilityService.Added)
            data.STACHECK = UtilityService.Modified;
        else {
            data.SSA_SRNREQ_ID = null;
            data.SSAD_SRN_REQ_ID = null;
        }
        ///// removing options from select
        removeOptions(eSelect);
        var eOption = document.createElement("option");
        eSelect.appendChild(eOption);
        UtilityService.getCostcenterByVerticalcode({ VER_CODE: data.VERTICAL }, 2).then(function (response) {
            // $timeout(function () {
            $scope.CostCenters = response.data;


            angular.forEach(response.data, function (item, key) {
                var eOption = document.createElement("option");
                eOption.setAttribute("value", item.Cost_Center_Code);
                eOption.setAttribute("text", item.Cost_Center_Name);
                eOption.innerHTML = item.Cost_Center_Name;
                eSelect.appendChild(eOption);
            });
            progress(0, '', false);
            //  }, 500);
        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }

    $scope.EmpChange = function (data, OldVal) {
        $scope.$apply(function () {
            $scope.OnEmpAllocSeats = true;
        });
        //$scope.SelectedEmp.push({ AUR_ID: OldVal });
        var valueArr = $scope.gridSpaceAllocOptions.rowData.map(function (item) { return item.AUR_ID });

        var isDuplicate = valueArr.some(function (item, idx) {
            if (item != '')
                return valueArr.indexOf(item) != idx
        });

        if (isDuplicate == true) {
            showNotification('error', 8, 'bottom-right', 'Employee Should Not be Same');
        }
        else {
            $scope.SelectedEmp = data;
            data.STATUS = 1004;
            if (data.STACHECK != UtilityService.Added)
                data.STACHECK = UtilityService.Modified;
            for (var i = 0; $scope.EmpDetails != null && i < $scope.EmpDetails.length; i += 1) {
                if ($scope.EmpDetails[i].AUR_ID === data.AUR_ID) {
                    $scope.EmpDetails[i].ticked = true;
                    data.ticked = true;
                }
            }
            var dataobj = { lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: data.AUR_ID, mode: 1 };
            MaploaderService.getEmpAllocSeat(dataobj).then(function (result) {
                if (result.data.length > 0) {
                    $scope.EmpAllocSeats.push(result.data[0]);
                }
                $scope.EmpAllocSeats = _.remove($scope.EmpAllocSeats, function (n) {
                    return _.find($scope.gridSpaceAllocOptions.rowData, { AUR_ID: n.AUR_ID });
                });
                $scope.OnEmpAllocSeats = false;
                $scope.EmpAllocSeats = _.uniqBy($scope.EmpAllocSeats, 'AUR_ID');
            });
        }
    }

    $scope.blkChanged = function (data) {
        data.STATUS = 1052;
        if (data.STACHECK != UtilityService.Added)
            data.STACHECK = UtilityService.Modified;
    }

    function removeOptions(selectbox) {
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }

    $scope.cstChange = function (data, eSelect) {
        data.STATUS = 1003;
        if (data.STACHECK != UtilityService.Added)
            data.STACHECK = UtilityService.Modified;
        data.AUR_ID = "";
        data.AUR_NAME = "";
        ///// removing options from select
        removeOptions(eSelect);
        var eOption = document.createElement("option");
        eOption.setAttribute("text", "Please select");
        eSelect.appendChild(eOption);

        var dataobj = {
            lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE,
            Item: data.VERTICAL, subitem: data.Cost_Center_Code
        };
        MaploaderService.getEmpDetails(dataobj).then(function (response) {
            if (response.data != null) {
                if (response.data.length != 0) {
                    $scope.EmpDetails = response.data;
                    angular.forEach($scope.EmpDetails, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.AUR_ID);
                        eOption.setAttribute("text", item.AUR_NAME);
                        eOption.innerHTML = item.AUR_NAME;
                        eSelect.appendChild(eOption);
                        if (data.AUR_ID === item.AUR_ID) {
                            eSelect.value = data.AUR_ID;
                            data.ticked = true;
                            item.ticked = true;
                        }
                        if (_.find($scope.Markers, { AUR_ID: item.AUR_ID }))
                            eOption.disabled = true;
                        progress(0, '', false);
                    });
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'No Employee Found');
                }
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message + $scope.BsmDet.Child);
            }

        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }
    $scope.checkshiftvalue = 0;
    //$scope.shiftChange = function (data) {

    //    if (data.STATUS != 1 && (data.SSA_SRNREQ_ID != "" || data.FROM_DATE != "")) {
    //        MaploaderService.spcAvailabilityByShift(data).then(function (response) {
    //            if (response.data == null) {
    //                console.log(data);
    //                //$scope.checkshiftvalue = 1;
    //                //$('#allocatespacebtn').prop("disabled", true);                    
    //                showNotification('error', 8, 'bottom-right', response.Message);
    //            }
    //            else {
    //                $('#allocatespacebtn').prop("disabled", false);
    //            }
    //        }, function (error) {
    //            console.log(error);
    //        });
    //    }
    //}

    $scope.FilterSpaces = function () {
        progress(0, 'Loading...', true);
        $scope.LoadMap($scope.SearchSpace.Floor[0].FLR_CODE);
        updateSummaryCount();
        $('#divFilter').toggle("slow");
        setTimeout(function () {
            $('#accordion').toggle("slow");
        }, 100)
        //$('#divFilter').toggle("slow");
        //$('#accordion').toggle("slow");
    }

    function createFromDatePicker(params) {
        var editing = true;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.FROM_DATE');
        newDate.type = "text";
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        $(newDate).datepicker({
            //format: 'dd M, yyyy',
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
        }).on("input change", function (e) {
            prefvalue = _.find($scope.SysPref, { SYSP_CODE: "Restrict Allocation to 90 Days" });
            if (prefvalue != undefined) {
                var checkdate = moment(e.target.value).add(90, 'day').format('MM/DD/YYYY');
                params.data.TO_DATE = checkdate;
                $scope.datedisable = true;
            }
        });
        return newDate;
    }

    function createToDatePicker(params) {
        var editing = true;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.TO_DATE');
        newDate.setAttribute('ng-disabled', 'datedisable==true');
        newDate.type = "text";
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        prefvalue = _.find($scope.SysPref, { SYSP_CODE: "Restrict Allocation to 90 Days" });
        if (prefvalue != undefined) {

            $scope.datedisable = true;
        }
        $(newDate).datepicker({
            //format: 'dd M, yyyy',
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,

        })
        return newDate;
    }

    $scope.gridSpaceAllocOptions = {
        columnDefs: $scope.columDefsAlloc,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
    };

    $scope.gridSpaceAllocation = {
        columnDefs: $scope.columDefSpaceAlloc,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
    };
    //Showing Vacant Seats On Map When Click On Seat Status - Vacant
    $scope.ShowVacantOnMap = function () {
        var vacspaces = [];
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            if (marker.STATUS == 1) {
                marker.setStyle(VacantStyle);
                vacspaces.push(marker);
            }
            progress(0, '', false);
        });
        $.each($scope.drawnItems._layers, function (Key, layer) {
            var spcid = _.find(vacspaces, { SPC_ID: layer.options.spaceid });
            if (spcid == undefined)
                layer.setStyle({ fillColor: "#E8E8E8" });
            else {
                layer.setStyle({ fillColor: "#228B22", opacity: 0.65 });
            }
            progress(0, '', false);
        });
    };

    //Total Seats On Map
    $scope.ShowTotalOnMap = function () {
        //$scope.LoadMarkers($scope.SearchSpace.Floor[0].FLR_CODE);
        $scope.Markerdata.clearLayers();
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            switch (marker.STATUS) {
                case 1: marker.setStyle(VacantStyle);
                    break;
                case 1002: marker.setStyle(AllocateStyle);
                    break;
                case 1004: marker.setStyle(OccupiedStyle);
                    break;
                case 1003: marker.setStyle(AllocateStyle);
                    break;
                case 1052: marker.setStyle(BlockStyle);
                    break;
                default: marker.setStyle(VacantStyle);
                    break;
            }
            marker.bindLabel(marker.SPC_DESC, {
                noHide: false,
                direction: 'auto',
                zIndexOffset: 2000
            });//.setOpacity(1);
            $scope.Markerdata.addLayer(marker);
        });
        $scope.loadThemes();
    };

    //Occupied To Employee on Map
    $scope.ShowOccupiedOnMap = function () {
        var occspaces = [];
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            if (marker.STATUS == 1004) {
                marker.setStyle(OccupiedStyle);
                occspaces.push(marker);
            }
            progress(0, '', false);
        });
        $.each($scope.drawnItems._layers, function (Key, layer) {
            var spcid = _.find(occspaces, { SPC_ID: layer.options.spaceid });
            if (spcid == undefined)
                layer.setStyle({ fillColor: "#E8E8E8" });
            else {
                layer.setStyle({ fillColor: "#E74C3C", opacity: 0.65 });
            }
            progress(0, '', false);
        });
    }

    //Vertical Costcenter On Map
    $scope.ShowCostCentre = function () {
        var costcentre = [];
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            if (marker.STATUS == 1003) {
                marker.setStyle(CstVer);
                costcentre.push(marker);
            }
            else if (marker.STATUS == 1002) {
                marker.setStyle(CstVer);
                costcentre.push(marker);
            }
            progress(0, '', false);
        });
        $.each($scope.drawnItems._layers, function (Key, layer) {
            var spcid = _.find(costcentre, { SPC_ID: layer.options.spaceid });
            if (spcid == undefined)
                layer.setStyle({ fillColor: "#E8E8E8" });
            else {
                layer.setStyle({ fillColor: "#3ccae7", opacity: 0.65 });
            }
            progress(0, '', false);
        });
    }


    $('#SeatAllocation').on('shown.bs.modal', function () {

        $scope.relfrom = {};
        var role = _.find($scope.GetRoleAndRM);
        var emp = _.find($scope.Employee);
        if (role.AUR_ROLE == 6) {
            angular.forEach($scope.selectedSpaces, function (value, key) {
                value.ticked = false;
                value.disabled = true;
                if (value.AUR_ID == emp.AUR_ID || value.AUR_ID == "") {
                    value.ticked = true;
                    value.disabled = false;
                }
            });
            var count = $scope.selectedSpaces.length;
            $scope.selectedSpaces.count = count;
            $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
            $scope.gridSpaceAllocOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridSpaceAllocOptions.columnApi.getColumn("Cost_Center_Name").colDef.headerName = $scope.BsmDet.Child;
            //$scope.gridSpaceAllocOptions.api.refreshView();
            $scope.gridSpaceAllocOptions.api.refreshHeader();
        }
        else {
            $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
            $scope.gridSpaceAllocOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridSpaceAllocOptions.columnApi.getColumn("Cost_Center_Name").colDef.headerName = $scope.BsmDet.Child;
            //$scope.gridSpaceAllocOptions.api.refreshView();
            $scope.gridSpaceAllocOptions.api.refreshHeader();
        }

    });

    $scope.ReleaseEmp = function (Relemp) {
        $scope.SelRelEmp = [];
        $scope.SelRelEmp.push(Relemp);
       
        
         var dataobj = { sad: $scope.SelRelEmp, reltype: 1002 };
     
        _.remove($scope.EmpAllocSeats, function (sel) {
            return sel.SSAD_SRN_REQ_ID === Relemp.SSAD_SRN_REQ_ID
        });
        MaploaderService.release(dataobj).then(function (response) {
            $scope.releasecount++;
            var result = $filter('filter')($scope.SelRelEmp, { SPC_FLR_ID: $scope.SearchSpace.Floor[0].FLR_CODE });
            for (var i = $scope.SelRelEmp.length - 1; i >= 0; i--) {
                if ($scope.SearchSpace.Floor[0].FLR_CODE == $scope.SelRelEmp[i].FLR_CODE) {
                    var spcid = _.find($scope.Markers, { SPC_ID: $scope.SelRelEmp[i].SPC_ID });
                    spcid.STATUS = 1;
                    spcid.SSA_SRNREQ_ID = "";
                    spcid.setStyle(VacantStyle);
                    spcid.bindLabel(spcid.SPC_NAME + " :Vacant", {
                        noHide: false,
                        direction: 'auto',
                        zIndexOffset: 2000
                    });//.setOpacity(1);
                    spcid.SPC_DESC = spcid.SPC_NAME + " :Vacant";
                    spcid.STACHECK = UtilityService.Deleted;
                }
                $('#SeatAllocation').modal('hide');
                $scope.selectedSpaces = [];
                showNotification('success', 8, 'bottom-right', "Seat has been released successfully");
                reloadMarkers();
            }
        });
    }

    $scope.Proceedalloc = function () {
        var prefval = _.find($scope.SysPref, { SYSP_CODE: "Duplicate allocation" })
        if ((prefval == undefined) || prefval.SYSP_VAL1 == undefined) {
            $scope.EmpAllocSeats = []
            progress(0, '', false);
        }
        else if (prefval.SYSP_VAL1 = 1042 && ($scope.EmpAllocSeats.length >= 1)) {
            showNotification('error', 8, 'bottom-right', 'Please Release Employee');
        }

    }


    $scope.Release = function (reltypecode, reltypename) {
        progress(0, 'Loading...', true);
        $scope.relfrom = {};
        $scope.relfrom.Name = reltypename;
        $scope.relfrom.Value = reltypecode;
        var dataobj = { sad: $filter('filter')($scope.selectedSpaces, function (x) { if (x.ticked == true) return x; }), reltype: reltypecode };


        if (dataobj.sad.length != 0) {
            MaploaderService.release(dataobj).then(function (response) {
                if (response.data != null) {
                    angular.forEach($scope.selectedSpaces, function (value, key) {
                        if (value.ticked == true) {
                            var spcid = _.find($scope.Markers, { SPC_ID: value.SPC_ID });
                            //var spclst = _.filter($scope.selectedSpaces, function (space) { if (space.SPC_ID == value.SPC_ID && space != value) return space });
                            //var length = 0;
                            //if (spclst != undefined)
                            //    length = spclst.length;
                            //if (length > 0)
                            //    updateSpcDetails(spclst, spcid);
                            //else {
                            if (dataobj.sad.length != $scope.selectedSpaces.length) {
                                var emp = _.find($scope.selectedSpaces, { ticked: false })
                                value.AUR_ID = emp.AUR_ID;
                                value.STACHECK = UtilityService.Deleted;
                                spcid.STATUS = 1004;
                                spcid.setStyle(OccupiedStyle);
                                spcid.ticked = false;
                                spcid.bindLabel(spcid.SPC_NAME + " :Occupied by " + emp.AUR_ID, {
                                    noHide: false,
                                    direction: 'auto',
                                    zIndexOffset: 2000
                                });//.setOpacity(1);
                                spcid.SPC_DESC = spcid.SPC_NAME + " :Occupied by " + emp.AUR_ID;
                                spcid.STACHECK = UtilityService.Deleted;
                            }
                            else {

                                switch ($scope.relfrom.Value) {
                                    case 1004:
                                        value.AUR_ID = "";
                                        value.STACHECK = UtilityService.Deleted;
                                        spcid.STATUS = 1003;
                                        spcid.setStyle(AllocateStyle);
                                        spcid.ticked = false;
                                        spcid.bindLabel(spcid.SPC_NAME + " : Allocated to " + value.Cost_Center_Name, {
                                            noHide: false,
                                            direction: 'auto',
                                            zIndexOffset: 2000
                                        });//.setOpacity(1);
                                        spcid.SPC_DESC = spcid.SPC_NAME + " : Allocated to " + value.Cost_Center_Name;
                                        spcid.STACHECK = UtilityService.Deleted;
                                        break;
                                    case 1003:
                                        value.Cost_Center_Code = "";
                                        value.SH_CODE = "";
                                        value.AUR_ID = "";
                                        value.STACHECK = UtilityService.Deleted;
                                        spcid.STATUS = 1002;
                                        spcid.ticked = false;
                                        spcid.setStyle(AllocateStyle);
                                        spcid.bindLabel(spcid.SPC_NAME + " : Allocated to " + value.VERTICAL, {
                                            noHide: false,
                                            direction: 'auto',
                                            zIndexOffset: 2000
                                        });//.setOpacity(1);
                                        spcid.SPC_DESC = spcid.SPC_NAME + " : Allocated to " + value.VERTICAL;
                                        spcid.STACHECK = UtilityService.Deleted;
                                        spcid.STATUS = 1002;
                                        break;
                                    case 1002:
                                        value.VERTICAL = "";
                                        value.Cost_Center_Code = "";
                                        value.SH_CODE = "";
                                        value.AUR_ID = "";
                                        value.FROM_DATE = "";
                                        value.TO_DATE = "";
                                        value.STATUS = 1;
                                        value.SSA_SRNREQ_ID = "";
                                        spcid.setStyle(VacantStyle);
                                        spcid.ticked = false;
                                        spcid.STATUS = 1;
                                        spcid.bindLabel(value.SPC_NAME + " : Vacant", {
                                            noHide: false,
                                            direction: 'auto',
                                            zIndexOffset: 2000
                                        });//.setOpacity(1);
                                        spcid.SSA_SRNREQ_ID = "";
                                        spcid.VERTICAL = "";
                                        spcid.Cost_Center_Code = "";
                                        spcid.SH_CODE = "";
                                        spcid.AUR_ID = "";
                                        spcid.FROM_DATE = "";
                                        spcid.TO_DATE = "";
                                        spcid.STATUS = 1;
                                        spcid.SSA_SRNREQ_ID = "";
                                        spcid.SPC_DESC = value.SPC_NAME + " : Vacant";
                                        spcid.STACHECK = UtilityService.Deleted;
                                        break;
                                    case 1052:
                                        value.VERTICAL = "";
                                        value.Cost_Center_Code = "";
                                        value.SH_CODE = "";
                                        value.AUR_ID = "";
                                        value.FROM_DATE = "";
                                        value.TO_DATE = "";
                                        value.STATUS = 1;
                                        value.SSA_SRNREQ_ID = "";
                                        spcid.setStyle(VacantStyle);
                                        spcid.STATUS = 1;
                                        spcid.ticked = false;
                                        spcid.bindLabel(value.SPC_NAME + " : Vacant", {
                                            noHide: false,
                                            direction: 'auto',
                                            zIndexOffset: 2000
                                        });//.setOpacity(1);
                                        spcid.SSA_SRNREQ_ID = "";
                                        spcid.VERTICAL = "";
                                        spcid.Cost_Center_Code = "";
                                        spcid.SH_CODE = "";
                                        spcid.AUR_ID = "";
                                        spcid.FROM_DATE = "";
                                        spcid.TO_DATE = "";
                                        spcid.STATUS = 1;
                                        spcid.SSA_SRNREQ_ID = "";
                                        spcid.SPC_DESC = value.SPC_NAME + " : Vacant";
                                        spcid.STACHECK = UtilityService.Deleted;
                                        break;

                                }
                                //}
                            }
                        }
                    });
                    setTimeout(function () {
                        $('#SeatAllocation').modal('hide');
                        $scope.selectedSpaces = [];
                        updateSummaryCount();
                        progress(0, '', false);
                        $scope.gridSpaceAllocOptions.api.refreshView();
                        showNotification('success', 8, 'bottom-right', "Selected Spaces Released From " + $scope.relfrom.Name);
                    }, 300);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                }
            }, function (error) {
            });
        }

        else {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "employee cannot release others seat");
        }

    }

    function updateSpcDetails(spclst, spcid) {
        var labelval = "";
        switch ($scope.relfrom.Value) {
            case 1002: spcid.setStyle(VacantStyle);
                spcid.STATUS = 1;
                labelval = spcid.SPC_NAME + " : Vacant";
                spcid.VERTICAL = "";
                spcid.Cost_Center_Name = "";
                spcid.SSA_SRNREQ_ID = "";
                spcid.SSAD_SRN_REQ_ID = "";
                break;
            case 1004: spcid.setStyle(AllocateStyle);
                labelval = spcid.SPC_NAME + " : Allocated to " + spcid.Cost_Center_Name;
                spcid.STATUS = 1003;
                break;
            case 1003: spcid.setStyle(AllocateStyle);
                labelval = spcid.SPC_NAME + " : Allocated to " + spcid.VER_NAME;
                spcid.STATUS = 1002;
                spcid.Cost_Center_Name = "";
                break;
            case 1052: spcid.setStyle(VacantStyle);
                labelval = spcid.SPC_NAME + " : Vacant";
                spcid.VERTICAL = "";
                spcid.Cost_Center_Name = "";
                spcid.SSA_SRNREQ_ID = "";
                spcid.SSAD_SRN_REQ_ID = "";
                break;
            default: spcid.setStyle(VacantStyle);
                break;
        }
        spcid.bindLabel(labelval, {
            noHide: false,
            direction: 'auto',
            zIndexOffset: 2000
        });//.setOpacity(1);
        spcid.SPC_DESC = labelval;
        spcid.SSA_SRNREQ_ID = "";
        spcid.STACHECK = UtilityService.Deleted;
        spcid.ticked = false;

    }

    $scope.AllocateSeats = function () {
        progress(0, 'Loading...', true);
        var count = 0;
        var flag = false;
        $scope.FinalSelectedSpaces = [];
        var data = $scope.Shifts.filter(function (item) {
            return !_.find($scope.gridSpaceAllocOptions.rowData, { SPACE_TYPE: item.SH_SEAT_TYPE });
        });

        var check = $scope.gridSpaceAllocOptions.rowData.filter(function (item) {
            return _.find(data, { SH_CODE: item.SH_CODE });
        });
        var valueArr = $scope.gridSpaceAllocOptions.rowData.map(function (item) { return item.AUR_ID });
        var isDuplicate = valueArr.some(function (item, idx) {
            if (item != '')
                return valueArr.indexOf(item) != idx
        });


        angular.forEach($scope.selectedSpaces, function (value, key) {

            if (value.ticked == true) {
                if (value.VERTICAL == undefined || value.VERTICAL == "") {
                    progress(0, '', false);
                    flag = true;
                    showNotification('error', 8, 'bottom-right', 'Please Select ' + $scope.BsmDet.Parent);
                    return;
                }
                else if (value.FROM_DATE == undefined) {
                    progress(0, '', false);
                    flag = true;
                    showNotification('error', 8, 'bottom-right', 'Please Select From Date');
                    return;
                }
                else if (value.TO_DATE == undefined) {
                    progress(0, '', false);
                    flag = true;
                    showNotification('error', 8, 'bottom-right', 'Please Select To Date');
                    return;
                }
                //else if (value.SH_CODE == undefined || value.SH_CODE == '' || value.SH_NAME == '') {
                //    progress(0, '', false);
                //    flag = true;
                //    showNotification('error', 8, 'bottom-right', 'Please Select Shift');
                //    return;
                //}

                else {
                    $scope.FinalSelectedSpaces.push(value);
                    count = count + 1;
                }
            }
        });
        if (flag) {
            return;
        }
        if (count == 0) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', 'Please select atleast employee for allocate');
            return;
        }
        //if (check.length > 0) {
        //    progress(0, '', false);
        //    flag = true;
        //    showNotification('error', 8, 'bottom-right', "Please Select Valid Shift");
        //    return;
        //}
        if (isDuplicate == true) {
            progress(0, '', false);
            flag = true;
            showNotification('error', 8, 'bottom-right', 'Employee Should Not be Same');
            return;
        }

        MaploaderService.allocateSeats($scope.FinalSelectedSpaces).then(function (response) {
            if (response.data != null) {
              
                angular.forEach(response.data, function (value, key) {
                    var spcid = _.find($scope.Markers, { SPC_ID: value.SPC_ID });
                    spcid.STATUS = value.STATUS;
                    spcid.VERTICAL = value.VERTICAL;
                    spcid.Cost_Center_Name = value.Cost_Center_Name;
                    spcid.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                    spcid.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                    spcid.AUR_ID = value.AUR_ID;
                    spcid.ticked = false;
                    spcid.SHIFT_TYPE = value.SHIFT_TYPE;
                    var labelval = "";
                    switch (value.STATUS) {
                        case 1: spcid.setStyle(VacantStyle);
                            labelval = value.SPC_DESC;
                            break;
                        case 1002: spcid.setStyle(AllocateStyle);
                            labelval = value.SPC_DESC;
                            break;
                        case 1004: spcid.setStyle(OccupiedStyle);
                            labelval = value.SPC_DESC;
                            break;
                        case 1003: spcid.setStyle(AllocateStyle);
                            labelval = value.SPC_DESC;
                            break;
                        case 1052: spcid.setStyle(BlockStyle);
                            labelval = value.SPC_DESC;
                            break;
                        default: spcid.setStyle(VacantStyle);
                            break;
                    }
                    spcid.bindLabel(labelval, {
                        noHide: false,
                        direction: 'auto',
                        zIndexOffset: 2000
                    });//.setOpacity(1);
                    spcid.SPC_DESC = labelval;

                });
                $('#SeatAllocation').modal('hide');
                $scope.selectedSpaces = [];
                updateSummaryCount();
                $scope.gridSpaceAllocOptions.api.refreshView();
                progress(0, '', false);
                showNotification('success', 8, 'bottom-right', 'Selected Spaces Allocated Successfully');
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        });

    }
    $scope.AllocAnother = function (data) {
        console.log($scope.selectedSpaces)
        //if (data.SPACE_TYPE == 1) {
        //    showNotification('error', 8, 'bottom-right', "Please select maximum of 1 person for dedicated");
        //}
        // else {

        var prefval = _.find($scope.SysPref, { SYSP_CODE: "Duplicate allocation" });
        if ((prefval == undefined) || prefval.SYSP_VAL1 == undefined) {

            var refdata = {};
            angular.copy(data, refdata);

            var prefval = _.find($scope.SysPref, { SYSP_CODE: "OVERLOAD" });
            if (prefval.SYSP_VAL1 >= $filter('filter')($scope.selectedSpaces, { SPC_ID: refdata.SPC_ID }).length + 1) {
                if (refdata.AUR_LONG_LEAVE_FROM_DT != '' && refdata.AUR_LONG_LEAVE_TO_DT != '' && refdata.AUR_LONG_LEAVE_FROM_DT != null && refdata.AUR_LONG_LEAVE_TO_DT != null) {
                    refdata.FROM_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_FROM_DT), 'MM/dd/yyyy');
                    refdata.TO_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_TO_DT), 'MM/dd/yyyy');
                }
                var role = _.find($scope.GetRoleAndRM);
                if (role.AUR_ROLE == 6) {
                    var emp = _.find($scope.selectedSpaces, function (x) { if (x.AUR_ID == role.AUR_ID) return x });
                    if (emp != undefined) {
                        showNotification('error', 8, 'bottom-right', 'Employee can not allocate for another');
                        return;
                    }
                    //var ver = _.find($scope.selectedSpaces, function (x) { if (x.VERTICAL == role.VER_CODE) return x });
                    //if (ver == undefined) {
                    //    showNotification('error', 8, 'bottom-right', 'you can not allocate for different verticals in single seat');
                    //    return;
                    //}
                    refdata.AUR_LEAVE_STATUS = "N";
                    refdata.SSAD_SRN_REQ_ID = null;
                    refdata.STACHECK = 16;
                    refdata.AUR_NAME = role.AUR_KNOWN_AS;
                    refdata.AUR_ID = role.AUR_ID;
                    refdata.VER_CODE = role.VER_CODE;
                    refdata.VER_NAME = role.VER_NAME;
                    refdata.Cost_Center_Code = role.COST_CENTER_CODE;
                    refdata.Cost_Center_Name = role.COST_CENTER_NAME;
                    $scope.selectedSpaces.push(refdata);
                    $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
                    var emp = _.find($scope.Employee);
                    angular.forEach($scope.selectedSpaces, function (value, key) {
                        value.ticked = false;
                        value.disabled = true;
                        if (value.AUR_ID == emp.AUR_ID || value.AUR_ID == "") {
                            value.ticked = true;
                            value.disabled = false;
                        }
                    });
                    var dataobj = { lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: role.AUR_ID, mode: 1 };
                    MaploaderService.getEmpAllocSeat(dataobj).then(function (result) {
                        if (result.data) {
                            $scope.EmpAllocSeats = result.data;
                            progress(0, '', false);
                        }
                    });
                }
                else {
                    refdata.AUR_LEAVE_STATUS = "N";
                    refdata.SSAD_SRN_REQ_ID = null;
                    refdata.STACHECK = 16;
                    refdata.AUR_NAME = null;
                    refdata.AUR_ID = null;
                    $scope.selectedSpaces.push(refdata);
                    $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
                }

            }
            else
                showNotification('error', 8, 'bottom-right', "Please select maximum of " + prefval.SYSP_VAL1 + " Spaces");

        }
        else if (prefval.SYSP_VAL1 = 1042 && ($scope.EmpAllocSeats.length >= 1)) {


            showNotification('error', 8, 'bottom-right', 'first relese employee');

        }

        else if (prefval.SYSP_VAL1 = 1042) {


            var refdata = {};
            angular.copy(data, refdata);

            var prefval = _.find($scope.SysPref, { SYSP_CODE: "OVERLOAD" });
            if (prefval.SYSP_VAL1 >= $filter('filter')($scope.selectedSpaces, { SPC_ID: refdata.SPC_ID }).length + 1) {
                if (refdata.AUR_LONG_LEAVE_FROM_DT != '' && refdata.AUR_LONG_LEAVE_TO_DT != '' && refdata.AUR_LONG_LEAVE_FROM_DT != null && refdata.AUR_LONG_LEAVE_TO_DT != null) {
                    refdata.FROM_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_FROM_DT), 'MM/dd/yyyy');
                    refdata.TO_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_TO_DT), 'MM/dd/yyyy');
                }
                if (role.AUR_ROLE == 6) {
                    var ver = _.find($scope.selectedSpaces, function (x) { if (x.VERTICAL == role.VER_CODE) return x });
                    if (ver == undefined) {
                        showNotification('error', 8, 'bottom-right', 'you can not allocate for different verticals in single seat');
                        return;
                    }
                    refdata.AUR_LEAVE_STATUS = "N";
                    refdata.SSAD_SRN_REQ_ID = null;
                    refdata.STACHECK = 16;
                    refdata.AUR_NAME = role.AUR_KNOWN_AS;
                    refdata.AUR_ID = role.AUR_ID;
                    refdata.VER_CODE = role.VER_CODE;
                    refdata.VER_NAME = role.VER_NAME;
                    refdata.Cost_Center_Code = role.COST_CENTER_CODE;
                    refdata.Cost_Center_Name = role.COST_CENTER_NAME;
                    $scope.selectedSpaces.push(refdata);
                    $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
                    MaploaderService.getEmpAllocSeat(dataobj).then(function (result) {
                        if (result.data) {
                            $scope.EmpAllocSeats = result.data;
                            progress(0, '', false);
                        }
                    });
                }
                else {
                    refdata.AUR_LEAVE_STATUS = "N";
                    refdata.SSAD_SRN_REQ_ID = null;
                    refdata.STACHECK = 16;
                    refdata.AUR_NAME = null;
                    refdata.AUR_ID = null;
                    $scope.selectedSpaces.push(refdata);
                    $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
                }

            }
            else
                showNotification('error', 8, 'bottom-right', "Please select maximum of " + prefval.SYSP_VAL1 + " Spaces");

        }
        //}

    }
    //$scope.AllocAnother = function (data) {

    //    var prefval = _.find($scope.SysPref, { SYSP_CODE: "Duplicate allocation" });
    //    if (prefval.SYSP_VAL1 = 1042 && ($scope.EmpAllocSeats.length >= 1)) {
    //        showNotification('error', 8, 'bottom-right', 'first relese employee');

    //    }
    //    else {
    //        var refdata = {};
    //        angular.copy(data, refdata);

    //        var prefval = _.find($scope.SysPref, { SYSP_CODE: "OVERLOAD" });
    //        if (prefval.SYSP_VAL1 >= $filter('filter')($scope.selectedSpaces, { SPC_ID: refdata.SPC_ID }).length + 1) {
    //            if (refdata.AUR_LONG_LEAVE_FROM_DT != '' && refdata.AUR_LONG_LEAVE_TO_DT != '' && refdata.AUR_LONG_LEAVE_FROM_DT != null && refdata.AUR_LONG_LEAVE_TO_DT != null) {
    //                refdata.FROM_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_FROM_DT), 'MM/dd/yyyy');
    //                refdata.TO_DATE = $filter('date')(new Date(refdata.AUR_LONG_LEAVE_TO_DT), 'MM/dd/yyyy');
    //            }

    //            refdata.AUR_LEAVE_STATUS = "N";
    //            refdata.SSAD_SRN_REQ_ID = null;
    //            refdata.STACHECK = 16;
    //            refdata.AUR_NAME = null;
    //            refdata.AUR_ID = null;
    //            $scope.selectedSpaces.push(refdata);
    //            $scope.gridSpaceAllocOptions.api.setRowData($scope.selectedSpaces);
    //        }
    //        else
    //            showNotification('error', 8, 'bottom-right', "Please select maximum of " + prefval.SYSP_VAL1 + " Spaces");
    //    }
    //}

    ///   Load location data
    $scope.show = function () {
        //UtilityService.getCountires(2).then(function (Countries) {
        //    progress(0, 'Loading...', true);
        //    if (Countries.data != null) {
        //        $scope.Country = Countries.data;
        //    }
        //    UtilityService.getCities(2).then(function (Ctresponse) {
        //        if (Ctresponse.data != null) {
        //            $scope.City = Ctresponse.data;
        //        }
        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Location = response.data;
            }
            UtilityService.getTowers(2).then(function (response) {
                if (response.data != null) {
                    $scope.Tower = response.data;
                }
                UtilityService.getFloors(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Floor = response.data;
                        angular.forEach($scope.Floor, function (Value, index) {
                            if (Value.FLR_CODE == GetParameterValues('flr_code')) {

                                //var cny = _.find($scope.Country, { CNY_CODE: Value.CNY_CODE });
                                //cny.ticked = true;
                                //_.remove($scope.Country, { ticked: Value.ticked = false });
                                //$scope.SearchSpace.Country[0] = cny;

                                //var cty = _.find($scope.City, { CTY_CODE: Value.CTY_CODE });
                                //cty.ticked = true;
                                //_.remove($scope.City, { ticked: Value.ticked = false });
                                //$scope.SearchSpace.City[0] = cty;

                                var twr = _.find($scope.Tower, { TWR_CODE: Value.TWR_CODE } || { LCM_CODE: Value.LCM_CODE });
                                twr.ticked = true;
                                _.remove($scope.Tower, { ticked: Value.ticked = false });
                                $scope.SearchSpace.Tower[0] = twr;

                                var lcm = _.find($scope.Location, { LCM_CODE: Value.LCM_CODE });
                                lcm.ticked = true;
                                _.remove($scope.Location, { ticked: Value.ticked = false });
                                $scope.SearchSpace.Location[0] = lcm;

                                var flr = _.find($scope.Floor, { FLR_CODE: Value.FLR_CODE } || { LCM_CODE: Value.LCM_CODE } || { TWR_CODE: Value.TWR_CODE });
                                flr.ticked = true;
                                _.remove($scope.Floor, { ticked: Value.ticked == false });
                                $scope.SearchSpace.Floor[0] = flr;
                            }
                        });

                        SpaceRequisitionService.getShifts($scope.SearchSpace.Location).then(function (response) {
                            $scope.Shifts = response.data;
                        }, function (error) {
                            console.log(error);
                        });

                        //MaploaderService.GetFlrIdbyLoc().then(function (response) {
                        //    $scope.LoadMap(response[0].FLR_ID);
                        //}, function (error) {
                        //});
                    }
                    UtilityService.getBussHeirarchy().then(function (response) {
                        if (response.data != null) {
                            $scope.BsmDet = response.data;
                        }
                        UtilityService.GetRoleAndReportingManger().then(function (response) {
                            if (response.data != null) {
                                $scope.GetRoleAndRM = response.data;

                            }
                        });
                    });
                });

            });

        });
        //    });
        //});
    }
    MaploaderService.GetFlrIdbyLoc().then(function (response) {
     $scope.LoadMap(GetParameterValues('flr_code'));
      
    }, function (error) {
    });

    //UtilityService.getBussHeirarchy().then(function (response) {
    //    if (response.data != null) {
    //        $scope.BsmDet = response.data;
    //    }
    //});
    //UtilityService.GetRoleAndReportingManger().then(function (response) {
    //    if (response.data != null) {
    //        $scope.GetRoleAndRM = response.data;

    //        console.log($scope.GetRoleAndRM);
    //    }
    //});
    UtilityService.GetEmployee().then(function (response) {
        if (response.data != null) {
            $scope.Employee = response.data;


        }
    });

    $scope.LoadMap = function (flrid) {

        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });

        $scope.Markerdata.clearLayers();
        $scope.MarkerLblLayer.clearLayers();
        $scope.MarkerMeetingLblLayer.clearLayers();
        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        map.addLayer($scope.Markerdata);

        MaploaderService.bindMap(flrid).then(function (response) {
            if (response.mapDetails != null) {

                angular.forEach(response.mapDetails, function (value, index) {
                    // do something

                    var wkt = new Wkt.Wkt();
                    wkt.read(value.Wkt);

                    var theLayer = wkt.toObject();
                    theLayer.dbId = value.ID;
                    theLayer.options.color = '#000000';
                    //theLayer.options.weight = 1;
                    if (prefval == undefined) {
                        theLayer.options.weight = 1;

                    }
                    else theLayer.options.weight = 0.3;



                    theLayer.options.seattype = value.SEATTYPE;
                    theLayer.options.spacetype = value.layer;
                    theLayer.options.seatstatus = value.STAID;
                    theLayer.options.spacesubtype = value.SPC_SUB_TYPE;
                    theLayer.options.spaceid = value.SPACE_ID;
                    theLayer.options.checked = false;

                    //var col = response.COLOR[value.layer] == undefined ? '#E8E8E8' : response.COLOR[value.layer];
                    if (value.layer == "FRE")
                        theLayer.setStyle({ fillColor: '#FA8072', opacity: 0.5, fillOpacity: 0.5 });
                    else
                        theLayer.setStyle({ fillColor: '#E8E8E8', opacity: 0.5, fillOpacity: 0.5 });
                    $scope.drawnItems.addLayer(theLayer);

                });

                $scope.bounds = [[response.BBOX[0].MinY, response.BBOX[0].MinX], [response.BBOX[0].MaxY, response.BBOX[0].MaxX]];
                $scope.LAYOUTNAME = response.BBOX[0].LAYOUTNAME;

                map.fitBounds($scope.bounds);
                map.whenReady(function (e) {
                    setTimeout(function () {
                        $scope.$apply(function () {
                            $scope.ZoomLvl = map.getZoom();
                        });
                    }, 200);
                });

                $scope.floorDetails = response.FloorDetails;
                $scope.LoadMarkers(response.FloorDetails);
                $scope.show();
                setTimeout(function () {
                    $scope.loadThemes();
                }, 500);
            }
        });
    }

    function updateSummaryCount() {
        MaploaderService.getLegendsSummary($scope.SearchSpace.Floor[0].FLR_CODE).then(function (response) {
            if (response != null) {
                $scope.SeatSummary = response;
            }
        }, function (error) {
        });
    }

    $scope.LoadMarkers = function (flrdata) {
        $scope.EmpAllocSeats = [];
        MaploaderService.bindMarkers(flrdata).then(function (response) {
            $scope.kiosk = response;
           if (response != null) {
                angular.forEach(response, function (value, index) {
                    var style = {};
                    if (value.SPC_TYPE_STATUS == 2) {

                        switch (value.STATUS) {
                            case 1: chairicon = VacantStyle;
                                break;
                            case 1001: chairicon = AllocateStyle;
                                break;
                            case 1002: chairicon = AllocateStyle;
                                break;
                            case 1004: chairicon = OccupiedStyle;
                                break;
                            case 1003: chairicon = AllocateStyle;
                                break;
                            case 1006: chairicon = ReservedStyle;
                                break;
                            case 1052: chairicon = BlockStyle;
                                break;
                            default: chairicon = VacantStyle;
                                break;
                        }
                        if (value.SPC_STA_ID == '0') {
                            switch (value.SPC_STA_ID) {
                                case 0: chairicon = InactiveStyle;
                                    break;
                            }
                        }

                        if (value.AUR_LEAVE_STATUS == 'Y') {
                            switch (value.AUR_LEAVE_STATUS) {
                                case 'Y': chairicon = LeaveStyle;
                                    break;
                            }
                        }
                        $scope.marker = _.find($scope.drawnItems._layers, { options: { spaceid: value.SPC_ID, spacetype: 'CHA' } });
                        $scope.marker.setStyle(chairicon);
                        $scope.marker.lat = value.x;
                        $scope.marker.lon = value.y;
                        $scope.marker.SPC_ID = value.SPC_ID;
                        $scope.marker.SPC_NAME = value.SPC_NAME;
                        $scope.marker.SPC_DESC = value.SPC_DESC;
                        $scope.marker.layer = value.SPC_TYPE_CODE;
                        $scope.marker.VERTICAL = value.VERTICAL;
                        $scope.marker.VER_NAME = value.VER_NAME;
                        $scope.marker.COSTCENTER = value.COSTCENTER;
                        $scope.marker.SPC_TYPE_CODE = value.SPC_TYPE_CODE;
                        $scope.marker.SPC_TYPE_NAME = value.SPC_TYPE_NAME;
                        $scope.marker.SST_CODE = value.SST_CODE;
                        $scope.marker.SST_NAME = value.SST_NAME;
                        $scope.marker.SHIFT_TYPE = value.SHIFT_TYPE;
                        $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                        $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                        $scope.marker.STACHECK = value.STACHECK;
                        $scope.marker.STATUS = value.STATUS;
                        $scope.marker.STA_DESC = value.STA_DESC;
                        $scope.marker.AUR_ID = value.AUR_ID;
                        $scope.marker.AUR_LEAVE_STATUS = value.AUR_LEAVE_STATUS;
                        $scope.marker.ticked = false;

                        //$scope.marker.on('contextmenu', function (e) {
                        //    $ngConfirm({
                        //        icon: 'fa fa-warning',
                        //        title: 'Confirm!',
                        //        content: 'Are You Sure You Want To Inactive Seat',
                        //        closeIcon: true,
                        //        closeIconClass: 'fa fa-close',
                        //        buttons: {
                        //            Confirm: {
                        //                text: 'Sure',
                        //                btnClass: 'btn-blue',
                        //                action: function (button) {
                        //                    var spcdet = {};
                        //                    $scope.selectedActSpaces = [];
                        //                    spcdet.SPACE_ID = value.SPC_ID;
                        //                    spcdet.SPC_TYPE = value.SPC_TYPE_NAME;
                        //                    spcdet.SPC_SUB_TYPE = value.SST_NAME;
                        //                    spcdet.ticked = true;
                        //                    $scope.selectedActSpaces.push(spcdet);

                        //                    var dataobj = { FLR_CODE: e.target.SPC_FLR_ID, SPC_LST: $scope.selectedActSpaces };
                        //                    MaploaderService.InactiveSeats(dataobj).then(function (response) {
                        //                        _.remove($scope.Markers, { SPC_ID: e.target.SPC_ID });
                        //                        reloadMarkers();
                        //                        $ngConfirm({
                        //                            icon: 'fa fa-check',
                        //                            title: 'Confirm!',
                        //                            content: 'Selected Space Has Been Inactive',
                        //                            buttons: {
                        //                                Confirm: {
                        //                                    text: 'Ok',
                        //                                    btnClass: 'btn-blue'
                        //                                },
                        //                            }
                        //                        });
                        //                    });

                        //                }
                        //            },
                        //            close: function ($scope, button) {
                        //            }
                        //        }
                        //    });
                        //});

                        $scope.marker.bindLabel(value.SPC_DESC, {
                            noHide: false,
                            direction: 'auto',
                            zIndexOffset: 2000
                        });//.setOpacity(1);

                        if (value.STATUS != 1006)
                            $scope.marker.on('click', markerclicked);

                        //var SpaceIdbyName = value.SPC_NAME.split('-');
                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [10, 10]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });

                        $scope.MarkerLblLayer_copy.addLayer($scope.MarkerLabel);
                        $scope.MarkerLblLayer.addLayer($scope.MarkerLabel);
                        $scope.Markers.push($scope.marker);
                        $scope.Markerdata.addLayer($scope.marker);
                    }
                    else {

                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [10, 20]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabel.bindLabel(value.SPC_DESC, {
                            noHide: false,
                            direction: 'auto',
                            zIndexOffset: 2000
                        });//.setOpacity(1);
                        $scope.MarkerMeetingLblLayer_copy.addLayer($scope.MarkerLabel);
                        $scope.MarkerMeetingLblLayer.addLayer($scope.MarkerLabel);

                    }
                });
                //angular.forEach($scope.Markers, function (marker, key) {
                //    $scope.Markerdata.addLayer(marker);
                //    //marker.addTo(map);
                //});
                $scope.gridOptions.api.setRowData($scope.Markers);
                $scope.gridOptions.api.refreshHeader();
                progress(0, '', false);
                //$scope.gridOptions.api.sizeColumnsToFit();
            }
            else {
                showNotification('error', 8, 'bottom-right', 'No records Found');
            }
        }, function (error) {
        });

        MaploaderService.bindCornerLables(flrdata).then(function (response) {
            if (response != null) {
                var X = -40;
                var Y = 10;
                var i = 0;
                angular.forEach(response, function (value, index) {
                    if (i == 18) {
                        X = X - 180;
                        Y = 10;
                        i = 0;
                    }
                    $scope.MarkerLabel = L.marker([value.SPACE_X_RIGHT, value.SPACE_Y_RIGHT], {
                        icon: L.divIcon({
                            className: 'text-labels',   // Set class for CSS styling
                            html: value.Column1,
                            iconAnchor: [X, Y]
                        }),
                        draggable: false,       // Allow label dragging...?
                        zIndexOffset: 1000     // Make appear above other map features
                    });

                    $scope.MarkerLblLayerEmpDetails_copy.addLayer($scope.MarkerLabel);
                    $scope.MarkerLblLayerEmpDetails.addLayer($scope.MarkerLabel);
                    Y = Y - 20;
                    i++;
                }
                )
            }
        }
        );
        setTimeout(function () {
            $scope.ShowEmponMap();
        }, 1000);
       
    }

    //LoadMarkers code repeated Only for printing purpose
    $scope.LoadMarkers2 = function (flrdata) {
        $scope.EmpAllocSeats = [];
        MaploaderService.bindMarkers(flrdata).then(function (response) {
            if (response != null) {
                angular.forEach(response, function (value, index) {
                    var style = {};
                    if (value.SPC_TYPE_STATUS == 2) {

                        //switch (value.STATUS) {
                        //    case 1: chairicon = VacantStyle;
                        //        break;
                        //    case 1001: chairicon = AllocateStyle;
                        //        break;
                        //    case 1002: chairicon = AllocateStyle;
                        //        break;
                        //    case 1004: chairicon = OccupiedStyle;
                        //        break;
                        //    case 1003: chairicon = AllocateStyle;
                        //        break;
                        //    case 1006: chairicon = ReservedStyle;
                        //        break;
                        //    case 1052: chairicon = BlockStyle;
                        //        break;
                        //    default: chairicon = VacantStyle;
                        //        break;
                        //}
                        //if (value.SPC_STA_ID == '0') {
                        //    switch (value.SPC_STA_ID) {
                        //        case 0: chairicon = InactiveStyle;
                        //            break;
                        //    }
                        //}

                        //if (value.AUR_LEAVE_STATUS == 'Y') {
                        //    switch (value.AUR_LEAVE_STATUS) {
                        //        case 'Y': chairicon = LeaveStyle;
                        //            break;
                        //    }
                        //}
                        //$scope.marker = angular.copy(_.find($scope.drawnItems._layers, { options: { spaceid: value.SPC_ID, spacetype: 'CHA' } }));
                        //$scope.marker.setStyle(chairicon);
                        //$scope.marker.lat = value.x;
                        //$scope.marker.lon = value.y;
                        //$scope.marker.SPC_ID = value.SPC_ID;
                        //$scope.marker.SPC_NAME = value.SPC_NAME;
                        //$scope.marker.SPC_DESC = value.SPC_DESC;
                        //$scope.marker.layer = value.SPC_TYPE_CODE;
                        //$scope.marker.VERTICAL = value.VERTICAL;
                        //$scope.marker.VER_NAME = value.VER_NAME;
                        //$scope.marker.COSTCENTER = value.COSTCENTER;
                        //$scope.marker.SPC_TYPE_CODE = value.SPC_TYPE_CODE;
                        //$scope.marker.SPC_TYPE_NAME = value.SPC_TYPE_NAME;
                        //$scope.marker.SST_CODE = value.SST_CODE;
                        //$scope.marker.SST_NAME = value.SST_NAME;
                        //$scope.marker.SHIFT_TYPE = value.SHIFT_TYPE;
                        //$scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                        //$scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                        //$scope.marker.STACHECK = value.STACHECK;
                        //$scope.marker.STATUS = value.STATUS;
                        //$scope.marker.STA_DESC = value.STA_DESC;
                        //$scope.marker.ticked = false;

                        //$scope.marker.on('contextmenu', function (e) {
                        //    $ngConfirm({
                        //        icon: 'fa fa-warning',
                        //        title: 'Confirm!',
                        //        content: 'Are You Sure You Want To Inactive Seat',
                        //        closeIcon: true,
                        //        closeIconClass: 'fa fa-close',
                        //        buttons: {
                        //            Confirm: {
                        //                text: 'Sure',
                        //                btnClass: 'btn-blue',
                        //                action: function (button) {
                        //                    var spcdet = {};
                        //                    $scope.selectedActSpaces = [];
                        //                    spcdet.SPACE_ID = value.SPC_ID;
                        //                    spcdet.SPC_TYPE = value.SPC_TYPE_NAME;
                        //                    spcdet.SPC_SUB_TYPE = value.SST_NAME;
                        //                    spcdet.ticked = true;
                        //                    $scope.selectedActSpaces.push(spcdet);

                        //                    var dataobj = { FLR_CODE: e.target.SPC_FLR_ID, SPC_LST: $scope.selectedActSpaces };
                        //                    MaploaderService.InactiveSeats(dataobj).then(function (response) {
                        //                        _.remove($scope.Markers, { SPC_ID: e.target.SPC_ID });
                        //                        reloadMarkers();
                        //                        $ngConfirm({
                        //                            icon: 'fa fa-check',
                        //                            title: 'Confirm!',
                        //                            content: 'Selected Space Has Been Inactive',
                        //                            buttons: {
                        //                                Confirm: {
                        //                                    text: 'Ok',
                        //                                    btnClass: 'btn-blue'
                        //                                },
                        //                            }
                        //                        });
                        //                    });

                        //                }
                        //            },
                        //            close: function ($scope, button) {
                        //            }
                        //        }
                        //    });
                        //});

                        //$scope.marker.bindLabel(value.SPC_DESC, {
                        //    noHide: false,
                        //    direction: 'auto',
                        //    zIndexOffset: 2000
                        //});//.setOpacity(1);

                        //if (value.STATUS != 1006)
                        //    $scope.marker.on('click', markerclicked);

                        //var SpaceIdbyName = value.SPC_NAME.split('-');
                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [5, 0.530]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });


                        $scope.MarkerLblLayer.addLayer($scope.MarkerLabel);

                        //$scope.Markers.push($scope.marker);
                        //$scope.Markerdata.addLayer($scope.marker);
                    }
                    else {

                        $scope.MarkerLabel = L.marker([value.x, value.y], {
                            icon: L.divIcon({
                                className: 'text-labels2',   // Set class for CSS styling
                                html: value.SPC_VIEW_NAME,
                                iconAnchor: [5, 0.530]
                            }),
                            draggable: false,       // Allow label dragging...?
                            zIndexOffset: 1000     // Make appear above other map features
                        });
                        $scope.MarkerLabel.bindLabel(value.SPC_DESC, {
                            noHide: false,
                            direction: 'auto',
                            zIndexOffset: 2000
                        });//.setOpacity(1);
                        $scope.MarkerMeetingLblLayer.addLayer($scope.MarkerLabel);

                    }
                });
                //angular.forEach($scope.Markers, function (marker, key) {
                //    $scope.Markerdata.addLayer(marker);
                //    //marker.addTo(map);
                //});
                $scope.gridOptions.api.setRowData($scope.Markers);
                $scope.gridOptions.api.refreshHeader();
                progress(0, '', false);
                //$scope.gridOptions.api.sizeColumnsToFit();
            }
            else {
                showNotification('error', 8, 'bottom-right', 'No records Found');
            }
        }, function (error) {
        });

        MaploaderService.bindCornerLables(flrdata).then(function (response) {
            if (response != null) {
                var X = -40;
                var Y = 10;
                var i = 0;
                angular.forEach(response, function (value, index) {
                    if (i == 60) {
                        X = X - 40;
                        Y = 10;
                        i = 0;
                    }
                    $scope.MarkerLabel = L.marker([value.SPACE_X_RIGHT, value.SPACE_Y_RIGHT], {
                        icon: L.divIcon({
                            className: 'text-labels2',   // Set class for CSS styling
                            html: value.Column1,
                            iconAnchor: [X, Y]
                        }),
                        draggable: false,       // Allow label dragging...?
                        zIndexOffset: 1000     // Make appear above other map features
                    });


                    $scope.MarkerLblLayerEmpDetails.addLayer($scope.MarkerLabel);
                    Y = Y - 5;
                    i++;
                }
                )
            }
        }
        );
    }

    UtilityService.GetRoleByUserId().then(function (response) {
        if (response.data != null) {
            $scope.Role = response.data;
            if ($scope.Role.Rol_id == 'N') {
                $scope.gridOptions.columnApi.setColumnVisible('ticked', false)
            }
            else { $scope.gridOptions.columnApi.setColumnVisible('ticked', true) }
        }
    });

    function markerclicked(e) {

        var role = _.find($scope.GetRoleAndRM);
        if ($scope.selectedSpaces.length != 0 && role.AUR_ROLE == 6 && this.ticked == false) {
            showNotification('error', 8, 'bottom-right', 'Employee can not access more than 1 seat');
            return;
        }

        if (role.AUR_ROLE == 6 && this.STATUS == 1004 && (this.SHIFT_TYPE == 3 || this.SHIFT_TYPE == 1) && role.AUR_ID != this.AUR_ID.trim()) {
            showNotification('error', 8, 'bottom-right', 'Employee can not access another person seat');
            return;
        }
        if (!this.ticked) {
            if ($scope.Role.Rol_id == 'N') {
                this.ticked = false;
            }
            else {
                
                this.setStyle(selctdChrStyle)
                this.ticked = true;
                var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, category: this.SPC_ID, subitem: this.SSA_SRNREQ_ID ? this.SSA_SRNREQ_ID : "" };
                MaploaderService.GetSpaceDetailsByREQID(dataobj).then(function (response) {

                    if (response != null) {
                        angular.forEach(response, function (value, index) {
                            $scope.selectedSpaces.push(value);
                            
                            //var role = _.find($scope.GetRoleAndRM);GetRoleAndRM
                            if (role.AUR_ROLE == 6) {
                                value.blocked = true;
                                //var ver = _.find($scope.selectedSpaces, function (x) { if (x.VERTICAL == role.VER_CODE) return x });
                                //if (ver == undefined) {
                                //    $scope.selectedSpaces = [];
                                //    showNotification('error', 8, 'bottom-right', 'you can not allocate for different verticals in single seat');
                                //     reloadMarkers();
                                //     return;

                                //}
                                //var cost = _.find($scope.selectedSpaces, function (x) { if ((x.STATUS == 1003 || x.STATUS == 1004) && x.Cost_Center_Code == role.COST_CENTER_CODE) return x });
                                //if (cost == undefined) {
                                //    $scope.selectedSpaces = [];
                                //    showNotification('error', 8, 'bottom-right', 'you can not allocate for different costcenter to employee');
                                //    reloadMarkers();
                                //    return;

                                //}
                                var data = { lcm_code: $scope.SearchSpace.Location[0].LCM_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: role.AUR_ID, mode: 1 };
                                MaploaderService.getEmpAllocSeat(data).then(function (result) {
                                    if (result.data) {
                                        $scope.EmpAllocSeats = result.data;
                                        if (role.AUR_ROLE == 6) {
                                            $scope.ProallocEnabledStatus = true;
                                        }
                                        progress(0, '', false);
                                    }
                                });
                            }

                            progress(0, '', false);
                            $('#SeatAllocation').modal('show');
                        })
                    }
                    $scope.EmpAllocSeats = _.remove($scope.EmpAllocSeats, function (n) {
                        return _.find($scope.gridSpaceAllocOptions.rowData, { AUR_ID: n.AUR_ID });
                    });
                    $scope.EmpAllocSeats = _.uniqBy($scope.EmpAllocSeats, 'AUR_ID');
                }, function (error) {
                });
            }
        }
        else {
            switch (this.STATUS) {
                case 1: chairicon = VacantStyle;
                    break;
                case 1002: chairicon = AllocateStyle;
                    break;
                case 1004: chairicon = OccupiedStyle;
                    break;
                case 1003: chairicon = AllocateStyle;
                    break;
                case 1052: chairicon = BlockStyle;
                    break;

                default: chairicon = VacantStyle;
                    break;
            }
            _.remove($scope.selectedSpaces, function (space) { return space.SPC_ID == e.target.SPC_ID });

            this.setStyle(chairicon)
            this.ticked = false;
        }
        $scope.gridOptions.api.refreshView();
    }

    /****************  Grid Display ***************/

    var columnDefs = [
        { headerName: "Select", field: "ticked", width: 90, template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkedChanged(data)' />", cellClass: 'grid-align', suppressFilter: true },
        //{ headerName: "Space ID", field: "SPC_ID", cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)" },
        { headerName: "Space ID", field: "SPC_NAME", cellClass: 'grid-align', enableSorting: true },
        { headerName: "Space Type", field: "SPC_TYPE_NAME", enableSorting: true },
        { headerName: "Space Sub Type", field: "SST_NAME", enableSorting: true },
        { headerName: "Status", field: "STA_DESC", enableSorting: true }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        showToolPanel: true,
        rowAggregatePanelShow: 'none',
        enableColResize: true,
        enableCellSelection: false,
        suppressRowClickSelection: true,
        onready: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode('Select All');
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = true;
                        value.setStyle(selctdChrStyle)
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = false;
                        value.setStyle(VacantStyle)
                        switch (value.STATUS) {
                            case 1: value.setStyle(VacantStyle);
                                break;
                            case 1002: value.setStyle(AllocateStyle);
                                break;
                            case 1004: value.setStyle(OccupiedStyle);
                                break;
                            case 1003: value.setStyle(AllocateStyle);
                                break;
                            case 1052: value.setStyle(BlockStyle);
                                break;
                            default: value.setStyle(VacantStyle);
                                break;
                        }

                    });
                });
            }
        });
        return eHeader;
    }

    /****************  Filter ***************/

    $("#filtertxt").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    });

    $("#InacFilter").change(function () {
        onSpaceReqFilterChanged($(this).val());
    }).keydown(function () {
        onSpaceReqFilterChanged($(this).val());
    }).keyup(function () {
        onSpaceReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onSpaceReqFilterChanged($(this).val());
    });

    $("#txtCountFilter").change(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).keydown(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).keyup(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).bind('paste', function () {
        onReq_SelSpacesFilterChanged($(this).val());
    });

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    function onSpaceReqFilterChanged(value) {
        $scope.gridSpaceAllocation.api.setQuickFilter(value);
    }

    function onReq_SelSpacesFilterChanged(value) {
        $scope.gridSpaceAllocOptions.api.setQuickFilter(value);
    }


    /****************  Check change event ***************/
    $scope.chkedChanged = function (data) {
        if (data.ticked) {
            if ($scope.Role.Rol_id == 'N') {
                this.ticked = false;
            }
            else {
                data.setStyle(selctdChrStyle)
                data.ticked = true;
                var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, category: data.SPC_ID, subitem: data.SSA_SRNREQ_ID ? data.SSA_SRNREQ_ID : "" };
                MaploaderService.GetSpaceDetailsByREQID(dataobj).then(function (response) {
                    if (response != null) {
                        angular.forEach(response, function (value, index) {
                            $scope.selectedSpaces.push(value);
                        })
                    }
                }, function (error) {
                });
            }
        }
        else {
            switch (data.STATUS) {
                case 1: chairicon = VacantStyle;
                    break;
                case 1002: chairicon = AllocateStyle;
                    break;
                case 1004: chairicon = OccupiedStyle;
                    break;
                case 1003: chairicon = AllocateStyle;
                    break;
                case 1052: chairicon = BlockStyle;
                    break;
                default: chairicon = VacantStyle;
                    break;
            }
            _.remove($scope.selectedSpaces, _.find($scope.selectedSpaces, { SPC_ID: data.SPC_ID }));

            data.setStyle(chairicon)
            data.ticked = false;
        }
    }

    /****************  Filters ***************/

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });

    $scope.CnyChangeAll = function () {
        $scope.SearchSpace.Country = $scope.Country;
        $scope.CnyChanged();
    }
    $scope.CnySelectNone = function () {
        $scope.SearchSpace.Country = [];
        $scope.CnyChanged();
    }
    $scope.CnyChanged = function () {
        if ($scope.SearchSpace.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.SearchSpace.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.SearchSpace.City = $scope.City;
        $scope.CtyChanged();
    }
    $scope.CtySelectNone = function () {
        $scope.SearchSpace.City = [];
        $scope.CtyChanged();
    }
    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.SearchSpace.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.SearchSpace.Location = $scope.Location;
        $scope.LcmChanged();
    }
    $scope.LcmSelectNone = function () {
        $scope.SearchSpace.Location = [];
        $scope.LcmChanged();
    }
    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.SearchSpace.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });
    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.SearchSpace.Tower = $scope.Tower;
        $scope.TwrChanged();
    }
    $scope.TwrSelectNone = function () {
        $scope.SearchSpace.Tower = [];
        $scope.TwrChanged();
    }
    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.SearchSpace.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SearchSpace.Location.push(lcm);
            }
        });
    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.SearchSpace.Floor = $scope.Floor;
        $scope.FlrChanged();
    }
    $scope.FlrSelectNone = function () {
        $scope.SearchSpace.Floor = [];
        $scope.FlrChanged();
    }
    $scope.FlrChanged = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SearchSpace.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SearchSpace.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SearchSpace.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SearchSpace.Tower.push(twr);
            }
        });
        $scope.loadThemes();
    }
    $scope.showStatusPanel = true;

    $scope.loadThemes = function () {

        $scope.showStatusPanel = true;
        progress(0, 'Loading...', true);
        $scope.SubItems = [];
        $scope.ddlSubItem = "";
        var item = {};
        $scope.ddlselval = _.find($scope.Items, { CODE: $scope.ddlItem });

        if ($scope.ddlItem) {
            if ($scope.ddlselval != undefined) {
                if ($scope.ddlselval.CHKDDL == 1) {
                    var dataobj = { flr_code: GetParameterValues('flr_code'), Item: $scope.ddlItem, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
                    MaploaderService.getallFilterbySubItem(dataobj).then(function (response) {
                        $scope.ddldisplay = true;
                        $scope.SubItems = response;
                        progress(0, '', false);
                    }, function (error) {
                        console.log(error);
                    });
                    progress(0, '', false);
                }
                else {
                    $scope.ddldisplay = false;
                    setTimeout(function () { $scope.GetLegendCount() }, 200);
                }
            }
            else {
                $scope.ddldisplay = false;
                setTimeout(function () { $scope.GetLegendCount() }, 200);
            }
        }
        map.scrollWheelZoom.enable();
    }

    $scope.loadSubDetails = function () {
        $scope.showStatusPanel = true;
        progress(0, 'Loading...', true);
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: $scope.ddlItem, subitem: $scope.ddlSubItem, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.GetSpaceDetailsBySUBITEM(dataobj).then(function (response) {
           
            if (response != null) {

                if ($scope.ddlItem == "login" || $scope.ddlItem == "liveattn") {
                    $.each($scope.drawnItems._layers, function (Key, layer) {
                        var spcid = _.find(response.Table, { SPC_ID: layer.options.spaceid });
                
                        if (spcid == undefined)
                            layer.setStyle({ fillColor: "#E8E8E8" });
                    });

                    angular.forEach($scope.Markers, function (marker, key) {

                        var spcid = _.find(response.Table, { SPC_ID: marker.SPC_ID });
                        if (spcid != null) {
                            marker.ticked = false;
                            switch (spcid.STATUS) {
                                case 1: marker.setStyle(VacantStyle);
                                    break;
                                case 1002: marker.setStyle(AllocateStyle);
                                    break;
                                case 1004: marker.setStyle(OccupiedStyle);
                                    break;
                                case 1003: marker.setStyle(AllocateStyle);
                                    break;
                                case 1052: marker.setStyle(BlockStyle);
                                    break;
                                default: marker.setStyle(VacantStyle);
                                    break;
                            }
                            marker.bindLabel(spcid.SPC_DESC, {
                                noHide: false,
                                direction: 'auto',
                                zIndexOffset: 2000
                            });//.setOpacity(1);
                        }
                    });
                    $scope.showStatusPanel = false;
                    $scope.SpcCount = response.Table1;

                }
                else {
                    $.each($scope.drawnItems._layers, function (Key, layer) {
                        var spcid = _.find(response.Table, { SPC_ID: layer.options.spaceid });
                        if (spcid == undefined)
                            layer.setStyle({ fillColor: "#E8E8E8" });
                        else {
                            if (layer.options.spacetype == 'CHA') {
                                switch (spcid.STATUS) {
                                    case 1: chairicon = VacantStyle;
                                        break;
                                    case 1002: chairicon = AllocateStyle;
                                        break;
                                    case 1004: chairicon = OccupiedStyle;
                                        break;
                                    case 1003: chairicon = AllocateStyle;
                                        break;
                                    case 1052: chairicon = BlockStyle;
                                        break;
                                    default: chairicon = VacantStyle;
                                        break;
                                }

                                layer.setStyle(chairicon);
                            }
                            else {
                                if ($scope.ddlItem == "shiftalloc") {
                                    $scope.SpcCount = response.Table1;
                                }
                                else {
                                    var spcclrobj = _.find($scope.SubItemColordet, { SPC_ID: spcid.SPC_ID });
                                    if (spcclrobj != undefined)
                                        layer.setStyle({ fillColor: spcclrobj.COLOR, opacity: 0.65 });
                                }
                            }

                        }
                    });
                    if ($scope.ddlItem == "shiftalloc") {
                        $scope.GetLegendCount();
                    }
                    else if ($scope.ddlItem == "liveattn" || $scope.ddlItem == "shift") {
                        $scope.showStatusPanel = false;
                        $scope.SpcCount = response.Table1;
                    }
                }

            }
            progress(0, '', false);
        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }

    $scope.GetLegendCount = function () {

        $scope.SpcCount = [];

        var themeobj = { Item: $scope.ddlItem, flr_code:GetParameterValues('flr_code'), subitem: $scope.ddlSubItem };
        $.post(UtilityService.path + '/api/KotakMaploader/GetLegendsCount', themeobj, function (result) {
            if ($scope.ddlItem == 'Vacant') {
                $scope.$apply(function () {
                    $scope.SpcCount = result.Table;
                });
                var vacspaces = [];
                angular.forEach($scope.Markers, function (marker, key) {
                    marker.ticked = false;
                    if (marker.STATUS == 1) {
                        marker.setStyle(VacantStyle);
                        vacspaces.push(marker);
                    }
                    progress(0, '', false);
                });
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find(vacspaces, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                    else {
                        layer.setStyle({ fillColor: "#228B22", opacity: 0.65 });
                    }
                    progress(0, '', false);
                });

            }

            else if ($scope.ddlItem == "entity") {
                if (result.Table) {
                    $scope.$apply(function () {
                        $scope.SpcCount = result.Table;
                        if ($scope.SpcCountArea.length == 0)
                            $scope.SpcCountArea = result.Table;
                    });
                    for (var i = 0; i < result.Table1.length; i++) {
                        $.each($scope.drawnItems._layers, function (Key, layer) {
                            if (layer.options.spaceid == result.Table1[i].SPC_ID && layer.options.spacetype != "CHA") {
                                layer.setStyle({ fillColor: result.Table1[i].COLOR, opacity: 0.65 });
                            }
                        });
                    }

                    progress(0, '', false);
                }
            }
            else if ($scope.ddlItem == "release") {
                $scope.$apply(function () {
                    $scope.SubItemColordet = result.Table;
                    $scope.SpcCount = result.Table1;
                });
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find($scope.SubItemColordet, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                });

                angular.forEach($scope.Markers, function (marker, key) {
                    marker.ticked = false;
                    switch (marker.STATUS) {
                        case 1: marker.setStyle(VacantStyle);
                            break;
                        case 1002: marker.setStyle(AllocateStyle);
                            break;
                        case 1004: marker.setStyle(OccupiedStyle);
                            break;
                        case 1003: marker.setStyle(AllocateStyle);
                            break;
                        case 1052: marker.setStyle(BlockStyle);
                            break;
                        default: marker.setStyle(VacantStyle);
                            break;
                    }
                });

                angular.forEach($scope.SubItemColordet, function (Value, key) {
                    var clrobj = _.find($scope.SpcCount, { CODE: Value.CODE });
                    if (clrobj != undefined)
                        $.each($scope.drawnItems._layers, function (Key, layer) {
                            if (layer.options.spaceid == Value.SPC_ID && layer.options.spacetype != "CHA") {
                                layer.setStyle({ fillColor: clrobj.COLOR, opacity: 0.65 });
                            }
                        });
                });

                progress(0, '', false);
            }

            else if ($scope.ddlItem != "verticalalloc" && $scope.ddlItem != "costcenteralloc" && $scope.ddlItem != "shiftalloc") {
                if (result.Table) {
                    $scope.$apply(function () {
                        $scope.SpcCount = result.Table;
                        if ($scope.SpcCountArea.length == 0)
                            $scope.SpcCountArea = result.Table;
                    });

                    for (var i = 0; i < result.Table.length; i++) {
                        $.each($scope.drawnItems._layers, function (Key, layer) {
                            if (layer.options[$scope.ddlItem] == result.Table[i].CODE && layer.options.spacetype != "CHA") {
                                layer.setStyle({ fillColor: result.Table[i].COLOR, opacity: 0.65 });
                            }
                        });
                    }

                    progress(0, '', false);
                }
            }

            else {
                $scope.SubItemColordet = result.Table;
                $.each($scope.drawnItems._layers, function (Key, layer) {
                    var spcid = _.find($scope.SubItemColordet, { SPC_ID: layer.options.spaceid });
                    if (spcid == undefined)
                        layer.setStyle({ fillColor: "#E8E8E8" });
                });

                angular.forEach($scope.Markers, function (marker, key) {
                    marker.ticked = false;
                    switch (marker.STATUS) {
                        case 1: marker.setStyle(VacantStyle);
                            break;
                        case 1002: marker.setStyle(AllocateStyle);
                            break;
                        case 1004: marker.setStyle(OccupiedStyle);
                            break;
                        case 1003: marker.setStyle(AllocateStyle);
                            break;
                        case 1052: marker.setStyle(BlockStyle);
                            break;
                        default: marker.setStyle(VacantStyle);
                            break;
                    }
                });

                angular.forEach($scope.SubItemColordet, function (Value, key) {
                    $.each($scope.drawnItems._layers, function (Key, layer) {
                        if (layer.options.spaceid == Value.SPC_ID && layer.options.spacetype == Value.SPC_LAYER && layer.options.spacetype != "CHA") {
                            layer.setStyle({ fillColor: Value.COLOR, opacity: 0.65 });
                            return;
                        }
                    });
                });
                $scope.$apply(function () {
                    $scope.ddldisplay = true;
                    $scope.SpcCount = result.Table1;
                    if ($scope.ddlItem != "shiftalloc") {
                        $scope.SubItems = result.Table1;
                    }
                });

                progress(0, '', false);
            }

        });
    }

    $scope.FilterData = function (filterdata, type) {
        var filterMarkers = $filter('filter')($scope.Markers, { STATUS: filterdata });
    }

    $('#divSeatStatus').on('shown.bs.collapse', function () {

        progress(0, '', true);
        if ($scope.SeatSummary == undefined) {
            updateSummaryCount();
            progress(0, 'Loading...', false);
        }
        else
            progress(0, 'Loading...', false);
    });

    $('#divSeatcap').on('shown.bs.collapse', function () {
        if ($scope.seatingCapacity == undefined)
            $scope.GetSeatingCapacity();
    });

    $('#divWorkarea').on('shown.bs.collapse', function () {
        if ($scope.SpcCountArea == undefined)
            $scope.GetSeatingArea();
    });

    $('#divotherarea').on('shown.bs.collapse', function () {
        if ($scope.Totalareadet == undefined)
            $scope.GetTotalAreaDetails()
    });

    $scope.SelectAllocEmp = function (SelectedEmp) {
        $scope.LoadDet = [];
        $scope.SelectedID = "";
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, Item: SelectedEmp, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.getAllocEmpDetails(dataobj).then(function (response) {
            if (response.data != null) {
                $scope.LoadDet = response.data;
                setTimeout(function () {
                    $('#Searchdata').selectpicker('refresh');
                }, 200);
            }
        });
    }

    $scope.GetTotalAreaDetails = function () {
        $scope.Totalareadet = [];
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.GetTotalAreaDetails(dataobj).then(function (result) {

            if (result.data) {
                $scope.Totalareadet = result.data;
                progress(0, '', false);
            }
        });
    }

    $scope.GetSeatingCapacity = function () {
        progress(0, 'Loading...', false);
        $scope.seatingCapacity = [];
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        MaploaderService.GetSeatingCapacity(dataobj).then(function (result) {
            if (result.data) {
                $scope.seatingCapacity = result.data;
                progress(0, '', false);
            }
        });
    }

    $scope.GetSeatingArea = function () {
        progress(0, 'Loading...', false);
        $scope.seatingCapacity = [];
        var dataobj = { flr_code: $scope.SearchSpace.Floor[0].FLR_CODE, twr_code: $scope.SearchSpace.Tower[0].TWR_CODE, lcm_code: $scope.SearchSpace.Location[0].LCM_CODE };
        var themeobj = { Item: 'spacetype', flr_code: $scope.SearchSpace.Floor[0].FLR_CODE };
        $.post(UtilityService.path + '/api/KotakMaploader/GetLegendsCount', themeobj, function (result) {
            $scope.$apply(function () {
                if ($scope.SpcCountArea.length == 0)
                    $scope.SpcCountArea = result.Table;
            });
        });
    }

    
    $scope.ShowEmponMap = function () {
       
        var marker = _.find($scope.kiosk, { SST_CODE: 'KIS' });
        var chairicon = new L.icon({ iconUrl: '/images/marker.png', iconSize: [25, 32] });
        var empMarker = L.marker(L.latLng(marker.x, marker.y), {
            icon: chairicon,
            draggable: false
        }).addTo(map);
        if (map.hasLayer($scope.MarkerLblLayer))
            map.removeLayer($scope.MarkerLblLayer);

        //setTimeout(function () {
        //    if (empMarker) { // check
        //        map.removeLayer(empMarker); // remove
        //        if (!map.hasLayer($scope.MarkerLblLayer)) {
        //            map.addLayer($scope.MarkerLblLayer);
        //        }
        //    }
        //}, 20000);

        if ($scope.ZoomLvl >= map.getZoom()) {
            //map.fitBounds($scope.bounds);
            setTimeout(function () {
                map.setView(L.latLng(marker.x, marker.y), map.getZoom() + 2);
            }, 200);
        }
        setTimeout(function () { map.panTo(L.latLng(marker.x, marker.y)); }, 400);


    }
   
   
    $scope.set_color = function (color) {
        return { 'background-color': color };
    }

    var zoomtreshold = 0;

    map.on('zoomend', function () {
        zoomtreshold = 0;
        map.addLayer($scope.MarkerMeetingLblLayer);

        if ($scope.ZoomLvl >= 14)
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 2;
        else if ($scope.ZoomLvl >= 12)
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 3;
        else if ($scope.ZoomLvl >= 9)
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 2;
        else
            zoomtreshold = zoomtreshold + $scope.ZoomLvl + 2;
        setTimeout(function () {
            if (map.getZoom() < zoomtreshold) {
                if (map.hasLayer($scope.MarkerLblLayer)) {
                    map.removeLayer($scope.MarkerLblLayer);
                } else {
                    console.log("no point layer active");
                }
            }

            if (map.getZoom() >= zoomtreshold) {
                if (map.hasLayer($scope.MarkerLblLayer)) {
                    console.log("layer already added");
                } else {
                    map.addLayer($scope.MarkerLblLayer);
                }
            }
        }, 200);

    });

    $scope.getShiftName = function (shcode) {
        var shift = _.find($scope.Shifts, { SH_CODE: shcode });
        if (shift)
            return shift.SH_NAME;
        return "";
    }

    var classHighlight = 'highlight';
    var $thumbs = $('.thumbnail').click(function (e) {
        e.preventDefault();
        $thumbs.removeClass(classHighlight);
        $(this).addClass(classHighlight);
    });

    function reloadMarkers() {
        $scope.Markerdata.clearLayers();
        angular.forEach($scope.Markers, function (marker, key) {
            marker.ticked = false;
            switch (marker.STATUS) {
                case 1: marker.setStyle(VacantStyle);
                    break;
                case 1002: marker.setStyle(AllocateStyle);
                    break;
                case 1004: marker.setStyle(OccupiedStyle);
                    break;
                case 1003: marker.setStyle(AllocateStyle);
                    break;
                case 1052: marker.setStyle(BlockStyle);
                    break;
                default: marker.setStyle(VacantStyle);
                    break;
            }

            if (marker.AUR_LEAVE_STATUS == 'Y') {
                switch (marker.AUR_LEAVE_STATUS) {
                    case 'Y': marker.setStyle(LeaveStyle);
                        break;
                }
            }
            marker.bindLabel(marker.SPC_DESC, {
                noHide: false,
                direction: 'auto',
                zIndexOffset: 2000
            });//.setOpacity(1);
            $scope.Markerdata.addLayer(marker);
        });
    }
   
});


function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return urlparam[1];
        }
    }
}
