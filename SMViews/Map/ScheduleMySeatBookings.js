﻿app.service("ScheduleMySeatBookingsService", function ($http, $q, UtilityService) {
    this.searchSpaces = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ScheduleMySeatBookings/SearchSpaces', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.allocateSeats = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ScheduleMySeatBookings/allocateSeats', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getVLANS = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MaploaderAPI/getVLANS')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.getEmployeeSpaceDetails = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ScheduleMySeatBookings/getEmployeeSpaceDetails', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


});

app.controller('ScheduleMySeatBookingsController', ['$scope', '$q', 'UtilityService', 'ScheduleMySeatBookingsService', 'SpaceRequisitionService', '$filter', function ($scope, $q, UtilityService, ScheduleMySeatBookingsService, SpaceRequisitionService, $filter) {
    $scope.ScheduleMySeatBookings = {};
    $scope.frmScheduleMySeatBookings = [];
    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Floors = [];
    $scope.MonthDates = [];
    $scope.AlterDays = [];
    $scope.Weekdays = [];
    $scope.SpaceIds = [];
    $scope.EmployeeDetails = [];
    $scope.SelectedDates = [];
    $scope.SelectAlterDays = false;
    $scope.SelectWeekly = false;
    $scope.SelectMonth = false;
    $scope.Form2Enable = false;
    $scope.SelectionType = false;
    $scope.Checing = false;
    $scope.Griddata = false;
    $scope.VLANS = [];

    UtilityService.getSysPreferences().then(function (response) {

        if (response.data != null) {
            $scope.SysPref = response.data;
        }

        if (window.innerWidth <= 780) {

            $scope.IsMobile = true;
            $scope.IsWeb = false;
        }
        else {
            $scope.gridOptions.api.setRowData(response.data);
            $scope.IsMobile = false;
            $scope.IsWeb = true;
        }
    });

    UtilityService.getAurNames().then(function (response) {

        if (response.data != null) {
            $scope.AurNames = [];
            //$scope.AurNames.push({ "AUR_ID": "", "AUR_NAME": "", "Cost_Center_Code": "", "Cost_Center_Name": "", "VER_CODE": "", "VER_NAME": "" })
            angular.forEach(response.data, function (value, key) {
                $scope.AurNames.push({ "AUR_ID": value.AUR_ID, "AUR_NAME": value.AUR_NAME, "Cost_Center_Code": value.Cost_Center_Code, "Cost_Center_Name": value.Cost_Center_Name, "VER_CODE": value.VER_CODE, "VER_NAME": value.VER_NAME, ticked: false });
            })
        }

        $scope.EmployeeDetails = $scope.AurNames;
        _.forEach($scope.AurNames, function (o) {
            if (o.AUR_ID == aur_id) {
                return o.ticked = true;
                //$scope.EmployeeDetails.push(o);
            }
        })
    });

    UtilityService.GetRoleAndReportingManger().then(function (response) {
        if (response.data != null) {
            $scope.GetRoleAndRM = response.data;
        }
    });
    ScheduleMySeatBookingsService.getVLANS().then(function (response) {
        $scope.VLANData = response.data;
        $scope.ScheduleMySeatBookings.VLAN = $scope.VLANData[0].VL_Code;

        _.forEach($scope.VLANData, function (o) {
            if (o.VL_Code == 'Def-VLAN') {
                return o.ticked = true;
            }
        });

    }, function (error) {
        console.log(error);
    });


    $scope.columDefsAlloc = [

        { headerName: "Delete", cellclass: "grid-align", suppressMenu: true, pinned: 'left', template: '<a ng-click="DeleteFromGrid(data)"><i class="fa fa fa-trash" style="color:red" aria-hidden="true"></i></a>', width: 50, cellStyle: { 'text-align': "center" } },
        { headerName: "From Date", field: "FROM_DATE", width: 150, cellClass: 'grid-align' },
        { headerName: "To Date", field: "TO_DATE", width: 150, cellClass: 'grid-align' }
    ];

    $scope.DeleteFromGrid = function (data) {

        var d = _.find($scope.FinalData, function (o) { return o.FROM_DATE == data.FROM_DATE && o.TO_DATE == data.TO_DATE; });
        d.ticked = false;
        $scope.gridOptions.api.setRowData([]);
        $scope.gridOptions.api.setRowData(_.filter(_.orderBy($scope.FinalData, 'FROM_DATE', 'asc'), function (o) { return o.ticked == true; }));
    };


    //var details = function (data) {
    //    $scope.temp = data;
    //    var i = 0;
    //    var datefiltervals = [];
    //    while (i <= $scope.temp.length - 1) {

    //        var start = moment($scope.temp[i].Id);
    //        var end = moment($scope.temp[i].Id);
    //        var startDate = moment($scope.temp[i].Id);

    //        if (i < $scope.temp.length && i < $scope.temp.length - 1) {
    //            var nextDate = moment($scope.temp[i + 1].Id);
    //            while ((moment(startDate).add(1, 'days').format('MM/DD/YYYY') === nextDate.format('MM/DD/YYYY')) && i < $scope.temp.length - 1) {
    //                i++;
    //                startDate = moment($scope.temp[i].Id);
    //                end = nextDate;
    //                if (i < $scope.temp.length - 1) {
    //                    nextDate = moment($scope.temp[i + 1].Id);
    //                }
    //                else {
    //                    break;
    //                }
    //            }
    //            datefiltervals.push({ "startDate": moment(start).format('MM/DD/YYYY'), "endDate": moment(end).format('MM/DD/YYYY') });
    //            i++;
    //        }
    //        else {
    //            datefiltervals.push({ "startDate": moment(start).format('MM/DD/YYYY'), "endDate": moment(end).format('MM/DD/YYYY') });
    //            i++;
    //        }
    //    }

    //    return datefiltervals;
    //};


    $scope.gridOptions = {
        columnDefs: $scope.columDefsAlloc,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false
    };
    $scope.Clear = function () {
        $scope.Griddata = false;

        //$scope.ScheduleMySeatBookings.FROM_DATE = '';
        //$scope.ScheduleMySeatBookings.TO_DATE = '';

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Floors, function (value, key) {
            value.ticked = false;
        });

    }


    $scope.binddata = function (data) {

        var selectedData
        selectedData = _.find($scope.AurNames, { AUR_ID: aur_id });
        if (selectedData != undefined) {
            $scope.AUR_ID = selectedData.AUR_ID;
            $scope.AUR_NAME = selectedData.AUR_NAME;
            $scope.VERTICAL = selectedData.VER_CODE;
            $scope.VER_NAME = selectedData.VER_NAME;
            $scope.Cost_Center_Code = selectedData.Cost_Center_Code;
            $scope.Cost_Center_Name = selectedData.Cost_Center_Name;
        }
    }


    $scope.LoadDetails = function () {
        progress(0, 'Loading...', true);

        UtilityService.getCountires(2).then(function (response) {

            if (response.data != null) {
                $scope.Country = response.data;

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;

                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;


                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;

                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                progress(0, '', false);
                                            }
                                        });

                                    }
                                });
                            }
                        });

                    }
                });
            }
        });
    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.ScheduleMySeatBookings.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });

    }

    $scope.locSelectAll = function () {
        $scope.ScheduleMySeatBookings.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.ScheduleMySeatBookings.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        $scope.ScheduleMySeatBookings.Locations = [];
        angular.forEach($scope.Towers, function (value, key) {

            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });

            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ScheduleMySeatBookings.Locations.push(lcm);

            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.ScheduleMySeatBookings.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.FloorChange = function () {


        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });



        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ScheduleMySeatBookings.Locations.push(lcm);
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.ScheduleMySeatBookings.Towers.push(twr);

            }
        });

        SpaceRequisitionService.getShifts($scope.ScheduleMySeatBookings.Locations).then(function (response) {
            $scope.Shifts = response.data;
            $scope.ScheduleMySeatBookings.Shift = $scope.Shifts[0].SH_CODE;
            _.forEach($scope.Shifts, function (o) {
                if (o.SH_CODE == $scope.ScheduleMySeatBookings.Shift) {
                    return o.ticked = true;
                }
            });



        }, function (error) {
            console.log(error);
        });
    }
    $scope.floorChangeAll = function () {
        $scope.ScheduleMySeatBookings.Floors = $scope.Floors;
        $scope.FloorChange();
    }

    setTimeout(function () {
        $scope.LoadDetails();
    }, 200);

    $scope.SearchSpaces = function () {

        if ($scope.ScheduleMySeatBookings.FROM_DATE == undefined || $scope.ScheduleMySeatBookings.TO_DATE == undefined) {
            showNotification('error', 8, 'bottom-right', 'Please Select From Date and To Date');
            return;
        }

        if (moment($scope.ScheduleMySeatBookings.FROM_DATE) > moment($scope.ScheduleMySeatBookings.TO_DATE)) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }

        $scope.CheckType('AlterDays');

        progress(0, 'Loading...', true);
        floorslst = _.filter($scope.ScheduleMySeatBookings.Floors, function (o) { return o.ticked == true; }).map(function (x) { return x.FLR_CODE; }).join(',');

        var searchObj = { flrlst: floorslst, FROM_DATE: $scope.ScheduleMySeatBookings.FROM_DATE, TO_DATE: $scope.ScheduleMySeatBookings.TO_DATE };
        ScheduleMySeatBookingsService.searchSpaces(searchObj).then(function (response) {
            if (response != null) {
                $scope.spaceDetails = response[1];
                $scope.SpaceId = [];
                _.forEach($scope.spaceDetails, function (key, value) {
                    $scope.SpaceId.push({ 'SPC_ID': key.SPC_ID, 'SPC_NAME': key.SPC_NAME });
                });
                $scope.SpaceIds = $scope.SpaceId;
                $scope.Form2Enable = true;

                progress(0, '', false);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            progress(0, '', false);
        });
       
    }

    $scope.CheckType = function (value) {


        $scope.Checing = true;
        if (value == 'AlterDays') {
            $scope.SelectAlterDays = true;
            $scope.SelectMonth = false;
            $scope.SelectWeekly = false;


            var enumerateDaysBetweenDates = function (startDate, endDate) {
                var dates = [];
                //startDate = moment(startDate).add(1, 'days');
                startDate = moment(startDate);
                endDate = moment(endDate).add(1, 'days');
                while (startDate.format('M/D/YYYY') !== endDate.format('M/D/YYYY')) {
                    dates.push({ "Id": startDate.format('M/D/YYYY'), "Date": startDate.format("ddd, MMM Do YYYY") });
                    startDate = moment(startDate).add(1, 'days');
                }
                return dates;
            };
            var date = enumerateDaysBetweenDates($scope.ScheduleMySeatBookings.FROM_DATE, $scope.ScheduleMySeatBookings.TO_DATE);
            $scope.AlterDays = date;
        }
        else if (value == 'Weekly') {
            $scope.SelectAlterDays = false;
            $scope.SelectMonth = false;
            $scope.SelectWeekly = true;
            $scope.Weeks = [{
                "Id": "Monday",
                "Name": "Monday"
            }, {
                "Id": "Tuesday",
                "Name": "Tuesday"
            },
            {
                "Id": "Wednesday",
                "Name": "Wednesday"
            }, {
                "Id": "Thursday",
                "Name": "Thursday"
            }, {
                "Id": "Friday",
                "Name": "Friday"
            }, {
                "Id": "Saturday",
                "Name": "Saturday"
            }, {
                "Id": "Sunday",
                "Name": "Sunday"
            }
            ];

            $scope.Weekdays = $scope.Weeks;
        }

        else {
            $scope.SelectAlterDays = false;
            $scope.SelectMonth = false;
            $scope.SelectWeekly = false;
        }
    }

    $scope.Check = function () {


        var params = { AUR_ID: $scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_ID };

        ScheduleMySeatBookingsService.getEmployeeSpaceDetails(params).then(function (response) {
            if (response[0].length != 0) {
                $scope.EmpAllocSeats = [];
                $scope.EmpAllocSeats = response[0];
            }
            else {
                $scope.EmpAllocSeats = [];
            }

        });



        $scope.binddata($scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_ID);

        $scope.Griddata = true;
        $scope.FinalData = [];
        if ($scope.value == 'Daily') {

            //swethan start

            var start = $scope.ScheduleMySeatBookings.FROM_DATE;
            var end = $scope.ScheduleMySeatBookings.TO_DATE;

            for ($scope.FinalData = [], dt = new Date(start); dt <= new Date(end); dt.setDate(dt.getDate() + 1)) {
                $scope.FinalData.push({
                    "FROM_DATE": moment(dt).format('MM/DD/YYYY'),
                    "TO_DATE": moment(dt).format('MM/DD/YYYY'),
                    ticked: true
                });
            };

            //$scope.FinalData.push({
            //    "FROM_DATE": $scope.ScheduleMySeatBookings.FROM_DATE,
            //    "TO_DATE": $scope.ScheduleMySeatBookings.TO_DATE,
            //    ticked: true
            //});
            //swethan end
        }
        else if ($scope.value == 'Weekly') {
            if ($scope.ScheduleMySeatBookings.WeeklyDays.length != 0) {
                var FetchDatesBasedOnWeek = function (startDate, endDate) {
                    var dates = [];
                    var start = startDate, end = endDate;
                    _.forEach($scope.ScheduleMySeatBookings.WeeklyDays, function (key, value) {
                        var week = key.Id;
                        startDate = moment(start);
                        endDate = moment(end).add(1, 'days');
                        while (startDate.format('M/D/YYYY') !== endDate.format('M/D/YYYY')) {
                            if (week == moment(startDate).format('dddd')) {
                                dates.push({ "Sort": moment(startDate).format('M/D/YYYY'), "Id": startDate.format('M/D/YYYY'), "Date": startDate.format("ddd, MMM Do YYYY") });
                            }
                            startDate = moment(startDate).add(1, 'days');
                        }

                    });
                    return _.orderBy(dates, 'Id', 'asc');
                };

                $scope.SelectedDates = FetchDatesBasedOnWeek($scope.ScheduleMySeatBookings.FROM_DATE, $scope.ScheduleMySeatBookings.TO_DATE);
                //swethan start
                _.forEach($scope.SelectedDates, function (key, value) {
                    $scope.FinalData.push({
                        "FROM_DATE": moment(key.Id).format("MM/DD/YYYY"),
                        "TO_DATE": moment(key.Id).format("MM/DD/YYYY"),
                        ticked: true
                    });
                });
                // $scope.FinalDates = details($scope.SelectedDates);

                //swethan end
            }
            _.forEach($scope.FinalDates, function (key, value) {
                $scope.FinalData.push({
                    "FROM_DATE": key.startDate, "TO_DATE": key.endDate, ticked: true
                });
            });
        }

        else if ($scope.value == 'AlterDays') {

            //swethan start
            _.forEach($scope.ScheduleMySeatBookings.AlterDays, function (key, value) {
                $scope.FinalData.push({
                    "FROM_DATE": moment(key.Id).format("MM/DD/YYYY"),
                    "TO_DATE": moment(key.Id).format("MM/DD/YYYY"),
                    ticked: true
                });
            });

            //$scope.FinalDates = details($scope.ScheduleMySeatBookings.AlterDays);
            //_.forEach($scope.FinalDates, function (key, value) {
            //    $scope.FinalData.push({
            //        "FROM_DATE": key.startDate, "TO_DATE": key.endDate, ticked: true
            //    });
            //});

            //swethan end
        }

        if ($scope.FinalData != null) {
            $scope.gridOptions.api.setRowData([]);
            $scope.gridOptions.api.setRowData(_.filter(_.orderBy($scope.FinalData, 'FROM_DATE', 'asc'), function (o) { return o.ticked == true; }));
        }

    }

    $scope.SpaceIdSelected = function () {
        $scope.RequiredShifts = [];
        $scope.Griddata = false;
        $scope.selectedSpaceID = $scope.ScheduleMySeatBookings.SpaceId[0].SPC_ID;
        $scope.SeatType = _.find($scope.spaceDetails, function (o) { return o.SPC_ID == $scope.selectedSpaceID });

        //_.forEach($scope.Shifts, function (o) {
        //    if (o.SH_SEAT_TYPE == $scope.SeatType.SPACE_TYPE) {
        //        $scope.RequiredShifts.push(o);
        //    }

        //if ($scope.SeatType.SPACE_TYPE == 4) {
        //    $scope.gridOptions.columnApi.setColumnVisible('SH_CODE', false);
        //    $scope.gridOptions.columnApi.setColumnVisible('FROM_TIME', true);
        //    $scope.gridOptions.columnApi.setColumnVisible('TO_TIME', true);
        //}
        //else {
        //    $scope.gridOptions.columnApi.setColumnVisible('SH_CODE', true);
        //    $scope.gridOptions.columnApi.setColumnVisible('FROM_TIME', false);
        //    $scope.gridOptions.columnApi.setColumnVisible('TO_TIME', false);
        //}

        //});


        $scope.SelectionType = true;

    };

    $scope.EmployeeIdSelected = function () {

        _.forEach($scope.AurNames, function (o) {

            if (o.AUR_ID == $scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_ID) {
                return o.ticked = true;
                //$scope.EmployeeDetails.push(o);
            }
        });

        $scope.Griddata = false;
    }


    var number = 0;
    function checkDates() {
        number = 0;
        _.forEach($scope.EmpAllocSeats, function (key, value) {

            var from = new Date(key.FROM_DATE);
            var to = new Date(key.TO_DATE);
            _.forEach($scope.FinalData, function (key1, value1) {
                var fromcheck = new Date(key1.FROM_DATE);
                var tocheck = new Date(key1.TO_DATE);
                if ((fromcheck >= from && fromcheck <= to) || (tocheck >= from && tocheck <= to)) {
                    progress(0, '', false);
                    number = number + 1;
                }
            });

        });
    };


    $scope.scheduleSeats = function () {
        progress(0, 'Loading...', true);

        $scope.selectedData = [];
        checkDates();


        if ($scope.gridOptions.rowData.length != 0) {

            _.forEach($scope.gridOptions.rowData, function (o) {


                if ($scope.ScheduleMySeatBookings.Shift[0].SH_CODE == undefined && $scope.SeatType.SPACE_TYPE != 4) {
                    progress(0, '', false);
                    flag = true;
                    progress(0, 'Loading...', false);
                    showNotification('error', 8, 'bottom-right', 'Please Select Shift');
                    return;
                } else if ($scope.SeatType.SPACE_TYPE == 4 /*&& (o.FROM_TIME == undefined || o.TO_TIME == undefined)*/) {
                    progress(0, '', false);
                    flag = true;
                    progress(0, 'Loading...', false);
                    showNotification('error', 8, 'bottom-right', 'Please Select Shift Timings');
                    return;
                }

                else if ($scope.ScheduleMySeatBookings.VLAN.length < 1) {
                    progress(0, '', false);
                    flag = true;
                    progress(0, 'Loading...', false);
                    showNotification('error', 8, 'bottom-right', 'Please Select VLAN');
                    return;
                }
                else {


                    $scope.selectedData.push({
                        "FROM_DATE": o.FROM_DATE, "TO_DATE": o.TO_DATE, "SH_CODE": $scope.ScheduleMySeatBookings.Shift[0].SH_CODE,
                        "SPC_ID": $scope.ScheduleMySeatBookings.SpaceId[0].SPC_ID,
                        //"AUR_ID": $scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_ID,
                        "AUR_ID": btoa($scope.ScheduleMySeatBookings.EmployeeDetails[0].AUR_ID),
                        "Cost_Center_Code": $scope.Cost_Center_Code,
                        "Cost_Center_Name": $scope.Cost_Center_Name, "SPACE_TYPE": "1", "STACHECK": 16, "STATUS": 1004,
                        "VERTICAL": $scope.VERTICAL, "VER_NAME": $scope.VER_NAME, "VL_Code": $scope.ScheduleMySeatBookings.VLAN[0].VL_Code
                    });


                }
            });

            if (number != 0) {
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', 'Selected dates already Scheduled');
            }

            else if ($scope.selectedData.length != 0) {
                ScheduleMySeatBookingsService.allocateSeats($scope.selectedData).then(function (response) {
                    if (response != 1) {
                        progress(0, 'Loading...', false);
                        showNotification('error', 8, 'bottom-right', 'Something went wrong');
                    }
                    else {
                        progress(0, 'Loading...', false);
                        showNotification('success', 8, 'bottom-right', 'Selected Spaces Allocated Successfully');
                        $scope.Griddata = false;
                        $scope.Form2Enable = false;
                    }
                });
            }

        } else {
            progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', 'Please select the dates');
        }
    }

}]);