﻿<%@ Page Language="VB" AutoEventWireup="false" Title="User Floor List" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
   <script defer src="http://code.angularjs.org/1.2.0rc1/angular-animate.min.js"></script>
    <script defer src="http://code.angularjs.org/1.2.0rc1/angular-touch.min.js"></script>
    <script defer src="Responsive.min.js"></script>
    <style>      
* {
    margin: 0;
    padding: 0;
}
.al-main
{
        margin-left: -44px !important;
}


.arrow {
    cursor: pointer;
    display: block;
    height: 64px;
    margin-top: -35px;
    outline: medium none;
    position: absolute;
    top: 50%;
    width: 64px;
    z-index: 5;
}
.arrow.prev {
    background-image: url("../../images/prev.png");
    left: -47px;
    opacity: 0.2;
    transition: all 0.2s linear 0s;
    background-size: 42px;
    background-repeat: no-repeat;
}
.arrow.next {
   background-image: url(../../images/next.png);
    opacity: 0.2;
    right: -97px;
    transition: all 0.2s linear 0s;
    background-size: 42px;
    background-repeat: no-repeat;
}
.arrow.prev:hover{
    opacity:1;
}
.arrow.next:hover{
    opacity:1;
}



.slider {
    border: 15px solid #FFFFFF;
    border-radius: 5px;
    height:430px;
    margin: -36px auto;
    position: relative;
    width: 500px;
        right: 13px;
    -webkit-perspective: 1000px;
    -moz-perspective: 1000px;
    -ms-perspective: 1000px;
    -o-perspective: 1000px;
    perspective: 1000px;

    -webkit-transform-style: preserve-3d;
    -moz-transform-style: preserve-3d;
    -ms-transform-style: preserve-3d;
    -o-transform-style: preserve-3d;
    transform-style: preserve-3d;
}
.slide {
    position: absolute;
    top: 0;
    left: 0;
}
.slide.ng-hide-add {
    opacity:1;

    -webkit-transition:1s linear all;
    -moz-transition:1s linear all;
    -o-transition:1s linear all;
    transition:1s linear all;

    -webkit-transform: rotateX(50deg) rotateY(30deg);
    -moz-transform: rotateX(50deg) rotateY(30deg);
    -ms-transform: rotateX(50deg) rotateY(30deg);
    -o-transform: rotateX(50deg) rotateY(30deg);
    transform: rotateX(50deg) rotateY(30deg);

    -webkit-transform-origin: right top 0;
    -moz-transform-origin: right top 0;
    -ms-transform-origin: right top 0;
    -o-transform-origin: right top 0;
    transform-origin: right top 0;
}
.slide.ng-hide-add.ng-hide-add-active {
    opacity:0;
}
.slide.ng-hide-remove {
    -webkit-transition:1s linear all;
    -moz-transition:1s linear all;
    -o-transition:1s linear all;
    transition:1s linear all;

    display:block!important;
    opacity:0;
}
.slide, .slide.ng-hide-remove.ng-hide-remove-active {
    opacity:1;
}

@media (max-width: 900px) {
 
    .slider {
        height: 312px;
        width: 500px;
    }
    .slider .slide {
        width: 100%;
    }
}
@media (max-width: 550px) {
  
    .slider {
        height: 188px;
        width: 300px;
    }
    .slider .slide {
        width: 100%;
    }
   
}
    </style>
</head>
<body>
    <div class="animsition" data-ng-controller="FloorLstController">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">View All Maps
                            </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <%-- <div class="clearfix">
                                <div class="box-footer text-right">
                                    <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                                </div>
                            </div>--%>
                            <br />

                            <form id="form1">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div style="height: 300px;">
                                            <input id="txtCountFilter" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%" />
                                            <div data-ag-grid="gridOptions" style="height: 90%;" class="ag-blue"></div>
                                        </div>
                                    </div>
                                </div>
                                
                            </form>
                            

                            
                        </div>
                        
                        <div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <%--<h4 class="modal-title">Tower Images</h4>--%>
        </div>
        <div class="modal-body">
         <div class="container slider">

        <!-- enumerate all photos -->
        <img data-ng-repeat="slideOptions in slideOptionsData" class="slide" data-ng-swipe-right="showPrev()" data-ng-swipe-left="showNext()" data-ng-show="isActive($index)" data-ng-src="{{slideOptions.TWI_UPLOAD_PATH}}" width="500" height ="350"/>

        <!-- prev / next controls -->
        <a class="arrow prev" href="#" data-ng-click="showPrev()"></a>
        <a class="arrow next" href="#" data-ng-click="showNext()"></a>

        <!-- extra navigation controls -->
        <%--<ul class="nav">
            <li ng-repeat="slideOptions in slideOptionsData" ng-class="{'active':isActive($index)}">
                <img src="{{slideOptions.TWI_UPLOAD_PATH}}"  ng-click="showPhoto($index);" />
            </li>
        </ul>--%>

    </div>
        </div>
        <div class="modal-footer">
         <%-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--%>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script defer src="../Utility.js"></script>
    <script defer>

        app.service('FloorLstService', function ($http, $q, UtilityService) {

            this.getFloorLst = function (result) {
                var deferred = $q.defer();
                return $http.get(UtilityService.path + '/api/MaploaderAPI/GetFloorLst')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
            this.GetImageList = function (Image) {
                var deferred = $q.defer();
                return $http.get(UtilityService.path + '/api/MaploaderAPI/GetImageList?Image=' + Image + ' ')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

        });
        app.controller('FloorLstController', function ($scope, $q, FloorLstService, $filter) {
            $scope.slideOptionsData = [];
            $scope.columDefsAlloc = [
            { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', filter: 'set' },
            { headerName: "Location", field: "LCM_NAME", cellClass: "grid-align" },
            { headerName: "Tower/ Building", field: "TWR_NAME", cellClass: "grid-align" },
            { headerName: "Floor", cellClass: "grid-align", field: "FLR_NAME" },
            {
                headerName: "View Map", cellClass: "grid-align", width: 150, filter: 'set',
                template: "<a href='../../SMViews/Map/KotakMaploader.aspx?lcm_code={{data.LCM_CODE}}&twr_code={{data.TWR_CODE}}&flr_code={{data.FLR_CODE}}'>View Map</a>"
            },

              //{ headerName: "View Image", field: "Image", width: 90, cellRenderer: ViewImageCellRendererFunc },
            ];
            $scope._Index = 0;
            // if a current image is the same as requested image
            $scope.isActive = function (index) {
                return $scope._Index === index;
            };
            function ImageClicked(Image) {
                FloorLstService.GetImageList(Image).then(function (response) {
                    if (response.data != null) {
                        console.log(response.data);
                        $scope.slideOptionsData = response.data;
                        //_.forEach($scope.slideOptionsData, function (n, key) {
                        //    console.log(n);
                        //        $scope.slideOptionsData.push(n);

                        //});

                    }
                    else
                        showNotification('error', 8, 'bottom-right', response.Message);
                });
            }
            function ViewImageCellRendererFunc(params) {
                params.$scope.ImageClicked = ImageClicked;
                return '<a ng-click="ImageClicked(data.TWR_CODE)" id="lnkView"  data-toggle="modal" data-target="#myModal">View Image</a>';
            }

            $scope.gridOptions = {
                columnDefs: $scope.columDefsAlloc,
                rowData: null,
                cellClass: 'grid-align',
                angularCompileRows: true,
                enableColResize: true,
                enableCellSelection: false,
                groupHideGroupColumns: true

            };

            FloorLstService.getFloorLst().then(function (response) {
                if (response.data != null) {
                    $scope.gridOptions.api.setRowData(response.data);
                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            });

            $("#txtCountFilter").change(function () {
                onReq_SelSpacesFilterChanged($(this).val());
            }).keydown(function () {
                onReq_SelSpacesFilterChanged($(this).val());
            }).keyup(function () {
                onReq_SelSpacesFilterChanged($(this).val());
            }).bind('paste', function () {
                onReq_SelSpacesFilterChanged($(this).val());
            });

            function onReq_SelSpacesFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
           
            // initial image index
            $scope._Index = 0;

            // if a current image is the same as requested image
            $scope.isActive = function (index) {
                return $scope._Index === index;
            };

            // show prev image
            $scope.showPrev = function () {
                $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.slideOptionsData.length - 1;
            };

            // show next image
            $scope.showNext = function () {
                $scope._Index = ($scope._Index < $scope.slideOptionsData.length - 1) ? ++$scope._Index : 0;
            };

            // show a certain image
            $scope.showPhoto = function (index) {
                $scope._Index = index;
            };

        });
    </script>
</body>
</html>

