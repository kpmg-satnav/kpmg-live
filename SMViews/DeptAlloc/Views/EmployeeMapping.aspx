﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="../../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
            width: 355px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>

</head>
<body data-ng-controller="EmployeeMappingController" class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Employee Mapping" ba-panel-class="with-scroll">

            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Employee Mapping</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <div data-ng-show="Viewstatus==0">
                        <form role="form" id="Form1" name="frmApproveAll" data-valid-submit="ApproveAllReq()" novalidate>
                            <h4>Employee Mapping Requisitions</h4>
                            <div>
                                <input id="EmpReqFilter" placeholder="Filter by any..." type="text" style="width: 25%;" />
                                <div style="height: 400px">
                                    <div data-ag-grid="EmpMapRequisitions" style="height: 100%;" class="ag-blue"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div data-ng-show="Viewstatus==1">

                        <div class="clearfix">

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label><b>Request ID :</b> </label>
                                    {{currentblkReq.SRN_REQ_ID}}
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label><b>Requested By :</b> </label>
                                    {{currentblkReq.AUR_KNOWN_AS}}
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label><b>Requested Date :</b> </label>
                                    {{currentblkReq.SRN_REQ_DATE| date: dateformat}}
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 ">
                                <div class="form-group">
                                    <label><b>Status :</b> </label>
                                    {{currentblkReq.STA_DESC}}
                                </div>
                            </div>

                        </div>

                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmEmpMapping.$submitted && frmEmpMapping.CNY_NAME.$invalid}">
                                    <label for="txtcode">Country <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="countrylist" data-output-model="SpaceAllocation.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                        data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="cnySelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="EnableStatus==0||EnableStatus == 1">
                                    </div>
                                </div>
                                <input type="text" data-ng-model="SpaceAllocation.selectedCountries" name="CNY_NAME" style="display: none" required="" />
                                <span id="message" class="error" data-ng-show="frmEmpMapping.$submitted && frmEmpMapping.CNY_NAME.$invalid" style="color: red">Please select country </span>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmEmpMapping.$submitted && frmEmpMapping.CTY_NAME.$invalid}">
                                    <label for="txtcode">City <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Citylst" data-output-model="SpaceAllocation.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                        data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="EnableStatus==0||EnableStatus == 1">
                                    </div>
                                </div>
                                <input type="text" data-ng-model="SpaceAllocation.selectedCities" name="CTY_NAME" style="display: none" required="" />
                                <span id="Span1" class="error" data-ng-show="frmEmpMapping.$submitted && frmEmpMapping.CTY_NAME.$invalid " style="color: red">Please select city </span>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmEmpMapping.$submitted && frmEmpMapping.LCM_NAME.$invalid}">
                                    <label for="txtcode">Location <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Locationlst" data-output-model="SpaceAllocation.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                        data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="EnableStatus == 0||EnableStatus == 1">
                                    </div>
                                </div>
                                <input type="text" data-ng-model="SpaceAllocation.selectedLocations" name="LCM_NAME" style="display: none" required="" />
                                <span id="Span2" class="error" data-ng-show="frmEmpMapping.$submitted && frmEmpMapping.LCM_NAME.$invalid" style="color: red">Please select location </span>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmEmpMapping.$submitted && frmEmpMapping.TWR_NAME.$invalid}">
                                    <label for="txtcode">Tower <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Towerlist" data-output-model="SpaceAllocation.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                        data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="EnableStatus == 0||EnableStatus == 1">
                                    </div>
                                </div>
                                <input type="text" data-ng-model="SpaceAllocation.selectedTowers" name="TWR_NAME" style="display: none" required="" />
                                <span id="Span3" class="error" data-ng-show="frmEmpMapping.$submitted && frmEmpMapping.TWR_NAME.$invalid" style="color: red">Please select tower </span>
                            </div>

                        </div>

                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmEmpMapping.$submitted && frmEmpMapping.FLR_NAME.$invalid}">
                                    <label for="txtcode">Floor <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Floorlist" data-output-model="SpaceAllocation.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                        data-on-item-click="FloorChange()" data-on-select-all="FloorChangeAll()" data-on-select-none="FloorSelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="EnableStatus == 0||EnableStatus == 1">
                                    </div>
                                </div>
                                <input type="text" data-ng-model="SpaceAllocation.selectedFloors" name="FLR_NAME" style="display: none" required="" />
                                <span id="Span4" class="error" data-ng-show="frmEmpMapping.$submitted && frmEmpMapping.FLR_NAME.$invalid" style="color: red">Please select floor </span>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmEmpMapping.$submitted && frmEmpMapping.VER_NAME.$invalid}">
                                    <label for="txtcode">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Verticallist" data-output-model="SpaceAllocation.selectedVerticals" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                        data-on-item-click="VerticalChange()" data-on-select-all="VerticalChange()" data-on-select-none="VerticalSelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="EnableStatus == 0||EnableStatus == 1">
                                    </div>
                                </div>
                                <input type="text" data-ng-model="SpaceAllocation.selectedVerticals" name="VER_NAME" style="display: none" required="" />
                                <span id="Span5" class="error" data-ng-show="frmEmpMapping.$submitted && frmEmpMapping.VER_NAME.$invalid" style="color: red">Please select  {{BsmDet.Parent |lowercase}} </span>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmEmpMapping.$submitted && frmEmpMapping.Cost_Center_Name.$invalid}">
                                    <label for="txtcode">{{BsmDet.Child}} <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Costcenterlist" data-output-model="SpaceAllocation.selectedCostcenters" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                        data-on-item-click="CostcenterChange()" data-on-select-all="CostcenterChange()" data-on-select-none="CostcenterSelectNone()" data-tick-property="ticked" max-labels="1" data-is-disabled="EnableStatus == 0||EnableStatus == 1">
                                    </div>
                                </div>
                                <input type="text" data-ng-model="SpaceAllocation.selectedCostcenters" name="Cost_Center_Name" style="display: none" required="" />
                                <span id="Span6" class="error" data-ng-show="frmEmpMapping.$submitted && frmEmpMapping.Cost_Center_Name.$invalid" style="color: red">Please select {{BsmDet.Child|lowercase}} </span>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmEmpMapping.$submitted && frmEmpMapping.SRN_FROM_DATE.$invalid}">
                                    <label class="control-label">From Date <span style="color: red;">*</span></label>
                                    <div class='input-group date' id='fromdate'>
                                        <input type="text" id="txtFrmDate" class="form-control" required="" placeholder="mm/dd/yyyy" name="SRN_FROM_DATE" data-ng-model="currentblkReq.SRN_FROM_DATE" data-ng-disabled="EnableStatus == 0||EnableStatus == 1" />
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmEmpMapping.$submitted && frmEmpMapping.SRN_TO_DATE.$invalid}">
                                    <label class="control-label">To Date <span style="color: red;">*</span></label>
                                    <div class='input-group date' id='todate'>
                                        <input type="text" id="txtTodate" class="form-control" required="" placeholder="mm/dd/yyyy" name="SRN_TO_DATE" data-ng-model="currentblkReq.SRN_TO_DATE" data-ng-disabled="EnableStatus == 0 ||EnableStatus == 1" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer text-right" data-ng-show="Viewstatus==1">
                                <input type="button" value="Back" class="btn btn-primary custom-button-color" data-ng-click="back()">
                            </div>
                        </div>

                        <form role="form" id="SelectedSpaces" name="frmSelectedSpaces" data-valid-submit="" data-ng-show="EnableStatus == 1" novalidate>
                            <div class="row">
                                <div class="col-md-12">
                                    <input id="EmpMapFilter" placeholder="Filter by any..." type="text" style="width: 25%;" />
                                    <div style="height: 250px">
                                        <div data-ag-grid="SpacesOptions" style="height: 100%; width: 100%" class="ag-blue"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <br />
                        <div class="clearfix">
                            <div class="row">
                                <div class="pull-left" style="padding-left: 25px">
                                    <div class="form-group" data-ng-class="{'has-error': frmCountReq.$submitted && frmCountReq.SRN_REQ_REM.$invalid}">
                                        <label class="control-label">Requestor Remarks <span style="color: red;">*</span></label>
                                        <%--<textarea rows="3" cols="50" name="SRN_REQ_REM" data-ng-model="currentblkReq.SRN_REQ_REM" required class="form-control" data-ng-disabled="EnableStatus == 0 ||EnableStatus == 1"></textarea>--%>
                                        <textarea rows="3" cols="50" name="SRN_REQ_REM" data-ng-model="currentblkReq.SRN_REQ_REM" required class="form-control" data-ng-disabled="EnableStatus == 0 ||EnableStatus == 1"></textarea>
                                        <span class="error" data-ng-show="frmCountReq.$submitted && frmCountReq.SRN_REQ_REM.$invalid">Please enter requestor remarks </span>
                                    </div>
                                </div>
                                <br />
                                <div class="row box-footer text-right" style="padding-right: 35px">
                                    <input type="button" id="Button1" data-ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                    <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="SaveSelectedSpaces()" />
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="historymodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Plot On the Map</h4>
                </div>

                <form role="form" name="form2" id="form3">
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <div isteven-multi-select data-input-model="MapFloors" selection-mode="single" helper-elements="filter" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                        data-on-item-click="FlrSectMap(data)" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <input type="button" id="Button2" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color" value="Proceed" />
                                </div>
                                <div class="col-md-4">
                                    Legends:
                                        <span class="label label-success pull-right">Available </span>
                                    <span class="label selectedseat pull-right">Selected Seats </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="main">
                                    <div id="leafletMap"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
        
    </script>
    <script defer src="../../Utility.min.js"></script>
    <script defer src="../Js/EmployeeMapping.js"></script>
    <script defer src="../Js/L1Approval.min.js"></script>
    <script defer src="../Js/SpaceRequisition.min.js"></script>
    <script defer src="../Js/SpaceAllocation.min.js"></script>
</body>
</html>
