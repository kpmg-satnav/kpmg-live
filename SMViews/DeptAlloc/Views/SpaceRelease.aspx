﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.css" rel="stylesheet" />
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />


    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>

</head>
<body data-ng-controller="SpaceReleaseController" class="amantra" onload="setDateVals()">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Space Release</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <div class="clearfix">
                        <div class="box-footer text-right">
                            <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                        </div>
                    </div>
                    <br />
                    <form role="form" id="spaceRelease" name="frmSpaceRelease" data-valid-submit="GetSpacesToRelease()" novalidate>
                        <div class="clearfix">

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceRelease.$submitted && frmSpaceRelease.CNY_NAME.$invalid}">
                                    <label>Country </label>
                                    <div isteven-multi-select data-input-model="Getcountry" data-output-model="SEM.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                        data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="cnySelectNone()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedCountries" name="CNY_NAME" style="display: none" />
                                    <span class="error" data-ng-show="frmSpaceRelease.$submitted && frmSpaceRelease.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceRelease.$submitted && frmSpaceRelease.CTY_NAME.$invalid}">
                                    <label>City <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Citylst" data-output-model="SEM.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                        data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedCities" name="CTY_NAME" style="display: none" />
                                    <span class="error" data-ng-show="frmSpaceRelease.$submitted && frmSpaceRelease.CTY_NAME.$invalid" style="color: red">Please select city </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceRelease.$submitted && frmSpaceRelease.LCM_NAME.$invalid}">
                                    <label>Location <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Locationlst" data-output-model="SEM.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                        data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedLocations" name="LCM_NAME" style="display: none" />
                                    <span class="error" data-ng-show="frmSpaceRelease.$submitted && frmSpaceRelease.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceRelease.$submitted && frmSpaceRelease.TWR_NAME.$invalid}">
                                    <label>Tower <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Towerlist" data-output-model="SEM.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                        data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedTowers" name="TWR_NAME" style="display: none" />
                                    <span class="error" data-ng-show="frmSpaceRelease.$submitted && frmSpaceRelease.TWR_NAME.$invalid" style="color: red">Please select tower </span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceRelease.$submitted && frmSpaceRelease.FLR_NAME.$invalid}">
                                    <label>Floor <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Floorlist" data-output-model="SEM.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                        data-on-item-click="FloorChange()" data-on-select-all="FloorChangeAll()" data-on-select-none="FloorSelectNone()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedFloors" name="FLR_NAME" style="display: none" />
                                    <span class="error" data-ng-show="frmSpaceRelease.$submitted && frmSpaceRelease.FLR_NAME.$invalid" style="color: red">Please select floor </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceRelease.$submitted && frmSpaceRelease.VER_NAME.$invalid}">
                                    <label>{{BsmDet.Parent}} </label>
                                    <div isteven-multi-select data-input-model="Verticallist" data-output-model="SEM.selectedVerticals" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                        data-on-item-click="VerticalChange()" data-on-select-all="VerticalChangeAll()" data-on-select-none="verticalSelectNone()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedVerticals" name="VER_NAME" style="display: none" />
                                    <span class="error" data-ng-show="frmSpaceRelease.$submitted && frmSpaceRelease.VER_NAME.$invalid" style="color: red">Please select {{BsmDet.Parent |lowercase}} </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceRelease.$submitted && frmSpaceRelease.Cost_Center_Name.$invalid}">
                                    <label>{{BsmDet.Child}} <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="CostCenterlist" data-output-model="SEM.selectedCostcenter" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                        data-on-item-click="CostCenterChange()" data-on-select-all="CostCenterChangeAll()" data-on-select-none="CostCenterSelectNone()" data-tick-property="ticked" max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="SEM.selectedCostcenter" name="Cost_Center_Name" style="display: none" />
                                    <span id="message" class="error" data-ng-show="frmSpaceRelease.$submitted && frmSpaceRelease.Cost_Center_Name.$invalid" style="color: red">Please select {{BsmDet.Child |lowercase}} </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceRelease.$submitted && frmSpaceRelease.FROM_DATE.$invalid}">
                                    <label>From Date </label>
                                    <div class="input-group date" id='fromdate'>
                                        <input type="text" class="form-control" data-ng-model="SEM.FROM_DATE" id="FROM_DATE" name="FROM_DATE" placeholder="mm/dd/yyyy" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="frmSpaceRelease.$submitted && frmSpaceRelease.FROM_DATE.$invalid" style="color: red">Please select from date </span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceRelease.$submitted && frmSpaceRelease.TO_DATE.$invalid}">
                                    <label>To Date </label>
                                    <div class="input-group date" id='todate'>
                                        <input type="text" class="form-control" data-ng-model="SEM.TO_DATE" id="TO_DATE" name="TO_DATE" placeholder="mm/dd/yyyy" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="frmSpaceRelease.$submitted && frmSpaceRelease.TO_DATE.$invalid" style="color: red">Please select to date </span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmSpaceRelease.$submitted && frmSpaceRelease.EMP_OR_DEPT.$invalid}">
                                    <label>Release From <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="SPACE_RELEASE" helper-elements="" data-output-model="release" button-label="icon release" item-label="icon release" tick-property="ticked" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="release" name="EMP_OR_DEPT" style="display: none" required="" />
                                    <span id="messageReleaseFrom" class="error" data-ng-show="frmSpaceRelease.$submitted && frmSpaceRelease.EMP_OR_DEPT.$invalid" style="color: red">Please select release from </span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Employee Id <span style="color: red;"></span></label>
                                    <%--<div data-ng-class="{'has-error': frmSpaceRelease.$submitted && frmEntityAdmin.ADM_Code.$invalid}">--%>
                                    <input type="text" class="form-control" name="EMP_ID" id="EMP_ID" maxlength="15" data-ng-model="SEM.EMP_ID" />&nbsp;
                                        <%--<span class="error" data-ng-show="frmEntityAdmin.$submitted && frmEntityAdmin.ADM_Code.$invalid" style="color: red">Please enter valid parent entity code </span>--%>
                                </div>
                            </div>

                        </div>
                        <div class="clearfix">
                            <div class="box-footer text-right">
                                <button type="submit" value="Search" class="btn btn-primary custom-button-color">Search</button>
                                <input type="button" id="btnNew" data-ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>
                    </form>
                    <br />

                    <form role="form" id="spaceReleaseDetails" name="frmSpaceReleaseDetails">
                        <div data-ng-show="SRShowGrid">
                            <input id="SpaceReleaseReqFilter" class="form-control" placeholder="Filter..." type="text" style="width: 25%" />
                            <div class="row">
                                <div class="col-md-12" style="height: 320px">
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 305px; width: auto"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <textarea style="height: 20%" data-ng-model="SSR_REQ_REM" class="form-control" placeholder="Remarks"></textarea>
                                </div>
                                <div class="col-md-4 text-right">
                                    <input type="button" id="btnviewinmap" ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                    <input type="submit" data-ng-click="ReleaseSelectedSpaces()" value="Release" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="historymodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Plot On the Map</h4>
                </div>

                <form role="form" name="form2" id="form3">
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <div isteven-multi-select data-input-model="MapFloors" selection-mode="single" helper-elements="filter" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                        data-on-item-click="FlrSectMap(data)" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <input type="button" id="Button1" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color" value="Proceed" />
                                </div>
                                <div class="col-md-4">
                                    Legends:
                                        <span class="label label-success pull-right">Available </span>
                                    <span class="label selectedseat pull-right">Selected Seats </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="main">
                                    <div id="leafletMap"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

        function setDateVals() {
            $('#FROM_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#TO_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        }

    </script>

    <script defer src="../../Utility.min.js"></script>
    <script defer src="../Js/SpaceRelease.js"></script>
</body>
</html>


