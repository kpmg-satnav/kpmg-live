﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/privillegechecking.aspx.cs" Inherits="PrivilegeChecking" %>


<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        span.error {
            color: red;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>
</head>

<body data-ng-controller="SpaceRequisitionController" class="amantra" onload="setDateVals()">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Space Requisition" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Space Requisition</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <div class="clearfix">
                        <div class="box-footer text-right">
                            <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                        </div>
                    </div>
                    <br />

                    <form id="Form1" name="frmSpaceRequisition" data-valid-submit="getSpaces()" novalidate>
                        <div class="row">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.CNY_NAME.$invalid}">
                                        <label class="control-label">Country <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Country" data-output-model="SpaceRequisition.Country" data-button-label="icon CNY_NAME"
                                            data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SpaceRequisition.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.CNY_NAME.$invalid">Please select country </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.CTY_NAME.$invalid}">
                                        <label class="control-label">City<span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="City" data-output-model="SpaceRequisition.City" data-button-label="icon CTY_NAME"
                                            data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SpaceRequisition.City[0]" name="CTY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.CTY_NAME.$invalid">Please select city </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.LCM_NAME.$invalid}">
                                        <label class="control-label">Location <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Locations" data-output-model="SpaceRequisition.Locations" data-button-label="icon LCM_NAME"
                                            data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SpaceRequisition.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.LCM_NAME.$invalid">Please select location </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.TWR_NAME.$invalid}">
                                        <label class="control-label">Tower <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Towers" data-output-model="SpaceRequisition.Towers" data-button-label="icon TWR_NAME "
                                            data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SpaceRequisition.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.TWR_NAME.$invalid">Please select tower </span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.FLR_NAME.$invalid}">
                                        <label class="control-label">Floor <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Floors" data-output-model="SpaceRequisition.Floors" data-button-label="icon FLR_NAME"
                                            data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SpaceRequisition.Floors[0]" name="FLR_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.FLR_NAME.$invalid">Please select floor </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.VER_NAME.$invalid}">
                                        <label class="control-label">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                        <div isteven-multi-select selection-mode="single" data-input-model="Verticals" data-output-model="SpaceRequisition.Verticals" data-button-label="icon VER_NAME"
                                            data-item-label=" icon VER_NAME maker" data-on-item-click="getCostcenterByVertical()" data-on-select-all="verticalChangeAll()" data-on-select-none="verticalSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SpaceRequisition.Verticals[0]" name="VER_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.VER_NAME.$invalid">Please select {{BsmDet.Parent |lowercase}} </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.Cost_Center_Name.$invalid}">
                                        <label class="control-label">{{BsmDet.Child}}<span style="color: red;">**</span></label>
                                        <div isteven-multi-select selection-mode="single" data-input-model="CostCenters" data-output-model="SpaceRequisition.CostCenters" data-button-label="icon Cost_Center_Name"
                                            data-on-item-click="CostCenterChagned()" data-on-select-all="costCenterChangeAll()" data-on-select-none="costCenterSelectNone()"
                                            data-item-label=" icon Cost_Center_Name maker" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SpaceRequisition.CostCenters[0]" name="Cost_Center_Name" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.Cost_Center_Name.$invalid">Please select {{BsmDet.Child|lowercase}}  </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.SRN_FROM_DATE.$invalid}">
                                        <label class="control-label">From Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" id='fromdate'>
                                            <input type="text" class="form-control" data-ng-model="SpcReqSearch.SRN_FROM_DATE" id="SRN_FROM_DATE" name="SRN_FROM_DATE" required="" placeholder="mm/dd/yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.SRN_FROM_DATE.$invalid">Please select from date </span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.SRN_TO_DATE.$invalid}">
                                        <label class="control-label">To Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" id='todate'>
                                            <input type="text" class="form-control" data-ng-model="SpcReqSearch.SRN_TO_DATE" id="SRN_TO_DATE" name="SRN_TO_DATE" required="" placeholder="mm/dd/yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.SRN_TO_DATE.$invalid">Please select to date </span>
                                    </div>
                                </div>


                                <div class="col-md-3 col-sm-6 col-xs-12" ng-hide="AllocationType.SYSP_VAL1 == '1041'">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.SH_NAME.$invalid}">
                                        <label class="control-label">Shift <span style="color: red;"></span></label>
                                        <div isteven-multi-select data-input-model="ShiftFilter" data-output-model="SpaceRequisition.ShiftFilter" data-button-label="icon SH_NAME"
                                            data-item-label="icon SH_NAME maker" data-on-item-click="ShiftChange()" data-on-select-all="shiftChangeAll()" data-on-select-none="ShiftSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="SpaceRequisition.ShiftFilter[0]" name="SH_NAME" style="display: none" />
                                        <%--  <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.SH_NAME.$invalid">Please select floor </span>--%>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12 box-footer pull-right">
                                    <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color">Search Spaces</button>
                                    <input type="button" id="btnNew" data-ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>
                    </form>

                    <form role="form" id="Form2" name="frmSpcReq" data-valid-submit="RaiseRequest()" novalidate data-ng-show="Markers.length !=0">

                        <div style="height: 275px">
                            <input id="filtertxt" placeholder="Filter..." type="text" style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 100%;" class="ag-blue"></div>
                        </div>
                        <br />
                        <br />
                        <div class="clearfix">
                            <div class="col-md-6 col-sm-6 col-xs-12 pull-left">
                                <div class="form-group">
                                    <label class="control-label">Requestor Remarks </label>
                                    <textarea rows="3" cols="50" name="SRN_REQ_REM" data-ng-model="SpcReqSearch.SRN_REQ_REM" class="form-control" placeholder="Requestor remarks"></textarea>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="col-md-6 col-sm-6 col-xs-12 row box-footer text-right">
                                <input type="button" id="btnviewinmap" data-ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                <button type="submit" id="btnRequest" class="btn btn-primary custom-button-color">Raise Request</button>
                            </div>
                        </div>
                    </form>

                    <form role="form" id="frmCountReq" name="frmCountReq" data-valid-submit="RaiseCountRequest()" novalidate data-ng-show="gridCountData.length !=0">
                        <div>
                            <input id="txtCountFilter" placeholder="Filter..." type="text" style="width: 25%" />
                            <div data-ag-grid="gridCountOptions" style="height: 150px;" class="ag-blue"></div>
                        </div>
                        <br />
                        <br />
                        <div class="clearfix">
                            <div class="col-md-6 col-sm-6 col-xs-12 pull-left">
                                <div class="form-group" data-ng-class="{'has-error': frmCountReq.$submitted && frmCountReq.SRN_REQ_REM.$invalid}">
                                    <label class="control-label">Requestor Remarks (Please enter EmpId/Name/CustomerName)<span style="color: red;">*</span></label>
                                    <textarea rows="3" cols="50" name="SRN_REQ_REM" data-ng-model="SpcReqSearch.SRN_REQ_REM" required class="form-control" placeholder="Please enter EmpId/Name/CustomerName"></textarea>
                                    <span class="error" data-ng-show="frmCountReq.$submitted && frmCountReq.SRN_REQ_REM.$invalid">Please enter requestor remarks </span>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="col-md-6 col-sm-6 col-xs-12 row box-footer text-right">
                                <button type="submit" id="btnCountRequest" class="btn btn-primary custom-button-color">Raise Request</button>
                            </div>
                        </div>
                    </form>

                </div>


            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" id="historymodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="H1">Plot On the Map</h4>
                </div>

                <form role="form" name="form2" id="form3">
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <div isteven-multi-select data-input-model="MapFloors" selection-mode="single" helper-elements="filter" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                        data-on-item-click="FlrSectMap(data)" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <input type="button" id="Button1" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color" value="Proceed" />
                                </div>
                                <div class="col-md-4">
                                    Legends:
                                        <span class="label label-success pull-right">Available </span>
                                    <span class="label selectedseat pull-right">Selected Seats </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="main">
                                    <div id="leafletMap"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <%--<script defer src="../../../Scripts/aggrid/ag-grid.min.js"></script>--%>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        function setDateVals() {
            $('#SRN_FROM_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#SRN_TO_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            // $('#SRN_FROM_DATE').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY')));
            //$('#SRN_TO_DATE').datepicker('setDate', new Date(moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY')));
        }
    </script>
    <script defer src="../../Utility.js"></script>

    <script defer src="../Js/SpaceRequisition.js"></script>

</body>
</html>
