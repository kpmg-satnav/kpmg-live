﻿
app.service("ViewSpaceExtReqService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    //For my Requisitions Grid
    this.GetMyReqList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ViewSpaceExtReq/GetMyReqList')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetPendingList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ViewSpaceExtReq/GetPendingReqList')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetUpdateApprList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceExtension/GetSpaceExtensionDetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetDetailsOnSelection = function (selectedid) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewSpaceExtReq/GetDetailsOnSelection', selectedid)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);
app.controller('ViewSpaceExtReqController', ['$scope', '$q', 'ViewSpaceExtReqService', 'SpaceExtensionService', 'UtilityService', '$timeout','$filter', function ($scope, $q, ViewSpaceExtReqService, SpaceExtensionService, UtilityService, $timeout, $filter) {
    $scope.Viewstatus = 0;
    $scope.SEReq = {};
    $scope.Getcountry = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.Verticallist = [];
    $scope.CostCenterlist = [];
    $scope.selectedSeats = [];
    $scope.currentblkReq = {};
    $scope.Searchgridata = [];
    $scope.EnableStatus = 0;
    $scope.disable = false;
    $scope.RetStatus = UtilityService.Added;
    $scope.selectedSpaces = [];
    $scope.ApproveSelectedSpaces = [];
    $scope.UpdateSelectedSpaces = [];
    $scope.tempspace = {};
    $scope.selectedPendingRequests = [];
    $scope.blkReq = [];
    $scope.Map = {};
    $scope.Map.Floor = [];
    $scope.Markers = [];
    $scope.SelRowData = [];
    var map = L.map('leafletMap');//.setView([17.561298804683357, 79.6684030798511], 11);

    function createDatePicker(params) {
        if (params.data.SSED_EXTN_DT != null) {
            params.data.SSED_EXTN_DT = moment(params.data.SSED_EXTN_DT).format('MM/DD/YYYY');
        }

        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.SSED_EXTN_DT');
        newDate.setAttribute('ng-change', 'checkIfSpaceIdIsAllocatedOrNotBeforeRequest(data)');
        newDate.type = "input";
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        $(newDate).datepicker({
            //format: 'dd M, yyyy',
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
        })
        return newDate;
    }

    $scope.MyrequisitonsDefs = [
        { headerName: "Requisition ID", field: "SSE_REQ_ID", cellClass: "grid-align", filter: 'set', template: '<a ng-click="onRowSelectedFunc(data,false)">{{data.SSE_REQ_ID}}</a>', pinned: 'left' },
        { headerName: "Status", width: 200, field: "STA_DESC", cellClass: "grid-align", pinned: 'left' },
        { headerName: "Requested By", field: "AUR_KNOWN_AS", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requested Date ", template: '<span>{{data.SSE_REQ_DT | date: "dd MMM, yyyy "}}</span>', cellClass: "grid-align", suppressMenu: true, },
        { headerName: "From Date", width: 120, template: '<span>{{data.SSE_FROM_DATE | date: "dd MMM, yyyy"}}</span>', field: "SSE_FROM_DATE", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "To Date", width: 120, template: '<span>{{data.SSE_TO_DATE | date: "dd MMM, yyyy"}}</span>', field: "SSE_TO_DATE", cellClass: "grid-align", suppressMenu: true, },

    ];
    $scope.MyrequisitonsOptions = {
        columnDefs: $scope.MyrequisitonsDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableColResize: true,
        enableFilter: true,
        enableSorting: true,
        width: 50,
        rowSelection: 'single',
    };


    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });

    ViewSpaceExtReqService.GetMyReqList().then(function (data) {
        $scope.gridata = data.data;
        $scope.MyrequisitonsOptions.api.setRowData($scope.gridata);
    }, function (error) {
        console.log(error);
    });
    function onReqFilterChanged(value) {
        $scope.MyrequisitonsOptions.api.setQuickFilter(value);
    }
    $("#ReqFilter").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    })


    $scope.approvalsDefs = [
        {
            headerName: "Select All", field: "Selected", width: 90, template: "<input type='checkbox' ng-model='data.Selected' ng-change='ApprovalchkChanged(data)'>",
            cellClass: 'grid-align', headerCellRenderer: headerCellRendererFuncAppr, pinned: 'left', suppressMenu: true,
        },
        {
            headerName: "Requisition ID", field: "SSE_REQ_ID", cellClass: "grid-align",
            filter: 'set', template: '<a ng-click="onRowSelectedFunc(data,true)">{{data.SSE_REQ_ID}}</a>', pinned: 'left'
        },
        { headerName: "Status", field: "STA_DESC", cellClass: "grid-align" },
        { headerName: "Requested By", field: "AUR_KNOWN_AS", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requested Date ", template: '<span>{{data.SSE_REQ_DT | date: "dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },
        { headerName: "From Date", width: 120, template: '<span>{{data.SSE_FROM_DATE | date: "dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },
        { headerName: "To Date", width: 120, template: '<span>{{data.SSE_TO_DATE | date: "dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },

    ];
    $scope.approvalOptions = {
        columnDefs: $scope.approvalsDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableColResize: true,
        enableFilter: true,
        enableSorting: true,
        width: 50,
        rowSelection: 'multiple',
    };
    function onAppFilterChanged(value) {
        $scope.approvalOptions.api.setQuickFilter(value);
    }
    $("#ApprvlFilter").change(function () {
        onAppFilterChanged($(this).val());
    }).keydown(function () {
        onAppFilterChanged($(this).val());
    }).keyup(function () {
        onAppFilterChanged($(this).val());
    }).bind('paste', function () {
        onAppFilterChanged($(this).val());
    })

    $scope.UpdateapprDefs = [
        { headerName: "Space", field: "SPC_NAME", width: 180, cellClass: 'grid-align', cellStyle: changeRowColor, pinned: 'left' },
        { headerName: "Space Type", field: "SSA_SPC_TYPE", width: 180, cellClass: 'grid-align', cellStyle: changeRowColor },
        { headerName: "Space Sub Type", field: "SSA_SPC_SUB_TYPE", width: 180, cellClass: 'grid-align', cellStyle: changeRowColor },
        { headerName: "Shift Type", field: "SH_NAME", width: 250, cellClass: 'grid-align', cellStyle: changeRowColor },
        { headerName: "", field: "SSED_VER_NAME", width: 150, cellClass: 'grid-align', cellStyle: changeRowColor },
        { headerName: "", field: "Cost_Center_Name", width: 150, cellClass: 'grid-align', cellStyle: changeRowColor },
        { headerName: "Employee", field: "AUR_KNOWN_AS", width: 200, cellClass: 'grid-align', cellStyle: changeRowColor },
        { headerName: "Designation", field: "EMP_DESIGNATION", width: 200, cellClass: 'grid-align', cellStyle: changeRowColor },
        {
            headerName: "From Date", template: '{{data.SSAD_FROM_DATE | date: "MM/dd/yyyy"}}', width: 120,
            cellClass: 'grid-align', cellStyle: changeRowColor, pinned: 'right', suppressMenu: true,
        },
        {
            headerName: "To Date", template: '{{data.SSAD_TO_DATE | date: "MM/dd/yyyy"}}', width: 120, cellClass: 'grid-align',
            cellStyle: changeRowColor, pinned: 'right', suppressMenu: true,
        },
        {
            headerName: "Extension Date", field: "SSED_EXTN_DT", width: 120, cellClass: 'grid-align', cellRenderer: createDatePicker,
            cellStyle: changeRowColor, pinned: 'right', suppressMenu: true,
        },
        {
            headerName: "Select All", field: "Selected", width: 90, template: "<input type='checkbox' ng-model='data.Selected' ng-change='chkChanged(data)'>",
            cellClass: 'grid-align', headerCellRenderer: headerCellRendererFuncUpdate, cellStyle: changeRowColor, pinned: 'right', suppressMenu: true,
        },
    ];
    $scope.UpdateapprOptions = {
        columnDefs: $scope.UpdateapprDefs,
        rowData: null,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableSorting: true,
        enableScrollbars: false,
        enableColResize: true,
        width: 50,
        rowSelection: 'multiple',

    };
    function onUpdateFilterChanged(value) {
        $scope.UpdateapprOptions.api.setQuickFilter(value);
    }
    $("#UpdteFilter").change(function () {
        onUpdateFilterChanged($(this).val());
    }).keydown(function () {
        onUpdateFilterChanged($(this).val());
    }).keyup(function () {
        onUpdateFilterChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFilterChanged($(this).val());
    })

    function headerCellRendererFuncAppr(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);

        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.approvalOptions.rowData, function (value, key) {
                        value.Selected = true;
                        value.STACHECK = UtilityService.Added;
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.approvalOptions.rowData, function (value, key) {
                        value.Selected = false;
                        value.STACHECK = UtilityService.Deleted;
                    });
                });
            }
        });
        return eHeader;
    }

    function headerCellRendererFuncUpdate(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);

        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.UpdateapprOptions.rowData, function (value, key) {
                        value.Selected = true;
                        //value.setIcon(selctdChricon);
                        if (value != undefined && value.SSED_REQ_ID != "") {
                            value.STACHECK = UtilityService.Unchanged;
                        }
                        else {
                            value.STACHECK = UtilityService.Added;
                        }
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.UpdateapprOptions.rowData, function (value, key) {
                        value.Selected = false;
                        //value.setIcon(Vacanticon)
                        if (value != undefined && value.SSED_REQ_ID != "") {
                            value.STACHECK = UtilityService.Deleted;
                        }
                    });
                });
            }
        });
        return eHeader;
    }


    $scope.chkChanged = function (selctedRow) {
        console.log(selctedRow);
        if ($scope.drawnItems) {
            $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: selctedRow.SRD_SPC_ID, spacetype: 'CHA' } });
            console.log(selctedRow);
            if (selctedRow.Selected) {
                $scope.chkr.setStyle(selctdChrStyle);
                $scope.chkr.Selected = true;
            }
            else {
                $scope.chkr.setStyle(VacantStyle);
                $scope.chkr.Selected = false;
            }
        }
        else {
            if (selctedRow.Selected && (selctedRow.SSED_REQ_ID != null && selctedRow.SSED_REQ_ID != ""))
                selctedRow.STACHECK = 16;
            else if (selctedRow.Selected && (selctedRow.SSED_REQ_ID == null || selctedRow.SSED_REQ_ID == ""))
                selctedRow.STACHECK = 4;
            else if (!selctedRow.Selected && (selctedRow.SSED_REQ_ID == null || selctedRow.SSED_REQ_ID == ""))
                selctedRow.STACHECK = 2;
            else if (!selctedRow.Selected && (selctedRow.SSED_REQ_ID == null != null && selctedRow.SSED_REQ_ID != ""))
                selctedRow.STACHECK = 8;
        }
    }


    $scope.onRowSelectedFunc = function (data, flag) {
        $scope.disable = true;
        $scope.Viewstatus = 1;
        progress(0, 'Loading...', true);
        $scope.Markers = [];
        $scope.SelRowData = [];
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        ViewSpaceExtReqService.GetDetailsOnSelection(data).then(function (response) {
            if (response.data != null) {
                progress(0, 'Loading...', false);
                if (response.data.spcextn.SSE_STA_ID == 1032 && flag) {
                    $scope.EnableStatus = 1;
                    $scope.EnableButton = false;
                }
                else if (response.data.spcextn.SSE_STA_ID == 1032 && response.data.spcextn.SSE_REQ_BY == UID) {
                    $scope.EnableStatus = 0;
                    $scope.EnableButton = true;
                }
                else if (response.data.spcextn.SSE_STA_ID != 1032 && response.data.spcextn.SSE_REQ_BY == UID) {
                    $scope.EnableStatus = 2;
                    $scope.EnableButton = true;
                }
                else if (response.data.spcextn.SSE_STA_ID == 1035 || response.data.spcextn.SSE_STA_ID == 1034) {
                    $scope.EnableStatus = 2;
                }

                $scope.currentblkReq = response.data.spcextn;
                $scope.selectedSpaces = response.data.spceextn_flr_ver.selectedSpaces;

                $scope.SEReq.selectedCountries = response.data.spceextn_flr_ver.selectedCountries;
                $scope.SEReq.selectedCities = response.data.spceextn_flr_ver.selectedCities;
                $scope.SEReq.selectedLocations = response.data.spceextn_flr_ver.selectedLocations;
                $scope.SEReq.selectedTowers = response.data.spceextn_flr_ver.selectedTowers;
                $scope.SEReq.selectedFloors = response.data.spceextn_flr_ver.selectedFloors;
                $scope.SEReq.selectedVerticals = response.data.spceextn_flr_ver.selectedVerticals;
                $scope.SEReq.selectedCostcenters = response.data.spceextn_flr_ver.selectedCostcenters;

                $scope.currentblkReq.SSE_REQ_REM = response.data.spcextn.SSE_REQ_REM;
                $scope.currentblkReq.SSE_APPR_REM = response.data.spcextn.SSE_APPR_REM;
                $scope.currentblkReq.SSE_REQ_ID = response.data.spcextn.SSE_REQ_ID;
                $scope.currentblkReq.AUR_KNOWN_AS = response.data.spcextn.AUR_KNOWN_AS;
                $scope.currentblkReq.STA_DESC = response.data.spcextn.STA_DESC;
                $scope.currentblkReq.SSE_REQ_DT = response.data.spcextn.SSE_REQ_DT;

                $scope.SEReq.FROM_DATE = $filter('date')(response.data.spcextn.SSE_FROM_DATE, "MM/dd/yyyy");
                $scope.SEReq.TO_DATE = $filter('date')(response.data.spcextn.SSE_TO_DATE, "MM/dd/yyyy");


                UtilityService.getCountires(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Getcountry = response.data;
                        if ($scope.SEReq.selectedCountries != null) {
                            for (i = 0; i < $scope.SEReq.selectedCountries.length; i++) {
                                var a = _.find($scope.Getcountry, { CNY_CODE: $scope.SEReq.selectedCountries[i].CNY_CODE });
                                a.ticked = true;
                            }
                        }
                    }
                });

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Citylst = response.data;
                        if ($scope.SEReq.selectedCities != null) {
                            for (i = 0; i < $scope.SEReq.selectedCities.length; i++) {
                                var a = _.find($scope.Citylst, { CTY_CODE: $scope.SEReq.selectedCities[i].CTY_CODE });
                                a.ticked = true;
                            }
                        }
                    }
                });

                UtilityService.getLocations(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Locationlst = response.data;
                        if ($scope.SEReq.selectedLocations != null) {
                            for (i = 0; i < $scope.SEReq.selectedLocations.length; i++) {
                                var a = _.find($scope.Locationlst, { LCM_CODE: $scope.SEReq.selectedLocations[i].LCM_CODE });
                                a.ticked = true;
                            }
                        }
                    }
                });

                UtilityService.getTowers(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Towerlist = response.data;
                        if ($scope.SEReq.selectedTowers != null) {
                            for (i = 0; i < $scope.SEReq.selectedTowers.length; i++) {
                                var a = _.find($scope.Towerlist, { TWR_CODE: $scope.SEReq.selectedTowers[i].TWR_CODE });
                                a.ticked = true;
                            }
                        }
                    }
                });

                UtilityService.getFloors(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Floorlist = response.data;
                    }
                    if ($scope.SEReq.selectedFloors != null) {
                        for (i = 0; i < $scope.SEReq.selectedFloors.length; i++) {
                            var a = _.find($scope.Floorlist, { FLR_CODE: $scope.SEReq.selectedFloors[i].FLR_CODE });
                            a.ticked = true;
                        }
                    }
                });

                UtilityService.getVerticals(3).then(function (Verdata) {
                    if (Verdata.data != null) {
                        $scope.Verticallist = Verdata.data;
                        if (response.data.spceextn_flr_ver.selectedVerticals != null) {
                            for (i = 0; i < response.data.spceextn_flr_ver.selectedVerticals.length; i++) {
                                var a = _.find($scope.Verticallist, { VER_CODE: response.data.spceextn_flr_ver.selectedVerticals[i].VER_CODE });
                                a.ticked = true;
                            }
                        }
                    }

                });


                UtilityService.getCostCenters(2).then(function (response) {
                    if (response.data != null) {
                        $scope.CostCenterlist = response.data;
                        if ($scope.SEReq.selectedCostcenters != null) {
                            for (i = 0; i < $scope.SEReq.selectedCostcenters.length; i++) {
                                var a = _.find($scope.CostCenterlist, { Cost_Center_Code: $scope.SEReq.selectedCostcenters[i].Cost_Center_Code });
                                a.ticked = true;
                            }
                        }
                    }
                });

                GetMarkers(response.data.UpdateGridDETAILS);
                $scope.UpdateapprOptions.api.setRowData($scope.Markers);
                $scope.UpdateapprOptions.columnApi.getColumn("SSED_VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
                $scope.UpdateapprOptions.columnApi.getColumn("Cost_Center_Name").colDef.headerName = $scope.BsmDet.Child;
                $scope.UpdateapprOptions.api.refreshHeader();
            }
            else {
                $scope.Viewstatus = 0;
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', response.Message);
                console.log(response.Info);
            }

        }, function (error) {
            console.log(error);
        });
    };

    ViewSpaceExtReqService.GetPendingList().then(function (data) {
        $scope.ApprOrRejectData = data.data;
        $scope.approvalOptions.api.setRowData($scope.ApprOrRejectData);
    }, function (error) {
        console.log(error);
    });


    UtilityService.getCountires(1).then(function (response) {
        $scope.Getcountry = response.data;
    }, function (error) {
        console.log(error);
    });
    $scope.CountryChanged = function () {
        UtilityService.getCitiesbyCny($scope.SEM.selectedCountries, 2).then(function (response) {
            $scope.Citylst = response.data
        }, function (error) {
            console.log(error);
        });
    }
    $scope.CnyChangeAll = function () {
        UtilityService.getCitiesbyCny($scope.Getcountry).then(function (response) {
            $scope.Citylst = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.cnySelectNone = function () {
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }
    $scope.CityChanged = function () {
        UtilityService.getLocationsByCity($scope.SEM.selectedCities, 2).then(function (response) {
            $scope.Locationlst = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.CtyChangeAll = function () {
        UtilityService.getLocationsByCity($scope.Citylst).then(function (response) {
            $scope.Locationlst = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.ctySelectNone = function () {
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }
    $scope.LocChange = function () {
        UtilityService.getTowerByLocation($scope.SEM.selectedLocations, 2).then(function (response) {
            $scope.Towerlist = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.LCMChangeAll = function () {
        UtilityService.getTowerByLocation($scope.Locationlst).then(function (response) {
            $scope.Towerlist = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.lcmSelectNone = function () {
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }
    $scope.TwrChange = function () {
        UtilityService.getFloorByTower($scope.SEM.selectedTowers, 2).then(function (response) {
            $scope.Floorlist = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.TwrChangeAll = function () {
        UtilityService.getFloorByTower($scope.Towerlist).then(function (response) {
            $scope.Floorlist = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.twrSelectNone = function () {
        $scope.Floorlist = [];
    }
    $scope.FloorChange = function () {
        UtilityService.getVerticals(2).then(function (response) {
            $scope.Verticallist = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.VerticalChange = function () {
        UtilityService.getCostcenterByVertical($scope.SEM.selectedVerticals, 2).then(function (data) {
            $scope.CostCenterlist = data.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.VerticalChangeAll = function () {
        UtilityService.getCostcenterByVertical($scope.Verticallist).then(function (data) {
            $scope.CostCenterlist = data.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.verticalSelectNone = function () {
        $scope.CostCenterlist = [];
    }
    function changeRowColor(params) {
        if (params.data.STATUS == "FAIL") {
            return { 'background-color': 'yellow' };
        }
    }

    $scope.checkIfSpaceIdIsAllocatedOrNotBeforeRequest = function (data) {
        $scope.dataobject = data;
        $scope.selspcObj = {};
        $scope.selspcObj.SSAD_SRN_REQ_ID = data.SSAD_SRN_REQ_ID;
        $scope.selspcObj.SSED_SPC_ID = data.SSED_SPC_ID;
        $scope.selspcObj.SPC_NAME = data.SPC_NAME;
        $scope.selspcObj.SSAD_FROM_DATE = data.SSAD_FROM_DATE;
        $scope.selspcObj.SSAD_TO_DATE = data.SSAD_TO_DATE;
        $scope.selspcObj.SSAD_FROM_TIME = data.SSAD_FROM_TIME;
        $scope.selspcObj.SSAD_TO_TIME = data.SSAD_TO_TIME;
        var checkObjDet = [];
        var todate = moment(data.SSAD_TO_DATE);
        var extndate = moment(data.SSED_EXTN_DT);
        if (todate > extndate) {
            data.Selected = false;
            showNotification('error', 8, 'bottom-right', "Space Extn Date should be greater than ToDate.");
        }

        else {
            SpaceExtensionService.checkIfSpaceIdIsAllocatedOrNotBeforeRequest($scope.selspcObj).then(function (response) {
                progress(0, 'Loading...', true);
                if (!data.Selected) {
                    data.Selected = true;
                    data.STACHECK = 4;
                }
                console.log(response);
                if (response.length != 0) {
                    data.Selected = false;
                    var status = response[0].STATUS;
                    if (status == "FAIL" && response[0].REMARKS != 'Already Under Requistion') {
                        progress(0, 'Loading...', false);
                        var foundobj = _.find($scope.Markers, { SSAD_SRN_REQ_ID: response[0].SSAD_SRN_REQ_ID });
                        foundobj.STATUS = "FAIL";
                        $scope.UpdateapprOptions.api.refreshView();
                        var msg = "Space is already allocated to other person between your selected to date extnsion date.";
                        showNotification('error', 8, 'bottom-right', response[0].REMARKS);
                    }

                    setTimeout(function () {
                        $scope.$apply(function () {
                            angular.forEach($scope.Markers, function (data) { data.STATUS = "Default" });
                            $scope.UpdateapprOptions.api.refreshView();
                            progress(0, 'Loading...', false);
                        });
                    }, 3000);
                }
                else {
                    $scope.chkChanged(data);
                    progress(0, 'Loading...', false);
                }

            }, function (error) {
                console.log(error);
                progress(0, 'Loading...', false);
            })
        }
    }

    //For Back Button
    $scope.back = function () {
        $scope.Viewstatus = 0;
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
        $scope.Getcountry = [];
        $scope.SelectAll = false;
        $scope.SelectAllApp = false;
        //$scope.ApprOrRejectData.Selected = false;
        $scope.EnableStatus = 0;
    }

    $scope.SelectAll = false;
    $scope.SelectAllApp = false;
    $scope.selectAllRows = function selectAll() {
        angular.forEach($scope.ApprOrRejectData, function (data) {
            data.Selected = $scope.SelectAll;
        });
    };
    $scope.selectAllAppRows = function SelectAllApp() {
        angular.forEach($scope.Searchgridata, function (data) {
            data.Selected = $scope.SelectAllApp;
        });
    };

    $scope.setStatus = function (status) {
        switch (status) {
            case 'Approve': $scope.RetStatus = UtilityService.Approved;
                $scope.currentblkReq.SSE_APPR_BY = UID;
                $scope.currentblkReq.SSE_UPDATED_BY = UID;
                break;
            case 'Reject': $scope.RetStatus = UtilityService.Rejected;
                $scope.currentblkReq.SSE_APPR_BY = UID;
                $scope.currentblkReq.SSE_UPDATED_BY = UID;
                break;
            case 'Update': $scope.RetStatus = UtilityService.Modified;
                $scope.currentblkReq.SSE_UPDATED_BY = UID;
                break;
            case 'Cancel': $scope.RetStatus = UtilityService.Canceled;
                $scope.currentblkReq.SSE_UPDATED_BY = UID;
                break;
            case 'ApproveAll':
                $scope.RetStatus = UtilityService.Approved;
                angular.forEach($scope.ApprOrRejectData, function (data) {
                    if (data.Selected == true) {
                        $scope.ApproveSelectedSpaces.push(data);
                    }
                });
                $scope.currentblkReq.SSE_APPR_BY = UID;
                $scope.currentblkReq.SSE_UPDATED_BY = UID;

                break;
            case 'RejectAll': $scope.RetStatus = UtilityService.Rejected;
                angular.forEach($scope.ApprOrRejectData, function (data) {
                    if (data.Selected == true) {
                        $scope.ApproveSelectedSpaces.push(data);
                    }
                });

                $scope.currentblkReq.SSE_APPR_BY = UID;
                $scope.currentblkReq.SSE_UPDATED_BY = UID;
                break;
        }
    }

    $scope.UpdateRequest = function () {
        var saveObjDet = [];
        var saveObjDet1 = [];
        $scope.selectedSpaces = [];
        angular.forEach($scope.Markers, function (data, key) {
            $scope.selspcObj = {};
            $scope.selspcObj.SSAD_SRN_REQ_ID = data.SSAD_SRN_REQ_ID;
            $scope.selspcObj.SSED_SPC_ID = data.SSED_SPC_ID;
            $scope.selspcObj.SSAD_FROM_DATE = data.SSAD_FROM_DATE;
            $scope.selspcObj.SSAD_TO_DATE = data.SSAD_TO_DATE;
            $scope.selspcObj.SSAD_FROM_TIME = data.SSAD_FROM_TIME;
            $scope.selspcObj.SSAD_TO_TIME = data.SSAD_TO_TIME;
            $scope.selspcObj.SSA_SPC_TYPE = data.SSA_SPC_TYPE;
            $scope.selspcObj.SSA_SPC_SUB_TYPE = data.SSA_SPC_SUB_TYPE;
            $scope.selspcObj.SSED_AUR_ID = data.SSED_AUR_ID;
            $scope.selspcObj.SSED_VER_NAME = data.SSED_VER_NAME;
            $scope.selspcObj.SSED_EXTN_DT = data.SSED_EXTN_DT;
            $scope.selspcObj.Selected = data.Selected;
            $scope.selspcObj.SSAD_ID = data.SSAD_ID;
            $scope.selspcObj.STACHECK = data.STACHECK;
            $scope.selspcObj.SSED_REQ_ID = data.SSED_REQ_ID;
            if (data.STACHECK == 8 || data.STACHECK == 4 || data.STACHECK == 16) {
                $scope.selectedSpaces.push($scope.selspcObj);
            }

        });

        angular.forEach($scope.selectedSpaces, function (saveObj) {
            var todate = moment(saveObj.SSAD_TO_DATE);
            var extndate = moment(saveObj.SSED_EXTN_DT);
            if (todate > extndate) {
                saveObjDet.push(saveObj);
            }
        })

        angular.forEach($scope.selectedSpaces, function (saveObj) {
            var todate = moment(saveObj.SSAD_TO_DATE);
            var extndate = moment(saveObj.SSED_EXTN_DT);
            if (saveObj.SSED_EXTN_DT == null) {
                saveObjDet1.push(saveObj);
            }
        })
        var cnt = _.filter($scope.Markers, { 'Selected': true }).length;
        if (cnt == 0) {
            showNotification('error', 8, 'bottom-right', "Please Check At Least One Space Id To Extend.");
        }

        else if (saveObjDet.length != 0) {
            showNotification('error', 8, 'bottom-right', 'Space Extn Date should be greater than ToDate.');

        }
        else if (saveObjDet1.length != 0) {
            showNotification('error', 8, 'bottom-right', 'Please Select Space Extension Date');
        }

        else {

            var ReqObj = {
                spcextn: $scope.currentblkReq,
                spceextn_flr_ver: { selectedFloors: $scope.SEReq.selectedFloors, selectedCostcenters: $scope.SEReq.selectedCostcenter, selectedSpaces: $scope.selectedSpaces },
                FLAG: $scope.RetStatus
            };
            console.log(ReqObj);
            SpaceExtensionService.RaiseRequest(ReqObj).then(function (response) {
                $scope.back();
                ViewSpaceExtReqService.GetMyReqList().then(function (data) {
                    $scope.MyrequisitonsOptions.api.setRowData([]);
                    $scope.gridata = data.data;
                    $scope.MyrequisitonsOptions.api.setRowData($scope.gridata);
                }, function (error) {
                    console.log(error);
                });
                ViewSpaceExtReqService.GetPendingList().then(function (data) {
                    $scope.ApprOrRejectData = data.data;
                    $scope.approvalOptions.api.setRowData($scope.ApprOrRejectData);
                }, function (error) {
                    console.log(error);
                })
                showNotification('success', 8, 'bottom-right', response.Message);
            });
        }
        //else {
        //    showNotification('error', 8, 'bottom-right', 'Space Extn Date should be greater than ToDate.');
        //}
    }

    $scope.UpdateAllRequests = function () {
        var ReqObj = { FLAG: $scope.RetStatus, spcextn_lst: $scope.ApproveSelectedSpaces, Remarks: $scope.blkReq.SSE_APPR_REM };
        if ($scope.ApproveSelectedSpaces.length == 0) {
            showNotification('error', 8, 'bottom-right', "Please Select  At Least One Space to Approve or Reject");
        }
        else {
            SpaceExtensionService.ApproveAndRejectRequests(ReqObj).then(function (response) {
                showNotification('success', 8, 'bottom-right', response.Message);

                ViewSpaceExtReqService.GetPendingList().then(function (data) {
                    $scope.ApprOrRejectData = data.data;
                    $scope.approvalOptions.api.setRowData($scope.ApprOrRejectData);
                }, function (error) {
                    console.log(error);
                });


            });
        }
    }

    $scope.setAllDates = function setAllExtnDates() {
        angular.forEach($scope.Markers, function (data) {
            data.SSED_EXTN_DT = $('#EXTN_DATE').val();
        });
    }

    ///// For map layout

    $scope.ViewinMap = function () {
        if ($scope.SelRowData.length == 0) {
            $scope.MapFloors = [];
            angular.forEach($scope.SEReq.selectedFloors, function (Value, Key) {
                Value.ticked = false;
                $scope.MapFloors.push(Value);
            });
            $scope.MapFloors[0].ticked = true;
            $scope.Map.Floor.push($scope.MapFloors[0]);
        }

        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {
        if ($scope.SelRowData.length == 0)
            $scope.loadmap();
    });

    $scope.FlrSectMap = function (data) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();

    }

    $scope.loadmap = function () {
        progress(0, 'Loading...', true);
        //$('#leafletMap').empty();

        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE, key_value: 1  };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post('../../../api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });
    }

    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            // do something
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
            // success
            // results: an array of data objects from each deferred.resolve(data) call
            function (results) {
                var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
                map.fitBounds(bounds);
                $scope.SelRowData = $filter('filter')($scope.Markers, { SPC_FLR_ID: $scope.Map.Floor[0].FLR_CODE });
                angular.forEach($scope.SelRowData, function (value, key) {

                    $scope.marker = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: value.SSED_SPC_ID, spacetype: 'CHA' } });
                    $scope.marker.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                    $scope.marker.SSED_SPC_ID = value.SSED_SPC_ID;
                    $scope.marker.SPC_NAME = value.SPC_NAME;
                    $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                    $scope.marker.lat = value.lat;
                    $scope.marker.lon = value.lon;
                    $scope.marker.SSAD_FROM_DATE = value.SSAD_FROM_DATE;
                    $scope.marker.SSAD_TO_DATE = value.SSAD_TO_DATE;
                    $scope.marker.SSAD_FROM_TIME = value.SSAD_FROM_TIME;
                    $scope.marker.SSAD_TO_TIME = value.SSAD_TO_TIME;
                    $scope.marker.SSA_SPC_TYPE = value.SSA_SPC_TYPE;
                    $scope.marker.SSA_SPC_SUB_TYPE = value.SSA_SPC_SUB_TYPE;
                    $scope.marker.SH_NAME = value.SH_NAME;
                    $scope.marker.Cost_Center_Name = value.Cost_Center_Name;
                    $scope.marker.AUR_KNOWN_AS = value.AUR_KNOWN_AS;
                    $scope.marker.SSED_AUR_ID = value.SSED_AUR_ID;
                    $scope.marker.SSED_VER_NAME = value.SSED_VER_NAME;
                    $scope.marker.EMP_DESIGNATION = value.EMP_DESIGNATION;
                    $scope.marker.STATUS = value.STATUS;
                    $scope.marker.SSED_EXTN_DT = value.SSED_EXTN_DT;
                    $scope.marker.Selected = value.Selected;
                    $scope.marker.SSAD_ID = value.SSAD_ID;
                    $scope.marker.SSED_REQ_ID = value.SSED_REQ_ID;

                    if (value.Selected)
                        $scope.marker.setStyle(selctdChrStyle);
                    else
                        $scope.marker.setStyle(VacantStyle);
                    $scope.marker.layer = value.SSA_SPC_TYPE;
                    $scope.marker.STACHECK = UtilityService.Unchanged;

                    $scope.marker.bindLabel(value.SPC_NAME);
                    $scope.marker.on('click', markerclicked);
                    $scope.marker.addTo(map);
                });
            },
            // error
            function (response) {
            }
        );
    };

    //var Vacanticon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_Green.gif',
    //    iconSize: [16, 16], // size of the icon
    //});
    //var selctdChricon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_yellow.gif',
    //    iconSize: [16, 16], // size of the icon
    //});


    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };

    function GetMarkers(data) {

        jQuery.each(data, function (index, value) {

            $scope.marker = {};
            $scope.marker.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
            $scope.marker.SSED_SPC_ID = value.SSED_SPC_ID;
            $scope.marker.SPC_NAME = value.SPC_NAME;
            $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
            $scope.marker.lat = value.lat;
            $scope.marker.lon = value.lon;
            $scope.marker.SSAD_FROM_DATE = value.SSAD_FROM_DATE;
            $scope.marker.SSAD_TO_DATE = value.SSAD_TO_DATE;
            $scope.marker.SSAD_FROM_TIME = value.SSAD_FROM_TIME;
            $scope.marker.SSAD_TO_TIME = value.SSAD_TO_TIME;
            $scope.marker.SSA_SPC_TYPE = value.SSA_SPC_TYPE;
            $scope.marker.SSA_SPC_SUB_TYPE = value.SSA_SPC_SUB_TYPE;
            $scope.marker.SH_NAME = value.SH_NAME;
            $scope.marker.Cost_Center_Name = value.Cost_Center_Name;
            $scope.marker.AUR_KNOWN_AS = value.AUR_KNOWN_AS;
            $scope.marker.SSED_AUR_ID = value.SSED_AUR_ID;
            $scope.marker.SSED_VER_NAME = value.SSED_VER_NAME;
            $scope.marker.EMP_DESIGNATION = value.EMP_DESIGNATION;
            $scope.marker.STATUS = value.STATUS;
            $scope.marker.SSED_EXTN_DT = value.SSED_EXTN_DT;
            $scope.marker.Selected = value.Selected;
            $scope.marker.SSAD_ID = value.SSAD_ID;
            $scope.marker.SSED_REQ_ID = value.SSED_REQ_ID;
            $scope.marker.layer = value.SSA_SPC_TYPE;
            $scope.marker.STACHECK = UtilityService.Unchanged;
            $scope.Markers.push($scope.marker);
        });
    };

    function markerclicked(e) {
        var marker = _.find($scope.Markers, { SVD_SPC_ID: this.SVD_SPC_ID });
        if (!this.Selected) {
            this.Selected = true;
            marker.ticked = true;
        }
        else {
            this.Selected = false;
            marker.Selected = false;
        }
        $scope.chkChanged(this);
        $scope.UpdateapprOptions.api.refreshView();
    }

    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true
        });
    };

}]);
