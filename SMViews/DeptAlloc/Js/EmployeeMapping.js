﻿app.service("EmployeeMappingService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    this.GetPendingEmpMapList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/EmployeeMapping/GetPendingEmpMapList')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetDetailsOnSelection = function (selectedid) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/EmployeeMapping/GetDetailsOnSelection', selectedid)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SaveSelectedSpaces = function (ReqObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/EmployeeMapping/SaveSelectedSpaces', ReqObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetRepeatColumns = function (ReqObj) {
        //console.log(ReqObj);
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/EmployeeMapping/GetRepeatColumns', ReqObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetRepeatColumns_FreeSeat = function (ReqObj) {
        //console.log(ReqObj);
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/EmployeeMapping/GetRepeatColumns_FreeSeat', ReqObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('EmployeeMappingController', ['$scope', '$q', 'UtilityService', 'L1ApprovalService', 'SpaceRequisitionService', 'EmployeeMappingService','$filter', function ($scope, $q, UtilityService, L1ApprovalService, SpaceRequisitionService, EmployeeMappingService, $filter) {
    $scope.Viewstatus = 0;
    $scope.EmpMapReqPageSize = '10';
    $scope.UpdateApprPageSize = '10';
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.Verticallist = [];
    $scope.Costcenterlist = [];
    $scope.AllocatedSeats = [];
    $scope.Flrlist = [];
    $scope.currentblkReq = {};
    $scope.RetStatus = UtilityService.Added;
    $scope.EnableStatus = 0;
    $scope.tempspace = {};
    $scope.selectedSpaces = [];
    $scope.tickedSpaces = [];
    $scope.selectedLocations = [];
    $scope.SpaceReqCount = [];
    $scope.sendCheckedValsObj = [];
    //$scope.L1Approve = {};
    $scope.EmpDetails = [];
    $scope.SysPreference = [];
    $scope.Type = [];
    $scope.Shifts = [];
    $scope.MapFloors = [];
    $scope.SelRowData = [];
    $scope.Markers = [];
    $scope.TempMarkers = [];
    $scope.SelLayers = [];
    $scope.SelectedSeatCount = {};
    $scope.chkdcount = [];
    $scope.delcnt = [];
    $scope.SpcReqSearch = {};
    var map = L.map('leafletMap');


    $scope.EmpMapReqDefs = [
        {
            headerName: "Requisition ID", field: "SRN_REQ_ID", cellClass: "grid-align", filter: 'set',
            template: '<a ng-click="onRowSelectedFunc(data)">{{data.SRN_REQ_ID}}</a>', suppressMenu: true, pinned: 'left'
        },
        { headerName: "Status", field: "STA_DESC", cellClass: "grid-align", pinned: 'left' },
        { headerName: "Requested By", field: "AUR_KNOWN_AS", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requested Date ", template: '<span>{{data.SRN_REQ_DT | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },
        { headerName: "", width: 138, field: "VER_NAME", cellClass: "grid-align" },
        { headerName: "", width: 138, field: "COST_CENTER_NAME", cellClass: "grid-align" },
        { headerName: "From Date", width: 140, template: '<span>{{data.SRN_FROM_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },
        { headerName: "To Date", width: 140, template: '<span>{{data.SRN_TO_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },

    ];

    $scope.EmpMapRequisitions = {
        columnDefs: $scope.EmpMapReqDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        enableSorting: true,
        rowSelection: 'multiple',
    };



    var SpacesColumnDefs = [
        { headerName: "Select All", field: "ticked", width: 40, suppressMenu: true, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)'>", headerCellRenderer: headerCellRendererFunc, pinned: 'left' },
        { headerName: "Space ID", field: "SRD_SPC_NAME", width: 70, cellClass: "grid-align", pinned: 'left' },
        { headerName: "Space Type", field: "SRD_SPC_TYPE_NAME", width: 50, cellClass: "grid-align" },
        { headerName: "Space Sub Type", field: "SRD_SPC_SUB_TYPE_NAME", width: 60, cellClass: "grid-align" },
        { headerName: "Grade Type", field: "SRC_REQ_SEL_TYPE", hide: true, width: 80, suppressMenu: true, cellClass: "grid-align" },
        { headerName: "Request Type", field: "SRC_REQ_TYPE", width: 90, hide: true, suppressMenu: true, cellClass: "grid-align" },
        {
            headerName: "Shift Type", field: "SRD_SH_CODE", width: 70, cellClass: "grid-align", filter: 'set',
            //template: "<select id={{rowIndex}}  class='form-control' data-ng-change='CheckSpace(data,this)' ng-model='data.SRD_SH_CODE'><option value=''>--Select--</option><option ng-repeat='Shift in Shifts | filter:data.SRD_LCM_CODE' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>",
            cellRenderer: function (params) {
                if (params.data.parent == undefined)
                    return "<select id='" + params.rowIndex + "' data-ng-change='CheckSpace(data," + params.rowIndex + ")' ng-model='data.SRD_SH_CODE' ng-disabled='data.SRD_SH_CODE'><option value=''>--Select--</option><option ng-repeat='Shift in Shifts | filter:data.SRD_LCM_CODE' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>";
                else
                    return params.data.SRD_SH_NAME;
            },
            suppressMenu: true,
        },
        {
            headerName: "Employee", field: "SRD_AUR_ID", width: 70, cellClass: "grid-align", filter: 'set',
            //template: "<select  class='form-control'  ng-model='data.SRD_AUR_ID' data-ng-change='GetDesignation(data,\"{{data.SRD_AUR_ID}}\")'><option value=''>--Select--</option><option data-ng-disabled='Emp.ticked' ng-repeat='Emp in EmpDetails' value='{{Emp.AUR_ID}}'>{{Emp.AUR_KNOWN_AS}}</option></select>",
            cellRenderer: function (params) {
                //console.log( params.data);
                //console.log(params.data.parent);
                return "<select id='" + params.rowIndex + "'  ng-model='data.SRD_AUR_ID' data-ng-change='GetDesignation(data,\"{{data.SRD_AUR_ID}}\")'><option value=''>--Select--</option><option data-ng-disabled='Emp.ticked' ng-repeat='Emp in EmpDetails' value='{{Emp.AUR_ID}}'>{{Emp.AUR_KNOWN_AS}}</option></select>";
            },
            suppressMenu: true,
        },
        {
            headerName: "Designation", field: "AUR_DES_NAME", width: 80, cellClass: "grid-align", filter: 'set',
            template: "<label> {{data.AUR_DES_NAME}}</label>", suppressMenu: true,
        },
    ];

    $scope.SpacesOptions = {
        columnDefs: SpacesColumnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.SpacesOptions.api.sizeColumnsToFit()
        }
    };
    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });
    function headerCellRendererFunc(params) {
        var cb1 = document.createElement('input');
        cb1.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle1 = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb1);
        eHeader.appendChild(eTitle1);

        cb1.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = true;
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.ticked = false;
                    });
                });
            }
        });

        return eHeader;
    }

    $scope.chkChanged = function (selctedRow) {
        if ($scope.drawnItems) {
            $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: selctedRow.SRD_SPC_ID, spacetype: 'CHA' } });

            if (selctedRow.ticked) {
                $scope.chkr.setStyle(selctdChrStyle);
                $scope.chkr.ticked = true;
            }
            else {
                $scope.chkr.setStyle(VacantStyle);
                $scope.chkr.ticked = false;
            }
        }
    }

    $scope.back = function () {
        //progress(0, '', false);
        $scope.Viewstatus = 0;
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
        $scope.Getcountry = [];
        $scope.EnableStatus = 0;
        $scope.tickedSpaces = [];
        $scope.Markers = [];
        $scope.SelRowData = [];
        //$scope.Markers = [];
        //$scope.markers = [];
    }

    $scope.GetDesignation = function (Aur, old) {

        Aur.AUR_DES_NAME = "";
        for (var i = 0; $scope.EmpDetails != null && i < $scope.EmpDetails.length; i += 1) {
            if ($scope.EmpDetails[i].AUR_ID === Aur.SRD_AUR_ID) {
                Aur.AUR_DES_NAME = $scope.EmpDetails[i].AUR_DES_NAME;
                $scope.EmpDetails[i].ticked = true;
                Aur.ticked = true;
            }
            if ($scope.EmpDetails[i].AUR_ID === old) {
                $scope.EmpDetails[i].ticked = false;
                Aur.ticked = false;
            }
        }
    }

    function onEmpMapReqFilterChanged(value) {
        $scope.EmpMapRequisitions.api.setQuickFilter(value);
    }
    $("#EmpReqFilter").change(function () {
        onEmpMapReqFilterChanged($(this).val());
    }).keydown(function () {
        onEmpMapReqFilterChanged($(this).val());
    }).keyup(function () {
        onEmpMapReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onEmpMapReqFilterChanged($(this).val());
    })

    function onEmpMapSubFilterChanged(value) {
        $scope.SpacesOptions.api.setQuickFilter(value);
    }
    $("#EmpMapFilter").change(function () {
        onEmpMapSubFilterChanged($(this).val());
    }).keydown(function () {
        onEmpMapSubFilterChanged($(this).val());
    }).keyup(function () {
        onEmpMapSubFilterChanged($(this).val());
    }).bind('paste', function () {
        onEmpMapSubFilterChanged($(this).val());
    })

    EmployeeMappingService.GetPendingEmpMapList().then(function (data) {
        progress(0, 'Loading...', true);
        if (data.data != null) {
            $scope.gridata = data.data;
            //$scope.EmpMapRequisitions.api.setDatasource($scope.createNewDatasource($scope.gridata, $scope.EmpMapReqPageSize));
            $scope.EmpMapRequisitions.api.setRowData([]);
            $scope.EmpMapRequisitions.api.setRowData($scope.gridata);
            $scope.EmpMapRequisitions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.EmpMapRequisitions.columnApi.getColumn("COST_CENTER_NAME").colDef.headerName = $scope.BsmDet.Child;
            $scope.EmpMapRequisitions.api.refreshHeader();
            progress(0, '', false);
        }
        else {
            $scope.EmpMapRequisitions.api.setRowData([]);
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', data.Message);
        }
    }, function (response) {
        progress(0, '', false);
    });
    UtilityService.getSysPreferences().then(function (response) {
        if (response.data != null) {
            $scope.SysPreference = response.data;
            $scope.AllocationType = _.find($scope.SysPreference, { SYSP_CODE: "Allocation Type" })
            $scope.ReqCountType = _.find($scope.SysPreference, { SYSP_CODE: "Requisition Count Type" })
        }
    });
    $scope.onRowSelectedFunc = function (data) {
        $scope.Viewstatus = 1;
        progress(0, 'Loading...', true);
        EmployeeMappingService.GetDetailsOnSelection(data).then(function (response) {
            $scope.TempMarkers = response.data.SELSPACES.spcreqcount;

            GetMarkers(response.data.SELSPACES.spcreqdet);
            if ($scope.Markers.length > 0) {
                $scope.EnableStatus = 1;
                $scope.SpacesOptions.api.setRowData([]);
                $scope.SpacesOptions.api.setRowData($scope.Markers);
                if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                    $scope.SpacesOptions.columnApi.setColumnVisible('SRC_REQ_SEL_TYPE', true);
                    $scope.SpacesOptions.columnApi.setColumnVisible('SRC_REQ_TYPE', true);
                }
            }

            $scope.currentblkReq = data;
            $scope.currentblkReq.SRN_FROM_DATE = $filter('date')(data.SRN_FROM_DATE, "MM/dd/yyyy");
            $scope.currentblkReq.SRN_TO_DATE = $filter('date')(data.SRN_TO_DATE, "MM/dd/yyyy");
            $scope.currentblkReq.SRN_REQ_DATE = $filter('date')(data.SRN_REQ_DT, "MM/dd/yyyy");
            $scope.selectedCostcenters = response.data.SELSPACES.cstlst;

            UtilityService.getCountires(2).then(function (Condata) {
                $scope.countrylist = Condata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                        var a = _.find($scope.countrylist, { CNY_CODE: response.data.SELSPACES.flrlst[i].CNY_CODE });
                        a.ticked = true;
                    }
                }
            });
            UtilityService.getCities(2).then(function (ctydata) {
                $scope.Citylst = ctydata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                        var a = _.find($scope.Citylst, { CTY_CODE: response.data.SELSPACES.flrlst[i].CTY_CODE });
                        a.ticked = true;
                    }
                }

            });
            UtilityService.getLocations(2).then(function (locdata) {
                $scope.Locationlst = locdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                        var a = _.find($scope.Locationlst, { LCM_CODE: response.data.SELSPACES.flrlst[i].LCM_CODE });
                        a.ticked = true;
                        $scope.selectedLocations.push(a);
                    }
                    SpaceRequisitionService.getShifts(response.data.SELSPACES.loclst).then(function (shft) {
                        $scope.Shifts = shft.data;
                        //console.log($scope.Shifts);
                    });
                }

            });
            UtilityService.getTowers(2).then(function (twrdata) {
                $scope.Towerlist = twrdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                        var a = _.find($scope.Towerlist, { TWR_CODE: response.data.SELSPACES.flrlst[i].TWR_CODE });
                        a.ticked = true;
                    }
                }

            });
            UtilityService.getFloors(2).then(function (Flrdata) {
                $scope.Floorlist = Flrdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                        var flr = _.find($scope.Floorlist, { FLR_CODE: response.data.SELSPACES.flrlst[i].FLR_CODE });
                        flr.ticked = true;
                        $scope.Flrlist.push(flr);
                    }
                }
            });
            UtilityService.getVerticals(2).then(function (Verdata) {
                $scope.Verticallist = Verdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.verlst.length; i++) {
                        var ver = _.find($scope.Verticallist, { VER_CODE: response.data.SELSPACES.cstlst[i].Vertical_Code });
                        ver.ticked = true;

                    }

                }
            });
            UtilityService.getCostCenters(2).then(function (Cosdata) {
                $scope.Costcenterlist = Cosdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.cstlst.length; i++) {
                        var cos = _.find($scope.Costcenterlist, { Cost_Center_Code: response.data.SELSPACES.cstlst[i].Cost_Center_Code });
                        cos.ticked = true;
                    }

                }


                setTimeout(function () {
                    $scope.currentblkReq.SRN_FROM_DATE = $scope.currentblkReq.SRN_FROM_DATE;
                    $scope.currentblkReq.SRN_TO_DATE = $scope.currentblkReq.SRN_TO_DATE;

                    var SpaceData = { cstlst: $scope.selectedCostcenters, spcreq: $scope.currentblkReq };
                    SpaceRequisitionService.getEmpDetails(SpaceData).then(function (response) {
                        if (response.data != null) {
                            $scope.EmpDetails = response.data;
                            progress(0, '', false);
                        }
                        else {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', response.Message);
                        }
                    }, function (error) {
                        progress(0, '', false);
                        console.log(error);
                    });
                }, 1000);
            });
        }, function (response) {
            progress(0, '', false);
        });
    };


    $scope.CheckSpace = function (Data, rowindex) {
        //console.log(Data);
        //console.log($scope.currentblkReq);
        $scope.selspcObj = {};
        $scope.spcreqdet = [];
        $scope.selspcObj.SRD_SH_CODE = Data.SRD_SH_CODE;
        $scope.selspcObj.SRD_SPC_ID = Data.SRD_SPC_ID;

        $scope.spcreqdet.push($scope.selspcObj);
        var SpaceData = { spcreq: $scope.currentblkReq, spcreqdet: $scope.spcreqdet };

        SpaceRequisitionService.CheckSpace(SpaceData).then(function (response) {

            if (response.data == 2) {
                Data.ticked = false;
                $scope.chkChanged(Data);
                Data.SRD_SH_CODE = '';
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else if (response.data == 0) {
                Data.ticked = false;
                $scope.chkChanged(Data);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else {
                var Rpt_Columns = _.find($scope.Shifts, { SH_CODE: Data.SRD_SH_CODE });
                //console.log(Rpt_Columns);
                _.remove($scope.Markers, function (marker) {
                    return marker.parent == rowindex;
                });
                if (Rpt_Columns.SH_SEAT_TYPE == 3) {
                    EmployeeMappingService.GetRepeatColumns_FreeSeat($scope.selspcObj).then(function (response) {
                        //console.log(response.data.length);
                        //console.log(Rpt_Columns);
                        //console.log(Rpt_Columns.REP_COL - (response.data.length + 1));
                        for (var i = 0; i < Rpt_Columns.REP_COL - (response.data.length + 1); i++) {
                            var newObject = {};
                            angular.copy(Data, newObject)
                            newObject.parent = rowindex;
                            newObject.SRD_SH_NAME = Rpt_Columns.SH_NAME;
                            newObject.SRD_SH_CODE = Rpt_Columns.SH_CODE;
                            newObject.ticked = false;
                            newObject.SRD_REQ_ID = "";
                            $scope.Markers.splice(rowindex + (i + 1), 0, newObject);
                        }
                        $scope.SpacesOptions.api.setRowData([]);
                        $scope.SpacesOptions.api.setRowData($scope.Markers);
                    }, function (response) {
                        progress(0, '', false);
                    });
                }
                else if (Rpt_Columns.SH_SEAT_TYPE == 2) {

                    $scope.selspcObj.SRN_FROM_DATE = $scope.currentblkReq.SRN_FROM_DATE;
                    $scope.selspcObj.SRN_TO_DATE = $scope.currentblkReq.SRN_TO_DATE;
                    EmployeeMappingService.GetRepeatColumns($scope.selspcObj).then(function (response) {
                        for (var i = 0; i < response.data.length; i++) {
                            var newObject = {};
                            angular.copy(Data, newObject)
                            newObject.parent = rowindex;
                            newObject.SRD_SH_NAME = response.data[i].SH_NAME;
                            newObject.SRD_SH_CODE = response.data[i].SH_CODE;
                            newObject.ticked = false;
                            newObject.SRD_REQ_ID = "";
                            $scope.Markers.splice(rowindex + (i + 1), 0, newObject);
                        }
                        $scope.SpacesOptions.api.setRowData([]);
                        $scope.SpacesOptions.api.setRowData($scope.Markers);

                    }, function (response) {
                        progress(0, '', false);
                    });
                }

                $scope.SpacesOptions.api.setRowData([]);
                $scope.SpacesOptions.api.setRowData($scope.Markers);

                $scope.chkChanged(Data);
                Data.ticked = true;

            }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.SaveSelectedSpaces = function () {
        progress(0, 'Loading...', true);
        var flag = false;
        var count = 0;
        for (var i = 0; $scope.TempMarkers != null && i < $scope.TempMarkers.length; i++) {
            count += $scope.TempMarkers[i].SRC_ALLC_CNT;
        }
        var cnt = _.countBy($scope.Markers, 'ticked');
        if (cnt.true != count) {
            $scope.tickedSpaces = [];
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', 'Please select ' + count + ' employee(s)');
            flag = true;
            return;
        }
        for (var i = 0; $scope.Markers != null && i < $scope.Markers.length; i++) {
            $scope.selspcObj = {};
            if ($scope.Markers[i].ticked && ($scope.Markers[i].SRD_SH_CODE == undefined || $scope.Markers[i].SRD_SH_CODE == "")) {
                $scope.tickedSpaces = [];
                flag = true;
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Please select shift');
                break;
            }
            else if ($scope.Markers[i].ticked && ($scope.Markers[i].SRD_AUR_ID == undefined || $scope.Markers[i].SRD_AUR_ID == "")) {
                $scope.tickedSpaces = [];
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Please select employee');
                flag = true;
                break;
            }
            else if ($scope.Markers[i].ticked && $scope.Markers[i].SRD_SH_CODE && $scope.Markers[i].SRD_AUR_ID) {
                $scope.selspcObj = {};
                $scope.selspcObj.SRD_REQ_ID = $scope.Markers[i].SRD_REQ_ID;
                $scope.selspcObj.SRD_SRNREQ_ID = $scope.Markers[i].SRD_SRNREQ_ID;
                $scope.selspcObj.SRD_SSA_SRNREQ_ID = $scope.Markers[i].SRD_SSA_SRNREQ_ID;
                $scope.selspcObj.SRD_SPC_ID = $scope.Markers[i].SRD_SPC_ID;
                $scope.selspcObj.SRD_SPC_NAME = $scope.Markers[i].SRD_SPC_NAME;
                $scope.selspcObj.SRD_SPC_TYPE = $scope.Markers[i].layer;
                //$scope.selspcObj.lat = data.lat;
                //$scope.selspcObj.lon = data.lon;
                $scope.selspcObj.SRD_SPC_TYPE_NAME = $scope.Markers[i].SRD_SPC_TYPE_NAME;
                $scope.selspcObj.SRD_SPC_SUB_TYPE = $scope.Markers[i].SRD_SPC_SUB_TYPE;
                $scope.selspcObj.SRD_SPC_SUB_TYPE_NAME = $scope.Markers[i].SRD_SPC_SUB_TYPE_NAME;
                $scope.selspcObj.SRD_SH_CODE = $scope.Markers[i].SRD_SH_CODE;
                $scope.selspcObj.SRD_AUR_ID = $scope.Markers[i].SRD_AUR_ID;
                $scope.selspcObj.SSA_FLR_CODE = $scope.Markers[i].SSA_FLR_CODE;
                $scope.selspcObj.ticked = $scope.Markers[i].ticked;
                $scope.selspcObj.STACHECK = $scope.Markers[i].STACHECK;
                $scope.tickedSpaces.push($scope.selspcObj);
            }
        };

        if (flag == false) {
            setTimeout(function () {
                var ReqObj = { spcreqdet: $scope.tickedSpaces, spcreq: $scope.currentblkReq };
                console.log(ReqObj);
                EmployeeMappingService.SaveSelectedSpaces(ReqObj).then(function (response) {
                    if (response.data != null) {
                        progress(0, '', false);
                        EmployeeMappingService.GetPendingEmpMapList().then(function (data) {
                            progress(0, 'Loading...', true);
                            if (data.data != null) {
                                $scope.EmpMapRequisitions.api.setRowData(data.data);
                                progress(0, '', false);
                            }
                            else {
                                $scope.EmpMapRequisitions.api.setRowData([]);
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', data.Message);
                            }
                        }, function (response) {
                            progress(0, '', false);
                        });

                        $scope.back();
                        showNotification('success', 8, 'bottom-right', response.Message);
                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', response.Message);
                    }
                });

            }, 500);
        }
    }

    ///// For map layout
    $scope.ViewinMap = function () {
        if ($scope.SelRowData.length == 0) {
            $scope.MapFloors = [];
            $scope.Map.Floor = [];
            angular.forEach($scope.SpaceAllocation.selectedFloors, function (Value, Key) {
                Value.ticked = false;
                //console.log(Value);
                $scope.MapFloors.push(Value);
            });
            $scope.MapFloors[0].ticked = true;
            $scope.Map.Floor.push($scope.MapFloors[0]);
        }
        $("#historymodal").modal('show');
    }

    $('#historymodal').on('shown.bs.modal', function () {
        progress(0, 'Loading...', true);
        if ($scope.SelRowData.length == 0) {
            $scope.loadmap();
        }
        else {
            progress(0, 'Loading...', false);
        }
    });

    $scope.FlrSectMap = function (data) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();
    }

    $scope.loadmap = function () {
        progress(0, 'Loading...', true);
        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE, key_value: 1 };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post(UtilityService.path + '/api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            //console.log(result);
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });

    }

    $scope.loadMapDetails = function (result) {
        //console.log(result);
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
            // success
            // results: an array of data objects from each deferred.resolve(data) call
            function (results) {
                var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
                map.fitBounds(bounds);
                //console.log($scope.Markers);
                $scope.SelRowData = $filter('filter')($scope.Markers, { SSA_FLR_CODE: $scope.Map.Floor[0].FLR_CODE });
                angular.forEach($scope.SelRowData, function (value, key) {
                    $scope.marker = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: value.SRD_SPC_ID, spacetype: 'CHA' } });
                    $scope.marker.SRD_REQ_ID = value.SRD_REQ_ID;
                    $scope.marker.SRD_SRNREQ_ID = value.SRD_SRNREQ_ID;
                    $scope.marker.SRD_SSA_SRNREQ_ID = value.SRD_SSA_SRNREQ_ID;
                    $scope.marker.SRD_SPC_ID = value.SRD_SPC_ID;
                    $scope.marker.SRD_SPC_NAME = value.SRD_SPC_NAME;
                    $scope.marker.layer = value.SRD_SPC_TYPE;
                    $scope.marker.SRD_SPC_TYPE_NAME = value.SRD_SPC_TYPE_NAME;
                    $scope.marker.SRD_SPC_SUB_TYPE = value.SRD_SPC_SUB_TYPE;
                    $scope.marker.SRD_SPC_SUB_TYPE_NAME = value.SRD_SPC_SUB_TYPE_NAME;
                    $scope.marker.SRD_SH_CODE = value.SRD_SH_CODE;
                    $scope.marker.SRD_AUR_ID = value.SRD_AUR_ID;
                    $scope.marker.SSA_FLR_CODE = value.SSA_FLR_CODE;
                    $scope.marker.STACHECK = value.STACHECK;
                    $scope.marker.ticked = value.ticked;
                    $scope.marker.SRC_REQ_SEL_TYPE = value.SRC_REQ_SEL_TYPE;
                    $scope.marker.SRC_REQ_TYPE = value.SRC_REQ_TYPE;
                    $scope.marker.SelectedSpacesList = null;
                    if (value.ticked)
                        $scope.marker.setStyle(selctdChrStyle);
                    else
                        $scope.marker.setStyle(VacantStyle);
                    $scope.marker.bindLabel(value.SRD_SPC_NAME);
                    $scope.marker.on('click', markerclicked);
                    $scope.marker.addTo(map);
                });
            },
            // error
            function (response) {
            }
        );
    };

    //var Vacanticon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_Green.gif',
    //    iconSize: [16, 16], // size of the icon
    //});
    //var selctdChricon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_yellow.gif',
    //    iconSize: [16, 16], // size of the icon
    //});

    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };

    function GetMarkers(data) {
        jQuery.each(data, function (index, value) {
            $scope.marker = {};
            $scope.marker.SRD_REQ_ID = value.SRD_REQ_ID;
            $scope.marker.SRD_SRNREQ_ID = value.SRD_SRNREQ_ID;
            $scope.marker.SRD_SSA_SRNREQ_ID = value.SRD_SSA_SRNREQ_ID;
            $scope.marker.SRD_SPC_ID = value.SRD_SPC_ID;
            $scope.marker.SRD_SPC_NAME = value.SRD_SPC_NAME;
            $scope.marker.layer = value.SRD_SPC_TYPE;
            $scope.marker.SRD_SPC_TYPE_NAME = value.SRD_SPC_TYPE_NAME;
            $scope.marker.SRD_SPC_SUB_TYPE = value.SRD_SPC_SUB_TYPE;
            $scope.marker.SRD_SPC_SUB_TYPE_NAME = value.SRD_SPC_SUB_TYPE_NAME;
            $scope.marker.SRD_SH_CODE = value.SRD_SH_CODE;
            $scope.marker.SRD_AUR_ID = value.SRD_AUR_ID;
            $scope.marker.SSA_FLR_CODE = value.SSA_FLR_CODE;
            $scope.marker.STACHECK = value.STACHECK;
            $scope.marker.ticked = value.ticked;
            $scope.marker.SRC_REQ_SEL_TYPE = value.SRC_REQ_SEL_TYPE;
            $scope.marker.SRC_REQ_TYPE = value.SRC_REQ_TYPE;
            $scope.marker.SelectedSpacesList = null;
            $scope.Markers.push($scope.marker);

        });
        //console.log($scope.Markers);
    };

    function markerclicked(e) {

        var marker = _.find($scope.Markers, { SRD_SPC_ID: this.SRD_SPC_ID });
        if (!this.ticked) {
            this.setStyle(selctdChrStyle)
            this.ticked = true;
            marker.ticked = true;
        }
        else {

            this.setStyle(VacantStyle)
            this.ticked = false;
            marker.ticked = false;
        }
        $scope.SpacesOptions.api.refreshView();
    }

}]);