﻿app.service("L2ApprovalService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    this.GetPendingList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/L2Approval/GetPendingSpaceRequisitions')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //this.GetDetailsOnSelection = function (selectedid) {
    //    var deferred = $q.defer();
    //    return $http.post(UtilityService.path + '/api/L2Approval/GetDetailsOnSelection', selectedid)
    //      .then(function (response) {
    //          deferred.resolve(response.data);
    //          return deferred.promise;
    //      }, function (response) {
    //          deferred.reject(response);
    //          return deferred.promise;
    //      });
    //};
    this.ApproveRequisitions = function (ReqObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/L2Approval/ApproveRequisitions', ReqObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.ApproveAllReq = function (ReqObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/L2Approval/ApproveAllReq', ReqObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('L2ApprovalController', ['$scope', '$q', 'L1ApprovalService', 'L2ApprovalService', 'UtilityService', 'SpaceRequisitionService','$filter', function ($scope, $q, L1ApprovalService, L2ApprovalService, UtilityService, SpaceRequisitionService, $filter) {
    $scope.Viewstatus = 0;
    $scope.ApprvlPageSize = '10';
    $scope.UpdateApprPageSize = '10';
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.Verticallist = [];
    $scope.Costcenterlist = [];
    $scope.selectedSeats = [];
    $scope.currentblkReq = {};
    $scope.tickedSpaces = [];
    $scope.RetStatus = UtilityService.Added;
    $scope.EnableStatus = 0;
    $scope.tempspace = {};
    $scope.selectedSpaces = [];
    $scope.selectedLocations = [];
    $scope.SpaceReqCount = [];
    $scope.sendCheckedValsObj = [];
    $scope.L2Approve = {};
    $scope.EmpDetails = [];
    $scope.SysPreference = [];
    $scope.Type = [];
    $scope.Shifts = [];
    $scope.MapFloors = [];
    $scope.Markers = [];
    $scope.SelLayers = [];
    $scope.SelRowData = [];
    $scope.EnableSpacesOptions = false;

    $scope.EnableApprove = true;
    $scope.ClosedSpacesOptions = true;
    $scope.EnableClosedSpacesOptions = false;
    var map = L.map('leafletMap');

    $scope.L2approvalsDefs = [
        { headerName: "Select All", width: 75, field: "ticked", pinned: 'left', template: "<input type='checkbox' ng-model='data.ticked'>", cellClass: 'grid-align', filter: 'set', headerCellRenderer: headerPendingCellRendererFunc, suppressMenu: true, },
        { headerName: "Requisition ID", field: "SRN_REQ_ID", cellClass: "grid-align", filter: 'set', template: '<a ng-click="onRowSelectedFunc(data)">{{data.SRN_REQ_ID}}</a>', pinned: 'left', suppressMenu: true, },
        { headerName: "Requested By", field: "AUR_KNOWN_AS", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requested Date ", template: '<span>{{data.SRN_REQ_DT | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align" },
        { headerName: "", width: 138, field: "VER_NAME", cellClass: "grid-align" },
        { headerName: "", width: 138, field: "COST_CENTER_NAME", cellClass: "grid-align" },
        { headerName: "From Date", width: 140, template: '<span>{{data.SRN_FROM_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },
        { headerName: "To Date", width: 140, template: '<span>{{data.SRN_TO_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Status", field: "STA_DESC", cellClass: "grid-align", pinned: 'right' }
    ];
    $scope.L2approvalOptions = {
        columnDefs: $scope.L2approvalsDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        enableColResize: true,
        rowSelection: 'multiple',

    };
    function onUpdateFilterChanged(value) {
        $scope.L2approvalOptions.api.setQuickFilter(value);
    }
    $("#ApprvlFilter").change(function () {
        onUpdateFilterChanged($(this).val());
    }).keydown(function () {
        onUpdateFilterChanged($(this).val());
    }).keyup(function () {
        onUpdateFilterChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFilterChanged($(this).val());
    })

    $scope.UpdateapprDefs = [
        { headerName: "Select All", field: "ticked", pinned: 'left', width: 80, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)'>", headerCellRenderer: headerCellRendererFunc, suppressMenu: true },
        { headerName: "Space ID", field: "SRD_SPC_NAME", width: 180, cellClass: "grid-align", suppressMenu: true, pinned: 'left' },
        { headerName: "Space Type", field: "SRD_SPC_TYPE_NAME", width: 120, cellClass: "grid-align" },
        { headerName: "Space Sub Type", field: "SRD_SPC_SUB_TYPE_NAME", width: 120, cellClass: "grid-align" },
        { headerName: "Shift Type", field: "SRD_SH_CODE", width: 180, suppressMenu: true, cellClass: "grid-align", filter: 'set', template: "<select ng-model='data.SRD_SH_CODE' ng-change='CheckSpace(data)' ><option value=''>--Select--</option><option ng-repeat='Shift in Shifts | filter:data.SRD_LCM_CODE' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>" },
        //{ headerName: "Employee", field: "SRD_AUR_ID", hide: true, width: 150, cellClass: "grid-align", filter: 'set', template: "<select  class='form-control'  ng-model='data.SRD_AUR_ID' data-ng-change='GetDesignation(data)'><option value=''>--Select--</option><option ng-repeat='Emp in EmpDetails' value='{{Emp.AUR_ID}}'>{{Emp.AUR_KNOWN_AS}}</option></select>" },
        { headerName: "Employee ID", field: "SRD_AUR_ID", hide: true, suppressMenu: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<select ng-model='data.SRD_AUR_ID' data-ng-change='GetDesignation(data,\"{{data.SRD_AUR_ID}}\")'><option value=''>--Select--</option><option data-ng-disabled='Emp.ticked' ng-repeat='Emp in EmpDetails' value='{{Emp.AUR_ID}}'>{{Emp.AUR_ID}}</option></select>" },
        { headerName: "Employee Name", field: "AUR_KNOWN_AS", suppressMenu: true, hide: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<label> {{data.AUR_KNOWN_AS}}</label>" },
        { headerName: "Existing Space Id", field: "ALLOC_SPC_ID", suppressMenu: true, hide: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<label> {{data.ALLOC_SPC_ID}}</label>" },
        { headerName: "Designation", field: "AUR_DES_NAME", hide: true, width: 150, cellClass: "grid-align", filter: 'set', template: "<label> {{data.AUR_DES_NAME}}</label>", suppressMenu: true, },

    ];
    $scope.UpdateapprOptions = {
        columnDefs: $scope.UpdateapprDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple',
        // enableSorting: true,
        //onReady: function () {
        //    $scope.UpdateapprOptions.api.sizeColumnsToFit()
        //}
    };
    function onUpdateFtrChanged(value) {
        $scope.UpdateapprOptions.api.setQuickFilter(value);
    }
    $("#UpdteFilter").change(function () {
        onUpdateFtrChanged($(this).val());
    }).keydown(function () {
        onUpdateFtrChanged($(this).val());
    }).keyup(function () {
        onUpdateFtrChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFtrChanged($(this).val());
    })

    var columnDefsCount = [
        { headerName: "Country", field: "SRC_CNY_NAME", cellClass: "grid-align" },
        { headerName: "City", field: "SRC_CTY_NAME", cellClass: "grid-align" },
        { headerName: "Location", field: "SRC_LCM_NAME", cellClass: "grid-align" },
        { headerName: "Tower", field: "SRC_TWR_NAME", cellClass: "grid-align" },
        { headerName: "Floor", field: "SRC_FLR_NAME", cellClass: "grid-align" },
        { headerName: "", field: "SRC_REQ_CNT", cellClass: "grid-align", filter: 'set', template: "<input type='number' ng-model='data.SRC_REQ_CNT'>", suppressMenu: true, },
        {
            headerName: "", field: "SRC_REQ_SEL_TYPE", cellClass: "grid-align", filter: 'set', suppressMenu: true,
            template: "<select ng-model='data.SRC_REQ_SEL_TYPE' data-ng-change='getSubTypes(data)'><option value=''>--Select--</option><option ng-repeat='Ty in Type' value='{{Ty.CODE}}'>{{Ty.NAME}}</option></select>"
        },
        {
            headerName: "", field: "SRC_REQ_TYPE", cellClass: "grid-align", filter: 'set', suppressMenu: true,
            template: "<select ng-init='' ng-model='data.SRC_REQ_TYPE'><option value=''>--Select--</option><option ng-repeat='SubTy in SubType' value='{{SubTy.CODE}}'>{{SubTy.NAME}}</option></select>"
        },
    ];
    $scope.gridCountOptions = {
        columnDefs: columnDefsCount,
        rowData: null,
        //enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.gridCountOptions.api.sizeColumnsToFit()
        }
    };
    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });
    UtilityService.getSysPreferences().then(function (response) {
        if (response.data != null) {
            $scope.SysPreference = response.data;
            $scope.AllocationType = _.find($scope.SysPreference, { SYSP_CODE: "Allocation Type" })
            $scope.ReqCountType = _.find($scope.SysPreference, { SYSP_CODE: "Requisition Count Type" })
        }
    });

    function gridloader() {

        L2ApprovalService.GetPendingList().then(function (data) {
            $scope.val1 = [];
            angular.forEach(data.data, function (val1) {
                if (val1.SRN_STA_ID == 1054) {

                    $scope.EnableClosedSpacesOptions = true;

                    $scope.ClosedSpacesOptions = false;
                }
            });
            progress(0, 'Loading...', true);
            if (data.data != null) {
                $scope.gridata = data.data;
                $scope.L2approvalOptions.api.setRowData([]);
                $scope.L2approvalOptions.api.setRowData($scope.gridata);
                $scope.L2approvalOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
                $scope.L2approvalOptions.columnApi.getColumn("COST_CENTER_NAME").colDef.headerName = $scope.BsmDet.Child;
                $scope.L2approvalOptions.api.refreshHeader();
                progress(0, '', false);
            }
            else {
                $scope.L2approvalOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', data.Message);

            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    gridloader();


    $scope.onRowSelectedFunc = function (data) {
        progress(0, 'Loading...', true);
        $scope.Shifts = [];
        $scope.Viewstatus = 1;
        $scope.selctedRow = data;

        L1ApprovalService.GetDetailsOnSelection(data).then(function (response) {
            //   progress(0, 'Loading...', true);
            $scope.currentblkReq = data;
            $scope.currentblkReq.SRN_FROM_DATE = $filter('date')(data.SRN_FROM_DATE, "MM/dd/yyyy");
            $scope.currentblkReq.SRN_TO_DATE = $filter('date')(data.SRN_TO_DATE, "MM/dd/yyyy");
            $scope.currentblkReq.SRN_REQ_DATE = $filter('date')(data.SRN_REQ_DT, "MM/dd/yyyy");
            angular.forEach(response.data.SELSPACES.spcreqdet, function (val) {
                if (val.SRD_STA_ID == 1054) {
                    $scope.EnableSpacesOptions = true;
                    $scope.EnableApprove = false;
                }
            });
            UtilityService.getCountires(2).then(function (Condata) {
                $scope.countrylist = Condata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                        var a = _.find($scope.countrylist, { CNY_CODE: response.data.SELSPACES.flrlst[i].CNY_CODE });
                        a.ticked = true;

                    }
                }
                else {
                    $scope.Citylst = [];
                    $scope.Locationlst = [];
                    $scope.Towerlist = [];
                    $scope.Floorlist = [];
                }
            });
            UtilityService.getCities(2).then(function (ctydata) {
                $scope.Citylst = ctydata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                        var a = _.find($scope.Citylst, { CTY_CODE: response.data.SELSPACES.flrlst[i].CTY_CODE });
                        a.ticked = true;
                    }
                }
                else {
                    $scope.Citylst = [];
                    $scope.Locationlst = [];
                    $scope.Towerlist = [];
                    $scope.Floorlist = [];
                }
            });
            UtilityService.getLocations(2).then(function (locdata) {
                $scope.Locationlst = locdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                        var a = _.find($scope.Locationlst, { LCM_CODE: response.data.SELSPACES.flrlst[i].LCM_CODE });
                        a.ticked = true;
                        $scope.selectedLocations.push(a);
                    }
                    SpaceRequisitionService.getShifts($scope.selectedLocations).then(function (shft) {
                        $scope.Shifts = [];
                        $scope.Shifts = shft.data;
                    });
                }
                else {
                    $scope.Citylst = [];
                    $scope.Locationlst = [];
                    $scope.Towerlist = [];
                    $scope.Floorlist = [];
                }
            });
            UtilityService.getTowers(2).then(function (twrdata) {
                $scope.Towerlist = twrdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                        var a = _.find($scope.Towerlist, { TWR_CODE: response.data.SELSPACES.flrlst[i].TWR_CODE });
                        a.ticked = true;
                    }
                }
                else {
                    $scope.Citylst = [];
                    $scope.Locationlst = [];
                    $scope.Towerlist = [];
                    $scope.Floorlist = [];
                }
            });
            UtilityService.getFloors(2).then(function (Flrdata) {
                $scope.Floorlist = Flrdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                        var flr = _.find($scope.Floorlist, { FLR_CODE: response.data.SELSPACES.flrlst[i].FLR_CODE });
                        flr.ticked = true;
                    }

                }
                else {
                    $scope.Floorlist = [];

                }

            });
            UtilityService.getVerticals(2).then(function (Verdata) {
                $scope.Verticallist = Verdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.verlst.length; i++) {
                        var ver = _.find($scope.Verticallist, { VER_CODE: response.data.SELSPACES.cstlst[i].Vertical_Code });
                        ver.ticked = true;
                        $scope.L2Approve.selectedVerticals = ver;
                    }

                }
                else {
                    $scope.Costcenterlist = [];

                }
            });
            UtilityService.getCostCenters(2).then(function (Cosdata) {
                $scope.Costcenterlist = Cosdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.cstlst.length; i++) {
                        var cos = _.find($scope.Costcenterlist, { Cost_Center_Code: response.data.SELSPACES.cstlst[i].Cost_Center_Code });
                        cos.ticked = true;
                    }

                }

            });
            $scope.selectedSpaces = response.data.SELSPACES.spcreqdet;

            if ($scope.AllocationType.SYSP_VAL1 == "1039" || $scope.AllocationType.SYSP_VAL1 == "1040") {
                $scope.Markers = [];
                GetMarkers(response.data.DETAILS);
                $scope.UpdateapprOptions.api.setRowData([]);
                $scope.UpdateapprOptions.api.setRowData((_.filter($scope.Markers, function (o) { return o.ticked == true; })));
            }

            if ($scope.AllocationType.SYSP_VAL1 == "1040") {
                $scope.UpdateapprOptions.columnApi.setColumnVisible('SRD_AUR_ID', true);
                $scope.UpdateapprOptions.columnApi.setColumnVisible('AUR_DES_NAME', true);
                $scope.UpdateapprOptions.columnApi.setColumnVisible('ALLOC_SPC_ID', true);
                $scope.UpdateapprOptions.columnApi.setColumnVisible('AUR_KNOWN_AS', true);
                var SpaceData = { cstlst: response.data.SELSPACES.cstlst, spcreq: $scope.currentblkReq };

                SpaceRequisitionService.getEmpDetails(SpaceData).then(function (emp) {
                    $scope.EmpDetails = emp.data;
                    angular.forEach($scope.Markers, function (Value, Key) {
                        if (Value.ticked) {
                            Value.AUR_DES_NAME = _.find($scope.EmpDetails, { AUR_ID: Value.SRD_AUR_ID }).AUR_DES_NAME;
                            Value.ALLOC_SPC_ID = _.find($scope.EmpDetails, { AUR_ID: Value.SRD_AUR_ID }).ALLOC_SPC_ID;
                            var selrow = _.find($scope.selectedSpaces, { SRD_REQ_ID: Value.SRD_REQ_ID });
                            selrow.ALLOC_SPC_ID = _.find($scope.EmpDetails, { AUR_ID: Value.SRD_AUR_ID }).ALLOC_SPC_ID;
                            Value.AUR_KNOWN_AS = _.find($scope.EmpDetails, { AUR_ID: Value.SRD_AUR_ID }).AUR_KNOWN_AS;
                            //console.log($scope.selectedSpaces);
                            progress(0, '', false);
                        }
                    });
                });
            }
            else if ($scope.AllocationType.SYSP_VAL1 == "1041") {
                $scope.EnableStatus = 1;
                $scope.gridata = [];
                $scope.gridCountOptions.rowData = [];
                var flrObj = { flrlst: response.data.SELSPACES.flrlst };


                if (response.data.SELSPACES.spcreqcount != null) {
                    $scope.gridCountData = response.data.SELSPACES.spcreqcount;
                    $scope.gridCountOptions.api.setRowData([]);
                    $scope.gridCountOptions.api.setRowData($scope.gridCountData);
                    $scope.SpaceReqCount = $scope.gridCountData;


                    if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Employee Count";
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_SEL_TYPE").colDef.headerName = "Grade Type";
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_TYPE").colDef.headerName = "Request Type";
                        $scope.gridCountOptions.api.refreshHeader();
                        $scope.Type = [];


                        SpaceRequisitionService.getEmpTypes(response.data.SELSPACES.cstlst).then(function (Empresponse) {
                            if (Empresponse.data != null) {
                                $scope.Type = Empresponse.data;

                            }
                        });
                        SpaceRequisitionService.getEmpSubTypes().then(function (Subresponse) {
                            if (Subresponse.data != null) {
                                $scope.SubType = Subresponse.data;
                            }
                        });

                    }
                    else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Space Count";
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_SEL_TYPE").colDef.headerName = "Space Type";
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_TYPE").colDef.headerName = "Space Sub Type";
                        $scope.gridCountOptions.api.refreshHeader();
                        $scope.Type = [];
                        $scope.SubType = [];
                        SpaceRequisitionService.getSpaceTypes().then(function (Spcresponse) {
                            if (Spcresponse.data != null) {
                                $scope.Type = Spcresponse.data;
                            }
                        });
                        SpaceRequisitionService.getSpaceSubTypes().then(function (Spcsbresponse) {
                            if (Spcsbresponse.data != null) {
                                $scope.SubType = Spcsbresponse.data;
                            }
                        });
                    }

                }
                else {
                    setTimeout(function () {
                        progress(0, '', false);
                    }, 1500)

                    showNotification('error', 8, 'bottom-right', 'No data available for selected vertical');
                }
            }
            progress(0, '', false);
        }, function (response) {
            progress(0, '', false);
        });
    };
    $scope.CheckSpace = function (Data) {
        $scope.selspcObj = {};
        $scope.spcreqdet = [];

        $scope.selspcObj.SRD_SH_CODE = Data.SRD_SH_CODE;
        $scope.selspcObj.SRD_SPC_ID = Data.SRD_SPC_ID;

        $scope.spcreqdet.push($scope.selspcObj);
        var SpaceData = { spcreq: $scope.currentblkReq, spcreqdet: $scope.spcreqdet };

        SpaceRequisitionService.CheckSpace(SpaceData).then(function (response) {
            if (response.data == 2) {
                Data.ticked = false;
                $scope.chkChanged(Data);
                Data.SRD_SH_CODE = '';
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else if (response.data == 0) {
                Data.ticked = false;
                $scope.chkChanged(Data);
                Data.SRD_SH_CODE = '';
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else {
                $scope.chkChanged(Data);
                Data.ticked = true;
            }
        }, function (error) {
            console.log(error);
        });
    }
    $scope.setStatus = function (status) {

        switch (status) {
            case 'Approve': $scope.RetStatus = UtilityService.Approved;

                $scope.currentblkReq.SRN_L2_APPR_BY = UID;
                break;
            case 'closed': $scope.RetStatus = UtilityService.closed;
                $scope.currentblkReq.SRN_L2_APPR_BY = UID;
                break;
            case 'Reject': $scope.RetStatus = UtilityService.Rejected;
                $scope.currentblkReq.SRN_L2_APPR_BY = UID;

                break;
            case 'ApproveAll':
                $scope.RetStatus = UtilityService.Approved;

                angular.forEach($scope.gridata, function (data) {
                    if (data.ticked == true) {
                        $scope.sendCheckedValsObj.push(data);
                    }
                });
                $scope.currentblkReq.SRN_L2_APPR_BY = UID;


                break;
            case 'RejectAll': $scope.RetStatus = UtilityService.Rejected;
                angular.forEach($scope.gridata, function (data) {
                    if (data.ticked == true) {
                        $scope.sendCheckedValsObj.push(data);
                    }
                });

                $scope.currentblkReq.SRN_L2_APPR_BY = UID;

                break;

        }
    }


    $scope.chkChanged = function (selctedRow) {
        if ($scope.drawnItems)
            $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: selctedRow.SRD_SPC_ID, spacetype: 'CHA' } });
        if (!selctedRow.ticked) {
            if ($scope.chkr) {
                $scope.chkr.setStyle(VacantStyle);
                $scope.chkr.ticked = false;
            }
            var selrow = _.find($scope.selectedSpaces, { SRD_SPC_ID: selctedRow.SRD_SPC_ID });
            selrow.STACHECK = UtilityService.Deleted;
            var ExistingIndex = _.indexOf($scope.selectedSpaces, selrow);
            console.log(ExistingIndex);
            $scope.selectedSpaces.splice(ExistingIndex, 1);		

        }
        else {
            if ($scope.chkr) {
                $scope.chkr.setStyle(selctdChrStyle);
                $scope.chkr.ticked = true;
            }
            $scope.tempspace = selctedRow;
            $scope.tempspace.STACHECK = UtilityService.Added;
            $scope.selectedSpaces.push($scope.tempspace);
            $scope.tempspace = {};
        }
        $scope.setStatus();
    }



    $scope.ApproveRequisitions = function () {
        progress(0, 'Loading...', true);
        $scope.tickedSpaces = [];

        angular.forEach($scope.selectedSpaces, function (data, key) {
            $scope.selspcObj = {};
            $scope.selspcObj.SRD_REQ_ID = data.SRD_REQ_ID;
            $scope.selspcObj.SRD_SRNREQ_ID = data.SRD_SRNREQ_ID;
            $scope.selspcObj.SRD_SSA_SRNREQ_ID = data.SRD_SSA_SRNREQ_ID;
            $scope.selspcObj.SRD_SPC_ID = data.SRD_SPC_ID;
            $scope.selspcObj.SRD_SPC_NAME = data.SRD_SPC_NAME;
            $scope.selspcObj.SRD_SPC_TYPE = data.layer;
            $scope.selspcObj.lat = data.lat;
            $scope.selspcObj.lon = data.lon;
            $scope.selspcObj.SRD_SPC_TYPE_NAME = data.SRD_SPC_TYPE_NAME;
            $scope.selspcObj.SRD_SPC_SUB_TYPE = data.SRD_SPC_SUB_TYPE;
            $scope.selspcObj.SRD_SPC_SUB_TYPE_NAME = data.SRD_SPC_SUB_TYPE_NAME;
            $scope.selspcObj.SRD_SH_CODE = data.SRD_SH_CODE;
            $scope.selspcObj.SRD_AUR_ID = data.SRD_AUR_ID;
            $scope.selspcObj.SSA_FLR_CODE = data.SSA_FLR_CODE;
            $scope.selspcObj.ticked = data.ticked;
            $scope.selspcObj.STACHECK = data.STACHECK;
            $scope.selspcObj.ALLOC_SPC_ID = data.ALLOC_SPC_ID;
           // $scope.selspcObj.AUR_KNOWN_AS_NAME = data.AUR_KNOWN_AS_NAME;
            $scope.selspcObj.AUR_KNOWN_AS = data.AUR_KNOWN_AS;
            $scope.tickedSpaces.push($scope.selspcObj);
        });

        $scope.tickedSpaces1 = [];
        angular.forEach($scope.selectedSpaces, function (data, key) {
            $scope.relspcobj = {};
            $scope.relspcobj.SPC_ID = data.ALLOC_SPC_ID;
            //$scope.relspcobj.AUR_KNOWN_AS_NAME = data.AUR_KNOWN_AS_NAME;
            $scope.relspcobj.AUR_KNOWN_AS = data.AUR_KNOWN_AS;
            $scope.relspcobj.SSA_SRNREQ_ID = data.SRD_SRNREQ_ID;
            $scope.relspcobj.SH_CODE = data.SRD_SH_CODE;
            $scope.relspcobj.VERTICAL = data.SRD_SPC_NAME;
            $scope.relspcobj.Cost_Center_Code = data.layer;
            $scope.relspcobj.AUR_ID = data.SRD_AUR_ID;
            $scope.relspcobj.STATUS = data.SRD_SPC_SUB_TYPE;
            //$scope.relspcobj.FROM_DATE =;
            //$scope.relspcobj.TO_DATE = "";
            $scope.relspcobj.STACHECK = data.STACHECK;
            $scope.relspcobj.ticked = data.ticked;
            $scope.tickedSpaces1.push($scope.relspcobj);
        });
        var flag = false;
        if ($scope.AllocationType.SYSP_VAL1 == "1039" || $scope.AllocationType.SYSP_VAL1 == "1040") {
            for (var i = 0; $scope.Markers != null && i < $scope.Markers.length; i += 1) {
                if ($scope.Markers[i].ticked && ($scope.Markers[i].SRD_SH_CODE == "" || $scope.Markers[i].SRD_SH_CODE == null)) {
                    $scope.selectedSpaces = [];
                    showNotification('error', 8, 'bottom-right', 'Please select shift');
                    flag = true;
                    break;
                }
                else if ($scope.Markers[i].ticked && $scope.Markers[i].SRD_AUR_ID == undefined && $scope.AllocationType.SYSP_VAL1 == "1040") {
                    $scope.selectedSpaces = [];
                    showNotification('error', 8, 'bottom-right', 'Please select employee');
                    flag = true;
                    break;
                }
                else if ($scope.Markers[i].ticked) {
                    $scope.selspcObj = $scope.Markers[i];
                    $scope.selectedSeats.push($scope.selspcObj);

                }
            };
        }
        else if ($scope.AllocationType.SYSP_VAL1 == "1041") {
            angular.forEach($scope.gridCountData, function (o) {
                if (o.SRC_REQ_SEL_TYPE == null || o.SRC_REQ_SEL_TYPE == "" || o.SRC_REQ_TYPE == null || o.SRC_REQ_TYPE == "" || o.SRC_REQ_CNT == 0) {
                    if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                        showNotification('error', 8, 'bottom-right', 'Please select Grade and RequestType');
                        flag = true;
                    }
                    else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                        showNotification('error', 8, 'bottom-right', 'Please select Space and Space Sub Type');
                        flag = true;
                    }
                    if (o.SRC_REQ_CNT == 0 && ($scope.ReqCountType.SYSP_VAL1 == "1037" || $scope.ReqCountType.SYSP_VAL1 == "1038")) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Count Must be Greater than Zero');
                        flag = true;
                    }
                }

            });
        }
        if (flag == false) {
            if (($scope.selectedSeats.length != 0 && ($scope.AllocationType.SYSP_VAL1 == "1039") || $scope.AllocationType.SYSP_VAL1 == "1040") ||
                $scope.AllocationType.SYSP_VAL1 == "1041") {
                //var ReqObj = { ALLOCSTA: $scope.RetStatus, spcreqcount: $scope.SpaceReqCount, spcreqdet: $scope.tickedSpaces, spcreq: $scope.currentblkReq };

                var ReqObj = { ALLOCSTA: $scope.RetStatus, spcreqcount: $scope.SpaceReqCount, spcreqdet: $scope.tickedSpaces, spcreq: $scope.currentblkReq, space_rel_details: $scope.tickedSpaces1, SRN_IT_REM: $scope.currentblkReq.SRN_IT_REM };
                //console.log(ReqObj);
                L2ApprovalService.ApproveRequisitions(ReqObj).then(function (response) {
                    if (response != null) {
                        progress(0, '', false);
                        showNotification('success', 8, 'bottom-right', response.Message);
                        $scope.UpdateapprOptions.api.refreshView();
                        $scope.gridCountOptions.api.refreshView();
                        gridloader();
                        $scope.back();
                        showNotification('success', 8, 'bottom-right', response.Message);
                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', response.Message);
                    }
                }, function (response) {
                    progress(0, '', false);
                });
            }
            else {
                if ($scope.AllocationType.SYSP_VAL1 == "1039" || $scope.AllocationType.SYSP_VAL1 == "1040") {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'Please select atleast one space ID to Approve Or Reject');
                }
            }
        }
    }
    $scope.ApproveAllReq = function () {
        progress(0, 'Loading...', true);
        var ReqObj = { ALLOCSTA: $scope.RetStatus, spcreqList: $scope.sendCheckedValsObj, SRN_L2_REM: $scope.SRN_L2_REM, SRN_IT_REM: $scope.SRN_IT_REM, SRN_SYS_PRF_CODE: $scope.AllocationType.SYSP_VAL1 };
        L2ApprovalService.ApproveAllReq(ReqObj).then(function (response) {
            if (response.data != null) {
                $scope.back();
                progress(0, '', false);
                L2ApprovalService.GetPendingList().then(function (data) {
                    if (data.data != null) {
                        $scope.gridata = data.data;
                        $scope.L2approvalOptions.api.setRowData([]);
                        $scope.L2approvalOptions.api.setRowData($scope.gridata);
                        progress(0, '', false);
                    }
                    else {
                        $scope.L2approvalOptions.api.setRowData([]);
                    }
                }, function (response) {
                });
                showNotification('success', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }

        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.GetDesignation = function (Aur, old) {
        Aur.AUR_DES_NAME = "";
        Aur.ALLOC_SPC_ID = "";
        Aur.AUR_KNOWN_AS = "";
        for (var i = 0; $scope.EmpDetails != null && i < $scope.EmpDetails.length; i += 1) {
            if ($scope.EmpDetails[i].AUR_ID === Aur.SRD_AUR_ID) {
                Aur.AUR_DES_NAME = $scope.EmpDetails[i].AUR_DES_NAME;
                Aur.ALLOC_SPC_ID = $scope.EmpDetails[i].ALLOC_SPC_ID;
                Aur.AUR_KNOWN_AS = $scope.EmpDetails[i].AUR_KNOWN_AS;
                $scope.EmpDetails[i].ticked = true;
            }
            if ($scope.EmpDetails[i].AUR_ID === old) {
                $scope.EmpDetails[i].ticked = false;
            }
        }
    }
    $scope.getSubTypes = function (SpaceType) {
        if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
            $scope.SubType = [];
            SpaceRequisitionService.getSubTypes(SpaceType.SRC_REQ_SEL_TYPE).then(function (response) {
                $scope.SubType = response.data;
            }, function (error) {
                console.log(error);
            });
        }
    }
    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);

        cb.addEventListener('change', function (e) {

            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.UpdateapprOptions.rowData, function (value, key) {
                        value.ticked = true;
                        $scope.tempspace = value;
                        $scope.tempspace.STACHECK = UtilityService.Added;
                        $scope.selectedSpaces.push($scope.tempspace);
                        $scope.tempspace = {};
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.UpdateapprOptions.rowData, function (value, key) {
                        value.ticked = false;
                        _.remove($scope.selectedSpaces, _.find($scope.selectedSpaces, { SRD_REQ_ID: value.SRD_REQ_ID }));
                        value.STACHECK = UtilityService.Deleted;
                        $scope.selectedSpaces.push(value);
                    });
                });
            }
        });
        return eHeader;
    }

    function headerPendingCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);

        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.L2approvalOptions.rowData, function (value, key) {
                        value.ticked = true;
                        $scope.tempspace = value;
                        $scope.tempspace.STACHECK = UtilityService.Added;
                        $scope.selectedSpaces.push($scope.tempspace);
                        $scope.tempspace = {};
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.L2approvalOptions.rowData, function (value, key) {
                        value.ticked = false;
                        _.remove($scope.selectedSpaces, _.find($scope.selectedSpaces, { SRD_REQ_ID: value.SRD_REQ_ID }));
                        value.STACHECK = UtilityService.Deleted;
                        $scope.selectedSpaces.push(value);
                    });
                });
            }
        });
        return eHeader;
    }


    $scope.back = function () {
        $scope.Viewstatus = 0;
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
        $scope.Getcountry = [];
        $scope.SelectAll = [];
        $scope.Shifts = [];


    }

    ///// For map layout
    $scope.ViewinMap = function () {
        if ($scope.SelRowData.length == 0) {
            $scope.MapFloors = [];
            $scope.Map.Floor = [];
            angular.forEach($scope.L2Approve.selectedFloors, function (Value, Key) {
                Value.ticked = false;
                $scope.MapFloors.push(Value);
            });
            $scope.MapFloors[0].ticked = true;
            $scope.Map.Floor.push($scope.MapFloors[0]);
        }
        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {
        progress(0, 'Loading...', true);

        if ($scope.SelRowData.length == 0) {
            $scope.loadmap();
        }

        else {
            progress(0, 'Loading...', false);
        }

    });
    $scope.FlrSectMap = function (data) {
        progress(0, 'Loading...', true);
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();
        progress(0, 'Loading...', false);
    }
    $scope.loadmap = function () {
        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post(UtilityService.path + '/api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
        });

    }
    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
            // success
            // results: an array of data objects from each deferred.resolve(data) call
            function (results) {
                var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
                map.fitBounds(bounds);
                $scope.SelRowData = $filter('filter')($scope.Markers, { SSA_FLR_CODE: $scope.Map.Floor[0].FLR_CODE });
                angular.forEach($scope.SelRowData, function (value, key) {
                    $scope.marker = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: value.SRD_SPC_ID, spacetype: 'CHA' } });
                    $scope.marker.SRD_REQ_ID = value.SRD_REQ_ID;
                    $scope.marker.SRD_SRNREQ_ID = value.SRD_SRNREQ_ID;
                    $scope.marker.SRD_SSA_SRNREQ_ID = value.SRD_SSA_SRNREQ_ID;
                    $scope.marker.SRD_SPC_ID = value.SRD_SPC_ID;
                    $scope.marker.SRD_SPC_NAME = value.SRD_SPC_NAME;
                    $scope.marker.layer = value.SRD_SPC_TYPE;
                    $scope.marker.SRD_SPC_TYPE_NAME = value.SRD_SPC_TYPE_NAME;
                    $scope.marker.SRD_SPC_SUB_TYPE = value.SRD_SPC_SUB_TYPE;
                    $scope.marker.SRD_SPC_SUB_TYPE_NAME = value.SRD_SPC_SUB_TYPE_NAME;
                    $scope.marker.SRD_SH_CODE = value.SRD_SH_CODE;
                    $scope.marker.SRD_AUR_ID = value.SRD_AUR_ID;
                    $scope.marker.SSA_FLR_CODE = value.SSA_FLR_CODE;
                    $scope.marker.STACHECK = value.STACHECK;
                    $scope.marker.ticked = value.ticked;
                    if (value.ticked)
                        $scope.marker.setStyle(selctdChrStyle);
                    else
                        $scope.marker.setStyle(VacantStyle);
                    $scope.marker.bindLabel(value.SRD_SPC_NAME);
                    $scope.marker.on('click', markerclicked);
                    $scope.marker.addTo(map);
                });
                progress(0, '', false);
            },
            // error
            function (response) {
            }
        );
    };

    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };

    function GetMarkers(data) {

        jQuery.each(data, function (index, value) {

            $scope.marker = {};
            $scope.marker.SRD_REQ_ID = value.SRD_REQ_ID;
            $scope.marker.SRD_SRNREQ_ID = value.SRD_SRNREQ_ID;
            $scope.marker.SRD_SSA_SRNREQ_ID = value.SRD_SSA_SRNREQ_ID;
            $scope.marker.SRD_SPC_ID = value.SRD_SPC_ID;
            $scope.marker.SRD_SPC_NAME = value.SRD_SPC_NAME;
            $scope.marker.layer = value.SRD_SPC_TYPE;
            $scope.marker.SRD_SPC_TYPE_NAME = value.SRD_SPC_TYPE_NAME;
            $scope.marker.SRD_SPC_SUB_TYPE = value.SRD_SPC_SUB_TYPE;
            $scope.marker.SRD_SPC_SUB_TYPE_NAME = value.SRD_SPC_SUB_TYPE_NAME;
            $scope.marker.SRD_SH_CODE = value.SRD_SH_CODE;
            $scope.marker.SRD_AUR_ID = value.SRD_AUR_ID;
            $scope.marker.SSA_FLR_CODE = value.SSA_FLR_CODE;
            $scope.marker.STACHECK = value.STACHECK;
            $scope.marker.ALLOC_SPC_ID = value.ALLOC_SPC_ID;
            $scope.marker.ticked = value.ticked;
            $scope.Markers.push($scope.marker);

        });
    };

    function markerclicked(e) {
        var marker = _.find($scope.Markers, { SRD_SPC_ID: this.SRD_SPC_ID });
        if (!this.ticked) {
            this.setStyle(selctdChrStyle)
            this.ticked = true;
            marker.ticked = true;
        }
        else {
            this.setStyle(VacantStyle)
            this.ticked = false;
            marker.ticked = false;
        }
        $scope.chkChanged(this);
        $scope.UpdateapprOptions.api.refreshView();
    }
}]);
