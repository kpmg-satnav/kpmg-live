﻿app.service("SpaceRequisitionService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    this.getSpaces = function (searchObj) {

        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/GetSpaces', searchObj)
         .then(function (response) {
             //console.log(response.data);
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getReqCountDetails = function (FlrObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/GetReqCountDetails', FlrObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getShifts = function (selLocs) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/GetShifts', selLocs)
         .then(function (response) {
             //console.log(response.data);
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getEmpDetails = function (selCostCenters) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/GetEmpDetails', selCostCenters)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getSubTypes = function (SpaceType) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/GetSubTypes?SpaceType=' + SpaceType + '')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.checkExists = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/CheckExists', data)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.CheckSpace = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/CheckSpace', data)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.raiseRequest = function (ReqObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/RaiseRequest', ReqObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.RaiseCountRequest = function (ReqObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/RaiseCountRequest', ReqObj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getEmpTypes = function (selCostCenters) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRequisition/GetEmpTypes', selCostCenters)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getEmpSubTypes = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceRequisition/getEmpSubTypes')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getSpaceTypes = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceRequisition/GetSpaceTypes')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getSpaceSubTypes = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceRequisition/GetSpaceSubTypes')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

}]);

app.controller('SpaceRequisitionController', ['$scope', '$q', 'SpaceRequisitionService', 'UtilityService','$filter', function ($scope, $q, SpaceRequisitionService, UtilityService, $filter) {

    $scope.SpaceRequisition = {};
    $scope.SpcReqSearch = {};

    $scope.Country = [];
    $scope.City = [];
    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Floors = [];
    $scope.Verticals = [];
    $scope.CostCenters = [];
    $scope.Shifts = [];
    $scope.EmpDetails = [];
    $scope.tempspace = {};
    $scope.selectedSpaces = [];
    $scope.selectedSpacesCount = [];
    $scope.gridata = [];
    $scope.gridCountData = [];

    $scope.SysPreference = [];
    $scope.AllocationType = {};
    $scope.ReqCountType = {};
    $scope.Type = [];
    $scope.SubType = [];

    $scope.SelRowData = [];
    $scope.MapFloors = [];
    $scope.Markers = [];
    $scope.SelLayers = [];
    $scope.ShiftFilter = [];

    var map = L.map('leafletMap');

    $scope.Clear = function () {

        angular.forEach($scope.Country, function (country) {
            country.ticked = false;
        });
        angular.forEach($scope.City, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Locations, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Towers, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floors, function (floor) {
            floor.ticked = false;
        });
        angular.forEach($scope.Verticals, function (vertical) {
            vertical.ticked = false;
        });
        angular.forEach($scope.CostCenters, function (costCenter) {
            costCenter.ticked = false;
        });
        angular.forEach($scope.ShiftFilter, function (shift) {
            shift.ticked = false;
        });
        $scope.SpcReqSearch = {};
        $scope.selectedSpaces = [];
        $scope.Markers = [];
        $scope.gridOptions.rowData = [];
        $scope.gridCountOptions.rowData = [];
        $scope.gridata = [];
        $scope.gridCountData = [];
        $scope.frmSpaceRequisition.$submitted = false;
    }

    $scope.LoadDetails = function () {
        progress(0, 'Loading...', true);
        UtilityService.getSysPreferences().then(function (response) {
            if (response.data != null) {
                $scope.SysPreference = response.data;
                $scope.AllocationType = _.find($scope.SysPreference, { SYSP_CODE: "Allocation Type" })
                $scope.ReqCountType = _.find($scope.SysPreference, { SYSP_CODE: "Requisition Count Type" })
            }
        });

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;

                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;


                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;

                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                progress(0, '', false);
                                            }
                                        });

                                    }
                                });
                            }
                        });

                    }
                });
            }
        });


        UtilityService.getVerticals(2).then(function (response) {
            if (response.data != null) {
                $scope.Verticals = response.data;
            }
        });

        UtilityService.getCostCenters(2).then(function (response) {
            if (response.data != null) {
                $scope.CostCenters = response.data;
            }
        });
    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.SpaceRequisition.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.SpaceRequisition.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.SpaceRequisition.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceRequisition.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.SpaceRequisition.City = $scope.City;
        $scope.getLocationsByCity();
    }
    //$scope.getShifts = function () {

    //}


    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.SpaceRequisition.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceRequisition.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SpaceRequisition.City[0] = cty;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.SpaceRequisition.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.SpaceRequisition.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny !== undefined && value.ticked === true) {
                cny.ticked = true;
                $scope.SpaceRequisition.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SpaceRequisition.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SpaceRequisition.Locations[0] = lcm;
            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.SpaceRequisition.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.getCostcenterByVertical = function () {
        UtilityService.getCostcenterByVertical($scope.SpaceRequisition.Verticals, 2).then(function (response) {
            if (response.data != null) {
                $scope.CostCenters = response.data;
            }
            else { $scope.CostCenters = [] }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.verticalChangeAll = function () {
        $scope.SpaceRequisition.Verticals = $scope.Verticals;
        $scope.getCostcenterByVertical();
    }

    $scope.FloorChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceRequisition.Country[0] = cny;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SpaceRequisition.City[0] = cty;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SpaceRequisition.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SpaceRequisition.Towers[0] = twr;
            }
        });

        SpaceRequisitionService.getShifts($scope.SpaceRequisition.Locations).then(function (response) {
            if (response.data != null) {
                $scope.ShiftFilter = [];
                $scope.ShiftFilter = response.data;
            }
        }, function (error) {
            console.log(error);
        });

    }
    $scope.floorChangeAll = function () {
        $scope.SpaceRequisition.Floors = $scope.Floors;
        $scope.FloorChange();
    }

    $scope.CostCenterChagned = function () {
        angular.forEach($scope.Verticals, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.CostCenters, function (value, key) {
            var ver = _.find($scope.Verticals, { VER_CODE: value.Vertical_Code });
            if (ver != undefined && value.ticked == true) {
                ver.ticked = true;
                $scope.SpaceRequisition.Verticals[0] = ver;
            }
        });
    }
    $scope.getSpaces = function () {
        progress(0, 'Loading...', true);

        angular.forEach($scope.ShiftFilter, function (value, key) {
            if (value.ticked == true) {
                $scope.Shifts = [];
                $scope.Shifts.push(value);
            }
        });

        if ($scope.ShiftFilter.length != 0 && $scope.Shifts.length == 0) {
            $scope.Shifts = $scope.ShiftFilter;
        }
        $scope.SpcReqSearch.SRN_FROM_DATE = $scope.SpcReqSearch.SRN_FROM_DATE;
        var SpaceData = { cstlst: $scope.SpaceRequisition.CostCenters, spcreq: $scope.SpcReqSearch };

        SpaceRequisitionService.getEmpDetails(SpaceData).then(function (response) {
            $scope.EmpDetails = response.data;
        }, function (error) {
            console.log(error);
        });
        SpaceRequisitionService.getShifts($scope.SpaceRequisition.Locations).then(function (response) {
            if (response.data != null) {
                $scope.Shifts = [];
                $scope.Shifts = response.data;
                console.log($scope.Shifts);
            }
        }, function (error) {
            console.log(error);
        });		

        var Shift = '';
        if ($scope.SpaceRequisition.ShiftFilter.length > 0)
            Shift = $scope.SpaceRequisition.ShiftFilter[0].SH_CODE;
        var searchObj = {
            flrlst: $scope.SpaceRequisition.Floors, verlst: $scope.SpaceRequisition.Verticals,
            cstlst: $scope.SpaceRequisition.CostCenters, spcreq: $scope.SpcReqSearch, ShiftFilter: Shift
        };
        $scope.Markers = [];
        if ($scope.AllocationType.SYSP_VAL1 == "1039") {
            if (moment($scope.SpcReqSearch.SRN_FROM_DATE) > moment($scope.SpcReqSearch.SRN_TO_DATE)) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            } else {
                SpaceRequisitionService.getSpaces(searchObj).then(function (response) {
                    if (response.data != null) {
                        $scope.Markers = [];
                        GetMarkers(response.data);
                        $scope.gridOptions.api.setRowData($scope.Markers);
                        progress(0, '', false);
                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'No data available for selected ' + $scope.BsmDet.Parent);

                    }
                });
            }
        }
            // 1040=Request Raised for Employee
        else if ($scope.AllocationType.SYSP_VAL1 == "1040") {
            if (moment($scope.SpcReqSearch.SRN_FROM_DATE) > moment($scope.SpcReqSearch.SRN_TO_DATE)) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            } else {
                SpaceRequisitionService.getSpaces(searchObj).then(function (response) {
                    if (response.data != null) {
                        $scope.Markers = [];
                        GetMarkers(response.data);
                        $scope.gridOptions.api.setRowData($scope.Markers);
                        $scope.gridOptions.columnApi.setColumnVisible('SRD_AUR_ID', true);
                        $scope.gridOptions.columnApi.setColumnVisible('AUR_DES_NAME', true);
                        $scope.gridOptions.columnApi.setColumnVisible('ALLOC_SPC_ID', true);
                        $scope.gridOptions.columnApi.setColumnVisible('AUR_KNOWN_AS', true);
                        //$scope.gridOptions.columnApi.setColumnVisible('AUR_KNOWN_AS_NAME', true);
                        progress(0, '', false);
                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'No data available for selected ' + $scope.BsmDet.Parent);
                    }
                });
            }
        }
            // 1041=Request Raised for Future
        else if ($scope.AllocationType.SYSP_VAL1 == "1041") {
            if (moment($scope.SpcReqSearch.SRN_FROM_DATE) > moment($scope.SpcReqSearch.SRN_TO_DATE)) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            } else {
                $scope.gridCountOptions.rowData = [];
                var flrObj = { flrlst: $scope.SpaceRequisition.Floors, REQ_CNT_STA_FUT: $scope.ReqCountType.SYSP_VAL1 };
                SpaceRequisitionService.getReqCountDetails(flrObj).then(function (response) {
                    if (response.data != null) {
                        $scope.gridCountData = response.data;
                        if (window.innerWidth <= 480) {
                            $scope.gridCountOptions.api.setColumnDefs(columnDefsCount);
                            $scope.gridCountOptions.api.setRowData($scope.gridCountData);
                        }
                        else {
                            $scope.gridCountOptions.api.setRowData($scope.gridCountData);
                        }
                        //$scope.gridCountOptions.api.setRowData($scope.gridCountData);
                        if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Employee Count";
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_SEL_TYPE").colDef.headerName = "Grade Type";
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_TYPE").colDef.headerName = "Request Type";
                            $scope.gridCountOptions.api.refreshHeader();
                            $scope.Type = [];
                            SpaceRequisitionService.getEmpTypes($scope.SpaceRequisition.CostCenters).then(function (response) {
                                if (response.data != null) {
                                    $scope.Type = response.data;
                                }
                            });
                            SpaceRequisitionService.getEmpSubTypes().then(function (response) {
                                console.log(response)
                                if (response.data != null) {
                                    $scope.SubType = response.data;
                                }
                            });
                            progress(0, '', false);
                        }
                        else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Space Count";
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_SEL_TYPE").colDef.headerName = "Space Type";
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_TYPE").colDef.headerName = "Space Sub Type";
                            $scope.gridCountOptions.api.refreshHeader();
                            $scope.Type = [];
                            $scope.SubType = [];
                            SpaceRequisitionService.getSpaceTypes().then(function (response) {
                                if (response.data != null) {
                                    $scope.Type = response.data;
                                }
                            });
                            SpaceRequisitionService.getSpaceSubTypes().then(function (response) {
                                if (response.data != null) {
                                    $scope.SubType = response.data;
                                }
                            });
                            progress(0, '', false);
                        }
                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'No data available for selected ' + $scope.BsmDet.Parent);
                    }
                });
            }
        }
    }

    $scope.RaiseRequest = function () {
        $scope.selectedSpaces = [];
        var flag = false;
        for (var i = 0; $scope.Markers != null && i < $scope.Markers.length; i += 1) {
            $scope.selspcObj = {};

            if ($scope.Markers[i].ticked && ($scope.Markers[i].SRD_SH_CODE == undefined || $scope.Markers[i].SRD_SH_CODE == '')) {
                $scope.selectedSpaces = [];
                showNotification('error', 8, 'bottom-right', 'Please select shift');
                flag = true;
                break;
            }
            else if ($scope.Markers[i].ticked && $scope.Markers[i].SRD_AUR_ID == undefined && $scope.AllocationType.SYSP_VAL1 == "1040") {
                $scope.selectedSpaces = [];
                showNotification('error', 8, 'bottom-right', 'Please select employee');
                flag = true;
                break;
            }
            else if (moment($scope.SpcReqSearch.SRN_FROM_DATE) > moment($scope.SpcReqSearch.SRN_TO_DATE)) {
                progress(0, '', false);
                flag = true;
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            }
            else if ($scope.Markers[i].ticked) {
                $scope.selspcObj.SRD_SSA_SRNREQ_ID = $scope.Markers[i].SRD_SSA_SRNREQ_ID;
                $scope.selspcObj.SRD_LCM_CODE = $scope.Markers[i].SRD_LCM_CODE;
                $scope.selspcObj.SRD_SPC_ID = $scope.Markers[i].SRD_SPC_ID;
                $scope.selspcObj.SRD_SPC_TYPE = $scope.Markers[i].layer;
                $scope.selspcObj.SRD_SPC_SUB_TYPE = $scope.Markers[i].SRD_SPC_SUB_TYPE;
                $scope.selspcObj.SRD_SH_CODE = $scope.Markers[i].SRD_SH_CODE;
                $scope.selspcObj.SRD_AUR_ID = $scope.Markers[i].SRD_AUR_ID;
                $scope.selspcObj.SSA_FLR_CODE = $scope.Markers[i].SSA_FLR_CODE;
                $scope.selspcObj.SRN_STA_ID = $scope.Markers[i].SRN_STA_ID;
                $scope.selspcObj.STACHECK = $scope.Markers[i].STACHECK;
                $scope.selspcObj.ticked = $scope.Markers[i].ticked;
                $scope.selectedSpaces.push($scope.selspcObj);
            }
        };
        $scope.SpcReqSearch.STACHECK = 4;
        //1039 = Request Raised  for Department
        if ($scope.AllocationType.SYSP_VAL1 == "1039") {
            $scope.SpcReqSearch.SRN_STA_ID = 1010;
            $scope.SpcReqSearch.SRN_SYS_PRF_CODE = "1039";
        }
            //1040 = Request Raised for Employee
        else if ($scope.AllocationType.SYSP_VAL1 == "1040") {
            $scope.SpcReqSearch.SRN_STA_ID = 1017;
            $scope.SpcReqSearch.SRN_SYS_PRF_CODE = "1040";
        }
        if (flag == false) {
            setTimeout(function () {
                if ($scope.selectedSpaces.length != 0) {

                    progress(0, 'Loading...', true);
                    var ReqObj = { flrlst: $scope.SpaceRequisition.Floors, cstlst: $scope.SpaceRequisition.CostCenters, spcreq: $scope.SpcReqSearch, spcreqdet: $scope.selectedSpaces, ALLOCSTA: 4 };
                    SpaceRequisitionService.raiseRequest(ReqObj).then(function (response) {
                        if (response.data != null) {
                            $scope.Clear();
                            progress(0, '', false);
                            showNotification('success', 8, 'bottom-right', response.Message);
                        }
                        else {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', response.Message);
                        }
                    }, function (response) {
                        progress(0, '', false);
                    });
                }
                else
                    showNotification('error', 8, 'bottom-right', 'Please select atleast one space ID to allocate');
            }, 500);
        }

    }

    $scope.RaiseCountRequest = function () {

        if (moment($scope.SpcReqSearch.SRN_FROM_DATE) > moment($scope.SpcReqSearch.SRN_TO_DATE)) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        } else {
            $scope.SpcReqSearch.STACHECK = 4;
            $scope.selectedSpaces = [];
            var valida = 0;
            angular.forEach($scope.gridCountData, function (o) {
                if (o.SRC_REQ_SEL_TYPE != null && o.SRC_REQ_SEL_TYPE != "" && o.SRC_REQ_TYPE != null && o.SRC_REQ_TYPE != "")
                    o.SRD_SH_CODE = $scope.ShiftFilter[0].SH_CODE;
                    $scope.selectedSpaces.push(o);
                if (o.SRC_REQ_CNT == 0) {
                    valida++;
                }
            });
            //1038 = Employee Count
            if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                $scope.SpcReqSearch.SRN_STA_ID = 1042;
                $scope.SpcReqSearch.SRN_SYS_PRF_CODE = "1041";
            }
                //1037 = Space Count
            else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                $scope.SpcReqSearch.SRN_STA_ID = 1042;
                $scope.SpcReqSearch.SRN_SYS_PRF_CODE = "1041";
            }

            setTimeout(function () {
                if (valida != 0) {
                    showNotification('error', 8, 'bottom-right', 'Please Enter Employee Count');
                }
                else {
                    if ($scope.selectedSpaces.length != 0) {
                        progress(0, 'Loading...', true);
                        var ReqObj = { flrlst: $scope.SpaceRequisition.Floors, cstlst: $scope.SpaceRequisition.CostCenters, spcreq: $scope.SpcReqSearch, spcreqcount: $scope.selectedSpaces, ALLOCSTA: 4, };
                        SpaceRequisitionService.RaiseCountRequest(ReqObj).then(function (response) {
                            if (response.data != null) {
                                $scope.Clear();
                                progress(0, '', false);
                                showNotification('success', 8, 'bottom-right', response.Message);
                            }
                            else {
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', response.Message);
                            }
                        }, function (response) {
                            progress(0, '', false);
                        });
                    }

                    else if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                        showNotification('error', 8, 'bottom-right', 'Please select Grade and RequestType');
                    }

                    else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                        showNotification('error', 8, 'bottom-right', 'Please select Space and Space Sub Type');
                    }
                }
            }, 500);

        }
    }

    //For Grid column Headers
    var columnDefs = [
        { headerName: "Select All", field: "ticked", width: 90, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)'>", headerCellRenderer: headerCellRendererFunc, suppressMenu: true, pinned: 'left' },
        { headerName: "Space ID", field: "SRD_SPC_NAME", width: 180, cellClass: "grid-align", pinned: 'left', suppressMenu: true, },
        { headerName: "Space Type", field: "SRD_SPC_TYPE_NAME", width: 150, cellClass: "grid-align" },
        { headerName: "Space Sub Type", field: "SRD_SPC_SUB_TYPE_NAME", width: 150, cellClass: "grid-align" },
        { headerName: "Floor", field: "SSA_FLR_CODE", width: 180, cellClass: "grid-align" },
        { headerName: "Shift Type", field: "SRD_SH_CODE", suppressMenu: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<select  data-ng-change='CheckSpace(data)' ng-model='data.SRD_SH_CODE'><option value=''>--Select--</option><option ng-repeat='Shift in Shifts | filter:data.SRD_LCM_CODE' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>" },
        {
            headerName: "Employee ID", field: "SRD_AUR_ID", suppressMenu: true, hide: true, width: 180, cellClass: "grid-align",
            filter: 'set',
            template: "<select  ng-model='data.SRD_AUR_ID' data-ng-change='GetDesignation(data,\"{{data.SRD_AUR_ID}}\")'><option value=''>--Select--</option><option data-ng-disabled='Emp.ticked' ng-repeat='Emp in EmpDetails' value='{{Emp.AUR_ID}}'>{{Emp.AUR_ID}}</option></select>"
        },
        { headerName: "Employee Name", field: "AUR_KNOWN_AS", suppressMenu: true, hide: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<label> {{data.AUR_KNOWN_AS}}</label>" },
        //{ headerName: "Employee", field: "SRD_AUR_ID", suppressMenu: true, hide: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<select  ng-model='data.SRD_AUR_ID' data-ng-change='GetDesignation(data,\"{{data.SRD_AUR_ID}}\")'><option value=''>--Select--</option><option data-ng-disabled='Emp.ticked' ng-repeat='Emp in EmpDetails' value='{{Emp.AUR_ID}}'>{{Emp.AUR_KNOWN_AS}}</option></select>" },
        { headerName: "Existing Space Id", field: "ALLOC_SPC_ID", suppressMenu: true, hide: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<label> {{data.ALLOC_SPC_ID}}</label>" },
        { headerName: "Costcenter", field: "AUR_DES_NAME", suppressMenu: true, hide: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<label> {{data.AUR_DES_NAME}}</label>" },
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };




    $scope.pageSize = '10';

    // For Grid column Headers
    var columnDefsCount = [
        { headerName: "Country", field: "SRC_CNY_NAME", cellClass: "grid-align" },
        { headerName: "City", field: "SRC_CTY_NAME", cellClass: "grid-align" },
        { headerName: "Location", field: "SRC_LCM_NAME", cellClass: "grid-align" },
        { headerName: "Tower", field: "SRC_TWR_NAME", cellClass: "grid-align" },
        { headerName: "Floor", field: "SRC_FLR_NAME", cellClass: "grid-align" },
        { headerName: "", field: "SRC_REQ_CNT", cellClass: "grid-align", filter: 'set', template: "<input type='textbox' ng-model='data.SRC_REQ_CNT'>", suppressMenu: true, },
        {
            headerName: "", field: "SRC_REQ_SEL_TYPE", cellClass: "grid-align", filter: 'set', suppressMenu: true,
            template: "<select  ng-model='data.SRC_REQ_SEL_TYPE' data-ng-change='getSubTypes(data)'><option value=''>--Select--</option><option ng-repeat='Ty in Type' value='{{Ty.CODE}}'>{{Ty.NAME}}</option></select>"
        },
        {
            headerName: "", field: "SRC_REQ_TYPE", cellClass: "grid-align", filter: 'set', suppressMenu: true,
            template: "<select  ng-init='' ng-model='data.SRC_REQ_TYPE'><option value=''>--Select--</option><option ng-repeat='SubTy in SubType' value='{{SubTy.CODE}}'>{{SubTy.NAME}}</option></select>"
        },
    ];

    $scope.gridCountOptions = {
        columnDefs: columnDefsCount,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.gridCountOptions.api.sizeColumnsToFit()
        }
    };

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        //var cb1 = document.createElement('input');
        //var br = document.createElement('br');
        cb.setAttribute('type', 'checkbox');
        //cb1.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        //var eHeader = document.createElement('label');
        //var eTitle = document.createTextNode('This page');
        //var eTitle1 = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);
        //eHeader.appendChild(br);
        //eHeader.appendChild(cb1);
        //eHeader.appendChild(eTitle1);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = true;
                    });
                    if ($scope.drawnItems) {
                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SVD_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.ticked = true;
                                value.setStyle(selctdChrStyle);
                            }
                        });
                    }
                });

            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = false;
                    });
                    if ($scope.drawnItems) {
                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SVD_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.ticked = false;
                                value.setStyle(VacantStyle);
                            }
                        });
                    }
                });

            }
        });


        return eHeader;
    }

    $scope.chkChanged = function (selctedRow) {
        if ($scope.drawnItems) {
            $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: selctedRow.SRD_SPC_ID, spacetype: 'CHA' } });
            if (selctedRow.ticked) {
                $scope.chkr.setStyle(selctdChrStyle);
                $scope.chkr.ticked = true;
            }
            else {
                $scope.chkr.setStyle(VacantStyle);
                $scope.chkr.ticked = false;
            }
        }
    }

    $scope.CheckSpace = function (Data) {
        $scope.selspcObj = {};
        $scope.spcreqdet = [];

        $scope.selspcObj.SRD_SH_CODE = Data.SRD_SH_CODE;
        $scope.selspcObj.SRD_SPC_ID = Data.SRD_SPC_ID;

        $scope.spcreqdet.push($scope.selspcObj);
        var SpaceData = { spcreq: $scope.SpcReqSearch, spcreqdet: $scope.spcreqdet };

        SpaceRequisitionService.CheckSpace(SpaceData).then(function (response) {
            if (response.data == 2) {
                Data.ticked = false;
                $scope.chkChanged(Data);
                Data.SRD_SH_CODE = '';
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else if (response.data == 0) {
                Data.ticked = false;
                $scope.chkChanged(Data);
                Data.SRD_SH_CODE = '';
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else {
                $scope.chkChanged(Data);
                Data.ticked = true;
            }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.GetDesignation = function (Aur, old) {
        //console.log(old);
        Aur.AUR_DES_NAME = "";
        Aur.ALLOC_SPC_ID = "";
        Aur.AUR_KNOWN_AS_NAME = "";
        Aur.AUR_KNOWN_AS = "";
        for (var i = 0; $scope.EmpDetails != null && i < $scope.EmpDetails.length; i += 1) {
            if ($scope.EmpDetails[i].AUR_ID === Aur.SRD_AUR_ID) {
                //SpaceRequisitionService.checkExists($scope.EmpDetails[i]).then(function (response) {
                //    $scope.SubType = response.data;
                //    //data.SRC_REQ_TYPE=
                //}, function (error) {
                //    console.log(error);
                //});
                Aur.AUR_DES_NAME = $scope.EmpDetails[i].AUR_DES_NAME;
                Aur.ALLOC_SPC_ID = $scope.EmpDetails[i].ALLOC_SPC_ID;
               // Aur.AUR_KNOWN_AS_NAME = $scope.EmpDetails[i].AUR_KNOWN_AS_NAME;
                Aur.AUR_KNOWN_AS = $scope.EmpDetails[i].AUR_KNOWN_AS;
                $scope.EmpDetails[i].ticked = true;
                //console.log($scope.EmpDetails);
            }
            if ($scope.EmpDetails[i].AUR_ID === old) {
                $scope.EmpDetails[i].ticked = false;
            }
        }
    }

    $scope.getSubTypes = function (SpaceType) {
        if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
            $scope.SubType = [];
            SpaceRequisitionService.getSubTypes(SpaceType.SRC_REQ_SEL_TYPE).then(function (response) {
                $scope.SubType = response.data;
                //data.SRC_REQ_TYPE=
            }, function (error) {
                console.log(error);
            });
        }
    }

    ///// For map layout
    $scope.ViewinMap = function () {
        if ($scope.SelRowData.length == 0) {
            $scope.MapFloors = [];
            $scope.Map.Floor = [];
            angular.forEach($scope.SpaceRequisition.Floors, function (Value, Key) {
                Value.ticked = false;
                $scope.MapFloors.push(Value);
            });
            $scope.MapFloors[0].ticked = true;
            $scope.Map.Floor.push($scope.MapFloors[0]);
        }
        $("#historymodal").modal('show');
    }

    $('#historymodal').on('shown.bs.modal', function () {
        progress(0, 'Loading...', true);

        if ($scope.SelRowData.length == 0) {
            $scope.loadmap();
        }
        else {
            progress(0, 'Loading...', false);
        }

    });
    $scope.FlrSectMap = function (data) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();

    }

    $scope.loadmap = function () {
        progress(0, 'Loading...', true);
        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE, key_value: 1 };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post(UtilityService.path + '/api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });

    }

    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
           function (results) {
               var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
               map.fitBounds(bounds);
               $scope.SelRowData = $filter('filter')($scope.Markers, { SSA_FLR_CODE: $scope.Map.Floor[0].FLR_CODE });
               angular.forEach($scope.SelRowData, function (value, key) {

                   $scope.marker = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: value.SRD_SPC_ID, spacetype: 'CHA' } });
                   $scope.marker.SRD_SSA_SRNREQ_ID = value.SRD_SSA_SRNREQ_ID;
                   $scope.marker.SRD_LCM_CODE = value.SRD_LCM_CODE;
                   $scope.marker.SRD_SPC_ID = value.SRD_SPC_ID;
                   $scope.marker.SRD_SPC_NAME = value.SRD_SPC_NAME;
                   $scope.marker.layer = value.SRD_SPC_TYPE;
                   $scope.marker.SRD_SPC_TYPE_NAME = value.SRD_SPC_TYPE_NAME;
                   $scope.marker.SRD_SPC_SUB_TYPE = value.SRD_SPC_SUB_TYPE;
                   $scope.marker.SRD_SPC_SUB_TYPE_NAME = value.SRD_SPC_SUB_TYPE_NAME;
                   $scope.marker.SRD_SH_CODE = value.SRD_SH_CODE;
                   $scope.marker.SRD_AUR_ID = value.SRD_AUR_ID;
                   $scope.marker.SSA_FLR_CODE = value.SSA_FLR_CODE;
                   $scope.marker.STACHECK = value.STACHECK;
                   $scope.marker.ticked = value.ticked;
                   if (value.ticked)
                       $scope.marker.setStyle(selctdChrStyle);
                   else
                       $scope.marker.setStyle(VacantStyle);
                   $scope.marker.bindLabel(value.SRD_SPC_ID);
                   $scope.marker.on('click', markerclicked);
                   $scope.marker.addTo(map);
               });
           },
           function (response) {
           }
        );
    };


    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };

    function GetMarkers(data) {
        jQuery.each(data, function (index, value) {
            $scope.marker = {};
            $scope.marker.SRD_SSA_SRNREQ_ID = value.SRD_SSA_SRNREQ_ID;
            $scope.marker.SRD_LCM_CODE = value.SRD_LCM_CODE;
            $scope.marker.SRD_SPC_ID = value.SRD_SPC_ID;
            $scope.marker.SRD_SPC_NAME = value.SRD_SPC_NAME;
            $scope.marker.layer = value.SRD_SPC_TYPE;
            $scope.marker.SRD_SPC_TYPE_NAME = value.SRD_SPC_TYPE_NAME;
            $scope.marker.SRD_SPC_SUB_TYPE = value.SRD_SPC_SUB_TYPE;
            $scope.marker.SRD_SPC_SUB_TYPE_NAME = value.SRD_SPC_SUB_TYPE_NAME;
            $scope.marker.SRD_SH_CODE = value.SRD_SH_CODE;
            $scope.marker.SRD_AUR_ID = value.SRD_AUR_ID;
            $scope.marker.SSA_FLR_CODE = value.SSA_FLR_CODE;
            $scope.marker.STACHECK = value.STACHECK;
            $scope.marker.ticked = value.ticked;
            $scope.Markers.push($scope.marker);
        });

    };

    function markerclicked(e) {

        var marker = _.find($scope.Markers, { SRD_SPC_ID: this.SRD_SPC_ID });
        if (!this.ticked) {
            this.setStyle(selctdChrStyle)
            this.ticked = true;
            marker.ticked = true;
        }
        else {
            this.setStyle(VacantStyle)
            this.ticked = false;
            marker.ticked = false;
        }
        $scope.gridOptions.api.refreshView();
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterCountChanged(value) {
        $scope.gridCountOptions.api.setQuickFilter(value);
    }

    $("#txtCountFilter").change(function () {
        onFilterCountChanged($(this).val());
    }).keydown(function () {
        onFilterCountChanged($(this).val());
    }).keyup(function () {
        onFilterCountChanged($(this).val());
    }).bind('paste', function () {
        onFilterCountChanged($(this).val());
    })

    setTimeout(function () {
        $scope.LoadDetails();
    }, 200);
}]);