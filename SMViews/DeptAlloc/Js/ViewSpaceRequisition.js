﻿app.service("ViewSpaceRequisitionService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetPendingList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ViewSpaceRequisition/GetPendingSpaceRequisitions')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.UpdateSpaceRequisition = function (ReqObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewSpaceRequisition/UpdateSpaceRequisition', ReqObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('ViewSpaceRequisitionController', ['$scope', '$q', 'ViewSpaceRequisitionService', 'UtilityService', 'L1ApprovalService', 'SpaceRequisitionService', '$filter', function ($scope, $q, ViewSpaceRequisitionService, UtilityService, L1ApprovalService, SpaceRequisitionService, $filter) {

    $scope.Viewstatus = 0;
    $scope.ApprvlPageSize = '20';
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.selectedLocations = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.Verticallist = [];
    $scope.Costcenterlist = [];
    $scope.selectedSeats = [];
    $scope.currentblkReq = {};
    $scope.RetStatus = UtilityService.Added;
    $scope.EnableStatus = 0;
    $scope.ButtonStatus = 1;
    $scope.tempspace = {};
    $scope.selectedSpaces = [];
    $scope.SpaceReqCount = [];
    $scope.tickedSpaces = [];
    $scope.sendCheckedValsObj = [];
    $scope.ViewSpcReq = {};
    $scope.EmpDetails = [];
    $scope.SysPreference = [];
    $scope.Type = [];
    $scope.Shifts = [];


    $scope.MapFloors = [];
    $scope.Markers = [];
    $scope.SelLayers = [];
    var map = L.map('leafletMap');
    $scope.SelRowData = [];

    $scope.ViewSpaceReqDefs = [
        { headerName: "Requisition ID", field: "SRN_REQ_ID", cellClass: "grid-align", suppressMenu: true, filter: 'set', template: '<a ng-click="onRowSelectedFunc(data)">{{data.SRN_REQ_ID}}</a>' },
        { headerName: "Requested By", field: "AUR_KNOWN_AS", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Requested Date ", template: '<span>{{data.SRN_REQ_DT | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true },
        { headerName: "", width: 138, field: "VER_NAME", cellClass: "grid-align", },
        { headerName: "", width: 138, field: "COST_CENTER_NAME", cellClass: "grid-align", },
        { headerName: "From Date", width: 140, template: '<span>{{data.SRN_FROM_DATE | date: "dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },
        { headerName: "To Date", width: 140, template: '<span>{{data.SRN_TO_DATE | date: "dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Status", field: "STA_DESC", cellClass: "grid-align",  }
    ];
    $scope.ViewVerticalReqOptions = {
        columnDefs: $scope.ViewSpaceReqDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple',
        enableColResize: true,
        //onReady: function () {
        //    $scope.ViewVerticalReqOptions.api.sizeColumnsToFit()
        //},

    };
    function onUpdateFilterChanged(value) {
        $scope.ViewVerticalReqOptions.api.setQuickFilter(value);
    }
    $("#ApprvlFilter").change(function () {
        onUpdateFilterChanged($(this).val());
    }).keydown(function () {
        onUpdateFilterChanged($(this).val());
    }).keyup(function () {
        onUpdateFilterChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFilterChanged($(this).val());
    })



    $scope.UpdateapprDefs = [
        { headerName: "Select All", field: "ticked", width: 90, cellClass: 'grid-align', filter: 'set', template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)'>", headerCellRenderer: headerCellRendererFunc, suppressMenu: true, pinned: 'left' },
        { headerName: "Space ID", field: "SRD_SPC_NAME", width: 180, cellClass: "grid-align", pinned: 'left', suppressMenu: true, },
        { headerName: "Space Type", field: "SRD_SPC_TYPE_NAME", width: 150, cellClass: "grid-align" },
        { headerName: "Space Sub Type", field: "SRD_SPC_SUB_TYPE_NAME", width: 120, cellClass: "grid-align" },
        { headerName: "Shift Type", field: "SRD_SH_CODE", width: 150, suppressMenu: true, cellClass: "grid-align", filter: 'set', template: "<select   ng-model='data.SRD_SH_CODE' ng-change='CheckSpace(data)' ><option value=''>--Select--</option><option ng-repeat='Shift in Shifts | filter:data.SRD_LCM_CODE' value='{{Shift.SH_CODE}}'>{{Shift.SH_NAME}}</option></select>" },
        { headerName: "Employee ID", field: "SRD_AUR_ID", hide: true, width: 200, suppressMenu: true, cellClass: "grid-align", filter: 'set', template: "<select   ng-model='data.SRD_AUR_ID' data-ng-change='GetDesignation(data,\"{{data.SRD_AUR_ID}}\")'><option value=''>--Select--</option><option data-ng-disabled='Emp.ticked' ng-repeat='Emp in EmpDetails' value='{{Emp.AUR_ID}}'>{{Emp.AUR_ID}}</option></select>" },
        { headerName: "Employee Name", field: "AUR_KNOWN_AS", suppressMenu: true, hide: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<label> {{data.AUR_KNOWN_AS}}</label>" },
        { headerName: "Existing Space Id", field: "ALLOC_SPC_ID", suppressMenu: true, hide: true, width: 180, cellClass: "grid-align", filter: 'set', template: "<label> {{data.ALLOC_SPC_ID}}</label>" },
        { headerName: "Costcenter", field: "AUR_DES_NAME", hide: true, width: 200, suppressMenu: true, cellClass: "grid-align", filter: 'set', template: "<label> {{data.AUR_DES_NAME}}</label>" },

    ];
    $scope.UpdateapprOptions = {
        columnDefs: $scope.UpdateapprDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple',
        enableColResize: true,


    };
    function onUpdateFtrChanged(value) {
        $scope.UpdateapprOptions.api.setQuickFilter(value);
    }
    $("#UpdteFilter").change(function () {
        onUpdateFtrChanged($(this).val());
    }).keydown(function () {
        onUpdateFtrChanged($(this).val());
    }).keyup(function () {
        onUpdateFtrChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFtrChanged($(this).val());
    })
    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });
    UtilityService.getSysPreferences().then(function (response) {
        if (response.data != null) {
            $scope.SysPreference = response.data;
            $scope.AllocationType = _.find($scope.SysPreference, { SYSP_CODE: "Allocation Type" })
            $scope.ReqCountType = _.find($scope.SysPreference, { SYSP_CODE: "Requisition Count Type" })
        }
    });

    ViewSpaceRequisitionService.GetPendingList().then(function (data) {
        progress(0, 'Loading...', true);
        if (data.data != null) {
            $scope.gridata = data.data;
            $scope.ViewVerticalReqOptions.api.setRowData([]);
            $scope.ViewVerticalReqOptions.api.setRowData($scope.gridata);
            $scope.ViewVerticalReqOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.ViewVerticalReqOptions.columnApi.getColumn("COST_CENTER_NAME").colDef.headerName = $scope.BsmDet.Child;
            $scope.ViewVerticalReqOptions.api.refreshHeader();
            progress(0, '', false);
        }
        else {
            $scope.ViewVerticalReqOptions.api.setRowData([]);
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', data.Message);

        }
    }, function (response) {
        progress(0, '', false);
    });


    var columnDefsCount = [
        { headerName: "Country", field: "SRC_CNY_NAME", cellClass: "grid-align" },
        { headerName: "City", field: "SRC_CTY_NAME", cellClass: "grid-align" },
        { headerName: "Location", field: "SRC_LCM_NAME", cellClass: "grid-align" },
        { headerName: "Tower", field: "SRC_TWR_NAME", cellClass: "grid-align" },
        { headerName: "Floor", field: "SRC_FLR_NAME", cellClass: "grid-align" },
        { headerName: "", field: "SRC_REQ_CNT", cellClass: "grid-align", filter: 'set', template: "<input type='textbox' ng-model='data.SRC_REQ_CNT'>", suppressMenu: true, },
        {
            headerName: "", field: "SRC_REQ_SEL_TYPE", cellClass: "grid-align", filter: 'set', suppressMenu: true,
            template: "<select  ng-model='data.SRC_REQ_SEL_TYPE' data-ng-change='getSubTypes(data)'><option value=''>--Select--</option><option ng-repeat='Ty in Type' value='{{Ty.CODE}}'>{{Ty.NAME}}</option></select>"
        },
        {
            headerName: "", field: "SRC_REQ_TYPE", cellClass: "grid-align", filter: 'set', suppressMenu: true,
            template: "<select  ng-init='' ng-model='data.SRC_REQ_TYPE'><option value=''>--Select--</option><option ng-repeat='SubTy in SubType' value='{{SubTy.CODE}}'>{{SubTy.NAME}}</option></select>"
        },
    ];
    $scope.gridCountOptions = {
        columnDefs: columnDefsCount,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        //enableFilter: true,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        enableColResize: true,
        onReady: function () {
            $scope.gridCountOptions.api.sizeColumnsToFit()
        }
    };



    $scope.CountryChanged = function () {
        UtilityService.getCitiesbyCny($scope.ViewSpcReq.selectedCountries, 2).then(function (response) {
            if (response.data != null) {
                $scope.Citylst = response.data
            } else {
                $scope.Citylst = [];
                $scope.Locationlst = [];
                $scope.Towerlist = [];
                $scope.Floorlist = [];
            }
        }, function (error) {
            console.log(error);
        });
    }
    $scope.CnyChangeAll = function () {
        $scope.ViewSpcReq.selectedCountries = $scope.countrylist;
        $scope.CountryChanged();
    }
    $scope.cnySelectNone = function () {
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];

    }

    $scope.CityChanged = function () {
        UtilityService.getLocationsByCity($scope.ViewSpcReq.selectedCities, 2).then(function (data) {

            if (data.data != null) {
                $scope.Locationlst = data.data;

            }
            else {
                $scope.Locationlst = [];
                $scope.Towerlist = [];
                $scope.Floorlist = [];
            }

        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.countrylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var cny = _.find($scope.countrylist, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ViewSpcReq.selectedCountries.push(cny);
            }
        });
    }
    $scope.CtyChangeAll = function () {
        $scope.ViewSpcReq.selectedCities = $scope.Citylst;
        $scope.CityChanged();
    }
    $scope.ctySelectNone = function () {
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    $scope.LocChange = function () {
        UtilityService.getTowerByLocation($scope.ViewSpcReq.selectedLocations, 2).then(function (data) {
            if (data.data != null) {
                $scope.Towerlist = data.data;

            } else {
                $scope.Towerlist = [];
                $scope.Floorlist = [];
            }
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.countrylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var cny = _.find($scope.countrylist, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ViewSpcReq.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Locationlst, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ViewSpcReq.selectedCities.push(cty);
            }
        });
    }
    $scope.LCMChangeAll = function () {
        $scope.ViewSpcReq.selectedLocations = $scope.Locationlst;
        $scope.LocChange();
    }
    $scope.lcmSelectNone = function () {
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    $scope.TwrChange = function () {
        UtilityService.getFloorByTower($scope.ViewSpcReq.selectedTowers, 2).then(function (data) {
            if (data.data != null) { $scope.Floorlist = data.data } else { $scope.Floorlist = [] }
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.countrylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            var cny = _.find($scope.countrylist, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ViewSpcReq.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ViewSpcReq.selectedCities.push(cty);
            }
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ViewSpcReq.selectedLocations.push(lcm);
            }
        });
    }
    $scope.TwrChangeAll = function () {
        $scope.ViewSpcReq.selectedTowers = $scope.Towerlist;
        $scope.TwrChange();
    }
    $scope.twrSelectNone = function () {
        $scope.Floorlist = [];
    }
    $scope.FloorChange = function () {

        angular.forEach($scope.countrylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var cny = _.find($scope.countrylist, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ViewSpcReq.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ViewSpcReq.selectedCities.push(cty);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ViewSpcReq.selectedLocations.push(lcm);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var twr = _.find($scope.Towerlist, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.ViewSpcReq.selectedTowers.push(twr);
            }
        });

    }
    $scope.FloorChangeAll = function () {
        $scope.ViewSpcReq.selectedFloors = $scope.Floorlist;
        $scope.FloorChange();
    }


    UtilityService.getVerticals(1).then(function (data) {
        if (data.data != null) { $scope.Verticallist = data.data } else { $scope.Verticallist = [] }
    }, function (error) {
        console.log(error);
    });


    $scope.VerticalChange = function () {
        UtilityService.getCostcenterByVertical($scope.currentblkReq.selectedVerticals, 2).then(function (data) {
            if (data.data != null) {
                $scope.Costcenterlist = data.data;
            }
            else {
                $scope.Costcenterlist = [];
            }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.CostcenterChange = function () {
        angular.forEach($scope.Verticallist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Costcenterlist, function (value, key) {
            var ver = _.find($scope.Verticallist, { VER_CODE: value.Vertical_Code });
            if (ver != undefined && value.ticked == true) {
                ver.ticked = true;
                $scope.currentblkReq.selectedVerticals = ver;
            }
        });
    }

    $scope.onRowSelectedFunc = function (data) {
        progress(0, 'Loading...', true);
        $scope.ShiftFilter = [];
        $scope.RefSelctVl = data;
        var selectedid = data.SRN_STA_ID;

        if (selectedid == '1010' || selectedid == '1017' || selectedid == '1042') {
            $scope.ButtonStatus = 1;
        }
        else {
            $scope.ButtonStatus = 2;
        }
        $scope.Viewstatus = 1;



        L1ApprovalService.GetDetailsOnSelection(data).then(function (response) {

            $scope.currentblkReq = data;
            $scope.currentblkReq.SRN_FROM_DATE = $filter('date')(data.SRN_FROM_DATE, "MM/dd/yyyy");
            $scope.currentblkReq.SRN_TO_DATE = $filter('date')(data.SRN_TO_DATE, "MM/dd/yyyy");
            $scope.currentblkReq.SRN_REQ_DATE = $filter('date')(data.SRN_REQ_DT, "MM/dd/yyyy");


            var shcnt = hasDuplicates(response.data.SELSPACES.spcreqdet, 'SRD_SH_CODE');


            UtilityService.getCountires(2).then(function (Condata) {
                $scope.countrylist = Condata.data;
                if (Condata.data != null) {
                    for (i = 0; i < response.data.SELSPACES.cnylst.length; i++) {
                        var a = _.find($scope.countrylist, { CNY_CODE: response.data.SELSPACES.cnylst[i].CNY_CODE });
                        a.ticked = true;
                    }
                }
                else {
                    $scope.Citylst = [];
                    $scope.Locationlst = [];
                    $scope.Towerlist = [];
                    $scope.Floorlist = [];
                }
            });
            UtilityService.getCities(2).then(function (ctydata) {
                $scope.Citylst = ctydata.data;
                if (ctydata.data != null) {
                    for (i = 0; i < response.data.SELSPACES.ctylst.length; i++) {
                        var a = _.find($scope.Citylst, { CTY_CODE: response.data.SELSPACES.ctylst[i].CTY_CODE });
                        a.ticked = true;

                    }
                }
                else {
                    $scope.Citylst = [];
                    $scope.Locationlst = [];
                    $scope.Towerlist = [];
                    $scope.Floorlist = [];
                }
            });
            UtilityService.getLocations(2).then(function (locdata) {
                $scope.Locationlst = locdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.loclst.length; i++) {
                        var a = _.find($scope.Locationlst, { LCM_CODE: response.data.SELSPACES.loclst[i].LCM_CODE });
                        a.ticked = true;
                        $scope.selectedLocations.push(a);
                    }
                    SpaceRequisitionService.getShifts($scope.selectedLocations).then(function (shft) {
                        $scope.Shifts = [];
                        $scope.Shifts = shft.data;
                        $scope.currentblkReq.ShiftFilter = [];
                        $scope.ShiftFilter = shft.data;
                        if (shcnt.length != 0) {
                            angular.forEach($scope.ShiftFilter, function (Value, Key) {
                                if (shcnt[0].SRD_SH_CODE != undefined && Value.SH_CODE == shcnt[0].SRD_SH_CODE) {
                                    Value.ticked = true;
                                }
                            });
                        }

                    });
                }
                else {
                    $scope.Citylst = [];
                    $scope.Locationlst = [];
                    $scope.Towerlist = [];
                    $scope.Floorlist = [];
                }
            });
            UtilityService.getTowers(2).then(function (twrdata) {
                $scope.Towerlist = twrdata.data;
                if (twrdata.data != null) {
                    for (i = 0; i < response.data.SELSPACES.twrlst.length; i++) {
                        var a = _.find($scope.Towerlist, { TWR_CODE: response.data.SELSPACES.twrlst[i].TWR_CODE });
                        a.ticked = true;

                    }
                }
                else {
                    $scope.Citylst = [];
                    $scope.Locationlst = [];
                    $scope.Towerlist = [];
                    $scope.Floorlist = [];
                }
            });
            UtilityService.getFloors(2).then(function (Flrdata) {
                $scope.Floorlist = Flrdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.flrlst.length; i++) {
                        var flr = _.find($scope.Floorlist, { FLR_CODE: response.data.SELSPACES.flrlst[i].FLR_CODE });
                        flr.ticked = true;
                    }
                }
                else {
                    $scope.Floorlist = [];

                }

            });
            UtilityService.getVerticals(2).then(function (Verdata) {
                $scope.Verticallist = Verdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.verlst.length; i++) {
                        var ver = _.find($scope.Verticallist, { VER_CODE: response.data.SELSPACES.cstlst[i].Vertical_Code });
                        ver.ticked = true;

                    }

                }
                else {
                    $scope.Costcenterlist = [];

                }
            });
            UtilityService.getCostCenters(2).then(function (Cosdata) {
                $scope.Costcenterlist = Cosdata.data;
                if (response.data != null) {
                    for (i = 0; i < response.data.SELSPACES.cstlst.length; i++) {
                        var cos = _.find($scope.Costcenterlist, { Cost_Center_Code: response.data.SELSPACES.cstlst[i].Cost_Center_Code });
                        cos.ticked = true;
                    }

                }

            });

            $scope.selectedSpaces = response.data.SELSPACES.spcreqdet;



            if ($scope.AllocationType.SYSP_VAL1 == "1039" || $scope.AllocationType.SYSP_VAL1 == "1040") {
                setTimeout(function () {
                    $scope.Markers = [];
                    GetMarkers(response.data.DETAILS);
                    $scope.UpdateapprOptions.api.setRowData([]);
                    $scope.UpdateapprOptions.api.setRowData($scope.Markers);
                }, 500);
            }
            if ($scope.AllocationType.SYSP_VAL1 == "1040") {
                $scope.UpdateapprOptions.columnApi.setColumnVisible('SRD_AUR_ID', true);
                $scope.UpdateapprOptions.columnApi.setColumnVisible('AUR_DES_NAME', true);
                $scope.UpdateapprOptions.columnApi.setColumnVisible('ALLOC_SPC_ID', true);
                $scope.UpdateapprOptions.columnApi.setColumnVisible('AUR_KNOWN_AS', true);
                //$scope.UpdateapprOptions.columnApi.setColumnVisible('AUR_KNOWN_AS_NAME', true);
                var SpaceData = { cstlst: response.data.SELSPACES.cstlst, spcreq: $scope.currentblkReq };
                SpaceRequisitionService.getEmpDetails(SpaceData).then(function (emp) {
                    $scope.EmpDetails = emp.data;
                    angular.forEach($scope.Markers, function (Value, Key) {
                        if (Value.ticked) {
                            var emp = _.find($scope.EmpDetails, { AUR_ID: Value.SRD_AUR_ID });
                            if (emp) {
                                Value.AUR_DES_NAME = emp.AUR_DES_NAME;
                                Value.ALLOC_SPC_ID = emp.ALLOC_SPC_ID;
                                Value.AUR_KNOWN_AS = emp.AUR_KNOWN_AS;
                                //Value.AUR_KNOWN_AS_NAME = emp.AUR_KNOWN_AS_NAME;
                            }
                            progress(0, '', false);
                        }
                    });
                });
            }
            else if ($scope.AllocationType.SYSP_VAL1 == "1041") {
                $scope.EnableStatus = 1;
                $scope.gridata = [];
                $scope.gridCountOptions.rowData = [];
                var flrObj = { flrlst: response.data.SELSPACES.flrlst };

                if (response.data.SELSPACES.spcreqcount != null) {
                    $scope.gridCountData = response.data.SELSPACES.spcreqcount;
                    $scope.gridCountOptions.api.setRowData([]);
                    $scope.gridCountOptions.api.setRowData($scope.gridCountData);
                    $scope.SpaceReqCount = $scope.gridCountData;


                    if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Employee Count";
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_SEL_TYPE").colDef.headerName = "Grade Type";
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_TYPE").colDef.headerName = "Request Type";
                        $scope.gridCountOptions.api.refreshHeader();
                        $scope.Type = [];


                        SpaceRequisitionService.getEmpTypes(response.data.SELSPACES.cstlst).then(function (Empresponse) {
                            if (Empresponse.data != null) {
                                $scope.Type = Empresponse.data;
                            }
                        });
                        SpaceRequisitionService.getEmpSubTypes().then(function (Subresponse) {
                            if (Subresponse.data != null) {
                                $scope.SubType = Subresponse.data;
                            }
                        });

                    }
                    else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Space Count";
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_SEL_TYPE").colDef.headerName = "Space Type";
                        $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_TYPE").colDef.headerName = "Space Sub Type";
                        $scope.gridCountOptions.api.refreshHeader();
                        $scope.Type = [];
                        $scope.SubType = [];
                        SpaceRequisitionService.getSpaceTypes().then(function (Spcresponse) {
                            if (Spcresponse.data != null) {
                                $scope.Type = Spcresponse.data;
                            }
                        });
                        SpaceRequisitionService.getSpaceSubTypes().then(function (Spcsbresponse) {
                            if (Spcsbresponse.data != null) {
                                $scope.SubType = Spcsbresponse.data;

                            }
                        });
                    }

                }
                else {
                    setTimeout(function () {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'No data available for selected vertical');
                    }, 1500)
                }
            }
            setTimeout(function () {
                progress(0, '', false);
            }, 1500)


        }, function (response) {
            progress(0, '', false);
        });
    };

    function hasDuplicates(a, ref) {
        return _.uniqBy(a, ref);
    }

    // When Search Spaces Button clicked
    $scope.getSpaces = function () {
        progress(0, 'Loading...', true);
        var searchObj = { flrlst: $scope.ViewSpcReq.selectedFloors, verlst: $scope.currentblkReq.selectedVerticals, cstlst: $scope.currentblkReq.selectedCostcenters, spcreq: $scope.currentblkReq };

        SpaceRequisitionService.getEmpDetails(searchObj).then(function (emp) {
            $scope.EmpDetails = emp.data;
            angular.forEach($scope.Markers, function (Value, Key) {
                if (Value.ticked) {
                    Value.AUR_DES_NAME = _.find($scope.EmpDetails, { AUR_ID: Value.SRD_AUR_ID }).AUR_DES_NAME;
                    Value.ALLOC_SPC_ID = _.find($scope.EmpDetails, { AUR_ID: Value.SRD_AUR_ID }).ALLOC_SPC_ID;
                    Value.ALLOC_SPC_ID = _.find($scope.EmpDetails, { AUR_ID: Value.SRD_AUR_ID }).ALLOC_SPC_ID;
                    progress(0, '', false);
                }
            });
        });
        //SpaceRequisitionService.getShifts($scope.ViewSpcReq.selectedLocations).then(function (shft) {
        //    $scope.Shifts = [];
        //    $scope.Shifts = shft.data;
        //    $scope.ShiftFilter = shft.data;

        //});
        $scope.Markers = [];
        if ($scope.AllocationType.SYSP_VAL1 == "1039") {
            if (moment($scope.currentblkReq.SRN_FROM_DATE) > moment($scope.currentblkReq.SRN_TO_DATE)) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            } else {
                setTimeout(function () {

                    SpaceRequisitionService.getSpaces(searchObj).then(function (response) {
                        if (response.data != null) {
                            $scope.Markers = [];
                            GetMarkers(response.data);
                            $scope.UpdateapprOptions.api.setRowData([]);
                            $scope.UpdateapprOptions.api.setRowData($scope.Markers);

                            progress(0, '', false);
                        }
                        else {
                            $scope.UpdateapprOptions.api.setRowData([]);
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', 'No data available');

                        }

                    });
                }, 500);
            }
        }
        // 1040=Request Raised for Employee
        else if ($scope.AllocationType.SYSP_VAL1 == "1040") {
            if (moment($scope.currentblkReq.SRN_FROM_DATE) > moment($scope.currentblkReq.SRN_TO_DATE)) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            } else {
                setTimeout(function () {
                    SpaceRequisitionService.getSpaces(searchObj).then(function (response) {
                        if (response.data != null) {
                            $scope.Markers = [];
                            GetMarkers(response.data);
                            $scope.UpdateapprOptions.api.setRowData([]);
                            $scope.UpdateapprOptions.api.setRowData($scope.Markers);
                            $scope.UpdateapprOptions.columnApi.setColumnVisible('SRD_AUR_ID', true);
                            $scope.UpdateapprOptions.columnApi.setColumnVisible('AUR_DES_NAME', true);
                            $scope.gridOptions.columnApi.setColumnVisible('ALLOC_SPC_ID', true);
                            $scope.gridOptions.columnApi.setColumnVisible('AUR_KNOWN_AS', true);
                            // $scope.gridOptions.columnApi.setColumnVisible('AUR_KNOWN_AS_NAME', true);
                            progress(0, '', false);
                        }
                        else {
                            $scope.UpdateapprOptions.api.setRowData([]);
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', 'No data available for selected vertical');
                        }
                    });
                }, 500);
            }
        }
        // 1041=Request Raised for Future
        else if ($scope.AllocationType.SYSP_VAL1 == "1041") {
            if (moment($scope.currentblkReq.SRN_FROM_DATE) > moment($scope.currentblkReq.SRN_TO_DATE)) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            } else {
                $scope.gridCountOptions.rowData = [];
                var flrObj = { flrlst: $scope.ViewSpcReq.selectedFloors, REQ_CNT_STA_FUT: $scope.ReqCountType.SYSP_VAL1 };
                SpaceRequisitionService.getReqCountDetails(flrObj).then(function (response) {

                    if (response.data != null) {
                        $scope.gridCountData = [];
                        $scope.gridCountOptions.api.setRowData([]);
                        $scope.gridCountData = response.data;
                        $scope.gridCountOptions.api.setRowData($scope.gridCountData);
                        $scope.SpaceReqCount = $scope.gridCountData;

                        if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Employee Count";
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_SEL_TYPE").colDef.headerName = "Grade Type";
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_TYPE").colDef.headerName = "Request Type";
                            $scope.gridCountOptions.api.refreshHeader();
                            $scope.Type = [];
                            SpaceRequisitionService.getEmpTypes($scope.currentblkReq.selectedCostcenters).then(function (response) {
                                if (response.data != null) {
                                    $scope.Type = response.data;
                                }
                            });
                            SpaceRequisitionService.getEmpSubTypes().then(function (response) {
                                if (response.data != null) {
                                    $scope.SubType = response.data;
                                }
                            });
                            progress(0, '', false);
                        }
                        else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_CNT").colDef.headerName = "Space Count";
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_SEL_TYPE").colDef.headerName = "Space Type";
                            $scope.gridCountOptions.columnApi.getColumn("SRC_REQ_TYPE").colDef.headerName = "Space Sub Type";
                            $scope.gridCountOptions.api.refreshHeader();
                            $scope.Type = [];
                            $scope.SubType = [];
                            SpaceRequisitionService.getSpaceTypes().then(function (response) {
                                if (response.data != null) {
                                    $scope.Type = response.data;
                                }
                            });
                            SpaceRequisitionService.getSpaceSubTypes().then(function (response) {
                                if (response.data != null) {
                                    $scope.SubType = response.data;
                                }
                            });
                            progress(0, '', false);
                        }
                    }
                    else {
                        progress(0, '', false);
                        $scope.gridCountOptions.api.setRowData([]);
                        showNotification('error', 8, 'bottom-right', 'No data available for selected vertical');
                    }
                });
            }
        }
    }

    $scope.setStatus = function (status) {

        switch (status) {
            case 'Update': $scope.RetStatus = UtilityService.Modified;
                $scope.currentblkReq.SVR_UPDATED_BY = UID;
                break;
            case 'Cancel': $scope.RetStatus = UtilityService.Canceled;
                $scope.currentblkReq.SVR_UPDATED_BY = UID;
                break;

        }
    }
    $scope.UpdateSpaceRequisition = function () {
        $scope.tickedSpaces = [];
        progress(0, 'Loading...', true);
        angular.forEach($scope.selectedSpaces, function (data, key) {
            $scope.selspcObj = {};
            $scope.selspcObj.SRD_REQ_ID = data.SRD_REQ_ID;
            $scope.selspcObj.SRD_SRNREQ_ID = data.SRD_SRNREQ_ID;
            $scope.selspcObj.SRD_SSA_SRNREQ_ID = data.SRD_SSA_SRNREQ_ID;
            $scope.selspcObj.SRD_SPC_ID = data.SRD_SPC_ID;
            $scope.selspcObj.SRD_SPC_NAME = data.SRD_SPC_NAME;
            $scope.selspcObj.SRD_SPC_TYPE = data.layer;
            $scope.selspcObj.lat = data.lat;
            $scope.selspcObj.lon = data.lon;
            $scope.selspcObj.SRD_SPC_TYPE_NAME = data.SRD_SPC_TYPE_NAME;
            $scope.selspcObj.SRD_SPC_SUB_TYPE = data.SRD_SPC_SUB_TYPE;
            $scope.selspcObj.SRD_SPC_SUB_TYPE_NAME = data.SRD_SPC_SUB_TYPE_NAME;
            $scope.selspcObj.SRD_SH_CODE = data.SRD_SH_CODE;
            $scope.selspcObj.SRD_AUR_ID = data.SRD_AUR_ID;
            $scope.selspcObj.SSA_FLR_CODE = data.SSA_FLR_CODE;
            $scope.selspcObj.ticked = data.ticked;
            $scope.selspcObj.STACHECK = data.STACHECK;
            if (data.STACHECK == 8 || data.STACHECK == 4 || data.STACHECK == 16) {
                $scope.tickedSpaces.push($scope.selspcObj);
            }

        });
        var flag = false;
        if (moment($scope.currentblkReq.SRN_FROM_DATE) > moment($scope.currentblkReq.SRN_TO_DATE)) {
            progress(0, '', false);
            flag = true;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }

        if ($scope.AllocationType.SYSP_VAL1 == "1039" || $scope.AllocationType.SYSP_VAL1 == "1040") {
            for (var i = 0; $scope.Markers != null && i < $scope.Markers.length; i += 1) {
                if ($scope.Markers[i].ticked && ($scope.Markers[i].SRD_SH_CODE == "" || $scope.Markers[i].SRD_SH_CODE == null)) {
                    $scope.selectedSpaces = [];
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'Please select shift');
                    flag = true;
                    break;
                }
                else if ($scope.Markers[i].ticked && $scope.Markers[i].SRD_AUR_ID == undefined && $scope.AllocationType.SYSP_VAL1 == "1040") {
                    $scope.selectedSpaces = [];
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'Please select employee');
                    flag = true;
                    break;
                }

                else if ($scope.Markers[i].ticked) {
                    $scope.selspcObj = $scope.Markers[i];
                    $scope.selectedSeats.push($scope.selspcObj);

                }
            };
        }
        else if ($scope.AllocationType.SYSP_VAL1 == "1041") {
            angular.forEach($scope.gridCountData, function (o) {
                if (o.SRC_REQ_SEL_TYPE == null || o.SRC_REQ_SEL_TYPE == "" || o.SRC_REQ_TYPE == null || o.SRC_REQ_TYPE == "") {
                    if ($scope.ReqCountType.SYSP_VAL1 == "1038") {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Please select Grade and RequestType');
                        flag = true;
                    }
                    else if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Please select Space and Space Sub Type');
                        flag = true;
                    }
                }

            });

        }
        if (flag == false) {

            if (($scope.selectedSeats.length != 0 && ($scope.AllocationType.SYSP_VAL1 == "1039") || $scope.AllocationType.SYSP_VAL1 == "1040") ||
                $scope.AllocationType.SYSP_VAL1 == "1041") {
                var ReqObj = { ALLOCSTA: $scope.RetStatus, spcreqcount: $scope.SpaceReqCount, spcreqdet: $scope.tickedSpaces, spcreq: $scope.currentblkReq, flrlst: $scope.Floorlist, SRN_SYS_PRF_CODE: $scope.AllocationType.SYSP_VAL1 };

                ViewSpaceRequisitionService.UpdateSpaceRequisition(ReqObj).then(function (response) {
                    if (response != null) {
                        progress(0, '', false);
                        ViewSpaceRequisitionService.GetPendingList().then(function (data) {
                            progress(0, 'Loading...', true);
                            if (data.data != null) {
                                $scope.gridata = data.data;
                                $scope.ViewVerticalReqOptions.api.setRowData([]);
                                $scope.ViewVerticalReqOptions.api.setRowData($scope.gridata);
                                progress(0, '', false);
                            }
                            else {
                                $scope.ViewVerticalReqOptions.api.setRowData([]);
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', data.Message);

                            }
                        }, function (response) {
                            progress(0, '', false);
                        });

                        $scope.back();
                        showNotification('success', 8, 'bottom-right', response.Message);

                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', response.Message);
                    }
                }, function (response) {
                    progress(0, '', false);
                });
            }
            else {

                if (($scope.AllocationType.SYSP_VAL1 == "1039" || $scope.AllocationType.SYSP_VAL1 == "1040") && $scope.RetStatus == 16) {

                    showNotification('error', 8, 'bottom-right', 'Please select atleast one space ID to Update');
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'Space requisition Cancelled Successfully');
                }
            }
        }
    }
    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);

        cb.addEventListener('change', function (e) {

            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.UpdateapprOptions.rowData, function (value, key) {
                        value.ticked = true;
                        //$scope.tempspace = value;
                        //$scope.tempspace.STACHECK = UtilityService.Added;
                        //$scope.selectedSpaces.push($scope.tempspace);
                        //$scope.tempspace = {};
                        //value.setIcon(selctdChricon);
                    });
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.UpdateapprOptions.rowData, function (value, key) {
                        value.ticked = false;
                        //_.remove($scope.selectedSpaces, _.find($scope.selectedSpaces, { SRD_REQ_ID: value.SRD_REQ_ID }));
                        //value.STACHECK = UtilityService.Deleted;
                        //$scope.selectedSpaces.push(value);
                        //value.setIcon(Vacanticon);
                    });
                });
            }
        });
        return eHeader;
    }
    $scope.chkChanged = function (selctedRow) {
        if ($scope.drawnItems)
            $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: selctedRow.SRD_SPC_ID, spacetype: 'CHA' } });
        if (!selctedRow.ticked) {
            if ($scope.chkr) {
                $scope.chkr.setStyle(VacantStyle);
                $scope.chkr.ticked = false;
            }
            var selrow = _.find($scope.selectedSpaces, { SRD_SSA_SRNREQ_ID: selctedRow.SRD_SSA_SRNREQ_ID });
            selrow.STACHECK = UtilityService.Deleted;
        }
        else {
            if ($scope.chkr) {
                $scope.chkr.setStyle(selctdChrStyle);
                $scope.chkr.ticked = true;
            }
            $scope.tempspace = selctedRow;
            $scope.tempspace.STACHECK = UtilityService.Added;
            $scope.selectedSpaces.push($scope.tempspace);
            $scope.tempspace = {};
        }

    }
    //$scope.shiftChanged = function (shiftcode) {
    //    var shf = _.remove($scope.selectedSpaces, _.find($scope.selectedSpaces, { SRD_REQ_ID: shiftcode.SRD_REQ_ID }));
    //    if (shiftcode.STACHECK == 0) {
    //        if (shiftcode.SRD_SH_CODE == '')
    //            shiftcode.STACHECK = UtilityService.Deleted;
    //        else
    //            shiftcode.STACHECK = UtilityService.Added;
    //    }
    //    $scope.selectedSpaces.push(shiftcode);
    //}
    $scope.CheckSpace = function (Data) {
        progress(0, 'Loading...', true);
        $scope.selspcObj = {};
        $scope.spcreqdet = [];

        $scope.selspcObj.SRD_SH_CODE = Data.SRD_SH_CODE;
        $scope.selspcObj.SRD_SPC_ID = Data.SRD_SPC_ID;

        $scope.spcreqdet.push($scope.selspcObj);
        var SpaceData = { spcreq: $scope.currentblkReq, spcreqdet: $scope.spcreqdet };

        SpaceRequisitionService.CheckSpace(SpaceData).then(function (response) {
            if (response.data == 2) {
                Data.ticked = false;
                $scope.chkChanged(Data);
                Data.SRD_SH_CODE = '';
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else if (response.data == 0) {
                Data.ticked = false;
                $scope.chkChanged(Data);
                Data.SRD_SH_CODE = '';
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else {
                $scope.chkChanged(Data);
                Data.ticked = true;
                progress(0, '', false);
            }
        }, function (error) {
            console.log(error);
        });
    }
    //to get space subtype based on selected space
    $scope.getSubTypes = function (SpaceType) {
        if ($scope.ReqCountType.SYSP_VAL1 == "1037") {
            $scope.SubType = [];
            SpaceRequisitionService.getSubTypes(SpaceType.SRC_REQ_SEL_TYPE).then(function (response) {
                $scope.SubType = response.data;
                //data.SRC_REQ_TYPE=
            }, function (error) {
                console.log(error);
            });
        }
    }
    $scope.GetDesignation = function (Aur, old) {
        Aur.AUR_DES_NAME = "";
        Aur.ALLOC_SPC_ID = "";
        //Aur.AUR_KNOWN_AS_NAME = "";
        Aur.AUR_KNOWN_AS = "";
        for (var i = 0; $scope.EmpDetails != null && i < $scope.EmpDetails.length; i += 1) {
            if ($scope.EmpDetails[i].AUR_ID === Aur.SRD_AUR_ID) {
                Aur.AUR_DES_NAME = $scope.EmpDetails[i].AUR_DES_NAME;
                Aur.ALLOC_SPC_ID = $scope.EmpDetails[i].ALLOC_SPC_ID;
                //Aur.AUR_KNOWN_AS_NAME = $scope.EmpDetails[i].AUR_KNOWN_AS_NAME;
                Aur.AUR_KNOWN_AS = $scope.EmpDetails[i].AUR_KNOWN_AS;
                $scope.EmpDetails[i].ticked = true;
            }
            if ($scope.EmpDetails[i].AUR_ID === old) {
                $scope.EmpDetails[i].ticked = false;
            }
        }
    }

    $scope.back = function () {
        $scope.Viewstatus = 0;
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
        $scope.countrylist = [];
        $scope.SelectAll = [];
        $scope.Shifts = [];
        $scope.EnableStatus = 0;
    }

    ///map
    $scope.ViewinMap = function () {
        if ($scope.SelRowData.length == 0) {
            $scope.MapFloors = [];
            $scope.Map.Floor = [];
            angular.forEach($scope.ViewSpcReq.selectedFloors, function (Value, Key) {
                Value.ticked = false;
                $scope.MapFloors.push(Value);
            });
            $scope.MapFloors[0].ticked = true;
            $scope.Map.Floor.push($scope.MapFloors[0]);
        }

        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {

        if ($scope.SelRowData.length == 0)
            $scope.loadmap();

    });
    $scope.FlrSectMap = function (data) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();

    }
    $scope.loadmap = function () {
        progress(0, 'Loading...', true);
        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE, key_value: 1 };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post(UtilityService.path + '/api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });

    }
    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
            // success
            // results: an array of data objects from each deferred.resolve(data) call
            function (results) {
                var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
                map.fitBounds(bounds);
                $scope.SelRowData = $filter('filter')($scope.Markers, { SSA_FLR_CODE: $scope.Map.Floor[0].FLR_CODE });
                angular.forEach($scope.SelRowData, function (value, key) {
                    $scope.marker = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: value.SRD_SPC_ID, spacetype: 'CHA' } });
                    $scope.marker.SRD_REQ_ID = value.SRD_REQ_ID;
                    $scope.marker.SRD_SRNREQ_ID = value.SRD_SRNREQ_ID;
                    $scope.marker.SRD_SSA_SRNREQ_ID = value.SRD_SSA_SRNREQ_ID;
                    $scope.marker.SRD_SPC_ID = value.SRD_SPC_ID;
                    $scope.marker.SRD_SPC_NAME = value.SRD_SPC_NAME;
                    $scope.marker.layer = value.SRD_SPC_TYPE;
                    $scope.marker.SRD_SPC_TYPE_NAME = value.SRD_SPC_TYPE_NAME;
                    $scope.marker.SRD_SPC_SUB_TYPE = value.SRD_SPC_SUB_TYPE;
                    $scope.marker.SRD_SPC_SUB_TYPE_NAME = value.SRD_SPC_SUB_TYPE_NAME;
                    $scope.marker.SRD_SH_CODE = value.SRD_SH_CODE;
                    $scope.marker.SRD_AUR_ID = value.SRD_AUR_ID;
                    $scope.marker.SSA_FLR_CODE = value.SSA_FLR_CODE;
                    $scope.marker.STACHECK = value.STACHECK;
                    $scope.marker.ticked = value.ticked;
                    if (value.ticked)
                        $scope.marker.setStyle(selctdChrStyle);
                    else
                        $scope.marker.setStyle(VacantStyle);
                    $scope.marker.bindLabel(value.SRD_SPC_NAME);
                    $scope.marker.on('click', markerclicked);
                    $scope.marker.addTo(map);
                });
            },
            function (response) {
            }
        );
    };

    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };

    function GetMarkers(data) {

        jQuery.each(data, function (index, value) {

            $scope.marker = {};
            $scope.marker.SRD_REQ_ID = value.SRD_REQ_ID;
            $scope.marker.SRD_SRNREQ_ID = value.SRD_SRNREQ_ID;
            $scope.marker.SRD_SSA_SRNREQ_ID = value.SRD_SSA_SRNREQ_ID;
            $scope.marker.SRD_SPC_ID = value.SRD_SPC_ID;
            $scope.marker.SRD_SPC_NAME = value.SRD_SPC_NAME;
            $scope.marker.layer = value.SRD_SPC_TYPE;
            $scope.marker.SRD_SPC_TYPE_NAME = value.SRD_SPC_TYPE_NAME;
            $scope.marker.SRD_SPC_SUB_TYPE = value.SRD_SPC_SUB_TYPE;
            $scope.marker.SRD_SPC_SUB_TYPE_NAME = value.SRD_SPC_SUB_TYPE_NAME;
            $scope.marker.SRD_SH_CODE = value.SRD_SH_CODE;
            $scope.marker.SRD_AUR_ID = value.SRD_AUR_ID;
            $scope.marker.SSA_FLR_CODE = value.SSA_FLR_CODE;
            $scope.marker.STACHECK = value.STACHECK;
            $scope.marker.ticked = value.ticked;
            $scope.Markers.push($scope.marker);

        });
    };

    function markerclicked(e) {
        var marker = _.find($scope.Markers, { SRD_SPC_ID: this.SRD_SPC_ID });
        if (!this.ticked) {
            this.setStyle(selctdChrStyle)
            this.ticked = true;
            marker.ticked = true;
        }
        else {
            this.setStyle(VacantStyle)
            this.ticked = false;
            marker.ticked = false;
        }

        $scope.chkChanged(marker);
        $scope.UpdateapprOptions.api.refreshView();
    }

}]);
function setup(id) {
    $('#' + id).datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true,
        todayHighlight: true
    });
};