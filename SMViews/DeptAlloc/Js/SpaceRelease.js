﻿app.service("SpaceReleaseService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    this.GetSpacesToRelease = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRelease/GetSpacesToRelease', searchObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.ReleaseSelectedSpaces = function (saveObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceRelease/ReleaseSelectedSpaces', saveObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('SpaceReleaseController', ['$scope', '$q', 'SpaceReleaseService', 'UtilityService','$filter', function ($scope, $q, SpaceReleaseService, UtilityService, $filter) {
    $scope.SpaceRelease = {};
    $scope.SEM = {};
    $scope.Getcountry = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.Verticallist = [];
    $scope.CostCenterlist = [];
    $scope.SRShowGrid = false;
    sendCheckedValsObj = [];
    $scope.tempspace = {};

    $scope.Map = {};
    $scope.Map.Floor = [];
    $scope.Markers = [];
    sendCheckedValsObj = [];
    $scope.SelRowData = [];
    var map = L.map('leafletMap');//.setView([17.561298804683357, 79.6684030798511], 11);


    $scope.SPACE_RELEASE = [
        { release: "Employee", RELEASE_STA_ID: "1049", ticked: false }, //////     From Employee to department 
        { release: "Department", RELEASE_STA_ID: "1050", ticked: false }, /////////  From Dept to Vertical
        { release: "Employee & Department", RELEASE_STA_ID: "1051", ticked: false }, //////////  From Employee To vertical 
        { release: "Blocked", RELEASE_STA_ID: "1052", ticked: false }
    ];

    $scope.LoadDetails = function () {
        progress(0, 'Loading...', true);
        UtilityService.getBussHeirarchy().then(function (response) {
            if (response.data != null) {
                $scope.BsmDet = response.data;
            }
        });

        UtilityService.getCountires(2).then(function (response) {
            $scope.Getcountry = response.data;

            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.Citylst = response.data;

                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Locationlst = response.data;

                            UtilityService.getTowers(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Towerlist = response.data;

                                    UtilityService.getFloors(2).then(function (response) {
                                        if (response.data != null) {
                                            $scope.Floorlist = response.data;
                                        }


                                        UtilityService.getVerticals(2).then(function (response) {
                                            $scope.Verticallist = response.data;
                                        }, function (error) {
                                            console.log(error);
                                            progress(0, '', false);
                                        });

                                        UtilityService.getCostCenters(2).then(function (data) {
                                            $scope.CostCenterlist = data.data;

                                            progress(0, '', false);
                                        }, function (error) {

                                            progress(0, '', false);
                                            console.log(error);
                                        });

                                    });
                                }
                            });

                        }
                    });
                }
            });


        }, function (error) {
            console.log(error);
        });
    }





    $scope.CountryChanged = function () {
        UtilityService.getCitiesbyCny($scope.SEM.selectedCountries, 2).then(function (response) {
            $scope.Citylst = response.data
        }, function (error) {
            console.log(error);
        });
    }

    $scope.CnyChangeAll = function () {
        UtilityService.getCitiesbyCny($scope.Getcountry, 2).then(function (response) {
            $scope.Citylst = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectNone = function () {
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];

    }

    $scope.CityChanged = function () {
        UtilityService.getLocationsByCity($scope.SEM.selectedCities, 2).then(function (response) {
            $scope.Locationlst = response.data;
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.Getcountry, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Citylst, function (value, key) {
            var cny = _.find($scope.Getcountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SEM.selectedCountries = $.makeArray(cny);
            }
        });
    }

    $scope.CtyChangeAll = function () {
        UtilityService.getLocationsByCity($scope.Citylst, 2).then(function (response) {
            $scope.Locationlst = response.data;
        }, function (error) {
            console.log(error);
        });
        $scope.CityChanged();
    }

    $scope.ctySelectNone = function () {
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
        $scope.CityChanged();
    }

    $scope.LocChange = function () {
        UtilityService.getTowerByLocation($scope.SEM.selectedLocations, 2).then(function (response) {
            $scope.Towerlist = response.data;
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.Getcountry, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Locationlst, function (value, key) {
            var cny = _.find($scope.Getcountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SEM.selectedCountries = $.makeArray(cny);
            }
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SEM.selectedCities = $.makeArray(cty);
            }
        });

    }

    $scope.LCMChangeAll = function () {
        UtilityService.getTowerByLocation($scope.Locationlst, 2).then(function (response) {
            $scope.Towerlist = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.lcmSelectNone = function () {
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    $scope.TwrChange = function () {
        UtilityService.getFloorByTower($scope.SEM.selectedTowers, 2).then(function (response) {
            $scope.Floorlist = response.data;
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.Getcountry, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            var cny = _.find($scope.Getcountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SEM.selectedCountries = $.makeArray(cny);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SEM.selectedCities = $.makeArray(cty);
            }
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SEM.selectedLocations = $.makeArray(lcm);
            }
        });

    }

    $scope.TwrChangeAll = function () {
        UtilityService.getFloorByTower($scope.Towerlist, 2).then(function (response) {
            $scope.Floorlist = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.twrSelectNone = function () {
        $scope.Floorlist = [];
    }

    $scope.FloorChangeAll = function () {
        $scope.Floorlist = $scope.Floor;
        // $scope.FloorChange();
    }

    $scope.FloorSelectNone = function () {
        $scope.Floorlist = [];
        $scope.FloorChange();
    }

    $scope.FloorChange = function () {
        angular.forEach($scope.Getcountry, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Floorlist, function (value, key) {
            var cny = _.find($scope.Getcountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SEM.selectedCountries = $.makeArray(cny);
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SEM.selectedCities = $.makeArray(cty);
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SEM.selectedLocations = $.makeArray(lcm);
            }
        });
        angular.forEach($scope.Floorlist, function (value, key) {
            var twr = _.find($scope.Towerlist, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SEM.selectedTowers = $.makeArray(twr);
            }
        });
    }

    $scope.VerticalChange = function () {
        UtilityService.getCostcenterByVertical($scope.SEM.selectedVerticals, 2).then(function (data) {
            $scope.CostCenterlist = data.data;
            console.log($scope.SEM.selectedVerticals);
        }, function (error) {
            console.log(error);
        });
    }

    $scope.VerticalChangeAll = function () {
        $scope.SEM.selectedVerticals = $scope.Verticallist;
        UtilityService.getCostcenterByVertical($scope.Verticallist, 2).then(function (data) {
            $scope.CostCenterlist = data.data;
        }, function (error) {
            console.log(error);
        });
        //$scope.CostCenterChange();
    }

    $scope.verticalSelectNone = function () {
        $scope.CostCenterlist = [];
        $scope.CostCenterChange();
    }

    $scope.CostCenterChange = function () {
        //  if ($scope.SEM.selectedVerticals.length == 0) {
        angular.forEach($scope.Verticallist, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.CostCenterlist, function (value, key) {
            var cc = _.find($scope.Verticallist, { VER_CODE: value.Vertical_Code });
            if (cc != undefined && value.ticked == true) {
                cc.ticked = true;
                $scope.SEM.selectedVerticals = cc;
            }
        });
        //}
    }

    $scope.CostCenterChangeAll = function () {
        $scope.CostCenterChange();
    }

    $scope.CostCenterSelectNone = function () {
        $scope.CostCenterChange();
    }

    var columnDefs = [
        {
            headerName: "Select All", field: "Selected", width: 75, template: "<input type='checkbox' ng-model='data.Selected' ng-change='chkChanged(data)'>",
            cellClass: 'grid-align', headerCellRenderer: headerCellRendererFunc, suppressMenu: true,
        },
        { headerName: "Space", field: "SPC_NAME", width: 180, cellClass: 'grid-align' },
        { headerName: "Employee", hide: true, field: "AUR_KNOWN_AS", width: 200, cellClass: 'grid-align' },
        { headerName: "Release Date", field: "SSR_REL_DATE", width: 180, cellClass: 'grid-align', cellRenderer: createFromDatePicker },
        { headerName: "Space Type", field: "SSA_SPC_TYPE", width: 120, cellClass: 'grid-align', },
        { headerName: "Space Sub Type", field: "SSA_SPC_SUB_TYPE", width: 120, cellClass: 'grid-align', },
        { headerName: "Shift Type", field: "SH_NAME", width: 250, cellClass: 'grid-align', },
        { headerName: "", field: "SSED_VER_NAME", width: 200, cellClass: 'grid-align', },
        { headerName: "", field: "Cost_Center_Name", width: 200, cellClass: 'grid-align', },

        { headerName: "Designation", hide: true, field: "EMP_DESIGNATION", width: 180, cellClass: 'grid-align' },
        { headerName: "From Date", template: '{{data.SSAD_FROM_DATE | date: "dd MMM, yyyy"}}', field: "SSAD_FROM_DATE", width: 130, cellClass: 'grid-align', suppressMenu: true, },
        { headerName: "To Date", template: '{{data.SSAD_TO_DATE | date: "dd MMM, yyyy"}}', field: "SSAD_TO_DATE", width: 130, cellClass: 'grid-align', suppressMenu: true, },

    ];

    function createFromDatePicker(params) {
        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.SSR_REL_DATE');
        newDate.type = "text";
        newDate.id = params.rowIndex;
        newDate.className = "pickDate";
        $(newDate).datepicker({
            //format: 'dd M, yyyy',
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
        })
        return newDate;
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        //rowData: null,
        enableSorting: true,
        //rowSelection: 'multiple',
        enableColResize: true,
        angularCompileRows: true,
    };

    $scope.GetSpacesToRelease = function () {
        $scope.SSR_REQ_REM = "";

        var fromdate = moment($scope.SEM.FROM_DATE);
        var todate = moment($scope.SEM.TO_DATE);
        if (fromdate > todate) {
            $scope.SRShowGrid = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);
            $scope.Markers = [];
            $scope.SelRowData = [];
            map.eachLayer(function (layer) {
                map.removeLayer(layer);
            });
            var selctdVal = _.find($scope.SPACE_RELEASE, { ticked: true });
            $scope.ReleaseFrom = selctdVal.RELEASE_STA_ID;
            $scope.SRVM = {
                EMP_ID: $scope.SEM.EMP_ID,
                FromDate: $scope.SEM.FROM_DATE,

                //RELEASE_STA_ID: $scope.ReleaseFrom,
                RELEASE_STA_ID: parseInt(selctdVal.RELEASE_STA_ID),
                ToDate: $scope.SEM.TO_DATE,
                selectedCities: $scope.SEM.selectedCities,
                selectedCostcenters: $scope.SEM.selectedCostcenter,
                selectedCountries: $scope.SEM.selectedCountries,
                selectedFloors: $scope.SEM.selectedFloors,
                selectedLocations: $scope.SEM.selectedLocations,
                selectedTowers: $scope.SEM.selectedTowers,
                selectedVerticals: $scope.SEM.selectedVerticals,


            };
            console.log($scope.SRVM);
            SpaceReleaseService.GetSpacesToRelease($scope.SRVM).then(function (response) {
                if (response.data == null) {
                    $scope.SRShowGrid = false;
                    progress(0, 'Loading...', false);
                    showNotification('error', 8, 'bottom-right', response.Message);

                }
                else {
                    $scope.SRShowGrid = true;
                    $scope.gridOptions.api.setRowData([]);

                    $scope.gridOptions.columnApi.getColumn("SSED_VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
                    $scope.gridOptions.columnApi.getColumn("Cost_Center_Name").colDef.headerName = $scope.BsmDet.Child;
                    $scope.gridOptions.api.refreshHeader();

                    $scope.gridata = response.data;
                    if ($scope.ReleaseFrom == "1049" || $scope.ReleaseFrom == "1051") {
                        $scope.gridOptions.columnApi.setColumnVisible('AUR_KNOWN_AS', true);
                        $scope.gridOptions.columnApi.setColumnVisible('EMP_DESIGNATION', true);
                    }
                    else {
                        $scope.gridOptions.columnApi.setColumnVisible('AUR_KNOWN_AS', false);
                        $scope.gridOptions.columnApi.setColumnVisible('EMP_DESIGNATION', false);
                    }
                    GetMarkers(response.data);
                    $scope.gridOptions.api.setRowData($scope.Markers);

                    progress(0, 'Loading...', false);
                }
            }, function (error) {
                console.log(error);
                progress(0, 'Loading...', false);
            });
        }
    }

    $scope.chkChanged = function (data) {
        if ($scope.drawnItems) {
            $scope.chkr = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: data.SVD_SPC_ID, spacetype: 'CHA' } });
            if (data.Selected) {
                $scope.chkr.setStyle(selctdChrStyle);
                $scope.chkr.Selected = true;
            }
            //data.setIcon(selctdChricon)
            else {
                $scope.chkr.setStyle(VacantStyle);
                $scope.chkr.Selected = false;
                //data.setIcon(Vacanticon)
            }
        }
    }

    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        cb.setAttribute('type', 'checkbox');

        var eHeader = document.createElement('label');
        var eTitle = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle);

        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.Selected = true;
                    });

                    if ($scope.drawnItems) {
                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SVD_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.Selected = true;
                                value.setStyle(selctdChrStyle);
                            }
                        });
                    }


                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.Markers, function (value, key) {
                        value.Selected = false;
                    });

                    if ($scope.drawnItems) {
                        angular.forEach($scope.drawnItems._layers, function (value, key) {
                            if (value.SVD_SPC_ID != undefined && value.options.spacetype == 'CHA') {
                                value.Selected = false;
                                value.setStyle(VacantStyle);
                            }
                        });
                    }

                });
            }
        });
        return eHeader;
    }

    $scope.ReleaseSelectedSpaces = function () {
        var selctdVal = _.find($scope.SPACE_RELEASE, { ticked: true });
        selctdVal = selctdVal.RELEASE_STA_ID;
        var sendCheckedValsObj = [];
        angular.forEach($scope.Markers, function (value, Key) {
            $scope.selspcObj = {};
            if (value.Selected) {
                $scope.selspcObj.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                $scope.selspcObj.SSAD_AUR_ID = value.SSAD_AUR_ID;
                $scope.selspcObj.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                $scope.selspcObj.SSAD_SRN_CC_ID = value.SSAD_SRN_CC_ID;
                $scope.selspcObj.SSED_SPC_ID = value.SSED_SPC_ID;
                $scope.selspcObj.SSAD_FROM_DATE = value.SSAD_FROM_DATE;
                $scope.selspcObj.SSAD_TO_DATE = value.SSAD_TO_DATE;
                $scope.selspcObj.SSAD_FROM_TIME = value.SSAD_FROM_TIME;
                $scope.selspcObj.SSAD_TO_TIME = value.SSAD_TO_TIME;
                $scope.selspcObj.AUR_KNOWN_AS = value.AUR_KNOWN_AS;
                $scope.selspcObj.SSED_AUR_ID = value.SSED_AUR_ID;
                $scope.selspcObj.STATUS = value.STATUS;
                $scope.selspcObj.SSR_REL_DATE = value.SSR_REL_DATE;
                $scope.selspcObj.Selected = value.Selected;
                $scope.selspcObj.SSAD_ID = value.SSAD_ID;

                sendCheckedValsObj.push($scope.selspcObj);
            }
        });
        if (sendCheckedValsObj.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please Check At Least One Space Id To Release.');
        }
        else {
            $scope.dataobject = {
                SSR: sendCheckedValsObj,
                Remarks: $scope.SSR_REQ_REM,
                RELEASE_STA_ID: selctdVal,
            };
            SpaceReleaseService.ReleaseSelectedSpaces($scope.dataobject).then(function (response) {
                if (response.data != null) {
                    $scope.Clear();
                    showNotification('success', 8, 'bottom-right', response.Message);
                }
                else {
                    showNotification('error', 8, 'bottom-right', response.Message);
                }
            }, function (response) {
                progress(0, '', false);
            });
        }

    }

    $scope.Clear = function () {
        //$scope.SEM = {};
        $scope.gridata = [];
        $scope.selectedSpaces = [];
        $scope.Markers = [];
        $scope.gridOptions.rowData = [];
        angular.forEach($scope.Getcountry, function (country) {
            country.ticked = false;
        });

        angular.forEach($scope.Citylst, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Towerlist, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floorlist, function (floor) {
            floor.ticked = false;
        });

        angular.forEach($scope.Verticallist, function (ver) {
            ver.ticked = false;
        });

        angular.forEach($scope.CostCenterlist, function (cc) {
            cc.ticked = false;
        });

        angular.forEach($scope.SPACE_RELEASE, function (SR) {
            SR.ticked = false;
        });

        $scope.SEM.Getcountry = [];
        $scope.SEM.Citylst = [];
        $scope.SEM.Locationlst = [];
        $scope.SEM.Towerlist = [];
        $scope.SEM.Floorlist = [];
        $scope.SEM.Verticallist = [];
        $scope.SEM.CostCenterlist = [];

        $scope.release = [];

        $scope.SEM.FROM_DATE = "";
        $scope.SEM.TO_DATE = "";
        $scope.SSR_REQ_REM = "";
        $scope.SRShowGrid = false;
        $scope.frmSpaceRelease.$submitted = false;
    }

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#SpaceReleaseReqFilter").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    })

    ///// For map layout
    $scope.ViewinMap = function () {
        if ($scope.SelRowData.length == 0) {
            $scope.MapFloors = [];
            angular.forEach($scope.SEM.selectedFloors, function (Value, Key) {
                Value.ticked = false;
                $scope.MapFloors.push(Value);
            });
            $scope.MapFloors[0].ticked = true;
            $scope.Map.Floor.push($scope.MapFloors[0]);
        }

        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {
        if ($scope.SelRowData.length == 0)
            $scope.loadmap();
    });

    $scope.FlrSectMap = function (data) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();

    }

    $scope.loadmap = function () {
        progress(0, 'Loading...', true);
        //$('#leafletMap').empty();

        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE, key_value: 1 };

        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post('../../../api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });
    }

    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            // do something
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SVD_SPC_ID = value.SPACE_ID;
            theLayer.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
            defer.resolve(theLayer);
            promises.push(defer.promise);
        });

        $q.all(promises).then(
            // success
            // results: an array of data objects from each deferred.resolve(data) call
            function (results) {
                var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
                map.fitBounds(bounds);
                $scope.SelRowData = $filter('filter')($scope.Markers, { SPC_FLR_ID: $scope.Map.Floor[0].FLR_CODE });
                angular.forEach($scope.SelRowData, function (value, key) {

                    $scope.marker = _.find($scope.drawnItems._layers, { options: { SVD_SPC_ID: value.SSED_SPC_ID, spacetype: 'CHA' } });
                    $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
                    $scope.marker.SSAD_AUR_ID = value.SSAD_AUR_ID;
                    $scope.marker.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
                    $scope.marker.SSAD_SRN_CC_ID = value.SSAD_SRN_CC_ID;
                    $scope.marker.SSED_SPC_ID = value.SSED_SPC_ID;
                    $scope.marker.SSAD_FROM_DATE = value.SSAD_FROM_DATE;
                    $scope.marker.SSAD_TO_DATE = value.SSAD_TO_DATE;
                    $scope.marker.SSAD_FROM_TIME = value.SSAD_FROM_TIME;
                    $scope.marker.SSAD_TO_TIME = value.SSAD_TO_TIME;
                    $scope.marker.SSA_SPC_TYPE = value.SSA_SPC_TYPE;
                    $scope.marker.SSA_SPC_SUB_TYPE = value.SSA_SPC_SUB_TYPE;
                    $scope.marker.SH_NAME = value.SH_NAME;
                    $scope.marker.Cost_Center_Name = value.Cost_Center_Name;
                    $scope.marker.AUR_KNOWN_AS = value.AUR_KNOWN_AS;
                    $scope.marker.SSED_AUR_ID = value.SSED_AUR_ID;
                    $scope.marker.SSED_VER_NAME = value.SSED_VER_NAME;
                    $scope.marker.EMP_DESIGNATION = value.EMP_DESIGNATION;
                    $scope.marker.STATUS = value.STATUS;
                    $scope.marker.SSR_REL_DATE = value.SSR_REL_DATE;
                    $scope.marker.Selected = value.Selected;
                    $scope.marker.SSAD_ID = value.SSAD_ID;
                    $scope.marker.SPC_NAME = value.SPC_NAME;
                    $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
                    $scope.marker.lat = value.lat;
                    $scope.marker.lon = value.lon;
                    if (value.Selected)
                        $scope.marker.setStyle(selctdChrStyle);
                    else
                        $scope.marker.setStyle(VacantStyle);
                    $scope.marker.layer = value.SSA_SPC_TYPE;
                    $scope.marker.bindLabel(value.SPC_NAME);
                    $scope.marker.on('click', markerclicked);
                    $scope.marker.addTo(map);
                });
            },
            // error
            function (response) {
            }
        );
    };

    //var Vacanticon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_Green.gif',
    //    iconSize: [16, 16], // size of the icon
    //});
    //var selctdChricon = L.icon({
    //    iconUrl: UtilityService.path + '/images/chair_yellow.gif',
    //    iconSize: [16, 16], // size of the icon
    //});


    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };

    function GetMarkers(data) {

        jQuery.each(data, function (index, value) {

            $scope.marker = {};

            $scope.marker.SSA_SRNREQ_ID = value.SSA_SRNREQ_ID;
            $scope.marker.SSAD_AUR_ID = value.SSAD_AUR_ID;
            $scope.marker.SSAD_SRN_REQ_ID = value.SSAD_SRN_REQ_ID;
            $scope.marker.SSAD_SRN_CC_ID = value.SSAD_SRN_CC_ID;
            $scope.marker.SSED_SPC_ID = value.SSED_SPC_ID;
            $scope.marker.SSAD_FROM_DATE = value.SSAD_FROM_DATE;
            $scope.marker.SSAD_TO_DATE = value.SSAD_TO_DATE;
            $scope.marker.SSAD_FROM_TIME = value.SSAD_FROM_TIME;
            $scope.marker.SSAD_TO_TIME = value.SSAD_TO_TIME;
            $scope.marker.SSA_SPC_TYPE = value.SSA_SPC_TYPE;
            $scope.marker.SSA_SPC_SUB_TYPE = value.SSA_SPC_SUB_TYPE;
            $scope.marker.SH_NAME = value.SH_NAME;
            $scope.marker.Cost_Center_Name = value.Cost_Center_Name;
            $scope.marker.AUR_KNOWN_AS = value.AUR_KNOWN_AS;
            $scope.marker.SSED_AUR_ID = value.SSED_AUR_ID;
            $scope.marker.SSED_VER_NAME = value.SSED_VER_NAME;
            $scope.marker.EMP_DESIGNATION = value.EMP_DESIGNATION;
            $scope.marker.STATUS = value.STATUS;
            $scope.marker.SSR_REL_DATE = value.SSR_REL_DATE;
            $scope.marker.Selected = value.Selected;
            $scope.marker.SSAD_ID = value.SSAD_ID;
            $scope.marker.SPC_NAME = value.SPC_NAME;
            $scope.marker.SPC_FLR_ID = value.SPC_FLR_ID;
            $scope.marker.lat = value.lat;
            $scope.marker.lon = value.lon;
            $scope.marker.layer = value.SSA_SPC_TYPE;
            $scope.Markers.push($scope.marker);
        });
    };

    function markerclicked(e) {
        var marker = _.find($scope.Markers, { SSED_SPC_ID: this.SSED_SPC_ID });
        if (!this.Selected) {
            this.setStyle(selctdChrStyle)
            this.Selected = true;
            marker.Selected = true;
        }
        else {
            this.setStyle(VacantStyle)
            this.Selected = false;
            marker.Selected = false;
        }
        $scope.gridOptions.api.refreshView();
    }

    setTimeout(function () {
        $scope.LoadDetails();
    }, 200);

}]);

