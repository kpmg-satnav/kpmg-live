﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/colorpicker.min.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>
    
</head>
<body data-ng-controller="SearchSpacesController" class="amantra" onload="setDateVals()">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Search Spaces</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <div class="clearfix">
                        <div class="box-footer text-right">
                            <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                        </div>
                    </div>
                    <br />
                    <form id="Form1" name="frmSearchspc" data-valid-submit="SearchSpaces()" novalidate>

                        <%--<label class="control-label"><span style="color: red;">&nbsp&nbsp**</span>&nbsp&nbsp{{Autoselect}}</label>--%>
                        <%--<div class="panel-body">--%>
                        <div class="row">
                            <div class="clearfix">

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CNY_NAME.$invalid}">
                                        <label class="control-label">Country <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Country" data-output-model="SearchSpace.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                            data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SearchSpace.Country" name="CNY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CNY_NAME.$invalid">Please select country </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid}">
                                        <label class="control-label">City <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="City" data-output-model="SearchSpace.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                            data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SearchSpace.City" name="CTY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.CTY_NAME.$invalid">Please select city </span>

                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.LCM_NAME.$invalid}">
                                        <label class="control-label">Location <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Location" data-output-model="SearchSpace.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                            data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SearchSpace.Location" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.LCM_NAME.$invalid">Please select location </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid}">
                                        <label class="control-label">Tower <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Tower" data-output-model="SearchSpace.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                            data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SearchSpace.Tower" name="TWR_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.TWR_NAME.$invalid">Please select tower </span>
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix">

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid}">
                                        <label class="control-label">Floor <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Floor" data-output-model="SearchSpace.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                            data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()" data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="SearchSpace.Floor" name="FLR_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.FLR_NAME.$invalid">Please select floor </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.SVR_FROM_DATE.$invalid}">
                                        <label class="control-label">From Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" style="width: 150px" id='fromdate'>
                                            <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="SVR_FROM_DATE" name="SVR_FROM_DATE" ng-model="currentblkReq.SVR_FROM_DATE" required />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.SVR_FROM_DATE.$invalid" style="color: red">Please select from date</span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSearchspc.$submitted && frmSearchspc.SVR_TO_DATE.$invalid}">
                                        <label class="control-label">To Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" style="width: 150px" id='todate'>
                                            <input type="text" id="SVR_TO_DATE" class="form-control" required="" placeholder="mm/dd/yyyy" name="SVR_TO_DATE" ng-model="currentblkReq.SVR_TO_DATE" />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmSearchspc.$submitted && frmSearchspc.SVR_TO_DATE.$invalid" style="color: red">Please select to date</span>
                                    </div>
                                </div>


                                <%--   <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div>
                                        </div>
                                    </div>--%>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="box-footer text-right">
                                <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color">Search Spaces</button>
                                <input type="button" id="btnNew" ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>
                    </form>

                    <form role="form" id="Form2" name="frmblkreq" data-valid-submit="RaiseRequest()" novalidate ng-show="Markers.length !=0">
                        <%--  <div class="tab-content">
                                    <div class="tab-pane fade in active" id="bu"></div>
                                    <div class="tab-pane fade" id="project" runat="server"></div>
                                </div>--%>


                        <div class="clearfix">
                            <div class="row">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div style="height: 325px;">
                                        <input class="form-control" id="filtertxt" placeholder="Filter..." type="text" style="width: 35%" />
                                        <div data-ag-grid="gridOptions" style="height: 90%;" class="ag-blue"></div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="panel-body">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li class="active"><a href="#bu" data-toggle="tab"><i class="flaticon-people"></i>
                                                    Allocate</a></li>
                                                <li><a href="#project" data-toggle="tab"><i class="flaticon-home"></i>
                                                    Block</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div id="bu" class="tab-pane fade in active modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmblkreq.$submitted && frmblkreq.SVR_VER_CODE.$invalid}">
                                                                <label class="control-label">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                                                <select name="SVR_VER_CODE" data-ng-model="currentblkReq.SVR_VER_CODE" class="selectpicker form-control" id="ddlvert" data-live-search="true" required="">
                                                                    <option value="" selected>--Select--</option>
                                                                    <option ng-repeat="Vert in Verticals" value="{{Vert.VER_CODE}}">{{Vert.VER_NAME}}</option>
                                                                </select>
                                                                <span class="error" data-ng-show="frmblkreq.$submitted && frmblkreq.SVR_VER_CODE.$invalid">Please select {{BsmDet.Parent |lowercase}} </span>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="box-footer text-right" style="padding-left: 16px">
                                                            <button type="submit" id="btnRequest" class="btn btn-primary custom-button-color pull-right text-right" ng-click="currentblkReq.ALLOCSTA = 4">Allocate to {{BsmDet.Parent}}</button>
                                                            <input type="button" id="btnviewinmap" ng-click="ViewinMap()" class="btn btn-primary custom-button-color pull-left" value="View In Map" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="project" class="tab-pane fade modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmblkreq.$submitted && frmblkreq.SVR_VER_CODE.$invalid}">
                                                                <label class="control-label">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                                                <select name="SVR_VER_CODE" data-ng-model="currentblkReq.SVR_VER_CODE" class="selectpicker form-control" id="ddlvertBlk" data-live-search="true" required="" ng-change="VerticalChange()">
                                                                    <option value="" selected>--Select--</option>
                                                                    <option ng-repeat="Vert in Verticals" value="{{Vert.VER_CODE}}">{{Vert.VER_NAME}}</option>
                                                                </select>
                                                                <span class="error" data-ng-show="frmblkreq.$submitted && frmblkreq.SVR_VER_CODE.$invalid">Please select {{BsmDet.Parent |lowercase}} </span>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group" data-ng-class="{'has-error': frmblkreq.$submitted && frmblkreq.SVR_COST_CODE.$invalid}">
                                                                <label class="control-label">{{BsmDet.Child}}</label>
                                                                <select name="SVR_COST_CODE" data-ng-model="currentblkReq.SVR_COST_CODE" class="form-control" id="ddlCostcenter" data-live-search="true">
                                                                    <option value="" selected>--Select--</option>
                                                                    <option ng-repeat="Costcenter in Costcenters | filter :  currentblkReq.SVR_VER_CODE" value="{{Costcenter.Cost_Center_Code}}">{{Costcenter.Cost_Center_Name}}</option>
                                                                </select>
                                                                <span class="error" data-ng-show="frmblkreq.$submitted && frmblkreq.SVR_COST_CODE.$invalid">Please select {{BsmDet.Child |lowercase}} </span>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="box-footer text-right" style="padding-left: 16px">
                                                            <button type="submit" id="btnBlock" class="btn btn-primary custom-button-color pull-right text-right" ng-click="currentblkReq.ALLOCSTA = 128">Block</button>
                                                            <input type="button" id="Button4" ng-click="ViewinMap()" class="btn btn-primary custom-button-color pull-left" value="View In Map" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--</form>-->
        </div>
    </div>


    <div class="slide_block">
        <div class="modal fade bs-example-modal-lg" id="historymodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="H1">Plot On the Map</h4>
                    </div>

                    <form role="form" name="form2" id="form3">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <div isteven-multi-select data-input-model="MapFloors" selection-mode="single" helper-elements="" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                            data-on-item-click="FlrSectMap(data)" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="button" id="Button1" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color" value="Proceed" />
                                    </div>
                                    <div class="col-md-4">
                                        Legends:
                                        <span class="label label-success pull-right">Available </span>
                                        <span class="label selectedseat pull-right">Selected Seats </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="main">
                                        <div id="leafletMap"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

        function setDateVals() {
            $('#SVR_FROM_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#SVR_TO_DATE').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        }

    </script>

    <script defer src="../../Utility.min.js"></script>
    <script defer src="../Js/SearchSpaces.js"></script>
    <script defer src="../Js/ViewVerticalRequisitionSM.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
</body>
</html>
