﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        span.error {
            color: red;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

         
    </style>
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.css" rel="stylesheet" />
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>

<body data-ng-controller="VerticalRequisitionController" class="amantra" onload="setDateVals()">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Vertical Requisition" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">{{BsmDet.Parent}} Requisition</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <div class="clearfix">
                        <div class="box-footer text-right">
                            <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                        </div>
                    </div>
                    <br />
                    <div>
                        <form id="Form1" name="frmVertReq" data-valid-submit="SearchSpaces()" novalidate>

                            <%--<label class="control-label"><span style="color: red;">&nbsp&nbsp**</span>&nbsp&nbsp{{Autoselect}}</label>--%>
                            <%--<div class="panel-body">--%>
                            <div class="row">
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.CNY_NAME.$invalid}">
                                            <label class="control-label">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Country" data-output-model="VertRequest.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                                data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VertRequest.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.CNY_NAME.$invalid">Please select country </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.CTY_NAME.$invalid}">
                                            <label class="control-label">City <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="City" data-output-model="VertRequest.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                                data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VertRequest.City[0]" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.CTY_NAME.$invalid">Please select city </span>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Location" data-output-model="VertRequest.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VertRequest.Location[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.LCM_NAME.$invalid">Please select location </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.TWR_NAME.$invalid}">
                                            <label class="control-label">Tower <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Tower" data-output-model="VertRequest.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                                data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VertRequest.Tower[0]" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.TWR_NAME.$invalid">Please select tower </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.FLR_NAME.$invalid}">
                                            <label class="control-label">Floor <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Floor" data-output-model="VertRequest.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()"
                                                data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="VertRequest.Floor[0]" name="FLR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.FLR_NAME.$invalid">Please select floor </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.SVR_FROM_DATE.$invalid}">
                                            <label class="control-label">From Date <span style="color: red;">*</span></label>
                                            <div class='input-group date' id='fromdate'>
                                                <input type="text" id="txtFrmDate" class="form-control" required="" placeholder="mm/dd/yyyy" name="SVR_FROM_DATE" ng-model="currentblkReq.SVR_FROM_DATE" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.SVR_FROM_DATE.$invalid" style="color: red">Please select from date </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmVertReq.$submitted && frmVertReq.SVR_TO_DATE.$invalid}">
                                            <label class="control-label">To Date <span style="color: red;">*</span></label>
                                            <div class='input-group date' id='todate'>
                                                <input type="text" id="txtTodate" class="form-control" required="" placeholder="mm/dd/yyyy" name="SVR_TO_DATE" ng-model="currentblkReq.SVR_TO_DATE" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmVertReq.$submitted && frmVertReq.SVR_TO_DATE.$invalid" style="color: red">Please select from date </span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color">Search Spaces</button>
                                    <input type="button" id="btnNew" ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </form>

                        <form role="form" id="Form2" name="frmblkreq" data-valid-submit="RaiseRequest()" novalidate ng-show="Markers.length !=0">
                            <div class="clearfix">
                                <div class="row">
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <div style="height: 325px;">
                                            <input id="filtertxt" class="form-control" placeholder="Filter..." type="text" style="width: 35%" />
                                            <div data-ag-grid="gridOptions" style="height: 90%;" class="ag-blue"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmblkreq.$submitted && frmblkreq.SVR_VER_CODE.$invalid}">
                                                <label class="control-label">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                                <select name="SVR_VER_CODE" data-ng-model="currentblkReq.SVR_VER_CODE" class="form-control selectpicker" data-live-search="true" id="ddlvert" required="">
                                                    <option value="" selected>--Select--</option>
                                                    <option ng-repeat="Vert in Verticals" value="{{Vert.VER_CODE}}">{{Vert.VER_NAME}}

                                                    </option>

                                                </select>
                                                <span class="error" data-ng-show="frmblkreq.$submitted && frmblkreq.SVR_VER_CODE.$invalid">Please select {{BsmDet.Parent}} </span>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmblkreq.$submitted && frmblkreq.TWR_NAME.$invalid}">
                                                <label class="control-label">Requestor Remarks <span style="color: red;"></span></label>
                                                <textarea cols="50" name="SVR_REQ_REM" data-ng-model="currentblkReq.SVR_REQ_REM" style="height: 20%" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="box-footer text-right">
                                            <button type="submit" id="btnRequest" class="btn btn-primary custom-button-color">Raise {{BsmDet.Parent}} Request</button>
                                            <input type="button" id="btnviewinmap" ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <!--</form>-->
            </div>
        </div>
    </div>

    <div class="slide_block">
        <div class="modal fade bs-example-modal-lg" id="historymodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="H1">Plot On the Map</h4>
                    </div>

                    <form role="form" name="form2" id="form3">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <div isteven-multi-select data-input-model="MapFloors" selection-mode="single" helper-elements="" data-output-model="Map.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                            data-on-item-click="FlrSectMap(data)" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="button" id="Button1" data-dismiss="modal" aria-hidden="true" class="btn btn-primary custom-button-color" value="Proceed" />
                                    </div>
                                    <div class="col-md-4">
                                        Legends:
                                        <span class="label label-success pull-right">Available </span>
                                        <span class="label selectedseat pull-right">Selected Seats </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="main">
                                        <div id="leafletMap"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        function setDateVals() {
            $('#txtFrmDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#txtTodate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        }
    </script>
    <script defer src="../../Utility.min.js"></script>
    <script defer src="../Js/VerticalRequisition.js"></script>
</body>
</html>
