﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    <%--<link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />--%>
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />


    <style>
        .grid-align
        {
            text-align: left;
        }

        a:hover
        {
            cursor: pointer;
        }

        .ag-cell
        {
            color: black;
        }

        .ag-header-cell-filtered
        {
            background-color: #4682B4;
        }

        .modal-header-primary
        {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word
        {
            color: #4813CA;
        }

        #pdf
        {
            color: #FF0023;
        }

        #excel
        {
            color: #2AE214;
        }

        .ag-header-cell-filtered
        {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button
        {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell
        {
            background-color: #1c2b36;
        }
    </style>

</head>
<body data-ng-controller="HistoryReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Area Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">History Report </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form2" name="HistoryReport" data-valid-submit="LoadData()" novalidate>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HistoryReport.$submitted && HistoryReport.CNY_NAME.$invalid}">
                                            <label class="control-label">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Country" data-output-model="HistoryRpt.Country" data-button-label="icon CNY_NAME"
                                                data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="HistoryRpt.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HistoryReport.$submitted && HistoryReport.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HistoryReport.$submitted && HistoryReport.CTY_NAME.$invalid}">
                                            <label class="control-label">City<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="City" data-output-model="HistoryRpt.City" data-button-label="icon CTY_NAME"
                                                data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="HistoryRpt.City[0]" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HistoryReport.$submitted && HistoryReport.CTY_NAME.$invalid" style="color: red">Please select city </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HistoryReport.$submitted && HistoryReport.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Locations" data-output-model="HistoryRpt.Locations" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="HistoryRpt.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HistoryReport.$submitted && HistoryReport.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HistoryReport.$submitted && HistoryReport.TWR_NAME.$invalid}">
                                            <label class="control-label">Tower <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Towers" data-output-model="HistoryRpt.Towers" data-button-label="icon TWR_NAME "
                                                data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="HistoryRpt.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HistoryReport.$submitted && HistoryReport.TWR_NAME.$invalid" style="color: red">Please select tower </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': HistoryReport.$submitted && HistoryReport.FLR_NAME.$invalid}">
                                            <label class="control-label">Floor <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Floors" data-output-model="HistoryRpt.Floors" data-button-label="icon FLR_NAME"
                                                data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="HistoryRpt.Floors[0]" name="FLR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HistoryReport.$submitted && HistoryReport.FLR_NAME.$invalid" style="color: red">Please select floor </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script defer src="../../Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="../../Dashboard/C3/c3.min.js"></script>
    <link href="../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script defer src="../../Scripts/jspdf.min.js"></script>
    <script defer src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script defer src="../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../Utility.js"></script>
    <script defer src="../HistoryReport.js"></script>
</body>

</html>
