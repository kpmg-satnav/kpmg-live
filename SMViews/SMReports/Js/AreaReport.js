﻿app.service("SpaceAreaReport", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetGridData = function (AreaRpt) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceAreaReport/GetAreaReportBindGrid', AreaRpt)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('SpaceAreaReportController', ['$scope', '$q', 'SpaceAreaReport', 'UtilityService', '$timeout', '$http', function ($scope, $q, SpaceAreaReport, UtilityService, $timeout, $http) {
    $scope.AreaReport = {};
    $scope.Viewstatus = 0;
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.EnableStatus = 0;
    $scope.CompanyVisible = 0;
    $scope.GridVisiblity = false;


    UtilityService.GetCompanies().then(function (response) {
        if (response.data != null) {
            $scope.Company = response.data;
            $scope.AreaReport.CNP_NAME = [];
            //$scope.AreaReport.CNP_NAME = parseInt(CompanySession);
            //console.log($scope.AreaReport.CNP_NAME);
            angular.forEach($scope.Company, function (value, key) {
                var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                if (a) {
                    a.ticked = true;
                    $scope.AreaReport.CNP_NAME.push(a);
                }
            });
            if (CompanySession == "1") { $scope.EnableStatus = 1; }
            else { $scope.EnableStatus = 0; }
        }
    });


    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        //console.log($scope.AreaReport.CNP_NAME);
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    CNP_NAME: $scope.AreaReport.CNP_NAME[0].CNP_ID
                };

                SpaceAreaReport.GetGridData(params).then(function (response) {
                    $scope.gridata = response.data;
                    if (response.data == null) {
                        $("#btNext").attr("disabled", true);
                        $scope.gridOptions.api.setRowData([]);
                        $scope.GridVisiblity = false;
                        progress(0, 'Loading...', false);
                        showNotification('error', 8, 'bottom-right', response.Message);

                    }
                    else {
                        $scope.GridVisiblity = true;
                        progress(0, 'Loading...', true);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        progress(0, 'Loading...', false);
                    }
                    $scope.AreaChartDetais();
                }, function (error) {
                    console.log(error);
                });
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    var columnDefs = [
        { headerName: "Child Entity", field: "CHE_CODE", width: 150, cellClass: 'grid-align', filter: 'set', suppressMenu: false },
        { headerName: "Country", field: "CNY_NAME", width: 150, cellClass: 'grid-align', filter: 'set', suppressMenu: true },
        { headerName: "City", field: "CTY_NAME", width: 200, cellClass: 'grid-align' },
        { headerName: "Location", field: "LCM_NAME", width: 150, cellClass: 'grid-align', },
        { headerName: "Tower", field: "TWR_NAME", width: 150, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Floor", field: "FLR_NAME", width: 150, cellClass: 'grid-align', },
        { headerName: "Floor Area", field: "FLR_AREA", width: 200, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Aisle Area", field: "AISLE_AREA", width: 200, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Common Area", field: "COMMON_AREA", width: 200, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Spaces", field: "TOTAL_SPACES", width: 80, cellClass: 'grid-align', suppressMenu: true }

    ];

    $scope.pageSize = '10';

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        rowData: null,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    //$scope.LoadData();

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
        } else {
            $scope.DocTypeVisible = 0;
        }
    }

    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    $("#Tabular").fadeIn();
    $("#table2").fadeIn();
    $("#Graphicaldiv").fadeOut();
    var chart;
    chart = c3.generate({
        data: {
            //x: 'x',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },

        },
        legend: {
            show: false,
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Location'],
                height: 130,
                show: true,
                //label: {
                //    text: 'Locations',
                //    position: 'outer-center'
                //}
            },
            y: {
                show: true,
                label: {
                    text: 'Area in Sq.Ft',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });

    function toggle(id) {
        chart.toggle(id);
    }

    d3.select('.container').insert('div', '.chart').attr('class', 'legend').selectAll('div').data({
        columns: [],
        cache: false,
        type: 'bar',
        empty: { label: { text: "Sorry, No Data Found" } },
        labels: true
    })
        .enter().append('div')
        .attr('data-id', function (id) {
            return id;
        })
        .html(function (id) {
            return id;
        })
        .each(function (id) {
            //d3.select(this).append('span').style
            d3.select(this).append('span').style('background-color', chart.color(id));
        })
        .on('mouseover', function (id) {
            chart.focus(id);
        })
        .on('mouseout', function (id) {
            chart.revert();
        })
        .on('click', function (id) {
            $(this).toggleClass("c3-legend-item-hidden")
            chart.toggle(id);
        });


    $scope.AreaChartDetais = function () {
        $http({
            url: UtilityService.path + '/api/SpaceAreaReport/GetAreaChartData/' + $scope.AreaReport.CNP_NAME[0].CNP_ID,
            method: 'POST',
            data: ''
        }).
            success(function (result) {
                chart.unload();
                chart.load({ columns: result });
            });
        setTimeout(function () {
            $("#SpcGraph").append(chart.element);
        }, 700);
    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" },
        { title: "Tower", key: "TWR_NAME" },
        { title: "Floor", key: "FLR_NAME" }, { title: "Floor Area", key: "FLR_AREA" },
        { title: "Aisle Area", key: "AISLE_AREA" },
        { title: "Common Area", key: "COMMON_AREA" }, { title: "Spaces", key: "TOTAL_SPACES" }
        ];
        progress(0, 'Loading...', true);
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A3');
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: 'auto' },
            columnStyles: { 0: { columnWidth: '15%' } }
        });
        doc.save("AreaReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        //var Filterparams = {
        //    skipHeader: false,
        //    skipFooters: false,
        //    skipGroups: false,
        //    allColumns: false,
        //    onlySelected: false,
        //    columnSeparator: ",",
        //    fileName: "AreaReport.csv"
        //}; $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        //setTimeout(function () {
        //    progress(0, 'Loading...', false);
        //}, 1000);
        var mapvalues = [];
        var columns = "";
        _.forEach($scope.gridOptions.api.columnController.allColumns, function (value) {

            columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

        });
        mapvalues.push(_.map($scope.gridOptions.api.inMemoryRowController.rowsAfterFilter, 'data'));
        alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("AreaReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (AreaReport, Type) {
        var searchval = $("#filtertxt").val();
        progress(0, 'Loading...', true);
        eaobj = {};
        eaobj.CNP_NAME = $scope.AreaReport.CNP_NAME[0].CNP_ID;
        eaobj.Type = Type;
        eaobj.SearchValue = searchval;
        eaobj.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        eaobj.PageSize = $scope.gridata[0].OVERALL_COUNT;
        if ($scope.HideColumns == 1 || $scope.HideColumns == 0) {
            progress(0, 'Loading...', true);

            if (Type == "pdf") {
                var columns = [];
                for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
                    var dt = ({ title: $scope.gridOptions.columnDefs[i].headerName, key: $scope.gridOptions.columnDefs[i].field, sno: $scope.gridOptions.columnDefs[i].sno });
                    columns.push(dt);
                }
                var sortorder = _.sortBy(columns, [function (o) { return o.sno; }]);
                var model = $scope.gridOptions.api.getModel();
                var data = [];
                model.forEachNodeAfterFilter(function (node) {
                    data.push(node.data);
                });
                var jsondata = JSON.parse(JSON.stringify(data));
                var doc = new jsPDF('p', 'pt', 'A3');
                doc.autoTable(sortorder, jsondata, {
                    startY: 43,
                    margin: { horizontal: 7 },
                    styles: { overflow: 'linebreak', columnWidth: 'auto' },
                    columnStyles: { 0: { columnWidth: '15%' } }
                });
                doc.save("AreaReport.pdf");
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);

            }
            else {
                console.log("hi");
                $scope.gridOptions.api.exportDataAsCsv($scope.gridOptions.columnDefs);
                progress(0, '', false);
            }

        }
        else if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/SpaceAreaReport/GetAreaReportdata',
                method: 'POST',
                data: eaobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'AreaReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

    setTimeout(function () {
        $scope.LoadData();
    }, 1000);

}]);