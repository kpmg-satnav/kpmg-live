﻿app.service("MultipleSeatingReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MultipleSeatingReport/GetMultipleSeatingDetails')
          .then(function (response) {
              console.log(response);
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('MultipleSeatingReportController', function ($scope, $q, $http, MultipleSeatingReportService, UtilityService, $timeout, $filter) {
    $scope.MultipleSeating = {};
    $scope.MultipleSeating.CNP_NAME = [];
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.EnableStatus = 0;
    $scope.CompanyVisible = 0;


    UtilityService.GetCompanies().then(function (response) {
        if (response.data != null) {
            $scope.Company = response.data;
            _.forEach($scope.Company, function (value, key) {            
                console.log(response);
                var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                a.ticked = true;
                $scope.MultipleSeating.CNP_NAME.push(a);
            });
            if (CompanySession == "1") { $scope.EnableStatus = 1; }
            else { $scope.EnableStatus = 0; }
        }
    });


    $scope.LoadData = function () {
        progress(0, 'Loading...', true);        
  
     

        MultipleSeatingReportService.GetGriddata().then(function (response) {
            console.log(response);
            ExportColumns = response.exportCols;
            $scope.Options.api.setColumnDefs(response.Coldef);
            $scope.gridata = response.griddata;

            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;              
                $scope.Options.api.setRowData([]);
            }
            else {
                $scope.GridVisiblity = true;
                $scope.Options.api.setRowData($scope.gridata);
                progress(0, 'Loading...', false);
            }

        })

    }, function (error) {
        console.log(error);
        progress(0, '', false);
    }



    setTimeout(function () {
        $scope.LoadData();
    }, 100);

    function onFilterChanged(value) {
        $scope.Options.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.Options = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupHideGroupColumns: true,
        angularCompileRows: true,

        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.Options.api.getFilterModel()))
                $scope.DocTypeVisible = 1;
            else
                $scope.DocTypeVisible = 0;
        },
        onReady: function () {
            $scope.Options.api.sizeColumnsToFit()
    }

        
    };
   

   
    $scope.GenerateReport = function (MultipleSeating, Type) {
        progress(0, 'Loading...', true);
        $scope.MultipleSeating.Type = Type;
        if ($scope.Options.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf1();
            }
            else {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    $scope.GenerateFilterExcel1();
                }
            }
        }
        else {
            if (Type == 'xls') {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    JSONToCSVConvertor($scope.gridata, "Multiple Seating Report", true, "MultipleSeating");
                }
                else {
                    JSONToCSVConvertor($scope.gridata, "Multiple Seating Report", true, "MultipleSeating");
                }
            }
            else if (Type == 'pdf') {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    $scope.GenerateFilterPdf1();
                }
                else {
                    $scope.GenerateFilterPdf1();
                }
            }
        };
        progress(0, '', false);
    }
    $scope.GenerateFilterPdf1 = function () {
        progress(0, 'Loading...', true);
        var columns = ExportColumns;
        var model = $scope.Options.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a3");
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: 'auto' },
            columnStyles: { 0: { columnWidth: '15%' } }
        });
        doc.save("MultipleSeating.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel1 = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "MultipleSeating.csv"
        };
        $scope.Options.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    //$scope.MultipleSeating.Request_Type = "All";
    //angular.forEach($scope.Cols, function (value, key) {
    //    value.ticked = true;
    //});

    $timeout($scope.LoadData, 100);
});
