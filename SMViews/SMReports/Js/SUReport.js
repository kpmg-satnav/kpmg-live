﻿app.service("SUReportService", function ($http, $q, UtilityService) {
    this.BindGrid = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SUReport/BindGrid')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.SpaceUtilChart = function (Chart) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SUReport/SpaceSUChart', Chart)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.SpaceTypes = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SUReport/GetSpaceTypes')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
});
app.controller('SUReportController', function ($scope, $q, SUReportService, UtilityService, $timeout, $http) {
    $scope.Viewstatus = 0;
    $scope.rptArea = {};
    $scope.GridVisiblity = true;
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.spacetype = [];
    $scope.SpaceUtil = {};
    $scope.TotalCount = [];

    SUReportService.SpaceTypes().then(function (response) {
        if (response != null) {
            $scope.spacetype = response;
        }
        else { progress("error", 'No SpaceTypes Found', true); }
    });

    $scope.BindGrid = function () {
        SUReportService.BindGrid().then(function (response) {
            $scope.TotalCount.push(response.data[0].WSCOUNT)
            $scope.TotalCount.push(response.data[0].WSODC_COUNT)
            $scope.TotalCount.push(response.data[0].WSUNRES_COUNT)
            $scope.TotalCount.push(response.data[0].WS_VACANT)
            $scope.TotalCount.push(response.data[0].WS_OCCUPIED_COUNT)
            $scope.TotalCount.push(response.data[0].CBCOUNT)
            $scope.TotalCount.push(response.data[0].CBODC_COUNT)
            $scope.TotalCount.push(response.data[0].CBUNRES_COUNT)
            $scope.TotalCount.push(response.data[0].CB_VACANT)
            $scope.TotalCount.push(response.data[0].CB_OCCUPIED_COUNT)
            progress(0, 'Loading...', true);
            $scope.gridata = response.data;
            if (response == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.gridOptions.api.setRowData($scope.gridata);
                onFloatingBottomCount(1);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
                $scope.SpaceUtilization("All");
            }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.showPopup = function () {
        $scope.SpaceUtil.spc_type
        $scope.SpaceUtilization($scope.SpaceUtil.spc_type);
    };
    var columnDefs = [
       { headerName: "Floor", field: "FLOORNAME", width: 150, cellClass: 'grid-align', pinned: 'left' },
       { headerName: "Work Station", field: "WORKSTATION", width: 150, cellClass: 'grid-align' },
       { headerName: "Work Station - ODC", field: "WSODC", width: 150, cellClass: 'grid-align' },
       { headerName: "Work Station - UnRestricted", field: "WSUNRES", width: 190, cellClass: 'grid-align' },
       { headerName: "Work Station - Occupied", field: "WS_OCCUPIED", width: 150, cellClass: 'grid-align' },
        { headerName: "WS/URES-Occupied", field: "URES_OCCUPIED", width: 150, cellClass: 'grid-align' },
        { headerName: "WS/ODC-Occupied", field: "ODC_OCCUPIED", width: 150, cellClass: 'grid-align' },
       { headerName: "Work Station - Vacant", field: "WS_VACANT_COUNT", width: 150, cellClass: 'grid-align' },
        { headerName: "WS/URES-Vacant", field: "WS_URES_VACANT", width: 150, cellClass: 'grid-align' },
         { headerName: "WS/ODC-Vacant", field: "WS_ODC_VACANT", width: 150, cellClass: 'grid-align' },
       { headerName: "Cabin", field: "CABIN", width: 150, cellClass: 'grid-align' },
       { headerName: "Cabin - ODC", field: "CBODC", width: 150, cellClass: 'grid-align' },
       { headerName: "Cabin - UnRestricted", field: "CBUNRES", width: 150, cellClass: 'grid-align' },
       { headerName: "Cabin - Occupied", field: "CB_OCCUPIED", width: 150, cellClass: 'grid-align' },
        { headerName: "CB/URES-Occupied", field: "CB_URES_OCCUPIED", width: 150, cellClass: 'grid-align' },
         { headerName: "CB/ODC-Occupied", field: "CB_ODC_OCCUPIED", width: 150, cellClass: 'grid-align' },
       { headerName: "Cabin - Vacant", field: "CABIN_VACANT", width: 150, cellClass: 'grid-align' },
        { headerName: "CB/URES-Vacant", field: "CB_URES_VACANT", width: 150, cellClass: 'grid-align' },
         { headerName: "CB/ODC-Vacant", field: "CB_ODC_VACANT", width: 150, cellClass: 'grid-align' },
       { headerName: "Space Utilitzation (%)", field: "SU", width: 130, cellClass: 'grid-align', suppressMenu: true }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        enableCellSelection: false,
        showGridFooter: true,
        showColumnFooter: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },
        floatingBottomRowData: []
    };

    function onFloatingBottomCount(footerRowsToFloat) {
        var count = Number(footerRowsToFloat);
        var rows = createData(count);
        $scope.gridOptions.api.setFloatingBottomRowData(rows);
    }

    function createData(count) {
        var result = [];
        for (var i = 0; i < count; i++) {
            result.push({
                FLOORNAME: ' Total ',
                WORKSTATION: sumbycolumn('WORKSTATION'),
                WSODC: sumbycolumn('WSODC'),
                WSUNRES: sumbycolumn('WSUNRES'),
                WS_OCCUPIED: sumbycolumn('WS_OCCUPIED'),
                URES_OCCUPIED: sumbycolumn('URES_OCCUPIED'),
                ODC_OCCUPIED: sumbycolumn('ODC_OCCUPIED'),
                WS_VACANT_COUNT: sumbycolumn('WS_VACANT_COUNT'),
                WS_URES_VACANT: sumbycolumn('WS_URES_VACANT'),
                WS_ODC_VACANT: sumbycolumn('WS_ODC_VACANT'),
                CABIN: sumbycolumn('CABIN'),
                CBODC: sumbycolumn('CBODC'),
                CBUNRES: sumbycolumn('CBUNRES'),
                CB_OCCUPIED: sumbycolumn('CB_OCCUPIED'),
                CB_URES_OCCUPIED: sumbycolumn('CB_URES_OCCUPIED'),
                CB_ODC_OCCUPIED: sumbycolumn('CB_ODC_OCCUPIED'),
                CABIN_VACANT: sumbycolumn('CABIN_VACANT'),
                CB_URES_VACANT: sumbycolumn('CB_URES_VACANT'),
                CB_ODC_VACANT: sumbycolumn('CB_ODC_VACANT'),

                SU: ((sumbycolumn('WS_OCCUPIED') / sumbycolumn('WORKSTATION')) * 100).toFixed(2),
            });
        }
        return result;
    }

    function sumbycolumn(property) {
        var i = $scope.gridata instanceof Array ? $scope.gridata.length : 0;
        var total = 0;
        while (i--) {
            total += parseInt($scope.gridata[i][property]);
        }
        return total;
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.BindGrid();


    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
                $("#SpaceTypeDropdown").fadeOut();
                $("#print").fadeOut();
                $("#count").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
                $("#SpaceTypeDropdown").fadeIn();
                $("#print").fadeIn();
                $("#count").fadeOut();
            });
        }
    });
    $("#Tabular").fadeIn();
    $("#table2").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $("#SpaceTypeDropdown").fadeOut();
    $("#print").fadeOut();
    $("#count").fadeIn();




    $scope.SpaceUtilization = function (data) {
        $scope.Columndata = [];
        var params = {
            SpaceType: data
        };
        SUReportService.SpaceUtilChart(params).then(function (response) {
            console.log(response);

            if (response.data != null) {



                var chart;
                chart = c3.generate({
                    data: {
                        x:'Floor',
                        columns: [],
                        type: 'bar',
                        empty: { label: { text: "Sorry, No Data Found" } },
                        labels: true
                    },
                    legend: {
                        show: true
                    },
                    axis: {
                        x: {
                            type: 'category',
                            categories: ['Occupied & Vacant'],
                            height: 130,
                            show: true,
                        },
                        y: {
                            show: true,
                            label: {
                                text: 'Count',
                                position: 'outer-middle'
                            },
                        }
                    },
                    width:
                    {
                        ratio: 0.5
                    }
                });

                $scope.Columndata.push(response.data.colData);
                angular.forEach(response.data.rowData, function (value, key) {
                    $scope.Columndata.push(value);
                });
                chart.unload();
                chart.load({ columns: $scope.Columndata });
                $scope.ColumnNames = response.data.Columnnames;

                setTimeout(function () {
                    $("#SUGraph").empty();
                    $("#SUGraph").append(chart.element);
                }, 700);

            }
        }, function (error) {
            console.log(error);
        });
    }
    $scope.GenReport = function (Type) {
        $scope.rptArea.DocType = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            progress(0, 'Loading...', true);
            if ($scope.rptArea.DocType == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            progress(0, 'Loading...', true);
            $http({
                url: UtilityService.path + '/api/SUReport/SUDataReport',
                method: 'POST',
                data: $scope.rptArea,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Space Utilization Report.' + Type;
                document.body.appendChild(a);
                a.click();
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }).error(function (data, status, headers, config) {

            });
        }
    };

    $scope.SpaceUtil.spc_type = "All"
});