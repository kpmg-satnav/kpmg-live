﻿app.service("SpaceMismatchReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (Mismatch) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceMismatchReport/GetMismatchDetails', Mismatch)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('SpaceMismatchReportController', function ($scope, $q, $http, SpaceMismatchReportService, UtilityService, $timeout, $filter) {
    $scope.Mismatch = {};
    $scope.Mismatch.CNP_NAME = [];
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];   
    $scope.EnableStatus = 0;
    $scope.CompanyVisible = 0;


    



    UtilityService.GetCompanies().then(function (response) {
        if (response.data != null) {
            $scope.Company = response.data;
            //$scope.Mismatch.CNP_NAME = parseInt(CompanySession);
            angular.forEach($scope.Company, function (value, key) {
                var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                a.ticked = true;
                $scope.Mismatch.CNP_NAME.push(a);
            });
            if (CompanySession == "1") { $scope.EnableStatus = 1; }
            else { $scope.EnableStatus = 0; }
        }

    });

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        console.log($scope.Mismatch);

        var dataobj = {
            flrlst: $scope.Mismatch.Floors,
            Request_Type: $scope.Mismatch.Request_Type,           
            Type: $scope.flag
        };
        console.log(dataobj);
        SpaceMismatchReportService.GetGriddata(dataobj).then(function (data) {
            $('#divbtnPart2').show();
            $scope.gridata = data.data;
            $scope.flag = "FALSE";
            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;
                $scope.gridOptions.api.setRowData([]);

                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', data.Message);
            }
            else {

                $scope.GridVisiblity = true;            
                $scope.gridOptions.api.setRowData([]);
                $scope.gridOptions.api.setRowData($scope.gridata);

                progress(0, '', false);
            }

        });

    }, function (error) {
        console.log(error);
        progress(0, '', false);
    }



                     setTimeout(function () {
                                                    $scope.LoadData();
                                                }, 100);                                           
                                        
                                    
                          

    $scope.Cols = [
        { COL: "Employee ID", value: "AUR_ID", ticked: false },
        { COL: "Employee Name", value: "AUR_KNOWN_AS", ticked: false },
        { COL: "HRMS Location", value: "HRIS_LOCATION", ticked: false },
        { COL: "Employee Allocated Location", value: "ALLOC_LOCATION", ticked: false },
        

    ];

    $scope.columnDefs = [
             { headerName: "Employee ID", field: "AUR_ID",  cellClass: 'grid-align' ,sno:1},
             { headerName: "Employee Name", field: "AUR_KNOWN_AS", cellClass: 'grid-align' ,sno:2},
             { headerName: "HRMS Location", field: "HRIS_LOCATION", cellClass: 'grid-align',sno:3 },
             { headerName: "Employee Allocated Location", field: "ALLOC_LOCATION", cellClass: 'grid-align',sno:4 },
             

    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,       
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,        
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };
    var dataSource = {
        //rowCount: ???, - not setting the row count, infinite paging will be used
        getRows: function (params) {
            // this code should contact the server for rows. however for the purposes of the demo,
            // the data is generated locally, a timer is used to give the experience of
            // an asynchronous call
            setTimeout(function () {
                var dataobj = {
                    flrlst: $scope.Mismatch.Floors,
                    Request_Type: $scope.Mismatch.Request_Type,
                    PageNum: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: $scope.gridOptions.paginationPageSize,
                    Type: $scope.flag
                };
                console.log(dataobj);
                SpaceMismatchReportService.GetGriddata(dataobj).then(function (data) {
                    $('#divbtnPart2').show();
                    $scope.gridata = data.data;
                    $scope.flag = "FALSE";
                    if ($scope.gridata == null) {
                        $scope.GridVisiblity = false;
                        $scope.gridOptions.api.setRowData([]);

                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', data.Message);
                    }
                    else {

                        $scope.GridVisiblity = true;                    
                        $scope.gridOptions.api.refreshHeader();
                        $scope.gridOptions.api.setRowData([]);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        setTimeout(function () {
                            console.log($scope.gridOptions.api.grid.paginationController.currentPage);
                        }, 200);
                        progress(0, '', false);
                    }

                });
            }, 500);
        }
    };

    function groupAggFunction(rows) {
        var sums = {
            AREA: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.AREA += parseFloat((data.AREA).toFixed(2));
        });
        return sums;
    }

    $scope.CustmPageLoad = function () {


    }, function (error) {
        console.log(error);
    }
    $scope.gridOptions.onColumnVisible = function (event) {
        if (event.visible) {
            $scope.HideColumns = 0;
            //console.log(event.column.colId + ' was made visible');     

            $scope.gridOptions.columnDefs.push(
           {
               headerName: event.column.colDef.headerName,
               field: event.column.colId,
               cellClass: "grid-align",
               width: 110,
               sno: event.column.colDef.sno,
               suppressMenu: true
           }
           );
            $scope.gridOptions.columnDefs.visible = true;
            _.sortBy($scope.gridOptions.columnDefs, [function (o) { return o.sno; }]);
        } else {
            //console.log($scope.gridOptions.columnDefs.length);
            $scope.HideColumns = 1;
            for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
                if ($scope.gridOptions.columnDefs[i].field === event.column.colId) {
                    $scope.gridOptions.columnDefs[i].visible = false;
                    $scope.gridOptions.columnDefs.splice(i, 1);
                }
            }
        }
    };
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Employee ID", key: "AUR_ID" }, { title: "Employee Name", key: "AUR_KNOWN_AS" },
            { title: "HRMS Location", key: "HRIS_LOCATION" }, { title: "Employee Allocated Location", key: "ALLOC_LOCATION" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: 'auto' },
            columnStyles: { 0: { columnWidth: '15%' } }
        });
        doc.save("LocationMismatchReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        //var Filterparams = {
        //    skipHeader: false,
        //    skipFooters: false,
        //    skipGroups: false,
        //    allColumns: false,
        //    onlySelected: false,
        //    columnSeparator: ',',
        //    fileName: "SpaceMismatchReport.csv"
        //};
        //$scope.gridOptions.api.exportDataAsCsv(Filterparams);
        //setTimeout(function () {
        //    progress(0, 'Loading...', false);
        //}, 1000);
        var mapvalues = [];
        var columns = "";
        _.forEach($scope.gridOptions.api.columnController.allColumns, function (value) {

            columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

        });
        mapvalues.push(_.map($scope.gridOptions.api.inMemoryRowController.rowsAfterFilter, 'data'));
        alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("LocationMismatchReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Mismatch, Type) {

        progress(0, 'Loading...', true);
        eaobj = {};
        eaobj.CNP_NAME = $scope.Mismatch.CNP_NAME[0].CNP_ID;
        eaobj.Type = Type;
       

        //Mismatch.Type = Type;
        //Mismatch.flrlst = Mismatch.Floors;
        if ($scope.HideColumns == 1 || $scope.HideColumns == 0) {
            progress(0, 'Loading...', true);

            var columns = [];
            var col = [];
            var mapvalues = [];
            for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
                var dt = ({ title: $scope.gridOptions.columnDefs[i].headerName, key: $scope.gridOptions.columnDefs[i].field, sno: $scope.gridOptions.columnDefs[i].sno });
                columns.push(dt);
            }
            var sortorder = _.sortBy(columns, [function (o) { return o.sno; }]);
            var model = $scope.gridOptions.api.getModel();
            var data = [];
            model.forEachNodeAfterFilter(function (node) {
                data.push(node.data);
            });
            if (Type == "pdf") {
               
                var jsondata = JSON.parse(JSON.stringify(data));
                var doc = new jsPDF('p', 'pt', 'A3');
                doc.autoTable(sortorder, jsondata, {
                    startY: 43,
                    margin: { horizontal: 7 },
                    styles: { overflow: 'linebreak', columnWidth: 'auto' },
                    columnStyles: { 0: { columnWidth: '15%' } }
                });
                doc.save("LocationMismatchReport.pdf");
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);

            }
            else {
                mapvalues.push(data);
                _.forEach($scope.gridOptions.columnDefs, function (value) {
                    col = col + value.field + " as [" + value.headerName + "],"
                });
                alasql('SELECT ' + col.slice(0, -1) + ' INTO XLSX("LocationMismatchReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }

        }
       else  if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                //url: UtilityService.path + '/api/MismatchReport/GetMismatchData',
                url: UtilityService.path + '/api/SpaceMismatchReport/GetMismatchDatadwn',
                method: 'POST',
                data: eaobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpaceMismatchReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

    //$scope.Mismatch.Request_Type = "All";
    //angular.forEach($scope.Cols, function (value, key) {
    //    value.ticked = true;
    //});

    
});
