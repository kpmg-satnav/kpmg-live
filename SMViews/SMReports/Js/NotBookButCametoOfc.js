﻿app.service("NotBookButCametoOfcService", function ($http, $q, UtilityService) {

});

app.controller('NotBookButCametoOfcController', ['$scope', '$q', 'UtilityService', 'NotBookButCametoOfcService', '$filter', function ($scope, $q, UtilityService, NotBookButCametoOfcService, $filter) {
    $scope.NotBookButCametoOfc = {};
    $scope.Country = [];
    $scope.City = [];
    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Floors = [];

    $scope.dates = function () {
        $scope.NotBookButCametoOfc.FromDate = moment().format('MM/DD/YYYY');
        $scope.NotBookButCametoOfc.ToDate = moment().format('MM/DD/YYYY');
    }

    setTimeout(function () {
        $scope.dates();
    }, 500);


    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                });
                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;
                        angular.forEach($scope.City, function (value, key) {
                            value.ticked = true;
                        });
                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                                angular.forEach($scope.Locations, function (value, key) {
                                    value.ticked = true;
                                });
                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;
                                        angular.forEach($scope.Towers, function (value, key) {
                                            value.ticked = true;
                                        });
                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                angular.forEach($scope.Floors, function (value, key) {
                                                    value.ticked = true;
                                                })
                                                setTimeout(function () {
                                                    $scope.dates();
                                                    $scope.LoadData();
                                                }, 500);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    $scope.Pageload();

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.NotBookButCametoOfc.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.NotBookButCametoOfc.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.NotBookButCametoOfc.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.NotBookButCametoOfc.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.NotBookButCametoOfc.City = $scope.City;
        $scope.getLocationsByCity();
    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.NotBookButCametoOfc.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.NotBookButCametoOfc.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.NotBookButCametoOfc.City[0] = cty;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.NotBookButCametoOfc.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.NotBookButCametoOfc.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.NotBookButCametoOfc.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.NotBookButCametoOfc.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.NotBookButCametoOfc.Locations[0] = lcm;
            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.NotBookButCametoOfc.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.FloorChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.NotBookButCametoOfc.Country[0] = cny;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.NotBookButCametoOfc.City[0] = cty;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.NotBookButCametoOfc.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.NotBookButCametoOfc.Towers[0] = twr;
            }
        });

    }
    $scope.floorChangeAll = function () {
        $scope.NotBookButCametoOfc.Floors = $scope.Floors;
        $scope.FloorChange();
    }
    $scope.dataObj = {};

    $scope.LoadData = function () {
        setTimeout(function () {
            $scope.fetchdata();
        }, 500);
    }

    $scope.fetchdata = function () {
        var Locations = _.filter($scope.NotBookButCametoOfc.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(',');
       
        $.ajax({
            url: '../../../api/AddHerence/NotBookButCametoOfc',
            type: 'post',
            data: {
                Locations: Locations,
                FromDateNb: $scope.NotBookButCametoOfc.FromDate,
                ToDateNb: $scope.NotBookButCametoOfc.ToDate
            },
            success: function (data) {
                $scope.gridOptions.api.setRowData(data);
                $scope.TotalReq = $scope.gridOptions.api.inMemoryRowController.rowsAfterFilter.length;

            }
        });
    };
    var columnDefs = [
        { headerName: "Location", field: "LCM_NAME", width: 180, cellClass: 'grid-align', },
        { headerName: "Employee Id", field: "AUR_ID", width: 180, cellClass: 'grid-align', },
        { headerName: "Employee Name", field: "AUR_KNOWN_AS", width: 180, cellClass: 'grid-align', },
        { headerName: "Team Name", field: "VERTICAL", width: 160, cellClass: 'grid-align', },
        { headerName: "Department", field: "COSTCENTER", width: 160, cellClass: 'grid-align', },
        { headerName: "Visited Date", field: "Visited_Date", template: '<span>{{data.Visited_Date | date:"dd/MM/yyyy"}}</span>', width: 160, cellClass: 'grid-align', }
    ]

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        angularCompileRows: true,
        rowData: null,

        onAfterFilterChanged: function () {
            $scope.TotalReq = $scope.gridOptions.api.inMemoryRowController.rowsAfterFilter.length;
        }
    }

    $("#anchExportExcel").click(function (event) {
        if ($scope.usable == false) {
            event.preventDefault();
        } else {
            progress(0, 'Downloading Please Wait...', true);
            var Filterparams = {
                skipHeader: false,
                skipFooters: false,
                skipGroups: false,
                allColumns: false,
                onlySelected: false,
                columnSeparator: ",",
                fileName: "Daily Attendance Report.csv"
            };
            //$scope.gridOptions.api.exportDataAsCsv(Filterparams);

            var csvString = $scope.gridOptions.api.getDataAsCsv(Filterparams);
            var fileNamePresent = Filterparams && Filterparams.fileName && Filterparams.fileName.length !== 0;
            var fileName = fileNamePresent ? Filterparams.fileName : 'export.csv';
            var blobObject = new Blob([csvString], {
                type: "text/csv;charset=utf-8;"
            });
            // Internet Explorer
            if (window.navigator.msSaveOrOpenBlob) {
                var fileData = [csvString];
                var blobObject = new Blob(fileData);
                window.navigator.msSaveOrOpenBlob(blobObject, fileName);
            } else {
                // Chrome
                var url = "data:text/plain;charset=utf-8," + encodeURIComponent(csvString);
                var downloadLink = document.createElement("a");
                downloadLink.href = url;
                downloadLink.href = window.URL.createObjectURL(blobObject);
                (downloadLink).download = fileName;

                document.body.appendChild(downloadLink);
                downloadLink.click();


                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }
        }

    });


    $scope.GenReport = function (RptByCategory, Type) {
        progress(0, 'Loading...', true);
        if (Type == "pdf") {
            $scope.GenerateFilterPdf();
        }
        else {
            $scope.GenerateFilterExcel();
        }

    }

    $("#filtertxt").change(function () {
        onUpdateFilterChanged($(this).val());
    }).keydown(function () {
        onUpdateFilterChanged($(this).val());
    }).keyup(function () {
        onUpdateFilterChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFilterChanged($(this).val());
    })
    function onUpdateFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

}]);