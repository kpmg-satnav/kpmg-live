﻿app.service("SpaceAvailabilityReportService", ['$http', '$q', 'UtilityService',
    function ($http, $q, UtilityService) {
        this.GetGriddata = function (SpaceAvailability) {
            deferred = $q.defer();
            return $http.post(UtilityService.path + '/api/SpaceAvailabilityReport/GetAvlGrid', SpaceAvailability)
                .then(function (response) {
                    deferred.resolve(response.data);
                    return deferred.promise;
                }, function (response) {
                    deferred.reject(response);
                    return deferred.promise;
                });
        };
        this.GetDetailsOnSelection = function (data) {
            deferred = $q.defer();
            return $http.post(UtilityService.path + '/api/SpaceAvailabilityReport/GetDetailsOnSelection', data)
                .then(function (response) {
                    deferred.resolve(response.data);
                    return deferred.promise;
                }, function (response) {
                    deferred.reject(response);
                    return deferred.promise;
                });
        };


        this.GetSearchData = function (data) {
            deferred = $q.defer();
            return $http.post(UtilityService.path + '/api/SpaceAvailabilityReport/GetSearchData', data)
                .then(function (response) {
                    deferred.resolve(response.data);
                    return deferred.promise;
                }, function (response) {
                    deferred.reject(response);
                    return deferred.promise;
                });
        };


    }]);

app.controller('SpaceAvailabilityReportController', ['$scope', '$q', '$http', 'SpaceAvailabilityReportService', 'UtilityService', '$timeout',
    function ($scope, $q, $http, SpaceAvailabilityReportService, UtilityService, $timeout) {
        $scope.SpaceAvailability = {};
        $scope.Avldata = {};
        $scope.Type = [];
        $scope.SelValue = [];
        $scope.ActionStatus = 0;
        $scope.DocTypeVisible = 0;
        $scope.PopDocTypeVisible = 0;
        $scope.CompanyVisible = 0;
        $scope.EnableStatus = 0;
        $scope.Company = [];

        setTimeout(function () {
            UtilityService.GetCompanies().then(function (response) {
                if (response.data != null) {
                    $scope.Company = response.data;
                    $scope.SpaceAvailability.CNP_NAME = parseInt(CompanySession);
                    angular.forEach($scope.Company, function (value, key) {
                        var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                        a.ticked = true;
                    });
                    if (CompanySession == "1") { $scope.EnableStatus = 1; }
                    else { $scope.EnableStatus = 0; }
                }

            });
        }, 500);

        $scope.columnDefs = [
            //{ headerName: "Child Entity", field: "CHE_CODE", width: 190, cellClass: 'grid-align', width: 110, sno: 1 },
            { headerName: "Country", field: "CNY_NAME", width: 190, cellClass: 'grid-align', width: 110, suppressMenu: true, sno: 1 },
            { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 110, sno: 2 },
            { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 160, sno: 3 },
            { headerName: "Location Code", field: "LCM_CODE", cellClass: 'grid-align', width: 160, sno: 4 },
            { headerName: "Tower", field: "TWR_NAME", cellClass: 'grid-align', width: 110, suppressMenu: true, sno: 5 },
            { headerName: "Floor", field: "FLR_NAME", cellClass: 'grid-align', width: 110, sno: 6 },
            { headerName: "Space Type", field: "SPC_TYPE", cellClass: 'grid-align', width: 110, sno: 7 },
            { headerName: "Space Sub Type", field: "SPACE_SUB_TYPE", cellClass: 'grid-align', width: 150, sno: 8 },
            //{ headerName: "Space Sub Type Code", field: "SPACE_SUB_TYPE_CODE", cellClass: 'grid-align', width: 150, sno: 9 },
            { headerName: "Total Seats", field: "TOTAL_SEATS_COUNT", cellClass: 'grid-align', width: 80, suppressMenu: true, pinned: 'right', sno: 10 },
            { headerName: "Vacant Seats", field: "VACANT_SEATS", cellClass: 'grid-align', width: 100, filter: 'set', template: '<a ng-click="ShowPopup(data)">{{data.VACANT_SEATS}}</a>', suppressMenu: true, pinned: 'right', sno: 11 }];

        var rowData = [];


        $scope.LoadData = function () {
            $("#btLast").hide();
            $("#btFirst").hide();
            var dataSource = {
                rowCount: null,
                getRows: function (params) {
                    var searchval = $("#filtertxt").val();
                    setTimeout(function () {
                        var lastRow = -1;
                        var dataObj = {
                            FromDate: $scope.SpaceAvailability.FromDate,
                            ToDate: $scope.SpaceAvailability.ToDate,
                            CNP_NAME: $scope.SpaceAvailability.CNP_NAME[0].CNP_ID,
                            PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                            PageSize: 10,
                            Type: $scope.flag,
                            SearchValue: searchval
                        };
                        if (searchval.trim() == "") {

                            SpaceAvailabilityReportService.GetGriddata(dataObj).then(function (data) {
                                $scope.gridata = data;
                                if ($scope.gridata == null) {
                                    $scope.gridOptions.api.setRowData([]);
                                    progress(0, 'Loading...', false);
                                }
                                else {
                                    $scope.gridOptions.api.setRowData($scope.gridata);
                                    setTimeout(function () {
                                        progress(0, 'Loading...', false);
                                    }, 600);

                                }
                                $scope.SpaceAvalChart();
                            }, function (error) {
                                console.log(error);
                            });

                        }
                        else {
                            SpaceAvailabilityReportService.GetSearchData(dataObj).then(function (response) {
                                if (response != null) {
                                    //   $scope.GridVisiblity2 = true;
                                    params.successCallback(response, lastRow);
                                }
                                else {
                                    params.failCallback();
                                    $("#btNext").attr("disabled", true);
                                    //   $scope.GridVisiblity2 = false;
                                    params.successCallback('', lastRow);
                                }
                                $scope.gridOptions.api.setRowData(response);
                            });
                        }


                    }, 1);

                }
            }
            $scope.gridOptions.api.setDatasource(dataSource);
        };
        //$scope.LoadData = function () {
        //    progress(0, 'Loading...', true);
        //    var params = {
        //        FromDate: $scope.SpaceAvailability.FromDate,
        //        ToDate: $scope.SpaceAvailability.ToDate,
        //        CNP_NAME: $scope.SpaceAvailability.CNP_NAME[0].CNP_ID
        //    };
        //    SpaceAvailabilityReportService.GetGriddata(params).then(function (data) {
        //        $scope.gridata = data;
        //        if ($scope.gridata == null) {
        //            $scope.gridOptions.api.setRowData([]);
        //                progress(0, 'Loading...', false);
        //        }
        //        else {
        //            $scope.gridOptions.api.setRowData($scope.gridata);
        //            setTimeout(function () {
        //                progress(0, 'Loading...', false);
        //            }, 600);

        //        }
        //        $scope.SpaceAvalChart();
        //    }, function (error) {
        //        console.log(error);
        //    });
        //}



        function onFilterChanged(value) {
            $scope.gridOptions.api.setQuickFilter(value);
            if (value) {
                $scope.DocTypeVisible = 1;
            } else {
                $scope.DocTypeVisible = 0;
            }

        }

        //$("#filtertxt").change(function () {
        //    onFilterChanged($(this).val());
        //}).keydown(function () {
        //    onFilterChanged($(this).val());
        //}).keyup(function () {
        //    onFilterChanged($(this).val());
        //}).bind('paste', function () {
        //    onFilterChanged($(this).val());
        //})

        $scope.gridOptions = {
            columnDefs: $scope.columnDefs,
            enableFilter: true,
            angularCompileRows: true,
            enableCellSelection: false,
            rowData: rowData,
            enableColResize: true,
            onAfterFilterChanged: function () {
                if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                    $scope.DocTypeVisible = 0;
                else
                    $scope.DocTypeVisible = 1;
            },
            showToolPanel: true

        };
        var PopDefs = [
            { headerName: "Country", field: "CNY_NAME", width: 190, cellClass: 'grid-align', width: 110 },
            { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 110 },
            { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 200 },
            { headerName: "Tower", field: "TWR_NAME", cellClass: 'grid-align', width: 150 },
            { headerName: "Floor", field: "FLR_NAME", cellClass: 'grid-align', width: 110 },
            { headerName: "Space Id", field: "SPC_ID", width: 190, cellClass: 'grid-align' },

        ];

        $scope.PopOptions = {
            columnDefs: PopDefs,
            enableFilter: true,
            angularCompileRows: true,
            enableCellSelection: false,
            rowData: null,
            enableColResize: true,
            onReady: function () {
                $scope.PopOptions.api.sizeColumnsToFit()
            },
            onAfterFilterChanged: function () {
                if (angular.equals({}, $scope.PopOptions.api.getFilterModel()))
                    $scope.PopDocTypeVisible = 0;
                else
                    $scope.PopDocTypeVisible = 1;
            }
        }
        $scope.ShowPopup = function (data) {
            var CompanyId = { CNP_NAME: $scope.SpaceAvailability.CNP_NAME[0].CNP_ID };
            $scope.SelValue = data;
            $scope.SelValue.CNP_NAME = CompanyId.CNP_NAME;
            $scope.CurrentProfile = [];
            $("#historymodal").modal('show');
        }
        $('#historymodal').on('shown.bs.modal', function () {
            SpaceAvailabilityReportService.GetDetailsOnSelection($scope.SelValue).then(function (response) {
                progress(0, 'Loading...', true);
                $scope.popdata = response.data;

                $scope.PopOptions.api.setRowData($scope.popdata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 500);

            });
        });

        $("#Tabular").fadeIn();

        $("#Graphicaldiv").fadeOut();
        $("#table2").fadeIn();
        var chart;
        chart = c3.generate({
            data: {
                columns: [],
                cache: false,
                type: 'bar',
                empty: { label: { text: "Sorry, No Data Found" } },
                labels: true
            },
            legend: {
                show: false,
            },
            axis: {
                x: {
                    type: 'category',
                    categories: ['Location'],
                    height: 130,
                    show: true,
                },
                y: {
                    show: true,
                    label: {
                        text: 'Vacant Seats Count',
                        position: 'outer-middle'
                    }
                }
            },

            width:
            {
                ratio: 0.5
            }
        });

        function toggle(id) {
            chart.toggle(id);
        }

        d3.select('.container').insert('div', '.chart').attr('class', 'legend').selectAll('div').data({
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
            labels: true
        })
            .enter().append('div')
            .attr('data-id', function (id) {
                return id;
            })
            .html(function (id) {
                return id;
            })
            .each(function (id) {
                //d3.select(this).append('span').style
                d3.select(this).append('span').style('background-color', chart.color(id));
            })
            .on('mouseover', function (id) {
                chart.focus(id);
            })
            .on('mouseout', function (id) {
                chart.revert();
            })
            .on('click', function (id) {
                $(this).toggleClass("c3-legend-item-hidden")
                chart.toggle(id);
            });

        $scope.SpaceAvalChart = function () {
            var chartparams = {
                CNP_NAME: $scope.SpaceAvailability.CNP_NAME[0].CNP_ID
            };
            $http({
                url: UtilityService.path + '/api/SpaceAvailabilityReport/GetSpaceAvailabilityChartData',
                method: 'POST',
                data: chartparams
            }).success(function (result) {
                chart.unload();
                chart.load({ columns: result });
            });
            setTimeout(function () {
                $("#AvalGraph").append(chart.element);
            }, 700);
        }
        $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
            if (state) {
                $("#Graphicaldiv").fadeOut(function () {
                    $("#Tabular").fadeIn();
                    $("#table2").fadeIn();
                });
            }
            else {
                $("#Tabular").fadeOut(function () {
                    $("#Graphicaldiv").fadeIn();
                    $("#table2").fadeOut();
                });
            }
        });


        $scope.GenerateFilterPdf = function () {
            progress(0, 'Loading...', true);
            var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" },
            { title: "Tower", key: "TWR_NAME" }, { title: "Location Code", key: "LCM_CODE" }, { title: "Space Sub Type Code", key: "SPACE_SUB_TYPE_CODE" },
            { title: "Floor", key: "FLR_NAME" }, { title: "Space Type", key: "SPC_TYPE" }, { title: "Space Sub Type", key: "SPACE_SUB_TYPE" },
            { title: "Total Seats", key: "TOTAL_SEATS_COUNT" }, { title: "Vacant Seats", key: "VACANT_SEATS" }];
            var model = $scope.gridOptions.api.getModel();
            var data = [];
            model.forEachNodeAfterFilter(function (node) {
                data.push(node.data);
            });
            var jsondata = JSON.parse(JSON.stringify(data)); 0
            var doc = new jsPDF('p', 'pt', 'A3');
            doc.autoTable(columns, jsondata, {
                startY: 43,
                margin: { horizontal: 7 },
                styles: { overflow: 'linebreak', columnWidth: 'auto' },
                columnStyles: { 0: { columnWidth: '15%' } }
            });
            doc.save("SpaceAvailabilityReport.pdf");
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }
        $scope.gridOptions.onColumnVisible = function (event) {
            if (event.visible) {
                $scope.HideColumns = 0;
                //console.log(event.column.colId + ' was made visible');     

                $scope.gridOptions.columnDefs.push(
                    {
                        headerName: event.column.colDef.headerName,
                        field: event.column.colId,
                        cellClass: "grid-align",
                        width: 110,
                        sno: event.column.colDef.sno,
                        suppressMenu: true
                    }
                );
                $scope.gridOptions.columnDefs.visible = true;
                _.sortBy($scope.gridOptions.columnDefs, [function (o) { return o.sno; }]);
            } else {
                //console.log($scope.gridOptions.columnDefs.length);
                $scope.HideColumns = 1;
                for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
                    if ($scope.gridOptions.columnDefs[i].field === event.column.colId) {
                        $scope.gridOptions.columnDefs[i].visible = false;
                        $scope.gridOptions.columnDefs.splice(i, 1);
                    }
                }
            }
        };
        $scope.GenerateFilterExcel = function () {
            progress(0, 'Loading...', true);
            //var Filterparams = {
            //    skipHeader: false,
            //    skipFooters: false,
            //    skipGroups: false,
            //    allColumns: false,
            //    onlySelected: false,
            //    columnSeparator: ",",
            //    fileName: "SpaceAvailabilityReport.csv"
            //};
            var mapvalues = [];
            var columns = "";
            _.forEach($scope.gridOptions.api.columnController.allColumns, function (value) {

                columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

            });
            mapvalues.push(_.map($scope.gridOptions.api.inMemoryRowController.rowsAfterFilter, 'data'));
            alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("SpaceAvailabilityReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }

        document.addEventListener("DOMContentLoaded", function () {
            var eGridDiv = document.getElementById('myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);
        });



        $scope.GenReport = function (Avldata, Type) {
           
            progress(0, 'Loading...', true);
            var chartparams = {
                CNP_NAME: $scope.SpaceAvailability.CNP_NAME[0].CNP_ID,
                Type: Type
            };
            // if ($scope.HideColumns == 1 || $scope.HideColumns == 0) {
            //     progress(0, 'Loading...', true);
            //     var columns = [];
            //     var col = [];
            //     var mapvalues = [];
            //     for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
            //         var dt = ({ title: $scope.gridOptions.columnDefs[i].headerName, key: $scope.gridOptions.columnDefs[i].field, sno: $scope.gridOptions.columnDefs[i].sno });
            //         columns.push(dt);
            //     }
            //     var sortorder = _.sortBy(columns, [function (o) { return o.sno; }]);
            //     var model = $scope.gridOptions.api.getModel();
            //     var data = [];
            //     model.forEachNodeAfterFilter(function (node) {
            //         data.push(node.data);
            //     });
            //     if (Type == "pdf") {

            //         var jsondata = JSON.parse(JSON.stringify(data));
            //         var doc = new jsPDF('p', 'pt', 'A3');
            //         doc.autoTable(sortorder, jsondata, {
            //             startY: 43,
            //             margin: { horizontal: 7 },
            //             styles: { overflow: 'linebreak', columnWidth: 'auto' },
            //             columnStyles: { 0: { columnWidth: '15%' } }
            //         });
            //         doc.save("SpaceAvailabilityReport.pdf");
            //         setTimeout(function () {
            //             progress(0, 'Loading...', false);
            //         }, 1000);

            //     }
            //     else {
            //         mapvalues.push(data);
            //         _.forEach($scope.gridOptions.columnDefs, function (value) {
            //             col = col + value.field + " as [" + value.headerName + "],"
            //         });
            //         alasql('SELECT ' + col.slice(0, -1) + ' INTO XLSX("SpaceAvailabilityReport.xlsx",{headers:true}) \
            //             FROM ?', JSON.parse(JSON.stringify(mapvalues)));
            //         setTimeout(function () {
            //             progress(0, 'Loading...', false);
            //         }, 1000);
            //     }

            // }
            //else  if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {


            //     if (Type == "pdf") {
            //         $scope.ActionStatus = 1;
            //         $scope.GenerateFilterPdf();
            //     }
            //     else {
            //         $scope.GenerateFilterExcel();
            //     }
            // }
            // else {
            $scope.ActionStatus = 0;
            $http({
                url: UtilityService.path + '/api/SpaceAvailabilityReport/GetAvailReport',
                method: 'POST',
                data: chartparams,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpaceAvailabilityReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {
            });

        }


        $scope.GenReportVacant = function (Avldata, Type) {
            progress(0, 'Loading...', true);
            $scope.SelValue.Type = Type;
            $http({
                url: UtilityService.path + '/api/SpaceAvailabilityReport/GetVacantSeatReport',
                method: 'POST',
                data: $scope.SelValue,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpaceVacantReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {
            });
        };


        setTimeout(function () { $scope.LoadData(); }, 1000);

    }]);