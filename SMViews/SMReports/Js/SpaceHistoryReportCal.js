﻿app.service("SpaceHistoryReportCal", function ($http, $q, UtilityService) {

});
app.controller('SpaceHistoryReportController', function ($scope, $q, $http, SpaceHistoryReportCal, UtilityService, $timeout, $filter) {
    $scope.spaceHistoryReport = {};
    $scope.Country = [];
    $scope.City = [];
    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Floors = [];



    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                });
                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;
                        angular.forEach($scope.City, function (value, key) {
                            value.ticked = true;
                        });
                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                                angular.forEach($scope.Locations, function (value, key) {
                                    value.ticked = true;
                                });
                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;
                                        angular.forEach($scope.Towers, function (value, key) {
                                            value.ticked = true;
                                        });
                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                angular.forEach($scope.Floors, function (value, key) {
                                                    value.ticked = true;
                                                })
                                                setTimeout(function () {
                                                    $scope.LoadData();
                                                }, 500);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    $scope.Pageload();

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.spaceHistoryReport.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.spaceHistoryReport.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.spaceHistoryReport.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.spaceHistoryReport.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.spaceHistoryReport.City = $scope.City;
        $scope.getLocationsByCity();
    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.spaceHistoryReport.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.spaceHistoryReport.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.spaceHistoryReport.City[0] = cty;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.spaceHistoryReport.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.spaceHistoryReport.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.spaceHistoryReport.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.spaceHistoryReport.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.spaceHistoryReport.Locations[0] = lcm;
            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.spaceHistoryReport.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.FloorChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.spaceHistoryReport.Country[0] = cny;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.spaceHistoryReport.City[0] = cty;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.spaceHistoryReport.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.spaceHistoryReport.Towers[0] = twr;
            }
        });

    }
    $scope.floorChangeAll = function () {
        $scope.spaceHistoryReport.Floors = $scope.Floors;
        $scope.FloorChange();
    }
    $scope.dataObj = {};

    $scope.LoadData = function () {
        var Locations = _.filter($scope.spaceHistoryReport.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(',');
        var Towers = _.filter($scope.spaceHistoryReport.Towers, function (o) { return o.ticked == true; }).map(function (x) { return x.TWR_CODE; }).join(',');
        var Floors = _.filter($scope.spaceHistoryReport.Floors, function (o) { return o.ticked == true; }).map(function (x) { return x.FLR_CODE; }).join(',');

        $scope.dataObj = {
            Location: Locations,
            Tower: Towers,
            Floor: Floors,
        }
        console.log($scope.dataObj);
        setTimeout(function () {
            $scope.viewCalendar();
        }, 500);
    }


    $scope.viewCalendar = function () {
        $('#calendar').fullCalendar('destroy');
        setTimeout(function () {
            $scope.initCal();
        }, 500);

        setTimeout(function () {
            $scope.$apply(function () {
            });
        }, 2000);
        if (true) {
        }
        else {
            showNotification('error', 8, 'bottom-right', "Please Select atleast one Floor to Proceed.");
        }
    }

    var columnDefs = [
        { headerName: "Space Id", field: "SPC_ID", width: 130, cellClass: 'grid-align', pinned: 'left', /*rowGroupIndex: 0, hide: true,*/ },
        { headerName: "Employee Id", field: "EMP_NAME", width: 180, cellClass: 'grid-align', },
        { headerName: "From Date", field: "FROM_DATE", width: 130, cellClass: 'grid-align', },
        { headerName: "To Date", field: "TO_DATE", width: 130, cellClass: 'grid-align', },
        { headerName: "From Time", field: "FROM_TIME", width: 100, cellClass: 'grid-align', },
        { headerName: "To Time", field: "TO_TIME", width: 100, cellClass: 'grid-align', },
        { headerName: "Booked Date", field: "CREATED_DT", width: 130, cellClass: 'grid-align', },
        { headerName: "Booked By ", field: "CREATED_BY", width: 180, cellClass: 'grid-align', },
        { headerName: "Status", field: "SEAT_STATUS", width: 160, cellClass: 'grid-align', },
    ]

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        angularCompileRows: true,
        rowData: null,

        onAfterFilterChanged: function () { 
            $scope.TotalReq = $scope.gridOptions.api.inMemoryRowController.rowsAfterFilter.length;
        }
    }

    $("#anchExportExcel").click(function (event) {
        if ($scope.usable == false) {
            event.preventDefault();
        } else {
            progress(0, 'Downloading Please Wait...', true);
            var Filterparams = {
                skipHeader: false,
                skipFooters: false,
                skipGroups: false,
                allColumns: false,
                onlySelected: false,
                columnSeparator: ",",
                fileName: "SpaceHistoryCalenderReport.csv"
            };
            //$scope.gridOptions.api.exportDataAsCsv(Filterparams);

            var csvString = $scope.gridOptions.api.getDataAsCsv(Filterparams);
            var fileNamePresent = Filterparams && Filterparams.fileName && Filterparams.fileName.length !== 0;
            var fileName = fileNamePresent ? Filterparams.fileName : 'export.csv';
            var blobObject = new Blob([csvString], {
                type: "text/csv;charset=utf-8;"
            });
            // Internet Explorer
            if (window.navigator.msSaveOrOpenBlob) {
                var fileData = [csvString];
                var blobObject = new Blob(fileData);
                window.navigator.msSaveOrOpenBlob(blobObject, fileName);
            } else {
                // Chrome
                var url = "data:text/plain;charset=utf-8," + encodeURIComponent(csvString);
                var downloadLink = document.createElement("a");
                downloadLink.href = url;
                downloadLink.href = window.URL.createObjectURL(blobObject);
                (downloadLink).download = fileName;

                document.body.appendChild(downloadLink);
                downloadLink.click();


                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }
        }

    });


    $scope.GenReport = function (RptByCategory, Type) {
        progress(0, 'Loading...', true);
        if (Type == "pdf") {
            $scope.GenerateFilterPdf();
        }
        else {
            $scope.GenerateFilterExcel();
        }

    }



    $("#filtertxt").change(function () {
        onUpdateFilterChanged($(this).val());
    }).keydown(function () {
        onUpdateFilterChanged($(this).val());
    }).keyup(function () {
        onUpdateFilterChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFilterChanged($(this).val());
    })
    function onUpdateFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

//intiate cal

$scope.initCal = function () {
    $('#calendar').fullCalendar({

        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,listDay,listWeek'
        },

        views: {
            listDay: { buttonText: 'list day' },
            listWeek: { buttonText: 'list week' },

        },

        eventLimit: true, // for all non-agenda views
        timeFormat: 'H:mm',
        // defaultView: 'month',
        defaultDate: moment().format('YYYY-MM-DD'),
        selectable: true,
        selectHelper: true,
        select: function (start, end) {
            //ShowEventPopup(start, end);
            //clearNUpdateVals();
        },
        editable: true,
        allDaySlot: false,
        selectable: true,
        events: function (start, end, timezone, callback) {
            console.log($scope.dataObj.Location);
            $.ajax({
                url: '../../../api/SpaceHistoryReportCal/GetDailyCount',
                type: 'post',
                data: {
                    FromDate: moment(start._d).format('YYYY-MM-DD'),
                    ToDate: moment(end._d).format('YYYY-MM-DD'),
                    Locations: $scope.dataObj.Location,
                    Towers: $scope.dataObj.Tower,
                    Floors: $scope.dataObj.Floor
                },
                success: function (data) {
                    var events = [];
                    angular.forEach(data, function (value, key) {
                        events.push({
                            title: value.COUNT_VAL,
                            start: value.STARTDATE,
                            end: value.ENDDATE,
                        });
                    });
                    callback(events);
                }
            });
        },
        eventClick: function (calEvent, jsEvent, view) {

            $.ajax({
                url: '../../../api/SpaceHistoryReportCal/GetDayData',
                type: 'post',
                data: {
                    FromDate: moment(calEvent.start._d).format('YYYY-MM-DD'),
                    Locations: $scope.dataObj.Location,
                    Towers: $scope.dataObj.Tower,
                    Floors: $scope.dataObj.Floor
                },
                success: function (data) {
                    debugger;
                    $scope.gridOptions.api.setRowData(data);
                    $scope.TotalReq = $scope.gridOptions.api.inMemoryRowController.rowsAfterFilter.length;

                }
            });
        },


        eventAfterRender: function (event, $el, view) {
            event.editable = false;

            var fromTime = $.fullCalendar.moment(event.start).format("HH:mm");
            var toTime = $.fullCalendar.moment(event.end).format("HH:mm");
            var formattedTime = '';
            //  var formattedTime = $.fullCalendar.formatDates(event.start, event.end, "HH:mm { - HH:mm}");
            // If FullCalendar has removed the title div, then add the title to the time div like FullCalendar would do
            if ($el.find(".fc-event-title").length === 0) {

                $el.find(".fc-time").text(formattedTime);
            }
            else {
                $el.find(".fc-time").text(formattedTime);
            }

        },

        eventRender: function (event, element) {
            var tooltip = '<div class="tooltipevent" style="width:240px;height:30px; background:black;color:white;padding:5px 0px 0px 5px;border-radious:50px">'
                + 'Total bookings on <b>' + moment(event.start._i).format('DD-MM-YYYY') + " </b> is  " + event.title + " <br/>"

            '</div>';
            var $tootlip = $(tooltip).appendTo('body');
            element.qtip({
                style: {
                    classes: 'qtip-bootstrap',

                },
                content: $tootlip,
                position: {
                    my: 'top left',  // Position my top left...
                    at: 'bottom right', // at the bottom right of...
                }

            });
            $('.tooltipevent').hide();
        },

        dayClick: function (date, allDay, jsEvent, view) {
            console.log('day clicked');
            $.ajax({
                url: '../../../api/SpaceHistoryReportCal/GetDayData',
                type: 'post',
                data: {
                    FromDate: moment(date._d).format('YYYY-MM-DD')
                },
                success: function (data) {
                    $scope.gridOptions.api.setRowData(data);
                    $scope.TotalReq = $scope.gridOptions.api.inMemoryRowController.rowsAfterFilter.length;

                }
            });
        },
    });

}

});