﻿app.service("PropertyWiseSpaceReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/PropertyWiseSpaceReport/GetCustomizedDetails', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SearchAllData = function (SearchSpaces) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/PropertyWiseSpaceReport/SearchAllData', SearchSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});
app.controller('PropertyWiseSpaceReportController', function ($scope, $q, $http, PropertyWiseSpaceReportService, UtilityService, $timeout, $filter) {
    $scope.Customized = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.GridVisiblity2 = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.Country = [];
    $scope.City = [];
    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Floors = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    $scope.CNP_NAME = [];




    $scope.Pageload = function () {

        UtilityService.GetCompanies().then(function (response) {
            //console.log(response.data);
            if (response.data != null) {
                $scope.Company = response.data;

                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.Customized.CNP_NAME.push(a);
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                });
                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;
                        angular.forEach($scope.City, function (value, key) {
                            value.ticked = true;
                        });
                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                                angular.forEach($scope.Locations, function (value, key) {
                                    value.ticked = true;
                                });
                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;
                                        angular.forEach($scope.Towers, function (value, key) {
                                            value.ticked = true;
                                        });
                                        UtilityService.getFloors(2).then(function (response) {
                                            console.log(response.data);
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                angular.forEach($scope.Floors, function (value, key) {
                                                    value.ticked = true;
                                                })
                                                setTimeout(function () {
                                                    $scope.LoadData();
                                                }, 500);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Customized.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.Customized.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customized.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.Customized.City = $scope.City;
        $scope.getLocationsByCity();
    }


    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.Customized.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });
    }


    $scope.FloorChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.Customized.Towers[0] = twr;
            }
        });

    }

    $scope.locSelectAll = function () {
        $scope.Customized.Locations = $scope.Locations;
        $scope.LocationChange();
    }

    $scope.LocationChange = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });
    }


    //$scope.Cols = [

    //    { COL: "Floor name", value: "FLR_NAME", ticked: false },
    //    { COL: "Floor Code", value: "FLR_CODE", ticked: false },
    //    { COL: "Property Address", value: "PM_PPT_ADDRESS", ticked: false },
    //    { COL: "Tower Name", value: "TWR_NAME", ticked: false },

    //    { COL: "Property Requisition id", value: "PM_PPT_PM_REQ_ID", ticked: false },
    //    { COL: "Property Name", value: "PM_PPT_NAME", ticked: false },
    //    { COL: "City Name", value: "CTY_NAME", ticked: false },


    //    { COL: "Location Name", value: "LCM_NAME", ticked: false },
    //    { COL: "Carpet Area", value: "PM_AR_CARPET_AREA", ticked: false },
    //    { COL: "Build UpArea", value: "PM_AR_BUA_AREA", ticked: false },
    //    { COL: "Common Area", value: "PM_AR_COM_AREA", ticked: false },
    //    { COL: "Rent Area", value: "PM_AR_RENT_AREA", ticked: false },

    //    { COL: "Usable Area", value: "PM_AR_USABEL_AREA", ticked: false },
    //    { COL: "Plot Area", value: "PM_AR_USABEL_AREA", ticked: false },
    //    { COL: "Plot Area", value: "PM_AR_PLOT_AREA", ticked: false },
    //    { COL: "purchase price", value: "PM_PUR_PRICE", ticked: false },

    //    { COL: "insurance No", value: "PM_INS_SNO", ticked: false },
    //    { COL: "insurance Created date", value: "PM_INS_CREATED_DT", ticked: false },
    //    { COL: "insurance end date", value: "PM_INS_END_DT", ticked: false },
    //    { COL: "insurance Amount", value: "PM_INS_AMOUNT", ticked: false },
    //    { COL: "Lease Requisition id", value: "PM_LES_PM_LR_REQ_ID", ticked: false },

    //    { COL: "Lease Basic Rent", value: "PM_LES_BASIC_RENT", ticked: false },
    //    { COL: "Lease Security Id", value: "PM_LES_SEC_DEP_MONTHS", ticked: false },
    //    { COL: "Toatl Rent", value: "PM_LES_TOT_RENT", ticked: false },
    //    { COL: "Lease Rent Per Sqrft", value: "PM_LES_RENT_PER_SQFT_CARPET", ticked: false },

    //];

    //$scope.columnDefs = [


    //    { headerName: "Floor name", field: "FLR_NAME", cellClass: 'grid-align', width: 130, },
    //    { headerName: "Floor Code", field: "FLR_CODE", cellClass: 'grid-align', width: 130, },
    //    { headerName: "Property Address", field: "PM_PPT_ADDRESS", cellClass: 'grid-align', width: 130, },
    //    { headerName: "Tower Name", field: "TWR_NAME", cellClass: 'grid-align', width: 100 },

    //    { headerName: "Property Requisition id", field: "PM_PPT_PM_REQ_ID", cellClass: 'grid-align', width: 130, },
    //    { headerName: "Property Name", field: "PM_PPT_NAME", cellClass: 'grid-align', width: 130, },
    //    { headerName: "City Name", field: "CTY_NAME", cellClass: 'grid-align', width: 100 },

    //    { headerName: "Location Name", field: "LCM_NAME", width: 100, cellClass: 'grid-align', width: 100, },
    //    { headerName: "Carpet Area", field: "PM_AR_CARPET_AREA", cellClass: 'grid-align', width: 100, suppressMenu: true },
    //    { headerName: "Build UpArea", field: "PM_AR_BUA_AREA", cellClass: 'grid-align', width: 100, suppressMenu: true },
    //    { headerName: "Common Area", field: "PM_AR_COM_AREA", width: 100 },
    //    { headerName: "Rent Area", field: "PM_AR_RENT_AREA", cellClass: 'grid-align', width: 100, },
    //    { headerName: "Usable Area", field: "PM_AR_USABEL_AREA", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
    //    { headerName: "Plot Area", field: "PM_AR_PLOT_AREA", cellClass: 'grid-align', width: 90, },
    //    { headerName: "purchase price", field: "PM_PUR_PRICE", cellClass: 'grid-align', width: 130 },
    //    { headerName: "insurance No", field: "PM_INS_SNO", cellClass: 'grid-align', width: 130 },
    //    { headerName: "insurance Created date", field: "PM_INS_CREATED_DT", cellClass: 'grid-align', width: 130 },
    //    { headerName: "insurance end date", field: "PM_INS_END_DT", cellClass: 'grid-align', width: 130 },
    //    { headerName: "insurance Amount", field: "PM_INS_AMOUNT", cellClass: 'grid-align', width: 130, aggFunc: 'sum' },
    //    { headerName: "Lease Requisition id ", field: "PM_LES_PM_LR_REQ_ID", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
    //    //{ headerName: "", field: "PM_LES_ENTITLED_AMT", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
    //    { headerName: "Lease Basic Rent", field: "PM_LES_BASIC_RENT", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
    //    { headerName: "Lease Security Id", field: "PM_LES_SEC_DEP_MONTHS", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
    //    { headerName: "Toatl Rent", field: "PM_LES_TOT_RENT", cellClass: 'grid-align', width: 100 },
    //    { headerName: "Lease Rent Per Sqrft", field: "PM_LES_RENT_PER_SQFT_CARPET", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },

    //];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    //$scope.gridOptions = {
    //    columnDefs: $scope.columnDefs,
    //    enableCellSelection: false,
    //    enableFilter: true,
    //    enableSorting: true,
    //    enableColResize: true,
    //    showToolPanel: true,
    //    groupAggFunction: groupAggFunction,
    //    groupHideGroupColumns: true,
    //    groupColumnDef: {
    //        headerName: "Country", field: "COUNTRY",
    //        cellRenderer: {
    //            renderer: "group"
    //        }
    //    },
    //    angularCompileRows: true,
    //    onAfterFilterChanged: function () {
    //        if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
    //            $scope.DocTypeVisible = 0;
    //        else
    //            $scope.DocTypeVisible = 1;
    //    }
    //};
    var Options = {
        onGridReady: function () {
            Options.api.sizeColumnsToFit();
        }
    };

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupHideGroupColumns: true,
        angularCompileRows: true,

        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.Options.api.getFilterModel()))
                $scope.DocTypeVisible = 1;
            else
                $scope.DocTypeVisible = 0;
        },
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }


    };

    function groupAggFunction(rows) {
        var sums = {
            BUILTUP_AREA: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.BUILTUP_AREA += parseFloat((data.BUILTUP_AREA).toFixed(2));
        });
        return sums;
    }

    //setTimeout(function () {
    //    progress(0, 'Loading...', true);
    //    $scope.LoadData();

    // }, 500);

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var params = {

            CNP_NAME: $scope.Customized.CNP_NAME[0].CNP_ID,
            flrlst: $scope.Customized.Floors,
            //loclst: $scope.Customized.Locations
        };
        PropertyWiseSpaceReportService.GetGriddata(params).then(function (response) {
            //console.log(data);
            console.log(response.Coldef);
            ExportColumns = response.exportCols;
            $scope.gridOptions.api.setColumnDefs(response.Coldef);
            $scope.gridata = response.griddata;

            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData($scope.gridata);
                //$scope.gridOptions.api.sizeColumnsToFit();
                progress(0, 'Loading...', false);
            }



            //$scope.gridata = data.data;
            //if ($scope.gridata == null) {
            //    $scope.GridVisiblity = false;
            //    $scope.GridVisiblity2 = true;
            //    $scope.gridOptions.api.setRowData([]);

            //}
            //else {
            //    progress(0, 'Loading...', true);
            //    $scope.GridVisiblity = true;
            //    $scope.GridVisiblity2 = true;

            //    $scope.gridOptions.api.refreshHeader();
            //    $scope.gridOptions.api.setRowData($scope.gridata);
            //    var cols = [];
            //    var unticked = _.filter($scope.Cols, function (item) {
            //        return item.ticked == false;
            //    });
            //    var ticked = _.filter($scope.Cols, function (item) {
            //        return item.ticked == true;
            //    });
            //    for (i = 0; i < unticked.length; i++) {
            //        cols[i] = unticked[i].value;
            //    }
            //    $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
            //    cols = [];
            //    for (i = 0; i < ticked.length; i++) {
            //        cols[i] = ticked[i].value;
            //    }
            //    $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
            //}
            progress(0, '', false);
        });

    }, function (error) {
        console.log(error);
    }

    $scope.GenerateReport = function (Customized, Type) {
        progress(0, 'Loading...', true);
        $scope.Customized.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf1();
            }
            else {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    $scope.GenerateFilterExcel1();
                }
            }
        }
        else {
            if (Type == 'xls') {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    //JSONToCSVConvertor($scope.gridata, "Lease and sapce Report", true, "LeaseandsapceReport");
                    $scope.GenerateFilterExcel1();
                }
                else {
                    //JSONToCSVConvertor($scope.gridata, "Lease and sapce Report", true, "LeaseandsapceReport");
                    $scope.GenerateFilterExcel1();
                }
            }
            else if (Type == 'pdf') {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    $scope.GenerateFilterPdf1();
                }
                else {
                    $scope.GenerateFilterPdf1();
                }
            }
        };
        progress(0, '', false);
    }


    $scope.GenerateFilterPdf1 = function () {
        progress(0, 'Loading...', true);
        var columns = ExportColumns;
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("l", "pt", "A0");
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: '170', cellWidth: 'wrap' },
            columnStyles: { 0: { columnWidth: '170' } }
        });
        doc.save("Lease and sapce Report.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel1 = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Lease and sapce Report.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.Pageload();


});
