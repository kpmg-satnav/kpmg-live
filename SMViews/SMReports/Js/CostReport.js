﻿app.service("CostReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetGridData = function (param) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CostReport/GetCostReportBindGrid', param)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('CostReportController', ['$scope', '$q', 'CostReportService', 'UtilityService', '$timeout', '$http', '$timeout', '$filter', function ($scope, $q, CostReportService, UtilityService, $timeout, $http, $timeout, $filter) {
    $scope.Viewstatus = 0;
    $scope.rptArea = {};
    $scope.rptArea.CNP_NAME = [];
    $scope.GridVisiblity = false;
    $scope.Type = [];
    $scope.DocTypeVisible = 0;

    var columnDefs = [
        { headerName: "Country", field: "CNY_NAME", width: 100, cellClass: 'grid-align', filter: 'set', suppressMenu: true, sno: 1 },
        { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align', sno: 2 },
        { headerName: "Location Code", field: "LCM_CODE", width: 200, cellClass: 'grid-align', sno: 3 },
        { headerName: "Location Name", field: "LCM_NAME", width: 200, cellClass: 'grid-align', sno: 4 },
        { headerName: "Tower", field: "TWR_NAME", width: 100, cellClass: 'grid-align', suppressMenu: true, sno: 5 },
        { headerName: "Floor", field: "FLR_NAME", width: 100, cellClass: 'grid-align', sno: 5 },
        { headerName: "Parent Entity", field: "PE_NAME", width: 100, cellClass: 'grid-align', sno: 6 },
        { headerName: "Child Entity", field: "CHE_NAME", width: 100, cellClass: 'grid-align',sno: 7 },
        { headerName: "", field: "VER_NAME", width: 100, cellClass: 'grid-align', suppressMenu: true, sno: 8 },
        { headerName: "", field: "DEP_NAME", width: 150, cellClass: 'grid-align', suppressMenu: true, sno: 9 },
        { headerName: "Shift", field: "SHIFT", width: 150, cellClass: 'grid-align', sno: 10 },
        { headerName: "Space Type", field: "SPC_TYPE", width: 150, cellClass: 'grid-align', sno: 11 },
        { headerName: "Total No.Of Allocated Seats", field: "ALLOC_SEATS_COUNT", width: 200, cellClass: 'grid-align', suppressMenu: true, aggFunc: 'sum', sno: 12 },
        { headerName: "Unit Cost", field: "UNITCOST", width: 130, cellClass: 'grid-align', suppressMenu: true, aggFunc: 'sum', sno: 13 },
        {
            headerName: "Total Business Unit WS Cost", field: "TOTAL_BUSS_UNITCOST", width: 200, cellClass: 'grid-align', pinned: 'right', suppressMenu: true, aggFunc: 'sum', sno: 14
        },
        { headerName: "Company", field: "CNP_NAME", width: 200, cellClass: 'grid-align', sno: 15 },
    ];
    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                });
                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;
                        angular.forEach($scope.City, function (value, key) {
                            value.ticked = true;
                        });
                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                                angular.forEach($scope.Locations, function (value, key) {
                                    value.ticked = true;
                                });
                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;
                                        angular.forEach($scope.Towers, function (value, key) {
                                            value.ticked = true;
                                        });
                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                angular.forEach($scope.Floors, function (value, key) {
                                                    value.ticked = true;
                                                })
                                                
                                            }
                                        });
                                        UtilityService.GetCompanies().then(function (response) {
                                            if (response.data != null) {
                                                $scope.Company = response.data;
                                                //$scope.rptArea.CNP_NAME = parseInt(CompanySession);
                                                angular.forEach($scope.Company, function (value, key) {
                                                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                                                    a.ticked = true;
                                                    $scope.rptArea.CNP_NAME.push(a);
                                                });
                                                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                                                else { $scope.EnableStatus = 0; }
                                            }
                                            $timeout(function () {
                                                $scope.GetCostBindGrid();
                                            }, 500);
                                            
                                        });
                                        
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    $scope.Pageload();
    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.rptArea.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.rptArea.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.rptArea.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.rptArea.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.rptArea.City = $scope.City;
        $scope.getLocationsByCity();
    }



    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.rptArea.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.rptArea.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.rptArea.City[0] = cty;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.rptArea.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.rptArea.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.rptArea.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.rptArea.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.rptArea.Locations[0] = lcm;
            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.rptArea.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.FloorChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.rptArea.Country[0] = cny;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.rptArea.City[0] = cty;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.rptArea.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.rptArea.Towers[0] = twr;
            }
        });

    }
    $scope.floorChangeAll = function () {
        $scope.rptArea.Floors = $scope.Floors;
        $scope.FloorChange();
    }


    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
            $scope.gridOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
            $scope.gridOptions.columnApi.getColumn("DEP_NAME").colDef.headerName = $scope.BsmDet.Child;
            $scope.gridOptions.api.refreshHeader();
        }
    });
    //'<i class="fa fa-inr"></i>'    '&#2352' +  'background-color': 'white' 
    var sum = 0;
    var res = 0;

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },
        //showToolPanel: true,
    };

    function BusinessCost(params) {
        var a = parseInt(params.data.ALLOC_SEATS_COUNT);
        var b = parseFloat(params.data.UNITCOST);
        GrandTotal(params);
        return a * b;
    }

    function GrandTotal(params) {
        if (angular.equals({}, $scope.gridOptions.api.getFilterModel())) {
            sum = sum + params.data.TOTAL_BUSS_UNITCOST;
        }
        else {
            sum = sum + params.data.TOTAL_BUSS_UNITCOST;
        }
        return sum;
    }

    function createData() {
        var result = [];
        result.push({
            //CNY_NAME: prefix + ' Country ' + i,
            //CTY_NAME: prefix + ' City ' + i,
            //LCM_NAME: prefix + ' Location ' + i,
            //TWR_NAME: prefix + ' Tower ' + i,
            //FLR_NAME: prefix + ' Floor ' + i,
            //VER_NAME: prefix + ' Vertical ' + i,
            //DEP_NAME: prefix + ' Department ' + i,
            //SHIFT: prefix + ' Shift ' + i,
            //SPC_TYPE: prefix + ' Space Type ' + i,
            //ALLOC_SEATS_COUNT: prefix + ' Total No.Of Allocated Seeats ' + i,
            //UNITCOST: prefix + ' Unit Cost Rs ' + i,
            //UNITCOST:0,
            TOTAL_BUSS_UNITCOST: 0,

        });
        //}

        return result;
        console.log(result);
    }

    //$scope.pageSize = '10';

    function onFloatingBottomCount(footerRowsToFloat) {
        var count = Number(footerRowsToFloat);
        //var rows = createData(count, 'Bottom');
        var rows = createData();
        $scope.gridOptions.api.setFloatingBottomRowData(rows);
    }

    $scope.GetCostBindGrid = function () {
    
        progress(0, 'Loading...', true);
        
        var params = {
                    cnylst: $scope.rptArea.Country,
                    ctylst: $scope.rptArea.City,
                    loclst: $scope.rptArea.Locations,
                    twrlst: $scope.rptArea.Towers,
                    flrlst: $scope.rptArea.Floors,
                    CNP_NAME: $scope.rptArea.CNP_NAME[0].CNP_ID
                };

                CostReportService.GetGridData(params).then(function (response) {
                    $scope.RptByUsrGrid = true;
                    $scope.gridata = response.data;
                    if (response.data == null) {
                        
                        $scope.gridOptions.api.setRowData([]);
                        progress(0, 'Loading...', false);
                        $scope.GridVisiblity = false;
                        showNotification('error', 8, 'bottom-right', response.Message);
                    }
                    else {
                        progress(0, 'Loading...', true);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        $scope.GridVisiblity = true;
                        progress(0, 'Loading...', false);
                    }

                    $scope.LocationWiseCostDetails();
                }, function (error) {
                    console.log(error);
                });
            
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
            sum = 0;
            res = 0;
        } else {
            $scope.DocTypeVisible = 0;
        }

    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $("#table2").fadeIn();


    var chart = c3.generate({
        data: {
            //x: 'x',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Entity'],
                height: 130,
                show: true,
                label: {
                    text: '',
                    position: 'outer-center'
                }
            },
            y: {
                show: true,
                label: {
                    text: 'Total Cost',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });
    $scope.LocationWiseCostDetails = function () {
        $http({
            url: UtilityService.path + '/api/CostReport/GetCostChartData',
            method: 'POST',
            data: ''
        }).
            success(function (result) {
                chart.unload();
                chart.load({ columns: result });
            });
        setTimeout(function () {
            $("#SpcGraph").empty();
            $("#SpcGraph").append(chart.element);
        }, 700);

    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" },
        { title: "Floor", key: "FLR_NAME" }, { title: "Parent Entity", key: "PE_NAME" }, { title: "Child Entity", key: "CHE_NAME" },
        { title: "Vertical", key: "VER_NAME" }, { title: "Department", key: "DEP_NAME" }, { title: "Shift", key: "SHIFT" }, { title: "Space Type", key: "SPC_TYPE" },
        { title: "Total No.Of Allocated Seeats", key: "ALLOC_SEATS_COUNT" }, { title: "Unit Cost Rs", key: "UNITCOST" },
        { title: "Total Business Unit WS Cost", key: "TOTAL_BUSS_UNITCOST" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A3');
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: 'auto' },
            columnStyles: { 0: { columnWidth: '15%' } }
        });
        doc.save("CostReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.gridOptions.onColumnVisible = function (event) {
        if (event.visible) {
            $scope.HideColumns = 0;
            //console.log(event.column.colId + ' was made visible');     

            $scope.gridOptions.columnDefs.push(
                {
                    headerName: event.column.colDef.headerName,
                    field: event.column.colId,
                    cellClass: "grid-align",
                    width: 110,
                    sno: event.column.colDef.sno,
                    suppressMenu: true
                }
            );
            $scope.gridOptions.columnDefs.visible = true;
            _.sortBy($scope.gridOptions.columnDefs, [function (o) { return o.sno; }]);
        } else {
            //console.log($scope.gridOptions.columnDefs.length);
            $scope.HideColumns = 1;
            for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
                if ($scope.gridOptions.columnDefs[i].field === event.column.colId) {
                    $scope.gridOptions.columnDefs[i].visible = false;
                    $scope.gridOptions.columnDefs.splice(i, 1);
                }
            }
        }
    };
    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        //var Filterparams = {
        //    skipHeader: false,
        //    skipFooters: false,
        //    skipGroups: false,
        //    allColumns: false,
        //    onlySelected: false,
        //    columnSeparator: ",",
        //    fileName: "CostReport.csv"
        //};

        //$scope.gridOptions.api.exportDataAsCsv(Filterparams);


        //setTimeout(function () {
        //    progress(0, 'Loading...', false);
        //}, 1000);
        var mapvalues = [];
        var columns = "";
        _.forEach($scope.gridOptions.api.columnController.allColumns, function (value) {

            columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

        });
        mapvalues.push(_.map($scope.gridOptions.api.inMemoryRowController.rowsAfterFilter, 'data'));
        alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("CostReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (rptArea, Type) {
        var searchval = $("#filtertxt").val();
        progress(0, 'Loading...', true);
        eaobj = {};
        // angular.copy(rptArea, eaobj);
        //eaobj.CNP_NAME = $scope.rptArea.CNP_NAME[0].CNP_ID;
        //eaobj.Type = Type;
        //eaobj.VERTICAL = $scope.BsmDet.Parent;
        //eaobj.COSTCENTER = $scope.BsmDet.Child;
        //eaobj.SearchValue = searchval;
        //eaobj.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        //eaobj.PageSize = $scope.gridata[0].OVERALL_COUNT;
        // rptArea.CNP_NAME = $scope.rptArea.CNP_NAME[0].CNP_ID
        //rptArea.Type = Type;
        var params = {
            cnylst: $scope.rptArea.Country,
            ctylst: $scope.rptArea.City,
            loclst: $scope.rptArea.Locations,
            twrlst: $scope.rptArea.Towers,
            flrlst: $scope.rptArea.Floors,
            CNP_NAME: $scope.rptArea.CNP_NAME[0].CNP_ID,
            Type :Type
        };

        if ($scope.HideColumns == 1 || $scope.HideColumns == 0) {
            progress(0, 'Loading...', true);
            var columns = [];
            var col = [];
            var mapvalues = [];
            for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
                var dt = ({ title: $scope.gridOptions.columnDefs[i].headerName, key: $scope.gridOptions.columnDefs[i].field, sno: $scope.gridOptions.columnDefs[i].sno });
                columns.push(dt);
            }
            var sortorder = _.sortBy(columns, [function (o) { return o.sno; }]);
            var model = $scope.gridOptions.api.getModel();
            var data = [];
            model.forEachNodeAfterFilter(function (node) {
                data.push(node.data);
            });
            if (Type == "pdf") {

                var jsondata = JSON.parse(JSON.stringify(data));
                var doc = new jsPDF('p', 'pt', 'A3');
                doc.autoTable(sortorder, jsondata, {
                    startY: 43,
                    margin: { horizontal: 7 },
                    styles: { overflow: 'linebreak', columnWidth: 'auto' },
                    columnStyles: { 0: { columnWidth: '15%' } }
                });
                doc.save("CostReport.pdf");
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);

            }
            else {
                mapvalues.push(data);
                _.forEach($scope.gridOptions.columnDefs, function (value) {
                    col = col + value.field + " as [" + value.headerName + "],"
                });
                alasql('SELECT ' + col.slice(0, -1) + ' INTO XLSX("CostReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);
            }

        }
        else if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $scope.DocTypeVisible = 0
            $http({
                url: '../../../api/CostReport/GetCostReportdata',
                method: 'POST',
                data: params,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'CostReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, 'Loading...', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

    //$timeout($scope.GetCostBindGrid, 500);
   

}]);

