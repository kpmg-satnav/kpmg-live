﻿app.service("SpaceSeatBookingHistroyService", function ($http, $q, UtilityService) {

    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.put(UtilityService.path + '/api/SpaceSeatBookingHistroy/SpaceSeatBookingHistroy')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});


app.controller('SpaceSeatBookingHistoryController', function ($scope, $q, $http, SpaceSeatBookingHistroyService, UtilityService, $timeout, $filter) {
    $scope.data = [];

    $scope.VerticalWiseAlloc = ''

    UtilityService.getSysPreferences().then(function (response) {
        if (response.data != null) {
            $scope.SysPref = response.data;

            $scope.VerticalWiseAlloc = _.find($scope.SysPref, { SYSP_CODE: "Booking Based on Vertical" });
        }
    });

    var a = [
        { headerName: "Space ID", field: "Space_ID", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "Employee Name", field: "Employee_Name", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "FROM DATE", field: "FROM_DATE", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "TO DATE", field: "TO_DATE", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "FROM TIME", field: "FROM_TIME", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "TO TIME", field: "TO_TIME", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "CREATED DATE", field: "CREATED_DATE", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "CREATED BY", field: "CREATED_BY", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "SEAT STATUS", field: "SEAT_STATUS", width: 0, cellClass: "grid-align", suppressMenu: false },
        { headerName: "SSA SRNREQ ID", field: "SSA_SRNREQ_ID", width: 0, cellClass: "grid-align", suppressMenu: false, hide: true },
        { headerName: "SSAD STA ID", field: "SSAD_STA_ID", width: 0, cellClass: "grid-align", suppressMenu: false, hide: true },
        { headerName: "SSAD SRN REQ ID", field: "SSAD_SRN_REQ_ID", width: 0, cellClass: "grid-align", suppressMenu: false, hide: true },
        { headerName: "RELEASE_SEAT", width: 100, field: "Released ", cellClass: "grid-align", filter: 'set', template: '<a ng-click="release(data)" data-ng-if="data.activeBtn">RELEASE</a>', pinned: 'right', suppressMenu: true }
    ];

    $scope.gridOptions = {
        columnDefs: a,
        enableCellSelection: false,
        enableFilter: true,
        rowData: [],
        enableSorting: true,
        angularCompileRows: true,
        rowSelection: 'multiple',
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    }
    $scope.gridOptions.getRowStyle = function (params) {
        if (params.node.FromDate <= moment(new Date)) {
            return { background: 'red' };
        }
    };

    function loaddata() {
        SpaceSeatBookingHistroyService.GetGriddata().then(function (data) {
            $scope.gridata = data.griddata;
            $scope.data = data.griddata;

            activeBtn = true;
            //a = data.Coldef;
            for (var i in $scope.gridata) {
                $scope.gridata[i].activeBtn = false;
                if ($scope.gridata[i].SSAD_STA_ID == "1004") {
                    $scope.gridata[i].activeBtn = true;
                }
                else {
                    $scope.gridata[i].activeBtn = false;
                }
            }
            progress(0, 'Loading...', true);
            if ($scope.gridata == null) {
                //$scope.gridOptions.api.setColumnDefs(data.Coldef);
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            else {
                if (window.innerWidth <= 780) {

                    $scope.IsMobile = true;
                    $scope.IsWeb = false;
                }
                else {


                    showNotification('', 8, 'bottom-right', '');
                    //$scope.gridOptions.api.setColumnDefs(data.Coldef);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                    $scope.IsMobile = false;
                    $scope.IsWeb = true;
                }

            }

            progress(0, 'Loading...', false);
        });
    };
    loaddata();
    $scope.release = function (det) {
        progress(0, 'Loading...', true);
        var emp = det.Employee_Name.split("/");
        var selectddata = [];
        selectddata.push({
            SPC_ID: det.Space_ID,
            SSAD_SRN_REQ_ID: det.SSAD_SRN_REQ_ID,
            SSA_SRNREQ_ID: det.SSA_SRNREQ_ID,
            AUR_ID: emp[0].trim(),
            AUR_NAME: emp[1].trim(),
            FROM_DATE: det.FROM_DATE,
            TO_DATE: det.TO_DATE,
            FROM_TIME: det.FROM_TIME,
            TO_TIME: det.TO_TIME,
            ticked: true
        });



        if ($scope.VerticalWiseAlloc.SYSP_VAL1 == 1) {
            var dataobj = { sad: selectddata, reltype: 1003 };
        }
        else {
            var dataobj = { sad: selectddata, reltype: 1002 };

        }


        $.ajax({
            type: "POST",
            url: window.location.origin + "/api/MaploaderAPI/ReleaseSelectedseat",
            data: JSON.stringify(dataobj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                showNotification('success', 8, 'bottom-right', 'Space Released Succesfully');
            },
            error: function () {
                showNotification('error', 8, 'bottom-right', 'Something went wrong');
            }
        });

        loaddata();
        progress(0, 'Loading...', false);
    }


});
