﻿app.service("CostSummaryReportService", function ($http, $q, UtilityService) {
    this.GetGridData = function (param) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CostSummary/GetCostReportBindGrid', param)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});
app.controller('CostSummaryReportController', function ($scope, $q, CostSummaryReportService, UtilityService, $timeout, $http, $filter) {
    $scope.SummaryReport = {};

    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Locations = response.data;
            angular.forEach($scope.Locations, function (value, key) {
                value.ticked = true;
            });
        }
        return UtilityService.getBussHeirarchy();
    }).then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
        return UtilityService.getVerticals(2);
    }).then(function (response) {
        if (response.data != null) {
            $scope.Verticals = response.data;
            angular.forEach($scope.Verticals, function (value, key) {
                value.ticked = true;
            });
        }

        setTimeout(function () {
        $scope.GetCostBindGrid();
        }, 500);

    })
    

    var columnDefs = [
        { headerName: "Location", field: "LCM_NAME", width: 150, cellClass: 'grid-align', filter: 'set', suppressMenu: true, sno: 1 },
        { headerName: "Vertical", field: "VER_NAME", width: 150, cellClass: 'grid-align', sno: 2 },
        { headerName: "Allocated Count", field: "ALLOC_SEATS_COUNT", width: 150, cellClass: 'grid-align', sno: 3 },
        { headerName: "Unit Cost", field: "UNITCOST", width: 200, cellClass: 'grid-align', sno: 3 },
        { headerName: "Total Business Unit WS Cost", field: "TOTAL_BUSS_UNITCOST", width: 200, cellClass: 'grid-align', sno: 4 }
    ];
    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,

        //showToolPanel: true,
    };
    $scope.GetCostBindGrid = function () {

        progress(0, 'Loading...', true);
       
        var params = {

            lcmlst: $scope.SummaryReport.Locations,
            verlst: $scope.SummaryReport.Verticals

        };
        

        CostSummaryReportService.GetGridData(params).then(function (response) {
            console.log(response);
            if (response.data == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);

                showNotification('error', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.gridOptions.api.setRowData(response.data.Table);
                progress(0, 'Loading...', false);
            }
        }, function (error) {
            console.log(error);
        });

    };
    $scope.GenReport = function (Type) {
        $scope.gridOptions.api.exportDataAsCsv($scope.gridOptions.columnDefs);

        //var params = {

        //    lcmlst: $scope.SummaryReport.Locations,
        //    verlst: $scope.SummaryReport.Verticals

        //};

        //progress(0, 'Downloading Please wait...', true);
        //$http({
        //    url: UtilityService.path + '/api/CostSummary/GetCostReportBindGrid',
        //    method: 'POST',
        //    data: params,
        //    responseType: 'arraybuffer'

        //}).success(function (data, status, headers, config) {
        //    var file = new Blob([data], {
        //        type: 'application/' + Type
        //    });
        //    var fileURL = URL.createObjectURL(file);
        //    $("#reportcontainer").attr("src", fileURL);
        //    var a = document.createElement('a');
        //    a.href = fileURL;
        //    a.target = '_blank';
        //    a.download = 'CostSummary.' + Type;
        //    document.body.appendChild(a);
        //    a.click();
        //    progress(0, '', false);
        //}).error(function (data, status, headers, config) {

        //});
    }
   
});