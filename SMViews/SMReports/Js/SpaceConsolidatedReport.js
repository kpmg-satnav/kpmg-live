﻿app.service("SpaceConsolidatedReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.BindGrid = function (Params) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceConsolidatedReport/BindGrid', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSummary = function (Params) {
        console.log(Params);
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceConsolidatedReport/GetSummary', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SpaceConsolidatedChart = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceConsolidatedReport/SpaceConsolidatedChart')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('SpaceConsolidatedReportController', ['$scope', '$q', 'SpaceConsolidatedReportService', '$filter', 'UtilityService', '$timeout', '$http', function ($scope, $q, SpaceConsolidatedReportService, $filter, UtilityService, $timeout, $http) {
    $scope.Consolidated = {};
    $scope.ChartParam = {};
    $scope.Viewstatus = 0;
    $scope.rptArea = {};
    $scope.rptareaprj = {};
    $scope.ProjectReport = {};
    $scope.GridVisiblity = false;
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.showsummary = true;
    $scope.GridVisiblityproject = false;
    $scope.GridVisiblitylocation = false;

    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                //$scope.Consolidated.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });
    }, 500);
    //UtilityService.getBussHeirarchy().then(function (response) {
    //    if (response.data != null) {
    //        $scope.BsmDet = response.data;
    //        //$scope.gridOptions.columnApi.getColumn("VERTICAL").colDef.headerName = "Allocated to " + $scope.BsmDet.Parent;
    //        //$scope.gridOptions.columnApi.getColumn("COSTCENTER").colDef.headerName = "Allocated to " + $scope.BsmDet.Child;
    //        //$scope.gridOptions.api.refreshHeader();
    //    }
    //});



    $scope.BindGrid = function () {
        var params = {
            CNP_NAME: $scope.Consolidated.CNP_NAME[0].CNP_ID,
            FLAG: 1
        };

        $scope.ChartParam = params;
        progress(0, 'Loading...', true);
        SpaceConsolidatedReportService.BindGrid(params).then(function (response) {
            progress(0, 'Loading...', true);
            //$scope.RptByUsrGrid = true;
            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.GridVisiblity = false;
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
                //showNotification('error', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.GridVisiblityproject = false;
                $scope.GridVisiblitylocation = false;
                $scope.gridOptions.api.setRowData([]);
                $scope.gridOptions.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }
            $scope.SpaceConsolidatedChart();
        }, function (error) {
            console.log(error);
        });
    };


    //$scope.BindGrid = function () {
    //    var params = {
    //        CNP_NAME: $scope.Consolidated.CNP_NAME[0].CNP_ID,
    //        FLAG: 1
    //    };
    //    $scope.ChartParam = params;
    //    SpaceConsolidatedReportService.BindGrid(params).then(function (response) {
    //        progress(0, 'Loading...', true);
    //        //$scope.RptByUsrGrid = true;
    //        $scope.gridata = response.data;
    //        if (response.data == null) {
    //            $scope.GridVisiblity = false;
    //            $scope.gridOptions.api.setRowData([]);
    //            progress(0, 'Loading...', false);
    //            //showNotification('error', 8, 'bottom-right', response.Message);
    //        }
    //        else {
    //            progress(0, 'Loading...', true);
    //            $scope.GridVisiblity = true;
    //            $scope.GridVisiblityproject = false;
    //            $scope.GridVisiblitylocation = false;
    //            $scope.gridOptions.api.setRowData([]);
    //            $scope.gridOptions.api.setRowData($scope.gridata);
    //            setTimeout(function () {
    //                progress(0, 'Loading...', false);
    //            }, 1000)
    //        }
    //        $scope.SpaceConsolidatedChart();
    //    }, function (error) {
    //        console.log(error);
    //    });
    //}

    var columnDefsLoc = [
        { headerName: "City", field: "CTY_NAME", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Location", field: "LCM_NAME", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Total Seats", field: "TOTAL_SEATS", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Vacant Seats", field: "VACANT_SEATS", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Occupied Seats", field: "OCCUPIED_SEATS", width: 160, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Employee Shared Seats", field: "EMPLOYEE_OCCUPIED_SEAT_COUNT", width: 180, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Company Name", field: "CNP_NAME", width: 170, cellClass: 'grid-align', suppressMenu: true }
    ];
    $scope.pageSize = '10';

    $scope.gridLocation = {
        columnDefs: columnDefsLoc,
        rowData: [],
        enableFilter: true,
        enableSorting: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        //groupAggFunction: groupaggfunctionloc,
        groupHideGroupColumns: false,
        groupColumnDef: {
            headerName: "City",
            cellRenderer: {
                renderer: "group"
            }

        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridLocation.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    }

    $scope.BindGrid_Location = function () {
        var params = {
            CNP_NAME: $scope.Consolidated.CNP_NAME[0].CNP_ID,
            FLAG: 2
        };

        $scope.ChartParam = params;
        progress(0, 'Loading...', true);
        // $scope.GridVisiblity = true;
        SpaceConsolidatedReportService.BindGrid(params).then(function (response) {
            progress(0, 'Loading...', true);
            //$scope.RptByUsrGrid = true;
            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.GridVisiblity = false;
                $scope.GridVisiblityproject = false;
                $scope.GridVisiblitylocation = true;
                $scope.gridLocation.api.setRowData([]);
                progress(0, 'Loading...', false);
                //showNotification('error', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = false;
                $scope.GridVisiblityproject = false;
                $scope.GridVisiblitylocation = true;
                $scope.gridLocation.api.setRowData([]);
                var TOTAL_SEATS_SUM = _.sumBy($scope.gridata, function (o) { return o.TOTAL_SEATS; });
                var TOTAL_SEATS_VACANT = _.sumBy($scope.gridata, function (o) { return o.VACANT_SEATS; });
                var TOTAL_SEATS_OCCUPIED = _.sumBy($scope.gridata, function (o) { return o.OCCUPIED_SEATS; });

                $scope.gridata.push({ CTY_NAME: '', LCM_NAME: 'Total', TOTAL_SEATS: TOTAL_SEATS_SUM, VACANT_SEATS: TOTAL_SEATS_VACANT, OCCUPIED_SEATS: TOTAL_SEATS_OCCUPIED });
                $scope.gridLocation.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }
            //$scope.SpaceConsolidatedChart();
        }, function (error) {
            console.log(error);
        });
    };

    //$scope.BindGrid_Location = function () {

    //    var params = {
    //        CNP_NAME: $scope.Consolidated.CNP_NAME[0].CNP_ID,
    //        FLAG: 2
    //    };
    //    console.log("hi");
    //    $scope.ChartParam = params;
    //    SpaceConsolidatedReportService.BindGrid(params).then(function (response) {
    //        progress(0, 'Loading...', true);
    //        //$scope.RptByUsrGrid = true;
    //        $scope.gridata = response.data;
    //        if (response.data == null) {
    //            $scope.GridVisiblity = false;
    //            $scope.GridVisiblityproject = false;
    //            $scope.GridVisiblitylocation = true;
    //            $scope.gridLocation.api.setRowData([]);
    //            progress(0, 'Loading...', false);
    //            //showNotification('error', 8, 'bottom-right', response.Message);
    //        }
    //        else {
    //            progress(0, 'Loading...', true);
    //            $scope.GridVisiblity = false;
    //            $scope.GridVisiblityproject = false;
    //            $scope.GridVisiblitylocation = true;
    //            $scope.gridLocation.api.setRowData([]);
    //            var TOTAL_SEATS_SUM = _.sumBy($scope.gridata, function (o) { return o.TOTAL_SEATS; });
    //            var TOTAL_SEATS_VACANT = _.sumBy($scope.gridata, function (o) { return o.VACANT_SEATS; });
    //            var TOTAL_SEATS_OCCUPIED = _.sumBy($scope.gridata, function (o) { return o.OCCUPIED_SEATS; });

    //            $scope.gridata.push({ CTY_NAME: '', LCM_NAME: 'Total', TOTAL_SEATS: TOTAL_SEATS_SUM, VACANT_SEATS: TOTAL_SEATS_VACANT, OCCUPIED_SEATS: TOTAL_SEATS_OCCUPIED });
    //            $scope.gridLocation.api.setRowData($scope.gridata);
    //            setTimeout(function () {
    //                progress(0, 'Loading...', false);
    //            }, 1000)
    //        }
    //        //$scope.SpaceConsolidatedChart();
    //    }, function (error) {
    //        console.log(error);
    //    });
    //}



    var columnDefs = [
        {
            headerName: "Country", field: "CNY_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 0, suppressMenu: true
        },
        { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 1, hide: true, suppressMenu: true },
        { headerName: "Location", field: "LCM_NAME", width: 200, cellClass: 'grid-align', rowGroupIndex: 2, hide: true },
        { headerName: "Tower", field: "TWR_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 3, hide: true },
        { headerName: "Floor", field: "FLR_NAME", width: 150, cellClass: 'grid-align' },
        { headerName: "Total Seats", field: "TOTAL_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Total Allocated Seats", field: "ALLOCATED_SEATS", width: 180, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Vacant Seats", field: "VACANT_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Occupied Seats", field: "OCCUPIED_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Employee Shared Seats", field: "EMPLOYEE_OCCUPIED_SEAT_COUNT", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Allocated But Not Occupied Seats", field: "ALLOCATED_VACANT", width: 250, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Blocked Seats", field: "BLOCKED_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Requested Seats", field: "REQUESTED_SEATS", width: 130, cellClass: 'grid-align', suppressMenu: true }
    ];

    $scope.pageSize = '10';


    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: [],
        //enableFilter: true,
        enableSorting: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "CNY_NAME",
            cellRenderer: {
                renderer: "group"
            }

        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };




    //function groupaggfunctionloc(rows) {

    //    var sums = {
    //        occupied_seats: 0,
    //        vacant_seats: 0,
    //        total_seats: 0

    //    };

    //    rows.foreach(function (row) {
    //        var data = row.data;
    //        sums.occupied_seats += parseint(data.OCCUPIED_SEATS);
    //        sums.vacant_seats += parseint(data.VACANT_SEATS);
    //        sums.total_seats += parseint(data.TOTAL_SEATS);

    //    });
    //    return sums;
    //};

    function groupAggFunction(rows) {

        var sums = {
            ALLOCATED_SEATS: 0,
            OCCUPIED_SEATS: 0,
            ALLOCATED_VACANT: 0,
            EMPLOYEE_OCCUPIED_SEAT_COUNT: 0,
            VACANT_SEATS: 0,
            TOTAL_SEATS: 0,
            REQUESTED_SEATS: 0,
            BLOCKED_SEATS: 0,
            SU: 0
        };

        rows.forEach(function (row) {
            var data = row.data;
            sums.ALLOCATED_SEATS += parseInt(data.ALLOCATED_SEATS);
            sums.OCCUPIED_SEATS += parseInt(data.OCCUPIED_SEATS);
            sums.EMPLOYEE_OCCUPIED_SEAT_COUNT += parseInt(data.EMPLOYEE_OCCUPIED_SEAT_COUNT);
            sums.ALLOCATED_VACANT += parseInt(data.ALLOCATED_VACANT);
            sums.VACANT_SEATS += parseInt(data.VACANT_SEATS);
            sums.TOTAL_SEATS += parseInt(data.TOTAL_SEATS);
            sums.REQUESTED_SEATS += parseInt(data.REQUESTED_SEATS);
            sums.BLOCKED_SEATS += parseInt(data.BLOCKED_SEATS);
            sums.SU += parseFloat(data.SU);
        });
        return sums;
    };


    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    });

    function onFilterChanged2(value) {
        $scope.Options.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $("#filteredproj").change(function () {
        onFilterChanged2($(this).val());
    }).keydown(function () {
        onFilterChanged2($(this).val());
    }).keyup(function () {
        onFilterChanged2($(this).val());
    }).bind('onFilterChanged1', function () {
        onFilterChanged2($(this).val());
    });

    function onFilterChanged1(value) {
        $scope.gridLocation.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $("#filteredloc").change(function () {
        onFilterChanged1($(this).val());
    }).keydown(function () {
        onFilterChanged1($(this).val());
    }).keyup(function () {
        onFilterChanged1($(this).val());
    }).bind('paste', function () {
        onFilterChanged1($(this).val());
    });

    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $("#table1").fadeIn();

    $scope.chart;
    $scope.chart = c3.generate({
        data: {
            x: "ALLOCSTA",
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                height: 130,
                show: true,
                tick: {
                    rotate: 75,
                    multiline: false
                }
            },
            y: {
                show: true,
                label: {
                    text: 'Count',
                    position: 'outer-middle'
                }
            }
        },
        width:
        {
            ratio: 0.5
        }
    });



    function tooltip_contents(d, defaultTitleFormat, defaultValueFormat, color) {
        var $$ = this, config = $$.config, CLASS = $$.CLASS,
            titleFormat = config.tooltip_format_title || defaultTitleFormat,
            nameFormat = config.tooltip_format_name || function (name) { return name; },
            valueFormat = config.tooltip_format_value || defaultValueFormat,
            text, i, title, value, name, bgcolor;

        for (i = 0; i < d.length; i++) {
            if (!(d[i] && (d[i].value || d[i].value === 0))) { continue; }

            if (!text) {
                text = "<table class='" + CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
            }

            name = nameFormat(d[i].name);
            value = d[i].value;
            bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

            text += "<tr class='" + CLASS.tooltipName + "-" + d[i].id + "'>";
            text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
            text += "<td class='value'>" + value + "</td>";
            text += "</tr>";
        }
        return text + "</table>";
    }

    //GetCostChartData
    //$scope.SpaceConsolidatedChart = function () {
    //    $scope.Columndata = [];
    //    var chartparams = {
    //        CNP_NAME: $scope.Consolidated.CNP_NAME[0].CNP_ID
    //    };
    //    SpaceConsolidatedReportService.SpaceConsolidatedChart(chartparams).then(function (response) {
    //        console.log(response);

    //        if (response.data != null) {
    //            $scope.chart.unload();
    //            $scope.chart.load({ columns: response.data });
    //            setTimeout(function () {
    //                $("#SpaceConsolidatedGraph").empty()
    //                $("#SpaceConsolidatedGraph").append($scope.chart.element);
    //            }, 700);
    //        }
    //        else if (response.data = null) {
    //            setTimeout(function () {
    //                $("#SpaceConsolidatedGraph").empty()
    //                $("#SpaceConsolidatedGraph").append($scope.chart.element);
    //            }, 700);
    //        }

    //$scope.SpaceConsolidatedChart = function () {
    //    $scope.Columndata = [];
    //    var chartparams = {
    //        CNP_NAME: $scope.Consolidated.CNP_NAME[0].CNP_ID
    //    };
    //    $http({
    //        url: UtilityService.path + '/api/SpaceConsolidatedReport/SpaceConsolidatedChart',
    //        method: 'POST',
    //        data: chartparams
    //    }).success(function (response) {
    //        console.log(response);
    //        $scope.Columndata.push(response.data.Columnnames);
    //        angular.forEach(response.data.Details, function (value, key) {
    //            $scope.Columndata.push(value);
    //        });
    //        $scope.chart.unload();
    //        $scope.chart.load({ columns: $scope.Columndata });
    //        $scope.ColumnNames = response.data.Columnnames;
    //    });
    //    setTimeout(function () {
    //        $("#SpaceConsolidatedGraph").empty();
    //        $("#SpaceConsolidatedGraph").append($scope.chart.element);
    //    }, 700);

    //}
    $scope.SpaceConsolidatedChart = function () {

        $.ajax({
            url: UtilityService.path + '/api/SpaceConsolidatedReport/SpaceConsolidatedChart',
            type: 'POST',
            data: '',
            success: function (result) {
                var chart = c3.generate({
                    data: {
                        x: 'x',
                        columns: [
                            result.Locations,
                            result.Occupied,
                            result.Vacant,
                        ],
                        type: 'bar',
                        types: {
                            'Vacant Count': 'line',
                        },
                        labels: true,

                    },
                    axis: {
                        x: {
                            type: 'category',
                            tick: {
                                rotate: 65,
                                multiline: false
                            },
                            height: 100
                        }
                    },
                    bindto: '#SpaceConsolidatedGraph'
                });
            }
        });
    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table1").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table1").fadeOut();
            });
        }

    });
    $scope.Options = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        rowData: [],
        rowSelection: 'multiple',
        rowDeselection: true,
        enableColResize: true,
        floatingFilter: true,

    };


    $scope.BindGrid_Project = function () {
        var params = {
            //SearchValue: searchval,
            //PageNumber: $scope.Options.api.grid.paginationController.currentPage + 1,
            //PageSize: 10,
            CNP_NAME: $scope.Consolidated.CNP_NAME[0].CNP_ID
        };

        $scope.ChartParam = params;
        progress(0, 'Loading...', true);
        SpaceConsolidatedReportService.GetSummary(params).then(function (response) {

            ExportColumns = response.exportCols;
            $scope.Options.api.setColumnDefs(response.Coldef);
            $scope.gridata1 = response.griddata;

            if ($scope.gridata1 == null) {
                $scope.GridVisiblity = false;
                $scope.GridVisiblitylocation = false;
                $scope.GridVisiblityproject = false;
                progress(0, 'Loading...', false);
                $scope.Options.api.setRowData([]);
            }
            else {
                $scope.GridVisiblity = false;
                $scope.GridVisiblitylocation = false;
                $scope.GridVisiblityproject = true;
                $scope.Options.api.setRowData($scope.gridata1);
                progress(0, 'Loading...', false);
            }

        }, function (error) {
            console.log(error);
        });
    };


    //$scope.BindGrid_LocationGrid_Project = function () {
    //    SpaceConsolidatedReportService.GetSummary().then(function (response) {

    //        ExportColumns = response.exportCols;
    //        $scope.Options.api.setColumnDefs(response.Coldef);
    //        $scope.gridata1 = response.griddata;

    //        if ($scope.gridata1 == null) {
    //            $scope.GridVisiblity = false;
    //            $scope.GridVisiblitylocation = false;
    //            $scope.GridVisiblityproject = false;
    //            $scope.Options.api.setRowData([]);
    //        }
    //        else {
    //            $scope.GridVisiblity = false;
    //            $scope.GridVisiblitylocation = false;
    //            $scope.GridVisiblityproject = true;
    //            $scope.Options.api.setRowData($scope.gridata1);

    //        }

    //    })
    //}

    //$scope.GenReportproject = function (ProjectReport, Type) {
    //    progress(0, 'Loading...', true);
    //    $scope.ProjectReport.Type = Type;
    //    if ($scope.Options.api.isAnyFilterPresent($scope.columnDefs)) {
    //        if (Type == "pdf") {
    //            $scope.GenerateFilterPdf1();
    //        }
    //        else {
    //            if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
    //                $scope.GenerateFilterExcel1();
    //            }
    //        }
    //    }
    //    else {
    //        if (Type == 'xls') {
    //            if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
    //                JSONToCSVConvertor($scope.gridata1, "Project Report", true, "ProjectReport");
    //            }
    //            else {
    //                JSONToCSVConvertor($scope.gridata1, "Project Report", true, "ProjectReport");
    //            }
    //        }
    //        else if (Type == 'pdf') {
    //            if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
    //                $scope.GenerateFilterPdf1();
    //            }
    //            else {
    //                $scope.GenerateFilterPdf1();
    //            }
    //        }
    //    };
    //    progress(0, '', false);
    //}

    $scope.GenReportproject = function (Type) {
        saobj = {};
        saobj.CNP_NAME = $scope.Consolidated.CNP_NAME[0].CNP_ID;
        saobj.DocType = Type;
        $scope.rptArea.DocType = Type;
        if ($scope.Options.api.isAnyFilterPresent($scope.columnDefs)) {
            progress(0, 'Loading...', true);
            if ($scope.rptArea.DocType == "pdf") {
                $scope.GenerateFilterPrjPdf();
            }
            else {
                $scope.GenerateFilterPrjExcel();
            }
        }
        else {
            progress(0, 'Loading...', true);
            $http({
                url: UtilityService.path + '/api/SpaceConsolidatedReport/Export_SpaceConsolidatedPrjRpt',
                method: 'POST',
                data: saobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpaceConsolidatedProjectReport.' + Type;
                document.body.appendChild(a);
                a.click();
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }).error(function (data, status, headers, config) {

            });
        }
    };

    $scope.GenerateFilterPrjExcel = function () {
        progress(0, 'Loading...', true);
        //var Filterparams = {
        //    skipHeader: false,
        //    skipFooters: false,
        //    skipGroups: false,
        //    allColumns: false,
        //    onlySelected: false,
        //    columnSeparator: ',',
        //    fileName: "SpaceConsolidatedReport.csv"
        //};
        //$scope.gridOptions.api.exportDataAsCsv(Filterparams);
        //setTimeout(function () {
        //    progress(0, 'Loading...', false);
        var mapvalues = [];
        var columns = "";
        _.forEach($scope.Options.api.columnController.allColumns, function (value) {

            columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

        });
        mapvalues.push(_.map($scope.Options.api.inMemoryRowController.rowsAfterFilter, 'data'));
        alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("SpaceConsolidatedPrjReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
        //}, 1000);
    }






    //$scope.GenerateFilterPdf1 = function () {
    //    progress(0, 'Loading...', true);
    //    var columns = ExportColumns;
    //    var model = $scope.Options.api.getModel();
    //    var data = [];
    //    model.forEachNodeAfterFilter(function (node) {
    //        data.push(node.data);
    //    });
    //    var jsondata = JSON.parse(JSON.stringify(data));
    //    var doc = new jsPDF("landscape", "pt", "a4");
    //    doc.autoTable(columns, jsondata);
    //    doc.save("ProjectReport.pdf");
    //    setTimeout(function () {
    //        progress(0, 'Loading...', false);
    //    }, 1000);
    //}

    //$scope.GenerateFilterExcel1 = function () {
    //    progress(0, 'Loading...', true);
    //    var Filterparams = {
    //        skipHeader: false,
    //        skipFooters: false,
    //        skipGroups: false,
    //        allColumns: false,
    //        onlySelected: false,
    //        columnSeparator: ',',
    //        fileName: "ProjectReport.csv"
    //    };
    //    $scope.Options.api.exportDataAsCsv(Filterparams);
    //    setTimeout(function () {
    //        progress(0, 'Loading...', false);
    //    }, 1000);
    //}




    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "Total Allocated Seats", key: "ALLOCATED_SEATS" }, { title: "Occupied Seats", key: "OCCUPIED_SEATS" }, { title: "Employee Shared Seats", key: "EMPLOYEE_OCCUPIED_SEAT_COUNT" }, { title: "Allocated But Not Occupied", key: "ALLOCATED_VACANT" }, { title: "Allocated But Not Occupied", key: "ALLOCATED_VACANT" }, { title: "Vacant Seats", key: "VACANT_SEATS" }, { title: "Total Seats", key: "TOTAL_SEATS" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        console.log(jsondata);
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: 'auto' },
            columnStyles: { 0: { columnWidth: '15%' } }
        });
        doc.save("SpaceConsolidatedReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        //var Filterparams = {
        //    skipHeader: false,
        //    skipFooters: false,
        //    skipGroups: false,
        //    allColumns: false,
        //    onlySelected: false,
        //    columnSeparator: ',',
        //    fileName: "SpaceConsolidatedReport.csv"
        //};
        //$scope.gridOptions.api.exportDataAsCsv(Filterparams);
        //setTimeout(function () {
        //    progress(0, 'Loading...', false);
        var mapvalues = [];
        var columns = "";
        _.forEach($scope.gridOptions.api.columnController.allColumns, function (value) {

            columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

        });
        mapvalues.push(_.map($scope.gridOptions.api.inMemoryRowController.rowsAfterFilter, 'data'));
        alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("SpaceConsolidatedReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
        //}, 1000);
    }


    $scope.GenReport = function (Type) {
        saobj = {};
        var searchval = $("#filtertxt").val();
        saobj.SearchValue = searchval,
            saobj.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1,
            saobj.PageSize = $scope.gridata[0].OVERALL_COUNT,
            saobj.CNP_NAME = $scope.Consolidated.CNP_NAME[0].CNP_ID;
        saobj.DocType = Type;
        saobj.FromDate = $scope.Consolidated.FromDate;
        //saobj.VERTICAL = $scope.BsmDet.Parent;
        //saobj.COSTCENTER = $scope.BsmDet.Child;
        console.log(saobj);
        $scope.rptArea.DocType = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            progress(0, 'Loading...', true);
            if ($scope.rptArea.DocType == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            progress(0, 'Loading...', true);
            $http({
                url: UtilityService.path + '/api/SpaceConsolidatedReport/Export_SpaceConsolidatedRpt',
                method: 'POST',
                data: saobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpaceConsolidatedReport.' + Type;
                document.body.appendChild(a);
                a.click();
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }).error(function (data, status, headers, config) {

            });
        }
    };


    $scope.GenReportlocation = function (Type) {
        saobj = {};
        var searchval = $("#filteredloc").val();
        saobj.SearchValue = searchval,
            saobj.PageNumber = $scope.gridLocation.api.grid.paginationController.currentPage + 1,
            saobj.PageSize = $scope.gridata[0].OVERALL_COUNT,
            saobj.CNP_NAME = $scope.Consolidated.CNP_NAME[0].CNP_ID;
        saobj.DocType = Type;
        saobj.FromDate = $scope.Consolidated.FromDate;
        //saobj.VERTICAL = $scope.BsmDet.Parent;
        //saobj.COSTCENTER = $scope.BsmDet.Child;
        console.log(saobj);
        $scope.rptareaprj.DocType = Type;
        if ($scope.gridLocation.api.isAnyFilterPresent($scope.columnDefsproject)) {
            progress(0, 'Loading...', true);
            if ($scope.rptareaprj.DocType == "pdf") {
                $scope.GenerateFilterLocationPdf();
            }
            else {
                $scope.GenerateFilterLocationExcel();
            }
        }
        else {
            progress(0, 'Loading...', true);
            $http({
                url: UtilityService.path + '/api/SpaceConsolidatedReport/Export_SpaceConsolidatedLocationRpt',
                method: 'POST',
                data: saobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpaceConsolidatedLocationReport.' + Type;
                document.body.appendChild(a);
                a.click();
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }).error(function (data, status, headers, config) {

            });
        }
    };


    $scope.GenerateFilterLocationPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "CITY", key: "CTY_NAME" }, { title: "LOCATION", key: "LCM_NAME" }, { title: "TOTAL SEATS", key: "TOTAL_SEATS" }, { title: "VACANT SEATS", key: "VACANT_SEATS" }, { title: "OCCUPIED SEATS", key: "OCCUPIED_VACANT" }];
        var model = $scope.gridLocation.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        console.log(jsondata);
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata, {
            startY: 43,
            margin: { horizontal: 7 },
            styles: { overflow: 'linebreak', columnWidth: 'auto' },
            columnStyles: { 0: { columnWidth: '15%' } }
        });
        doc.save("SpaceConsolidatedLocationReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


    $scope.GenerateFilterLocationExcel = function () {
        progress(0, 'Loading...', true);
        var mapvalues = [];
        var columns = "";
        _.forEach($scope.gridLocation.api.columnController.allColumns, function (value) {

            columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

        });
        mapvalues.push(_.map($scope.gridLocation.api.inMemoryRowController.rowsAfterFilter, 'data'));
        alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("SpaceConsolidatedlocationReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);

    }

    setTimeout(function () { $scope.BindGrid(); }, 1000);

}]);

