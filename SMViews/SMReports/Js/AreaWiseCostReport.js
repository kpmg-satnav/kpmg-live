﻿
app.service("AreaWiseCostService", function ($http, $q, UtilityService) {

    this.getSUDetails = function (dataobj) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AreaWiseCost/GetAreaWiseCostDetails', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.GetGridData = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AreaWiseCost/GetAreaWiseCostDetails', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetDetailsOnSelection = function (dataobj) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AreaWiseCost/GetDetailsOnSelection', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

});



app.controller("AreaWiseCostController", function ($scope, $http, $q, UtilityService, AreaWiseCostService) {
    $scope.AreaWiseCostReport = {};
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Country = [];
    $scope.City = [];
    $scope.Locations = [];
    $scope.Verticals = [];
    $scope.Costcenters = [];
    $scope.Segments = [];
    $scope.Spanleaders = [];
    $scope.AreaWiseCostReport.TRNDTYPE = "Monthly";
    $scope.WeekGriddata = [];
    $scope.showgrid = 0;
    $scope.Avldata = {};
    $scope.Type = [];

    $scope.SearchOptions = [
      { Id: 0, Code: 'L.LCM_NAME', Name: 'Location' },
      { Id: 1, Code: 'FLOOR', Name: 'Floor' },
      { Id: 2, Code: 'V.VER_NAME', Name: 'Vertical' },
      { Id: 3, Code: 'C.COST_CENTER_NAME', Name: 'Costcenter' },
      { Id: 4, Code: 'SHIFT', Name: 'Shift' },
       { Id: 5, Code: 'Parent Entity', Name: 'ParentEntity' },
        { Id: 6, Code: 'Child Entity', Name: 'ChildEntity' }
    ];

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });
    $scope.VerticalChange = function () {
        $scope.currentblkReq.SVR_COST_CODE = '';
        $('.selectpicker').selectpicker('refresh');
    }

    $scope.Pageload = function () {
        progress(0, 'Loading...', true);
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;
                    }

                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Locations = response.data;
                        }
                        progress(0, '', false);

                        UtilityService.getVerticals(3).then(function (response) {
                            if (response.data != null) {
                                $scope.Verticals = response.data;
                            }
                        });
                    });
                });
            }
        });
    }

    ///******* Loading vertical details  ********//////////
    $scope.VerSelectAll = function () {
        $scope.AreaWiseCostReport.Verticals = $scope.Verticals;
        $scope.getCostcenterByVertical();
    }

    $scope.getCostcenterByVertical = function () {
        progress(0, 'Loading...', true);
        UtilityService.getCostcenterByVertical($scope.AreaWiseCostReport.Verticals, 2).then(function (response) {
            if (response.data != null) {
                $scope.Costcenters = response.data
                progress(0, '', false);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Departments found');
            }

        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }

    ///******* Loading costcenter details  ********//////////
    $scope.CostcenterSelectAll = function () {
        $scope.AreaWiseCostReport.Costcenters = $scope.Costcenters;

    }

    $scope.getSegmentByCostcenter = function () {
        progress(0, 'Loading...', true);
        UtilityService.getSegmentByCostcenters($scope.AreaWiseCostReport.Costcenters, 2).then(function (response) {
            if (response.data != null) {
                $scope.Segments = response.data
                progress(0, '', false);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Departments found');
            }

        }, function (error) {
            progress(0, '', false);
            console.log(error);
        });
    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.AreaWiseCostReport.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.AreaWiseCostReport.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.AreaWiseCostReport.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.AreaWiseCostReport.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.AreaWiseCostReport.City = $scope.City;
        $scope.getLocationsByCity();
    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.AreaWiseCostReport.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.AreaWiseCostReport.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.AreaWiseCostReport.City[0] = cty;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.AreaWiseCostReport.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.setGridByOption = function () {
        $scope.AreaWiseCostReport.GRPBY = "";
        $scope.AreaWiseCostReport.GRPTEXT = "";
        angular.forEach($scope.SelOptions, function (value, key) {
            $scope.AreaWiseCostReport.GRPBY = $scope.AreaWiseCostReport.GRPBY + value.Name + ',';
        });
    }

    $scope.LoadData = function () {
        var params = {
            Countrylst: $scope.AreaWiseCostReport.Country,
            Citylst: $scope.AreaWiseCostReport.City,
            Locationlst: $scope.AreaWiseCostReport.Locations,
            Verticallst: $scope.AreaWiseCostReport.Verticals,
            Costcenterlst: $scope.AreaWiseCostReport.Costcenters,
            Grouplst: $scope.AreaWiseCostReport.GRPBY
        };

        progress(0, 'Loading...', true);

        $scope.showgrid = 1;

        AreaWiseCostService.GetGridData(params).then(function (response) {
            if (response.data != null) {
                $scope.gridata = response.data.Griddata;
                $scope.columnDefs = response.data.Coldef;
                response.data.Coldef.push({ headerName: "Allocated Count", field: "Allocated_Count", width: 190, cellClass: 'grid-align', width: 110, template: '<a ng-click="ShowPopup(data)">{{data.Allocated_Count}}</a>' });
                response.data.Coldef.push({ headerName: "Unit Cost", field: "Unit_Cost", width: 190, cellClass: 'grid-align', width: 110 });
                response.data.Coldef.push({ headerName: "Total Cost", field: "TotalCost", width: 190, cellClass: 'grid-align', width: 110 });

                $scope.gridOptions.api.setColumnDefs(response.data.Coldef);
                if ($scope.gridata == null) {
                    $scope.gridOptions.api.setRowData([]);
                }
                else {
                    console.log($scope.gridOptions.columnDefs);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "No Records");
            }
        });
    }
    setTimeout(function () {
        $scope.Pageload();
    }, 200);



    $scope.GenReport = function (Customized, Type) {
        progress(0, 'Downloading Please wait...', true);
        Customized.Type = Type;
        var params = {
            Countrylst: Customized.Country,
            Citylst: Customized.City,
            Locationlst: Customized.Locations,
            Verticallst: Customized.Verticals,
            Costcenterlst: Customized.Costcenters,
            Grouplst: Customized.GRPBY,
            Type: Type
        };

        $http({
            url: UtilityService.path + '/api/AreaWiseCost/GetAreaDetailsRPT',
            method: 'POST',
            data: params,
            responseType: 'arraybuffer'

        }).success(function (data, status, headers, config) {
            if (requiredFeaturesSupported()) {
                var blobObject = new Blob([data]);
                window.navigator.msSaveBlob(blobObject, 'AreaWiseCostReport.' + Type);
            }
            else {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'AreaWiseCostReport.' + Type;
                document.body.appendChild(a);
                a.click();
            }
            progress(0, '', false);
        }).error(function (data, status, headers, config) {
            alert('error');
        });

        function requiredFeaturesSupported() {
            return (BlobConstructor() && msSaveOrOpenBlobSupported());
        }

        function BlobConstructor() {
            if (!window.Blob) {
                //document.getElementsByTagName('body')[0].innerHTML = "<h1>The Blob constructor is not supported - upgrade your browser and try again.</h1>";
                return false;
            } // if

            return true;
        } // BlobConstructor

        function msSaveOrOpenBlobSupported() {
            if (!window.navigator.msSaveOrOpenBlob) { // If msSaveOrOpenBlob() is supported, then so is msSaveBlob().
                //document.getElementsByTagName('body')[0].innerHTML = "<h1>The msSaveOrOpenBlob API is not supported - try upgrading your version of IE to the latest version.</h1>";
                return false;
            } // if

            return true;
        } // msSaveOrOpenBlobSupported



    }
    $scope.columnDefs = [];
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        enableFilter: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    //Popup
    var PopDefs = [
                    { headerName: "Space Id", field: "SPC_ID", width: 190, cellClass: 'grid-align', width: 110, suppressMenu: true },
                    { headerName: "Space Type", field: "SPC_TYPE_NAME", width: 190, cellClass: 'grid-align', width: 110,suppressMenu:true },
                    { headerName: "Shift Name", field: "SH_NAME", width: 190, cellClass: 'grid-align', width: 110,suppressMenu:true },
                    { headerName: "Vertical", field: "VER_NAME", width: 190, cellClass: 'grid-align', width: 110 },
                    { headerName: "Costcenter", field: "COST_CENTER_NAME", width: 190, cellClass: 'grid-align', width: 110 },
                    { headerName: "Parent Entity", field: "PE_NAME", width: 190, cellClass: 'grid-align', width: 110 },
                    { headerName: "Child Entity", field: "CHE_NAME", width: 190, cellClass: 'grid-align', width: 110 },
                    { headerName: "Country", field: "CNY_NAME", width: 190, cellClass: 'grid-align', width: 110 },
                    { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 110 },
                    { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 200 },
                    { headerName: "Tower", field: "TWR_NAME", cellClass: 'grid-align', width: 150 },
                    { headerName: "Floor", field: "FLR_NAME", cellClass: 'grid-align', width: 110 },
                ];

    $scope.PopOptions = {
        columnDefs: PopDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
        onReady: function () {
            $scope.PopOptions.api.sizeColumnsToFit()
        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.PopOptions.api.getFilterModel()))
                $scope.PopDocTypeVisible = 0;
            else
                $scope.PopDocTypeVisible = 1;
        }
    }
    $scope.ShowPopup = function (response) {
        console.log(response);
        $scope.SelValue = response;
        $("#historymodal").modal('show');
    }

    $('#historymodal').on('shown.bs.modal', function () {
        //var dataobj = { Spcdata: $scope.SelValue };
        //console.log($scope.SelValue);
        AreaWiseCostService.GetDetailsOnSelection($scope.SelValue).then(function (response) {
            progress(0, 'Loading...', true);
            $scope.popdata = response;
            console.log($scope.popdata);
            $scope.PopOptions.api.setRowData($scope.popdata);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 500);

        });
    });

    $scope.GenAreaWiseCostReport = function (Avldata, Type) {
        progress(0, 'Loading...', true);
        //$scope.SelValue.Type = Type;
        console.log($scope.SelValue);       
        $http({
            url: UtilityService.path + '/api/AreaWiseCost/GetAreawisecostReport',
            method: 'POST',
            data: $scope.SelValue,
            responseType: 'arraybuffer'

        }).success(function (data, status, headers, config) {
            var file = new Blob([data], {
                type: 'application/' + Type
            });
            var fileURL = URL.createObjectURL(file);
            $("#reportcontainer").attr("src", fileURL);
            var a = document.createElement('a');
            a.href = fileURL;
            a.target = '_blank';
            a.download = 'AreawisecostpopupReport.' + Type;
            document.body.appendChild(a);
            a.click();
            progress(0, '', false);
        }).error(function (data, status, headers, config) {
        });
    };

});