﻿app.service("SpacePlanningReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    //Bind Grid
    this.BindGridData = function (searchObj) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpacePlanningReport/BindGridData', searchObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.BindGridData1 = function (searchObj) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpacePlanningReport/BindGridData1', searchObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('SpacePlanningReportController', ['$scope', '$q', 'SpacePlanningReportService', '$http', '$timeout', 'UtilityService', function ($scope, $q, SpacePlanningReportService, $http, $timeout, UtilityService) {

    $scope.options = [
        { id: 1, type: 'Location' },
        { id: 2, type: 'Vertical' },
        //{ id: 3, type: 'Cost_Center' }
    ];
    $scope.select = $scope.options[0]

    $scope.SpacePlanningRpt = {};
    $scope.City = [];
    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Verticals = [];
    $scope.CostCenters = [];
    $scope.DocTypeVisible = 0;
    $scope.GridVisiblity = true;
    $scope.Verticalgrid = false;
    $scope.locationgrid = true;
    $scope.Company = [];
    $scope.SpaceType = {};
    $scope.repeatCategorylist = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;
    $scope.CompanyVisible = 0;

    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.SpacePlanningRpt.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }
        });
    }, 100);

    $scope.search = function () {
        if ($scope.select.type == 'Location') {
            $scope.getGridData();
        }
        else
            $scope.getGridVerticalData();

    }

    $scope.getGridData = function () {
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();
        progress(0, 'Loading...', true);


        var dataSource = {
            rowCount: null,
            getRows: function (params) {

                var searchObj = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    CNP_NAME: $scope.SpacePlanningRpt.CNP_NAME[0].CNP_ID
                };
                //console.log(searchObj);
                SpacePlanningReportService.BindGridData(searchObj).then(function (response) {
                    //progress(0, 'Loading...', true);
                    $scope.gridata = response.data;
                    debugger;
                    if (response.data != null) {
                        $("#btNext").attr("disabled", true);
                        $scope.Verticalgrid = false;
                        $scope.locationgrid = true;
                        $scope.GridVisiblitylocation = true;
                        $scope.GridVisiblityvertical = false;
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        $scope.gridOptions.api.refreshHeader();
                    }
                    else {
                        $scope.Verticalgrid = false;
                        $scope.locationgrid = false;
                        $scope.gridOptions.api.setRowData([]);
                    }
                    $scope.SpaceChartDetails(searchObj);
                    progress(0, 'Loading...', false);
                }, 400);
            }

        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };


    //for GridView
    var columnDefs = [
        { headerName: "Location", width: 250, field: "SP_LOC", cellClass: 'grid-align' },
        { headerName: "Built Capacity", field: "BUILD_CAPACITY", width: 100, suppressMenu: true, cellClass: 'grid-align' },
        { headerName: "Utilized Capacity", field: "UTILIZED_SEATS", width: 130, suppressMenu: true, cellClass: 'grid-align' },
        { headerName: "Available Capacity", field: "VACANT", width: 140, suppressMenu: true, cellClass: 'grid-align' },
        { headerName: "New Capacity Required", field: "TOTAL_SEATS_REQUIRED", width: 170, cellClass: 'grid-align' },
        { headerName: "Remaining Available Capacity", field: "REQUIREMENT_PENDING", width: 180, suppressMenu: true, cellClass: 'grid-align' },
        { headerName: "Status", field: "STAUS", width: 100, cellClass: 'grid-align' },
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple',
        enableColResize: true,
    };

    setTimeout(function () {
        $scope.getGridData();
    }, 500);

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.getGridVerticalData = function () {
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();
        progress(0, 'Loading...', true);


        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                var searchObj = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    CNP_NAME: $scope.SpacePlanningRpt.CNP_NAME[0].CNP_ID
                };
                //console.log(searchObj);
                SpacePlanningReportService.BindGridData1(searchObj).then(function (response) {
                    //progress(0, 'Loading...', true);
                    $scope.griddata = response.data;
                    if (response.data != null) {

                        $scope.GridVisiblitylocation = false;
                        $scope.GridVisiblityvertical = true;
                        $scope.Verticalgrid = true;
                        $scope.locationgrid = false;
                        $scope.gridVerticalOptions.api.setRowData([]);
                        $scope.gridVerticalOptions.api.setRowData($scope.griddata);
                        $scope.gridVerticalOptions.api.refreshHeader();
                    }
                    else {
                        $scope.Verticalgrid = false;
                        $scope.locationgrid = false;
                        $scope.gridVerticalOptions.api.setRowData([]);
                    }
                    $scope.SpaceChartDetails(searchObj);
                    progress(0, 'Loading...', false);
                }, 400);
            }

        }
        $scope.gridOptions.api.setDatasource(dataSource);
    }

    var columnDefsvert = [
        { headerName: "Vertical Name", width: 250, field: "VER_NAME", cellClass: 'grid-align' },
        { headerName: "Total Capacity", field: "Total_Seats", width: 100, suppressMenu: true, cellClass: 'grid-align' },
        { headerName: "Utilized Capacity", field: "UTILIZED_SEATS", width: 130, suppressMenu: true, cellClass: 'grid-align' },
        { headerName: "Available Capacity", field: "VACANT", width: 140, suppressMenu: true, cellClass: 'grid-align' },
        { headerName: "New Capacity Required", field: "TOTAL_SEATS_REQUIRED", width: 170, cellClass: 'grid-align' },
        { headerName: "Remaining Available Capacity", field: "REQUIREMENT_PENDING", width: 180, suppressMenu: true, cellClass: 'grid-align' },
        { headerName: "Status", field: "STAUS", width: 100, cellClass: 'grid-align' },
    ];

    $scope.gridVerticalOptions = {
        columnDefs: columnDefsvert,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple',
        enableColResize: true,
    };


    function onFilterChanged1(value) {
        $scope.gridVerticalOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $("#filteredtxt").change(function () {
        onFilterChanged1($(this).val());
    }).keydown(function () {
        onFilterChanged1($(this).val());
    }).keyup(function () {
        onFilterChanged1($(this).val());
    }).bind('paste', function () {
        onFilterChanged1($(this).val());
    })



    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });

    $("#Tabular").fadeIn();
    $("#table2").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $scope.chart;
    $scope.chart = c3.generate({
        data: {
            x: 'LOCATION',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
            labels: true
        },
        legend: {
            show: false,
        },
        axis: {
            x: {
                type: 'category',
                height: 130,
                show: true,
                tick: {
                    rotate: 75,
                    multiline: false
                }
            },
            y: {
                show: true,
                label: {
                    text: 'Count',
                    position: 'outer-middle'
                }
            }
        },
        width:
        {
            ratio: 0.5
        }
    });

    function toggle(id) {
        chart.toggle(id);
    }

    d3.select('.container').insert('div', '.chart').attr('class', 'legend').selectAll('div').data({
        columns: [],
        cache: false,
        type: 'bar',
        empty: { label: { text: "Sorry, No Data Found" } },
        labels: true
    })
        .enter().append('div')
        .attr('data-id', function (id) {
            return id;
        })
        .html(function (id) {
            return id;
        })
        .each(function (id) {
            //d3.select(this).append('span').style
            d3.select(this).append('span').style('background-color', chart.color(id));
        })
        .on('mouseover', function (id) {
            chart.focus(id);
        })
        .on('mouseout', function (id) {
            chart.revert();
        })
        .on('click', function (id) {
            $(this).toggleClass("c3-legend-item-hidden")
            chart.toggle(id);
        });

    $scope.SpaceChartDetails = function (searchObj) {
        $scope.Columndata = [];
        $http({
            url: UtilityService.path + '/api/SpacePlanningReport/GetLocChartData',
            method: 'POST',
            data: searchObj
        }).success(function (response) {
            console.log(response);
            $scope.Columndata.push(response.data.Columnnames);
            angular.forEach(response.data.Details, function (value, key) {
                $scope.Columndata.push(value);
            });
            $scope.chart.unload();
            $scope.chart.load({ columns: $scope.Columndata });
            $scope.ColumnNames = response.data.Columnnames;
        });
        setTimeout(function () {
            $("#SpcGraph").empty();
            $("#SpcGraph").append($scope.chart.element);
        }, 100);

    }

    $scope.GenReport = function (spcdata, Type) {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        var dataobj = {
            Request_Type: $scope.SpacePlanningRpt.Request_Type,
            CNP_NAME: $scope.SpacePlanningRpt.CNP_NAME[0].CNP_ID,
            SearchValue: searchval,
            PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
            PageSize: $scope.gridata[0].OVERALL_COUNT,
            Type: Type
        };
        console.log(dataobj);
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (spcdata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/SpacePlanningReport/GetSpacePlanningReportdata',
                method: 'POST',
                data: dataobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpacePlanningReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "SpacePlanningReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


    $scope.GenReportvertical = function (spcdata, Type) {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        var dataobj = {
            Request_Type: $scope.SpacePlanningRpt.Request_Type,
            CNP_NAME: $scope.SpacePlanningRpt.CNP_NAME[0].CNP_ID,
            SearchValue: searchval,
            PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
            PageSize: $scope.gridata[0].OVERALL_COUNT,
            Type: Type
        };
        if ($scope.gridVerticalOptions.api.isAnyFilterPresent($scope.columnDefss)) {
            if (spcdata.Type == "pdf") {
                $scope.GenerateFilterVerticalPdf();
            }
            else {
                $scope.GenerateFilterVerticalExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/SpacePlanningReport/GetSpacePlanningVerticalReportdata',
                method: 'POST',
                data: dataobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SpacePlanningVerticalReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

    $scope.GenerateFilterVerticalExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "SpacePlanningVerticalReport.csv"
        };
        $scope.gridVerticalOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

}]);


