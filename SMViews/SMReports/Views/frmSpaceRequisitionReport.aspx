﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-body-container {
            color: black;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
            /*border-bottom: 1px solid #eee;*/
            /*background-color: #428bca;*/
            /*-webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;*/
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="SpaceRequisitionReportController" onload="setDateVals()" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">Space Requisition Report</h3>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <form id="form1" name="frmSpaceRequisition" data-valid-submit="LoadData()">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Select Range</label>
                                        <br />
                                        <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                            <%--                                            <option value="">Select Range</option>--%>
                                            <option value="TODAY">Today</option>
                                            <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="LASTMONTH">Last Month</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">From Date</label>
                                        <div class="input-group date" id='fromdate'>
                                            <input type="text" class="form-control" data-ng-model="SpaceRequisition.FromDate" id="FromDate" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.FromDate.$invalid" style="color: red;">Please From Date</span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">To Date</label>
                                        <div class="input-group date" id='todate'>
                                            <input type="text" class="form-control" data-ng-model="SpaceRequisition.ToDate" id="ToDate" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.ToDate.$invalid" style="color: red;">Please To Date</span>
                                    </div>

                                </div>


                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmSpaceRequisition.$submitted && frmSpaceRequisition.Request_Type.$invalid}">
                                        <label for="txtcode">Request Type</label>
                                        <select class="selectpicker" data-ng-model="SpaceRequisition.Request_Type" id="Request_Type" name="Request_Type" required="">
                                            <option value="ALL" selected>--ALL--</option>
                                            <option value="Space Requisition">Space Requisition</option>
                                            <option value="Space Extension">Space Extension</option>
                                            <option value="Vertical Requisition">Vertical Requisition</option>
                                        </select>
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.Request_Type.$invalid" style="color: red;">Please Select Request Type</span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                    <div class="form-group">
                                        <label class="control-label">Company</label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="SpaceRequisition.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                            item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="SpaceRequisition.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmSpaceRequisition.$submitted && frmSpaceRequisition.Company.$invalid" style="color: red">Please Select Company </span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <div class="box-footer text-right">
                                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                        <%--<a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>--%>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row" style="padding-left: 18px">
                                <div class="col-md-6">
                                    <label>View In : </label>
                                    <input id="viewswitch" type="checkbox" checked data-size="medium"
                                        data-on-text="<span class='fa fa-table'></span>"
                                        data-off-text="<span class='fa fa-bar-chart'></span>" />

                                </div>
                                <div class="col-md-6" id="table2" data-ng-show="GridVisiblity2">
                                    <br />
                                    <a data-ng-click="GenReport(SpaceRequisition,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(SpaceRequisition,'xlsx')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(SpaceRequisition,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                            </div>
                            <%--  <div class="col-md-6">
                                <div class="box-footer text-right">
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" />--%>
                            <%--<a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>--%>
                            <%--       </div>
                            </div>--%>

                            <%-- </div>--%>
                            <%-- </form>

                            <form id="form2">--%>
                            <div id="Tabular">
                                <%-- <div class="row">
                                    <a data-ng-click="GenReport(SpaceRequisition,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0"  title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(SpaceRequisition,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(SpaceRequisition,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>--%>
                                <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                    <%-- <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />--%>
                                    <div class="input-group" style="width: 20%">
                                        <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary custom-button-color" type="submit">
                                                <i class="glyphicon glyphicon-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>

                                </div>
                            </div>
                            <div id="Graphicaldiv" data-ng-show="GridVisiblity">
                                <div id="SpcGraph">&nbsp</div>
                            </div>
                        </form>
                        <div class="modal fade bs-example-modal-lg col-md-12 " id="historymodal">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="panel-group box box-primary" id="Div2">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <div class="panel-heading ">
                                                <h4 class="panel-title modal-header-primary" style="padding-left: 17px" data-target="#collapseTwo">History Details</h4>
                                                <form role="form" name="form2" id="form4">
                                                    <div class="clearfix">
                                                        <div class="col-md-12">
                                                            <div class="box">
                                                                <div class="box-danger table-responsive">
                                                                    <div data-ag-grid="PopOptions" class="ag-blue" style="height: 305px; width: 100%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/jspdf.min.js"></script>
    <script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script defer src="../Js/SpaceRequisition_Report.min.js"></script>
    <script defer type="text/javascript">
        var CompanySession = '<%= Session["COMPANYID"]%>';
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

        }
    </script>
    <script defer src="../../Utility.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
</body>
</html>
