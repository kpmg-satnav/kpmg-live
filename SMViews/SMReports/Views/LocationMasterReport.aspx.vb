﻿Imports Microsoft.Reporting.WebForms
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Configuration.ConfigurationManager
Partial Class SMViews_SMReports_Views_LocationMasterReport
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            LoadReport()
        End If
    End Sub
    Public Sub LoadReport()
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "EXPORT_MASTER_DATA")
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            Dim rds As New ReportDataSource()
            rds.Name = "LocMasterDS"
            rds.Value = ds.Tables(0)
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Space_Mgmt/LocMasterRpt.rdlc")
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.Visible = True
            ReportViewer1.LocalReport.EnableExternalImages = True
            '' Dim imagePath As String = New Uri(Server.MapPath("~/BootStrapCSS/images/logo_quick.gif")).AbsoluteUri
            ''Dim parameter As New ReportParameter("ImagePath", imagePath)
            ''ReportViewer1.LocalReport.SetParameters(parameter)
            ReportViewer1.LocalReport.DisplayName = "Location Master Report"
            ReportViewer1.LocalReport.Refresh()
        End If

    End Sub
End Class
