﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <%--    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="CostReportController" class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Space Requisition" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Space Cost Report</h3>
                </div>
                <form id="RptArea" name="frmrptArea" data-valid-submit="GetCostBindGrid()">
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Country</label>
                                <div isteven-multi-select data-input-model="Country" data-output-model="rptArea.Country" data-button-label="icon CNY_NAME"
                                    data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">City</label>
                                <div isteven-multi-select data-input-model="City" data-output-model="rptArea.City" data-button-label="icon CTY_NAME"
                                    data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Location</label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="rptArea.Locations" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Tower</label>
                                <div isteven-multi-select data-input-model="Towers" data-output-model="rptArea.Towers" data-button-label="icon TWR_NAME "
                                    data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix" style="padding-top: 30px">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Floor</label>
                                <div isteven-multi-select data-input-model="Floors" data-output-model="rptArea.Floors" data-button-label="icon FLR_NAME"
                                    data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Company</label>
                                <div isteven-multi-select data-input-model="Company" data-output-model="rptArea.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                    item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="rptArea.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmrptArea.$submitted && frmrptArea.Company.$invalid" style="color: red">Please Select Company </span>
                            </div>
                        </div>
                        <br />
                        <div class="box-footer text-left" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                            <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <div class="row" style="padding-left: 18px">
                            <div class="col-md-6">
                                <label>View In : </label>
                                <input id="viewswitch" type="checkbox" checked data-size="small" data-on-text="<span class='fa fa-table'></span>"
                                    data-off-text="<span class='fa fa-bar-chart'></span>" />
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12" id="table2" data-ng-show="GridVisiblity">
                                <br />
                                <a data-ng-click="GenReport(rptArea,'doc')"><i id="word" data-ng-show="DocTypeVisible==0" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(rptArea,'xlsx')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(rptArea,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                            </div>
                        </div>
                        <br />

                        <div id="Tabular">
                            <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>
                        <div id="Graphicaldiv">
                            <div id="TotalGraph">&nbsp</div>
                            <div id="SpcGraph">&nbsp</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script defer src="../../../Scripts/jspdf.min.js"></script>
    <script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script defer src="../../Utility.min.js"></script>
    <script defer src="../Js/CostReport.js"></script>
</body>
</html>
