﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        
    </style>
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body data-ng-controller="SpaceAreaReportController" class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Area Report" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Area Report </h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="AreaReport" name="AreaReport" data-valid-submit="LoadData()">
                        <div class="clearfix" data-ng-show="CompanyVisible==0">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Company</label>
                                    <div isteven-multi-select data-input-model="Company" data-output-model="AreaReport.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                        item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="AreaReport.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="AreaReport.$submitted && AreaReport.Company.$invalid" style="color: red">Please Select Company </span>
                                </div>
                            </div>
                            <br />
                            <div class="box-footer text-left" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>
                        <div class="row" style="padding-left: 18px">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label>View In : </label>
                                <input id="viewswitch" type="checkbox" checked data-size="small" data-on-text="<span class='fa fa-table'></span>"
                                    data-off-text="<span class='fa fa-bar-chart'></span>" />
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12" id="table2" data-ng-show="GridVisiblity">
                                <br />
                                <a data-ng-click="GenReport(AreaReport,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(AreaReport,'xlsx')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(AreaReport,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                            </div>
                        </div>
                        <br />
                        <div id="Tabular">
                            <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <%--<input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />--%>
                                <div class="input-group" style="width: 20%">
                                    <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary custom-button-color" type="submit">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>
                        <div id="Graphicaldiv">
                            <div id="SpcGraph">&nbsp</div>
                            <div id="TotalGraph">&nbsp</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script defer src="../../../Scripts/jspdf.min.js"></script>
    <script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script defer src="../Js/AreaReport.min.js"></script>
    <script defer src="../../Utility.min.js"></script>
</body>
</html>
