﻿<%@ Page Language="C#" %>

<!DOCTYPE html>




<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/bootstrap-timepicker.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/qtip/qtip.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/fullcalendar_V3.0.css" rel="stylesheet" />


    <style>
        .fc-content {
            color: white;
            background: green;
            font-size: 15px;
            text-align: center;
            border: 1px solid blue;
        }

            .fc-content:hover {
                color: whtie;
                background: black;
                border-radius: 50%;
                transition: 1s;
            }

        .fc-toolbar .fc-center {
            color: white;
        }

        .fc-toolbar {
            background: #00DAB9;
        }

        .fc-button {
            color: blue;
        }

            .fc-button:hover {
                background: black;
                color: #fff;
                transition: .5s;
            }

        .fc-day-header {
            color: white;
            background: black;
        }

        .fc-day-grid {
            background: #e6fffb;
        }

        .fc-today {
            background: pink !important;
        }

        .fc-day-number {
            color: white;
            background: black;
            border-radius: 100%;
            border: 1px dashed red;
        }

            .fc-day-number:hover {
                color: black;
                background: white;
                border: 1px dashed orange;
                transition: 1s;
            }

        /*.timepicker {
            z-index: 1151 !important;
        }*/


        .fc-time {
            color: white !important;
        }

        .fc-title {
            color: white !important;
        }

        .grid-align {
            text-align: center;
        }


        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .calender-div {
            align-items: center;
            padding: 2% 25% 2% 25%;
            background: #E9EDEE;
        }

        #calendar {
            display: block;
        }
    </style>
</head>
<body data-ng-controller="SpaceHistoryReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Space Seat Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Space Calender Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <div class="clearfix">


                                <form id="form1" name="spaceHistoryReport" data-valid-submit="LoadData()" novalidate>

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': spaceHistoryReport.$submitted && spaceHistoryReport.CNY_NAME.$invalid}">
                                            <label class="control-label">Country <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Country" data-output-model="spaceHistoryReport.Country" data-button-label="icon CNY_NAME"
                                                data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="spaceHistoryReport.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="spaceHistoryReport.$submitted && spaceHistoryReport.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': spaceHistoryReport.$submitted && spaceHistoryReport.CTY_NAME.$invalid}">
                                            <label class="control-label">City<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="City" data-output-model="spaceHistoryReport.City" data-button-label="icon CTY_NAME"
                                                data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="spaceHistoryReport.City[0]" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="spaceHistoryReport.$submitted && spaceHistoryReport.CTY_NAME.$invalid" style="color: red">Please select city </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': spaceHistoryReport.$submitted && spaceHistoryReport.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Locations" data-output-model="spaceHistoryReport.Locations" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="spaceHistoryReport.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="spaceHistoryReport.$submitted && spaceHistoryReport.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': spaceHistoryReport.$submitted && spaceHistoryReport.TWR_NAME.$invalid}">
                                            <label class="control-label">Tower <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Towers" data-output-model="spaceHistoryReport.Towers" data-button-label="icon TWR_NAME "
                                                data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="spaceHistoryReport.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="spaceHistoryReport.$submitted && spaceHistoryReport.TWR_NAME.$invalid" style="color: red">Please select tower </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': spaceHistoryReport.$submitted && spaceHistoryReport.FLR_NAME.$invalid}">
                                            <label class="control-label">Floor <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Floors" data-output-model="spaceHistoryReport.Floors" data-button-label="icon FLR_NAME"
                                                data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="spaceHistoryReport.Floors[0]" name="FLR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="spaceHistoryReport.$submitted && spaceHistoryReport.FLR_NAME.$invalid" style="color: red">Please select floor </span>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="col-md-12 col-sm-6 col-xs-12" style="padding-bottom:5px">
                                    <div class="box-footer text-right">
                                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                                    </form>

                                <div class="col-md-12 calender-div" >
                                    <div  id='calendar' style="width: 100%"></div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12" style ="padding-top:5px ">
                                        <div class="input-group" style="width: 100%">
                                            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                           
                                        </div>
                                    </div>
                                     <div class="col-md-3  text-left" style ="padding-top:5px">
                                                <label>Total No.of Requests Fetched : {{TotalReq}}</label>
                                            </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="padding-top:10px"">
                                        <div class="form-group text-right">
                                            <a ng-class="{'adisabled': !usable}" id="anchExportExcel"><i id="I3" style="padding-right: 20px" data-toggle="tooltip" title="Export to Excel"
                                            class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto;padding-top:10px"></div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
    <script src="../../../Scripts/bootstrap-timepicker.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/fullcalendar_V3.0.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "angucomplete-alt"]);
    </script>
    <script src="../../../SMViews/Utility.js" defer></script>
    <script src="../Js/SpaceHistoryReportCal.js"></script>


    <script src="../../../BootStrapCSS/qtip/qtip.js" defer></script>
    <script>

</script>
</body>

</html>
