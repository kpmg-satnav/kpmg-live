﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%----%>
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        .word {
            color: #4813CA;
        }

        .pdf {
            color: #FF0023;
        }

        .excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }


        .with-nav-tabs.panel-primary .nav-tabs > .open > a,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
            color: #fff;
            background-color: #3071a9;
            border-color: transparent;
        }

        .with-nav-tabs.panel-primary .nav-tabs > li.active > a,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
            color: #428bca;
            background-color: #fff;
            border-color: #428bca;
            border-bottom-color: transparent;
        }
    </style>
</head>
<body data-ng-controller="SpaceConsolidatedReportController" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Space Allocation" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">Space Consolidated Report</h3>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <div class="clearfix row">
                            <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                <div class="form-group">
                                    <label class="control-label">Company</label>
                                    <div isteven-multi-select data-input-model="Company" data-output-model="Consolidated.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                        item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="Consolidated.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmConsolidated.$submitted && frmConsolidated.Company.$invalid" style="color: red">Please Select Company </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 18px">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="BindGrid()" />
                            </div>

                            <div style="padding-left: 18px">
                                <div class="col-md-3  col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">View In : </label>
                                        <div data-ng-show="showsummary">
                                            <input id="viewswitch" type="checkbox" checked data-size="medium"
                                                data-on-text="<span class='fa fa-table'></span>"
                                                data-off-text="<span class='fa fa-bar-chart'></span>" />
                                        </div>
                                        <%-- <div data-ng-show="showproject"><input id="viewswitchprj" type="checkbox" checked data-size="medium"
                                                      data-on-text="<span class='fa fa-table'></span>"
                                                         data-off-text="<span class='fa fa-bar-chart'></span>" />
                                         </div>
                                        <div data-ng-show="showlocation"><input id="viewswitchloc" type="checkbox" checked data-size="medium"
                                                      data-on-text="<span class='fa fa-table'></span>"
                                                         data-off-text="<span class='fa fa-bar-chart'></span>" />
                                         </div>--%>
                                    </div>
                                </div>
                                <div class="col-md-3  col-sm-6 col-xs-12" id="table1" data-ng-show="GridVisiblity">
                                    <br />
                                    <a data-ng-click="GenReport('doc')"><i data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="word fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport('xls')"><i data-toggle="tooltip" title="Export to Excel" class="excel fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport('pdf')"><i data-toggle="tooltip" title="Export to Pdf" class="pdf fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                                <div class="col-md-3  col-sm-6 col-xs-12" id="table2" data-ng-show="GridVisiblityproject">
                                    <br />
                                    <%--                                         <a data-ng-click="GenReport('doc')"><i id="I1" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>--%>
                                    <a data-ng-click="GenReportproject('xls')"><i data-toggle="tooltip" title="Export to Excel" class="excel fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReportproject('pdf')"><i data-toggle="tooltip" title="Export to Pdf" class=" pdf fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                                <div class="col-md-3  col-sm-6 col-xs-12" id="table3" data-ng-show="GridVisiblitylocation">
                                    <br />
                                    <%--<a data-ng-click="GenReport('doc')"><i id="I1" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>--%>
                                    <a data-ng-click="GenReportlocation('xls')"><i data-toggle="tooltip" title="Export to Excel" class="excel fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReportlocation('pdf')"><i data-toggle="tooltip" title="Export to Pdf" class="pdf fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <br />

                        <form id="form2" class="row">
                            <div id="Tabular">
                                <%--<div class="row">
                                    <a data-ng-click="GenReport('doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport('xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport('pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>--%>

                                <%--<div class="row">--%>
                                <div class="col-md-12">
                                    <div class="panel-heading ">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tabSummary" data-ng-click="BindGrid()" data-toggle="tab">Summary</a></li>
                                            <li><a href="#tabProject" data-ng-click="BindGrid_Project()" data-toggle="tab">Project Wise</a></li>
                                            <li><a href="#tabLocation" data-ng-click="BindGrid_Location()" data-toggle="tab">Location Wise</a></li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="tabSummary">

                                                <%--<div class="input-group" style="width: 20%">
                                                        <input type="text" class="form-control" id="filtertxt" placeholder="Search" name="srch-term" />
                                                        <div class="input-group-btn">
                                                            <button class="btn btn-primary custom-button-color" type="submit" data-ng-click="BindGrid()">
                                                                <i class="glyphicon glyphicon-search"></i>
                                                            </button>
                                                        </div>
                                                    </div>--%>
                                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                            </div>
                                            <div class="tab-pane fade" id="tabLocation">
                                                <%--<div class="input-group" style="width: 20%">
                                                        <input type="text" class="form-control" id="filteredloc" placeholder="Search" name="srch-term" />
                                                        <div class="input-group-btn">
                                                            <button class="btn btn-primary custom-button-color" type="submit" data-ng-click="BindGrid_Location()">
                                                                <i class="glyphicon glyphicon-search"></i>
                                                            </button>
                                                        </div>
                                                    </div>--%>

                                                <input type="text" class="form-control" id="filteredloc" placeholder="Filter by any..." style="width: 25%" />
                                                <div data-ag-grid="gridLocation" class="ag-blue" style="height: 310px; width: auto"></div>
                                            </div>
                                            <div class="tab-pane fade" id="tabProject">
                                                <%--<div class="input-group" style="width: 20%">
                                                        <input type="text" class="form-control" id="filteredproj" placeholder="Search" name="srch-term" />
                                                        <div class="input-group-btn">
                                                            <button class="btn btn-primary custom-button-color" type="submit" data-ng-click="BindGrid_Location()">
                                                                <i class="glyphicon glyphicon-search"></i>
                                                            </button>
                                                        </div>
                                                    </div>--%>
                                                <input type="text" class="form-control" id="filteredproj" placeholder="Filter by any..." style="width: 25%" />
                                                <div data-ag-grid="Options" class="ag-blue" style="height: 310px; width: auto"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--</div>--%>
                            </div>
                            <div id="Graphicaldiv">
                                <div id="SpaceConsolidatedGraph">&nbsp</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script defer src="../../../Scripts/jspdf.min.js"></script>
    <script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer>
        //agGrid.initialiseAgGridWithAngular1(angular);
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script defer src="../../../Scripts/JSONToCSVConvertor.js"></script>
    <script defer src="../Js/SpaceConsolidatedReport.js"></script>

    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../Utility.min.js"></script>
</body>
</html>
