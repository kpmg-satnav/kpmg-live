﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Space Planning Report</title>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-body-container {
            color: black;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
            /*border-bottom: 1px solid #eee;*/
            /*background-color: #428bca;*/
            /*-webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;*/
        }

        .word {
            color: #4813CA;
        }

        .pdf {
            color: #FF0023;
        }

        .excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="SpacePlanningReportController" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">Space Planning Report</h3>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <form id="form1" name="frmSpacePlanningRpt" data-valid-submit="search()">
                            <%--<div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpacePlanningRpt.$submitted && frmSpacePlanningRpt.CTY_NAME.$invalid}">
                                            <label class="control-label">City<span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="City" data-output-model="SpacePlanningRpt.City" data-button-label="icon CTY_NAME"
                                                data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpacePlanningRpt.City[0]" name="CTY_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpacePlanningRpt.$submitted && frmSpacePlanningRpt.CTY_NAME.$invalid">Please select city </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpacePlanningRpt.$submitted && frmSpacePlanningRpt.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Locations" data-output-model="SpacePlanningRpt.Locations" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpacePlanningRpt.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpacePlanningRpt.$submitted && frmSpacePlanningRpt.LCM_NAME.$invalid">Please select location </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpacePlanningRpt.$submitted && frmSpacePlanningRpt.TWR_NAME.$invalid}">
                                            <label class="control-label">Facility Name <span style="color: red;">**</span></label>
                                            <div isteven-multi-select data-input-model="Towers" data-output-model="SpacePlanningRpt.Towers" data-button-label="icon TWR_NAME "
                                                data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpacePlanningRpt.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpacePlanningRpt.$submitted && frmSpacePlanningRpt.TWR_NAME.$invalid">Please select tower </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpacePlanningRpt.$submitted && frmSpacePlanningRpt.VER_NAME.$invalid}">
                                            <label class="control-label">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                            <div isteven-multi-select  data-input-model="Verticals" data-output-model="SpacePlanningRpt.Verticals" data-button-label="icon VER_NAME"
                                                data-item-label=" icon VER_NAME maker" data-on-item-click="VerticalChange()" data-on-select-all="VerticalChangeAll()" data-on-select-none="verticalSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpacePlanningRpt.Verticals[0]" name="VER_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpacePlanningRpt.$submitted && frmSpacePlanningRpt.VER_NAME.$invalid">Please select {{BsmDet.Parent |lowercase}} </span>
                                        </div>
                                    </div>
                                </div>--%>
                            <div class="clearfix">
                                <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmSpacePlanningRpt.$submitted && frmSpacePlanningRpt.Cost_Center_Name.$invalid}">
                                            <label class="control-label">{{BsmDet.Child}}<span style="color: red;">**</span></label>
                                            <div isteven-multi-select  data-input-model="CostCenters" data-output-model="SpacePlanningRpt.CostCenters" data-button-label="icon Cost_Center_Name"
                                                data-on-item-click="CostCenterChange()" data-on-select-all="CostCenterChangeAll()" data-on-select-none="CostCenterSelectNone()"
                                                data-item-label=" icon Cost_Center_Name maker" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="SpacePlanningRpt.CostCenters[0]" name="Cost_Center_Name" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpacePlanningRpt.$submitted && frmSpacePlanningRpt.Cost_Center_Name.$invalid">Please select {{BsmDet.Child|lowercase}}  </span>
                                        </div>
                                    </div>--%>
                                <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                        <div class="form-group">
                                            <label class="control-label">Company</label>
                                            <div isteven-multi-select data-input-model="Company" data-output-model="SpacePlanningRpt.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                                item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="SpacePlanningRpt.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmSpacePlanningRpt.$submitted && frmSpacePlanningRpt.Company.$invalid" style="color: red">Please Select Company </span>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-8" data-ng-show="CompanyVisible==0">
                                        <div class="form-group">

                                            <label class="control-label">Type</label>
                                            <br />
                                            <select name="type" ng-model="select" ng-dropdown required ng-options="option.type for option in options" class="selectpicker">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 20px;">
                                        <div class="box-footer text-left">
                                            <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row" style="padding-left: 18px; padding-top: 18px">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label>View In : </label>
                                    <input id="viewswitch" type="checkbox" checked data-size="medium"
                                        data-on-text="<span class='fa fa-table'></span>"
                                        data-off-text="<span class='fa fa-bar-chart'></span>" />
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12" id="table2">
                                    <div class="col-md-5  col-sm-6 col-xs-12" data-ng-show="GridVisiblitylocation">
                                        <br />
                                        <a data-ng-click="GenReport(SpacePlanningRpt,'doc')"><i data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="word fa fa-file-word-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReport(SpacePlanningRpt,'xls')"><i data-toggle="tooltip" title="Export to Excel" class="excel fa fa-file-excel-o fa-2x pull-right"></i></a>
                                        <%--<a data-ng-click="GenReport(SpacePlanningRpt,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>--%>
                                    </div>
                                    <div class="col-md-5  col-sm-6 col-xs-12" data-ng-show="GridVisiblityvertical">
                                        <br />
                                        <a data-ng-click="GenReportvertical(SpacePlanningRpt,'xls')"><i data-toggle="tooltip" title="Export to Excel" class="excel fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <br />
                        </form>
                        <form id="form2">
                            <div id="Tabular">
                                <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px" ng-show="locationgrid">
                                    <div class="input-group" style="width: 20%">
                                        <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary custom-button-color" type="submit">
                                                <i class="glyphicon glyphicon-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: 100%"></div>
                                </div>
                                <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px" ng-show="Verticalgrid">
                                    <%--<input type="text" class="form-control" id="filteredtxt" placeholder="Filter by any..." style="width: 25%" />--%>
                                    <div data-ag-grid="gridVerticalOptions" class="ag-blue" style="height: 310px; width: 100%"></div>
                                </div>
                                <%--<div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px" ng-if="select.type == 'Cost_Center'">
                                        <input type="text" class="form-control" id="filtertext" placeholder="Filter by any..." style="width: 25%" />
                                        <div data-ag-grid="gridcostcenterOptions" class="ag-blue" style="height: 310px; width: 100%"></div>
                                    </div>--%>
                            </div>
                            <div id="Graphicaldiv" data-ng-show="GridVisiblity">
                                <div id="SpcGraph">&nbsp</div>

                                <%--   <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px" ng-if="selected.type == 'Vertical'">
                                        <input type="text" class="form-control" id="filteredtxt" placeholder="Filter by any..." style="width: 25%" />
                                        <div data-ag-grid="gridVerticalOptions" class="ag-blue" style="height: 310px; width: 100%"></div>
                                    </div>--%>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/jspdf.min.js"></script>
    <script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <%--<script defer src="../Js/SpacePlanningReport.js"></script>--%>
    <script defer src="../Js/SpacePlanningReport.min.js"></script>
    <script defer src="../../Utility.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script defer type="text/javascript">
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
</body>
</html>
