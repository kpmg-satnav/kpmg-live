﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script defer>

        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

        };
    </script>
    <style>
        #pageRowSummaryPanel {
            display: none;
        }

        .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        
    </style>
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>





</head>
<body data-ng-controller="CustomizedReportController" class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Area Report" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Customizable Report </h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" name="CustomizedReport" data-valid-submit="LoadData()" novalidate>

                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CNY_NAME.$invalid}">
                                    <label class="control-label">Country <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Country" data-output-model="Customized.Country" data-button-label="icon CNY_NAME"
                                        data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Customized.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CTY_NAME.$invalid}">
                                    <label class="control-label">City<span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="City" data-output-model="Customized.City" data-button-label="icon CTY_NAME"
                                        data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Customized.City[0]" name="CTY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CTY_NAME.$invalid" style="color: red">Please select city </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid}">
                                    <label class="control-label">Location <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Locations" data-output-model="Customized.Locations" data-button-label="icon LCM_NAME"
                                        data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Customized.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.TWR_NAME.$invalid}">
                                    <label class="control-label">Tower <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Towers" data-output-model="Customized.Towers" data-button-label="icon TWR_NAME "
                                        data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Customized.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.TWR_NAME.$invalid" style="color: red">Please select tower </span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.FLR_NAME.$invalid}">
                                    <label class="control-label">Floor <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Floors" data-output-model="Customized.Floors" data-button-label="icon FLR_NAME"
                                        data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Customized.Floors[0]" name="FLR_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FLR_NAME.$invalid" style="color: red">Please select floor </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.Request_Type.$invalid}">
                                    <label class="control-label">Status Type <span style="color: red;">*</span></label>

                                    <select id="Request_Type" name="Request_Type" required="" data-ng-model="Customized.Request_Type" class="form-control">
                                        <option value="">--Select--</option>
                                        <option ng-repeat="sta in statuslst" value="{{sta.Code}}">{{sta.Name}}</option>
                                    </select>
                                    <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Request_Type.$invalid" style="color: red">Please Select Status</span>
                                </div>
                            </div>


                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.Columns.$invalid}">
                                    <label class="control-label">Select Columns <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Cols" data-output-model="COL" button-label="icon COL" item-label="icon COL" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="COL" name="Columns" style="display: none" required="" />
                                    <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Columns.$invalid" style="color: red">Please Select Columns </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CNP_NAME.$invalid}">
                                    <label class="control-label">Company</label>
                                    <div isteven-multi-select data-input-model="Company" data-output-model="Customized.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                        item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="Customized.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Company.$invalid" style="color: red">Please Select Company </span>
                                </div>
                            </div>

                        </div>
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CHE_NAME.$invalid}">
                                    <label class="control-label">Entity</label>
                                    <div isteven-multi-select data-input-model="Entity" data-output-model="Customized.CHE_NAME" button-label="icon CHE_NAME" data-is-disabled="EnableStatus==0"
                                        item-label="icon CHE_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Customized.CHE_NAME" name="CHE_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Entity.$invalid" style="color: red">Please Select Company </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Select Range</label>
                                    <br />
                                    <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                        <option value="Select">Select</option>
                                        <option value="TODAY">Today</option>
                                        <option value="YESTERDAY">Yesterday</option>
                                        <option value="7">Last 7 Days</option>
                                        <option value="30">Last 30 Days</option>
                                        <option value="THISMONTH">This Month</option>
                                        <option value="LASTMONTH">Last Month</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">From Date</label>
                                    <div class="input-group date" id='fromdate'>
                                        <input type="text" class="form-control" data-ng-model="Customized.FromDate" id="FromDate" name="FromDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                        </span>
                                    </div>
                                    <%--<span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FromDate.$invalid" style="color: red;">Please Select From Date</span>--%>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">To Date</label>
                                    <div class="input-group date" id='todate'>
                                        <input type="text" class="form-control" data-ng-model="Customized.ToDate" id="ToDate" name="ToDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                        </span>
                                    </div>
                                    <%--<span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.ToDate.$invalid" style="color: red;">Please Select To Date</span>--%>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-11 col-sm-6 col-xs-12">
                            <br />
                            <div class="box-footer text-right">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-11 col-sm-6 col-xs-12" id="table2" data-ng-show="GridVisiblity" style="padding-top: 10px">
                                <a data-ng-click="GenReport(Customized,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(Customized,'xlsx')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(Customized,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                            </div>
                        </div>

                        <div id="Tabular" data-ng-show="GridVisiblity2">
                            <%--<div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                        <div class="input-group" style="width: 20%">
                                            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary custom-button-color" type="submit">
                                                    <i class="glyphicon glyphicon-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                    </div>--%>
                            <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="../../../Dashboard/C3/c3.min.js"></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script defer src="../../../Scripts/jspdf.min.js"></script>
    <script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';


        //function setDateVals() {
        //    $('#FromDate').datepicker({
        //        format: 'mm/dd/yyyy',
        //        autoclose: true,
        //        todayHighlight: true
        //    });
        //    $('#ToDate').datepicker({
        //        format: 'mm/dd/yyyy',
        //        autoclose: true,
        //        todayHighlight: true
        //    });
        //    $('#FromDate').datepicker('setDate', new Date(moment().startOf('month').format('MM/DD/YYYY')));
        //    $('#ToDate').datepicker('setDate', new Date(moment().endOf('month').format('MM/DD/YYYY')));
        //}





    </script>
    <script defer src="../../Utility.min.js"></script>
    <script defer src="../Js/CustomizedReport.js"></script>
    <script defer type="text/javascript">
        var CompanySession = '<%= Session["COMPANYID"]%>';
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

        }
    </script>
</body>
</html>
