﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <%--    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="CostSummaryReportController" class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Space Requisition" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Cost Summary Report</h3>
                </div>
                <form id="SummaryReport" name="SummaryReport" data-valid-submit="GetCostBindGrid()">
                    <div class="clearfix">

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Location</label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="SummaryReport.Locations" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">{{BsmDet.Parent}}</label>
                                <div isteven-multi-select data-input-model="Verticals" data-output-model="SummaryReport.Verticals" data-button-label="icon VER_NAME"
                                    data-item-label=" icon VER_NAME maker" data-on-select-all="verticalChangeAll()" data-on-select-none="verticalSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>

                            </div>
                        </div>

                        <br />
                        <div class="box-footer text-left" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                            <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <div class="row" style="padding-left: 18px">
                            <div class="col-md-6 col-sm-6 col-xs-12" id="table2">
                                <br />
                                <a data-ng-click="GenReport('xlsx')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>

                            </div>
                        </div>
                        <br />
                        <div id="Tabular">
                            <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: 850px"></div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script defer src="../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script defer src="../../../Scripts/jspdf.min.js"></script>
    <script defer src="../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script defer src="../../Utility.min.js"></script>
    <script src="../Js/CostSummary.js"></script>
</body>
</html>
