﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

     <script type="text/javascript" defer>

         function setup(id) {
             $('#' + id).datepicker({
                 format: 'mm/dd/yyyy',
                 autoclose: true,
                 todayHighlight: true
             });
         };
     </script>
  <style>
        .grid-align {
            text-align: center;
        }
        .list-group-item {
                position: relative;
    display: block;
    
    background-color: #fff;
    border: 1px solid #ddd;

        }

        .column {
            float: left;
            width: 50%;
            padding: 6px;
        }

        /* Clear floats after the columns */
        .row:after {
           
            content: "";
            display: table;
            clear: both;
        }
    </style>
    
</head>
    

<body  class="amantra">
     <div class="animsition" data-ng-controller="SpaceSeatBookingHistoryController">
        <div class="container-fluid page-content-inner">    
           
               <div ba-panel ba-panel-title="Space Seat Book" ba-panel-class="with-scroll">


                    <div class="panel">
                         <div class="panel-heading" style="height: 41px; background-color:#0e2f5d ">
                            <h3 class="panel-title"  style="color:#fff" >My Bookings</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                                <form id="form1" name="SpaceSeatBookingHistory" novalidate>
                                     <div class="col-md-0">
                                    <div class="form-group">
                                               <div data-ng-show="IsMobile"  style="padding:inherit">

                                            <ul class="list-group" id="ulThemes">
                                                <li class="list-group-item" data-ng-repeat="item in data" >
                                                    <div class="row">
                                                        <div class="column">
                                                            <label class="form-label" style="color: #0e2f5d;font-size: 12px">Space Id :</label>
                                                            <br/>
                                                            <label class="form-label" style="color: rgb(0,0,0); font-size: 10px" data-ng-bind="item.Space_ID"></label>
                                                        </div>

                                                      

                                                        <div class="column">
                                                            <label class="form-label" style="color: #0e2f5d;font-size: 12px">Created By :</label>
                                                          <br/>
                                                            <label class="form-label" style="color: rgb(0,0,0); font-size: 10px" data-ng-bind="item.CREATED_BY"></label>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="column">
                                                            <label class="form-label" style="color: #0e2f5d;font-size: 12px">From Date :</label>
															<br/>
                                                            <label class="form-label" style="color: rgb(0,0,0); font-size: 10px" data-ng-bind="item.FROM_DATE"></label>
                                                            <label class="form-label" style="color: rgb(0,0,0); font-size: 10px" data-ng-bind="item.FROM_TIME"></label>
                                                        </div>
                                                        <div class="column">
                                                            <label class="form-label" style="color: #0e2f5d;font-size: 12px">To Date :</label>
															<br/>
                                                            <label class="form-label" style="color: rgb(0,0,0); font-size: 10px" data-ng-bind="item.TO_DATE"></label>
                                                            <label class="form-label" style="color: rgb(0,0,0); font-size: 10px" data-ng-bind="item.TO_TIME"></label>

                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="column">
                                                            <label class="form-label" style="color: #0e2f5d;font-size: 12px">Status :</label><br />
                                                            <label class="form-label" style="color: rgb(0,0,0); font-size: 10px" data-ng-bind="item.SEAT_STATUS"></label>
                                                        </div>
                                                        <div class="column">
                                                            <button class="btn-primary"  style="width:100px;height:30px;font-size:12px" data-ng-click="release(item)"  data-ng-if="item.SSAD_STA_ID=='1004'">RELEASE</button>

                                                        </div>
                                                    </div>


                                                </li>



                                                <br/>

                                            </ul>

                                          

                                        </div>

                            <div class="row" style="padding-left: 30px" data-ng-show="IsWeb">
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>

                                        

 </div>
                                    </div>
    </form>

                        </div>
                    </div>






                </div>
           
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
   <%-- <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>--%>
   
    <%--<script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Utility.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>
    <script src="../Js/SpaceSeatBookingHistory.js"></script>--%>




     <script defer>
         var app = angular.module('QuickFMS', ["agGrid"]);
     </script>
    <script defer src="../../Utility.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script defer src="../Js/SpaceSeatBookingHistory.js" ></script>
</body>
</html>

