﻿
app.directive('validSubmit', ['$parse', function ($parse) {
    return {
        require: 'form',
        link: function (scope, element, iAttrs, form) {
            form.$submitted = false;
            var fn = $parse(iAttrs.validSubmit);
            element.on('submit', function (event) {
                scope.$apply(function () {
                    form.$submitted = true;
                    if (form.$valid) {
                        fn(scope, { $event: event });
                        form.$submitted = false;
                    }
                });
            });
        }
    };
}
]);

app.service("UtilityService", ['$http','$q', function ($http, $q) {

    /////////// For DEV
    this.path = window.location.origin;
    this.RegExpCode = {};
    this.RegExpName = {};
    this.RegExpRemarks = {};
    this.RegExpPhone = {};
    this.RegExpEmail = {};
    this.RegExpPassword = {};
    this.RegExpDate = {};
    this.RegExpAddress = {};
    this.RegExpTxt = {};
    this.RegExpLat = {};
    this.RegExpLong = {};
    this.RegExpFax = {};
    this.RegExpMobile = {};
    this.RegExpNumber = {};


    this.RegExpCode.REGEXP = /^[ a-zA-Z0-9-_/:.() ]*$/;
    this.RegExpCode.REGMSG = "Please Enter Code in Alphanumeric, Special Character (Space-/_:) Allowed";
    this.RegExpName.REGEXP = /^[ a-zA-Z0-9-/_:&.() ]*$/;
    this.RegExpName.REGMSG = "Please Enter Name in Alphanumeric, Special Character (Space-/_&:) Allowed";
    this.RegExpRemarks.REGEXP = /^[ a-zA-Z0-9-/_@$&*.)(,]*$/;
    this.RegExpRemarks.REGMSG = "Please Enter Remarks upto 500 Characters, special characters (-/_@$)(&*,space) are allowed";
    this.RegExpEmail.REGEXP = /^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z-.]+\.[a-zA-Z.]{2,5}$/;
    this.RegExpEmail.REGMSG = "Please Enter a valid email address, example: you@yourdomain.com";
    this.RegExpPhone.REGEXP = /^[0-9-]+$/;
    this.RegExpPhone.REGMSG = "Please Enter numbers only";
    this.RegExpPassword.REGEXP = /^[a-zA-Z0-9-/_@&$%*^]+$/;
    this.RegExpPassword.REGMSG = "Please Enter password in alphanumeric, special characters (-/_@&$%*^space) are allowed";
    this.RegExpDate.REGEXP = /^[0-9/]+$/;
    this.RegExpDate.REGMSG = "Please Enter Valid Date";
    this.RegExpAddress.REGEXP = /^[ a-zA-Z0-9-/_@$&*)(,#.]*$/;
    this.RegExpAddress.REGMSG = "Please Enter address in alphanumeric, special characters (-/_@$&*)(,#space) are allowed";
    this.RegExpTxt.REGEXP = /^[ a-zA-Z0-9-/_@:&.]*$/;
    this.RegExpTxt.REGMSG = "Please Enter text in alphanumeric, special characters (-/_@&space:) are allowed";
    this.RegExpLat.REGEXP = /^[0-9.]+$/;
    this.RegExpLat.REGMSG = "Please Enter Valid Latitude";
    this.RegExpLong.REGEXP = /^[0-9.]+$/;
    this.RegExpLong.REGMSG = "Please Enter Valid Longitude";
    this.RegExpFax.REGEXP = /^[0-9-]+$/;
    this.RegExpFax.REGMSG = "Please Enter numbers only";
    this.RegExpMobile.REGEXP = /^[0-9-]+$/;
    this.RegExpMobile.REGMSG = "Please Enter numbers only";
    this.RegExpNumber.REGEXP = /^[0-9-]+$/;
    this.RegExpNumber.REGMSG = "Please Enter numbers only";
    //////////////////For UAT database
    // this.path = window.location.origin + "/RCAP";


    var deferred = $q.defer();


    // get employees names for auto complete

    this.GetEmployeeNameAndID = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetEmployeeNameAndID')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };

    this.getSysPreferences = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetSysPreferences')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };

    this.GetCompanies = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetCompanies')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };

    this.GetTypes = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetTypes')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };

    this.GetSubTypes = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetSubTypes')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };

    this.GetBrand = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetBrand')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };

    this.GetModel = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetModel')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };
    //get all users
    this.getallUsers = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetAllUsers')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };


    this.getCountires = function (ID) {
        deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetCountries?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getCities = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetCities?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getLocations = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetLocations?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.GetLocationsall = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetLocationsall?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.Getmaincat = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/Getmaincat')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.Getsubcat = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/Getsubcat')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.Getchildcat = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/Getchildcat')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.Getstatus = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/Getstatus')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };


    this.GetContractStatus = function () {
        var deferred = $q.defer();
        return $http.get(this.path + '/api/Utility/GetContractStatus')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetContractCategories = function (ID) {
        var deferred = $q.defer();
        return $http.get(this.path + '/api/Utility/GetContractCategories?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetContractSubcategories = function (ID) {
        var deferred = $q.defer();
        return $http.get(this.path + '/api/Utility/GetContractSubcategories?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getTowers = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetTowers?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;

    };

    this.getFloors = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetFloors?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getVerticals = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetVerticals?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getAurNames = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/getAurNames')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getAllEmployees = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetAllEmployees?text=' + Id + '')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };

    this.getVerticalsByFlrid = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetVerticalsByFlrid?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getCostCenters = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetCostCenters?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getDepartments = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetDepartments')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getDesignation = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetDesignation')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getCostCentersByFlrid = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetCostCentersByFlrid?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.GetLocationsByCostcenters = function (costlst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetLocationsByCostcenters?id=' + ID + ' ', costlst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.getCostcenterByVertical = function (selectedVerticals, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetCostcenterByVertical?id=' + ID + ' ', selectedVerticals)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getCostcenterByVerticalcode = function (selectedVerticals, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetCostcenterByVerticalcode?id=' + ID + ' ', selectedVerticals)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getCitiesbyCny = function (cnylst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetCitiesBycountry?id=' + ID + ' ', cnylst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.getsubbymain = function (mainlst) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/getsubbymain', mainlst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.getchildbysub = function (sublst) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/getchildbysub', sublst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getLocationsByCity = function (ctylst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetLocationsByCity?id=' + ID + ' ', ctylst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getTowerByLocation = function (lcmlst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetTowerByLocation?id=' + ID + ' ', lcmlst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getFloorByTower = function (twrlst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetFloorByTower?id=' + ID + ' ', twrlst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;

    };
    this.getParentEntity = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetParentEntity?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;

    };

    this.getChildEntity = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetChildEntity?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;

    };

    this.getchildbyparent = function (pelst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetChildEntityByParent?id=' + ID + ' ', pelst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getverticalbychild = function (chelst, ID) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetVerticalByChildEntity?id=' + ID + ' ', chelst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.getBussHeirarchy = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetBussHeirarchy')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.GetRoles = function (ID) {
        var deferred = $q.defer();
        return $http.get(this.path + '/api/Utility/GetRoles?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetCategories = function (ID) {
        var deferred = $q.defer();
        return $http.get(this.path + '/api/Utility/GetCategories?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSubCategories = function (ID) {
        var deferred = $q.defer();
        return $http.get(this.path + '/api/Utility/GetSubCategories?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetBrands = function (ID) {
        var deferred = $q.defer();
        return $http.get(this.path + '/api/Utility/GetBrands?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetRoleByUserId = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetRoleByUserId')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.GetModels = function (ID) {
        var deferred = $q.defer();
        return $http.get(this.path + '/api/Utility/GetModels?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.GetRoleID = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetRoleID')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };


    this.Canceled = 1;
    this.Unchanged = 2;
    this.Added = 4;
    this.Deleted = 8;
    this.Modified = 16;
    this.Approved = 32;
    this.Rejected = 64;
    this.closed = 256;
    this.ReqDate = 'MMM dd, yyyy';

    this.DateValidationOnSubmit = 'To Date Should be greater than From Date';

    // guest house booking

    this.getReservationTypes = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetReservationTypes?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.GetFacilityNamesbyType = function (ID, rtlst) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetFacilityNamesbyType?id=' + ID + ' ', rtlst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };


    this.Get_GH_Timings = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/Get_GH_Timings')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };

    this.GetFacilityNamesbyLocation = function (ID, lcmlst, rtlst) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetFacilityNamesbyLocation?id=' + ID + ' ', lcmlst, rtlst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };


    this.GetReservationTypesByFacility = function (ID, rflst) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetReservationTypesByFacility?id=' + ID + ' ', rflst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    // property 

    this.getPropertyTypes = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetPropTypes')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };


    this.getPropertyNames = function (lcmlst, ptlst) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetProperties', lcmlst, ptlst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };


    // for employee type guest house

    this.GetRoleAndReportingManger = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetRoleAndReportingManger')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.GetEmployee = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetEmployee')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.GetEmployeeTypes = function () {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetEmployeeTypes')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };

    this.GetEmpEmailByID = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetEmpEmailByID?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);

            }, function (response) {
                deferred.reject(response);

            });
        return deferred.promise;
    };

    // end

    // for concur integration room availability page without session start

    this.GetCountriesWithoutSession = function (ID) {
        deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetCountriesWithoutSession?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.GetCitiesWithoutSession = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetCitiesWithoutSession?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.GetLocationsWithoutSession = function (ID) {
        var deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetLocationsWithoutSession?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };


    this.GetFacilityNamesbyLocationWithoutSession = function (ID, lcmlst, rtlst) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetFacilityNamesbyLocationWithoutSession?id=' + ID + ' ', lcmlst, rtlst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };


    this.GetReservationTypesByFacilityWithoutSession = function (ID, rflst) {
        var deferred = $q.defer();
        $http.post(this.path + '/api/Utility/GetReservationTypesByFacilityWithoutSession?id=' + ID + ' ', rflst)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };


    this.getGeography = function (ID) {
        deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetGeography?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };


    this.getFunction = function (ID) {
        deferred = $q.defer();
        $http.get(this.path + '/api/Utility/GetFunction?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };


}]);

/** Messenger Call SetUp **/
Messenger.options = {
    'parentLocations': ['body'],
    extraClasses: 'messenger-fixed messenger-on-top messenger-on-right',
    theme: 'future'
};


var progressIndicator = function (trgt, size) {
    var cl = new CanvasLoader(trgt);
    cl.setColor('#05569A');
    cl.setShape('spiral');
    cl.setDiameter(size);
    cl.setDensity(100);
    cl.setRange(1);
    cl.setSpeed(8);
    cl.setFPS(50);
    cl.show();
};

var progress = function (timeout, message, show) {
    Messenger().hideAll();
    if (!show) {
        $('.prg').remove();
        return true;
    }
    //
    var prgid = Math.ceil(Math.random() * 456);
    //
    $('.prg').remove();
    //
    var prg = '<ul id="prg_' + prgid + '" class="prg messenger messenger-fixed messenger-on-right messenger-on-bottom messenger-theme-future">';
    prg += '<li class="messenger-message-slot">';
    prg += '<div class="messenger-message message default alert-default messenger-will-hide-after">';
    prg += '<div class="messenger-message-inner">' + message + '</div>';
    prg += '<div class="messenger-spinner" id="prg_sp_' + prgid + '">';
    prg += '<span class="messenger-spinner-side messenger-spinner-side-left">';
    prg += '<span class="messenger-spinner-fill"></span>';
    prg += '</span>';
    prg += '<span class="messenger-spinner-side messenger-spinner-side-right">';
    prg += '<span class="messenger-spinner-fill"></span>';
    prg += '</span>';
    prg += '</div>';
    prg += '</div>';
    prg += '</li>';
    prg += '</ul>';
    //
    $('.amantra').append(prg);
    //
    progressIndicator("prg_sp_" + prgid, 26);
    //
    $('.prg').fadeIn();
    //
    if (timeout != 0)
        setTimeout(function () {
            $('.prg').remove();
        }, timeout * 1000);
    return true;
};



var showNotification = function (_type, _timeout, _template, _message) {
    switch (_template) {
        case "top":
            {
                Messenger.options = {
                    'parentLocations': ['body'],
                    'extraClasses': 'messenger-fixed messenger-on-top',
                    'theme': 'block'
                };
                break;
            }
        case "bottom":
            {
                Messenger.options = {
                    'parentLocations': ['body'],
                    'extraClasses': 'messenger-fixed messenger-on-bottom',
                    'theme': 'block'
                };
                break;
            }
        case "right":
            {
                Messenger.options = {
                    'parentLocations': ['body'],
                    'extraClasses': 'messenger-fixed messenger-on-top messenger-on-right',
                    'theme': 'air'
                };
                break;
            }
        case "bottom-left":
            {
                Messenger.options = {
                    'parentLocations': ['body'],
                    'extraClasses': 'messenger-fixed messenger-on-bottom messenger-on-left',
                    'theme': 'air'
                };
                break;
            }
        case "bottom-right":
            {
                Messenger.options = {
                    'parentLocations': ['body'],
                    'extraClasses': 'messenger messenger-fixed messenger-on-bottom messenger-on-right messenger-theme-future',
                    'theme': 'Future'
                };
                break;
            }
    }
    Messenger().hideAll();
    switch (_type) {
        case 'success':
            Messenger().post({
                id: 'amantra',
                message: _message,
                type: _type,
                hideAfter: 8, //_timeout
                hideOnNavigate: true,
                showCloseButton: true
            });
            break;
        case 'error':
            Messenger().post({
                id: 'amantra',
                message: _message,
                type: _type,
                hideAfter: 8, //_timeout
                hideOnNavigate: true,
                showCloseButton: true
            });
            break;
        case 'Logout':
            Messenger().post({
                id: 'amantra',
                message: _message,
                type: _type,
                hideAfter: 10, //_timeout
                hideOnNavigate: false,
                showCloseButton: true
            });
            break;
    }

};

function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return decodeURIComponent(urlparam[1]);
        }
    }
}

function BindCompanyForGHModule() {
    $.ajax({
        url: '../../../api/Utility/GetCompanies',
        type: 'GET',
        success: function (result) {

            $.each(result.data, function (key, value) {
                $("#ddlCompany").append($("<option></option>").val(value.CNP_ID).html(value.CNP_NAME));
            });
        }
    });
}