﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Scripts/angular-confirm.css" rel="stylesheet" />
    <script defer>

        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

        };
    </script>
    <style>
        .map {
            height: 500px;
            width: 80%;
            padding: 10px;
        }

        #pageRowSummaryPanel {
            display: none;
        }

        .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }

        .with-nav-tabs.panel-primary .nav-tabs > .open > a,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
            color: #fff;
            background-color: #3071a9;
            border-color: transparent;
        }

        .with-nav-tabs.panel-primary .nav-tabs > li.active > a,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
            color: #428bca;
            background-color: #fff;
            border-color: #428bca;
            border-bottom-color: transparent;
        }
    </style>
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="ViewMoveRequestController" class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="View Move Requisition" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">View Move Requisition</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">

                    <%--<form role="form" id="Form1" name="frmViewRequisition" novalidate>--%>
                    <div data-ng-show="Viewstatus==0">

                        <h4></h4>

                        <div>

                            <input id="filtertxt" class="form-control" placeholder="Searh By Any..." type="text" style="width: 30%" />
                        </div>

                        <div style="height: 350px">
                            <div data-ag-grid="gridOptionsDetails" style="height: 100%;" class="ag-blue"></div>
                        </div>


                        <%--  </form>--%>
                    </div>

                    <div data-ng-show="Viewstatus==1">
                        <%--  <form id="Form2" name="frmViewRequisition" novalidate>--%>
                        <div class="clearfix">

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label><b>Request ID :</b> </label>
                                    {{MoveRequestDetails.REQ_ID}}
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label><b>Requested By :</b> </label>
                                    {{MoveRequestDetails.REQ_BY}}
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label><b>Requested Date :</b> </label>
                                    {{MoveRequestDetails.MR_CREATED_DT| date: dateformat}}
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 ">
                                <div class="form-group">
                                    <label><b>Status :</b> </label>
                                    {{MoveRequestDetails.STA_DESC}}
                                </div>
                            </div>

                        </div>
                        <div id="Source" class="panel-collapse collapse in">
                            <br />
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">

                                    <div class="form-group">
                                        <label class="control-label">Country</label>
                                        <div isteven-multi-select data-input-model="Country" id="cny" data-output-model="MoveRequest.Country" data-button-label="icon CNY_NAME"
                                            data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1" data-is-disabled="datadisable">
                                        </div>
                                        <%-- <input type="text" data-ng-model="MoveRequest.Country[0]" name="CNY_NAME" style="display: none" required="" />--%>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">City</label>
                                        <div isteven-multi-select data-input-model="City" data-output-model="MoveRequest.City" data-button-label="icon CTY_NAME"
                                            data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="MoveRequest.City[0]" name="CTY_NAME" style="display: none" required="" />

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Location</label>
                                        <div isteven-multi-select data-input-model="Locations" data-output-model="MoveRequest.Locations" data-button-label="icon LCM_NAME"
                                            data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="MoveRequest.Locations[0]" name="LCM_NAME" style="display: none" required="" />

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Tower</label>
                                        <div isteven-multi-select data-input-model="Towers" data-output-model="MoveRequest.Towers" data-button-label="icon TWR_NAME "
                                            data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="MoveRequest.Towers[0]" name="TWR_NAME" style="display: none" required="" />

                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Floor</label>
                                        <div isteven-multi-select data-input-model="Floors" data-output-model="MoveRequest.Floors" data-button-label="icon FLR_NAME"
                                            data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="MoveRequest.Floors[0]" name="FLR_NAME" style="display: none" required="" />

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">{{BsmDet.Parent}}</label>
                                        <div isteven-multi-select data-input-model="Verticals" data-output-model="MoveRequest.Verticals" data-button-label="icon VER_NAME"
                                            data-item-label=" icon VER_NAME maker" data-on-item-click="getCostcenterByVertical()" data-on-select-all="verticalChangeAll()" data-on-select-none="verticalSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="MoveRequest.Verticals[0]" name="VER_NAME" style="display: none" required="" />

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">{{BsmDet.Child}}</label>
                                        <div isteven-multi-select data-input-model="CostCenters" data-output-model="MoveRequest.CostCenters" data-button-label="icon Cost_Center_Name"
                                            data-on-item-click="CostCenterChagned()" data-on-select-all="costCenterChangeAll()" data-on-select-none="costCenterSelectNone()"
                                            data-item-label=" icon Cost_Center_Name maker" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="MoveRequest.CostCenters[0]" name="Cost_Center_Name" style="display: none" required="" />

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="destination">
                            <div class="panel-heading">
                                <h4 class="panel-title panel-heading-qfms-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#Dest">Destination Details</a>
                                </h4>
                            </div>
                            <div id="Dest" class="panel-collapse collapse in">
                                <br />
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">

                                        <div class="form-group">
                                            <label class="control-label">Country</label>
                                            <div isteven-multi-select id="destcny" data-input-model="DestCountry" data-output-model="DestMoveRequest.Country" data-button-label="icon CNY_NAME"
                                                data-item-label="icon CNY_NAME maker" data-on-item-click="DestgetCitiesbyCny()" data-on-select-all="DestcnySelectAll()" data-on-select-none="DestcnySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="DestMoveRequest.Country[0]" name="DestCNY_NAME" style="display: none" required="" />

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">City</label>
                                            <div isteven-multi-select data-input-model="DestCity" data-output-model="DestMoveRequest.City" data-button-label="icon CTY_NAME"
                                                data-item-label="icon CTY_NAME maker" data-on-item-click="DestgetLocationsByCity()" data-on-select-all="DestctySelectAll()" data-on-select-none="DestctySelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="DestMoveRequest.City[0]" name="DestCTY_NAME" style="display: none" required="" />

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Location</label>
                                            <div isteven-multi-select data-input-model="DestLocations" data-output-model="DestMoveRequest.Locations" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="DestgetTowerByLocation()" data-on-select-all="DestlocSelectAll()" data-on-select-none="DestlcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="DestMoveRequest.Locations[0]" name="DestLCM_NAME" style="display: none" required="" />

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Tower</label>
                                            <div isteven-multi-select data-input-model="DestTowers" data-output-model="DestMoveRequest.Towers" data-button-label="icon TWR_NAME "
                                                data-item-label="icon TWR_NAME maker" data-on-item-click="DestgetFloorByTower()" data-on-select-all="DesttwrSelectAll()" data-on-select-none="DesttwrSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="DestMoveRequest.Towers[0]" name="DestTWR_NAME" style="display: none" required="" />

                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Floor</label>
                                            <div isteven-multi-select data-input-model="DestFloors" data-output-model="DestMoveRequest.Floors" data-button-label="icon FLR_NAME"
                                                data-item-label="icon FLR_NAME maker" data-on-item-click="DestFloorChange()" data-on-select-all="DestfloorChangeAll()" data-on-select-none="DestFloorSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="DestMoveRequest.Floors[0]" name="DestFLR_NAME" style="display: none" required="" />

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">{{BsmDet.Parent}}</label>
                                            <div isteven-multi-select data-input-model="DestVerticals" data-output-model="DestMoveRequest.Verticals" data-button-label="icon VER_NAME"
                                                data-item-label=" icon VER_NAME maker" data-on-item-click="DestgetCostcenterByVertical()" data-on-select-all="DestverticalChangeAll()" data-on-select-none="DestverticalSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="DestMoveRequest.Verticals[0]" name="DestVER_NAME" style="display: none" required="" />

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">{{BsmDet.Child}}</label>
                                            <div isteven-multi-select data-input-model="DestCostCenters" data-output-model="DestMoveRequest.CostCenters" data-button-label="icon Cost_Center_Name"
                                                data-on-item-click="DestCostCenterChagned()" data-on-select-all="DestcostCenterChangeAll()" data-on-select-none="DestcostCenterSelectNone()"
                                                data-item-label=" icon Cost_Center_Name maker" data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="DestMoveRequest.CostCenters[0]" name="DestCost_Center_Name" style="display: none" required="" />

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>



                        <div class="col-md-10">

                            <h4>Move To</h4>
                            <input type="radio" name="destradio" value="0" checked ng-click="Radiochange('0')" />
                            Same Project
                                  <input type="radio" name="destradio" value="1" ng-click="Radiochange('1')" />Nearest Availability
                                  <input type="radio" name="destradio" value="2" ng-click="Radiochange('2')" />
                            Destination 
                                   
                        </div>

                        <div class="col-md-11 col-sm-6 col-xs-12">
                            <br />
                            <div class="box-footer text-right">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="Search()" />
                                <input type="button" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="Clear()" />
                            </div>
                        </div>
                        <%-- </form>--%>
                        <br />
                        <br />
                        <br />
                        <%--  <form id="form3" name="DestRequest" novalidate>--%>
                        <div class="col-md-8 col-sm-12 col-xs-12" style="width: 100%">
                            <div style="height: 325px;">
                                <input id="filtertxt" class="form-control" placeholder="Searh By Any..." type="text" style="width: 30%" />
                                <div data-ag-grid="gridOptions" style="height: 260px; width: 100%" class="ag-blue"></div>
                                <br />
                                <div class="box-footer text-right">
                                    <input type="button" id="btnviewinmap" data-ng-click="ViewinMap()" class="btn btn-primary custom-button-color" value="View In Map" />
                                    <button type="submit" id="btnRequest" class="btn btn-primary custom-button-color" data-ng-click="RaiseRequst()">Update Request</button>

                                </div>
                            </div>
                        </div>

                        <%-- </form>--%>
                    </div>

                </div>


            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script src="../../../BootStrapCSS/Scripts/angular-confirm.js"></script>




    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "cp.ngConfirm"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';

    </script>
    <script src="../JS/MoveRequest.js"></script>
    <script src="../JS/ViewMoveRequest.js"></script>
    <script defer src="../../Utility.min.js"></script>

</body>
</html>
