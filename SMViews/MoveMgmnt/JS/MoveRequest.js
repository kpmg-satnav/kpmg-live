﻿app.service("MoveRequestService", function ($http, $q, UtilityService) {

    this.getSpaces = function (data) {

        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MoveRequest/GetSpaces',data)
            .then(function (response) {
           
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetEmployeeDetails = function (SPC_ID) {

        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MoveRequest/GetEmployeeDetails?SPC_ID='+ SPC_ID)
            .then(function (response) {
              
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.RaiseRequst = function (data) {

        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MoveRequest/RaiseRequst', data)
            .then(function (response) {

                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    
    
});
app.controller('MoveRequestController', function ($scope, $q, $http, MoveRequestService, UtilityService, $timeout, $filter, $ngConfirm) {
    $scope.DestMoveRequest = {};
    $scope.MoveRequest = {};
    $scope.DestCountry = [];
    $scope.DestCity = [];
    $scope.DestLocations = [];
    $scope.DestTowers = [];
    $scope.DestFloors = [];
    $scope.DestVerticals = [];
    $scope.DestCostCenters = [];
    $scope.BsmDet = { Parent: "", Child: "" };
    $scope.destination = true;
    $scope.radioval = 0;
    $scope.grid = false;
    $scope.SelRowData = [];
    var map = L.map('Map');
    var map1 = L.map('Map1');
    $scope.Clear = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Floors, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Verticals, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.CostCenters, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestCountry, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestCity, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestLocations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestTowers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestFloors, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestVerticals, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestCostCenters, function (value, key) {
            value.ticked = false;
        });
    }

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });
    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {

            if (response.data != null) {
                $scope.Country = response.data;
                angular.copy($scope.Country, $scope.DestCountry);

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;
                        angular.copy($scope.City, $scope.DestCity);
                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                                angular.copy($scope.Locations, $scope.DestLocations);
                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;
                                        angular.copy($scope.Towers, $scope.DestTowers);
                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                angular.copy($scope.Floors, $scope.DestFloors);
                                            }
                                        });
                                        UtilityService.getVerticals(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Verticals = response.data;
                                                angular.copy($scope.Verticals, $scope.DestVerticals);
                                            }
                                        });

                                        UtilityService.getCostCenters(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.CostCenters = response.data;
                                                angular.copy($scope.CostCenters, $scope.DestCostCenters);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }


    $scope.Pageload();

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.MoveRequest.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.DestgetCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.DestMoveRequest.Country, 2).then(function (response) {
            $scope.DestCity = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.MoveRequest.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }
    $scope.DestcnySelectAll = function () {
        $scope.DestMoveRequest.Country = $scope.DestCountry;
        $scope.DestgetCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.MoveRequest.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.MoveRequest.Country[0] = cny;
            }
        });
    }

    $scope.DestgetLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.DestMoveRequest.City, 2).then(function (response) {
            $scope.DestLocations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.DestCountry, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestCity, function (value, key) {
            var cny = _.find($scope.DestCountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DestMoveRequest.Country = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.MoveRequest.City = $scope.City;
        $scope.getLocationsByCity();
    }

    $scope.DestctySelectAll = function () {
        $scope.DestMoveRequest.City = $scope.DestCity;
        $scope.DestgetLocationsByCity();
    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.MoveRequest.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.MoveRequest.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.MoveRequest.City[0] = cty;
            }
        });
    }

    $scope.DestgetTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.DestMoveRequest.Locations, 2).then(function (response) {
            $scope.DestTowers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.DestCountry, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestCity, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestLocations, function (value, key) {
            var cny = _.find($scope.DestCountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DestMoveRequest.Country = cny;
            }
        });

        angular.forEach($scope.DestLocations, function (value, key) {
            var cty = _.find($scope.DestCity, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.DestMoveRequest.City = cty;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.MoveRequest.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.DestlocSelectAll = function () {
        $scope.DestMoveRequest.Locations = $scope.DestLocations;
        $scope.DestgetTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.MoveRequest.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.MoveRequest.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.MoveRequest.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.MoveRequest.Locations[0] = lcm;
            }
        });
    }

    $scope.DestgetFloorByTower = function () {
        UtilityService.getFloorByTower($scope.DestMoveRequest.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.DestFloors = response.data;
            else
                $scope.DestFloors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.DestCountry, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestCity, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestLocations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestTowers, function (value, key) {
            var cny = _.find($scope.DestCountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DestMoveRequest.Country = cny;
            }
        });

        angular.forEach($scope.DestTowers, function (value, key) {
            var cty = _.find($scope.DestCity, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.DestMoveRequest.City = cty;
            }
        });

        angular.forEach($scope.DestTowers, function (value, key) {
            var lcm = _.find($scope.DestLocations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.DestMoveRequest.Locations = lcm;
            }
        });
    }
    $scope.twrSelectAll = function () {
        $scope.MoveRequest.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.DesttwrSelectAll = function () {
        $scope.DestMoveRequest.Towers = $scope.DestTowers;
        $scope.DestgetFloorByTower();
    }

    $scope.FloorChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.MoveRequest.Country[0] = cny;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.MoveRequest.City[0] = cty;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.MoveRequest.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.MoveRequest.Towers[0] = twr;
            }
        });

    }

    $scope.DestFloorChange = function () {

        angular.forEach($scope.DestCountry, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestCity, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestTowers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestLocations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.DestFloors, function (value, key) {
            var cny = _.find($scope.DestCountry, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DestMoveRequest.Country[0] = cny;
            }
        });

        angular.forEach($scope.DestFloors, function (value, key) {
            var cty = _.find($scope.DestCity, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.DestMoveRequest.City[0] = cty;
            }
        });

        angular.forEach($scope.DestFloors, function (value, key) {
            var lcm = _.find($scope.DestLocations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.DestMoveRequest.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.DestFloors, function (value, key) {
            var twr = _.find($scope.DestTowers, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.DestMoveRequest.Towers[0] = twr;
            }
        });

    }
    $scope.floorChangeAll = function () {
        $scope.MoveRequest.Floors = $scope.Floors;
        $scope.FloorChange();
    }

    $scope.DestfloorChangeAll = function () {
        $scope.DestMoveRequest.Floors = $scope.DestFloors;
        $scope.DestFloorChange();
    }

    $scope.getCostcenterByVertical = function () {
        UtilityService.getCostcenterByVertical($scope.MoveRequest.Verticals, 2).then(function (response) {
            if (response.data != null) {
                $scope.CostCenters = response.data;
            }
            else { $scope.CostCenters = [] }
        }, function (error) {
            console.log(error);
        });
    }
    $scope.DestgetCostcenterByVertical = function () {
        UtilityService.getCostcenterByVertical($scope.DestMoveRequest.Verticals, 2).then(function (response) {
            if (response.data != null) {
                $scope.DestCostCenters = response.data;
            }
            else { $scope.DestCostCenters = [] }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.verticalChangeAll = function () {
        $scope.MoveRequest.Verticals = $scope.Verticals;
        $scope.getCostcenterByVertical();
    }

    $scope.DestverticalChangeAll = function () {
        $scope.DestMoveRequest.Verticals = $scope.Verticals;
        $scope.DestgetCostcenterByVertical();
    }

    $scope.CostCenterChagned = function () {
        angular.forEach($scope.Verticals, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.CostCenters, function (value, key) {
            var ver = _.find($scope.Verticals, { VER_CODE: value.Vertical_Code });
            if (ver != undefined && value.ticked == true) {
                ver.ticked = true;
                $scope.MoveRequest.Verticals[0] = ver;
            }
        });
    }

    $scope.DestCostCenterChagned = function () {
        angular.forEach($scope.DestVerticals, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.DestCostCenters, function (value, key) {
            var ver = _.find($scope.DestVerticals, { VER_CODE: value.Vertical_Code });
            if (ver != undefined && value.ticked == true) {
                ver.ticked = true;
                $scope.DestMoveRequest.Verticals[0] = ver;
            }
        });
    }
    $scope.costCenterChangeAll = function () {
        $scope.MoveRequest.CostCenters = $scope.Verticals;
        $scope.CostCenterChagned();
    }
    $scope.DestcostCenterChangeAll = function () {
        $scope.DestMoveRequest.CostCenters = $scope.DestVerticals;
        $scope.DestCostCenterChagned();
    }

    var columnDefs = [
        {
            headerName: "Select All", field: "ticked", width: 90, template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)' />",

        },
        { headerName: "Source Space ID", field: "SPC_ID", cellClass: "grid-align" },
        { headerName: "Source Floor", field: "SPC_FLR_ID", cellClass: "grid-align" },
        { headerName: "Source Employee", field: "AUR_KNOWN_AS", cellClass: "grid-align" },
        { headerName: "Destination Space ID", width: 110, cellClass: "grid-align", field: "DEST_SPC_ID", cellRenderer: customEditor },
        {
            headerName: "Type of Movement", field: "TYPE_ID", suppressMenu: true, width: 180, cellClass: "grid-align",
            filter: 'set',
            template: "<select  ng-model='data.TYPE_ID' data-ng-change='Typechange(data)'><option value=''>--Select--</option><option  ng-repeat='T in MovementTypes' value='{{T.value}}'>{{T.Name}}</option></select>"
        },

        { headerName: "Destination Floor", field: "DEST_FLOOR_ID", cellClass: "grid-align", template: "<label> {{data.DEST_FLOOR_ID}}</label>" },

        { headerName: "Destination Employee", width: 110, cellClass: "grid-align", field: "DEST_EMP_ID", cellRenderer: customEditor },
        { headerName: "Vacant For Destination", width: 110, cellClass: "grid-align", field: "VACANT_SPC_ID", cellRenderer: customEditor },

        { headerName: "Vacant Floor", field: "VACANT_FLOOR", cellClass: "grid-align", template: "<label> {{data.VACANT_FLOOR}}</label>" }
    ];



    $scope.MovementTypes = [{ value: "1", Name: "Swap" }, { value: "2", Name: "Allocate" }, { value: "3", Name: "2 Movements" }];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableSorting: true,
        enableCellSelection: true,
        //enableFilter: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };
    $("#filtertxt").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    });
    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    //$timeout(function () {
    //    $scope.gridOptions.columnApi.setColumnVisible('VACANT_SPC_ID', true);
    //}, 2000);
    $scope.Radiochange = function (val) {
        if (val == 2) {

            $scope.destination = false;
            $scope.radioval = 2;
        }
        else if (val == 1) {
            $scope.destination = true;
            $scope.radioval = 1;
        }
        else {
            $scope.destination = true;
            $scope.radioval = 0;
        }

    };
    $scope.Typechange = function (data) {
        if (data.DEST_SPC_ID == undefined) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select Destination space id");
            data.TYPE_ID = "";
            return;

        }
        else if (data.DEST_SPC_ID == "") {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "The select Destination space id is not valid,please change");
            data.TYPE_ID = "";
            return;

        }
        else if (data.SSA_STA_ID ==1 && data.TYPE_ID !=2) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select Allocate for vacant spaces");
            data.TYPE_ID = "";
            return;

        }
        else if (data.SSA_STA_ID ==1002 && data.TYPE_ID != 2) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select Allocate for" + $scope.BsmDet.Parent +" allocated seats");
            data.TYPE_ID = "";
            return;

        }
        else if (data.SSA_STA_ID == 1003 && data.TYPE_ID != 2) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select Allocate for" + $scope.BsmDet.Child + " allocated seats");
            data.TYPE_ID = "";
            return;

        }
       
    }
    $scope.GetEmployeeDetails = function (data, eSelect) {
        
        data.ticked = true;
        removeOptions(eSelect);
        var eOption = document.createElement("option");
        eSelect.appendChild(eOption);
        MoveRequestService.GetEmployeeDetails(data.DEST_SPC_ID).then(function (response) {
           
            var SD = _.find($scope.SourceDetails, { SPC_ID: data.SPC_ID });

            if (response.data[0].SSA_STA_ID == 1002 && response.data[0].SSA_VER_CODE !=SD.SSA_VER_CODE) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "The Selected " + data.DEST_SPC_ID +" Space id already allocated to another" + $scope.BsmDet.Parent + " ,Please release");
                data.DEST_SPC_ID = "";
               return;

            }
            else if (response.data[0].SSA_STA_ID == 1003 && response.data[0].SSAD_COST_CENTER != SD.SSAD_COST_CENTER) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "The Selected " + data.DEST_SPC_ID +" Space id already allocated to another" + $scope.BsmDet.Child + " ,Please release");
                data.DEST_SPC_ID = "";
                return;

            }
            else if (response.data[0].SVR_STA_ID == 1006) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "The Selected" + data.DEST_SPC_ID +" Space id already under vertical requisition,Please release");
                data.DEST_SPC_ID = "";
                return;

            }
            else if (response.data[0].SRN_STA_ID == 1010 || response.data[0].SRN_STA_ID == 1017) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "The Selected " + data.DEST_SPC_ID+" Space id already under space requisition,Please release");
                data.DEST_SPC_ID = "";
                return;

            }
            else if (response.data[0].MR_STATUS == 1053) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "The Selected" + data.DEST_SPC_ID + " Space id already under Movement requisition,Please Approve");
                data.DEST_SPC_ID = "";
                return;

            }
            
            data.DEST_FLOOR_ID = response.data[0].DEST_FLOOR_ID;
            $scope.Destinationspacedetails = response.data;
            angular.forEach(response.data, function (item, key) {
                var eOption = document.createElement("option");
                eOption.setAttribute("value", item.DEST_EMP_ID);
                eOption.setAttribute("text", item.DEST_EMP_ID);
                eOption.innerHTML = item.DEST_EMP_ID;
                eSelect.appendChild(eOption);
            });
            progress(0, '', false);


        });
    }
    $scope.GetVacantDetails = function (data) {
        
        //data.VACANT_FLOOR = data.VACANT_SPC_ID.substring(0, data.VACANT_SPC_ID.lastIndexOf('/')).replace(/\//g, '-');
        var flr = _.find($scope.VacantDetails, { VACANT_SPC_ID: data.VACANT_SPC_ID });
        if (flr.MR_STATUS == 1053) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "The Selected" + data.VACANT_SPC_ID + " Space id already under Movement requisition,Please Approve");
            data.VACANT_SPC_ID = "";
            return;
        }
        data.VACANT_FLOOR = flr.SPC_FLR_ID;

    }

    $scope.Search = function () {
       
      
        if ($scope.MoveRequest.Country.length == 0 && $scope.MoveRequest.City.length == 0 && $scope.MoveRequest.Locations.length == 0 && $scope.MoveRequest.Towers.length == 0 && $scope.MoveRequest.Floors.length == 0 &&
            $scope.MoveRequest.Verticals.length == 0 && $scope.MoveRequest.CostCenters.length == 0) {
            showNotification('error', 8, 'bottom-right', "Please select atleast one source details");
            return;
        }
       else if ($scope.radioval == 2 && $scope.DestMoveRequest.Country.length == 0 && $scope.DestMoveRequest.City.length == 0 && $scope.DestMoveRequest.Locations.length == 0 && $scope.DestMoveRequest.Towers.length == 0 && $scope.DestMoveRequest.Floors.length == 0 &&
            $scope.DestMoveRequest.Verticals.length == 0 && $scope.DestMoveRequest.CostCenters.length == 0) {
            showNotification('error', 8, 'bottom-right', "Please select atleast one destination details");
            return;
        }
        else if ($scope.radioval == 0 && $scope.MoveRequest.CostCenters.length == 0) {
            showNotification('error', 8, 'bottom-right', "Please select atleast one" + $scope.BsmDet.Child + " to move into same project");
            return;
        }
        else if ($scope.radioval == 1 && ($scope.MoveRequest.Locations.length == 0 && $scope.MoveRequest.Towers.length == 0 && $scope.MoveRequest.Floors.length == 0)) {
            showNotification('error', 8, 'bottom-right', "Please select atleast one location/tower/floor to move to neasrest availability");
            return;
        }
        $scope.grid = true;
        var dataobj =
        {
            cnylst: $scope.MoveRequest.Country,
            ctylst: $scope.MoveRequest.City,
            loclst: $scope.MoveRequest.Locations,
            twrlst: $scope.MoveRequest.Towers,
            flrlst: $scope.MoveRequest.Floors,
            verlst: $scope.MoveRequest.Verticals,
            cstlst: $scope.MoveRequest.CostCenters,
            Destcnylst: $scope.DestMoveRequest.Country,
            Destctylst: $scope.DestMoveRequest.City,
            Destloclst: $scope.DestMoveRequest.Locations,
            Desttwrlst: $scope.DestMoveRequest.Towers,
            Destflrlst: $scope.DestMoveRequest.Floors,
            Destverlst: $scope.DestMoveRequest.Verticals,
            Destcstlst: $scope.DestMoveRequest.CostCenters,
            Type: $scope.radioval
        };
        $scope.Markers = [];
        $scope.Markers1 = [];
        MoveRequestService.getSpaces(dataobj).then(function (response) {
            if (response.data != null) {

                //GetMarkers(response.data);
                $scope.gridOptions.api.setRowData(response.data);
                $scope.SourceDetails = response.data;
                
                $scope.SpaceDetails = response.DestSpaceids;
                //GetMarkers1($scope.SpaceDetails);
                $timeout(function () {
                    $scope.VacantDetails = response.VacantSpaceids;
                }, 500);
            }
        });

    }

    function customEditor(params) {
        var editing = false;

        var eCell = document.createElement('div');

        var eLabel = document.createTextNode('Please Select');
        if (params.value)
            eLabel.nodeValue = params.value;
        eCell.appendChild(eLabel);

        var eSelect = document.createElement("select");
        eSelect.setAttribute("width", 50);
      
        eCell.addEventListener('click', function () {
            if (!editing) {
                progress(0, 'Loading...', true);
                if (params.column.colId === "DEST_SPC_ID") {
                    removeOptions(eSelect);
                    var eOption = document.createElement("option");
                    eSelect.appendChild(eOption);
                    angular.forEach($scope.SpaceDetails, function (item, key) {
                        var eOption = document.createElement("option");
                        eOption.setAttribute("value", item.DEST_SPC_ID);
                        eOption.setAttribute("text", item.DEST_SPC_ID);
                       
                        angular.forEach($scope.gridOptions.rowData, function (val, key) {
                            if (val.DEST_SPC_ID == item.DEST_SPC_ID) {
                                eOption.setAttribute("disabled", item.DEST_SPC_ID);
                            }
                        });
                        
                        eOption.innerHTML = item.DEST_SPC_ID;
                        eSelect.appendChild(eOption);
                    });
                    progress(0, '', false);
                }
                else if (params.column.colId === "DEST_EMP_ID") {
                    if (params.data.DEST_SPC_ID == undefined) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', "Please select Destination space id");
                        return;

                    }
                    else {
                        $scope.GetEmployeeDetails(params.data, eSelect);
                    }
                    progress(0, '', false);
                }
                else {
                    if (params.data.TYPE_ID == undefined) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', "Please select Movement type");
                        return;

                    }
                   else if (params.data.TYPE_ID !=3) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', "Please select Movement type as 2 movements");
                        return;

                    }
                    else if (params.data.DEST_EMP_ID == undefined) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', "Please select Destination Employee");
                        return;

                    }
                    removeOptions(eSelect);
                    var eOption1 = document.createElement("option");
                    eSelect.appendChild(eOption1);
                    angular.forEach($scope.VacantDetails, function (item, key) {
                        var eOption1 = document.createElement("option");
                        eOption1.setAttribute("value", item.VACANT_SPC_ID);
                        eOption1.setAttribute("text", item.VACANT_SPC_ID);
                        angular.forEach($scope.gridOptions.rowData, function (val, key) {
                            if (val.VACANT_SPC_ID == item.VACANT_SPC_ID) {
                                eOption1.setAttribute("disabled", item.VACANT_SPC_ID);
                            }
                        });
                        eOption1.innerHTML = item.VACANT_SPC_ID;
                        eSelect.appendChild(eOption1);
                    });
                    progress(0, '', false);
                }
                setTimeout(function () {
                    eSelect.value = params.value;
                    eCell.removeChild(eLabel);
                    eCell.appendChild(eSelect);
                    eSelect.focus();
                    editing = true;
                }, 200);
            }
        });

        eSelect.addEventListener('blur', function () {
            if (editing) {
                editing = false;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
            }
        });
        eSelect.addEventListener('change', function () {
            if (editing) {
                editing = false;

                var newValue = eSelect.options[eSelect.selectedIndex].text;
                eLabel.nodeValue = newValue;
                eCell.removeChild(eSelect);
                eCell.appendChild(eLabel);
                if (params.column.colId === "DEST_SPC_ID") {
                    params.data.DEST_SPC_ID = eSelect.value;
                    params.data.DEST_SPC_ID = newValue;

                    $scope.GetEmployeeDetails(params.data, eSelect);
                }
                else if (params.column.colId === "DEST_EMP_ID") {
                    params.data.DEST_EMP_ID = eSelect.value;
                    params.data.DEST_EMP_ID = newValue;
                }
                else {
                    params.data.VACANT_SPC_ID = eSelect.value;
                    params.data.VACANT_SPC_ID = newValue;
                    $timeout(function () {
                        $scope.GetVacantDetails(params.data);
                    }, 500);


                }

            }
        });
        return eCell;
    }
    function removeOptions(selectbox) {
        var i;
        for (i = selectbox.options.length - 1; i >= 0; i--) {
            selectbox.remove(i);
        }
    }
    
    $scope.RaiseRequst = function () {
        $scope.SeletedSpaces = [];

        var count = _.countBy($scope.gridOptions.rowData, { ticked: true });
        if (count.true==undefined) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', "Please select atleast one space id");
            return;
        }
        angular.forEach($scope.gridOptions.rowData, function (val, key) {
            
            if (val.ticked == true && (val.DEST_SPC_ID == "" || val.DEST_SPC_ID == undefined)) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "Please select destination space id");
                return;
            }
            else if (val.ticked == true && (val.TYPE_ID == "" || val.TYPE_ID == undefined)) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "Please select Movement Type");
                return;
            }
            else if (val.ticked == true && (val.TYPE_ID == 1 || val.TYPE_ID == 3) && (val.DEST_EMP_ID == undefined || val.DEST_EMP_ID == "")) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "Please select Destination Employee");
                return;
            }
            else if (val.ticked == true && val.TYPE_ID == 3 && val.VACANT_SPC_ID == undefined) {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "Please select Vacant Space id");
                return;
            }
            else if (val.ticked == true && val.TYPE_ID == 3 &&  val.VACANT_SPC_ID == "") {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "Please select Valid vacant Space id");
                return;
            }

            else if (val.ticked == true)
            {

                $scope.SeletedSpaces.push(val);

            }
        });
        if ($scope.SeletedSpaces.length != 0) {
            $ngConfirm({
                icon: 'fa fa-warning',
                title: 'Confirm!',
                content: 'Are You Sure You Want To Move Seat',
                closeIcon: true,
                closeIconClass: 'fa fa-close',
                buttons: {
                    Confirm: {
                        text: 'Sure',
                        btnClass: 'btn-blue',
                        action: function (button) {
                            MoveRequestService.RaiseRequst($scope.SeletedSpaces).then(function (response) {

                                if (response.data != null) {
                                    progress(0, '', false);
                                    showNotification('success', 8, 'bottom-right', response.data);
                                    $scope.grid = false;
                                    $scope.Clear();
                                }
                            });

                        }
                    },
                    close: function ($scope, button) {
                    }
                }
            });
            
        }
       
        
    };

    ///// For map layout

    $scope.ViewinMap = function () {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        map1.eachLayer(function (layer) {
            map1.removeLayer(layer);
        });
        $scope.SelRowData = [];
        $scope.sourcefloors2 = [];
        $scope.sourcefloors = [];
        $scope.Destinationfloors = [];
        
        angular.forEach($scope.Floors, function (Value, Key) {
            var sur = _.find($scope.SourceDetails, { SPC_FLR_ID: Value.FLR_CODE });
            if (sur != undefined) {
                $scope.sourcefloors2.push(Value);
            }
            angular.copy($scope.sourcefloors2, $scope.sourcefloors);
        });
       
        angular.forEach($scope.Floors, function (Value, Key) {
            var dest = _.find($scope.SpaceDetails, { spc_flr_id: Value.FLR_CODE });
            if (dest != undefined) {
                $scope.Destinationfloors.push(Value);
            }

        });
     
        $scope.MapFloors = [];
        $scope.DestMapFloors = [];
      $timeout(function () {
            if ($scope.SelRowData.length == 0) {
                
                angular.forEach($scope.sourcefloors, function (Value, Key) {
                    Value.ticked = false;
                    $scope.MapFloors.push(Value);
                   
                });
                angular.forEach($scope.Destinationfloors, function (Value, Key) {
                    Value.ticked = false;
                   $scope.DestMapFloors.push(Value);
                });
                
                    $scope.MapFloors[0].ticked = true;
                    $scope.DestMapFloors[0].ticked = true;
                
                $scope.Map.Floor = [];
                $scope.DestMap.Floor = [];
                $scope.Map.Floor.push($scope.MapFloors[0]);
                $scope.DestMap.Floor.push($scope.DestMapFloors[0]);
               
                }
            }, 1000);
                $("#historymodal").modal('show');
               
        }
   

    $('#historymodal').on('shown.bs.modal', function () {
        progress(0, 'Loading...', true);

        if ($scope.SelRowData.length == 0) {
            $timeout(function () {
                $scope.loadmap();
            }, 1000);
            $timeout(function () {
                $scope.loadDestmap();

            }, 1000);
        }
        else {
            progress(0, 'Loading...', false);
        }

    });

    $scope.FlrSectMap = function (data) {
        map.eachLayer(function (layer) {
            map.removeLayer(layer);
        });
        $scope.loadmap();

    }
    $scope.DestFlrSectMap = function (data) {
        map1.eachLayer(function (layer) {
            map1.removeLayer(layer);
        });
        $scope.loadDestmap();

    }

    $scope.loadmap = function () {
        progress(0, 'Loading...', true);
        $scope.drawnItems = new L.FeatureGroup();
        map.addLayer($scope.drawnItems);
        var dataobj = { flr_code: $scope.Map.Floor[0].FLR_CODE, key_value: 1  };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post(UtilityService.path + '/api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadMapDetails(result);
            progress(0, '', false);
        });

    }

    $scope.loadDestmap = function () {
        progress(0, 'Loading...', true);
        $scope.drawnItems1 = new L.FeatureGroup();
        map1.addLayer($scope.drawnItems1);
        var dataobj = { flr_code: $scope.DestMap.Floor[0].FLR_CODE };
        //Get Background Map
        var arrlayers = ['DSK', 'FUR'];
        $.post(UtilityService.path + '/api/MaploaderAPI/GetMapItems', dataobj, function (result) {
            flrid = result.FloorDetails;
            $scope.loadDestMapDetails(result);
            progress(0, '', false);
        });

    }

    $scope.loadMapDetails = function (result) {
        var promises = [];
        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            var wkt = new Wkt.Wkt();
            wkt.read(value.Wkt);
            var theLayer = wkt.toObject();
            theLayer.dbId = value.ID;
            theLayer.options.color = "#000000";
            theLayer.options.weight = 1;
            theLayer.options.seattype = value.SEATTYPE;
            theLayer.options.spacetype = value.layer;
            theLayer.options.seatstatus = value.STAID;
            theLayer.options.SPACE_ID = value.SPACE_ID;
            theLayer.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer.setStyle({ fillColor: col });
            $scope.drawnItems.addLayer(theLayer);
           defer.resolve(theLayer);
            promises.push(defer.promise);
           
        });

        $q.all(promises).then(
            function (results) {
              
                var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
               
                map.fitBounds(bounds);
                
                $scope.SelRowData = $filter('filter')($scope.gridOptions.rowData, { SPC_FLR_ID: $scope.Map.Floor[0].FLR_CODE });
               
                angular.forEach($scope.SelRowData, function (value, key) {
                    
                    $scope.marker = _.find($scope.drawnItems._layers, { options: { SPACE_ID: value.SPC_ID, spacetype: 'CHA' } });
                   
                    if (value.ticked)
                        $scope.marker.setStyle(selctdChrStyle);
                    else
                        $scope.marker.setStyle(OccupiedStyle);
                    $scope.marker.bindLabel(value.SPC_ID + ' /Occupied by ' + value.AUR_KNOWN_AS);
                    $scope.marker.addTo(map);
                    
                });
            },
            function (response) {
            }
        );
    };

    $scope.loadDestMapDetails = function (result) {
        var promises = [];
        

        angular.forEach(result.mapDetails, function (value, index) {
            var defer = $q.defer();
            var wkt1 = new Wkt.Wkt();
            wkt1.read(value.Wkt);
            var theLayer1 = wkt1.toObject();
            theLayer1.dbId = value.ID;
            theLayer1.options.color = "#000000";
            theLayer1.options.weight = 1;
            theLayer1.options.seattype = value.SEATTYPE;
            theLayer1.options.spacetype = value.layer;
            theLayer1.options.seatstatus = value.STAID;
            theLayer1.options.SPACE_ID = value.SPACE_ID;
            theLayer1.options.checked = false;
            var SeattypeLayer = $.extend(true, {}, theLayer1);
            var col = result.COLOR[value.layer] == undefined ? '#E8E8E8' : result.COLOR[value.layer];
            theLayer1.setStyle({ fillColor: col });

            $scope.drawnItems1.addLayer(theLayer1);
            defer.resolve(theLayer1);
            promises.push(defer.promise);

        });

        $q.all(promises).then(
            function (results) {

                var bounds = [[result.BBOX[0].MinY, result.BBOX[0].MinX], [result.BBOX[0].MaxY, result.BBOX[0].MaxX]];
                map1.fitBounds(bounds);
               
                $scope.SelRowData = $filter('filter')($scope.SpaceDetails, { spc_flr_id: $scope.DestMap.Floor[0].FLR_CODE });

                angular.forEach($scope.SelRowData, function (value, key) {

                    $scope.marker1 = _.find($scope.drawnItems1._layers, { options: { SPACE_ID: value.DEST_SPC_ID, spacetype: 'CHA' } });

                    var spc = _.find($scope.gridOptions.rowData, { DEST_SPC_ID: value.DEST_SPC_ID });
                    
                    if (spc != undefined) {
                        $scope.marker1.setStyle(selctdChrStyle);
                        $scope.marker1.bindLabel(value.DEST_SPC_ID);
                    }

                    else {
                        if (value.SSA_STA_ID == 1) {
                            $scope.marker1.setStyle(VacantStyle);
                            $scope.marker1.bindLabel(value.DEST_SPC_ID);
                        }
                         
                        else if (value.SSA_STA_ID == 1002) {
                            $scope.marker1.setStyle(AllocateStyle);
                            $scope.marker1.bindLabel(value.DEST_SPC_ID + ' / Allocated to ' + value.VER_NAME);
                        }
                        else if (value.SSA_STA_ID == 1003) {
                            $scope.marker1.setStyle(AllocateStyle);
                            $scope.marker1.bindLabel(value.DEST_SPC_ID + ' / Allocated to ' + value.Cost_Center_Name);
                        }
                        else if (value.SSA_STA_ID == 1004) {
                            $scope.marker1.setStyle(OccupiedStyle);
                            $scope.marker1.bindLabel(value.DEST_SPC_ID + ' /Occupied by ' + value.EMP_NAMES);
                        }
                            
                    }
                        
                    $scope.marker1.addTo(map1);
                   
            });
            },
            function (response) {
            }
        );
    };

    var VacantStyle = { fillColor: '#78AB46', opacity: 0.8, fillOpacity: 0.8 };
    var selctdChrStyle = { fillColor: '#ebf442', opacity: 0.8, fillOpacity: 0.8 };
    var OccupiedStyle = { fillColor: '#E74C3C', opacity: 0.8, fillOpacity: 0.8 };
    var AllocateStyle = { fillColor: '#3498DB', opacity: 0.8, fillOpacity: 0.8 };

});