﻿app.service("DemandPlanService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    var deferred = $q.defer();
    
}]);

app.controller('DemandPlanController', ['$scope', '$q', 'DemandPlanService', 'UtilityService', '$filter', function ($scope, $q, DemandPlanService, UtilityService, $filter) {
    //  Upload File
    $scope.OSPdata = [];
    $scope.ShowGrid = 0;
    $scope.uploadEnable = 1;
    $scope.Datastore = [];

    //UtilityService.getBussHeirarchy().then(function (response) {
    //    if (response.data != null) {
    //        $scope.BsmDet = response.data;
    //    }
    //});

    $scope.UploadFile = function () {
        debugger;    
        if ($('#FileUpl', $('#FileUsrUpl')).val()) {
            progress(0, 'Loading...', true);
            var filetype = $('#FileUpl', $('#FileUsrUpl')).val().substring($('#FileUpl', $('#FileUsrUpl')).val().lastIndexOf(".") + 1);
            if (filetype == "xlsx" || filetype == "xls") {
                if (!window.FormData) {
                    redirect(); // if IE8
                }
                else {
                    var formData = new FormData();
                    var UplFile = $('#FileUpl')[0];
                    formData.append("UplFile", UplFile.files[0]); 
                    $.ajax({
                        url: UtilityService.path + "/api/DemandPlan/UploadTemplate",
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {                           
                            if (response.data != null) {
                                $scope.ShowGrid = 1;
                                $scope.$apply(function () {
                                    $scope.ShowGrid = 1;
                                    $scope.uploadEnable = 0;
                                    $scope.Datastore = response.data.LST;
                                    $scope.REQ_ID = response.data.REQID;
                                    $scope.gridOptions.api.setRowData($scope.Datastore);
                                    //$scope.gridOptions.columnApi.getColumn("DemandRaisedBy").colDef.headerName = $scope.BsmDet.Parent;
                                    //$scope.gridOptions.columnApi.getColumn("DeamndRaisedOn").colDef.headerName = $scope.BsmDet.Child;
                                    $scope.gridOptions.api.refreshHeader();
                                    progress(0, '', false);
                                });


                                $scope.gridata = response.data[0];
                                console.table(response);
                                if (response != null) {
                                    $scope.gridOptions.api.setColumnDefs(response.Coldef);
                                    $scope.gridOptions.api.setRowData($scope.gridata);
                                }
                                else {
                                    $scope.gridOptions.api.setColumnDefs(response.Coldef);
                                    $scope.gridOptions.api.setRowData([]);
                                }


                                $scope.DispDiv = true;
                                progress(0, '', false);
                                showNotification('success', 8, 'bottom-right', response.Message);

                            }
                            else {
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', response.Message);
                            }
                        }
                    });
                }

            }
            else
                progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', 'Please Upload only Excel File(s)');
        }
        else
            showNotification('error', 8, 'bottom-right', 'Please Select File To Upload');

    }

    var columnDefs = [

        { headerName: "Created By", cellClass: "grid-align", field: "DemandRaisedBy", width: 120, suppressMenu: true },
        { headerName: "Created On", cellClass: "grid-align", field: "DeamndRaisedOn", width: 130, suppressMenu: true },
        { headerName: "Approved By", cellClass: "grid-align", field: "DemandApprovedBy", width: 150, suppressMenu: true },
        { headerName: "Approved On", cellClass: "grid-align", field: "DemandApprovedOn", width: 130, suppressMenu: true, },
        { headerName: "Status", cellClass: "grid-align", field: "DemandStatus", width: 120, suppressMenu: true, },
        { headerName: "Allocation Status", cellClass: "grid-align", field: "FulfilmentStatus", width: 120, suppressMenu: true },
        { headerName: "Geography", cellClass: "grid-align", field: "Geography", width: 140, suppressMenu: true },
        { headerName: "Location", cellClass: "grid-align", field: "Location", width: 120, suppressMenu: true },
        { headerName: "Floor", cellClass: "grid-align", field: "Floor", width: 140, suppressMenu: true },
        { headerName: "Function", cellClass: "grid-align", field: "Function", width: 100, suppressMenu: true },
        { headerName: "SubFunction", cellClass: "grid-align", field: "SubFunction", width: 110, suppressMenu: true },
        { headerName: "Team", cellClass: "grid-align", field: "Teams", width: 70, suppressMenu: true },
        { headerName: "JAN", cellClass: "grid-align", field: "Month1", width: 70, suppressMenu: true },
        { headerName: "FEB", cellClass: "grid-align", field: "Month2", width: 70, suppressMenu: true },
        { headerName: "MAR", cellClass: "grid-align", field: "Month3", width: 70, suppressMenu: true },
        { headerName: "APR", cellClass: "grid-align", field: "Month4", width: 70, suppressMenu: true },
        { headerName: "MAY", cellClass: "grid-align", field: "Month5", width: 70, suppressMenu: true },
        { headerName: "JUN", cellClass: "grid-align", field: "Month6", width: 70, suppressMenu: true },
        { headerName: "JUL", cellClass: "grid-align", field: "Month7", width: 70, suppressMenu: true },
        { headerName: "AUG", cellClass: "grid-align", field: "Month8", width: 70, suppressMenu: true },
        { headerName: "SEP", cellClass: "grid-align", field: "Month9", width: 70, suppressMenu: true },
        { headerName: "OCT", cellClass: "grid-align", field: "Month10", width: 70, suppressMenu: true },
        { headerName: "NOV", cellClass: "grid-align", field: "Month11", width: 70, suppressMenu: true },
        { headerName: "DEC", cellClass: "grid-align", field: "Month12", width: 70, suppressMenu: true },
        { headerName: "Q1", cellClass: "grid-align", field: "CurrentYearQ1", width: 120, suppressMenu: true },
        { headerName: "Q2", cellClass: "grid-align", field: "CurrentYearQ2", width: 120, suppressMenu: true },
        { headerName: "Q3", cellClass: "grid-align", field: "CurrentYearQ3", width: 120, suppressMenu: true },
        { headerName: "Q4", cellClass: "grid-align", field: "CurrentYearQ4", width: 120, suppressMenu: true },
        { headerName: "NY Q1", cellClass: "grid-align", field: "NextYearQ1", width: 120, suppressMenu: true },
        { headerName: "NY Q2", cellClass: "grid-align", field: "NextYearQ2", width: 120, suppressMenu: true },
        { headerName: "NY Q3", cellClass: "grid-align", field: "NextYearQ3", width: 120, suppressMenu: true },
        { headerName: "NY Q4", cellClass: "grid-align", field: "NextYearQ4", width: 120, suppressMenu: true }
        //{ headerName: "Allocated WS", cellClass: "grid-align", width: 100, template: "<input type='textbox' ng-model='data.REQWS' ng-blur ='refreshGrid(data,\"WS\")' />", suppressMenu: true },
        ////ng-model='data.SRC_REQ_CNT'
        //{ headerName: "Vacant Cabins", cellClass: "grid-align", field: "AVAILABLE_CB", width: 150, suppressMenu: true },
        //{ headerName: "Allocated Cabins", cellClass: "grid-align", template: "<input type='textbox' ng-model='data.REQCAB' ng-blur ='refreshGrid(data,\"CB\")' />", width: 150, suppressMenu: true },
        ////{ headerName: "Available TWD", cellClass: "grid-align", field: "AVAILABLE_TWD", width: 100, suppressMenu: true },
        ////{ headerName: "Allocated TWD", cellClass: "grid-align", template: "<input type='textbox' ng-model='data.REQTWD' ng-blur ='refreshGrid(data,\"TWD\")' />", width: 100, suppressMenu: true },
        //{ headerName: "Available Capacity After Allocation(Available Capacity-(Allocated WS+CB))", cellClass: "grid-align", field: "TOTALSEAT", width: 450, suppressMenu: true },
        //{ headerName: "Pending(Required-(Allocated WS+CB))", cellClass: "grid-align", field: "TOTALSEAT_PENDING", width: 300, suppressMenu: true },
        //{
        //    headerName: "Required WS",
        //    field: "REQWS",

        //    cellRenderer: createReqWS,
        //}, 
        //{
        //    headerName: "Required Cabins",
        //    field: "REQCAB",

        //    cellRenderer: createReqCAB,
        //}
    ];





    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableFilter: true,
        enableCellSelection: false,
        enableSorting: true,


        //onAfterFilterChanged: function () {
        //    if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
        //        $scope.DocTypeVisible = 0;
        //    else
        //        $scope.DocTypeVisible = 1;
        //}
        
 
    };

    //$scope.GenerateFilterPdf = function () {

    //    progress(0, 'Loading...', true)
    //    var columns = [{ title: "Created By", key: "DemandRaisedBy" }, { title: "Created On", key: "DeamndRaisedOn" }, { title: "Approved By", key: "DemandApprovedBy" }, { title: "Approved On", key: "DemandApprovedOn" }, { tittle: "Status", key: "DemandStatus" }, { tittle: "Allocation Status", Key: "FulfilmentStatus" }, { tittle: "Geography", Key: "Geography" }, { tittle: "Location", Key: "Location" }, { tittle: "Floor", Key: "Floor" }, { tittle: "Function", Key: "Function" }, { tittle: "SubFunction", Key: "SubFunction" }, { tittle: "Team", Key: "Teams" }, { tittle: "JAN", Key: "Month1" }, { tittle: "FEB", Key: "Month2" }, { tittle: "MAR", Key: "Month3" }, { tittle: "APR", Key: "Month4" }, { tittle: "MAY", Key: "Month5" }, { tittle: "JUN", Key: "Month6" }, { tittle: "JUL", Key: "Month7" }, { tittle: "AUG", Key: "Month8" }, { tittle: "SEP", Key: "Month9" }, { tittle: "OCT", Key: "Month10" }, { tittle: "NOV", Key: "Month11" }, { tittle: "DEC", Key: "Month12" }, { tittle: "Q1", Key: "CurrentYearQ1" }, { tittle: "Q2", Key: "CurrentYearQ2" }, { tittle: "Q3", Key: "CurrentYearQ3" }, { tittle: "Q4", Key: "CurrentYearQ2" }, { tittle: "NY Q1", Key: "NextYearQ1" }, { tittle: "NY Q2", Key: "NextYearQ2" }, { tittle: "NY Q3", Key: "NextYearQ3" }, { tittle: "NY Q4", Key: "NextYearQ4" }];
    //    var model = $scope.gridOptions.api.getModel();
    //    var data = [];
    //    model.forEachNodeAfterFilter(function (node) {
    //        data.push(node.data)
    //    });
    //    var jsondata = JSON.parse(JSON.stringify(data));
    //    var doc = new jsPDF("landscape", "pt", "a4");
    //    doc.autoTable(columns, jsondata);
    //    doc.save("clsDemandPlan.pdf");
    //    setTimeout(function () {
    //        progress(0, 'Loading...', false)
    //    }, 1000);
    //}

    //$scope.GenerateFilterExcel = function () {
    //    progress(0, 'Loading...', true);
    //    progress(0, 'Loading...', true);
    //    var Filterparams = {
    //        skipHeader: false,
    //        skipFooters: false,
    //        skipGroups: false,
    //        allColumns: false,
    //        onlySelected: false,
    //        columnSeparator: ',',
    //        fileName: "clsDemandPlan.csv"
    //    };
    //    $scope.gridOptions.api.exportDataAsCsv(Filterparams);
    //    setTimeout(function () {
    //        progress(0, 'Loading...', false);
    //    }, 1000);
    //}
    //$scope.GenReport = function (DemandPlan, Type) {
    //    console.log($scope.gridata);


    //$scope.ComputeData = function () {
    //    progress(0, 'Loading...', true);

    //    $scope.dataobject = { savelst: $scope.SpcTypeCntLst, REQID: $scope.REQ_ID };
    //    $.ajax({
    //        url: UtilityService.path + "/api/SpaceProjection/ComputeData",
    //        type: "POST",
    //        data: $scope.dataobject,
    //        cache: false,
    //        success: function (response) {
    //            console.log(response);
    //            $scope.GridData = response.Table;

    //            $scope.ShowGrid = 1;
    //            $scope.uploadEnable = 0;
    //            console.log($scope.GridData);
    //            $scope.gridOptions.api.setRowData($scope.GridData);
    //            //angular.forEach($scope.GridData, function (Value, index) {
    //            //    Value.TOTALSEAT_PENDING = Value.TOTAL_SEATS_REQUIRED;
    //            //});
    //            //$scope.gridOptions.api.refreshView();
    //            progress(0, '', false);
    //        }
    //    });

    //}

    //$scope.REQ_ID = "";
    ////To save a record
    //$scope.SaveDetails = function () {
    //    progress(0, 'Loading...', true);
    //    console.log($scope.gridOptions.rowData);


    //    angular.forEach($scope.gridOptions.rowData, function (value, index) {
    //        $scope.SpcTypeCnt = {};
    //        $scope.SpcTypeCnt.REQWS = value.REQWS;
    //        $scope.SpcTypeCnt.REQCAB = value.REQCAB;
    //        $scope.SpcTypeCnt.VER_NAME = value.VER_NAME;
    //        $scope.SpcTypeCnt.COST_CENTER_NAME = value.COST_CENTER_NAME;
    //        $scope.SpcTypeCnt.REQID = $scope.REQ_ID
    //        $scope.SpcTypeCntLst.push($scope.SpcTypeCnt);
    //    });



    //    $scope.gridOptions.api.forEachNodeAfterFilterAndSort(function (val, index) {
    //        $scope.OSPdata.push(val.data);
    //    });

    //    $scope.dataobject = { savelst: $scope.SpcTypeCntLst, REQID: $scope.REQ_ID };

    //    console.log($scope.dataobject);

    //    SpaceProjectionService.SaveDetails($scope.dataobject).then(function (dataobj) {
    //        console.log(dataobj);
    //        if (dataobj.Table[0].FLAG == 1) {
    //            progress(0, '', false);
    //            showNotification('success', 8, 'bottom-right', "Saved Successfully and Req Id is:-" + dataobj.Table[0].REQID);
    //            $('#FileUpl').val('');
    //            $scope.uploadEnable = 1;
    //            $scope.ShowGrid = 0;
    //            $scope.gridOptions.api.setRowData([]);
    //        }
    //        else {
    //            progress(0, 'Loading...', false);
    //            showNotification('error', 8, 'bottom-right', "Somethig went wrong. Please try again later.");
    //        }
    //    }, function (response) {
    //        progress(0, '', false);
    //    });
    //}
    ////function createReqWS(params) {

    ////    var editing = false;
    ////    var RWS;
    ////    RWS = document.createElement('input');
    ////    RWS.setAttribute('ng-model', 'data.REQWS');
    ////   // RWS.setAttribute('ng-change', 'refreshGrid(data)');
    ////    RWS.type = "input";
    ////    RWS.id = params.rowIndex;

    ////    return RWS;
    ////}
    ////function createReqCAB(params) {

    ////    var editing = false;
    ////    var RWS;
    ////    RWS = document.createElement('input');
    ////    RWS.setAttribute('ng-model', 'data.REQCAB');
    ////    // RWS.setAttribute('ng-change', 'refreshGrid(data)');
    ////    RWS.type = "input";
    ////    RWS.id = params.rowIndex;

    ////    return RWS;
    ////}
    //$scope.SpcTypeCntLst = [];
    //$scope.SpcTypeCnt = {};
    //$scope.refreshGrid = function (data, spacetype) {

    //    //_.remove($scope.SpcTypeCntLst, function (space) { return space.ID == data.EU_ID });
    //    //$scope.SpcTypeCntLst = _.reject($scope.SpcTypeCntLst, function (d) {
    //    //    return d.ID == data.EU_ID;
    //    //});
    //    //$scope.SpcTypeCnt = {};
    //    //$scope.SpcTypeCnt.ID = data.EU_ID;
    //    //$scope.SpcTypeCnt.REQWS = data.REQWS;
    //    //$scope.SpcTypeCnt.REQCAB = data.REQCAB;
    //    ////$scope.SpcTypeCnt.REQTWD = data.REQTWD;
    //    //$scope.SpcTypeCntLst.push($scope.SpcTypeCnt);
    //    var AVAILABLE_WS = 0, AVAILABLE_CB = 0; // AVAILABLE_TWD=0
    //    if (data.refAVAILABLE_WS == null)
    //        data.refAVAILABLE_WS = data.AVAILABLE_WS;

    //    var result = $filter('filter')($scope.GridData, { LOCATION: data.LOCATION, TOWER: data.TOWER });

    //    total_CB = _.sumBy(result, function (o) {
    //        if (o.REQCAB == "")
    //            return 0;
    //        else
    //            return parseInt(o.REQCAB);
    //    });

    //    total_WS = _.sumBy(result, function (o) {
    //        if (o.REQWS == "")
    //            return 0;
    //        else
    //            return parseInt(o.REQWS);
    //    });
    //    //total_TWD = _.sumBy(result, function (o) {
    //    //    if (o.REQTWD == "")
    //    //        return 0;
    //    //    else
    //    //        return parseInt(o.REQTWD);
    //    //});

    //    if (spacetype == "WS") {
    //        if (parseInt(data.AVAILABLE_SEATS) != 0 && data.REQWS != null && data.REQWS != "") {
    //            AVAILABLE_WS = data.AVAILABLE_SEATS - (total_WS + total_CB);//+ total_TWD
    //            PENDING_WS = data.TOTAL_SEATS_REQUIRED - (total_WS + total_CB); //+total_TWD
    //            angular.forEach(result, function (Value, index) {
    //                Value.TOTALSEAT = AVAILABLE_WS;
    //                Value.TOTALSEAT_PENDING = PENDING_WS;
    //            });
    //            $scope.gridOptions.api.refreshView();
    //        }
    //    }
    //    //else if (spacetype == "TWD") {
    //    //    if (parseInt(data.AVAILABLE_SEATS) != 0 && data.REQTWD != null && data.REQTWD != "") {
    //    //        AVAILABLE_TWD = data.AVAILABLE_SEATS - (total_WS + total_CB + total_TWD);
    //    //         angular.forEach(result, function (Value, index) {
    //    //             Value.TOTALSEAT = AVAILABLE_TWD;
    //    //         });
    //    //         $scope.gridOptions.api.refreshView();
    //    //     }
    //    // }
    //    else {
    //        if (parseInt(data.AVAILABLE_SEATS) != 0 && data.REQCAB != null && data.REQCAB != "") {

    //            AVAILABLE_CB = data.AVAILABLE_SEATS - (total_WS + total_CB);//+ total_TWD
    //            PENDING_CB = data.TOTAL_SEATS_REQUIRED - (total_WS + total_CB); //+total_TWD
    //            angular.forEach(result, function (Value, index) {
    //                Value.TOTALSEAT = AVAILABLE_CB;
    //                Value.TOTALSEAT_PENDING = PENDING_CB;
    //            });
    //            $scope.gridOptions.api.refreshView();
    //        }
    //    }

    //    // console.log($scope.SpcTypeCntLst);
    //}

}]);