﻿app.service("ViewSpaceProjectionService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    var deferred = $q.defer();

    this.GetPlanList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ViewSpaceProjection/GetPlanRequisitions')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetPlansListbyReqId = function (REQ_ID) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewSpaceProjection/GetPlansListbyReqId?REQ_ID=' + REQ_ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //this.UpdatePlanRequisition = function (ReqObj) {
    //    var deferred = $q.defer();
    //    return $http.post(UtilityService.path + '/api/ViewSpaceProjection/UpdatePlanRequisition', ReqObj)
    //      .then(function (response) {
    //          deferred.resolve(response.data);
    //          return deferred.promise;
    //      }, function (response) {
    //          deferred.reject(response);
    //          return deferred.promise;
    //      });
    //};

    this.UpdateDetails = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewSpaceProjection/UpdateDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.CancelDetails = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewSpaceProjection/CancelDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('ViewSpaceProjectionController', ['$scope', '$q', 'ViewSpaceProjectionService', 'UtilityService', '$filter', function ($scope, $q, ViewSpaceProjectionService, UtilityService, $filter) {
    /////  Upload File
    $scope.ShowGrid = 0;
    $scope.Viewstatus = 0;
    $scope.selectedPlans = [];
    $scope.tickedSpaces = [];

    $scope.EnableStatus = 0;

    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });


    ViewSpaceProjectionService.GetPlanList().then(function (data) {
        progress(0, 'Loading...', true);
        if (data.data != null) {
            $scope.Viewstatus = 1;
            $scope.gridata1 = data.data;
            $scope.ViewOfficePlanReqs.api.setRowData([]);
            $scope.ViewOfficePlanReqs.api.setRowData($scope.gridata1);
            $scope.ViewOfficePlanReqs.api.refreshHeader();
            progress(0, '', false);
        }
        else {
            $scope.ViewOfficePlanReqs.api.setRowData([]);
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', data.Message);

        }
    }, function (response) {
        progress(0, '', false);
    });


    var columnDefs = [
        { headerName: "Requisition ID", cellClass: "grid-align", field: "OSP_REQ_ID", template: '<a ng-click="onRowSelectedFunc(data)">{{data.OSP_REQ_ID}}</a>' },
        { headerName: "Created Date", cellClass: "grid-align", field: "OSP_CREATED_DATE", suppressMenu: true, },
        { headerName: "Created By", cellClass: "grid-align", field: "OSP_CREATED_BY", suppressMenu: true, },
        { headerName: "Status", cellClass: "grid-align", field: "OSP_STA_ID", suppressMenu: true, },
    ];

    $scope.ViewOfficePlanReqs = {
        columnDefs: columnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableFilter: true,
        enableCellSelection: false,
        enableSorting: true,
    };

    var column1Defs = [
        { headerName: "City", cellClass: "grid-align", width: 130, field: "CITY", },
        { headerName: "Location", cellClass: "grid-align", field: "LOCATION", width: 210, suppressMenu: true, },
        { headerName: "Facility Name", cellClass: "grid-align", field: "Facility", width: 210, suppressMenu: true, },
        { headerName: "", cellClass: "grid-align", field: "VER_NAME", width: 500, suppressMenu: true, },
        { headerName: "", cellClass: "grid-align", field: "COST_CENTER_NAME", width: 550, suppressMenu: true, },
        { headerName: "Grade", cellClass: "grid-align", field: "GRADE", width: 50, suppressMenu: true, },
        { headerName: "Q1", cellClass: "grid-align", field: "Q1", width: 50, suppressMenu: true, },
        { headerName: "Q2", cellClass: "grid-align", field: "Q2", width: 50, suppressMenu: true, },

        { headerName: "Q3", cellClass: "grid-align", field: "Q3", width: 50, suppressMenu: true, },
        { headerName: "Q4", cellClass: "grid-align", field: "Q4", width: 50, suppressMenu: true, },

        { headerName: "FY1", cellClass: "grid-align", field: "FY1", width: 50, suppressMenu: true, },
        { headerName: "FY2", cellClass: "grid-align", field: "FY2", width: 50, suppressMenu: true, },

        { headerName: "FY3", cellClass: "grid-align", field: "FY3", width: 50, suppressMenu: true, },
        { headerName: "Built Capacity", cellClass: "grid-align", field: "BUILD_CAPACITY", width: 150, suppressMenu: true },
        { headerName: "Utilized Capacity", cellClass: "grid-align", field: "UTILIZED_SEATS", width: 150, suppressMenu: true },
        { headerName: "Available Capacity", cellClass: "grid-align", field: "AVAILABLE_SEATS", width: 150, suppressMenu: true, },
        { headerName: "New Capacity Required", cellClass: "grid-align", field: "TOTAL_SEATS_REQUIRED", width: 150, suppressMenu: true, },
        { headerName: "Vacant WS", cellClass: "grid-align", field: "AVAILABLE_WS", width: 100, suppressMenu: true, },
        { headerName: "Allocated WS", cellClass: "grid-align", field: "REQ_WS", suppressMenu: true, width: 100, template: "<input type='textbox' ng-model='data.REQ_WS' ng-blur ='refreshGrid(data,\"WS\")' />" },
        { headerName: "Vacant Cabins", cellClass: "grid-align", field: "AVAILABLE_CB", width: 150, suppressMenu: true, },
        { headerName: "Allocated Cabins", cellClass: "grid-align", field: "REQ_CB", width: 150, suppressMenu: true, template: "<input type='textbox' ng-model='data.REQ_CB' ng-blur ='refreshGrid(data,\"CB\")' />" },
        //{ headerName: "Available TWD", cellClass: "grid-align", field: "AVAILABLE_TWD", width: 100,suppressMenu: true, },
        //{ headerName: "Allocated TWD", cellClass: "grid-align", field: "REQ_TWD", width: 100,suppressMenu: true, template: "<input type='textbox' ng-model='data.REQ_TWD' ng-blur ='refreshGrid(data,\"TWD\")' />" },
        { headerName: "Available Capacity After Allocation(Available Capacity-(Allocated WS+CB))", cellClass: "grid-align", field: "TOTALSEAT", width: 450, suppressMenu: true },
        { headerName: "Pending(Required-(Allocated WS+CB))", cellClass: "grid-align", field: "TOTALSEAT_PENDING", width: 300, suppressMenu: true },

    ];

    $scope.gridModifyOptions = {
        columnDefs: column1Defs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableFilter: true,
        enableCellSelection: false,
        enableSorting: true,
    };

    $scope.onRowSelectedFunc = function (data) {
        progress(0, 'Loading...', true);
        $scope.CurrentObj = data;
        ViewSpaceProjectionService.GetPlansListbyReqId(data.OSP_REQ_ID).then(function (data) {
            progress(0, 'Loading...', true);
            if (data.data != null) {
                $scope.Viewstatus = 0;
                $scope.EnableStatus = 1;
                $scope.gridata2 = data.data;
                var output =
                    _($scope.gridata2)
                        .groupBy('Facility')
                        .map((objs, key) => ({
                            'Facility': key,
                            'REQWS': _.sumBy(objs, 'REQ_WS'),
                            //'REQTWD': _.sumBy(objs, 'REQ_TWD'),
                            'REQCB': _.sumBy(objs, 'REQ_CB')
                        }))
                        .value();
                console.log(output);

                angular.forEach(output, function (Value, index) {
                    angular.forEach($scope.gridata2, function (inrValue, inrIndex) {
                        if (inrValue.Facility == Value.Facility) {
                            inrValue.TOTALSEAT = inrValue.AVAILABLE_SEATS - (Value.REQWS + Value.REQCB); //+ Value.REQTWD
                            inrValue.TOTALSEAT_PENDING = inrValue.TOTAL_SEATS_REQUIRED - (Value.REQWS + Value.REQCB); //+ Value.REQTWD
                        }
                    });
                });

                console.log($scope.gridata2);
                $scope.gridModifyOptions.api.setRowData([]);
                $scope.gridModifyOptions.api.setRowData($scope.gridata2);
                //$scope.gridModifyOptions.api.refreshHeader();
                $scope.gridModifyOptions.columnApi.getColumn("VER_NAME").colDef.headerName = $scope.BsmDet.Parent;
                $scope.gridModifyOptions.columnApi.getColumn("COST_CENTER_NAME").colDef.headerName = $scope.BsmDet.Child;
                $scope.gridModifyOptions.api.refreshHeader();
                progress(0, '', false);
            }
            else {

                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', data.Message);

            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.UpdatePlanRequisition = function () {
        $scope.tickedSpaces = [];
        progress(0, 'Loading...', true);
        $scope.dataobject = { savelst: $scope.SpcTypeCntLst, REQID: $scope.CurrentObj.OSP_REQ_ID };
        console.log($scope.dataobject);
        ViewSpaceProjectionService.UpdateDetails($scope.dataobject).then(function (dataobj) {
            $scope.Viewstatus = 1;
            $scope.EnableStatus = 0;
            progress(0, '', false);
            showNotification('success', 8, 'bottom-right', 'Request Modified successfully');
        }, function (response) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', 'Something went wrong');
        });
        progress(0, '', false);
    }

    $scope.Cancel = function () {
        $scope.tickedSpaces = [];
        progress(0, 'Loading...', true);
        $scope.dataobject = { REQID: $scope.CurrentObj.OSP_REQ_ID };

        ViewSpaceProjectionService.CancelDetails($scope.dataobject).then(function (dataobj) {
            $scope.Viewstatus = 1;
            $scope.EnableStatus = 0;
            progress(0, 'Loading...', false);
            showNotification('success', 8, 'bottom-right', 'Request cancelled successfully');
        }, function (response) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', 'Something went wrong');
        });
        progress(0, 'Loading...', false);
    }

    $scope.back = function () {
        $scope.Viewstatus = 1;
        $scope.EnableStatus = 0;
    }

    $scope.SpcTypeCntLst = [];
    $scope.SpcTypeCnt = {};

    $scope.refreshGrid = function (data, spacetype) {
        console.log(spacetype);
        //_.remove($scope.SpcTypeCntLst, function (space) { return space.ID == data.EU_ID });
        $scope.SpcTypeCntLst = _.reject($scope.SpcTypeCntLst, function (d) {
            return d.ID == data.EU_ID;
        });
        $scope.SpcTypeCnt = {};
        $scope.SpcTypeCnt.ID = data.EU_ID;
        $scope.SpcTypeCnt.REQWS = data.REQ_WS;
        $scope.SpcTypeCnt.REQCAB = data.REQ_CB;
        //$scope.SpcTypeCnt.REQTWD = data.REQ_TWD;
        $scope.SpcTypeCntLst.push($scope.SpcTypeCnt);
        var AVAILABLE_WS = 0, AVAILABLE_CB = 0; //,AVAILABLE_TWD=0
        var PENDING_WS = 0, PENDING_CB = 0; //,PENDING_TWD=0
        if (data.refAVAILABLE_WS == null)
            data.refAVAILABLE_WS = data.AVAILABLE_WS;

        var result = $filter('filter')($scope.gridata2, { LOCATION: data.LOCATION, Facility: data.Facility });

        total_CB = _.sumBy(result, function (o) {
            if (o.REQ_CB == "" || o.REQ_CB == null)
                return 0;
            else
                return parseInt(o.REQ_CB);
        });

        total_WS = _.sumBy(result, function (o) {
            if (o.REQ_WS == "" || o.REQ_WS == null)
                return 0;
            else
                return parseInt(o.REQ_WS);
        });

        //total_TWD = _.sumBy(result, function (o) {
        //    if (o.REQ_TWD == "" || o.REQ_TWD == null)
        //        return 0;
        //    else
        //        return parseInt(o.REQ_TWD);
        //});

        if (spacetype == "WS") {
            if (parseInt(data.AVAILABLE_SEATS) != 0 && data.REQ_WS != null && data.REQ_WS != "") {
                AVAILABLE_WS = data.AVAILABLE_SEATS - (total_WS + total_CB); //+total_TWD
                PENDING_WS = data.TOTAL_SEATS_REQUIRED - (total_WS + total_CB); //+total_TWD
                angular.forEach(result, function (Value, index) {
                    Value.TOTALSEAT = AVAILABLE_WS;
                    Value.TOTALSEAT_PENDING = PENDING_WS;
                });
                $scope.gridModifyOptions.api.refreshView();
            }
        }
        //else if (spacetype == "TWD") {
        //    if (parseInt(data.AVAILABLE_SEATS) != 0 && data.REQ_TWD != null && data.REQ_TWD != "") {
        //        AVAILABLE_TWD = data.AVAILABLE_SEATS - (total_WS + total_CB+total_TWD);
        //        PENDING_TWD = data.TOTAL_SEATS_REQUIRED - (total_WS + total_CB+total_TWD);
        //        angular.forEach(result, function (Value, index) {
        //            Value.TOTALSEAT = AVAILABLE_TWD;
        //            Value.TOTALSEAT_PENDING = PENDING_TWD;
        //        });
        //        $scope.gridModifyOptions.api.refreshView();
        //    }
        //}
        else {
            if (parseInt(data.AVAILABLE_SEATS) != 0 && data.REQ_CB != null && data.REQ_CB != "") {

                AVAILABLE_CB = data.AVAILABLE_SEATS - (total_WS + total_CB); //+total_TWD
                PENDING_CB = data.TOTAL_SEATS_REQUIRED - (total_WS + total_CB); //+total_TWD
                angular.forEach(result, function (Value, index) {
                    Value.TOTALSEAT = AVAILABLE_CB;
                    Value.TOTALSEAT_PENDING = PENDING_CB;
                });
                $scope.gridModifyOptions.api.refreshView();
            }
        }
    }

}]);