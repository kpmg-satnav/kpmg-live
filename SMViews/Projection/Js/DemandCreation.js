﻿app.service("DemandCreationService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetPendingList = function () {
     
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/DemandCreation/GetDemandRequests')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.Submit = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/DemandCreation/Submit', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('DemandCreationController', function ($scope, $q, UtilityService, $filter, DemandCreationService)
{

    $scope.Viewstatus = 0;
    $scope.Locations = [];
    $scope.Geography = [];
    $scope.Verticals = [];
    $scope.Function = [];
    $scope.ParentEntity = [];

    UtilityService.getLocations(1).then(function (response) {
        if (response.data != null) {
            $scope.Locations = response.data;
            //angular.forEach($scope.Locations, function (value, key) {
            //    value.ticked = true;
            //    $scope.Customized.Locations.push(value);
            //});
        }
    });

    UtilityService.getFunction(1).then(function (response) {
        if (response.data != null) {
            $scope.Function = response.data;
        }
        UtilityService.getGeography(1).then(function (response) {

            if (response.data != null) {
                $scope.Geography = response.data;
            }
        });
    });

    UtilityService.getVerticals(1).then(function (response) {
        if (response.data != null) {
            $scope.Verticals = response.data;
        }
    });
    UtilityService.getParentEntity(1).then(function (response) {
        if (response.data != null) {
            $scope.ParentEntity = response.data;
        }
    });

    $scope.ColumnDefs = [
        { headerName: "Demand ID", width: 100, field: "Demand ID", cellClass: "grid-align", filter: 'set', /*template: '<a ng-click="onRowSelectedFunc(data)">{{data.Demand ID}}</a>' */},
        { headerName: "Demand Raised By", field: "CREATED_BY", cellClass: "grid-align", suppressMenu: true, },
        { headerName: "Geography", width: 100, field: "Geography", cellClass: "grid-align", suppressMenu: true },
        { headerName: "Teams", width: 100, field: "Teams", cellClass: "grid-align", suppressMenu: true },
        { headerName: "Function", width: 100, field: "Function", cellClass: "grid-align", suppressMenu: true },
        { headerName: "Sub Function", width: 100, field: "Sub Function", cellClass: "grid-align", suppressMenu: true },
        { headerName: "Location", width: 100, field: "Location", cellClass: "grid-align", suppressMenu: true },
        { headerName: "Month 1", width: 100, field: "Month1", cellClass: "grid-align", suppressMenu: true },
        { headerName: "Month 2", width: 100, field: "Month2", cellClass: "grid-align", suppressMenu: true },
        { headerName: "Month 3", width: 100, field: "Month3", cellClass: "grid-align", suppressMenu: true },     
        { headerName: "CurrentYear 1", width: 100, field: "CurrentYearQ1", cellClass: "grid-align", suppressMenu: true },
        { headerName: "CurrentYear 2", width: 100, field: "CurrentYearQ2", cellClass: "grid-align", suppressMenu: true },
        { headerName: "CurrentYear 3", width: 100, field: "CurrentYearQ3", cellClass: "grid-align", suppressMenu: true },      
        { headerName: "NextYear 1", width: 100, field: "NextYearQ1", cellClass: "grid-align", suppressMenu: true },
        { headerName: "NextYear 2", width: 100, field: "NextYearQ2", cellClass: "grid-align", suppressMenu: true },
        { headerName: "NextYear 3", width: 100, field: "NextYearQ3", cellClass: "grid-align", suppressMenu: true },     
        { headerName: "Demand Status", field: "Demand Status", cellClass: "grid-align", suppressMenu: true }
    ];



    $scope.gridOptions = {
        columnDefs: $scope.ColumnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple',
        enableColResize: true,

    };

    function GetPendingList()
    {
        DemandCreationService.GetPendingList().then(function (data) {
            console.log(data);
            progress(0, 'Loading...', true);
            if (data != null) {
                debugger;
                $scope.gridOptions.api.setRow
                $scope.gridata = data;
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.gridOptions.api.refreshHeader();
                progress(0, '', false);
            }
            else {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', data.Message);

            }
        }, function (response) {
            progress(0, '', false);
        });
    };

    //GetPendingList();

    //$scope.SaveDemandCreation = function () {

    //};
  

    $scope.submit = function () {
        debugger;
        $scope.dataobj = {
            Geography: $scope.Geography,
            Location: $scope.Location,
            Function: $scope.Function,
            Verticals: $scope.Verticals,
            ParentEntity: $scope.ParentEntity,
            Jan: $scope.DemandCreation.Jan,
            Feb: $scope.DemandCreation.Feb,
            Mar: $scope.DemandCreation.Mar,
            Apr: $scope.DemandCreation.Apr,
            May: $scope.DemandCreation.May,
            Jun: $scope.DemandCreation.Jun,
            Jal: $scope.DemandCreation.Jal,
            Aug: $scope.DemandCreation.Aug,
            Sep: $scope.DemandCreation.Sep,
            Oct: $scope.DemandCreation.Oct,
            Nov: $scope.DemandCreation.Nov,
            Dec: $scope.DemandCreation.Dec,
            CYQ1: $scope.DemandCreation.CYQ1,
            CYQ2: $scope.DemandCreation.CYQ2,
            CYQ3: $scope.DemandCreation.CYQ3,
            CYQ4: $scope.DemandCreation.CYQ4,
            NYQ1: $scope.DemandCreation.NYQ1,
            NYQ2: $scope.DemandCreation.NYQ2,
            NYQ3: $scope.DemandCreation.NYQ3,
            NYQ4: $scope.DemandCreation.NYQ4
        };
        console.log($scope.dataobj);
        DemandCreationService.Submit(dataobj).then(function (response) {
            if (response != null) {
                showNotification('success', 8, 'bottom-right', response.Message);
                $scope.selectedspaces = [];
                $scope.grid = false;
                $scope.Clear();

            }
        });
    }
});

    