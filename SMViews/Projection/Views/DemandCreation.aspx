﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DemandCreation.aspx.cs" Inherits="SMViews_Projection_Views_DemandCreation" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        span.error {
            color: red;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />

</head>

<body data-ng-controller="DemandCreationController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Shift Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Demand Creation</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <%-- <div class="row">                               
                                    <div class="panel-body color">--%>
                    <form id="form1" runat="server">


                        <%--<label class="control-label"><span style="color: red;">&nbsp&nbsp**</span>&nbsp&nbsp{{Autoselect}}</label>--%>
                        <%--<div class="panel-body">--%>
                        <div class="row form-inline">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.GRY_NAME.$invalid}">
                                    <label class="control-label">Geography <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Geography" data-output-model="DemandCreation.Geography" data-button-label="icon GRY_NAME"
                                        data-item-label="icon GRY_NAME maker"
                                        data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="DemandCreation.Geography[0]" name="GRY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.GRY_NAME.$invalid" style="color: red">Please select Geography </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.VER_NAME.$invalid}">
                                    <label class="control-label">Teams <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Verticals" data-output-model="DemandCreation.Verticals" data-button-label="icon VER_NAME"
                                        data-item-label="icon VER_NAME maker"
                                        data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="DemandCreation.Verticals[0]" name="VER_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.VER_NAME.$invalid" style="color: red">Please select Teams </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.Fun_NAME.$invalid}">
                                    <label class="control-label">Function <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Function" data-output-model="DemandCreation.Function" data-button-label="icon Fun_NAME"
                                        data-item-label="icon Fun_NAME maker"
                                        data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="DemandCreation.Function[0]" name="Fun_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.Fun_NAME.$invalid" style="color: red">Please select Function </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.PE_NAME.$invalid}">
                                    <label class="control-label">Sub Function <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="ParentEntity" data-output-model="DemandCreation.ParentEntity" data-button-label="icon PE_NAME"
                                        data-item-label="icon PE_NAME maker"
                                        data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="DemandCreation.ParentEntity[0]" name="PE_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.PE_NAME.$invalid" style="color: red">Please select Sub Function </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.LCM_NAME.$invalid}">
                                    <label class="control-label">Location <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Locations" data-output-model="DemandCreation.Locations" data-button-label="icon LCM_NAME"
                                        data-item-label="icon LCM_NAME maker" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="DemandCreation.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                </div>
                            </div>
                        </div>
                        <%--   <div class="col-md-1 col-sm-1 col-xs-2">
                                <div class="form-group">
                                    <label>JAN<span style="color: red;">*</span></label>
                                    <asp:TextBox ID="txtVerticalName" runat="server" CssClass="form-control"
                                        TabIndex="3" MaxLength="50"></asp:TextBox>
                                </div>
                            </div>--%>
                    </form>
                </div>
                <div class="row form-inline">
                    <div class="panel-heading">
                        <h3>Current Financial year</h3>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>JAN</label>
                            <div data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.JAN.$invalid}">
                                <input id="JAN" type="text" name="JAN" maxlength="3" data-ng-model="DemandCreation.JAN" data-ng-pattern="/[0-9.]/" data-ng-model="DemandCreation.JAN" autofocus class="form-control" required="required" />&nbsp;  
                       <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.JAN.$invalid" style="color: red">please enter number </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>FEB</label>
                            <div data-ng_class="{'has_error':DemandCreation.$submitted && DemandCreation.JAN.$invalid}">
                                <input id="FEB" type="text" name="FEB" maxlength="3" data-ng-model="DemandCreation.FEB" data-ng_pattren="/[0-9.]/" data-ng-model="DemandCreation.JAN" autofocus class="form-control" required="required" />&nbsp;
                                        <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.FEB.$invalid" style="color: red">please enter number </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>MAR</label>
                            <div data-ng_class="{'has_error':DemandCreation.$submitted && DemandCreation.MAR.$invalid}">
                                <input id="MAR" type="text" name="MAR" maxlength="3" data-ng-model="DemandCreation.MAR" data-ng_pattren="/[0-9.]/" data-ng-model="DemandCreation.MAR" autofocus class="form-control" required="required" />&nbsp;
                                        <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.MAR.$invalid" style="color: red">please enter number </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>APR</label>
                            <div data-ng_class="{'has_error':DemandCreation.$submitted && DemandCreation.APR.$invalid}">
                                <input id="APR" type="text" name="APR" maxlength="3" data-ng-model="DemandCreation.APR" data-ng_pattren="/[0-9.]/" data-ng-model="DemandCreation.APR" autofocus class="form-control" required="required" />&nbsp;
                                        <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.APR.$invalid" style="color: red">please enter number </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>MAY</label>
                            <div data-ng_class="{'has_error':DemandCreation.$submitted && DemandCreation.MAY.$invalid}">
                                <input id="MAY" type="text" name="MAY" maxlength="3" data-ng-model="DemandCreation.MAY" data-ng_pattren="/[0-9.]/" data-ng-model="DemandCreation.MAY" autofocus class="form-control" required="required" />&nbsp;
                                        <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.MAY.$invalid" style="color: red">please enter number </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>JUN</label>
                            <div data-ng_class="{'has_error':DemandCreation.$submitted && DemandCreation.JUN.$invalid}">
                                <input id="JUN" type="text" name="JUN" maxlength="3" data-ng-model="DemandCreation.JUN" data-ng_pattren="/[0-9.]/" data-ng-model="DemandCreation.JUN" autofocus class="form-control" required="required" />&nbsp;
                                        <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.JUN.$invalid" style="color: red">please enter number </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>JUL</label>
                            <div data-ng_class="{'has_error':DemandCreation.$submitted && DemandCreation.JUL.$invalid}">
                                <input id="JUL" type="text" name="JUL" maxlength="3" data-ng-model="DemandCreation.JUL" data-ng_pattren="/[0-9.]/" data-ng-model="DemandCreation.JUL" autofocus class="form-control" required="required" />&nbsp;
                                        <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.JUL.$invalid" style="color: red">please enter number </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>AUG</label>
                            <div data-ng_class="{'has_error':DemandCreation.$submitted && DemandCreation.AUG.$invalid}">
                                <input id="AUG" type="text" name="AUG" maxlength="3" data-ng-model="DemandCreation.AUG" data-ng_pattren="/[0-9.]/" data-ng-model="DemandCreation.AUG" autofocus class="form-control" required="required" />&nbsp;
                                        <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.AUG.$invalid" style="color: red">please enter number </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>SEP</label>
                            <div data-ng_class="{'has_error':DemandCreation.$submitted && DemandCreation.SEP.$invalid}">
                                <input id="SEP" type="text" name="SEP" maxlength="3" data-ng-model="DemandCreation.SEP" data-ng_pattren="/[0-9.]/" data-ng-model="DemandCreation.SEP" autofocus class="form-control" required="required" />&nbsp;
                                        <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.SEP.$invalid" style="color: red">please enter number </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>OCT</label>
                            <div data-ng_class="{'has_error':DemandCreation.$submitted && DemandCreation.OCT.$invalid}">
                                <input id="OCT" type="text" name="OCT" maxlength="3" data-ng-model="DemandCreation.OCT" data-ng_pattren="/[0-9.]/" data-ng-model="DemandCreation.OCT" autofocus class="form-control" required="required" />&nbsp;
                                        <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.OCT.$invalid" style="color: red">please enter number </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>NOV</label>
                            <div data-ng_class="{'has_error':DemandCreation.$submitted && DemandCreation.NOV.$invalid}">
                                <input id="NOV" type="text" name="NOV" maxlength="3" data-ng-model="DemandCreation.NOV" data-ng_pattren="/[0-9.]/" data-ng-model="DemandCreation.NOV" autofocus class="form-control" required="required" />&nbsp;
                                        <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.NOV.$invalid" style="color: red">please enter number </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>DEC</label>
                            <div data-ng_class="{'has_error':DemandCreation.$submitted && DemandCreation.DEC.$invalid}">
                                <input id="DEC" type="text" name="DEC" maxlength="3" data-ng-model="DemandCreation.DEC" data-ng_pattren="/[0-9.]/" data-ng-model="DemandCreation.DEC" autofocus class="form-control" required="required" />&nbsp;
                                        <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.DEC.$invalid" style="color: red">please enter number </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-inline">
                    <div class="panel-heading">
                        <h3>Next Financial year &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Next To Next Financial year</h3> 
                    <%--</div>--%>
                     <%--   <h3>Next Financial year  style="margin-left:2.5em">Next To Next Financial year</p></h3>--%>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>Q1</label>
                            <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.NEXT FYQ1.$invalid" style="color: red">* </span>
                            <div data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.NEXT FYQ1.$invalid}">
                                <input id="NEXT FYQ1" type="text" name="NEXT FYQ1" maxlength="3" data-ng-model="DemandCreation.NEXT FYQ1" data-ng-pattern="/[0-9.]/" data-ng-model="DemandCreation.NEXT FYQ1" autofocus class="form-control" required="required" />&nbsp;  
                                    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>Q2</label>
                            <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.NEXT FYQ2.$invalid" style="color: red">* </span>
                            <div data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.NEXT FYQ2.$invalid}">
                                <input id="NEXT FYQ2" type="text" name="NEXT FYQ2" maxlength="3" data-ng-model="DemandCreation.NEXT FYQ2" data-ng-pattern="/[0-9.]/" data-ng-model="DemandCreation.NEXT FYQ2" autofocus class="form-control" required="required" />&nbsp;  
                                    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>Q3</label>
                            <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.NEXT FYQ3.$invalid" style="color: red">* </span>
                            <div data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.NEXT FYQ3.$invalid}">
                                <input id="NEXT FYQ3" type="text" name="NEXT FYQ3" maxlength="3" data-ng-model="DemandCreation.NEXT FYQ3" data-ng-pattern="/[0-9.]/" data-ng-model="DemandCreation.NEXT FYQ3" autofocus class="form-control" required="required" />&nbsp;  
                                    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>Q4</label><span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.NEXT FYQ4.$invalid" style="color: red">* </span>
                            <div data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.NEXT FYQ4.$invalid}">
                                <input id="NEXT FYQ4" type="text" name="NEXT FYQ4" maxlength="3" data-ng-model="DemandCreation.NEXT FYQ4" data-ng-pattern="/[0-9.]/" data-ng-model="DemandCreation.NEXT FYQ4" autofocus class="form-control" required="required" />&nbsp;  
                                    
                            </div>
                        </div>
                    </div>                        
                    <%--<h3>Next To Next Financial year</h3>--%>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>Q1</label>
                            <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.NextToNext FYQ1.$invalid" style="color: red">* </span>
                            <div data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.NextToNext FYQ1.$invalid}">
                                <input id="NextToNext FYQ1" type="text" name="NextToNext FYQ1" maxlength="3" data-ng-model="DemandCreation.NextToNext FYQ1" data-ng-pattern="/[0-9.]/" data-ng-model="DemandCreation.NextToNext FYQ1" autofocus class="form-control" required="required" />&nbsp;  
                                    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>Q2</label>
                            <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.NextToNext FYQ2.$invalid" style="color: red">* </span>
                            <div data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.NextToNext FYQ2.$invalid}">
                                <input id="NextToNext FYQ2" type="text" name="NextToNext FYQ2" maxlength="3" data-ng-model="DemandCreation.NextToNext FYQ2" data-ng-pattern="/[0-9.]/" data-ng-model="DemandCreation.NextToNext FYQ2" autofocus class="form-control" required="required" />&nbsp;  
                                    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>Q3</label>
                            <span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.NextToNext FYQ3.$invalid" style="color: red">* </span>
                            <div data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.NextToNext FYQ3.$invalid}">
                                <input id="NextToNext FYQ3" type="text" name="NextToNext FYQ3" maxlength="3" data-ng-model="DemandCreation.NextToNext FYQ3" data-ng-pattern="/[0-9.]/" data-ng-model="DemandCreation.NextToNext FYQ3" autofocus class="form-control" required="required" />&nbsp;  
                                    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="form-group">
                            <label>Q4</label><span class="error" data-ng-show="DemandCreation.$submitted && DemandCreation.NextToNext FYQ4.$invalid" style="color: red">* </span>
                            <div data-ng-class="{'has-error': DemandCreation.$submitted && DemandCreation.NextToNext FYQ4.$invalid}">
                                <input id="NextToNext FYQ4" type="text" name="NextToNext FYQ4" maxlength="3" data-ng-model="DemandCreation.NextToNext FYQ4" data-ng-pattern="/[0-9.]/" data-ng-model="DemandCreation.NextToNext FYQ4" autofocus class="form-control" required="required" />&nbsp;  
                                    
                            </div>
                        </div>
                    </div>
                     </div>
                     </div>
                </div>

                <div class="clearfix" ng-show="DocumentDetails.length!=0">
                    <div class="col-md-12">
                        <div class="box">
                            <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                        </div>
                    </div>
                </div>
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-primary" data-ng-click="submit()">Submit</button>
                    <button type="Back" class="btn btn-primary" data-ng-click="Back()">Back</button>
                </div>
                <%--  <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnSubmit" runat="server" Width="76px" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" TabIndex="7"></asp:Button>
                <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" TabIndex="8"></asp:Button>
                <div class="row">
                    <div class="col-md-12">
                        <br />
                        <div style="height: 350px">
                            <div data-ag-grid="gridOptions" style="height: 100%; width: 100%" class="ag-blue"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script defer src="../../Utility.min.js"></script>
    <script src="../Js/DemandCreation.js"></script>
    <script src="../../Utility.js" defer></script>
</body>
</html>
