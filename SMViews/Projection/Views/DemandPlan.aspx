﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DemandPlan.aspx.cs" Inherits="SMViews_Projection_Views_DemandPlan" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        span.error {
            color: red;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.css" rel="stylesheet" />
</head>

<body data-ng-controller="DemandPlanController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Shift Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Demand Planning</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 50px;">
                            <%-- <div class="row">                               
                                    <div class="panel-body color">--%>
                            <form role="form" id="FileUsrUpl" name="FileUsrUpl" data-valid-submit="UploadFile()" novalidate>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text=" Click here to View the Template" NavigateUrl="~/Masters/Mas_Webfiles/Office_Space_Planning.xlsx"></asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <label class="custom-file">
                                        <input type="file" name="UPLFILE" id="FileUpl" required="" accept=".xls,.xlsx" class="custom-file-input">
                                        <span class="custom-file-control"></span>
                                    </label>
                                </div>
                                <div class="box-footer text-right">
                                    <input type="submit" data-ng-disabled="uploadEnable == 0" id="btnUpload" class="btn btn-primary custom-button-color" value="Upload" />
                                    <%--<input type="button" data-ng-show="ShowGrid == 1" id="btnCompute" data-ng-click="ComputeData()" class="btn btn-primary custom-button-color" value="Compute" />--%>
                                </div>
                                <br />
                                <div data-ng-show="ShowGrid == 1">
                                    <%--<input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />--%>
                                    <div data-ag-grid="gridOptions"  class="ag-blue"  style="height: 310px; width: 100%"></div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.draw.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/wicket-leaflet.js"></script>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/leaflet.label-src.js"></script>
    <script defer src="../../../BootStrapCSS/leaflet/addons/leaflet.contextmenu.js"></script>
    <script defer src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../Scripts/moment.min.js"></script>
    <script defer src="../../Utility.min.js"></script>
    <script defer src="../Js/DemandPlan.js"></script>
</body>
</html>

