﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="../../BootStrapCSS/Bootstrapswitch/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Bootstrapswitch/css/bootstrap.css" rel="stylesheet" />
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />



    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>

</head>

<body data-ng-controller="SpaceProjectioncontroller" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Space Projection</legend>
                    </fieldset>


                    <form role="form" id="form2" name="frmSpaceProjection" class="form-horizontal well" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-primary">
                                    <div class="box-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <label>Country Name<span style="color: red;">*</span></label>
                                                <div data-ng-class="{'has-error': frmSpaceProjection.$submitted && frmSpaceProjection.SSP_CNY_CODE.$invalid}">
                                                    <select id="ddlCountry" name="SSP_CNY_CODE" class="form-control" data-ng-model="SP_MasterVals.SSP_CNY_CODE" required data-live-search="true">
                                                        <option value="">--Select--</option>
                                                        <option data-ng-repeat="countrycategory in countrylist" value="{{countrycategory.CNY_CODE}}">{{countrycategory.CNY_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmSpaceProjection.$submitted && frmSpaceProjection.SSP_CNY_CODE.$invalid" style="color: red">Please Select Country</span>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <label>Years<span style="color: red;">*</span></label>

                                                <div data-ng-class="{'has-error': frmSpaceProjection.$submitted && frmSpaceProjection.SSP_YEAR.$invalid}">
                                                    <select id="ddlSPYear" name="SSP_YEAR" class="form-control" data-ng-model="SP_MasterVals.SSP_YEAR" required data-live-search="true">
                                                        <option value="">--Select--</option>
                                                        <option data-ng-repeat="spyear in yearlist" value="{{spyear.Year}}">{{spyear.Year}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmSpaceProjection.$submitted && frmSpaceProjection.SSP_YEAR.$invalid" style="color: red">Please Select Year</span>
                                                </div>

                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <label>Vertical<span style="color: red;">*</span></label>

                                                <div data-ng-class="{'has-error': frmSpaceProjection.$submitted && frmSpaceProjection.VER_NAME.$invalid}">

                                                    <select id="ddlMain" name="VER_NAME" class="form-control" data-ng-model="SP_MasterVals.VER_NAME" required data-live-search="true" data-ng-change="GetSPRJ_Details()">
                                                        <option value="">-- Select --</option>
                                                        <option data-ng-repeat="vertical in VerticalList" value="{{vertical.VER_CODE}}">{{vertical.VER_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmSpaceProjection.$submitted && frmSpaceProjection.VER_NAME.$invalid" style="color: red">Please Select Vertical</span>
                                                </div>

                                            </div>
                                        </div>
                                        <div>&nbsp;</div>

                                        <div>&nbsp;</div>

                                    </div>


                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12" data-ng-show="SpaceProjection_Details">
                                <div class="box">
                                    <div class="box-body">
                                        <table data-ng-table="tableParams" class="table ng-table-responsive">
                                            <tr>
                                                <th>City </th>
                                                <th data-ng-repeat="monthNames in MonthListdata">{{monthNames.monthName}} {{ selectedYear }}</th>
                                            </tr>
                                            <tr data-ng-repeat="spMaster in SP_Master">
                                                <td style="vertical-align: central !important;">
                                                    <label>{{spMaster.Key.CTY_NAME}}</label>
                                                </td>

                                                <td data-ng-repeat="spDetails in spMaster.Value" data-ng-form="innerForm">
                                                    <div class="input-group margin">
                                                        <div data-ng-class="{'has-error': innerForm.$submitted && innerForm.SSPD_PRJ_VALUE.$invalid}">
                                                            <input class="form-control input-sm" placeholder="" data-ng-model="spDetails.Value.SSPD_PRJ_VALUE" data-ng-pattern="/^[0-9]{1,5}$/" name="SSPD_PRJ_VALUE" required="" type="text" data-ng-change="doSum($index)">
                                                        </div>
                                                        <span class="error" data-ng-show="innerForm.SSPD_PRJ_VALUE.$error.pattern" style="color: red">Enter Valid Number</span>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <th>SUM </th>
                                                <th data-ng-repeat="total in totals">{{total.Value }}
                                                </th>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                                <div class="clearfix">

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <label>Remarks</label>
                                        <div>
                                        <textarea rows="2" name="SSP_REQ_REM" class="form-control" data-ng-model="SP_MasterVals.SSP_REQ_REM" required></textarea>
                                        
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <input type="submit" value="Save as Draft" data-ng-click="SaveSP_Details_As_Draft('1026');" class="btn btn-primary" />
                                            <input type="submit" value="Submit Projection" data-ng-click="SaveSP_Details_As_Draft('1027');" class="btn btn-primary" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../Scripts/aggrid/ag-grid.min.js"></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script defer src="../../Utility.js"></script>
    <script defer src="../Js/SpaceProjection.js"></script>
</body>
</html>
