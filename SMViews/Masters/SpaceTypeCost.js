﻿app.service("SpaceTypeCostService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    this.getFloordata = function (flrcode) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceTypeCost/BindGridData?flrcode=' + flrcode + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.getLocationdata = function (lcmcode) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceTypeCost/GetLocationData?lcmcode=' + lcmcode + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpcLst = function (flrcode) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceTypeCost/GetSpcLst?flrcode=' + flrcode + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.ShowLocationGrid = function (lcmcode) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceTypeCost/ShowLocationGrid?lcmcode=' + lcmcode + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.save = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceTypeCost/SaveCostData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.locationsave = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceTypeCost/SaveLocationCostData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };




    //--------------- Submit the data-------------
    this.Costupdate = function (flrcode) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceTypeCost/Costupdate?flrcode=' + flrcode + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getAreaType = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SpaceTypeCost/GetAreaType')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.setAreaType = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceTypeCost/SetAreaType', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getAreaTypeByLocation = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SpaceTypeCost/GetAreaTypeByLocation', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('SpaceTypeCostController', ['$scope', '$q', '$location', 'SpaceTypeCostService', 'UtilityService', '$filter','$http', function ($scope, $q, $location, SpaceTypeCostService, UtilityService, $filter, $http) {
    $scope.SpaceTypeCost = {};
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.BsmDet = {};
    $scope.LocationStatus = 0;
    $scope.SpaceTypeCost.costinfo = "N";
    $scope.GridVisibility = 1;
    $scope.GridLocation = 0;

    $scope.Load = function () {
        UtilityService.getCountires(2).then(function (Countries) {
            if (Countries.data != null) {
                $scope.Country = Countries.data;
            }
        });
        UtilityService.getBussHeirarchy().then(function (response) {
            if (response.data != null) {
                $scope.BsmDet = response.data;
            }
        });
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
            }
        });
        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
            }
        });
        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Location = response.data;
            }
        });
        UtilityService.getTowers(2).then(function (response) {
            if (response.data != null) {
                $scope.Tower = response.data;
            }
        });

        UtilityService.getFloors(2).then(function (response) {
            if (response.data != null) {
                $scope.Floor = response.data;
            }
        });

        //child
        UtilityService.getChildEntity(1).then(function (response) {
            $scope.Childlist = response.data;
            if (response.data.selectedChildEntity != null) {
                for (i = 0; i < response.data.selectedChildEntity.length; i++) {
                    var a = _.find($scope.Childlist, { CHE_CODE: response.data.selectedChildEntity[i].CHE_CODE });
                    if (a)
                        a.ticked = true;
                }
            }
        });


        SpaceTypeCostService.GetSpcLst().then(function (response) {
            if (response.data != null) {
                $scope.gridOptions.api.setRowData(response.data);
            }
        });

    }

    $scope.areatypeChanged = function () {
        if ($scope.SpaceTypeCost.areatype == 'A') {
            SpaceTypeCostService.getAreaType().then(function (response) {
                if (response.data != null) {
                    console.log(response.data);
                    $scope.AreaType = response.data.areatypes;
                    $scope.TypeDetails = response.data.typedetails;
                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            }, function (response) {
                progress(0, '', false);
            });
        }
    }

    $scope.ShowAreaType = function (SATD_SAT_ID) {
        var areatype = _.find($scope.AreaType, { SAT_ID: SATD_SAT_ID });
        return areatype.SAT_CODE;
    }

    $scope.saveAreatypes = function () {
        var dataobj = { stcd: $scope.TypeDetails, lcmlst: $scope.SpaceTypeCost.Location };
        console.log(dataobj);
        SpaceTypeCostService.setAreaType(dataobj).then(function (response) {
            if (response.data != null) {

            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }

    /***    Page loading        ***/
    $scope.Load();

    //// Country Events
    $scope.CnyChangeAll = function () {
        $scope.SpaceTypeCost.Country = $scope.Country;
        $scope.CnyChanged();
    }

    $scope.CnySelectNone = function () {
        $scope.SpaceTypeCost.Country = [];
        $scope.CnyChanged();
    }

    $scope.CnyChanged = function () {
        if ($scope.SpaceTypeCost.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.SpaceTypeCost.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.SpaceTypeCost.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.SpaceTypeCost.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.SpaceTypeCost.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceTypeCost.Country.push(cny);
            }
        });
    }
    $scope.redirect = function () {
        window.location = "/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx";
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.SpaceTypeCost.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.SpaceTypeCost.Location = [];
        $scope.LcmChanged();
    }

    $scope.LcmChanged = function () {

        if ($scope.SpaceTypeCost.areatype == 'A') {

            SpaceTypeCostService.getAreaTypeByLocation($scope.SpaceTypeCost.Location).then(function (response) {
                if (response.data != null) {
                    $scope.TypeDetails = response.data;
                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            }, function (response) {
                progress(0, '', false);
            });
        }

        UtilityService.getTowerByLocation($scope.SpaceTypeCost.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceTypeCost.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SpaceTypeCost.City.push(cty);
            }
        });


    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.SpaceTypeCost.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    $scope.TwrSelectNone = function () {
        $scope.SpaceTypeCost.Tower = [];
        $scope.TwrChanged();
    }

    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.SpaceTypeCost.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceTypeCost.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SpaceTypeCost.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SpaceTypeCost.Location[0].push(lcm);
            }
        });

    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.SpaceTypeCost.Floor = $scope.Floor;
        $scope.FlrChanged();
    }

    $scope.FlrSelectNone = function () {
        $scope.SpaceTypeCost.Floor = [];
        $scope.FlrChanged();
    }



    $scope.FlrChanged = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.SpaceTypeCost.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.SpaceTypeCost.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.SpaceTypeCost.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.SpaceTypeCost.Tower.push(twr);
            }
        });

    }

    $scope.SpaceChange = function () {
        $scope.LocationStatus = 0;
    }
    $scope.LocationChange = function () {
        $scope.LocationStatus = 1;
    }

    //Get cost input table data

    $scope.getfloordetails = function () {
        console.log($scope.SpaceTypeCost.costinfo)
        if ($scope.SpaceTypeCost.costinfo == "Y") {
            $scope.viewstatus = 1;
            $scope.GridVisibility = 1;
            $scope.GridLocation = 0;
        }
        else if ($scope.SpaceTypeCost.costinfo == "N") {
            $scope.viewstatus = 2;
            $scope.GridVisibility = 1;
            $scope.GridLocation = 0;
        }
        else {
            $scope.GridVisibility = 0;
            $scope.GridLocation = 0;
            $scope.viewstatus = 3;
        }

        if ($scope.SpaceTypeCost.costinfo == "Y" || $scope.SpaceTypeCost.costinfo == "N") {
            $scope.GridVisibility = 1;
            $scope.GridLocation = 0;
            $scope.SPCDetails = [];
            progress(0, 'Loading...', true);
            var flrcode = $scope.SpaceTypeCost.Floor[0].FLR_CODE;
            SpaceTypeCostService.getFloordata(flrcode).then(function (response) {
                if (response != null) {
                    $scope.SPCDetails = response;
                    if ($scope.SpaceTypeCost.costinfo == "Y") {
                        $scope.SPCDetails[0].lstseattype = [];
                    }

                    progress(0, '', false);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'No Data Found');
                }
            }, function (response) {
                progress(0, '', false);
            });
        }

        else {
            $scope.GridVisibility = 0;
            $scope.GridLocation = 0;
            $scope.LOCDetails = [];
            progress(0, 'Loading...', true);
            var lcmcode = $scope.SpaceTypeCost.Location[0].LCM_CODE;

            SpaceTypeCostService.getLocationdata(lcmcode).then(function (response) {
                if (response != null) {
                    $scope.LOCDetails = response;
                    progress(0, '', false);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'No Data Found');
                }
            }, function (response) {
                progress(0, '', false);
            });
        }
    }



    $scope.save = function () {
        if ($scope.SpaceTypeCost.costinfo == "Y" || $scope.SpaceTypeCost.costinfo == "N") {
            $scope.GridVisibility = 1;
            $scope.GridLocation = 0;
            progress(0, 'Loading...', true);
            SpaceTypeCostService.save($scope.SPCDetails).then(function (response) {
                if (response != null) {
                    SpaceTypeCostService.GetSpcLst($scope.SpaceTypeCost.Floor[0].FLR_CODE).then(function (response) {
                        if (response.data != null) {
                            $scope.gridOptions.api.setRowData(response.data);
                        }
                    });

                    progress(0, '', false);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'No Data Found');
                }
            }, function (response) {
                progress(0, '', false);
            });
        }

        else {
            $scope.GridVisibility = 0;
            $scope.GridLocation = 1;
            progress(0, 'Loading...', true);
            console.log($scope.LOCDetails);
            SpaceTypeCostService.locationsave($scope.LOCDetails).then(function (response) {
                if (response != null) {
                    SpaceTypeCostService.ShowLocationGrid($scope.LOCDetails[0].LCM_CODE).then(function (response) {
                        if (response.data != null) {
                            $scope.gridData.api.setRowData(response.data);
                        }
                    });

                    progress(0, '', false);
                }
                else {
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'No Data Found');
                }
            }, function (response) {
                progress(0, '', false);
            });

        }
    }

    var columnDefslocation = [
        { headerName: "Location Code", field: "LCM_CODE", cellClass: "grid-align", width: 160 },
        { headerName: "Location Name", field: "LCM_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Space ID", field: "SPC_ID", cellClass: "grid-align", width: 100 },
        { headerName: "Space Type", field: "SPC_TYPE_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Seat Cost", field: "SEAT_COST", cellClass: "grid-align", width: 200 },

    ];
    $scope.gridData = {
        columnDefs: columnDefslocation,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableFilter: true,
        enableCellSelection: false,
        enableSorting: true,
    };

    function onFilterChanged(value) {
        $scope.gridData.api.setQuickFilter(value);
    }
    $("#filtergrid").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    });

    $scope.SPCDetails = [];


    var columnDefs = [
        { headerName: "Location", field: "LCM_NAME", cellClass: "grid-align", width: 160 },
        { headerName: "Tower", field: "TWR_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Floor", field: "FLR_NAME", cellClass: "grid-align", width: 200 },
        { headerName: "Space ID", field: "SPC_ID", cellClass: "grid-align", width: 200 },
        { headerName: "Seat Type", field: "SPC_TYPE", cellClass: "grid-align", width: 100 },
        { headerName: "Seat Area", field: "SPC_AREA", cellClass: "grid-align", width: 100 },
        { headerName: "Fixed Cost", field: "SPC_FCLTY_COST", cellClass: "grid-align", width: 100 },
        { headerName: "Variable Cost", field: "SC_VAR_COST_VALUE", cellClass: "grid-align", width: 100 }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableFilter: true,
        enableCellSelection: false,
        enableSorting: true,
    };

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#filtertxt").change(function () {
        onReqFilterChanged($(this).val());
    }).keydown(function () {
        onReqFilterChanged($(this).val());
    }).keyup(function () {
        onReqFilterChanged($(this).val());
    }).bind('paste', function () {
        onReqFilterChanged($(this).val());
    });


    $scope.Clear = function () {
        $scope.SpaceTypeCost = {};
        angular.forEach($scope.Country, function (country) {
            country.ticked = false;
        });

        angular.forEach($scope.City, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Location, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Tower, function (tower) {
            tower.ticked = false;
        });
        angular.forEach($scope.Floor, function (floor) {
            floor.ticked = false;
        });
        $scope.SpaceTypeCost.Country = [];
        $scope.SpaceTypeCost.City = [];
        $scope.SpaceTypeCost.Location = [];
        $scope.SpaceTypeCost.Tower = [];
        $scope.SpaceTypeCost.Floor = [];
    };

}]);