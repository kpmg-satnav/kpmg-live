﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/angular-confirm.css" rel="stylesheet" />

    <style>
        .ng-confirm-title-c{
            font-size:18px !important;
        }
        .ng-confirm-content{
             font-size:15px !important;
        }
    </style>
</head>
<body data-ng-controller="CovidDistaningController" class="amantra">
   <div class="animsition">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Add Service Type" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Configure Social Distancing</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form role="form" id="form1" name="CovidDistancingMaster"  novalidate>
                        <div class="clearfix">
                            <div class="row">
                                <div class="clearfix col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmCovidDistancing.$submitted && frmCovidDistancing.LCM_NAME.$invalid}">
                                            <label class="control-label">Location</label>
                                            <div isteven-multi-select data-input-model="Locations" data-output-model="CovidDistancing.Locations" data-button-label="icon LCM_NAME"
                                                data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="CovidDistancing.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmCovidDistancing.$submitted && frmCovidDistancing.LCM_NAME.$invalid" style="color: red">Please select Location</span>

                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Tower</label>
                                            <div isteven-multi-select data-input-model="Towers" data-output-model="CovidDistancing.Towers" data-button-label="icon TWR_NAME "
                                                data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="CovidDistancing.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmCovidDistancing.$submitted && frmCovidDistancing.TWR_NAME.$invalid" style="color: red">Please select Tower</span>



                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Floor</label>
                                            <div isteven-multi-select data-input-model="Floors" data-output-model="CovidDistancing.Floors" data-button-label="icon FLR_NAME"
                                                data-item-label="icon FLR_NAME maker" data-on-item-click="FloorChange()" data-on-select-all="floorChangeAll()" data-on-select-none="FloorSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                            <input type="text" data-ng-model="CovidDistancing.Floors[0]" name="FLR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmCovidDistancing.$submitted && frmCovidDistancing.FLR_NAME.$invalid" style="color: red">Please select Floor</span>

                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <label for="txtcode" class="lang-change">Distance b/w seats</label>
                                            <select name="pm" data-ng-options="pm.Name for pm in Types" data-ng-model="CovidDistancing.Type" class="selectpicker">
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>
  
                           <%-- <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 text-right" >
                                    <label class="col-md-3 col-sm-6 col-xs-12 control-label">
                                        Single Seat Distance
                                                <input type="radio" id="Single" value="1" class="clsRadioButton" name="SchType"   />
                                    </label>
                                    <label class="col-md-3  col-sm-6  col-xs-12 control-label">
                                        Double Seat Distance
                                                <input type="radio" id="Double" value="2" class="clsRadioButton" name="SchType"  />
                                    </label>
                                </div>
                                

                            </div>--%>
                            <div class="col-md-12 col-sm-12 col-xs-12 box-footer align-right">
                                    <input type="button" data-ng-click="Activate()" value="Activte Seats" class="btn btn-primary custom-button-color" />
                                    <input type="button" data-ng-click="InActivate()" value="InActivate Seats" class="btn btn-primary custom-button-color" />
                                </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
       </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../BootStrapCSS/Scripts/angular-confirm.js"></script>
    <script defer src="../Utility.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["isteven-multi-select", "cp.ngConfirm"]);
    </script>
    <script src="CovidDistancingMaster.js"></script>
    <%--<script defer>

        function Activate() {
            var type = $('.clsRadioButton:checked').val();
            var  floor=
            $.ajax({
                type: "GET",
                url:"../../api/Utility/ActivateSeats",
                //data: "{'id':'1'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    alert(response);
                }
            });
           
        }

    </script>--%>


</body>

</html>
