﻿app.service("LeaveRequestService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaveRequest/GetGrid')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SaveLeaveDetails = function (LeaveReq) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaveRequest/SaveLeaveDetails', LeaveReq)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.DeleteData = function (LeaveReq) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaveRequest/DeleteData', LeaveReq)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };



}]);
app.controller('LeaveRequestController', ['$scope', '$q', '$http', 'LeaveRequestService', 'UtilityService', '$timeout','$filter', function ($scope, $q, $http, LeaveRequestService, UtilityService, $timeout, $filter) {
    $scope.Leave = {};
    $scope.EmployeeSelect = [];
    $scope.EmployeeSelectName = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.remoteUrlRequestFn = function (str) {
        return { q: str };
    };
    $scope.residentSelected = function (selected) {
        $scope.EmployeeSelect = selected.originalObject.AUR_ID
        $scope.EmployeeSelectName = selected.originalObject.NAME
    }

    $scope.SubmitData = function () {
        var params = {
            FromDate: $scope.Leave.FromDate,
            ToDate: $scope.Leave.ToDate,
            SelectedEmployee: $scope.EmployeeSelect,
            LeaveReason: $scope.Leave.Reason
        };
        var fromdate = moment($scope.Leave.FromDate);
        var todate = moment($scope.Leave.ToDate);
        if (fromdate > todate) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);
            LeaveRequestService.SaveLeaveDetails(params).then(function (response) {
                if ($scope.IsInEdit = false) {
                    showNotification('success', 8, 'bottom-right', 'Leave Request Raised For Employee :' + $scope.EmployeeSelectName);
                }
                else
                    showNotification('success', 8, 'bottom-right', 'Leave Request Updated For Employee :' + $scope.EmployeeSelectName);
                $scope.LoadData();
            });
            progress(0, 'Loading...', false);
        }

    }

    $scope.Reset = function () {
        $scope.Leave = {};
        document.getElementById('ex7').value = ""

    }


    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        LeaveRequestService.GetGriddata().then(function (data) {
            $scope.gridata = data;
            $scope.gridOptions.api.setRowData($scope.gridata);
            progress(0, 'Loading...', false);
        }, function (error) {
            console.log(error);
        });
        progress(0, 'Loading...', false);
    }


    $scope.columnDefs = [
        { headerName: "Employee Name", field: "NAME", cellClass: 'grid-align', width: 250, },
        { headerName: "Employee Email", field: "AUR_EMAIL", cellClass: 'grid-align', width: 180, },
        { headerName: "Leave Start Date", field: "FromDate", template: '<span>{{data.FromDate | date:"dd MMM, yyyy"}}</span>', width: 100, cellClass: 'grid-align', suppressMenu: true, },
        { headerName: "Leave End Date", field: "ToDate", template: '<span>{{data.ToDate | date:"dd MMM, yyyy"}}</span>', width: 100, cellClass: 'grid-align', suppressMenu: true, },
        { headerName: "Leave Reason", field: "Reason", cellClass: 'grid-align', width: 150, },
        { headerName: "Edit", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 100 },
        { headerName: "Delete", template: '<a data-ng-click="Delete(data)"><i class="fa fa-trash fa-fw"></i></a>', cellClass: 'grid-align', width: 100 }
    ];

    $scope.selectedEmp = function (selected) {
        if (selected) {
            $scope.selectedEmployee = selected.originalObject;
            $scope.$broadcast('angucomplete-alt:changeInput', 'ex7', $scope.selectedEmployee.NAME);
            $scope.Leave.NAME = $scope.selectedEmployee.NAME;

        } else {

        }
    };

    $scope.reselectedEmp = function (obj) {
        if (obj) {
            $scope.selectedEmployee = obj.selected.data;
            $scope.$broadcast('angucomplete-alt:changeInput', 'ex7', $scope.selectedEmployee.NAME);
            $scope.Leave.NAME = $scope.selectedEmployee.NAME;

        } else {


        }
    };
    $scope.EditData = function (data) {
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        angular.copy(data, $scope.Leave);
        $scope.Leave.FromDate = moment().format('MM/DD/YYYY');
        $scope.Leave.ToDate = moment().format('MM/DD/YYYY');
        var Id = data.ID
        $scope.EmployeeSelect = Id
        $scope.selectedEmpDB = {
            selected: {
                data: { Id: Id, NAME: data.NAME, ticked: false },
                title: ""
            }
        };
        $scope.reselectedEmp($scope.selectedEmpDB);
    }

    $scope.Delete = function (data) {
        var params = {
            SelectedEmployee: data.ID,
        };
        progress(0, 'Loading...', true);
        LeaveRequestService.DeleteData(params).then(function (data) {
            setTimeout(function () {
                $scope.LoadData();
            }, 1000);
            showNotification('success', 8, 'bottom-right', 'Leave Request For Employee Has Been Deleted');
        }, function (error) {
            console.log(error);
        });
        progress(0, 'Loading...', false);

    }
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })


    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        enableCellSelection: false,
        suppressHorizontalScroll: true,
        onready: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }


    };
    setTimeout(function () {
        $scope.LoadData();
    }, 1000);
}]);