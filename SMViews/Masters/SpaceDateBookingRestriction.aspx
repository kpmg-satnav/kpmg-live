﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SpaceDateBookingRestriction.aspx.vb" Inherits="SMViews_Masters_SpaceDateBookingRestriction" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

   
</head>
<body>
     <div class="container-fluid page-content-inner">
                <div ba-panel ba-panel-title="Shift Master" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
                    <div class="bgc-gray p-20 m-b-25">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Space Booking Date Restriction</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 50px;">
                            <form id="form1" runat="server">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="divmessagebackground"
                                    ForeColor="red" ValidationGroup="Val1" />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Visible="False">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                                

                                
                                    <div class="row">
                                        
                                        <div class="col-sm-3 col-xs-6">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">No. of Days Restrict<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="countValidator" runat="server" ControlToValidate="totalcount" 
                                                    Display="none" ErrorMessage="Please Enter No. of Days you want to Restrict" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="TypeCheck" runat="server"
                                                    ControlToValidate="totalcount"  Display="None" ErrorMessage="Only numeric allowed in Count."
                                                    ValidationExpression="^[0-9]*$" ValidationGroup="Val1">
                                                </asp:RegularExpressionValidator>
                                                <div class="col-md-12">
                                                    <asp:TextBox ID="totalcount" runat="server" CssClass="form-control" autocomplete="off"  placeholder="Enter No. of Days to Restrict"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                

                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Update" ValidationGroup="Val1"></asp:Button>
                                        <asp:Button ID="btnClear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear"></asp:Button>
                                        <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" PostBackUrl="~/Masters/Mas_Webfiles/frmMasSpaceMasters.aspx"></asp:Button>
                                    </div>
                                </div>

                                <div>
                                    <label id="Result" runat="server" style="color:green"  class="col-md-12 control-label">Restriced no of days :</label>
                                    

                                </div>
                               
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>
        function ShowHelpWindow() {
            window.open('frmPrjHead.aspx', 'Window', 'toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=690,height=450');
            return false;
        }
    </script>
</body>
</html>


