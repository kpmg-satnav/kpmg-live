﻿app.service("CovidDistancingService", function ($http, $q, UtilityService) {
});
app.controller('CovidDistaningController', ['$scope', '$q', 'UtilityService', 'CovidDistancingService', '$ngConfirm', function ($scope, $q, UtilityService, CovidDistancingService, $ngConfirm) {

    $scope.CovidDistancing = {};
    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Floors = [];

    $scope.Types = [{
        "Id": 1,
        "Name": "1"
    }, {
        "Id": 2,
        "Name": "2"
    }];

    $scope.CovidDistancing.Type = $scope.Types[0];


    $scope.LoadDetails = function () {
        progress(0, 'Loading...', true);

        UtilityService.getCountires(2).then(function (response) {

            if (response.data != null) {
                $scope.Country = response.data;

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;

                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;


                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;

                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                progress(0, '', false);
                                            }
                                        });

                                    }
                                });
                            }
                        });

                    }
                });
            }
        });$scope.Locations
    }

    $scope.LoadDetails = function () {
        progress(0, 'Loading...', true);

        UtilityService.getCountires(2).then(function (response) {

            if (response.data != null) {
                $scope.Country = response.data;

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;

                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;


                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Towers = response.data;

                                        UtilityService.getFloors(2).then(function (response) {
                                            if (response.data != null) {
                                                $scope.Floors = response.data;
                                                progress(0, '', false);
                                            }
                                        });

                                    }
                                });
                            }
                        });

                    }
                });
            }
        });
    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.CovidDistancing.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });

    }

    $scope.locSelectAll = function () {
        $scope.CovidDistancing.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.CovidDistancing.Towers, 2).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        $scope.CovidDistancing.Locations = [];
        angular.forEach($scope.Towers, function (value, key) {

            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });

            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.CovidDistancing.Locations.push(lcm);

            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.CovidDistancing.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.FloorChange = function () {


        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });



        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.CovidDistancing.Locations.push(lcm);
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE, CNY_CODE: value.CNY_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.CovidDistancing.Towers.push(twr);

            }
        });

        //SpaceRequisitionService.getShifts($scope.CovidDistancing.Locations).then(function (response) {
        //    $scope.Shifts = response.data;
        //    $scope.CovidDistancing.Shift = $scope.Shifts[0].SH_CODE;
        //    _.forEach($scope.Shifts, function (o) {
        //        if (o.SH_CODE == $scope.CovidDistancing.Shift) {
        //            return o.ticked = true;
        //        }
        //    });
        //}, function (error) {
        //    console.log(error);
        //});
    }
    $scope.floorChangeAll = function () {
        $scope.CovidDistancing.Floors = $scope.Floors;
        $scope.FloorChange();
    }

    setTimeout(function () {
        $scope.LoadDetails();
        
    }, 200);


    $scope.Activate = function () {

        $ngConfirm({
            icon: 'fa fa-check',
            title: 'Confirm!',
            content: 'You want to active spaces',
            buttons: {
                Confirm: {
                    text: 'Ok',
                    btnClass: 'btn-blue',
                    action: function (button) {
                        var selType = $scope.CovidDistancing.Type.Id;
                        floorsList = _.filter($scope.CovidDistancing.Floors, function (o) { return o.ticked == true; }).map(function (x) { return x.FLR_CODE; }).join(',');

                        if (floorsList == '') {
                            showNotification('error', 8, 'bottom-right', "Please select atleast one floor");
                        }
                        else {
                            progress(0, 'Loading...', true);
                            var param = {
                                type: selType,
                                Floorlist: floorsList
                            }

                            $.ajax({
                                type: "POST",
                                url: "../../api/Utility/ActivateSeats",
                                data: param,
                                //dataType: "json",
                                //contentType: "application/json; charset=utf-8",
                                success: function (response) {
                                    
                                    progress(0, 'Loading...', false);
                                    showNotification('success', 8, 'bottom-right', "Seats inactive successfully");
                                },
                                error: function (e) {
                                    showNotification('error', 8, 'bottom-right', "Something Went Wrong");
                                }
                            });
                        }
                    }
                    
                },
                close: function ($scope, button) {
                                }
            }
        });

    }


    $scope.InActivate = function () {

        $ngConfirm({
            icon: 'fa fa-check',
            title: 'Confirm!',
            content: 'You want to inactive spaces',
            buttons: {
                Confirm: {
                    text: 'Ok',
                    btnClass: 'btn-blue',
                    action: function (button) {
                        var selType = $scope.CovidDistancing.Type.Id;
                        floorsList = _.filter($scope.CovidDistancing.Floors, function (o) { return o.ticked == true; }).map(function (x) { return x.FLR_CODE; }).join(',');

                        if (floorsList == '') {
                            showNotification('error', 8, 'bottom-right', "Please select atleast one floor");
                        }
                        else {
                            progress(0, 'Loading...', true);
                            var param = {
                                type: selType,
                                Floorlist: floorsList
                            }
                            // "{'type':selType,'Floorlist':floorsList}"

                            $.ajax({
                                type: "POST",
                                //contentType: "application/json; charset=utf-8",
                                url: "../../api/Utility/InActivateSeats",
                                data: param,
                                //dataType: "json",
                                
                                success: function (response) {
                                   
                                    progress(0, 'Loading...', false);
                                    showNotification('success', 8, 'bottom-right', "Seats inactive successfully");
                                },
                                error: function (e) {
                                    showNotification('error', 8, 'bottom-right', "Something Went Wrong");
                                }
                            });
                        }
                    }

                },
                close: function ($scope, button) {
                }
            }
        });

    }


}]);

