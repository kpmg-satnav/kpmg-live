﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>


    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="LeaveRequestController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Employee Leave/Onsite Requisition</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" name="LeaveRequest">
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Search Employee</label>
                                    <div angucomplete-alt
                                        id="ex7"
                                        pause="500"
                                        selected-object="residentSelected"
                                        remote-url="../../api/Utility/GetEmployeeNameAndID"
                                        remote-url-request-formatter="remoteUrlRequestFn"
                                        remote-url-data-field="items"
                                        title-field="NAME"
                                        minlength="2"
                                        input-class="form-control form-control-small"
                                        match-class="highlight">
                                    </div>
                                    <input type="text" id="txtrptTo" data-ng-model="Leave.NAME" style="display: none" name="NAME" required />
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>From Date</label>
                                    <div class="input-group date" id='fromdatebooking'>
                                        <input type="text" class="form-control" data-ng-model="Leave.FromDate" id="LeaveFromDate" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('fromdatebooking')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="LeaveRequest.$submitted && LeaveRequest.FromDate.$invalid" style="color: red;">Please Select From Date</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>To Date</label>
                                    <div class="input-group date" id='todatebooking'>
                                        <input type="text" class="form-control" data-ng-model="Leave.ToDate" id="LeaveToDate" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('todatebooking')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="LeaveRequest.$submitted && LeaveRequest.ToDate.$invalid" style="color: red;">Please Select To Date</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Reason For Leave</label>
                                    <input type="text" class="form-control" data-ng-model="Leave.Reason" id="TxtReason" />
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="SubmitData()" data-ng-show="ActionStatus==0" />
                                <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-click="SubmitData()" data-ng-show="ActionStatus==1" />
                                <input type="button" id="btnNew" data-ng-click="Reset()" value="Clear" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>
                        <div style="height: 320px">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 90%;" class="ag-blue"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../BootStrapCSS/FMS_UI/js/moment.js"></script>
    <script defer src="../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "angucomplete-alt"]);
    </script>

    <script defer type="text/javascript">
        var CompanySession = '<%= Session["COMPANYID"]%>';
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        }
    </script>
    <script defer src="LeaveRequest.min.js"></script>
    <script defer src="../Utility.min.js"></script>
    <script defer src="../../BootStrapCSS/FMS_UI/js/moment.js"></script>
</body>
</html>
