﻿app.service("CustomizedReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/DailyUsageReport/GetDailyUsageDetails', Customized)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('CustomizedReportController', function ($scope, $q, $http, CustomizedReportService, UtilityService, $timeout, $filter) {
    $scope.Customized = {};
    $scope.Customized.CNP_NAME = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.visible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.BsmDet = { Parent: "", Child: "" };
    $scope.EnableMonthYearStatus = false;
    $scope.EnableYearStatus = false;
    $scope.EnableHalfyearStatus = false;
    $scope.EnableQuarterStatus = false;
    $scope.EnableDateTextboxes = false;
    $scope.Header;
    $scope.Pageload = function () {

        UtilityService.getCountires(1).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getCities(1).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getLocations(1).then(function (response) {
            if (response.data != null) {
                $scope.Locations = response.data;
                angular.forEach($scope.Locations, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getTowers(1).then(function (response) {
            if (response.data != null) {
                $scope.Towers = response.data;
                angular.forEach($scope.Towers, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getFloors(1).then(function (response) {
            if (response.data != null) {
                $scope.Floors = response.data;
            }
        });

        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                //$scope.Customized.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.Customized.CNP_NAME.push(a);
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });
    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Customized.Country, 1).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.Customized.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customized.City, 1).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.Customized.City = $scope.City;
        $scope.getLocationsByCity();
    }
    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.Customized.Locations, 1).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.Customized.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.Customized.Towers, 1).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.Customized.Towers = $scope.Towers;
        $scope.getFloorByTower();
    }

    $scope.FloorChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });

        angular.forEach($scope.Floors, function (value, key) {
            var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.Customized.Towers[0] = twr;
            }
        });

    }
    $scope.LocationChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });
    }
    $scope.TowerChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
     
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });

     

    }
    $scope.floorChangeAll = function () {
        $scope.Customized.Floors = $scope.Floors;
        $scope.FloorChange();
    }
    $scope.LocationSelectNone = function () {
        $scope.Locationlst = [];
        $scope.LocationChange();
    }
  
    $scope.twrSelectNone = function () {
        $scope.Towerlist = [];
        $scope.TowerChange();
    }

    $scope.Customized.QuickSelectedParam = "Monthly";
    //$scope.EnableDateTextboxes = false;
    $scope.EnableHalfyearStatus = false;
    $scope.EnableQuarterStatus = false;
    $scope.EnableYearStatus = false;
    $scope.EnableMonthYearStatus = false;

    $scope.rptDateRanges = function () {
        switch ($scope.Customized.QuickSelectedParam) {
            case 'Daily':
                $scope.Customized.FromDate = moment().format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                $scope.EnableDateTextboxes = true;
                $scope.EnableHalfyearStatus = false;
                $scope.EnableQuarterStatus = false;
                $scope.EnableYearStatus = false;
                $scope.EnableMonthYearStatus = false;
     
                break;
             
            case 'YESTERDAY':
                $scope.Customized.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case 'Weekly':
                $scope.Customized.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
              
                $scope.EnableHalfyearStatus = false;
                $scope.EnableQuarterStatus = false;           
                $scope.EnableMonthYearStatus = false;
                $scope.EnableDateTextboxes = false;
                $scope.EnableYearStatus = true;
           
                break;
            case '30':
                $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Customized.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                //$scope.EnableMonthYearStatus = true;
                //$scope.EnableYearStatus = false;
                //$scope.EnableHalfyearStatus = false;
                //$scope.EnableQuarterStatus = false;
                //$scope.EnableDateTextboxes = false;
                //$scope.EnableMonthYearStatus = false;

                break;
            case 'Monthly':
                $scope.Customized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');

              
                $scope.EnableHalfyearStatus = false;
                $scope.EnableQuarterStatus = false;
                $scope.EnableDateTextboxes = false;
                $scope.EnableMonthYearStatus = true;
                $scope.EnableYearStatus = true;
                break;
            case 'Quarterly':
                //$scope.Customized.FromDate = moment().subtract(3, 'month').startOf('month').format('MM/DD/YYYY');
                //$scope.Customized.ToDate = moment().format('MM/DD/YYYY');
    
                $scope.EnableHalfyearStatus = false;
                $scope.EnableQuarterStatus = true;
              
                $scope.EnableDateTextboxes = false;
                $scope.EnableMonthYearStatus = false;
                $scope.EnableYearStatus = true;
               

                break;
            case 'Half Yearly':
                $scope.Customized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                //$('#divhalfyear').show();
                $scope.EnableHalfyearStatus = false;
                $scope.EnableQuarterStatus = false;
               
                $scope.EnableDateTextboxes = false;
                $scope.EnableMonthYearStatus = false;
                $scope.EnableYearStatus = true;
                break;
            case 'Yearly':
                ////$scope.Customized.FromDate = moment().subtract(1, 'year').startOf('month').format('MM/DD/YYYY');
                ////$scope.Customized.ToDate = moment().format('MM/DD/YYYY');     
                $scope.EnableHalfyearStatus = false;
                $scope.EnableDateTextboxes = false;
                $scope.EnableQuarterStatus = false;
                $scope.EnableMonthYearStatus = false;
                $scope.EnableYearStatus = true;

                break;
               

        }
    }

    $scope.columnDefs = [
             { headerName: "Country", field: "COUNTRY", width: 100, cellClass: 'grid-align', width: 80 },
             { headerName: "City", field: "CITY", cellClass: 'grid-align', width: 100 },
             { headerName: "Location", field: "LOCATION", cellClass: 'grid-align', width: 150 },
             { headerName: "Tower", field: "TOWER", cellClass: 'grid-align', width: 100 },
             { headerName: "", field: "CAL_LOG_DATE", cellClass: 'grid-align', width: 200 },
                
             { headerName: "Utility Type", field: "RESOURCE", cellClass: 'grid-align', width: 150 },
               { headerName: "Meter No", field: "SERIAL_NO", cellClass: 'grid-align', width: 150, suppressMenu: true },
             { headerName: "Consumed Quantity/Unit", field: "TOTAL", cellClass: 'grid-align', width: 180, suppressMenu: true },
                //{ headerName: "Quantity/Unit", field: "QUANTITY", cellClass: 'grid-align', width: 150, suppressMenu: true },
           
             
           
          

    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        //groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    //function groupAggFunction(rows) {
    //    var sums = {
    //        Requisition_Id: 0
    //    };
    //    rows.forEach(function (row) {
    //        var data = row.data;
    //        sums.Requisition_Id += parseFloat((data.Requisition_Id).toFixed(2));
    //    });
    //    return sums;
    //}
    $scope.LoadData = function (stat) {
        var comapanyid = CompanySession;
        if (stat == 1) {
            $scope.rptDateRanges();
        }
        else
            comapanyid = $scope.Customized.CNP_NAME[0].CNP_ID;
       
        var params = {
            //flrlst: $scope.Customized.Floors,
            twrlst: $scope.Customized.Towers,
            FromDate: $scope.Customized.FromDate,
            ToDate: $scope.Customized.ToDate,
            STAT: stat,
            DateParam: $scope.Customized.DateParam,
            QuickSelectedParam: $scope.Customized.QuickSelectedParam,
            companyid: comapanyid
        };

        CustomizedReportService.GetGriddata(params).then(function (data) {
            progress(0, 'Loading...', true);
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
               
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            else {
               
                showNotification('', 8, 'bottom-right', '');
                //progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.columnApi.getColumn("CAL_LOG_DATE").colDef.headerName = $scope.Customized.QuickSelectedParam;
                $scope.gridOptions.api.refreshHeader();          
                $scope.gridOptions.api.setRowData($scope.gridata);
               //progress(0, '', false);
            }
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        });
       
    }, function (error) {
        console.log(error); progress(0, '', false);
    }
    $scope.CustmPageLoad = function () {


    }, function (error) {
        console.log(error);
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "COUNTRY" }, { title: "City", key: "CITY" },
            { title: "Location", key: "LOCATION" }, { title: "Requisition Id", key: "Requisition_Id" }, { title: "Requested By", key: "Requested_By" }, { title: "Requested Date", key: "Requested_Date" },
            { title: "Service Category", key: "Service_Category" }, { title: "Service Type", key: "Service_Type" }, { title: "Convenient Date", key: "Convenient_Date" },
            { title: "Convenient Time", key: "Convenient_Time" }, { title: "User Type", key: "User_Type" }, { title: "Closed Time", key: "Closed_Time" }, { title: "Request Description", key: "Request_Description" }, { title: "Comments", key: "Comments" }, { title: "Status", key: "Status" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("DailyUsageReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });
    $scope.LoadData(1);

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "DailyUsageReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Customized, Type) {
        progress(0, 'Loading...', true);
        Customized.Type = Type;
        Customized.twrlst = Customized.Towers;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/DailyUsageReport/GetDailyUsageData',
                method: 'POST',
                data: Customized,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'DailyUsageReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    $scope.Pageload();

});
