﻿app.service("UnitMappingService", function ($http, $q) {
    var deferred = $q.defer();
    this.getEntityAdmin = function () {
        deferred = $q.defer();
        return $http.get('../../api/EntityAdmin')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    //SAVE
    this.saveEntityAdmin = function (EntyAdmin) {
        deferred = $q.defer();
        return $http.post('../../api/EntityAdmin/Create', EntyAdmin)
          .then(function (response) {
              deferred.resolve(response);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    //UPDATE BY ID
    this.updateEntityAdmin = function (EntyAdminUpdate) {
        deferred = $q.defer();
        return $http.post('../../api/EntityAdmin/UpdateEntityAdminData/', EntyAdminUpdate)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    //Bind Grid
    this.GetEntityAdminGridData = function () {
        deferred = $q.defer();
        return $http.get('../../api/EntityAdmin/BindEntityGrid')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});

app.controller('UnitMappingController', function ($scope, $q, UnitMappingService, $timeout) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.EntityAdmin = {};
    $scope.repeatCategorylist = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;

    //to Save the data
    $scope.Save = function () {
        if ($scope.IsInEdit) {
            EntityAdminService.updateEntityAdmin($scope.EntityAdmin).then(function (repeat) {
                var updatedobj = {};
                angular.copy($scope.EntityAdmin, updatedobj)
                $scope.gridata.unshift(updatedobj);
                $scope.ShowMessage = true;
                $scope.Success = "Data Updated Successfully";

                EntityAdminService.GetEntityAdminGridData().then(function (data) {
                    $scope.gridata = data;
                    //$scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                    console.log(error);
                });



                $scope.IsInEdit = false;
                $scope.ClearData();
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
            }, function (error) {
                console.log(error);
            })
        }
        else {
            $scope.EntityAdmin.ADM_Status_Id = "1";
            EntityAdminService.saveEntityAdmin($scope.EntityAdmin).then(function (response) {
                $scope.ShowMessage = true;
                $scope.Success = "Data Inserted Successfully";
                var savedobj = {};
                angular.copy($scope.EntityAdmin, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
                $scope.EntityAdmin = {};
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    }
    //for GridView
    var columnDefs = [
       { headerName: "Parent Entity Code", field: "ADM_Code", width: 100, cellClass: 'grid-align' },
       { headerName: "Parent Entity Name", field: "ADM_Name", width: 150, cellClass: 'grid-align' },
       //{ headerName: "Entity Admin", template: "{{data.ADM_Code}} / {{data.ADM_Name}}", width: 300, cellClass: 'grid-align' },
       { headerName: "Status", template: "{{ShowStatus(data.ADM_Status_Id)}}", width: 100, cellClass: 'grid-align', suppressMenu: true },
       { headerName: "Edit", width: 40, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        EntityAdminService.GetEntityAdminGridData().then(function (data) {
            $scope.gridata = data;
            //$scope.createNewDatasource();
            $scope.gridOptions.api.setRowData(data);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 700);
        }, function (error) {
            console.log(error);
        });
    }
    //$scope.pageSize = '10';

    //$scope.createNewDatasource = function () {
    //    var dataSource = {
    //        pageSize: parseInt($scope.pageSize),
    //        getRows: function (params) {
    //            setTimeout(function () {
    //                var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
    //                var lastRow = -1;
    //                if ($scope.gridata.length <= params.endRow) {
    //                    lastRow = $scope.gridata.length;
    //                }
    //                params.successCallback(rowsThisPage, lastRow);
    //            }, 500);
    //        }
    //    };
    //    $scope.gridOptions.api.setDatasource(dataSource);
    //}

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        suppressHorizontalScroll: true,
        enableFilter: true,

        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };
    $timeout($scope.LoadData, 1000);
    $scope.EditFunction = function (data) {
        $scope.EntityAdmin = {};
        angular.copy(data, $scope.EntityAdmin);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
    }
    $scope.ClearData = function () {
        $scope.EntityAdmin = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
    }
    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
});