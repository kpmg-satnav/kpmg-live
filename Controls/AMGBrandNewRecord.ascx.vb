Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AMGBrandNewRecord
    Inherits System.Web.UI.UserControl

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/MASTERS/MAS_Webfiles/frmMASMasters.aspx")
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim ValidateCode As Integer
        ValidateCode = ValidateBrandCode()
        If ValidateCode = 0 Then
            lblMsg.Text = "Code already exist please select another code"
        ElseIf ValidateCode = 1 Then
            insertnewrecord()
        End If

        
    End Sub
    Public Function ValidateBrandCode()
        Dim ValidateCode As Integer
        Dim AAB_CODE As String = txtCode.Text
        Dim sp5 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_BRAND_CODE")
        sp5.Command.AddParameter("@AAB_CODE", AAB_CODE, DbType.String)
        ValidateCode = sp5.ExecuteScalar()
        Return ValidateCode
    End Function
    Public Sub Cleardata()
        txtCode.Text = ""
        txtRemarks.Text = ""
        txtName.Text = ""
    End Sub
    Public Sub insertnewrecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_AMG_BRAND")

            sp1.Command.AddParameter("@AAB_CODE", txtCode.Text, DbType.String)
            sp1.Command.AddParameter("@AAB_NAME", txtName.Text, DbType.String)
            sp1.Command.AddParameter("@AAB_STA_ID", rdbtnBrandID.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AAB_UPT_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@AAB_UPT_DT", getoffsetdatetime(DateTime.Now).ToString(), DbType.Date)
            sp1.Command.AddParameter("@AAB_REM", txtRemarks.Text, DbType.String)
            sp1.ExecuteScalar()
            lblMsg.Text = "New Brand Added"
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
End Class
