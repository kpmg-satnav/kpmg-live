Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.IO

Partial Class Controls_AMGVendorNewRecord1
    Inherits System.Web.UI.UserControl

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else


            Dim ValidateCode As Integer
            ValidateCode = ValidateVendorCode()

            Dim cntSelect As Integer = 0

            For i As Integer = 0 To lstTypes.Items.Count - 1
                If lstTypes.Items(i).Selected = True Then
                    cntSelect += 1
                End If
            Next
            If cntSelect = 0 Then
                lblMsg.Text = "Please select Vendor Type."
                Exit Sub
            End If
            Dim cntVenCat As Integer = 0
            For i As Integer = 0 To chkVendorCat.Items.Count - 1
                If chkVendorCat.Items(i).Selected = True Then
                    cntVenCat += 1
                End If
            Next
            If cntVenCat = 0 Then
                lblMsg.Text = "Please Select Vendor Category."
                Exit Sub
            End If
            If ValidateCode = 0 Then
                lblMsg.Text = "Vendor Code Already Exist Please Select Another Code"
            ElseIf ValidateCode = 1 Then
                insertnewrecord()

            End If
        End If
    End Sub
    Private Sub BindState()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_BIND_STATES")
        ddlState.DataSource = sp.GetReader
        ddlState.DataTextField = "AM_ST_NAME"
        ddlState.DataValueField = "AM_ST_CODE"
        ddlState.DataBind()
        ddlState.Items.Insert(0, "--Select--")
    End Sub
    Public Sub fillgrid()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMG_VENDOR")
        sp.Command.AddParameter("@AVR_NAME", txtfindcode.Text, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        ViewState("DS") = ds
        gvDetails_AVR.DataSource = ds
        gvDetails_AVR.DataBind()
        For i As Integer = 0 To gvDetails_AVR.Rows.Count - 1
            Dim lblStatus_AVR As Label = CType(gvDetails_AVR.Rows(i).FindControl("lblStatus_AVR"), Label)
            If lblStatus_AVR.Text = "0" Then
                lblStatus_AVR.Text = "Inactive"
            Else
                lblStatus_AVR.Text = "Active"
            End If
        Next

        'gvExportToExcel.DataSource = ds
        'gvExportToExcel.DataBind()
        'For i As Integer = 0 To gvExportToExcel.Rows.Count - 1
        '    Dim lblStatus_AVR As Label = CType(gvExportToExcel.Rows(i).FindControl("lblStatus_AVR"), Label)
        '    If lblStatus_AVR.Text = "0" Then
        '        lblStatus_AVR.Text = "Inactive"
        '    Else
        '        lblStatus_AVR.Text = "Active"
        '    End If

        'Next


    End Sub
    Public Function ValidateVendorCode()
        Dim ValidateCode As Integer
        Dim AVR_CODE As String = txtCode.Text
        Dim sp5 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_VENDOR_CODE")
        sp5.Command.AddParameter("@AVR_CODE", AVR_CODE, DbType.String)
        ValidateCode = sp5.ExecuteScalar()
        Return ValidateCode
    End Function
    Public Sub insertnewrecord()
        Try

            For i As Integer = 0 To lstTypes.Items.Count - 1
                If lstTypes.Items(i).Selected = True Then
                    InsertVendorType(txtCode.Text, lstTypes.Items(i).Text, lstTypes.Items(i).Value)
                End If
            Next
            For j As Integer = 0 To chkVendorCat.Items.Count - 1
                If chkVendorCat.Items(j).Selected = True Then
                    InsertVendorCategory(txtCode.Text, chkVendorCat.Items(j).Value)
                End If
            Next
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADD_AMG_VENDOR")
            sp1.Command.AddParameter("@AVR_CODE", txtCode.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_NAME", txtName.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_GRADE", "A", DbType.String)
            sp1.Command.AddParameter("@AVR_ADDR", txtAddress.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_CITY", ddlCity.SelectedItem.Value, DbType.String)
            'sp1.Command.AddParameter("@AVR_STATE", ddlState.SelectedItem.Value, DbType.String)
            'sp1.Command.AddParameter("@AVR_COUNTRY", txtCountry.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_PHNO", txtPhone.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_MOBILE_PHNO", txtMobile.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_EMAIL", txtEmail.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_STATUS", rdbtnStatus.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AVR_STA_ID", rdbtnStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AVR_UPT_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@AVR_REMARKS", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_ACCNO", txtBankAcNo.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_BRN_NAME", txtBranchName.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_IFSC_CODE", txtIFSCCode.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_BANK_NAME", txtBankName.Text, DbType.String)

            sp1.Command.AddParameter("@AVR_PAN_NO", txtPANno.Text, DbType.String)
            'sp1.Command.AddParameter("@AVR_GIR_NO", txtGIRno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_TAN_NO", txtTANno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_STATE", ddlState.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AVR_GSTNO", txtgsttno.Text, DbType.String)
            'sp1.Command.AddParameter("@AVR_WSTNO", txtwctno.Text, DbType.String)
            'sp1.Command.AddParameter("@AVR_CSTNO", txtcstno.Text, DbType.String)
            'sp1.Command.AddParameter("@AVR_STCIRCLE", txtstcircle.Text, DbType.String)
            sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp1.ExecuteScalar()


            lblMsg.Text = "Vendor Details Added Successfully"
            Cleardata()
            fillgrid()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Private Sub ddlState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlState.SelectedIndexChanged
    '    Dim ad = String.Join(",", ddlState.Items.OfType(Of ListItem)().Where(r >= r.Selected).[Select](r >= r.Value))

    'End Sub
    Private Sub InsertVendorCategory(ByVal VendorCode As String, ByVal VendorCatName As String)
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@AMG_VEN_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = VendorCode
        param(1) = New SqlParameter("@CAT_NAME", SqlDbType.NVarChar, 200)
        param(1).Value = VendorCatName
        param(2) = New SqlParameter("@COMPANYID", SqlDbType.Int)
        param(2).Value = Session("COMPANYID")
        ObjSubsonic.GetSubSonicExecute("INSERT_VERDOR_CATEGORY", param)
    End Sub

    Private Sub InsertVendorType(ByVal VendorCode As String, ByVal VendorTypeCode As String, ByVal VendorTypeId As Integer)
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@AMG_VENDOR_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = VendorCode
        param(1) = New SqlParameter("@TYPE_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = VendorTypeCode
        param(2) = New SqlParameter("@TYPE_ID", SqlDbType.Int)
        param(2).Value = VendorTypeId
        param(3) = New SqlParameter("@COMPANYID", SqlDbType.Int)
        param(3).Value = Session("COMPANYID")
        ObjSubsonic.GetSubSonicExecute("INSERT_VERDORTYPE", param)

    End Sub

    Public Sub Cleardata()
        txtCode.Text = ""
        txtName.Text = ""
        txtAddress.Text = ""
        txtPhone.Text = ""
        txtMobile.Text = ""
        txtEmail.Text = ""
        txtRemarks.Text = ""
        ddlCity.SelectedIndex = 0
        txtBankAcNo.Text = ""
        txtBankName.Text = ""
        txtBranchName.Text = ""
        txtIFSCCode.Text = ""
        'ddlState.SelectedItem.Value = 0
        lstTypes.ClearSelection()
        chkVendorCat.ClearSelection()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/Masters/MAS_Webfiles/frmMASMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If
        If Not IsPostBack Then
            'RegExpCode.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
            'RegExpName.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
            'RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()

            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                'RegExpCode.ValidationExpression = User_Validation.GetValidationExpressionForCode()
                'RegExpName.ValidationExpression = User_Validation.GetValidationExpressionForName()
                'RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks()
                'RegExpPhone.ValidationExpression = User_Validation.GetValidationExpressionForPhone()
                'RegExpPhone2.ValidationExpression = User_Validation.GetValidationExpressionForPhone()
                'RegExpEmail.ValidationExpression = User_Validation.GetValidationExpressionForEmail()
                'RegExpAssessment.ValidationExpression = User_Validation.GetValidationExpressionForName()
                'RegExpCST.ValidationExpression = User_Validation.GetValidationExpressionForPhone()
                'RegExpWCT.ValidationExpression = User_Validation.GetValidationExpressionForPhone()
                'RegExpGIR.ValidationExpression = User_Validation.GetValidationExpressionForPhone()
                'RegExpLST.ValidationExpression = User_Validation.GetValidationExpressionForPhone()
                'RegExpTAN.ValidationExpression = User_Validation.GetValidationExpressionForPhone()
                'RegExpPAN.ValidationExpression = User_Validation.GetValidationExpressionForPhone()
                'RegExpIFSC.ValidationExpression = User_Validation.GetValidationExpressionForPhone()
                'RegExpBranchName.ValidationExpression = User_Validation.GetValidationExpressionForName()
                'RegExpBankAC.ValidationExpression = User_Validation.GetValidationExpressionForPhone()
                'RegExpBankName.ValidationExpression = User_Validation.GetValidationExpressionForName()
                'RegExpAddress.ValidationExpression = User_Validation.GetValidationExpressionForName()
                FillTypes()
                gvDetails_AVR.PageIndex = 0
                fillgrid()
                BindState()
                Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITY_ALL")
                sp2.Command.AddParameter("@dummy", " ", DbType.String)
                ddlCity.DataSource = sp2.GetDataSet()
                ddlCity.DataTextField = "CTY_NAME"
                ddlCity.DataValueField = "CTY_CODE"
                ddlCity.DataBind()
                ddlCity.Items.Insert(0, "--Select--")
            End If
        End If
    End Sub
    Protected Sub gvDetails_AVR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails_AVR.PageIndexChanging
        gvDetails_AVR.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub gvDetails_AVR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDetails_AVR.SelectedIndexChanged
        lblMsg.Text = ""
        lblMsg.Visible = False
        Dim rowIndex As Integer = gvDetails_AVR.SelectedIndex
        Dim lbl As Label = DirectCast(gvDetails_AVR.Rows(rowIndex).FindControl("lbl"), Label)
    End Sub

    Protected Sub gvDetails_AVR_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDetails_AVR.RowDeleting

    End Sub

    Private Sub FillTypes()

        ObjSubsonic.BindListBox(lstTypes, "GET_TYPES", "VT_TYPE", "ID")
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        '  Response.Redirect("frmAMGVendorGetDetails.aspx")
        Response.Redirect("~/Masters/MAS_Webfiles/frmMASMasters.aspx")
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        fillgrid()
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        ExportToExcelEmployeeAllocationReport()
    End Sub
    Private Sub ExportToExcelEmployeeAllocationReport()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMG_VENDOR_EXCEL")
        Dim ds As New DataSet
        ds = sp.GetDataSet
        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        Export("Vendor.xls", gv)
    End Sub
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
End Class
