Imports System.Data
Partial Class Controls_AddStockItems
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            btnsubmit.Visible = True
            fillgrid()
        End If
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_ADD_STOCKITEMS_GRID")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        gvItemStock.DataSource = sp.GetDataSet()
        gvItemStock.DataBind()
    End Sub
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim validatecode As Integer
        validatecode = ValidateAIMCode()
        If validatecode = "0" Then
            lblMsg.Text = "Code Already Exist,Please enter another Code"
        ElseIf validatecode = "1" Then
            insertnewrecord()
        End If
    End Sub
    Private Function ValidateAIMCode()
        Dim validatecode As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_VALIDATE_AIMCODE")
        sp.Command.AddParameter("@AIMCODE", txtAIMC.Text, DbType.String)
        validatecode = sp.ExecuteScalar()
        Return validatecode
    End Function
    Private Sub insertnewrecord()
        Try
            Dim Esc1 As Double = 0
            Dim Esc2 As Double = 0
            Dim Esc3 As Double = 0


            If IsNumeric(txtESC1.Text) = True Then
                Esc1 = txtESC1.Text
            End If
            If IsNumeric(txtESC2.Text) = True Then
                Esc2 = txtESC2.Text
            End If
            If IsNumeric(txtESC2.Text) = True Then
                Esc3 = txtESC3.Text
            End If


            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_ADD_STOCKITEMS")
            sp.Command.AddParameter("@AIM_CODE", txtAIMC.Text, DbType.String)
            sp.Command.AddParameter("@AIM_UNIT_RATE", txtUntPrc.Text, DbType.Double)
            sp.Command.AddParameter("@AIM_RT1", Esc1, DbType.Double)
            sp.Command.AddParameter("@AIM_RT1Email", txtEmail1.Text, DbType.String)
            sp.Command.AddParameter("@AIM_RT2", Esc2, DbType.Double)
            sp.Command.AddParameter("@AIM_RT2Email", txtEmail2.Text, DbType.String)
            sp.Command.AddParameter("@AIM_RT3", Esc3, DbType.Double)
            sp.Command.AddParameter("@AIM_RT3Email", txtEmail3.Text, DbType.String)
            sp.Command.AddParameter("@AIM_REM", txtRem.Text, DbType.String)
            sp.Command.AddParameter("@AIM_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@AIM_MINORDQTY", txtMinOrdr.Text, DbType.Int32)
            sp.Command.AddParameter("@AIM_NAME", txtName.Text, DbType.String)
            sp.Command.AddParameter("@AIM_UP_BY", Session("Uid"), DbType.String)
            sp.ExecuteScalar()
            cleardata()
            lblMsg.Visible = True
            lblMsg.Text = "New Item added successfully"
            fillgrid()
            'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?ID=9")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub gvItemStock_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItemStock.PageIndexChanging
        gvItemStock.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
    Protected Sub gvItemStock_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItemStock.RowCommand
        If e.CommandName = "DELETE" Then
            If gvItemStock.Rows.Count > 0 Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                Dim lblID As Label = DirectCast(gvItemStock.Rows(rowIndex).FindControl("lblID"), Label)
                Dim id1 As Integer = CInt(lblID.Text)
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_DELETE_STOCKITEMS")
                sp.Command.AddParameter("@id1", id1, DbType.Int32)
                sp.ExecuteScalar()
                fillgrid()
            End If
        End If
    End Sub
    Private Sub cleardata()
        txtAIMC.Text = ""
        txtUntPrc.Text = ""
        txtESC1.Text = ""
        txtESC2.Text = ""
        txtESC3.Text = ""
        txtEmail1.Text = ""
        txtEmail2.Text = ""
        txtEmail3.Text = ""
        txtRem.Text = ""
        txtMinOrdr.Text = ""
        txtName.Text = ""
        ddlStatus.SelectedIndex = 0
    End Sub
    Protected Sub gvItemStock_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvItemStock.RowDeleting
    End Sub
End Class

