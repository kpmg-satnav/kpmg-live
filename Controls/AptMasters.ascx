<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AptMasters.ascx.vb" Inherits="Controls_AptMasters" %>




<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Journey Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvfromdate" runat="server" ControlToValidate="txtJrDate"
                    Display="None" ErrorMessage="Please Pick Journey Date " ValidationGroup="Val2"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Click On the TextBox  to Pick the date')" onmouseout="UnTip()">
                        <div class='input-group date' id='fromdate'>
                            <asp:TextBox ID="txtJrDate" runat="server" CssClass="form-control" TabIndex="1"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Purpose Of ravel<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvpurpose" runat="server" ControlToValidate="txtPurpose"
                    Display="None" ErrorMessage="Please Enter Purpose of Travel " ValidationGroup="Val2"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter Purpose with maximum Limit of 500 characters)" onmouseout="UnTip()">
                        <asp:TextBox ID="txtPurpose" runat="server" CssClass="form-control" TextMode="MultiLine"
                            Rows="3" MaxLength="500" TabIndex="1"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row text-center">
                <strong>ADD EXPENSES</strong>
            </div>
        </div>
    </div>
</div>
<div id="panel1" runat="server">


    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Expense For<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvExpense" runat="server" ValidationGroup="Val1"
                        ErrorMessage="Please Select Expense For" Display="None" ControlToValidate="ddlExpense"
                        InitialValue="0"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlExpense" runat="server" CssClass="selectpicker" data-live-search="true"
                            AutoPostBack="True" OnSelectedIndexChanged="ddlExpense_SelectedIndexChanged" TabIndex="1">
                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                            <asp:ListItem Value="TCKT">Tickets</asp:ListItem>
                            <asp:ListItem Value="CONV">Conveyance</asp:ListItem>
                            <asp:ListItem Value="DLAL">Daily Allowance</asp:ListItem>
                            <asp:ListItem Value="BL">Boarding &amp; Lodging</asp:ListItem>
                            <asp:ListItem Value="PH">Phone</asp:ListItem>
                            <asp:ListItem Value="MSC">Miscellaneous</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6" id="tr2" runat="Server">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Mode of Travel<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvMode" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select Mode Of Travel"
                        Display="None" ControlToValidate="ddlMode" InitialValue="0"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlMode" runat="server" CssClass="selectpicker" data-live-search="true"
                            AutoPostBack="True" TabIndex="1">
                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                            <asp:ListItem Value="FLT">Flight</asp:ListItem>
                            <asp:ListItem Value="TRN">Train</asp:ListItem>
                            <asp:ListItem Value="BS">Bus</asp:ListItem>
                            <asp:ListItem Value="CR">Car</asp:ListItem>
                            <asp:ListItem Value="ATO">Auto</asp:ListItem>
                            <asp:ListItem Value="BKE">Bike</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>






    <div class="row" id="tr1" runat="server">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Approximate (Kilometers)<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvKm" runat="server" ControlToValidate="txtKM" Display="None"
                        ErrorMessage="Please Enter Approximate Kilometers " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revkm" runat="Server" ControlToValidate="txtKM"
                        ValidationExpression="[-+]?[0-9]*\.?[0-9]+" ValidationGroup="Val1" Display="None"
                        ErrorMessage="Please enter valid Approximate Kilometers"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtKM" runat="server" CssClass="form-control" MaxLength="8"
                            AutoPostBack="True" TabIndex="1"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Amount<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvamount" runat="server" ControlToValidate="txtAmount"
                        Display="None" ErrorMessage="Please Enter Amount " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revAAmount" runat="Server" ControlToValidate="txtAmount"
                        ValidationExpression="[-+]?[0-9]*\.?[0-9]+" ValidationGroup="Val1" Display="None"
                        ErrorMessage="Please enter valid Amount"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" MaxLength="8" TabIndex="1"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                        Display="None" ErrorMessage="Please Enter Remarks " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter Purpose with maximum Limit of 500 characters)" onmouseout="UnTip()">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                                Rows="3" MaxLength="500" TabIndex="1"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">

                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                    CausesValidation="true" TabIndex="1" />
                <asp:Button ID="btnModify" runat="server" Text="Modify" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                    CausesValidation="true" />
                <asp:TextBox ID="txtStore" runat="server" CssClass="form-control" Visible="FALSE"></asp:TextBox>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblTamount" runat="server" Text="Total Amount"></asp:Label>
                <asp:Label ID="lblTotalAmount" runat="server"> </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvItems" runat="server" EmptyDataText="No Domestic Conveyance Request Found."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Journey Date">
                        <ItemTemplate>
                            <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("JR_DATE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Expense">
                        <ItemTemplate>
                            <asp:Label ID="lblMode" runat="server" Text='<%#Eval("EXPENSE_FOR")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblTmode" runat="server" Text='<%#Eval("TRAVEL_MODE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approx. KMs.">
                        <ItemTemplate>
                            <asp:Label ID="lblAppkm" runat="server" Text='<%#Eval("KM")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblpurs" runat="server" Text='<%#Eval("PURPOSE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("AMOUNT", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("REMARKS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField Text="Edit" CommandName="EDIT" />
                    <asp:ButtonField Text="Delete" CommandName="DELETE" />
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">

            <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val2"
                CausesValidation="true" />

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvdetails" runat="server" EmptyDataText="No Domestic Conveyance Request Found."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Journey Date">
                        <ItemTemplate>
                            <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("CNV_JOURNEY_DATE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Expense">
                        <ItemTemplate>
                            <asp:Label ID="lblMode" runat="server" Text='<%#Eval("CNV_EXPENSE_MODE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblTmode" runat="server" Text='<%#Eval("CNV_TRVL_MODE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approx KMs">
                        <ItemTemplate>
                            <asp:Label ID="lblAppkm" runat="server" Text='<%#Eval("CNV_KM")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purpose of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblpurs" runat="server" Text='<%#Eval("CNV_PURPOSE_TRVL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("CNV_TOTAL_AMOUNT","{0:c2}" )%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("CNV_rEMARKS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>

