<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Mapped_Unmapped_AstDispose.ascx.vb"
    Inherits="Controls_Mapped_Unmapped_AstDispose" %>

<body>

      
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">

                <label class="col-md-12 control-label">Asset Category<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="reqastcat" runat="server" ErrorMessage="Please Enter Asset Category" InitialValue="--Select--"
                    ControlToValidate="ddlastCat" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlastCat" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True" Style="width: 77px">
                    </asp:DropDownList>

                </div>
            </div>
        </div>


        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">

                <label class="col-md-12 control-label">Asset Sub Category <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rersubastcat" runat="server" ErrorMessage="Please Enter Asset sub Category" InitialValue="--Select--"
                    ControlToValidate="ddlastsubCat" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlastsubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>

                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">

                <label class="col-md-12 control-label">Asset Brand/Make<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>

            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">

                <label class="col-md-12 control-label">Asset Model<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>
                </div>

            </div>
        </div>
    </div>

    <br />
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">

                <label class="col-md-12 control-label">
                    Location<span style="color: red;">*</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Select Location" Display="None" ControlToValidate="ddlLocation"
                        InitialValue="--Select--"></asp:RequiredFieldValidator>
                </label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" CssClass="selectpicker" data-live-search="true" runat="server"></asp:DropDownList>
                </div>


            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnView" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" />
                <asp:Button ID="btnclear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" ValidationGroup="Val1" CausesValidation="False" />
            </div>
        </div>
    </div>
    <div class="row" id="panel1" runat="server">
        <div class="col-md-3">
            <asp:TextBox ID="txtSearch" runat="server" placeholder="Search By Any..." CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <asp:Button ID="btnSearch" CssClass="btn btn-primary custom-button-color" runat="server" Text="Filter"
                    CausesValidation="true" TabIndex="2" />
            </div>
        </div>

        <div class="col-md-12" style="overflow-x: auto;">
            <asp:GridView ID="gvAstList" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                AllowPaging="True" EmptyDataText="No Asset Dispose Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkAll" runat="server"
                                ToolTip="Click to check all" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:ChildClick(this);" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location">
                        <ItemTemplate>
                            <asp:Label ID="lblLCM_NAME" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset ID">
                        <ItemTemplate>
                            <asp:Label ID="lblAIM_NAME" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Name">
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("AAT_DESC")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Code" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblAstCode" runat="server" Text='<%#Eval("AAT_CODE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Vendor Name">
                        <ItemTemplate>
                            <asp:Label ID="lblVen_NAME" runat="server" Text='<%#Eval("Ven_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Model">
                        <ItemTemplate>
                            <asp:Label ID="lblModel_NAME" runat="server" Text='<%#Eval("AAT_MODEL_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purchase Date">
                        <ItemTemplate>
                            <asp:Label ID="lblpur_Date" runat="server" Text='<%#Eval("PURCHASEDDATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Age(Till date)">
                        <ItemTemplate>
                            <asp:Label ID="lbl_age" runat="server" Text='<%#Eval("ASSET_AGE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actual Cost ">
                        <ItemTemplate>
                            <asp:Label ID="lbl_cost" runat="server" Text='<%# Eval("COST")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lblActualCost" runat="server" Text='<%# Eval("COST", "{0:c2}")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--  <asp:TemplateField HeaderText="Asset Salvage Value ">
                    <ItemTemplate>
                        <asp:Label ID="lbl_salvge" runat="server" Text='<%#Eval("AAT_AST_SLVGVAL")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>

                    <asp:TemplateField HeaderText="Depreciated(%)">
                        <ItemTemplate>
                            <asp:Label ID="lbl_Dep_per" runat="server" Text='<%#Eval("DEP_PERCENTAGE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Depreciation Cost(Till Date)">
                        <ItemTemplate>
                            <asp:Label ID="lbl_Dep_val" runat="server" Text='<%#Eval("DEPVALUE")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lblDepCost" runat="server" Text='<%#Eval("DEPVALUE", "{0:c2}")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Current Value">
                        <ItemTemplate>
                            <asp:Label ID="lbl_Cur_val" runat="server" Text='<%#Eval("CURVALUE")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lblCurrentvalue" runat="server" Text='<%#Eval("CURVALUE", "{0:c2}")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Tag Id" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lbl_emp_tag_id" runat="server" Text='<%#Eval("EMP_TAG_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Mapped To">
                        <ItemTemplate>
                            <asp:Label ID="lbl_asset_mapped" runat="server" Text='<%#Eval("AUR_KNOWN_AS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lbl_status" runat="server" Text='<%#Eval("STATUS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Life Span">
                        <ItemTemplate>
                            <asp:Label ID="lbllifespan" runat="server" Text='<%#Eval("AST_LIFE_TIME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date of Disposal">

                        <ItemTemplate>
                            <%--<div class='input-group date' id='Disposals_date'>
                                <asp:TextBox ID="txtdisposaldate" runat="server" CssClass="selectpicker"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup(this.id)"></span>
                                </span>
                            </div>--%>
                            <div class="input-group date" id='Disposals_date' onclick="setup(this.id)" runat="server">
                                <asp:TextBox ID="txtdisposaldate" CssClass="selectpicker" runat="server" Style="height: 31px"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type of Disposals">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlAssetDis" runat="server" CssClass="selectpicker">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Recovery Value">
                        <ItemTemplate>
                            <asp:TextBox ID="txtRecoveryval" runat="server" CssClass="selectpicker" MaxLength="10"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Upload File">
                        <ItemTemplate>
                            <asp:FileUpload ID="fu1" runat="server" EnableViewState="true" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
    <br />
    <div class="row" id="remarks" runat="server">
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="col-md-12">Remarks <span style="color: red;">*</span> </label>
                <asp:RequiredFieldValidator ID="rfvtxtRemarks" runat="server" ControlToValidate="txtRemarks" Display="None" ErrorMessage="Please Enter Remarks"
                    ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-12">
                    <asp:TextBox ID="txtRemarks" CssClass="form-control" Width="100%" Height="30%"
                        runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                </div>
        </div>
    </div>

    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="DisposeSelected" runat="server" Text="Dispose"  ValidationGroup="Val1" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
    </div>

    <script src="../BootStrapCSS/Scripts/bootstrap-select.js"></script>
    <script src="../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script type="text/javascript">

    <%-- Modal popup block --%>
        function showPopWin(id) {
            $("#modalcontentframe").attr("src", "frmMapped_Unmapped_AstDisposeDTLS.aspx?astid=" + id);
            $("#myModal").modal().fadeIn();
            return false;
        }
        function refreshSelectpicker() {
            $("#<%=ddlAstBrand.ClientID%>").selectpicker();
            $("#<%= ddlastCat.ClientID%>").selectpicker();
            $("#<%= ddlastsubCat.ClientID%>").selectpicker();
            $("#<%= ddlLocation.ClientID%>").selectpicker();
            $("#<%= ddlAstModel.ClientID%>").selectpicker();
            $("#<%=gvAstList.ClientID %>").selectpicker();

            $('#<%=gvAstList.ClientID %>').find('[id$="ddlAssetDis"]').selectpicker();
            $('#<%=gvAstList.ClientID %>').find('[id$="txtRecoveryval"]').selectpicker();
        }
        refreshSelectpicker();



    </script>


    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.gvAstList.Rows.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }

        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvAstList.ClientID%>');
            var TargetChildControl = "chkSelect";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvAstList.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }

    </script>
    <script type="text/javascript">
        $(function setup() {
            $("[id$=Disposals_date]").datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                showOn: 'focus'
            });
        });
    </script>


    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Mapped & Unmapped Asset Dispose</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" src="#" width="100%" height="250px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>
</body>

