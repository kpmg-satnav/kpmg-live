<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssetLocationReport.ascx.vb" Inherits="Controls_AssetLocationReport" %>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Asset Location Report
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Asset Location Report</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
            <table id="tb1" runat="server" width="100%">
            <tr id="tr3" runat="Server" width="100%">
                                <td align="left" width="50%">
                                    <asp:Label ID="lbllocation" runat="server" CssClass="bodytext" Text="Select Location"> </asp:Label>
                                    <font class="clsNote">*</font>
                                     <asp:RequiredFieldValidator ID="rfvloc" runat="server" ControlToValidate="ddlLoc"
                                        Display="Dynamic" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="0">
                                </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" width="50%">
                                    <asp:DropDownList ID="ddlLoc" runat="server" Width="97%" CssClass="dropDown" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            </table>
                            <asp:Panel ID="panle1" runat="Server" Width="100%">
                             <table id="table1" cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td align="left">
                            <asp:GridView ID="gvitems" runat="server" EmptyDataText="There are no data records to display."
                                Width="100%" AutoGenerateColumns="false" AllowPaging="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="Assets Count">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcode" runat="Server" Text='<%# Eval("AssetsCount") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Product Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblname" runat="server" Text='<%# Eval("PRODUCTNAME") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <asp:Label ID="lblname" runat="server" Text='<%# Eval("LCM_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    
                                    
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                            </asp:Panel>
            </td>
            <td background="../../Images/table_right_mid_bg.gif">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td>
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
