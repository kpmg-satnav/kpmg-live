<%@ Control Language="VB" AutoEventWireup="false" CodeFile="searchbySpaceid.ascx.vb" Inherits="Controls_searchbySpaceid" %>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js">
</script>

<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Get Space Status
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0" id="TABLE1" onclick="return TABLE1_onclick()">
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Get Space Status</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
            
              <asp:ValidationSummary ID="ValidationSummary1" runat="server"  CssClass="clsMessage"
                                ForeColor="" ValidationGroup="Val1" /><br />
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label2" runat="server" CssClass="clsMessage"></asp:Label><br />
                <table id="table4" cellspacing="0" cellpadding="1" width="100%" border="1">
                   <tr>
                        <td align="left" width="50%">
                          Space ID
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtSpcName"
                                Display="None" ErrorMessage="Please Enter the Space ID!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:TextBox ID="txtSpcName" runat="server" CssClass="textBox" Width="96%" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            &nbsp;<asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="button" ValidationGroup="Val1" />&nbsp;

                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                <br />
                 <br />
               
            </td>
            <td background="../../Images/table_right_mid_bg.gif">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td background="../../images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td>
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>