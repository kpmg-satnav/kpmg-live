<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditUserProfile.ascx.vb" Inherits="Controls_EditUserProfile" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="clearfix">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Employee ID</label>
            <asp:TextBox ID="txtEmpID" runat="server" CssClass="form-control" MaxLength="50" Enabled="false"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Name<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtFrstName"
                Display="None" ErrorMessage="Please Enter the First Name!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:TextBox ID="txtFrstName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Email<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail"
                Display="None" ErrorMessage="Please Enter the Email!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Phone no.</label>
            <asp:TextBox ID="txtRes" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>
    </div>
</div>
<div class="clearfix">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Date of Birth<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDOB"
                ErrorMessage="Please Select Date of Birth" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>
            <div class='input-group date' id='fromdate'>
                <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Date of Joining<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtDOJ"
                ErrorMessage="Please Select Date of Joining" ValidationGroup="Val1" SetFocusOnError="True"
                Display="None"></asp:RequiredFieldValidator>
            <div class='input-group date' id='Div1'>
                <asp:TextBox ID="txtDOJ" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Country<span style="color: red;">*</span></label>
            <asp:CompareValidator ID="CompareValidator3" runat="server" ValueToCompare="--Select--"
                Operator="NotEqual" ControlToValidate="ddlcountry" Display="None" ErrorMessage="Please Select Country"
                ValidationGroup="Val1"></asp:CompareValidator>
            <asp:DropDownList ID="ddlcountry" runat="server" CssClass="selectpicker" data-live-search="true"
                TabIndex="6" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Time Zone<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RFVtimezone" runat="server" ControlToValidate="ddlTimeZone"
                Display="None" ErrorMessage="Please Select Time Zone" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlTimeZone" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="7"></asp:DropDownList>
        </div>
    </div>
</div>

<div class="clearfix">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Reporting Manager<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlRept"
                Display="None" ErrorMessage="Please Select Reporting Manager" ValidationGroup="Val1"
                InitialValue="--Select--">
            </asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlRept" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True"></asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Extension No</label>
            <asp:RegularExpressionValidator ID="rev1" runat="server" ControlToValidate="txtExtn"
                Display="None" ErrorMessage="Invalid Extension No." ValidationExpression="^[0-9]*$"
                ValidationGroup="Val1"></asp:RegularExpressionValidator>
            <asp:TextBox ID="txtExtn" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Select Department<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlDept"
                Display="None" ErrorMessage="Please Select Department" ValidationGroup="Val1"
                InitialValue="--Select Department--">
            </asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlDept" runat="server" CssClass="selectpicker" data-live-search="true"
                AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Designation</label>
            <asp:DropDownList ID="ddldesn" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                TabIndex="14">
            </asp:DropDownList>
        </div>
    </div>
</div>

<div class="clearfix">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Status<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                Display="None" ErrorMessage="Select Status" ValidationGroup="Val1" InitialValue="--Select Status--"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true">
                <asp:ListItem>--Select Status--</asp:ListItem>
                <asp:ListItem Value="1">Active</asp:ListItem>
                <asp:ListItem Value="0">InActive</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
</div>
<div></div>
<div class="clearfix">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <h4>
                <label class="control-label" style="text-align: left;">Personal Details </label>
            </h4>
        </div>
    </div>
</div>
<div></div>
<div class="clearfix">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Father's Name</label>
            <asp:TextBox ID="txtFthrName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Blood Group</label>
            <asp:TextBox ID="txtBldGrp" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Date of Anniversary</label>
            <div class='input-group date' id='Div2'>
                <asp:TextBox ID="txtDOA" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Address</label>
            <asp:TextBox ID="txtAddr" runat="server" CssClass="form-control" MaxLength="50" Rows="3" Height="25%" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
</div>
<div class="clearfix">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Select Shift<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfshift" runat="server" ControlToValidate="ddlShift"
                Display="None" ErrorMessage="Please Select Shift" ValidationGroup="Val1"
                InitialValue="--Select Shift--">
            </asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlShift" runat="server" CssClass="selectpicker" data-live-search="true">
                <asp:ListItem Value="0">General</asp:ListItem>
                <asp:ListItem Value="1">First</asp:ListItem>
                <asp:ListItem Value="2">Second</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Experience</label>
            <asp:TextBox ID="txtExp" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Skills</label>
            <asp:TextBox ID="txtSkills" runat="server" CssClass="form-control" MaxLength="200" Rows="3" Height="25%" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Qualification</label>
            <asp:TextBox ID="txtQlfctn" runat="server" CssClass="form-control" MaxLength="200" Rows="3" Height="25%" TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
</div>
<div class="clearfix">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Passport No</label>
            <asp:TextBox ID="txtPsptNo" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>
    </div>
   <%-- <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Pan Card No</label>
            <asp:TextBox ID="txtPanNo" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>
    </div>--%>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Insurance No</label>
            <asp:TextBox ID="txtInsNo" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Emergency No</label>
            <asp:TextBox ID="txtEmerNo" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>
    </div>
</div>
<div class="clearfix">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnModify" runat="server" Text="Modify" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
        </div>
    </div>
</div>


