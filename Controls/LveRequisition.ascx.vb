Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_LveRequisition
    Inherits System.Web.UI.UserControl
    Dim cnt As Integer
    Dim intparlve As Double
    Dim NumAppliedDays As Decimal
    Dim AvailableBalanceLeaves As Decimal
    Dim strLveBal As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDep()
            Dept()
            BindRM()
            BindDetails()
            BindLeaveType()
            btnSubmit.Visible = False
            txtNoLeaves.ReadOnly = True
            txtTotalLeave.ReadOnly = True
            txtBalLeaves.ReadOnly = True
            txtAssociateName.ReadOnly = True
            txtAssociateID.ReadOnly = True
            ddlDep.Enabled = False
            ddlRM.Enabled = False
            txtDesig.ReadOnly = True
            lblMsg.Text = ""
            tr2.Visible = False
            tr1.Visible = False
            'txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
            'txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

            txtFromDate.Text = getoffsetdate(Date.Today)
            'txtFromDate.Attributes.Add("onClick", "displayDatePicker('" + txtFromDate.ClientID + "')")
            'txtFromDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        End If
        txtFromDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindDep()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DEP")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDep.DataSource = sp.GetDataSet()
            ddlDep.DataTextField = "DEP_NAME"
            ddlDep.DataValueField = "DEP_CODE"
            ddlDep.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindRM()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_EMPLOYEES_RM")
            'sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            'sp.Command.AddParameter("@dep_code", ddlDep.SelectedItem.Value, DbType.String)
            ddlRM.DataSource = sp.GetDataSet()
            ddlRM.DataTextField = "AUR_FIRST_NAME"
            ddlRM.DataValueField = "AUR_ID"
            ddlRM.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Dept()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_USER_DEPT")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMANTRA_USER_GETASSOCIATE_DETAILS")
            sp.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtAssociateName.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
                txtAssociateID.Text = ds.Tables(0).Rows(0).Item("AUR_NO")
                ddlDep.ClearSelection()
                ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
                'ddlRM.ClearSelection()
                ddlRM.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_REPORTING_TO")).Selected = True
                txtDesig.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLeaveType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_TYPE_GETLEAVE_TYPE2")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            ddlLeaveType.DataSource = sp.GetDataSet()
            ddlLeaveType.DataTextField = "LVE_LEAVE_TYPE"
            ddlLeaveType.DataValueField = "LVE_LEAVE_ID"
            ddlLeaveType.DataBind()
            ddlLeaveType.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnLeaves_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeaves.Click
        lblMsg.Text = ""

        Dim startDate As DateTime
        Dim endDate As DateTime

        If DateTime.TryParse(txtFromDate.Text, startDate) AndAlso DateTime.TryParse(txtToDate.Text, endDate) Then

        Else
            lblMsg.Text = "Please Select Proper Date"
            Exit Sub
        End If


        If txtFromDate.Text = "" Or txtToDate.Text = "" Or txtAddress.Text = "" Or txtReason.Text = "" Or ddlLeaveType.SelectedIndex = 0 Then
            lblMsg.Text = "Please Fill Mandatory Fields"
            lblMsg.Visible = True
            Exit Sub
        End If 
        'If CDate(txtFromDate.Text).Equals(CDate(txtToDate.Text)) Then
        '    If cboHr.SelectedIndex = 0 Or cboMin.SelectedIndex = 0 Or ddlHH.SelectedIndex = 0 Or ddlMM.SelectedIndex = 0 Then
        '        lblMsg.Text = "Please Select From and To Time"
        '        lblMsg.Visible = True
        '        Exit Sub
        '    Else
        '        Dim val1 As Decimal = CDbl(cboHr.SelectedItem.Text) * 60
        '        Dim min1 As Decimal = CDbl(val1 + cboMin.SelectedItem.Text)

        '        Dim val2 As Decimal = CDbl(ddlHH.SelectedItem.Text) * 60
        '        Dim min2 As Decimal = CDbl(val2 + ddlMM.SelectedItem.Text)
        '        If min1 - min2 > 0 Then
        '            lblMsg.Text = "To Time Should be greater than From Time"
        '            lblMsg.Visible = True
        '            Exit Sub
        '        Else
        '            lblMsg.Text = ""
        '        End If
        '    End If
        'End If
        If CDate(txtToDate.Text) < CDate(txtFromDate.Text) Then

            lblMsg.Text = "ToDate Should be more than From Date"
            lblMsg.Visible = True
        Else
            lblMsg.Text = ""
            BindLeaves()
            
        End If
    End Sub
    Private Sub BindLeaves()
        Dim Errors As Integer
        Errors = checkErrors()
        If Errors = 1 Then
            Select Case ddlLeaveType.SelectedItem.Value
                Case 1   ' Loss Of Pay

                    If LossOfPay() Then
                        lblMsg.Text = " Request For Leaves With Loss oF Pay!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If

                Case 2    ' Paid Leave
                    If Paidfunction() Then
                        lblMsg.Text = " Request For Paid Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If
                Case 3 ' On Travel

                    If OnTravel() Then
                        lblMsg.Text = " Request For On Travel Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If


                Case 4 ' Maternity Leave

                    If MaternityFunction() Then
                        lblMsg.Text = " Request For Maternity Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If


                Case 5 ' Paternity Leave

                    If PaternityFunction() Then
                        lblMsg.Text = " Request For Paternity Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If

                Case 6  ' Compensatory Off

                    If CompensatoryFunction() Then
                        lblMsg.Text = " Request For Compensatory Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If

                Case 7 ' Casual Leave

                    If Casualfunction() Then
                        lblMsg.Text = " Request For Casual Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If

                Case 8   ' Loss Of Pay

                    If LossOfPay() Then
                        lblMsg.Text = " Request For Leaves With Loss oF Pay!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If

                Case 9   ' Paid Leave
                    If Paidfunction() Then
                        lblMsg.Text = " Request For Paid Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If

                Case 10 ' On Travel

                    If OnTravel() Then
                        lblMsg.Text = " Request For On Travel Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If

                Case 12 ' Maternity Leave

                    If MaternityFunction() Then
                        lblMsg.Text = " Request For Maternity Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If


                Case 13 ' Paternity Leave

                    If PaternityFunction() Then
                        lblMsg.Text = " Request For Paternity Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If
                Case 14 ' Compensatory Off

                    If CompensatoryFunction() Then
                        lblMsg.Text = " Request For Compensatory Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If

                Case 18   ' Loss Of Pay

                    If LossOfPay() Then
                        lblMsg.Text = " Request For Leaves With Loss oF Pay!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If

                Case 17   ' Paid Leave
                    If Paidfunction() Then
                        lblMsg.Text = " Request For Paid Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If

                Case 22 ' On Travel

                    If OnTravel() Then
                        lblMsg.Text = " Request For On Travel Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If

                Case 19 ' Maternity Leave

                    If MaternityFunction() Then
                        lblMsg.Text = " Request For Maternity Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If


                Case 20 ' Paternity Leave

                    If PaternityFunction() Then
                        lblMsg.Text = " Request For Paternity Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If
                Case 21 ' Compensatory Off

                    If CompensatoryFunction() Then
                        lblMsg.Text = " Request For Compensatory Leaves!! click on Submit "
                        lblMsg.Visible = True
                        tr2.Visible = True
                        btnSubmit.Visible = True
                    End If
            End Select
        Else
            lblMsg.Text = "You Have Already applied Leaves on Requested Dates"
            lblMsg.Visible = True
            tr2.Visible = False
            btnSubmit.Visible = False
        End If
    End Sub
    Private Function checkErrors()
        Dim validateLeave As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_REQ_VALIDATE")
        sp.Command.AddParameter("@LMS_AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@LMS_FDATE", txtFromDate.Text, DbType.DateTime)
        sp.Command.AddParameter("@LMS_TDATE", txtToDate.Text, DbType.DateTime)
        validateLeave = sp.ExecuteScalar()
        Return validateLeave
    End Function
    Private Function Paidfunction()
        lblMsg.Text = ""
        lblMsg.Visible = True
        'btnSubmit.Enabled = False
        tr2.Visible = True
        AvailableBalanceLeaves = balanceleave()
        NumAppliedDays = Holiday()
        intparlve = 0
        If CDate(txtToDate.Text) > CDate(txtFromDate.Text) Then
            If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
                If timefunction() = 1 Then
                    intparlve = NumAppliedDays + 0.5
                ElseIf timefunction() = 0 Then
                    intparlve = NumAppliedDays + 1
                Else
                    lblMsg.Text = "No Need To Apply Leave For Below 2 Hrs"
                    Return 0
                End If
            End If
        ElseIf CDate(txtFromDate.Text).Equals(CDate(txtToDate.Text)) Then
            If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
                If timefunction() = 1 Then
                    If NumAppliedDays = 0 Then
                        intparlve = 0
                    Else
                        intparlve = NumAppliedDays - 0.5
                    End If
                ElseIf timefunction() = 0 Then
                    If NumAppliedDays = 0 Then
                        intparlve = 0
                    Else
                        intparlve = NumAppliedDays
                    End If
                Else
                    lblMsg.Text = "No Need To Apply Leave For Below 2 Hrs"
                    Return 0
                End If
            End If
        End If
        If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
            NumAppliedDays = intparlve
        Else
            NumAppliedDays = NumAppliedDays
        End If

        txtNoLeaves.Text = NumAppliedDays
        txtTotalLeave.Text = AvailableBalanceLeaves
        txtBalLeaves.Text = Val(txtTotalLeave.Text) - Val(txtNoLeaves.Text)

        If NumAppliedDays > AvailableBalanceLeaves Then
            lblMsg.Text = "Sorry! You Have Requested More than Available Paid Leaves"
            Return 0
        End If
        Return 1
    End Function
    Private Function Casualfunction()
        lblMsg.Text = ""
        'btnSubmit.Enabled = False
        tr2.Visible = True
        AvailableBalanceLeaves = balanceleave()
        NumAppliedDays = Holiday()
        intparlve = 0
        If CDate(txtToDate.Text) > CDate(txtFromDate.Text) Then
            If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
                If timefunction() = 1 Then
                    intparlve = NumAppliedDays + 0.5
                ElseIf timefunction() = 0 Then
                    intparlve = NumAppliedDays + 1
                Else
                    lblMsg.Text = "No Need To Apply Leave For Below 2 Hrs"
                    Return 0
                End If
            End If
        ElseIf CDate(txtFromDate.Text).Equals(CDate(txtToDate.Text)) Then
            If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
                If timefunction() = 1 Then
                    If NumAppliedDays = 0 Then
                        intparlve = 0
                    Else
                        intparlve = NumAppliedDays - 0.5
                    End If

                ElseIf timefunction() = 0 Then
                    If NumAppliedDays = 0 Then
                        intparlve = 0
                    Else
                        intparlve = NumAppliedDays
                    End If
                Else
                    lblMsg.Text = "No Need To Apply Leave For Below 2 Hrs"
                    Return 0
                End If
            End If
        End If
        If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
            NumAppliedDays = intparlve
        Else
            NumAppliedDays = NumAppliedDays
        End If
            txtNoLeaves.Text = NumAppliedDays
            txtTotalLeave.Text = AvailableBalanceLeaves
            txtBalLeaves.Text = Val(txtTotalLeave.Text) - Val(txtNoLeaves.Text)

            If NumAppliedDays > AvailableBalanceLeaves Then
                lblMsg.Text = "Sorry! You Have Requested More than Available Leaves"
                Return 0
            End If
        Return 1
    End Function
    Private Function CompensatoryFunction()
        lblMsg.Text = ""
        ' btnSubmit.Enabled = False
        tr2.Visible = True
        Dim Compensatoryleaves As Decimal = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_COMP_NO_DAYS")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        'Dim ds As New DataSet
        'ds = sp.GetDataSet()

        'Compensatoryleaves = ds.Tables(0).Rows(0).Item("COMPOFF")

        Dim dr As SqlDataReader = sp.GetReader()
        Do While dr.Read
            If (dr.GetValue(0).Equals(System.DBNull.Value)) Then
                Compensatoryleaves = 0
            Else
                Compensatoryleaves = dr.GetValue(0)
            End If
        Loop
        dr.Close()
        Dim compLeaves As Decimal
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_COMP_LEAVES")
        sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim drObj As SqlDataReader = sp1.GetReader()
        Do While drObj.Read
            If (drObj.GetValue(0).Equals(System.DBNull.Value)) Then
                compLeaves = 0
            Else
                compLeaves = drObj.GetValue(0)
            End If
        Loop
        drObj.Close()
        AvailableBalanceLeaves = compLeaves - Compensatoryleaves
        NumAppliedDays = Holiday()
        intparlve = 0
        If CDate(txtToDate.Text) > CDate(txtFromDate.Text) Then
            If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
                If timefunction() = 1 Then
                    intparlve = NumAppliedDays + 0.5
                ElseIf timefunction() = 0 Then
                    intparlve = NumAppliedDays + 1
                Else
                    lblMsg.Text = "No Need To Apply Leave For Below 2 Hrs"
                    Return 0
                End If
            End If
        ElseIf CDate(txtFromDate.Text).Equals(CDate(txtToDate.Text)) Then
            If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
                If timefunction() = 1 Then
                    If NumAppliedDays = 0 Then
                        intparlve = 0
                    Else
                        intparlve = NumAppliedDays - 0.5
                    End If

                ElseIf timefunction() = 0 Then
                    If NumAppliedDays = 0 Then
                        intparlve = 0
                    Else
                        intparlve = NumAppliedDays
                    End If
                Else
                    lblMsg.Text = "No Need To Apply Leave For Below 2 Hrs"
                    Return 0
                End If
            End If
        End If
        If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
            NumAppliedDays = intparlve
        Else
            NumAppliedDays = NumAppliedDays
        End If
        txtNoLeaves.Text = NumAppliedDays
        txtTotalLeave.Text = AvailableBalanceLeaves
        txtBalLeaves.Text = Val(txtTotalLeave.Text) - Val(txtNoLeaves.Text)

        If NumAppliedDays > AvailableBalanceLeaves Then
            lblMsg.Text = "Sorry! You Have Requested More than Available Compensatory Leaves"
            Return 0
        End If
        Return 1
    End Function
    Private Function MaternityFunction()
        lblMsg.Text = ""
        ' btnSubmit.Enabled = False
        tr2.Visible = True

        txtTotalLeave.Text = 90
        Dim Maternityleaves As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMANTRA_USER_GET_GENDER")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim dr As SqlDataReader = sp.GetReader()
        If dr.Read Then
            If dr.GetValue(0) <> "FEMALE" Then
                dr.Close()
                lblMsg.Text = "Female Associates Can Only Apply For Maternity Leaves"
                Return 0
            End If
        End If
        dr.Close()
        NumAppliedDays = DateDiff(DateInterval.Day, CDate(txtFromDate.Text), CDate(txtToDate.Text)) + 1
        txtNoLeaves.Text = NumAppliedDays
        txtTotalLeave.Text = 90
        txtBalLeaves.Text = 0
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_REQ_MATERNITY")
        sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim drObj As SqlDataReader = sp1.GetReader()
        If drObj.Read Then
            If (drObj.GetValue(0).Equals(System.DBNull.Value)) Then
                Maternityleaves = 0
            Else
                Maternityleaves = drObj.GetValue(0)
                If Maternityleaves = 2 Then
                    lblMsg.Text = "Maternity Leaves Can Be Applied Only For 2 Times"
                    Return 0
                Else
                    If Val(txtNoLeaves.Text) > 90 Then
                        drObj.Close()
                        lblMsg.Text = "Maternity Leave will be Granted Only for 90 days"
                        Return 0
                    End If
                End If
            End If
        End If
        drObj.Close()
        Return 1
    End Function
    Private Function OnTravel()
        lblMsg.Text = ""
        'btnSubmit.Enabled = False
        tr2.Visible = True
        AvailableBalanceLeaves = balanceleave()
        NumAppliedDays = Holiday()
        intparlve = 0
        If CDate(txtToDate.Text) > CDate(txtFromDate.Text) Then
            If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
                If timefunction() = 1 Then
                    intparlve = NumAppliedDays + 0.5
                ElseIf timefunction() = 0 Then
                    intparlve = NumAppliedDays + 1
                Else
                    lblMsg.Text = "No Need To Apply Leave For Below 2 Hrs"
                    Return 0
                End If
            End If
        ElseIf CDate(txtFromDate.Text).Equals(CDate(txtToDate.Text)) Then
            If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
                If timefunction() = 1 Then
                    If NumAppliedDays = 0 Then
                        intparlve = 0
                    Else
                        intparlve = NumAppliedDays - 0.5
                    End If
                ElseIf timefunction() = 0 Then
                    If NumAppliedDays = 0 Then
                        intparlve = 0
                    Else
                        intparlve = NumAppliedDays
                    End If
                Else
                    lblMsg.Text = "No Need To Apply Leave For Below 2 Hrs"
                    Return 0
                End If
            End If
        End If
        If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
            NumAppliedDays = intparlve
        Else
            NumAppliedDays = NumAppliedDays
        End If
        txtNoLeaves.Text = NumAppliedDays
        txtTotalLeave.Text = AvailableBalanceLeaves
        txtBalLeaves.Text = AvailableBalanceLeaves
        Return 1
    End Function
    Private Function PaternityFunction()
        lblMsg.Text = ""
        'btnSubmit.Enabled = False
        tr2.Visible = True
        NumAppliedDays = Holiday()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMANTRA_USER_GET_GENDER")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim dr As SqlDataReader = sp.GetReader()
        If dr.Read Then
            If dr.GetValue(0) <> "MALE" Then
                dr.Close()
                lblMsg.Text = "Male Associates Can Only Apply For Paternity Leaves"
                Return 0
            End If
        End If
        dr.Close()
        NumAppliedDays = NumAppliedDays
        txtNoLeaves.Text = NumAppliedDays
        txtTotalLeave.Text = 3
        txtBalLeaves.Text = 0
        Dim Paternityleaves
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_REQ_PATERNITY")
        sp1.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim drObj As SqlDataReader = sp1.GetReader()
        If drObj.Read Then
            If (drObj.GetValue(0).Equals(System.DBNull.Value)) Then
                Paternityleaves = 0
            Else
                Paternityleaves = drObj.GetValue(0)
                If Paternityleaves = 2 Then
                    lblMsg.Text = "Paternity Leaves Can Be Applied Only For 2 Times"
                    Return 0
                Else
                    If NumAppliedDays > 3 Then
                        drObj.Close()
                        lblMsg.Text = "Paternity Leave will be Granted Only for 3 days"
                        Return 0
                    End If
                End If
            End If
        End If
        drObj.Close()
        Return 1
    End Function
    Private Function LossOfPay()
        lblMsg.Text = ""
        lblMsg.Visible = True
        'btnSubmit.Enabled = False
        tr2.Visible = True
        AvailableBalanceLeaves = balanceleave()
        NumAppliedDays = Holiday()
        intparlve = 0
        If CDate(txtToDate.Text) > CDate(txtFromDate.Text) Then
            If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
                If timefunction() = 1 Then
                    intparlve = NumAppliedDays + 0.5
                ElseIf timefunction() = 0 Then
                    intparlve = NumAppliedDays + 1
                Else
                    lblMsg.Text = "No Need To Apply Leave For Below 2 Hrs"
                    Return 0
                End If
            End If
        ElseIf CDate(txtFromDate.Text).Equals(CDate(txtToDate.Text)) Then
            If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
                If timefunction() = 1 Then
                    If NumAppliedDays = 0 Then
                        intparlve = 0
                    Else
                        intparlve = NumAppliedDays - 0.5
                    End If

                ElseIf timefunction() = 0 Then
                    If NumAppliedDays = 0 Then
                        intparlve = 0
                    Else
                        intparlve = NumAppliedDays
                    End If
                Else
                    lblMsg.Text = "No Need To Apply Leave For Below 2 Hrs"
                    Return 0
                End If
            End If
        End If
        If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
            NumAppliedDays = intparlve
        Else
            NumAppliedDays = NumAppliedDays
        End If
        txtNoLeaves.Text = NumAppliedDays
        txtTotalLeave.Text = AvailableBalanceLeaves
        txtBalLeaves.Text = AvailableBalanceLeaves
        Return 1
    End Function
    Private Function balanceleave()
        Dim leavedays As Double
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVES_REQ_BINDLEAVES")
        sp.Command.AddParameter("@DEP_CODE", ddlDep.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@EMP_CODE", txtAssociateID.Text, DbType.String)
        sp.Command.AddParameter("@LEAVE_TYPE", ddlLeaveType.SelectedItem.Value, DbType.String)
        leavedays = sp.ExecuteScalar()
        Return leavedays
    End Function
    Private Function Holiday() As Integer
        Dim i As Integer
        'Dim n As Integer
        Dim v As Date
        cnt = 0
        Dim j As Integer
        Dim H As Date        
        j = DateDiff(DateInterval.Day, CDate(txtFromDate.Text), CDate(txtToDate.Text)) + 1
        v = CDate(txtFromDate.Text)
        For i = 1 To j
            If Weekday(v) <> 1 Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_HOLIDAY_shydate")
                sp.Command.AddParameter("@dummy", 1, DbType.Int32)
                Dim drTime As SqlDataReader = sp.GetReader()
                While drTime.Read
                    H = drTime("SHY_DATE")
                    If v = H Then
                        cnt = cnt - 1
                    End If
                End While
                drTime.Close()
                cnt = cnt + 1
            End If
            v = v.AddDays(1)
        Next i
        Return cnt
    End Function
    Private Function timefunction() As Integer
        Dim val1 As Decimal = CDbl(cboHr.SelectedItem.Text) * 60
        Dim min1 As Decimal = CDbl(val1 + cboMin.SelectedItem.Text)
        Dim val2 As Decimal = CDbl(ddlHH.SelectedItem.Text) * 60
        Dim min2 As Decimal = CDbl(val2 + ddlMM.SelectedItem.Text)
        If (min1 - min2) > 240 Or (min2 - min1) > 240 Then
            Return 0
        ElseIf (((min1 - min2) > 120 Or (min2 - min1) > 120) And ((min1 - min2) < 240 Or (min2 - min1) < 240)) Then
            Return 1
        Else
            Return 2
        End If
    End Function
    Protected Sub ddlLeaveType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLeaveType.SelectedIndexChanged
        tr2.Visible = False
        btnSubmit.Visible = False
        lblMsg.Text = "Please Click On the Leave(s) Applied Button"
        lblMsg.Visible = True


    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
           
            Dim REQID As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") + "/" + txtAssociateID.Text
            Dim Req As Integer
            Req = ValidateReq(REQID)
            If Req = 0 Then
                lblMsg.Text = "Your Request Has Been Submitted Already"
                lblMsg.Visible = True
            Else
                INSERT_LEAVE_REQ(REQID)
                mail_LMSReq(REQID)
                Response.Redirect("status.aspx?StaId=1&ReqId=" + REQID)
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub mail_LMSReq(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strMsgStr As String                            'To hold the sql query
        Dim strReqId As String                             'To hold the Requisition ID
        Dim strReqDate As String                           'To hold the requisition date
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strLeaveType As String                          'To hold the Leave Type
        Dim strHrEmail As String                              'To hold the HR Email
        Dim noofdays, fromdt, todt, Contact, reason As String
        strSubj = " Leave Requisition."
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_MAIL_REQUEST")
        sp.Command.AddParameter("@LMS_LEAVE_REQ", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strReqId = ds.Tables(0).Rows(0).Item("LMS_REQ_ID")
        strReqDate = ds.Tables(0).Rows(0).Item("LMS_REQ_DATE")
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        noofdays = ds.Tables(0).Rows(0).Item("LMS_NO_DAYS")
        fromdt = ds.Tables(0).Rows(0).Item("LMS_FDATE")
        todt = ds.Tables(0).Rows(0).Item("LMS_TDATE")
        Contact = ds.Tables(0).Rows(0).Item("LMS_CNT_NO")
        reason = ds.Tables(0).Rows(0).Item("LMS_LEAVE_REASON")
        strLeaveType = ds.Tables(0).Rows(0).Item("LMS_LEAVE_TYPE")
        Dim strfrommail, strfromname As String
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USER_MAIL")
        sp1.Command.AddParameter("@REQID", REQID, DbType.String)
        Dim ObjDR As SqlDataReader
        ObjDR = sp.GetReader()
        While ObjDR.Read
            strfrommail = "amantraadmin@satnavtech.com"
            strfromname = "Employee Service Desk"
        End While
        ObjDR.Close()
        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "Employee Service Desk"
        strSubj = " Leave Request - by " & strEmpName & " from " & fromdt & " - " & todt & " for " & noofdays & " days."
        'Mail message for the Requisitioner. 
        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform you that following are the  Details of your Leave Request." & "<br>" & _
"<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
"<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
"<td width=50% align=left>" & strEmpName & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
"<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>From " & fromdt & " to " & todt & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Reason: </U></td>" & _
"<td width=50% align=left>" & reason & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Contact No: </U></td>" & _
"<td width=50% align=left>" & Contact & "</td></tr>" & _
"<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
"Thanking You, " & "<br><br>" & _
"Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL")
        sp2.Command.AddParameter("@REQID", REQID, DbType.String)
        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                    "This is to inform you that following request by " & strEmpName & " for Leave is pending for your Approval." & "<br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & strEmpName & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
                    "<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>From " & fromdt & " to " & todt & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Reason: </U></td>" & _
"<td width=50% align=left>" & reason & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Type Of Leave: </U></td>" & _
"<td width=50% align=left>" & strLeaveType & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Contact No: </U></td>" & _
"<td width=50% align=left>" & Contact & "</td></tr>" & _
"<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
"Click here to <a href=""http://projects.a-mantra.com/AmantraAMPS/EFM/EFM_Webfiles/frmLveRMApproval.aspx?ReqID=" & REQID & """>Accept / Reject</a><br><br>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strEmpName & "." & "<br>"
        'Response.Write(strMsgAdm)
        'objCom.Dispose()
        'Mail  for the Requisitioner. 
        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, strReqId, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, strReqId, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal MailFrom As String, ByVal Id As String, ByVal subject As String, ByVal msg As String, ByVal strHrEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_MAIL_ADD")
        sp.Command.AddParameter("@AMT_ID", Id, DbType.String)
        sp.Command.AddParameter("@AMT_MESSAGE", msg, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_TO", MailTo, DbType.String)
        sp.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
        sp.Command.AddParameter("@AMT_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@AMT_FROM", MailFrom, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_CC", strHrEmail, DbType.String)
        sp.Command.AddParameter("@AMT_BDYFRMT", 0, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Private Function CheckLeaveReq()
        Dim ValidateLeaveReq As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_REQ_VALIDATE")
        sp.Command.AddParameter("@LMS_AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@LMS_FDATE", txtFromDate.Text, DbType.DateTime)
        sp.Command.AddParameter("@LMS_TDATE", txtToDate.Text, DbType.DateTime)
        ValidateLeaveReq = sp.ExecuteScalar()
        Return ValidateLeaveReq
    End Function
    Private Function ValidateReq(ByVal REQID As String)
        Dim Req As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_VALIDATE_REQID")
        sp.Command.AddParameter("@REQID", REQID, DbType.String)
        Req = sp.ExecuteScalar()
        Return Req
    End Function
    Private Sub INSERT_LEAVE_REQ(ByVal REQID As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_REQ_ADD")
            sp.Command.AddParameter("@LMS_REQ_ID", REQID, DbType.String)
            sp.Command.AddParameter("@LMS_AUR_NO", txtAssociateID.Text, DbType.String)
            sp.Command.AddParameter("@LMS_AUR_ID", txtAssociateID.Text, DbType.String)
            sp.Command.AddParameter("@LMS_RM_ID", ddlRM.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LMS_FDATE", txtFromDate.Text, DbType.DateTime)
            sp.Command.AddParameter("@LMS_TDATE", txtToDate.Text, DbType.DateTime)
            sp.Command.AddParameter("@LMS_LEAVE_TYPE", ddlLeaveType.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LMS_CNT_ADDRESS", txtAddress.Text, DbType.String)
            sp.Command.AddParameter("@LMS_LEAVE_REASON", txtReason.Text, DbType.String)
            sp.Command.AddParameter("@LMS_NO_DAYS", txtNoLeaves.Text, DbType.Decimal)
            sp.Command.AddParameter("@LMS_BAL_LEAVES", txtBalLeaves.Text, DbType.Decimal)
            sp.Command.AddParameter("@LMS_LEAVE_ID", txtContactNo.Text, DbType.String)
            If cboHr.SelectedIndex > 0 And cboMin.SelectedIndex > 0 And ddlHH.SelectedIndex > 0 And ddlMM.SelectedIndex > 0 Then
                'Dim tim1 As String = (cboHr.SelectedItem.Value) + ":" + (cboMin.SelectedItem.Value)
                'Dim tim2 As String = (ddlHH.SelectedItem.Value) + ":" + (ddlMM.SelectedItem.Value)
                sp.Command.AddParameter("@LMS_FTIMEP", cboHr.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@LMS_FTIMEM", cboMin.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@LMS_TTIMEP", ddlHH.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@LMS_TTIMEM", ddlMM.SelectedItem.Value, DbType.String)
            Else
                sp.Command.AddParameter("@LMS_FTIMEP", 0, DbType.String)
                sp.Command.AddParameter("@LMS_FTIMEM", 0, DbType.String)
                sp.Command.AddParameter("@LMS_TTIMEP", 0, DbType.String)
                sp.Command.AddParameter("@LMS_TTIMEM", 0, DbType.String)
            End If
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub rdbtnListLeave_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbtnListLeave.SelectedIndexChanged
        If rdbtnListLeave.SelectedItem.Value = "0" Then
            tr1.Visible = False
           
        Else
            tr1.Visible = True
           
        End If
        cboHr.SelectedIndex = -1
        cboMin.SelectedIndex = -1
        ddlHH.SelectedIndex = -1
        ddlMM.SelectedIndex = -1
    End Sub
End Class
