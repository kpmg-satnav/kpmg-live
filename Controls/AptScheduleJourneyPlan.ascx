﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AptScheduleJourneyPlan.ascx.vb" Inherits="Controls_AptScheduleJourneyPlan" %>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Request Id<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtreqid" TabIndex="1" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Request Name<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtreqname"
                    Display="None" ErrorMessage="Please Enter Request Name " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtreqname" TabIndex="2" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate Name<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateName" TabIndex="3" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Department<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlDep" TabIndex="4" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate ID<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateID" TabIndex="5" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Reporting Manager<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlRM" TabIndex="6" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                    <%--<asp:TextBox ID="txtRM" runat="server" CssClass="form-control"></asp:TextBox>--%>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Designation<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDesig" TabIndex="7" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Origin<span style="color: red;">*</span></label>

                <asp:RequiredFieldValidator ID="rfvorigin" runat="server" ControlToValidate="txtOrigin"
                    Display="None" ErrorMessage="Please Enter Origin " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtOrigin" TabIndex="8" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Destination<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDestinantion"
                    Display="None" ErrorMessage="Please Enter Destination " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDestinantion" TabIndex="9" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Start Date<span style="color: red;">*</span></label>

                <asp:RequiredFieldValidator ID="rfvfromdate" runat="server" ControlToValidate="txtstartdate"
                    Display="None" ErrorMessage="Please Pick Start Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Click On the TextBox  to Pick the date')" onmouseout="UnTip()">
                        <div class='input-group date' id='fromdate'>
                            <asp:TextBox ID="txtstartdate" TabIndex="10" runat="server" CssClass="form-control"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">End date<span style="color: red;">*</span></label>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtenddate"
                    Display="None" ErrorMessage="Please Pick End Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Click On the TextBox  to Pick the date')" onmouseout="UnTip()">
                        <div class='input-group date' id='todate'>
                            <asp:TextBox ID="txtenddate" TabIndex="11" runat="server" CssClass="form-control"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('todate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Preferred Mode Of Travel<span style="color: red;">*</span></label>

                <asp:RequiredFieldValidator ID="rfvmodeoftravel" runat="server" ValidationGroup="Val1"
                    ErrorMessage="Please Select Mode Of Travel" Display="None" ControlToValidate="ddlmodeoftravel"
                    InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlmodeoftravel" TabIndex="12" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                        <asp:ListItem Value="FLT">Flight</asp:ListItem>
                        <asp:ListItem Value="TRN">Train</asp:ListItem>
                        <asp:ListItem Value="BS">Bus</asp:ListItem>
                        <asp:ListItem Value="CR">Car</asp:ListItem>
                        <asp:ListItem Value="ATO">Auto</asp:ListItem>
                        <asp:ListItem Value="BKE">Bike</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Time<span style="color: red;">*</span></label>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Val1"
                    ErrorMessage="Please select time!" Display="None" ControlToValidate="ddltime"
                    InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddltime" TabIndex="13" runat="server" CssClass="selectpicker" data-live-search="true">
                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                        <asp:ListItem Value="6AM-12PM">6AM-12PM</asp:ListItem>
                        <asp:ListItem Value="12PM-6PM">12PM-6PM</asp:ListItem>
                        <asp:ListItem Value="6PM-6AM">6PM-6AM</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    <asp:CheckBox ID="chktravelassistance" runat="server" />
                    Need Travel Assistance <span style="color: red;">*</span></label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">


            <asp:Button ID="btnAdd" runat="server" TabIndex="14"  Text="Add" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                CausesValidation="true" Visible="true" />

            <asp:Button ID="btnModify" runat="server" Text="Modify" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                CausesValidation="true" Visible="false" />
            <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="btn btn-primary custom-button-color"
                CausesValidation="true" Visible="false" />
            <asp:TextBox ID="txtsno" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>

        </div>
    </div>
</div>
<div class="row">
   <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvItems" runat="server" EmptyDataText="No Schedule Journey Plan Found."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Start Date">
                        <ItemTemplate>
                            <asp:Label ID="lblstartdate" runat="server" Text='<%#Eval("STARTDATE")%>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="End Date">
                        <ItemTemplate>
                            <asp:Label ID="lblenddate" runat="server" Text='<%#Eval("ENDDATE")%>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="From">
                        <ItemTemplate>
                            <asp:Label ID="lblFrom" runat="server" Text='<%#Eval("ORIGIN")%>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To">
                        <ItemTemplate>
                            <asp:Label ID="lblTo" runat="server" Text='<%#Eval("DESTINATION")%>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblMode" runat="server" Text='<%#Eval("MODEOFTRAVEL")%>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Time">
                        <ItemTemplate>
                            <asp:Label ID="lbltime" runat="server" Text='<%#Eval("TIME")%>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Travel Assistance">
                        <ItemTemplate>
                            <asp:Label ID="lbltravelassistance" runat="server" Text='<%#Eval("TRAVELASSISTANCE")%>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField Text="Edit" CommandName="EDIT" />
                    <asp:ButtonField Text="Delete" CommandName="DELETE" />
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">           

                <asp:Button ID="btnsubmit" runat="server" Text="Schedule Trip" CssClass="btn btn-primary custom-button-color"
                    CausesValidation="false" Visible="false" />           
        </div>
    </div>
</div>


