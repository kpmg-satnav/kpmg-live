Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_EditUserProfile
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim obj As New clsMasters


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
      
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
            txtDOB.Attributes.Add("readonly", "readonly")
            txtDOJ.Attributes.Add("readonly", "readonly")
        Else
            If Not IsPostBack Then
                Dim UID As String = Session("uid")
                If Session("uid") = "" Then
                    Response.Redirect(Application("FMGLogout"))
                Else
                    obj.BindCountry(ddlcountry)
                    'Get_Shift()
                    GetUserDetails()
                End If
            End If
        End If
    End Sub

    Public Sub BindCountry(ByVal ddl As DropDownList)
        strSQL = "usp_getActiveCountries"
        BindCombo(strSQL, ddl, "CNY_NAME", "CNY_CODE")
        ddl.SelectedIndex = 0
    End Sub

    Protected Sub ddlcountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcountry.SelectedIndexChanged
        If ddlcountry.SelectedIndex > 0 Then
            BindTimeZones(ddlcountry.SelectedItem.Value)
        End If
    End Sub

    Private Sub BindTimeZones(country As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TIME_ZONES_BYCTYID")
        sp.Command.AddParameter("@CNY_CODE", ddlcountry.SelectedItem.Value, DbType.String)
        ddlTimeZone.DataSource = sp.GetDataSet()
        ddlTimeZone.DataTextField = "TIME_ZONE_NAME"
        ddlTimeZone.DataValueField = "TIME_ZONE"
        ddlTimeZone.DataBind()
        ddlTimeZone.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub GetUserDetails()
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("uid")
        ds = ObjSubsonic.GetSubSonicDataSet("GET_AMANTRA_USERDETAILS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            txtEmpID.Text = ds.Tables(0).Rows(0).Item("AUR_ID")
            ' txtITID.Text = ds.Tables(0).Rows(0).Item("AD_ID")
            txtFrstName.Text = ds.Tables(0).Rows(0).Item("AUR_FIRST_NAME")
            'txtMdlName.Text = ds.Tables(0).Rows(0).Item("AUR_MIDDLE_NAME")
            'txtLstName.Text = ds.Tables(0).Rows(0).Item("AUR_LAST_NAME")
            txtEmail.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
            txtRes.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
            txtDOB.Text = ds.Tables(0).Rows(0).Item("AUR_DOB")
            txtDOJ.Text = ds.Tables(0).Rows(0).Item("AUR_DOJ")
            'Reporting Manager 
            GetReportingManager(ds.Tables(0).Rows(0).Item("AUR_REPORTING_TO"))

            txtExtn.Text = ds.Tables(0).Rows(0).Item("AUR_EXTENSION")
            'Department
            GetEmployeeDepartment(ds.Tables(0).Rows(0).Item("AUR_DEP_ID"))
            GetEmployeeDesignation(ds.Tables(0).Rows(0).Item("AUR_DESGN_ID"))
            Dim li As ListItem = Nothing
            li = ddlcountry.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_COUNTRY"))
            If Not li Is Nothing Then
                ddlcountry.SelectedIndex = -1
                li.Selected = True
                BindTimeZones(ds.Tables(0).Rows(0).Item("AUR_COUNTRY"))
                Dim li2 As ListItem = Nothing
                li2 = ddlTimeZone.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_TIME_ZONE"))
                If Not li2 Is Nothing Then
                    ddlTimeZone.SelectedIndex = -1
                    li2.Selected = True
                End If
            End If
            'Status
            GetStatus(ds.Tables(0).Rows(0).Item("AUR_STA_ID"))

            If ds.Tables(1).Rows.Count > 0 Then
                txtFthrName.Text = ds.Tables(1).Rows(0).Item("AUR_FATHERS_NAME")
                txtBldGrp.Text = ds.Tables(1).Rows(0).Item("AUR_BLOOD_GROUP")
                txtDOA.Text = ds.Tables(1).Rows(0).Item("AUR_ANNIVERSARY_DATE")
                txtAddr.Text = ds.Tables(1).Rows(0).Item("AUR_ADDRESS")
                'Shift
                txtExp.Text = ds.Tables(1).Rows(0).Item("AUR_EXP")
                txtSkills.Text = ds.Tables(1).Rows(0).Item("AUR_SKILLS")
                txtQlfctn.Text = ds.Tables(1).Rows(0).Item("AUR_QUALIFICATION")
                txtPsptNo.Text = ds.Tables(1).Rows(0).Item("AUR_PASSPORT_NO")
                'txtPanNo.Text = ds.Tables(1).Rows(0).Item("AUR_PAN_CARD_NUMBER")
                txtInsNo.Text = ds.Tables(1).Rows(0).Item("AUR_INSURANCE_NUMBER")
                txtEmerNo.Text = ds.Tables(1).Rows(0).Item("AUR_EMERGENCY_CONTACT_NO")
                Dim li3 As ListItem = Nothing
                li3 = ddlShift.Items.FindByValue(ds.Tables(1).Rows(0).Item("AUR_SHIFT"))
                If Not li3 Is Nothing Then
                    li3.Selected = True
                End If
            End If

        End If
    End Sub


    Private Sub GetReportingManager(ByVal ReportMangerAurID As String)


        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_REPORTING_MANAGER")
        'sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        'ddlRept.DataSource = sp.GetDataSet()
        'ddlRept.DataTextField = "AUR_ID"
        'ddlRept.DataValueField = "AUR_ID"
        'ddlRept.DataBind()

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.Int)
        param(0).Value = 1
        ObjSubsonic.Binddropdown(ddlRept, "BIND_REPORTING_MANAGER", "AUR_KNOWN_AS", "AUR_ID", param)
        Try
            If ReportMangerAurID = "" Then
            Else
                ddlRept.Items.FindByValue(ReportMangerAurID).Selected = True
            End If
        Catch ex As Exception

        End Try

        'Dim param(0) As SqlParameter
        'param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        'param(0).Value = Session("uid")
        'ObjSubsonic.Binddropdown(ddlRept, "AST_GET_USERS", "AUR_KNOWN_AS", "AUR_ID", param)
        'Try
        '    If ReportMangerAurID = "" Then
        '    Else
        '        ddlRept.Items.FindByValue(ReportMangerAurID).Selected = True
        '    End If
        'Catch ex As Exception

        'End Try

    End Sub

    Private Sub GetEmployeeDepartment(ByVal DeptId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.Int)
        param(0).Value = 1
        ObjSubsonic.Binddropdown(ddlDept, "AST_GET_DEPT", "DEP_NAME", "DEP_CODE", param)
        Try
            If DeptId = "" Then
            Else
                ddlDept.Items.FindByValue(DeptId).Selected = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetEmployeeDesignation(ByVal DesgId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.Int)
        param(0).Value = 1
        ObjSubsonic.Binddropdown(ddldesn, "GET_DESIGNATION", "DSN_AMT_TITLE", "DSN_CODE", param)
        Try
            If DesgId = "" Then
            Else
                ddldesn.Items.FindByValue(DesgId).Selected = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetStatus(ByVal AurId As Integer)
        If AurId = "2" Then
            ddlStatus.Items.FindByValue("1").Selected = True
        Else
            ddlStatus.Items.FindByValue(AurId).Selected = True
        End If
    End Sub


    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        modifydata()

    End Sub
    Private Sub Get_Shift()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Get_Shift")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlShift.DataSource = sp.GetDataSet()
        ddlShift.DataValueField = "SHIFT_ID"
        ddlShift.DataTextField = "SHIFT_NAME"
        'ddlShift.DataValueField = "SHIFT_DEP"
        'ddlShift.DataValueField = "SHIFT_STA_ID"
        ddlShift.DataBind()
        ddlShift.Items.Insert(0, "--Select Shift--")
    End Sub
    Private Sub modifydata()

        If (txtDOB.Text) <> "" Then
            If (txtDOJ.Text) <> "" Then
                If CDate(txtDOB.Text) > CDate(txtDOJ.Text) Then
                    lblMsg.Text = "Date of Birth Date Must be older than Joining Date."
                    lblMsg.Visible = True
                    Exit Sub
                End If
            End If
        End If

        'Modifying the details of user in AMANTRA_USER based on user login
        Dim mdydob As String = CDate(txtDOB.Text).ToString("MM/dd/yyyy")
        Dim mdydoj As String = CDate(txtDOJ.Text).ToString("MM/dd/yyyy")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_USERPROF_MODIFY")
        sp.Command.AddParameter("@id", Session("Uid"), DbType.String)
        sp.Command.AddParameter("@AUR_FIRST_NAME", txtFrstName.Text, DbType.String)
        sp.Command.AddParameter("@AUR_MIDDLE_NAME", "", DbType.String)
        sp.Command.AddParameter("@AUR_LAST_NAME", "", DbType.String)
        sp.Command.AddParameter("@AUR_EMAIL", txtEmail.Text, DbType.String)
        'commented  by praveen 18 mar 2014 and modified in the procedure
        sp.Command.AddParameter("@AD_ID", Session("Uid"), DbType.String)
        sp.Command.AddParameter("@AUR_EXTENSION", txtExtn.Text, DbType.String)
        sp.Command.AddParameter("@AUR_DEP_ID", ddlDept.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_DESG_ID", ddldesn.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
        sp.Command.AddParameter("@AUR_RES_NUMBER", txtRes.Text, DbType.String)
        sp.Command.AddParameter("@AUR_DOB", mdydob, DbType.Date)
        sp.Command.AddParameter("@AUR_DOJ", mdydoj, DbType.Date)
        sp.Command.AddParameter("@AUR_REPORTING_TO", ddlRept.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_COUNTRY", ddlcountry.SelectedValue, DbType.String)
        sp.Command.AddParameter("@AUR_TIME_ZONE", ddlTimeZone.SelectedValue, DbType.String)
        sp.ExecuteScalar()
        Dim mdydoa As String


        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_USER_PER_DET_MDFY")
        sp1.Command.AddParameter("@id", Session("Uid"), DbType.String)
        sp1.Command.AddParameter("@AUR_FATHERS_NAME", txtFthrName.Text, DbType.String)
        sp1.Command.AddParameter("@AUR_BLOOD_GROUP", txtBldGrp.Text, DbType.String)
        If txtDOA.Text = "" Then
            sp1.Command.AddParameter("@AUR_ANNIVERSARY_DATE", DBNull.Value, DbType.Date)
        Else
            mdydoa = CDate(txtDOA.Text).ToString("MM/dd/yyyy")
            sp1.Command.AddParameter("@AUR_ANNIVERSARY_DATE", mdydoa, DbType.Date)
        End If


        sp1.Command.AddParameter("@AUR_ADDRESS", txtAddr.Text, DbType.String)
        sp1.Command.AddParameter("@AUR_EXP", txtExp.Text, DbType.String)
        sp1.Command.AddParameter("@AUR_SKILLS", txtSkills.Text, DbType.String)
        sp1.Command.AddParameter("@AUR_QUALIFICATION", txtQlfctn.Text, DbType.String)
        sp1.Command.AddParameter("@AUR_PASSPORT_NO", txtPsptNo.Text, DbType.String)
        sp1.Command.AddParameter("@AUR_PAN_CARD_NUMBER", "", DbType.String)
        sp1.Command.AddParameter("@AUR_INSURANCE_NUMBER", txtInsNo.Text, DbType.String)
        sp1.Command.AddParameter("@AUR_EMERGENCY_CONTACT_NO", txtEmerNo.Text, DbType.String)
        sp1.Command.AddParameter("@AUR_SHIFT", ddlShift.SelectedItem.Value, DbType.Int32)
        sp1.ExecuteScalar()

        getuseroffsetandculture()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=500")

    End Sub

    Public Sub getuseroffsetandculture()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_OFFSET_CULTURE_BYID")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("useroffset") = ds.Tables(0).Rows(0).Item("AUR_TIME_ZONE")
            Session("userculture") = ds.Tables(0).Rows(0).Item("AUR_CULTURE")
            Dim userculturecookie As HttpCookie = New HttpCookie("userculture")
            userculturecookie.Value = Convert.ToString(ds.Tables(0).Rows(0).Item("AUR_CULTURE"))
            Response.Cookies.Add(userculturecookie)
        End If
    End Sub
End Class
