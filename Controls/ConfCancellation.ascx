<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ConfCancellation.ascx.vb" Inherits="Controls_ConfCancellation" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Employee <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvType" runat="server" ControlToValidate="ddlEmployee"
                    Display="None" ErrorMessage="Please Select Employee" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">

            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"
                CausesValidation="True" />

        </div>
    </div>
</div>
<div id="tablegrid1" runat="server">

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">

                <asp:Button ID="btnExportExcel" runat="server" Text="Export to Excel" CssClass="btn btn-primary custom-button-color" OnClick="btnExportExcel_Click" />

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <cc1:ExportPanel ID="ExportPanel" runat="server">
                <asp:GridView ID="gvItems" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="True" AutoGenerateColumns="false"
                    EmptyDataText="No Employee Details Found">
                    <Columns>
                        <asp:TemplateField HeaderText="Associate">
                            <ItemTemplate>
                                <asp:Label ID="lblAssociate" runat="Server" Text='<%#Eval("EMP_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:Label ID="lbldate" runat="server" Text='<%#Eval("EVENTDATE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Time">
                            <ItemTemplate>
                                <asp:Label ID="lblTime" runat="server" Text='<%#Eval("EVENTTIME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblstat" runat="server" Text='<%#Eval("STAT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </cc1:ExportPanel>
        </div>
    </div>
</div>
