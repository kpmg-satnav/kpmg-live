<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddAssetSubCategory.ascx.vb" Inherits="Controls_AddAssetSubCategory" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-10" ForeColor="Red">
                </asp:Label>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <div class="Radio-btn-s">
            <label class="btn">
                <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                    ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                Add</label>
            <label class="btn">
                <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                    ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                Modify
            </label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label id="lblAssetBrand" runat="server" visible="False">Asset Sub Category<span style="color: red;">*</span></label>
            <%--    <asp:CompareValidator ID="cvassetsub" runat="server" Display="None" ControlToValidate="ddlBrand"
                ErrorMessage="Please Select The Asset Sub Category" InitialValue="--Select--" Operator="NotEqual"></asp:CompareValidator>--%>
            <%--          <asp:RequiredFieldValidator ID="rfvsubcat" runat="server" ControlToValidate="ddlBrand"
                Display="none" InitialValue="--Select--" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>

            <asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                ToolTip="Select Asset Sub Category" AutoPostBack="True" Visible="False">
            </asp:DropDownList>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Sub Category Code<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtBrand"
                Display="none" ErrorMessage="Please Enter Asset Sub Category Code " ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegExpCode" runat="server" ControlToValidate="txtBrand"
                ErrorMessage="Please Enter Valid Asset Sub Category Code" Display="None" ValidationGroup="Val1">
            </asp:RegularExpressionValidator>

            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlBrand" Display="None" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Sub Category Name<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBrandName"
                Display="none" ErrorMessage="Please Enter Asset Sub Category Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>

            <asp:RegularExpressionValidator ID="RegExpName" runat="server" ControlToValidate="txtBrandName"
                ErrorMessage="Please Enter Valid Asset Sub Category Name" Display="None" ValidationGroup="Val1">
            </asp:RegularExpressionValidator>

            <div onmouseover="Tip('Enter name in alphabets,numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtBrandName" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Category<span style="color: red;">*</span></label>
            <asp:RequiredFieldValidator ID="rfvassetcat" runat="server" ControlToValidate="ddlAssetCat"
                Display="none" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

            <asp:DropDownList ID="ddlAssetCat" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="Select Asset Category" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Status</label>
            <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="Select Status">
                <%--<asp:ListItem>--Select--</asp:ListItem>--%>
                <asp:ListItem Value="1">Active</asp:ListItem>
                <asp:ListItem Value="0">InActive</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

</div>

<div class="row ">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Life Span</label>
            <div onmouseover="Tip('Numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtLife" runat="server" CssClass="form-control" MaxLength="3" onkeypress='return isNumber(event)'></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Remarks</label>
            <asp:RegularExpressionValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtBrandName"
                ErrorMessage="Please Enter Valid Remarks" Display="None" ValidationGroup="Val1">
            </asp:RegularExpressionValidator>
            <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                <asp:TextBox ID="txtRemarks" runat="server" Height="30%" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-default btn-primary" runat="server" Text="Add" ValidationGroup="Val1" />
            <asp:Button ID="btnBack" CssClass="btn btn-default btn-primary" runat="server" Text="Back" OnClick="btnBack_Click" />
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <asp:GridView ID="gvCat" runat="server" AllowPaging="True" AllowSorting="False" RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Asset Sub Category Found." CssClass="table table-bordered table-hover table-striped" PagerStyle-HorizontalAlign="Center">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("AST_SUBCAT_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sub Category Code">
                        <ItemTemplate>
                            <asp:Label ID="lblCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("AST_SUBCAT_CODE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Sub Category Name">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#BIND("AST_SUBCAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Category Name">
                        <ItemTemplate>
                            <asp:Label ID="lblType" runat="server" CssClass="lblStatus" Text='<%#BIND("VT_TYPE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#BIND("AST_SUBCATSTA_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle ForeColor="Black" BackColor="#d3d3d3" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<script type="text/ecmascript">
    function refreshSelectpicker() {
        $("#<%=ddlAssetCat.ClientID()%>").selectpicker();
        $("#<%=ddlBrand.ClientID()%>").selectpicker();
        $("#<%=ddlStatus.ClientID%>").selectpicker();
    }
    refreshSelectpicker();



    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>
