﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic

Partial Class Controls_LocationSerivceEmpMapping
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lblMsg.Text = ""

        If Not IsPostBack Then
            ' bindrequests()
            bindcity()
            fillgrid()
            gvSer.Visible = True
            btnSubmit.Text = "Add"

        End If
    End Sub

    Public Sub bindrequests(lcm_code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GETACTIVESERREQUESTTYPE")
        sp.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
        ddlServiceCat.DataSource = sp.GetDataSet()
        ddlServiceCat.DataTextField = "SER_NAME"
        ddlServiceCat.DataValueField = "SER_CODE"
        ddlServiceCat.DataBind()
        ddlServiceCat.Items.Insert(0, "--Select--")
    End Sub

    Public Sub bindcity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_Location_GetAll")
        ' sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, "--Select--")
    End Sub

    'Protected Sub rbActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActions.SelectedIndexChanged
    '    Dim iLoop As Integer
    '    For iLoop = 0 To rbActions.Items.Count - 1
    '        If rbActions.Items(iLoop).Selected = True Then
    '            If rbActions.Items(iLoop).Text = "Add" Then
    '                'ddlBrand.Visible = False
    '                'lblAssetBrand.Visible = False
    '                cleardata()
    '                btnSubmit.Text = "Add"
    '                lblMsg.Visible = False
    '                txtEmpId.Enabled = True
    '                fillgrid()
    '            ElseIf rbActions.Items(iLoop).Text = "Modify" Then
    '                'ddlLocation.Visible = True
    '                'lblAssetBrand.Visible = True
    '                cleardata()
    '                btnSubmit.Text = "Modify"
    '                lblMsg.Visible = False

    '                ' getassetbrand()
    '                fillgrid()
    '            End If
    '        End If
    '    Next
    'End Sub

    Private Sub cleardata()
        txtEmpId.Text = ""
        txtSecEmpId.Text = ""
        txtTeritaryEmpId.Text = ""
        ddlLocation.SelectedIndex = 0
        ddlServiceCat.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If btnSubmit.Text = "Add" Then
            Dim validatecode As String = validateempid(txtEmpId.Text)
            Dim validatecode2 As String = validateempid(txtSecEmpId.Text)
            Dim validatecode3 As String = validateempid(txtTeritaryEmpId.Text)

            If validatecode = "0" Then
                If validatecode2 = "0" Then
                    If validatecode3 = "0" Then

                        insertnewrecord()

                    Else
                        lblMsg.Visible = True
                        lblMsg.Text = "Employee Id " + txtTeritaryEmpId.Text + "  not existed...Please enter valid employee id"
                        txtTeritaryEmpId.Text = ""
                        txtTeritaryEmpId.Focus()
                        Exit Sub
                    End If
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Employee Id " + txtSecEmpId.Text + "  not existed...Please enter valid employee id"
                    txtSecEmpId.Text = ""
                    txtSecEmpId.Focus()
                    Exit Sub
                End If
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Employee Id " + txtEmpId.Text + "  not existed...Please enter valid employee id"
                txtEmpId.Text = ""
                txtEmpId.Focus()
                Exit Sub
            End If
        Else
            Dim validatecode As String = validateempid(txtEmpId.Text)
            Dim validatecode2 As String = validateempid(txtSecEmpId.Text)
            Dim validatecode3 As String = validateempid(txtTeritaryEmpId.Text)
            If validatecode = "0" Then
                If validatecode2 = "0" Then
                    If validatecode3 = "0" Then
                        updaterecord()
                    Else
                        lblMsg.Visible = True
                        lblMsg.Text = "Employee Id " + txtTeritaryEmpId.Text + "  not existed...Please enter valid employee id"
                        txtTeritaryEmpId.Text = ""
                        txtTeritaryEmpId.Focus()
                        Exit Sub
                    End If
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Employee Id " + txtSecEmpId.Text + "  not existed...Please enter valid employee id"
                    txtSecEmpId.Text = ""
                    txtSecEmpId.Focus()
                    Exit Sub
                End If
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Employee Id " + txtEmpId.Text + "  not existed...Please enter valid employee id"
                txtEmpId.Text = ""
                txtEmpId.Focus()
                Exit Sub
            End If

        End If
    End Sub

    Public Function validateempid(empid As String) As String
        Dim validatecode As String
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_AUR_ID")
        sp.Command.AddParameter("@AUR_ID", empid, DbType.String)
        validatecode = sp.ExecuteScalar
        Return validatecode
    End Function

    Public Sub updaterecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SRQUSP_UPDATE_ASSETCATEGORY")
            sp1.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SRQ_MAP_REQID", ddlServiceCat.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SRQ_MAP_AUR_ID", txtEmpId.Text, DbType.String)
            sp1.Command.AddParameter("@SRQ_MAP_AUR_ID2", txtSecEmpId.Text, DbType.String)
            sp1.Command.AddParameter("@SRQ_MAP_AUR_ID3", txtTeritaryEmpId.Text, DbType.String)
            sp1.Command.AddParameter("@SRQ_MAPSTA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@id", CInt(lblTemp.Text), DbType.Int32)
            sp1.Command.AddParameter("@SRQ_MAPUPT_BY", Session("Uid"), DbType.String)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "Location incharges modified successfully..."
            cleardata()
            btnSubmit.Text = "Add"
            ddlLocation.Enabled = True
            ddlServiceCat.Enabled = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub insertnewrecord()
        Try
            Dim chk As String
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SRQUSP_INSERT_ASSETCATEGORY")
            sp1.Command.AddParameter("@SRQ_LCM_ID", ddlLocation.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SRQ_MAP_REQID", ddlServiceCat.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SRQ_MAP_AUR_ID", txtEmpId.Text, DbType.String)
            sp1.Command.AddParameter("@SRQ_MAP_AUR_ID2", txtSecEmpId.Text, DbType.String)
            sp1.Command.AddParameter("@SRQ_MAP_AUR_ID3", txtTeritaryEmpId.Text, DbType.String)
            sp1.Command.AddParameter("@SRQ_MAPSTA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@SRQ_MAPUPT_BY", Session("Uid"), DbType.String)
            chk = sp1.ExecuteScalar()
            If chk = "0" Then
                lblMsg.Visible = True
                lblMsg.Text = "Location incharges already added for this service..."
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Location incharges inserted successfully..."
                cleardata()
            End If
            fillgrid()
            cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SRQUSP_GET_SETEMPMAPPING")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvSer.DataSource = ds
        gvSer.DataBind()
        For i As Integer = 0 To gvSer.Rows.Count - 1
            Dim lblstatus As Label = CType(gvSer.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "InActive"
            End If
        Next
    End Sub

    Protected Sub gvSer_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvSer.PageIndexChanging
        gvSer.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub gvSer_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvSer.RowCommand

        If e.CommandName = "EDIT" Then
            btnSubmit.Text = "Modify"
            lblMsg.Text = ""
            If gvSer.Rows.Count > 0 Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                Dim lblID As Label = DirectCast(gvSer.Rows(rowIndex).FindControl("lblID"), Label)
                Dim id As Integer = CInt(lblID.Text)
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SRQ_LOC_GETSRQMAPPINGBIND")
                sp.Command.AddParameter("@id", id, DbType.Int32)
                lblTemp.Text = id
                Dim ds As DataSet
                ds = sp.GetDataSet()

                If ds.Tables(0).Rows.Count > 0 Then

                    ddlLocation.Enabled = False
                    ddlServiceCat.Enabled = False
                    bindcity()
                    ddlLocation.ClearSelection()
                    ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("LCM_CODE")).Selected = True
                    Dim lcm_code As String
                    lcm_code = ds.Tables(0).Rows(0).Item("LCM_CODE")
                    bindrequests(lcm_code)

                    ddlServiceCat.ClearSelection()
                    ddlServiceCat.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_CODE")).Selected = True
                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("SRQ_MAPSTA_ID")).Selected = True
                    txtEmpId.Text = ds.Tables(0).Rows(0).Item("SRQ_MAP_AUR_ID")
                    txtSecEmpId.Text = ds.Tables(0).Rows(0).Item("SRQ_MAP_AUR_ID2")
                    txtTeritaryEmpId.Text = ds.Tables(0).Rows(0).Item("SRQ_MAP_AUR_ID3")

                End If

            End If
            'ElseIf e.CommandName = "Status" Then
            '    'Changing the status from active to inactive and vice versa...
            '    Dim lnkStatus As LinkButton = DirectCast(e.CommandSource, LinkButton)
            '    Dim gvRow As GridViewRow = DirectCast(lnkStatus.NamingContainer, GridViewRow)
            '    Dim status As Integer = CInt(e.CommandArgument)
            '    Dim lblId As Label = DirectCast(gvRow.FindControl("lblId"), Label)
            '    Dim id1 As String = lblId.Text
            '    Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_UPDATE_SPACE_STATUS")
            '    sp2.Command.AddParameter("@id1", id1, DbType.String)
            '    sp2.Command.AddParameter("@status", 1 - status, DbType.Int32)
            '    sp2.ExecuteScalar()
            '    gvSer.Visible = True
            '    fillgrid()
        End If


        'fillgrid()
    End Sub

    Protected Sub gvSer_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvSer.RowEditing

    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        lblMsg.Text = ""
        If ddlLocation.SelectedIndex <> 0 Then
            bindrequests(ddlLocation.SelectedItem.Value)
        Else

        End If
    End Sub

End Class
