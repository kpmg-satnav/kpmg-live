<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DisposableAssetsRequisition.ascx.vb"
    Inherits="Controls_DisposableAssetsRequisition" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="../BootStrapCSS/Scripts/bootstrap-select.min.js"></script>
    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.gvSurrenderAstReq.Rows.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }
        <%--function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkItem";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = CheckBox.checked;
            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }--%>

        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvSurrenderAstReq.ClientID%>');
            var TargetChildControl = "chkSelect";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvSurrenderAstReq.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>

</head>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row" style="overflow-x: auto; width: auto">
    <div class="col-md-12">
        <fieldset>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Val1" CssClass="alert alert-danger" ForeColor="Red" />
            <legend>Disposal Requisitions</legend>
            <br />
            <asp:GridView ID="gvSurrenderAstReq" runat="server" EmptyDataText="No Surrender Disposal Requisitions Found." AllowPaging="true" PageSize="10"
                AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="SurrchkSelect" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requisition ID" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("SREQ_ID", "~/FAM/FAM_WebFiles/frmDisposableAssetsRequisitionDTLS.aspx?Req_id={0}")%>'
                                Text='<%# Eval("SREQ_ID")%> '></asp:HyperLink>
                            <asp:Label ID="lblSREQ_ID" runat="server" Text='<%#Eval("SREQ_ID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="left" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lblLCM_NAME" runat="server" Text='<%#Eval("LCM_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Asset Id" ItemStyle-HorizontalAlign="left" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lbl_AST_ID" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lblAAT_AST_CODE" runat="server" Text='<%#Eval("AAT_DESC")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Model" ItemStyle-HorizontalAlign="left" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("AAT_MODEL_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Actual Cost" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lbl_cost" runat="server" Text='<%#Eval("AST_ACTUAL_COST")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Depreciated(%)" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lbl_Dep_per" runat="server" Text='<%#Eval("AST_DEPR_PERCENTAGE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Depreciation Cost(Till Date)" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lbl_Dep_val" runat="server" Text='<%#Eval("AST_DEPR_COST")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Current Value" Visible="true">
                        <ItemTemplate>
                            <asp:Label ID="lbl_Cur_val" runat="server" Text='<%#Eval("AST_CURRENT_VALUE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Dispose Date" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblSurrenderDate" runat="server" Text='<%#Eval("DREQ_REQUESITION_DT") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Salvage Value" ItemStyle-HorizontalAlign="left" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblSalvage" runat="server" Text='<%#Eval("AST_SALAVAGE_VALUE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                   
                   
                    <asp:TemplateField HeaderText="Type of Disposals">
                        <ItemTemplate>
                            <%--<asp:DropDownList ID="ddlAssetDis" runat="server" CssClass="selectpicker">
                            </asp:DropDownList>--%>
                            <asp:Label ID="ddlAssetDis" runat="server" Text='<%#Eval("TAG_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Recovery Value">
                        <ItemTemplate>
                            <%--<asp:TextBox ID="txtRecoveryval" runat="server" CssClass="selectpicker" MaxLength="10"></asp:TextBox>--%>
                            <asp:Label ID="txtRecoveryval" runat="server" Text='<%#Eval("AST_RECOVERY_VAL")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purchase Date">
                        <ItemTemplate>
                            <asp:Label ID="lblpur_Date" runat="server" Text='<%#Eval("PURCHASEDDATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Age(Till date)">
                        <ItemTemplate>
                            <asp:Label ID="lbl_age" runat="server" Text='<%#Eval("ASSET_AGE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested By">
                        <ItemTemplate>
                            <asp:Label ID="lblSurBy" runat="server" Text='<%#Eval("AUR_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblSurStatus" runat="server" Text='<%#Eval("STA_TITLE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="Depreciated(%)">
                        <ItemTemplate>
                            <asp:Label ID="lbl_Dep_per" runat="server" Text='<%#Eval("DEP_PERCENTAGE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <%--  <asp:TemplateField ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("SREQ_ID") %>'
                                CommandName="Edit">Details</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </fieldset>
    </div>
</div>
<br />
<%--<div class="row" style="margin-top: 10px">
    <div class="col-md-12">

        <fieldset>
            <legend>Disposal Requisitions</legend>
            <br />
            <asp:GridView ID="gvAstDispoReq" runat="server" EmptyDataText="No Disposal Requisitions Found." AllowPaging="true" PageSize="10"
                CssClass="table table-condensed table-bordered table-hover table-striped" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="DispchkSelect" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requesition ID" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("DREQ_ID", "~/FAM/FAM_WebFiles/frmAdminAstDisposableDTLS.aspx?Req_id={0}")%>'
                                Text='<%# Eval("DREQ_ID")%> '></asp:HyperLink>
                            <asp:Label ID="lblDREQ_ID" runat="server" Text='<%#Eval("DREQ_ID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblAST_CODE" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Model Name" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("AAT_MODEL_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblLCM_NAME" runat="server" Text='<%#Eval("LCM_NAME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requisition Date" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblDREQ_REQUESITION_DT" runat="server" Text='<%#Eval("DREQ_REQUESITION_DT") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <asp:Label ID="lblSurStatus" runat="server" Text='<%#Eval("STA_TITLE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                  
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </fieldset>

    </div>
</div>--%>
<br />
<div class="col-md-12" style="overflow-x: auto;">
    
    <div class="row" id="divremarks" runat="server">
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">

                <label class="col-md-12 control-label">Remarks:<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                    Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"
                    Enabled="true"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>

            </div>
        </div>
        <%--</div>
<div class="row">--%>
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnApprovAll" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                <asp:Button ID="btnCancel" Text="Reject" runat="server" CssClass="btn btn-primary custom-button-color" />
                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-primary custom-button-color" />
            </div>
        </div>
    </div>
