<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AMGVendorNewRecord.ascx.vb"
    Inherits="Controls_AMGVendorNewRecord" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>


<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<div>
    <table id="table2" cellspacing="0" cellpadding="0" width="98%" align="center" border="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">Add New Vendor<hr align="center" width="60%" /> </asp:Label>
            </td>
        </tr>
        <tr>
            <td width="100%" align="center">
            </td>
        </tr>
    </table>
    <asp:Panel ID="PanelGridview" runat="server" Width="85%">
        <table id="Table3" cellspacing="0" cellpadding="0" style="vertical-align: top; width: 95%"
            align="center" border="0">
            <tr>
                <td style="width: 10px">
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" />
                </td>
                <td class="tableHEADER" align="left" style="width: 100%">
                    &nbsp;<strong>Add New Vendor</strong></td>
                <td>
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
                </td>
                <td align="left" >
                 <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <asp:Panel ID="Panel1" runat="server" Width="100%">
                        <table id="table1" cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">
                            <tr>
                                <td align="left" style="width: 50%; height: 26px;">
                                    Enter Code<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfCode" runat="server" ControlToValidate="txtCode"
                                        Display="None" ErrorMessage="Please enter code !" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="width: 50%; height: 26px;">
                                    <asp:TextBox ID="txtCode" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 50%; height: 26px;">
                                    Enter Name<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                        Display="None" ErrorMessage="Please enter Name !" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="width: 50%; height: 26px;">
                                    <asp:TextBox ID="txtName" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 50%; height: 26px;">
                                    Select Grade<font class="clsNote">*</font>
                                </td>
                                <td align="left" style="width: 50%; height: 26px;">
                                    <asp:RadioButtonList ID="rdbtnGrade" runat="server" CssClass="clsRadioButton" RepeatDirection="Horizontal"
                                        Width="97%">
                                        <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                                        <asp:ListItem Value="0">InActive</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 50%; height: 26px;">
                                    Enter Address<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress"
                                        Display="None" ErrorMessage="Please enter Address !" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator></td>
                                <td align="left" style="width: 50%; height: 26px;">
                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="clsTextField" Width="97%" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                           
                            <tr>
                                <td align="left" style="width: 50%; height: 26px;">
                                    Select City<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="cvcity" runat="server" InitialValue="0" ControlToValidate="ddlCity"
                                        Display="None" ErrorMessage="Please Select City !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="width: 50%; height: 26px;">
                                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="clsComboBox" Width="97%">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 50%; height: 26px;">
                                    Enter Phone Number<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvPhoneNo" runat="server" ControlToValidate="txtPhone"
                                        Display="None" ErrorMessage="Please enter PhoneNumber !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="width: 50%; height: 26px;">
                                    <asp:TextBox ID="txtPhone" runat="server" CssClass="clsTextField" Width="97%" MaxLength="16"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 50%; height: 26px;">
                                    Enter Mobile Number<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfMobile" runat="server" ControlToValidate="txtMobile"
                                        Display="None" ErrorMessage="Please enter MobileNumber !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="none" ControlToValidate="txtMobile"
                                        ErrorMessage="Please enter Mobile" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" style="width: 50%; height: 26px;">
                                    <asp:TextBox ID="txtMobile" runat="server" CssClass="clsTextField" Width="97%" MaxLength="16"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 50%; height: 26px;">
                                    Enter Email Address<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfEmail" runat="server" ControlToValidate="txtEmail"
                                        Display="None" ErrorMessage="Please enter Emailaddress !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" ValidationGroup="Val1"
                                        ErrorMessage="Please enter valid Email" Display="none" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                    </asp:RegularExpressionValidator>
                                </td>
                                <td align="left" style="width: 50%; height: 26px;">
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 50%; height: 26px;">
                                    Please Select Status<font class="clsNote">*</font>
                                </td>
                                <td align="center" style="width: 50%; height: 26px;">
                                    <asp:RadioButtonList ID="rdbtnStatus" runat="server" CssClass="clsRadioButton" Width="97%"
                                        RepeatDirection="Horizontal">
                                        <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                                        <asp:ListItem Value="0">InActive</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                           
                            <tr>
                                <td align="left" style="width: 50%; height: 26px;">
                                    Enter Remarks<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfRemarks" runat="server" ControlToValidate="txtRemarks"
                                        Display="None" ErrorMessage="Please enter Remarks" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="width: 50%; height: 26px;">
                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="clsTextField" Width="97%" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" width="50%" colspan="2">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="button" Width="76px" TabIndex="18"
                                        ValidationGroup="Val1" Text="Submit" />
                                    &nbsp;
                                    <asp:Button ID="btnBack" runat="server" CssClass="button" Width="76px" Text="Back" />
                                </td>
                            </tr>
                            
                        </table>
                    </asp:Panel>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px; width: 524px;" background="../../Images/table_bot_mid_bg.gif">
                    <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </asp:Panel>
</div>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
    </Triggers>

</asp:UpdatePanel>
