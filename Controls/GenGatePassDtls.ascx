<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GenGatePassDtls.ascx.vb"
    Inherits="Controls_GenGatePassDtls" %>
<script type="text/javascript">
<!--
    function printPartOfPage(elementId) {
        var printContent = document.getElementById(elementId);
        var windowUrl = 'about:blank';
        var uniqueName = new Date();
        var windowName = 'Print' + uniqueName.getTime();
        var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');
        printWindow.document.write(printContent.innerHTML);
        printWindow.document.close();
        printWindow.focus();
        printWindow.print();
        printWindow.close();
    }
    // -->
</script>
<div id ="print">
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <div class="row">
                <div class="col-md-11">
                    The following assets are being moved from <b>
                            <asp:Label ID="lblFromLoc" runat="server"></asp:Label></b>, to 
                    <b>
                        <asp:Label ID="lblToLoc" runat="server"></asp:Label></b>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" style="margin-top: 10px">
    
        <asp:GridView ID="gvItems" runat="server" EmptyDataText="No Assets Found." AllowPaging="true"
            PageSize="10" CssClass="table table-condensed table-bordered table-hover table-striped" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblAAS_AAT_CODE" runat="server" Text='<%#Eval("AAT_CODE") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblAAT_NAME" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>
    
</div>
    <br />
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="form-group">
            <div class="row">
                <div class="col-md-11">
                    The Request <b>
                        <asp:Label ID="lblReqId" runat="server" CssClass="bodytext"></asp:Label></b>
                    has been raised by <b>
                        <asp:Label ID="lblRaisedBy" runat="server"></asp:Label></b>, approved by <b>
                            <asp:Label ID="lblapprovedby" runat="server"></asp:Label></b>.
                    <br />
                    <hr />
                    This is to inform that these assets are now being shifted from this premise and
                            will be accepted by
                     
                    <b>
                        <asp:Label ID="lblreceivedby" runat="server"></asp:Label></b> at the new premise.
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick="printPartOfPage('print')" CssClass="btn btn-primary custom-button-color"></asp:Button>
            <asp:Button ID="btnGetPass" runat="server" Text="Gate Pass Generate" CssClass="btn btn-primary custom-button-color"></asp:Button>
        </div>
    </div>
</div>


