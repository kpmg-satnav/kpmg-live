Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class Controls_AddVendorRateContract
    Inherits System.Web.UI.UserControl

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged

        If rbActions.Checked = True Then
            clearsubpanel()
            ddlVendor.SelectedIndex = 0
            'ddlAssetCategory.SelectedIndex = 0
            'ddlAstSubCat.SelectedIndex = 0
            'ddlAstBrand.SelectedIndex = 0
            'ddlModel.SelectedIndex = 0
            cleardata()
            btnSubmit.Text = "Add"
            lblMsg.Visible = False
            subpanel.Visible = False
            mainpanel.Visible = True
            lblVendorNameData.Enabled = True
            lblCatData.Enabled = True
            lblSubCatData.Enabled = True
            lblBrandData.Enabled = True
            lblModelData.Enabled = True
        Else
            cleardata()
            btnSubmit.Text = "Modify"
            lblMsg.Visible = False
            getvendorcontract()
            subpanel.Visible = True
            mainpanel.Visible = False
            clearsubpanel()
            ddlVendor.SelectedIndex = 0
            lblVendorNameData.Enabled = False
            lblCatData.Enabled = False
            lblSubCatData.Enabled = False
            lblBrandData.Enabled = False
            lblModelData.Enabled = False
            'ddlAssetCategory.SelectedIndex = 0
            'ddlAstSubCat.SelectedIndex = 0
            'ddlAstBrand.SelectedIndex = 0
            'ddlModel.SelectedIndex = 0
        End If

    End Sub

    Private Sub cleardata()
        Try
            'ddlAssetCategory.Items.Clear()
            'ddlAstSubCat.Items.Clear()
            'ddlAstBrand.Items.Clear()
            'ddlVendor.Items.Clear()
            'ddlModel.Items.Clear()           
            txtFDate.Text = ""
            txtTDate.Text = ""
            txtPrice.Text = ""
            txtActualCost.Text = ""
            txtDiscount.Text = ""
            txtRemarks.Text = ""
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub getvendorcontract()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_VETCONTRACT")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlVenRate.DataSource = sp.GetDataSet()
        ddlVenRate.DataTextField = "AST_MD_CATID"
        ddlVenRate.DataValueField = "AMG_VENCON_ID"
        ddlVenRate.DataBind()
        ddlVenRate.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If
        RegularExpressionValidator1.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
        RegularExpressionValidator2.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        revPropertyType.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()

        txtFDate.Attributes.Add("readonly", "readonly")
        txtTDate.Attributes.Add("readonly", "readonly")
        If Not IsPostBack Then
            subpanel.Visible = False
            mainpanel.Visible = True
            getvendors()
            fillgrid()
            btnSubmit.Text = "Add"
            'txtFDate.Attributes.Add("onClick", "displayDatePicker('" + txtFDate.ClientID + "')")
            'txtFDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            'txtTDate.Attributes.Add("onClick", "displayDatePicker('" + txtTDate.ClientID + "')")
            'txtTDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtFDate.Attributes.Add("Readonly", "Readonly")
            txtTDate.Attributes.Add("Readonly", "Readonly")
            lblFDate.Attributes.Add("Readonly", "Readonly")
            lblTDate.Attributes.Add("Readonly", "Readonly")

            'lblFDate.Attributes.Add("onClick", "displayDatePicker('" + lblFDate.ClientID + "')")
            'lblFDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            'lblTDate.Attributes.Add("onClick", "displayDatePicker('" + lblTDate.ClientID + "')")
            'lblTDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

        End If
    End Sub

    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()
        ddlModel.Items.Insert(0, "--Select--")

    End Sub

    Private Sub getbrandbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlAstSubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, "--Select--")

    End Sub

    Private Sub getassetsubcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", ddlAssetCategory.SelectedItem.Value, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, "--Select--")
    End Sub

    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", ddlVendor.SelectedItem.Value, DbType.String)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        ddlAssetCategory.Items.Insert(0, "--Select--")
    End Sub

    Private Sub getvendors()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ALLVENDORS")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlVendor.DataSource = sp.GetDataSet()
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataValueField = "AVR_CODE"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, "--Select--")

    End Sub

    Protected Sub ddlVendor_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendor.TextChanged
        If ddlVendor.SelectedIndex <> 0 Then
            ddlAstSubCat.Items.Clear()
            ddlAstBrand.Items.Clear()
            ddlModel.Items.Clear()
            txtFDate.Text = ""
            txtTDate.Text = ""
            txtPrice.Text = ""
            getassetcategory()
        End If
    End Sub

    Protected Sub ddlAssetCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        If ddlAssetCategory.SelectedIndex <> 0 Then
            ddlAstBrand.Items.Clear()
            ddlModel.Items.Clear()
            txtFDate.Text = ""
            txtTDate.Text = ""
            txtPrice.Text = ""
            getassetsubcategory()
        End If
    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        If ddlAstSubCat.SelectedIndex <> 0 Then
            ddlModel.Items.Clear()
            txtFDate.Text = ""
            txtTDate.Text = ""
            txtPrice.Text = ""
            getbrandbycatsubcat()
        End If
    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        If ddlAstBrand.SelectedIndex <> 0 Then
            txtFDate.Text = ""
            txtTDate.Text = ""
            txtPrice.Text = ""
            getmakebycatsubcat()
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'If CDate(txtFDate.Text) < getoffsetdatetime(DateTime.Now) Then
        '    lblMsg.Text = "Please Select Valid Date From Date Cannot be less than Current Date"
        '    Exit Sub
        'Else

        If btnSubmit.Text = "Add" Then
            If CDate(txtTDate.Text) < CDate(txtFDate.Text) Then
                lblMsg.Text = "Selected To Date cannot be less than From Date"
                Exit Sub
            End If
            Dim ValidateCode As Integer
            ValidateCode = ValidateBrand()
            If ValidateCode = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "This contract already exist please enter another Model..."
            ElseIf ValidateCode = 1 Then
                insertnewrecord()
                fillgrid()

            End If
        Else

            If ddlVenRate.SelectedIndex <> 0 Then

                modifydata()
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Please select vendor rate contract"
            End If
        End If
    End Sub

    Public Function ValidateBrand()
        Dim ValidateCode As Integer
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_VALIDATEVENDCONT")
        sp1.Command.AddParameter("@AMG_VENCAN_VENID", ddlVendor.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AMG_VENCON_MDLID", ddlModel.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AMG_VENCON_BRID", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AMG_VENCON_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AMG_VENCOT_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp1.Command.AddParameter("@AMG_VEN_FDATE", txtFDate.Text, DbType.Date)
        sp1.Command.AddParameter("@AMG_VEN_TDATE", txtTDate.Text, DbType.Date)

        ValidateCode = sp1.ExecuteScalar()
        Return ValidateCode
    End Function

    Private Sub insertnewrecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_INSERT_RATECONTRACT")
            sp1.Command.AddParameter("@AMG_VENCAN_RT", txtPrice.Text, DbType.Double)
            sp1.Command.AddParameter("@AMG_VENCAN_VENID", ddlVendor.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AMG_VENCON_MDLID", ddlModel.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AMG_VENCON_BRID", ddlAstBrand.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AMG_VENCON_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AMG_VENCOT_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AMG_VEN_FDATE", txtFDate.Text, DbType.Date)
            sp1.Command.AddParameter("@AMG_VEN_TDATE", txtTDate.Text, DbType.Date)
            sp1.Command.AddParameter("@AMG_VENCON_CREATEDBY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@AMG_ACT_AMT", txtActualCost.Text, DbType.Double)
            sp1.Command.AddParameter("@AMG_DISC_AMT", txtDiscount.Text, DbType.Double)
            sp1.Command.AddParameter("@AMG_VEN_REM", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "New Vendor Contract Added Successfully..."
            'cleardata()
            ddlAssetCategory.SelectedIndex = 0
            ddlAstSubCat.SelectedIndex = 0
            ddlAstBrand.SelectedIndex = 0
            ddlVendor.SelectedIndex = 0
            ddlModel.SelectedIndex = 0
            txtFDate.Text = ""
            txtTDate.Text = ""
            txtPrice.Text = ""
            txtActualCost.Text = ""
            txtDiscount.Text = ""
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

    Private Sub modifydata()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_MODIFY_RATECONTRACT")
            sp1.Command.AddParameter("@AMG_VENCON_ID", ddlVenRate.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AMG_VENCAN_RT", txtPrice.Text, DbType.Double)
            sp1.Command.AddParameter("@AMG_VENCON_UPDATEDBY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@AMG_FRM_DATE", lblFDate.Text, DbType.String)
            sp1.Command.AddParameter("@AMG_TO_DATE", lblTDate.Text, DbType.String)
            sp1.Command.AddParameter("@AMG_ACT_AMT", txtActualCost.Text, DbType.Double)
            sp1.Command.AddParameter("@AMG_DISC_AMT", txtDiscount.Text, DbType.Double)
            sp1.Command.AddParameter("@AMG_VEN_REM", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "Vendor Contract Modified Successfully..."
            clearsubpanel()
            cleardata()
            ' ddlAssetCategory.SelectedIndex = 0
            'ddlAstSubCat.SelectedIndex = 0
            'ddlAstBrand.SelectedIndex = 0
            'ddlVendor.SelectedIndex = 0
            ddlVenRate.SelectedIndex = 0
            txtPrice.Text = ""
            txtActualCost.Text = ""
            txtDiscount.Text = ""

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_VETCONTRACTGRID")
        sp.Command.AddParameter("@dummy", 1, DbType.Int16)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvBrand.DataSource = ds
        gvBrand.DataBind()


        'For i As Integer = 0 To gvBrand.Rows.Count - 1
        '    Dim lblstatus As Label = CType(gvBrand.Rows(i).FindControl("lblstatus"), Label)
        '    If lblstatus.Text = "1" Then
        '        lblstatus.Text = "Active"
        '    Else
        '        lblstatus.Text = "InActive"
        '    End If
        'Next
    End Sub

    Protected Sub ddlVenRate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVenRate.SelectedIndexChanged
        lblMsg.Text = " "
        If ddlVenRate.SelectedIndex <> 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_VETCONTRACTDRPDN")
            sp.Command.AddParameter("@AMG_VENCON_ID", ddlVenRate.SelectedItem.Value, DbType.Int32)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count > 0 Then
                txtPrice.Text = ds.Tables(0).Rows(0).Item("AMG_VENCAN_RT")
                txtActualCost.Text = ds.Tables(0).Rows(0).Item("AMG_ACT_AMT")
                txtDiscount.Text = ds.Tables(0).Rows(0).Item("AMG_DISC_AMT")
                lblVendorNameData.Text = ds.Tables(0).Rows(0).Item("AVR_NAME")
                lblCatData.Text = ds.Tables(0).Rows(0).Item("VT_TYPE")
                lblSubCatData.Text = ds.Tables(0).Rows(0).Item("AST_SUBCAT_NAME")
                lblBrandData.Text = ds.Tables(0).Rows(0).Item("manufacturer")
                lblModelData.Text = ds.Tables(0).Rows(0).Item("AST_MD_NAME")
                lblFDate.Text = ds.Tables(0).Rows(0).Item("AMG_VEN_FDATE")
                lblTDate.Text = ds.Tables(0).Rows(0).Item("AMG_VEN_TDATE")
                txtRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("AMG_VEN_REM"))

            End If
        Else
            cleardata()

        End If
    End Sub

    Private Sub clearsubpanel()
        lblVendorNameData.Text = ""
        lblCatData.Text = ""
        lblSubCatData.Text = ""
        lblBrandData.Text = ""
        lblModelData.Text = ""
        txtPrice.Text = ""
        lblFDate.Text = ""
        lblTDate.Text = ""
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/FAM/Masters/Mas_Webfiles/frmAssetMasters.aspx")
    End Sub

    Protected Sub gvBrand_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBrand.PageIndexChanging
        gvBrand.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub txtDiscount_TextChanged(sender As Object, e As EventArgs) Handles txtDiscount.TextChanged

        If txtDiscount.Text = Nothing Then
            txtDiscount.Text = 0
        End If
        If txtActualCost.Text = Nothing Then
            txtActualCost.Text = 0
        End If

        Dim discount As Integer
        discount = (txtActualCost.Text / 100) * txtDiscount.Text
        txtPrice.Text = Convert.ToInt32(txtActualCost.Text) - discount


    End Sub

    Protected Sub txtActualCost_TextChanged(sender As Object, e As EventArgs) Handles txtActualCost.TextChanged

        If txtDiscount.Text = Nothing Then
            txtDiscount.Text = 0
        End If
        If txtActualCost.Text = Nothing Then
            txtActualCost.Text = 0
        End If

        Dim discount As Integer
        discount = (txtActualCost.Text / 100) * txtDiscount.Text
        txtPrice.Text = Convert.ToInt32(txtActualCost.Text) - discount
    End Sub

End Class
