Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.io
Partial Class Controls_AttDiscrepancyHR
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If CDate(txtFrmDate.Text) > CDate(txtToDate.Text) Then
                tablegrid1.Visible = False

                lblMsg.Text = "To Date Should be Greater than From Date"
            ElseIf ddlEmployee.SelectedIndex = 0 Or txtFrmDate.Text = "" Or txtToDate.Text = "" Then
                lblMsg.Text = "Please Fill Mandatory Fields"
                tablegrid1.Visible = False

            Else

                BindGrid()
                lblMsg.Text = ""
                tablegrid1.Visible = True
                If gvItems.Rows.Count > 0 Then
                    btnExportExcel.Visible = True
                Else
                    btnExportExcel.Visible = False
                End If
            End If
           
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then


            BindEmployee()
            tablegrid1.Visible = False
            btnExportExcel.Visible = False
            
        End If
        txtFrmDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DISCREPANCY_EMPLOYEE")
            sp.Command.AddParameter("@FROM_DATE", txtFrmDate.Text, DbType.Date)
            sp.Command.AddParameter("@TO_DATE", txtToDate.Text, DbType.Date)
            sp.Command.AddParameter("@AUR_ID", ddlEmployee.SelectedItem.Value, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            gvItems.DataSource = ds
            gvItems.DataBind()
            
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindEmployee()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_ASSOCIATE")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlEmployee.DataSource = sp.GetDataSet()
            ddlEmployee.DataTextField = "AUR_FIRST_NAME"
            ddlEmployee.DataValueField = "AUR_ID"
            ddlEmployee.DataBind()
            ddlEmployee.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlEmployee.Items.Insert(1, New ListItem("--All--", "--All--"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"DISCREPANCY_EMPLOYEE")
        sp.Command.AddParameter("@FROM_DATE", txtFrmDate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_DATE", txtToDate.Text, DbType.Date)
        sp.Command.AddParameter("@AUR_ID", ddlEmployee.SelectedItem.Value, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        Export("Attendance_Discrepancy_Details" & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") & ".xls", gv)
    End Sub
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

End Class
