<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AdminAstDisposableDTLS.ascx.vb"
    Inherits="Controls_AdminAstDisposableDTLS" %>

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label class="col-md-12 control-label">Asset Name:</label>
            <div class="col-md-7">
                <asp:Label ID="lblAstName" runat="server"></asp:Label>
            </div>

        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label class="col-md-12 control-label">Model Name:</label>
            <div class="col-md-7">
                <asp:Label ID="lblModelName" runat="server"></asp:Label>

            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label class="col-md-12 control-label">Location:</label>
            <div class="col-md-12">
                <asp:Label ID="lblLocation" runat="server"></asp:Label>

            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">

            <label class="col-md-12 control-label">Asset Surrender Date:</label>
            <div class="col-md-12">
                <asp:Label ID="lblAstSurDt" runat="server"></asp:Label>
            </div>

        </div>
    </div>
</div>

<br />

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            
                <label class="col-md-12 control-label">Requesition Id:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblSurReq_id" runat="server"></asp:Label>
                </div>
            
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            
                <label class="col-md-12 control-label">Asset Salvage Value:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstSalvage" runat="server"></asp:Label>
                </div>
            
        </div>
    </div>
</div>
<br />
<div class="row">
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            
                <label class="col-md-12 control-label">Remarks:<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                </div>
            
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnApprov" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" />
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
</div>
