Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_ManualPunching
    Inherits System.Web.UI.UserControl

    Protected Sub btnPunchIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPunchIn.Click
        lblMsg.Text = ""
        Dim val As String = getoffsetdatetime(DateTime.Now).ToString()
        Bind_SWPIntime(val)
        lblMsg.Text = "Your Punch-In Time Is " & getoffsetdatetime(DateTime.Now).ToString()
    End Sub

    Protected Sub btnPunchOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPunchOut.Click
        lblMsg.Text = ""
        Dim val As String = getoffsetdatetime(DateTime.Now).ToString()
        Bind_SWPOuttime(val)
        lblMsg.Text = "Your Punch-Out Time Is " & getoffsetdatetime(DateTime.Now).ToString()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = "Please Click on Punch-In button Inorder to do Manual Punching"
    End Sub
    Private Sub Bind_SWPIntime(ByVal val As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SWP_INTIME_MANUAL")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@SWP_INTIME", Val, DbType.DateTime)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub
    Private Sub Bind_SWPOuttime(ByVal val As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_SWP_OUTTIME_MANUAL")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@SWP_OUTTIME", val, DbType.DateTime)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub
End Class
