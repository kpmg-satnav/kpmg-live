﻿
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic

Partial Class Controls_spaceasset
    Inherits System.Web.UI.UserControl

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        If txtasset.Text <> "" Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Asset_Space")
            sp.Command.AddParameter("@ASSET_CODE", txtasset.Text, DbType.String)
            sp.Command.AddParameter("@SPACE_ID", Request.QueryString("id"), DbType.String)
            Dim i As Integer = sp.ExecuteScalar()
            If i = 1 Then
                lblmsg.Text = "Asset Placed Succesfully in the specified location"
            ElseIf i = 2 Then
                lblmsg.Text = "Asset code not exists"
            End If
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            txtasset.Text = ""
        End If
    End Sub
End Class
