<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UpdateAdminSurrenderRequisitions.ascx.vb"
    Inherits="Controls_UpdateAdminSurrenderRequisitions" %>


<%--<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Code:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstCode" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Name:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstName" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Location:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblLocation" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Date:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstDate" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Allocated to Employee Date:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstAllocDt" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset Surrender Date:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblAstSurDt" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Surrender Requesition Id:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblSurReq_id" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">RM Approval Date:</label>
                <div class="col-md-7">
                    <asp:Label ID="lblSREQ_RMAPPROVAL_DATE" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
</div>--%>
<%----------------------------------------------------------------------------------------%>
<div id="SurReqDet" runat="server">
    <div class="row" style="margin-top: 10px">
    <div class="col-md-12">
      
            <asp:GridView ID="SurReqDetItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                EmptyDataText="No Asset(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>
                     <asp:TemplateField HeaderText="Asset code" Visible="false">                       
                        <ItemTemplate>
                            <asp:Label ID="lblAstCode" runat="server" Text='<%#Eval("AAT_AST_CODE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Name">                       
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="AssetName" Visible="false">                      
                        <ItemTemplate>                          
                           <asp:Label ID="lblModelName" runat="server" Text='<%#Eval("AAT_MODEL_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location">                       
                        <ItemTemplate>
                            <asp:Label ID="lblLocation" Visible="false" runat="server" Text='<%#Eval("LCM_CODE")%>'></asp:Label>
                            <asp:Label ID="lbllocationname" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Asset  Date">                       
                        <ItemTemplate>                           
                            <asp:Label ID="lblAstDate" runat="server" Text='<%#Eval("AAT_UPT_DT")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Allocated To Employee Date">                       
                        <ItemTemplate>                           
                            <asp:Label ID="lblAstAllocDt" runat="server" Text='<%#Eval("AAT_ALLOCATED_DATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Asset Surrender Date">                       
                        <ItemTemplate>                           
                            <asp:Label ID="lblAstSurDt" runat="server" Text='<%#Eval("AAT_SURRENDERED_DATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surrender Requesition Id">                       
                        <ItemTemplate>                           
                            <asp:Label ID="lblSurReq_id" runat="server" Text='<%#Eval("SREQ_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="RM Approval Date">                       
                        <ItemTemplate>                           
                            <asp:Label ID="lblSREQ_RMAPPROVAL_DATE" runat="server" Text='<%#Eval("SREQ_RMAPPROVAL_DATE")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approved By">                       
                        <ItemTemplate>                           
                            <asp:Label ID="lblRM_NAME" runat="server" Text='<%#Eval("RM_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Satus" Visible="false">                       
                        <ItemTemplate>                           
                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("STA_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                 
              
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
       
    </div>
    </div>
    <br /><br />
</div>

<div class="row">
    <%--<div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Approved By (RM):</label>
                <div class="col-md-7">
                    <asp:Label ID="lblRM_NAME" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>--%>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">RM Remarks:<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <%--<asp:Label ID="lblRMRemarks" runat="server"></asp:Label>--%>
                      <asp:TextBox CssClass="form-control" ID="lblRMRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Admin Remarks:<span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:TextBox CssClass="form-control" ID="txtRMRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Asset to be:</label>
                <div class="col-md-7">
                    <asp:RadioButtonList ID="rdbList" runat="server" AutoPostBack="True" RepeatDirection="Vertical">
                        <asp:ListItem Value="0">Dispose</asp:ListItem>
                        <asp:ListItem Selected="True" Value="1">Re-Use</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
        </div>
    </div>
    
</div>

<div id="pnlDispose" visible="false" runat="server">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Asset Value:</label>
                    <div class="col-md-7">
                        <asp:TextBox CssClass="form-control" Enabled="false" ID="txtSalvageVal" runat="server"></asp:TextBox>

                        Asset Value = (Cost - Salvage Value) / Life
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">    

            <asp:Button ID="btnApprov" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" />
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
</div>
