<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssetModify.ascx.vb"
    Inherits="Controls_AssetModify" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

        <div>
            <table id="table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">Asset<hr align="center" width="60%" /> </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="center">
                    </td>
                </tr>
            </table>
            <asp:Panel ID="PanelGridview" runat="server" Width="85%">
                <table id="Table3" cellspacing="0" cellpadding="0" style="vertical-align: top; width: 95%"
                    align="center" border="0">
                     <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                    <tr>
                        <td style="width: 10px">
                            <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" />
                        </td>
                        <td class="tableHEADER" align="left" style="width: 100%">
                            &nbsp;<strong>Asset Modification Details</strong></td>
                        <td>
                            <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
                        </td>
                        <td align="left" >
                         <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                            <asp:Panel ID="Panel1" runat="server" Width="100%">
                                <table id="table1" cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">
                                    
                                    <tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            Name<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                                Display="None" ErrorMessage="Please enter code !" ValidationGroup="Val1">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="width: 50%;height:26px">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            Model Name<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvModelName" runat="server" ControlToValidate="txtModelName"
                                                Display="None" ErrorMessage="Please enter ModelName !" ValidationGroup="Val1">
                                            </asp:RequiredFieldValidator></td>
                                        <td align="left" style="width: 50%;height:26px">
                                            <asp:TextBox ID="txtModelName" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            Select AssetGroupName<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="cvAssetgrpName" runat="server" InitialValue="0" ControlToValidate="drdGrp_Name"
                                                Display="None" ErrorMessage="Please Select AssetName !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="width: 50%;height:26px">
                                            <asp:DropDownList ID="drdGrp_Name" runat="server" CssClass="clsComboBox" Width="97%">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            Select AssetVendorName<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="cvAssetVendorName" runat="server" InitialValue="0"
                                                ControlToValidate="drdVendor_Name" Display="None" ErrorMessage="Please Select AssetName !"
                                                ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="width: 50%;height:26px">
                                            <asp:DropDownList ID="drdVendor_Name" runat="server" CssClass="clsComboBox" Width="97%">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            Select AssetBrandName<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="cvAssetBrandName" runat="server" InitialValue="0"
                                                ControlToValidate="drdBrand_Name" Display="None" ErrorMessage="Please Select AssetName !"
                                                ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="width: 50%;height:26px">
                                            <asp:DropDownList ID="drdBrand_Name" runat="server" CssClass="clsComboBox" Width="97%">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            AssetRequired<font class="clsNote">*</font>
                                        </td>
                                        <td align="left" style="width: 50%;height:26px">
                                            <asp:RadioButtonList ID="assetreq" runat="server" CssClass="clsRadioButton" RepeatDirection="Horizontal"
                                                Width="97%">
                                                <asp:ListItem Value="True" Selected="True">Yes</asp:ListItem>
                                                <asp:ListItem Value="False">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            Asset STAID<font class="clsNote">*</font>
                                        </td>
                                        <td align="center" style="width: 50%;height:26px">
                                            <asp:RadioButtonList ID="assetid" runat="server" CssClass="clsRadioButton" Width="97%"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Value="True" Selected="True">Active</asp:ListItem>
                                                <asp:ListItem Value="False">InActive</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            Asset Owned<font class="clsNote">*</font>
                                        </td>
                                        <td align="left" style="width: 50%;height:26px">
                                            <asp:RadioButtonList ID="assetOwned" runat="server" CssClass="clsRadioButton" Width="97%"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Value="True" Selected="True">Owned</asp:ListItem>
                                                <asp:ListItem Value="False">Rented</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            AssetPurchasedStatus<font class="clsNote">*</font>
                                        </td>
                                        <td align="left">
                                            <asp:RadioButtonList ID="AssetPurchasedStatus" CssClass="clsRadioButton" runat="server"
                                                Width="97%" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="True" Selected="True">Purchased</asp:ListItem>
                                                <asp:ListItem Value="False">Rebilished</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            AssetSpaceFixed<font class="clsNote">*</font>
                                        </td>
                                        <td align="left">
                                            <asp:RadioButtonList ID="rdbtnAssetSpace" runat="server" CssClass="clsRadioButton"
                                                Width="97%" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="True" Selected="True">Fixed</asp:ListItem>
                                                <asp:ListItem Value="False">Movable</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            AssetUser<font class="clsNote">*</font>
                                        </td>
                                        <td align="left">
                                            <asp:RadioButtonList ID="rdbtnAssetUser" runat="server" Width="97%" RepeatDirection="horizontal">
                                                <asp:ListItem Value="True">Fixed</asp:ListItem>
                                                <asp:ListItem Value="False" Selected="True">Movable</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    &nbsp;<tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            AssetCondition<font class="clsNote">*</font>
                                        </td>
                                        <td align="left" style="width: 50%;height:26px">
                                            <asp:RadioButtonList ID="rdbtnAssetCons" CssClass="clsRadioButton" runat="server"
                                                Width="97%" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="True" Selected="True">Workable</asp:ListItem>
                                                <asp:ListItem Value="False">Disposable</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width: 50%;height:26px">
                                            Remarks<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfRemarks" runat="server" ControlToValidate="txtRemarks"
                                                Display="None" ErrorMessage="Please enter Remarks" ValidationGroup="Val1">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="width: 50%;height:26px">
                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="clsTextField" Width="97%" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" width="50%" colspan="2">
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Width="76px" TabIndex="18"
                                                ValidationGroup="Val1" Text="Submit" />
                                            &nbsp;
                                            <asp:Button ID="btnBack" runat="server" CssClass="button" Width="76px" Text="Back" />
                                        </td>
                                    </tr>
                                   
                                </table>
                            </asp:Panel>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px; width: 524px;" background="../../Images/table_bot_mid_bg.gif">
                            <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px; width: 17px;">
                            <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
        <asp:PostBackTrigger ControlID="btnBack" />
    </Triggers>
</asp:UpdatePanel>
