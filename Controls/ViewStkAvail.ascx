<%@ Control Language="VB" AutoEventWireup="false" CodeFile="~/Controls/ViewStkAvail.ascx.vb" Inherits="Controls_ViewStkAvail" %>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>
<div id="Uploadfile" class="row" runat="server">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Upload Document  (Only Excel ) <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                    ControlToValidate="fpBrowseDoc" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseDoc"
                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                    ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                </asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div class="btn btn-default">
                        <i class="fa fa-folder-open-o fa-lg"></i>
                        <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <div class="col-md-5  control-label">
                    <asp:Button ID="btnbrowse" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Upload" ValidationGroup="Val2" />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Category<span style="color: red;"></span></label>
            <asp:RequiredFieldValidator ID="reqastcat" runat="server" ErrorMessage="Please Enter Asset Category" InitialValue="--Select--"
                ControlToValidate="ddlastCat" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlastCat" runat="server" CssClass="selectpicker" data-live-search="true"
                AutoPostBack="True" Style="width: 77px">
            </asp:DropDownList>
        </div>
    </div>


    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Sub Category <span style="color: red;"></span></label>
            <asp:RequiredFieldValidator ID="rersubastcat" runat="server" ErrorMessage="Please Enter Asset sub Category" InitialValue="--Select--"
                ControlToValidate="ddlastsubCat" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlastsubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Brand/Make<span style="color: red;"></span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Model<span style="color: red;"></span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>


</div>
<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Location<span style="color: red;"></span></label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-xs-12" style="padding-top: 17px">

        <div class="form-group">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="View Availability" ValidationGroup="Val1" CausesValidation="true" />
            <asp:Button ID="btnclear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" ValidationGroup="Val1" CausesValidation="False" />
            <asp:Button ID="btnExport" runat="server" CssClass="btn btn-primary custom-button-color" Text="Export To Excel" CausesValidation="False" Visible="false" />
        </div>
    </div>
</div>


<div id="panel1" runat="Server">
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">

            <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False"
                AllowSorting="True" AllowPaging="True" EmptyDataText="No Assets Found" CssClass="table table-condensed table-bordered table-hover table-striped">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>



                    <asp:TemplateField HeaderText="Asset Code" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblAssCode" runat="server" Text='<%#Eval("Asset Code")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset ID">
                        <ItemTemplate>
                            <asp:Label ID="lblAssName" runat="server" Text='<%#Eval("Asset Name")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Asset Name">
                        <ItemTemplate>
                            <asp:Label ID="lblAstDesc" runat="server" Text='<%#Eval("AAT_DESC")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Serial Number">
                        <ItemTemplate>
                            <asp:Label ID="lblsrno" runat="server" Text='<%#Eval("Serial Number")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category Name">
                        <ItemTemplate>
                            <asp:Label ID="lblBarnd" runat="server" Text='<%#Eval("Category")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sub Category Name">
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("Subcategory Name")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Brand Name">
                        <ItemTemplate>
                            <asp:Label ID="lblBarnd" runat="server" Text='<%#Eval("Brand")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Model Name">
                        <ItemTemplate>
                            <asp:Label ID="lblAstName" runat="server" Text='<%#Eval("Asset Model Name")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Code" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblStock_id" Visible="false" runat="server" Text='<%#Eval("Asset Model Id")%>'></asp:Label>
                            <asp:Label ID="lblAstCode" runat="server" Text='<%#Eval("Asset Model Code")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Age">
                        <ItemTemplate>
                            <asp:Label ID="lblAssAge" runat="server" Text='<%#Eval("Asset Age")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Generated Date">
                        <ItemTemplate>
                            <asp:Label ID="lblGenDate" runat="server" Text='<%#Eval("Asset Purchased Date")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location">
                        <ItemTemplate>
                            <asp:Label ID="lblLCM_CODE" runat="server" Text='<%#Eval("Location Name")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Space Id" Visible="FALSE">
                        <ItemTemplate>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Id" Visible="FALSE">
                        <ItemTemplate>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>

        </div>
    </div>
</div>
<div id="Panel2" runat="Server">
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12">
            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False"
                AllowSorting="True" AllowPaging="True" EmptyDataText="No Assets Found" CssClass="table table-condensed table-bordered table-hover table-striped">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>

                    <asp:TemplateField HeaderText="Asset Code">
                        <ItemTemplate>
                            <asp:Label ID="lblAssCode" runat="server" Text='<%#Eval("ASSET_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Name">
                        <ItemTemplate>
                            <asp:Label ID="lblAssName" runat="server" Text='<%#Eval("ASSET_NAME")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Space Id">
                        <ItemTemplate>
                            <asp:Label ID="lblAssCode" runat="server" Text='<%#Eval("SPACE_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Employee Id">
                        <ItemTemplate>
                            <asp:Label ID="lblAssName" runat="server" Text='<%#Eval("EMP_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblAssName" runat="server" Text='<%#Eval("REMARKS")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>

        </div>
    </div>
</div>

<script >
    function refreshSelectpicker() {
        $("#<%=ddlLocation.ClientID%>").selectpicker();
        $("#<%= ddlastCat.ClientID%>").selectpicker();
        $("#<%= ddlAstBrand.ClientID%>").selectpicker();
        $("#<%= ddlAstModel.ClientID%>").selectpicker();
        $("#<%= ddlastsubCat.ClientID%>").selectpicker();
    };
    refreshSelectpicker();
</script>