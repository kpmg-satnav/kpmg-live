<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ConfRequisition.ascx.vb"
    Inherits="Controls_ConfRequisition" %>
<asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Select Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvfrm" runat="server" ControlToValidate="txtFrmDate"
                    ErrorMessage="Please select Date" Display="none" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='fromdate'>
                        <asp:TextBox ID="txtFrmDate" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" CausesValidation="True" />
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvItems" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="True" AutoGenerateColumns="false"
                EmptyDataText="No Swipe History Found." BorderStyle="None">
                <Columns>
                    <asp:TemplateField HeaderText="Associate">
                        <ItemTemplate>
                            <asp:Label ID="lblAssociate" runat="Server" Text='<%#Eval("EMP_ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="In-Time">
                        <ItemTemplate>
                            <asp:Label ID="lblInTime" runat="server" Text='<%#Eval("INTIME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Out-Time">
                        <ItemTemplate>
                            <asp:Label ID="lblOutTime" runat="server" Text='<%#Eval("OUTTIME") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Working">
                        <ItemTemplate>
                            <asp:Label ID="lblWorking" runat="server" Text='<%#Eval("WORKING") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label"> </label>
                <div class="col-md-7">

                    <asp:GridView ID="gvIn" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="True" AutoGenerateColumns="false"
                        EmptyDataText="No In-Out History Found." BorderStyle="None">
                        <Columns>
                            <asp:TemplateField HeaderText="In-Time">
                                <ItemTemplate>
                                    <asp:Label ID="lblInTime1" runat="server" Text='<%#Eval("INTIME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label"></label>
                <div class="col-md-7">
                    <asp:GridView ID="gvOut" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="True" AutoGenerateColumns="false"
                        BorderStyle="None">
                        <Columns>
                            <asp:TemplateField HeaderText="Out-Time">
                                <ItemTemplate>
                                    <asp:Label ID="lblOutTime1" runat="server" Text='<%#Eval("OUTTIME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</div>



