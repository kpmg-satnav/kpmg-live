﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UploadSpaceData.ascx.vb" Inherits="Controls_UploadSpaceData" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

        <div>
            <table id="table1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td align="center" width="100%">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                            ForeColor="Black">Upload Employee Space Data
             <hr align="center" width="60%" /></asp:Label></td>
                </tr>
            </table>
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
                <table id="table4" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                    <tr>
                        <td align="left" width="100%" colspan="3">
                            <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                        <td width="100%" class="tableHEADER" align="left">
                            <strong>&nbsp;Upload Employee Space Data</strong>
                        </td>
                        <td>
                            <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                                ForeColor="" ValidationGroup="Val1" />
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            
                            <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                            <asp:Panel ID="panel2" runat="Server" GroupingText="Upload Excel">
                                    <table id="tdetails" runat="Server" width="100%" cellpadding="2" cellspacing="0" align="center" border="1">
                                        <tr>
                                            <td colspan="2">
                                                <asp:HyperLink ID="hyp" runat="server" Text=" Click here to View the Template" NavigateUrl="~/Masters/Mas_Webfiles/NewAsset.xls"></asp:HyperLink>

                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left" style="height: 26px; width: 50%">Upload Document  (Only Excel )<font class="clsNote">*</font>
                                                <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                                                    ControlToValidate="fpBrowseDoc" InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" ControlToValidate="fpBrowseDoc"
                                                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                                                    ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> </asp:RegularExpressionValidator>
                                            </td>
                                            <td align="left" style="height: 26px; width: 50%">
                                                <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="97%" /></td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:Button ID="btnbrowse" runat="Server" CssClass="button" Text="Upload" ValidationGroup="Val2" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            <table id="table2" cellspacing="0" cellpadding="0" width="100%" border="1">
                          
                                <tr>
                                    <td colspan="2" align="center" style="height: 39px">
                                        <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" PostBackUrl="~/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx" CausesValidation="False" />
                                        &nbsp;&nbsp;&nbsp;
                                 <%--       <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Add" ValidationGroup="Val1" />--%>

                                        <%--   <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Back" />--%>&nbsp;
                                    </td>
                                </tr>
                            </table>

                         
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </ContentTemplate>
   
</asp:UpdatePanel>