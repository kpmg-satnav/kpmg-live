Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AssetGetDetails
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvDetails_AAT.PageIndex = 0
        End If
        fillgrid()
    End Sub
    Protected Sub gvDetails_AAT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDetails_AAT.PageIndexChanging
        gvDetails_AAT.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
    Private Sub fillgrid()
        lbtn1.Visible = False
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Select_AMG_ASSET")
        sp.Command.AddParameter("@dummy", "", DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvDetails_AAT.DataSource = ds
        gvDetails_AAT.DataBind()
        For i As Integer = 0 To gvDetails_AAT.Rows.Count - 1

            Dim lblReq_AAT As Label = CType(gvDetails_AAT.Rows(i).FindControl("lblReq_AAT"), Label)
            If lblReq_AAT.Text = "0" Then
                lblReq_AAT.Text = "Inactive"
            Else
                lblReq_AAT.Text = "Active"
            End If
            Dim lblStaID_AAT As Label = CType(gvDetails_AAT.Rows(i).FindControl("lblStaID_AAT"), Label)
            If lblStaID_AAT.Text = "0" Then
                lblStaID_AAT.Text = "Inactive"
            Else
                lblStaID_AAT.Text = "Active"

            End If
            Dim lblOwned_AAT As Label = CType(gvDetails_AAT.Rows(i).FindControl("lblOwned_AAT"), Label)
            If lblOwned_AAT.Text = "0" Then
                lblOwned_AAT.Text = "Rented"
            Else
                lblOwned_AAT.Text = "Owned"

            End If
            Dim lblPurchasedStatus_AAT As Label = CType(gvDetails_AAT.Rows(i).FindControl("lblPurchasedStatus_AAT"), Label)
            If lblPurchasedStatus_AAT.Text = "0" Then
                lblPurchasedStatus_AAT.Text = "Rebilished"
            Else
                lblPurchasedStatus_AAT.Text = "Purchased"

            End If
            Dim LblAAT_SPC_FIXED As Label = CType(gvDetails_AAT.Rows(i).FindControl("LblAAT_SPC_FIXED"), Label)
            If LblAAT_SPC_FIXED.Text = "0" Then
                LblAAT_SPC_FIXED.Text = "Fixed"
            Else
                LblAAT_SPC_FIXED.Text = "Movable"

            End If
            Dim lblAAT_USR_MOVABLE As Label = CType(gvDetails_AAT.Rows(i).FindControl("lblAAT_USR_MOVABLE"), Label)
            If lblAAT_USR_MOVABLE.Text = "0" Then
                lblAAT_USR_MOVABLE.Text = "Fixed"
            Else
                lblAAT_USR_MOVABLE.Text = "Movable"

            End If
            Dim lblAAT_AST_CONS As Label = CType(gvDetails_AAT.Rows(i).FindControl("lblAAT_AST_CONS"), Label)
            If lblAAT_AST_CONS.Text = "0" Then
                lblAAT_AST_CONS.Text = "Disposable"
            Else
                lblAAT_AST_CONS.Text = "Workable"
            End If
        Next
    End Sub
    Protected Sub gvDetails_AAT_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDetails_AAT.SelectedIndexChanged
        Dim rowIndex As Integer = gvDetails_AAT.SelectedIndex
        Dim lbl As Label = DirectCast(gvDetails_AAT.Rows(rowIndex).FindControl("lbl"), Label)
    End Sub
    

    'Protected Sub gvDetails_AAT_DataBound(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim row As GridViewRow = gvDetails_AAT.BottomPagerRow
    '    Dim Start As Integer = 1
    '    For i As Integer = 1 To gvDetails_AAT.PageCount - 1
    '        Dim lbtn As New LinkButton()
    '        lbtn.CommandName = "Page"
    '        lbtn.CommandArgument = i

    '        If i = gvDetails_AAT.PageIndex + 1 Then
    '            lbtn.BackColor = Drawing.Color.BlanchedAlmond
    '        End If

    '        lbtn.Text = (Start).ToString()
    '        lbtn.ToolTip = "Page " & i
    '        Start += 1
    '        Dim place As PlaceHolder = TryCast(row.FindControl("PlaceHolder1"), PlaceHolder)
    '        place.Controls.Add(lbtn)

    '        Dim lbl As New Label()
    '        lbl.Text = " "
    '        place.Controls.Add(lbl)
    '    Next i
    'End Sub
    'Protected Sub gvDetails_AAT_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDetails_AAT.RowDeleting

    'End Sub

    'Protected Sub gvDetails_AAT_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDetails_AAT.DataBound
    '    Dim row As GridViewRow = gvDetails_AAT.BottomPagerRow
    '    If row Is Nothing Then
    '        Return

    '    End If
    '    Dim ddlPages As DropDownList = DirectCast(row.FindControl("ddlPages"), DropDownList)
    '    Dim lblPageCount As Label = DirectCast(row.FindControl("lblPageCount"), Label)
    '    If ddlPages IsNot Nothing Then
    '        ' populate pager 
    '        For i As Integer = 0 To gvDetails_AAT.PageCount - 1
    '            Dim intPageNumber As Integer = i + 1
    '            Dim lstItem As New ListItem(intPageNumber.ToString())
    '            If i = gvDetails_AAT.PageIndex Then
    '                lstItem.Selected = True
    '            End If
    '            ddlPages.Items.Add(lstItem)
    '        Next
    '    End If
    '    If lblPageCount IsNot Nothing Then
    '        lblPageCount.Text = gvDetails_AAT.PageCount.ToString()
    '    End If



    'End Sub
    'Protected Sub ddlPages_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDetails_AAT.SelectedIndexChanged
    '    Dim row As GridViewRow = gvDetails_AAT.BottomPagerRow
    '    Dim ddlPages As DropDownList = DirectCast(row.FindControl("ddlPages"), DropDownList)
    '    gvDetails_AAT.PageIndex = ddlPages.SelectedIndex
    '    fillgrid()

    'End Sub

    'Protected Sub Paginate(ByVal sender As Object, ByVal e As CommandEventArgs)
    '    ' get the current page selected     int intCurIndex = grdListings.PageIndex;
    '    Dim intCurIndex As Integer = gvDetails_AAT.PageIndex

    '    Select Case e.CommandArgument.ToString().ToLower()
    '        Case "first"
    '            gvDetails_AAT.PageIndex = 0
    '            Exit Select
    '        Case "prev"
    '            gvDetails_AAT.PageIndex = intCurIndex - 1
    '            Exit Select
    '        Case "next"
    '            gvDetails_AAT.PageIndex = intCurIndex + 1
    '            Exit Select
    '        Case "last"
    '            gvDetails_AAT.PageIndex = gvDetails_AAT.PageCount
    '            Exit Select
    '    End Select

    '    fillgrid()
    'End Sub
    Protected Sub btnfincode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfincode.Click
        If txtfindcode.Text = "" Then
            lblMsg.Text = "No Record found"
            gvDetails_AAT.Visible = False
        Else
            Dim AAT_CODE As String = txtfindcode.Text
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RECORD")
            sp2.Command.AddParameter("@AAT_CODE", AAT_CODE, DbType.String)
            Dim ds2 As New DataSet
            ds2 = sp2.GetDataSet()
            gvDetails_AAT.DataSource = ds2
            gvDetails_AAT.DataBind()
            For i As Integer = 0 To gvDetails_AAT.Rows.Count - 1

                Dim lblReq_AAT As Label = CType(gvDetails_AAT.Rows(i).FindControl("lblReq_AAT"), Label)
                If lblReq_AAT.Text = "0" Then
                    lblReq_AAT.Text = "Inactive"
                Else
                    lblReq_AAT.Text = "Active"
                End If
                Dim lblStaID_AAT As Label = CType(gvDetails_AAT.Rows(i).FindControl("lblStaID_AAT"), Label)
                If lblStaID_AAT.Text = "0" Then
                    lblStaID_AAT.Text = "Inactive"
                Else
                    lblStaID_AAT.Text = "Active"

                End If
                Dim lblOwned_AAT As Label = CType(gvDetails_AAT.Rows(i).FindControl("lblOwned_AAT"), Label)
                If lblOwned_AAT.Text = "0" Then
                    lblOwned_AAT.Text = "Rented"
                Else
                    lblOwned_AAT.Text = "Owned"

                End If
                Dim lblPurchasedStatus_AAT As Label = CType(gvDetails_AAT.Rows(i).FindControl("lblPurchasedStatus_AAT"), Label)
                If lblPurchasedStatus_AAT.Text = "0" Then
                    lblPurchasedStatus_AAT.Text = "Rebilished"
                Else
                    lblPurchasedStatus_AAT.Text = "Purchased"

                End If
                Dim LblAAT_SPC_FIXED As Label = CType(gvDetails_AAT.Rows(i).FindControl("LblAAT_SPC_FIXED"), Label)
                If LblAAT_SPC_FIXED.Text = "0" Then
                    LblAAT_SPC_FIXED.Text = "Fixed"
                Else
                    LblAAT_SPC_FIXED.Text = "Movable"

                End If
                Dim lblAAT_USR_MOVABLE As Label = CType(gvDetails_AAT.Rows(i).FindControl("lblAAT_USR_MOVABLE"), Label)
                If lblAAT_USR_MOVABLE.Text = "0" Then
                    lblAAT_USR_MOVABLE.Text = "Fixed"
                Else
                    lblAAT_USR_MOVABLE.Text = "Movable"

                End If
                Dim lblAAT_AST_CONS As Label = CType(gvDetails_AAT.Rows(i).FindControl("lblAAT_AST_CONS"), Label)
                If lblAAT_AST_CONS.Text = "0" Then
                    lblAAT_AST_CONS.Text = "Disposable"
                Else
                    lblAAT_AST_CONS.Text = "Workable"
                End If
            Next
            lbtn1.Visible = True
        End If
    End Sub
    Public Function ValidateAATcode()
        Dim ValidCode As Integer
        Dim AAT_CODE As String = txtfindcode.Text
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RECORD")
        sp3.Command.AddParameter("@AAT_CODE", AAT_CODE, DbType.String)
        ValidCode = sp3.ExecuteScalar()
        Return ValidCode
    End Function

    Protected Sub gvDetails_AAT_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDetails_AAT.RowCommand
        If e.CommandName = "DELETE" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblCode_AAT As Label = DirectCast(gvDetails_AAT.Rows(rowIndex).FindControl("lblCode_AAT"), Label)
            Dim id As String = lblCode_AAT.Text
            Dim SP1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMG_AAT_DEL")
            SP1.Command.AddParameter("@AAT_CODE", id, DbType.String)
            SP1.ExecuteScalar()
        End If
        fillgrid()
    End Sub

    Protected Sub lbtn1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtn1.Click
        If Not IsPostBack Then
            gvDetails_AAT.PageIndex = 0
        End If
        fillgrid()
    End Sub

    Protected Sub gvDetails_AAT_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)

    End Sub
End Class
