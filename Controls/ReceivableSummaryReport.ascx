<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ReceivableSummaryReport.ascx.vb"
    Inherits="Controls_ReceivableSummaryReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">From Date</label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="rfvcode" runat="server" ControlToValidate="txtfindcode"
                                ErrorMessage="Please Enter From Date" ValidationGroup="Val1" SetFocusOnError="True"
                                Display="none"></asp:RequiredFieldValidator>
                            <div class='input-group date' id='fromdate'>
                                <asp:TextBox ID="txtfindcode" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">To Date</label>
                        <div class="col-md-7">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txttoDate"
                                ErrorMessage="Please Enter To Date" ValidationGroup="Val1" SetFocusOnError="True"
                                Display="none"></asp:RequiredFieldValidator>
                            <div class='input-group date' id='todate'>
                                <asp:TextBox ID="txttoDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnfincode" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
        </div>
    </div>
</div>
<%--<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row col-md-12 control-label">               
                <asp:Label ID="lblMRentPaid" runat="server"></asp:Label>
                <asp:Label ID="lblMOutStndng" runat="server"> </asp:Label>
            </div>
        </div>
    </div>

</div>--%>


<div class="row">
    <div class="col-md-5">
          <div class="form-group">
        <ul>
            <li class="list-group-item">
                <span class="badge">
                    <label for="lblMRentPaid"><%=Rentpaid %></label>
                </span>
                Total Tenant Rent               
            </li>
        </ul>
    </div>
        </div>
    <div class="col-md-5">
          <div class="form-group">
        <ul>
            <li class="list-group-item">
                <span class="badge">
                    <label for="lblMOutStndng"><%=Outstandingamt %></label>
                </span>
                Total Rent Amount          
            </li>
        </ul>
    </div>
        </div>
</div>


<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnexporttoexcel" runat="Server" Text="Export to Excel" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
</div>

<div class="row table table table-condensed table-responsive">
    <div class="form-group">
        <div class="col-md-12">
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
        </div>
    </div>
</div>
      

<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <asp:GridView ID="gvLDetails_PybleSmrRpt" runat="server" AutoGenerateColumns="False"
            AllowSorting="True" AllowPaging="True" PageSize="5" EmptyDataText="No Records Found"
            OnPageIndexChanging="gvLDetails_PybleSmrRpt_PageIndexChanging" CssClass="table table-condensed table-bordered table-hover table-striped">
            <PagerSettings Mode="NumericFirstLast" />
            <Columns>
                <asp:TemplateField HeaderText="Tenant Name">
                    <ItemTemplate>
                        <asp:Label ID="lblTenantID" runat="server" Text='<%#Eval("TenantID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Building ID">
                    <ItemTemplate>
                        <asp:Label ID="lblTenantID" runat="server" Text='<%#Eval("PN_NAME")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="RentPaid">
                    <ItemTemplate>
                        <asp:Label ID="lblRentPaid" runat="server" Text='<%# Eval("RentPaid","{0:c2}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Outstanding Amount">
                    <ItemTemplate>
                        <asp:Label ID="lblOutStanding" runat="server" Text='<%# Eval("Outstanding", "{0:c2}")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PaidDate">
                    <ItemTemplate>
                        <asp:Label ID="lblPaidDate" runat="server" Text='<%#Eval("PaidDate")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FromDate">
                    <ItemTemplate>
                        <asp:Label ID="lblFromDate" runat="server" Text='<%#Eval("FromDate")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ToDate">
                    <ItemTemplate>
                        <asp:Label ID="lblToDate" runat="server" Text='<%#Eval("ToDate")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TDS">
                    <ItemTemplate>
                        <asp:Label ID="lblTDS" runat="server" Text='<%#Eval("TDS")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Payment_Mode">
                    <ItemTemplate>
                        <asp:Label ID="lblPayment_Mode" runat="server" Text='<%#Eval("Payment_Mode")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ChequeNo">
                    <ItemTemplate>
                        <asp:Label ID="lblChequeNo" runat="server" Text='<%#Eval("ChequeNo")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ChequeDate">
                    <ItemTemplate>
                        <asp:Label ID="lblChequeDate" runat="server" Text='<%#Eval("ChequeDate")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="IssuingBank">
                    <ItemTemplate>
                        <asp:Label ID="lblIssuingBank" runat="server" Text='<%#Eval("IssuingBank")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="IFSC">
                    <ItemTemplate>
                        <asp:Label ID="lblIFCB" runat="server" Text='<%#Eval("IFCB")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AccountNumber">
                    <ItemTemplate>
                        <asp:Label ID="lblAccountNumber" runat="server" Text='<%#Eval("AccountNumber")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
            <FooterStyle CssClass="GVFixedFooter" />
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />

        </asp:GridView>

    </div>
</div>

