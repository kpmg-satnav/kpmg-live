﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AptUploadSchedulePlan.ascx.vb" Inherits="Controls_AptUploadSchedulePlan" %>
<div class="row">
    <div class="col-md-12 text-center">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvitems1" runat="server" EmptyDataText="No Journey plan Document Found."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Request Id">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="Server" Text='<%#Eval("REQUESTID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested Date">
                        <ItemTemplate>
                            <asp:Label ID="lblreqdate" runat="server" Text='<%#Eval("REQUESTED_ON")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Request Name">
                        <ItemTemplate>
                            <asp:Label ID="lblreqname" runat="server" Text='<%#Eval("REQUESTNAME")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("STA_TITLE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField Text="Select" CommandName="Select" />
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <div class="form-group">
            <div class="row">
                <asp:TextBox ID="txtstore" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
</div>
<div id="pnluploaddoc" runat="server" visible="false">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row text-center">
                    <strong>UPLOAD</strong>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row ">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select Category<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvCat" runat="server" ControlToValidate="ddlCategory"
                        Display="none" ErrorMessage="Please Select Category" ValidationGroup="Val1"
                        InitialValue="0"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlCategory" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Select Document Type<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvDocType" runat="server" ControlToValidate="ddlDocType"
                        Display="none" ErrorMessage="Please Select Document Type" ValidationGroup="Val1"
                        InitialValue="0"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlDocType" runat="server" CssClass="selectpicker" data-live-search="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Document Title<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvtitle" runat="server" Display="None" ErrorMessage="Please Enter Title"
                        ControlToValidate="txtDocTitle" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtDocTitle" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Browse Document<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvdoc" runat="server" Display="None" ErrorMessage="Please Browse File"
                        ControlToValidate="fpBrowseDoc" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseDoc"
                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                        ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$"> 
                    </asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:FileUpload ID="fpBrowseDoc" runat="Server" CssClass="btn btn-primary custom-button-color" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                    CausesValidation="true" />
            </div>
        </div>
    </div>
</div>
