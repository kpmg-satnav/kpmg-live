<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AMGTagNewRecord.ascx.vb"
    Inherits="Controls_AssetTagNewRecord" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<table id="table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
    <tr>
        <td width="100%" align="center">
            <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">Asset Tagged NewRecord<hr align="center" width="60%" /> </asp:Label>
        </td>
    </tr>
    <tr>
        <td width="100%" align="center">
        </td>
    </tr>
</table>
<asp:Panel ID="PNLCONTAINER" runat="server" Width="65%" Height="95%">
    <table cellpadding="0" cellspacing="0" style="vertical-align: top;" width="95%" align="center"
        border="0">
        <tr>
            <td style="width: 10px">
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" />
            </td>
            <td width="100%" class="tableHEADER" align="left">
                <strong>&nbsp; Add New Record </strong>
            </td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" />
            </td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif" style="width: 10px; height: 176px;">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <asp:Panel ID="Panel1" runat="server" Width="100%">
                    <table id="table1" cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                Select Vendor
                                <asp:RequiredFieldValidator ID="cvVendorCode" runat="server" InitialValue="0" ControlToValidate="ddlVendorCode"
                                    Display="none" ErrorMessage="Please Select Vendor Code !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:DropDownList ID="ddlVendorCode" runat="server" CssClass="clsComboBox" Width="97%"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                Select Group
                                <asp:RequiredFieldValidator ID="cvGrpCode" runat="server" InitialValue="0" ControlToValidate="ddlGrpCode"
                                    Display="none" ErrorMessage="Please Select Group Code !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                            <td align="left" style="height: 26px; width: 50%">
                                <asp:DropDownList ID="ddlGrpCode" runat="server" CssClass="clsComboBox" Width="97%"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                Select Brand
                                <asp:RequiredFieldValidator ID="cvBrandCode" runat="server" InitialValue="0" ControlToValidate="ddlBrandCode"
                                    Display="none" ErrorMessage="Please Select Brand Code !" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; width: 50%">
                                <asp:DropDownList ID="ddlBrandCode" runat="server" CssClass="clsComboBox" Width="97%"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                Select Asset
                                <asp:RequiredFieldValidator ID="cvCode" runat="server" InitialValue="0" ControlToValidate="ddlCode"
                                    Display="none" ErrorMessage="Please Select Code !" ValidationGroup="Val1"></asp:RequiredFieldValidator></td>
                            <td align="left" style="height: 26px; width: 50%">
                                <asp:DropDownList ID="ddlCode" runat="server" CssClass="clsComboBox" Width="97%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                Select Employee
                                <asp:RequiredFieldValidator ID="CompareValidator1" runat="server" InitialValue="0"
                                    ControlToValidate="ddlEmpID_AATTAG" Display="none" ErrorMessage="Please Select Employee !"
                                    ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; width: 50%">
                                <asp:DropDownList ID="ddlEmpID_AATTAG" runat="server" CssClass="clsComboBox" Width="97%">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Width="76px" Text="Submit"
                                    ValidationGroup="Val1" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 176px;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</asp:Panel>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
    </Triggers>
</asp:UpdatePanel>
