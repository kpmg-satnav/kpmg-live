
Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common

Partial Class Controls_AstAttributeSelection
    Inherits System.Web.UI.UserControl
    Private selectedAttributes_Renamed As Commerce.Common.Attributes

    Public Property SelectedAttributes() As Commerce.Common.Attributes
        Get

            'loop through the controls and 
            'create an attribute selection from them
            Dim selectedAttribute As Commerce.Common.Attribute
            Dim selection As Commerce.Common.AttributeSelection
            Dim priceAdjustment As Decimal = 0

            If Not product_Renamed.Attributes Is Nothing Then
                For Each att As Commerce.Common.Attribute In product_Renamed.Attributes
                    'pull the control out
                    'and set the values/price adjustments
                    selectedAttribute = New Commerce.Common.Attribute()
                    selectedAttribute.SelectionType = att.SelectionType
                    selectedAttribute.Selections = New System.Collections.Generic.List(Of AttributeSelection)()
                    selectedAttribute.Name = att.Name
                    selectedAttribute.Description = att.Description
                    'based on the type, pull out the selection
                    Select Case att.SelectionType
                        Case AttributeType.SingleSelection
                            Dim ddl As DropDownList = CType(pnlAttControls.FindControl(att.Name), DropDownList)
                            selection = New AttributeSelection()
                            selection.Value = ddl.SelectedValue
                            selection.PriceAdjustment = att.GetPriceAdjustment(selection.Value)
                            selectedAttribute.Selections.Add(selection)
                        Case AttributeType.MultipleSelection
                            Dim chk As CheckBoxList = CType(pnlAttControls.FindControl(att.Name), CheckBoxList)
                            For Each item As ListItem In chk.Items
                                If item.Selected Then
                                    selection = New AttributeSelection()
                                    selection.Value = item.Text
                                    selection.PriceAdjustment = att.GetPriceAdjustment(selection.Value)
                                    selectedAttribute.Selections.Add(selection)
                                End If
                            Next item
                        Case AttributeType.UserInput
                            Dim t As TextBox = CType(pnlAttControls.FindControl(att.Name), TextBox)
                            selection = New AttributeSelection()
                            selection.Value = t.Text
                            selectedAttribute.Selections.Add(selection)
                        Case Else
                    End Select
                    selectedAttributes_Renamed.Add(selectedAttribute)
                Next att
            End If

            Return selectedAttributes_Renamed

        End Get
        Set(ByVal value As Commerce.Common.Attributes)
            selectedAttributes_Renamed = Value
        End Set
    End Property
    Private product_Renamed As Commerce.Common.Product

    Public Property Product() As Commerce.Common.Product
        Get
            Return product_Renamed
        End Get
        Set(ByVal value As Commerce.Common.Product)
            product_Renamed = Value
        End Set
    End Property

    Private Sub LoadControls()
        Dim tbl As HtmlTable = New HtmlTable()
        tbl.ID = "tblAtts"
        tbl.Attributes.Add("class", "blackfont")
        Dim tr As HtmlTableRow
        Dim td As HtmlTableCell
        Dim indexer As Integer = 0
        If Not product_Renamed.Attributes Is Nothing Then
            selectedAttributes_Renamed = New Attributes()
            For Each att As Commerce.Common.Attribute In product_Renamed.Attributes
                tr = New HtmlTableRow()
                td = New HtmlTableCell()
                td.Attributes.Add("class", "blackfont")
                Dim lblSingle As Label = New Label()
                lblSingle.CssClass = "blackfont"

                lblSingle.Text = "<table width=100% cellspacing=2 cellpadding=2>"
                'lblSingle.Text &= "<b><div class='blackfont' style='float:left;width:120px;'>" & att.Name & "</div></b>"
                If att.Description <> String.Empty Then
                    'lblSingle.Text &= "<span class=smalltext>" & att.Description & "</span> "
                    lblSingle.Text &= "<b><div class='blackfont' style='float:left;width:120px;'>" & att.Description & "</div></b>"
                End If
                lblSingle.ID = "lbl" & att.Name + indexer.ToString()
                td.Controls.Add(lblSingle)
                indexer += 1
                Select Case att.SelectionType
                    Case AttributeType.SingleSelection
                        lblSingle.Text &= "&nbsp;"
                        Dim ddl As DropDownList = New DropDownList()
                        ddl.ID = att.Name
                        ddl.DataSource = att.Selections
                        ddl.DataTextField = "FormattedValue"
                        ddl.DataValueField = "Value"
                        ddl.DataBind()
                        ddl.SelectedIndex = 0
                        td.Controls.Add(ddl)
                        'lblSingle.Text &= "</td></tr>"

                    Case AttributeType.MultipleSelection
                        lblSingle.Text &= "<br>"
                        Dim chkList As CheckBoxList = New CheckBoxList()
                        chkList.ID = att.Name
                        chkList.DataSource = att.Selections
                        chkList.DataTextField = "Value"
                        chkList.DataValueField = "Value"
                        chkList.DataBind()
                        chkList.RepeatDirection = RepeatDirection.Horizontal
                        td.Controls.Add(chkList)
                    Case AttributeType.UserInput
                        lblSingle.Text &= "&nbsp;"
                        Dim t As TextBox = New TextBox()
                        t.ID = att.Name
                        t.TextMode = TextBoxMode.MultiLine
                        t.Height = Unit.Pixel(40)
                        t.Width = Unit.Pixel(300)
                        td.Controls.Add(t)
                        'lblSingle.Text &= "</td></tr>"
                End Select
                'lblSingle.Text &= "</table>"
                'td.Controls.Add(lblSingle)
                tr.Cells.Add(td)
                tbl.Rows.Add(tr)
            Next att
            pnlAttControls.Controls.Add(tbl)
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'if (!Page.IsPostBack)
        LoadControls()
    End Sub
End Class

