﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UploadSpaceAllocationData.ascx.vb" Inherits="Controls_UploadSpaceAllocationData" %>


<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <asp:HyperLink ID="hyp" runat="server" Text=" Click here to view the template" NavigateUrl="~/Masters/Mas_Webfiles/Upload SpaceAllocation Data.xlsx"></asp:HyperLink>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <label class="col-md-4 control-label">Upload Document  (Only Excel ) <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                    ControlToValidate="fpBrowseDoc" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" ControlToValidate="fpBrowseDoc"
                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                    ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> </asp:RegularExpressionValidator>
                <div class="col-md-4">
                    <div class="btn btn-default">
                        <i class="fa fa-folder-open-o fa-lg"></i>
                        <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                    </div>
                </div>
                <div class="col-md-4  control-label">
                    <asp:Button ID="btnbrowse" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Upload" ValidationGroup="Val1" OnClientClick="loadcontrol();" />
                    <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" PostBackUrl="~/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx" CausesValidation="False" />
                </div>
            </div>
        </div>
    </div>
   <%-- <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <div class="col-md-5  control-label">
                    <asp:Button ID="btnbrowse" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Upload" ValidationGroup="Val1" OnClientClick="loadcontrol();" />
                    <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" PostBackUrl="~/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx" CausesValidation="False" />
                </div>
                <div class="col-md-7">
                    <asp:HyperLink ID="hyp" runat="server" Text=" Click here to view the template" NavigateUrl="~/Masters/Mas_Webfiles/Upload SpaceAllocation Data.xlsx"></asp:HyperLink>
                </div>
            </div>
        </div>
    </div>--%>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnExport" runat="server" Text="Export To Excel" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <asp:GridView ID="GridView1" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped">
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
        </asp:GridView>
    </div>
</div>
