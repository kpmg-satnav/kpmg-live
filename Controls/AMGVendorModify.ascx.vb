Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AMGVendorModify
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Not Page.IsValid Then
            Exit Sub
        End If

        Dim AVR_CODE As String = Request.QueryString("code")
        Dim AVR_NAME As String = txtName.Text

        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDT_AMG_VENDOR")
            sp1.Command.AddParameter("@AVR_CODE", AVR_CODE, DbType.String)
            sp1.Command.AddParameter("@AVR_NAME", AVR_NAME, DbType.String)
            sp1.Command.AddParameter("@AVR_ADDR", txtAddress.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_CITY", ddlCity.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AVR_PHNO", txtPhone.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_MOBILE_PHNO", txtMobile.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_EMAIL", txtEmail.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_STATUS", rdbtnStatus.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AVR_STA_ID", rdbtnStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AVR_UPT_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@AVR_REMARKS", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_ACCNO", txtBankAcNo.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_BRN_NAME", txtBranchName.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_IFSC_CODE", txtIFSCCode.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_BANK_NAME", txtBankName.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_PAN_NO", txtPANno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_TAN_NO", txtTANno.Text, DbType.String)
            sp1.Command.AddParameter("@AVR_STATE", ddlState.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@AVR_GSTNO", txtgsttno.Text, DbType.String)
            'sp1.Command.AddParameter("@AVR_GIR_NO", txtGIRno.Text, DbType.String)
            'sp1.Command.AddParameter("@AVR_LSTNO", txtlstno.Text, DbType.String)
            'sp1.Command.AddParameter("@AVR_WSTNO", txtwctno.Text, DbType.String)
            'sp1.Command.AddParameter("@AVR_CSTNO", txtcstno.Text, DbType.String)
            'sp1.Command.AddParameter("@AVR_STCIRCLE", txtstcircle.Text, DbType.String)
            sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp1.ExecuteScalar()

            For i As Integer = 0 To lstTypes.Items.Count - 1
                If lstTypes.Items(i).Selected = True Then
                    InsertVendorType(AVR_CODE, lstTypes.Items(i).Text, lstTypes.Items(i).Value)
                End If
            Next
            For j As Integer = 0 To chkVendorCat.Items.Count - 1
                If chkVendorCat.Items(j).Selected = True Then
                    InsertVendorCategory(AVR_CODE, chkVendorCat.Items(j).Value)
                End If
            Next
            lblMsg.Text = "Vendor Details Modified Successfully"
            btnSubmit.Enabled = False
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub Cleardata()
        txtName.Text = ""
        txtAddress.Text = ""
        ddlCity.SelectedValue = 0
        ' ddlState.SelectedValue = 0
        'txtCountry.Text = ""
        txtPhone.Text = ""
        txtMobile.Text = ""
        txtEmail.Text = ""
        'ddlUser.SelectedItem.Value = 0
        txtRemarks.Text = ""
        txtBankAcNo.Text = ""
        txtBankName.Text = ""
        txtBranchName.Text = ""
        txtIFSCCode.Text = ""
        'txtcstno.Text = ""
        'txtstcircle.Text = ""
        txtPANno.Text = ""
        'txtGIRno.Text = ""
        txtTANno.Text = ""
        'txtlstno.Text = ""
        'txtwctno.Text = ""
        lstTypes.ClearSelection()
        chkVendorCat.ClearSelection()
    End Sub
    Private Sub BindState()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_BIND_STATES")
        ddlState.DataSource = sp.GetReader
        ddlState.DataTextField = "AM_ST_NAME"
        ddlState.DataValueField = "AM_ST_CODE"
        ddlState.DataBind()
        ddlState.Items.Insert(0, "--Select--")
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = "/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx"
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using

        'revEmail.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
        RegularExpressionValidator1.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegularExpressionValidator2.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()


        If Not IsPostBack Then
            FillTypes()
            BindState()
            Get_City()
            Dim AVR_CODE As String = Request.QueryString("code")
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSETVENDOR_DETAILS")
            sp3.Command.AddParameter("@AVR_CODE", AVR_CODE, DbType.String)
            Dim DS3 As New DataSet
            DS3 = sp3.GetDataSet()

            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_VEN_TYPE_AND_CAT")
            sp4.Command.AddParameter("@VEN_CODE", AVR_CODE, DbType.String)
            Dim DS4 As New DataSet
            DS4 = sp4.GetDataSet()

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CHECK_VEN_CODE_CATEGORY")
            sp1.Command.AddParameter("@VEN_CODE", AVR_CODE, DbType.String)
            sp1.Command.AddParameter("@COMPANYID", Session("COMPANYID"))
            Dim CNT As Integer = sp1.ExecuteScalar()

            If DS3.Tables(0).Rows.Count > 0 Then
                txtName.Text = DS3.Tables(0).Rows(0).Item("AVR_NAME")
                txtAddress.Text = DS3.Tables(0).Rows(0).Item("AVR_ADDR")
                Dim li1 As New ListItem
                li1 = ddlCity.Items.FindByText(DS3.Tables(0).Rows(0).Item("AVR_CITY"))
                If Not li1 Is Nothing Then
                    ddlCity.ClearSelection()
                    li1.Selected = True
                End If
                Dim li2 As New ListItem
                li2 = ddlState.Items.FindByText(DS3.Tables(0).Rows(0).Item("AVR_STATE_ID"))
                If Not li2 Is Nothing Then
                    ddlState.ClearSelection()
                    li2.Selected = True
                End If

                txtPhone.Text = DS3.Tables(0).Rows(0).Item("AVR_PHNO")
                txtMobile.Text = DS3.Tables(0).Rows(0).Item("AVR_MOBILE_PHNO")
                txtEmail.Text = DS3.Tables(0).Rows(0).Item("AVR_EMAIL")
                txtBankName.Text = DS3.Tables(0).Rows(0).Item("AVR_BANK_NAME")
                txtBankAcNo.Text = DS3.Tables(0).Rows(0).Item("AVR_ACCNO")
                txtBranchName.Text = DS3.Tables(0).Rows(0).Item("AVR_BRN_NAME")
                txtIFSCCode.Text = DS3.Tables(0).Rows(0).Item("AVR_IFSC_CODE")
                Dim lblStatus As Integer = DS3.Tables(0).Rows(0).Item("AVR_STATUS")
                txtgsttno.Text = DS3.Tables(0).Rows(0).Item("AVR_GSTNO")

                txtPANno.Text = DS3.Tables(0).Rows(0).Item("AVR_PAN_NO")
                txtTANno.Text = DS3.Tables(0).Rows(0).Item("AVR_TAN_NO")
                'txtlstno.Text = DS3.Tables(0).Rows(0).Item("AVR_LSTNO")
                'txtGIRno.Text = DS3.Tables(0).Rows(0).Item("AVR_GIR_NO")
                'txtwctno.Text = DS3.Tables(0).Rows(0).Item("AVR_WSTNO")
                'txtcstno.Text = DS3.Tables(0).Rows(0).Item("AVR_CSTNO")
                'txtstcircle.Text = DS3.Tables(0).Rows(0).Item("AVR_STCIRCLE")
                Dim cntSelect As Integer = 0
                Dim cntVenCat As Integer = 0
                If DS4.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To chkVendorCat.Items.Count - 1
                        For j As Integer = 0 To DS4.Tables(0).Rows.Count - 1
                            If chkVendorCat.Items(i).Value = DS4.Tables(0).Rows(j).Item("CAT_NAME") Then
                                chkVendorCat.Items(i).Selected = True
                                If CNT > 0 Then
                                    If chkVendorCat.Items(i).Value = "Service" Then
                                        chkVendorCat.Items(i).Enabled = False
                                        lblMsg.Visible = True
                                        lblMsg.Text = "Vendor is selected for AMC under Service Category, you can not modify it."
                                    End If

                                End If
                                cntVenCat += 1
                            End If
                        Next
                    Next
                    'For i As Integer = 0 To lstTypes.Items.Count - 1
                    '    For j As Integer = 0 To DS4.Tables(1).Rows.Count - 1
                    '        If lstTypes.Items(i).Value = DS4.Tables(1).Rows(j).Item("TYPE_ID") Then
                    '            lstTypes.Items(i).Selected = True
                    '            lstTypes.Items(i).Enabled = False
                    '            cntSelect += 1
                    '        End If
                    '    Next
                    'Next

                    For j As Integer = 0 To DS4.Tables(1).Rows.Count - 1
                        lstTypes.Items.FindByValue(DS4.Tables(1).Rows(j).Item("TYPE_ID")).Selected = True
                    Next
                    If cntVenCat = 0 Then
                        lblMsg.Text = "Please Select Vendor Category."
                        Exit Sub
                    End If
                    'If cntSelect = 0 Then
                    '    lblMsg.Text = "Please Select Vendor Type."
                    '    Exit Sub
                    'End If
                End If


                If lblStatus = 0 Then
                    rdbtnStatus.SelectedValue = 0
                Else
                    rdbtnStatus.SelectedValue = 1
                End If

                txtRemarks.Text = DS3.Tables(0).Rows(0).Item("AVR_REMARKS")
            End If


        End If
    End Sub

    Private Sub FillTypes()


        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TYPES")
        Dim dummy As String = sp.Command.CommandSql

        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables.Count > 0 Then

            lstTypes.DataSource = ds
            lstTypes.DataTextField = "VT_TYPE"
            lstTypes.DataValueField = "ID"
            lstTypes.DataBind()
        End If

    End Sub
    Private Sub InsertVendorCategory(ByVal VendorCode As String, ByVal VendorCatName As String)
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@AMG_VEN_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = VendorCode
        param(1) = New SqlParameter("@CAT_NAME", SqlDbType.NVarChar, 200)
        param(1).Value = VendorCatName
        param(2) = New SqlParameter("@COMPANYID", SqlDbType.Int)
        param(2).Value = Session("COMPANYID")
        ObjSubsonic.GetSubSonicExecute("INSERT_VERDOR_CATEGORY", param)
    End Sub

    Private Sub InsertVendorType(ByVal VendorCode As String, ByVal VendorTypeCode As String, ByVal VendorTypeId As Integer)
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@AMG_VENDOR_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = VendorCode
        param(1) = New SqlParameter("@TYPE_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = VendorTypeCode
        param(2) = New SqlParameter("@TYPE_ID", SqlDbType.Int)
        param(2).Value = VendorTypeId
        param(3) = New SqlParameter("@COMPANYID", SqlDbType.Int)
        param(3).Value = Session("COMPANYID")
        ObjSubsonic.GetSubSonicExecute("INSERT_VERDORTYPE", param)

    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmAMGVendorNewRecord1.aspx")
    End Sub

    'Protected Sub ddlState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlState.SelectedIndexChanged
    '    If ddlState.SelectedIndex > 0 Then
    '        Get_City()
    '    Else
    '        ddlCity.Items.Clear()
    '        ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))
    '        ddlCity.SelectedValue = 0
    '    End If
    'End Sub
    Private Sub Get_City()
        Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITY_ALL")
        sp4.Command.AddParameter("dummy", "", DbType.String)
        sp4.Command.AddParameter("@USR_ID", Session("Uid"), DbType.String)
        Dim ds4 As New DataSet
        ds4 = sp4.GetDataSet()
        ddlCity.DataSource = ds4
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
End Class
