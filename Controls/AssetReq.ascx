<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssetReq.ascx.vb" Inherits="Controls_AssetReq" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">



    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.gvItems.Rows.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }


        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkSelect";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }


        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvItems.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>

</head>
<body>
    <%--    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>--%>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">                  
                    </asp:Label>
                </div>
            </div>
        </div>
    </div>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" CssClass="alert alert-danger" />
    <div class="row" id="hide1" runat="server">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Upload Document(Only Excel)<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                        ControlToValidate="fpBrowseDoc" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" ControlToValidate="fpBrowseDoc"
                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only Excel file allowed"
                        ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                    </asp:RegularExpressionValidator>
                    <div class="col-md-6">
                        <div class="btn-default">
                            <i class="fa fa-folder-open-o fa-lg"></i>
                            <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-5  control-label">
                        <asp:Button ID="btnUpload" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Upload" ValidationGroup="Val1" />
                    </div>
                    <div>
                        <asp:HyperLink ID="hyp" runat="server" Text=" Click here to View the Template" NavigateUrl="~/Masters/Mas_Webfiles/CapitalAssetReq.xlsx"></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnExport" runat="server" Text="Export To Excel" CssClass="btn btn-primary custom-button-color" />
            </div>
        </div>
    </div>

    <div class="row" style="padding-bottom: 17px">
        <div class="col-md-12">
            <div class="form-group">
                <asp:GridView ID="GvUpload" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped">
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>

            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Employee Id</label>
                <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Category</label>
                <asp:RequiredFieldValidator ID="rfvastcat" runat="server" ControlToValidate="ddlAstCat"
                    Display="None" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1"
                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddlAstCat" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Sub Category </label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstSubCat"
                    Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Brand/Make </label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Brand/Make !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Model </label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                    Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Location </label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                    ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
                </asp:DropDownList>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnSearch" runat="server" CausesValidation="true" CssClass="btn btn-primary custom-button-color" Text="Search" />
                <asp:Button ID="btnclear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" CausesValidation="False" />
            </div>
        </div>
    </div>
    <div id="pnlItems" runat="server">
        <div class="row">
            <fieldset>
                <legend>Assets List</legend>
            </fieldset>
        </div>
        <div class="row">


            <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 320px">
                <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false"
                    EmptyDataText="No Asset Requisition Found." ShowHeader="True" CssClass="table table-condensed table-bordered table-hover table-striped">
                    <Columns>
                        <asp:TemplateField HeaderText="Asset Model" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:Label ID="lblproductname" Text='<%#Eval("AST_MD_NAME") %>' runat="server"></asp:Label>
                                <asp:Label ID="lblmdcode" Text='<%#Eval("AST_MD_CODE") %>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="Category" HeaderText="Category" ItemStyle-HorizontalAlign="left">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category" ItemStyle-HorizontalAlign="left">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="manufacturer" HeaderText="Brand" ItemStyle-HorizontalAlign="left">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:RequiredFieldValidator ID="rfvqty" runat="server" Display="None" ErrorMessage="Please Enter Quantity"
                                    ControlToValidate="txtQty" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="2"></asp:TextBox>
                                <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("AST_MD_CODE") %>' Visible="false"></asp:Label>


                                <asp:Label ID="lbl_vt_code" Text='<%#Eval("VT_CODE") %>' runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lbl_ast_subcat_code" Text='<%#Eval("AST_SUBCAT_CODE") %>' runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lbl_manufactuer_code" Text='<%#Eval("manufactuer_code") %>' runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lbl_ast_md_code" Text='<%#Eval("AST_MD_CODE") %>' runat="server" Visible="false"></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Estimated Cost" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:TextBox ID="txtEstCost" runat="server" Width="50px" MaxLength="10">0</asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" ItemStyle-HorizontalAlign="Center" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                    ToolTip="Click to check all" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                            </ItemTemplate>
                            <ItemStyle Width="50px" HorizontalAlign="Center" />
                        </asp:TemplateField>


                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="row">
        <br />
    </div>
    <div class="row" id="remarksAndSubmitBtn" runat="server">
         <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Upload Document<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvdoc" runat="server" Display="None" ErrorMessage="Please Select Document"
                    ControlToValidate="fpBrowseDocc" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="btn-default" style="display: block!important;">
                    <i class="fa fa-folder-open-o fa-lg"></i>
                    <asp:FileUpload ID="fpBrowseDocc" runat="Server" Width="90%" AllowMultiple="True" />
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="form-group">
                <label class="col-md-12">Remarks<span style="color: red;">*</span> </label>
                <%--<asp:RegularExpressionValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtRemarks"
                ErrorMessage="Please Enter Valid Remarks" Display="None"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>--%>
                <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                    Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val2"
                    Enabled="true"></asp:RequiredFieldValidator>
                <div class="col-md-12">
                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
       
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnSubmit" Text="Submit" runat="server" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq();" />
            </div>
        </div>
    </div>
    <%--        </ContentTemplate>
    </asp:UpdatePanel>--%>
</body>
</html>

<script type="text/ecmascript">
    function refreshSelectpicker() {
        $("#<%=ddlAstCat.ClientID%>").selectpicker();
        $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
        $("#<%=ddlAstBrand.ClientID%>").selectpicker();
        $("#<%=ddlLocation.ClientID%>").selectpicker();
        $("#<%=ddlAstModel.ClientID%>").selectpicker();
        $("#<%=ddlEmp.ClientID%>").selectpicker();

    };
    refreshSelectpicker();
</script>
<%--  <link rel="stylesheet " type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">--%>

<%-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

