<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InterAssetMovementReportDtls.ascx.vb" Inherits="Controls_InterAssetMovementReportDtls" %>
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Inter Asset Movement Report Details
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <div id="Div1">
        <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Inter Asset Movement Report Details</strong></td>
                <td>
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                    <table width="100%" cellpadding="2px">
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left">
                                <table width="100%" cellpadding="2">
                                    <tr>
                                        <td style="height: 26px">
                                            From Location</td>
                                        <td style="height: 26px">
                                            <asp:DropDownList ID="ddlSLoc" runat="server" Width="275px" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From Tower</td>
                                        <td>
                                            <asp:DropDownList ID="ddlSTower" runat="server" Width="275px" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From Floor</td>
                                        <td>
                                            <asp:DropDownList ID="ddlSFloor" runat="server" Width="275px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Person To Receive Assets</td>
                                        <td>
                                            <asp:DropDownList ID="ddlEmp" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <fieldset id="fldNewReq" runat="server" visible="false">
                                                <legend>New Request Summary</legend>
                                                <table cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td width="250">
                                                            Request raised By</td>
                                                        <td>
                                                            <asp:Label ID="txtPendingApproved" CssClass="bodytext" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Remarks</td>
                                                        <td>
                                                            <asp:Label ID="txtPendingRemarks" CssClass="bodytext" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Date</td>
                                                        <td>
                                                            <asp:Label ID="txtPendingDate" CssClass="bodytext" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <fieldset id="fldITApproval" runat="server" visible="false">
                                                <legend>IT Approval/Reject Summary</legend>
                                                <table id="tblITSummary" runat="server" cellpadding="0" cellspacing="0" border="0"
                                                    visible="false">
                                                    <tr>
                                                        <td width="250">
                                                            Request Approved by</td>
                                                        <td>
                                                            <asp:Label ID="lblITApproval" CssClass="bodytext" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Remarks</td>
                                                        <td>
                                                            <asp:Label ID="lblITRemarks" CssClass="bodytext" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Date</td>
                                                        <td>
                                                            <asp:Label ID="lblITDate" CssClass="bodytext" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                              <asp:Button ID="btnBack" runat="server" CssClass="button" Text="Back" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
</div>
