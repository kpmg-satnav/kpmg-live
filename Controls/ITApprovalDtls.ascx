<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ITApprovalDtls.ascx.vb"
    Inherits="Controls_ITApprovalDtls" %>
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div class="row">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Val1" CssClass="alert alert-danger" ForeColor="Red" />

            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-12 control-label">From Location</label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlSLoc" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-12 control-label">To Location</label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlDLoc" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" Enabled="False">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-12 control-label">Person Sending</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="ddlFromPerson" runat="server" CssClass="form-control" Width="200px" Enabled="False"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-12 control-label">Person Receiving</label>
                        <div class="col-md-7">
                            <asp:TextBox ID="ddlToPerson" runat="server" CssClass="form-control" Width="200px" Enabled="False"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Sending Date </label>
                    <div class="input-group date" style="width: 150px" id='fromdate'>
                        <asp:TextBox runat="server" type="text" class="form-control" placeholder="mm/dd/yyyy" ID="txtSendDate" Enabled="false"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('txtSendDate')"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-12 control-label">Remarks<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRemarks"
                            Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="multiline" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 10px">
            <div class="col-md-12">
                <asp:GridView ID="gvItems" runat="server" EmptyDataText="No Assets Found." AllowPaging="true"
                    PageSize="10" CssClass="table table-condensed table-bordered table-hover table-striped" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:Label ID="lblAAS_AAT_CODE" runat="server" Text='<%#Eval("AAT_CODE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:Label ID="lblAAT_NAME" runat="server" Text='<%#Eval("AAT_DESC")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>

        <br />
        <div class="row">
            <div class="col-md-12 text-right">
                <div class="form-group">
                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Approve" ValidationGroup="Val1" />
                    <asp:Button ID="btnReject" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reject" />
                    <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" />

                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<script>
    function refreshSelectpicker() {
        $("#<%=ddlDLoc.ClientID%>").selectpicker();
            $("#<%=ddlSLoc.ClientID%>").selectpicker();


    }
    refreshSelectpicker();
</script>
