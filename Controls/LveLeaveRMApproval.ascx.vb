Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_LveLeaveRMApproval
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDep()
            'Dept()
            BindRM()
            'BindLeaveType()
            BindDetails(Request.QueryString("ReqID"))
            txtAddress.ReadOnly = True
            txtAssociateID.ReadOnly = True
            txtAssociateName.ReadOnly = True
            txtContactNo.ReadOnly = True
            txtDesig.ReadOnly = True
            txtFromDate.ReadOnly = True
            txtNoLeaves.ReadOnly = True
            'txtReason.ReadOnly = True
            txtToDate.ReadOnly = True
            txtTotalLeave.ReadOnly = True
        End If
    End Sub
    Private Sub Dept()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_USER_DEPT")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDep()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DEP")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDep.DataSource = sp.GetDataSet()
            ddlDep.DataTextField = "DEP_NAME"
            ddlDep.DataValueField = "DEP_CODE"
            ddlDep.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindRM()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_EMPLOYEES_RM")
            'sp.Command.AddParameter("@dep_code", ddlDep.SelectedItem.Value, DbType.String)
            ddlRM.DataSource = sp.GetDataSet()
            ddlRM.DataTextField = "AUR_FIRST_NAME"
            ddlRM.DataValueField = "AUR_ID"
            ddlRM.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    'Private Sub BindLeaveType()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_TYPE_GETLEAVE_TYPE1")
    '        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
    '        ddlLeaveType.DataSource = sp.GetDataSet()
    '        ddlLeaveType.DataTextField = "LVE_LEAVE_TYPE"
    '        ddlLeaveType.DataValueField = "LVE_LEAVE_ID"
    '        ddlLeaveType.DataBind()
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    Private Sub BindDetails(ByVal ReqID As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_CANCEL_REQUISITION_GETBYREQID")
            sp.Command.AddParameter("@REQID", ReqID, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            'If ds.Tables(0).Rows.Count > 0 Then
            txtAssociateName.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
            txtAssociateID.Text = ds.Tables(0).Rows(0).Item("LMS_AUR_NO")
            ddlDep.ClearSelection()
            ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
            ddlRM.ClearSelection()
            ddlRM.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_REPORTING_TO")).Selected = True

            txtDesig.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
            txtContactNo.Text = ds.Tables(0).Rows(0).Item("LMS_LEAVE_ID")
            txtFromDate.Text = ds.Tables(0).Rows(0).Item("LMS_FDATE")
            txtToDate.Text = ds.Tables(0).Rows(0).Item("LMS_TDATE")
            cboHr.ClearSelection()
            cboHr.Items.FindByValue(ds.Tables(0).Rows(0).Item("LMS_FTIMEP")).Selected = True
            cboMin.ClearSelection()
            cboMin.Items.FindByValue(ds.Tables(0).Rows(0).Item("LMS_FTIMEM")).Selected = True
            ddlHH.ClearSelection()
            ddlHH.Items.FindByValue(ds.Tables(0).Rows(0).Item("LMS_TTIMEP")).Selected = True
            ddlMM.ClearSelection()
            ddlMM.Items.FindByValue(ds.Tables(0).Rows(0).Item("LMS_TTIMEM")).Selected = True
            txtAddress.Text = ds.Tables(0).Rows(0).Item("LMS_CNT_ADDRESS")
            txtReason.Text = ds.Tables(0).Rows(0).Item("LMS_LEAVE_REASON")
            txtstore.Text = ds.Tables(0).Rows(0).Item("LMS_LEAVE_TYPE1")
            ddlLeaveType.Items.Insert(0, New ListItem(ds.Tables(0).Rows(0).Item("LMS_LEAVE_TYPE"), 0))
            txtNoLeaves.Text = ds.Tables(0).Rows(0).Item("LMS_NO_DAYS")
            txtTotalLeave.Text = ds.Tables(0).Rows(0).Item("LMS_TOT_LEAVES")
            ' End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try

            Dim REQID As String = Request.QueryString("REQID")
            Update_Leaves(REQID)
            Update_Employee_Leaves(REQID)
            Mail_RmApproved(REQID)
            Response.Redirect("status.aspx?StaId=1&ReqID=" + REQID)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Mail_RmApproved(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strMsgStr As String                            'To hold the sql query
        Dim strReqId As String                             'To hold the Requisition ID
        Dim strReqDate As String                           'To hold the requisition date
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strLeaveType As String                          'To hold the Leave Type
        Dim strHrEmail As String                              'To hold the HR Email
        Dim noofdays, fromdt, todt, Contact, reason As String
        strSubj = " Leave Approval."
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_MAIL_REQUEST1")
        sp.Command.AddParameter("@LMS_LEAVE_REQ", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strReqId = ds.Tables(0).Rows(0).Item("LMS_REQ_ID")
        strReqDate = ds.Tables(0).Rows(0).Item("LMS_REQ_DATE")
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        noofdays = ds.Tables(0).Rows(0).Item("LMS_NO_DAYS")
        fromdt = ds.Tables(0).Rows(0).Item("LMS_FDATE")
        todt = ds.Tables(0).Rows(0).Item("LMS_TDATE")
        Contact = ds.Tables(0).Rows(0).Item("LMS_CNT_NO")
        reason = ds.Tables(0).Rows(0).Item("LMS_LEAVE_REASON")
        strLeaveType = ds.Tables(0).Rows(0).Item("LMS_LEAVE_TYPE")

        Dim strfrommail, strfromname As String

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USER_MAIL")
        sp1.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim ObjDR As SqlDataReader
        ObjDR = sp.GetReader()
        While ObjDR.Read
            strfrommail = "amantraadmin@satnavtech.com"
            strfromname = "amantraadmin"
        End While
        ObjDR.Close()
        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "amantraadmin"




        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform you that following are the  Details of Approved Leave Request." & "<br>" & _
"<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
"<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
"<td width=50% align=left>" & strEmpName & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
"<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>From " & fromdt & " to " & todt & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Reason: </U></td>" & _
"<td width=50% align=left>" & reason & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Contact No: </U></td>" & _
"<td width=50% align=left>" & Contact & "</td></tr>" & _
"<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
"Thanking You, " & "<br><br>" & _
"Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL")
        sp2.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                    "This is to inform you that following request by " & strEmpName & " for Leave has been Approved." & "<br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & strEmpName & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
                    "<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>From " & fromdt & " to " & todt & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Reason: </U></td>" & _
"<td width=50% align=left>" & reason & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Type Of Leave: </U></td>" & _
"<td width=50% align=left>" & strLeaveType & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Contact No: </U></td>" & _
"<td width=50% align=left>" & Contact & "</td></tr>" & _
"<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strEmpName & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner. 
        strSubj = " Leave Request Approved - by " & strAppEmpName & " from " & fromdt & " - " & todt & " for " & noofdays & " days."
        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, strReqId, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, strReqId, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal MailFrom As String, ByVal Id As String, ByVal subject As String, ByVal msg As String, ByVal strHrEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_MAIL_ADD")
        sp.Command.AddParameter("@AMT_ID", Id, DbType.String)
        sp.Command.AddParameter("@AMT_MESSAGE", msg, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_TO", MailTo, DbType.String)
        sp.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
        sp.Command.AddParameter("@AMT_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@AMT_FROM", MailFrom, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_CC", strHrEmail, DbType.String)
        sp.Command.AddParameter("@AMT_BDYFRMT", 0, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Private Sub Update_Leaves(ByVal Reqid As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVES_RM_APPROVAL1")
            sp.Command.AddParameter("REQID", Reqid, DbType.String)
            sp.Command.AddParameter("@REASON", txtReason.Text, DbType.String)
            sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Update_Employee_Leaves(ByVal reqid As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_EMPLOYEE_LEAVES_UPDATE")
            sp.Command.AddParameter("@Reqid", reqid, DbType.String)
            sp.Command.AddParameter("@Leave_type", txtstore.Text, DbType.Int32)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Dim REQID As String = Request.QueryString("REQID")
            Reject(REQID)
            Mail_RmRejected(REQID)
            Response.Redirect("status.aspx?StaId=1&ReqID=" + REQID)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Reject(ByVal REQID As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_EMPLOYEE_LEAVE_REJECT")
        sp.Command.AddParameter("@Reqid", REQID, DbType.String)
        sp.ExecuteScalar()
    End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    'Response.Write("frmLveApproval.aspx")
    '    Response.Redirect("frmLveApproval.aspx")

    'End Sub
    Private Sub Mail_RmRejected(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strMsgStr As String                            'To hold the sql query
        Dim strReqId As String                             'To hold the Requisition ID
        Dim strReqDate As String                           'To hold the requisition date
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strLeaveType As String                          'To hold the Leave Type
        Dim strHrEmail As String                              'To hold the HR Email
        Dim noofdays, fromdt, todt, Contact, reason As String
        strSubj = " Leave Rejected."
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_MAIL_REQUEST2")
        sp.Command.AddParameter("@LMS_LEAVE_REQ", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strReqId = ds.Tables(0).Rows(0).Item("LMS_REQ_ID")
        strReqDate = ds.Tables(0).Rows(0).Item("LMS_REQ_DATE")
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        noofdays = ds.Tables(0).Rows(0).Item("LMS_NO_DAYS")
        fromdt = ds.Tables(0).Rows(0).Item("LMS_FDATE")
        todt = ds.Tables(0).Rows(0).Item("LMS_TDATE")
        Contact = ds.Tables(0).Rows(0).Item("LMS_CNT_NO")
        reason = ds.Tables(0).Rows(0).Item("LMS_LEAVE_REASON")
        strLeaveType = ds.Tables(0).Rows(0).Item("LMS_LEAVE_TYPE")

        Dim strfrommail, strfromname As String

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USER_MAIL")
        sp1.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim ObjDR As SqlDataReader
        ObjDR = sp.GetReader()
        While ObjDR.Read
            strfrommail = "amantraadmin@satnavtech.com"
            strfromname = "amantraadmin"
        End While
        ObjDR.Close()
        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "amantraadmin"




        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform you that following are the  Details of Rejected Leave Request." & "<br>" & _
"<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
"<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
"<td width=50% align=left>" & strEmpName & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
"<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>From " & fromdt & " to " & todt & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Reason: </U></td>" & _
"<td width=50% align=left>" & reason & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Contact No: </U></td>" & _
"<td width=50% align=left>" & Contact & "</td></tr>" & _
"<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
"Thanking You, " & "<br><br>" & _
"Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL")
        sp2.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                    "This is to inform you that following request by " & strEmpName & " for Leave has been Rejected." & "<br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & strEmpName & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
                    "<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
"<td width=50% align=left>From " & fromdt & " to " & todt & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Reason: </U></td>" & _
"<td width=50% align=left>" & reason & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Type Of Leave: </U></td>" & _
"<td width=50% align=left>" & strLeaveType & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Contact No: </U></td>" & _
"<td width=50% align=left>" & Contact & "</td></tr>" & _
"<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strEmpName & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner. 
        strSubj = " Leave Request Rejected - by " & strAppEmpName & " from " & fromdt & " - " & todt & " for " & noofdays & " days."
        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, strReqId, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, strReqId, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Try
            Response.Redirect("frmLveApproval.aspx")

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
