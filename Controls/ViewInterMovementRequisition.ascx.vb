Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_ViewInterMovementRequisition
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim strLCM_CODE As String
    Dim strTWR_CODE As String
    Dim strfloor As String
    Dim StrEmp As String
    Dim Strast As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                BindGrid()
            End If
            
        End If
    End Sub

    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ASSET_GET_INTERREQUISITIONS")
            gvgriditems.DataSource = sp.GetDataSet()
            gvgriditems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvgriditems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvgriditems.PageIndexChanging
        gvgriditems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvgriditems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvgriditems.RowCommand
        If e.CommandName = "ViewDetails" Then
            Response.Redirect("InterMovementRequisitionDTLS.aspx?Req_id=" & e.CommandArgument)
        End If
    End Sub
End Class
