Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_LveViewRequisition
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGridC()
        End If
    End Sub

    Private Sub BindGridC()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_COMPOFF_APPROVAL")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvItemsC.DataSource = sp.GetDataSet()
            gvItemsC.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItemsC_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItemsC.PageIndexChanging
        gvItemsC.PageIndex = e.NewPageIndex()
        BindGridC()
    End Sub
End Class
