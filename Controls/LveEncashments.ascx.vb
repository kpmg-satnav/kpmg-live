Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_LveEncashments
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDep()
            Dept()
            BindRM()
            BindDetails()
            BindLeaves()
            BindLeaveType()
            BindCompLeaves()
            ddlDep.Enabled = False
            ddlRM.Enabled = False
            btnSubmit.Visible = False
            txtAssociateID.ReadOnly = True
            txtAssociateName.ReadOnly = True
            txtCompLeaves.ReadOnly = True
            txtContactNo.ReadOnly = True
            txtContactNo.Text = 30
            txtDesig.ReadOnly = True
            txtNoLeaves.ReadOnly = True
            txtPleaves.ReadOnly = True
        End If
    End Sub
    Private Sub BindDep()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DEP")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDep.DataSource = sp.GetDataSet()
            ddlDep.DataTextField = "DEP_NAME"
            ddlDep.DataValueField = "DEP_CODE"
            ddlDep.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Dept()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_USER_DEPT")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindRM()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_EMPLOYEES")
            sp.Command.AddParameter("@dep_code", ddlDep.SelectedItem.Value, DbType.String)
            ddlRM.DataSource = sp.GetDataSet()
            ddlRM.DataTextField = "AUR_FIRST_NAME"
            ddlRM.DataValueField = "AUR_ID"
            ddlRM.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMANTRA_USER_GETASSOCIATE_DETAILS")
            sp.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtAssociateName.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
                txtAssociateID.Text = ds.Tables(0).Rows(0).Item("AUR_NO")
                ddlDep.ClearSelection()
                ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
                txtDesig.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
                'ddlRM.ClearSelection()
                ddlRM.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_REPORTING_TO")).Selected = True

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLeaveType()
        Try
            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_TYPE_GETLEAVE_TYPE")
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_TYPE_GETLEAVE_TYPE2")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            ddlLeave.DataSource = sp.GetDataSet()
            ddlLeave.DataTextField = "LVE_LEAVE_TYPE"
            ddlLeave.DataValueField = "LVE_LEAVE_ID"
            ddlLeave.DataBind()
            ddlLeave.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLeaves()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_BALANCE_LEAVES")
            sp.Command.AddParameter("@AUR_ID", txtAssociateID.Text, DbType.String)
            sp.Command.AddParameter("@DEP_ID", ddlDep.SelectedItem.Value, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            txtPleaves.Text = ds.Tables(0).Rows(0).Item("LEAVES")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCompLeaves()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_BALANCE_COMP_LEAVES")
            sp.Command.AddParameter("@AUR_ID", txtAssociateID.Text, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            txtCompLeaves.Text = ds.Tables(0).Rows(0).Item("COMPLEAVES")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If txtEncash.Text = "" Then
                lblMsg.Text = "Please Enter 'Applied Days' for Encashment"
            ElseIf CDbl(txtEncash.Text) < 30 Then
                lblMsg.Text = "Sorry Minimum Leave Balance For Encashment is 30"
                btnSubmit.Visible = False
            Else

                Dim REQID As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") + "/" + txtAssociateID.Text
                Dim Req As Integer
                Req = ValidateReq(REQID)
                If Req = 0 Then
                    lblMsg.Text = "Your Request Has Been Submitted Already"
                    lblMsg.Visible = True
                Else
                    INSERT_LEAVE_ENCASH(REQID)
                    MAIL_LVEENCASH(REQID)
                    Response.Redirect("status.aspx?StaId=3&ReqId=" + REQID, False)
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub MAIL_LVEENCASH(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strMsgStr As String                            'To hold the sql query
        Dim strReqId As String                             'To hold the Requisition ID
        Dim strReqDate As String                           'To hold the requisition date
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strLeaveType As String                          'To hold the Leave Type
        Dim strHrEmail As String                              'To hold the HR Email
        Dim noofdays As String
        strSubj = "Leave Encashment Requisition."
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_ENCASH_MAIL_REQUEST")
        sp.Command.AddParameter("@LMS_LEAVE_REQ", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strReqId = ds.Tables(0).Rows(0).Item("LVE_REQ_ID")
        strReqDate = ds.Tables(0).Rows(0).Item("LVE_REQUESTED_ON")
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        noofdays = ds.Tables(0).Rows(0).Item("LVE_ENCASH_DAYS")
        Dim strfrommail, strfromname As String

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USER_MAIL_ENCASH")
        sp1.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim ObjDR As SqlDataReader
        ObjDR = sp.GetReader()
        While ObjDR.Read
            strfrommail = "amantraadmin@satnavtech.com"
            strfromname = "Employee Service Desk"
        End While
        ObjDR.Close()
        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "Employee Service Desk"




        'Mail message for the Requisitioner. 

        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform you that following are the  Details of Leave Encashment Requisition." & "<br>" & _
"<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
"<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
"<td width=50% align=left>" & strEmpName & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
"<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr></table>" & _
"Thanking You, " & "<br><br>" & _
"Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_ENCASH")
        sp2.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                    "This is to inform you that following request by " & strEmpName & " for Leave Encashment Requisition and is pending for Approval." & "<br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & strEmpName & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
                    "<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr></table>" & _
"Click here to <a href=""http://projects.a-mantra.com/amantraAMPS/EFM/EFM_Webfiles/frmLveRMApprovalEncash.aspx?ReqID=" & REQID & """>Accept / Reject</a><br><br>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strEmpName & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner. 
        strSubj = " Leave Encashment Requisition - by " & strEmpName & noofdays & " days."
        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, strReqId, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, strReqId, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal MailFrom As String, ByVal Id As String, ByVal subject As String, ByVal msg As String, ByVal strHrEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_MAIL_ADD")
        sp.Command.AddParameter("@AMT_ID", Id, DbType.String)
        sp.Command.AddParameter("@AMT_MESSAGE", msg, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_TO", MailTo, DbType.String)
        sp.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
        sp.Command.AddParameter("@AMT_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@AMT_FROM", MailFrom, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_CC", strHrEmail, DbType.String)
        sp.Command.AddParameter("@AMT_BDYFRMT", 0, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Private Function ValidateReq(ByVal REQID As String)
        Dim Req As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_VALIDATE_REQID")
        sp.Command.AddParameter("@REQID", REQID, DbType.String)
        Req = sp.ExecuteScalar()
        Return Req
    End Function
  


    Protected Sub btnLeaves_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeaves.Click
        If txtEncash.Text = "" Then
            lblMsg.Text = "Please Enter 'Applied Days' for Encashment"
        ElseIf CDbl(txtEncash.Text) < 30 Then
            lblMsg.Text = "Sorry Minimum Leave Balance For Encashment is 30"
            btnSubmit.Visible = False

        Else
            lblMsg.Text = ""
            Dim leavedays As Decimal
            leavedays = txtPleaves.Text
            If leavedays > 5 Then
                If txtEncash.Text <= leavedays - 5 Then
                    txtNoLeaves.Text = leavedays - (5 + txtEncash.Text)
                    btnSubmit.Visible = True
                Else

                    lblMsg.Text = "Selected Days  are More than Encash days!!"
                    btnSubmit.Visible = False
                End If
            Else
                lblMsg.Enabled = True
                lblMsg.Text = ""
                lblMsg.Text = "Sorry!You Cannot Encash"
                btnSubmit.Visible = False
            End If
        End If

    End Sub
    Private Sub INSERT_LEAVE_ENCASH(ByVal ReqID As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_ENCASH")
            sp.Command.AddParameter("@LVE_REQ_ID", ReqID, DbType.String)
            sp.Command.AddParameter("@LVE_EMP_ID", txtAssociateID.Text, DbType.String)
            sp.Command.AddParameter("@LVE_ENCASH_DAYS", txtEncash.Text, DbType.Decimal)
            sp.Command.AddParameter("@LVE_DSGN_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@LVE_LEAVETYPE", ddlLeave.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LVE_RM", ddlRM.SelectedValue, DbType.String)
            sp.Command.AddParameter("@LVE_ENCASHTYPE", ddlEncash.SelectedValue, DbType.String)
            sp.Command.AddParameter("@LVE_BALANCE", txtNoLeaves.Text, DbType.Decimal)

            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlLeave_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLeave.SelectedIndexChanged
       
        lblMsg.Text = ""
        If ddlLeave.SelectedItem.Value = 6 Or ddlLeave.SelectedItem.Value = 14 Then
            lblMsg.Text = "You Can't Encash Compensatory Leaves"
            btnSubmit.Visible = False
        ElseIf ddlLeave.SelectedItem.Value = 2 Or ddlLeave.SelectedItem.Value = 9 Then
            Dim leavedays As Decimal
            leavedays = txtPleaves.Text
            Dim remdays As Decimal
            'GET TOTAL LEAVE APPLIED DAYS
            Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_LMS_LEAVE_REQ")
            SP.Command.AddParameter("@AUR_ID", txtAssociateID.Text, DbType.String)
            Dim ds As New DataSet
            ds = SP.GetDataSet()
            remdays = ds.Tables(0).Rows(0).Item("Leaves")

            Dim leftdays As Decimal
            leftdays = leavedays - remdays
            If leftdays > 30 Then
                lblMsg.Text = "Encashment is Possible ! No.Of Days to Encash is " & leavedays - 30
                'btnSubmit.Visible = True
            End If
        Else
            btnSubmit.Visible = False
            lblMsg.Text = "Encashment is Possible only for Paid Leaves"
        End If
    End Sub
   
End Class
