Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class Controls_SelFloors
    Inherits System.Web.UI.UserControl
    Dim strSQL As String = String.Empty
    Public Shared spcids As String
    Dim objMsater As New clsMasters
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        'Put user code to initialize the page here

        Dim selfloor, seltower, towerflag, selBld, selCity As String
        Dim strMode As String
        Dim i
        selfloor = Request.QueryString("FLR_CODE")
        strMode = Request.QueryString("MODE")
        seltower = Request.QueryString("Twr_Code")
        selBld = Request.QueryString("BDG_CODE")
        'Session("Cty_Id") = drpdwnLocation.SelectedItem.Value
        selCity = Session("Cty_Id")
        If strMode = "EDIT" Then
            ' seltower = 0
            towerflag = 0
        End If

        'ObjCon.ConnectionString = conSql
        'ObjCon.Open()


        If Not IsPostBack() Then
            'Response.Write(selCity)

            '******************* Location binding ****************
            strSQL = "select lcm_code,LCM_name from " & Session("TENANT") & "."  & "location  where lcm_cty_id= '" & Session("Cty_Id") & "'"
            BindCombo(strSQL, drpdwnLocation, "LCM_name", "lcm_code")
            '*****************************************************
            drpdwnLocation.Items.FindByValue(Session("LOC_ID")).Selected = True
            txtMode.Text = Request.QueryString("MODE")



            strSQL = "select FLR_TWR_ID from  " & Session("TENANT") & "."  & "FLOOR where  FLR_CODE='" & selfloor & "' and flr_twr_id = '" & seltower & "' and flr_bdg_id = '" & selBld & "'"
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
            If ObjDR.Read() Then
                seltower = ObjDR("FLR_TWR_ID").ToString

            End If
            ObjDR.Close()


            strSQL = "select TWR_Code,TWR_NAME from  " & Session("TENANT") & "."  & "TOWER where TWR_BDG_ID in (select FLR_BDG_ID from  " & Session("TENANT") & "."  & "FLOOR where  FLR_CODE='" & selfloor & "') and TWR_LOC_ID = '" & selBld & "' and twr_cty_id= '" & Session("Cty_Id") & "'"
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
            If ObjDR.Read() Then
                towerflag = 1
            End If
            ObjDR.Close()

            If towerflag = 1 Then

                strSQL = "select TWR_code,TWR_NAME from  " & Session("TENANT") & "."  & "TOWER where (TWR_ID=0 or TWR_BDG_ID in (select FLR_BDG_ID from  " & Session("TENANT") & "."  & "FLOOR where FLR_CODE='" & selfloor & "')) and TWR_STA_ID=1  and TWR_LOC_ID = '" & selBld & "' and twr_cty_id= '" & Session("Cty_Id") & "'"
                ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
                drpdwnTwm_Id.DataSource = ObjDR
                drpdwnTwm_Id.DataValueField = "TWR_code"
                drpdwnTwm_Id.DataTextField = "TWR_NAME"
                drpdwnTwm_Id.DataBind()
                ObjDR.Close()


                For i = 0 To drpdwnTwm_Id.Items.Count - 1
                    If drpdwnTwm_Id.Items(i).Value <> "--Select--" And drpdwnTwm_Id.Items(i).Value = seltower Then
                        drpdwnTwm_Id.SelectedIndex = i
                        Exit For
                    End If
                Next

                strSQL = "select FLR_CODE,FLR_NAME from  " & Session("TENANT") & "."  & "FLOOR where  FLR_TWR_ID='" & seltower & "' and FLR_STA_ID=1"
                ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
                drpdwnFlm_Id.DataSource = ObjDR
                drpdwnFlm_Id.DataValueField = "FLR_CODE"
                drpdwnFlm_Id.DataTextField = "FLR_NAME"
                drpdwnFlm_Id.DataBind()
                ObjDR.Close()


                For i = 0 To drpdwnFlm_Id.Items.Count - 1
                    If drpdwnFlm_Id.Items(i).Value <> "--Select--" And drpdwnFlm_Id.Items(i).Value = selfloor Then
                        drpdwnFlm_Id.SelectedIndex = i
                        'txtFlr.Text = drpdwnFlm_Id.SelectedItem.Value
                        Exit For
                    End If
                Next
            Else
                drpdwnTwm_Id.Items.Insert("0", "No Towers")

                strSQL = "select FLR_CODE,FLR_NAME from  " & Session("TENANT") & "."  & "FLOOR where FLR_BDG_ID in (select FLR_BDG_ID from  " & Session("TENANT") & "."  & "FLOOR where  FLR_CODE='" & selfloor & "')"
                ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
                drpdwnFlm_Id.DataSource = ObjDR
                drpdwnFlm_Id.DataValueField = "FLR_CODE"
                drpdwnFlm_Id.DataTextField = "FLR_NAME"
                drpdwnFlm_Id.DataBind()
                ObjDR.Close()


                For i = 0 To drpdwnFlm_Id.Items.Count - 1
                    If drpdwnFlm_Id.Items(i).Value <> "--Select--" And drpdwnFlm_Id.Items(i).Value = selfloor Then
                        drpdwnFlm_Id.SelectedIndex = i
                        'txtFlr.Text = drpdwnFlm_Id.SelectedItem.Value
                        Exit For
                    End If
                Next
            End If
        End If

    End Sub

    Private Sub drpdwnTwm_Id_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpdwnTwm_Id.SelectedIndexChanged


        drpdwnFlm_Id.Items.Clear()
        strSQL = "select FLR_CODE,FLR_NAME from  " & Session("TENANT") & "."  & "FLOOR where FLR_TWR_ID='" & drpdwnTwm_Id.SelectedItem.Value & "' and flR_sta_id=1"
        ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        drpdwnFlm_Id.DataSource = ObjDR
        drpdwnFlm_Id.DataValueField = "FLR_CODE"
        drpdwnFlm_Id.DataTextField = "FLR_NAME"
        drpdwnFlm_Id.DataBind()
        Try
            txtFlr.Text = drpdwnFlm_Id.SelectedItem.Value
            ObjDR.Close()
        Catch ex As Exception

        End Try
        Session("Twr_Code") = drpdwnTwm_Id.SelectedItem.Value

    End Sub



    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        'Session("Key") = TxtKey1.Value
        spcids = TxtKey1.Value
        'Response.Write(Session("Key"))
    End Sub

    Private Sub drpdwnFlm_Id_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpdwnFlm_Id.SelectedIndexChanged

        'txtFlr.Text = drpdwnFlm_Id.SelectedItem.Value
    End Sub



    Private Sub txtFlr_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFlr.TextChanged

    End Sub

    Protected Sub drpdwnLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpdwnLocation.SelectedIndexChanged
        Try
            If drpdwnLocation.SelectedIndex <> -1 And drpdwnLocation.SelectedIndex <> 0 Then
                objMsater.BindTowerLoc(drpdwnTwm_Id, drpdwnLocation.SelectedItem.Value)
            Else
                drpdwnTwm_Id.Items.Clear()
                drpdwnTwm_Id.Items.Clear()
                drpdwnTwm_Id.Items.Add("--Select--")
                'drpdwnTwm_Id.Items.Add("--Select--")
            End If


            'strSQL = "select TWR_code,TWR_NAME from  " & Session("TENANT") & "."  & "TOWER where (TWR_ID=0 or TWR_BDG_ID in (select FLR_BDG_ID from  " & Session("TENANT") & "."  & "FLOOR where FLR_CODE='" & txtFlr.Text & "')) and TWR_STA_ID=1"
            'ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
            'drpdwnTwm_Id.DataSource = ObjDR
            'drpdwnTwm_Id.DataValueField = "TWR_code"
            'drpdwnTwm_Id.DataTextField = "TWR_NAME"
            'drpdwnTwm_Id.DataBind()
            'ObjDR.Close()


            'For i As Integer = 0 To drpdwnTwm_Id.Items.Count - 1
            '    If drpdwnTwm_Id.Items(i).Value <> "--Select--" Then
            '        drpdwnTwm_Id.SelectedIndex = i
            '        Exit For
            '    End If
            'Next

            strSQL = "select FLR_CODE,FLR_NAME from  " & Session("TENANT") & "."  & "FLOOR where  FLR_TWR_ID='" & drpdwnTwm_Id.SelectedItem.Value & "' and FLR_STA_ID=1"
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
            drpdwnFlm_Id.DataSource = ObjDR
            drpdwnFlm_Id.DataValueField = "FLR_CODE"
            drpdwnFlm_Id.DataTextField = "FLR_NAME"
            drpdwnFlm_Id.DataBind()
            ObjDR.Close()


            For i As Integer = 0 To drpdwnFlm_Id.Items.Count - 1
                If drpdwnFlm_Id.Items(i).Value <> "--Select--" And drpdwnFlm_Id.Items(i).Value = txtFlr.Text Then
                    drpdwnFlm_Id.SelectedIndex = i
                    'txtFlr.Text = drpdwnFlm_Id.SelectedItem.Value
                    Exit For
                End If
            Next


            Session("BDG_ID") = drpdwnLocation.SelectedItem.Value

        Catch ex As Exception
            PopUpMessage(ex.Message, Page)
        End Try
    End Sub
End Class
