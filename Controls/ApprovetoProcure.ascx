<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApprovetoProcure.ascx.vb" Inherits="Controls_ApprovetoProcure" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkSelect";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvItems.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>

</head>
<body>
                      <div class="row">
                      <div class="col-md-12 text-left">
                         <div class="form-group">
                       <div class="row">
                       <div class="col-md-3">
                           <br class="Apple-interchange-newline" />
                           <span style="color: rgb(102, 102, 102); font-family: Roboto, sans-serif; font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"></span><asp:TextBox ID="txtSearch" runat="server" placeholder="Search By Any..." CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-8" style="padding-top:22px">
                        <asp:Button ID="btnSearch" CssClass="btn btn-primary custom-button-color"   runat="server" Text="Search"
                          CausesValidation="true" TabIndex="2" />
                          </div>
                          </div>
                          </div>
                           </div>
                       </div>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-12 pull-right">
            <div class="row" style="overflow: auto; width: 100%; min-height: 20px; max-height: 500px">

                <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" EmptyDataText="No Requisitions Found To Approve."
                    CssClass="table table-condensed table-bordered table-hover table-striped">
                    <Columns>
                        <%--<asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                            <HeaderTemplate>
                                Select All
                            <input id="chkSelect2" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkSelect', this.checked)">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" Checked='<%#Bind("ticked") %>' runat="server" onclick="javascript:ChildClick(this);" />
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                    ToolTip="Click to check all" />
                                <%--OnCheckedChanged="chkAll_CheckedChanged"--%>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" Checked='<%#Bind("ticked") %>' runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                            </ItemTemplate>
                            <HeaderStyle Width="50px" HorizontalAlign="Center" />
                            <ItemStyle Width="50px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="AIR_REQ_ID" HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left" />--%>
                        <asp:TemplateField HeaderText="Requisition ID" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIR_REQ_ID","~/FAM/FAM_WebFiles/frmAssetRequisitionDetails_Procure.aspx?RID={0}") %>'
                                    Text='<%# Eval("AIR_REQ_ID")%> '>
                                </asp:HyperLink>
                                <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblReqID" runat="server" Text='<%#Eval("AIR_REQ_ID") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="VT_TYPE" HeaderText="Category" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="TOTAL_REQ_QTY" HeaderText="Total Requested Qty" Visible="false" ItemStyle-HorizontalAlign="left" />
                        <%-- <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="manufacturer" HeaderText="Brand" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="AST_MD_NAME" HeaderText="Model" ItemStyle-HorizontalAlign="left" />--%>
                        <asp:BoundField DataField="AUR_NAME" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="AIR_REQ_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                        <asp:BoundField DataField="STA_TITLE" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                        <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center">
                        <ItemTemplate>
                            <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIR_REQ_ID","~/FAM/FAM_WebFiles/frmAssetRequisitionDetails_Procure.aspx?RID={0}") %>' Text="View Details"></asp:HyperLink>
                            <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>
    <br>
    <div class="row" id="divapprove" runat="server">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Approver Remarks</label>
                    <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRM"
                        Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtRM" runat="server" CssClass="form-control" MaxLength="100"
                            Height="50px" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 text-right">
            <div class="form-group">
                <asp:Button ID="btnsubmit" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq();" ValidationGroup="Val1" />
                <asp:Button ID="btnCancel" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq();" />
            </div>
        </div>
    </div>
    <div class="row">
    </div>
</body>
</html>
