Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.Drawing
Partial Class Controls_Mapped_Unmapped_AstDispose
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim DisposeReq As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        ''gvAstList.Attributes.Add("Readonly", "Readonly")
        If Not IsPostBack Then
            Page.Form.Attributes.Add("enctype", "multipart/form-data")
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
                panel1.Visible = False
                lblMsg.Text = ""
            Else
                BindLocation()
                getassetcategory()
                getsubcategorybycat(ddlastCat.SelectedItem.Value)
                ddlastsubCat.SelectedIndex = If(ddlastsubCat.Items.Count > 1, 1, 0)
                getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
                ddlAstBrand.SelectedIndex = If(ddlAstBrand.Items.Count > 1, 1, 0)
                getModelbycatsubcat()
                ddlAstModel.SelectedIndex = If(ddlAstModel.Items.Count > 1, 1, 0)
                BindStockReport()
                lblMsg.Text = ""
                'ddlLocation.Items.Insert(1, New ListItem("--All--", "1"))
                panel1.Visible = True
                If gvAstList.Rows.Count = 0 Then
                    gvAstList.DataSource = Nothing
                    gvAstList.DataBind()
                End If
            End If
        End If
    End Sub

    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")

        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        ddlLocation.Items.Insert(0, New ListItem("--All--", "All"))
        ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 1, 0)

        'ddlLocation.Items.Remove("--All--")
    End Sub
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlastCat.DataSource = sp.GetDataSet()
        ddlastCat.DataTextField = "VT_TYPE"
        ddlastCat.DataValueField = "VT_CODE"
        ddlastCat.DataBind()
        'ddlastCat.Items.Insert(0, "--Select--")
        ddlastCat.Items.Remove("--Select--")
        ddlastCat.Items.Insert(0, New ListItem("--All--", "All"))
        ddlastCat.SelectedIndex = If(ddlastCat.Items.Count > 1, 1, 0)
    End Sub
    Private Sub getbrandbycatsubcat(ByVal astcatcode As String, ByVal astsubcatcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlastsubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--All--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        'ddlAstModel.Items.Insert(0, New ListItem("--All--", ""))


    End Sub
    Private Sub getModelbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlastsubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))


    End Sub


    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        ddlastsubCat.DataSource = sp.GetDataSet()
        ddlastsubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlastsubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlastsubCat.DataBind()
        ' ddlastsubCat.Items.Insert(0, "--Select--")
        ddlastsubCat.Items.Insert(0, New ListItem("--All--", "All"))

    End Sub
    Protected Sub ddlastCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlastCat.SelectedIndexChanged
        getsubcategorybycat(ddlastCat.SelectedItem.Value)
        getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
        getModelbycatsubcat()
        BindStockReport()
    End Sub
    Protected Sub ddlastsubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlastsubCat.SelectedIndexChanged
        If ddlastsubCat.SelectedIndex > 0 Then
            getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
            getModelbycatsubcat()
            BindStockReport()
        End If

    End Sub
    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        If ddlAstBrand.SelectedIndex > 0 Then
            getModelbycatsubcat()
            BindStockReport()
        End If

    End Sub


    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex = 1 Then

            ElseIf ddlLocation.SelectedIndex > 1 Then

            Else
                gvAstList.DataSource = Nothing
                gvAstList.DataBind()

            End If
            BindStockReport()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindStockReport()

        Dim param(7) As SqlParameter
        param(0) = New SqlParameter("@AST_CAT_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlastCat.SelectedItem.Value
        param(1) = New SqlParameter("@AST_SUBCAT_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlastsubCat.SelectedItem.Value
        param(2) = New SqlParameter("@AST_BRND", SqlDbType.NVarChar, 200)
        param(2).Value = ddlAstBrand.SelectedItem.Value
        param(3) = New SqlParameter("@AST_MODEL", SqlDbType.NVarChar, 200)
        param(3).Value = ddlAstModel.SelectedItem.Value
        param(4) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(4).Value = ddlLocation.SelectedItem.Value
        param(5) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(5).Value = Session("uid")

        param(6) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
        param(6).Value = Session("COMPANYID")
        param(7) = New SqlParameter("@DEP_TYPE", SqlDbType.NVarChar, 200)
        param(7).Value = Session("DepMethod")
        ObjSubsonic.BindGridView(gvAstList, "GET_ALLASTS", param)
        panel1.Visible = True
        If gvAstList.Rows.Count = 0 Then
            gvAstList.DataSource = Nothing
            gvAstList.DataBind()
            remarks.Visible = False
        Else
            remarks.Visible = True

        End If

    End Sub

    Protected Sub gvAstList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAstList.PageIndexChanging
        gvAstList.PageIndex = e.NewPageIndex
        BindStockReport()
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        BindStockReport()
    End Sub


    Function GetTodaysReqCount() As String
        Dim cnt As String
        cnt = 0
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TODAYS_DISPOSE_REQUEST_ID")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ds = sp.GetDataSet()
        cnt = CInt(ds.Tables(0).Rows(0).Item("cnt"))
        Return cnt
    End Function


    Protected Sub DisposeSelected_Click(sender As Object, e As EventArgs) Handles DisposeSelected.Click

        Try
            'Req_id = ObjSubsonic.RIDGENARATION("Admin/Ast/Dispose/")
            Dim AstTaggedId As Integer = 0

            Dim count As Integer = 0
            For Each gvRow As GridViewRow In gvAstList.Rows
                Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    count = count + 1
                    Exit For
                End If
            Next
            If count > 0 Then
                Dim Req_id As String
                For Each gvRow As GridViewRow In gvAstList.Rows

                    Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        Dim lblAstCode As Label = DirectCast(gvRow.FindControl("lblAstCode"), Label)
                        Dim lbl_salvge As Label = DirectCast(gvRow.FindControl("lbl_salvge"), Label)
                        Dim lblEmpAstTaggedId As Label = DirectCast(gvRow.FindControl("lbl_emp_tag_id"), Label)

                        Dim lbl_Dep_per As Label = DirectCast(gvRow.FindControl("lbl_Dep_per"), Label)
                        Dim lbl_Dep_cost As Label = DirectCast(gvRow.FindControl("lbl_Dep_val"), Label)
                        Dim lbl_Current_val As Label = DirectCast(gvRow.FindControl("lbl_Cur_val"), Label)
                        Dim lbl_Actual_cost As Label = DirectCast(gvRow.FindControl("lbl_cost"), Label)
                        Dim ddlAssetDis As DropDownList = DirectCast(gvRow.FindControl("ddlAssetDis"), DropDownList)
                        Dim txtdisposaldate As TextBox = DirectCast(gvRow.FindControl("txtdisposaldate"), TextBox)
                        Dim sdate As Date = Date.Parse(txtdisposaldate.Text)
                        Dim txtRecoveryval As TextBox = DirectCast(gvRow.FindControl("txtRecoveryval"), TextBox)

                        Dim fileupload1 As FileUpload = DirectCast(gvRow.FindControl("fu1"), FileUpload)
                        Dim filename As String = fileupload1.FileName


                        Dim dstnPath As String = System.IO.Path.GetTempPath
                        lblMsg.Text = ""
                        If chkSelect.Checked = True Then

                            For Each File In fileupload1.PostedFiles
                                If File.ContentLength > 0 Then
                                    Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & File.FileName
                                    File.SaveAs(filePath)
                                End If
                            Next

                            If String.IsNullOrEmpty(lblEmpAstTaggedId.Text) = True Then
                            Else
                                AstTaggedId = lblEmpAstTaggedId.Text
                            End If
                            Req_id = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyy")
                            DisposeReq = "Admin/Ast/Dispose/" + Req_id + "/" + GetTodaysReqCount().ToString()
                            Dim param(20) As SqlParameter
                            param(0) = New SqlParameter("@DREQ_TS", SqlDbType.NVarChar, 200)
                            param(0).Value = DisposeReq
                            param(1) = New SqlParameter("@DREQ_ID", SqlDbType.NVarChar, 200)
                            param(1).Value = DisposeReq
                            param(2) = New SqlParameter("@AST_TAGGED_ID", SqlDbType.Int)
                            param(2).Value = AstTaggedId
                            param(3) = New SqlParameter("@DREQ_REQUESITION_DT", SqlDbType.DateTime)
                            param(3).Value = getoffsetdatetime(DateTime.Now)
                            param(4) = New SqlParameter("@DREQ_REQUESTED_BY", SqlDbType.NVarChar, 200)
                            param(4).Value = Session("uid")
                            param(5) = New SqlParameter("@DREQ_REQUESTED_REMARKS", SqlDbType.NVarChar, 2000)
                            param(5).Value = txtRemarks.Text
                            param(6) = New SqlParameter("@DREQ_STATUS", SqlDbType.Int)
                            param(6).Value = 1040
                            param(7) = New SqlParameter("@AST_DISPOSE", SqlDbType.Bit)
                            param(7).Value = True
                            param(8) = New SqlParameter("@DREQ_ADMIN_DT", SqlDbType.Int)
                            param(8).Value = ""
                            param(9) = New SqlParameter("@DREQ_ADMIN_BY", SqlDbType.Int)
                            param(9).Value = ""
                            param(10) = New SqlParameter("@DREQ_ADMIN_REMARKS", SqlDbType.Int)
                            param(10).Value = ""
                            param(11) = New SqlParameter("@AST_CODE", SqlDbType.NVarChar, 200)
                            param(11).Value = lblAstCode.Text
                            param(12) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
                            param(12).Value = Session("COMPANYID")
                            param(13) = New SqlParameter("@AST_DEP_PER", SqlDbType.NVarChar, 200)
                            param(13).Value = lbl_Dep_per.Text
                            param(14) = New SqlParameter("@AST_DEP_COST", SqlDbType.NVarChar, 200)
                            param(14).Value = lbl_Dep_cost.Text
                            param(15) = New SqlParameter("@AST_CUR_VAL", SqlDbType.NVarChar, 200)
                            param(15).Value = lbl_Current_val.Text
                            param(16) = New SqlParameter("@AST_ACTUAL_COST", SqlDbType.NVarChar, 200)
                            param(16).Value = lbl_Actual_cost.Text
                            param(17) = New SqlParameter("@AST_DISPOSAL_ASSET", SqlDbType.NVarChar, 200)
                            param(17).Value = ddlAssetDis.SelectedValue
                            param(18) = New SqlParameter("@AST_DISPOSAL_DATE", SqlDbType.Date)
                            param(18).Value = txtdisposaldate.Text
                            param(19) = New SqlParameter("@AST_RECOVERY_VAL", SqlDbType.Money)
                            param(19).Value = txtRecoveryval.Text
                            param(20) = New SqlParameter("@AST_UPLOAD_DOC", SqlDbType.NVarChar)
                            param(20).Value = filename

                            ObjSubsonic.GetSubSonicExecute("INSERT_DISPOSE_REQ", param)
                            send_mail(DisposeReq, lblAstCode.Text)
                        End If
                    End If



                Next
                lblMsg.Visible = True
                lblMsg.Text = "Dispose request has been raised Successfully"
                panel1.Visible = False
                remarks.Visible = False
            Else
                lblMsg.Text = "Please select the assets to Dispose"
            End If
        Catch ex As Exception
            lblMsg.Text = "Something went wrong"
        End Try
    End Sub
    Public Sub send_mail(ByVal reqid As String, ByVal astcode As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_DISPOSE_REQUISITION")
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Command.AddParameter("@AST_CODE", astcode, DbType.String)
        sp1.Execute()
    End Sub
    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub
    Public Sub cleardata()
        ddlastCat.ClearSelection()
        ddlastsubCat.ClearSelection()
        ddlLocation.ClearSelection()
        ddlAstBrand.ClearSelection()
        ddlAstModel.ClearSelection()
        gvAstList.DataSource = Nothing
        gvAstList.DataBind()
        remarks.Visible = False
    End Sub


    'Protected Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
    '    lblMsg.Visible = False
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_ASSET_DETAILS_SEARCH1")
    '    sp.Command.AddParameter("@SEARCH_CRITERIA", txtSearch.Text, Data.DbType.String)
    '    sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
    '    gvAstList.DataSource = sp.GetDataSet
    '    gvAstList.DataBind()
    'End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        lblMsg.Visible = False
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_FILTER_MAPPED_DETAILS")
        sp.Command.AddParameter("@AST_FILTER", txtSearch.Text, Data.DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)

        'sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        gvAstList.DataSource = sp.GetDataSet
        gvAstList.DataBind()
    End Sub

    Private Sub gvAstList_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvAstList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ddlAssetDis As DropDownList = (TryCast(e.Row.FindControl("ddlAssetDis"), DropDownList))
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMG_ASSET_STATUS")
            'sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            Dim dt As DataSet = New DataSet()
            dt = sp.GetDataSet()
            ddlAssetDis.DataSource = dt
            ddlAssetDis.DataTextField = "TAG_NAME"
            ddlAssetDis.DataValueField = "TAG_CODE"
            ddlAssetDis.DataBind()
            ddlAssetDis.Items.Insert(0, New ListItem("--Select--", "Select"))

            'Dim txtdisposaldate As TextBox = CType(e.Row.Cells(0).FindControl("txtdisposaldate"), TextBox)
            'txtdisposaldate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            'txtdisposaldate.Attributes.Add("onClick", "displayDatePicker('" + txtdisposaldate.ClientID + "')")

        End If
        '''ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    'Protected Sub gvAstList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAstList.RowCommand
    '    Try
    '        Dim lnkSurrender As LinkButton = DirectCast(e.CommandSource, LinkButton)
    '        Dim gvRow As GridViewRow = DirectCast(lnkSurrender.NamingContainer, GridViewRow)
    '        Dim txtSurrendered_TAG As TextBox = DirectCast(gvRow.FindControl("txtSurrendered_TAG"), TextBox)

    '    Catch ex As Exception

    '    End Try
    'End Sub
    'Private Sub fileupload()


    '    Dim orgfilename As String = fu1.FileName
    '    Dim filename As String = ""
    '    lblMsg.Text = ""

    '    Dim SurrenderList As List(Of ImageClas) = New List(Of ImageClas)
    '    Dim IC As ImageClas
    '    Dim i As Int32 = 0
    '    Dim fileSize As Int64 = 0
    '    If fu1.PostedFiles IsNot Nothing Then
    '        Dim count = fu1.PostedFiles.Count
    '        While (i < count)
    '            fileSize = fu1.PostedFiles(i).ContentLength + fileSize
    '            i = i + 1
    '        End While
    '        If (fileSize > 20971520) Then
    '            lblMsg.Text = "Upload files size could not be greater than 20 MB."
    '            Exit Sub
    '        End If
    '        For Each File In fu1.PostedFiles
    '            If File.ContentLength > 0 Then
    '                Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
    '                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Upload_Time & "_" & File.FileName
    '                File.SaveAs(filePath)
    '                IC = New ImageClas()
    '                IC.Filename = File.FileName
    '                IC.FilePath = Upload_Time & "_" & File.FileName
    '                SurrenderList.Add(IC)

    '            End If
    '        Next
    '    End If
    'End Sub

End Class
