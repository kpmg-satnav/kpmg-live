<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UpdateRentDetails.ascx.vb"
    Inherits="Controls_UpdateRentDetails" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                     Property Type <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                    Display="None" ErrorMessage="Please Select Property Type" ValidationGroup="Val1"
                    InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlproptype" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                     City <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label"> Location <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                     Property <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvTenant" runat="server" ControlToValidate="ddlTenant"
                    Display="None" ErrorMessage="Please Select Property" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlTenant" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Rental Receipt Number <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvReceipt" runat="server" ControlToValidate="txtReceipt"
                    Display="None" ErrorMessage="Please Enter Rental Receipt Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <%--<asp:RegularExpressionValidator ID="revReceipt" Display="None" runat="server"
                                ControlToValidate="txtReceipt" ErrorMessage="Invalid Receipt" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>--%>
                <div class="col-md-7">
                    <asp:TextBox ID="txtReceipt" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Paid Date <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvpaiddate" runat="server" ControlToValidate="txtPaiddate"
                    Display="None" ErrorMessage="Please Enter Paid Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='Paiddate'>
                        <asp:TextBox ID="txtPaiddate" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('Paiddate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    From Date <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvFrmDate" runat="server" ControlToValidate="txtFrmDate"
                    Display="None" ErrorMessage="Please Enter From Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='fromdate'>
                        <asp:TextBox ID="txtFrmDate" runat="server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    To Date <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvToDate" runat="server" ControlToValidate="txtToDate"
                    Display="None" ErrorMessage="Please Enter To Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div class='input-group date' id='Todate'>
                        <asp:TextBox ID="txtToDate" runat="Server" CssClass="form-control"></asp:TextBox>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar" onclick="setup('Todate')"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">


    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Rent Amount <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvamount" runat="server" ControlToValidate="txtamount"
                    Display="None" ErrorMessage="Please Enter Rent Amount" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revamount" Display="None" runat="server" ControlToValidate="txtamount"
                    ErrorMessage="Enter Valid Amount" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter Amount with maximum length 15')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtamount" runat="Server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Outstanding Amount <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvOut" runat="server" ControlToValidate="txtOamount"
                    Display="None" ErrorMessage="Please Enter Outstanding Amount" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                    ControlToValidate="txtOamount" ErrorMessage="Enter Valid OutstandingAmount" ValidationExpression="^[0-9]*$"
                    ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter Amount with maximum length 15')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtOamount" runat="Server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                     Payment Mode <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="cvmode" runat="server" ControlToValidate="ddlpaymentmode"
                    Display="None" ErrorMessage="Please Select Payment Mode" ValidationGroup="Val1"
                    InitialValue="--Select PaymentMode--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlpaymentmode" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                        <asp:ListItem>--select PaymentMode--</asp:ListItem>
                        <asp:ListItem Value="1">Cheque</asp:ListItem>
                        <asp:ListItem Value="2">Cash</asp:ListItem>
                        <asp:ListItem Value="3">Bank Transfer</asp:ListItem>
                    </asp:DropDownList>&nbsp;
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    TDS <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvTds" runat="server" ControlToValidate="txtTDS"
                    Display="None" ErrorMessage="Please enter TDS" ValidationGroup="Val1">
                </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvTds" runat="server" ControlToValidate="txtTDS" ErrorMessage="Please enter valid TDS"
                    ValidationGroup="Val1" Type="double" Operator="DataTypeCheck" Display="None"></asp:CompareValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter Decimals Only ')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtTDS" runat="Server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Tenant <span style="color: red;">*</span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlemp" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Payment Remarks <span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                    Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revremarks" Display="None" runat="server" ControlToValidate="txtRemarks"
                    ErrorMessage="Enter Valid Remarks" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter Remarks with Maximum 250 characters and No special characters allowed ')"
                        onmouseout="UnTip()">
                        <asp:TextBox ID="txtRemarks" runat="Server" CssClass="form-control" MaxLength="250"
                            Rows="3" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="panel1" runat="Server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Cheque No <span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvCheque" runat="server" ControlToValidate="txtCheque"
                        Display="None" ErrorMessage="Please Enter Cheque Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rvcheque" Display="None" runat="server" ControlToValidate="txtCheque"
                        ErrorMessage="Invalid Cheque Number" ValidationExpression="[0-9]{3,8}" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter Numbers and No Special Characters allowed ')" onmouseout="UnTip()">
                            <asp:TextBox ID="txtCheque" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Bank Name <span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                        Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revBankName" Display="None" runat="server" ControlToValidate="txtBankName"
                        ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                            onmouseout="UnTip()">
                            <asp:TextBox ID="txtBankName" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Account Number <span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                        Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revAccno" Display="None" runat="server" ControlToValidate="txtAccNo"
                        ErrorMessage="Enter Valid Account Number" ValidationExpression="^[0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter  Numbers only and No Special Characters allowed ')"
                            onmouseout="UnTip()">
                            <asp:TextBox ID="txtAccNo" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="panel2" runat="Server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">
                        Issuing Bank Name <span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtIBankName"
                        Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                        ControlToValidate="txtIBankName" ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"
                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                            onmouseout="UnTip()">
                            <asp:TextBox ID="txtIBankName" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Deposited Bank <span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfDeposited" runat="server" ControlToValidate="txtDeposited"
                        Display="None" ErrorMessage="Please Enter Deposited Bank" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revDeposited" Display="None" runat="server" ControlToValidate="txtDeposited"
                        ErrorMessage="Enter Valid Bank Branch" ValidationExpression="^[A-Za-z0-9 ]*$"
                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                            onmouseout="UnTip()">
                            <asp:TextBox ID="txtDeposited" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">IFSC Code <span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvIFCB" runat="server" ControlToValidate="txtIFCB"
                        Display="None" ErrorMessage="Please Enter IFSC Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="REVIFCB" Display="None" runat="server" ControlToValidate="txtIFCB"
                        ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                            onmouseout="UnTip()">
                            <asp:TextBox ID="txtIFCB" runat="Server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">

            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                CausesValidation="true" />

        </div>
    </div>
</div>
