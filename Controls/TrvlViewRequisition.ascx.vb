Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_TrvlViewRequisition
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindCategory()
        End If

    End Sub
    Private Sub BindCategory()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_CATEGORY_GET")
            ddlCategory.DataSource = sp.GetDataSet()
            ddlCategory.DataTextField = "CATEGORY"
            ddlCategory.DataValueField = "SNO"
            ddlCategory.DataBind()
            ddlCategory.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        If ddlCategory.SelectedIndex > 0 Then
            lblMsg.Text = ""
            BindDocType()
        Else
            ddlDocType.Items.Clear()
            ddlDocType.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlDocType.SelectedIndex = 0
        End If
        
    End Sub
    Private Sub BindDocType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOC_TYPE_CATEGORY")
            sp.Command.AddParameter("@CATEGORY", ddlCategory.SelectedItem.Value, DbType.Int32)
            ddlDocType.DataSource = sp.GetDataSet()
            ddlDocType.DataTextField = "DOC_TYPE"
            ddlDocType.DataValueField = "SNO"
            ddlDocType.DataBind()
            ddlDocType.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim orgfilename As String = ""
        Dim repdocdatetime As String = ""
        Try
            If (fpBrowseDoc.HasFile) Then
                orgfilename = fpBrowseDoc.FileName
                repdocdatetime = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & orgfilename
                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime
                fpBrowseDoc.PostedFile.SaveAs(filePath)
            End If
            insertnewrecord(repdocdatetime)
            Cleardata()
            lblMsg.Text = "Document Uploaded Successfully"
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Public Sub insertnewrecord(ByVal repdocdatetime As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DOCUMENTS_ADD")
            Dim orgfilename As String = fpBrowseDoc.FileName
            sp.Command.AddParameter("@CATEGORY", ddlCategory.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@DOC_TYPE", ddlDocType.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@DOC_TITLE", txtDocTitle.Text, DbType.String)
            sp.Command.AddParameter("@DOC_ORG_FILENAME", fpBrowseDoc.FileName, DbType.String)
            sp.Command.AddParameter("@DOC", repdocdatetime, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Cleardata()
        ddlCategory.SelectedIndex = 0
        ddlDocType.Items.Clear()
        ddlDocType.Items.Insert(0, New ListItem("--Select--", "0"))
        ddlDocType.SelectedIndex = 0
        txtDocTitle.Text = ""
    End Sub
End Class
