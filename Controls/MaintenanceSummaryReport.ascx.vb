imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO

Partial Class Controls_MaintenanceSummaryReport
    Inherits System.Web.UI.UserControl

    Dim id1 As String
    Dim ids As String
    Dim idro As String
    Dim idrc As String
    Dim idrot As String
    Dim idrct As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            gvTOCReqs.PageIndex = 0
            
        End If
        fillgridgvAll()
        fillgridgvTenant()
        fillgridgvProp()
        fillgridgvTOCReqs()
        If gvTOCReqs.Rows.Count > 0 Then
            btnexporttoexcel.Visible = True
        Else
            btnexporttoexcel.Visible = False
        End If
        If gvProp.Rows.Count > 0 Then
            btnExprtBldng.Visible = True
        Else
            btnExprtBldng.Visible = False
        End If
        If gvAll.Rows.Count > 0 Then
            btnExprtAll.Visible = True
        Else
            btnExprtAll.Visible = False
        End If
        If gvTenant.Rows.Count > 0 Then
            btnExprtTenant.Visible = True
        Else
            btnExprtTenant.Visible = False
        End If
        If gvEBLDNGRTYPEO.Rows.Count > 0 Then
            btnEBLDNGRTYPEO.Visible = True
        Else
            btnEBLDNGRTYPEO.Visible = False
        End If
        If gvEBLDNGRTYPEC.Rows.Count > 0 Then
            btnEBLDNGRTYPEC.Visible = True
        Else
            btnEBLDNGRTYPEC.Visible = False
        End If
    End Sub

    Protected Sub btnexporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexporttoexcel.Click
        Export("Tot_No_Of_O_C_" + getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") + ".xls", gvTOCReqs)
    End Sub

#Region "Export"
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub


    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
#End Region



    Protected Sub gvAll_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAll.PageIndexChanging
        gvAll.PageIndex = e.NewPageIndex
        fillgridgvAll()

    End Sub
    Private Sub fillgridgvAll()

        '[GET_REQTYPE_ORS_CRS] For getting the Open and Closed Reqs Based on the Request Type

        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQTYPE_ORS_CRS")
        sp3.Command.AddParameter("@dummy", "", DbType.String)
        Dim ds3 As New DataSet
        ds3 = sp3.GetDataSet
        gvAll.DataSource = ds3
        gvAll.DataBind()

    End Sub
    Private Sub fillgridgvTenant()
        '[GET_TENANT_ORS_CRS] For getting the Open and Closed Reqs Based on the Tenanat Name
        Dim sp5 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TENANT_ORS_CRS")
        sp5.Command.AddParameter("@dummy", "", DbType.String)
        Dim ds5 As New DataSet
        ds5 = sp5.GetDataSet
        gvTenant.DataSource = ds5
        gvTenant.DataBind()
    End Sub
    Private Sub fillgridgvProp()
        '[GET_BDG_ORS_CRS] For getting the Open and Closed Reqs Based on the Building Name

        Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_BDG_ORS_CRS")
        sp4.Command.AddParameter("@dummy", "", DbType.String)
        Dim ds4 As New DataSet
        ds4 = sp4.GetDataSet
        gvProp.DataSource = ds4
        gvProp.DataBind()
    End Sub
    Private Sub fillgridgvTOCReqs()

        '[GET_T_OPEN_REQS_T_CLSD_REQS] for total open and closed reqs
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_T_OPEN_REQS_T_CLSD_REQS")
        sp1.Command.AddParameter("@dummy", "", DbType.String)
        Dim ds1 As New DataSet
        ds1 = sp1.GetDataSet
        gvTOCReqs.DataSource = ds1
        gvTOCReqs.DataBind()

    End Sub


    Protected Sub gvTenant_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTenant.PageIndexChanging
        gvTenant.PageIndex = e.NewPageIndex
        fillgridgvTenant()
    End Sub

    Protected Sub gvTOCReqs_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTOCReqs.PageIndexChanging
        gvTOCReqs.PageIndex = e.NewPageIndex
        fillgridgvTOCReqs()
    End Sub

    Protected Sub btnExprtAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExprtAll.Click
        Export("Tot_No_Of_TO_TC_ReqType" + getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") + ".xls", gvAll)
    End Sub

    Protected Sub btnExprtBldng_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExprtBldng.Click
        Export("BLDNG_ORS_CRS" + getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") + ".xls", gvProp)
    End Sub

    Protected Sub btnExprtTenant_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExprtTenant.Click
        Export("TNT_ORS_CRS" + getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") + ".xls", gvTenant)
    End Sub




    
    Public Sub bindPART_BDNG(ByVal BDG_NAME As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_OPEN_REQS_PART_BDNG")
        sp1.Command.AddParameter("@BDG_NAME", BDG_NAME, DbType.String)
        Dim ds1 As New DataSet
        ds1 = sp1.GetDataSet()
        gvEBLDNGRTYPEO.DataSource = ds1
        gvEBLDNGRTYPEO.DataBind()
        For i As Integer = 0 To gvEBLDNGRTYPEO.Rows.Count - 1
            Dim lblREQUEST_STATUS As Label = CType(gvEBLDNGRTYPEO.Rows(i).FindControl("lblREQUEST_STATUS"), Label)
            If lblREQUEST_STATUS.Text = "0" Then
                lblREQUEST_STATUS.Text = "Pending"
            ElseIf lblREQUEST_STATUS.Text = "1" Then
                lblREQUEST_STATUS.Text = "InProgress"
            Else
                lblREQUEST_STATUS.Text = "Completed"
            End If
        Next

    End Sub
    Public Sub bindPART_BDNGC(ByVal BDG_NAME As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CLOSED_REQS_PART_BDNG")
        sp.Command.AddParameter("@BDG_NAME", BDG_NAME, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        gvEBLDNGRTYPEC.DataSource = ds
        gvEBLDNGRTYPEC.DataBind()
        For i As Integer = 0 To gvEBLDNGRTYPEC.Rows.Count - 1
            Dim lblREQUEST_STATUSC As Label = CType(gvEBLDNGRTYPEC.Rows(i).FindControl("lblREQUEST_STATUSC"), Label)
            If lblREQUEST_STATUSC.Text = "0" Then
                lblREQUEST_STATUSC.Text = "Pending"
            ElseIf lblREQUEST_STATUSC.Text = "1" Then
                lblREQUEST_STATUSC.Text = "InProgress"
            Else
                lblREQUEST_STATUSC.Text = "Completed"
            End If
        Next
    End Sub

    Protected Sub btnEBLDNGRTYPEO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEBLDNGRTYPEO.Click
        Export("REQ_TYPE_O" + getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") + ".xls", gvEBLDNGRTYPEO)
    End Sub

    Protected Sub btnEBLDNGRTYPEC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEBLDNGRTYPEC.Click
        Export("REQ_TYPE_C" + getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") + ".xls", gvEBLDNGRTYPEC)
    End Sub

   

    



    Protected Sub gvEBLDNGRTYPEO_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEBLDNGRTYPEO.PageIndexChanging
        gvEBLDNGRTYPEO.PageIndex = e.NewPageIndex
        If Session("OpenReq") = "1" Then
            fillgridgvTOReqsD()
        ElseIf Session("OpenReq") = "2" Then
            bindPART_BDNG(Session("tempO"))
        ElseIf Session("OpenReq") = "3" Then
            bindPART_RQSTO(Session("tempOAll"))
        ElseIf Session("OpenReq") = "4" Then
            bindPART_RQSTOT(Session("tempOTAll"))
        End If




    End Sub

    Protected Sub gvEBLDNGRTYPEC_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvEBLDNGRTYPEC.PageIndexChanging
        gvEBLDNGRTYPEC.PageIndex = e.NewPageIndex
        If Session("CloseReq") = "1" Then
            fillgridgvTCReqsD()
        ElseIf Session("CloseReq") = "2" Then
            bindPART_BDNG(Session("tempC"))
        ElseIf Session("CloseReq") = "3" Then
            bindPART_RQSTC(Session("tempCAll"))
        ElseIf Session("CloseReq") = "4" Then
            bindPART_RQSTCT(Session("tempCTAll"))
        End If

    End Sub

    Protected Sub gvEBLDNGRTYPEC_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles gvEBLDNGRTYPEC.SelectedIndexChanging

    End Sub

    Protected Sub gvTOCReqs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTOCReqs.RowCommand
        If e.CommandName = "TotOpenReqs" Then
            gvEBLDNGRTYPEO.Visible = True
            gvEBLDNGRTYPEC.Visible = False

            Session("OpenReq") = "1"
            fillgridgvTOReqsD()
            If gvEBLDNGRTYPEO.Rows.Count > 0 Then
                btnEBLDNGRTYPEO.Visible = True
            Else
                btnEBLDNGRTYPEO.Visible = False
            End If

        ElseIf e.CommandName = "TotClosedReqs" Then

            gvEBLDNGRTYPEO.Visible = False
            gvEBLDNGRTYPEC.Visible = True
            Session("CloseReq") = "1"
            fillgridgvTCReqsD()
            If gvEBLDNGRTYPEC.Rows.Count > 0 Then
                btnEBLDNGRTYPEC.Visible = True
            Else
                btnEBLDNGRTYPEC.Visible = False
            End If
        End If
    End Sub

    Private Sub fillgridgvTOReqsD()

        '[GET_T_OPEN_REQSD] for total open reqs in detail
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_T_OPEN_REQSD")
        sp1.Command.AddParameter("@dummy", "", DbType.String)
        Dim ds1 As New DataSet
        ds1 = sp1.GetDataSet
        gvEBLDNGRTYPEO.DataSource = ds1
        gvEBLDNGRTYPEO.DataBind()
        For i As Integer = 0 To gvEBLDNGRTYPEO.Rows.Count - 1
            Dim lblREQUEST_STATUS As Label = CType(gvEBLDNGRTYPEO.Rows(i).FindControl("lblREQUEST_STATUS"), Label)
            If lblREQUEST_STATUS.Text = "0" Then
                lblREQUEST_STATUS.Text = "Pending"
            ElseIf lblREQUEST_STATUS.Text = "1" Then
                lblREQUEST_STATUS.Text = "InProgress"
            Else
                lblREQUEST_STATUS.Text = "Completed"
            End If
        Next


    End Sub

    Private Sub fillgridgvTCReqsD()

        '[GET_T_CLOSED_REQSD] for total Close reqs in detail
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_T_CLOSED_REQSD")
        sp1.Command.AddParameter("@dummy", "", DbType.String)
        Dim ds1 As New DataSet
        ds1 = sp1.GetDataSet
        gvEBLDNGRTYPEC.DataSource = ds1
        gvEBLDNGRTYPEC.DataBind()
        For i As Integer = 0 To gvEBLDNGRTYPEC.Rows.Count - 1
            Dim lblREQUEST_STATUSC As Label = CType(gvEBLDNGRTYPEC.Rows(i).FindControl("lblREQUEST_STATUSC"), Label)
            If lblREQUEST_STATUSC.Text = "0" Then
                lblREQUEST_STATUSC.Text = "Pending"
            ElseIf lblREQUEST_STATUSC.Text = "1" Then
                lblREQUEST_STATUSC.Text = "InProgress"
            Else
                lblREQUEST_STATUSC.Text = "Completed"
            End If
        Next

    End Sub

    Protected Sub gvAll_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAll.RowCommand
        If e.CommandName = "OpenReqType" Then
      
            gvEBLDNGRTYPEO.Visible = True
            gvEBLDNGRTYPEC.Visible = False
            Session("OpenReq") = "3"

            idro = e.CommandArgument
            Session("tempOAll") = idro
            bindPART_RQSTO(idro)
            If gvEBLDNGRTYPEO.Rows.Count > 0 Then
                btnEBLDNGRTYPEO.Visible = True
            Else
                btnEBLDNGRTYPEO.Visible = False
            End If



        ElseIf e.CommandName = "ClosedReqType" Then

            gvEBLDNGRTYPEO.Visible = False
            gvEBLDNGRTYPEC.Visible = True
            Session("CloseReq") = "3"
            idrc = e.CommandArgument
            Session("tempCAll") = idrc
            bindPART_RQSTC(idrc)
            If gvEBLDNGRTYPEC.Rows.Count > 0 Then
                btnEBLDNGRTYPEC.Visible = True
            Else
                btnEBLDNGRTYPEO.Visible = False
            End If
        End If
    End Sub
    Public Sub bindPART_RQSTO(ByVal Request_Type As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_OPEN_REQS_PART_REQS")
        sp1.Command.AddParameter("@Request_Type", Request_Type, DbType.String)
        Dim ds1 As New DataSet
        ds1 = sp1.GetDataSet()
        gvEBLDNGRTYPEO.DataSource = ds1
        gvEBLDNGRTYPEO.DataBind()
        For i As Integer = 0 To gvEBLDNGRTYPEO.Rows.Count - 1
            Dim lblREQUEST_STATUS As Label = CType(gvEBLDNGRTYPEO.Rows(i).FindControl("lblREQUEST_STATUS"), Label)
            If lblREQUEST_STATUS.Text = "0" Then
                lblREQUEST_STATUS.Text = "Pending"
            ElseIf lblREQUEST_STATUS.Text = "1" Then
                lblREQUEST_STATUS.Text = "InProgress"
            Else
                lblREQUEST_STATUS.Text = "Completed"
            End If
        Next

    End Sub
    Public Sub bindPART_RQSTC(ByVal Request_Type As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CLOSED_REQS_PART_REQS")
        sp.Command.AddParameter("@Request_Type", Request_Type, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        gvEBLDNGRTYPEC.DataSource = ds
        gvEBLDNGRTYPEC.DataBind()
        For i As Integer = 0 To gvEBLDNGRTYPEC.Rows.Count - 1
            Dim lblREQUEST_STATUSC As Label = CType(gvEBLDNGRTYPEC.Rows(i).FindControl("lblREQUEST_STATUSC"), Label)
            If lblREQUEST_STATUSC.Text = "0" Then
                lblREQUEST_STATUSC.Text = "Pending"
            ElseIf lblREQUEST_STATUSC.Text = "1" Then
                lblREQUEST_STATUSC.Text = "InProgress"
            Else
                lblREQUEST_STATUSC.Text = "Completed"
            End If
        Next
    End Sub

    Protected Sub gvTenant_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTenant.RowCommand
        If e.CommandName = "OpenTenant" Then

            gvEBLDNGRTYPEO.Visible = True
            gvEBLDNGRTYPEC.Visible = False
            Session("OpenReq") = "4"

            idrot = e.CommandArgument
            Session("tempOTAll") = idrot
            bindPART_RQSTOT(idrot)
            If gvEBLDNGRTYPEO.Rows.Count > 0 Then
                btnEBLDNGRTYPEO.Visible = True
            Else
                btnEBLDNGRTYPEO.Visible = False

            End If


        ElseIf e.CommandName = "ClosedTenant" Then

            gvEBLDNGRTYPEO.Visible = False
            gvEBLDNGRTYPEC.Visible = True
            Session("CloseReq") = "4"
            idrct = e.CommandArgument

            Session("tempCTAll") = idrct
            bindPART_RQSTCT(idrct)
            If gvEBLDNGRTYPEC.Rows.Count > 0 Then
                btnEBLDNGRTYPEC.Visible = True
            Else
                btnEBLDNGRTYPEC.Visible = False
            End If
        End If
    End Sub
    Public Sub bindPART_RQSTOT(ByVal TEN_NAME As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_OPEN_REQS_PART_TNT")
        sp1.Command.AddParameter("@TEN_NAME", TEN_NAME, DbType.String)
        Dim ds1 As New DataSet
        ds1 = sp1.GetDataSet()
        gvEBLDNGRTYPEO.DataSource = ds1
        gvEBLDNGRTYPEO.DataBind()
        For i As Integer = 0 To gvEBLDNGRTYPEO.Rows.Count - 1
            Dim lblREQUEST_STATUS As Label = CType(gvEBLDNGRTYPEO.Rows(i).FindControl("lblREQUEST_STATUS"), Label)
            If lblREQUEST_STATUS.Text = "0" Then
                lblREQUEST_STATUS.Text = "Pending"
            ElseIf lblREQUEST_STATUS.Text = "1" Then
                lblREQUEST_STATUS.Text = "InProgress"
            Else
                lblREQUEST_STATUS.Text = "Completed"
            End If
        Next

    End Sub
    Public Sub bindPART_RQSTCT(ByVal TEN_NAME As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CLOSED_REQS_PART_TNT")
        sp.Command.AddParameter("@TEN_NAME", TEN_NAME, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        gvEBLDNGRTYPEC.DataSource = ds
        gvEBLDNGRTYPEC.DataBind()
        For i As Integer = 0 To gvEBLDNGRTYPEC.Rows.Count - 1
            Dim lblREQUEST_STATUSC As Label = CType(gvEBLDNGRTYPEC.Rows(i).FindControl("lblREQUEST_STATUSC"), Label)
            If lblREQUEST_STATUSC.Text = "0" Then
                lblREQUEST_STATUSC.Text = "Pending"
            ElseIf lblREQUEST_STATUSC.Text = "1" Then
                lblREQUEST_STATUSC.Text = "InProgress"
            Else
                lblREQUEST_STATUSC.Text = "Completed"
            End If
        Next
    End Sub

    Protected Sub gvProp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvProp.PageIndexChanging
        gvProp.PageIndex = e.NewPageIndex()
        fillgridgvProp()
    End Sub

    Protected Sub gvProp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvProp.RowCommand
        If e.CommandName = "OpenBuilding" Then
            gvEBLDNGRTYPEO.Visible = True
            gvEBLDNGRTYPEC.Visible = False

            Session("OpenReq") = "2"
            id1 = e.CommandArgument
            Session("tempO") = id1
            bindPART_BDNG(id1)
            If gvEBLDNGRTYPEO.Rows.Count > 0 Then
                btnEBLDNGRTYPEO.Visible = True
            Else
                btnEBLDNGRTYPEO.Visible = False
            End If

        ElseIf e.CommandName = "ClosedBuilding" Then

            gvEBLDNGRTYPEO.Visible = False
            gvEBLDNGRTYPEC.Visible = True
            Session("CloseReq") = "2"
            ids = e.CommandArgument
            Session("tempC") = ids
            bindPART_BDNGC(ids)
            If gvEBLDNGRTYPEC.Rows.Count > 0 Then
                btnEBLDNGRTYPEC.Visible = True
            Else
                btnEBLDNGRTYPEC.Visible = False
            End If
        End If

    End Sub
End Class