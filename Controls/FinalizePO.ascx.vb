Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Partial Class Controls_FinalizePO
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            BindGrid()
            BindFinalizeGrid()
            lblMsg.Text = ""
        End If
    End Sub

    Private Sub BindGrid()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_PO_GetByStatusId")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_GetByStatusId_BYAURID")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), Data.DbType.String)

        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
        If gvItems.Rows.Count = 0 Then
            divRemarks.Visible = False

        Else
            divRemarks.Visible = True
        End If


    End Sub

    Private Sub BindFinalizeGrid()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_PO_GetByStatusId")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[USP_AMG_ITEM_PO_GETFINALIZEPO_REMARKS]")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), Data.DbType.String)
        FinalizeGrid.DataSource = sp.GetDataSet
        FinalizeGrid.DataBind()

    End Sub

    Protected Sub FinalizeGrid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles FinalizeGrid.PageIndexChanging
        FinalizeGrid.PageIndex = e.NewPageIndex
        BindFinalizeGrid()
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnApproveAll_Click(sender As Object, e As EventArgs) Handles btnApproveAll.Click
        Dim count As Integer = 0
        For Each gvRow As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked = True Then
                count = count + 1
                Exit For
            End If
        Next
        If count > 0 Then
            For Each gvRow As GridViewRow In gvItems.Rows
                Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)


                lblMsg.Text = ""
                If chkSelect.Checked = True Then

                    Dim lblPoId As Label = DirectCast(gvRow.FindControl("lblPoId"), Label)
                    Dim lblAdvance As Label = DirectCast(gvRow.FindControl("lblAdvance"), Label)
                    Session("lblPoId") = lblPoId.Text



                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_UpdateStatusFinal")
                    sp.Command.AddParameter("@AIP_PO_ID", lblPoId.Text, DbType.String)
                    sp.Command.AddParameter("@StatusId", 5, DbType.Int32)
                    sp.Command.AddParameter("@AIP_PO_REMARKS", txtRemarks.Text, DbType.String)
                    sp.Command.AddParameter("@ADVANCE", lblAdvance.Text, DbType.String)
                    sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.String)
                    sp.ExecuteScalar()
                    DownloadPO()

                    ''send_mail_Finalize_PO(lblPoId.Text)
                End If
            Next
            lblMsg.Visible = True

            lblMsg.Text = "PO(s) has been Approved Successfully"
            Response.Redirect(Request.RawUrl)
        Else
            lblMsg.Text = "Please select PO(s) to Approve"
        End If


    End Sub
    Private Sub DownloadPO()
        Try
            Dim viewer As ReportViewer = New ReportViewer()
            Dim param() As SqlParameter
            Dim ObjSubSonic As New clsSubSonicCommonFunctions


            Dim rid As String
            rid = Session("lblPoId")
            Dim rid1 As String
            rid1 = Session("lblPoId")
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@PO", SqlDbType.NVarChar, 200)
            param(0).Value = Session("lblPoId")
            Dim ds5 As New DataSet
            ds5 = ObjSubSonic.GetSubSonicDataSet("AM_EXPORT_PO", param)
            Dim rds As New ReportDataSource()
            rds.Name = "ExportPO"
            rds.Value = ds5.Tables(0)
            viewer.Reset()
            viewer.LocalReport.DataSources.Add(rds)
            viewer.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/ExportPO.rdlc")
            Dim ci As New CultureInfo(Session("userculture").ToString())
            Dim nfi As NumberFormatInfo = ci.NumberFormat
            'Dim p1 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())
            'Dim p2 As New ReportParameter("ImageVal", BindLogo())
            viewer.LocalReport.EnableExternalImages = True
            Dim imagePath As String = BindLogo()
            Dim parameter As New ReportParameter("ImagePath", imagePath)
            viewer.LocalReport.SetParameters(parameter)

            viewer.LocalReport.Refresh()

            viewer.ProcessingMode = ProcessingMode.Local
            'viewer.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/ExportPO.rdlc")

            rid = rid.Substring(rid.Length - 4)
            Dim FileName As String = "Finalize PO" & rid & ".pdf" 'DateTime.Now.ToString("ddMMyyyyhhmmss") & ".pdf"
            ''Dim FileName As String = "Finalize PO" & DateTime.Now.ToString("ddMMyyyyhhmmss") & ".pdf"
            Dim extension As String
            Dim FileFullpath As String
            Dim encoding As String
            Dim mimeType As String
            Dim streams As String()
            Dim warnings As Warning()
            Dim contentType As String = "application/pdf"
            'Dim mybytes As Byte() = viewer.LocalReport.Render("PDF", Nothing, extension, encoding, mimeType, streams, warnings)
            FileFullpath = Server.MapPath("~/FinalizePO_pdfs/") & FileName
            'Using fs As FileStream = File.Create(Server.MapPath("~/FinalizePO_pdfs/") & FileName)
            '    fs.Write(mybytes, 0, mybytes.Length)
            'End Using

            Response.ContentType = contentType
            Response.AddHeader("Content-Disposition", "attachment; filename=" & FileName)
            ''Response.WriteFile(Server.MapPath("~/FinalizePO_pdfs/" & FileName))
            ''Response.Flush()
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "sp_get_po_mails")
            sp3.Command.AddParameter("@PO", rid1, DbType.String)
            Dim ds3 As DataSet = sp3.GetDataSet()
            If (ds3.Tables(0).Rows.Count > 0) Then
                SendMail(FileFullpath, ds3.Tables(0).Rows(0)("TOMAIL").ToString(), ds3.Tables(0).Rows(0)("CCMAIL").ToString(), rid1)
            End If
        Catch ex As Exception
            Dim s As String

        End Try
    End Sub

    'Public Sub send_mail_Finalize_PO(ByVal POnum As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_FINALIZE")
    '    sp.Command.AddParameter("@REQ_ID", POnum, DbType.String)
    '    sp.Execute()
    'End Sub
    Public Function BindLogo() As String
        'Dim imagePath As String
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Update_Get_LogoImage")
        sp3.Command.AddParameter("@type", "2", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp3.Command.AddParameter("@Tenant", Session("TENANT"), DbType.String)
        Dim ds3 As DataSet = sp3.GetDataSet()
        If ds3.Tables(0).Rows.Count > 0 Then
            Return "https://live.quickfms.com/BootStrapCSS/images/" & ds3.Tables(0).Rows(0).Item("IMAGENAME")

        Else
            Return "https://live.quickfms.com/BootStrapCSS/images/yourlogo.png"
        End If
        'Return New Uri(Server.MapPath(imagePath)).AbsoluteUri
    End Function

    'Public Function BindLogo() As String
    '    Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Update_Get_LogoImage")
    '    sp3.Command.AddParameter("@type", "2", DbType.String)
    '    sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
    '    Dim ds3 As DataSet = sp3.GetDataSet()
    '    If ds3.Tables(0).Rows.Count > 0 Then
    '        Return "http://nxtver.quickfms.com/BootStrapCSS/images/" & ds3.Tables(0).Rows(0).Item("IMAGENAME")
    '    Else
    '        Return "http://nxtver.quickfms.com/BootStrapCSS/images/yourlogo.png"
    '    End If
    'End Function

    Public Shared Sub SendMail(ByVal FileFullpath As String, ByVal TOMAIL As String, ByVal CCMAIL As String, ByVal requestId As String)

        Dim smtp As SmtpClient = New SmtpClient()
        Dim fromaddr As MailAddress = New MailAddress(ConfigurationManager.AppSettings("from"))
        smtp.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("mailid"), ConfigurationManager.AppSettings("password"))
        smtp.Host = ConfigurationManager.AppSettings("Host")
        smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings("Port"))
        smtp.EnableSsl = True
        Dim mailMessage As MailMessage = New MailMessage()
        mailMessage.From = fromaddr

        mailMessage.Subject = "AHFL Purchase Order " + requestId '"Finalize PO/" + requestId

        Dim str As String = "<!DOCTYPE html><html><body><p>Dear Sir/Madam,</p></br><p>We are glad to issue PO for supply and installation of material as stated in attached PO. Kindly get the same delivered/installed within agreed TAT and confirm us.</p></br><p>Kindly do not hesitate to contact at undersigned, in case of any clarification/discrepancies.</p><div>Best Regards,</div><div>Rupesh Kairamkonda</div><div>Dy. Manager Administration (Procurment)</div><div>Tel: (91-22) 3950 9900 | Mob: +91 98333 97314</div><div>Email: rupesh.kairamkonda@aadharhousing.com</div><p>This is an auto-generated E-mail, hence do not reply to this E-mail id. </p></body></html>"
        mailMessage.Body = str
        mailMessage.IsBodyHtml = True
        'Dim TOemailarr As String = TOMAIL--
        mailMessage.[To].Add(TOMAIL)
        'Dim TOemailarr As String() = ConfigurationManager.AppSettings("GLOBALTO").ToString().Split(";"c)

        'For Each [to] As String In TOemailarr
        '    mailMessage.[To].Add([to])
        'Next
        'Dim Bccemailarr As String = CCMAIL
        'Dim Bccemailarr As String() = ConfigurationManager.AppSettings("GLOBALBCC").ToString().Split(";"c)

        'For Each bcc As String In Bccemailarr
        mailMessage.CC.Add(CCMAIL)
        'Next

        Using attachment As Attachment = New Attachment(FileFullpath, MediaTypeNames.Application.Octet)

            Try
                Dim disposition As Mime.ContentDisposition = attachment.ContentDisposition
                disposition.CreationDate = File.GetCreationTime(FileFullpath)
                disposition.ModificationDate = File.GetLastWriteTime(FileFullpath)
                disposition.ReadDate = File.GetLastAccessTime(FileFullpath)
                disposition.FileName = Path.GetFileName(FileFullpath)
                disposition.Size = New FileInfo(FileFullpath).Length
                disposition.DispositionType = DispositionTypeNames.Attachment
                mailMessage.Attachments.Add(attachment)
            Catch ex As Exception
            End Try

            If mailMessage.[To].Count() > 0 Then smtp.Send(mailMessage)
        End Using
    End Sub
End Class
