Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_HRApproval
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If IsNumeric(txtBudgetNumber.Text) = 0 Then
                lblMsg.Text = "Please Enter Only Numerics for Budgeted Number"
                lblMsg.Visible = True
            Else
                UPDATE_FORECAST()
                Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=41")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function IsNumeric(ByVal strVal As String) As Integer
        Try
            Convert.ToInt32(strVal)
            Return 1
        Catch
            Return 0
        End Try
    End Function
    Private Sub UPDATE_FORECAST()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_HR_FORECAST_APPROVED")
        sp.Command.AddParameter("@REQ", txtSno.Text, DbType.Int32)
        sp.Command.AddParameter("@BUDGETNUMBER", txtBudgetNumber.Text, DbType.Decimal)
        sp.ExecuteScalar()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLocation()
            BindDept()
            BindDesig()
            BindGrid()
            tr1.Visible = False
            txtFnumber.ReadOnly = True
            table1.Visible = False
        End If
    End Sub
    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LOCATION_GET")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlLocation.DataSource = sp.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ' ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDept()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DEPARTMENT")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDept.DataSource = sp.GetDataSet()
            ddlDept.DataTextField = "DEP_NAME"
            ddlDept.DataValueField = "DEP_CODE"
            ddlDept.DataBind()
            'ddlDept.Items.Insert(0, New ListItem("--Select--", "0"))

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDesig()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DESIGNATION")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDesig.DataSource = sp.GetDataSet()
            ddlDesig.DataTextField = "DSN_AMT_TITLE"
            ddlDesig.DataValueField = "DSN_CODE"
            ddlDesig.DataBind()
            'ddlDesig.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_FORECAST_DETAILS")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            gvitems.DataSource = sp.GetDataSet()
            gvitems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDetails(ByVal Req As Integer)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_FORECAST_DETAILS_APPROVAL")
            sp.Command.AddParameter("@REQ", Req, DbType.Int32)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtSno.Text = ds.Tables(0).Rows(0).Item("SNO")
                ddlLocation.ClearSelection()
                Dim li As ListItem = Nothing
                li = ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("Location"))
                If Not li Is Nothing Then
                    li.Selected = True
                End If
                ddlDept.ClearSelection()
                Dim li1 As ListItem = Nothing

                li1 = ddlDept.Items.FindByValue(ds.Tables(0).Rows(0).Item("Department"))
                If Not li1 Is Nothing Then
                    li1.Selected = True
                End If
                ddlDesig.ClearSelection()
                Dim li2 As ListItem = Nothing

                li2 = ddlDesig.Items.FindByText(ds.Tables(0).Rows(0).Item("Designation"))
                If Not li2 Is Nothing Then
                    li2.Selected = True
                End If


                txtFnumber.Text = ds.Tables(0).Rows(0).Item("FORECASTED_NO")
                ddlFMonth.ClearSelection()
                ddlFMonth.Items.FindByValue(ds.Tables(0).Rows(0).Item("F_MONTH")).Selected = True
                ddlFYear.ClearSelection()
                ddlFYear.Items.FindByText(ds.Tables(0).Rows(0).Item("F_YEAR")).Selected = True
                ddlTMonth.ClearSelection()
                ddlTMonth.Items.FindByValue(ds.Tables(0).Rows(0).Item("T_MONTH")).Selected = True
                ddlTYear.ClearSelection()
                ddlTYear.Items.FindByText(ds.Tables(0).Rows(0).Item("T_YEAR")).Selected = True
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        lblMsg.Text = ""
        Dim lbtnApprove As LinkButton = DirectCast(e.CommandSource, LinkButton)
        Dim gvRow As GridViewRow = DirectCast(lbtnApprove.NamingContainer, GridViewRow)
        Dim lblsno As Label = DirectCast(gvRow.FindControl("lblsno"), Label)
        Dim Req As Integer = lblsno.Text
        If e.CommandName = "Approve" Then
            BindDetails(Req)
            table1.Visible = True
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        table1.Visible = False
        lblMsg.Text = ""
		txtBudgetNumber.Text=""
    End Sub
End Class


