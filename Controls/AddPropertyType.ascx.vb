Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic

Partial Class Controls_AddPropertyType
    Inherits System.Web.UI.UserControl
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Page.IsValid Then
            If rbActions.Checked = True Then
                lblMsg.Text = ""
                Dim ValidateCode As Integer
                ValidateCode = ValidatePropertyCode()
                If ValidateCode = 0 Then
                    lblMsg.Text = "PropertyType Already Exist..."
                ElseIf ValidateCode = 1 Then
                    insertnewrecord()
                    fillgrid()
                    ' insertRole()
                    lblMsg.Text = "Property Type Added Successfully"
                End If
                'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=1")
                cleardata()
            Else
                Try
                    lblMsg.Text = ""
                    Dim ValidateCode As Integer
                    ValidateCode = ValidatePropertyCode1()
                    If ValidateCode = 0 Then
                        lblMsg.Text = "Property Type Already Exist..."
                    ElseIf ValidateCode = 1 Then
                        Updaterecord()
                        fillgrid()
                        'updaterole()
                    End If
                    lblMsg.Text = "Property Type Modified Successfully"
                    cmbPKeys.ClearSelection()
                    'ddllease.ClearSelection()
                    cleardata()

                    'lblMsg.Visible = True
                    'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=35")

                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try
            End If

        Else
            lblMsg.Text = ""
        End If





    End Sub

    'Private Sub insertRole()
    '    Try

    '        Dim li As ListItem
    '        For Each li In lirole.Items
    '            ' For Each li As ListItem In lirole.Items
    '            If li.Selected = True Then
    '                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_PROPERTY_ROLE")
    '                sp.Command.AddParameter("@PROPCODE", txtPropType.Text, DbType.String)
    '                sp.Command.AddParameter("@ROLEID", li.Value, DbType.String)
    '                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
    '                sp.ExecuteScalar()
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    Public Function ValidatePropertyCode()
        Dim ValidateCode As Integer
        Dim PN_PROPERTYTYPE As String = txtPropType.Text
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_PROPERTY_CODE")
        sp1.Command.AddParameter("@PN_PROPERTYTYPE", PN_PROPERTYTYPE, DbType.String)
        sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        ValidateCode = sp1.ExecuteScalar()
        Return ValidateCode
    End Function

    Public Sub insertnewrecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADD_PROPERTY")
            sp1.Command.AddParameter("@PN_PROPERTYTYPE", txtPropType.Text, DbType.String)
            sp1.Command.AddParameter("@PN_TYPESTATUS", ddlStatus.SelectedItem.Value, DbType.Int32)
            'sp1.Command.AddParameter("@LEASE_STA", ddllease.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp1.ExecuteScalar()
            fillgrid()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub cleardata()
        txtPropType.Text = ""
        cmbPKeys.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
        'lirole.SelectedIndex = -1
        'ddllease.SelectedIndex = 0
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim PN_PROPERTYTYPE As String = txtSearch.Text
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PRPTYPE_DETGRD")
        sp2.Command.AddParameter("@PN_PROPERTYTYPE", PN_PROPERTYTYPE, DbType.String)
        sp2.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        gvPropType.DataSource = sp2.GetDataSet()
        gvPropType.DataBind()
        For i As Integer = 0 To gvPropType.Rows.Count - 1
            Dim lblstatus As Label = CType(gvPropType.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "InActive"
            End If
        Next
        lblMsg.Visible = False
        lbtn1.Visible = True
    End Sub

    Protected Sub lbtn1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtn1.Click
        If Not IsPostBack Then
            gvPropType.PageIndex = 0
        End If
        fillgrid()
        txtSearch.Text = ""
        lblMsg.Visible = False
    End Sub

    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PRPTYPE_DETGRD")
        sp.Command.AddParameter("@PN_PROPERTYTYPE", "", DbType.String)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        Dim ds As New DataSet

        ds = sp.GetDataSet
        gvPropType.DataSource = ds
        gvPropType.DataBind()
        'If gvPropType.Rows.Count > 0 Then
        '    pnl.Visible = True
        '    btnexporttoexcel.Enabled = True
        'Else
        '    pnl.Visible = False
        '    btnexporttoexcel.Enabled = False
        'End If

        For i As Integer = 0 To gvPropType.Rows.Count - 1
            Dim lblstatus As Label = CType(gvPropType.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "InActive"
            End If
        Next
        lbtn1.Visible = False

    End Sub

    Protected Sub gvPropType_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPropType.PageIndexChanging
        gvPropType.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/MaintenanceManagement/Masters/Mas_Webfiles/frmPropMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If

        revPropertyName.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()



        If Not IsPostBack Then
            lblMsg.Text = ""
            trPropertyType.Visible = False
            gvPropType.PageIndex = 0
            'lirole.SelectedIndex = -1
            'BindRole()
            fillgrid()
            BindPropertyType()
            rbActions.Checked = True
            lbtn1.Visible = False
            gvPropType.Visible = True
            If gvPropType.Rows.Count > 0 Then
                pnl.Visible = True
            Else
                pnl.Visible = False
            End If
        ElseIf rbActions.Checked = True Then
            btnSubmit.Text = "Add"
            'lirole.SelectedIndex = -1
        Else

            btnSubmit.Text = "Modify"
        End If
    End Sub

    'Private Sub BindRole()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETROLES")
    '        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
    '        lirole.DataSource = sp.GetDataSet()
    '        lirole.DataTextField = "ROL_DESCRIPTION"
    '        lirole.DataValueField = "ROL_ID"
    '        lirole.DataBind()
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    'Protected Sub gvPropType_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)

    '    If e.CommandName = "UpdateStatus" Then
    '        Dim lnkStatus As LinkButton = DirectCast(e.CommandSource, LinkButton)
    '        Dim gvRow As GridViewRow = DirectCast(lnkStatus.NamingContainer, GridViewRow)
    '        Dim status As Integer = CInt(e.CommandArgument)
    '        Dim lblId As Label = DirectCast(gvRow.FindControl("lblId"), Label)
    '        Response.Write(lblId.Text)
    '        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_PRP_STATUS")
    '        sp2.Command.AddParameter("@PN_TYPEID", lblId.Text, DbType.Int32)

    '        sp2.Command.AddParameter("@PN_TYPESTATUS", 1 - status, DbType.Int32)
    '        sp2.ExecuteScalar()
    '    End If
    '    fillgrid()
    'End Sub
    'Protected Sub btnexporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexporttoexcel.Click
    '    exportpanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
    '    exportpanel1.FileName = "Property Type"
    'End Sub
    ' Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '   Response.Redirect("frmPropertyMasters.aspx")
    ' End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged


        If rbActions.Checked = True Then
            cleardata()
            'lirole.SelectedIndex = -1
            trPropertyType.Visible = False
        Else

            BindPropertyType()
            trPropertyType.Visible = True
            cmbPKeys_SelectedIndexChanged(cmbPKeys, EventArgs.Empty)
        End If
    End Sub

    Private Sub BindPropertyType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PRPTYPE_DETGRD")
        sp.Command.AddParameter("@PN_PROPERTYTYPE", "", DbType.String)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        'cmbPKeys.DataSource = sp.GetReader()
        'cmbPKeys.DataTextField = "PN_PROPERTYTYPE"
        'cmbPKeys.DataValueField = "PN_TYPEID"
        'cmbPKeys.DataBind()
        cmbPKeys.DataSource = sp.GetDataSet()
        cmbPKeys.DataTextField = "PN_PROPERTYTYPE"
        cmbPKeys.DataValueField = "PN_TYPEID"
        cmbPKeys.DataBind()
        cmbPKeys.Items.Insert(0, New ListItem("--Select--", 0))
        'cmbPKeys.DataTextField = "LESSOR_NAME"
        'cmbPKeys.DataValueField = "LESSOR_ID"

    End Sub

    Protected Sub cmbPKeys_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPKeys.SelectedIndexChanged
        BindProperty()

        'lirole.SelectedIndex = -1
        'BindPropertyRole()

        'If cmbPKeys.SelectedIndex > 0 Then
        '    BindProperty()
        '    lirole.SelectedIndex = -1
        '    BindPropertyRole()
        'Else
        '    cleardata()
        'End If
    End Sub

    Private Sub BindProperty()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_PROPERTY_ROLE_DETAILS")
        sp.Command.AddParameter("@sno", cmbPKeys.SelectedItem.Value, Data.DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            txtPropType.Text = ds.Tables(0).Rows(0).Item("PN_PROPERTYTYPE")

            'ddllease.ClearSelection()
            'ddllease.Items.FindByValue(ds.Tables(0).Rows(0).Item("LEASE_STA")).Selected = True

            'ddllease.ClearSelection()
            'ddllease.SelectedValue = ds.Tables(0).Rows(0).Item("LEASE_STA")
            Dim stat As Integer = ds.Tables(0).Rows(0).Item("PN_TYPESTATUS")
            If stat = 0 Then
                ddlStatus.SelectedValue = 0
            Else
                ddlStatus.SelectedValue = 1
            End If
        End If
    End Sub

    'Private Sub BindPropertyRole()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROPERTY_ROLE")
    '        sp.Command.AddParameter("@sno", cmbPKeys.SelectedItem.Value, DbType.Int32)
    '        Dim dr As SqlDataReader
    '        dr = sp.GetReader()
    '        While dr.Read()
    '            'Dim li As ListItem = Nothing
    '            lirole.Items.FindByValue(dr("ROLEID")).Selected = True

    '            'If Not li Is Nothing Then
    '            '    li.Selected = True
    '            'End If
    '        End While

    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    Private Function ValidatePropertyCode1()
        Dim validatecode As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_PROPERTY_CODE1")
        sp.Command.AddParameter("@Property_TYPE", txtPropType.Text, DbType.String)
        sp.Command.AddParameter("@sno", cmbPKeys.SelectedItem.Value, DbType.Int32)
        validatecode = sp.ExecuteScalar()
        Return validatecode
    End Function

    Private Sub Updaterecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_PROPERTY_TYPE")
            sp1.Command.AddParameter("@sno", cmbPKeys.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@PN_PROPERTYTYPE", txtPropType.Text, DbType.String)
            sp1.Command.AddParameter("@PN_TYPESTATUS", ddlStatus.SelectedItem.Value, DbType.Int32)
            'sp1.Command.AddParameter("@PN_LEASESTATUS", ddllease.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp1.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Private Sub updaterole()
    '    Try
    '        DELROLE()
    '        For Each li As ListItem In lirole.Items
    '            If li.Selected = True Then
    '                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_PROPERTY_ROLE")
    '                sp.Command.AddParameter("@PROPCODE", txtPropType.Text, DbType.String)
    '                sp.Command.AddParameter("@ROLEID", li.Value, DbType.String)
    '                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
    '                sp.ExecuteScalar()
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    Private Sub delrole()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "del_propertyrole")
            sp.Command.AddParameter("@propertyid", cmbPKeys.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~/MaintenanceManagement/Masters/Mas_Webfiles/frmPropMasters.aspx")
    End Sub

    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub
End Class