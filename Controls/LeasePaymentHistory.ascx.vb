Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_LeasePaymentHistory
    Inherits System.Web.UI.UserControl

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If txtempid.Text <> "" Then
            BindGrid()
        Else
            lblMsg.Text = "Please Enter Any Search Criteria"
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtfromdate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
        If Not IsPostBack Then
            'txtempid.Text = Session("UID")

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CHECK_ADMIN_OR_INCHARGE")
            sp.Command.AddParameter("@AUR_ID", Session("UID").ToString(), DbType.String)
            Dim str As String = sp.ExecuteScalar()

            If str = "1" Then
                txtempid.Enabled = True
            Else
                txtempid.Enabled = False
            End If

            TR.Visible = False
            btnGo.Visible = False

            'txtfromdate.Text = Date.Today
            'txtfromdate.Attributes.Add("onClick", "displayDatePicker('" + txtfromdate.ClientID + "')")
            'txtfromdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

            'txtToDate.Text = Date.Today
            'txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
            'txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

        End If
    End Sub

    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GETLEASES_PAYMENTHISTORY")
            sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)
            gvitems.DataSource = sp.GetDataSet()
            gvitems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "View" Then
            
            Dim lnkviewdetails As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkviewdetails.NamingContainer, GridViewRow)
            Dim lbllease As Label = DirectCast(gvRow.FindControl("lbllease"), Label)
            txtstore.Text = lbllease.Text
            pnlgrid2.Visible = True
            BindGrid2()
            If gvitems1.Rows.Count > 0 Then
                TR.Visible = True
                btnGo.Visible = True
            Else
                TR.Visible = False
                btnGo.Visible = False
            End If
        Else
            pnlgrid2.Visible = False
            TR.Visible = False
            btnGo.Visible = False
        End If
    End Sub

    Private Sub BindGrid2()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GETPAYMENTS1")
            sp.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
            If txtfromdate.Text = "" Then
                sp.Command.AddParameter("@FROM_DATE", DBNull.Value, DbType.Date)
            Else
                sp.Command.AddParameter("@FROM_DATE", txtfromdate.Text, DbType.Date)
            End If

            If txtToDate.Text = "" Then
                sp.Command.AddParameter("@TO_DATE", DBNull.Value, DbType.Date)
            Else
                sp.Command.AddParameter("@TO_DATE", txtToDate.Text, DbType.Date)
            End If


            gvitems1.DataSource = sp.GetDataSet()
            gvitems1.DataBind()

            For i As Integer = 0 To gvitems1.Rows.Count - 1
                Dim lblpayment As Label = CType(gvitems1.Rows(i).FindControl("lblpayment"), Label)
                If lblpayment.Text = 1 Then
                    lblpayment.Text = "DD"
                ElseIf lblpayment.Text = 2 Then
                    lblpayment.Text = "Account Credit"
                ElseIf lblpayment.Text = 3 Then
                    lblpayment.Text = "NEFT"
                End If
            Next

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems1.PageIndexChanging
        gvitems1.PageIndex = e.NewPageIndex()
        BindGrid2()
    End Sub

    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        If txtfromdate.Text = "" Then
            lblMsg.Text = "Please Enter From Date"
        ElseIf txtToDate.Text = "" Then
            lblMsg.Text = "Please Enter TO Date"
        ElseIf CDate(txtfromdate.Text) > CDate(txtToDate.Text) Then
            lblMsg.Text = "To Date Should be greater than from Date"
        Else
            lblMsg.Text = ""

            BindGrid2()
            '' ''Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GETPAYMENTS1")
            '' ''sp.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
            '' ''sp.Command.AddParameter("@FROM_DATE", txtfromdate.Text, DbType.Date)
            '' ''sp.Command.AddParameter("@TO_DATE", txtToDate.Text, DbType.Date)
            '' ''gvitems1.DataSource = sp.GetDataSet()
            '' ''gvitems1.DataBind()
            '' ''For i As Integer = 0 To gvitems1.Rows.Count - 1
            '' ''    Dim lblpayment As Label = CType(gvitems1.Rows(i).FindControl("lblpayment"), Label)
            '' ''    If lblpayment.Text = 1 Then
            '' ''        lblpayment.Text = "DD"
            '' ''    ElseIf lblpayment.Text = 2 Then
            '' ''        lblpayment.Text = "Account Credit"
            '' ''    ElseIf lblpayment.Text = 3 Then
            '' ''        lblpayment.Text = "NEFT"
            '' ''    End If
            ''''Next
        End If

    End Sub
End Class
