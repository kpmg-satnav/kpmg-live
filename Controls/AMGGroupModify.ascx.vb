Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AMGGroupModify
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
     
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDT_AMG_GROUP")

            sp1.Command.AddParameter("@AAG_CODE", Request.QueryString("code"), DbType.String)
            sp1.Command.AddParameter("@AAG_NAME", txtName.Text, DbType.String)
            sp1.Command.AddParameter("@AAG_STA_ID", rdbtnGroupID.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@AAG_UPT_BY", Session("Uid"), DbType.String)

            sp1.Command.AddParameter("@AAG_REM", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@AAG_EXH_CODE", txtEXHCode.Text, DbType.String)
            sp1.Command.AddParameter("@AAG_MFType", txtMFType.Text, DbType.String)

            sp1.ExecuteScalar()
            lblMsg.Text = "Data Modified succesfully"
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Public Sub Cleardata()
        txtName.Text = ""

        txtRemarks.Text = ""
        txtMFType.Text = ""
        txtEXHCode.Text = ""

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim AAG_CODE As String = Request.QueryString("code")
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_GROUP_CODE")
            sp3.Command.AddParameter("@AAG_CODE", AAG_CODE, DbType.String)
            Dim DS3 As New DataSet
            DS3 = sp3.GetDataSet
            If DS3.Tables(0).Rows.Count > 0 Then
                txtName.Text = DS3.Tables(0).Rows(0).Item("AAG_NAME")

                Dim rdbtnstaid As Integer = DS3.Tables(0).Rows(0).Item("AAG_STA_ID")
                If rdbtnstaid = 0 Then
                    rdbtnGroupID.SelectedValue = "0"
                Else
                    rdbtnGroupID.SelectedValue = "1"
                End If
                txtRemarks.Text = DS3.Tables(0).Rows(0).Item("AAG_REM")
                txtEXHCode.Text = DS3.Tables(0).Rows(0).Item("AAG_EXH_CODE")
                txtMFType.Text = DS3.Tables(0).Rows(0).Item("AAG_MFTYPE")
            End If


        End If

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmAMGGroupGetDetails.aspx")
    End Sub
End Class
