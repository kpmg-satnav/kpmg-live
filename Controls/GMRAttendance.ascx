<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GMRAttendance.ascx.vb" Inherits="Controls_GMRAttendance" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>

<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

<script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>

<script language="javascript" type="text/javascript" src="../../Scripts/Cal.js"></script>

<div>
    <table id="table2" cellspacing="0" cellpadding="0" width="98%" align="center" border="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">GMR Attendance Register<hr align="center" width="60%" /> </asp:Label>
            </td>
        </tr>
        <tr>
            <td width="100%" align="center">
            </td>
        </tr>
    </table>
    <table id="Table3" cellspacing="0" cellpadding="0" style="vertical-align: top; width: 95%"
        align="center" border="0">
        <tr>
            <td colspan="3" align="left">
                <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 10px">
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" />
            </td>
            <td class="tableHEADER" align="left" style="width: 100%">
                &nbsp;<strong>GMRAttendance Register</strong></td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
            </td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                
                   
              
             
                <table id="tablegrid" runat="server" cellpadding="0" cellspacing="0" width="100%"
                    border="1">
                    <tr>
                        <td align="left" style="height: 26px; width: 50%">
                            Select From Date <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvfrm" runat="server" ControlToValidate="txtFrmDate"
                                ErrorMessage="Please select FromDate" Display="none" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 26px; width: 50%">
                            <asp:TextBox ID="txtFrmDate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%">
                            Select To Date <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvToDate" runat="server" ControlToValidate="txtToDate"
                                ErrorMessage="Please select FromDate" Display="none" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="tevtodate" runat="server" Type="Date" Operator="LessThanEqual"
                                                                    ErrorMessage="To-Date Should be More than From-Date!"
                                                                    ControlToValidate="txtFrmDate" ControlToCompare="txtToDate" ValidationGroup="Val1" Display="None"></asp:CompareValidator>
                        </td>
                        <td align="left" style="height: 26px; width: 50%">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" ValidationGroup="Val1"
                                CausesValidation="True" />
                        </td>
                    </tr>
                   <%-- </table>
                    <table align="center" runat="server" cellpadding="0" cellspacing="0" width="1%">--%>
                    <tr>
                    <td align="right" colspan="2" >
                    <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export to Excel" />
                    </td>
                    </tr>
                    <tr>
                        <td style="height: 20px" colspan="2" >
                         <cc1:ExportPanel ID="ExportPanel" runat="server">
                            <asp:GridView ID="gvItems" runat="server" Width="100%" AllowPaging="false" AutoGenerateColumns="true"
                                EmptyDataText="Sorry! No Records Found">
                                
                            </asp:GridView>
                             </cc1:ExportPanel>
                        </td>
                    </tr>
                </table>
               
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px; width: 524px;" background="../../Images/table_bot_mid_bg.gif">
                <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>