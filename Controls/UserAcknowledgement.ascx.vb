﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic

Imports clsSubSonicCommonFunctions
Partial Class Controls_UserAcknowledgement
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        lblMsg.Text = ""
        'remarks.Text = ""
        btnSubmit.Visible = False

        If Not IsPostBack Then
            BindFiltersData()
            BindLocation()
            getassetcategory()
            getsubcategorybycat(ddlastCat.SelectedItem.Value)
            'ddlastsubCat.SelectedIndex = If(ddlastsubCat.Items.Count > 1, 1, 0)
            getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
            'ddlAstBrand.SelectedIndex = If(ddlAstBrand.Items.Count > 1, 1, 0)
            getModelbycatsubcat()
            'ddlAstModel.SelectedIndex = If(ddlAstModel.Items.Count > 1, 1, 0)
            BindGrid(Session("uid"))
        End If

    End Sub
    Protected Sub BindFiltersData()
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AST_APPR_DETAILS")
        sp2.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
        Dim ast = sp2.ExecuteScalar()
        If ast = 1 Then
            'divSearch.Visible = True
            BindFilters.Visible = True
            'remk.Visible = True
        Else
            '   divSearch.Visible = False
            BindFilters.Visible = False
            '  remk.Visible = False


        End If

    End Sub



    Private Sub BindGrid(ByVal EmpId As String)
        Try


            Dim param(6) As SqlParameter

            param(0) = New SqlParameter("@AAT_EMP_ID", SqlDbType.NVarChar, 200)
            param(0).Value = EmpId
            param(1) = New SqlParameter("@categoryID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlastCat.SelectedItem.Value
            param(2) = New SqlParameter("@SubcategoryID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlastsubCat.SelectedItem.Value
            param(3) = New SqlParameter("@brand", SqlDbType.NVarChar, 200)
            param(3).Value = ddlAstBrand.SelectedItem.Value
            param(4) = New SqlParameter("@Model", SqlDbType.NVarChar, 200)
            param(4).Value = ddlAstModel.SelectedItem.Value
            'param(5) = New SqlParameter("@Emp_Name", SqlDbType.NVarChar, 200)
            'param(5).Value = txtSearchEmpName.Text
            'param(6) = New SqlParameter("@Asset_Name", SqlDbType.NVarChar, 200)
            'param(6).Value = txtAssetName.Text
            'param(7) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
            'param(7).Value = ddlLocation.SelectedItem.Value
            param(5) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
            param(5).Value = ddlLocation.SelectedItem.Value
            param(6) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
            param(6).Value = Session("COMPANYID")
            ObjSubSonic.BindGridView(gvItems, "GET_TAGDTLS", param)
            If gvItems.Rows.Count = 0 Then
                'txtremarks.Visible = False
                'divSearch.Visible = False
                btnSubmit.Visible = False

            Else
                btnSubmit.Visible = True

            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Protected Sub gvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItems.RowDataBound
    '    If (e.Row.RowType = DataControlRowType.DataRow) Then
    '        Dim txtSurrendered_TAG As TextBox = CType(e.Row.Cells(0).FindControl("txtSurrendered_TAG"), TextBox)
    '        txtSurrendered_TAG.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
    '        txtSurrendered_TAG.Attributes.Add("onClick", "displayDatePicker('" + txtSurrendered_TAG.ClientID + "')")
    '    End If
    'End Sub
    Protected Sub gvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItems.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim txtLnkText As LinkButton = CType(e.Row.Cells(0).FindControl("lnkAcknowledge"), LinkButton)
            If txtLnkText.Text = "DONE" Then
                txtLnkText.Enabled = False
            Else
                txtLnkText.Enabled = True
            End If

        End If
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        Try
            If e.CommandName = "Acknowledge" Then
                Dim lnkAcknowledge As LinkButton = DirectCast(e.CommandSource, LinkButton)
                Dim gvRow As GridViewRow = DirectCast(lnkAcknowledge.NamingContainer, GridViewRow)
                'Dim txtSurrendered_TAG As TextBox = DirectCast(gvRow.FindControl("txtSurrendered_TAG"), TextBox)
                Dim lblAur_id As Label = DirectCast(gvRow.FindControl("lblAur_id"), Label)
                Dim lblAstSno As Label = DirectCast(gvRow.FindControl("lblAstSno"), Label)
                '  If txtSurrendered_TAG.Text IsNot Nothing And txtSurrendered_TAG.Text <> "" Then
                '    Dim sdate As Date = Convert.ToDateTime(txtSurrendered_TAG.Text)
                Dim ddlAck As DropDownList = DirectCast(gvRow.FindControl("ddlAck"), DropDownList)
                Dim lblCode As Label = DirectCast(gvRow.FindControl("lblCode"), Label)
                Dim stat As Integer = 1035
                Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USER_ACKNOWLEDGEMENT")
                sp2.Command.AddParameter("@acode", lblCode.Text, DbType.String)
                sp2.Command.AddParameter("@EMPID", lblAur_id.Text, DbType.String)
                sp2.Command.AddParameter("@AAT_TAG_STATUS", stat, DbType.Int32)
                sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
                sp2.Command.AddParameter("@ACK_STATUS", ddlAck.SelectedValue, DbType.String)
                sp2.ExecuteScalar()
                lblMsg.Text = "Asset  Acknowledged Successfully "
                'Else
                '    lblMsg.Text = "Please select Date"
                'End If

            End If

            BindGrid(Session("uid"))
            panel1.Visible = True
        Catch ex As Exception

        End Try
    End Sub

    Public Sub send_mail(ByVal reqid As String, ByVal AAT_CODE As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_SURRENDER_REQUISITION")
        sp1.Command.AddParameter("@REQID", reqid, DbType.String)
        sp1.Command.AddParameter("@AAT_CODE", AAT_CODE, DbType.String)
        sp1.Execute()
    End Sub




    Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid(Session("uid"))
    End Sub


    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindGrid(Session("uid"))
    End Sub
    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")

        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        ddlLocation.Items.Insert(0, New ListItem("--All--", "All"))
        ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 1, 0)

        'ddlLocation.Items.Remove("--All--")
    End Sub
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlastCat.DataSource = sp.GetDataSet()
        ddlastCat.DataTextField = "VT_TYPE"
        ddlastCat.DataValueField = "VT_CODE"
        ddlastCat.DataBind()
        'ddlastCat.Items.Insert(0, "--Select--")
        ddlastCat.Items.Remove("--Select--")
        ddlastCat.Items.Insert(0, New ListItem("--All--", "All"))
        'ddlastCat.SelectedIndex = If(ddlastCat.Items.Count > 1, 1, 0)

    End Sub
    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        ddlastsubCat.DataSource = sp.GetDataSet()
        ddlastsubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlastsubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlastsubCat.DataBind()
        ' ddlastsubCat.Items.Insert(0, "--Select--")
        ddlastsubCat.Items.Insert(0, New ListItem("--All--", "All"))

    End Sub
    Private Sub getbrandbycatsubcat(ByVal astcatcode As String, ByVal astsubcatcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlastsubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--All--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        'ddlAstModel.Items.Insert(0, New ListItem("--All--", ""))


    End Sub
    Private Sub getModelbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlastsubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))


    End Sub
    Protected Sub ddlastCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlastCat.SelectedIndexChanged
        getsubcategorybycat(ddlastCat.SelectedItem.Value)
        getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
        getModelbycatsubcat()
    End Sub
    Protected Sub ddlastsubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlastsubCat.SelectedIndexChanged
        If ddlastsubCat.SelectedIndex > 0 Then
            getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
            getModelbycatsubcat()

        End If

    End Sub
    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        If ddlAstBrand.SelectedIndex > 0 Then
            getModelbycatsubcat()
        End If

    End Sub

    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub
    Public Sub cleardata()
        ddlastCat.ClearSelection()
        ddlastsubCat.ClearSelection()
        ddlLocation.ClearSelection()
        ddlAstBrand.ClearSelection()
        ddlAstModel.ClearSelection()
        gvItems.DataSource = Nothing
        gvItems.DataBind()
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid(Session("uid"))
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        BindGrid(Session("uid"))
    End Sub


    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim count As Integer = 0
        Dim ast_code As String = String.Empty
        For Each gvRow As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked = True Then
                count = count + 1
                Exit For
            End If
        Next
        If count > 0 Then

            For Each gvRow As GridViewRow In gvItems.Rows
                Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)
                lblMsg.Text = ""
                If chkSelect.Checked = True Then
                    'Dim txtSurrendered_TAG As TextBox = DirectCast(gvRow.FindControl("txtSurrendered_TAG"), TextBox)

                    'If txtSurrendered_TAG.Text.ToString() = "" Then
                    '    lblMsg.Text = "Please select Date"
                    '    Exit Sub
                    'Else



                    '    Dim sdate As Date = Date.Parse(txtSurrendered_TAG.Text)

                    '    Dim lblCode As Label = DirectCast(gvRow.FindControl("lblCode"), Label)
                    '    Dim lblAur_id As Label = DirectCast(gvRow.FindControl("lblAur_id"), Label)
                    '    ast_code = ast_code & "" & lblCode.Text & ",<br>"
                    '    Dim lblAstSno As Label = DirectCast(gvRow.FindControl("lblAstSno"), Label)
                    '    Dim ddlAck As DropDownList = DirectCast(gvRow.FindControl("ddlAck"), DropDownList)
                    '    Dim stat As Integer = 1035


                    '    Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_TAG_STATUS")
                    '    sp2.Command.AddParameter("@acode", lblCode.Text, DbType.String)
                    '    sp2.Command.AddParameter("@EMPID", lblAur_id.Text, DbType.String)
                    '    sp2.Command.AddParameter("@AAT_SURRENDERED_DATE", sdate, DbType.Date)
                    '    sp2.Command.AddParameter("@AAT_TAG_STATUS", stat, DbType.Int32)
                    '    sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.Int32)
                    '    sp2.Command.AddParameter("@ACK_STATUS", ddlAck.SelectedValue, DbType.Int32)
                    '    sp2.Execute()


                    'End If                 

                    Dim lblAur_id As Label = DirectCast(gvRow.FindControl("lblAur_id"), Label)
                    Dim lblAstSno As Label = DirectCast(gvRow.FindControl("lblAstSno"), Label)
                    Dim ddlAck As DropDownList = DirectCast(gvRow.FindControl("ddlAck"), DropDownList)
                    Dim lblCode As Label = DirectCast(gvRow.FindControl("lblCode"), Label)
                    Dim stat As Integer = 1035
                    Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USER_ACKNOWLEDGEMENT")
                    sp2.Command.AddParameter("@acode", lblCode.Text, DbType.String)
                    sp2.Command.AddParameter("@EMPID", lblAur_id.Text, DbType.String)
                    sp2.Command.AddParameter("@AAT_TAG_STATUS", stat, DbType.Int32)
                    sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
                    sp2.Command.AddParameter("@ACK_STATUS", ddlAck.SelectedValue, DbType.String)
                    sp2.ExecuteScalar()
                    'Else

                End If
            Next
            lblMsg.Text = "Assets  Acknowledged Successfully "
        Else
            lblMsg.Text = "Please select the assets to Acknowledge "
        End If
        BindGrid(Session("uid"))
        panel1.Visible = True
    End Sub

End Class
