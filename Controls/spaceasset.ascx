﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="spaceasset.ascx.vb" Inherits="Controls_spaceasset" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<table ID="tab" runat="server" CellPadding="1" CellSpacing="0" Border="1" style="text-align:center " width="100%">
  
    <tr>
        <td colspan="2">

            <asp:Label ID="lblmsg" runat="server"></asp:Label>
        </td>

    </tr>
      <tr>
        <td align="left" style="height:26px;width:50%">
            Asset code<font class="clsNote">*</font>
                             <asp:RequiredFieldValidator ID="rfvempid" runat="server"  ControlToValidate="txtasset" Display="Dynamic" ErrorMessage="Please Enter Asset Code" Enabled="true"></asp:RequiredFieldValidator>

        </td>
        <td align="left" style="height:26px;width:50%">
           <asp:TextBox ID="txtasset" runat="server" CssClass="clsTextField" Width="97%" AutoComplete="off" AutoCompleteType="Disabled"></asp:TextBox>
                             <cc1:AutoCompleteExtender ID="autoCompleteEx1" runat="server" BehaviorID="AutoCompleteEx1" CompletionSetCount="5" MinimumPrefixLength="1" TargetControlID="txtasset" ServicePath="~/Searchusers1.asmx" ServiceMethod="SearchUser1" ContextKey="0" CompletionInterval="1000" DelimiterCharacters=";,">
                             </cc1:AutoCompleteExtender>
        </td>

    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Place Asset" CausesValidation="true"  />
        </td>

    </tr>

</table>


