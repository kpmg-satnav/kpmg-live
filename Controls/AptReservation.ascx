<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AptReservation.ascx.vb"
    Inherits="Controls_AptReservation" %>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvItems" runat="server" EmptyDataText="No More Domestic Conveyance To Approve."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Requisition ID" Visible="True">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="Server" Text='<%#Eval("CNV_REQ_ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested By">
                        <ItemTemplate>
                            <asp:Label ID="lblAur_id" runat="server" Text='<%#Eval("CNV_AUR_ID")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:ButtonField Text="View Details" CommandName="View" />
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>

<div id="pnl" runat="server">

    <div class="row">
      <div class="col-md-12">
            <div class="row">
                <asp:GridView ID="gvrm" runat="server"
                    RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                    AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="FALSE">
                            <ItemTemplate>
                                <asp:Label ID="lblID1" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkbx" runat="server" />

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Journey Date">
                            <ItemTemplate>
                                <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("CNV_JOURNEY_DATE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mode of Expense">
                            <ItemTemplate>
                                <asp:Label ID="lblMode" runat="server" Text='<%#Eval("CNV_eXPENSE_MODE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mode of Travel">
                            <ItemTemplate>
                                <asp:Label ID="lblTmode" runat="server" Text='<%#Eval("CNV_TRVL_MODE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Approx. KMs.">
                            <ItemTemplate>
                                <asp:Label ID="lblAppkm" runat="server" Text='<%#Eval("CNV_KM")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Purpose of Travel">
                            <ItemTemplate>
                                <asp:Label ID="lblpurs" runat="server" Text='<%#Eval("CNV_PURPOSE_TRVL")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("CNV_TOTAL_AMOUNT", "{0:c2}")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                            <ItemTemplate>
                                <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("CNV_rEMARKS")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>

    <div id="tr1" runat="server">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                            Display="None" ErrorMessage="Please Enter Remarks " ValidationGroup="Val2"></asp:RequiredFieldValidator>
                        <div class="col-md-7">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                                Rows="3" MaxLength="500"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnsubmit" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val2"
                    CausesValidation="true" />
                <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val2"
                    CausesValidation="true" />
                <asp:TextBox ID="txtstore" runat="Server" CssClass="form-control" Visible="false"></asp:TextBox>
            </div>
        </div>
    </div>
</div>
<div id="t1" runat="server" visible="False">
    <div class="row">
      <div class="col-md-12">
            <div class="row">
                <asp:GridView ID="gvdetails" runat="server"
                    RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                    AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="FALSE">
                            <ItemTemplate>
                                <asp:Label ID="lblID1" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Journey Date">
                            <ItemTemplate>
                                <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("CNV_JOURNEY_DATE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mode of Expense">
                            <ItemTemplate>
                                <asp:Label ID="lblMode" runat="server" Text='<%#Eval("CNV_EXPENSE_MODE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mode of Travel">
                            <ItemTemplate>
                                <asp:Label ID="lblTmode" runat="server" Text='<%#Eval("CNV_TRVL_MODE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Approx. KMs.">
                            <ItemTemplate>
                                <asp:Label ID="lblAppkm" runat="server" Text='<%#Eval("CNV_KM")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Purpose of Travel">
                            <ItemTemplate>
                                <asp:Label ID="lblpurs" runat="server" Text='<%#Eval("CNV_PURPOSE_TRVL")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("CNV_TOTAL_AMOUNT", "{0:c2}")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                            <ItemTemplate>
                                <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("CNV_rEMARKS")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RM Remarks">
                            <ItemTemplate>
                                <asp:Label ID="lblrmRemarks" runat="server" Text='<%#Eval("CNV_RM_rEMARKS")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>
</div>
