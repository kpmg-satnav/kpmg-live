<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EmployeeClaimedNMappedAssets.ascx.vb"
    Inherits="Controls_EmployeeClaimedNMappedAssets" %>
    
<script language="javascript" type="text/javascript" src="<%=Page.ResolveUrl("~/Scripts/DateTimePicker.js")%>"></script>
<div>
    

                <table width="100%" cellpadding="2">
                    <tr>
                        <td align="left" valign="top" >
                            <fieldset>
                                <legend>Mapped Assets </legend>
                                <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                    EmptyDataText="No Asset(s) Found." Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="server" Text='<%#Eval("AST_SNO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AssetCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcode" runat="server" Text='<%#Eval("AAT_AST_CODE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AssetName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblname" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Surrendered Date">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtSurrendered_TAG" runat="server" CssClass="textBox" Text='<%#Eval("AAT_SURRENDERED_DATE")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Surrender">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnksurrender" runat="server" CommandName="Surrender">Surrender</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <fieldset>
                                <legend>To be Claimed Assets</legend>
                                <asp:GridView ID="gvClaimedAsts" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                    EmptyDataText="No Asset(s) Found." Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="server" Text='<%#Eval("AST_SNO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AssetCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcode" runat="server" Text='<%#Eval("AAT_AST_CODE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AssetName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblname" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbldate" runat="server" Text='<%#Eval("AAT_SURRENDERED_DATE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Claim Now">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkClaim" runat="server" CommandName="Claim">Claim Now</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                        </td>
                    </tr>
                </table>
           
    
</div>
