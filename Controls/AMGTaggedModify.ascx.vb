Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AMGTaggedModify
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If (ddlcode.SelectedItem.Value = "--Select--") Then
            ddlcode.Visible = False
            lblerror.Text = "No code is available to tag"
            lblerror.Visible = True
            Dim AAT_EMP_ID As String = ddlEmpID_AATTAG.SelectedItem.Value
            Dim AAT_TAG_STATUS As Integer
            If (rdbtnStatus_AATTAG.SelectedItem.Value = True) Then
                AAT_TAG_STATUS = 0
            Else
                AAT_TAG_STATUS = 1
            End If



            Try
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_AMG_TAGUPDT")
                sp1.Command.AddParameter("@AST_SNO", Request.QueryString("id"), DbType.Int32)
                sp1.Command.AddParameter("@AAT_EMP_ID", AAT_EMP_ID, DbType.String)
                sp1.Command.AddParameter("@AAT_COONR_ID", AAT_EMP_ID, DbType.String)
                sp1.Command.AddParameter("@AAT_TAG_STATUS", AAT_TAG_STATUS, DbType.Int32)
                sp1.ExecuteScalar()
                'lblMsg.Visible = True
                lblMsg.Text = "Data Modified Succesfully"
                ClearData()

            Catch ex As Exception
                Response.Write(ex.Message)
            End Try


        Else
            Dim AAT_EMP_ID As String = ddlEmpID_AATTAG.SelectedItem.Value

            Dim AAT_TAG_STATUS As Integer
            If (rdbtnStatus_AATTAG.SelectedItem.Value = True) Then
                AAT_TAG_STATUS = 0
            Else
                AAT_TAG_STATUS = 1
            End If



            Try
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_AMG1_TAGUPDT")
                sp1.Command.AddParameter("@AST_SNO", Request.QueryString("id"), DbType.Int32)
                sp1.Command.AddParameter("@AAT_AST_CODE", ddlcode.SelectedItem.Value, DbType.String)
                sp1.Command.AddParameter("@AAT_EMP_ID", AAT_EMP_ID, DbType.String)

                sp1.Command.AddParameter("@AAT_COONR_ID", AAT_EMP_ID, DbType.String)
                sp1.Command.AddParameter("@AAT_TAG_STATUS", AAT_TAG_STATUS, DbType.Int32)
                sp1.ExecuteScalar()
                'lblMsg.Visible = True
                lblMsg.Text = "Data Modified Succesfully"
                ClearData()

            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If

        

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_EMP_BRAND")
            sp2.Command.AddParameter("@dummy", "", DbType.String)
            Dim ds2 As New DataSet
            ds2 = sp2.GetDataSet()
            ddlEmpID_AATTAG.DataSource = ds2
            ddlEmpID_AATTAG.Items.Insert(0, "--Select--")
            ddlEmpID_AATTAG.DataTextField = "AUR_FIRST_NAME"
            ddlEmpID_AATTAG.DataValueField = "AUR_ID"
            ddlEmpID_AATTAG.DataBind()
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AAT_TAG_STATUS")
            sp3.Command.AddParameter("@dummy", 1, DbType.Int32)   
            ddlcode.DataSource = sp3.GetDataSet()

            ddlcode.DataTextField = "AAT_AST_CODE"
            ddlcode.DataBind()
            ddlcode.Items.Insert(0, "--Select--")

            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_AMG_TAGMODIFY")
            sp4.Command.AddParameter("@AST_SNO", Request.QueryString("id"), DbType.Int32)
            Dim DS4 As New DataSet
            DS4 = sp4.GetDataSet
            If DS4.Tables(0).Rows.Count > 0 Then

                ddlEmpID_AATTAG.SelectedItem.Value = DS4.Tables(0).Rows(0).Item("AAT_EMP_ID")
                Dim rdbtnstaid As Integer = DS4.Tables(0).Rows(0).Item("AAT_TAG_STATUS")
                If rdbtnstaid = 0 Then
                    rdbtnStatus_AATTAG.SelectedItem.Value = 0
                Else
                    rdbtnStatus_AATTAG.SelectedItem.Value = 1
                End If
            End If

        End If







        
    End Sub
    Public Sub ClearData()
        ddlEmpID_AATTAG.SelectedItem.Value = 0

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmAMGTaggedGetDetails.aspx")
    End Sub
End Class
