<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AptViewRequisition.ascx.vb"
    Inherits="Controls_AptViewRequisition" %>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Associate Name<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateName" TabIndex="1" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">
                    Department<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlDep" runat="server" TabIndex="2" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                    <%-- <asp:TextBox ID="txtDept" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>--%>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate ID<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateID" TabIndex="3" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Reporting Manager<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlRM" TabIndex="4" runat="server" CssClass="selectpicker" data-live-search="true">
                    </asp:DropDownList>
                    <%--<asp:TextBox ID="txtRM" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>--%>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Designation<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDesig" TabIndex="5" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Journey Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvjdate" runat="server" ControlToValidate="txtJrDate"
                    Display="None" ErrorMessage="Please Pick Journey Date " ValidationGroup="Val2"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Click On the TextBox  to Pick the date')" onmouseout="UnTip()">
                        <div class='input-group date' id='fromdate'>
                            <asp:TextBox ID="txtJrDate" TabIndex="6" runat="server" CssClass="form-control"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">From (place)<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvfrom" runat="server" ControlToValidate="txtFrom"
                    Display="None" ErrorMessage="Please Enter From Place " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtFrom" TabIndex="7" runat="server" CssClass="form-control" MaxLength="500"
                        Rows="3" TextMode="multiline"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">To (place)<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvto" runat="server" ControlToValidate="txtTp" Display="None"
                    ErrorMessage="Please Enter To Place" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtTp" TabIndex="8" runat="server" CssClass="form-control" MaxLength="500"
                        Rows="3" TextMode="multiline"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Mode of Travel<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvmode" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select Mode of travel"
                    Display="None" ControlToValidate="ddlMode" InitialValue="0"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlMode" TabIndex="9" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                        <asp:ListItem Value="FLT">Flight</asp:ListItem>
                        <asp:ListItem Value="TRN">Train</asp:ListItem>
                        <asp:ListItem Value="BS">Bus</asp:ListItem>
                        <asp:ListItem Value="CR">Car</asp:ListItem>
                        <asp:ListItem Value="ATO">Auto</asp:ListItem>
                        <asp:ListItem Value="BKE">Bike</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Amount<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount"
                    Display="None" ErrorMessage="Please Enter Amount" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revAAmount" runat="Server" ControlToValidate="txtAmount"
                    ValidationExpression="[-+]?[0-9]*\.?[0-9]+" ValidationGroup="Val1" Display="None"
                    ErrorMessage="Please enter valid Amount"></asp:RegularExpressionValidator>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAmount" TabIndex="10" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Purpose of travel<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvpurpose" runat="server" ControlToValidate="txtPurpose"
                    Display="None" ErrorMessage="Please Enter Purpose Of Travel " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter Purpose with maximum Limit of 1000 characters')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtPurpose" TabIndex="11" runat="server" CssClass="form-control" TextMode="MultiLine"
                            Rows="3" MaxLength="1000"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                    Display="None" ErrorMessage="Please Enter Remarks " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <div onmouseover="Tip('Enter Remarks with maximum Limit of 1000 characters')" onmouseout="UnTip()">
                        <asp:TextBox ID="txtRemarks" TabIndex="12" runat="server" CssClass="form-control" TextMode="MultiLine"
                            Rows="3" MaxLength="1000"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="tr1" runat="server">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Approximate (Kilometers)<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvKm" runat="server" ControlToValidate="txtKM" Display="None"
                        ErrorMessage="Please Enter Approximate Kilometers " ValidationGroup="Val2"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revkm" runat="Server" ControlToValidate="txtKM"
                        ValidationExpression="[-+]?[0-9]*\.?[0-9]+" ValidationGroup="Val1" Display="None"
                        ErrorMessage="Please enter valid Approximate Kilometers"></asp:RegularExpressionValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtKM" TabIndex="13" runat="server" CssClass="form-control" MaxLength="8"
                            AutoPostBack="True"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">

            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                CausesValidation="true" />
            <asp:Button ID="btnModify" runat="server" Text="Modify" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                CausesValidation="true" />
            <asp:TextBox ID="txtStore" runat="server" CssClass="form-control" Visible="FALSE"></asp:TextBox>
            <asp:TextBox ID="txtstore1" runat="server" CssClass="form-control" Visible="FALSE"></asp:TextBox>

        </div>
    </div>
</div>
<div id="table4" runat="Server">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblTamount" runat="server" Text="Total Amount"></asp:Label>
                    <asp:Label ID="lblTotalAmount" runat="server"> </asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <asp:GridView ID="gvItems" runat="server" EmptyDataText="No Conveyance Request Found."
                    RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                    AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="FALSE">
                            <ItemTemplate>
                                <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Journey Date">
                            <ItemTemplate>
                                <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("ESP_JRNY_DATE")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="From">
                            <ItemTemplate>
                                <asp:Label ID="lblFrom" runat="server" Text='<%#Eval("ESP_FROM")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="To">
                            <ItemTemplate>
                                <asp:Label ID="lblTo" runat="server" Text='<%#Eval("ESP_TO")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mode of Travel">
                            <ItemTemplate>
                                <asp:Label ID="lblMode" runat="server" Text='<%#Eval("ESP_MODE_TRVL")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Purpose of Travel">
                            <ItemTemplate>
                                <asp:Label ID="lblPurpose" runat="server" Text='<%#Eval("ESP_PURPOSE_TRVL")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Appox. Kms">
                            <ItemTemplate>
                                <asp:Label ID="lblkms" runat="server" Text='<%#Eval("ESP_APPRX_KM")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("AMOUNT", "{0:c2}")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblRem" runat="server" Text='<%#Eval("REMARKS")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField Text="Edit" CommandName="EDIT" />
                        <asp:ButtonField Text="Delete" CommandName="DELETE" />
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">

                <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" />

            </div>
        </div>
    </div>
</div>
<div id="t1" runat="server" visible="false">
    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="gvdetails" runat="server" EmptyDataText="No Conveyance Request Found."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Journey Date">
                        <ItemTemplate>
                            <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("ESP_JRNY_DATE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From">
                        <ItemTemplate>
                            <asp:Label ID="lblFrom" runat="server" Text='<%#Eval("ESP_FROM")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To">
                        <ItemTemplate>
                            <asp:Label ID="lblTo" runat="server" Text='<%#Eval("ESP_TO")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblMode" runat="server" Text='<%#Eval("ESP_MODE_TRVL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purpose of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblPurpose" runat="server" Text='<%#Eval("ESP_PURPOSE_TRVL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Appox. Kms">
                        <ItemTemplate>
                            <asp:Label ID="lblkms" runat="server" Text='<%#Eval("ESP_APPRX_KM")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("AMOUNT", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblRem" runat="server" Text='<%#Eval("REMARKS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>

</div>
