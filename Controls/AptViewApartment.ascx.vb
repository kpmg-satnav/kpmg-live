Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AptViewApartment
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            gvItems.Visible = False
            lblTamount.Visible = False
            lblTotalAmount.Text = ""
            btnAdd.Visible = False
            btnModify.Visible = False
            tr2.Visible = False
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONV_GETDETAILS")
            sp.Command.AddParameter("@ESP_AUR_ID", Session("UID"), DbType.String)
            gvitems1.DataSource = sp.GetDataSet()
            gvitems1.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems1.PageIndexChanging
        gvitems1.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems1.RowCommand
        If e.CommandName = "View" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvitems1.Rows(rowIndex).FindControl("lblID"), Label)
            Dim id As String = lblID.Text
            txtStore.Text = id
            BindDetails(txtStore.Text)

            If gvItems.Rows.Count > 0 Then
                btnAdd.Visible = True
                btnModify.Visible = True
                tr2.Visible = True


            Else
                btnAdd.Visible = False
                btnModify.Visible = False
                lblTamount.Visible = False
                lblTotalAmount.Text = ""
                tr2.Visible = False
            End If
            total_amount(txtStore.Text)
            gvItems.Visible = True
        End If
    End Sub
    Private Sub BindDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_BIND")
            sp.Command.AddParameter("@ESP_REQ_ID", id, DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
            
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub total_amount(ByVal id As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_TOTAL_AMOUNT")
        sp.Command.AddParameter("@ESP_REQ_ID", id, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            lblTotalAmount.Visible = True
            lblTamount.Visible = True
            lblTotalAmount.Text = "Rs." & "" & ds.Tables(0).Rows(0).Item("Amount")
        End If
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindDetails(txtStore.Text)
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try

            Dim flag As Boolean
            flag = False
            For i As Integer = 0 To gvItems.Rows.Count - 1

                Dim lblID1 As Label = DirectCast(gvItems.Rows(i).FindControl("lblID1"), Label)
                Dim chbx As CheckBox = DirectCast(gvItems.Rows(i).FindControl("chkbx"), CheckBox)
                If chbx.Checked = True Then
                    flag = True
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_aPPROVE")
                    sp.Command.AddParameter("@ESP_REQ_ID", txtStore.Text, DbType.String)
                    sp.Command.AddParameter("@SNO", lblID1.Text, DbType.Int32)
                    sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
                    sp.ExecuteScalar()
                End If
            Next
            If flag = False Then
                lblMsg.Text = "Please select any of the checkbox to Approve or Reject"
            Else
                Mail_RmApproved(txtStore.Text)
                BindDetails(txtStore.Text)
                BindGrid()

                total_amount(txtStore.Text)
                If gvItems.Rows.Count > 0 Then
                    btnAdd.Visible = True
                    btnModify.Visible = True
                    lblTamount.Visible = True
                    lblTotalAmount.Visible = True
                    tr2.Visible = True
                Else
                    btnAdd.Visible = False
                    btnModify.Visible = False
                    lblTamount.Text = ""
                    lblTotalAmount.Visible = False
                    tr2.Visible = False
                End If
                lblMsg.Text = "Local Conveyance Requisition Approved Successfully by RM"
            End If


        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Mail_RmApproved(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
      
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email

        Dim strHrEmail As String = ""                             'To hold the HR Email
        Dim StrFinEmpName As String = ""
        Dim strFinEmpEmail As String = ""
       
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_APPROVED_REQUISITIONS_MAIL")
        sp1.Command.AddParameter("@ESP_REQ_ID", REQID, DbType.String)
        gvdetails.DataSource = sp1.GetDataSet()
        gvdetails.DataBind()



        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONEVYANCE_BIND_DETAILS_MAIL")
        sp.Command.AddParameter("@ESP_REQ_ID", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        Dim strfrommail, strfromname As String

    
        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "Employee Service Desk"

        strSubj = " Local Conveyance Request - by " & strEmpName & "."


        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
         "This is to inform your that these are the Details of Your Approved Local Conveyance Requisitions" & _
         "<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
         "<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
         "<td align=left width=12.5%><U>Journey Date: </U></td>" & _
          "<td  align=left width=12.5%><U>From Place: </U></td>" & _
         "<td  align=left width=12.5%><U>To Place: </U></td>" & _
         "<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
         "<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
           "<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
         "<td  align=left width=12.5%><U>Amount </U></td>" & _
         "<td  align=left width=12.5%><U>Remarks </U></td>" & _
         "<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvdetails.Rows.Count - 1
            strMsg = strMsg + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblFrom"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblTo"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblPurpose"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblkms"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblrem"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblRMrem"), Label).Text & "</td></tr></table>"
        Next
        strMsg = strMsg + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"

        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_LOCAL_CONVEYANCE_ESP")
        sp2.Command.AddParameter("@ESP_REQ_ID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")

        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_LOCAL_CONVEYANCE_ESP_FINANCE")

        Dim DS3 As New DataSet()
        DS3 = sp3.GetDataSet()
        If DS3.Tables(0).Rows.Count > 0 Then
            StrFinEmpName = DS3.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
            strFinEmpEmail = DS3.Tables(0).Rows(0).Item("AUR_EMAIL")
            'strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        End If

        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
"This is to inform your that these are the Details of  " & strEmpName & "'s  Approved Local Conveyance Requisitions" & _
"<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
       "<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
       "<td align=left width=12.5%><U>Journey Date: </U></td>" & _
        "<td  align=left width=12.5%><U>From Place: </U></td>" & _
       "<td  align=left width=12.5%><U>To Place: </U></td>" & _
       "<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
       "<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
        "<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
       "<td  align=left width=12.5%><U>Amount </U></td>" & _
       "<td  align=left width=12.5%><U>Remarks </U></td>" & _
         "<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvdetails.Rows.Count - 1
            strMsgAdm = strMsgAdm + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblFrom"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblTo"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblPurpose"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblkms"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblrem"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblRMrem"), Label).Text & "</td></tr></table>"
        Next
        strMsgAdm = strMsgAdm + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strEmpName & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner. 

        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, REQID, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, REQID, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
        If strFinEmpEmail <> "" Then
            Send_Mail(strFinEmpEmail, strfrommail, REQID, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If

    End Sub
    Private Sub Mail_RmRejected(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strHrEmail As String = ""                             'To hold the HR Email


        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_REJECTED_REQUISITIONS_MAIL")
        sp1.Command.AddParameter("@ESP_REQ_ID", REQID, DbType.String)
        gvdetails.DataSource = sp1.GetDataSet()
        gvdetails.DataBind()

        strSubj = " Local Conveyance Request Rejected."

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONEVYANCE_BIND_DETAILS_MAIL")
        sp.Command.AddParameter("@ESP_REQ_ID", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")


        Dim strfrommail, strfromname As String
        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "Employee Service Desk"

        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
          "This is to inform your that these are the Details of Your Rejected Local Conveyance Requisitions" & _
          "<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
          "<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
          "<td align=left width=12.5%><U>Journey Date: </U></td>" & _
           "<td  align=left width=12.5%><U>From Place: </U></td>" & _
          "<td  align=left width=12.5%><U>To Place: </U></td>" & _
          "<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
          "<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
          "<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
            "<td  align=left width=12.5%><U>Amount </U></td>" & _
          "<td  align=left width=12.5%><U>Remarks </U></td>" & _
          "<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"

        For i As Integer = 0 To gvdetails.Rows.Count - 1
            strMsg = strMsg + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblFrom"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblTo"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblPurpose"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblkms"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblrem"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblRMrem"), Label).Text & "</td></tr></table>"
        Next
        strMsg = strMsg + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_LOCAL_CONVEYANCE_ESP")
        sp2.Command.AddParameter("@ESP_REQ_ID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        'strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")

        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
 "This is to inform your that these are the Details of  " & strEmpName & "'s  Rejected Local Conveyance Requisitions" & _
 "<table width=100% cellpadding=4 cellspacing=0><tr><td><hr width=100%></td></tr>" & _
        "<tr><td  align=left width=12.5%><U>Requestor Name: </U></td>" & _
        "<td align=left width=12.5%><U>Journey Date: </U></td>" & _
         "<td  align=left width=12.5%><U>From Place: </U></td>" & _
        "<td  align=left width=12.5%><U>To Place: </U></td>" & _
        "<td  align=left width=12.5%><U>Mode of Travel: </U></td>" & _
        "<td  align=left width=12.5%><U>Purpose of Travel: </U></td>" & _
          "<td  align=left width=12.5%><U>Approx Kms: </U></td>" & _
        "<td  align=left width=12.5%><U>Amount </U></td>" & _
         "<td  align=left width=12.5%><U>Remarks </U></td>" & _
        "<td  align=left width=12.5%><U>RM Remarks </U></td></tr></table>"
        For i As Integer = 0 To gvdetails.Rows.Count - 1
            strMsgAdm = strMsgAdm + "<table width=100% cellpadding=4 cellspacing=0 border=1><tr><td><hr width=100%></td></tr>" & _
"<tr><td  align=left width=12.5%>" & strEmpName & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lbljrdate"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblFrom"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblTo"), Label).Text & "</td>" & _
"<td align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblmode"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblPurpose"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblkms"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblAmount"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblrem"), Label).Text & "</td>" & _
"<td  align=left width=12.5%>" & CType(gvdetails.Rows(i).FindControl("lblRMrem"), Label).Text & "</td></tr></table>"
        Next
        strMsgAdm = strMsgAdm + "Thanking You, " & "<br><br>" & _
    "Regards, " & "<br>" & strEmpName & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner. 

        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, REQID, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, REQID, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal MailFrom As String, ByVal Id As String, ByVal subject As String, ByVal msg As String, ByVal strHrEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_MAIL_ADD")
        sp.Command.AddParameter("@AMT_ID", Id, DbType.String)
        sp.Command.AddParameter("@AMT_MESSAGE", msg, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_TO", MailTo, DbType.String)
        sp.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
        sp.Command.AddParameter("@AMT_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@AMT_FROM", MailFrom, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_CC", strHrEmail, DbType.String)
        sp.Command.AddParameter("@AMT_BDYFRMT", 0, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click
        Try
            Dim flag As Boolean
            flag = False
            For i As Integer = 0 To gvItems.Rows.Count - 1

                Dim lblID1 As Label = DirectCast(gvItems.Rows(i).FindControl("lblID1"), Label)
                Dim chbx As CheckBox = DirectCast(gvItems.Rows(i).FindControl("chkbx"), CheckBox)
                If chbx.Checked = True Then
                    flag = True
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_LOCAL_CONVEYANCE_MODIFY")
                    sp.Command.AddParameter("@ESP_REQ_ID", txtStore.Text, DbType.String)
                    sp.Command.AddParameter("@SNO", lblID1.Text, DbType.Int32)
                    sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
                    sp.ExecuteScalar()
                End If
            Next
            If flag = False Then
                lblMsg.Text = "Please select any of the checkbox to Approve or Reject"
            Else
                Mail_RmRejected(txtStore.Text)
                BindDetails(txtStore.Text)
                BindGrid()
                total_amount(txtStore.Text)
                If gvItems.Rows.Count > 0 Then
                    btnAdd.Visible = True
                    btnModify.Visible = True
                    lblTamount.Visible = True
                    lblTotalAmount.Visible = True
                    tr2.Visible = True
                Else
                    btnAdd.Visible = False
                    btnModify.Visible = False
                    lblTamount.Text = ""
                    lblTotalAmount.Visible = False
                    tr2.Visible = False
                End If
                lblMsg.Text = "Local Conveyance Requisition Rejected  by RM"
            End If


        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
