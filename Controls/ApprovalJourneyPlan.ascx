﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApprovalJourneyPlan.ascx.vb" Inherits="Controls_ApprovalJourneyPlan" %>


<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>






<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvitems1" runat="server" EmptyDataText="No Approval Journey Plan Found."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Request Id">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="Server" Text='<%#Eval("REQUESTID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Requested Date">
                        <ItemTemplate>
                            <asp:Label ID="lblreqdate" runat="server" Text='<%#Eval("REQUESTED_ON")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Request Name">
                        <ItemTemplate>
                            <asp:Label ID="lblreqname" runat="server" Text='<%#Eval("REQUESTNAME")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("STA_TITLE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField Text="View Details" CommandName="View" />
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            </asp:GridView>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblsno" runat="server" Visible="false"></asp:Label>
            </div>
        </div>
    </div>
</div>





<div class="row">
    <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvItems" runat="server" EmptyDataText="Sorry! No Available Records..."
                RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Start Date">
                        <ItemTemplate>
                            <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("STARTDATE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <ItemTemplate>
                            <asp:Label ID="lblEnddate" runat="server" Text='<%#Eval("ENDDATE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From">
                        <ItemTemplate>
                            <asp:Label ID="lblFrom" runat="server" Text='<%#Eval("ORIGIN")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To">
                        <ItemTemplate>
                            <asp:Label ID="lblTo" runat="server" Text='<%#Eval("DESTINATION")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblMode" runat="server" Text='<%#Eval("MODEOFTRAVEL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Travel Assistance">
                        <ItemTemplate>
                            <asp:Label ID="lblTRAVELASSISTANCE" runat="server" Text='<%#Eval("TRAVELASSISTANCE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Time">
                        <ItemTemplate>
                            <asp:Label ID="lblTIME" runat="server" Text='<%#Eval("TIME")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField CommandName="View" Text="View" HeaderText="Comments" />
                    <asp:ButtonField CommandName="Add" Text="Add" HeaderText="Comments" />
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>

<asp:Panel ID="pnlview" runat="server"  Visible="false">  

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <asp:GridView ID="gvviewcomments" runat="server" EmptyDataText="Sorry! No Available Records..."
                    RowStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                    AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Comments">
                            <ItemTemplate>
                                <asp:Label ID="lblcommnets" runat="server" Text='<%#Eval("COMMENT")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Commented By">
                            <ItemTemplate>
                                <asp:Label ID="lblcommentedby" runat="server" Text='<%#Eval("AUR_KNOWN_AS")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Commented on">
                            <ItemTemplate>
                                <asp:Label ID="lblcomenteddate" runat="server" Text='<%#Eval("COMMENTED_ON")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>

</asp:Panel>


<asp:Panel ID="pnladdcomments" runat="server" Visible="false">

    <h4>Add Comments</h4>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Comment<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtcomment"
                        Display="None" ErrorMessage="Please Enter Comments" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtcomment" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="btnaddcommnet" runat="server" Text="Add Comment" CssClass="btn btn-primary custom-button-color"
                    ValidationGroup="Val1" />
            </div>
        </div>
    </div>


</asp:Panel>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnAdd" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                CausesValidation="false" Visible="false" />
            <asp:Button ID="btnModify" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                CausesValidation="false" Visible="false" />

            <asp:Button ID="btnsubmit" runat="server" Text="View Details" CssClass="btn btn-primary custom-button-color"
                CausesValidation="false" Visible="false" />

            <asp:TextBox ID="txtStore" runat="server" CssClass="form-control" Visible="FALSE"></asp:TextBox>
        </div>
    </div>
</div>



