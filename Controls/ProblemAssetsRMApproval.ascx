<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProblemAssetsRMApproval.ascx.vb"
    Inherits="Controls_ProblemAssetsRMApproval" %>

<script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>

<script language="javascript" type="text/javascript" src="../../Scripts/Cal.js"></script>

<div>
    <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                    ForeColor="Black">RM Approval for Asset Requisitions
             <hr align="center" width="60%" /></asp:Label></td>
        </tr>
    </table>
    <asp:Panel ID="PNLCONTAINER" runat="server" Width="85%" Height="100%">
        <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="left" width="100%" colspan="3">
                    <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    <strong>&nbsp;RM Approval for Assets Requisitions</strong>
                </td>
                <td>
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" ValidationGroup="Val1" />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <asp:TextBox ID="txtstore" runat="server" Visible="false" ></asp:TextBox>
                    <asp:Panel ID="pnlRequest" runat="server" Width="100%" GroupingText="Pending Requests">
                        <table id="tabrequests" runat="server" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" style="height: 26px">
                                    <asp:GridView ID="gvRequests" runat="server" Width="100%" AllowPaging="true" AllowSorting="true"
                                        RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="center" SelectedRowStyle-VerticalAlign="Top"  
                                        PageSize="10" AutoGenerateColumns="false" EmptyDataText="Sorry No Requests Raised Till Now">
                                        <Columns>
                                            <asp:TemplateField Visible="FALSE">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblreqid" runat="Server" Text='<%#Eval("REQ_ID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Requested By">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRequestedby" runat="Server" Text='<%#Eval("AUR_ID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:ButtonField Text="View Details" CommandName="View" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnlAssets" runat="Server" Width="100%" GroupingText="Asset Details">
                        <table id="tabassets" runat="Server" cellpadding="1" cellspacing="0" width="100%"
                            border="1">
                            <tr>
                                <td colspan="2">
                                    <asp:GridView ID="gvAssetDetails" runat="server" EmptyDataText="Sorry! No Available Records..."
                                        RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="center" Width="100%"
                                        AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField Visible="FALSE">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblastcode" runat="Server" Text='<%#Eval("AAT_AST_CODE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkbxasset" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Asset">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblasset" runat="Server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                          
                                              <asp:TemplateField HeaderText="Requested On">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRequestedOn" runat="server" Text='<%#Eval("REQUESTED_DATE")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr id="tr1" runat="server">
                            <td align="left" style="height:26px;width:50%">
                            Employee Remarks
                            </td>
                            <td align="left" style="height:26px;width:50%">
                            <asp:TextBox ID="txtremarks" runat="Server" CssClass="clsTextField" Width="97%" MaxLength="4000"
                                        Rows="2" TextMode="MultiLine"></asp:TextBox>
                            </td>
                            </tr>
                            <tr id="tr2" runat="server" >
                            
                                <td align="left" style="height: 26px; width: 50%">
                                   RM Remarks <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvrmrks" runat="server" ControlToValidate="txtrmrks"
                                        ErrorMessage="Please Enter Remarks" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:TextBox ID="txtrmrks" runat="server" CssClass="clsTextField" Width="97%" MaxLength="4000"
                                        Rows="2" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnsubmit" runat="Server" CssClass="button" Text="Approve" CausesValidation="true"
                                        ValidationGroup="Val1" />
                                         <asp:Button ID="btnReject" runat="Server" CssClass="button" Text="Reject" CausesValidation="true"
                                        ValidationGroup="Val1" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                    <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </asp:Panel>
