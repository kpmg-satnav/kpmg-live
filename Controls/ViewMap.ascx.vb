Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class Controls_ViewMap
    Inherits System.Web.UI.UserControl
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim FLR_CODE As String = Session("FLR_CODE")
        Dim TWR_CODE As String = Session("Twr_Code")
        Dim BDG_CODE As String = Session("BDG_ID")

        Dim selfloor, selfbuild As String
        Dim filepath As String = String.Empty
        Dim selTwr As String = String.Empty
        'Response.Write("<LINK href=""../../Scripts/styAMTamantra.css"" rel=""stylesheet"">")


        selfloor = Session("FLR_CODE")
        selfbuild = Session("BDG_ID")
        selTwr = Session("Twr_Code")

        strSQL = "select FLR_USR_MAP from  " & Session("TENANT") & "."  & "FLOOR where FLR_code='" & selfloor & "' and flr_twr_id='" & selTwr & "' and flr_bdg_id = '" & selfbuild & "'"
        ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

        If ObjDR.Read() Then
            filepath = ObjDR("FLR_USR_MAP").ToString()
        End If
        ObjDR.Close()
        '

        If Trim(filepath) = "" Or Trim(filepath) = "NA" Or IsDBNull(filepath) = True Then
            lbl1.Text = "<br><br><br><bR><bR><br><Center><h1>Under Construction</h1></center>"
            '  Response.End()
        End If




        lbl1.Text = "<script language=javascript src='Scripts/Map_User.js'></script>"
        lbl1.Text = lbl1.Text & "<SCRIPT language=VBScript src='Scripts/vb_geometryroutines.bas'></SCRIPT>"
        lbl1.Text = lbl1.Text & "<SCRIPT language=JavaScript src='Scripts/mapcontrols.js'></SCRIPT>"

        lbl1.Text = lbl1.Text & "<SCRIPT LANGUAGE='VBScript'>"
        lbl1.Text = lbl1.Text & "Sub map_onViewChanged(map)"
        lbl1.Text = lbl1.Text & "onViewChanged(map)"
        lbl1.Text = lbl1.Text & "End Sub"
        lbl1.Text = lbl1.Text & "</SCRIPT>"

        lbl1.Text = lbl1.Text & "<SCRIPT LANGUAGE='VBScript'>"
        lbl1.Text = lbl1.Text & "Sub map_onDoubleClickObject(mapObj)"
        lbl1.Text = lbl1.Text & "mapObj.DoubleClickHandled = onDoubleClickObject(mapObj)"
        lbl1.Text = lbl1.Text & "End Sub"
        lbl1.Text = lbl1.Text & "</SCRIPT>"

        lbl1.Text = lbl1.Text & "<SCRIPT LANGUAGE='VBScript'>"
        lbl1.Text = lbl1.Text & "Sub map_onSelectionChanged(map)"
        lbl1.Text = lbl1.Text & "onSelectionChanged(map)"
        lbl1.Text = lbl1.Text & "End Sub"
        lbl1.Text = lbl1.Text & "</SCRIPT>"

        ' lbl1.Text = lbl1.Text & "<form name=f1>"

        lbl1.Text = lbl1.Text & "<OBJECT id=map height=70% width=60% classid=CLSID:62789780-B744-11D0-986B-00609731A21D VIEWASTEXT codebase=""~/SpatialData/mgaxctrl.cab#Version=6,5,5,7"">"

        lbl1.Text = lbl1.Text & "<PARAM NAME=URL VALUE=" & filepath & ">"
        lbl1.Text = lbl1.Text & "<PARAM NAME=Lat VALUE=0>"
        lbl1.Text = lbl1.Text & "<PARAM NAME=Lon VALUE=0>"
        lbl1.Text = lbl1.Text & "<PARAM NAME=MapScale VALUE=0>"
        lbl1.Text = lbl1.Text & "<PARAM NAME=MapWidth VALUE=0>"
        lbl1.Text = lbl1.Text & "<PARAM NAME=MapUnits VALUE=M>"
        lbl1.Text = lbl1.Text & "<PARAM NAME=ToolBar VALUE=Off>"
        lbl1.Text = lbl1.Text & "<PARAM NAME=StatusBar VALUE=Off>"
        lbl1.Text = lbl1.Text & "<PARAM NAME=LayersViewWidth VALUE=0>"
        lbl1.Text = lbl1.Text & "<PARAM NAME=BSCRC VALUE=100>"
        lbl1.Text = lbl1.Text & "<PARAM NAME=DefaultTarget VALUE="">"
        lbl1.Text = lbl1.Text & "<PARAM NAME=ErrorTarget VALUE="">"
        lbl1.Text = lbl1.Text & "<PARAM NAME=ObjectLinkTarget VALUE="">"
        lbl1.Text = lbl1.Text & "<PARAM NAME=ReportTarget VALUE="">"
        lbl1.Text = lbl1.Text & "<PARAM NAME=URLList VALUE=On>"
        lbl1.Text = lbl1.Text & "<PARAM NAME=URLListTarget VALUE="">"
        lbl1.Text = lbl1.Text & "<PARAM NAME=AutoLinkLayers VALUE="">"
        lbl1.Text = lbl1.Text & "<PARAM NAME=AutoLinkTarget VALUE="">"
        lbl1.Text = lbl1.Text & "<PARAM NAME=AutoLinkDelay VALUE=20>"
        lbl1.Text = lbl1.Text & "</OBJECT>"

        'lbl1.Text = lbl1.Text & "</form>"
    End Sub
End Class
