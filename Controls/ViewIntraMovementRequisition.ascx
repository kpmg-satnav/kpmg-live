<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewIntraMovementRequisition.ascx.vb" Inherits="Controls_ViewIntraMovementRequisition" %>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" align="center">
            <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                ForeColor="Black">Intra Movement Requisition
             <hr align="center" width="60%" /></asp:Label>
            &nbsp;
            <br />
        </td>
    </tr>
</table>
<table width="85%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
    border="0">
    <tr>
        <td>
            <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
        <td width="100%" class="tableHEADER" align="left">
            &nbsp;<strong>Intra Movement Requisition</strong></td>
        <td>
            <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
    </tr>
    <tr>
        <td background="../../Images/table_left_mid_bg.gif">
            &nbsp;</td>
        <td align="left">
            <table id="Table1" width="100%" runat="server" cellpadding="0" cellspacing="0">
                <tr>
                      <td align="right" colspan="2">
                        <a href="IntraMovementRequisition.aspx">Raise Intra Movement Requisition </a>
                    </td>
                </tr>
                <tr>
                      <td align="right" colspan="2">
                     &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:GridView ID="gvgriditems" runat="server" AllowPaging="true" AllowSorting="true"
                            AutoGenerateColumns="false" Width="100%" EmptyDataText="No Records Found">
                            <Columns>
                                 
                                <asp:TemplateField HeaderText="Requisition">
                                    <ItemTemplate>
                                        <asp:Label ID="lblasset" runat="server" Text='<%#Eval("MMR_REQ_ID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Raised by">
                                    <ItemTemplate>
                                        <asp:Label ID="lblreqraised" runat="server" Text='<%#Eval("MMR_RAISEDBY") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Requested Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lbldate" runat="server" Text='<%#Eval("MMR_MVMT_DATE") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="false">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkViewDetails" Text="View Details" CommandArgument='<%#Eval("MMR_REQ_ID") %>'
                                            CommandName="ViewDetails" runat="server"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </td>
        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width: 10px; height: 17px;">
            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
        <td style="height: 17px; width: 17px;">
            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
    </tr>
</table>
