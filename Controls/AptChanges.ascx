<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AptChanges.ascx.vb" Inherits="Controls_AptChanges" %>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row text-center">
                <strong>LOCAL CONVEYANCE REQUESTS</strong>
            </div>
        </div>
    </div>
</div>
<div id="panellocal" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <asp:GridView ID="gvLocalRequests" runat="server" EmptyDataText="No Local Conveyance Request Found."
                    RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                    AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false"
                    SelectedRowStyle-VerticalAlign="Top">
                    <Columns>
                        <asp:TemplateField HeaderText="Requisition Id" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbllocalID" runat="Server" Text='<%#Eval("ESP_REQ_ID")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Requested By">
                            <ItemTemplate>
                                <asp:Label ID="lbllocalemployee" runat="server" Text='<%#Eval("AUR_NAME")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField Text="View Details" CommandName="View" />
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                     <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="row text-center">
                <strong>DOMESTIC CONVEYANCE REQUESTS</strong>
            </div>
        </div>
    </div>
</div>

<div id="panDomestic" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <asp:GridView ID="gvDomesticRequests" runat="server" EmptyDataText="No Domestic Conveyance Found."
                    RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                    AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false"
                    SelectedRowStyle-VerticalAlign="Top">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" Visible="FALSE">
                            <ItemTemplate>
                                <asp:Label ID="lblDomesticID" runat="Server" Text='<%#Eval("CNV_REQ_ID")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Requested By">
                            <ItemTemplate>
                                <asp:Label ID="lbldomesticEmployee" runat="server" Text='<%#Eval("CNV_AUR_ID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField Text="View Details" CommandName="View" />
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                     <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblTamount" runat="server" Text="Total Amount"></asp:Label>
                <asp:Label ID="lblTotalAmount" runat="server"> </asp:Label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvlocaldetails" runat="server" EmptyDataText="Sorry! No Available Records..."
                RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lbllocalID1" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkbxlocal" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Journey Date">
                        <ItemTemplate>
                            <asp:Label ID="lbllocaljrdate" runat="server" Text='<%#Eval("ESP_JRNY_DATE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="From">
                        <ItemTemplate>
                            <asp:Label ID="lbllocalFrom" runat="server" Text='<%#Eval("ESP_FROM")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To">
                        <ItemTemplate>
                            <asp:Label ID="lbllocalTo" runat="server" Text='<%#Eval("ESP_TO")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lbllocalMode" runat="server" Text='<%#Eval("ESP_MODE_TRVL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purpose of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lbllocalPurpose" runat="server" Text='<%#Eval("ESP_PURPOSE_TRVL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Appox Kms">
                        <ItemTemplate>
                            <asp:Label ID="lbllocalkms" runat="server" Text='<%#Eval("ESP_APPRX_KM")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:Label ID="lbllocalAmount" runat="server" Text='<%#Eval("AMOUNT", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lbllocalRem" runat="server" Text='<%#Eval("REMARKS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                 <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>

<div id="tr2" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                        Display="None" ErrorMessage="Please Enter Remarks " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine"
                            Rows="3" MaxLength="1000"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">            
                <asp:Button ID="btnApproveLocal" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color"
                    CausesValidation="true" ValidationGroup="Val1" />
                <asp:Button ID="btnRejectLocal" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" CausesValidation="true" />
                <asp:TextBox ID="txtStore" runat="server" CssClass="form-control" Visible="FALSE"></asp:TextBox>            
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvdomesticdetails" runat="server" EmptyDataText="Sorry! No Available Records..."
                RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lbldomesticID1" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkbxdomestic" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Journey Date">
                        <ItemTemplate>
                            <asp:Label ID="lbldomesticjrdate" runat="server" Text='<%#Eval("CNV_JOURNEY_DATE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Expense">
                        <ItemTemplate>
                            <asp:Label ID="lbldomesticMode" runat="server" Text='<%#Eval("CNV_eXPENSE_MODE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lbldomesticTmode" runat="server" Text='<%#Eval("CNV_TRVL_MODE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approx KMs">
                        <ItemTemplate>
                            <asp:Label ID="lbldomesticAppkm" runat="server" Text='<%#Eval("CNV_KM")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purpose of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lbldomesticpurs" runat="server" Text='<%#Eval("CNV_PURPOSE_TRVL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:Label ID="lbldomesticAmount" runat="server" Text='<%#Eval("CNV_TOTAL_AMOUNT", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lbldomesticRemarks" runat="server" Text='<%#Eval("CNV_REMARKS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                 <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div id="tr3" runat="server">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtremarksdomestic" ValidationGroup="Val2"
                        Display="None" ErrorMessage="Please Enter Remarks "></asp:RequiredFieldValidator>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtremarksdomestic" runat="server" CssClass="form-control"
                            TextMode="MultiLine" Rows="3" MaxLength="1000"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">           
                <asp:Button ID="btnApproveDomestic" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color"
                    CausesValidation="true" ValidationGroup="Val2" />
                <asp:Button ID="btnRejectDomestic" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color"
                    CausesValidation="true" />
                <asp:TextBox ID="txtstore1" runat="Server" Width="20%" Visible="false"></asp:TextBox>            
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvlocal1details" runat="server" EmptyDataText="Sorry! No Available Records..."
                RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Journey Date">
                        <ItemTemplate>
                            <asp:Label ID="lbljrdate" runat="server" Text='<%#Eval("ESP_JRNY_DATE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="From">
                        <ItemTemplate>
                            <asp:Label ID="lblFrom" runat="server" Text='<%#Eval("ESP_FROM")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="To">
                        <ItemTemplate>
                            <asp:Label ID="lblTo" runat="server" Text='<%#Eval("ESP_TO")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblMode" runat="server" Text='<%#Eval("ESP_MODE_TRVL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purpose of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblPurpose" runat="server" Text='<%#Eval("ESP_PURPOSE_TRVL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Appox Kms">
                        <ItemTemplate>
                            <asp:Label ID="lblkms" runat="server" Text='<%#Eval("ESP_APPRX_KM")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("AMOUNT", "{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblRem" runat="server" Text='<%#Eval("REMARKS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="RM Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblRMRem" runat="server" Text='<%#Eval("RM_REMARKS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                 <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <asp:GridView ID="gvDomestic1Details" runat="server" EmptyDataText="Sorry! No Available Records..."
                RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" CssClass="table table-condensed table-bordered table-hover table-striped"
                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="ID" Visible="FALSE">
                        <ItemTemplate>
                            <asp:Label ID="lblDomestic1ID1" runat="Server" Text='<%#Eval("SNO")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Journey Date">
                        <ItemTemplate>
                            <asp:Label ID="lblDomestic1jrdate" runat="server" Text='<%#Eval("CNV_JOURNEY_DATE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Expense">
                        <ItemTemplate>
                            <asp:Label ID="lblDomestic1Mode" runat="server" Text='<%#Eval("CNV_EXPENSE_MODE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblDomestic1Tmode" runat="server" Text='<%#Eval("CNV_TRVL_MODE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approx KMs">
                        <ItemTemplate>
                            <asp:Label ID="lblDomestic1Appkm" runat="server" Text='<%#Eval("CNV_KM")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Purpose of Travel">
                        <ItemTemplate>
                            <asp:Label ID="lblDomestic1purs" runat="server" Text='<%#Eval("CNV_PURPOSE_TRVL")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblDomestic1Amount" runat="server" Text='<%#Eval("CNV_TOTAL_AMOUNT","{0:c2}")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblDomestic1Remarks" runat="server" Text='<%#Eval("CNV_rEMARKS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="RM Remarks">
                        <ItemTemplate>
                            <asp:Label ID="lblDomestic1rmRemarks" runat="server" Text='<%#Eval("CNV_RM_rEMARKS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                 <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>




