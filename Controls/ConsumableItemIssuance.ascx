﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ConsumableItemIssuance.ascx.vb" Inherits="Controls_ConsumableItemIssuance" %>
<div>
    <script type="text/javascript">
        function printreq(url, url2) {
            window.open(url2, '_parent');
            window.open(url, '_blank');
        }

    </script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Consumable Item Issuance
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0" id="TABLE1"
        onclick="return TABLE1_onclick()">
        <tr>
            <td align="left" colspan="3">
                <asp:Label align="left" ID="Label9" runat="server" CssClass="note" Text="(*) Mandatory fields"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong> Consumable Item Issuance </strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label2" runat="server" CssClass="clsMessage"></asp:Label><br />
                <br />
                <table id="table4" cellspacing="0" cellpadding="3" width="100%" border="1" style="border-collapse:collapse">
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label1" runat="server" CssClass="clslabel" Text="Requisition ID"></asp:Label>
                            <font class="clsNote">*</font>
                        </td>
                        <td align="left" width="50%">
                            <asp:Label ID="lblReqId" runat="server" CssClass="clslabel"></asp:Label>
                            <asp:Label ID="lblTemp" runat="server" CssClass="clslabel" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label4" runat="server" CssClass="clslabel" Text="Location"></asp:Label>
                            <font class="clsNote">*</font>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblLoc" runat="server" CssClass="clslabel"></asp:Label>
                        </td>
                    </tr>
                    <%--  <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblItemCode" Visible="false" runat="server" CssClass="clslabel"></asp:Label>
                            <asp:Label ID="lblLocName" Visible="false" runat="server" CssClass="clslabel"></asp:Label>
                            <asp:Label ID="lblTwrName" Visible="false" runat="server" CssClass="clslabel"></asp:Label>
                            <asp:Label ID="lblFlrName" Visible="false" runat="server" CssClass="clslabel"></asp:Label>
                  
                        </td>--%>                       <%-- <td align="left">
                            <asp:Label ID="lblTwr" runat="server" CssClass="clslabel"></asp:Label>
                        </td>--%>                 
                   <%--     <tr>
                            <td align="left" width="50%">
                                <asp:Label ID="Label6" runat="server" CssClass="clslabel" Text="Floor"></asp:Label>
                                <font class="clsNote">*</font>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblFlr" runat="server" CssClass="clslabel"></asp:Label>
                            </td>
                        </tr>--%>
                    <tr>
                        <td align="left" colspan="2">
                            <fieldset>
                                <legend>Item List</legend>
                                <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                    EmptyDataText="No Asset(s) Found." Width="100%">
                                   
                                        <Columns>
                                   <asp:BoundField DataField="AST_MD_id" HeaderText="Item Code" ItemStyle-HorizontalAlign="left" Visible="false" />
                                        <asp:BoundField DataField="AST_MD_NAME" HeaderText="Item Name" ItemStyle-HorizontalAlign="left" />
                                        <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AID_QTY") %>'></asp:TextBox>
                                                <asp:Label ID="lblProductid" runat="server" Text='<%#Eval("AST_MD_id") %>' Visible="false"></asp:Label>
                                                 <asp:Label ID="lblMinOrdQty" runat="server" Text='<%#Eval("AST_MD_MINQTY") %>' Visible="false"> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Available Qty" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAvailQty" runat="server" Text='<%#Eval("AID_AVLBL_QTY") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReq_status" runat="server" Visible="false" Text='<%# Eval("AIR_STA_ID") %>'></asp:Label>
                                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label7" runat="server" CssClass="clslabel" Text="Remarks"></asp:Label>
                            <font class="clsNote">*</font>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblRem" runat="server" CssClass="clslabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label8" runat="server" CssClass="clslabel" Text="RM Remarks"></asp:Label>
                            <font class="clsNote">*</font>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblRMRem" runat="server" CssClass="clslabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="lblStoreRem" runat="server" CssClass="bodytext" Text="SM Remarks"></asp:Label>
                            <font class="clsNote">*</font>
                        </td>
                        <td align="left" width="50%">
                            <asp:Label ID="lblSMRem" runat="server" CssClass="clslabel"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            <asp:Label ID="Label3" runat="server" CssClass="bodytext" Text="Remarks"></asp:Label>
                            <font class="clsNote">*</font>
                        </td>
                        <td align="left" width="50%">
                            <asp:TextBox ID="txtISSRem" runat="server" CssClass="textBox" Width="97%" MaxLength="100"
                                Rows="3" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            &nbsp;<asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="button" ValidationGroup="Val1" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" />
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="button" PostBackUrl="~/FAM/FAM_Webfiles/frmViewConsumableIssuance.aspx" />&nbsp;
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                <br />
                <br />
            </td>
            <td background="../../Images/table_right_mid_bg.gif">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td background="../../images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td>
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>