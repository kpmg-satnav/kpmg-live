Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.Text
Imports System.Collections.Generic
Imports System.Web.Services
Partial Class Controls_SpaceAssetMapping
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                lblMsg.Visible = False

                BindCategories()
                getassetsubcategory()
                ddlAstSubCat.SelectedIndex = If(ddlAstSubCat.Items.Count > 1, 1, 0)
                getbrandbycatsubcat()
                ddlAstBrand.SelectedIndex = If(ddlAstBrand.Items.Count > 1, 1, 0)
                getModelbycatsubcat()
                ddlModel.SelectedIndex = If(ddlModel.Items.Count > 1, 1, 0)
                ddlReqId.Visible = False
                panel1.Visible = True
                lbreq.Visible = False
                btnSubmit.Visible = False
                txtremrks.Visible = False
                ddlLocation.Items.Clear()
                BindLocation()
                If Session("Procurement") = 1 Then
                    ddlReqId.Visible = True
                    lbreq.Visible = True
                    bindreqid()
                End If
                'If (CheckBox1.Checked = True And CheckBox2.Checked = True) Or (CheckBox2.Checked = True) Then
                '    gvItems.Columns(6).Visible = True
                'Else
                '    gvItems.Columns(6).Visible = False
                'End If

                If (RbtEmployee.Checked = True) Then
                    gvItems.Columns(9).Visible = True
                Else
                    gvItems.Columns(9).Visible = False
                End If

                BindAssets()
                LoadGridData()
            End If
        End If

    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbtSpace.CheckedChanged, RbtEmployee.CheckedChanged
        lblMsg.Visible = False
        Try
            'If RbtEmployee.Checked = True Then
            ddlLocation.Items.Clear()
            BindLocation()
            BindAssets()
            LoadGridData()
            'End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub BindCategories()
        'GetChildRows("0")
        'ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, New ListItem("--All--", "All"))
        ddlAstCat.SelectedIndex = If(ddlAstCat.Items.Count > 1, 1, 0)
    End Sub

    Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
        ddlAstSubCat.Items.Clear()
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()
        ddlReqId.Items.Clear()
        ddlLocation.Items.Clear()
        gvItems.Visible = False
        getassetsubcategory()
        getbrandbycatsubcat()
        getModelbycatsubcat()
        BindLocation()

    End Sub

    Private Sub getassetsubcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
        ddlAstSubCat.SelectedIndex = 0

    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()
        ddlLocation.Items.Clear()
        ddlReqId.Items.Clear()
        gvItems.Visible = False
        getbrandbycatsubcat()
        getModelbycatsubcat()
        BindLocation()

    End Sub

    Private Sub getbrandbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlAstSubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        ddlModel.Items.Clear()
        ddlLocation.Items.Clear()
        ddlReqId.Items.Clear()
        gvItems.Visible = False
        getModelbycatsubcat()
        BindLocation()

    End Sub

    Private Sub getModelbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()
        'ddlModel.Items.Insert(0, "--ALL--")
        ddlModel.Items.Insert(0, New ListItem("--All--", "All"))

    End Sub

    Protected Sub ddlModel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlModel.SelectedIndexChanged
        If Session("Procurement") = 1 Then
            bindreqid()
        Else
            ddlLocation.Items.Clear()
            BindLocation()
        End If


    End Sub

    Private Sub bindreqid()
        Try


            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GETALLREQID_ON_CATEGORY_FILTERS")
            sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@AST_MODEL", ddlModel.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LOC_CODE", ddlLocation.SelectedItem.Value, DbType.String)
            ddlReqId.DataSource = sp.GetDataSet()
            ddlReqId.DataTextField = "AIR_REQ_TS"
            ddlReqId.DataValueField = "AIR_REQ_TS"
            ddlReqId.DataBind()
            'ddlReqId.Items.Insert(0, New ListItem("--Select--", "0"))
            ' GridView1.Visible = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
    '    Try
    '        lblMsg.Text = ""
    '        If ddlLocation.SelectedIndex > 0 Then
    '            ' BindTower()
    '            panel1.Visible = True
    '            BindAssets()
    '            For Each drow As GridViewRow In gvItems.Rows
    '                Dim ddlAssets As DropDownList = DirectCast(drow.FindControl("ddlassets"), DropDownList)
    '                Dim lbllocation As Label = DirectCast(drow.FindControl("lbllocation"), Label)
    '                Dim ddlTower As DropDownList = DirectCast(drow.FindControl("ddlTower"), DropDownList)
    '                loadtower(lbllocation.Text, ddlTower)
    '                'Space_Loadddl(ddlSpace, lbllocation.Text)
    '            Next

    '        Else
    '            'ddlTower.Items.Clear()
    '            'ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
    '            'ddlTower.SelectedValue = 0
    '            'ddlfloor.Items.Clear()
    '            'ddlfloor.Items.Insert(0, New ListItem("--Select--", "0"))
    '            'ddlfloor.SelectedValue = 0
    '            panel1.Visible = False
    '        End If

    '        'Try
    '        '    If ddlfloor.SelectedIndex > 0 Then
    '        '        BindAssets()
    '        '        'EmpLoad()
    '        '        panel1.Visible = True

    '        '    Else
    '        '        panel1.Visible = False

    '        '    End If


    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    Private Sub loadtower(ByVal location_name As String, ByVal ddlTower As DropDownList)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_TOWERBYLOC")
        ' sp.Command.AddParameter("@LCM_CODE", Session("UID"), DbType.String)
        sp.Command.AddParameter("@LCM_CODE", location_name, DbType.String)
        ddlTower.DataSource = sp.GetDataSet()
        ddlTower.DataTextField = "TWR_NAME"
        ddlTower.DataValueField = "TWR_CODE"
        ddlTower.DataBind()
        'ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))

    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblMsg.Text = ""
        Dim ddl As DropDownList = CType(sender, DropDownList)
        Dim row As GridViewRow = CType(ddl.NamingContainer, GridViewRow)
        Dim index As Integer = row.RowIndex
        Dim lbllocation As Label = DirectCast(gvItems.Rows(index).FindControl("lbllocation"), Label)
        Dim ddlTower As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlTower"), DropDownList)
        Dim ddlFloor As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlFloor"), DropDownList)
        If ddlTower.SelectedItem.Text <> "--Select--" Then
            loadfloor(lbllocation.Text, ddlTower.SelectedItem.Value, ddlFloor)
        Else
            ddlFloor.Items.Clear()
        End If
    End Sub

    Private Sub loadfloor(ByVal location_code As String, ByVal tower_code As String, ByVal ddlFloor As DropDownList)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_FLOORBYLOCTOWER")
        ' sp.Command.AddParameter("@LCM_CODE", Session("UID"), DbType.String)
        sp.Command.AddParameter("@LCM_CODE", location_code, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", tower_code, DbType.String)
        ddlFloor.DataSource = sp.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        'ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblMsg.Text = ""
        Dim ddl As DropDownList = CType(sender, DropDownList)
        Dim row As GridViewRow = CType(ddl.NamingContainer, GridViewRow)
        Dim index As Integer = row.RowIndex
        Dim lbllocation As Label = DirectCast(gvItems.Rows(index).FindControl("lbllocation"), Label)
        Dim ddlTower As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlTower"), DropDownList)
        Dim ddlFloor As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlFloor"), DropDownList)
        Dim ddlSpace As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlSpace"), DropDownList)
        If ddlFloor.SelectedItem.Value <> "--Select--" Then
            Space_Loadddl(ddlSpace, lbllocation.Text, ddlTower.SelectedItem.Value, ddlFloor.SelectedItem.Value)
        Else
            ddlSpace.Items.Clear()
        End If
    End Sub

    Private Sub Space_Loadddl(ByVal ddlSpace As DropDownList, ByVal locationid As String, ByVal tower_code As String, ByVal floor_code As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_SPACE_ASSETMAPPING")
            ' sp.Command.AddParameter("@LCM_CODE", Session("UID"), DbType.String)
            sp.Command.AddParameter("@LCM_CODE", locationid, DbType.String)
            sp.Command.AddParameter("@TWR_CODE", tower_code, DbType.String)
            sp.Command.AddParameter("@FLR_CODE", floor_code, DbType.String)
            ddlSpace.DataSource = sp.GetDataSet()
            ddlSpace.DataTextField = "SPC_ID"
            ddlSpace.DataValueField = "SPC_ID"
            ddlSpace.DataBind()
            ddlSpace.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")

        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        ddlLocation.Items.Insert(0, New ListItem("--All--", "ALL"))
        ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 1, 0)
        gvItems.Visible = True
        panel1.Visible = True


        'ddlLocation.Items.Remove("--All--")
    End Sub

    Private Sub BindAssets()
        Try
            panel1.Visible = True
            If Session("Procurement") = 1 Then
                If ddlReqId.SelectedIndex < 0 Then
                    lblMsg.Visible = True
                    lblMsg.Text = "No Requisitions Found"
                Else
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_ASTSREQID")
                    sp.Command.AddParameter("@REQ_ID", ddlReqId.SelectedItem.Value, DbType.String)
                    sp.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
                    gvItems.DataSource = sp.GetDataSet()
                    gvItems.DataBind()

                End If
            Else

                Dim param(7) As SqlParameter
                param(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
                param(0).Value = ddlLocation.SelectedItem.Value
                param(1) = New SqlParameter("@SubcategoryID", SqlDbType.NVarChar, 200)
                param(1).Value = ddlAstSubCat.SelectedItem.Value
                param(2) = New SqlParameter("@categoryID", SqlDbType.NVarChar, 200)
                param(2).Value = ddlAstCat.SelectedItem.Value
                param(3) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
                param(3).Value = Session("UID")
                param(4) = New SqlParameter("@brand", SqlDbType.NVarChar, 200)
                param(4).Value = ddlAstBrand.SelectedItem.Value
                param(5) = New SqlParameter("@Model", SqlDbType.NVarChar, 200)
                param(5).Value = ddlModel.SelectedItem.Value
                param(6) = New SqlParameter("@req_type", SqlDbType.NVarChar, 200)
                param(6).Value = ddlasttype.SelectedItem.Value
                param(7) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
                param(7).Value = Session("COMPANYID")
                ObjSubsonic.BindGridView(gvItems, "AM_GET_ASTLOCTWRFLR", param)
            End If
            'If (CheckBox1.Checked = True And CheckBox2.Checked = True) Or (CheckBox2.Checked = True) Then
            If RbtEmployee.Checked = True Then
                ' CheckBox1.Enabled = False
                gvItems.Columns(6).Visible = False
                gvItems.Columns(7).Visible = False
                gvItems.Columns(8).Visible = False
                gvItems.Columns(9).Visible = True
                For Each drow As GridViewRow In gvItems.Rows
                    'Dim ddlEmp As DropDownList = DirectCast(drow.FindControl("ddlEmployee"), DropDownList)
                    'Emp_Loadddl(ddlEmp)
                Next

            Else
                ' CheckBox2.Enabled = False
                gvItems.Columns(6).Visible = True
                gvItems.Columns(7).Visible = True
                gvItems.Columns(9).Visible = False
                gvItems.Columns(8).Visible = True
            End If

            If gvItems.Rows.Count > 0 Then
                btnSubmit.Visible = True
                txtremrks.Visible = True
            Else
                btnSubmit.Visible = False
                txtremrks.Visible = False
            End If


        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        lblMsg.Text = ""
        GridView1.PageIndex = e.NewPageIndex
        BindAssets()
    End Sub

    Protected Sub ddlReqId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReqId.SelectedIndexChanged
        Try
            gvItems.Visible = True
            lblMsg.Text = ""
            panel1.Visible = False
            If ddlReqId.SelectedIndex > 0 Then
                If ddlLocation.Items.Count > 0 Then
                    ' BindTower()
                    panel1.Visible = True
                    BindAssets()
                    For Each drow As GridViewRow In gvItems.Rows
                        Dim ddlAssets As DropDownList = DirectCast(drow.FindControl("ddlassets"), DropDownList)
                        Dim lbllocation As Label = DirectCast(drow.FindControl("lbllocation"), Label)
                        Dim ddlTower As DropDownList = DirectCast(drow.FindControl("ddlTower"), DropDownList)
                        loadtower(lbllocation.Text, ddlTower)
                        ' Space_Loadddl(ddlSpace, lbllocation.Text)
                    Next

                Else
                    'ddlTower.Items.Clear()
                    'ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
                    'ddlTower.SelectedValue = 0
                    'ddlfloor.Items.Clear()
                    'ddlfloor.Items.Insert(0, New ListItem("--Select--", "0"))
                    'ddlfloor.SelectedValue = 0
                    panel1.Visible = False
                End If

            Else
                ddlLocation.Items.Clear()
                ddlLocation.Items.Insert(0, New ListItem("--ALL--", "ALL"))
                'ddlLocation.SelectedValue = 0

            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim count As Integer = 0
            For Each row As GridViewRow In gvItems.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim ddlSpace As DropDownList = DirectCast(row.FindControl("ddlSpace"), DropDownList)
                Dim ddlTower As DropDownList = DirectCast(row.FindControl("ddlTower"), DropDownList)
                Dim ddlFloor As DropDownList = DirectCast(row.FindControl("ddlFloor"), DropDownList)
                Dim ddlAssetspace As DropDownList = DirectCast(row.FindControl("ddlAssetspace"), DropDownList)
                Dim ddlEmployee As TextBox = DirectCast(row.FindControl("ddlEmployee"), TextBox)
                'Dim ddlEmployee As DropDownList = DirectCast(row.FindControl("ddlEmployee"), DropDownList)

                If chkSelect.Checked Then
                    'If ddlTower.SelectedItem.Text = "--Select--" Then
                    '    lblMsg.Visible = True
                    '    lblMsg.Text = "Please select tower for selected checkbox"
                    '    Exit Sub
                    'ElseIf ddlFloor.SelectedItem.Text = "--Select--" Then
                    '    lblMsg.Visible = True
                    '    lblMsg.Text = "Please select floor for selected checkbox"
                    '    Exit Sub
                    'ElseIf (ddlSpace.SelectedItem.Text = "--Select--" And RbtSpace.Checked = True) And (RbtEmployee.Checked = False) Then
                    If RbtSpace.Checked = True Then
                        If ddlSpace.SelectedItem.Text = "--Select--" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Please select space id for selected checkbox"
                            Exit Sub
                        Else
                            count = count + 1
                        End If
                    ElseIf RbtEmployee.Checked = True Then
                        'If ddlEmployee.SelectedItem.Text = "--Select--" Then
                        '    lblMsg.Visible = True
                        '    lblMsg.Text = "Please select Employee id for selected checkbox"
                        '    Exit Sub
                        'Else
                        '    count = count + 1
                        'End If
                        If ddlEmployee.Text = "" Then
                            lblMsg.Visible = True
                            lblMsg.Text = "Please select space id for selected checkbox"
                            Exit Sub
                        Else
                            count = count + 1
                        End If
                    End If
                End If
            Next



            If count > 0 Then
                For Each row As GridViewRow In gvItems.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                    Dim ddlSpace As DropDownList = DirectCast(row.FindControl("ddlSpace"), DropDownList)
                    Dim ddlTower As DropDownList = DirectCast(row.FindControl("ddlTower"), DropDownList)
                    Dim ddlFloor As DropDownList = DirectCast(row.FindControl("ddlFloor"), DropDownList)
                    Dim ddlEmployee As TextBox = DirectCast(row.FindControl("ddlEmployee"), TextBox)
                    'Dim ddlEmp As DropDownList = DirectCast(row.FindControl("ddlEmployee"), DropDownList)
                    'Dim ddlSpace As DropDownList = DirectCast(row.FindControl("ddlSpace"), DropDownList)
                    Dim AssetCode As Label = DirectCast(row.FindControl("lblassetName"), Label)
                    Dim lbllocation As Label = DirectCast(row.FindControl("lbllocation"), Label)
                    Dim lblAssetId As Label = DirectCast(row.FindControl("lblAssetId"), Label)

                    If chkSelect.Checked Then
                        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_AMT_ASSET_SPACE_STAT")
                        sp1.Command.AddParameter("@LOC_ID", lbllocation.Text, DbType.String)
                        If RbtSpace.Checked = True Then
                            sp1.Command.AddParameter("@AAT_SPC_ID", ddlSpace.SelectedItem.Value, DbType.String)
                            sp1.Command.AddParameter("@TWR_ID", ddlTower.SelectedItem.Value, DbType.String)
                            sp1.Command.AddParameter("@FLR_ID", ddlFloor.SelectedItem.Value, DbType.String)



                        Else
                            sp1.Command.AddParameter("@AAT_SPC_ID", "", DbType.String)
                            sp1.Command.AddParameter("@TWR_ID", "", DbType.String)
                            sp1.Command.AddParameter("@FLR_ID", "", DbType.String)
                        End If

                        sp1.Command.AddParameter("@AAT_AST_CODE", AssetCode.Text, DbType.String)
                        sp1.ExecuteScalar()

                        If (RbtSpace.Checked = True And RbtEmployee.Checked = True) Or (RbtEmployee.Checked = True) Then
                            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ALLOCATE_SPACEASSET")
                            sp.Command.AddParameter("@AAT_AST_CODE", AssetCode.Text, DbType.String)
                            If RbtSpace.Checked = True Then
                                sp.Command.AddParameter("@AAT_SPC_ID", ddlSpace.SelectedItem.Value, DbType.String)
                            Else
                                sp.Command.AddParameter("@AAT_SPC_ID", "", DbType.String)
                            End If

                            If Session("Procurement") = 1 Then
                                sp.Command.AddParameter("@AAT_ITEM_REQUISITION", ddlReqId.SelectedItem.Value, DbType.String)
                            End If
                            sp.Command.AddParameter("@AAT_EMP_ID", Split(ddlEmployee.Text, "/")(0), DbType.String)
                            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
                            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
                            sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
                            sp.ExecuteScalar()
                        End If

                        If Session("Procurement") = 1 Then
                            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_UPDATE_ASTLBLGENERATED")
                            sp3.Command.AddParameter("@REQ_ID", ddlReqId.SelectedItem.Value, DbType.String)
                            sp3.Command.AddParameter("@AIPD_AST_CODE", lblAssetId.Text, DbType.String) ''MODEL CODE
                            sp3.Command.AddParameter("@AIPD_ASTLBL_QTY", count, DbType.Int32)
                            sp3.ExecuteScalar()
                        End If

                    End If
                Next
                lblMsg.Visible = True
                lblMsg.Text = "Asset mapped successfully..."
                btnSubmit.Visible = False
                txtremrks.Visible = False
                panel1.Visible = False
                ' ddlReqId.SelectedIndex = 0
                'ddlLocation.Items.Clear()
                'End after apporving inter movement assets in same location update the qunatity for the particular request
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Please select requested no.of asset(S) only..."
                Exit Sub
            End If
            If count = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "Please select atleast one check box..."
                Exit Sub
            End If

        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub

    Private Sub Emp_Loadddl(ByVal ddlEmp As DropDownList)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_AEMP_LOC")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.String)
            'sp.Command.AddParameter("@tow", ddlTower.SelectedItem.Value, DbType.String)
            'sp.Command.AddParameter("@flr", ddlfloor.SelectedItem.Value, DbType.String)
            ddlEmp.DataSource = sp.GetDataSet()
            ddlEmp.DataTextField = "AUR_FIRST_NAME"
            ddlEmp.DataValueField = "AUR_ID"
            ddlEmp.DataBind()
            ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindAssets()
        LoadGridData()
    End Sub

    Protected Sub ddlSpace_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList = CType(sender, DropDownList)
        Dim row As GridViewRow = CType(ddl.NamingContainer, GridViewRow)
        Dim index As Integer = row.RowIndex
        Dim lbllocation As Label = DirectCast(gvItems.Rows(index).FindControl("lbllocation"), Label)
        Dim ddlTower As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlTower"), DropDownList)
        Dim ddlFloor As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlFloor"), DropDownList)
        Dim ddlSpace As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlSpace"), DropDownList)

    End Sub

    'Protected Sub ddlEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim ddl As DropDownList = CType(sender, DropDownList)
    '    Dim row As GridViewRow = CType(ddl.NamingContainer, GridViewRow)
    '    Dim index As Integer = row.RowIndex
    '    Dim lbllocation As Label = DirectCast(gvItems.Rows(index).FindControl("lbllocation"), Label)
    '    Dim ddlTower As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlTower"), DropDownList)
    '    Dim ddlFloor As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlFloor"), DropDownList)
    '    Dim ddlSpace As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlSpace"), DropDownList)
    '    Dim ddlEmployee As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlEmployee"), DropDownList)

    'End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            gvItems.Visible = True
            lblMsg.Text = ""

            'If ddlReqId.SelectedIndex > 0 Then
            If Session("Procurement") = 1 Then
                bindreqid()
            End If

            panel1.Visible = True
            'If ddlReqId.SelectedIndex < 0 Then
            '    lblMsg.Visible = True
            '    lblMsg.Text = "No Requisitions Found"
            'Else
            '    BindAssets()
            'End If
            BindAssets()
            LoadGridData()



            'Else
            '    ddlLocation.Items.Clear()
            '    ddlLocation.Items.Insert(0, New ListItem("--ALL--", "0"))
            '    ddlLocation.SelectedValue = 0

            'End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

    Private Sub LoadGridData()
        For Each drow As GridViewRow In gvItems.Rows
            Dim ddlAssets As DropDownList = DirectCast(drow.FindControl("ddlassets"), DropDownList)
            Dim lbllocation As Label = DirectCast(drow.FindControl("lbllocation"), Label)
            Dim ddlTower As DropDownList = DirectCast(drow.FindControl("ddlTower"), DropDownList)
            Dim ddlFloor As DropDownList = DirectCast(drow.FindControl("ddlFloor"), DropDownList)
            Dim ddlSpace As DropDownList = DirectCast(drow.FindControl("ddlSpace"), DropDownList)
            loadtower(lbllocation.Text, ddlTower)
            loadfloor(lbllocation.Text, ddlTower.SelectedItem.Value, ddlFloor)
            Space_Loadddl(ddlSpace, lbllocation.Text, ddlTower.SelectedItem.Value, ddlFloor.SelectedItem.Value)

        Next
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            bindreqid()
            If Session("Procurement") = 1 Then
                If ddlReqId.SelectedIndex < 0 Then
                    lblMsg.Visible = True
                    lblMsg.Text = "No Requisitions Found"
                End If
            End If


            If gvItems.Rows.Count > 0 Then
                ' ddlReqId.Visible = False
                panel1.Visible = True
                ' lbreq.Visible = True

            End If

            BindAssets()
            LoadGridData()

            'If (CheckBox1.Checked = True And CheckBox2.Checked = True) Or (CheckBox2.Checked = True) Then
            '    gvItems.Columns(6).Visible = True
            'Else
            '    gvItems.Columns(6).Visible = False
            'End If
            'If (CheckBox2.Checked = True) Then
            '    gvItems.Columns(6).Visible = True
            'Else
            '    gvItems.Columns(6).Visible = False
            'End If
            'If (CheckBox1.Checked = True And CheckBox2.Checked = True) Or (CheckBox2.Checked = True) Then
            '    gvItems.Columns(6).Visible = True
            'Else
            '    gvItems.Columns(6).Visible = False
            'End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub

    Public Sub cleardata()
        ddlAstCat.ClearSelection()
        ddlAstSubCat.ClearSelection()
        ddlLocation.ClearSelection()
        ddlAstBrand.ClearSelection()
        ddlModel.ClearSelection()
        gvItems.DataSource = Nothing
        gvItems.DataBind()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ASSET_EMPLOYEE_SEARCH")
        sp.Command.AddParameter("@SEARCH_CRITERIA", txtSearch.Text, Data.DbType.String)
        'sp.Command.AddParameter("@StatusId", 1004, Data.DbType.Int32)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()

        For Each drow As GridViewRow In gvItems.Rows
            'Dim ddlEmp As DropDownList = DirectCast(drow.FindControl("ddlEmployee"), DropDownList)
            'Emp_Loadddl(ddlEmp)

        Next
    End Sub
    '<System.Web.Script.Services.ScriptMethod(), System.Web.Services.WebMethod()>
    'Public Shared Function GetAutoCompleteData(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

    '    Dim sp As New SubSonic.StoredProcedure("GET_EMP_DETAILS_GRID")
    '    sp.Command.AddParameter("@SEARCH_CRITERIA", prefixText)

    '    Dim customers As List(Of String) = New List(Of String)
    '    Dim sdr As SqlDataReader = sp.GetReader()
    '    While sdr.Read
    '        customers.Add(sdr("AUR_KNOWN_AS").ToString)
    '    End While

    '    Return customers

    'End Function

End Class
