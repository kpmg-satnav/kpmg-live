Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_EditAssetPrices
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                BindLocation()
                ddlLocation.Items.Insert(1, New ListItem("--All--", "1"))
                ddlTower.Items.Clear()
                ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlTower.SelectedValue = 0
                ddlFloor.Items.Clear()
                ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlFloor.SelectedValue = 0
            End If
        End If
    End Sub

    Private Sub BindLocation()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex = 1 Then
                ddlTower.Items.Clear()
                ddlTower.Items.Insert(0, New ListItem("--All--", "0"))
                ddlTower.SelectedValue = 0
                ddlFloor.Items.Clear()
                ddlFloor.Items.Insert(0, New ListItem("--All--", "0"))
                ddlFloor.SelectedValue = 0
            ElseIf ddlLocation.SelectedIndex > 1 Then
                BindTower()
            Else
                ddlTower.Items.Clear()
                ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlTower.SelectedValue = 0
                ddlFloor.Items.Clear()
                ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlFloor.SelectedValue = 0
                
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub BindTower()
        Try
            ddlTower.Items.Clear()
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
            param(0).Value = ddlLocation.SelectedItem.Value
            ObjSubsonic.Binddropdown(ddlTower, "GET_LOCTWR", "TWR_NAME", "TWR_CODE", param)

            ddlTower.Items.Insert(1, New ListItem("--All--", "1"))
            ddlTower.ClearSelection()
            ddlTower.Items(0).Selected = True

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            If ddlTower.SelectedIndex = 1 Then
                ddlFloor.Items.Clear()
                ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlFloor.Items.Insert(1, New ListItem("--All--", "1"))
                ddlFloor.ClearSelection()
                ddlFloor.Items(0).Selected = True
            ElseIf ddlTower.SelectedIndex > 0 Then
                lblMsg.Text = ""
                BindFloor()
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlFloor.SelectedValue = 0
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindFloor()
        Try
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
            param(0).Value = ddlLocation.SelectedItem.Value
            param(1) = New SqlParameter("@dummy1", SqlDbType.NVarChar, 100)
            param(1).Value = ddlTower.SelectedItem.Value
            ObjSubsonic.Binddropdown(ddlFloor, "GET_TWRFLR", "FLR_NAME", "FLR_CODE", param)
            ddlFloor.Items.Insert(1, New ListItem("--All--", "1"))
            ddlFloor.ClearSelection()
            ddlFloor.Items(0).Selected = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        BindStockReport()
    End Sub

    Private Sub BindStockReport()
        Dim Lcm_code As String = ""
        Dim Twr_code As String = ""
        Dim Flr_code As String = ""
        Dim Aim_code As String = ""


        If ddlLocation.SelectedItem.Value = "--Select--" Then
            lblMsg.Text = "Select Location"
            Exit Sub
        ElseIf ddlLocation.SelectedItem.Value = "1" Then
            Lcm_code = ""
            lblMsg.Text = ""
        Else
            lblMsg.Text = ""
            Lcm_code = ddlLocation.SelectedItem.Value

        End If

        If ddlLocation.SelectedItem.Value = "1" Then
            Twr_code = ""
            Flr_code = ""
            lblMsg.Text = ""
        Else

            If ddlTower.SelectedItem.Text = "--Select--" Then
                lblMsg.Text = "Select Tower"
                Exit Sub
            ElseIf ddlTower.SelectedItem.Value = "1" Then
                Twr_code = ""
            Else
                lblMsg.Text = ""
                Twr_code = ddlTower.SelectedItem.Value
            End If
            If ddlFloor.SelectedItem.Text = "--Select--" Then
                lblMsg.Text = "Select Floor"
                Exit Sub
            ElseIf ddlFloor.SelectedItem.Value = "1" Then
                Flr_code = ""
            Else
                lblMsg.Text = ""
                Flr_code = ddlFloor.SelectedItem.Value
            End If


        End If



        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = Lcm_code
        param(1) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = Twr_code
        param(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = Flr_code
        ObjSubsonic.BindGridView(gvAstList, "GET_ALLASSETS", param)
        If gvAstList.Rows.Count = 0 Then
            gvAstList.DataSource = Nothing
            gvAstList.DataBind()
            Fieldset1.Visible = False
            btnSubmit.Visible = False
        Else
            Fieldset1.Visible = True
            btnSubmit.Visible = True
            
        End If




    End Sub

    Protected Sub gvAstList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAstList.PageIndexChanging
        gvAstList.PageIndex = e.NewPageIndex
        BindStockReport()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim cntSelect As Integer = 0
        For Each gRow As GridViewRow In gvAstList.Rows
            Dim chkSelect As CheckBox = CType(gRow.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked = True Then
                cntSelect += 1
            End If
        Next
        If cntSelect > 0 Then
            For Each gRow As GridViewRow In gvAstList.Rows
                Dim chkSelect As CheckBox = CType(gRow.FindControl("chkSelect"), CheckBox)
                Dim lblAAT_ID As Label = CType(gRow.FindControl("lblAAT_ID"), Label)
                Dim txtAstPrice As TextBox = CType(gRow.FindControl("txtAstPrice"), TextBox)
                Dim lblAIM_NAME As Label = CType(gRow.FindControl("lblAIM_NAME"), Label)
                Dim lblSalvageValue As Label = CType(gRow.FindControl("lblSalvageValue"), Label)
                lblMsg.Text = ""
                If chkSelect.Checked = True Then
                    ' Updating Asset Cost

                    If IsNumeric(txtAstPrice.Text) = False Then
                        lblMsg.Text = " Invalid Asset Cost for " & lblAIM_NAME.Text
                        Exit Sub
                    ElseIf CDbl(txtAstPrice.Text) <= CDbl(lblSalvageValue.Text) Then
                        lblMsg.Text = " Invalid Asset Cost for " & lblAIM_NAME.Text & ", it should be greater than the salvage value."
                        Exit Sub
                    Else
                        lblMsg.Text = ""
                    End If



                    Dim param(1) As SqlParameter
                    param(0) = New SqlParameter("@AAT_ID", SqlDbType.BigInt)
                    param(0).Value = CInt(lblAAT_ID.Text)
                    param(1) = New SqlParameter("@AST_COST", SqlDbType.Float)
                    param(1).Value = CDbl(txtAstPrice.Text)

                    ObjSubsonic.GetSubSonicExecute("UPDATEAST_COST", param)
                    lblMsg.Text = "Asset Cost Updated Successfully."
                End If

            Next

        Else
            lblMsg.Text = "Please select at least one asset."
            Exit Sub
        End If
        BindStockReport()
    End Sub
End Class
