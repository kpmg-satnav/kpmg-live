Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class Controls_EditPropertyType
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindRole()
            BindProperty()
            BindPropertyRole()
        End If
    End Sub
    Private Sub BindRole()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETROLES")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            lirole.DataSource = sp.GetDataSet()
            lirole.DataTextField = "ROL_DESCRIPTION"
            lirole.DataValueField = "ROL_ID"
            lirole.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim ValidateCode As Integer
            ValidateCode = ValidatePropertyCode()
            If ValidateCode = 0 Then
                lblMsg.Text = "PropertyType already exist please enter another propertType"
            ElseIf ValidateCode = 1 Then
                Updaterecord()
                UpdateRole()
            End If
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=35")

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Updaterecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_PROPERTY_TYPE")
            sp1.Command.AddParameter("@sno", Request.QueryString("id"), DbType.Int32)
            sp1.Command.AddParameter("@PN_PROPERTYTYPE", txtPropType.Text, DbType.String)
            sp1.Command.AddParameter("@PN_TYPESTATUS", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub updaterole()
        Try
            DELROLE()
            For Each li As ListItem In lirole.Items
                If li.Selected = True Then
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_PROPERTY_ROLE")
                    sp.Command.AddParameter("@PROPCODE", txtPropType.Text, DbType.String)
                    sp.Command.AddParameter("@ROLEID", li.Value, DbType.String)
                    sp.ExecuteScalar()
                End If
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub delrole()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"del_propertyrole")
            sp.Command.AddParameter("@propertyid", Request.QueryString("id"), DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function ValidatePropertyCode()
        Dim validatecode As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_PROPERTY_CODE1")
        sp.Command.AddParameter("@Property_TYPE", txtPropType.Text, DbType.String)
        sp.Command.AddParameter("@sno", Request.QueryString("id"), DbType.Int32)
        validatecode = sp.ExecuteScalar()
        Return validatecode
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Commented by Netaji to send the page back to prvious page
        'Response.Redirect("frmAddLeaseType.aspx")
        Response.Redirect("frmAddPropertyType.aspx")
    End Sub
    
    Private Sub BindPropertyRole()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROPERTY_ROLE")
            sp.Command.AddParameter("@sno", Request.QueryString("id"), DbType.Int32)
            Dim dr As SqlDataReader
            dr = sp.GetReader()
            While dr.Read()
                'Dim li As ListItem = Nothing
                lirole.Items.FindByValue(dr("ROLEID")).Selected = True

                'If Not li Is Nothing Then
                '    li.Selected = True
                'End If
            End While
            
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindProperty()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROPERTY_ROLE_DETAILS")
        sp.Command.AddParameter("@sno", Request.QueryString("id"), Data.DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            txtPropType.Text = ds.Tables(0).Rows(0).Item("PN_PROPERTYTYPE")
            Dim stat As Integer = ds.Tables(0).Rows(0).Item("PN_TYPESTATUS")
            If stat = 0 Then
                ddlStatus.SelectedValue = 0
            Else
                ddlStatus.SelectedValue = 1
            End If
        End If
    End Sub
End Class
