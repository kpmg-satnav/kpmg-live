Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_AddPropertyDocuments
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CITYNAME")
                sp3.Command.AddParameter("@DUMMY", "", DbType.String)
                ddlCity.DataSource = sp3.GetDataSet()
                ddlCity.DataTextField = "CTY_NAME"
                ddlCity.DataValueField = "CTY_CODE"
                ddlCity.DataBind()
                ddlCity.Items.Insert(0, New ListItem("---Select---", "0"))

            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            lblMsg.Visible = False
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim orgfilename As String = fpBrowseDoc.FileName
        Dim repdocdatetime As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & orgfilename
        Try
            If (fpBrowseDoc.HasFile) Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(fpBrowseDoc.FileName)
                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime
                fpBrowseDoc.PostedFile.SaveAs(filePath)
                lblMsg.Visible = True
                lblMsg.Text = "File upload successfully"
                insertnewrecord(repdocdatetime)
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Select the file"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PRPIDNAME")
            sp4.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            ddlPropIDName.DataSource = sp4.GetDataSet()
            ddlPropIDName.DataTextField = "PN_NAME"
            ddlPropIDName.DataValueField = "BDG_ID"
            ddlPropIDName.DataBind()
            ddlPropIDName.Items.Insert(0, New ListItem("--Select--", "0"))
        Else
            ddlPropIDName.Items.Clear()
            ddlPropIDName.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlPropIDName.SelectedIndex = 0
        End If
    End Sub
    Public Sub insertnewrecord(ByVal repdocdatetime As String)
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_PROPERTY_DOCS")
            Dim orgfilename As String = fpBrowseDoc.FileName
            sp1.Command.AddParameter("@RPT_CITY", ddlCity.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@RPT_FOR", ddlPropIDName.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@RPT_DATE", getoffsetdatetime(DateTime.Now), DbType.Date)
            sp1.Command.AddParameter("@RPT_TITLE", txtDocTitle.Text, DbType.String)
            sp1.Command.AddParameter("@RPT_ORG_FILENAME", fpBrowseDoc.FileName, DbType.String)
            sp1.Command.AddParameter("@RPT_DOC", repdocdatetime, DbType.String)
            sp1.ExecuteScalar()
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=6")
            cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub cleardata()
        ddlCity.SelectedIndex = 0
        ddlPropIDName.SelectedIndex = 0
        txtDocTitle.Text = ""
    End Sub
End Class
