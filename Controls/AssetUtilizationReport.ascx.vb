Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO
Partial Class Controls_ViewLeaseAgreements
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            BindGrid1()
        End If
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASSET_STATUSREPORT")
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
    End Sub
    Private Sub BindGrid1()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASSET_UTILIZATION_REPORT")
        gvitems1.DataSource = sp.GetDataSet
        gvitems1.DataBind()
    End Sub
   
    Protected Sub btnexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexcel.Click
        exportpanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        exportpanel1.FileName = "Asset Status Report"
    End Sub

    Protected Sub btnexport1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport1.Click
        exportpanel2.ExportType = ControlFreak.ExportPanel.AppType.Excel
        exportpanel2.FileName = "Asset Utilization Report"
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems1.PageIndexChanging
        gvitems1.PageIndex = e.NewPageIndex()
        BindGrid1()
    End Sub
End Class
