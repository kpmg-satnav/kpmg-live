﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CategoryVendorMaster.ascx.vb" Inherits="Controls_CategoryVendorMaster" %>
 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

        <div>
            <table id="table1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td align="center" width="100%">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                            ForeColor="Black">Vendor Category Master <hr align="center" width="60%" /></asp:Label></td>
                </tr>
            </table>
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
                <table id="table4" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                    <tr>
                        <td align="left" width="100%" colspan="3">
                            <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                        <td width="100%" class="tableHEADER" align="left">
                            <strong>&nbsp;Vendor Category Master</strong>
                        </td>
                        <td>
                            <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                                ForeColor="" ValidationGroup="Val1" />
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            
                            <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                            <table id="table2" cellspacing="0" cellpadding="3" width="100%" border="1">
                         <%--       <tr>
                                    <td align="center" width="50%" colspan="2">
                                        <asp:RadioButtonList ID="rbActions" runat="server" Width="176px" Height="13px" AutoPostBack="True"
                                            Font-Bold="True" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbActions_SelectedIndexChanged">
                                            <asp:ListItem Value="Add" Selected="true">Add</asp:ListItem>
                                            <asp:ListItem Value="Modify">Modify</asp:ListItem>
                                        </asp:RadioButtonList></td>
                                </tr>--%>
                                <tr>
                                    <td width="50%">Vendor ID <font class="clsNote">*</font> <asp:RequiredFieldValidator ID="reqvendid" runat="server" ControlToValidate="txtVendId"
                                            Display="none" ErrorMessage="Please enter Vendor Id!" ValidationGroup="Val1"></asp:RequiredFieldValidator></td>
                                    <td width="50%">
                                        <asp:TextBox ID="txtVendId" runat="server" ValidationGroup="Val1" CssClass="clsTextField" Width="99%"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td width="50%">Vendor Name <font class="clsNote">*</font> <asp:RequiredFieldValidator ID="reqvendname" runat="server" ControlToValidate="txtVendName"
                                            Display="none" ErrorMessage="Please enter Vendor Name!" ValidationGroup="Val1"></asp:RequiredFieldValidator></td>
                                    <td width="50%">
                                        <asp:TextBox ID="txtVendName" runat="server" ValidationGroup="Val1" CssClass="clsTextField" Width="99%"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        
                                            Select Service Category <font class="clsNote">*</font>
                                               <asp:Label ID="lblTemp" runat="server" Visible="false"></asp:Label>
                                            <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlServiceCat"
                                                ErrorMessage="Please Select Service Category!" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                                        
                                    </td>
                                    <td width="50%">
                                        <asp:DropDownList ID="ddlServiceCat" runat="server" CssClass="clsComboBox" Width="100%"
                                            AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                        <td align="left" style="height: 26px; width: 50%">
                                            Service Type<font class="clsNote"> *</font>
                                            <asp:RequiredFieldValidator ID="rfvreqtype" runat="server" ControlToValidate="ddlServiceType"
                                                Display="NONE" ErrorMessage="Please Select Service Request Type" ValidationGroup="Val1"
                                                InitialValue="--Select--" Enabled="true"></asp:RequiredFieldValidator></td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:DropDownList ID="ddlServiceType" runat="server" TabIndex="12" CssClass="clsComboBox"
                                                Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                <tr>
                                    <td width="50%">
                                       
                                            Select City<font class="clsNote"> *</font>
                                            <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None" ControlToValidate="ddlCity"
                                                ErrorMessage="Please Select City!" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                                       
                                    </td>
                                    <td width="50%">
                                        <asp:DropDownList ID="ddlCity" runat="server" CssClass="clsComboBox" Width="100%"
                                            AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                    <tr>
                                    <td width="50%">
                                        
                                            Select Location<font class="clsNote"> *</font>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ControlToValidate="ddlLocation"
                                                ErrorMessage="Please Select Location!" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                                       
                                    </td>
                                    <td width="50%">
                                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="clsComboBox" Width="100%">
                                        </asp:DropDownList></td>
                                </tr>
                                
                              
                                <tr>
                                    <td align="left" style="height: 16px; width: 50%;">Select Status<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                                            Display="none" ErrorMessage="Please Status !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="height: 16px; width: 50%;">
                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="clsComboBox" Width="99%">
                                            <asp:ListItem>--Select--</asp:ListItem>
                                            <asp:ListItem Value="1">Active</asp:ListItem>
                                            <asp:ListItem Value="0">InActive</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="height: 16px; width: 50%;">Vendor Address<font class="clsNote">*</font>
                                        </td>
                                    <td align="left" style="height: 16px; width: 50%;">
                                        <asp:TextBox ID="txtaddress" runat="server"  ValidationGroup="Val1" CssClass="clsTextField" Width="99%" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" style="height: 39px">
                                        <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" PostBackUrl="~/WorkSpace/SMS_Webfiles/helpmasters.aspx" CausesValidation="False" />
                                        &nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Add" ValidationGroup="Val1" />

                                        <%--   <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Back" />--%>&nbsp;
                                    </td>
                                </tr>
                            </table>

                            <table id="Table5" width="100%" runat="Server" cellpadding="0" cellspacing="0" border="1">
                                <tr>
                                    <td align="center" style="height: 20px">
                                        <asp:GridView ID="gvSer" runat="server" AllowPaging="True" AllowSorting="False"
                                            RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                                            PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Records Found">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" CssClass="lblASTID" Text='<%#Eval("CTV_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Serive Category">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblname" runat="server" CssClass="lblASTCode" Text='<%#Eval("CTV_VENDOR_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="City">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCity" runat="server" CssClass="lblStatus" Text='<%#Bind("CTV_VEND_CTY")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLoc" runat="server" CssClass="lblStatus" Text='<%#Bind("CTV_VEND_LOC")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Service Category">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcat" runat="server" CssClass="lblStatus" Text='<%#Bind("CTV_CAT_TYPE")%>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblcategory" runat="server" CssClass="CATEGORY" Text='<%#Bind("CATEGORY")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                     <asp:TemplateField HeaderText="Service Category Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcat_type" runat="server" CssClass="lblStatus" Text='<%#Bind("CTV_CAT_PROBLEM")%>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblcategrory_type" runat="server" CssClass="lblStatus" Text='<%#Bind("CATEGORY_TYPE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#Bind("CTV_STATUS")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:ButtonField Text="EDIT" CommandName="EDIT" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </ContentTemplate>
   
</asp:UpdatePanel>