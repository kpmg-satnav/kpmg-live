<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeaveBalance.ascx.vb"
    Inherits="Controls_LeaveBalance" %>



<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>









<asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Select Associate ID<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvID" runat="server" ControlToValidate="ddlID"
                    Display="None" ErrorMessage="Please Select Associate ID" ValidationGroup="Val1"
                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                <div class="col-md-7">
                    <asp:DropDownList ID="ddlID" runat="server" CssClass="selectpicker" data-live-search="true"
                        AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Associate Name<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtAssociateName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Designation<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDesig" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Department<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtDept" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Reporting Manager<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtRM" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <label class="col-md-5 control-label">Contact Number<span style="color: red;"></span></label>
                <div class="col-md-7">
                    <asp:TextBox ID="txtContactNo" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="pnl1" runat="server">


    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-4 control-label">Balanace Paid Leaves As On<span style="color: red;"></span></label>
                    <asp:Label ID="lblTodayDate" runat="server" CssClass="col-md-4 control-label"></asp:Label>
                    <label class="col-md-4 control-label">
                        <asp:TextBox ID="txtBalPaidLs" runat="server" CssClass="form-control"></asp:TextBox>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-4 control-label">Balance Compensatory Off As On<span style="color: red;"></span></label>
                    <asp:Label ID="lblTdate" runat="server" CssClass="col-md-4 control-label"></asp:Label>
                    <label class="col-md-4 control-label">
                        <asp:TextBox ID="txtBalCom" runat="server" CssClass="form-control"></asp:TextBox>
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="pnl2" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <asp:Label ID="lblbal" runat="server" CssClass="col-md-4 control-label"></asp:Label>

                    <asp:Label ID="lblToDate" runat="server" CssClass="col-md-4 control-label"></asp:Label>

                    <label class="col-md-4 control-label">
                        <asp:TextBox ID="txtBalPM" runat="server" CssClass="form-control"></asp:TextBox>
                    </label>
                </div>
            </div>
        </div>
    </div>


</div>
