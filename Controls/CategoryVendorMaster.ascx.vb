﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic

Partial Class Controls_CategoryVendorMaster
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bindrequests()
            bindcity()
            fillgrid()
            gvSer.Visible = True
            btnSubmit.Text = "Add"
        End If
    End Sub
    Public Sub bindrequests()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETACTIVESERREQUESTTYPE")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlServiceCat.DataSource = sp.GetDataSet()
        ddlServiceCat.DataTextField = "CAT_NAME"
        ddlServiceCat.DataValueField = "CAT_ID"
        ddlServiceCat.DataBind()
        ddlServiceCat.Items.Insert(0, "--Select--")
    End Sub
    Private Sub BindService_request_Type(ByVal idparent As Integer)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@IDPARENT", SqlDbType.Int)
        param(0).Value = idparent
        ObjSubSonic.Binddropdown(ddlServiceType, "GET_CATEGORIES", "CATEGORYNAME", "IDCATEGORY", param)
    End Sub
    Public Sub bindcity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETACTIVESERCITY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, "--Select--")
    End Sub
    Public Sub bindLocation()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTY_CODE", SqlDbType.Int)
        param(0).Value = ddlCity.SelectedValue
        ObjSubSonic.Binddropdown(ddlLocation, "GET_LOCATION_BYCITY", "LCM_NAME", "LCM_CODE", param)
    End Sub
    Public Sub cleardata()
        ddlCity.ClearSelection()
        ddlLocation.ClearSelection()
        ddlServiceCat.ClearSelection()
        ddlServiceType.ClearSelection()
        ddlStatus.ClearSelection()
        txtVendId.Text = String.Empty
        txtVendName.Text = String.Empty
        txtaddress.Text = String.Empty
        txtVendId.Enabled = True
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If btnSubmit.Text = "Add" Then
            insertnewrecord()
        Else
            updaterecord()
        End If
    End Sub

    Public Sub updaterecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CTV_UPDATE_VENDOR")
            sp1.Command.AddParameter("@CTV_ID", CInt(lblTemp.Text), DbType.Int32)
            sp1.Command.AddParameter("@CTV_VENDOR_NAME", txtVendName.Text, DbType.String)
            sp1.Command.AddParameter("@CTV_VEND_CTY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@CTV_VEND_LOC", ddlLocation.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@CTV_CAT_TYPE", ddlServiceCat.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@CTV_CAT_PROBLEM", ddlServiceType.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@CTV_UP_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@CTV_STATUS", CInt(ddlStatus.SelectedValue), DbType.Int32)
            sp1.Command.AddParameter("@CTV_VENDOR_ADDR", txtaddress.Text, DbType.String)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "Record updated successfully..."
            cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub insertnewrecord()
        Try
            Dim flag As Integer
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CTV_INSERT_VENDOR")
            sp1.Command.AddParameter("@CTV_VENDOR_ID", txtVendId.Text, DbType.String)
            sp1.Command.AddParameter("@CTV_VENDOR_NAME", txtVendName.Text, DbType.String)
            sp1.Command.AddParameter("@CTV_VEND_CTY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@CTV_VEND_LOC", ddlLocation.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@CTV_CAT_TYPE", ddlServiceCat.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@CTV_CAT_PROBLEM", ddlServiceType.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@CTV_UP_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@CTV_STATUS", CInt(ddlStatus.SelectedValue), DbType.Int32)
            sp1.Command.AddParameter("@CTV_VENDOR_ADDR", txtaddress.Text, DbType.String)
            flag = sp1.ExecuteScalar()
            If flag = 0 Then
                fillgrid()
                lblMsg.Visible = True
                lblMsg.Text = "Record inserted successfully..."
                cleardata()
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Vendor ID alredy Exists"

            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CTV_GETCATEGORY_VENDOR")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvSer.DataSource = ds
        gvSer.DataBind()
        For i As Integer = 0 To gvSer.Rows.Count - 1
            Dim lblstatus As Label = CType(gvSer.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "InActive"
            End If
        Next
    End Sub

    Protected Sub gvSer_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvSer.PageIndexChanging
        gvSer.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub gvSer_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvSer.RowCommand

        If e.CommandName = "EDIT" Then
            btnSubmit.Text = "Modify"
            If gvSer.Rows.Count > 0 Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                Dim lblID As Label = DirectCast(gvSer.Rows(rowIndex).FindControl("lblID"), Label)
                Dim id As Integer = CInt(lblID.Text)
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CTV_EDIT_GETCATEGORY_VENDOR")
                sp.Command.AddParameter("@id", id, DbType.Int32)
                lblTemp.Text = id
                Dim ds As DataSet
                ds = sp.GetDataSet()

                If ds.Tables(0).Rows.Count > 0 Then

                    txtVendId.Enabled = False
                    ddlServiceCat.ClearSelection()
                    ddlServiceCat.Items.FindByValue(ds.Tables(0).Rows(0).Item("CTV_CAT_TYPE")).Selected = True
                    ObjSubSonic.Binddropdown(ddlServiceType, "CTV_GET_CATEGORY", "Cat_name", "cat_id")
                    ddlServiceType.Items.FindByValue(ds.Tables(0).Rows(0).Item("CTV_CAT_PROBLEM")).Selected = True
                    ddlCity.ClearSelection()
                    ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CTV_VEND_CTY")).Selected = True
                    Dim param(0) As SqlParameter
                    param(0) = New SqlParameter("@dummy", SqlDbType.Int)
                    param(0).Value = 1
                    ObjSubSonic.Binddropdown(ddlLocation, "GET_BDGLOCATION", "LCM_NAME", "LCM_CODE", param)
                    ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("CTV_VEND_LOC")).Selected = True
                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("CTV_STATUS")).Selected = True
                    lblTemp.Text = ds.Tables(0).Rows(0).Item("CTV_ID").ToString()
                    txtVendId.Text = ds.Tables(0).Rows(0).Item("CTV_VENDOR_ID").ToString()
                    txtVendName.Text = ds.Tables(0).Rows(0).Item("CTV_VENDOR_NAME").ToString()
                    txtaddress.Text = ds.Tables(0).Rows(0).Item("CTV_VENDOR_ADDR").ToString()


                End If

            End If
        End If


        'fillgrid()
    End Sub

    Protected Sub ddlServiceCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlServiceCat.SelectedIndexChanged
        If ddlServiceCat.SelectedIndex <> 0 Then
            BindService_request_Type(ddlServiceCat.SelectedValue)
        End If
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex <> 0 Then
            bindLocation()
        End If
    End Sub

    Protected Sub gvSer_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvSer.RowEditing

    End Sub
End Class
