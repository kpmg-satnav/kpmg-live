﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports NumToWordRupees

Partial Class Controls_GenerateIssuePrint
    Inherits System.Web.UI.UserControl

    Dim ntw As New NumToWordRupees

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnprint.Attributes.Add("OnClick", "JavaScript:printPartOfPage('Div1')")
        Dim reqid = Request.QueryString("id")
        ' BindVendorDetails()
        BindOtherDetails(reqid)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_CONSUMABLES_FOR_PRINTISSUEGRD")
        sp.Command.AddParameter("@Req_id", reqid, DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
        'Dim total As Integer = 0
        'For Each row As GridViewRow In gvItems.Rows
        '    Dim txtTotPrice As Label = DirectCast(row.FindControl("txtQty"), Label)
        '    Dim tot As Integer
        '    tot = CInt(txtTotPrice.Text)
        '    total = total + tot

        '    'lblTotalPrice.Text = total
        'Next
        'Dim lblTotalPrice As Label = DirectCast(gvItems.FooterRow.FindControl("txtQty"), Label)
        'lblTotalPrice.Text = Convert.ToDecimal(total).ToString("#,##0")
        Try

        Catch ex As Exception

        End Try
        'Response.Write(total)
    End Sub

    Private Sub BindOtherDetails(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_CONSUMABLES_FOR_PRINTISSUEDETAILS")
        sp.Command.AddParameter("@Req_id", ReqId, DbType.String)

        Dim dr As SqlDataReader = sp.GetReader()
        If dr.Read() Then
            lblLocation.Text = "Location    :" & dr("LCM_NAME").ToString()
            lblDate.Text = "Raised Date :" & dr("AIR_REQ_DATE").ToString()
            lblraised.Text = "Raised BY :" & dr("AIR_AUR_ID").ToString()
            lblReqid.Text += ReqId
        End If
    End Sub
    Dim total As Integer = 0
    Protected Sub gvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItems.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            total += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "AID_QTY"))
        End If
        If e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(0).Text = "Total Items   : "
            e.Row.Cells(0).HorizontalAlign = HorizontalAlign.Right
            e.Row.Cells(1).Text = total

        End If

    End Sub
End Class
