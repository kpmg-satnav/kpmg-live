Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Controls_ConfChanges
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            tablegrid1.Visible = True
           
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_CHECK_STATUS")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
            For i As Integer = 0 To gvItems.Rows.Count - 1
                Dim lblstat As Label = CType(gvItems.Rows(i).FindControl("lblstat"), Label)
                If lblstat.Text = "0" Then
                    lblstat.Text = "IN"
                Else
                    lblstat.Text = "OUT"
                End If
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
End Class
