Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions

Partial Class Controls_RMApprovalDetailsl
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim CatId As String
    Dim asstsubcat As String
    Dim asstbrand As String
    Dim astmodel As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Page)
        If scriptManager IsNot Nothing Then

            scriptManager.RegisterPostBackControl(grdDocs)

        End If

        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
            'RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        Else
            If Not IsPostBack Then
                Dim UID As String = Session("uid")

                BindUsers(UID)
                '  BindCategories()
                getassetcategory()
                ddlAstBrand.Items.Insert(0, New ListItem("No Brand", "No Brand"))
                ddlAstModel.Items.Insert(0, New ListItem("No Model", "No Model"))
                pnlItems.Visible = True
                BindRequisition()
                grid1.Visible = False
            End If
        End If
    End Sub
    'Private Function GetCurrentUser() As Integer
    '    If String.IsNullOrEmpty(Session("UID")) Then
    '        Return 0
    '    Else
    '        Return CInt(Session("UID"))
    '    End If
    'End Function
    Private Sub BindRequisition()
        Dim ReqId As String = Request("RID")
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_REQUISITION_GetByReqId")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId

                Dim RaisedBy As Integer = 0
                Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)

                Dim li As ListItem = ddlEmp.Items.FindByValue(CStr(RaisedBy))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                'Dim CatId As Integer = 0
                'Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)

                'li = ddlAstCat.Items.FindByValue(CStr(CatId))
                'If Not li Is Nothing Then
                '    li.Selected = True
                'End If
                'Dim CatId As String
                ddlAstCat.Enabled = False
                CatId = dr("AIR_ITEM_TYPE")
                li = ddlAstCat.Items.FindByValue(CStr(CatId))

                If Not li Is Nothing Then
                    li.Selected = True
                End If
                ddlAstCat.Enabled = False


                getassetsubcategory(CatId)
                ddlAstSubCat.Enabled = True
                asstsubcat = dr("AIR_ITEM_SUBCAT")

                ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True

                getbrandbycatsubcat(CatId, asstsubcat)

                asstbrand = dr("AIR_ITEM_BRD")
                ddlAstBrand.Items.FindByValue(asstbrand).Selected = True
                ddlAstBrand.Enabled = True

                getmakebycatsubcat()


                astmodel = dr("AIR_ITEM_MOD")
                ddlAstModel.Items.FindByValue(astmodel).Selected = True
                ddlAstModel.Enabled = True

                txtRemarks.Text = dr("AIR_REMARKS")
                txtRMRemarks.Text = dr("AIR_RM_REMARKS")
                'txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                txtStatus.Text = dr("STA_TITLE")
                txtQty.Text = dr("qty")
                If String.IsNullOrEmpty(Trim(txtRemarks.Text)) Then
                    tr3.Visible = False
                Else
                    tr3.Visible = True
                End If

                'If String.IsNullOrEmpty(Trim(txtAdminRemarks.Text)) Then
                '    tr2.Visible = False
                'Else
                '    tr2.Visible = True
                'End If

                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)

                'If RaisedBy = GetCurrentUser() Then
                '    If StatusId = 1001 Or StatusId = 1002 Then
                '        btnApprove.Enabled = True
                '        btnCancel.Enabled = True
                '    Else
                '        btnApprove.Enabled = False
                '        btnCancel.Enabled = False
                '    End If
                'Else
                '    btnApprove.Enabled = False
                '    btnCancel.Enabled = False
                'End If

                'BindGrid()
                BindDocuments(ReqId)
                'For Each row As GridViewRow In gvItems.Rows
                '    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                '    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
                '    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)

                '    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_GetDetailsByReqIdAndProductId")
                '    sp1.Command.AddParameter("@ReqId", ReqId, DbType.String)
                '    sp1.Command.AddParameter("@ProductId", CInt(lblProductId.Text), DbType.Int32)
                '    Dim dr1 As SqlDataReader = sp1.GetReader
                '    If dr1.Read() Then
                '        chkSelect.Checked = True
                '        txtQty.Text = dr1("AID_QTY")
                '        If txtQty.Text = "NULL" Or txtQty.Text = "" Or txtQty.Text = 0 Then
                '            row.Visible = False
                '        Else
                '            row.Visible = True
                '        End If
                '    Else
                '        row.Visible = False
                '    End If
                'Next
            Else
                lblMsg.Text = "No such requisition found."
            End If
        End If
    End Sub

    Public Sub BindDocuments(ByVal ReqId As String)
        Dim dtDocs As New DataTable("Documents")
        Dim param(0) As SqlParameter
        'param = New SqlParameter(1) {}
        param(0) = New SqlParameter("@AMG_REQID", SqlDbType.NVarChar, 50)
        param(0).Value = ReqId
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("AST_Level1_DOCS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdDocs.DataSource = ds
            grdDocs.DataBind()
            tblGridDocs.Visible = True
            lblDocsMsg.Text = ""
        Else
            tblGridDocs.Visible = False
            lblMsg.Visible = True
            lblDocsMsg.Text = "No Documents Available"
        End If
        dtDocs = Nothing
    End Sub


    Private Sub grdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.ItemCommand
        If e.CommandName = "Download" Then
            Dim ID = grdDocs.DataKeys(e.Item.ItemIndex)
            e.Item.BackColor = Drawing.Color.LightSteelBlue
            Dim filePath As String
            filePath = grdDocs.Items(e.Item.ItemIndex).Cells(1).Text
            filePath = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & filePath
            Response.ContentType = "application/octet-stream"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "UploadFiles\", "") & """")
            Response.WriteFile(filePath)
            'Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If
    End Sub
    Private Sub getassetsubcategory(ByVal assetcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    Private Sub getbrandbycatsubcat(ByVal assetcode As String, ByVal assetsubcat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", assetsubcat, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--Select--")
        ddlAstBrand.Items.Insert(0, New ListItem("No Brand", "No Brand"))
    End Sub
    'Private Sub BindGrid()
    '    'Dim CatId As Integer = 0
    '    'Integer.TryParse(ddlAstCat.SelectedItem.Value, CatId)
    '    'gvItems.DataSource = ProductController.GetByCategoryID_DataSet(CatId)
    '    'gvItems.DataBind()


    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@ReqId", SqlDbType.NVarChar, 200)
    '    param(0).Value = Request.QueryString("RID")
    '    'ObjSubsonic.BindGridView(gvItems, "GET_AMGITEM_REQUISITION", param)


    '    ObjSubsonic.BindGridView(gvItems, "GET_AMGITEM_VIEW_CAPITAL_ASSET_REQUISITION", param)
    '    pnlItems.Visible = True
    'End Sub

    Dim dsCIRGrid As New DataSet()
    Private Sub BindGrid()
        'Dim tickedcount = 0
        Dim ReqId As String = Request("RID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMGITEM_VIEW_CAPITAL_ASSET_REQUISITION")
        sp.Command.AddParameter("@AST_MD_CATID", CatId, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", asstsubcat, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", "ALL", DbType.String)
        sp.Command.AddParameter("@AST_MD_MODEL_ID", "ALL", DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQID", ReqId, DbType.String)
        dsCIRGrid = sp.GetDataSet
        gvItems.DataSource = dsCIRGrid

        gvItems.DataBind()

    End Sub
    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged

        getmakebycatsubcat()
    End Sub
    Protected Sub ddlAstSubCatSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstSubCat.SelectedIndexChanged

        getassetsubcategory()
    End Sub
    Private Sub getassetsubcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlAstSubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--Select--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    Private Sub BindUsers(ByVal aur_id As String)


        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = aur_id
        ObjSubsonic.Binddropdown(ddlEmp, "AMT_BINDUSERS_SP_Raisedby", "NAME", "AUR_ID", param)

        Dim mDS As New DataSet
        mDS = ObjSubsonic.GetSubSonicDataSet("AMT_BINDUSERS_SP_Raisedby", param)

        Dim li As ListItem = ddlEmp.Items.FindByValue(mDS.Tables(0).Rows(0).Item("AUR_ID"))
        If Not li Is Nothing Then
            li.Selected = True
        End If

        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMT_GetUserDtls_SP")
        'ddlEmp.DataSource = sp.GetReader
        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "AUR_ID"
        'ddlEmp.DataBind()
        'ddlEmp.Items.Insert(0, New ListItem("--Select Employee--", "0"))





        'Dim li As ListItem = ddlEmp.Items.FindByValue(Session("UID"))
        'If Not li Is Nothing Then
        '    li.Selected = True
        'End If
    End Sub
    Private Sub BindCategories()
        'GetChildRows("0")
        'ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIESS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, "--Select--")
    End Sub

    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        ddlastCat.DataSource = sp.GetDataSet()
        ddlastCat.DataTextField = "VT_TYPE"
        ddlastCat.DataValueField = "VT_CODE"
        ddlastCat.DataBind()
        ' ddlAstCat.SelectedIndex = If(ddlAstCat.Items.Count > 1, 0, 0)
    End Sub

    'Private Sub GetChildRows(ByVal i As String)
    '    Dim str As String = ""
    '    Dim id
    '    If i = "0" Then
    '        id = CType(i, Integer)
    '    Else
    '        Dim id1 As Array = Split(i, "~")
    '        str = id1(0).ToString & "  --"
    '        id = CType(id1(1), Integer)
    '    End If
    '    Dim objConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
    '    Dim da As New SqlDataAdapter("select CategoryId,CategoryName,ParentId FROM CSK_Store_Category WHERE ParentId = " & id, objConn)
    '    Dim ds As New DataSet
    '    da.Fill(ds)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        For Each dr As DataRow In ds.Tables(0).Rows
    '            Dim j As Integer = CType(dr("CategoryId"), Integer)
    '            If id = 0 Then
    '                str = ""
    '            End If
    '            Dim li As ListItem = New ListItem(str & dr("CategoryName").ToString, dr("CategoryId"))
    '            ddlAstCat.Items.Add(li)
    '            GetChildRows(str & "~" & j)
    '        Next
    '    End If
    'End Sub

    'Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
    '    If ddlAstCat.SelectedIndex > 0 Then
    '        Dim CatId As Integer = 0
    '        Integer.TryParse(ddlAstCat.SelectedItem.Value, CatId)
    '        gvItems.DataSource = ProductController.GetByCategoryID_DataSet(CatId)
    '        gvItems.DataBind()
    '        pnlItems.Visible = True
    '    End If
    'End Sub
    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        Dim reqqty As Integer = txtQty.Text
        Dim apprqty As Integer = txtaprvqty.Text
        If reqqty < apprqty Then
            lblMsg.Text = "You enter more than requested quantity"
            Exit Sub
        End If
        Dim Company_Id As Integer = Session("COMPANYID")
        Dim ReqId As String = GetRequestId()
        Dim strASSET_LIST As New ArrayList
        UpdateData(ReqId, Trim(txtRMRemarks.Text))
        'Dim VT_Code As String
        'Dim Ast_SubCat_Code As String
        'Dim manufactuer_code As String
        'Dim Ast_Md_code As String
        'DeleteRequistionItems(ReqId)
        'Dim i As Integer = 0
        'For Each row As GridViewRow In gvItems.Rows
        '    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
        '    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
        '    Dim lblSno As Label = DirectCast(row.FindControl("srno"), Label)
        '    Dim lblProductname As Label = DirectCast(row.FindControl("lblProductname"), Label)
        '    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
        '    Dim txtEstCost As TextBox = DirectCast(row.FindControl("txtEstCost"), TextBox)
        '    Dim lbl_vt_code As Label = DirectCast(row.FindControl("lbl_vt_code"), Label)
        '    Dim lbl_ast_subcat_code As Label = DirectCast(row.FindControl("lbl_ast_subcat_code"), Label)
        '    Dim lbl_manufactuer_code As Label = DirectCast(row.FindControl("lbl_manufactuer_code"), Label)
        '    Dim lbl_ast_md_code As Label = DirectCast(row.FindControl("lbl_ast_md_code"), Label)

        '    VT_Code = lbl_vt_code.Text
        '    Ast_SubCat_Code = lbl_ast_subcat_code.Text
        '    manufactuer_code = lbl_manufactuer_code.Text
        '    Ast_Md_code = lbl_ast_md_code.Text
        '    If chkSelect.Checked Then
        '        'InsertDetails(ReqId, lblProductId.Text, CInt(Trim(txtQty.Text)), CInt(Trim(txtEstCost.Text)))
        '        InsertDetails(ReqId, lblProductId.Text, CInt(Trim(txtQty.Text)), CInt(Trim(txtEstCost.Text)), VT_Code, Ast_SubCat_Code, manufactuer_code, Ast_Md_code, Company_Id, i)
        '        'strASSET_LIST.Insert(i, lblProductId.Text & "," & lblProductname.Text & "," & txtQty.Text)
        '        i += 1
        '    End If

        'Next


        'Dim MailTemplateId As Integer
        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetRequisitionRMApproval"))


        ''**** While approving by RM 3 mail goes like ****
        ''1. Request Approved copy goes to request raised person.
        ''2. One copy for RM.
        ''3. RM Approval copy to Admin

        ''True -- indicates for Approval mails.
        'getRequestDetails(MailTemplateId, True)

        Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)
    End Sub


    Private Sub getRequestDetails(ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        Dim ReqId As String = GetRequestId()
        Dim strASSET_LIST As New ArrayList
        Dim i As Integer = 0
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim lblProductname As Label = DirectCast(row.FindControl("lblProductname"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            If chkSelect.Checked Then
                strASSET_LIST.Insert(i, lblProductId.Text & "," & lblProductname.Text & "," & txtQty.Text)
            End If
            i += 1
        Next
        'SendMail(ReqId, strASSET_LIST, Session("uid"), txtRMRemarks.Text, MailStatus, App_Rej_status)
    End Sub


    Private Sub SendMail(ByVal strReqId As String, ByVal AstList As ArrayList, ByVal Req_raised As String, ByVal strRemarks As String, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        Dim st As String = ""
        'lblMsg.Text = st
        Try
            Dim to_mail As String = String.Empty
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim strEmail As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strKnownas As String = String.Empty
            Dim strFMG As String = String.Empty
            Dim strBUHead As String = String.Empty
            Dim strRR As String = String.Empty
            Dim strUKnownas As String = String.Empty
            Dim strSubject As String = String.Empty
            Dim dsGET_ASSET_REQ_EMAILS As New DataSet

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strReqId

            dsGET_ASSET_REQ_EMAILS = objsubsonic.GetSubSonicDataSet("GET_ASSET_REQ_EMAILS", param)

            If dsGET_ASSET_REQ_EMAILS.Tables(0).Rows.Count > 0 Then
                strRR = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("EMAIL")
                strUKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RAISED_AUR_KNOWN_AS")
                strRM = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_EMAIL")
                strKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_KNOWN_AS")
            End If


            '----------- Get Mail Content from MailMaster Table ---------------------
            Dim strAstList As String = String.Empty


            Dim dsMail_content As New DataSet
            Dim paramMail_content(0) As SqlParameter
            paramMail_content(0) = New SqlParameter("@Mail_id", SqlDbType.Int)
            paramMail_content(0).Value = MailStatus
            dsMail_content = objsubsonic.GetSubSonicDataSet("GET_MAILMASTER_ID", paramMail_content)
            If dsMail_content.Tables(0).Rows.Count > 0 Then
                body = dsMail_content.Tables(0).Rows(0).Item("MAIL_BODY")
                strSubject = dsMail_content.Tables(0).Rows(0).Item("MAIL_SUBJECT")
            End If

            strAstList = "<table width='200' border='1'><tr><td><strong>Asset Name </strong></td><td><strong>Quantity</strong></td></tr>"
            For i As Integer = 0 To AstList.Count - 1
                Dim p1
                p1 = AstList.Item(i).Split(",")

                strAstList = strAstList & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(2) & "</td></tr>"
            Next
            strAstList = strAstList & "</table> "


            Dim strApproveList As String = String.Empty
            Dim strRejectList As String = String.Empty

            Dim AppLink As String = String.Empty
            AppLink = ConfigurationManager.AppSettings("AppLink").ToString


            'strApproveList = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href='" & AppLink & "?rid=" & strReqId & "'> Click to Approve </a></td></tr></table>"

            'strRejectList = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href='" & AppLink & "?rid=" & strReqId & "'>  Click to Reject </a></td></tr></table>"
            body = body.Replace("@@RAISED_USER", strUKnownas)
            body = body.Replace("@@REQ_ID", strReqId)
            body = body.Replace("@@Astlist", strAstList)
            body = body.Replace("@@REMARKS", strRemarks)
            body = body.Replace("@@RM", strKnownas)


            'If App_Rej_status = True Then
            '    '1. Request Approved copy goes to request raised person.
            '    Insert_AmtMail(body, strRR, strSubject, strRM)
            '    '2. One copy for RM.
            '    Insert_AmtMail(body, strRM, strSubject, "")
            '    '3. RM Approval copy to Admin
            '    Insert_AmtMail(body, strRR, strSubject, strRM)
            'Else
            '    '1. Request Rejected copy goes to request raised person.
            '    Insert_AmtMail(body, strRR, strSubject, strRM)
            '    '2. One copy for RM.
            '    Insert_AmtMail(body, strRM, strSubject, "")
            'End If

        Catch ex As Exception

            Throw (ex)
        End Try


    End Sub

    Private Sub Insert_AmtMail(ByVal strBody As String, ByVal strEMAIL As String, ByVal strSubject As String, ByVal strCC As String)
        Dim paramMail(7) As SqlParameter
        paramMail(0) = New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
        paramMail(0).Value = "Abcd"
        paramMail(1) = New SqlParameter("@VC_MSG", SqlDbType.NVarChar, 50)
        paramMail(1).Value = strBody
        paramMail(2) = New SqlParameter("@vc_mail", SqlDbType.NVarChar, 50)
        paramMail(2).Value = strEMAIL
        paramMail(3) = New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 50)
        paramMail(3).Value = strSubject
        paramMail(4) = New SqlParameter("@DT_MAILTIME", SqlDbType.NVarChar, 50)
        paramMail(4).Value = getoffsetdatetime(DateTime.Now)
        paramMail(5) = New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
        paramMail(5).Value = "Request Submitted"
        paramMail(6) = New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
        paramMail(6).Value = "Normal Mail"
        paramMail(7) = New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 50)
        paramMail(7).Value = strCC
        ObjSubsonic.GetSubSonicExecute("USP_SPACE_INSERT_AMTMAIL", paramMail)
    End Sub


    Private Sub UpdateData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisition_UpdateByRM")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CatId", ddlAstCat.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@STATUS", 1004, DbType.Int32)
        sp.Command.AddParameter("@AIR_ITEM_MOD", ddlAstModel.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CompanyId", Session("CompanyId"), DbType.String)
        sp.Command.AddParameter("@QNTY", txtaprvqty.Text, DbType.Int32)
        sp.Command.AddParameter("@EST_COST", txtaprvcost.Text, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As String, ByVal Qty As Integer, ByVal EstCost As Integer, ByVal VT_CODE As String,
        ByVal AST_SUBCAT_CODE As String, ByVal manufactuer_code As String, ByVal AST_MD_CODE As String, ByVal Company_Id As Integer, ByVal id As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_updatebyrm")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.Command.AddParameter("@EST_COST", EstCost, DbType.Int32)
        sp.Command.AddParameter("@ITEM_TYPE", VT_CODE, DbType.String)
        sp.Command.AddParameter("@ITEM_SUBCAT", AST_SUBCAT_CODE, DbType.String)
        sp.Command.AddParameter("@ITEM_BRD", manufactuer_code, DbType.String)
        sp.Command.AddParameter("@ITEM_MOD", AST_MD_CODE, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Company_Id, DbType.String)
        sp.Command.AddParameter("@delsno", id, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    'Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As String, ByVal Qty As Integer, ByVal EstCost As Integer)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_updatebyrm")
    '    sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
    '    sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
    '    sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
    '    sp.Command.AddParameter("@EST_COST", EstCost, DbType.Int32)
    '    sp.ExecuteScalar()
    'End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Dim MailTemplateId As Integer
        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetRequisitionRMReject"))
        'getRequestDetails(MailTemplateId, False)
        If Not Page.IsValid Then
            Exit Sub
        End If
        CancelData(GetRequestId, Trim(txtRMRemarks.Text))
        Response.Redirect("frmAssetThanks.aspx?RID=" + GetRequestId())
    End Sub
    Private Sub CancelData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CAP_AssetRequisition_APP_REJ_BY_L2")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CatId", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@STATUS", 1005, DbType.Int32)
        sp.Command.AddParameter("@AIR_ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CompanyId", Session("CompanyId"), DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub DeleteRequistionItems(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_DeleteByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.ExecuteScalar()
    End Sub
    

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmRMApproval.aspx")
    End Sub
    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        ddlAstModel.Items.Insert(0, New ListItem("No Model", "No Model"))
        'ddlAstModel.Items.Insert(0, "--All--")
    End Sub
End Class
