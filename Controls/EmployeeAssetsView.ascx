<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EmployeeAssetsView.ascx.vb"
    Inherits="Controls_EmployeeAssets" %>
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Employee Asset List
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td align="left" width="100%" colspan="3">
                <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Employee Asset List</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server"
                    CssClass="clsMessage" ForeColor="" />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                <asp:Panel ID="pnlcontainer" runat="server" Width="100%" GroupingText="Approved Requests">
                    <table id="tabcontainer" runat="server" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left">
                                <asp:GridView ID="gvitems" runat="Server" EmptyDataText="Sorry!No Approved Asset Requests Found"
                                    AllowPaging="true" AllowSorting="true" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblassetcode" runat="server" Text='<%#Eval("AAT_AST_CODE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset">
                                            <ItemTemplate>
                                                <asp:Label ID="lblasset" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Remarks">
                                            <ItemTemplate>
                                                <asp:Label ID="lblemprmrks" runat="server" Text='<%#Eval("REMARKS") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Posted On">
                                            <ItemTemplate>
                                                <asp:Label ID="lblposton" runat="server" Text='<%#Eval("REQUESTED_DATE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="RM Remarks">
                                            <ItemTemplate>
                                                <asp:Label ID="lblrmrmrks" runat="server" Text='<%#Eval("RM_REMARKS") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Posted On">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRmPostOn" runat="server" Text='<%#Eval("RM_POSTED_ON") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                 <asp:Panel ID="panelrejected" runat="server" Width="100%" GroupingText="Rejected Requests">
                    <table id="tabrejected" runat="server" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left">
                                <asp:GridView ID="gvrejected" runat="Server" EmptyDataText="Sorry!No Rejected Asset Requests Found"
                                    AllowPaging="true" AllowSorting="true" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblrassetcode" runat="server" Text='<%#Eval("AAT_AST_CODE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset">
                                            <ItemTemplate>
                                                <asp:Label ID="lblrasset" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Remarks">
                                            <ItemTemplate>
                                                <asp:Label ID="lblremprmrks" runat="server" Text='<%#Eval("REMARKS") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Posted On">
                                            <ItemTemplate>
                                                <asp:Label ID="lblrposton" runat="server" Text='<%#Eval("REQUESTED_DATE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="RM Remarks">
                                            <ItemTemplate>
                                                <asp:Label ID="lblrrmrmrks" runat="server" Text='<%#Eval("RM_REMARKS") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Posted On">
                                            <ItemTemplate>
                                                <asp:Label ID="lblrRmPostOn" runat="server" Text='<%#Eval("RM_POSTED_ON") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
