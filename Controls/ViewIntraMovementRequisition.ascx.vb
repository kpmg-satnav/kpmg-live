Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class Controls_ViewIntraMovementRequisition
    Inherits System.Web.UI.UserControl

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub

    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ASSET_GET_INTRAREQUISITIONS")
            gvgriditems.DataSource = sp.GetDataSet()
            gvgriditems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

 
    Protected Sub gvgriditems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvgriditems.PageIndexChanging
        gvgriditems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub


    Protected Sub gvgriditems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvgriditems.RowCommand
        If e.CommandName = "ViewDetails" Then
            Response.Redirect("IntraMovementRequisitionDTLS.aspx?Req_id=" & e.CommandArgument)
        End If
    End Sub

End Class
