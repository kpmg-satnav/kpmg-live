Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_InterMovementRequisition1
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Dim AAS_SNO, AAS_AAT_CODE, AAS_BDG_ID, AAS_FLR_ID, LCM_CODE, LCM_NAME, AAT_EMP_ID As String
    Dim Twr_code, Flr_code As String
    Dim strLCM_CODE As String
    Dim strTWR_CODE As String
    Dim strfloor As String
    Dim StrEmp As String
    Dim Strast As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        lblMsg.Text = ""
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                BindLocation()
                getassetcategory()
                BindDestLoactions()
                'BindAssets()
            End If
        End If
    End Sub

    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        ddlAssetCategory.Items.Insert(0, "--Select--")
        ddlAssetCategory.SelectedIndex = 1

        ddlAstSubCat.Items.Insert(0, New ListItem("--ALL--", "ALL"))
        ddlAstBrand.Items.Insert(0, New ListItem("--ALL--", "ALL"))
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Protected Sub ddlAssetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()
        getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
        DisableDestinationLoc()
        ddlAstBrand.Items.Insert(0, New ListItem("--ALL--", "ALL"))
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ddlAstSubCat.Items.Clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", categorycode, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        ddlModel.Items.Clear()
        'ddlAstBrand.Items.Clear()
        getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
        DisableDestinationLoc()
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Private Sub getbrandbycatsubcat(ByVal category As String, ByVal subcategory As String)
        ddlAstBrand.Items.Clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        'ddlModel.Items.Clear()
        getmakebycatsubcat()
        DisableDestinationLoc()
    End Sub

    Private Sub getmakebycatsubcat()
        ddlModel.Items.Clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub


    'Protected Sub TextBox1_BranchM_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
    '    'ddlModel.Items.Clear()
    '    'getmakebycatsubcat()
    '    BranchManager()
    'End Sub



    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        DisableDestinationLoc()

    End Sub

    Private Sub DisableDestinationLoc()
        Try
            If ddlLocation.SelectedIndex >= 0 Then
                ddlDestLoc.Items.FindByValue(ddlLocation.SelectedValue).Attributes.Add("disabled", "disabled")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub



    Private Sub BindDestLoactions()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_LOCATIONS")
            sp.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            ddlDestLoc.DataSource = sp.GetDataSet()
            ddlDestLoc.DataTextField = "LCM_NAME"
            ddlDestLoc.DataValueField = "LCM_CODE"
            ddlDestLoc.DataBind()
            ddlDestLoc.Items.Insert(0, New ListItem("--Select--", "-1"))
            ' BranchManager()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Protected Sub ddlDestLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDestLoc.SelectedIndexChanged
        BranchManager(ddlDestLoc.SelectedItem.Value)

    End Sub

    Private Sub BranchManager(ByVal loc As String)
        'ddlModel.Items.Clear()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_BRANCH_MANAGER_BYLOCATION")
        'sp.Command.AddParameter("@AUR_LOCATION", ddlDestLoc.SelectedItem.Value, DbType.String)
        Txtmanager.Text = ""
        Dim Manager As String
        Dim dr As SqlDataReader
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_LOCATION", SqlDbType.VarChar, 50)
        param(0).Value = loc
        dr = ObjSubsonic.GetSubSonicDataReader("AST_BRANCH_MANAGER_BYLOCATION", param)
        If dr.Read Then
            Manager = dr(0)
            If (Manager = "") Then
                lblMsg.Text = "No Person is availble For Receiving"
            End If
            Txtmanager.Text = Manager
        Else
            lblMsg.Text = "No Person is availble For Receiving"
        End If
        If dr.IsClosed = False Then '
            dr.Close()
        End If



    End Sub
    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCTION")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select--", "-1"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CAPASSETS_FOR_INTER_MOVEMENT")
            sp.Command.AddParameter("@LOCTN_CODE", ddlLocation.SelectedValue, DbType.String)
            sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedValue, DbType.String)
            sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedValue, DbType.String)
            sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedValue, DbType.String)
            sp.Command.AddParameter("@AST_MODEL", ddlModel.SelectedValue, DbType.String)
            'sp.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            grdAssets.DataSource = ds
            grdAssets.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        BindGrid()
        btnAstReq.Visible = IIf(grdAssets.Rows.Count > 0, True, False)
        txtremrks.Visible = IIf(grdAssets.Rows.Count > 0, True, False)
    End Sub

    Private Sub SendMail(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_SEND_MAIL_ASSET_INTER_MVMT_REQUISITION")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@MODE", "INSERT", DbType.String)
        sp.Execute()
    End Sub

    Protected Sub btnAstReq_Click(sender As Object, e As EventArgs) Handles btnAstReq.Click
        If Not Page.IsValid Then
            Exit Sub
        End If

        Dim AstParid As String

        'PARENT
        For Each row As GridViewRow In grdAssets.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked Then
                Dim assetcode As Label = DirectCast(row.FindControl("lblAstCode"), Label)
                Dim assetname As Label = DirectCast(row.FindControl("lblAssetname"), Label)
                Dim reqid As String = "MMR" + "/" + ddlLocation.SelectedValue + "/" + Session("uid") + "/"
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_ASSET_MVMT_REQUISITION")
                sp.Command.AddParameter("@CREATEDBY", Session("uid"), DbType.String)
                sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
                sp.Command.AddParameter("@REQID", reqid, DbType.String)
                sp.Command.AddParameter("@MMR_LRSTATUS_ID", 1, DbType.Int32)
                sp.Command.AddParameter("@FROMLOC", ddlLocation.SelectedValue, DbType.String)
                sp.Command.AddParameter("@DESTLOC", ddlDestLoc.SelectedValue, DbType.String)
                sp.Command.AddParameter("@ASSETCODE", assetcode.Text, DbType.String)
                sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
                sp.Command.AddParameter("@MMR_RECVD_BY", Txtmanager.Text, DbType.String)
                AstParid = sp.ExecuteScalar()

            End If
        Next

        'CHILD
        For Each row As GridViewRow In grdAssets.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked Then
                Dim assetcode As Label = DirectCast(row.FindControl("lblAstCode"), Label)
                Dim assetname As Label = DirectCast(row.FindControl("lblAssetname"), Label)
                Dim astComments As TextBox = DirectCast(row.FindControl("txtcomments"), TextBox)
                ' Dim splitAssetname As String() = assetname.Text.Split("/")
                ' splitAssetname(0) = ddlDestLoc.SelectedValue
                ' Dim modyAssetName As String = String.Join("/", splitAssetname.ToArray())

                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & ".AM_USP_MMT_INTRA_MVMT_REQ_Insert" & "")
                sp1.Command.AddParameter("@MMR_REQ_ID", AstParid, DbType.String)
                'sp1.Command.AddParameter("@ASSETNAME", modyAssetName, DbType.String)
                sp1.Command.AddParameter("@ASSETCODE", assetcode.Text, DbType.String)
                sp1.Command.AddParameter("@RAISEDBY", Session("uid"), DbType.String)
                sp1.Command.AddParameter("@COMMENT", astComments.Text, DbType.String)
                sp1.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
                sp1.ExecuteScalar()
            End If
        Next

        SendMail(AstParid)
        Response.Redirect("frmAssetThanks.aspx?RID=intermovement&reqid=" + AstParid + "")
    End Sub

End Class
