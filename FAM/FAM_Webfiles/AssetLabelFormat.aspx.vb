﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Imports System.Data.OleDb
Imports System.IO
Imports System.Collections.Generic

Partial Class FAM_FAM_Webfiles_AssetLabelFormat
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ''ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End Using
        End If

        If Not IsPostBack Then
            lblMsg.Visible = False
            EditData()

        End If
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If CheckLoc.Checked = False And CheckCat.Checked = False And CheckSubcat.Checked = False And CheckBrand.Checked = False And CheckModel.Checked = False Then
            lblMsg.Visible = True
            lblMsg1.Visible = False
            LabelFormat.Visible = False

            lblMsg.Text = "Please Select Atleast One Value"
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_BSM_INSERT")
            sp.Command.AddParameter("@PREFIX", txtcmpny.Text, DbType.String)
            sp.Command.AddParameter("@CITY", CheckCity.Checked, DbType.Boolean)
            sp.Command.AddParameter("@LOC", CheckLoc.Checked, DbType.Boolean)
            sp.Command.AddParameter("@CAT", CheckCat.Checked, DbType.Boolean)
            sp.Command.AddParameter("@SUBCAT", CheckSubcat.Checked, DbType.Boolean)
            sp.Command.AddParameter("@BRAND", CheckBrand.Checked, DbType.Boolean)
            sp.Command.AddParameter("@MODEL", CheckModel.Checked, DbType.Boolean)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp.ExecuteScalar()
            lblMsg.Visible = True
            lblMsg1.Visible = False
            LabelFormat1.Visible = False
            bindlabel()
            lblMsg.Text = "Asset Label Inserted Successfully"
        End If
    End Sub

    Protected Sub btnView_Click(sender As Object, e As EventArgs) Handles btnView.Click
        lblMsg.Text = ""
        bindlabel()

    End Sub
    Private Sub bindlabel()
        Dim AstCode As String = "PrefixText" + "/" + "CityName" + "/" + "LocationName" + "/" + "Category" + "/" + "SubCategoryName" + "/" + "BrandName" + "/" + "Model" + "/" + "00001"
        'If CheckLoc.Checked = False And CheckCat.Checked = False And CheckSubcat.Checked = False And CheckBrand.Checked = False And CheckModel.Checked = False Then
        '    lblMsg.Visible = True
        '    LabelFormat.Visible = False
        '    lblMsg.Text = "Please Select Atleast One Value"
        'Else
        If txtcmpny.Text = "" Then
            AstCode = Replace(AstCode, "PrefixText/", "")
        End If
        If CheckCity.Checked = False Then
            AstCode = Replace(AstCode, "CityName/", "")
        End If
        If CheckLoc.Checked = False Then
            AstCode = Replace(AstCode, "LocationName/", "")
        End If

        If CheckCat.Checked = False Then
            AstCode = Replace(AstCode, "Category/", "")
        End If
        If CheckSubcat.Checked = False Then
            AstCode = Replace(AstCode, "SubCategoryName/", "")
        End If
        If CheckBrand.Checked = False Then
            AstCode = Replace(AstCode, "BrandName/", "")
        End If
        If CheckModel.Checked = False Then
            AstCode = Replace(AstCode, "Model/", "")
        End If

        LabelFormat.Visible = True
        LabelFormat.Text = AstCode
        ' End If

    End Sub

    Private Sub EditData()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_BSM_EDIT")
        Dim dt As DataTable
        dt = sp.GetDataSet().Tables(0)
        If dt.Rows.Count = 0 Then
            txtcmpny.Text = ""
            CheckCity.Checked = False
            CheckLoc.Checked = False
            CheckSubcat.Checked = False
            CheckCat.Checked = False
            CheckBrand.Checked = False
            CheckModel.Checked = False
            lblMsg.Visible = False

        ElseIf dt.Rows(0).Item("AST_BSM_STATUS") = "1" Then
            If dt.Rows(0).Item("AST_BSM_PREFIX") <> "" Then
                txtcmpny.Text = dt.Rows(0).Item("AST_BSM_PREFIX")
            End If
            If dt.Rows(0).Item("AST_BSM_CITY") = "1" Then
                CheckCity.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_LOCATION") = "1" Then
                CheckLoc.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_CAT") = "1" Then
                CheckCat.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_SUB_CAT") = "1" Then
                CheckSubcat.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_BRAND") = "1" Then
                CheckBrand.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_MODEL") = "1" Then
                CheckModel.Checked = True
            End If
            bindlabel()
        End If

        If dt.Rows(0).Item("AST_BSM_STATUS") = 2 Then
            If dt.Rows(0).Item("AST_BSM_PREFIX") <> "" Then
                txtcmpny1.Text = dt.Rows(0).Item("AST_BSM_PREFIX")
            End If
            If dt.Rows(0).Item("AST_BSM_CITY") = "1" Then
                CheckCity1.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_LOCATION") = "1" Then
                CheckLocation.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_CAT") = "1" Then
                CheckCategory.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_SUB_CAT") = "1" Then
                CheckSubCategory.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_BRAND") = "1" Then
                CheckBrand1.Checked = True
            End If
            If dt.Rows(0).Item("AST_BSM_MODEL") = "1" Then
                CheckModel1.Checked = True
            End If
            bindlabel1()
        End If

    End Sub

    Protected Sub btnView1_Click(sender As Object, e As EventArgs) Handles btnView1.Click
        lblMsg1.Text = ""
        bindlabel1()

    End Sub
    Private Sub bindlabel1()
        Dim AstCode As String = "PrefixText" + "/" + "CityName" + "/" + "LocationName" + "/" + "Category" + "/" + "SubCategoryName" + "/" + "BrandName" + "/" + "Model" + "/"
        'If CheckLoc.Checked = False And CheckCat.Checked = False And CheckSubcat.Checked = False And CheckBrand.Checked = False And CheckModel.Checked = False Then
        '    lblMsg1.Visible = True
        '    LabelFormat.Visible = False
        '    lblMsg1.Text = "Please Select Atleast One Value"
        'Else
        If txtcmpny1.Text = "" Then
            AstCode = Replace(AstCode, "PrefixText/", "")
        End If
        If CheckCity1.Checked = False Then
            AstCode = Replace(AstCode, "CityName/", "")
        End If
        If CheckLocation.Checked = False Then
            AstCode = Replace(AstCode, "LocationName/", "")
        End If
        If CheckCategory.Checked = False Then
            AstCode = Replace(AstCode, "Category/", "")

        End If

        If CheckSubCategory.Checked = False Then
            AstCode = Replace(AstCode, "SubCategoryName/", "")
        End If
        If CheckBrand1.Checked = False Then
            AstCode = Replace(AstCode, "BrandName/", "")
        End If
        If CheckModel1.Checked = False Then
            AstCode = Replace(AstCode, "Model/", "")
        End If

        LabelFormat1.Visible = True
        LabelFormat1.Text = AstCode
        ' End If

    End Sub
    Protected Sub btnSubmit1_Click(sender As Object, e As EventArgs) Handles btnSubmit1.Click
        If CheckCity1.Checked = False And CheckLocation.Checked = False And CheckCategory.Checked = False And CheckSubCategory.Checked = False And CheckBrand1.Checked = False And CheckModel1.Checked = False Then
            lblMsg1.Visible = True
            lblMsg.Visible = False
            LabelFormat1.Visible = False
            LabelFormat.Visible = False
            lblMsg1.Text = "Please Select Atleast One Value"
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_BSM_INSERT_1")
            sp.Command.AddParameter("@PREFIX", txtcmpny1.Text, DbType.String)
            sp.Command.AddParameter("@CITY", CheckCity1.Checked, DbType.Boolean)
            sp.Command.AddParameter("@LOC", CheckLocation.Checked, DbType.Boolean)
            sp.Command.AddParameter("@CAT", CheckCategory.Checked, DbType.Boolean)
            sp.Command.AddParameter("@SUBCAT", CheckSubCategory.Checked, DbType.Boolean)
            sp.Command.AddParameter("@BRAND", CheckBrand1.Checked, DbType.Boolean)
            sp.Command.AddParameter("@MODEL", CheckModel1.Checked, DbType.Boolean)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp.ExecuteScalar()
            lblMsg1.Visible = True
            lblMsg.Visible = False
            LabelFormat.Visible = False
            bindlabel1()
            lblMsg1.Text = "Asset Label Inserted Successfully"
        End If
    End Sub
End Class
