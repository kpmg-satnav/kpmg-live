Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_frmGetDipreciationCost
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                BindLocation()
                ddlLocation.Items.Insert(1, New ListItem("--All--", "1"))
                ddlTower.Items.Clear()
                ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlTower.SelectedValue = 0
                ddlFloor.Items.Clear()
                ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlFloor.SelectedValue = 0
            End If
        End If
    End Sub

    Private Sub BindLocation()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
    End Sub


    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex = 1 Then

                ddlTower.Items.Clear()
                ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlTower.Items.Insert(1, New ListItem("--All--", "1"))
                ddlTower.SelectedValue = 0

                ddlFloor.Items.Clear()
                ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlFloor.Items.Insert(1, New ListItem("--All--", "1"))
                ddlFloor.SelectedValue = 0


            ElseIf ddlLocation.SelectedIndex > 1 Then
                BindTower()
            Else
                ddlTower.Items.Clear()
                ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlTower.SelectedValue = 0
                ddlFloor.Items.Clear()
                ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlFloor.SelectedValue = 0
                gvAstList.DataSource = Nothing
                gvAstList.DataBind()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Private Sub BindTower()
        Try
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
            param(0).Value = ddlLocation.SelectedItem.Value
            ObjSubsonic.Binddropdown(ddlTower, "GET_LOCTWR", "TWR_NAME", "TWR_CODE", param)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindFloor()
        Try
            ddlFloor.Items.Clear()
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
            param(0).Value = ddlLocation.SelectedItem.Value
            param(1) = New SqlParameter("@dummy1", SqlDbType.NVarChar, 100)
            param(1).Value = ddlTower.SelectedItem.Value
            ObjSubsonic.Binddropdown(ddlFloor, "GET_TWRFLR", "FLR_NAME", "FLR_CODE", param)


            ddlFloor.Items.Insert(1, New ListItem("--All--", "1"))
            ddlFloor.ClearSelection()
            ddlFloor.Items(0).Selected = True

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            If ddlTower.SelectedIndex = 0 Then
                lblMsg.Text = ""
                BindFloor()
            ElseIf ddlTower.SelectedIndex > 0 Then
                lblMsg.Text = ""
                BindFloor()
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlFloor.SelectedValue = 0
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub



    Private Sub BindStockReport()
        Dim Lcm_code As String = ""
        Dim Twr_code As String = ""
        Dim Flr_code As String = ""
        Dim Aim_code As String = ""
        If ddlLocation.SelectedItem.Value = "--Select--" Then
            lblMsg.Text = "Select Location"
            Exit Sub
        ElseIf ddlLocation.SelectedItem.Value = "1" Then
            Lcm_code = ""
            lblMsg.Text = ""
        Else
            lblMsg.Text = ""
            Lcm_code = ddlLocation.SelectedItem.Value

        End If

        If ddlLocation.SelectedItem.Value = "1" Then
            Twr_code = ""
            Flr_code = ""
            lblMsg.Text = ""
        Else
            If ddlTower.SelectedItem.Text = "--Select--" Then
                lblMsg.Text = "Select Tower"
                Exit Sub
            ElseIf ddlTower.SelectedItem.Value = "1" Then
                Twr_code = ""
                Flr_code = ""
            Else
                lblMsg.Text = ""
                Twr_code = ddlTower.SelectedItem.Value
            End If
            If ddlFloor.SelectedItem.Text = "--Select--" Then
                lblMsg.Text = "Select Floor"
                Exit Sub
            ElseIf ddlFloor.SelectedItem.Value = "1" Then
                lblMsg.Text = ""
                Flr_code = ""
            Else
                lblMsg.Text = ""
                Flr_code = ddlFloor.SelectedItem.Value
            End If
        End If

        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = Lcm_code
        param(1) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = Twr_code
        param(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = Flr_code
        ObjSubsonic.BindGridView(gvAstList, "GET_ALLASSETS", param)

       
        If gvAstList.Rows.Count = 0 Then
            gvAstList.DataSource = Nothing
            gvAstList.DataBind()
        End If
    End Sub

    Protected Sub gvAstList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAstList.PageIndexChanging
        gvAstList.PageIndex = e.NewPageIndex
        BindStockReport()
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        BindStockReport()
    End Sub

    Protected Sub gvAstList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAstList.RowDataBound
        For i As Integer = 0 To gvAstList.Rows.Count - 1
            Dim lblAAT_CODE As Label = CType(gvAstList.Rows(i).FindControl("lblAAT_CODE"), Label)
            Dim lblAstDepCost As Label = CType(gvAstList.Rows(i).FindControl("lblAstDepCost"), Label)
            Dim lbllife As Label = CType(gvAstList.Rows(i).FindControl("lbllife"), Label)
            Dim lblRETAILPRICE As Label = CType(gvAstList.Rows(i).FindControl("lblRETAILPRICE"), Label)
            Dim lblsalvage As Label = CType(gvAstList.Rows(i).FindControl("lblsalvage"), Label)
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@AST_CODE", SqlDbType.NVarChar, 200)
            param(0).Value = lblAAT_CODE.Text
            Dim ds As New DataSet
            ds = objsubsonic.GetSubSonicDataSet("GETAST_PRICE", param)
            If ds.Tables(0).Rows.Count > 0 Then
                lblAstDepCost.Text = FormatNumber(ds.Tables(0).Rows(0).Item("DEP_COST"), 2)
                lbllife.Text = ds.Tables(0).Rows(0).Item("AAT_UPT_DT")
                lblRETAILPRICE.Text = FormatNumber(ds.Tables(0).Rows(0).Item("retailPrice"), 2)
                lblsalvage.Text = FormatNumber(ds.Tables(0).Rows(0).Item("SALVAGEVALUE"), 2)
            Else
                lblAstDepCost.Text = ""
            End If
        Next
    End Sub
End Class
