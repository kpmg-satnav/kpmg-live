﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="FAM_FAM_Webfiles_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>Facility Management Services::a-mantra</title>
    <link href="../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/amantra.min.css" rel="stylesheet" />


    <link href="../../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/form-style.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../BootStrapCSS/datepicker.css" />
    
</head>
<body>

  <%--<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display: block; position: static; margin-bottom: 5px; *width: 180px;">
    <li><a tabindex="-1" href="#">Action</a></li>
    <li><a tabindex="-1" href="#">Another action</a></li>
    <li><a tabindex="-1" href="#">Something else here</a></li>
    <li class="divider"></li>
    <li class="dropdown-submenu"> <a tabindex="-1" href="#">More options</a>
        <ul class="dropdown-menu">
            <li><a tabindex="-1" href="#">Second level link</a></li>
            <li><a tabindex="-1" href="#">Second level link</a></li>
            <li><a tabindex="-1" href="#">Second level link</a></li>
            <li><a tabindex="-1" href="#">Second level link</a></li>
            <li><a tabindex="-1" href="#">Second level link</a></li>
        </ul>
    </li>
</ul>
         

<ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu" style="display: block; position: static; margin-bottom: 5px; *width: 180px;">
    <li><a tabindex="-1" href="#">Action</a></li>
    <li><a tabindex="-1" href="#">Another action</a></li>
    <li><a tabindex="-1" href="#">Something else here</a></li>
    <li class="divider"></li>
    <li class="dropdown-submenu pull-left"> <a tabindex="-1" href="#">More options</a>
        <ul class="dropdown-menu">
            <li><a tabindex="-1" href="#">Second level link</a></li>
            <li><a tabindex="-1" href="#">Second level link</a></li>
            <li><a tabindex="-1" href="#">Second level link</a></li>
            <li><a tabindex="-1" href="#">Second level link</a></li>
            <li><a tabindex="-1" href="#">Second level link</a></li>
        </ul>
    </li>
</ul>--%>
  
    

               
        
           
          
                           <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="nav1">

   <%--                 <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu" style="display: block; position: static; margin-bottom: 5px; *width: 180px;">
    <li><a tabindex="-1" href="#">Action</a></li>
    <li><a tabindex="-1" href="#">Another action</a></li>
    <li><a tabindex="-1" href="#">Something else here</a></li>
    <li class="divider"></li>
    <li class="dropdown-submenu pull-left"> <a tabindex="-1" href="#">More options</a>
        <ul class="dropdown-menu">
            <li><a tabindex="-1" href="#">Second level link</a></li>
            <li><a tabindex="-1" href="#">Second level link</a></li>
            <li><a tabindex="-1" href="#">Second level link</a></li>
            <li><a tabindex="-1" href="#">Second level link</a></li>
            <li><a tabindex="-1" href="#">Second level link</a></li>
        </ul>
    </li>
</ul>--%>
                     <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu" style="display: block; position: static; margin-bottom: 5px; *width: 180px;">
                  <li><a tabindex="-1" href="#">Action</a></li>
                  <li><a tabindex="-1" href="#">Another action</a></li>
                  <li><a tabindex="-1" href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-submenu">
                    <a tabindex="-1" href="#">More options</a>
                    <ul class="dropdown-menu">
                      <li><a tabindex="-1" href="#">Second level link</a></li>
                      <li><a tabindex="-1" href="#">Second level link</a></li>
                      <li><a tabindex="-1" href="#">Second level link</a></li>
                      <li><a tabindex="-1" href="#">Second level link</a></li>
                      <li><a tabindex="-1" href="#">Second level link</a></li>
                    </ul>
                  </li>
                </ul>
                    </div>
         
        

    <script src='<%= Page.ResolveUrl("../../BootStrapCSS/Scripts/jquery.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("../../BootStrapCSS/Scripts/bootstrap-select.min.js")%>' type="text/javascript" defer></script>
    <script src='<%= Page.ResolveUrl("../../BootStrapCSS/Scripts/bootstrap.min.js")%>' type="text/javascript" defer></script>
    <script src='<%= Page.ResolveUrl("../../BootStrapCSS/Scripts/bootstrap-datepicker.js")%>' type="text/javascript" defer></script>

</body>
</html>
