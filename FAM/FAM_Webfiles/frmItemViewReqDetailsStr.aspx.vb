Imports System.Data.SqlClient
Imports System.Data

Partial Class FAM_FAM_Webfiles_frmItemViewReqDetailsStr
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        If Not IsPostBack Then
            RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
            BindCategories()
            btnRaiseIndent.Visible = False
            dispdata()
        End If
    End Sub

    Private Sub BindUsers(ByVal AUR_ID As String, ByVal Company As Integer)

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = AUR_ID
        param(1) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
        param(1).Value = Company

        ObjSubsonic.Binddropdown(ddlEmp, "AM_AMT_BINDUSERS_SP", "NAME", "AUR_ID", param)
        Dim li As ListItem = ddlEmp.Items.FindByValue(AUR_ID)
        If Not li Is Nothing Then
            li.Selected = True
        End If
    End Sub

    Private Sub BindCategories()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIESS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, "--Select--")

    End Sub

    Private Sub getassetsubcategory(ByVal assetcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", assetcode, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Private Sub getbrandbycatsubcat(ByVal assetcode As String, ByVal assetsubcat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", assetsubcat, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ' ddlAstBrand.Items.Insert(0, "--Select--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    Private Sub dispdata()

        Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_FOR_ASSETGRIDVR_SMAPPROVAL")
        SP.Command.AddParameter("@Req_id", Request.QueryString("RID"), DbType.String)
        Dim ds As New DataSet
        ds = SP.GetDataSet()
        Dim req_status As Integer = 0
        Dim qty As Integer = 0
        Dim Companyid As Integer
        Companyid = Session("CompanyId")
        If ds.Tables(0).Rows.Count > 0 Then
            lblReqId.Text = ds.Tables(0).Rows(0).Item("AIR_REQ_TS")
            BindUsers(ds.Tables(0).Rows(0).Item("AIR_AUR_ID"), Companyid)
            Dim CatId As String

            CatId = ds.Tables(0).Rows(0).Item("AIR_ITEM_TYPE")
            Dim li As ListItem = ddlAstCat.Items.FindByValue(CStr(CatId))

            If Not li Is Nothing Then
                li.Selected = True
            End If
            ddlAstCat.Enabled = False
            getassetsubcategory(CatId)
            ddlAstSubCat.Enabled = False
            Dim asstsubcat As String = ds.Tables(0).Rows(0).Item("AIR_ITEM_SUBCAT")

            ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True

            getbrandbycatsubcat(CatId, asstsubcat)

            Dim asstbrand As String = ds.Tables(0).Rows(0).Item("AIR_ITEM_BRD")
            ddlAstBrand.Items.FindByValue(asstbrand).Selected = True
            ddlAstBrand.Enabled = False

            getmakebycatsubcat()
            ddlAstModel.Items.FindByValue(ds.Tables(0).Rows(0).Item("AIR_ITEM_MOD")).Selected = True
            ddlAstModel.Enabled = False
            BindLocation()

            ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("AIR_REQ_LOC")).Selected = True
            ddlLocation.Enabled = False

            lblRem.Text = ds.Tables(0).Rows(0).Item("AIR_REMARKS")
            lblRMRem.Text = ds.Tables(0).Rows(0).Item("APPR_REMARKS")

            req_status = ds.Tables(0).Rows(0).Item("AIR_STA_ID")
            qty = ds.Tables(0).Rows(0).Item("AID_AVLBL_QTY")

        End If
        If req_status = 1504 Or req_status = 1511 Then
            btnsubmit.Visible = True
            btnCancel.Enabled = True
        Else
            btnsubmit.Enabled = False
            btnCancel.Enabled = False
            txtSTRem.Enabled = False
        End If

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        ObjSubsonic.BindGridView(gvItems, "AST_GET_CONSUMABLES_FOR_ASSETGRIDVR_SMAPPROVAL", param)
        Dim aid_qty As String
        For Each row As DataRow In ds.Tables(0).Rows

            qty = row.Item("AID_AVLBL_QTY")
            If qty = 0 Then
                For Each row1 As GridViewRow In gvItems.Rows
                    Dim lblAvailQty As Label = DirectCast(row1.FindControl("lblAvailQty"), Label)
                    aid_qty = lblAvailQty.Text
                    If aid_qty = "0" Then



                        Dim txtQty As TextBox = DirectCast(row1.FindControl("txtQty"), TextBox)
                        txtQty.Enabled = False
                        Dim txtQtyissue As TextBox = DirectCast(row1.FindControl("txtQtyissue"), TextBox)
                        txtQtyissue.Enabled = False

                        Dim chkSelect As CheckBox = DirectCast(row1.FindControl("chkSelect"), CheckBox)
                        chkSelect.Enabled = False

                    End If

                Next
                btnRaiseIndent.Visible = True
            End If
        Next row

    End Sub



    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("frmItemViewReqStr.aspx")
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Validate(Request.QueryString("RID"))

    End Sub

    Private Sub Validate(ByVal ReqId As String)


        Dim count As Integer = 0
        Dim Message As String = String.Empty
        Dim sta_id As Integer = 0
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim lblAvailQty As Label = DirectCast(row.FindControl("lblAvailQty"), Label)
            Dim txtQtyissue As TextBox = DirectCast(row.FindControl("txtQtyissue"), TextBox)


            If chkSelect.Checked Then
                If String.IsNullOrEmpty(txtQtyissue.Text) Then
                    lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                    Exit Sub

                ElseIf CInt(txtQtyissue.Text) > CInt(txtQty.Text) Then
                    lblMsg.Text = "Please enter quantity less than the approved quantity"
                    Exit Sub

                ElseIf CInt(lblAvailQty.Text) < CInt(txtQtyissue.Text) Then
                    lblMsg.Text = "Please enter quantity less than the available quantity"
                    Exit Sub
                ElseIf IsNumeric(txtQty.Text) = True Then
                    count = count + 1
                    If count = 0 Then
                        lblMsg.Text = "Sorry! Request has not been Raised You haven't select any Products and make Quantity more than Zero"
                        Exit Sub
                    End If
                Else
                    lblMsg.Text = "Please Enter Quantity In Numerics Only"
                    Exit Sub
                End If

                If CInt(txtQtyissue.Text) < CInt(txtQty.Text) Then
                    sta_id = 1511
                Else
                    sta_id = 1506
                End If

            End If
        Next
        If count > 0 Then

            '' commented for the Consumable issuance
            '' BY Praveen on 08th Aug 2014

            For Each row As GridViewRow In gvItems.Rows

                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
                Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
                Dim lblMinOrdQty As Label = DirectCast(row.FindControl("lblMinOrdQty"), Label)
                Dim txtQtyissue As TextBox = DirectCast(row.FindControl("txtQtyissue"), TextBox)
                If chkSelect.Checked Then
                    If Not String.IsNullOrEmpty(txtQtyissue.Text) Then
                        InsertDetails(ReqId, lblProductid.Text, CInt(Trim(txtQtyissue.Text)))
                        UpdateData(ReqId, Trim(txtSTRem.Text), sta_id)
                    End If
                End If
            Next

            '' ends here
            send_mail(Request.QueryString("RID"))
            Response.Redirect("frmAssetThanks.aspx?RID=" + Request.QueryString("RID"))
        ElseIf count = 0 Then
            lblMsg.Text = "Sorry! Request Has not been Raised You haven't select any Products and make Quantity more than Zero"
        Else
            lblMsg.Text = "Please Enter Quantity For Selected Checkbox"
        End If
    End Sub

    Public Sub send_mail(ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_STOREMNGR_APPROVAL")
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Execute()
    End Sub

    Private Sub UpdateData(ByVal ReqId As String, ByVal Remarks As String, ByVal sta_id As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_CONSUMABLES_UpdateByReqIdSM")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@StatusId", sta_id, DbType.Int32)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        sp.ExecuteScalar()
    End Sub


    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As String, ByVal Qty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_UPDATE_REQ_DETAILS")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        sp.ExecuteScalar()
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_UpdateByReqIdSMCANCEL")
        sp.Command.AddParameter("@ReqId", Request.QueryString("RID"), DbType.String)
        sp.Command.AddParameter("@Remarks", txtSTRem.Text, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@StatusId", 1507, DbType.Int32)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        sp.ExecuteScalar()
        send_mail_reject(Request.QueryString("RID"))
        Response.Redirect("frmAssetThanks.aspx?RID=" + Request.QueryString("RID"))

    End Sub

    Public Sub send_mail_reject(ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_STOREMNGR_REJECT")
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Execute()
    End Sub
    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        'ddlAstModel.Items.Insert(0, "--All--")
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))

    End Sub

    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")

        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ' ddlLocation.Items.Insert(0, New ListItem("--All--", ""))
        ddlLocation.Items.Remove("--Select--")
        'ddlLocation.Items.Remove("--All--")
        '   ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 1, 0)
    End Sub

    Protected Sub btnRaiseIndent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRaiseIndent.Click
        Response.Redirect("~/FAM/FAM_WebFiles/frmRaiseIndent_AddStock.aspx?RID=" + Request.QueryString("RID"))

    End Sub
End Class
