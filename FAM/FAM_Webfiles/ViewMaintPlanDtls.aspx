<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewMaintPlanDtls.aspx.vb" Inherits="FAM_FAM_Webfiles_ViewMaintPlanDtls" %>

<%@ Register Src="../../Controls/ViewMaintPlanDtls.ascx" TagName="ViewMaintPlanDtls"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>View Maintenance Plan Details</title>
    
     <%-- <style type="text/css">
	@import url("topmenu.css");
	
</style>--%>

    
     

    <link rel="stylesheet" type="text/css" href="http://projects.a-mantra.com/amantrafam/js/Default.css" />
    <link rel="stylesheet" type="text/css" href="http://projects.a-mantra.com/amantrafam/Scripts/styAMTamantra.css" />
  
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:ViewMaintPlanDtls ID="ViewMaintPlanDtls1" runat="server" />
    
    </div>
    </form>
</body>
</html>
