<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAdminAstDisposableDTLS.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAdminAstDisposableDTLS"
    Title="Admin Asset Dispose Requisitions Details" %>

<%@ Register Src="../../Controls/AdminAstDisposableDTLS.ascx" TagName="AdminAstDisposableDTLS" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Admin Asset Dispose Requisitions Details" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Admin Asset Dispose Requisitions Details</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <uc1:AdminAstDisposableDTLS ID="AdminAstDisposableDTLS1" runat="server" />
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

