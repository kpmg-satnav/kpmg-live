<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmGenerateAssetLabels.aspx.vb" Inherits="FAM_FAM_Webfiles_frmGenerateAssetLabels" Title="Asset Lable Generation" %>

<%@ Register Src="../../Controls/GenerateAssetLabels.ascx" TagName="GenerateAssetLabels" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                startDate:new Date(),
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Generate Asset Labels" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Generate Asset Labels</h3>
                </div>
                <asp:Label ID="lblmsg" runat="server"></asp:Label>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <%-- <%--       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:updatepanel runat="server">
                                      <ContentTemplate>--%>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <uc1:GenerateAssetLabels ID="GenerateAssetLabels1" runat="server"></uc1:GenerateAssetLabels>
                        <%-- </ContentTemplate>
                                          </asp:updatepanel>--%>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
