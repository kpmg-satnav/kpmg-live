
Partial Class FAM_FAM_Webfiles_frmItemViewReq
    Inherits System.Web.UI.Page

    Dim obj As New clsSubSonicCommonFunctions()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            fillgrid()
        End If
    End Sub
    Private Sub fillgrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_CONSUMABLES_FOR_ASSETVIEWREQUEST")
            sp.Command.AddParameter("@AUR_ID", Session("Uid"), Data.DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), Data.DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_CONSUMABLES_FILTER_GRID")
            sp.Command.AddParameter("@AUR_ID", Session("Uid"), Data.DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), Data.DbType.String)
            sp.Command.AddParameter("@SEARCH", txtSearch.Text, Data.DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception

        End Try
    End Sub
End Class
