﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewApproval.aspx.vb" Inherits="FAM_FAM_Webfiles_frmViewApproval" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="View & Approve" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">View & Approve</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                    EmptyDataText="No View Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>

                                        <asp:TemplateField HeaderText="Requisition ID" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIR_REQ_ID","~/FAM/FAM_WebFiles/frmAssetRequisitionDetails.aspx?RID={0}") %>'
                                                    Text='<%#Eval("AIR_REQ_ID")%>'></asp:HyperLink>
                                                <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:BoundField DataField="AUR_NAME" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                                        <asp:BoundField DataField="AIR_REQ_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                                        <asp:BoundField DataField="STA_TITLE" HeaderText="Status" ItemStyle-HorizontalAlign="left" />

                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
