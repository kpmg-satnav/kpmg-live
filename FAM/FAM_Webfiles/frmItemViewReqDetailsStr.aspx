<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmItemViewReqDetailsStr.aspx.vb" Inherits="FAM_FAM_Webfiles_frmItemViewReqDetailsStr" Title="SM Approval" %>

<%--<%@ Register Src="../../Controls/ItemViewReqDetailsStr.ascx" TagName="ItemViewReqDetailsStr"
    TagPrefix="uc1" %>--%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.gvItems.Rows.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }
        <%--function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkItem";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = CheckBox.checked;
            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }--%>

        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkItem";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvItems.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Consumuable Requisition Approval 2</h3>
                </div>

                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="Assetpanel" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                    ForeColor="Red" ValidationGroup="Val1" />
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">                  
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Requisition Id</label>
                                            <div class="col-md-12">
                                                <asp:Label ID="lblReqId" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Raised By</label>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Asset Category</label>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlAstCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Asset Sub Category</label>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Asset Brand/Make</label>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Asset Model</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                                                Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Location</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                                                ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12">
                                        <div class="panel-heading-qfms">
                                            <h3 class="panel-title panel-heading-qfms-title">Item List</h3>
                                        </div>
                                        <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                            EmptyDataText="No Asset(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:BoundField DataField="AST_MD_id" HeaderText="Item Code" ItemStyle-HorizontalAlign="left" Visible="false" />
                                                <asp:BoundField DataField="AST_MD_NAME" HeaderText="Item Name" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="Category" HeaderText="Category Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="manufacturer" HeaderText="Brand Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Quantity Yet To Be Issued" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQty" runat="server" MaxLength="10" Text='<%#Eval("AID_QTY") %>' onkeyup="ValidateText(this);"></asp:TextBox>

                                                        <asp:Label ID="lblunits" runat="server" Text='<%#Eval("UNITS")%>'></asp:Label>
                                                        <asp:Label ID="lblProductid" runat="server" Text='<%#Eval("AST_MD_CODE")%>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblMinOrdQty" runat="server" Text='<%#Eval("AST_MD_MINQTY") %>' Visible="false"> </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Available Quantity" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAvailQty" runat="server" Text='<%#Eval("AID_AVLBL_QTY") %>'></asp:Label>
                                                        <asp:Label ID="lblunits1" runat="server" Text='<%#Eval("UNITS")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Quantity Issued" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQtyissue" runat="server" MaxLength="10" Text="" onkeyup="ValidateText(this);"></asp:TextBox>
                                                        <asp:Label ID="lblunits2" runat="server" Text='<%#Eval("UNITS")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAll" ItemStyle-HorizontalAlign="Center" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                                            ToolTip="Click to check all" />
                                                    </HeaderTemplate>

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReq_status" runat="server" Visible="false" Text='<%# Eval("AIR_STA_ID") %>'></asp:Label>
                                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:ChildClick(this);"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Remarks<span style="color: red;">*</span></label>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="lblRem" runat="server" CssClass="form-control" MaxLength="100" Height="30%" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">L1 Remarks<span style="color: red;">*</span></label>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="lblRMRem" runat="server" CssClass="form-control" MaxLength="100" Height="30%" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblStoreRem" runat="server" class="col-md-12 control-label">L2 Remarks<span style="color: red;">*</span></asp:Label>
                                            <asp:RegularExpressionValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtSTRem"
                                                ErrorMessage="Please Enter Valid Remarks" Display="None"
                                                ValidationGroup="Val1">
                                            </asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtSTRem"
                                                Display="None" ErrorMessage="Please Enter L2 Remarks!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtSTRem" runat="server" CssClass="form-control" MaxLength="100"
                                                    Height="30%" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="btnRaiseIndent" runat="server" Text="Raise Indent" CssClass="btn btn-primary custom-button-color" />
                                            <asp:Button ID="btnsubmit" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" />
                                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
<script type="text/ecmascript" defer>
    function refreshSelectpicker() {
        $("#<%=ddlAstCat.ClientID%>").selectpicker();
        $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
        $("#<%=ddlAstBrand.ClientID%>").selectpicker();
        $("#<%=ddlLocation.ClientID%>").selectpicker();
        $("#<%=ddlAstModel.ClientID%>").selectpicker();
        $("#<%=ddlEmp.ClientID%>").selectpicker();

    };
    refreshSelectpicker();
</script>



