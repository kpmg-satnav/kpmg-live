<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetRequisitionDetails_CO.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAssetRequisitionDetails_CO" Title="Asset Requisition Details" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" lang="javascript" defer>

        function ValidateText(i) {
            if (i.value.length > 0) {
                i.value = i.value.replace(/[^\d]+/g, '');
            }
        }

    </script>

    <script type="text/javascript" defer>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.gvItemsInter.Rows.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }
        <%--function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItemsInter.ClientID%>');
            var TargetChildControl = "chkItem";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = CheckBox.checked;
            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }--%>

        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItemsInter.ClientID%>');
            var TargetChildControl = "chkItem";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvItemsInter.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>

    <script type="text/javascript" defer>
        function CheckAllDataGridCheckBoxesIntra(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.gvItemsIntra.Rows.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }
        <%--function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItemsInter.ClientID%>');
            var TargetChildControl = "chkItem";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = CheckBox.checked;
            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }--%>

        function ChildClickIntra(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItemsIntra.ClientID%>');
            var TargetChildControl = "chkSelect";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

         <%--check box validation--%>
        function validateCheckBoxesIntra() {
            var gridView = document.getElementById("<%=gvItemsIntra.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>


    <%-- <style type="text/css">
        .modalBackground {
            background-color: white;
            /*filter: alpha(opacity=90);*/
            /*opacity: 0.8;*/
        }

        .modalPopup {
            /*background-color: #FFFFFF;*/
            background-color: black;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 140px;
        }
    </style>--%>
</head>
<body>



    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Coordinator Check Details" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Coordinator Check Details</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" />
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Requisition Id</label>
                                            <asp:TextBox ID="lblReqId" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Raised By</label>
                                            <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false" Visible="false"></asp:DropDownList>
                                            <asp:TextBox ID="txtemp" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Asset Category</label>
                                            <asp:DropDownList ID="ddlAstCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Asset Sub Category</label>
                                            <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Asset Brand/Make</label>
                                            <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Asset Model </label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstBrand"
                                                Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Location </label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                                                ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <asp:TextBox ID="txtStatus" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                            <div id="Div1" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Requestor</label>
                                                    <asp:TextBox ID="reqqer" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div id="Div2" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Level1 Approver</label>
                                                    <asp:TextBox ID="rmid" runat="server" Enabled="false" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div id="Div3" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Level2 Approver</label>
                                                    <asp:TextBox ID="admid" runat="server" Enabled="false" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                <div class="row">
                                    <div id="trRemarks" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Requestor Remarks</label>
                                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>



                                    <div id="trRMRemarks" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Level1 Remarks</label>
                                            <asp:TextBox ID="txtRMRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div id="trAdminRemarks" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Level2 Remarks</label>
                                            <asp:TextBox ID="txtAdminRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <%--<strong>Documents</strong>--%>
                                            <label class="col-md-7 control-label">Requisition Uploaded Documents</label>
                                            <asp:Label ID="lblDocsMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            <div id="tblGridDocs" runat="server">
                                                <asp:DataGrid ID="grdDocs" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" DataKeyField="ID"
                                                    EmptyDataText="No Documents Found." AutoGenerateColumns="False" PageSize="5">
                                                    <Columns>
                                                        <asp:BoundColumn Visible="False" DataField="ID" HeaderText="ID"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="AMG_FILEPATH" HeaderText="Document Name">
                                                            <HeaderStyle></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="AMG_CREATED_DT" HeaderText="Document Date">
                                                            <HeaderStyle></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:ButtonColumn Text="Download" CommandName="Download">
                                                            <HeaderStyle></HeaderStyle>
                                                        </asp:ButtonColumn>

                                                    </Columns>
                                                    <HeaderStyle ForeColor="white" BackColor="Black" />
                                                    <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
                                                </asp:DataGrid>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Change Location</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                                                ControlToValidate="ddlRlocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlRlocation" runat="server" CssClass="selectpicker" AutoPostBack="true" data-live-search="true" OnSelectedIndexChanged="ddlRlocation_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>


                                <div id="pnlItems" runat="server" class="row">
                                    <div class="col-md-12">
                                        <div class="panel-heading-qfms">
                                            <h3 class="panel-title panel-heading-qfms-title">Assets list</h3>
                                        </div>
                                        <%--<div class="text-right">
                                    <asp:Button ID="btnRefresh" runat="server" Text="Refresh Grid" CssClass="btn btn-primary custom-button-color" />
                                </div>--%>
                                        <div>&nbsp</div>
                                        <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                            EmptyDataText="No Requisition(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:BoundField DataField="AST_MD_ID" HeaderText="Asset Code" ItemStyle-HorizontalAlign="left" Visible="false" />
                                                <asp:TemplateField HeaderText="Model Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProductname" runat="server" Text='<%#Eval("AST_MD_NAME") %>' Visible="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Quantity" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQty" runat="server" MaxLength="10" Text='<%#Eval("AID_QTY") %>' ReadOnly="true"></asp:TextBox>
                                                        <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("AST_MD_CODE")%>' Visible="false"></asp:Label>

                                                        <asp:Label ID="lbl_vt_code" Text='<%#Eval("VT_CODE") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbl_ast_subcat_code" Text='<%#Eval("AST_SUBCAT_CODE") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbl_manufactuer_code" Text='<%#Eval("manufactuer_code") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbl_ast_md_code" Text='<%#Eval("AST_MD_CODE") %>' runat="server" Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Available Stock" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPStock" runat="server" Text='<%#Eval("Stock") %>' Visible="false"></asp:Label>
                                                        <%--<a href="#" onclick="showPopWin('frmViewAssetsInStock.aspx?id=<%#Eval("AST_MD_id") %>&req_id=<%#Eval("AID_REQ_ID") %>&qty=<%#Eval("AID_QTY")-Eval("AID_MVM_QTY") %>',800,350,null)"><%# Eval("Stock") %></a>--%>
                                                        <%--<a href="#" onclick="showPopUpInterMvmtData1()"><%# Eval("Stock") %></a>--%>

                                                        <%--<asp:Button ID="btnShowPopup" Text='<%# Eval("Stock") %>' runat="server" CommandName="BindInterMovement" CommandArgument='<%#Eval("AST_MD_CODE")%>' ></asp:Button>--%>

                                                        <asp:LinkButton ID="Button1" Text='<%# Eval("Stock") %>' runat="server" CommandName="BindInterMovement" CommandArgument='<%#Eval("AST_MD_CODE")%>'></asp:LinkButton>


                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Inter Movement" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbInterMvnt" runat="server" Text='<%# Eval("INTERSTOCK") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Intra Movement" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbIntraMvnt" runat="server" Text='<%# Eval("INTRASTOCK") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%--   <asp:TemplateField HeaderText="Approved Quantity" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblApprQty" runat="server" Text='<%# Eval("AID_MVM_QTY") %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Quantity From Stock" ItemStyle-HorizontalAlign="left" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStock" runat="server" Text='<%# Eval("Stock") %>' Visible="false">
                                                        </asp:Label>
                                                        <asp:TextBox ID="txtStockQty" runat="server" MaxLength="10" Visible="false"></asp:TextBox>
                                                        <asp:Label ID="lblStockmsg" runat="server" CssClass="error" Text='<%# Eval("Stock") %>' Visible="false">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Quantity To Purchase" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>

                                                        <asp:TextBox ID="txtPurchaseQty" runat="server" MaxLength="10" onkeyup="ValidateText(this);"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnUpdate" Text="Update" CssClass="button" runat="server" CommandName="UpdateRecord" CommandArgument='<%#Eval("AST_MD_CODE")%>'></asp:Button>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="btnSubmit" Text="Update and Approve" runat="server" CssClass="btn btn-primary custom-button-color" />
                                            <asp:Button ID="btnCancel" Text="Reject" runat="server" CssClass="btn btn-primary custom-button-color" />
                                            <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" OnClick="btnBack_Click" />
                                        </div>
                                    </div>
                                </div>

                                <asp:LinkButton Text="" ID="lnkFake" runat="server" />
                                <cc1:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
                                    CancelControlID="btnpopupclose" BackgroundCssClass="modal fade bs-example-modal-lg">
                                </cc1:ModalPopupExtender>

                                <asp:Panel ID="pnlPopup" runat="server" TabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" id="btnpopupclose" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">View Assets in Stock</h4>
                                            </div>
                                            <div class="modal-body" id="modelcontainer">
                                                <%-- Content loads here --%>
                                                <div id="pnlItemsIntra" runat="server">
                                                    <div class="clearfix" style="margin-top: 10px">
                                                        <div class="col-md-12">
                                                            <fieldset>
                                                                <legend>Asset Intra Movement</legend>
                                                                <asp:GridView ID="gvItemsIntra" runat="server" AllowPaging="false" AutoGenerateColumns="false" EmptyDataText="No Asset(s) Found."
                                                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Asset">
                                                                            <ItemStyle Width="15%" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblasset" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Asset Name">
                                                                            <ItemStyle Width="15%" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblassetName" runat="server" Text='<%#Eval("AAT_CODE") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Floor" Visible="false">
                                                                            <ItemStyle Width="15%" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblfloor" runat="server" Text='<%#Eval("FLR_NAME") %>' Visible="false"></asp:Label>
                                                                                <asp:Label ID="lblasset_md_code" runat="server" Text='<%#Eval("AST_MD_CODE")%>' Visible="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Tower" Visible="false">
                                                                            <ItemStyle Width="15%" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbltower" runat="server" Text='<%#Eval("TWR_NAME") %>' Visible="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Location">
                                                                            <ItemStyle Width="15%" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbllocation" runat="server" Text='<%#Eval("LCM_CODE") %>' Visible="false"></asp:Label>
                                                                                <asp:Label ID="lbllocationname" runat="server" Text='<%#Eval("LOCATION") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="City">
                                                                            <ItemStyle Width="10%" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblcity" runat="server" Text='<%#Eval("CITY") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Employee">
                                                                            <ItemStyle Width="10%" />
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddlemp" runat="server" CssClass="selectpicker" data-live-search="true" Width="100%">
                                                                                </asp:DropDownList>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <%--      <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                                                        <ItemStyle Width="5%" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                                        <asp:TemplateField>
                                                                            <ItemStyle Width="5%" />
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxesIntra('chkSelect', this.checked);"
                                                                                    ToolTip="Click to check all" />
                                                                            </HeaderTemplate>

                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClickIntra(this);" />
                                                                            </ItemTemplate>

                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                    <PagerStyle CssClass="pagination-ys" />
                                                                </asp:GridView>
                                                            </fieldset>
                                                            <div class="row">
                                                                <div class="col-md-12 text-right">
                                                                    <div class="form-group">
                                                                        <asp:Button ID="btnSubmitIntra" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" OnClientClick="javascript:return validateCheckBoxesIntra();" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div id="pnlItemsInter" runat="server">
                                                    <div class="clearfix" style="margin-top: 10px">

                                                        <div class="clearfix">
                                                            <asp:Label ID="lblPopUpMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                            </asp:Label>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <fieldset>
                                                                <legend>Asset Inter Movement</legend>


                                                                <div class="clearfix" style="overflow: auto; width: 100%; min-height: 20px; max-height: 250px">
                                                                    <asp:GridView ID="gvItemsInter" runat="server" AllowPaging="false" AutoGenerateColumns="false" EmptyDataText="No Asset(s) Found."
                                                                        CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Location">
                                                                                <ItemStyle Width="15%" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lbllocation" runat="server" Text='<%#Eval("LCM_CODE") %>' Visible="false"></asp:Label>
                                                                                    <asp:Label ID="lbllocationname" runat="server" Text='<%#Eval("LOCATION") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Asset">
                                                                                <ItemStyle Width="15%" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblasset" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Asset Name">
                                                                                <ItemStyle Width="15%" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblassetName" runat="server" Text='<%#Eval("AAT_CODE") %>'></asp:Label>
                                                                                    <asp:Label ID="lblAstModelCode" runat="server" Text='<%#Eval("AST_MD_CODE") %>' Visible="false"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField>
                                                                                <ItemStyle Width="5%" />
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkItem', this.checked);"
                                                                                        ToolTip="Click to check all" />
                                                                                </HeaderTemplate>

                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkItem" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                                                                                </ItemTemplate>

                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                        <PagerStyle CssClass="pagination-ys" />
                                                                    </asp:GridView>
                                                                </div>
                                                            </fieldset>
                                                            <br />
                                                            <div class="clearfix">
                                                                <div class="col-md-12 text-right">
                                                                    <div class="form-group">
                                                                        <asp:Button ID="btnSubmitInter" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" OnClientClick="javascript:return validateCheckBoxesMyReq();" />
                                                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary custom-button-color" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block --%>

    <%-- Modal popup block--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>


        //$(".close").click(function () {
        //    location.reload();
        //});
        var asset_code, requestid;
        function showPopWin(id, req_id, qty) {
            //$("#modalcontentframe").attr("src", "frmViewAssetsInStock.aspx?id=" + id + "&req_id=" + req_id + "&qty=" + qty);

            //$("#myModal").modal().fadeIn();
            $("#interNintraMovement").modal().fadeIn();

            fillInterMovementData(id, req_id);
            return false;
        }

        function showPopUpInterMvmtData1() {
            $("#interNintraMovement").modal().fadeIn();
            return false;
        }

        function showPopUpInterMvmtData() {
            $("#interNintraMovement").dialog({
                title: "jQuery Popup from Server Side",
                width: 430,
                height: 250,
                modal: true,
                buttons: {
                    Close: function () {
                        $(this).dialog('close');
                    }
                }
            });
        };



        function fillInterMovementData(asset_code, requestid) {
            var Model = { 'RequisitionID': requestid, 'AssetCode': asset_code };
            $.ajax({
                type: "POST",
                url: '../../api/AssetInterNIntraMovement/GetInterMovementsData',
                data: Model,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    console.log(data);
                }
            });
        }
        function ShowPopup() {
            alert('hi');
        };
        function refreshSelectpicker() {
            $("#<%=ddlAstCat.ClientID%>").selectpicker();
            $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
            $("#<%=ddlAstBrand.ClientID%>").selectpicker();
            $("#<%=gvItemsInter.ClientID%>").selectpicker();
            $("#<%=ddlRlocation.ClientID%>").selectpicker();
            $("#<%=ddlLocation.ClientID%>").selectpicker();
            $("#<%=ddlAstModel.ClientID%>").selectpicker();
        }
        refreshSelectpicker();
    </script>
</body>
</html>
