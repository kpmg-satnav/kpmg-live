<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAmgVendorNewRecord1.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAmgVendorNewRecord1" Title="Add Vendor" %>

<%@ Register Src="../../Controls/AMGVendorNewRecord1.ascx" TagName="AMGVendorNewRecord1"
    TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Asset Brand Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Asset Vendor Master</h3>
                </div>
                <div class="panel-content">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                        <uc1:AMGVendorNewRecord1 ID="AMGVendorNewRecord1_1" runat="server" />
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
<script type="text/javascript" defer>
    $("#btnSubmit").click(function () {
        $('#lblMsg').text("")
    });
</script>
</html>

