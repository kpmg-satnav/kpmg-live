﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Level2AppCap.aspx.vb" Inherits="FAM_FAM_Webfiles_Level2AppCap" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.gvItems.Rows.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }


            function ChildClick(CheckBox) {
                //Get target base & child control.
                var TargetBaseControl = document.getElementById('<%= Me.gvCaptApprovals.ClientID%>');
                var TargetChildControl = "chkSelect";
                //Get all the control of the type INPUT in the base control.
                var Inputs = TargetBaseControl.getElementsByTagName("input");
                // check to see if all other checkboxes are checked
                for (var n = 0; n < Inputs.length; ++n)
                    if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                        // Whoops, there is an unchecked checkbox, make sure
                        // that the header checkbox is unchecked
                        if (!Inputs[n].checked) {
                            Inputs[0].checked = false;
                            return;
                        }
                    }
                // If we reach here, ALL GridView checkboxes are checked
                Inputs[0].checked = true;
            }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvCaptApprovals.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Level2 Approval" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Level2 Approval</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="Assetpanel" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                    ForeColor="Red" ValidationGroup="Val1" />
                                <div class="row">
                                    <div class="col-md-12 text-left">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Search By Any..." CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:Button ID="btnSearch" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search"
                                                        CausesValidation="true" TabIndex="2" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                            EmptyDataText="No Requisitions Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>

                                                <%-- <asp:TemplateField HeaderText="Requisition ID" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIR_REQ_ID","~/FAM/FAM_WebFiles/frmAssetRequisitionDetails.aspx?RID={0}") %>'
                                                            Text='<%#Eval("AIR_REQ_ID")%>'></asp:HyperLink>
                                                        <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>

                                                <asp:TemplateField HeaderText="Requisition ID" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="hLinkDetails" runat="server" Text='<%#Eval("AIR_REQ_ID")%>' CommandArgument='<%#Eval("AIR_REQ_ID")%>' CommandName="GetCaptApprovalDetails"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:BoundField DataField="AUR_NAME" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="AIR_REQ_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="STA_TITLE" HeaderText="Status" ItemStyle-HorizontalAlign="left" />

                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                    <asp:HiddenField ID="hdnCANO" runat="server" />
                                    <div class="clearfix"></div>
                                    <div id="updatepanel" runat="server" visible="false" style="padding-top: 10px">
                                        <div class="col-md-12">
                                            <div class="panel-heading-qfms">
                                                <h3 class="panel-title panel-heading-qfms-title"><b>View Details</b> </h3>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                            </asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Requisition Id</label>
                                                        <asp:TextBox ID="lblReqId" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Raised By</label>
                                                        <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false" Visible="false"></asp:DropDownList>
                                                        <asp:TextBox ID="txtemp" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Asset Category</label>
                                                        <asp:DropDownList ID="ddlAstCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" Enabled="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Asset Sub Category</label>
                                                        <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" Enabled="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Asset Brand/Make</label>
                                                        <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" Enabled="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Asset Model </label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                                                            Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

                                                        <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" Enabled="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Location </label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                                                            ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="pnlItems" runat="server">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">

                                                            <div class="panel-heading-qfms">
                                                                <h3 class="panel-title panel-heading-qfms-title"><b>Assets List</b> </h3>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row" style="margin-top: 10px">
                                                    <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 320px">
                                                        <asp:GridView ID="gvCaptApprovals" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                                            EmptyDataText="No Asset(s) FounD." CssClass="table table-condensed table-bordered table-hover table-striped">
                                                            <Columns>
                                                                <asp:BoundField DataField="AST_MD_id" HeaderText="Code" ItemStyle-HorizontalAlign="left" Visible="FALSE" />
                                                                <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblproductname" Text='<%#Eval("AST_MD_NAME") %>' runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Category" HeaderText="Category Name" ItemStyle-HorizontalAlign="left">
                                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category Name" ItemStyle-HorizontalAlign="left">
                                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="manufacturer" HeaderText="Brand Name" ItemStyle-HorizontalAlign="left">
                                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText=" Requested Quantity" ItemStyle-HorizontalAlign="left">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtQty" runat="server" Width="50%" MaxLength="10" Text='<%#Eval("AID_QTY") %>'></asp:TextBox>
                                                                        <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("AST_MD_CODE")%>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lbl_vt_code" Text='<%#Eval("VT_CODE") %>' runat="server" Visible="false"></asp:Label>
                                                                        <asp:Label ID="lbl_ast_subcat_code" Text='<%#Eval("AST_SUBCAT_CODE") %>' runat="server" Visible="false"></asp:Label>
                                                                        <asp:Label ID="lbl_manufactuer_code" Text='<%#Eval("manufactuer_code") %>' runat="server" Visible="false"></asp:Label>
                                                                        <asp:Label ID="lbl_ast_md_code" Text='<%#Eval("AST_MD_CODE") %>' runat="server" Visible="false"></asp:Label>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Estimated Cost" ItemStyle-HorizontalAlign="left">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtEstCost" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AID_EST_COST")%>'></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Quantity To Purchase" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>

                                                                <asp:TextBox ID="txtPurchaseQty" runat="server" MaxLength="10" Text='<%#Eval("AID_QTY") %>' Enabled="false" onkeyup="ValidateText(this);"></asp:TextBox>
                                                            </ItemTemplate>
                                                                     </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="left">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                                                            ToolTip="Click to check all" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblReq_status" runat="server" Visible="false" Text='<%# Eval("AIR_STA_ID") %>'></asp:Label>
                                                                        <asp:CheckBox ID="chkSelect" Checked='<%#Bind("ticked") %>' runat="server" onclick="javascript:ChildClick(this);"></asp:CheckBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                            <PagerStyle CssClass="pagination-ys" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <br />
                                            <div class="row">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Status</label>
                                                        <asp:TextBox ID="txtStatus" runat="server" CssClass="form-control" ReadOnly="true">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Requestor Remarks</label>
                                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="form-control">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Level1 Remarks</label>
                                                        <asp:TextBox ID="txtL1Remarks" runat="server" TextMode="MultiLine" CssClass="form-control"
                                                            ReadOnly="True">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Level2 Remarks</label>
                                                        <asp:RegularExpressionValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtRemarks"
                                                            ErrorMessage="Please Enter Valid Remarks" Display="None"
                                                            ValidationGroup="Val1">
                                                        </asp:RegularExpressionValidator>
                                                        <asp:TextBox ID="txtL2Remarks" runat="server" TextMode="MultiLine" CssClass="form-control">                                                           
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <br />
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <div class="form-group">
                                                        <asp:Button ID="btnApprove" Text="Approve" runat="server" CssClass="btn btn-primary custom-button-color" />
                                                        <asp:Button ID="btnReject" Text="Reject" runat="server" CssClass="btn btn-primary custom-button-color" />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/ecmascript" defer>
        function refreshSelectpicker() {
            $("#<%=ddlAstCat.ClientID%>").selectpicker();
            $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
            $("#<%=ddlAstBrand.ClientID%>").selectpicker();
            $("#<%=ddlAstModel.ClientID%>").selectpicker();
            $("#<%=ddlEmp.ClientID%>").selectpicker();
            $("#<%=ddlLocation.ClientID%>").selectpicker();

        };
        refreshSelectpicker();
    </script>
</body>
</html>
