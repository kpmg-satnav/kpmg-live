<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewadminSurrenderRequisitions.aspx.vb"
    Inherits="FAM_FAM_Webfiles_frmViewadminSurrenderRequisitions" Title="View RM Approved Surrender Requistions" %>

<%@ Register Src="../../Controls/ViewSurrenderRequisitions_admin.ascx" TagName="ViewSurrenderRequisitions_admin" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="View RM Approved Asset Surrender Requisitions" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">View RM Approved Asset Surrender Requisitions</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" class="form-horizontal well" runat="server">
                        <uc1:ViewSurrenderRequisitions_admin ID="ViewSurrenderRequisitions_admin1" runat="server" />
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
