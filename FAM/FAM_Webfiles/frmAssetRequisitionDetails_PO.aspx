<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetRequisitionDetails_PO.aspx.vb"
    Inherits="FAM_FAM_Webfiles_frmAssetRequisitionDetails_PO" Title="PO Generation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->


</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="PO Generation" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Generate PO</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">

                    <form id="form1" name="frm" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val2" />
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <%--<label class="col-md-3 btn btn-default pull-right">--%>

                                        <label class="btn btn-default pull-right">
                                            <asp:RadioButton value="0" runat="server" name="rbActions" ID="Rbtwithout" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                                ToolTip="Please Select Space to Space Asset Mapping and Select Employee to Enployee Asset Mapping" />Without Contract
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="btn btn-default" style="margin-left: 25px">
                                            <asp:RadioButton value="1" runat="server" name="rbActions" ID="Rbtwith" GroupName="rbActions" AutoPostBack="true"
                                                ToolTip="Please Select Space to Space Asset Mapping and Select Employee to Enployee Asset Mapping" />With Contract

                                        </label>
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 300px">
                                        <asp:GridView ID="gvvendorreqs" runat="server" AutoGenerateColumns="false" EmptyDataText="No Requisitions Found To Approve."
                                            CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        Select
                                                          <%--  <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                                                ToolTip="Click to check all" />--%>
                                                        <%--OnCheckedChanged="chkAll_CheckedChanged"--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" Checked='<%#Bind("ticked") %>' runat="server" ToolTip="Click to check" OnCheckedChanged="chkSelect_CheckedChanged" AutoPostBack="true" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                                    <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <%--<asp:BoundField DataField="AIR_REQ_ID" HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left" />--%>
                                                <asp:TemplateField HeaderText="Requisition ID" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <%--  <asp:HyperLink ID="hLinkDetails" runat="server" Text='<%# Eval("AIR_REQ_ID")%> '>                                                                
                                                            </asp:HyperLink>--%>
                                                        <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblReqID" runat="server" Text='<%#Eval("AIR_REQ_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- <asp:BoundField DataField="AIR_REQ_ID" HeaderText="Request Id" ItemStyle-HorizontalAlign="left" />--%>
                                                <asp:BoundField DataField="AIR_REQ_LOC" HeaderText="Location" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="VT_TYPE" HeaderText="Category" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="TOTAL_REQ_QTY" HeaderText="Total Requested Qty" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="AUR_NAME" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="AIR_REQ_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="AIR_APR1_BY" HeaderText="Approved By" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="AIR_APR1_DATE" HeaderText="Approved Date" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="STA_TITLE" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <div id="divvendors" runat="server" class="col-md-6">
                                    <div class="form-group">
                                        <label>Vendor <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlVendor2"
                                            Display="none" ErrorMessage="Please Select Vendor !" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlVendor2" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div id="divven" runat="server" class="col-md-6">
                                    <div class="form-group">
                                        <label>Vendor <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlVendor"
                                            Display="none" ErrorMessage="Please Select Vendor !" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="pnlItems" runat="server">
                                    <div class="row" style="margin-top: 10px">
                                        <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 300px">
                                            <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false"
                                                EmptyDataText="No Asset(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="left">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect1', this.checked);"
                                                                ToolTip="Click to check all" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect1" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="AID_REQ_ID" HeaderText="Request Id" ItemStyle-HorizontalAlign="left" />
                                                    <%--<asp:BoundField DataField="AIR_REQ_LOC" HeaderText="Location" ItemStyle-HorizontalAlign="left" />--%>
                                                    <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblReqLoc" runat="server" Text='<%#Eval("AIR_REQ_LOC") %>' Visible="true"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="AST_MD_CODE" HeaderText="Asset Code" ItemStyle-HorizontalAlign="left" />
                                                    <asp:BoundField DataField="AST_MD_NAME" HeaderText="Asset Name" ItemStyle-HorizontalAlign="left" />
                                                    <asp:TemplateField HeaderText="Requested Qty" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" MaxLength="10" Text='<%#Eval("AID_QTY") %>'
                                                                ReadOnly="true"></asp:TextBox>
                                                            <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("AST_MD_CODE") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblReqId" runat="server" Text='<%#Eval("AID_REQ_ID") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Stock" HeaderText="Available Stock" ItemStyle-HorizontalAlign="left" />
                                                    <%--<asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" DataFormatString="{0:c2}"
                                                        ItemStyle-HorizontalAlign="left" />--%>

                                                    <asp:TemplateField HeaderText="Unit Price" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="form-control" MaxLength="10" Text='<%#formatnumber(Eval("UnitPrice"),2) %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Qty From Stock" ItemStyle-HorizontalAlign="left" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtStockQty" runat="server" CssClass="form-control" MaxLength="10" Text='<%#Eval("AID_MVM_QTY") %>'
                                                                ReadOnly="true"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Qty To Purchase" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtPurchaseQty" runat="server" CssClass="form-control" MaxLength="10" Text='<%#Eval("AID_ORD_QTY") %>'
                                                                ReadOnly="true"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Price" ItemStyle-HorizontalAlign="left" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control" MaxLength="10" Text='<%#formatnumber(Eval("Price"),2) %>'
                                                                ReadOnly="true"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Price" HeaderText="Price" DataFormatString="Rs. {0}" ItemStyle-HorizontalAlign="left"
                                                        Visible="false" />
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row ">
                                                <%--<div class="col-md-3">
                                                </div>
                                                <label class="col-md-3 control-label"><strong>Tax Details</strong></label>
                                                <div class="col-md-3">
                                                </div>--%>
                                                <div class="col-md-3">
                                                    <strong>Payment Terms</strong>
                                                </div>
                                            </div>
                                        </div>

                                        <%-- <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label>Entry Tax</label>
                                            </div>
                                            <asp:RequiredFieldValidator ID="rfvtax" runat="server" Display="None" ErrorMessage="Enter Tax"
                                                ValidationGroup="Val1" ControlToValidate="txtFFIP"></asp:RequiredFieldValidator>
                                            <div class="col-md-5">
                                                <asp:TextBox ID="txtFFIP" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>--%>


                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label>Advance</label>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfvadvance" runat="server" Display="None" ErrorMessage="Advance"
                                                    ValidationGroup="Val1" ControlToValidate="txtAdvance"></asp:RequiredFieldValidator>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="txtAdvance" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label>On Delivery</label>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfvdelivery" runat="server" Display="None" ErrorMessage="On Delivery"
                                                    ValidationGroup="Val1" ControlToValidate="txtDelivery"></asp:RequiredFieldValidator>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="txtDelivery" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <%--<div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label>VAT</label>
                                            </div>
                                            <asp:RequiredFieldValidator ID="rfvvat" runat="server" Display="None" ErrorMessage="VAT"
                                                ValidationGroup="Val1" ControlToValidate="txtWst"></asp:RequiredFieldValidator>
                                            <div class="col-md-5">
                                                <asp:TextBox ID="txtWst" runat="server" CssClass="form-control" Text="0"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>--%>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label>Installation</label>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfvinstallation" runat="server" Display="None"
                                                    ErrorMessage="Installation" ValidationGroup="Val1" ControlToValidate="txtInstallation"></asp:RequiredFieldValidator>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="txtInstallation" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <%--       <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label>Octroi</label>
                                            </div>
                                            <asp:RequiredFieldValidator ID="rfvoctroi" runat="server" Display="None" ErrorMessage="Octrai"
                                                ValidationGroup="Val1" ControlToValidate="txtOctrai"></asp:RequiredFieldValidator>
                                            <div class="col-md-5">
                                                <asp:TextBox ID="txtOctrai" runat="server" CssClass="form-control" Text="0"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>--%>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label>Commissioning</label>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfvcomm" runat="server" Display="None" ErrorMessage="Commissioning"
                                                    ValidationGroup="Val1" ControlToValidate="txtCommissioning"></asp:RequiredFieldValidator>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="txtCommissioning" runat="server" CssClass="form-control" Text="0"></asp:TextBox>

                                                </div>
                                            </div>
                                        </div>
                                        <%-- <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label>Service Tax</label>
                                            </div>
                                            <asp:RequiredFieldValidator ID="rfvservice" runat="server" Display="None" ErrorMessage="Service Tax"
                                                ValidationGroup="Val1" ControlToValidate="txtServiceTax"></asp:RequiredFieldValidator>
                                            <div class="col-md-5">
                                                <asp:TextBox ID="txtServiceTax" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>--%>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label>Retention</label>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfvretention" runat="server" Display="None" ErrorMessage="Retention"
                                                    ValidationGroup="Val1" ControlToValidate="txtRetention"></asp:RequiredFieldValidator>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="txtRetention" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <%--  <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label>Others</label>
                                            </div>
                                            <asp:RequiredFieldValidator ID="rfvothers" runat="server" Display="None" ErrorMessage="Others"
                                                ValidationGroup="Val1" ControlToValidate="txtOthers"></asp:RequiredFieldValidator>
                                            <div class="col-md-5">
                                                <asp:TextBox ID="txtOthers" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>--%>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label>Payments</label>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfvpayments" runat="server" Display="None" ErrorMessage="Payments"
                                                    ValidationGroup="Val1" ControlToValidate="txtPayments"></asp:RequiredFieldValidator>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="txtPayments" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row ">
                                                <div class="col-md-3">
                                                    <strong>Tax Details</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label for="txtcode">Tax</label>
                                                </div>
                                                <asp:CompareValidator ID="rfvtax" runat="server" Display="None" ControlToValidate="ddlTax"
                                                    ErrorMessage="Please Select Tax" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                                </asp:CompareValidator>
                                                <div class="col-md-5">
                                                    <asp:DropDownList ID="ddlTax" runat="server" AutoPostBack="true" CssClass="form-control selectpicker" data-live-search="true">
                                                        <asp:ListItem Selected="True">Exclusive</asp:ListItem>
                                                        <asp:ListItem>Inclusive</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="gst" runat="server">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="col-md-3">
                                                        <label>CGST</label>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="rfvcst" runat="server" Display="None" ErrorMessage="CGST"
                                                        ValidationGroup="Val1" ControlToValidate="txtCst"></asp:RequiredFieldValidator>
                                                    <div class="col-md-5">
                                                        <asp:TextBox ID="txtCst" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="gst1" runat="server">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="col-md-3">
                                                        <label>SGST/UTGST</label>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="rfvsgst" runat="server" Display="None" ErrorMessage="SGST/UTGST"
                                                        ValidationGroup="Val1" ControlToValidate="txtsgst"></asp:RequiredFieldValidator>
                                                    <div class="col-md-5">
                                                        <asp:TextBox ID="txtsgst" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="col-md-3">
                                                        <label>IGST</label>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="rfvigst" runat="server" Display="None" ErrorMessage="IGST"
                                                        ValidationGroup="Val1" ControlToValidate="txtigst"></asp:RequiredFieldValidator>
                                                    <div class="col-md-5">
                                                        <asp:TextBox ID="txtigst" runat="server" CssClass="form-control" Text="0"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <div class="form-group">
                                                <asp:Button ID="btnTotalCost" runat="server" Text="Get Total Cost" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" OnClientClick="javascript:return validateCheckBoxesMyReq();" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Total Cost<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvcose" runat="server" Display="None" ErrorMessage="Please Enter Total Cost"
                                                    ValidationGroup="Val2" ControlToValidate="txtTotalCost"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtTotalCost" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Expected Date Of Delivery<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvdate" runat="server" Display="none" ErrorMessage="Please Select Expected Date Of Delivery"
                                                    ValidationGroup="Val2" ControlToValidate="txtDOD"></asp:RequiredFieldValidator>
                                                <div class='input-group date' id='fromdate'>
                                                    <asp:TextBox ID="txtDOD" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Exchange Charges<span style="color: red;">*</span></label>
                                                <asp:RegularExpressionValidator ID="RegExpNumber" runat="server" ControlToValidate="txtExchange"
                                                    ErrorMessage="Please Enter Valid Depreciation" Display="None"
                                                    ValidationGroup="Val1">
                                                </asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="rfvcharges" runat="server" Display="None" ErrorMessage="Please Enter Exchange Charges"
                                                    ControlToValidate="txtExchange" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtExchange" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Remarks<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRemarks"
                                                    Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtRemarks" Height="50px" runat="server" CssClass="form-control" TextMode="multiline"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Address<span style="color: red;"></span></label>
                                                <asp:TextBox ID="txtAdd" Height="50px" runat="server" CssClass="form-control" TextMode="multiline"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Contact Number<span style="color: red;"></span></label>
                                                <asp:RegularExpressionValidator ID="reqnum" runat="server" ControlToValidate="txtNumber"
                                                    ErrorMessage="Please Enter Valid Depreciation" Display="None"
                                                    ValidationGroup="Val1">
                                                </asp:RegularExpressionValidator>
                                                <asp:TextBox ID="txtNumber" Height="35px" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Quotation Reference<span style="color: red;"></span></label>
                                                <asp:TextBox ID="txtQutref" Height="35px" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Quotation Date<span style="color: red;"></span></label>
                                                <asp:TextBox ID="txtQutdate" Height="35px" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-xs-6">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">State</label>
                                                <div class="col-md-12">
                                                    <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="True" CssClass="form-control selectpicker" data-live-search="true" ToolTip="Select State">
                                                        <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>State GSTN<span style="color: red;"></span></label>
                                                <asp:TextBox ID="txtStateGST" Height="35px" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Billing Address<span style="color: red;"></span></label>
                                                <asp:TextBox ID="txtSAddress" Height="50px" runat="server" CssClass="form-control" TextMode="multiline"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Terms and Conditions<span style="color: red;"></span></label>
                                                <asp:TextBox ID="txtTerms" Height="100px" Width="550px" runat="server" CssClass="form-control" TextMode="multiline"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <div class="form-group">
                                                <asp:Button ID="btnSubmit" Text="Generate PO" runat="server" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val2" />
                                                <asp:Button ID="btnCancel" Text="Reject" runat="server" CssClass="btn btn-primary custom-button-color" Visible="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkSelect1";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

          <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvvendorreqs.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;

        }



        function setup(id) {
            $('#' + id).datepicker({
                startDate:new Date(),
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        function refreshSelectpicker() {
            $("#<%=ddlVendor.ClientID%>").selectpicker();
            $("#<%=ddlVendor2.ClientID%>").selectpicker();
            $("#<%=ddlTax.ClientID%>").selectpicker();
            $("#<%=ddlState.ClientID%>").selectpicker();
        }
        refreshSelectpicker();

    </script>
</body>
</html>





