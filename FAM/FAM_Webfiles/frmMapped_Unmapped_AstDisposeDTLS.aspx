<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMapped_Unmapped_AstDisposeDTLS.aspx.vb" Inherits="FAM_FAM_Webfiles_frmMapped_Unmapped_AstDisposeDTLS" %>

<%@ Register Src="../../Controls/Mapped_Unmapped_AstDisposeDTLS.ascx" TagName="Mapped_Unmapped_AstDisposeDTLS" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>

    <form id="form2" class="form-horizontal well" runat="server">
        <%--<asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" />--%>
        <uc1:Mapped_Unmapped_AstDisposeDTLS ID="Mapped_Unmapped_AstDisposeDTLS1" runat="server" />
    </form>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
