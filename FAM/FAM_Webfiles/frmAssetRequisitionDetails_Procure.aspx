<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetRequisitionDetails_Procure.aspx.vb"
    Inherits="FAM_FAM_Webfiles_frmAssetRequisitionDetails_Procure" Title="View Asset Requisition Details" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
    <style type="text/css">
        .custom-button-color {
            height: 26px;
        }
    </style>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Approve To Procure" ba-panel-class="with-scroll" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Approve To Procure</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="Assetpanel" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red"
                                    ValidationGroup="Val1" />
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Requisition Id:</label>
                                            <asp:TextBox ID="lblReqId" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Raised By:</label>
                                            <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Asset Category:</label>
                                            <asp:DropDownList ID="ddlAstCat" Enabled="false" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Asset Sub Category</label>
                                            <asp:DropDownList ID="ddlAstSubCat" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Asset Brand/Make</label>
                                            <asp:DropDownList ID="ddlAstBrand" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Asset Model<span style="color: red;"></span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                                                Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>

                                            <asp:DropDownList ID="ddlAstModel" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Location<span style="color: red;"></span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                                                ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlLocation" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <asp:TextBox ID="txtStatus" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                </div>
                                 <div class="row">
                                            <div id="Div1" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Requestor</label>
                                                    <asp:TextBox ID="reqqer" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div id="Div2" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Level1 Approver</label>
                                                    <asp:TextBox ID="rmid" runat="server" Enabled="false" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div id="Div3" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Level2 Approver</label>
                                                    <asp:TextBox ID="admid" runat="server" Enabled="false" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                <div class="row" id="trRemarks" runat="server">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Requestor Remarks:</label>
                                            <asp:TextBox ID="txtRemarks" runat="server" Height="50px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" id="trRMRemarks" runat="server">
                                        <div class="form-group">

                                            <label>Approver 1 Remarks:</label>

                                            <asp:TextBox ID="txtRMRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" ReadOnly="True"></asp:TextBox>

                                        </div>
                                    </div>
                                
                                <div  id="trAdminRemarks" runat="server">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <label>Approver 2 Remarks:</label>
                                            <asp:TextBox ID="txtAdminRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" ReadOnly="True"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                                    </div>
                                <br />
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="form-group">
                                                    <%--<strong>Documents</strong>--%>
                                                    <label class="col-md-7 control-label">Requisition Uploaded Documents</label>
                                                    <asp:Label ID="lblDocsMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                    </asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-3 col-xs-12">
                                                <div class="form-group">
                                                    <div id="tblGridDocs" runat="server">
                                                        <asp:DataGrid ID="grdDocs" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" DataKeyField="ID"
                                                            EmptyDataText="No Documents Found." AutoGenerateColumns="False" PageSize="5">
                                                            <Columns>
                                                                <asp:BoundColumn Visible="False" DataField="ID" HeaderText="ID"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="AMG_FILEPATH" HeaderText="Document Name">
                                                                    <HeaderStyle></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="AMG_CREATED_DT" HeaderText="Document Date">
                                                                    <HeaderStyle></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:ButtonColumn Text="Download" CommandName="Download">
                                                                    <HeaderStyle></HeaderStyle>
                                                                </asp:ButtonColumn>

                                                            </Columns>
                                                            <HeaderStyle ForeColor="white" BackColor="Black" />
                                                            <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                <div id="pnlItems" runat="server">
                                    <div class="row" style="margin-top: 10px">
                                        <div class="col-md-12">
                                            <h3>Assets List </h3>
                                            <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                EmptyDataText="No Asset(s) Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                                <Columns>
                                                    <asp:BoundField DataField="AST_MD_CODE" HeaderText="Asset Code" ItemStyle-HorizontalAlign="left" Visible="False" />
                                                    <asp:BoundField DataField="AST_MD_NAME" HeaderText="Asset Model" ItemStyle-HorizontalAlign="left" />
                                                    <asp:TemplateField HeaderText="Requested Qty" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtQty" runat="server" MaxLength="10" Text='<%#Eval("AID_QTY") %>' ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("AST_MD_CODE") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Stock" HeaderText="Available Stock" ItemStyle-HorizontalAlign="left" />
                                                    <asp:TemplateField HeaderText="Qty From Stock" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtStockQty" runat="server" MaxLength="10" Text='<%#Eval("AID_MVM_QTY") %>' ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Qty To Purchase" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtPurchaseQty" runat="server" MaxLength="10" Text='<%#Eval("AID_ORD_QTY") %>' ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPurchaseQty"
                                                                Display="None" ErrorMessage="Please Enter Qty To Purchase!" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnUpdate" Text="Update" CssClass="btn btn-primary custom-button-color" runat="server" CommandName="UpdateRecord" CommandArgument='<%#Eval("AST_MD_CODE") %>'></asp:Button>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div id="tr1" runat="Server" class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Remarks</label>
                                        <asp:TextBox ID="txtapprovetorem" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="btnSubmit" Text="Approve" runat="server" CssClass="btn btn-primary custom-button-color" />
                                            <asp:Button ID="btnCancel" Text="Reject" runat="server" CssClass="btn btn-primary custom-button-color" />
                                            <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" OnClick="btnBack_Click" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
<script type="text/ecmascript" defer>
    function refreshSelectpicker() {
        $("#<%=ddlAstCat.ClientID%>").selectpicker();
        $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
        $("#<%=ddlAstBrand.ClientID%>").selectpicker();
        $("#<%=ddlLocation.ClientID%>").selectpicker();
        $("#<%=ddlAstModel.ClientID%>").selectpicker();
        $("#<%=ddlEmp.ClientID%>").selectpicker();

    };
    refreshSelectpicker();
</script>







