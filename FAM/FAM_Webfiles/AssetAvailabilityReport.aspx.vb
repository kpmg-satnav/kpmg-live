﻿Imports System.Data
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager

Imports SubSonic

Partial Class FAM_FAM_Webfiles_AssetAvailabilityReport
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            If Not IsPostBack Then
                ReportViewer1.Visible = False
                'Bind_Asset_Categories()
                getassetbrand()
                'Get_Asset_Report()
            End If

        Catch exp As System.Exception
            Response.Write(exp.Message)
        End Try
    End Sub

    Public Sub Bind_Asset_Categories()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_REQUISITION_GetAsset")
        sp.Command.AddParameter("@categoryID", ddlCat.SelectedItem.Value, DbType.String)
        ddlasset.DataSource = sp.GetDataSet()
        ddlasset.DataTextField = "AAT_NAME"
        ddlasset.DataValueField = "AAT_CODE"
        ddlasset.DataBind()
        ddlasset.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub getassetbrand()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlCat.DataSource = sp.GetDataSet()
        ddlCat.DataTextField = "VT_TYPE"
        ddlCat.DataValueField = "VT_CODE"
        ddlCat.DataBind()
        ddlCat.Items.Insert(0, "--Select--")

    End Sub

    Private Sub Get_Asset_Report()
        Dim Category As String = ""
        Dim Asset As String = ""

        Try
            If ddlCat.SelectedItem.Value = "--All--" Then
                Category = ""
            Else
                Category = ddlCat.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try

        Try

            If ddlasset.SelectedItem.Value = "--All--" Then
                Asset = "All"
            Else
                Asset = ddlasset.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ASSET_AVAIL_REPORT")
        sp.Command.AddParameter("@categoryID", Category, DbType.String)
        sp.Command.AddParameter("@SubcategoryID", Asset, DbType.String)

        Dim ds As New DataSet
        ds = sp.GetDataSet()

        Dim rds As New ReportDataSource()
        rds.Name = "ViewAssetAvailability"
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/ViewAssetAvailability.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
    End Sub

    Protected Sub ddlCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCat.SelectedIndexChanged
        ReportViewer1.Visible = False
        lblMsg.Visible = False
        ddlasset.Enabled = True
        ddlasset.Items.Clear()
        ddlasset.Items.Insert("0", "--All--")
        If ddlCat.SelectedItem.Value <> "--All--" Then
            Dim param() As SqlParameter = New SqlParameter(0) {}
            param(0) = New SqlParameter("@categoryID", SqlDbType.NVarChar, 200)
            param(0).Value = ddlCat.SelectedItem.Value()
            ObjSubSonic.Binddropdown(ddlasset, "USP_AMG_ITEM_REQUISITION_GetAsset", "AAT_NAME", "AAT_CODE", param)
        End If
    End Sub

    Protected Sub ddlasset_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlasset.SelectedIndexChanged
        lblMsg.Text = ""
        ReportViewer1.Visible = False
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        lblMsg.Visible = False
        Get_Asset_Report()
    End Sub
End Class
