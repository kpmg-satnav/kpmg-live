<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmViewPODetails.aspx.vb" Inherits="FAM_FAM_Webfiles_frmViewPODetails" title="PO Details" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript" defer></script>
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">PO Details
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>PO Details</strong></td>
                <td>
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="center" valign="top" height="100%">
                    <table width="100%" cellpadding="2px">
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Label ID="lblMsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" valign="top">
                                <asp:Panel ID="pnlItems" runat="server" Width="100%">
                                    <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                        EmptyDataText="No Asset(s) Found." Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="sku" HeaderText="Asset Code" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="productname" HeaderText="Asset Name" ItemStyle-HorizontalAlign="left" />
                                            <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AIPD_QTY") %>' ReadOnly="true"></asp:TextBox>
                                                    <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("ProductId") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           <%-- <asp:BoundField DataField="Stock" HeaderText="Available Stock" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" DataFormatString="Rs. {0}" ItemStyle-HorizontalAlign="left" Visible="false" />--%>
                                            <asp:TemplateField HeaderText="Unit Price" ItemStyle-HorizontalAlign="left" >
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtUnitPrice" runat="server" Width="50px" MaxLength="10" Text='<%#formatnumber(Eval("AIPD_RATE"),2) %>' ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           <%-- <asp:TemplateField HeaderText="Qty From Stock" ItemStyle-HorizontalAlign="left" Visible="false">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtStockQty" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AID_MVM_QTY") %>' ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Qty To Purchase" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtPurchaseQty" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AIPD_ACT_QTY") %>' ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="Price" ItemStyle-HorizontalAlign="left" Visible="false">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtPrice" runat="server" Width="50px" MaxLength="10" Text='<%#formatnumber(Eval("Price"),2) %>'  ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Price" HeaderText="Price" DataFormatString="Rs. {0}" ItemStyle-HorizontalAlign="left" Visible="false" />
                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnUpdate" Text="Update" CssClass="button" runat="server" CommandName="UpdateRecord" CommandArgument='<%#Eval("ProductId") %>'></asp:Button>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                    <table width="100%" padding="2px">
                                        <tr>
                                            <td colspan="2" align="center">Tax Details</td>
                                            <td colspan="2" align="center">Payment Terms</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Entry Tax</td>
                                            <td>
                                                <asp:TextBox ID="txtFFIP" runat="server" width="275px" Text="0" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td>Advance</td>
                                            <td>
                                                <asp:TextBox ID="txtAdvance" runat="server" width="275px" Text="0" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>CST/ST</td>
                                            <td>
                                                <asp:TextBox ID="txtCst" runat="server" width="275px" Text="0" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td>On Delivery</td>
                                            <td>
                                                <asp:TextBox ID="txtDelivery" runat="server" width="275px" Text="0" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                VAT</td>
                                            <td>
                                                <asp:TextBox ID="txtWst" runat="server" width="275px" Text="0" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td>Installation</td>
                                            <td>
                                                <asp:TextBox ID="txtInstallation" runat="server" width="275px" Text="0" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Octroi</td>
                                            <td>
                                                <asp:TextBox ID="txtOctrai" runat="server" width="275px" Text="0" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td>Commissioning</td>
                                            <td>
                                                <asp:TextBox ID="txtCommissioning" runat="server" width="275px" Text="0" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Service Tax</td>
                                            <td>
                                                <asp:TextBox ID="txtServiceTax" runat="server" width="275px" Text="0" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td>Retention</td>
                                            <td>
                                                <asp:TextBox ID="txtRetention" runat="server" width="275px" Text="0" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Others</td>
                                            <td>
                                                <asp:TextBox ID="txtOthers" runat="server" width="275px" Text="0" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td>Payments</td>
                                            <td>
                                                <asp:TextBox ID="txtPayments" runat="server" width="275px" Text="0" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trCalcTotalCost" runat="server" visible="false">
                                            <td colspan="4" align="center">
                                                <asp:Button ID="btnTotalCost" runat="server" Text="Get Total Cost" CssClass="button" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Total Cost</td>
                                            <td>
                                                <asp:TextBox ID="txtTotalCost" runat="server" width="275px" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td>Vendor</td>
                                            <td>
                                                <asp:DropDownList ID="ddlVendor" runat="server" width="275px" Enabled="False"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 26px">Requestor Id</td>
                                            <td style="height: 26px">
                                                <asp:TextBox ID="txtRequesterId" runat="server" width="275px" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td style="height: 26px">Requestor Name</td>
                                            <td style="height: 26px">
                                                <asp:TextBox ID="txtRequesterName" runat="server" width="275px" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Requestor Dept</td>
                                            <td>
                                                <asp:TextBox ID="txtDeptId" runat="server" width="275px" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td>Requestor Location</td>
                                            <td>
                                                <asp:TextBox ID="txtLocation" runat="server" width="275px" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Expected Date Of Delivery</td>
                                            <td>
                                                <ew:calendarpopup id="txtDOD" runat="server" Enabled="False"></ew:calendarpopup>
                                                <%--<asp:TextBox ID="txtDOD" runat="server" width="275px"></asp:TextBox>--%>
                                            </td>
                                            <td>Exchange Charges</td>
                                            <td>
                                                <asp:TextBox ID="txtExchange" runat="server" width="275px" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Requestor Remarks</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtRequestorRemarks" runat="server" width="475px" TextMode="multiline" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>PO Raised By</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtPORaisedBy" runat="server" width="475px" TextMode="multiline" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr  id="trRemarks" runat="server" visible="false">
                                            <td>Remarks</td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtRemarks" runat="server" width="475px" TextMode="multiline"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr id="trSubmit" runat="server" visible="false">
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnSubmit" Text="Approve" runat="server" CssClass="button" Visible="false" />&nbsp;
                                <asp:Button ID="btnUpdate" Text="Update and Approve" runat="server" CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>

</asp:Content>

