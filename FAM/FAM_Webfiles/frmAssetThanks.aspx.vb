Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common
Imports System.Data.SqlClient

Partial Class FAM_FAM_Webfiles_frmAssetThanks
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim ReqId As String = GetRequestId()
            Dim Message As String = ""
            If String.IsNullOrEmpty(ReqId) AndAlso String.IsNullOrEmpty(Request("PO")) Then
                Message = "Sorry! Unknown problem occurred while raising Asset Requisition."
                lblMsg.Text = Message
            ElseIf String.IsNullOrEmpty(ReqId) Then
                Message = "PO  (" + Request("PO") + ") Generated."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "assetlabels" Then
                Message = "Asset Labels Generated Successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "intramovement" Then
                Message = "Intra movement request submitted successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "intermovement" Then
                Message = "Inter movement requisition (" + Request("reqid") + ") Has Been Raised Successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "astreject" Then
                Dim strReq_id As String = ""
                strReq_id = Request.QueryString("Req_id")
                Message = "Asset Rejection request (<b>" & strReq_id & "</b>) submitted successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "maintsc" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "<b><center><br><br> Requisition ID : " & MaintSc_ReqId & "<br>Has Been " & Request.QueryString("staid") & "updated.<br><br>Thank You For Using The System.</center>"
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "editmaintsc" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "<b><center><br><br> Requisition ID : " & MaintSc_ReqId & "<br>Has Been " & Request.QueryString("staid") & "updated.<br><br>Thank You For Using The System.</center>"
                lblMsg.Text = Message
                '*****************
            ElseIf LCase(ReqId) = "eintmvmt" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Inter movement requisition (" + Request("reqid") + ") Updated  Successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "mvmtcancelled" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Inter movement requisition (" + Request("reqid") + ") Canceled successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "surrenderreq" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Surrender request submitted successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "surrenderreqrmapprove" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Surrender request approved successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "surrenderreqrmreject" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Surrender request rejected successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "EIntMVMTModify" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Inter movement request modified successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "intermvmtit" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Inter movement IT request Approved successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "intermvmtitrej" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Inter movement IT request rejected."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "intermvmtitdetails" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Inter movement IT Request successfully approved."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "assetmovementnotegenerated" Then
                Dim AMN_ReqId As String
                AMN_ReqId = Request.QueryString("MReqId")
                Message = "Asset Movement Note Generated Successfully."
                lblMsg.Text = Message
                'ElseIf LCase(ReqId) = "intermvmtitrej" Then
                '    Dim MaintSc_ReqId As String
                '    MaintSc_ReqId = Request.QueryString("MReqId")
                '    Message = "Inter movement IT request rejected."
                '    lblMsg.Text = Message
            ElseIf LCase(ReqId) = "outwardapp" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Request successfully approved."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "outwardrej" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Request rejected."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "inwardapp" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Request successfully approved."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "gengatepass" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Gate pass generated successfully."
                lblMsg.Text = Message

            ElseIf LCase(ReqId) = "itemreq" Then
                lblHead.Text = "Item Requisition Status"
                'lblSubHead.Text = "Item Requisition Status"
                'pnlMain.GroupingText = "Item Requisition Status"
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("ReqId")
                Dim StatusIdItem As Integer = GetRequestStatusItem(MaintSc_ReqId)

                Select Case StatusIdItem
                    Case 1027
                        Message = "Capital Item Requisition (" + MaintSc_ReqId + ") raised successfully."

                End Select

                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "itemreqmodified" Then
                lblHead.Text = "Item Requisition Status"
                'lblSubHead.Text = "Item Requisition Status"
                'pnlMain.GroupingText = "Item Requisition Status"
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("ReqId")
                Dim StatusIdItem As Integer = GetRequestStatusItem(MaintSc_ReqId)

                Select Case StatusIdItem
                    Case 1028
                        Message = "Capital Item Requisition (" + MaintSc_ReqId + ") has been modified successfully."
                    Case 1029
                        Message = "Capital Item Requisition (" + MaintSc_ReqId + ") has been canceled successfully."
                End Select

                lblMsg.Text = Message

            ElseIf LCase(ReqId) = "itemreqrmapproved" Then
                lblHead.Text = "Item Requisition Status"
                'lblSubHead.Text = "Item Requisition Status"
                'pnlMain.GroupingText = "Item Requisition Status"
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("ReqId")
                Dim StatusIdItem As Integer = GetRequestStatusItem(MaintSc_ReqId)

                Select Case StatusIdItem
                    Case 1030
                        Message = "Capital Item Requisition (" + MaintSc_ReqId + ") has been Approved by RM successfully."
                    Case 1031
                        Message = "Capital Item Requisition (" + MaintSc_ReqId + ") has been Canceled by RM successfully."
                End Select

                lblMsg.Text = Message
		 ElseIf LCase(ReqId) = "addstock" Then
                Message = "Stock Added Successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "modifystock" Then
                Message = "Stock Modified Successfully."
                lblMsg.Text = Message
                '*********************
            ElseIf LCase(ReqId) = "itemreqrmapprovedbysm" Then
                lblHead.Text = "Item Requisition Status"
                'lblSubHead.Text = "Item Requisition Status"
                'pnlMain.GroupingText = "Item Requisition Status"
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("ReqId")
                Dim StatusIdItem As Integer = GetRequestStatusItem(MaintSc_ReqId)

                Select Case StatusIdItem
                    Case 1032
                        Message = "Capital Item Requisition (" + MaintSc_ReqId + ") has been Approved by SM successfully."
                    Case 1033
                        Message = "Capital Item Requisition (" + MaintSc_ReqId + ") has been Canceled by SM successfully."
					Case 1034
                        Message = "Capital Item Requisition (" + MaintSc_ReqId + ") has been Pending by SM successfully."
                End Select

                lblMsg.Text = Message

                '*********************
            Else

                Dim StatusId As Integer = GetRequestStatus(ReqId)
                Select Case StatusId
                    Case 1001
                        Message = "Capital Asset Requisition (" + ReqId + ") has been raised successfully."
                    Case 1002
                        Message = "Capital Asset Requisition (" + ReqId + ") has been updated successfully."
                    Case 1003
                        Message = "Capital Asset Requisition (" + ReqId + ") has been canceled successfully."
                    Case 1004
                        Message = "Capital Asset Requisition (" + ReqId + ") has been approved."
                    Case 3014
                        Message = "Capital Asset Requisition (" + ReqId + ") has been approved."
                    Case 1005
                        Message = "Capital Asset Requisition (" + ReqId + ") has been rejected."
                    Case 1006
                        Message = "Capital Asset Requisition (" + ReqId + ") has been approved by Admin."
                    Case 1007
                        Message = "Capital Asset Requisition (" + ReqId + ") has been rejected by Admin."
                    Case 1008
                        If Session("ITM_TYPE") = "1" Then
                            Message = "Consumable Asset Requisition (" + ReqId + ") has been Raised by RM."
                        Else
                            Message = "Capital Asset Requisition (" + ReqId + ") has been approved by RM."

                        End If
                    Case 1009
                        Message = "Capital Asset Requisition (" + ReqId + ") has been rejected by RM."
                    Case 1010
                        'Message = "Capital Asset Requisition (" + ReqId + ") has been approved by Coordinator Check."

                        Message = "Capital Asset Requisition (" + ReqId + ") , Intra Movment Count:-(" + Request("IntraCount") + ") And Inter Movement Count:- (" + Request("InterCount") + ")  has been approved by Coordinator Check."

                    Case 1011
                        Message = "Capital Asset Requisition (" + ReqId + ") has been rejected by Coordinator Check."
                    Case 1012
                        Message = "Asset Requisition (" + ReqId + ") has been approved for procurement."
                    Case 1013
                        Message = "Asset Requisition (" + ReqId + ") has been rejected for procurement."
                    Case 1014
                        Message = "PO (" + Request("PO") + ") has been generated for capital asset requisition (" + ReqId + ")."
                    Case 1015
                        Message = "Capital Asset Requisition (" + ReqId + ") has been updated successfully."
                    Case 1016
                        Message = "Capital Asset Requisition (" + ReqId + ") has been updated successfully."
                    Case 3015
                        Message = "Capital Asset Requisition (" + ReqId + ") has been approved by L2 Level."
                    Case 1501
                        Message = "Consumable Asset Requisition (" + ReqId + ") has been raised successfully."
                    Case 1502
                        Message = "Consumable Asset Requisition (" + ReqId + ") has been updated successfully."
                    Case 1503
                        Message = "Consumable Asset Requisition (" + ReqId + ") has been canceled successfully."
                    Case 1504
                        Message = "Consumable Asset Requisition (" + ReqId + ") has been approved by L1."
                    Case 1505
                        Message = "Consumable Asset Requisition (" + ReqId + ") has been rejected by L1."
                    Case 1506
                        Message = "Consumable Asset Requisition (" + ReqId + ") has been approved by Admin."
                    Case 1507
                        Message = "Consumable Asset Requisition (" + ReqId + ") has been rejected by Admin."
                    Case 1508
                        Message = "Consumable Asset Requisition (" + ReqId + ") is ready for Issuance."
                    Case 1509
                        Message = "Consumable Asset Requisition (" + ReqId + ") Issued Successfully."
                    Case 1510
                        Message = "Consumable Asset Requisition (" + ReqId + ") has been canceled By L2."
                    Case 1511
                        Message = "Consumable Asset Requisition (" + ReqId + ") has been Approved and issued (Partially) By L2."
                    Case 1512
                        Message = "Consumable Asset Requisition (" + ReqId + ") has been Completed Successfully."
                End Select

                lblMsg.Text = Message
                SendMailTuUser()
                SendMailToRM()
            End If
        End If
    End Sub
    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function
    Private Function GetRequestStatus(ByVal ReqId As String) As String
        Dim StatusId As Integer = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_REQUISITION_GetByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, Data.DbType.String)
        Dim dr As sqlDatareader = sp.GetReader
        If dr.Read() Then
            Integer.TryParse(dr("AIR_STA_ID"), StatusId)
            Session("ITM_TYPE") = dr("CONS_STATUS").ToString()
        End If
        Return StatusId
    End Function
    Private Function GetRequestStatusItem(ByVal ReqId As String) As String
        Dim StatusId As Integer = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_CONSBLE_ITEM_REQUISITION_GetByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, Data.DbType.String)
        Dim dr As SqlDataReader = sp.GetReader
        If dr.Read() Then
            Integer.TryParse(dr("AIR_STA_ID"), StatusId)
        End If
        Return StatusId
    End Function
    Private Sub SendMailTuUser()

    End Sub
    Private Sub SendMailToRM()

    End Sub
End Class
