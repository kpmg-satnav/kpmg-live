Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class FAM_FAM_Webfiles_frmViewAssetsinStock
    Inherits System.Web.UI.Page

    Dim totapproved As Integer

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))
    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))
    'End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            BindGrid()
            ' BindGridInter()


        End If
        totapproved = CInt(Request.QueryString("qty"))
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AXIS_GETASSETS_INSTOCK")
            sp.Command.AddParameter("@AssetCode", Request.QueryString("id"), DbType.String)
            sp.Command.AddParameter("@REQ_ID", Request.QueryString("req_id"), DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
            If gvItems.Rows.Count = 0 Then
                pnlItemsInter.Visible = True
                btnSubmit.Visible = False
                BindGridInter()
            End If
            For Each drow As GridViewRow In gvItems.Rows
                Dim ddlAssets As DropDownList = DirectCast(drow.FindControl("ddlassets"), DropDownList)
                Dim lbllocation As Label = DirectCast(drow.FindControl("lbllocation"), Label)
                Dim ddlEmp As DropDownList = DirectCast(drow.FindControl("ddlEmp"), DropDownList)
                'Dim ddlTower As DropDownList = DirectCast(drow.FindControl("ddlTower"), DropDownList)

                'loadtower(lbllocation.Text, ddlTower)
                Emp_Loadddl(ddlEmp, lbllocation.Text)
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGridInter()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GETASSETS_INSTOCKINTER")
            sp.Command.AddParameter("@AssetCode", Request.QueryString("id"), DbType.String)
            sp.Command.AddParameter("@REQ_ID", Request.QueryString("req_id"), DbType.String)
            gvItemsInter.DataSource = sp.GetDataSet()
            gvItemsInter.DataBind()
            'For Each drow As GridViewRow In gvItemsInter.Rows

            'Dim ddlAssets As DropDownList = DirectCast(drow.FindControl("ddlassets"), DropDownList)
            'Dim lbllocation As Label = DirectCast(drow.FindControl("lbllocation"), Label)
            'Dim ddlEmp As DropDownList = DirectCast(drow.FindControl("ddlEmp"), DropDownList)
            'Dim ddlTower As DropDownList = DirectCast(drow.FindControl("ddlTower"), DropDownList)
            'loadtower(lbllocation.Text, ddlTower)
            'Emp_Loadddl(ddlEmp, lbllocation.Text)

            ' Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub loadtower(ByVal location_name As String, ByVal ddlTower As DropDownList)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_TOWERBYLOC")
        ' sp.Command.AddParameter("@LCM_CODE", Session("UID"), DbType.String)
        sp.Command.AddParameter("@LCM_CODE", location_name, DbType.String)
        ddlTower.DataSource = sp.GetDataSet()
        ddlTower.DataTextField = "TWR_NAME"
        ddlTower.DataValueField = "TWR_CODE"
        ddlTower.DataBind()
        ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))

    End Sub
    'Private Sub Emp_Loadddl(ByVal ddlEmp As DropDownList, ByVal locationid As String, ByVal tower_code As String, ByVal floor_code As String)
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_SPACE_ASSETMAPPING")
    '        ' sp.Command.AddParameter("@LCM_CODE", Session("UID"), DbType.String)
    '        sp.Command.AddParameter("@LCM_CODE", locationid, DbType.String)
    '        sp.Command.AddParameter("@TWR_CODE", tower_code, DbType.String)
    '        sp.Command.AddParameter("@FLR_CODE", floor_code, DbType.String)
    '        ddlEmp.DataSource = sp.GetDataSet()
    '        ddlEmp.DataTextField = "SPC_ID"
    '        ddlEmp.DataValueField = "SPC_VIEW_NAME"
    '        ddlEmp.DataBind()
    '        ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    Private Sub Emp_Loadddl(ByVal ddlEmp As DropDownList, ByVal locationid As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AEMP_LOC")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@LOC_ID", locationid, DbType.String)
            ddlEmp.DataSource = sp.GetDataSet()
            ddlEmp.DataTextField = "AUR_FIRST_NAME"
            ddlEmp.DataValueField = "AUR_ID"
            ddlEmp.DataBind()
            ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim count As Integer = 0
            Dim count1 As Integer = 0
            Dim totqty As Integer = totapproved
            For Each row As GridViewRow In gvItems.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                ' Dim ddlEmp As DropDownList = DirectCast(row.FindControl("ddlEmp"), DropDownList)
                'Dim ddlTower As DropDownList = DirectCast(row.FindControl("ddlTower"), DropDownList)
                'Dim ddlFloor As DropDownList = DirectCast(row.FindControl("ddlFloor"), DropDownList)
                If chkSelect.Checked Then
                    'If ddlTower.SelectedItem.Text = "--Select--" Then
                    '    lblMsg.Visible = True
                    '    lblMsg.Text = "Please select tower for selected checkbox"
                    '    Exit Sub
                    'ElseIf ddlFloor.SelectedItem.Text = "--Select--" Then
                    '    lblMsg.Visible = True
                    '    lblMsg.Text = "Please select floor selected checkbox"
                    '    Exit Sub
                    'If ddlEmp.SelectedItem.Text = "--Select--" Then
                    '    lblMsg.Visible = True
                    '    lblMsg.Text = "Please select space id selected checkbox"
                    '    Exit Sub
                    'Else
                    count = count + 1

                    '(Integer.Parse(lblProductId1.Text), ddlPO.SelectedItem.Value, Integer.Parse(txtQty.Text))
                    'End If

                End If

            Next
            If count = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "Please select the Asset(s) to map with employee..."
                Exit Sub
            End If
            If count <= totapproved Then
                For Each row As GridViewRow In gvItems.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                    Dim ddlEmp As DropDownList = DirectCast(row.FindControl("ddlEmp"), DropDownList)
                    Dim ddlTower As DropDownList = DirectCast(row.FindControl("ddlTower"), DropDownList)
                    Dim ddlFloor As DropDownList = DirectCast(row.FindControl("ddlFloor"), DropDownList)
                    Dim AssetCode As Label = DirectCast(row.FindControl("lblassetName"), Label)
                    Dim lbllocation As Label = DirectCast(row.FindControl("lbllocation"), Label)
                    If chkSelect.Checked Then
                        If ddlEmp.SelectedIndex > 0 Then
                            'If ddlTower.SelectedItem.Text = "--Select--" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Please select tower for selected checkbox"
                            '    Exit Sub
                            'ElseIf ddlFloor.SelectedItem.Text = "--Select--" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Please select floor selected checkbox"
                            '    Exit Sub
                            'ElseIf ddlEmp.SelectedItem.Text = "--Select--" Then
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Please select space id selected checkbox"
                            '    Exit Sub
                            'Else
                            '    count1 = count1 + 1
                            'Start inserting asset data


                            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_AMT_ASSET_SPACE_STAT")
                            sp1.Command.AddParameter("@LOC_ID", lbllocation.Text, DbType.String)
                            sp1.Command.AddParameter("@TWR_ID", "", DbType.String)
                            sp1.Command.AddParameter("@FLR_ID", "", DbType.String)
                            sp1.Command.AddParameter("@AAT_AST_CODE", AssetCode.Text, DbType.String)
                            sp1.ExecuteScalar()
                            'Dim ddlAssets As DropDownList = CType(gvRow.FindControl("ddlAssets"), DropDownList)
                            'Dim ddlEmp As DropDownList = CType(gvRow.FindControl("ddlEmp"), DropDownList)
                            'Asset = ddlAssets.SelectedItem.Value
                            'Emp = ddlEmp.SelectedItem.Value

                            ' ALLOCATE_SPACEASSET
                            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ALLOCATE_SPACEASSET_INTRA")
                            sp.Command.AddParameter("@AAT_AST_CODE", AssetCode.Text, DbType.String)
                            sp.Command.AddParameter("@AAT_SPC_ID", "", DbType.String)
                            sp.Command.AddParameter("@AAT_EMP_ID", Session("UID"), DbType.String)
                            sp.Command.AddParameter("@AAT_ITEM_REQUISITION", Request.QueryString("req_id"), DbType.String)
                            sp.ExecuteScalar()

                            '--------------- updating the asset tagged table to 1035 (Assigned)-------------
                            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDATE_ALLOCATE_ASSET")
                            sp2.Command.AddParameter("@AAT_AST_CODE", AssetCode.Text, DbType.String)
                            sp2.Command.AddParameter("@AAT_EMP_ID", ddlEmp.SelectedValue, DbType.String)
                            sp2.Command.AddParameter("@AAT_REQ_ID", Request.QueryString("req_id"), DbType.String)
                            sp2.ExecuteScalar()
                            send_mail(Request.QueryString("req_id"), AssetCode.Text)
                            'End inserting asset data
                            '(Integer.Parse(lblProductId1.Text), ddlPO.SelectedItem.Value, Integer.Parse(txtQty.Text))
                            'End If
                        Else
                            lblMsg.Text = "Please select Employee to map the asset."
                            Exit Sub
                        End If

                    End If
                Next

                Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_UPDATE_MVMNTQUAN")
                sp3.Command.AddParameter("@REQ_ID", Request.QueryString("req_id"), DbType.String)
                sp3.Command.AddParameter("@AST_CODE", Request.QueryString("id"), DbType.String)
                sp3.Command.AddParameter("@APPRVDQTY", count, DbType.Int32)
                sp3.ExecuteScalar()
                totapproved = totapproved - count
                lblMsg.Visible = True
                lblMsg.Text = "Asset mapped successfully..."
                pnlItems.Visible = False
                'End after apporving inter movement assets in same location update the qunatity for the particular request
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Please select requested no.of asset(S) only..."
            End If
            'Start after apporving inter movement assets in same location update the qunatity for the particular request




        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub

    Public Sub send_mail(ByVal reqid As String, ByVal AAT_CODE As String)
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_EMP_MAP")
        sp1.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp1.Command.AddParameter("@AAT_CODE", AAT_CODE, DbType.String)
        sp1.Execute()
    End Sub
    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddl As DropDownList = CType(sender, DropDownList)
        Dim row As GridViewRow = CType(ddl.NamingContainer, GridViewRow)
        Dim index As Integer = row.RowIndex
        Dim lbllocation As Label = DirectCast(gvItems.Rows(index).FindControl("lbllocation"), Label)
        Dim ddlTower As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlTower"), DropDownList)
        Dim ddlFloor As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlFloor"), DropDownList)
        If ddlTower.SelectedItem.Text <> "--Select--" Then
            loadfloor(lbllocation.Text, ddlTower.SelectedItem.Value, ddlFloor)
        Else
            ddlFloor.Items.Clear()
        End If
    End Sub
    Private Sub loadfloor(ByVal location_code As String, ByVal tower_code As String, ByVal ddlFloor As DropDownList)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_FLOORBYLOCTOWER")
        ' sp.Command.AddParameter("@LCM_CODE", Session("UID"), DbType.String)
        sp.Command.AddParameter("@LCM_CODE", location_code, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", tower_code, DbType.String)
        ddlFloor.DataSource = sp.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    'Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim ddl As DropDownList = CType(sender, DropDownList)
    '    Dim row As GridViewRow = CType(ddl.NamingContainer, GridViewRow)
    '    Dim index As Integer = row.RowIndex
    '    Dim lbllocation As Label = DirectCast(gvItems.Rows(index).FindControl("lbllocation"), Label)
    '    Dim ddlEmp As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlEmp"), DropDownList)
    '    Dim ddlTower As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlTower"), DropDownList)
    '    Dim ddlFloor As DropDownList = DirectCast(gvItems.Rows(index).FindControl("ddlFloor"), DropDownList)
    '    If ddlFloor.SelectedItem.Value <> "--Select" Then
    '        Emp_Loadddl(ddlEmp, lbllocation.Text, ddlTower.SelectedItem.Value, ddlFloor.SelectedItem.Value)
    '    Else
    '        ddlEmp.Items.Clear()
    '    End If
    'End Sub
    Protected Sub btnSubmitInter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitInter.Click
        Try
            Dim count As Integer = 0
            Dim count1 As Integer = 0
            Dim totqty As Integer = totapproved
            For Each row As GridViewRow In gvItemsInter.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim ddlEmp As DropDownList = DirectCast(row.FindControl("ddlEmp"), DropDownList)
                If chkSelect.Checked Then
                    count = count + 1
                End If
            Next
            Dim location As String = ""
            location = getlocationofuser(Session("Uid"))
            Dim ReqId As String = ""
            If count = 0 Then
                lblMsg.Text = "Please select atleast any one of the assets."
                Exit Sub
            End If
            'ReqId = GenerateRequestId(location, "", "", Session("Uid"))
            If count <= totapproved Then
                For Each row As GridViewRow In gvItemsInter.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                    Dim lblassetName As Label = DirectCast(row.FindControl("lblassetName"), Label)
                    Dim lblLocation As Label = DirectCast(row.FindControl("lbllocation"), Label)
                    If chkSelect.Checked Then
                        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_MMT_INTER_MVMT_REQ_Insert")
                        sp.Command.AddParameter("@MMR_REQ_ID", "REQ", DbType.String)
                        sp.Command.AddParameter("@MMR_AST_CODE", lblassetName.Text, DbType.String)
                        sp.Command.AddParameter("@MMR_FROMBDG_ID", lblLocation.Text, DbType.String)
                        sp.Command.AddParameter("@MMR_FROMFLR_ID", "", DbType.String)
                        sp.Command.AddParameter("@MMR_RAISEDBY", Session("UID"), DbType.String)
                        sp.Command.AddParameter("@MMR_RECVD_BY", Session("UID"), DbType.String)
                        sp.Command.AddParameter("@MMR_MVMT_TYPE", "", DbType.String)
                        sp.Command.AddParameter("@MMR_MVMT_PURPOSE", "", DbType.String)
                        sp.Command.AddParameter("@MMR_LRSTATUS_ID", 1017, DbType.Int32)
                        sp.Command.AddParameter("@MMR_COMMENTS", "", DbType.String)
                        sp.Command.AddParameter("@MMR_FROMEMP_ID", 0, DbType.String)
                        sp.Command.AddParameter("@MMR_ITEM_REQUISITION", Request.QueryString("req_id"), DbType.String)
                        sp.ExecuteScalar()
                    End If
                Next
                'Start after apporving inter movement assets in different location update the qunatity for the particular request
                Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_UPDATE_MVMNTQUAN")
                sp3.Command.AddParameter("@REQ_ID", Request.QueryString("req_id"), DbType.String)
                sp3.Command.AddParameter("@AST_CODE", Request.QueryString("id"), DbType.String)
                sp3.Command.AddParameter("@APPRVDQTY", count, DbType.Int32)
                sp3.ExecuteScalar()
                'End after apporving inter movement assets in different location update the qunatity for the particular request
                totapproved = totapproved - count
                lblMsg.Visible = True
                lblMsg.Text = "Asset mapped successfully..."
                pnlItemsInter.Visible = False
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Please select requested no.of asset(S) only..."
            End If


            If count = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "Please select the Asset(s) to map with employee..."
                Exit Sub
            End If

        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub
    Private Function GenerateRequestId(ByVal LocId As String, ByVal TowerId As String, ByVal FloorId As String, ByVal EmpId As String) As String
        Try
            Dim locationto As String = getlocationofuser(EmpId)
            Dim ReqId As String = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_MMT_INTER_MVMT_REQ_GetMaxMMR_ID")
            Dim SNO As String = CStr(sp.ExecuteScalar())
            'ReqId = "MMR/" + LocId + "/" + TowerId + "/" + FloorId + "/" + EmpId + "/" + SNO
            ReqId = "MMR/" + LocId + "/" + locationto + "/" + EmpId + "/" + SNO
            Return ReqId
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Function
    Private Function getlocationofuser(ByVal empid As String) As String
        Dim locationto As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_BTEMPID")
        sp.Command.AddParameter("@AUR_ID", empid, DbType.String)
        locationto = sp.ExecuteScalar()
        Return locationto
    End Function

    Protected Sub gvItemsInter_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItemsInter.PageIndexChanging
        gvItemsInter.PageIndex = e.NewPageIndex()
        BindGridInter()
    End Sub
End Class
