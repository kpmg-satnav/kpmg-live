Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class frmAstLocationSummary
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Dim Prd_id As Integer
    Dim strLocation As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Prd_id = Request.QueryString("id")

        If Not IsPostBack Then
            BindMappedAssets(Prd_id, strLocation)
            BindUnMappedAssets(Prd_id, strLocation)
        End If
    End Sub
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
        'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
        'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))
        'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))
        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))

        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
        'Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))
    End Sub
    Private Sub BindMappedAssets(ByVal PrdId As Integer, ByVal LCM_Code As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@PRD_ID", SqlDbType.Int)
        param(0).Value = PrdId
        ObjSubSonic.BindGridView(gvItems, "GET_MAPPED_ASTSLIST_PRODUCT", param)
    End Sub

    Private Sub BindUnMappedAssets(ByVal PrdId As Integer, ByVal LCM_Code As String)

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@PRD_ID", SqlDbType.Int)
        param(0).Value = PrdId
        ObjSubSonic.BindGridView(gvUnmappedAst, "GET_UNMAPPED_ASTSLIST_PRODUCT", param)

    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindMappedAssets(Prd_id, strLocation)

    End Sub

    Protected Sub gvUnmappedAst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvUnmappedAst.PageIndexChanging
        gvUnmappedAst.PageIndex = e.NewPageIndex
        BindUnMappedAssets(Prd_id, strLocation)
    End Sub
End Class