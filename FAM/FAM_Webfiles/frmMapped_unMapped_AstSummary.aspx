<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMapped_unMapped_AstSummary.aspx.vb"
    Inherits="FAM_FAM_Webfiles_frmMapped_unMapped_AstSummary" %>

<%@ Register Src="../../Controls/Mapped_unMapped_AstSummary.ascx" TagName="Mapped_unMapped_AstSummary"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Asset Status </title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
                border="0">
                <tr>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                            width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong>Asset Status</strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                            width="16" /></td>
                </tr>
                <tr>
                    <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                        &nbsp;</td>
                    <td align="left">
                        <uc1:Mapped_unMapped_AstSummary ID="Mapped_unMapped_AstSummary1" runat="server" />
                    </td>
                    <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                        height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                            width="9" /></td>
                    <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                            width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                            width="16" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
