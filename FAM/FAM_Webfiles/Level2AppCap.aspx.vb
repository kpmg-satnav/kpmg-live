﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports SqlHelper
Partial Class FAM_FAM_Webfiles_Level2AppCap

    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim CatId As String
    Dim asstsubcat As String
    Dim asstbrand As String
    Dim astmodel As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)

        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
                RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CAPT_LEVEL1_APPROVALS_GETBYAURID")
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvItems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItems.RowCommand
        Try
            If e.CommandName = "GetCaptApprovalDetails" Then
                hdnCANO.Value = e.CommandArgument
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim RowIndex As Integer = row.RowIndex
                'Dim ReqID As String = DirectCast(row.FindControl("lblReqID"), LinkButton).Text.ToString()
                Session("REQ_ID") = e.CommandArgument
                GetCaptApprovalDetails(hdnCANO.Value)
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GetCaptApprovalDetails(ByVal Reqid As String)
        'ol pg
        BindUsers()
        getassetcategory()
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        '  ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
        'getassetsubcategory()
        pnlItems.Visible = True
        ' BindRequisition()
        BindBasicReqDetails()
        BindGridSub()
        UpdatePanel.Visible = True
    End Sub

    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        ddlastCat.DataSource = sp.GetDataSet()
        ddlastCat.DataTextField = "VT_TYPE"
        ddlastCat.DataValueField = "VT_CODE"
        ddlastCat.DataBind()
        ' ddlAstCat.SelectedIndex = If(ddlAstCat.Items.Count > 1, 0, 0)
    End Sub

    Private Sub BindBasicReqDetails()
        Dim ReqId As String = Session("REQ_ID")
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AMG_ITEM_REQUISITION_GetByReqId_NP")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), Data.DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId

                'Dim RaisedBy As Integer = 0
                'Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)

                Dim RaisedBy As String
                RaisedBy = dr("AIR_AUR_ID")

                Dim Raised As String
                Raised = dr("AUR_KNOWN_AS")

                BindUsers()
                ddlEmp.ClearSelection()
                Dim li As ListItem = ddlEmp.Items.FindByValue(CStr(RaisedBy))
                If Not li Is Nothing Then
                    li.Selected = True
                End If
                txtemp.Text = RaisedBy + Raised

                'Dim CatId As Integer = 0
                'Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)
                'Dim CatId As String
                ddlAstCat.ClearSelection()
                CatId = dr("AIR_ITEM_TYPE")
                li = ddlAstCat.Items.FindByValue(CStr(CatId))

                If Not li Is Nothing Then
                    li.Selected = True
                End If
                '   ddlAstCat.Enabled = False


                getassetsubcategory(CatId)
                '   ddlAstSubCat.Enabled = False
                'Dim asstsubcat As String = dr("AIR_ITEM_SUBCAT")
                asstsubcat = dr("AIR_ITEM_SUBCAT")
                ddlAstSubCat.ClearSelection()
                ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True

                getbrandbycatsubcat(CatId, asstsubcat)

                'Dim asstbrand As String = dr("AIR_ITEM_BRD")
                asstbrand = dr("AIR_ITEM_BRD")
                ddlAstBrand.ClearSelection()
                ddlAstBrand.Items.FindByValue(asstbrand).Selected = True
                '  ddlAstBrand.Enabled = False


                getmakebycatsubcat()

                BindLocation1()

                Dim Loc As String = dr("AIR_REQ_LOC")
                ddlLocation.ClearSelection()
                ddlLocation.Items.FindByValue(Loc).Selected = True

                astmodel = dr("AIR_ITEM_MOD")
                ddlAstModel.ClearSelection()
                ddlAstModel.Items.FindByValue(astmodel).Selected = True

                txtRemarks.Text = dr("AIR_REMARKS")
                txtL1Remarks.Text = dr("AIR_APR1_REM")
                'txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                txtStatus.Text = dr("STA_TITLE")


                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)
            End If
        End If
    End Sub


    Private Sub BindLocation1()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
    End Sub

    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
        'ddlAstModel.Items.Insert(0, "--All--")
    End Sub

    Private Sub getassetsubcategory(ByVal assetcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Private Sub getbrandbycatsubcat(ByVal assetcode As String, ByVal assetsubcat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", assetsubcat, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--Select--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Dim dsCIRGrid As New DataSet()
    Private Sub BindGridSub()
        'Dim tickedcount = 0
        Dim ReqId As String = Session("REQ_ID") 'Request("RID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMGITEM_VIEW_CAPITAL_ASSET_REQUISITION")
        sp.Command.AddParameter("@AST_MD_CATID", CatId, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", asstsubcat, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", asstbrand, DbType.String)
        sp.Command.AddParameter("@AST_MD_MODEL_ID", astmodel, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQID", ReqId, DbType.String)
        dsCIRGrid = sp.GetDataSet
        gvCaptApprovals.DataSource = dsCIRGrid

        gvCaptApprovals.DataBind()

    End Sub

    Private Sub BindUsers()

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID")

        param(1) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("COMPANYID")

        ObjSubsonic.Binddropdown(ddlEmp, "AM_AMT_bindUsers_SP", "NAME", "AUR_ID", param)
        Dim li As ListItem = ddlEmp.Items.FindByValue(Session("UID"))
        If Not li Is Nothing Then
            li.Selected = True
        End If
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMT_GetUserDtls_SP")
        'ddlEmp.DataSource = sp.GetReader
        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "AUR_ID"
        'ddlEmp.DataBind()
        'ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Function GetRequestId() As String
        Dim ReqId As String = Session("REQ_ID")
        Return ReqId
    End Function

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Dim ReqId As String = GetRequestId()
        Dim strASSET_LIST As New ArrayList
        UpdateData(ReqId, Trim(txtL2Remarks.Text))
        'DeleteRequistionItems(ReqId)
        Dim i As Integer = 0
        For Each row As GridViewRow In gvCaptApprovals.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim lblProductname As Label = DirectCast(row.FindControl("lblProductname"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtEstCost As TextBox = DirectCast(row.FindControl("txtEstCost"), TextBox)
            If chkSelect.Checked Then
                InsertDetails(ReqId, lblProductId.Text, CInt(Trim(txtQty.Text)), CInt(Trim(txtEstCost.Text)), i)

                'strASSET_LIST.Insert(i, lblProductId.Text & "," & lblProductname.Text & "," & txtQty.Text)
            End If
            i += 1
        Next

        Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)
    End Sub

    Private Sub UpdateData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CAP_AssetRequisition_APP_REJ_BY_L2")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CatId", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@STATUS", 3015, DbType.Int32)
        sp.Command.AddParameter("@AIR_ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CompanyId", Session("CompanyId"), DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As String, ByVal Qty As Integer, ByVal EstCost As Integer, ByVal id As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_updatebyrm")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.Command.AddParameter("@EST_COST", EstCost, DbType.Int32)
        sp.Command.AddParameter("@CompanyId", Session("CompanyId"), DbType.String)
        sp.Command.AddParameter("@ITEM_TYPE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@ITEM_MOD", ddlAstModel.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@delsno", id, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        RejectData(GetRequestId, Trim(txtL2Remarks.Text))
        Response.Redirect("frmAssetThanks.aspx?RID=" + GetRequestId())
    End Sub

    Private Sub RejectData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CAP_AssetRequisition_APP_REJ_BY_L2")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CatId", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@STATUS", 1005, DbType.Int32)
        sp.Command.AddParameter("@AIR_ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CompanyId", Session("CompanyId"), DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        lblMsg.Visible = False
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_LEVEL2_APPROVAL_SEARCH")
        sp.Command.AddParameter("@search_criteria", txtSearch.Text, Data.DbType.String)
        sp.Command.AddParameter("@AURID", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
    End Sub
End Class


