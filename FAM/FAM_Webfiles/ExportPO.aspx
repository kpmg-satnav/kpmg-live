﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ExportPO.aspx.vb" Inherits="FAM_FAM_Webfiles_ExportPO" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script src="../../BlurScripts/BlurJs/jquery.js"></script>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="View Work Order" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">PO Details</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <div class="row table table table-condensed table-responsive">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                                </div>
                                <div class="col-md-12">
                                    <input type="hidden" id="lblpoid" name="abcName" runat="server" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        $(document).ready(function () {
            $("#ReportViewer1_ctl05_ctl04_ctl00_Menu a").click(function () {

                if ($(this).attr("title") == "PDF") {
                    event.preventDefault();
                    var param = document.getElementById('lblpoid').value;
                    $.ajax({
                        type: "POST",
                        url: 'http://localhost:50443/api/Utility/GetAssetPOid?Reqid=' + param + '',    // CALL WEB API TO SAVE THE FILES.
                        enctype: 'multipart/form-data',
                        contentType: false,
                        processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                        cache: false,
                        data: param,
                        dataType: "json",
                        //success: OnSuccess,
                        success: function (response) {
                            console.log(response.data)

                        },
                    });
                }
            });
        });
    </script>
</body>
</html>
