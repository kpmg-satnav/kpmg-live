﻿Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports clsSubSonicCommonFunctions
Imports System.IO
Partial Class FAM_FAM_Webfiles_Exports_StockUpdateStockHistory
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim export As New Export
    Dim ds As DataSet

    Private Sub ExportToExcel()
        Dim param(8) As SqlParameter

        param(0) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(0).Value = Request.QueryString("page")
        param(1) = New SqlParameter("@SortColumnName", SqlDbType.Char)
        param(1).Value = Request.QueryString("sidx").Replace("AssetCategory_list_", "")
        param(2) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
        param(2).Value = Request.QueryString("sord")
        param(3) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(3).Value = Request.QueryString("rows")
        param(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(4).Value = 100
        param(5) = New SqlParameter("@LCM_CODE ", SqlDbType.NVarChar, 200)
        param(5).Value = Request.QueryString("lt")
        param(6) = New SqlParameter("@TWR_CODE ", SqlDbType.NVarChar, 200)
        param(6).Value = Request.QueryString("tw")
        param(7) = New SqlParameter("@FLR_CODE ", SqlDbType.NVarChar, 200)
        param(7).Value = Request.QueryString("ft")
        param(8) = New SqlParameter("@AIM_CODE ", SqlDbType.NVarChar, 200)
        param(8).Value = Request.QueryString("as")
        ds = objsubsonic.GetSubSonicDataSet("GET_ITEM_STOCK_HISTORY_V", param)


        ds.Tables(0).Columns(1).ColumnName = "ASSET CODE"
        ds.Tables(0).Columns(2).ColumnName = "ASSET NAME"
        ds.Tables(0).Columns(3).ColumnName = "AVAILABLE STOCK"
        ds.Tables(0).Columns(4).ColumnName = "RECEIVED DATE"
        ds.Tables(0).Columns(5).ColumnName = "REMARKS"
        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        export.Export("Consumable_Stock_History_Report.xls", gv)

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ExportToExcel()
        End If
    End Sub
End Class
