﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IssuanceInvoice.aspx.cs" Inherits="FAM_FAM_Webfiles_IssuanceInvoice" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Item Issuance</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BootStrapCSS/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <link href="../../BootStrapCSS/amantra.min.css" rel='stylesheet' type='text/css' />
    <link href="../../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel='stylesheet' type='text/css' />
    <link href="../../BootStrapCSS/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/jtable/themes/jqueryui/jtable_jqueryui.css" rel="stylesheet" />
    
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        
                        <form id="form1" class="form-horizontal" runat="server">
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="515px"></rsweb:ReportViewer>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<script src="../../BootStrapCSS/knockout-3.3.0.js" defer></script>
<script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
<script src="../../BootStrapCSS/Scripts/bootstrap.min.js" defer></script>
<script src="../../BootStrapCSS/Scripts/bootstrap-select.min.js" defer></script>
