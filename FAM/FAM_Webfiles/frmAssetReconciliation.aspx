﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetReconciliation.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAssetReconciliation" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Asset Reconciliation" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Asset Reconciliation</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">

                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server"
                            CssClass="alert alert-danger"
                            ForeColor="red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label class="col-md-12 control-label">Select City<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                        Display="None" ErrorMessage="Please Select  City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                    <div class="col-md-7">
                                        <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                            OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label class="col-md-12 control-label">Select Location<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="rfvlocation" runat="server" ControlToValidate="ddlLocation"
                                        Display="None" ErrorMessage="Please Select  Location" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                    <div class="col-md-7">
                                        <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                        </asp:DropDownList>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label class="col-md-12 control-label">Select Tower</label>
                                    <asp:RequiredFieldValidator ID="rfvtower" runat="server" ControlToValidate="ddlTower"
                                        Display="None" ErrorMessage="Please Select Tower" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                    <div class="col-md-7">
                                        <asp:DropDownList ID="ddlTower" runat="server"
                                            CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-12 control-label">Select Floor</label>
                                        <asp:RequiredFieldValidator ID="rfvfloor" runat="server" ControlToValidate="ddlFloor"
                                            Display="None" ErrorMessage="Please Select  Floor" InitialValue="-- Select --"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlFloor" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <label class="col-md-12 control-label">Upload File (Only Excel)<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                                        ControlToValidate="fpBrowseDoc" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseDoc"
                                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only Excel file allowed"
                                        ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                                    </asp:RegularExpressionValidator>
                                    <div class="col-md-7">
                                        <div class="btn btn-default">
                                            <i class="fa fa-folder-open-o fa-lg"></i>
                                            <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">

                                    <div class="col-md-12 control-label ">
                                        <asp:HyperLink ID="hyp" runat="server" class="fa fa-download fa-2x" aria-hidden="true" NavigateUrl="~/Masters/Mas_Webfiles/Asset_Barcode_Template.xlsx" title="Download Template"></asp:HyperLink>
                                    </div>
                                    <div class="col-md-7 text-right">
                                        <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-primary custom-button-color" Text="Upload"></asp:Button>
                                        <asp:Button ID="btnrecon" CssClass="btn btn-primary custom-button-color" runat="server" Text="Reconcile " />
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-top: 10px" id="head1" runat="server">
                            <div class="col-md-12 pull-right">
                                <h4>Barcode Generated Assets</h4>
                                <asp:GridView ID="GridView1" runat="server" EmptyDataText="Sorry! No Available Records..."
                                    CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10 " AutoGenerateColumns="true">
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnExport" runat="server" CssClass="btn btn-primary custom-button-color" Text="Export to Excel" Visible="false" />
                                    <%--<asp:ImageButton ID="btnExport" runat="server" ImageUrl="../../images/excel_icon.gif" Visible="false" title="Export to Excel" Width="30px" />--%>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="head2" runat="server">
                            <div class="col-md-12">
                                <asp:GridView ID="GridView2" runat="server" EmptyDataText="Sorry! No Available Records..."
                                    CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10" AllowPaging="true" AutoGenerateColumns="true">
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
