<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetMasters.aspx.vb" Inherits="Masters_Mas_Webfiles_frmAssetMasters" Title="Asset Masters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-5b02d1ea3b.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>

</head>
<body>

    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Asset Masters" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Asset Masters</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <div class="box-body">
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink10" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmAddAssetCategory.aspx">Asset Category Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLinkSubCat" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmAddAssetSubCategory.aspx">Asset Sub Category Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink6" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmAddAssetBrand.aspx">Brand/Make Master</asp:HyperLink>
                                </div>
                            </div>
                            <br />
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLinkModel" class="btn btn-block btn-primary" runat="server" NavigateUrl="~/FAM/FAM_Webfiles/frmAddAssetModel.aspx">Asset Model Master</asp:HyperLink>
                                </div>
                                <%--<div class="col-md-4 col-sm-12 col-xs-12">
                                        <asp:HyperLink ID="HyperLink2" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmAMGVendorNewRecord1.aspx">Add Vendor</asp:HyperLink>
                                    </div>--%>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink3" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmAddVendorRateContract.aspx">Vendor Rate Contract</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink7AddAsse" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/Masters/Mas_Webfiles/frmAddAsset.aspx">Add Capital Asset </asp:HyperLink>
                                </div>
                            </div>
                            <br />
                            <div class="clearfix">

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="hypconasset" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmaddConsumablesstock.aspx">Add Consumable Asset </asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/AssetLabelFormat.aspx">Asset Label Format</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink2" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/AdminFunctions/ADF_WebFiles/CategoryRoleMapping.aspx">Category Role Mapping</asp:HyperLink>
                                </div>
                            </div>
                            <br />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

