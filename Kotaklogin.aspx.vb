﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Security
Imports System.Web.Security
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System
Imports System.Net.Mail
Imports System.Security.Claims
Imports System.Threading
Imports Microsoft.AspNet.Identity

Partial Class Kotaklogin
    Inherits System.Web.UI.Page
    Protected Sub btnSignIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSignIn.Click

        Session("UID") = ""
        Session("uname") = ""
        Session("COMPANYID") = ""
        Session("TENANT") = "KLI.dbo"
        Session("useroffset") = "+05:30"
        Dim staid As String = ""
        Session("UID") = txtUsrId.Text
        staid = ValidateUser(txtUsrId.Text)
        If staid <> "0" Then

            Session("COMPANYID") = 1
            Session("uname") = staid
            Dim lastlogin As DateTime = savelastlogin(Session("UID"))
            Session("Lastlogintime") = lastlogin.ToString("dd MMM yyyy HH:mm:ss")

            Response.Redirect("~/frmAMTDefault.aspx")

        Else
            lbl1.Text = "User does nor exists"
        End If


    End Sub
    Public Function ValidateUser(ByVal UserName As String) As String
        getuseroffsetandculture()
        Dim ValidateStatus As String = ""
        'ValidateStatus = SPs.ValidateUser(UserName, Password).ExecuteScalar
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Kotak_VALIDATE_USER")
        sp.Command.AddParameter("@USR_ID", UserName, DbType.String)
        ValidateStatus = sp.ExecuteScalar()


        Return ValidateStatus
    End Function
    Public Function savelastlogin(ByVal UserName As String) As DateTime
        Dim ValidateStatus As DateTime = getoffsetdatetime(DateTime.Now)
        Dim Mode As String = "1"
        Dim ds As New DataSet

        Try

            Dim param As SqlParameter() = New SqlParameter(2) {}
            param(0) = New SqlParameter("@USR_ID", SqlDbType.NVarChar)
            param(0).Value = UserName
            param(1) = New SqlParameter("@Mode", SqlDbType.NVarChar)
            param(1).Value = Mode
            param(2) = New SqlParameter("@TIMEOUT", SqlDbType.NVarChar)
            param(2).Value = AppSettings("TIMEOUT").ToString()
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SAVE_USER_LASTLOGIN", param)
            Session("LoginUniqueID") = ds.Tables(0).Rows(0).Item("SNO")
            ValidateStatus = ds.Tables(0).Rows(0).Item("LASTLOGINTIME")
        Catch ex As Exception
        Finally

        End Try
        Return ValidateStatus
    End Function
    Public Sub getuseroffsetandculture()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_OFFSET_CULTURE_BYID")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("useroffset") = ds.Tables(0).Rows(0).Item("AUR_TIME_ZONE")
            Session("userculture") = ds.Tables(0).Rows(0).Item("AUR_CULTURE")
            Session("usercountry") = Convert.ToString(ds.Tables(0).Rows(0).Item("AUR_COUNTRY"))
            Session("location") = Convert.ToString(ds.Tables(0).Rows(0).Item("AUR_LOCATION"))
            'Dim useroffsetcookie As HttpCookie = New HttpCookie("useroffset")
            'useroffsetcookie.Value = ds.Tables(0).Rows(0).Item("AUR_TIME_ZONE")
            'Response.Cookies.Add(useroffsetcookie)
            Dim userculturecookie As HttpCookie = New HttpCookie("userculture")
            userculturecookie.Value = ds.Tables(0).Rows(0).Item("AUR_CULTURE")
            Response.Cookies.Add(userculturecookie)

        End If
    End Sub


End Class
