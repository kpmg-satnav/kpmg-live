Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO

Partial Class WorkSpace_SMS_Webfiles_frmPropertyLevelApproval
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then
            scriptManager.RegisterPostBackControl(btnApprove)
            scriptManager.RegisterPostBackControl(gvPropdocs)
        End If
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        revtxtToilet.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        revpropertyname.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        'rgetxtESTD.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        RegularExpressionValidator6.ValidationExpression = User_Validation.GetValidationExpressionForLat.VAL_EXPR()
        RegularExpressionValidator9.ValidationExpression = User_Validation.GetValidationExpressionForLong.VAL_EXPR()
        REVPHNO.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        RegularExpressionValidator10.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        rfv.ValidationExpression = User_Validation.GetValidationExpressionForEmail.VAL_EXPR()
        RegularExpressionValidator12.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revBuiltupArea.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revCarpetarea.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revCommon.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revRent.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revUsable.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        RegularExpressionValidator13.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        rfvtxtCeilingHight.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        rfvtxtBeamBottomHight.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revmaxcapacity.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        revOptCapacity.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        rfvtxtSeatingCapacity.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        revtxtFSI.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revtxtEfficiency.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revtxtPurPrice.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revtxtMarketValue.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revgovt.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        If Not IsPostBack Then
            BindAcquisitionthrough()
            fillgrid()
            BindRequestTypes()
            BindPropType()
            BindInsuranceType()
            BindUserCities()
            BindFlooringTypes()
            EnableControls(Me.Page.Form.Controls, False)
        End If
    End Sub
    Public Sub EnableControls(ctrl As ControlCollection, isEnable As Boolean)
        For Each item As Control In ctrl
            If item.[GetType]() = GetType(Panel) OrElse item.[GetType]() = GetType(HtmlGenericControl) Then
                EnableControls(item.Controls, isEnable)
            ElseIf item.[GetType]() = GetType(DropDownList) Then
                DirectCast(item, DropDownList).Enabled = isEnable
            ElseIf item.[GetType]() = GetType(TextBox) Then
                DirectCast(item, TextBox).Enabled = isEnable
            ElseIf item.[GetType]() = GetType(FileUpload) Then
                DirectCast(item, FileUpload).Enabled = isEnable
            ElseIf item.[GetType]() = GetType(HtmlInputButton) Then
                DirectCast(item, HtmlInputButton).Disabled = Not isEnable
            End If
        Next
    End Sub
    Private Sub BindFlooringTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_FLOORING_TYPES")
        ddlFlooringType.DataSource = sp3.GetDataSet()
        ddlFlooringType.DataTextField = "PM_FT_TYPE"
        ddlFlooringType.DataValueField = "PM_FT_SNO"
        ddlFlooringType.DataBind()
        ddlFlooringType.Items.Insert(0, New ListItem("--Select Flooring Type--", 0))
    End Sub

    Private Sub BindRequestTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_REQUEST_TYPES")
        ddlReqType.DataSource = sp3.GetDataSet()
        ddlReqType.DataTextField = "PM_RT_TYPE"
        ddlReqType.DataValueField = "PM_RT_SNO"
        ddlReqType.DataBind()
        ddlReqType.Items.Insert(0, New ListItem("--Select Request Type--", 0))
    End Sub

    Private Sub BindUserCities()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp3.Command.AddParameter("@DUMMY", "", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp3.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select City--", 0))
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select Property Type--", 0))
    End Sub
    Private Sub BindAcquisitionthrough()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACQISITION_THROUGH")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlAcqThr.DataSource = sp3.GetDataSet()
        ddlAcqThr.DataTextField = "PN_ACQISITION_THROUGH"
        ddlAcqThr.DataValueField = "PN_ACQISITION_ID"
        ddlAcqThr.DataBind()
        ddlAcqThr.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindInsuranceType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_INSURANCE_TYPE")
        ddlInsuranceType.DataSource = sp.GetDataSet()
        ddlInsuranceType.DataTextField = "INSURANCE_TYPE"
        ddlInsuranceType.DataValueField = "ID"
        ddlInsuranceType.DataBind()
        ddlInsuranceType.Items.Insert(0, New ListItem("--Select Insurance Type--", 0))
    End Sub

    Private Sub GetLocationsbyCity(ByVal city As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
        sp2.Command.AddParameter("@CITY", city)
        sp2.Command.AddParameter("@USR_ID", Session("uid"))
        ddlLocation.DataSource = sp2.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            GetLocationsbyCity(ddlCity.SelectedValue)
            ddlTower.Items.Clear()
        Else
            ddlLocation.Items.Clear()
        End If
    End Sub

    Private Sub GetTowerbyLoc(ByVal loc As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWERSBYLCMID")
        sp2.Command.AddParameter("@LCMID", loc, DbType.String)
        sp2.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
        ddlTower.DataSource = sp2.GetDataSet()
        ddlTower.DataTextField = "TWR_NAME"
        ddlTower.DataValueField = "TWR_CODE"
        ddlTower.DataBind()
        ddlTower.Items.Insert(0, New ListItem("--Select Tower--", "0"))
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex > 0 Then
            GetTowerbyLoc(ddlLocation.SelectedValue)
        Else
            ddlTower.Items.Clear()
        End If
    End Sub

    Private Sub GetFloorsbyTwr(ByVal tower As String, ByVal cty As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_getFloors_masters")
        sp2.Command.AddParameter("@TWR_ID", tower, DbType.String)
        sp2.Command.AddParameter("@FLR_LOC_ID", ddlLocation.SelectedValue, DbType.String)
        sp2.Command.AddParameter("@FLR_CITY_CODE", cty, DbType.String)
        sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlFloor.DataSource = sp2.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, New ListItem("--Select Floor--", "0"))
        txtFloor.Text = ddlFloor.Items.Count - 1
    End Sub


    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        If ddlTower.SelectedIndex > 0 Then
            GetFloorsbyTwr(ddlTower.SelectedValue, ddlCity.SelectedValue)
        Else
            ddlFloor.Items.Clear()
        End If
    End Sub

    Protected Sub txtESTD_TextChanged(sender As Object, e As EventArgs) Handles txtESTD.TextChanged
        If Integer.TryParse(txtESTD.Text, 0) Then
            txtAge.Text = DateTime.Now.Year - Convert.ToInt32(txtESTD.Text)
        Else
            txtAge.Text = ""
        End If
    End Sub

    Protected Sub ddlRecommended_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRecommended.SelectedIndexChanged
        divRecommanded.Visible = IIf(ddlRecommended.SelectedValue = "1", True, False)
    End Sub

    Protected Sub ddlFSI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFSI.SelectedIndexChanged
        divFSI.Visible = IIf(ddlFSI.SelectedValue = "1", True, False)
    End Sub

    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTY_REQS_FORAPPROVAL")
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvReqs.DataSource = ds
        gvReqs.DataBind()
        Session("Reqs") = ds
    End Sub

    Protected Sub gvReqs_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvReqs.PageIndexChanging
        gvReqs.PageIndex = e.NewPageIndex()
        gvReqs.DataSource = Session("Reqs")
        gvReqs.DataBind()
    End Sub

    Protected Sub gvReqs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReqs.RowCommand
        Try
            If e.CommandName = "GetProperties" Then
                panel1.Visible = True
                hdnReqid.Value = e.CommandArgument
                GetProperties(hdnReqid.Value)
            Else
                panel1.Visible = False
            End If
            panel2.Visible = False
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GetProperties(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PPTS_FOR_APPROVAL")
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp.Command.AddParameter("@REQ_ID", Reqid, DbType.String)
        sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvrReqProperties.DataSource = ds
        gvrReqProperties.DataBind()
        Session("reqDetails") = ds
    End Sub

    'Protected Sub gvrReqProperties_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvrReqProperties.PageIndexChanging
    '    gvrReqProperties.PageIndex = e.NewPageIndex()
    '    gvrReqProperties.DataSource = Session("reqDetails")
    '    gvrReqProperties.DataBind()
    'End Sub

    Protected Sub gvrReqProperties_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvrReqProperties.RowCommand
        Try
            If e.CommandName = "ViewDetails" Then
                panel2.Visible = True
                hdnSno.Value = e.CommandArgument
                BindDetails(hdnSno.Value)

                'bind curr loc details
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PPTY_CURR_LOC_SUMMARY")
                sp.Command.AddParameter("@PROP_ID", hdnSno.Value, DbType.Int32)
                sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)

                Dim ds As New DataSet
                ds = sp.GetDataSet()
                If ds.Tables(0).Rows.Count > 0 Then
                    'txtLocation.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
                    lblLoc.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
                    txtpptsCount.Text = ds.Tables(0).Rows(0).Item("TOTPPTS")
                    txtMonth.Text = ds.Tables(0).Rows(0).Item("TOTRENT")
                    txtsqft.Text = ds.Tables(0).Rows(0).Item("TOTAREA")
                End If

                'panel2.Visible = True
                ' ClientScript.RegisterStartupScript(Me.[GetType](), "none", "ShowPopup()", True)
            Else
                panel2.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindDetails(ByVal sno As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PRPTY_DETAILS")
        sp.Command.AddParameter("@PROP_ID", sno, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            'General Details

            txtReqID.Text = ds.Tables(0).Rows(0).Item("PM_PPT_PM_REQ_ID")
            ddlReqType.ClearSelection()
            ddlReqType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_REQ_TYPE")).Selected = True
            'ds.Tables(0).Rows(0).Item("PM_PPT_REQ_TYPE") = ddlReqType.SelectedValue
            ddlPprtNature.ClearSelection()
            ddlPprtNature.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_NATURE")).Selected = True
            ddlAcqThr.ClearSelection()
            ddlAcqThr.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_ACQ_THR")).Selected = True

            ddlCity.ClearSelection()
            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE")).Selected = True
            GetLocationsbyCity(ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE"))
            ddlLocation.ClearSelection()
            ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE")).Selected = True
            GetTowerbyLoc(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE"))
            ddlTower.ClearSelection()
            ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE")).Selected = True

            GetFloorsbyTwr(ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE"), ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE"))
            ddlFloor.ClearSelection()
            'If Not ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_FLR_CODE")).Selected = True Then
            '    ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE")).Selected = True
            'End If
            If ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE").ToString() <> "" Then
                ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE")).Selected = True
            End If

            'For Each item As String In ds.Tables(0).Rows(0).Item("FLOORLIST").ToString().Split(",")
            '    ddlFloor.Items.FindByValue(item).Selected = True
            'Next
            'ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_FLR_CODE")).Selected = True
            txtToilet.Text = ds.Tables(0).Rows(0).Item("PM_PPT_TOT_TOI")  'IIf(txtToilet.Text = "", 0, txtToilet.Text)
            ddlPropertyType.SelectedValue = ds.Tables(0).Rows(0).Item("PM_PPT_TYPE")
            txtPropIDName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_NAME")
            If (ds.Tables(0).Rows(0).Item("PM_PPT_ESTD").ToString() <> "") Then
                txtESTD.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ESTD")
            End If
            'txtESTD.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_ESTD"))  'IIf(txtESTD.Text = "", "", Convert.ToString(txtESTD.Text))
            txtAge.Text = ds.Tables(0).Rows(0).Item("PM_PPT_AGE")   'IIf(txtAge.Text = "", "", Convert.ToString(txtAge.Text))
            txtPropDesc.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ADDRESS")
            txtSignageLocation.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SIGN_LOC")

            txtSocityName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SOC_NAME")
            txtlat.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LAT")
            txtlong.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LONG")
            txtOwnScopeWork.Text = ds.Tables(0).Rows(0).Item("PM_PPT_OWN_SCOPE")
            ddlRecommended.ClearSelection()
            ddlRecommended.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_PPT_RECOMMENDED") = True, 1, 0)
            'ddlRecommended.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_RECOMMENDED")).Selected = True
            If ddlRecommended.SelectedValue = 1 Then
                txtRecmRemarks.Visible = True
                txtRecmRemarks.Text = ds.Tables(0).Rows(0).Item("PM_PPT_RECO_REM")
            Else
                divRecommanded.Visible = False
            End If

            'Owner Details
            txtownrname.Text = ds.Tables(0).Rows(0).Item("PM_OWN_NAME")
            txtphno.Text = ds.Tables(0).Rows(0).Item("PM_OWN_PH_NO")
            txtPrvOwnName.Text = ds.Tables(0).Rows(0).Item("PM_PREV_OWN_NAME")
            txtPrvOwnPhNo.Text = ds.Tables(0).Rows(0).Item("PM_PREV_OWN_PH_NO")

            txtOwnEmail.Text = ds.Tables(0).Rows(0).Item("PM_OWN_EMAIL")

            'Area Details
            txtCarpetArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_CARPET_AREA"))
            txtBuiltupArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_BUA_AREA"))
            txtCommonArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_COM_AREA"))
            txtRentableArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_RENT_AREA"))

            txtUsableArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_USABEL_AREA"))
            txtSuperBulArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_SBU_AREA"))
            txtPlotArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_PLOT_AREA"))
            txtCeilingHight.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_FTC_HIGHT"))

            txtBeamBottomHight.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_FTBB_HIGHT"))
            txtMaxCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_MAX_CAP"))
            txtOptCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_OPT_CAP"))
            txtSeatingCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_SEATING_CAP"))

            ddlFlooringType.ClearSelection()
            ddlFlooringType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_AR_FLOOR_TYPE")).Selected = True
            ddlFSI.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_AR_FSI") = True, 1, 0)
            txtFSI.Text = ds.Tables(0).Rows(0).Item("PM_AR_FSI_RATIO")
            txtEfficiency.Text = ds.Tables(0).Rows(0).Item("PM_AR_PREF_EFF")
            txtInspection.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_BY")
            If (ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT").ToString() <> "") Then
                txtsdate.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT")
            End If
            'PURCHASE DETAILS
            txtPurPrice.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_PUR_PRICE"), 2)
            txtPurDate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PUR_DATE"))
            txtMarketValue.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_PUR_MARKET_VALUE"), 2)

            'GOVT DETAILS
            txtIRDA.Text = ds.Tables(0).Rows(0).Item("PM_GOV_IRDA")
            txtPCcode.Text = ds.Tables(0).Rows(0).Item("PM_GOV_PC_CODE")
            txtGovtPropCode.Text = ds.Tables(0).Rows(0).Item("PM_GOV_PROP_CODE")
            txtUOM_CODE.Text = ds.Tables(0).Rows(0).Item("PM_GOV_UOM_CODE")

            'INSURANCE DETAILS
            ddlInsuranceType.ClearSelection()
            ddlInsuranceType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_INS_TYPE")).Selected = True
            txtInsuranceVendor.Text = ds.Tables(0).Rows(0).Item("PM_INS_VENDOR")
            txtInsuranceAmt.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_INS_AMOUNT"), 2)
            txtInsurancePolNum.Text = ds.Tables(0).Rows(0).Item("PM_INS_PNO")

            txtInsuranceStartdate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_INS_START_DT"))
            txtInsuranceEnddate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_INS_END_DT"))
            txtentity.Text = ds.Tables(0).Rows(0).Item("CHE_NAME")
            If (ds.Tables(0).Rows(0).Item("PM_OWN_LANDLINE").ToString() <> "") Then
                txtllno.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OWN_LANDLINE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_POA").ToString() = "") Then
                ddlAgreementbyPOA.SelectedValue = "--Select--"
            Else
                ddlAgreementbyPOA.ClearSelection()
                ddlAgreementbyPOA.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_POA")).Selected = True
            End If
            'physical condition
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_WALLS").ToString() <> "") Then
                txtWalls.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_WALLS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_ROOF").ToString() <> "") Then
                txtRoof.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_ROOF"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_CEILING").ToString() <> "") Then
                txtCeiling.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_CEILING"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_WINDOWS").ToString() <> "") Then
                txtwindows.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_WINDOWS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_DAMAGE").ToString() <> "") Then
                txtDamage.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_DAMAGE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_SEEPAGE").ToString() <> "") Then
                txtSeepage.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_SEEPAGE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_OTHR_TNT").ToString() <> "") Then
                txtOthrtnt.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_OTHR_TNT"))
            End If


            'LANDLORD SCOPE OF WORK
            If (ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED").ToString() = "") Then
                ddlvitrified.SelectedValue = "--Select--"
            Else
                ddlvitrified.ClearSelection()
                ddlvitrified.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED")).Selected = True
            End If
            txtVitriRemarks.Text = ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED_RMKS")
            If (ds.Tables(0).Rows(0).Item("PM_LLS_WASHROOMS").ToString() = "") Then
                ddlWashroom.SelectedValue = "--Select--"
            Else
                ddlWashroom.ClearSelection()
                ddlWashroom.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_WASHROOMS")).Selected = True
            End If
            txtWashroom.Text = ds.Tables(0).Rows(0).Item("PM_LLS_WASH_RMKS")
            If (ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY").ToString() = "") Then
                ddlPantry.SelectedValue = "--Select--"
            Else
                ddlPantry.ClearSelection()
                ddlPantry.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY")).Selected = True
            End If
            txtPantry.Text = ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY_RMKS")
            If (ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER").ToString() = "") Then
                ddlShutter.SelectedValue = "--Select--"
            Else
                ddlShutter.ClearSelection()
                ddlShutter.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER")).Selected = True
            End If
            txtShutter.Text = ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER_RMKS")
            If (ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS").ToString() = "") Then
                ddlOthers.SelectedValue = "--Select--"
            Else
                ddlOthers.ClearSelection()
                ddlOthers.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS_RMKS").ToString() <> "") Then
                txtOthers.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS_RMKS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_WORK_DAYS").ToString() <> "") Then
                txtllwork.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_LLS_WORK_DAYS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_EXISTING").ToString() <> "") Then
                txtElectricEx.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_EXISTING"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_REQUIRED").ToString() <> "") Then
                txtElectricRq.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_REQUIRED"))
            End If
            'COST DETAILS
            If (ds.Tables(0).Rows(0).Item("PM_COST_RENT").ToString() <> "") Then
                txtRent.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_RENT"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_RENT_SFT").ToString() <> "") Then
                txtsft.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_RENT_SFT"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_RATIO").ToString() <> "") Then
                txtRatio.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_RATIO"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_OWN_SHARE").ToString() <> "") Then
                txtownshare.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_OWN_SHARE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_SECDEPOSIT").ToString() <> "") Then
                txtsecdepmonths.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_SECDEPOSIT"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_COST_GST").ToString() = "") Then
                ddlgst.SelectedValue = "--Select--"
            Else
                ddlgst.ClearSelection()
                ddlgst.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_COST_GST")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_MAINTENANCE").ToString() <> "") Then
                txtmaintenance.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_MAINTENANCE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_ESC_RENTALS").ToString() <> "") Then
                txtesc.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_ESC_RENTALS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_RENT_FREE").ToString() <> "") Then
                txtRentFree.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_RENT_FREE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_STAMP").ToString() = "") Then
                ddlStampReg.SelectedValue = "--Select--"
            Else
                ddlStampReg.ClearSelection()
                ddlStampReg.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_COST_STAMP")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_AGREEMENT_PERIOD").ToString() <> "") Then
                txtAgree.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_AGREEMENT_PERIOD"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_FLOORING").ToString() = "") Then
                ddlFlooring.SelectedValue = "--Select--"
            Else
                ddlFlooring.ClearSelection()
                ddlFlooring.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_OTHR_FLOORING")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_FLRG_RMKS").ToString() <> "") Then
                txtFlooring.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_FLRG_RMKS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_WASHRM_EXISTING").ToString() <> "") Then
                txtWashExisting.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_WASHRM_EXISTING"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_WASHRM_REQUIRED").ToString() <> "") Then
                txtWashRequirement.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_WASHRM_REQUIRED"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_POTABLE_WTR").ToString() = "") Then
                ddlPotableWater.SelectedValue = "--Select--"
            Else
                ddlPotableWater.ClearSelection()
                ddlPotableWater.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_OTHR_POTABLE_WTR")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_SIG_LENGTH").ToString() <> "") Then
                txtsiglength.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_SIG_LENGTH"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_SIG_WIDTH").ToString() <> "") Then
                txtsigwidth.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_SIG_WIDTH"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_AC_OUTDOOR").ToString() <> "") Then
                txtACOutdoor.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_AC_OUTDOOR"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_GSB").ToString() <> "") Then
                txtGSB.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_GSB"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_TITLE").ToString() = "") Then
                ddlTitle.SelectedValue = "--Select--"
            Else
                ddlTitle.ClearSelection()
                ddlTitle.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_TITLE")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_TITLLE_RMKS").ToString() <> "") Then
                txtTitleRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_TITLLE_RMKS"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_DOC_OCCUPANCY").ToString() = "") Then
                ddlOccupancy.SelectedValue = "--Select--"
            Else
                ddlOccupancy.ClearSelection()
                ddlOccupancy.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_OCCUPANCY")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_OCC_RMKS").ToString() <> "") Then
                txtOccupancyRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_OCC_RMKS"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_DOC_BUILD").ToString() = "") Then
                ddlBuilding.SelectedValue = "--Select--"
            Else
                ddlBuilding.ClearSelection()
                ddlBuilding.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_BUILD")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_OCC_RMKS").ToString() <> "") Then
                txtBuildingRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_BUILD_RMKS"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_DOC_PAN").ToString() = "") Then
                ddlPAN.SelectedValue = "--Select--"
            Else
                ddlPAN.ClearSelection()
                ddlPAN.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_PAN")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_PAN_RMKS").ToString() <> "") Then
                txtPANRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_PAN_RMKS"))
            End If


            If (ds.Tables(0).Rows(0).Item("PM_DOC_TAX").ToString() = "") Then
                ddlTax.SelectedValue = "--Select--"
            Else
                ddlTax.ClearSelection()
                ddlTax.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_TAX")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_TAX_RMKS").ToString() <> "") Then
                txtTaxRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_TAX_RMKS"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_DOC_OTHER_INFO").ToString() <> "") Then
                txtOtherInfo.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_OTHER_INFO"))
            End If

            '  If ds.Tables(1).Rows.Count > 0 Then
            gvPropdocs.DataSource = ds.Tables(1)
            gvPropdocs.DataBind()
            'End If

        End If
    End Sub

    Protected Sub gvPropdocs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPropdocs.RowCommand
        Try
            If e.CommandName = "Download" Then
                Response.ContentType = ContentType
                Dim imgPath As String = "../../images/Property_Images/" + e.CommandArgument
                Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(imgPath)))
                Response.WriteFile(imgPath)
                Response.End()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'multi approval
    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Try
            If txtMultiRemarks.Text <> Nothing Then
                Dim ds As New DataSet
                For Each row As GridViewRow In gvrReqProperties.Rows
                    Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                    Dim lblPropSno As Label = DirectCast(row.FindControl("lblID"), Label)
                    Dim param As SqlParameter() = New SqlParameter(5) {}
                    param(0) = New SqlParameter("@PROP_SNO", lblPropSno.Text)

                    param(2) = New SqlParameter("@AUR_ID", Session("uid"))
                    param(3) = New SqlParameter("@REMARKS", txtMultiRemarks.Text)
                    param(4) = New SqlParameter("@REQ_ID", hdnReqid.Value)
                    param(5) = New SqlParameter("@CMP_ID", Session("COMPANYID"))
                    If chkselect.Checked = True Then
                        param(1) = New SqlParameter("@STA_ID", 4002)
                    Else
                        param(1) = New SqlParameter("@STA_ID", 4003)
                    End If
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_MULTI_PROP_APP_REJ", param)
                Next

                lblmsg.Text = "Property Approved Successfully"
                txtMultiRemarks.Text = String.Empty
                fillgrid()
                panel1.Visible = False
            Else
                lblmsg.Text = "Please Enter Remarks"
            End If
        Catch ex As Exception

        End Try
    End Sub

    'multi rejection
    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Try
            If txtMultiRemarks.Text <> Nothing Then
                Dim ds As New DataSet
                For Each row As GridViewRow In gvrReqProperties.Rows
                    Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                    Dim lblPropSno As Label = DirectCast(row.FindControl("lblID"), Label)

                    Dim param As SqlParameter() = New SqlParameter(5) {}
                    param(0) = New SqlParameter("@PROP_SNO", lblPropSno.Text)
                    param(1) = New SqlParameter("@STA_ID", 4003)
                    param(2) = New SqlParameter("@AUR_ID", Session("uid"))
                    param(3) = New SqlParameter("@REMARKS", txtMultiRemarks.Text)
                    param(4) = New SqlParameter("@REQ_ID", hdnReqid.Value)
                    param(5) = New SqlParameter("@CMP_ID", Session("COMPANYID"))
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_MULTI_PROP_APP_REJ", param)
                Next

                lblmsg.Text = "Property Rejected Successfully"
                txtMultiRemarks.Text = String.Empty
                fillgrid()
                panel1.Visible = False
            Else
                lblmsg.Text = "Please Enter Remarks"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PPTS_FOR_APPROVAL_BYSERACH")
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp.Command.AddParameter("@PROP_NAME", txtsearch.Text, DbType.String)
        sp.Command.AddParameter("@REQ_ID", hdnReqid.Value, DbType.String)
        sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvrReqProperties.DataSource = ds
        gvrReqProperties.DataBind()
        Session("reqDetails") = ds
    End Sub

    Protected Sub btnsrch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsrch.Click
        Try
            panel1.Visible = False
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTIES_BY_SEARCH")
            sp.Command.AddParameter("@PROP_NAME", txtSrch.Text, DbType.String)
            sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            gvReqs.DataSource = ds
            gvReqs.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
