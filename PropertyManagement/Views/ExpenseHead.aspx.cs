﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;

public partial class PropertyManagement_Views_ExpenseHead : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UID"] == null || Session["UID"] == "")
        {
            Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
        }
        else
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            string host = HttpContext.Current.Request.Url.Host;
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 50);
            param[0].Value = HttpContext.Current.Session["UID"];
            param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
            param[1].Value = "/Masters/MAS_Webfiles/frmMASMasters.aspx";
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
            {
                if (sdr.HasRows) { }
                else
                {
                    Response.Redirect(ConfigurationManager.AppSettings["FMGLogout"]);
                }
            }
        }

    }
}