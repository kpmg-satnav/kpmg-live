Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports SqlHelper

Partial Class WorkSpace_SMS_Webfiles_frmHRAddLease
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet

    Public Class ImageClas
        Private _fn As String

        Public Property Filename() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String

        Public Property FilePath() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property

    End Class

    Private Sub BindReqId()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GENERATE_REQUESTID_ADD_LEASE")
        sp3.Command.Parameters.Add("@REQ_TYPE", "ADDLEASE", DbType.String)
        Dim dt = DateTime.Now.ToString("MMddyyyy")
        lblLeaseReqId.Text = dt + "/LESREQ/" + Convert.ToString(sp3.ExecuteScalar())
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then
            scriptManager.RegisterPostBackControl(btnSubmit)
            scriptManager.RegisterPostBackControl(btnLandlord)

        End If
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using

        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            ClearTextBox(Me)
            AssigTextValues()
            ClearDropdown(Me)
            BindProperty()
            BindPropType()
            BindCity()
            BindStatus()
            BindLeaseExpences()
            BindFlooringTypes()
            BindTenure()
            BindPayMode()
            BindReqId()
            lblMsg.Text = ""
            panel1.Visible = False
            panel2.Visible = False
            txtsdate.Text = DateTime.Now.ToString("MM/dd/yyyy")
            txtedate.Text = DateTime.Now.AddYears(1).ToString("MM/dd/yyyy")
            txtrentcommencementdate.Text = DateTime.Now.ToString("MM/dd/yyyy")
            txtstartagreDT.Text = DateTime.Now.ToString("MM/dd/yyyy")
            ''rentrevisionpanelmonth.Visible = False
            RentRevisionPanel.Visible = False
            LandloardRentRevisionPanel.Visible = False
            ''BindRentRevision()
        End If
        txtsdate.Attributes.Add("readonly", "readonly")
        txtedate.Attributes.Add("readonly", "readonly")
    End Sub

    Public Sub txtIntervaldu_TextChanged(sender As Object, e As EventArgs)
        BindRentRevisiondt()
        For Each item As RepeaterItem In Repeater2.Items
            If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                'Dim lbl = CType(item.FindControl("lblEscno"), Label)
                Dim lbtxt = CType(item.FindControl("ddlYear"), TextBox)
                Dim lbmnth = CType(item.FindControl("ddlMonth"), TextBox)
                Dim lbday = CType(item.FindControl("ddlDay"), TextBox)
                Dim lbesc = CType(item.FindControl("TextBox1"), TextBox)
                Dim lbyr = CType(item.FindControl("lbyear"), Label)
                Dim lbmth = CType(item.FindControl("lbmnt"), Label)
                Dim lbdy = CType(item.FindControl("lbldy"), Label)
                Dim lble = CType(item.FindControl("lbldf"), Label)
                'lbl.Visible = False
                lbtxt.Visible = False
                lbmnth.Visible = False
                lbesc.Visible = False
                lbday.Visible = False
                lbyr.Visible = False
                lbmth.Visible = False
                lbdy.Visible = False
                lble.Visible = False
            End If
        Next
    End Sub
    Public Sub txtnofEscltions_TextChanged(sender As Object, e As EventArgs)
        BindRentRevisiondtflex()
        For Each item As RepeaterItem In Repeater2.Items
            If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                Dim lbl = CType(item.FindControl("lblRevYear"), Label)
                Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
                'lbl.Visible = False
                lbtxt.Visible = False
            End If
        Next
    End Sub

    Public Sub BindRentRevisiondtflex()
        If txtnofEscltions.Text <> "" Then
            Dim RR_lst As New List(Of RentRevision)()
            Dim rr_obj As New RentRevision()
            Dim SubEscDATE As DateTime = txtsdate.Text
            For i As Integer = 1 To Convert.ToInt32(txtnofEscltions.Text)
                rr_obj = New RentRevision()
                d = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtnofEscltions.Text))
                'rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
                'rr_obj.RR_Year = d
                txtInterval = d
                rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(i) + ":"
                rr_obj.RR_Percentage = "0"
                rr_obj.pm_les_esc_id = ""
                'i = i + 1
                'rr_obj.
                For Each item As RepeaterItem In Repeater2.Items
                    If (item.ItemIndex = i - 1) Then
                        If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                            Dim tect = CType(item.FindControl("TextBox1"), TextBox)
                            ' If (tect.Text <> 0) Then
                            'Dim tectr = CType(item.FindControl("TextBox1"), TextBox).Text
                            Dim lbl = CType(item.FindControl("lblRevYear"), Label)
                            Dim lblYear = IIf(CType(item.FindControl("ddlYear"), TextBox).Text = "", 0, CType(item.FindControl("ddlYear"), TextBox).Text)
                            Dim lblMonth = IIf(CType(item.FindControl("ddlMonth"), TextBox).Text = "", 0, CType(item.FindControl("ddlMonth"), TextBox).Text)
                            Dim lblDay = IIf(CType(item.FindControl("ddlDay"), TextBox).Text = "", 0, CType(item.FindControl("ddlDay"), TextBox).Text)
                            SubEscDATE = Convert.ToDateTime(SubEscDATE).AddYears(Convert.ToInt32(lblYear))
                            SubEscDATE = Convert.ToDateTime(SubEscDATE).AddMonths(Convert.ToInt32(lblMonth))
                            SubEscDATE = Convert.ToDateTime(SubEscDATE).AddDays(Convert.ToInt32(lblDay))
                            rr_obj.RR_Year = SubEscDATE
                            rr_obj.RR_YEARS = lblYear
                            rr_obj.RR_MONTHS = lblMonth
                            rr_obj.RR_DAYS = lblDay
                            rr_obj.RR_Percentage = tect.Text
                            lbl.Visible = True
                            'Else
                            '    rr_obj.RR_Year = ""
                            'End If
                        End If
                    End If
                Next
                RR_lst.Add(rr_obj)
            Next
            Repeater2.DataSource = RR_lst
            Repeater2.DataBind()
            Session("escdt") = SubEscDATE
            If (SubEscDATE >= Convert.ToDateTime(txtedate.Text)) Then
                lblmesg.Text = "Escalation date should not be greater than Lease Expiry Date"
            Else
                lblmesg.Text = ""
            End If
            'RentRevisionPanelflex.Visible = True
            RentRevisionPanel.Visible = True
        End If
    End Sub

    Public Sub TextBox1_TextChanged()
        BindRentRevisiondtflex()
        For Each item As RepeaterItem In Repeater2.Items
            If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                Dim lbl = CType(item.FindControl("lblRevYear"), Label)
                Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
                lbl.Visible = True
                lbtxt.Visible = False
            End If
        Next
    End Sub
    Public Sub ddlDay_TextChanged()
        BindRentRevisiondtflex()
        For Each item As RepeaterItem In Repeater2.Items
            If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                Dim lbl = CType(item.FindControl("lblRevYear"), Label)
                Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
                lbl.Visible = True
                lbtxt.Visible = False
            End If
        Next
    End Sub
    Public Sub ddlMonth_TextChanged()
        BindRentRevisiondtflex()
        For Each item As RepeaterItem In Repeater2.Items
            If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                Dim lbl = CType(item.FindControl("lblRevYear"), Label)
                Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
                lbl.Visible = True
                lbtxt.Visible = False
            End If
        Next
    End Sub
    Public Sub ddlYear_TextChanged()
        BindRentRevisiondtflex()
        For Each item As RepeaterItem In Repeater2.Items
            If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                Dim lbl = CType(item.FindControl("lblRevYear"), Label)
                Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
                lbl.Visible = True
                lbtxt.Visible = False
            End If
        Next
    End Sub
    Dim rowCount As Integer = 0
    Dim Duration As Integer = 0
    Dim d As DateTime
    Dim txtInterval As String
    Dim FinalRent As Integer = 0
    Dim LandloardFinalRent As Integer = 0

    Public Sub BindRentRevisiondt()
        lblmesg.Text = ""
        If txtIntervaldu.Text <> "" Then
            If ddlintervaltype.SelectedValue = "Yearly" Then
                rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
                txtInterval = txtsdate.Text
                Duration = rowCount / Convert.ToInt32(txtIntervaldu.Text)
                If Duration >= 1 Then
                    'RentRevisionPanelflex.Visible = False
                    RentRevisionPanel.Visible = True
                    Dim RR_lst As New List(Of RentRevision)()
                    Dim rr_obj As New RentRevision()
                    If Convert.ToInt32(txtIntervaldu.Text) > rowCount Then
                        lblmesg.Text = "Interval Duration should be in between Lease Agreement Dates"
                    End If
                    Dim j As Integer = 1
                    For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To rowCount
                        rr_obj = New RentRevision()
                        d = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtIntervaldu.Text))
                        'rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
                        rr_obj.RR_Year = d
                        rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(j) + ":"
                        txtInterval = d
                        rr_obj.RR_Percentage = "0"
                        rr_obj.pm_les_esc_id = ""
                        i = i + (Convert.ToInt32(txtIntervaldu.Text) - 1)
                        j = j + 1
                        RR_lst.Add(rr_obj)
                    Next
                    Repeater2.DataSource = RR_lst
                    Repeater2.DataBind()
                Else
                    'RentRevisionPanelflex.Visible = False
                    RentRevisionPanel.Visible = False
                    If Convert.ToInt32(txtIntervaldu.Text) > rowCount Then
                        lblmesg.Text = "Interval Duration should be in between Lease Agreement Dates"
                    End If
                End If
            Else ddlintervaltype.SelectedValue = "Monthly"
                Dim rowCount As Integer = 0
                Dim months1 As Integer = (DateTime.Parse(txtedate.Text).Year * 12 + DateTime.Parse(txtedate.Text).Month) - (DateTime.Parse(txtsdate.Text).Year * 12 + DateTime.Parse(txtsdate.Text).Month)
                Dim j As Integer = 1
                txtInterval = txtsdate.Text
                Dim Duration As Integer = 0
                Duration = months1 / Convert.ToInt32(txtIntervaldu.Text)
                If Convert.ToInt32(txtIntervaldu.Text) > months1 Then
                    lblmesg.Text = "Interval Duration should be in between Lease Agreement Dates"
                End If
                If Duration >= 1 Then
                    'RentRevisionPanelflex.Visible = False
                    RentRevisionPanel.Visible = True
                    Dim RR_lst As New List(Of RentRevision)()
                    Dim rr_obj As New RentRevision()
                    ''For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To months1 - Convert.ToInt32(txtIntervaldu.Text)
                    For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To months1
                        rr_obj = New RentRevision()
                        d = Convert.ToDateTime(txtInterval).AddMonths(Convert.ToInt32(txtIntervaldu.Text))
                        rr_obj.RR_Year = d
                        rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(j) + ":"
                        txtInterval = d
                        j = j + 1
                        rr_obj.RR_Percentage = "0"
                        rr_obj.pm_les_esc_id = ""
                        i = i + (Convert.ToInt32(txtIntervaldu.Text) - 1)
                        RR_lst.Add(rr_obj)
                    Next
                    Repeater2.DataSource = RR_lst
                    Repeater2.DataBind()
                Else
                    'RentRevisionPanelflex.Visible = False
                    RentRevisionPanel.Visible = False
                End If
            End If
        End If

        'Dim Basicrent As Int32 = txttotalrent.Text
        'Dim startdate As DateTime = txtsdate.Text
        'Dim S = startdate.Month
        'Dim days = startdate.Day
        'Dim h = startdate.DayOfWeek
        'Dim CurrentMonthDays As Int16 = DateTime.DaysInMonth(startdate.Year, startdate.Month)

        'Dim T1 As Int16
        'Dim T2 As Int16
        'Dim T3 As Int16
        'Dim T4 As Int16
        'Dim T5 As Int16
        'Dim prorata As Int16
        'Dim T7 As Int16
        'Dim escaltedrent As Int16
        'T1 = Basicrent / CurrentMonthDays
        'T2 = CurrentMonthDays - days + 1
        'T3 = T1 * T2
        'T4 = T3 * (15 / 100) + T3
        'T5 = CurrentMonthDays - T2
        'prorata = T5 * T1
        'T7 = prorata + T4
        'escaltedrent = Basicrent * (15 / 100) + Basicrent
        'lblmesg.Text = ""
        'If txtIntervaldu.Text <> "" Then
        'If ddlintervaltype.SelectedValue = "Yearly" Then
        '    rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
        '    txtInterval = txtsdate.Text
        '    Duration = rowCount / Convert.ToInt32(txtIntervaldu.Text)
        '    If Duration >= 1 Then
        '        RentRevisionPanel.Visible = True
        '        Dim RR_lst As New List(Of RentRevision)()
        '        Dim rr_obj As New RentRevision()
        '        If Convert.ToInt32(txtIntervaldu.Text) > rowCount Then
        '            lblmesg.Text = "Interval Duration should be in between Lease Agreement Dates"
        '        End If
        '        For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To rowCount
        '            rr_obj = New RentRevision()
        '            d = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtIntervaldu.Text))
        '            'rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
        '            rr_obj.RR_Year = d
        '            txtInterval = d
        '            rr_obj.RR_Percentage = "0"
        '            i = i + (Convert.ToInt32(txtIntervaldu.Text) - 1)
        '            RR_lst.Add(rr_obj)
        '        Next
        '        rpRevision.DataSource = RR_lst
        '        rpRevision.DataBind()
        '    Else
        '        RentRevisionPanel.Visible = False
        '    End If
        'Else ddlintervaltype.SelectedValue = "Monthly"
        '    Dim rowCount As Integer = 0
        '    Dim months1 As Integer = (DateTime.Parse(txtedate.Text).Year * 12 + DateTime.Parse(txtedate.Text).Month) - (DateTime.Parse(txtsdate.Text).Year * 12 + DateTime.Parse(txtsdate.Text).Month)

        '    txtInterval = txtsdate.Text
        '    Dim Duration As Integer = 0
        '    Duration = months1 / Convert.ToInt32(txtIntervaldu.Text)
        '    If Convert.ToInt32(txtIntervaldu.Text) > months1 Then
        '        lblmesg.Text = "Interval Duration should be in between Lease Agreement Dates"
        '    End If
        '    If Duration > 1 Then
        '        RentRevisionPanel.Visible = True
        '        Dim RR_lst As New List(Of RentRevision)()
        '        Dim rr_obj As New RentRevision()
        '        ''For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To months1 - Convert.ToInt32(txtIntervaldu.Text)
        '        For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To months1
        '            rr_obj = New RentRevision()
        '            d = Convert.ToDateTime(txtInterval).AddMonths(Convert.ToInt32(txtIntervaldu.Text))
        '            rr_obj.RR_Year = d
        '            txtInterval = d

        '            rr_obj.RR_Percentage = "0"
        '            i = i + (Convert.ToInt32(txtIntervaldu.Text) - 1)
        '            RR_lst.Add(rr_obj)
        '        Next
        '        rpRevision.DataSource = RR_lst
        '        rpRevision.DataBind()
        '    Else
        '        RentRevisionPanel.Visible = False
        '    End If
        'End If
        'End If
        'If rowCount > 1 Then
        '    RentRevisionPanel.Visible = True
        '    Dim RR_lst As New List(Of RentRevision)()
        '    Dim rr_obj As New RentRevision()
        '    For i As Integer = 1 To rowCount - 1
        '        rr_obj = New RentRevision()
        '        rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
        '        rr_obj.RR_Percentage = "0"
        '        RR_lst.Add(rr_obj)
        '    Next
        '    rpRevision.DataSource = RR_lst
        '    rpRevision.DataBind()
        'Else
        '    RentRevisionPanel.Visible = False
        'End If

    End Sub

    Private Sub txtlock_TextChanged(sender As Object, e As EventArgs) Handles txtlock.TextChanged
        'Dim Month As Integer
        Dim Month1 As Date = txtrentcommencementdate.Text
        Dim NOTICEPREIOD As String = txtNotiePeriod.Text
        'Dim Month2 As Date = Convert.ToDateTime(Month1).AddMonths(txtlock.Text)
        'Txtlockinpeamonth.Text = Month2
        ''Dim Month3 As Date = txtNotiePeriod.Text
        ''Dim Month As Date = txtlock.Text
        If NOTICEPREIOD = "0" Then
            Dim Month2 As Date = Convert.ToDateTime(Month1).AddMonths(txtlock.Text)
            Txtlockinpeamonth.Text = Month2
        Else
            Dim Month3 As Date = txtrentcommencementdate.Text
            Dim TOTALMONTHS As Int32 = Convert.ToInt32(NOTICEPREIOD) + Convert.ToInt32(txtlock.Text)
            Dim Month2 As Date = Convert.ToDateTime(Month3).AddMonths(TOTALMONTHS)
            ''Dim Month3 As Date = txtNotiePeriod.Text
            'Dim Month4 As Date = Convert.ToDateTime(Month2).AddMonths(txtNotiePeriod.Text)
            Txtlockinpeamonth.Text = Month2
        End If

    End Sub
    Private Sub txtNotiePeriod_TextChanged(sender As Object, e As EventArgs) Handles txtNotiePeriod.TextChanged
        Dim LOCKINPERIOD As String = txtlock.Text
        If LOCKINPERIOD = "0" Then
            Dim Month6 As Date = txtrentcommencementdate.Text
            Dim Month7 As Date = Convert.ToDateTime(Month6).AddMonths(txtNotiePeriod.Text)
            Txtlockinpeamonth.Text = Month7
        Else
            Dim Month3 As Date = txtrentcommencementdate.Text
            Dim TOTALMONTHS As Int32 = Convert.ToInt32(txtNotiePeriod.Text) + Convert.ToInt32(txtlock.Text)
            Dim Month2 As Date = Convert.ToDateTime(Month3).AddMonths(TOTALMONTHS)
            ''Dim Month3 As Date = txtNotiePeriod.Text
            'Dim Month4 As Date = Convert.ToDateTime(Month2).AddMonths(txtNotiePeriod.Text)
            Txtlockinpeamonth.Text = Month2
        End If

    End Sub
    Private Sub BindProperty()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_BIND_PROPERTIES_FOR_LEASE")
        sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        ddlproperty.DataSource = sp.GetDataSet()
        ddlproperty.DataTextField = "PN_NAME"
        ddlproperty.DataValueField = "PM_PPT_SNO"
        ddlproperty.DataBind()
        ddlproperty.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Private Sub BindLeaseExpences()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_SERVICE_TYPE")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        gvLeaseExpences.DataSource = sp.GetDataSet()
        gvLeaseExpences.DataBind()
        For i As Integer = 0 To gvLeaseExpences.Rows.Count - 1
            Dim ddlServiceProvider As DropDownList = CType(gvLeaseExpences.Rows(i).FindControl("ddlServiceProvider"), DropDownList)
            ObjSubSonic.Binddropdown(ddlServiceProvider, "PM_GET_SERVICE_PROVIDER", "NAME", "CODE")
        Next
    End Sub

    Private Sub BindTenure()
        ObjSubSonic.Binddropdown(ddlTenure, "PM_GET_PAYMENT_TERMS", "PM_PT_NAME", "PM_PT_SNO")
        ddlTenure.Items.RemoveAt(0)
    End Sub

    Private Sub BindPayMode()
        ObjSubSonic.Binddropdown(ddlpaymentmode, "PM_GET_PAYMENT_MODE", "NAME", "CODE")
    End Sub

    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITY_ADM")
        sp.Command.AddParameter("@MODE", 2, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("Uid").ToString, DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "--Select--"))

        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITY")
        'sp.Command.AddParameter("@dummy", 0, DbType.String)
        'ddlCity.DataSource = sp.GetDataSet()
        'ddlCity.DataTextField = "CTY_NAME"
        'ddlCity.DataValueField = "CTY_CODE"
        'ddlCity.DataBind()
        'ddlCity.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub

    Public Sub ClearTextBox(ByVal root As Control)
        For Each ctrl As Control In root.Controls
            ClearTextBox(ctrl)
            If TypeOf ctrl Is TextBox Then
                CType(ctrl, TextBox).Text = ""
            End If
        Next ctrl
    End Sub

    Public Sub ClearDropdown(ByVal root As Control)
        For Each ctrl As Control In root.Controls
            ClearDropdown(ctrl)
            If TypeOf ctrl Is DropDownList Then
                CType(ctrl, DropDownList).ClearSelection()
            End If
        Next ctrl
    End Sub
    Private Sub ddlintervaltype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlintervaltype.SelectedIndexChanged
        If ddlintervaltype.SelectedValue = "Flexy" Then
            Escflex.Visible = True
            Escduration.Visible = False
            RentRevisionPanel.Visible = False
            txtnofEscltions.Text = ""
        Else
            Escflex.Visible = False
            Escduration.Visible = True
            RentRevisionPanel.Visible = False
            txtIntervaldu.Text = ""
        End If
    End Sub
    'Public Sub ddlGST_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If txtservicetax.SelectedValue = "Yes" Then
    '        Idgst.Visible = True
    '    Else
    '        Idgst.Visible = False
    '    End If
    'End Sub
    'Dim cgst As Double = 0
    'Dim sgst As Double = 0
    'Dim igst As Double = 0
    'Public Sub TXTSGST_TextChanged(sender As Object, e As EventArgs) Handles TXTSGST.TextChanged
    '    If (TXTSGST.Text <> "") Then
    '        sgst = Convert.ToInt32(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) * (Convert.ToInt32(TXTSGST.Text) / 100)
    '    End If
    '    If (TXTCGST.Text <> "") Then
    '        cgst = Convert.ToInt32(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) * (Convert.ToInt32(TXTCGST.Text) / 100)
    '    End If
    '    If (TXTIGST.Text <> "") Then
    '        igst = Convert.ToInt32(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) * (Convert.ToInt32(TXTIGST.Text) / 100)
    '    End If
    '    txtrentgst.Text = sgst + cgst + igst + Convert.ToInt32(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text))
    'End Sub
    'Public Sub TXTCGST_TextChanged(sender As Object, e As EventArgs) Handles TXTCGST.TextChanged
    '    If (TXTSGST.Text <> "") Then
    '        sgst = Convert.ToInt32(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) * (Convert.ToInt32(TXTSGST.Text) / 100)
    '    End If
    '    If (TXTCGST.Text <> "") Then
    '        cgst = Convert.ToInt32(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) * (Convert.ToInt32(TXTCGST.Text) / 100)
    '    End If
    '    If (TXTIGST.Text <> "") Then
    '        igst = Convert.ToInt32(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) * (Convert.ToInt32(TXTIGST.Text) / 100)
    '    End If
    '    txtrentgst.Text = sgst + cgst + igst + Convert.ToInt32(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text))
    'End Sub
    'Public Sub TXTIGST_TextChanged(sender As Object, e As EventArgs) Handles TXTIGST.TextChanged
    '    If (TXTSGST.Text <> "") Then
    '        sgst = Convert.ToInt32(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) * (Convert.ToInt32(TXTSGST.Text) / 100)
    '    End If
    '    If (TXTCGST.Text <> "") Then
    '        cgst = Convert.ToInt32(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) * (Convert.ToInt32(TXTCGST.Text) / 100)
    '    End If
    '    If (TXTIGST.Text <> "") Then
    '        igst = Convert.ToInt32(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) * (Convert.ToInt32(TXTIGST.Text) / 100)
    '    End If
    '    txtrentgst.Text = sgst + cgst + igst + Convert.ToInt32(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text))
    'End Sub
    'Dim llcgs As Double = 0
    'Dim llsgs As Double = 0
    'Dim lligs As Double = 0
    'Public Sub llsgst_TextChanged(sender As Object, e As EventArgs) Handles llsgst.TextChanged
    '    If (llsgst.Text <> "") Then
    '        llsgs = Convert.ToInt32(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text)) * (Convert.ToInt32(llsgst.Text) / 100)
    '    End If
    '    If (llcgst.Text <> "") Then
    '        llcgs = Convert.ToInt32(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text)) * (Convert.ToInt32(llcgst.Text) / 100)
    '    End If
    '    If (lligst.Text <> "") Then
    '        lligs = Convert.ToInt32(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text)) * (Convert.ToInt32(lligst.Text) / 100)
    '    End If
    '    llrentgst.Text = llsgs + llcgs + lligs + Convert.ToInt32(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text))
    'End Sub
    'Public Sub llcgst_TextChanged(sender As Object, e As EventArgs) Handles llcgst.TextChanged
    '    If (llsgst.Text <> "") Then
    '        llsgs = Convert.ToInt32(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text)) * (Convert.ToInt32(llsgst.Text) / 100)
    '    End If
    '    If (llcgst.Text <> "") Then
    '        llcgs = Convert.ToInt32(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text)) * (Convert.ToInt32(llcgst.Text) / 100)
    '    End If
    '    If (lligst.Text <> "") Then
    '        lligs = Convert.ToInt32(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text)) * (Convert.ToInt32(lligst.Text) / 100)
    '    End If
    '    llrentgst.Text = llsgs + llcgs + lligs + Convert.ToInt32(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text))
    'End Sub
    'Public Sub lligst_TextChanged(sender As Object, e As EventArgs) Handles lligst.TextChanged
    '    If (llsgst.Text <> "") Then
    '        llsgs = Convert.ToInt32(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text)) * (Convert.ToInt32(llsgst.Text) / 100)
    '    End If
    '    If (llcgst.Text <> "") Then
    '        llcgs = Convert.ToInt32(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text)) * (Convert.ToInt32(llcgst.Text) / 100)
    '    End If
    '    If (lligst.Text <> "") Then
    '        lligs = Convert.ToInt32(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text)) * (Convert.ToInt32(lligst.Text) / 100)
    '    End If
    '    llrentgst.Text = llsgs + llcgs + lligs + Convert.ToInt32(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text))
    'End Sub
    Public Sub AssigTextValues()
        txtInteriorCost.Text = 0
        txtfurniture.Text = 0
        txtbrokerage.Text = 0
        txtpfees.Text = 0
        txttotalrent.Text = 0
        txttotalrent.Text = 0
        txtlock.Text = 0
        txtLeasePeiodinYears.Text = 0
        txtNotiePeriod.Text = 0
        txtAvailablePower.Text = 0
        txtAdditionalPowerKWA.Text = 0
        txtNoOfTwoWheelerParking.Text = 0
        txtNoOfCarsParking.Text = 0
        txtDistanceFromAirPort.Text = 0
        txtDistanceFromRailwayStation.Text = 0
        txtDistanceFromBustop.Text = 0
        txtpmonthrent.Text = 0
        txtpsecdep.Text = 0
        txtbrkamount.Text = 0
        txtbrkamount.Text = 0
        txtbrkmob.Text = 0
        txtNoLanlords.Text = 0
    End Sub

    Protected Sub ddlpaymentmode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlpaymentmode.SelectedIndexChanged
        If ddlpaymentmode.SelectedIndex > 0 Then
            If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
                panel1.Visible = True
                panel2.Visible = False
            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                panel1.Visible = False
                panel2.Visible = True
            Else
                panel1.Visible = False
                panel2.Visible = False
            End If
        End If
    End Sub

    Public Sub Send_Mail(ByVal MailTo As String, ByVal subject As String, ByVal msg As String, ByVal strRMEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_LEASE_INSERT_AMTMAIL")
        sp.Command.AddParameter("@VC_ID", ID, DbType.String)
        sp.Command.AddParameter("@VC_MSG", msg, DbType.String)
        sp.Command.AddParameter("@VC_MAIL", MailTo, DbType.String)
        sp.Command.AddParameter("@VC_SUB", subject, DbType.String)
        sp.Command.AddParameter("@DT_MAILTIME", Date.Today(), DbType.String)
        sp.Command.AddParameter("@VC_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@VC_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@VC_MAIL_CC", strRMEmail, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub ddlproperty_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlproperty.SelectedIndexChanged
        If ddlproperty.SelectedIndex > 0 Then
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@PROP_CODE", SqlDbType.VarChar, 200)
            param(0).Value = ddlproperty.SelectedValue
            ds = New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("BIND_DETAILS_BY_PROP", param)
            If ds.Tables(0).Rows.Count > 0 Then
                txtPropAddedBy.Text = ds.Tables(0).Rows(0).Item("ADDED_BY").ToString()
                txtApprovedBy.Text = ds.Tables(0).Rows(0).Item("APPR_BY").ToString()
                txtentity.Text = ds.Tables(0).Rows(0).Item("CHE_NAME").ToString()
                'txtPropCode.Text = ds.Tables(0).Rows(0).Item("PM_PPT_CODE").ToString()
                txtInspection.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_BY").ToString()
                If (ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT").ToString() <> "") Then
                    txtInspecteddate.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT")
                End If
                ''txtInspecteddate.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT").ToString()
                txtBuiltupArea.Text = ds.Tables(0).Rows(0).Item("PM_AR_BUA_AREA").ToString()
                txtCarpetArea.Text = ds.Tables(0).Rows(0).Item("PM_AR_CARPET_AREA").ToString()
                txtCommonArea.Text = ds.Tables(0).Rows(0).Item("PM_AR_COM_AREA").ToString()
                txtSuperBulArea.Text = ds.Tables(0).Rows(0).Item("PM_AR_SBU_AREA").ToString()
                txtUsableArea.Text = ds.Tables(0).Rows(0).Item("PM_AR_USABEL_AREA").ToString()
                txtRentableArea.Text = ds.Tables(0).Rows(0).Item("PM_AR_RENT_AREA").ToString()
                txtPlotArea.Text = ds.Tables(0).Rows(0).Item("PM_AR_PLOT_AREA").ToString()
                txtCeilingHight.Text = ds.Tables(0).Rows(0).Item("PM_AR_FTC_HIGHT").ToString()
                txtBeamBottomHight.Text = ds.Tables(0).Rows(0).Item("PM_AR_FTBB_HIGHT").ToString()
                txtMaxCapacity.Text = ds.Tables(0).Rows(0).Item("PM_AR_MAX_CAP").ToString()
                txtOptCapacity.Text = ds.Tables(0).Rows(0).Item("PM_AR_OPT_CAP").ToString()
                txtSeatingCapacity.Text = ds.Tables(0).Rows(0).Item("PM_AR_SEATING_CAP").ToString()

                txtvitrified.Text = ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED").ToString()
                txtvitriremarks.Text = ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED_RMKS").ToString()
                txtwashroom.Text = ds.Tables(0).Rows(0).Item("PM_LLS_WASHROOMS").ToString()
                txtwashroomremarks.Text = ds.Tables(0).Rows(0).Item("PM_LLS_WASH_RMKS").ToString()
                txtpantry.Text = ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY").ToString()
                txtpantryremarks.Text = ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY_RMKS").ToString()
                txtshutter.Text = ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER").ToString()
                txtshutterremarks.Text = ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER_RMKS").ToString()
                txtothers.Text = ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS").ToString()
                txtothersremarks.Text = ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS_RMKS").ToString()
                txtllwork.Text = ds.Tables(0).Rows(0).Item("PM_LLS_WORK_DAYS").ToString()
                txtelectricex.Text = ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_EXISTING").ToString()
                txtelectricrq.Text = ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_REQUIRED").ToString()



                If (ds.Tables(0).Rows(0).Item("PM_AR_FLOOR_TYPE").ToString() <> "") Then
                    ddlFlooringType.ClearSelection()
                    ddlFlooringType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_AR_FLOOR_TYPE")).Selected = True
                End If

                If (ds.Tables(0).Rows(0).Item("PM_AR_FSI").ToString() = "") Then
                    ddlFSI.ClearSelection()
                    ddlFSI.SelectedValue = ""
                Else
                    ddlFSI.ClearSelection()
                    ddlFSI.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_AR_FSI") = True, 1, 0)
                End If

                txtFSI.Text = ds.Tables(0).Rows(0).Item("PM_AR_FSI_RATIO")
                txtEfficiency.Text = ds.Tables(0).Rows(0).Item("PM_AR_PREF_EFF")

                If ds.Tables(0).Rows(0).Item("PM_PPT_POA").ToString() <> "" Then
                    ddlAgreementbyPOA.ClearSelection()
                    ddlAgreementbyPOA.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_POA")).Selected = True
                    If ds.Tables(0).Rows(0).Item("PM_PPT_POA") = "Yes" Then
                        panPOA.Visible = True
                        If (ds.Tables(0).Rows(0).Item("PM_POA_NAME").ToString() <> "") Then
                            txtPOAName.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_POA_NAME"))
                        End If
                        If (ds.Tables(0).Rows(0).Item("PM_POA_ADDRESS").ToString() <> "") Then
                            txtPOAAddress.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_POA_ADDRESS"))
                        End If
                        If (ds.Tables(0).Rows(0).Item("PM_POA_PHONE_NO").ToString() <> "") Then
                            txtPOAMobile.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_POA_PHONE_NO"))
                        End If
                        If (ds.Tables(0).Rows(0).Item("PM_POA_EMAIL").ToString() <> "") Then
                            txtPOAEmail.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_POA_EMAIL"))
                        End If
                        If (ds.Tables(0).Rows(0).Item("PM_POA_LL_TYPE").ToString() <> "") Then
                            txtLLtype.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_POA_LL_TYPE"))
                        End If
                    Else
                        panPOA.Visible = False

                    End If
                End If
                ddlPropertyType.ClearSelection()
                ddlPropertyType.SelectedValue = ds.Tables(0).Rows(0).Item("PM_PPT_TYPE")

            End If
        Else
            txtPropAddedBy.Text = ""
            txtApprovedBy.Text = ""
        End If
    End Sub
    Protected Sub ddlFSI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFSI.SelectedIndexChanged
        divFSI.Visible = IIf(ddlFSI.SelectedValue = "1", True, False)
    End Sub
    Protected Sub ddlAgreementbyPOA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAgreementbyPOA.SelectedIndexChanged
        If ddlAgreementbyPOA.SelectedValue = "Yes" Then
            panPOA.Visible = True
        Else
            panPOA.Visible = False
        End If
    End Sub

    'Protected Sub ddlServiceTaxApplicable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlServiceTaxApplicable.SelectedIndexChanged
    '    AddLeaseDetails.Visible = False
    '    Landlord.Visible = True
    '    If ddlServiceTaxApplicable.SelectedValue = "Yes" Then
    '        llgst.Visible = True
    '    Else
    '        llgst.Visible = False
    '    End If
    'End Sub

    Protected Sub ddlPropertyTaxApplicable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPropertyTaxApplicable.SelectedIndexChanged
        AddLeaseDetails.Visible = False
        Landlord.Visible = True
        If ddlPropertyTaxApplicable.SelectedValue = "Yes" Then
            Property1.Visible = True
            txtPropertyTax.Text = ""
        Else
            Property1.Visible = False
            txtPropertyTax.Text = "0"
        End If
    End Sub

    Protected Sub ddlElectricalMeter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlElectricalMeter.SelectedIndexChanged
        If ddlElectricalMeter.SelectedValue = "Yes" Then
            Meter.Visible = True
            txtMeterLocation.Text = ""
        Else
            Meter.Visible = False
            txtMeterLocation.Text = ""
        End If
    End Sub

    Protected Sub txtInvestedArea_TextChanged(sender As Object, e As EventArgs) Handles txtInvestedArea.TextChanged
        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtfurniture.Text = "", 0, txtfurniture.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
    End Sub

    Protected Sub txtmain1_TextChanged(sender As Object, e As EventArgs) Handles txtmain1.TextChanged
        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtfurniture.Text = "", 0, txtfurniture.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
    End Sub

    Protected Sub txtfurniture_TextChanged(sender As Object, e As EventArgs) Handles txtfurniture.TextChanged
        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtfurniture.Text = "", 0, txtfurniture.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
    End Sub


    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            If Not Page.IsValid Then
                Exit Sub
            End If
            Dim firstDate1 As Date = Date.Parse(txtsdate.Text)
            Dim SecondDate1 As Date = Date.Parse(txtedate.Text)
            If firstDate1.Date > SecondDate1.Date Then
                lblmesg.Text = "Agreement Start Date must be less than Agreement End Date"
                Exit Sub
            End If
            If Session("escdt") >= SecondDate1 Then
                lblmesg.Text = "Escalation date should not be greater than Lease Expiry Date"
                Exit Sub
            End If
            If txtSuperBulArea.Text <> 0 And txtBuiltupArea.Text <> 0 Then
                If CInt(txtBuiltupArea.Text) > CInt(txtSuperBulArea.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Builtup Area cannot be more than Super Builtup Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            If txtBuiltupArea.Text <> 0 And txtCarpetArea.Text <> 0 Then
                If CInt(txtBuiltupArea.Text) < CInt(txtCarpetArea.Text) Then 'Or CInt(txtBuiltupArea.Text) > CInt(txtRentableArea.Text) 
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Builtup Area cannot be less than Carpet Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            If txtCarpetArea.Text <> 0 And txtBuiltupArea.Text <> 0 Then
                If CInt(txtCarpetArea.Text) > CInt(txtBuiltupArea.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be more than Builtup Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            If txtBuiltupArea.Text <> 0 And txtRentableArea.Text <> 0 Then
                If CInt(txtBuiltupArea.Text) < CInt(txtRentableArea.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Builtup Area cannot be less than Rentable Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            If txtCarpetArea.Text <> 0 And txtRentableArea.Text <> 0 Then
                If CInt(txtCarpetArea.Text) < CInt(txtRentableArea.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be less than Rentable Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            If txtSuperBulArea.Text <> 0 And txtRentableArea.Text <> 0 Then
                If CInt(txtSuperBulArea.Text) < CInt(txtRentableArea.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Super Builtup Area cannot be less than Rentable Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            If txtCarpetArea.Text <> 0 And txtSuperBulArea.Text <> 0 Then
                If CInt(txtCarpetArea.Text) > CInt(txtSuperBulArea.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Super Builtup Area cannot be less than Carpet Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If

            Dim LE_lst As New List(Of LeaseExpenses)()
            Dim LE_obj As New LeaseExpenses()
            For Each row As GridViewRow In gvLeaseExpences.Rows
                LE_obj = New LeaseExpenses()
                Dim lblServiceID As Label = DirectCast(row.FindControl("lblServiceID"), Label)
                Dim ddlServiceProvider As DropDownList = DirectCast(row.FindControl("ddlServiceProvider"), DropDownList)
                Dim ddlInputType As DropDownList = DirectCast(row.FindControl("ddlInputType"), DropDownList)
                Dim txtValue As TextBox = DirectCast(row.FindControl("txtValue"), TextBox)
                Dim ddlPaidBy As DropDownList = DirectCast(row.FindControl("ddlPaidBy"), DropDownList)
                If ddlServiceProvider.SelectedIndex > 0 Then
                    If ddlInputType.SelectedIndex > 0 Then
                        If txtValue.Text <> "" Or txtValue.Text <> String.Empty Then
                            If ddlPaidBy.SelectedIndex > 0 Then
                                LE_obj.PM_EXP_HEAD = lblServiceID.Text
                                LE_obj.PM_EXP_SERV_PROVIDER = ddlServiceProvider.SelectedValue
                                LE_obj.PM_EXP_INP_TYPE = ddlInputType.SelectedValue
                                LE_obj.PM_EXP_LES_VAL = IIf(txtValue.Text = "", 0, txtValue.Text)
                                LE_obj.PM_EXP_PAID_BY = ddlPaidBy.SelectedValue
                                LE_lst.Add(LE_obj)
                            Else
                                lblMsg.Text = "Please select paid by against service provider in Lease Expenses"
                                Exit Sub
                            End If
                        Else
                            lblMsg.Text = "Please enter Component of the lease value against service provider in Lease Expenses"
                            Exit Sub
                        End If
                    Else
                        lblMsg.Text = "Please select input type against service provider in Lease Expenses"
                        Exit Sub
                    End If
                End If
            Next


            'For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To 10

            '    Dim item As RepeaterItem = TryCast((TryCast(sender, Button)).NamingContainer, RepeaterItem)
            '    Dim message As String = (TryCast(item.FindControl("lblRevYear"), Label)).Text
            '    Dim txtValues As String = (TryCast(item.FindControl("txtRevision"), TextBox)).Text
            'Next

            Dim param(89) As SqlParameter
            'Area &Cost
            param(0) = New SqlParameter("@CTS_NUMBER", SqlDbType.VarChar)
            param(0).Value = txtLnumber.Text
            param(1) = New SqlParameter("@ENTITLE_LEASE_AMOUNT", SqlDbType.Decimal)
            param(1).Value = IIf(txtentitle.Text = "", 0, txtentitle.Text)
            param(2) = New SqlParameter("@LEASE_RENT", SqlDbType.Decimal)
            param(2).Value = IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)
            param(3) = New SqlParameter("@SECURITY_DEPOSIT", SqlDbType.Decimal)
            param(3).Value = IIf(txtpay.Text = "", 0, txtpay.Text)
            param(4) = New SqlParameter("@SECURITY_DEP_MONTHS", SqlDbType.Int)
            param(4).Value = Convert.ToInt32(ddlSecurityDepMonths.SelectedValue)
            param(5) = New SqlParameter("@RENT_FREE_PRD", SqlDbType.Int)
            param(5).Value = Convert.ToInt32(txtRentFreePeriod.Text)
            param(6) = New SqlParameter("@RENT_SFT_CARPET", SqlDbType.Decimal)
            param(6).Value = IIf(txtRentPerSqftCarpet.Text = "", 0, txtRentPerSqftCarpet.Text)
            param(7) = New SqlParameter("@RENT_SFT_BUA", SqlDbType.Decimal)
            param(7).Value = IIf(txtRentPerSqftBUA.Text = "", 0, txtRentPerSqftBUA.Text)
            param(8) = New SqlParameter("@INTERIORCOST", SqlDbType.Decimal)
            param(8).Value = IIf(txtInteriorCost.Text = "", 0, txtInteriorCost.Text)

            'Charges
            param(9) = New SqlParameter("@REGISTRATION_CHARGES", SqlDbType.Decimal)
            param(9).Value = IIf(txtregcharges.Text = "", 0, txtregcharges.Text)
            param(10) = New SqlParameter("@STAMP_DUTY", SqlDbType.Decimal)
            param(10).Value = IIf(txtsduty.Text = "", 0, txtsduty.Text)
            param(11) = New SqlParameter("@FURNITURE", SqlDbType.Decimal)
            param(11).Value = IIf(txtfurniture.Text = "", 0, txtfurniture.Text)
            param(12) = New SqlParameter("@PROFESSIONAL_FEES", SqlDbType.Decimal)
            param(12).Value = IIf(txtpfees.Text = "", 0, txtpfees.Text)
            param(13) = New SqlParameter("@MAINTENANCE_CHARGES", SqlDbType.Decimal)
            param(13).Value = IIf(txtmain1.Text = "", 0, txtmain1.Text)
            'param(14) = New SqlParameter("@SERVICE_TAX", SqlDbType.VarChar)
            'param(14).Value = txtservicetax.SelectedValue
            param(14) = New SqlParameter("@GST_TAX", SqlDbType.VarChar)
            param(14).Value = txtservicetax.SelectedValue
            param(15) = New SqlParameter("@PROPERTY_TAX", SqlDbType.Decimal)
            param(15).Value = IIf(txtproptax.Text = "", 0, txtproptax.Text)
            param(16) = New SqlParameter("@BROKERAGE_AMOUNT_CHARGE", SqlDbType.Decimal)
            param(16).Value = IIf(txtbrokerage.Text = "", 0, txtbrokerage.Text)

            'AGREEMENT DETAILS 
            param(17) = New SqlParameter("@EFF_DOA", SqlDbType.DateTime)
            param(17).Value = Convert.ToDateTime(txtsdate.Text)
            param(18) = New SqlParameter("@EXP_DOA", SqlDbType.DateTime)
            param(18).Value = Convert.ToDateTime(txtedate.Text)
            param(19) = New SqlParameter("@PPT_SNO", SqlDbType.Int)
            param(19).Value = ddlproperty.SelectedValue
            param(20) = New SqlParameter("@LOCKINPERIOD", SqlDbType.Int)
            param(20).Value = Convert.ToInt32(IIf(txtlock.Text = "", 0, txtlock.Text))
            param(21) = New SqlParameter("@LEASEPERIOD", SqlDbType.Int)
            param(21).Value = Convert.ToInt32(IIf(txtLeasePeiodinYears.Text = "", 0, txtLeasePeiodinYears.Text))
            param(22) = New SqlParameter("@NOTICEPERIOD", SqlDbType.Int)
            param(22).Value = Convert.ToInt32(IIf(txtNotiePeriod.Text = "", 0, txtNotiePeriod.Text))

            'BROKAGE DETAILS
            param(23) = New SqlParameter("@BROKERAGE_AMOUNT", SqlDbType.Decimal)
            param(23).Value = IIf(txtbrkamount.Text = "", 0, txtbrkamount.Text)
            param(24) = New SqlParameter("@BROKER_NAME", SqlDbType.VarChar)
            param(24).Value = txtbrkname.Text
            param(25) = New SqlParameter("@BROKER_ADDR", SqlDbType.VarChar)
            param(25).Value = txtbrkaddr.Text
            param(26) = New SqlParameter("@BROKER_PAN", SqlDbType.VarChar)
            param(26).Value = txtbrkpan.Text
            param(27) = New SqlParameter("@BROKER_EMAIL", SqlDbType.VarChar)
            param(27).Value = txtbrkremail.Text
            param(28) = New SqlParameter("@BROKER_CONTACT", SqlDbType.VarChar)
            param(28).Value = txtbrkmob.Text

            'UTILITY/POWER BACKUP DETAILS
            param(29) = New SqlParameter("@DGSET", SqlDbType.VarChar)
            param(29).Value = ddlDgSet.SelectedValue
            param(30) = New SqlParameter("@DGSET_COM_UNIT", SqlDbType.VarChar)
            param(30).Value = txtDgSetPerUnit.Text
            param(31) = New SqlParameter("@DGSET_LOCATION", SqlDbType.VarChar)
            param(31).Value = txtDgSetLocation.Text
            param(32) = New SqlParameter("@SPACE_SERVO_STAB", SqlDbType.VarChar)
            param(32).Value = txtSpaceServoStab.Text
            param(33) = New SqlParameter("@ELEC_METER", SqlDbType.VarChar)
            param(33).Value = ddlElectricalMeter.SelectedValue
            param(34) = New SqlParameter("@METER_LOC", SqlDbType.VarChar)
            param(34).Value = txtMeterLocation.Text
            param(35) = New SqlParameter("@EARTHING_PIT", SqlDbType.VarChar)
            param(35).Value = txtEarthingPit.Text
            param(36) = New SqlParameter("@AVAIL_POWER", SqlDbType.Float)
            param(36).Value = IIf(txtAvailablePower.Text = "", 0, txtAvailablePower.Text)
            param(37) = New SqlParameter("@ADDITIONAL_POWER", SqlDbType.Float)
            param(37).Value = IIf(txtAdditionalPowerKWA.Text = "", 0, txtAdditionalPowerKWA.Text)
            param(38) = New SqlParameter("@POWER_SPEC", SqlDbType.VarChar)
            param(38).Value = txtPowerSpecification.Text

            'OTHER SERVICES
            param(39) = New SqlParameter("@TWO_WHL_PARK", SqlDbType.Int)
            param(39).Value = Convert.ToInt32(txtNoOfTwoWheelerParking.Text)
            param(40) = New SqlParameter("@CAR_PARK", SqlDbType.Int)
            param(40).Value = Convert.ToInt32(txtNoOfCarsParking.Text)
            param(41) = New SqlParameter("@AIRPRT_DIST", SqlDbType.Float)
            param(41).Value = IIf(txtDistanceFromAirPort.Text = "", 0, txtDistanceFromAirPort.Text)
            param(42) = New SqlParameter("@RAIL_DIST", SqlDbType.Float)
            param(42).Value = IIf(txtDistanceFromRailwayStation.Text = "", 0, txtDistanceFromRailwayStation.Text)
            param(43) = New SqlParameter("@RAIL_BUSTOP", SqlDbType.Float)
            param(43).Value = IIf(txtDistanceFromBustop.Text = "", 0, txtDistanceFromBustop.Text)

            'LEASE ESCALATION DETAILS   
            param(44) = New SqlParameter("@OPTION_LEASE_ESC", SqlDbType.VarChar)
            param(44).Value = ddlesc.SelectedValue
            'param(45) = New SqlParameter("@ESC_TYPE", SqlDbType.VarChar)
            'param(45).Value = ddlLeaseEscType.SelectedValue 
            ''param(45) = New SqlParameter("@LL_ESCAL_ON", SqlDbType.VarChar)
            ''param(45).Value = ddlEscalation.SelectedValue


            'param(46) = New SqlParameter("@LEASEHOLD_IMPROVEMENTS", SqlDbType.VarChar)
            'param(46).Value = txtLeaseHoldImprovements.Text
            ''param(46) = New SqlParameter("@LL_ESCAL_TYPE", SqlDbType.VarChar)
            ''param(46).Value = ddlEscalation.SelectedValue
            'param(47) = New SqlParameter("@LEASE_COMMENTS", SqlDbType.VarChar)
            'param(47).Value = txtComments.Text
            ''param(47) = New SqlParameter("@LL_ESCAL_INTVL_TYPE", SqlDbType.VarChar)
            ''param(47).Value = ddlintervaltype.SelectedValue
            'param(48) = New SqlParameter("@DUE_DILIGENCE", SqlDbType.VarChar)
            'param(48).Value = ddlDueDilegence.SelectedValue
            ''param(48) = New SqlParameter("@LL_ESCAL_INTVL", SqlDbType.VarChar)
            ''param(48).Value = txtIntervaldu.Text
            Dim selectedItems As String = [String].Join(",", ReminderCheckList.Items.OfType(Of ListItem)().Where(Function(r) r.Selected).[Select](Function(r) r.Value))
            param(45) = New SqlParameter("@REMINDER_BEFORE", SqlDbType.VarChar)
            param(45).Value = selectedItems

            'Rent Revision
            param(46) = New SqlParameter("@TOTAL_RENT", SqlDbType.Decimal)
            param(46).Value = IIf(txttotalrent.Text = "", 0, txttotalrent.Text)
            param(47) = New SqlParameter("@TENURE", SqlDbType.VarChar)
            param(47).Value = ddlTenure.SelectedValue

            'Other Details
            param(48) = New SqlParameter("@COMPETE_VISCINITY", SqlDbType.VarChar)
            param(48).Value = txtCompetitorsVicinity.Text
            param(49) = New SqlParameter("@ROLLING_SHUTTER", SqlDbType.VarChar)
            param(49).Value = ddlRollingShutter.SelectedValue
            param(50) = New SqlParameter("@OFFCIE_EQUIPMENTS", SqlDbType.VarChar)
            param(50).Value = txtOfficeEquipments.Text

            Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC As ImageClas
            Dim i As Int32 = 0
            Dim fileSize As Int64 = 0
            If fu1.PostedFiles IsNot Nothing Then
                Dim count = fu1.PostedFiles.Count
                While (i < count)
                    fileSize = fu1.PostedFiles(i).ContentLength + fileSize
                    i = i + 1
                End While
                If (fileSize > 20971520) Then
                    lblMsg.Text = "Upload GSB Images size should not be greater than 20 MB."
                    Exit Sub
                End If
                For Each File In fu1.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC = New ImageClas()
                        IC.Filename = File.FileName
                        IC.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass.Add(IC)
                    End If
                Next
            End If

            param(51) = New SqlParameter("@IMAGES", SqlDbType.Structured)
            param(51).Value = UtilityService.ConvertToDataTable(Imgclass)
            param(52) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(52).Value = Session("Uid").ToString
            param(53) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(53).Value = Session("COMPANYID")
            param(54) = New SqlParameter("@PPT_CODE", SqlDbType.VarChar)
            param(54).Value = txtPropCode.Text
            param(55) = New SqlParameter("@POA_SIGNED", SqlDbType.VarChar)
            param(55).Value = ddlAgreementbyPOA.SelectedValue
            param(56) = New SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
            param(56).Value = lblLeaseReqId.Text

            'POA Details
            param(57) = New SqlParameter("@POA_NAME", SqlDbType.VarChar)
            param(57).Value = txtPOAName.Text
            param(58) = New SqlParameter("@POA_ADDRESS", SqlDbType.VarChar)
            param(58).Value = txtPOAAddress.Text
            param(59) = New SqlParameter("@POA_MOBILE", SqlDbType.VarChar)
            param(59).Value = txtPOAMobile.Text
            param(60) = New SqlParameter("@POA_EMAIL", SqlDbType.VarChar)
            param(60).Value = txtPOAEmail.Text

            'Rent Revision
            'Dim RR_lst As New List(Of RentRevision)()
            'Dim rr_obj As New RentRevision()
            'For Each i As RepeaterItem In rpRevision.Items
            '    rr_obj = New RentRevision()
            '    Dim txtRevision As TextBox = DirectCast(i.FindControl("txtRevision"), TextBox)
            '    Dim lblRevYear As Label = DirectCast(i.FindControl("lblRevYear"), Label)
            '    If txtRevision IsNot Nothing Then
            '        rr_obj.RR_Year = lblRevYear.Text
            '        rr_obj.RR_Percentage = IIf(txtRevision.Text = "", 0, txtRevision.Text)
            '        RR_lst.Add(rr_obj)
            '    End If
            'Next
            'param(61) = New SqlParameter("@REVISIONLIST", SqlDbType.Structured)
            'param(61).Value = UtilityService.ConvertToDataTable(RR_lst)
            param(61) = New SqlParameter("@PM_LAD_LOCK_INPEDATE", SqlDbType.DateTime)
            'param(61).Value = Convert.ToDateTime(Txtlockinpeamonth.Text)
            param(61).Value = IIf(Txtlockinpeamonth.Text = "", DBNull.Value, Txtlockinpeamonth.Text)

            param(62) = New SqlParameter("@LEASEEXPENSESLIST", SqlDbType.Structured)
            param(62).Value = UtilityService.ConvertToDataTable(LE_lst)
            param(63) = New SqlParameter("@COST_TYPE", SqlDbType.VarChar)
            param(63).Value = rblCostType.SelectedValue
            param(64) = New SqlParameter("@SEAT_COST", SqlDbType.Decimal)
            param(64).Value = IIf(txtSeatCost.Text = "", 0, txtSeatCost.Text)

            param(65) = New SqlParameter("@NO_OF_LL", SqlDbType.Int)
            param(65).Value = Convert.ToInt32(txtNoLanlords.Text)

            param(66) = New SqlParameter("@PM_LO_ADDI_PARK_CHRG", SqlDbType.Decimal)
            param(66).Value = IIf(txtadditionalparking.Text = "", 0, txtadditionalparking.Text)
            param(67) = New SqlParameter("@RENT_DATE", SqlDbType.DateTime)
            param(67).Value = Convert.ToDateTime(txtrentcommencementdate.Text)

            param(68) = New SqlParameter("@PM_LAD_START_DT_AGREEMENT", SqlDbType.DateTime)
            param(68).Value = Convert.ToDateTime(txtstartagreDT.Text)
            param(69) = New SqlParameter("@LEASE_STATUS", SqlDbType.VarChar)
            param(69).Value = ddlstatus.SelectedValue


            param(70) = New SqlParameter("@CARPET_AREA", SqlDbType.VarChar)
            param(70).Value = txtCarpetArea.Text

            param(71) = New SqlParameter("@BUILTUP_AREA", SqlDbType.VarChar)
            param(71).Value = txtBuiltupArea.Text
            param(72) = New SqlParameter("@COMMON_AREA", SqlDbType.VarChar)
            param(72).Value = txtCommonArea.Text
            param(73) = New SqlParameter("@RENTABLE_AREA", SqlDbType.VarChar)
            param(73).Value = txtRentableArea.Text

            param(74) = New SqlParameter("@USABLE_AREA", SqlDbType.VarChar)
            param(74).Value = txtUsableArea.Text

            param(75) = New SqlParameter("@SUPER_BLT_AREA", SqlDbType.VarChar)
            param(75).Value = txtSuperBulArea.Text
            param(76) = New SqlParameter("@PLOT_AREA", SqlDbType.VarChar)
            param(76).Value = txtPlotArea.Text

            param(77) = New SqlParameter("@FTC_HIGHT", SqlDbType.VarChar)
            param(77).Value = txtCeilingHight.Text
            param(78) = New SqlParameter("@FTBB_HIGHT", SqlDbType.VarChar)
            param(78).Value = txtBeamBottomHight.Text


            param(79) = New SqlParameter("@MAX_CAPACITY", SqlDbType.VarChar)
            param(79).Value = txtMaxCapacity.Text
            param(80) = New SqlParameter("@OPTIMUM_CAPACITY", SqlDbType.VarChar)
            param(80).Value = txtOptCapacity.Text

            param(81) = New SqlParameter("@SEATING_CAPACITY", SqlDbType.VarChar)
            param(81).Value = txtSeatingCapacity.Text
            param(82) = New SqlParameter("@FLR_TYPE", SqlDbType.VarChar)
            param(82).Value = ddlFlooringType.SelectedValue
            param(83) = New SqlParameter("@FSI", SqlDbType.VarChar)
            param(83).Value = ddlFSI.SelectedValue
            param(84) = New SqlParameter("@FSI_RATIO", SqlDbType.VarChar)
            param(84).Value = txtFSI.Text
            param(85) = New SqlParameter("@PFR_EFF", SqlDbType.VarChar)
            param(85).Value = txtEfficiency.Text
            param(86) = New SqlParameter("@PROPERTY_TYPE", SqlDbType.VarChar)
            param(86).Value = ddlPropertyType.SelectedValue
            param(87) = New SqlParameter("@POA_LL_TYPE", SqlDbType.VarChar)
            param(87).Value = txtLLtype.Text
            param(88) = New SqlParameter("@AMENTIPAID", SqlDbType.VarChar)
            param(88).Value = ddlamenpaid.SelectedValue
            param(89) = New SqlParameter("@MAINPAID", SqlDbType.VarChar)
            param(89).Value = ddlmaintpaid.SelectedValue
            'param(90) = New SqlParameter("@SGST", SqlDbType.VarChar)
            'param(90).Value = TXTSGST.Text
            ''param(91) = New SqlParameter("@CGST", SqlDbType.VarChar)
            ''param(91).Value = TXTCGST.Text
            'param(92) = New SqlParameter("@IGST", SqlDbType.VarChar)
            'param(92).Value = TXTIGST.Text
            ViewState("NO_OF_LL") = Convert.ToInt32(txtNoLanlords.Text)
            ViewState("LES_TOT_RENT") = Convert.ToDecimal(txtInvestedArea.Text)
            ViewState("LES_SEC_DEPOSIT") = Convert.ToDecimal(txtpay.Text)
            ViewState("LES_MAINT_CRGS") = Convert.ToDecimal(txtmain1.Text)
            ViewState("LES_AMENTS_CRGS") = Convert.ToDecimal(txtfurniture.Text)

            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_ADD_LEASE", param)


                If rblEscalationType.SelectedValue = "Lease" Then
                    'ClearTextBox(Me)
                    'ClearDropdown(Me)
                    'AssigTextValues()
                End If
                Landlord.Visible = True
                If rblEscalationType.SelectedValue = "LandLoard" Then
                    LandlordEscalation.Visible = True
                End If
                AddLeaseDetails.Visible = False
                'EscalationType1.Visible = True
                'EscalationType2.Visible = True
                While sdr.Read()
                    If sdr("result").ToString() = "SUCCESS" Then
                        'lblMsg.Text = "Lease Request Raised Successfully : " + lblLeaseReqId.Text
                        'lblLeaseReqId.Text = ""
                        Session("ID") = sdr("ID").ToString()
                        Session("REQ_ID") = sdr("REQUEST_ID").ToString()

                        'BindReqId()
                        panPOA.Visible = False
                        'If rblEscalationType.SelectedValue = "Lease" Then
                        '    'ClearTextBox(Me)
                        '    'ClearDropdown(Me)
                        '    'AssigTextValues()
                        'End If
                        Landlord.Visible = True
                        AddLeaseDetails.Visible = False
                    Else
                        lblMsg.Text = "Something went wrong. Please try again later."
                    End If
                End While
                sdr.Close()
            End Using

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub txtedate_TextChanged(sender As Object, e As EventArgs) Handles txtedate.TextChanged
        Dim firstDate1 As Date = Date.Parse(txtsdate.Text)
        Dim SecondDate1 As Date = Date.Parse(txtedate.Text)
        If firstDate1.Date < SecondDate1.Date Then
            Dim years1 As Integer = DateDiff(DateInterval.Year, CDate(SecondDate1), CDate(firstDate1))
            'Dim frmdate As DateTime = txtsdate.Text
            'Dim todate As DateTime = txtedate.Text
            txtLeasePeiodinYears.Text = years1
            lblmesg.Text = ""
            If ddlintervaltype.SelectedValue = "Flexy" Then
                BindRentRevisiondtflex()
                For Each item As RepeaterItem In Repeater2.Items
                    If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                        Dim lbl = CType(item.FindControl("lblRevYear"), Label)
                        Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
                        lbl.Visible = False
                        lbtxt.Visible = False
                    End If
                Next
            Else
                BindRentRevisiondt()
                For Each item As RepeaterItem In Repeater2.Items
                    If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                        'Dim lbl = CType(item.FindControl("lblEscno"), Label)
                        Dim lbtxt = CType(item.FindControl("ddlYear"), TextBox)
                        Dim lbmnth = CType(item.FindControl("ddlMonth"), TextBox)
                        Dim lbday = CType(item.FindControl("ddlDay"), TextBox)
                        Dim lbesc = CType(item.FindControl("TextBox1"), TextBox)
                        Dim lbyr = CType(item.FindControl("lbyear"), Label)
                        Dim lbmth = CType(item.FindControl("lbmnt"), Label)
                        Dim lbdy = CType(item.FindControl("lbldy"), Label)
                        Dim lble = CType(item.FindControl("lbldf"), Label)
                        'lbl.Visible = False
                        lbtxt.Visible = False
                        lbmnth.Visible = False
                        lbesc.Visible = False
                        lbday.Visible = False
                        lbyr.Visible = False
                        lbmth.Visible = False
                        lbdy.Visible = False
                        lble.Visible = False
                    End If
                Next
            End If
        Else
            lblmesg.Text = "Agreement Start Date must be less than Agreement End Date"
            RentRevisionPanel.Visible = False
        End If
    End Sub
    Private Sub BindFlooringTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_FLOORING_TYPES")
        ddlFlooringType.DataSource = sp3.GetDataSet()
        ddlFlooringType.DataTextField = "PM_FT_TYPE"
        ddlFlooringType.DataValueField = "PM_FT_SNO"
        ddlFlooringType.DataBind()
        ddlFlooringType.Items.Insert(0, New ListItem("--Select Flooring Type--", 0))
    End Sub
    Protected Sub btnLandlord_Click(sender As Object, e As EventArgs) Handles btnLandlord.Click
        lblMsgLL.Text = ""
        lblMsg.Text = ""
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_RENT_DETAILS")
            sp1.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.Int32)
            sp1.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
            sp1.Command.AddParameter("@ID", Session("ID"), DbType.Int32)
            sp1.Command.AddParameter("@NOOFLANDORD", Convert.ToInt32(txtNoLanlords.Text), SqlDbType.Int)
            Dim ds As New DataSet
            ds = sp1.GetDataSet

            'To check the lanlord count
            If ds.Tables(0).Rows(0).Item("LL_COUNT") >= ViewState("NO_OF_LL") Then
                lblMsgLL.Text = "You have already reached maximum number of landlords " + ViewState("NO_OF_LL").ToString()
                Exit Sub
            End If

            'To check the total rent and security deposit if it is last lanlord.
            If ViewState("NO_OF_LL") = ds.Tables(0).Rows(0).Item("LL_COUNT") + 1 Then

                Dim TOT_LL_RENT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_RENT")) + Convert.ToDecimal(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text))
                Dim TOT_LL_SEC_DEPOSIT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_SEC_DEPOSIT")) + Convert.ToDecimal(IIf(txtpsecdep.Text = "", 0, txtpsecdep.Text))
                Dim TOT_LL_MAINT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_MAINT")) + Convert.ToDecimal(IIf(txtllmaint.Text = "", 0, txtllmaint.Text))
                Dim TOT_LL_AMENTIES As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_AMENITIES")) + Convert.ToDecimal(IIf(txtllAments.Text = "", 0, txtllAments.Text))

                If TOT_LL_RENT <> ViewState("LES_TOT_RENT") Then
                    lblMsgLL.Text = "Landlord Rent Payable Should be " + (Math.Round(ViewState("LES_TOT_RENT"), 0) - Math.Round(ds.Tables(0).Rows(0).Item("TOT_LL_RENT"))).ToString() + ", Should Match With Lease Total Rent " + Math.Round(ViewState("LES_TOT_RENT"), 0).ToString()
                    Exit Sub
                End If
                If TOT_LL_SEC_DEPOSIT <> ViewState("LES_SEC_DEPOSIT") Then
                    lblMsgLL.Text = "Landlord Security Deposit Should be " + (Math.Round(ViewState("LES_SEC_DEPOSIT"), 0) - Math.Round(ds.Tables(0).Rows(0).Item("TOT_LL_SEC_DEPOSIT"))).ToString() + ", Should Match With Lease Total Security Deposit " + Math.Round(ViewState("LES_SEC_DEPOSIT"), 0).ToString()
                    Exit Sub
                End If
                'If ddlmaintpaid.SelectedValue = "Company" Then
                If TOT_LL_MAINT <> ViewState("LES_MAINT_CRGS") Then
                    lblMsgLL.Text = "Landlords Total Maintenance Should be" + (Math.Round(ViewState("LES_MAINT_CRGS"), 0) - Math.Round(ds.Tables(0).Rows(0).Item("TOT_LL_MAINT"))).ToString() + ", Should Match With Lease Maintenance Amount" + Math.Round(ViewState("LES_MAINT_CRGS"), 0).ToString()
                    'lblMsgLL.Text = "Landlords Total Maintenance Should Match With Lease Maintenance Amount " + txtmain1.Text.ToString()
                    Exit Sub
                End If
                'End If
                'If ddlamenpaid.SelectedValue = "Company" Then
                If TOT_LL_AMENTIES <> ViewState("LES_AMENTS_CRGS") Then
                    lblMsgLL.Text = "Landlords Total Amenities Should be" + (Math.Round(ViewState("LES_AMENTS_CRGS"), 0) - Math.Round(ds.Tables(0).Rows(0).Item("TOT_LL_AMENITIES"))).ToString() + ", Should Match With Lease Amenities Amount " + Math.Round(ViewState("LES_AMENTS_CRGS"), 0).ToString()
                    'lblMsgLL.Text = "Landlords Total Amenities Should Match With Lease Amenities Amount " + txtfurniture.Text.ToString()
                    Exit Sub
                End If
                'End If
                'btnLandlord.Enabled = False
            Else
                ' To check the total rent and security deposit if it is not last lanlord.
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim TOT_LL_RENT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_RENT")) + Convert.ToDecimal(IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text))
                    Dim TOT_LL_SEC_DEPOSIT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_SEC_DEPOSIT")) + Convert.ToDecimal(IIf(txtpsecdep.Text = "", 0, txtpsecdep.Text))
                    Dim TOT_LL_MAINT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_MAINT")) + Convert.ToDecimal(IIf(txtllmaint.Text = "", 0, txtllmaint.Text))
                    Dim TOT_LL_AMENTIES As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_AMENITIES")) + Convert.ToDecimal(IIf(txtllAments.Text = "", 0, txtllAments.Text))
                    If TOT_LL_RENT > ViewState("LES_TOT_RENT") Then
                        'lblMsgLL.Text = "Landlord Rent Payable " + Math.Round(TOT_LL_RENT, 0).ToString() + " Should Not Be More Than Lease Total Rent " + Math.Round(ViewState("LES_TOT_RENT"), 0).ToString()
                        lblMsgLL.Text = "Landlord Rent Payable can't be more than " + (Math.Round(ViewState("LES_TOT_RENT"), 0) - Math.Round(ds.Tables(0).Rows(0).Item("TOT_LL_RENT"))).ToString() + ", Should Match With Lease Total Rent " + Math.Round(ViewState("LES_TOT_RENT"), 0).ToString()
                        Exit Sub
                    End If
                    If TOT_LL_SEC_DEPOSIT > ViewState("LES_SEC_DEPOSIT") Then
                        'lblMsgLL.Text = "Landlord Security Deposit " + Math.Round(TOT_LL_SEC_DEPOSIT, 0).ToString() + " Should Not Be More Than Lease Security Deposit " + Math.Round(ViewState("LES_SEC_DEPOSIT"), 0).ToString()
                        lblMsgLL.Text = "Landlord Security Deposit can't be more than " + (Math.Round(ViewState("LES_SEC_DEPOSIT"), 0) - Math.Round(ds.Tables(0).Rows(0).Item("TOT_LL_SEC_DEPOSIT"))).ToString() + ", Should Match With Lease Total Security Deposit " + Math.Round(ViewState("LES_SEC_DEPOSIT"), 0).ToString()
                        Exit Sub
                    End If
                    'If ddlmaintpaid.SelectedValue = "Company" Then
                    If TOT_LL_MAINT > txtmain1.Text Then
                        lblMsgLL.Text = "Landlords Total Maintenance can't be more than Lease Maintenance Amount " + txtmain1.Text.ToString()
                        Exit Sub
                    End If
                    'End If
                    'If ddlamenpaid.SelectedValue = "Company" Then
                    If TOT_LL_AMENTIES > txtfurniture.Text Then
                        lblMsgLL.Text = "Landlords Total Amenities can't be more than Lease Amenities Amount " + txtfurniture.Text.ToString()
                        Exit Sub
                    End If
                    'End If
                End If
            End If

            Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC As ImageClas
            Dim i As Int32 = 0
            Dim fileSize As Int64 = 0
            If TDSFu1.PostedFiles IsNot Nothing Then
                Dim count = TDSFu1.PostedFiles.Count
                While (i < count)
                    fileSize = TDSFu1.PostedFiles(i).ContentLength + fileSize
                    i = i + 1
                End While
                If (fileSize > 20971520) Then
                    lblMsg.Text = "Upload GSB Document size should not be greater than 20 MB."
                    Exit Sub
                End If
                For Each File In TDSFu1.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC = New ImageClas()
                        IC.Filename = File.FileName
                        IC.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass.Add(IC)
                    End If
                Next
            End If

            Dim param As SqlParameter() = New SqlParameter(33) {}
            param(0) = New SqlParameter("@ID", DbType.Int32)
            param(0).Value = Session("ID")
            param(1) = New SqlParameter("@REQ_ID", DbType.String)
            param(1).Value = Session("REQ_ID")
            param(2) = New SqlParameter("@LAN_NAME", DbType.String)
            param(2).Value = txtName.Text
            param(3) = New SqlParameter("@LAN_ADDRESS1", DbType.String)
            param(3).Value = txtAddress.Text
            param(4) = New SqlParameter("@LAN_ADDRESS2", DbType.String)
            param(4).Value = txtAddress2.Text
            param(5) = New SqlParameter("@LAN_ADDRESS3", DbType.String)
            param(5).Value = txtAddress3.Text
            param(6) = New SqlParameter("@LAN_STATE", DbType.String)
            param(6).Value = txtL1State.Text
            param(7) = New SqlParameter("@LAN_CITY", DbType.String)
            param(7).Value = ddlCity.SelectedValue
            param(8) = New SqlParameter("@LAN_PINCODE", DbType.String)
            param(8).Value = txtld1Pin.Text
            param(9) = New SqlParameter("@LAN_PAN", DbType.String)
            param(9).Value = txtPAN.Text
            param(10) = New SqlParameter("@LAN_GST_TAX", DbType.String)
            param(10).Value = ddlServiceTaxApplicable.SelectedValue
            param(11) = New SqlParameter("@LAN_GST_NUM", DbType.String)
            param(11).Value = IIf(txtServiceTaxlnd.Text = "", DBNull.Value, txtServiceTaxlnd.Text)
            param(12) = New SqlParameter("@LAN_PROP_TAX_APP", DbType.String)
            param(12).Value = ddlPropertyTaxApplicable.SelectedValue
            param(13) = New SqlParameter("@LAN_PROP_TAX", DbType.Decimal)
            param(13).Value = IIf(txtPropertyTax.Text = "", DBNull.Value, txtPropertyTax.Text)
            param(14) = New SqlParameter("@LAN_CONTACT_DET", DbType.String)
            param(14).Value = txtContactDetails.Text
            param(15) = New SqlParameter("@LAN_AMOUNT_IN", DbType.String)
            param(15).Value = ddlAmountIn.SelectedValue
            param(16) = New SqlParameter("@LAN_RENT", DbType.Decimal)
            param(16).Value = IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text)
            param(17) = New SqlParameter("@LAN_SECURITY", DbType.Decimal)
            param(17).Value = IIf(txtpsecdep.Text = "", 0, txtpsecdep.Text)
            param(18) = New SqlParameter("@LAN_PAY_MODE", DbType.Int32)
            param(18).Value = ddlpaymentmode.SelectedValue
            param(19) = New SqlParameter("@CHEQUENO", DbType.String)
            param(19).Value = txtcheqno.Text

            param(20) = New SqlParameter("@RENT_TYPE", DbType.String)
            param(20).Value = ddlrenttype.SelectedValue
            param(21) = New SqlParameter("@LAN_MAINT", DbType.Decimal)
            param(21).Value = txtllmaint.Text
            param(22) = New SqlParameter("@LAN_AMENTS", DbType.Decimal)
            param(22).Value = txtllAments.Text

            If txtchedddt.Text <> "" Then
                param(23) = New SqlParameter("@CHEQUEDT", DbType.DateTime)
                param(23).Value = txtchedddt.Text
            Else
                param(23) = New SqlParameter("@CHEQUEDT", DbType.DateTime)
                param(23).Value = DBNull.Value
            End If

            If ddlpaymentmode.SelectedIndex > 0 Then
                If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
                    param(24) = New SqlParameter("@LAN_BANK", DbType.String)
                    param(24).Value = txtNeftBank.Text
                    param(25) = New SqlParameter("@LAN_ACCNO", DbType.String)
                    param(25).Value = txtAccNo.Text
                    param(26) = New SqlParameter("@LAN_NEFT_BRANCH", DbType.String)
                    param(26).Value = DBNull.Value
                    param(27) = New SqlParameter("@LAN_NEFT_IFSC", DbType.String)
                    param(27).Value = DBNull.Value
                ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                    param(24) = New SqlParameter("@LAN_BANK", DbType.String)
                    param(24).Value = txtNeftBank.Text
                    param(25) = New SqlParameter("@LAN_ACCNO", DbType.String)
                    param(25).Value = txtNeftAccNo.Text
                    param(26) = New SqlParameter("@LAN_NEFT_BRANCH", DbType.String)
                    param(26).Value = txtNeftBrnch.Text
                    param(27) = New SqlParameter("@LAN_NEFT_IFSC", DbType.String)
                    param(27).Value = txtNeftIFSC.Text
                Else
                    param(24) = New SqlParameter("@LAN_BANK", DbType.String)
                    param(24).Value = DBNull.Value
                    param(25) = New SqlParameter("@LAN_ACCNO", DbType.String)
                    param(25).Value = DBNull.Value
                    param(26) = New SqlParameter("@LAN_NEFT_BRANCH", DbType.String)
                    param(26).Value = DBNull.Value
                    param(27) = New SqlParameter("@LAN_NEFT_IFSC", DbType.String)
                    param(27).Value = DBNull.Value
                End If
            End If
            param(28) = New SqlParameter("@COMPANYID", DbType.Int32)
            param(28).Value = Session("COMPANYID")
            param(29) = New SqlParameter("@PM_LL_TDS", DbType.String)
            param(29).Value = ddltds.SelectedValue
            param(30) = New SqlParameter("@TDS_PERCENTAGE", DbType.String)
            param(30).Value = txttdsper.Text
            param(31) = New SqlParameter("@IMAGES", SqlDbType.Structured)
            param(31).Value = UtilityService.ConvertToDataTable(Imgclass)
            param(32) = New SqlParameter("@LAN_EMAIL", DbType.String)
            param(32).Value = txtldemail.Text
            param(33) = New SqlParameter("@AUR_ID", DbType.String)
            param(33).Value = Session("Uid")


            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_ADD_LANDLORD_DETAILS", param)
            If (rblEscalationType.SelectedValue = "Lease") Then
                Escalation()
            Else
                Landloardescalationdeatis()
            End If
            ''Landloardescalationdeatis()
            If res = "SUCCESS" Then
                ddlAmountIn.Enabled = False
                btnFinalize.Enabled = True
                GetLandlords()
                'btnBack.Enabled = True
                lblMsg.Text = "Landlord Details Added Successfully"
                ClearLandlord()
                ClearLandlordEscalation()
                'ClearTextBox(Me)
                'ClearDropdown(Me)
                'AssigTextValues()
                txtName.Focus()
                panel1.Visible = False
                panel2.Visible = False
            Else
                lblMsg.Text = "Something went wrong. Please try again later."
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub ClearLandlordEscalation()
        ddllandloardescalation.ClearSelection()
        ddllandloardescaltype.ClearSelection()
        'ddllandloardinterval.ClearSelection()
        ddllandloardescAmount.ClearSelection()
        txtnoescduration.Text = ""
        txtllescdurtn.Text = ""

    End Sub
    Public Sub ClearLandlord()
        txtName.Text = ""
        txtAddress.Text = ""
        txtAddress2.Text = ""
        txtAddress3.Text = ""
        txtL1State.Text = ""
        ddlCity.ClearSelection()
        txtld1Pin.Text = ""
        txtPAN.Text = ""
        ddlServiceTaxApplicable.ClearSelection()
        txtServiceTaxlnd.Text = "0"
        ddlPropertyTaxApplicable.ClearSelection()
        txtPropertyTax.Text = "0"
        txtContactDetails.Text = ""
        txtldemail.Text = ""
        'ddlAmountIn.ClearSelection()
        txtpmonthrent.Text = "0"
        txtpsecdep.Text = "0"
        ddlpaymentmode.ClearSelection()
        txtBankName.Text = ""
        txtAccNo.Text = ""
        txtNeftBank.Text = ""
        txtNeftAccNo.Text = ""
        txtNeftBrnch.Text = ""
        txtNeftIFSC.Text = ""
        txtcheqno.Text = ""
        txtchedddt.Text = ""
        ddlrenttype.ClearSelection()
        txtllmaint.Text = 0
        txtllAments.Text = 0
    End Sub

    'Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
    '    AddLeaseDetails.Visible = True
    '    Landlord.Visible = False
    '    ddlAmountIn.Enabled = True
    'End Sub
    Private Sub BindStatus()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PCL_GET_PROJECTTYPES")
        ddlstatus.DataSource = sp3.GetDataSet()
        ddlstatus.DataTextField = "PROJECTNAM"
        ddlstatus.DataValueField = "PROJECTCD"
        ddlstatus.DataBind()
        ddlstatus.SelectedIndex = 0
        ddlstatus.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub
    Protected Sub btnFinalize_Click(sender As Object, e As EventArgs) Handles btnFinalize.Click
        lblMsgLL.Text = ""
        lblMsg.Text = ""
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_RENT_DETAILS")
        sp1.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.Int32)
        sp1.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
        sp1.Command.AddParameter("@ID", Session("ID"), DbType.Int32)
        sp1.Command.AddParameter("@NOOFLANDORD", Convert.ToInt32(txtNoLanlords.Text), SqlDbType.Int)
        Dim ds As New DataSet
        ds = sp1.GetDataSet
        If ds.Tables(0).Rows(0).Item("LL_COUNT") <> ViewState("NO_OF_LL") Then
            lblMsgLL.Text = "Please add " + (ViewState("NO_OF_LL") - ds.Tables(0).Rows(0).Item("LL_COUNT")).ToString() + " more landlords"
            Exit Sub
        Else
            lblMsg.Text = "Lease Request Raised Successfully : " + Session("REQ_ID").ToString
            AddLeaseDetails.Visible = True
            Landlord.Visible = False
            RentRevisionPanel.Visible = False
            LandloardRentRevisionPanel.Visible = False
            ddlAmountIn.Enabled = True
        End If
        ClearTextBox(Me)
        ClearDropdown(Me)
        AssigTextValues()
    End Sub

    Protected Sub ddlDgSet_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDgSet.SelectedIndexChanged
        If ddlDgSet.SelectedValue = "Landlord" Then
            Dgset.Visible = True
        Else
            Dgset.Visible = False
        End If
    End Sub

    Protected Sub txtsdate_TextChanged(sender As Object, e As EventArgs) Handles txtsdate.TextChanged
        '' BindRentRevision()
        Dim Date3 As Date = txtsdate.Text
        txtrentcommencementdate.Text = Date3.AddDays(IIf(txtRentFreePeriod.Text = "", 0, txtRentFreePeriod.Text)).ToString("MM/dd/yyyy")
    End Sub

    Protected Sub rblCostType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblCostType.SelectedIndexChanged
        If rblCostType.SelectedValue = "Sqft" Then
            Costype1.Visible = True
            Costype2.Visible = False
            txtSeatCost.Text = ""
        ElseIf rblCostType.SelectedValue = "Seat" Then
            Costype1.Visible = False
            Costype2.Visible = True
            txtRentPerSqftCarpet.Text = ""
            txtRentPerSqftBUA.Text = ""
        End If
    End Sub

    Private Sub GetLandlords()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LANDLORDS")
        sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.Int32)
        sp.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
        sp.Command.AddParameter("@ID", Session("ID"), DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count = 1 Then
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SEND_MAIL_ADD_LEASE")
            sp1.Command.AddParameter("@REQ_BY", Session("Uid").ToString, DbType.String)
            sp1.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
            sp1.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.Int32)
            sp1.ExecuteScalar()
        End If

        If ds.Tables(0).Rows.Count > 0 Then
            TotalLandlords.Visible = True
            gvLandlords.DataSource = ds
            gvLandlords.DataBind()
        Else
            TotalLandlords.Visible = False
        End If
    End Sub


    Protected Sub txtRentFreePeriod_TextChanged(sender As Object, e As EventArgs) Handles txtRentFreePeriod.TextChanged
        Dim days As Integer
        Dim Date1 As Date = txtsdate.Text
        Dim Date2 As Date = DateTime.Now.ToString("MM/dd/yyyy")
        days = DateDiff("d", Date1, Date2)

        If txtRentFreePeriod.Text <> "" Then
            txtrentcommencementdate.Text = DateTime.Now.AddDays(days + txtRentFreePeriod.Text).ToString("MM/dd/yyyy")
        Else
            txtrentcommencementdate.Text = DateTime.Now.ToString("MM/dd/yyyy")
        End If

    End Sub

    Protected Sub txtrentcommencementdate_TextChanged(sender As Object, e As EventArgs) Handles txtrentcommencementdate.TextChanged
        Dim days As Integer

        Dim Date1 As Date = txtsdate.Text
        Dim Date2 As Date = txtrentcommencementdate.Text
        days = DateDiff("d", Date1, Date2)
        txtRentFreePeriod.Text = days

    End Sub
    Private Sub rblEscalationType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblEscalationType.SelectedIndexChanged
        If rblEscalationType.SelectedValue = "Lease" Then
            EscalationType1.Visible = True
            EscalationType2.Visible = True
            txtIntervaldu.Text = ""
            txtnofEscltions.Text = ""
        ElseIf rblEscalationType.SelectedValue = "LandLoard" Then
            EscalationType1.Visible = False
            EscalationType2.Visible = False
            RentRevisionPanel.Visible = False
        End If
    End Sub

    'Private Sub ddlintervaltype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlintervaltype.SelectedIndexChanged
    '    RentRevisionPanel.Visible = True
    'End Sub

    Private Sub ddlesc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlesc.SelectedIndexChanged
        If ddlesc.SelectedValue = "Yes" Then
            EscalationType.Visible = True
        Else ddlesc.SelectedValue = "No"
            EscalationType.Visible = False
            EscalationType1.Visible = False
            EscalationType2.Visible = False
        End If
    End Sub

    Private Sub ddllandloardescalation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddllandloardescalation.SelectedIndexChanged
        If ddllandloardescalation.SelectedValue = "Yes" Then
            Landloardescalation.Visible = True
            Landloardescalation1.Visible = True
        Else ddllandloardescalation.SelectedValue = "No"
            Landloardescalation.Visible = False
            Landloardescalation1.Visible = False
            LandloardRentRevisionPanel.Visible = False
            txtllescdurtn.Text = " "
            txtnoescduration.Text = ""
        End If
    End Sub
    Private Sub ddllandloardinterval_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddllandloardinterval.SelectedIndexChanged
        If ddllandloardinterval.SelectedValue = "Flexy" Then
            Landloardescalation1.Visible = True
            LLescduration.Visible = False
            LandloardRentRevisionPanel.Visible = False
        Else
            Landloardescalation1.Visible = False
            LLescduration.Visible = True
            LandloardRentRevisionPanel.Visible = False
        End If
    End Sub
    'Private Sub txtescduration_TextChanged(sender As Object, e As EventArgs) Handles txtescduration.TextChanged
    '    LandloardBindRentRevision()
    'End Sub
    Public Sub txtllescdurtn_TextChanged(sender As Object, e As EventArgs)
        LandloardBindRentRevision()
        For Each item As RepeaterItem In Repeater1.Items
            If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                'Dim lbl = CType(item.FindControl("lblEscno"), Label)
                Dim lbtxt = CType(item.FindControl("ddlYear"), TextBox)
                Dim lbmnth = CType(item.FindControl("ddlMonth"), TextBox)
                Dim lbday = CType(item.FindControl("ddlDay"), TextBox)
                Dim lbesc = CType(item.FindControl("TextBox1"), TextBox)
                Dim lbyr = CType(item.FindControl("lbyear"), Label)
                Dim lbmth = CType(item.FindControl("lbmnt"), Label)
                Dim lbdy = CType(item.FindControl("lbldy"), Label)
                Dim lble = CType(item.FindControl("lbldf"), Label)
                'lbl.Visible = False
                lbtxt.Visible = False
                lbmnth.Visible = False
                lbesc.Visible = False
                lbday.Visible = False
                lbyr.Visible = False
                lbmth.Visible = False
                lbdy.Visible = False
                lble.Visible = False
            End If
        Next
    End Sub
    Public Sub txtnoescduration_TextChanged(sender As Object, e As EventArgs)
        LandloardBindRentRevisionflex()
        For Each item As RepeaterItem In Repeater1.Items
            If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                Dim lbl = CType(item.FindControl("lblRevYear"), Label)
                Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
                lbl.Visible = False
                lbtxt.Visible = False
            End If
        Next
    End Sub
    Public Sub LandloardBindRentRevisionflex()
        LandloardRentRevisionPanel.Visible = True
        lblmesg.Text = ""
        If txtnoescduration.Text <> "" Then
            Dim RR_lst As New List(Of RentRevision)()
            Dim rr_obj As New RentRevision()
            Dim SubEscDATE As DateTime = txtsdate.Text
            For i As Integer = 1 To Convert.ToInt32(txtnoescduration.Text)
                rr_obj = New RentRevision()
                d = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtnoescduration.Text))
                rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(i) + ":"
                rr_obj.RR_Percentage = "0"
                rr_obj.pm_les_esc_id = ""
                'i = i + 1
                'rr_obj.
                For Each item As RepeaterItem In Repeater1.Items
                    If (item.ItemIndex = i - 1) Then
                        If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                            Dim tect = CType(item.FindControl("TextBox1"), TextBox)
                            ' If (tect.Text <> 0) Then
                            'Dim tectr = CType(item.FindControl("TextBox1"), TextBox).Text
                            Dim lbl = CType(item.FindControl("lblRevYear"), Label)
                            Dim lblYear = IIf(CType(item.FindControl("ddlYear"), TextBox).Text = "", 0, CType(item.FindControl("ddlYear"), TextBox).Text)
                            Dim lblMonth = IIf(CType(item.FindControl("ddlMonth"), TextBox).Text = "", 0, CType(item.FindControl("ddlMonth"), TextBox).Text)
                            Dim lblDay = IIf(CType(item.FindControl("ddlDay"), TextBox).Text = "", 0, CType(item.FindControl("ddlDay"), TextBox).Text)
                            SubEscDATE = Convert.ToDateTime(SubEscDATE).AddYears(Convert.ToInt32(lblYear))
                            SubEscDATE = Convert.ToDateTime(SubEscDATE).AddMonths(Convert.ToInt32(lblMonth))
                            SubEscDATE = Convert.ToDateTime(SubEscDATE).AddDays(Convert.ToInt32(lblDay))
                            rr_obj.RR_Year = SubEscDATE
                            rr_obj.RR_YEARS = lblYear
                            rr_obj.RR_MONTHS = lblMonth
                            rr_obj.RR_DAYS = lblDay
                            rr_obj.RR_Percentage = tect.Text
                            lbl.Visible = True
                            'Else
                            '    rr_obj.RR_Year = ""
                            'End If
                        End If
                    End If
                Next
                RR_lst.Add(rr_obj)
            Next
            Repeater1.DataSource = RR_lst
            Repeater1.DataBind()
            Session("escdt") = SubEscDATE
            If (SubEscDATE >= Convert.ToDateTime(txtedate.Text)) Then
                lblmesg.Text = "Escalation date should not be greater than Lease Expiry Date"
            Else
                lblmesg.Text = ""
            End If
            LandloardRentRevisionPanel.Visible = True
        End If
    End Sub
    Public Sub LandloardBindRentRevision()

        If txtllescdurtn.Text <> "" Then
            If ddllandloardinterval.SelectedValue = "Yearly" Then
                rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
                txtInterval = txtsdate.Text
                Dim j As Integer = 1
                Duration = rowCount / Convert.ToInt32(txtllescdurtn.Text)
                If Duration >= 1 Then
                    LandloardRentRevisionPanel.Visible = True
                    Dim RR_lst As New List(Of RentRevision)()
                    Dim rr_obj As New RentRevision()
                    For i As Integer = Convert.ToInt32(txtllescdurtn.Text) To rowCount
                        rr_obj = New RentRevision()
                        d = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtllescdurtn.Text))
                        'rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
                        rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(j) + ":"
                        rr_obj.RR_Year = d
                        txtInterval = d
                        rr_obj.RR_Percentage = "0"
                        i = i + (Convert.ToInt32(txtllescdurtn.Text) - 1)
                        RR_lst.Add(rr_obj)
                        j = j + 1
                    Next
                    Repeater1.DataSource = RR_lst
                    Repeater1.DataBind()
                Else
                    LandloardRentRevisionPanel.Visible = False
                End If
            Else ddllandloardinterval.SelectedValue = "Monthly"
                Dim rowCount As Integer = 0
                Dim months1 As Integer = (DateTime.Parse(txtedate.Text).Year * 12 + DateTime.Parse(txtedate.Text).Month) - (DateTime.Parse(txtsdate.Text).Year * 12 + DateTime.Parse(txtsdate.Text).Month)

                txtInterval = txtsdate.Text
                Dim Duration As Integer = 0
                Dim j As Integer = 1
                Duration = months1 / Convert.ToInt32(txtllescdurtn.Text)
                If Duration > 1 Then
                    LandloardRentRevisionPanel.Visible = True
                    Dim RR_lst As New List(Of RentRevision)()
                    Dim rr_obj As New RentRevision()
                    ''For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To months1 - Convert.ToInt32(txtIntervaldu.Text)
                    For i As Integer = Convert.ToInt32(txtllescdurtn.Text) To months1
                        rr_obj = New RentRevision()
                        d = Convert.ToDateTime(txtInterval).AddMonths(Convert.ToInt32(txtllescdurtn.Text))
                        rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(j) + ":"
                        rr_obj.RR_Year = d
                        txtInterval = d
                        rr_obj.RR_Percentage = "0"
                        i = i + (Convert.ToInt32(txtllescdurtn.Text) - 1)
                        RR_lst.Add(rr_obj)
                        j = j + 1
                    Next
                    Repeater1.DataSource = RR_lst
                    Repeater1.DataBind()
                Else
                    LandloardRentRevisionPanel.Visible = False
                End If
            End If
        End If
    End Sub
    Private Sub ddllandloardescaltype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddllandloardescaltype.SelectedIndexChanged
        LandloardRentRevisionPanel.Visible = True
    End Sub
    Public Sub Escalation()
        Dim Basicrent As Int32 = txtInvestedArea.Text
        Dim LandBasicrent As Int32 = txtpmonthrent.Text

        Dim startdate As DateTime = txtsdate.Text
        Dim S = startdate.Month
        Dim days = startdate.Day
        Dim h = startdate.DayOfWeek
        Dim CurrentMonthDays As Int16 = DateTime.DaysInMonth(startdate.Year, startdate.Month)

        Dim SDATE As DateTime = txtsdate.Text
        Dim EDATE As DateTime
        Dim T1 As Decimal
        Dim T2 As Decimal
        Dim T3 As Decimal
        Dim T4 As Decimal
        Dim T5 As Decimal
        Dim prorata As Decimal
        Dim T7 As Decimal
        Dim escaltedrent As Decimal

        Dim L1 As Decimal
        Dim L2 As Decimal
        Dim L3 As Decimal
        Dim L4 As Decimal
        Dim L5 As Decimal
        Dim Landloardprorata As Decimal
        Dim L7 As Decimal
        Dim Landloardescaltedrent As Decimal

        Dim i As Integer
        i = 0
        Dim current As Integer
        current = 0
        Dim Landloardcurrent As Integer
        Landloardcurrent = 0

        Dim Revision As String
        Dim lblYear As Integer
        Dim lblDay As Integer
        Dim lblMonth As Integer
        Dim PM_LL_ID As String
        Dim durationesc As Integer
        For Each item As RepeaterItem In Repeater2.Items
            'Dim lblName As String = CType(item.FindControl("lblRevYear"), Label).Text
            If (ddlintervaltype.SelectedValue = "Flexy") Then
                lblYear = IIf(CType(item.FindControl("ddlYear"), TextBox).Text = "", 0, CType(item.FindControl("ddlYear"), TextBox).Text)
                lblMonth = IIf(CType(item.FindControl("ddlMonth"), TextBox).Text = "", 0, CType(item.FindControl("ddlMonth"), TextBox).Text)
                lblDay = IIf(CType(item.FindControl("ddlDay"), TextBox).Text = "", 0, CType(item.FindControl("ddlDay"), TextBox).Text)
                Revision = CType(item.FindControl("TextBox1"), TextBox).Text
                PM_LL_ID = CType(item.FindControl("HDN_PM_LL_ID"), HiddenField).Value
                SDATE = Convert.ToDateTime(SDATE).AddYears(Convert.ToInt32(lblYear))
                SDATE = Convert.ToDateTime(SDATE).AddMonths(Convert.ToInt32(lblMonth))
                SDATE = Convert.ToDateTime(SDATE).AddDays(Convert.ToInt32(lblDay))
                Dim lblName As DateTime = startdate
                durationesc = Convert.ToInt32(txtnofEscltions.Text)
                EDATE = Convert.ToDateTime(SDATE).AddDays(-1)
            Else
                SDATE = CType(item.FindControl("lblRevYear"), Label).Text
                Revision = CType(item.FindControl("txtRevision"), TextBox).Text
                PM_LL_ID = CType(item.FindControl("HDN_PM_LL_ID"), HiddenField).Value
                durationesc = Convert.ToInt32(txtIntervaldu.Text)
                EDATE = Convert.ToDateTime(SDATE).AddDays(-1)
            End If
            If (Revision <> Nothing) Then
                Dim RevisionValue As Integer = Revision
                Dim totalRent As Integer = txtInvestedArea.Text
                Dim a As String = lblYear

                Dim Tenure As Integer = ddlTenure.SelectedItem.Value
                Dim Rent As Integer = 0
                rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
                Duration = rowCount / durationesc 'Convert.ToInt32(txtIntervaldu.Text)
                Dim LLescamount As Decimal = RevisionValue / Convert.ToInt32(txtNoLanlords.Text)

                If ddlamount.SelectedItem.Value = "Value" Then
                    If (i = 0) Then

                        T1 = Basicrent / CurrentMonthDays
                        T2 = CurrentMonthDays - days + 1
                        T3 = T1 * T2
                        T4 = RevisionValue + T3
                        T5 = CurrentMonthDays - T2
                        T7 = T5 * T1
                        prorata = T7 + T4
                        escaltedrent = Basicrent + (RevisionValue)
                        FinalRent = escaltedrent

                        L1 = LandBasicrent / CurrentMonthDays
                        L2 = CurrentMonthDays - days + 1
                        L3 = L1 * L2
                        L4 = LLescamount + L3
                        L5 = CurrentMonthDays - L2
                        L7 = L5 * L1
                        Landloardprorata = L7 + L4
                        Landloardescaltedrent = LandBasicrent + (LLescamount)
                        LandloardFinalRent = Landloardescaltedrent
                    End If
                    i = i + 1
                    If (i > 1) Then
                        FinalRent = current + (RevisionValue)
                        T1 = current / CurrentMonthDays
                        T2 = CurrentMonthDays - days + 1
                        T3 = T1 * T2
                        T4 = RevisionValue + T3
                        T5 = CurrentMonthDays - T2
                        T7 = T5 * T1
                        prorata = T7 + T4

                        ''LandloardFinalRent = Landloardcurrent + Landloardescaltedrent
                        LandloardFinalRent = LLescamount + Landloardcurrent
                        L1 = Landloardcurrent / CurrentMonthDays
                        L2 = CurrentMonthDays - days + 1
                        L3 = L1 * L2
                        L4 = LLescamount + L3
                        L5 = CurrentMonthDays - L2
                        L7 = L5 * L1
                        Landloardprorata = L7 + L4

                    End If
                    current = FinalRent
                    Landloardcurrent = LandloardFinalRent

                Else ddlamount.SelectedItem.Value = "Percentage"
                    If (i = 0) Then
                        T1 = Basicrent / CurrentMonthDays
                        T2 = CurrentMonthDays - days + 1
                        T3 = T1 * T2
                        T4 = T3 * (RevisionValue / 100) + T3
                        T5 = CurrentMonthDays - T2
                        T7 = T5 * T1
                        prorata = T7 + T4
                        escaltedrent = Basicrent * (RevisionValue / 100) + Basicrent
                        FinalRent = escaltedrent

                        L1 = LandBasicrent / CurrentMonthDays
                        L2 = CurrentMonthDays - days + 1
                        L3 = L1 * L2
                        L4 = L3 * (RevisionValue / 100) + L3
                        L5 = CurrentMonthDays - L2
                        L7 = L5 * L1
                        Landloardprorata = L7 + L4
                        Landloardescaltedrent = LandBasicrent * (RevisionValue / 100) + LandBasicrent
                        LandloardFinalRent = Landloardescaltedrent
                    End If
                    i = i + 1
                    If (i > 1) Then
                        FinalRent = current * (RevisionValue / 100) + current
                        T1 = current / CurrentMonthDays
                        T2 = CurrentMonthDays - days + 1
                        T3 = T1 * T2
                        T4 = T3 * (RevisionValue / 100) + T3
                        T5 = CurrentMonthDays - T2
                        T7 = T5 * T1
                        prorata = T7 + T4

                        LandloardFinalRent = Landloardcurrent * (RevisionValue / 100) + Landloardcurrent
                        L1 = Landloardcurrent / CurrentMonthDays
                        L2 = CurrentMonthDays - days + 1
                        L3 = L1 * L2
                        L4 = L3 * (RevisionValue / 100) + L3
                        L5 = CurrentMonthDays - L2
                        L7 = L5 * L1
                        Landloardprorata = L7 + L4
                    End If
                    current = FinalRent
                    Landloardcurrent = LandloardFinalRent

                End If
            End If
            Dim params(17) As SqlParameter
            'Area &Cost
            params(0) = New SqlParameter("@PM_LES_ESC_PLR_REQ_ID", SqlDbType.VarChar)
            params(0).Value = lblLeaseReqId.Text
            params(1) = New SqlParameter("@PM_LES_ESC", SqlDbType.VarChar)
            params(1).Value = ddlesc.SelectedItem.Value
            params(2) = New SqlParameter("@PM_LES_ESC_ON", SqlDbType.VarChar)
            params(2).Value = rblEscalationType.SelectedItem.Value
            params(3) = New SqlParameter("@PM_LES_ESC_TYPE", SqlDbType.VarChar)
            params(3).Value = ddlEscalation.SelectedItem.Value
            params(4) = New SqlParameter("@PM_LES_ESC_INT_TYPE", SqlDbType.VarChar)
            params(4).Value = ddlintervaltype.SelectedItem.Value
            params(5) = New SqlParameter("@PM_LES_ESC_AMOUNTIN", SqlDbType.VarChar)
            params(5).Value = ddlamount.SelectedItem.Value
            params(6) = New SqlParameter("@PM_LES_ESC_INT_DUR", SqlDbType.Int)
            params(6).Value = durationesc
            params(7) = New SqlParameter("@PM_LES_ESC_INT_DATE", SqlDbType.DateTime)
            params(7).Value = SDATE

            If ddlamount.SelectedItem.Value = "Value" Then
                params(8) = New SqlParameter("@PM_LES_ESC_AMTVAL", SqlDbType.Float)
                params(8).Value = Revision
                params(9) = New SqlParameter("@PM_LES_ESC_AMTPER", SqlDbType.Float)
                params(9).Value = "0"

            Else
                params(8) = New SqlParameter("@PM_LES_ESC_AMTVAL", SqlDbType.Float)
                params(8).Value = "0"
                params(9) = New SqlParameter("@PM_LES_ESC_AMTPER", SqlDbType.Float)
                params(9).Value = Revision
                'params(10) = New SqlParameter("@PM_LES_ESC_AMOUNT", SqlDbType.Float)
                'params(10).Value = FinalRent
            End If
            params(10) = New SqlParameter("@PM_LES_ESC_AMOUNT", SqlDbType.Float)
            params(10).Value = FinalRent
            params(11) = New SqlParameter("@PM_LES_ESC_PRORATA", SqlDbType.Float)
            params(11).Value = prorata
            params(12) = New SqlParameter("@PM_LES_LL_ESC_RENT", SqlDbType.Float)
            params(12).Value = LandloardFinalRent
            params(13) = New SqlParameter("@PM_ESC_LL_PRORATA", SqlDbType.Float)
            params(13).Value = Landloardprorata
            params(14) = New SqlParameter("@PM_ESC_AGREEMENT_SDATE", SqlDbType.DateTime)
            params(14).Value = startdate
            params(15) = New SqlParameter("@PM_LES_ESC_MONTHS", SqlDbType.Int)
            params(15).Value = lblMonth
            params(16) = New SqlParameter("@PM_LES_ESC_YEARS", SqlDbType.Int)
            params(16).Value = lblYear
            params(17) = New SqlParameter("@PM_LES_ESC_DAYS", SqlDbType.Int)
            params(17).Value = lblDay
            'params(18) = New SqlParameter("@PM_LES_ESC_EDT", SqlDbType.DateTime)
            'params(18).Value = EDATE
            SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SP_LEASE_ESCALATIONDATA", params)

        Next
    End Sub
    Public Sub Landloardescalationdeatis()

        Dim Basicrent As Int32 = txtInvestedArea.Text
        ''Dim LeaseAmount As Int32 = txttotalrent.Text
        Dim LandBasicrent As Int32 = txtpmonthrent.Text

        Dim startdate As DateTime = txtsdate.Text
        Dim S = startdate.Month
        Dim days = startdate.Day
        Dim h = startdate.DayOfWeek
        Dim CurrentMonthDays As Int16 = DateTime.DaysInMonth(startdate.Year, startdate.Month)

        Dim SDATE As DateTime = txtsdate.Text
        Dim EDATE As DateTime
        Dim T1 As Decimal
        Dim T2 As Decimal
        Dim T3 As Decimal
        Dim T4 As Decimal
        Dim T5 As Decimal
        Dim prorata As Decimal
        Dim T7 As Decimal
        Dim escaltedrent As Decimal

        Dim L1 As Decimal
        Dim L2 As Decimal
        Dim L3 As Decimal
        Dim L4 As Decimal
        Dim L5 As Decimal
        Dim Landloardprorata As Decimal
        Dim L7 As Decimal
        Dim Landloardescaltedrent As Decimal

        Dim i As Integer
        i = 0
        Dim current As Integer
        current = 0
        Dim Landloardcurrent As Integer
        Landloardcurrent = 0
        Dim Revision As String
        Dim lblYear As Integer
        Dim lblDay As Integer
        Dim lblMonth As Integer
        Dim PM_LL_ID As String
        Dim durationesc As Integer
        For Each item As RepeaterItem In Repeater1.Items
            'Dim lblName As String = CType(item.FindControl("lblRevYear"), Label).Text
            'Dim Revision As String = CType(item.FindControl("txtRevision"), TextBox).Text
            If (ddllandloardinterval.SelectedValue = "Flexy") Then
                lblYear = IIf(CType(item.FindControl("ddlYear"), TextBox).Text = "", 0, CType(item.FindControl("ddlYear"), TextBox).Text)
                lblMonth = IIf(CType(item.FindControl("ddlMonth"), TextBox).Text = "", 0, CType(item.FindControl("ddlMonth"), TextBox).Text)
                lblDay = IIf(CType(item.FindControl("ddlDay"), TextBox).Text = "", 0, CType(item.FindControl("ddlDay"), TextBox).Text)
                Revision = CType(item.FindControl("TextBox1"), TextBox).Text
                PM_LL_ID = CType(item.FindControl("HDN_PM_LL_ID"), HiddenField).Value
                SDATE = Convert.ToDateTime(SDATE).AddYears(Convert.ToInt32(lblYear))
                SDATE = Convert.ToDateTime(SDATE).AddMonths(Convert.ToInt32(lblMonth))
                SDATE = Convert.ToDateTime(SDATE).AddDays(Convert.ToInt32(lblDay))
                Dim lblName As DateTime = startdate
                durationesc = Convert.ToInt32(txtnoescduration.Text)
                EDATE = Convert.ToDateTime(SDATE).AddDays(-1)
            Else
                SDATE = CType(item.FindControl("lblRevYear"), Label).Text
                EDATE = Convert.ToDateTime(SDATE).AddDays(-1)
                Revision = CType(item.FindControl("txtRevision"), TextBox).Text
                PM_LL_ID = CType(item.FindControl("HDN_PM_LL_ID"), HiddenField).Value
                durationesc = Convert.ToInt32(txtllescdurtn.Text)
            End If
            If (Revision <> Nothing) Then
                Dim RevisionValue As Integer = Revision
                Dim totalRent As Integer = txtInvestedArea.Text
                Dim a As String = lblYear

                Dim Tenure As Integer = ddlTenure.SelectedItem.Value
                Dim Rent As Integer = 0
                rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
                Duration = rowCount / Convert.ToInt32(txtllescdurtn.Text)
                Dim LLescamount As Decimal = RevisionValue / Convert.ToInt32(txtNoLanlords.Text)
                If ddllandloardescAmount.SelectedItem.Value = "Value" Then
                    If (i = 0) Then
                        T1 = Basicrent / CurrentMonthDays
                        T2 = CurrentMonthDays - days + 1
                        T3 = T1 * T2
                        T4 = RevisionValue + T3
                        T5 = CurrentMonthDays - T2
                        T7 = T5 * T1
                        prorata = T7 + T4
                        escaltedrent = Basicrent + (RevisionValue)
                        FinalRent = escaltedrent

                        L1 = LLescamount / CurrentMonthDays
                        L2 = CurrentMonthDays - days + 1
                        L3 = L1 * L2
                        L4 = LandBasicrent + L3
                        'L5 = CurrentMonthDays - L2
                        'L7 = L5 * L1
                        Landloardprorata = L4
                        Landloardescaltedrent = LandBasicrent + (LLescamount)
                        LandloardFinalRent = Landloardescaltedrent
                        'L1 = LandBasicrent / CurrentMonthDays
                        'L2 = CurrentMonthDays - days + 1
                        'L3 = L1 * L2
                        'L4 = RevisionValue + L3
                        'L5 = CurrentMonthDays - L2
                        'L7 = L5 * L1
                        'Landloardprorata = L7 + L4
                        'Landloardescaltedrent = LandBasicrent + (RevisionValue)
                        'LandloardFinalRent = Landloardescaltedrent
                    End If
                    i = i + 1
                    If (i > 1) Then
                        FinalRent = current + (RevisionValue)
                        T1 = current / CurrentMonthDays
                        T2 = CurrentMonthDays - days + 1
                        T3 = T1 * T2
                        T4 = RevisionValue + T3
                        T5 = CurrentMonthDays - T2
                        T7 = T5 * T1
                        prorata = T7 + T4

                        LandloardFinalRent = Landloardcurrent + LLescamount ''+ Landloardescaltedrent
                        L1 = LLescamount / CurrentMonthDays
                        L2 = CurrentMonthDays - days + 1
                        L3 = L1 * L2
                        L4 = Landloardcurrent + L3
                        'L5 = CurrentMonthDays - L2
                        'L7 = L5 * L1
                        Landloardprorata = L4

                        'LandloardFinalRent = Landloardcurrent + Landloardescaltedrent
                        'L1 = LandBasicrent / CurrentMonthDays
                        'L2 = CurrentMonthDays - days + 1
                        'L3 = L1 * L2
                        'L4 = RevisionValue + L3
                        'L5 = CurrentMonthDays - L2
                        'L7 = L5 * L1
                        'Landloardprorata = L7 + L4

                    End If
                    current = FinalRent
                    Landloardcurrent = LandloardFinalRent

                Else ddllandloardescAmount.SelectedItem.Value = "Percentage"
                    If (i = 0) Then
                        T1 = Basicrent / CurrentMonthDays
                        T2 = CurrentMonthDays - days + 1
                        T3 = T1 * T2
                        T4 = T3 * (RevisionValue / 100) + T3
                        T5 = CurrentMonthDays - T2
                        T7 = T5 * T1
                        prorata = T7 + T4
                        escaltedrent = Basicrent * (RevisionValue / 100) + Basicrent
                        FinalRent = escaltedrent

                        L1 = LandBasicrent / CurrentMonthDays
                        L2 = CurrentMonthDays - days + 1
                        L3 = L1 * L2
                        L4 = L3 * (RevisionValue / 100) + L3
                        L5 = CurrentMonthDays - L2
                        L7 = L5 * L1
                        Landloardprorata = L7 + L4
                        Landloardescaltedrent = LandBasicrent * (RevisionValue / 100) + LandBasicrent
                        LandloardFinalRent = Landloardescaltedrent
                    End If
                    i = i + 1
                    If (i > 1) Then
                        FinalRent = current * (RevisionValue / 100) + current
                        'FinalRent = current  * (RevisionValue / 100) + escaltedrent
                        T1 = current / CurrentMonthDays
                        T2 = CurrentMonthDays - days + 1
                        T3 = T1 * T2
                        T4 = T3 * (RevisionValue / 100) + T3
                        T5 = CurrentMonthDays - T2
                        T7 = T5 * T1
                        prorata = T7 + T4


                        LandloardFinalRent = Landloardcurrent * (RevisionValue / 100) + Landloardcurrent
                        L1 = Landloardcurrent / CurrentMonthDays
                        L2 = CurrentMonthDays - days + 1
                        L3 = L1 * L2
                        L4 = L3 * (RevisionValue / 100) + L3
                        L5 = CurrentMonthDays - L2
                        L7 = L5 * L1
                        Landloardprorata = L7 + L4

                    End If
                    current = FinalRent
                    Landloardcurrent = LandloardFinalRent

                End If
            End If
            Dim params(17) As SqlParameter
            'Area &Cost
            params(0) = New SqlParameter("@PM_LES_ESC_PLR_REQ_ID", SqlDbType.VarChar)
            params(0).Value = lblLeaseReqId.Text
            params(1) = New SqlParameter("@PM_LES_ESC", SqlDbType.VarChar)
            params(1).Value = ddllandloardescalation.SelectedItem.Value
            params(2) = New SqlParameter("@PM_LES_ESC_ON", SqlDbType.VarChar)
            params(2).Value = rblEscalationType.SelectedItem.Value
            params(3) = New SqlParameter("@PM_LES_ESC_TYPE", SqlDbType.VarChar)
            params(3).Value = ddllandloardescaltype.SelectedItem.Value
            params(4) = New SqlParameter("@PM_LES_ESC_INT_TYPE", SqlDbType.VarChar)
            params(4).Value = ddllandloardinterval.SelectedValue
            params(5) = New SqlParameter("@PM_LES_ESC_AMOUNTIN", SqlDbType.VarChar)
            params(5).Value = ddllandloardescAmount.SelectedItem.Value
            params(6) = New SqlParameter("@PM_LES_ESC_INT_DUR", SqlDbType.Int)
            params(6).Value = durationesc
            params(7) = New SqlParameter("@PM_LES_ESC_INT_DATE", SqlDbType.DateTime)
            params(7).Value = SDATE

            If ddllandloardescAmount.SelectedItem.Value = "Value" Then
                params(8) = New SqlParameter("@PM_LES_ESC_AMTVAL", SqlDbType.Float)
                params(8).Value = Revision
                params(9) = New SqlParameter("@PM_LES_ESC_AMTPER", SqlDbType.Float)
                params(9).Value = "0"

            Else
                params(8) = New SqlParameter("@PM_LES_ESC_AMTVAL", SqlDbType.Float)
                params(8).Value = "0"
                params(9) = New SqlParameter("@PM_LES_ESC_AMTPER", SqlDbType.Float)
                params(9).Value = Revision
                'params(10) = New SqlParameter("@PM_LES_ESC_AMOUNT", SqlDbType.Float)
                'params(10).Value = FinalRent
            End If
            params(10) = New SqlParameter("@PM_LES_ESC_AMOUNT", SqlDbType.Float)
            params(10).Value = Basicrent
            params(11) = New SqlParameter("@PM_LES_ESC_PRORATA", SqlDbType.Float)
            params(11).Value = prorata
            params(12) = New SqlParameter("@PM_LES_LL_ESC_RENT", SqlDbType.Float)
            params(12).Value = LandloardFinalRent
            params(13) = New SqlParameter("@PM_ESC_LL_PRORATA", SqlDbType.Float)
            params(13).Value = Landloardprorata
            params(14) = New SqlParameter("@PM_ESC_AGREEMENT_SDATE", SqlDbType.DateTime)
            params(14).Value = startdate
            params(15) = New SqlParameter("@PM_LES_ESC_MONTHS", SqlDbType.Int)
            params(15).Value = lblMonth
            params(16) = New SqlParameter("@PM_LES_ESC_YEARS", SqlDbType.Int)
            params(16).Value = lblYear
            params(17) = New SqlParameter("@PM_LES_ESC_DAYS", SqlDbType.Int)
            params(17).Value = lblDay
            'params(18) = New SqlParameter("@PM_LES_ESC_EDT", SqlDbType.DateTime)
            'params(18).Value = EDATE
            SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SP_LEASE_ESCALATIONDATA", params)

        Next
    End Sub
    Protected Sub ddltds_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddltds.SelectedIndexChanged

        '' TDSDIV.Visible = IIf(ddltds.SelectedValue = "1", True, False)
        If ddltds.SelectedValue = "1" Then
            TDSDIV.Visible = True
        Else
            TDSDIV.Visible = False
            txttdsper.Text = 0
        End If
    End Sub
End Class
