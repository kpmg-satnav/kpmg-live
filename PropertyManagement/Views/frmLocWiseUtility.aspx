﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }
    </style>

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="LocUtilityController" class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Add Utility Expenses" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Add Utility Expenses</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form role="form" id="form1" name="frmLocationUtility" data-valid-submit="SubmitData()" novalidate>
                        <div class="row">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-4 col-xs-6">
                                    <div class="form-group" onmouseover="Tip('choose file to upload')" onmouseout="UnTip()">
                                        <label>Upload File :</label>
                                        <input type="file" name="ExUpload" id="File3" required="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />

                                        <%--    <a data-ng-click="ExcelDownload()">Click Here To Download Template</a>--%>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-6">

                                    <input type="button" value="Upload Excel" class='btn btn-primary custom-button-color' id="btnUplaodExcel" data-ng-click="UploadFile()" />
                                    <input type="button" value="download Excel" class='btn btn-primary custom-button-color' id="btnDownload" data-ng-click="ExcelDownload()" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Location<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.LCM_CODE.$invalid}">
                                            <select id="LCM_CODE" name="LCM_CODE" data-ng-model="SaveExpUtility.LCM_CODE" ng-change="GetUtilityDetails()" class="selectpicker" data-live-search="true" required="required">
                                                <option value="">--Select--</option>
                                                <option data-ng-repeat="Locutil in LocUtilityList" value="{{Locutil.LCM_CODE}}">{{Locutil.LCM_NAME}}</option>
                                            </select>
                                            <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.LCM_CODE.$invalid" style="color: red">Please select location name </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Expense Head<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.EXP_CODE.$invalid}">
                                            <select id="EXP_CODE" name="EXP_CODE" data-ng-model="SaveExpUtility.EXP_CODE" ng-change="GetUtilityDetails()" class="selectpicker" data-live-search="true" required="required">
                                                <option value="">--Select--</option>
                                                <option data-ng-repeat="Exputil in ExpenseUtility" value="{{Exputil.EXP_CODE}}">{{Exputil.EXP_NAME}}</option>
                                            </select>
                                            <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.EXP_CODE.$invalid" style="color: red">Please select expenses head name </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Bill Date<span style="color: red;">*</span></label>
                                        <div class="input-group date" id='FromDate'>
                                            <input type="text" class="form-control" data-ng-model="SaveExpUtility.FromDate" id="Text2" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('FromDate')"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.FromDate.$invalid" style="color: red;">Please select Bill Date</span>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Bill No.<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.BILL_NO.$invalid}">
                                            <input id="BILL_NO" type="text" name="BILL_NO" maxlength="15" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" data-ng-model="SaveExpUtility.BILL_NO" autofocus class="form-control" required="required" />&nbsp;
                                            <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.BILL_NO.$invalid" style="color: red">Please enter bill No. </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Invoice/PO No.<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.BILL_INVOICE.$invalid}">
                                            <input id="BILL_INVOICE" type="text" name="BILL_INVOICE" maxlength="50" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="SaveExpUtility.BILL_INVOICE" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.BILL_INVOICE.$invalid" style="color: red">Please enter valid bill invoice </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Amount (Including Tax)<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.AMT.$invalid}">
                                            <input id="AMT" type="number" min="0" name="AMT" maxlength="15" data-ng-pattern="/[0-9.]/" data-ng-model="SaveExpUtility.AMT" autofocus class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.AMT.$invalid" style="color: red">Please enter valid amount </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Vendor Name</label>
                                        <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.VENDOR.$invalid}">
                                            <input id="VENDOR" type="text" name="VENDOR" maxlength="15" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" data-ng-model="SaveExpUtility.VENDOR" autofocus class="form-control" />&nbsp;                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Vendor Email</label>
                                        <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.VEN_MAIL.$invalid}">
                                            <input id="VEN_MAIL" type="text" name="VEN_MAIL" maxlength="50" data-ng-pattern="/^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z-.]+\.[a-zA-Z.]{2,5}$/" data-ng-model="SaveExpUtility.VEN_MAIL" class="form-control" />&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="clearfix">
                                <div class="form-group">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <label>Vendor Phone No.</label>
                                        <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.VEN_PHNO.$invalid}">
                                            <input id="VEN_PHNO" type="text" name="VEN_PHNO" maxlength="50" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="SaveExpUtility.VEN_PHNO" class="form-control" />&nbsp;                                                    
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Department<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.DEP_CODE.$invalid}">
                                            <select id="DEP_CODE" name="DEP_CODE" data-ng-model="SaveExpUtility.DEP_CODE" class="selectpicker" data-live-search="true" required="required">
                                                <option value="">--Select--</option>
                                                <option data-ng-repeat="Ulist in UserList" value="{{Ulist.DEP_CODE}}">{{Ulist.DEP_NAME}}</option>
                                            </select>
                                            <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.DEP_CODE.$invalid" style="color: red">Please select bill generated by </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Remarks</label>
                                        <textarea id="REM" runat="server" data-ng-model="SaveExpUtility.REM" class="form-control" style="height: 30%" maxlength="500"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px;">
                                    <div class="form-group">
                                        <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="SaveExpUtility.STATUS=='New'" />
                                        <input type="button" value="Modify" ng-click="UpdateExpUtilityDetails()" class='btn btn-primary custom-button-color' data-ng-show="SaveExpUtility.STATUS=='Modify'" />
                                        <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/LocationUtility.js"></script>


</body>
</html>
