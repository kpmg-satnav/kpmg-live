﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript">
        function setup(id) {
            $('.date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                orientation: 'bottom'
            });
        };
    </script>
    <style>
        .ag-header-container {
            background-color: black;
        }

        .ag-header {
            background-color: #4682B4;
        }

        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-row, ag-header, ag-header-row {
            color: black;
        }
    </style>
</head>
<body data-ng-controller="WorkRequestController" class="amantra">

    <div class="al-content" style="height: 500px;">
        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="View All Properties" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading">
                        <h3 class="panel-title panel-heading-qfms-title">Work Request</h3>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <form id="form0" name="frmProp" data-valid-submit="LoadData()">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                    <div class="form-group">
                                        <label class="control-label">Company</label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="WorkRequest.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                            item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="WorkRequest.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmProp.$submitted && frmProp.Company.$invalid" style="color: red">Please Select Company </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-left" style="padding-left: 30px; padding-right: 30px; padding-top: 20px">
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                            <%-- </form>
                            <form id="form1" name="WorkReq" runat="server">--%>
                            <div id="TotalRequest">
                                <%--<input id="filtertxt" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%" />--%>
                                <div class="input-group" style="width: 20%">
                                    <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary custom-button-color" type="submit">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div data-ag-grid="gridOptions" style="height: 300px;" class="ag-blue"></div>
                            </div>
                            <br />
                            <br />
                            <div id="PropertyReq" data-ng-show="PropertyDetailsVisible">
                                <input id="filtertxt1" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%" />
                                <div data-ag-grid="gridOptionsProperty" style="height: 300px;" class="ag-blue"></div>
                            </div>
                        </form>
                    </div>

                    <div class="modal fade bs-example-modal-lg col-md-12 " id="historymodal" data-backdrop="false">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="panel-group box box-primary" id="Div2">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <div class="panel-heading ">
                                            <h4 class="panel-title modal-header-primary" style="padding-left: 17px" data-target="#collapseTwo">Work Request Details</h4>
                                            <form role="form" name="form2" id="form3">
                                                <div class="row">
                                                    <div class="col-md-12" id="table2">
                                                        <br />
                                                        <a data-ng-click="GenReport(WorkRequest,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                                        <a data-ng-click="GenReport(WorkRequest,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                                        <a data-ng-click="GenReport(WorkRequest,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="box">
                                                            <div class="box-danger table-responsive">
                                                                <div data-ag-grid="PopOptions" class="ag-blue" style="height: 305px; width: 100%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <link href="../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script src="../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../JS/WorkRequest.min.js"></script>
    <script src="../../SMViews/Utility.min.js"></script>
</body>
</html>
