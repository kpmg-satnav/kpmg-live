<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmUpdateWorkRequest.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmUpdateWorkRequest" Title="Update Work Request" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Update Work Request" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Update Work Request</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Work Request<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="cvProperty" runat="server" ControlToValidate="ddlWorkRequest"
                                                Display="None" ErrorMessage="Please Select Work Request" ValidationGroup="Val1"
                                                InitialValue="0"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlWorkRequest" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Work Title<span style="color: red;">*</span></label>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtWorkTitle" runat="server" CssClass="form-control" MaxLength="50"
                                                    Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Work Specifications<span style="color: red;">*</span></label>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtWorkSpec" runat="server" CssClass="form-control" Height="30%" TextMode="MultiLine" MaxLength="250" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Estimated Amount<span style="color: red;">*</span></label>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">City<span style="color: red;">*</span></label>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtcity" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Location<span style="color: red;">*</span></label>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control" MaxLength="50"
                                                    Enabled="False"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Property Type <span style="color: red;">*</span></label>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtproptype" runat="server" CssClass="form-control"
                                                    Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Property <span style="color: red;">*</span></label>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtProperty" runat="server" CssClass="form-control"
                                                    Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Start Date<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvstartdate" runat="server" Display="none" ErrorMessage="Please Enter Start date"
                                                ControlToValidate="txtStartDate" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <div class='input-group date' id='startdate'>
                                                    <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('startdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Expected End Date<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvexpecteddate" runat="server" Display="none" ErrorMessage="Please Enter Expected date"
                                                ControlToValidate="txtExpiryDate" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <div class='input-group date' id='Enddate'>
                                                    <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('Enddate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Paid Amount<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpamount"
                                                Display="none" ErrorMessage="Please Enter Paid Amount" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="none" runat="server"
                                                ControlToValidate="txtpamount" ErrorMessage="Invalid Paid Amount" ValidationExpression="^[0-9 ]*$"
                                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtpamount" runat="server" CssClass="form-control" MaxLength="8" AutoPostBack="True"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Outstanding Amount<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfv1" runat="server" ControlToValidate="txtoamount"
                                                Display="none" ErrorMessage="Please Enter Outstanding Amount" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtoamount" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Work Status<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="cvworkstatus" runat="server" ControlToValidate="ddlwstatus"
                                                Display="none" ErrorMessage="Please Select Work status" ValidationGroup="Val1"
                                                InitialValue="--Select Status--"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlwstatus" runat="server" CssClass="form-control selectpicker" data-live-search="true"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Work Condition<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                                                Display="none" ErrorMessage="Work Condition Required" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%" MaxLength="250"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 20px; padding-left: 27px">
                                        <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1" />
                                    </div>
                                </div>


                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvwrequest" runat="server" EmptyDataText="No Update Work Request Found."
                                            CssClass="table table-condensed table-bordered table-hover table-striped" AutoGenerateColumns="false" AllowPaging="true" PageSize="5">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Work RequestID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblid" runat="Server" Text='<%# Eval("PN_WORKREQUEST_REQ") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Start Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsdate" runat="server" Text='<%# Eval("START_DATE") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="End Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbledate" runat="server" Text='<%# Eval("END_DATE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Paid Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpamount" runat="server" Text='<%# Eval("PAID_AMOUNT", "{0:c2}")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Outstanding Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbloamount" runat="server" Text='<%# Eval("OUTSTANDING_AMOUNT","{0:c2}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Item Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstatus" runat="server" Text='<%# Eval("STATUS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Work Condition">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCondition" runat="server" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
<script>
    function refreshSelectpicker() {
        $("#<%=ddlWorkRequest.ClientID%>").selectpicker();
           

#End ExternalSource

#ExternalSource ("D:\MRUDULA\Tortoise\QuickFMS_3.2_DEV\PropertyManagement\Views\frmUpdateWorkRequest.aspx", 13)
            __o = ddlwstatus.ClientID%>").selectpicker();
             

        }
        refreshSelectpicker();


</script>



