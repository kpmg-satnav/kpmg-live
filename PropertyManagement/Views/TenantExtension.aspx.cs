﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PropertyManagement_Views_TenantExtension : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "anything", "refreshSelectpicker();", true);
        string path = HttpContext.Current.Request.Url.AbsolutePath;
        string host = HttpContext.Current.Request.Url.Host;
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 50);
        param[0].Value = Session["UID"];
        param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
        param[1].Value = path;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
        {
            if (Session["UID"].ToString() == "")
            {
                Response.Redirect(Application["FMGLogout"].ToString());
            }
            else
            {
                if (sdr.HasRows)
                {
                }
                else
                {
                    Response.Redirect(Application["FMGLogout"].ToString());
                }
            }
        }
        if (!IsPostBack)
        {
            BindGrid();
            // BindRejectedPPts();
        }
    }

    private void BindGrid()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_TENANT_DETAILS");
            sp.Command.AddParameter("@USER", Session["uid"], DbType.String);
            sp.Command.AddParameter("@COMPANY", Session["COMPANYID"], DbType.String);
            sp.Command.AddParameter("@FLAG", 6, DbType.Int32);
            gvLDetails_Lease.DataSource = sp.GetDataSet();
            gvLDetails_Lease.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    //private void BindRejectedPPts()
    //{
    //    try
    //    {
    //        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_TENANT_DETAILS");
    //        sp.Command.AddParameter("@USER", Session["uid"], DbType.String);           
    //        sp.Command.AddParameter("@FLAG", 7, DbType.Int32);
    //        gvrejected.DataSource = sp.GetDataSet();
    //        gvrejected.DataBind();
    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //}

    protected void gvLDetails_Lease_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Extension")
            {
                lblmsg.Text = string.Empty;
                panel1.Visible = true;
                //GetProperties(hdnReqid.Value);
                GridViewRow gvRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int rowIndex = gvRow.RowIndex;

                hdnPropCode.Value = ((Label)gvLDetails_Lease.Rows[rowIndex].FindControl("lblSNO")).Text;

                txtPname.Text = ((Label)gvLDetails_Lease.Rows[rowIndex].FindControl("lblpname")).Text;
                txtPterms.Text = ((Label)gvLDetails_Lease.Rows[rowIndex].FindControl("LblPAYTName")).Text;
                TntFrm.Text = ((Label)gvLDetails_Lease.Rows[rowIndex].FindControl("lblEdate")).Text;
                string Payid = ((Label)gvLDetails_Lease.Rows[rowIndex].FindControl("lblPAYid")).Text;
                txtRent.Text = ((Label)gvLDetails_Lease.Rows[rowIndex].FindControl("LblTenRent")).Text;

                hdnpayId.Value = Payid;
                txtpayCount.Text = "1";
                if (Payid == "1")
                    txttoDt.Text = Convert.ToDateTime(TntFrm.Text).AddMonths(1).AddDays(-1).ToString("MM/dd/yyyy");
                else if (Payid == "2")
                    txttoDt.Text = Convert.ToDateTime(TntFrm.Text).AddMonths(3).AddDays(-1).ToString("MM/dd/yyyy");
                else if (Payid == "3")
                    txttoDt.Text = Convert.ToDateTime(TntFrm.Text).AddMonths(12).AddDays(-1).ToString("MM/dd/yyyy");
                else if (Payid == "4")
                    txttoDt.Text = Convert.ToDateTime(TntFrm.Text).AddMonths(6).AddDays(-1).ToString("MM/dd/yyyy");
            }
            else
            {
                panel1.Visible = false;
            }
            //panel2.Visible = false;
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void gvLDetails_Lease_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        gvLDetails_Lease.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    public class ImageClas
    {

        private string _fn;
        public string Filename
        {
            get { return _fn; }
            set { _fn = value; }
        }


        private string _Fpath;
        public string FilePath
        {
            get { return _Fpath; }
            set { _Fpath = value; }
        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_CHECK_PPTY_EXT_REQ");
        sp.Command.AddParameter("@PROP_SNO", hdnPropCode.Value, DbType.Int32);
        int RES = Convert.ToInt32(sp.ExecuteScalar());
        if (RES > 0)
        {
            lblmsg.Text = txtPname.Text + " is Already Under Tenant Extension Or Renewal Requisition Process";
            return;
        }

        SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_TEN_EXTENSION_REQUISITION");
        sp3.Command.AddParameter("@TEN_SNO", hdnPropCode.Value, DbType.String);
        sp3.Command.AddParameter("@PAYID", hdnpayId.Value, DbType.String);
        sp3.Command.AddParameter("@NOOF_PAYTERMS", Convert.ToInt32(txtpayCount.Text), DbType.Int32);

        sp3.Command.AddParameter("@EXT_STARTDT", TntFrm.Text, DbType.Date);
        sp3.Command.AddParameter("@EXT_ENDDT", txttoDt.Text, DbType.Date);
        sp3.Command.AddParameter("@RENT", Convert.ToDecimal(txtRent.Text), DbType.Decimal);
            
        if (txtSecurityDeposit.Text=="")       
            sp3.Command.AddParameter("@SEC_DEPOSIT", "", DbType.String);
            else 
            sp3.Command.AddParameter("@SEC_DEPOSIT", Convert.ToDecimal(txtSecurityDeposit.Text), DbType.String);  
              
        sp3.Command.AddParameter("@REMS", txtRemarks.Text, DbType.String);
        sp3.Command.AddParameter("@CREATED_BY", Session["uid"], DbType.String);
        sp3.Command.AddParameter("@CMP_ID", Session["COMPANYID"], DbType.String);
        Object Obj= sp3.ExecuteScalar();

        
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@SNO", DbType.Int32);
        param[0].Value = Obj;
        List<ImageClas> Imgclass = new List<ImageClas>();
		ImageClas IC = default(ImageClas);
		if (fu1.PostedFiles != null) {
			foreach (var File_loopVariable in fu1.PostedFiles) {
                if (File_loopVariable.ContentLength > 0)
                {
					string Upload_Time = (DateTime.Now).ToString("ddMMyyyyhhmm");
                    string filePath = Server.MapPath ("~\\images\\Property_Images\\" + Upload_Time + "_" + File_loopVariable.FileName);
                    File_loopVariable.SaveAs(filePath);
					IC = new ImageClas();
                    IC.Filename = File_loopVariable.FileName;
                    IC.FilePath = Upload_Time + "_" + File_loopVariable.FileName;
					Imgclass.Add(IC);
				}
			}
		}
        param[1] = new SqlParameter("@IMAGES", SqlDbType.Structured);
        param[1].Value =  UtilityService.ConvertToDataTable(Imgclass);
        param[2] = new SqlParameter("@CREATED_BY", DbType.String);
        param[2].Value = Session["uid"];
        param[3] = new SqlParameter("@COMPANY", DbType.String);
        param[3].Value = Session["COMPANYID"];
        int ret = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_TEN_EXTENSION_DOCS", param);

        BindGrid();
        panel1.Visible = false;
        lblmsg.Text = "Request Submitted Successfully";
    }

    protected void txtpayCount_TextChanged(object sender, EventArgs e)
    {
        int PayTermCount = Convert.ToInt32(txtpayCount.Text);

        if (hdnpayId.Value == "1")
            txttoDt.Text = Convert.ToDateTime(TntFrm.Text).AddMonths(1 * PayTermCount).AddDays(-1).ToString("MM/dd/yyyy");
        else if (hdnpayId.Value == "2")
            txttoDt.Text = Convert.ToDateTime(TntFrm.Text).AddMonths(3 * PayTermCount).AddDays(-1).ToString("MM/dd/yyyy");
        else if (hdnpayId.Value == "3")
            txttoDt.Text = Convert.ToDateTime(TntFrm.Text).AddMonths(12 * PayTermCount).AddDays(-1).ToString("MM/dd/yyyy");
        else if (hdnpayId.Value == "4")
            txttoDt.Text = Convert.ToDateTime(TntFrm.Text).AddMonths(6 * PayTermCount).AddDays(-1).ToString("MM/dd/yyyy");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_TENANT_DETAILS");
        sp.Command.AddParameter("@USER", Session["uid"], DbType.String);
        sp.Command.AddParameter("@COMPANY", Session["COMPANYID"], DbType.String);
        sp.Command.AddParameter("@FLAG", 10, DbType.Int32);
        sp.Command.AddParameter("@TEN_CODE", txtSearch.Text, DbType.String);
        gvLDetails_Lease.DataSource = sp.GetDataSet();
        gvLDetails_Lease.DataBind();
    }
}