﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class WorkSpace_SMS_Webfiles_LeaseExtensionAppRej
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet
    Dim RentRevision As DataTable
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'If Session("UID") = "" Then
        '    Response.Redirect(Application("FMGLogout"))
        'End If

        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then

            scriptManager.RegisterPostBackControl(grdDocs)


        End If
        If Request.QueryString("id") <> "" Then
            Session("id") = Request.QueryString("rid")
        End If

        If Not IsPostBack Then
            RegExpRem.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
            BindDetails()
            BindDocuments()

        End If
        txttodate.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_EXTENSION_DETAILS")
            sp.Command.AddParameter("@LEASE", Request.QueryString("ID"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtstore.Text = ds.Tables(0).Rows(0).Item("REQ_ID")
                txtfromdate.Text = ds.Tables(0).Rows(0).Item("PM_LE_START_DT")
                txtrent.Text = ds.Tables(0).Rows(0).Item("RENT_AMOUNT")
                lblstrtdate.Text = ds.Tables(0).Rows(0).Item("START_DATE").ToString()
                lblenddate.Text = ds.Tables(0).Rows(0).Item("END_DATE").ToString()
                txtsdep.Text = ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")
                txttodate.Text = ds.Tables(0).Rows(0).Item("PM_LE_END_DT")
                'DocLink.Text = ds.Tables(0).Rows(0).Item("PM_LE_POS_LETTER_NAME").ToString()
            End If
            pnl1.Visible = True

        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub


    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~/PropertyManagement/Views/frmApproveLeaseExtension.aspx")
    End Sub


    Protected Sub btnapprove_Click(sender As Object, e As EventArgs) Handles btnapprove.Click
        Try
            If CDate(txttodate.Text) < CDate(txtfromdate.Text) Then
                lblMsg.Text = "Lease End Date should be greater than Start date"
                Exit Sub
            Else
                lblMsg.Text = ""
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_EXTENSION_APPROVE")
                sp.Command.AddParameter("@LEASEID", Request.QueryString("ID"), DbType.String)
                sp.Command.AddParameter("@EXTENDED_DATE", txttodate.Text, DbType.Date)
                sp.Command.AddParameter("@APPRV_REMARKS", txtremarks.Text, DbType.String)
                sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
                sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
                sp.ExecuteScalar()
                'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=60", False)


                lblMsg.Visible = True
                lblMsg.Text = "Lease Extension Approved Successfully"

            End If
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnreject_Click(sender As Object, e As EventArgs) Handles btnreject.Click
        Try
            If CDate(txttodate.Text) < CDate(txtfromdate.Text) Then
                lblMsg.Text = "Lease End Date should be greater than Start date"
                Exit Sub
            Else
                lblMsg.Text = ""
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_EXTENSION_REJECT")
                sp.Command.AddParameter("@LEASEID", Request.QueryString("ID"), DbType.String)
                sp.Command.AddParameter("@APPRV_REMARKS", txtremarks.Text, DbType.String)
                sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
                sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
                sp.ExecuteScalar()
                'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=60", False)


                lblMsg.Visible = True
                lblMsg.Text = "Lease Extension Rejected Successfully"

            End If
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub


    Public Sub BindDocuments()
        Dim dtDocs As New DataTable("Documents")
        param = New SqlParameter(1) {}
        param(0) = New SqlParameter("@PM_LEDOC_PM_LES_SNO", DbType.String)
        param(0).Value = Request.QueryString("ID")
        param(1) = New SqlParameter("@COMPANYID", DbType.Int32)
        param(1).Value = Session("COMPANYID")
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("[PM_GET_LEASE_EXTENTION_DOCS]", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdDocs.DataSource = ds
            grdDocs.DataBind()
            tblGridDocs.Visible = True
            tblGridDocs.Visible = True
            lblDocsMsg.Text = ""
        Else
            tblGridDocs.Visible = False
            lblMsg.Visible = True
            lblDocsMsg.Text = "No Documents Available"
        End If
        dtDocs = Nothing
    End Sub

    Private Sub grdDocs_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.DeleteCommand
        If grdDocs.EditItemIndex = -1 Then
            Dim Bid As String
            Bid = grdDocs.DataKeys(e.Item.ItemIndex)
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@PM_LEDOC_SNO", SqlDbType.NVarChar, 50)
            param(0).Value = Bid
            ObjSubSonic.GetSubSonicDataSet("[DELETE_LEASE_EXTENTION_DOCS]", param)
            BindDocuments()
        End If
    End Sub

    Private Sub grdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.ItemCommand
        If e.CommandName = "Download" Then
            Dim pm_ldoc_sno = grdDocs.DataKeys(e.Item.ItemIndex)
            e.Item.BackColor = Drawing.Color.LightSteelBlue
            Dim filePath As String
            filePath = grdDocs.Items(e.Item.ItemIndex).Cells(1).Text
            filePath = "~\UploadFiles\" & filePath
            Response.ContentType = "application/CSV"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "~\UploadFiles", "") & """")
            Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If
    End Sub
End Class
