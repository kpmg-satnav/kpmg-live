﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SubSonic;
using System.IO;
using System.Net;
using System.Drawing;

public partial class WorkSpace_SMS_Webfiles_LeaseServiceAgreement : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Form.Attributes.Add("enctype", "multipart/form-data");
        ScriptManager scriptManager = ScriptManager.GetCurrent(this);
        if (scriptManager != null)
        {
            scriptManager.RegisterPostBackControl(btnSubmit);
        }
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "anything", "refreshSelectpicker();", true);
        string path = HttpContext.Current.Request.Url.AbsolutePath;
        string host = HttpContext.Current.Request.Url.Host;
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 50);
        param[0].Value = Session["UID"];
        param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
        param[1].Value = path;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
        {
            if (Session["UID"].ToString() == "")
            {
                Response.Redirect(Application["FMGLogout"].ToString());
            }
            else
            {
                if (sdr.HasRows)
                {
                }
                else
                {
                    Response.Redirect(Application["FMGLogout"].ToString());
                }
            }
        }
        revAccno.ValidationExpression = User_Validation.GetValidationExpressionForNumber().VAL_EXPR;
        revBankName.ValidationExpression = User_Validation.GetValidationExpressionForNormalText().VAL_EXPR;
        RegularExpressionValidator3.ValidationExpression = User_Validation.GetValidationExpressionForNormalText().VAL_EXPR;
        revDeposited.ValidationExpression = User_Validation.GetValidationExpressionForNormalText().VAL_EXPR;
        revl3brnch.ValidationExpression = User_Validation.GetValidationExpressionForNormalText().VAL_EXPR;
        REVIFsc.ValidationExpression = User_Validation.GetValidationExpressionForNormalText().VAL_EXPR;

        if (!IsPostBack)
        {
            FillGridData();
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = false;
            GetServiceType();
            GetServiceProvider();
            GetServiceFreq();
            GetPaymentMode();
            GetPaymentTerms();
            GetVendors();
        }

    }


    private void FillGridData()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_SERVICE_AGREEMENT_GRID_DATA");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        ViewState["reqDetails"] = ds;
        gvItems.DataSource = ViewState["reqDetails"];
        gvItems.DataBind();

    }
    protected void GetServiceType()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_SERVICE_TYPE");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ddlServiceType.DataSource = sp.GetDataSet();
        ddlServiceType.DataTextField = "NAME";
        ddlServiceType.DataValueField = "CODE";
        ddlServiceType.DataBind();
        ddlServiceType.Items.Insert(0, "--Select--");

    }
    protected void GetServiceProvider()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_SERVICE_PROVIDER");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ddlServiceProvider.DataSource = sp.GetDataSet();
        ddlServiceProvider.DataTextField = "NAME";
        ddlServiceProvider.DataValueField = "CODE";
        ddlServiceProvider.DataBind();
        ddlServiceProvider.Items.Insert(0, "--Select--");

    }
    protected void GetServiceFreq()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_SERVICE_FREQ");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ddlServiceFreq.DataSource = sp.GetDataSet();
        ddlServiceFreq.DataTextField = "NAME";
        ddlServiceFreq.DataValueField = "CODE";
        ddlServiceFreq.DataBind();
        ddlServiceFreq.Items.Insert(0, "--Select--");
    }
    protected void GetPaymentTerms()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_PAYMENT_TERMS");
        ddlPaymentTerm.DataSource = sp.GetDataSet();
        ddlPaymentTerm.DataTextField = "PM_PT_NAME";
        ddlPaymentTerm.DataValueField = "PM_PT_SNO";
        ddlPaymentTerm.DataBind();
        ddlPaymentTerm.Items.Insert(0, "--Select--");
    }
    protected void GetPaymentMode()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_PAYMENT_MODE");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ddlPaymentType.DataSource = sp.GetDataSet();
        ddlPaymentType.DataTextField = "NAME";
        ddlPaymentType.DataValueField = "CODE";
        ddlPaymentType.DataBind();
        ddlPaymentType.Items.Insert(0, "--Select--");
    }

    protected void GetVendors()
    {

        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AST_GET_VENDORS");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ddlVendor.DataSource = sp.GetDataSet();
        ddlVendor.DataTextField = "AVR_NAME";
        ddlVendor.DataValueField = "AVR_CODE";
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, "--Select--");
    }
    protected void gvItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvItems.PageIndex = e.NewPageIndex;
        gvItems.DataSource = ViewState["reqDetails"];
        gvItems.DataBind();
        lblMsg.Text = "";
    }
    protected void gvItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        lblMsg.Text = "";
        if (e.CommandName == "ServiceAgreement")
        {
            GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            int RowIndex = gvr.RowIndex;
            foreach (GridViewRow row in gvItems.Rows)
            {
                if (row.RowIndex == RowIndex)
                {
                    gvItems.Rows[RowIndex].BackColor = System.Drawing.Color.SkyBlue;
                }
                else
                {
                    gvItems.Rows[row.RowIndex].BackColor = Color.Empty;
                }
            }
            LinkButton lnkLease = (LinkButton)e.CommandSource;
            GridViewRow gvRow = (GridViewRow)lnkLease.NamingContainer;
            Label lblLseName = (Label)gvRow.FindControl("lblLseName");
            txtleaseid.Text = lblLseName.Text;

            panel1.Visible = true;
        }
        else
        {
            panel1.Visible = false;
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_SEARCH_SERVICE_AGREEMENT_BY_LEASE");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        sp.Command.AddParameter("@LEASEID", txtReqId.Text, DbType.String);
        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        ViewState["reqDetails"] = ds;
        gvItems.DataSource = ViewState["reqDetails"];
        gvItems.DataBind();
    }

    private void BrowseFiles(int flag, string filename)
    {
        try
        {

            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_ADD_LEASE_SERVICE_AGREEMENT_DOCS");
            sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
            sp.Command.AddParameter("@LEASE_ID", txtleaseid.Text, DbType.String);
            sp.Command.AddParameter("@DOC_NAME", filename, DbType.String);
            sp.Command.AddParameter("@FLAG", flag, DbType.Int32);
            sp.ExecuteScalar();

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {

            string filename = "";
            int flag = 0;


            if ((BrowseServAgree.HasFile))
            {
                string orgfilenameAgree = BrowseServAgree.FileName;
                string fileExt = null;
                fileExt = System.IO.Path.GetExtension(BrowseServAgree.FileName);
                string filePath = Request.PhysicalApplicationPath.ToString() + "UploadFiles\\" + orgfilenameAgree;
                BrowseServAgree.PostedFile.SaveAs(filePath);
                filename = orgfilenameAgree;
                flag = 0;
                BrowseFiles(flag, filename);
            }
            if ((BrowseInvoice.HasFile))
            {
                string orgfilenameInvo = BrowseInvoice.FileName;
                string fileExt = null;
                fileExt = System.IO.Path.GetExtension(BrowseInvoice.FileName);
                string filePath = Request.PhysicalApplicationPath.ToString() + "UploadFiles\\" + orgfilenameInvo;
                BrowseInvoice.PostedFile.SaveAs(filePath);
                filename = orgfilenameInvo;
                flag = 1;
                BrowseFiles(flag, filename);
            }
            if ((BrowseTaxDoc.HasFile))
            {
                string orgfilenameTax = BrowseTaxDoc.FileName;
                string fileExt = null;
                fileExt = System.IO.Path.GetExtension(BrowseTaxDoc.FileName);
                string filePath = Request.PhysicalApplicationPath.ToString() + "UploadFiles\\" + orgfilenameTax;
                BrowseInvoice.PostedFile.SaveAs(filePath);
                filename = orgfilenameTax;
                flag = 2;
                BrowseFiles(flag, filename);
            }

            int ValidateCode = 0;
            ValidateCode = Convert.ToInt32(ValidateService());
            if (ValidateCode == 0)
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Service Agreement Created for the Selected Criteria";
            }
            else if (ValidateCode == 1)
            {
                InsertRecord();
            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

    }
    public void InsertRecord()
    {
        try
        {

            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_ADD_LEASE_SERVICE_AGREEMENT");
            sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
            sp.Command.AddParameter("@LEASE_ID", txtleaseid.Text, DbType.String);
            sp.Command.AddParameter("@SER_TYPE", ddlServiceType.SelectedItem.Text, DbType.String);
            sp.Command.AddParameter("@SER_PROV", ddlServiceProvider.SelectedItem.Text, DbType.String);
            sp.Command.AddParameter("@VENDOR", ddlVendor.SelectedItem.Text, DbType.String);
            sp.Command.AddParameter("@SER_FREQ", ddlServiceFreq.SelectedItem.Text, DbType.String);
            sp.Command.AddParameter("@PAY_TERMS", ddlPaymentTerm.SelectedItem.Text, DbType.String);
            sp.Command.AddParameter("@PAY_MODE", ddlPaymentType.SelectedItem.Text, DbType.String);
            sp.Command.AddParameter("@AGREE_TITLE", txtAgreeTitle.Text, DbType.String);
            sp.Command.AddParameter("@COST", txtCost.Text, DbType.Decimal);
            sp.Command.AddParameter("@OTH_COST", txtOtherCost.Text, DbType.Decimal);
            sp.Command.AddParameter("@TAX", txtTax.Text, DbType.Decimal);
            sp.Command.AddParameter("@TOTAL", txtTotal.Text, DbType.Decimal);
            sp.Command.AddParameter("@TERM_DAYS", txtTerm.Text == "" ? "0" : txtTerm.Text, DbType.Int32);
            sp.Command.AddParameter("@AGREE_START_DATE", AgreeStartDate.Text, DbType.DateTime);
            sp.Command.AddParameter("@AGREE_END_DATE", AgreeEndDate.Text, DbType.DateTime);
            sp.Command.AddParameter("@PAY_DATE", PaymentDate.Text, DbType.DateTime);
            sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String);


            if (ddlPaymentType.SelectedItem.Value == "1")
            {
                sp.Command.AddParameter("@ACC_NO", "", DbType.String);
                sp.Command.AddParameter("@BANK", "", DbType.String);
                sp.Command.AddParameter("@BRANCH", "", DbType.String);
                sp.Command.AddParameter("@IFSC", "", DbType.String);

            }
            else if (ddlPaymentType.SelectedItem.Value == "2")
            {
                sp.Command.AddParameter("@ACC_NO", txtAccNo.Text, DbType.String);
                sp.Command.AddParameter("@BANK", txtBankName.Text, DbType.String);
                sp.Command.AddParameter("@BRANCH", "", DbType.String);
                sp.Command.AddParameter("@IFSC", "", DbType.String);

            }
            else if (ddlPaymentType.SelectedItem.Value == "3")
            {
                sp.Command.AddParameter("@ACC_NO", txtAccTwo.Text, DbType.String);
                sp.Command.AddParameter("@BANK", txtBankTwo.Text, DbType.String);
                sp.Command.AddParameter("@BRANCH", txtbrnch.Text, DbType.String);
                sp.Command.AddParameter("@IFSC", txtIFSC.Text, DbType.String);

            }
            else if (ddlPaymentType.SelectedItem.Value == "4")
            {
                sp.Command.AddParameter("@ACC_NO", txtAccNo.Text, DbType.String);
                sp.Command.AddParameter("@BANK", txtBankName.Text, DbType.String);
                sp.Command.AddParameter("@BRANCH", "", DbType.String);
                sp.Command.AddParameter("@IFSC", "", DbType.String);
            }

            sp.ExecuteScalar();

            lblMsg.Visible = true;
            lblMsg.Text = "Lease Service Agreement Created Successfully";
            ClearDropdown(this);
            ClearTextBox(this);

        }

        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    public object ValidateService()
    {
        int ValidateCode = 0;
        SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_VALIDATE_SERVICE_AGREEMENT");
        sp1.Command.AddParameter("@SER_TYPE", ddlServiceType.SelectedItem.Text, DbType.String);
        sp1.Command.AddParameter("@SER_PROV", ddlServiceProvider.SelectedItem.Text, DbType.String);
        sp1.Command.AddParameter("@VENDOR", ddlVendor.SelectedItem.Text, DbType.String);
        sp1.Command.AddParameter("@SER_FREQ", ddlServiceFreq.SelectedItem.Text, DbType.String);
        sp1.Command.AddParameter("@AGREE_TITLE", txtAgreeTitle.Text, DbType.String);
        sp1.Command.AddParameter("@AGREE_START_DATE", AgreeStartDate.Text, DbType.Date);
        sp1.Command.AddParameter("@AGREE_END_DATE", AgreeEndDate.Text, DbType.Date);
        sp1.Command.AddParameter("@LEASE_ID", txtleaseid.Text, DbType.String);

        ValidateCode = Convert.ToInt32(sp1.ExecuteScalar());
        return ValidateCode;
    }

    protected void txtreset_Click(object sender, EventArgs e)
    {
        FillGridData();
        txtReqId.Text = "";

    }

    protected void txtCost_TextChanged(object sender, EventArgs e)
    {
        Double cost = Convert.ToDouble(txtCost.Text);
        Double tax = Convert.ToDouble(txtTax.Text);
        Double othcost = Convert.ToDouble(txtOtherCost.Text);
        txtTotal.Text = (cost + tax + othcost).ToString();

    }
    protected void txtOtherCost_TextChanged(object sender, EventArgs e)
    {
        Double cost = Convert.ToDouble(txtCost.Text);
        Double tax = Convert.ToDouble(txtTax.Text);
        Double othcost = Convert.ToDouble(txtOtherCost.Text);
        txtTotal.Text = (cost + tax + othcost).ToString();
    }
    protected void txtTax_TextChanged(object sender, EventArgs e)
    {
        Double cost = Convert.ToDouble(txtCost.Text);
        Double tax = Convert.ToDouble(txtTax.Text);
        Double othcost = Convert.ToDouble(txtOtherCost.Text);
        txtTotal.Text = (cost + tax + othcost).ToString();
    }

    protected void ddlPaymentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPaymentType.SelectedIndex > 0)
        {
            if (ddlPaymentType.SelectedItem.Value == "1")
            {
                panel2.Visible = false;
                panel3.Visible = false;
            }
            else if (ddlPaymentType.SelectedItem.Value == "2")
            {
                panel2.Visible = true;
                panel3.Visible = false;
            }
            else if (ddlPaymentType.SelectedItem.Value == "3")
            {
                panel2.Visible = false;
                panel3.Visible = true;
            }
            else if (ddlPaymentType.SelectedItem.Value == "4")
            {
                panel2.Visible = true;
                panel3.Visible = false;
            }
        }
    }

    public void ClearTextBox(Control root)
    {
        foreach (Control ctrl in root.Controls)
        {
            ClearTextBox(ctrl);
            if (ctrl is TextBox)
            {
                ((TextBox)ctrl).Text = "";
            }
        }
    }
    public void ClearDropdown(Control root)
    {
        foreach (Control ctrl in root.Controls)
        {
            ClearDropdown(ctrl);
            if (ctrl is DropDownList)
            {
                ((DropDownList)ctrl).ClearSelection();
            }
        }
    }
}