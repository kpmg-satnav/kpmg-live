﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SubSonic;
using System.IO;
using System.Net;
using SubSonic;

public partial class WorkSpace_SMS_Webfiles_SubLeaseApproveDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            Session["id"] = Request.QueryString["rid"];
        }
        if (!IsPostBack)
        {

            SubLeaseDetails();
        }

    }


    protected void SubLeaseDetails()
    {

        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_SUB_LEASE_DETAILS");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        sp.Command.AddParameter("@LEASEID", Request.QueryString["id"], DbType.String);
        IDataReader dr;
        dr = sp.GetReader();
        if (dr.Read())
        {
            txtleaseid.Text = Convert.ToString(dr["LEASE_ID"]);
            txtPropCd.Text = Convert.ToString(dr["PM_PPT_CODE"]);
            txtPropName.Text = Convert.ToString(dr["PM_PPT_NAME"]);
            txtCountry.Text = Convert.ToString(dr["CNY_NAME"]);
            txtCity.Text = Convert.ToString(dr["CTY_NAME"]);
            txtLocation.Text = Convert.ToString(dr["LCM_NAME"]);
            txtLseStrDt.Text = Convert.ToDateTime(dr["LEASE_ST_DT"]).ToString("MM/dd/yyyy");
            txtLseEndDt.Text = Convert.ToDateTime(dr["LEASE_END_DT"]).ToString("MM/dd/yyyy");
            txtLnumber.Text = Convert.ToString(dr["PM_LES_CTS_NO"]);
            txtentitle.Text = Convert.ToDecimal(dr["PM_LES_ENTITLED_AMT"]).ToString();
            txtBasicRent.Text = Convert.ToDecimal(dr["PM_LES_BASIC_RENT"]).ToString();
            txtSD.Text = Convert.ToDecimal(dr["PM_LES_SEC_DEPOSIT"]).ToString();
            txtSDMonths.Text = Convert.ToInt32(dr["PM_LES_SEC_DEP_MONTHS"]).ToString();
            txtRentFreePeriod.Text = Convert.ToString(dr["PM_LES_RENT_FREE_PERIOD"]);
            txtRentPerSqftCarpet.Text = Convert.ToDecimal(dr["PM_LES_RENT_PER_SQFT_CARPET"]).ToString();
            txtRentPerSqftBUA.Text = Convert.ToDecimal(dr["PM_LES_RENT_PER_SQFT_BUA"]).ToString();
            txtInteriorCost.Text = Convert.ToDecimal(dr["PM_LES_INTERIOR_COST"]).ToString();
            txtCarArea.Text = Convert.ToDecimal(dr["PM_AR_CARPET_AREA"]).ToString();
            txtBUA.Text = Convert.ToDecimal(dr["PM_AR_BUA_AREA"]).ToString();
            txtRemain.Text = Convert.ToDecimal(dr["REMAINING_AREA_SEAT"]).ToString();
            txtSubGroup.Text = Convert.ToString(dr["CNP_NAME"]);
            AgreeStartDate.Text = Convert.ToDateTime(dr["PM_SLA_AGREE_ST_DT"]).ToString("MM/dd/yyyy");
            AgreeEndDate.Text = Convert.ToDateTime(dr["PM_SLA_AGREE_END_DT"]).ToString("MM/dd/yyyy");
            txtSeat.Text = Convert.ToString(dr["PM_SLA_AREA_OR_SEATING"]);
            txtCost.Text = Math.Round(decimal.Parse(dr["PM_SLA_COST"].ToString()), 2).ToString();
            //txtCost.Text = decimal.Round(dr["PM_SLA_COST"]),2 ,MidpointRounding.AwayFromZero);
            txtMaintChrg.Text = Math.Round(decimal.Parse(dr["PM_SLA_MAINT_CHARGES"].ToString()), 2).ToString();
            //txtMaintChrg.Text = Convert.ToString(dr["PM_SLA_MAINT_CHARGES"]);
            txtTotal.Text = Math.Round(decimal.Parse(dr["PM_SLA_COST"].ToString()), 2).ToString();
            txtsecdpst.Text = Math.Round(decimal.Parse(dr["PM_SLA_SECURITY_DEPS"].ToString()), 2).ToString();
            txtPayTerm.Text = Convert.ToString(dr["PM_SLA_PAY_TERMS"]);
            PaymentDate.Text = Convert.ToDateTime(dr["PM_SLA_PAY_DATE"]).ToString("MM/dd/yyyy");
            txtPayType.Text = Convert.ToString(dr["PM_SLA_PAY_TYPE"]);
            txtAccNo.Text = Convert.ToString(dr["PM_SLA_ACC_SERIAL_NO"]);
            txtAccTwo.Text = Convert.ToString(dr["PM_SLA_ACC_SERIAL_NO"]);
            txtBankName.Text = Convert.ToString(dr["PM_SLA_BANK"]);
            txtBankTwo.Text = Convert.ToString(dr["PM_SLA_BANK"]);
            txtbrnch.Text = Convert.ToString(dr["PM_SLA_BRANCH"]);
            txtIFSC.Text = Convert.ToString(dr["PM_SLA_IFSC"]);
            string costType = Convert.ToString(dr["PM_SLA_COST_TYPE"]);
            rblCostType.Items.FindByValue(costType).Selected = true;
            if (costType == "Seat")
            {
                Costype1.Visible = false;
                Costype2.Visible = true;
            }
            else
            {
                Costype1.Visible = true;
                Costype2.Visible = false;
            }
            txtSeatCost.Text = Convert.ToDouble(dr["PM_SLA_SEAT_COST"]).ToString();
            txtSubCarpet.Text = Convert.ToDouble(dr["PM_SLA_RENT_SQFT_CARPET"]).ToString();
            txtSubBUA.Text = Convert.ToDouble(dr["PM_SLA_RENT_PER_SQFT_BUA"]).ToString();
            txtsublesstatus.Text= Convert.ToString(dr["SUBLEASE_STATUS"]);
        }

    }

    private void UpdateSLAll(int STA_ID)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@SUB_LEASE_ID", Request.QueryString["id"]);
            param[1] = new SqlParameter("@STA_ID", STA_ID);
            param[2] = new SqlParameter("@APPRV_REMARKS", txtRemarks.Text);
            param[3] = new SqlParameter("@AUR_ID", Session["UID"].ToString());
            param[4] = new SqlParameter("@COMPANYID", Session["COMPANYID"]);
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_SUB_LEASE_APPROVE_REJECT", param);

            lblMsg.Text = "Please Select Atleast One Checkbox";

        }



        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void btnapprove_Click(object sender, EventArgs e)
    {
        if (txtsublesstatus.Text == "Renewed Sub Lease Agreement" || txtsublesstatus.Text == "Renewed Sub Lease Approved")
        {
            UpdateSLAll(4031);

            lblMsg.Visible = true;
            lblMsg.Text = "Sub Lease Renewal Approved Successfully";
            btnapprove.Enabled = false;
            btnreject.Enabled = false;
           
        }
        else if (txtsublesstatus.Text == "Sub Lease Agreement" || txtsublesstatus.Text == "Sub Lease Approved")
        {
            UpdateSLAll(4010);

            lblMsg.Visible = true;
            lblMsg.Text = "Sub Lease Agreement Approved Successfully";
            btnapprove.Enabled = false;
            btnreject.Enabled = false;
            
        }

    }
    

    protected void btnreject_Click(object sender, EventArgs e)
    {

        UpdateSLAll(4011);

        lblMsg.Visible = true;
        lblMsg.Text = "Sub Lease Agreement Rejected Successfully";
        btnreject.Enabled = false;
        btnapprove.Enabled = false;
       

    }

    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/PropertyManagement/Views/SubLeaseApprove.aspx");
    }
}