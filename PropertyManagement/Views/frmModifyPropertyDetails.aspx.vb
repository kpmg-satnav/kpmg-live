Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO

Partial Class WorkSpace_SMS_Webfiles_frmModifyPropertyDetails
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then
            scriptManager.RegisterPostBackControl(btnSubmit)
            scriptManager.RegisterPostBackControl(gvPropdocs)
        End If
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)

        revtxtToilet.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        revpropertyname.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        'rgetxtESTD.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        RegularExpressionValidator6.ValidationExpression = User_Validation.GetValidationExpressionForLat.VAL_EXPR()
        RegularExpressionValidator9.ValidationExpression = User_Validation.GetValidationExpressionForLong.VAL_EXPR()
        REVPHNO.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        RegularExpressionValidator10.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        rfv.ValidationExpression = User_Validation.GetValidationExpressionForEmail.VAL_EXPR()
        RegularExpressionValidator12.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revBuiltupArea.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revCarpetarea.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revCommon.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revRent.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revUsable.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        RegularExpressionValidator13.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        rfvtxtCeilingHight.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        rfvtxtBeamBottomHight.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revmaxcapacity.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        revOptCapacity.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        rfvtxtSeatingCapacity.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        revtxtFSI.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revtxtEfficiency.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revtxtPurPrice.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revtxtMarketValue.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revgovt.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        If Not IsPostBack Then
            txtReqID.Text = Request.QueryString("id")
            BindAcquisitionthrough()
            BindRequestTypes()
            BindPropType()
            BindInsuranceType()
            BindUserCities()
            BindEntity()
            BindFlooringTypes()
            BindDetails()
            If Request.QueryString("staid") <> "4001" Then  'If Approved/Reject then readonly all control values and hide submit btn
                'EnableControls(Me.Page.Form.Controls, False)
                'btnSubmit.Visible = False
                btnSubmit.Visible = False
                txtPropIDName.Enabled = True

                'ElseIf Request.QueryString("staid") = "4002" Then  'If Approved/Reject then readonly all control values and hide submit btn
                '    EnableControls(Me.Page.Form.Controls, False)
                '    btnSubmit.Visible = True
                '    txtPropIDName.Enabled = True
            End If



        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        Dim status As Boolean = False

        For Each c As Control In Page.Controls

            For Each ctrl As Control In c.Controls

                If TypeOf ctrl Is TextBox Then
                    CType(ctrl, TextBox).Enabled = status
                ElseIf TypeOf ctrl Is Button Then
                    CType(ctrl, Button).Enabled = status
                ElseIf TypeOf ctrl Is RadioButton Then
                    CType(ctrl, RadioButton).Enabled = status
                ElseIf TypeOf ctrl Is ImageButton Then
                    CType(ctrl, ImageButton).Enabled = status
                ElseIf TypeOf ctrl Is CheckBox Then
                    CType(ctrl, CheckBox).Enabled = status
                ElseIf TypeOf ctrl Is DropDownList Then
                    CType(ctrl, DropDownList).Enabled = status
                ElseIf TypeOf ctrl Is HyperLink Then
                    CType(ctrl, HyperLink).Enabled = status
                ElseIf TypeOf ctrl Is LinkButton Then
                    CType(ctrl, LinkButton).Enabled = status
                End If
            Next
        Next
    End Sub
    Protected Sub ddlAgreementbyPOA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAgreementbyPOA.SelectedIndexChanged
        If ddlAgreementbyPOA.SelectedValue = "Yes" Then
            panPOA.Visible = True
        Else
            panPOA.Visible = False
        End If
    End Sub
    Public Sub EnableControls(ctrl As ControlCollection, isEnable As Boolean)
        For Each item As Control In ctrl
            If item.[GetType]() = GetType(Panel) OrElse item.[GetType]() = GetType(HtmlGenericControl) Then
                EnableControls(item.Controls, isEnable)
            ElseIf item.[GetType]() = GetType(DropDownList) Then
                DirectCast(item, DropDownList).Enabled = isEnable
            ElseIf item.[GetType]() = GetType(TextBox) Then
                DirectCast(item, TextBox).Enabled = isEnable
            ElseIf item.[GetType]() = GetType(FileUpload) Then
                DirectCast(item, FileUpload).Enabled = isEnable
            ElseIf item.[GetType]() = GetType(HtmlInputButton) Then
                DirectCast(item, HtmlInputButton).Disabled = Not isEnable
            End If
        Next
    End Sub

    Private Sub BindFlooringTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_FLOORING_TYPES")
        ddlFlooringType.DataSource = sp3.GetDataSet()
        ddlFlooringType.DataTextField = "PM_FT_TYPE"
        ddlFlooringType.DataValueField = "PM_FT_SNO"
        ddlFlooringType.DataBind()
        ddlFlooringType.Items.Insert(0, New ListItem("--Select Flooring Type--", 0))
    End Sub
    Private Sub BindEntity()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ENTITY_TYPES")
        ddlentity.DataSource = sp3.GetDataSet()
        ddlentity.DataTextField = "CHE_NAME"
        ddlentity.DataValueField = "CHE_CODE"
        ddlentity.DataBind()
        ddlentity.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub
    Private Sub BindReqId()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GENERATE_REQUESTID")
        sp3.Command.Parameters.Add("@REQ_TYPE", "ADDPROPERTY", DbType.String)
        Dim dt = DateTime.Now.ToString("MMddyyyyHHmm")
        txtReqID.Text = dt + "/PRPREQ/" + Convert.ToString(sp3.ExecuteScalar())
    End Sub

    Private Sub BindRequestTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_REQUEST_TYPES")
        ddlReqType.DataSource = sp3.GetDataSet()
        ddlReqType.DataTextField = "PM_RT_TYPE"
        ddlReqType.DataValueField = "PM_RT_SNO"
        ddlReqType.DataBind()
        ddlReqType.Items.Insert(0, New ListItem("--Select Request Type--", 0))
    End Sub

    Private Sub BindUserCities()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp3.Command.AddParameter("@DUMMY", "", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp3.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select City--", 0))
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select Property Type--", 0))
    End Sub

    Private Sub BindInsuranceType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_INSURANCE_TYPE")
        ddlInsuranceType.DataSource = sp.GetDataSet()
        ddlInsuranceType.DataTextField = "INSURANCE_TYPE"
        ddlInsuranceType.DataValueField = "ID"
        ddlInsuranceType.DataBind()
        ddlInsuranceType.Items.Insert(0, New ListItem("--Select Insurance Type--", 0))
    End Sub
    Private Sub BindAcquisitionthrough()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACQISITION_THROUGH")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlAcqThr.DataSource = sp3.GetDataSet()
        ddlAcqThr.DataTextField = "PN_ACQISITION_THROUGH"
        ddlAcqThr.DataValueField = "PN_ACQISITION_ID"
        ddlAcqThr.DataBind()
        ddlAcqThr.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub GetLocationsbyCity(ByVal city As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
        sp2.Command.AddParameter("@CITY", city)
        sp2.Command.AddParameter("@USR_ID", Session("uid"))
        ddlLocation.DataSource = sp2.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            GetLocationsbyCity(ddlCity.SelectedValue)
            ddlTower.Items.Clear()
        Else
            ddlLocation.Items.Clear()
        End If
    End Sub

    Private Sub GetTowerbyLoc(ByVal loc As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWERSBYLCMID")
        sp2.Command.AddParameter("@LCMID", loc, DbType.String)
        sp2.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
        ddlTower.DataSource = sp2.GetDataSet()
        ddlTower.DataTextField = "TWR_NAME"
        ddlTower.DataValueField = "TWR_CODE"
        ddlTower.DataBind()
        ddlTower.Items.Insert(0, New ListItem("--Select Tower--", "0"))
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex > 0 Then
            GetTowerbyLoc(ddlLocation.SelectedValue)
        Else
            ddlTower.Items.Clear()
        End If
    End Sub

    Private Sub GetFloorsbyTwr(ByVal tower As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_getFloors_masters")
        sp2.Command.AddParameter("@TWR_ID", tower, DbType.String)
        sp2.Command.AddParameter("@FLR_LOC_ID", ddlLocation.SelectedValue, DbType.String)
        sp2.Command.AddParameter("@FLR_CITY_CODE", ddlCity.SelectedValue, DbType.String)
        sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlFloor.DataSource = sp2.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, New ListItem("--Select Floor--", "0"))
        txtFloor.Text = ddlFloor.Items.Count - 1
    End Sub


    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        If ddlTower.SelectedIndex > 0 Then
            GetFloorsbyTwr(ddlTower.SelectedValue)
        Else
            ddlFloor.Items.Clear()
        End If
    End Sub


    Protected Sub txtESTD_TextChanged(sender As Object, e As EventArgs) Handles txtESTD.TextChanged
        If (txtESTD.Text <> "") Then
            Dim frmdate As DateTime = txtESTD.Text
            Dim ts As Integer = (DateTime.Now - frmdate).Days
            Dim year As Integer = Math.Floor(ts / 365)
            Dim Month As Integer = (Math.Floor(ts / 30)) - (year * 12)
            txtAge.Text = (Convert.ToString(year) + " Year  " + Convert.ToString(Month)) + " Months"
        Else
            txtAge.Text = ""
        End If
    End Sub

    Protected Sub ddlRecommended_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRecommended.SelectedIndexChanged
        divRecommanded.Visible = IIf(ddlRecommended.SelectedValue = "1", True, False)
    End Sub

    Protected Sub ddlFSI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFSI.SelectedIndexChanged
        divFSI.Visible = IIf(ddlFSI.SelectedValue = "1", True, False)
    End Sub

    Private Sub Cleardata()
        ddlReqType.SelectedIndex = 0
        ddlPprtNature.SelectedIndex = 0
        ddlAcqThr.SelectedIndex = 0
        ddlCity.SelectedIndex = 0
        ddlLocation.Items.Clear()
        ddlTower.Items.Clear()
        txtFloor.Text = ""
        ddlFloor.Items.Clear()
        txtToilet.Text = ""
        ddlPropertyType.SelectedIndex = 0
        txtPropIDName.Text = ""
        txtESTD.Text = ""
        txtAge.Text = ""
        txtPropDesc.Text = ""
        txtSignageLocation.Text = ""
        txtSocityName.Text = ""
        txtlat.Text = ""
        txtlong.Text = ""
        txtOwnScopeWork.Text = ""
        txtRecmRemarks.Text = ""

        txtownrname.Text = ""
        txtphno.Text = ""
        txtPrvOwnName.Text = ""
        txtPrvOwnPhNo.Text = ""
        txtOwnEmail.Text = ""

        txtCarpetArea.Text = ""
        txtBuiltupArea.Text = ""
        txtCommonArea.Text = ""
        txtRentableArea.Text = ""
        txtUsableArea.Text = ""
        txtSuperBulArea.Text = ""
        txtPlotArea.Text = ""
        txtCeilingHight.Text = ""
        txtBeamBottomHight.Text = ""
        txtMaxCapacity.Text = ""
        txtOptCapacity.Text = ""
        txtSeatingCapacity.Text = ""
        ddlFlooringType.SelectedIndex = 0
        txtFSI.Text = ""
        txtEfficiency.Text = ""

        txtPurPrice.Text = ""
        txtPurDate.Text = ""
        txtMarketValue.Text = ""

        txtIRDA.Text = ""
        txtPCcode.Text = ""
        txtGovtPropCode.Text = ""
        txtUOM_CODE.Text = ""

        ddlInsuranceType.SelectedIndex = 0
        txtInsuranceVendor.Text = ""
        txtInsuranceAmt.Text = ""
        txtInsurancePolNum.Text = ""
        txtInsuranceStartdate.Text = ""
        txtInsuranceEnddate.Text = ""

        gvPropdocs.DataSource = Nothing
        gvPropdocs.DataBind()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'If CInt(txtCarpetArea.Text) > CInt(txtBuiltupArea.Text) Then
        '    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be more than builtup area');</SCRIPT>", False)
        'ElseIf CInt(txtCommonArea.Text) > CInt(txtCarpetArea.Text) Or CInt(txtCommonArea.Text) > CInt(txtBuiltupArea.Text) Then
        '    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Common Area cannot be more than builtup area and Carpet');</SCRIPT>", False)

        'ElseIf CInt(txtRentableArea.Text) > CInt(txtBuiltupArea.Text) Or CInt(txtRentableArea.Text) > CInt(txtCarpetArea.Text) Or CInt(txtRentableArea.Text) > CInt(txtUsableArea.Text) Then
        '    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Rentable area cannot be more than built up, carpet area and usable area');</SCRIPT>", False)
        'Else
        'UpdatepptyDetails()
        'Cleardata()
        'BindReqId()
        'End If
        'If (Session("TENANT") <> "BNP.dbo") Then
        '    If (txtESTD.Text = "") Then
        '        lblmsg.Text = "Please Enter ESTD(Year)"
        '        txtESTD.Focus()
        '        Exit Sub
        '    End If
        'End If
        If Not Page.IsValid Then
            Exit Sub
        End If
        If txtSuperBulArea.Text <> 0 And txtBuiltupArea.Text <> 0 Then
            If CInt(txtBuiltupArea.Text) > CInt(txtSuperBulArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Builtup Area cannot be more than Super Builtup area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtBuiltupArea.Text <> 0 And txtCarpetArea.Text <> 0 Then
            If CInt(txtBuiltupArea.Text) < CInt(txtCarpetArea.Text) Then 'Or CInt(txtBuiltupArea.Text) > CInt(txtRentableArea.Text) 
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Builtup Area cannot be less than Carpet Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtCarpetArea.Text <> 0 And txtBuiltupArea.Text <> 0 Then
            If CInt(txtCarpetArea.Text) > CInt(txtBuiltupArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be more than Bulitup Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtBuiltupArea.Text <> 0 And txtRentableArea.Text <> 0 Then
            If CInt(txtBuiltupArea.Text) < CInt(txtRentableArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Builtup Area cannot be less than Rentable Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtCarpetArea.Text <> 0 And txtRentableArea.Text <> 0 Then
            If CInt(txtCarpetArea.Text) < CInt(txtRentableArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be less than Rentable Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtSuperBulArea.Text <> 0 And txtRentableArea.Text <> 0 Then
            If CInt(txtSuperBulArea.Text) < CInt(txtRentableArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Super Builtup Area cannot be less than Rentable Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtCarpetArea.Text <> 0 And txtSuperBulArea.Text <> 0 Then
            If CInt(txtCarpetArea.Text) > CInt(txtSuperBulArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Super Builtup Area cannot be less than Carpet Area');</SCRIPT>", False)
                Exit Sub
            Else
                UpdatepptyDetails()
                Cleardata()
                BindReqId()
            End If
        Else
            UpdatepptyDetails()
            Cleardata()
            BindReqId()
        End If

        '    Else
        '        UpdatepptyDetails()
        '        Cleardata()
        '        BindReqId()
        '    End If
        'End If
    End Sub

    Public Sub UpdatepptyDetails()
        Try
            Dim param As SqlParameter() = New SqlParameter(123) {}

            'General Details
            param(0) = New SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
            param(0).Value = txtReqID.Text
            param(1) = New SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar)
            param(1).Value = ddlReqType.SelectedValue
            param(2) = New SqlParameter("@PROP_NATURE", SqlDbType.VarChar)
            param(2).Value = ddlPprtNature.SelectedValue
            param(3) = New SqlParameter("@ACQ_TRH", SqlDbType.VarChar)
            param(3).Value = ddlAcqThr.SelectedValue

            param(4) = New SqlParameter("@CITY", SqlDbType.VarChar)
            param(4).Value = ddlCity.SelectedValue
            param(5) = New SqlParameter("@LOCATION", SqlDbType.VarChar)
            param(5).Value = ddlLocation.SelectedValue
            param(6) = New SqlParameter("@TOWER", SqlDbType.VarChar)
            param(6).Value = ddlTower.SelectedValue

            param(7) = New SqlParameter("@FLOOR", SqlDbType.VarChar)
            'param(7).Value = ddlFloor.SelectedValue
            param(7).Value = ([String].Join(",", ddlFloor.Items.OfType(Of ListItem)().Where(Function(r) r.Selected).[Select](Function(r) r.Value)))
            param(8) = New SqlParameter("@TOI_BLKS", SqlDbType.Int)
            param(8).Value = IIf(txtToilet.Text = "", 0, txtToilet.Text)
            param(9) = New SqlParameter("@PROP_TYPE", SqlDbType.VarChar)
            param(9).Value = ddlPropertyType.SelectedValue
            param(10) = New SqlParameter("@PROP_NAME", SqlDbType.VarChar)
            param(10).Value = txtPropIDName.Text

            param(11) = New SqlParameter("@ESTD_YR", SqlDbType.Date)
            param(11).Value = IIf(txtESTD.Text = "", DBNull.Value, txtESTD.Text)
            param(12) = New SqlParameter("@AGE", SqlDbType.VarChar)
            param(12).Value = IIf(txtAge.Text = "", "", Convert.ToString(txtAge.Text))
            param(13) = New SqlParameter("@PROP_ADDR", SqlDbType.VarChar)
            param(13).Value = txtPropDesc.Text
            param(14) = New SqlParameter("@SING_LOC", SqlDbType.VarChar)
            param(14).Value = txtSignageLocation.Text

            param(15) = New SqlParameter("@SOC_NAME", SqlDbType.VarChar)
            param(15).Value = txtSocityName.Text
            param(16) = New SqlParameter("@LATITUDE", SqlDbType.VarChar)
            param(16).Value = txtlat.Text
            param(17) = New SqlParameter("@LONGITUDE", SqlDbType.VarChar)
            param(17).Value = txtlong.Text
            param(18) = New SqlParameter("@SCOPE_WK", SqlDbType.VarChar)
            param(18).Value = txtOwnScopeWork.Text

            param(19) = New SqlParameter("@RECM_PROP", SqlDbType.VarChar)
            param(19).Value = ddlRecommended.SelectedValue
            param(20) = New SqlParameter("@RECM_PROP_REM", SqlDbType.VarChar)
            param(20).Value = txtRecmRemarks.Text

            'Owner Details
            param(21) = New SqlParameter("@OWN_NAME", SqlDbType.VarChar)
            param(21).Value = txtownrname.Text
            param(22) = New SqlParameter("@OWN_PH", SqlDbType.VarChar)
            param(22).Value = txtphno.Text
            param(23) = New SqlParameter("@PREV_OWN_NAME", SqlDbType.VarChar)
            param(23).Value = txtPrvOwnName.Text
            param(24) = New SqlParameter("@PREV_OWN_PH", SqlDbType.VarChar)
            param(24).Value = txtPrvOwnPhNo.Text

            param(25) = New SqlParameter("@OWN_EMAIL", SqlDbType.VarChar)
            param(25).Value = txtOwnEmail.Text

            'Area Details
            param(26) = New SqlParameter("@CARPET_AREA", SqlDbType.Decimal)
            param(26).Value = Convert.ToDecimal(txtCarpetArea.Text)
            param(27) = New SqlParameter("@BUILTUP_AREA", SqlDbType.Decimal)
            param(27).Value = Convert.ToDecimal(txtBuiltupArea.Text)
            param(28) = New SqlParameter("@COMMON_AREA", SqlDbType.Decimal)
            param(28).Value = Convert.ToDecimal(txtCommonArea.Text)
            param(29) = New SqlParameter("@RENTABLE_AREA", SqlDbType.Decimal)
            param(29).Value = Convert.ToDecimal(txtRentableArea.Text)

            param(30) = New SqlParameter("@USABLE_AREA", SqlDbType.Decimal)
            param(30).Value = Convert.ToDecimal(txtUsableArea.Text)
            param(31) = New SqlParameter("@SUPER_BLT_AREA", SqlDbType.Decimal)
            param(31).Value = Convert.ToDecimal(txtSuperBulArea.Text)
            param(32) = New SqlParameter("@PLOT_AREA", SqlDbType.Decimal)
            param(32).Value = Convert.ToDecimal(txtPlotArea.Text)
            param(33) = New SqlParameter("@FTC_HIGHT", SqlDbType.Decimal)
            param(33).Value = IIf(txtCeilingHight.Text = "", 0, txtCeilingHight.Text)

            param(34) = New SqlParameter("@FTBB_HIGHT", SqlDbType.Decimal)
            param(34).Value = IIf(txtBeamBottomHight.Text = "", 0, txtBeamBottomHight.Text)
            param(35) = New SqlParameter("@MAX_CAPACITY", SqlDbType.Int)
            param(35).Value = Convert.ToInt32(txtMaxCapacity.Text)
            param(36) = New SqlParameter("@OPTIMUM_CAPACITY", SqlDbType.Int)
            param(36).Value = IIf(txtOptCapacity.Text = "", 0, txtOptCapacity.Text)
            param(37) = New SqlParameter("@SEATING_CAPACITY", SqlDbType.Int)
            param(37).Value = IIf(txtSeatingCapacity.Text = "", 0, txtSeatingCapacity.Text)

            param(38) = New SqlParameter("@FLR_TYPE", SqlDbType.VarChar)
            param(38).Value = ddlFlooringType.SelectedValue
            param(39) = New SqlParameter("@FSI", SqlDbType.VarChar)
            param(39).Value = ddlFSI.SelectedValue
            param(40) = New SqlParameter("@FSI_RATIO", SqlDbType.Decimal)
            param(40).Value = IIf(txtFSI.Text = "", 0, txtFSI.Text)
            param(41) = New SqlParameter("@PFR_EFF", SqlDbType.VarChar)
            param(41).Value = txtEfficiency.Text

            'PURCHASE DETAILS
            param(42) = New SqlParameter("@PUR_PRICE", SqlDbType.Decimal)
            param(42).Value = IIf(txtPurPrice.Text = "", 0, txtPurPrice.Text)
            param(43) = New SqlParameter("@PUR_DATE", SqlDbType.Date)
            param(43).Value = IIf(txtPurDate.Text = "", DBNull.Value, txtPurDate.Text)
            param(44) = New SqlParameter("@MARK_VALUE", SqlDbType.Decimal)
            param(44).Value = IIf(txtMarketValue.Text = "", 0, txtMarketValue.Text)

            'GOVT DETAILS
            param(45) = New SqlParameter("@IRDA", SqlDbType.VarChar)
            param(45).Value = txtIRDA.Text
            param(46) = New SqlParameter("@PC_CODE", SqlDbType.VarChar)
            param(46).Value = txtPCcode.Text
            param(47) = New SqlParameter("@PROP_CODE", SqlDbType.VarChar)
            param(47).Value = txtGovtPropCode.Text
            param(48) = New SqlParameter("@UOM_CODE", SqlDbType.VarChar)
            param(48).Value = txtUOM_CODE.Text

            'INSURANCE DETAILS
            param(49) = New SqlParameter("@IN_TYPE", SqlDbType.VarChar)
            param(49).Value = ddlInsuranceType.SelectedValue
            param(50) = New SqlParameter("@IN_VENDOR", SqlDbType.VarChar)
            param(50).Value = txtInsuranceVendor.Text
            param(51) = New SqlParameter("@IN_AMOUNT", SqlDbType.Decimal)
            param(51).Value = IIf(txtInsuranceAmt.Text = "", 0, txtInsuranceAmt.Text)
            param(52) = New SqlParameter("@IN_POLICY_NUMBER", SqlDbType.VarChar)
            param(52).Value = txtInsurancePolNum.Text

            param(53) = New SqlParameter("@IN_SDATE", SqlDbType.VarChar)
            param(53).Value = IIf(txtInsuranceStartdate.Text = "", DBNull.Value, txtInsuranceStartdate.Text)
            param(54) = New SqlParameter("@IN_EDATE", SqlDbType.VarChar)
            param(54).Value = IIf(txtInsuranceEnddate.Text = "", DBNull.Value, txtInsuranceEnddate.Text)

            param(55) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(55).Value = Session("uid")
            param(56) = New SqlParameter("@PPTY_ID", SqlDbType.Int)
            param(56).Value = Request.QueryString("id")

            ' PROPERTY IMAGES UPLOAD
            Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC As ImageClas
            Dim i As Int32 = 0
            Dim fileSize As Int64 = 0
            If fu1.PostedFiles IsNot Nothing Then
                For Each File In fu1.PostedFiles
                    Dim count = fu1.PostedFiles.Count
                    While (i < count)
                        fileSize = fu1.PostedFiles(i).ContentLength + fileSize
                        i = i + 1
                    End While
                    If (fileSize > 20971520) Then
                        lblmsg.Text = "Upload Property Images size should not be greater than 20 MB."
                        Exit Sub
                    End If
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC = New ImageClas()
                        IC.Filename = File.FileName
                        IC.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass.Add(IC)
                    End If
                Next
            End If
            Dim Imgclass1 As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC1 As ImageClas
            i = 0
            fileSize = 0
            If fu2.PostedFiles IsNot Nothing Then
                Dim count = fu2.PostedFiles.Count
                While (i < count)
                    fileSize = fu2.PostedFiles(i).ContentLength + fileSize
                    i = i + 1
                End While
                If (fileSize > 20971520) Then
                    lblmsg.Text = "Upload Signage Images size should not be greater than 20 MB."
                    Exit Sub
                End If
                For Each File In fu2.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC1 = New ImageClas()
                        IC1.Filename = File.FileName
                        IC1.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass1.Add(IC1)
                    End If
                Next
            End If
            Dim Imgclass2 As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC2 As ImageClas
            i = 0
            fileSize = 0
            If fu3.PostedFiles IsNot Nothing Then
                Dim count = fu3.PostedFiles.Count
                While (i < count)
                    fileSize = fu3.PostedFiles(i).ContentLength + fileSize
                    i = i + 1
                End While
                If (fileSize > 20971520) Then
                    lblmsg.Text = "Upload AC Outdoor Images size should not be greater than 20 MB."
                    Exit Sub
                End If
                For Each File In fu3.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC2 = New ImageClas()
                        IC2.Filename = File.FileName
                        IC2.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass2.Add(IC2)
                    End If
                Next
            End If
            i = 0
            fileSize = 0
            Dim Imgclass3 As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC3 As ImageClas

            If fu4.PostedFiles IsNot Nothing Then
                Dim count = fu4.PostedFiles.Count
                While (i < count)
                    fileSize = fu4.PostedFiles(i).ContentLength + fileSize
                    i = i + 1
                End While
                If (fileSize > 20971520) Then
                    lblmsg.Text = "Upload GSB Images size should not be greater than 20 MB."
                    Exit Sub
                End If
                For Each File In fu4.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC3 = New ImageClas()
                        IC3.Filename = File.FileName
                        IC3.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass3.Add(IC3)
                    End If
                Next
            End If

            param(57) = New SqlParameter("@IMAGES", SqlDbType.Structured)
            param(57).Value = UtilityService.ConvertToDataTable(Imgclass)

            param(58) = New SqlParameter("@STA_ID", SqlDbType.Int)
            param(58).Value = 4001
            param(59) = New SqlParameter("@APP_REJ", SqlDbType.VarChar)
            param(59).Value = ""
            param(60) = New SqlParameter("@CMP_ID", SqlDbType.VarChar)
            param(60).Value = Session("COMPANYID")
            param(61) = New SqlParameter("@ENTITY", SqlDbType.VarChar)
            param(61).Value = ddlentity.SelectedValue
            param(62) = New SqlParameter("@OWN_LANDLINE", SqlDbType.VarChar)
            param(62).Value = IIf(txtllno.Text = "", DBNull.Value, txtllno.Text)
            param(63) = New SqlParameter("@POA_SIGNED", SqlDbType.VarChar)
            param(63).Value = IIf(ddlAgreementbyPOA.SelectedValue = "", DBNull.Value, ddlAgreementbyPOA.SelectedValue)
            'POA Details
            param(64) = New SqlParameter("@POA_NAME", SqlDbType.VarChar)
            param(64).Value = IIf(txtPOAName.Text = "", DBNull.Value, txtPOAName.Text)
            param(65) = New SqlParameter("@POA_ADDRESS", SqlDbType.VarChar)
            param(65).Value = IIf(txtPOAAddress.Text = "", DBNull.Value, txtPOAAddress.Text)
            param(66) = New SqlParameter("@POA_MOBILE", SqlDbType.VarChar)
            param(66).Value = IIf(txtPOAMobile.Text = "", DBNull.Value, txtPOAMobile.Text)
            param(67) = New SqlParameter("@POA_EMAIL", SqlDbType.VarChar)
            param(67).Value = IIf(txtPOAEmail.Text = "", DBNull.Value, txtPOAEmail.Text)
            param(68) = New SqlParameter("@POA_LLTYPE", SqlDbType.VarChar)
            param(68).Value = IIf(txtLLtype.Text = "", DBNull.Value, txtLLtype.Text)
            param(68) = New SqlParameter("@PHYCDTN_CEILING", SqlDbType.VarChar)
            param(68).Value = IIf(txtCeiling.Text = "", DBNull.Value, txtCeiling.Text)
            param(69) = New SqlParameter("@PHYCDTN_WINDOWS", SqlDbType.VarChar)
            param(69).Value = IIf(txtwindows.Text = "", DBNull.Value, txtwindows.Text)
            param(70) = New SqlParameter("@PHYCDTN_DAMAGE", SqlDbType.VarChar)
            param(70).Value = IIf(txtDamage.Text = "", DBNull.Value, txtDamage.Text)
            param(71) = New SqlParameter("@PHYCDTN_SEEPAGE", SqlDbType.VarChar)
            param(71).Value = IIf(txtSeepage.Text = "", DBNull.Value, txtSeepage.Text)
            param(72) = New SqlParameter("@PHYCDTN_OTHRTNT", SqlDbType.VarChar)
            param(72).Value = IIf(txtOthrtnt.Text = "", DBNull.Value, txtOthrtnt.Text)
            'Landlord's Scope of work 
            param(73) = New SqlParameter("@LL_VITRIFIED", SqlDbType.VarChar)
            param(73).Value = IIf(ddlvitrified.SelectedValue = "", DBNull.Value, ddlvitrified.SelectedValue)
            param(74) = New SqlParameter("@LL_VITRIFIED_RMKS", SqlDbType.VarChar)
            param(74).Value = IIf(txtVitriRemarks.Text = "", DBNull.Value, txtVitriRemarks.Text)
            param(75) = New SqlParameter("@LL_WASHRMS", SqlDbType.VarChar)
            param(75).Value = IIf(ddlWashroom.SelectedValue = "", DBNull.Value, ddlWashroom.SelectedValue)
            param(76) = New SqlParameter("@LL_WASHRM_RMKS", SqlDbType.VarChar)
            param(76).Value = IIf(txtWashroom.Text = "", DBNull.Value, txtWashroom.Text)
            param(77) = New SqlParameter("@LL_PANTRY", SqlDbType.VarChar)
            param(77).Value = IIf(ddlPantry.SelectedValue = "", DBNull.Value, ddlPantry.SelectedValue)
            param(78) = New SqlParameter("@LL_PANTRY_RMKS", SqlDbType.VarChar)
            param(78).Value = IIf(txtPantry.Text = "", DBNull.Value, txtPantry.Text)
            param(79) = New SqlParameter("@LL_SHUTTER", SqlDbType.VarChar)
            param(79).Value = IIf(ddlShutter.SelectedValue = "", DBNull.Value, ddlShutter.SelectedValue)
            param(80) = New SqlParameter("@LL_SHUTTER_RMKS", SqlDbType.VarChar)
            param(80).Value = IIf(txtShutter.Text = "", DBNull.Value, txtShutter.Text)
            param(81) = New SqlParameter("@LL_OTHERS", SqlDbType.VarChar)
            param(81).Value = IIf(ddlOthers.SelectedValue = "", DBNull.Value, ddlOthers.SelectedValue)
            param(82) = New SqlParameter("@LL_OTHERS_RMKS", SqlDbType.VarChar)
            param(82).Value = IIf(txtOthers.Text = "", DBNull.Value, txtOthers.Text)
            param(83) = New SqlParameter("@LL_WORK_DAYS", SqlDbType.VarChar)
            param(83).Value = IIf(txtllwork.Text = "", DBNull.Value, txtllwork.Text)
            param(84) = New SqlParameter("@LL_ELEC_EXISTING", SqlDbType.VarChar)
            param(84).Value = IIf(txtElectricEx.Text = "", DBNull.Value, txtElectricEx.Text)
            param(85) = New SqlParameter("@LL_ELEC_REQUIRED", SqlDbType.VarChar)
            param(85).Value = IIf(txtElectricRq.Text = "", DBNull.Value, txtElectricRq.Text)
            'Other Details
            param(89) = New SqlParameter("@OTHR_FLOORING", SqlDbType.VarChar)
            param(89).Value = IIf(ddlFlooring.SelectedValue = "", DBNull.Value, ddlFlooring.SelectedValue)
            param(90) = New SqlParameter("@OTHR_FLRG_RMKS", SqlDbType.VarChar)
            param(90).Value = IIf(txtFlooring.Text = "", DBNull.Value, txtFlooring.Text)
            param(91) = New SqlParameter("@OTHR_WASH_EXISTING", SqlDbType.VarChar)
            param(91).Value = IIf(txtWashExisting.Text = "", DBNull.Value, txtWashExisting.Text)
            param(92) = New SqlParameter("@OTHR_WASH_REQUIRED", SqlDbType.VarChar)
            param(92).Value = IIf(txtWashRequirement.Text = "", DBNull.Value, txtWashRequirement.Text)
            param(93) = New SqlParameter("@OTHR_POTABLE_WTR", SqlDbType.VarChar)
            param(93).Value = IIf(ddlPotableWater.SelectedValue = "", DBNull.Value, ddlPotableWater.SelectedValue)
            'Cost Details
            param(94) = New SqlParameter("@COST_RENT", SqlDbType.VarChar)
            param(94).Value = IIf(txtRent.Text = "", DBNull.Value, txtRent.Text)
            param(95) = New SqlParameter("@COST_RENT_SFT", SqlDbType.VarChar)
            param(95).Value = IIf(txtsft.Text = "", DBNull.Value, txtsft.Text)
            param(96) = New SqlParameter("@COST_RATIO", SqlDbType.VarChar)
            param(96).Value = IIf(txtRatio.Text = "", DBNull.Value, txtRatio.Text)
            param(97) = New SqlParameter("@COST_OWN_SHARE", SqlDbType.VarChar)
            param(97).Value = IIf(txtownshare.Text = "", DBNull.Value, txtownshare.Text)
            param(98) = New SqlParameter("@COST_SECDEP", SqlDbType.VarChar)
            param(98).Value = IIf(txtsecdepmonths.Text = "", DBNull.Value, txtsecdepmonths.Text)
            param(99) = New SqlParameter("@COST_GST", SqlDbType.VarChar)
            param(99).Value = IIf(ddlgst.SelectedValue = "", DBNull.Value, ddlgst.SelectedValue)
            param(100) = New SqlParameter("@COST_MAINTENANCE", SqlDbType.VarChar)
            param(100).Value = IIf(txtmaintenance.Text = "", DBNull.Value, txtmaintenance.Text)
            param(101) = New SqlParameter("@COST_ESC", SqlDbType.VarChar)
            param(101).Value = IIf(txtesc.Text = "", DBNull.Value, txtesc.Text)
            param(102) = New SqlParameter("@COST_RENT_FREE", SqlDbType.VarChar)
            param(102).Value = IIf(txtRentFree.Text = "", DBNull.Value, txtRentFree.Text)
            param(103) = New SqlParameter("@COST_STAMP", SqlDbType.VarChar)
            param(103).Value = IIf(ddlStampReg.SelectedValue = "", DBNull.Value, ddlStampReg.SelectedValue)
            param(104) = New SqlParameter("@COST_AGREE", SqlDbType.VarChar)
            param(104).Value = IIf(txtAgree.Text = "", DBNull.Value, txtAgree.Text)
            'DOCUMENTS VAILABLE
            param(105) = New SqlParameter("@DOC_TITLE", SqlDbType.VarChar)
            param(105).Value = IIf(ddlTitle.SelectedValue = "", DBNull.Value, ddlTitle.SelectedValue)
            param(106) = New SqlParameter("@DOC_TITLE_RMKS", SqlDbType.VarChar)
            param(106).Value = IIf(txtTitleRemarks.Text = "", DBNull.Value, txtTitleRemarks.Text)
            param(107) = New SqlParameter("@DOC_OCCUP", SqlDbType.VarChar)
            param(107).Value = IIf(ddlOccupancy.SelectedValue = "", DBNull.Value, ddlOccupancy.SelectedValue)
            param(108) = New SqlParameter("@DOC_OCCUP_RMKS", SqlDbType.VarChar)
            param(108).Value = IIf(txtOccupancyRemarks.Text = "", DBNull.Value, txtOccupancyRemarks.Text)
            param(109) = New SqlParameter("@DOC_BUILD", SqlDbType.VarChar)
            param(109).Value = IIf(ddlBuilding.SelectedValue = "", DBNull.Value, ddlBuilding.SelectedValue)
            param(110) = New SqlParameter("@DOC_BUILD_RMKS", SqlDbType.VarChar)
            param(110).Value = IIf(txtBuildingRemarks.Text = "", DBNull.Value, txtBuildingRemarks.Text)
            param(111) = New SqlParameter("@DOC_PAN", SqlDbType.VarChar)
            param(111).Value = IIf(ddlPAN.SelectedValue = "", DBNull.Value, ddlPAN.SelectedValue)
            param(112) = New SqlParameter("@DOC_PAN_RMKS", SqlDbType.VarChar)
            param(112).Value = IIf(txtPANRemarks.Text = "", DBNull.Value, txtPANRemarks.Text)
            param(86) = New SqlParameter("@DOC_TAX", SqlDbType.VarChar)
            param(86).Value = IIf(ddlTax.SelectedValue = "", DBNull.Value, ddlTax.SelectedValue)
            param(87) = New SqlParameter("@DOC_TAX_RMKS", SqlDbType.VarChar)
            param(87).Value = IIf(txtTaxRemarks.Text = "", DBNull.Value, txtTaxRemarks.Text)
            param(88) = New SqlParameter("@DOC_OTHR_INFO", SqlDbType.VarChar)
            param(88).Value = IIf(txtOtherInfo.Text = "", DBNull.Value, txtOtherInfo.Text)

            param(113) = New SqlParameter("@SIGNAGEIMAGES", SqlDbType.Structured)
            param(113).Value = UtilityService.ConvertToDataTable(Imgclass1)
            param(114) = New SqlParameter("@SIG_LENGTH", SqlDbType.VarChar)
            param(114).Value = IIf(txtsiglength.Text = "", DBNull.Value, txtsiglength.Text)
            param(115) = New SqlParameter("@SIG_WIDTH", SqlDbType.VarChar)
            param(115).Value = IIf(txtsigwidth.Text = "", DBNull.Value, txtsigwidth.Text)
            param(116) = New SqlParameter("@AC_OUTDOOR", SqlDbType.VarChar)
            param(116).Value = IIf(txtACOutdoor.Text = "", DBNull.Value, txtACOutdoor.Text)
            param(117) = New SqlParameter("@GSB", SqlDbType.VarChar)
            param(117).Value = IIf(txtGSB.Text = "", DBNull.Value, txtGSB.Text)
            param(118) = New SqlParameter("@AC_OUTDOOR_IMAGE", SqlDbType.Structured)
            param(118).Value = UtilityService.ConvertToDataTable(Imgclass2)
            param(119) = New SqlParameter("@GSB_IMAGE", SqlDbType.Structured)
            param(119).Value = UtilityService.ConvertToDataTable(Imgclass3)
            param(120) = New SqlParameter("@INSPECTION_DATE", SqlDbType.DateTime)
            param(120).Value = Convert.ToDateTime(txtsdate.Text)
            param(121) = New SqlParameter("@INSPECTION_BY", SqlDbType.VarChar)
            param(121).Value = IIf(txtInspection.Text = "", DBNull.Value, txtInspection.Text)
            param(122) = New SqlParameter("@PHYCDTN_WALLS", SqlDbType.VarChar)
            param(122).Value = IIf(txtWalls.Text = "", DBNull.Value, txtWalls.Text)
            param(123) = New SqlParameter("@PHYCDTN_ROOF", SqlDbType.VarChar)
            param(123).Value = IIf(txtRoof.Text = "", DBNull.Value, txtRoof.Text)

            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_UPDATE_PROPERTY_DETAILS", param)
            If res = "SUCCESS" Then
                lblmsg.Text = "Property updated Successfully"
            Else
                lblmsg.Text = "Something went wrong. Please try again later."
            End If
            Cleardata()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Class ImageClas
        Private _fn As String
        Public Property Filename() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String
        Public Property FilePath() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property

    End Class

    Private Sub BindDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PRPTY_DETAILS")
        sp.Command.AddParameter("@PROP_ID", Request.QueryString("id"), DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            'General Details

            txtReqID.Text = ds.Tables(0).Rows(0).Item("PM_PPT_PM_REQ_ID")
            ddlReqType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_REQ_TYPE")).Selected = True
            'ds.Tables(0).Rows(0).Item("PM_PPT_REQ_TYPE") = ddlReqType.SelectedValue
            ddlPprtNature.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_NATURE")).Selected = True
            ddlAcqThr.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_ACQ_THR")).Selected = True

            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE")).Selected = True
            GetLocationsbyCity(ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE"))
            ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE")).Selected = True
            GetTowerbyLoc(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE"))
            ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE")).Selected = True
            If Not ddlentity.Items.FindByValue(ds.Tables(0).Rows(0).Item("pm_ppt_entity")).Selected = True Then
                ddlentity.Items.FindByValue(ds.Tables(0).Rows(0).Item("pm_ppt_entity")).Selected = True
            End If
            ddlentity.Items.FindByValue(ds.Tables(0).Rows(0).Item("pm_ppt_entity")).Selected = True
            GetFloorsbyTwr(ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE"))

            'For Each item As String In ds.Tables(0).Rows(0).Item("FLOORLIST").ToString().Split(",")
            If ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE").ToString() <> "" Then
                ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE")).Selected = True
            End If

            'ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE")).Selected = True
            'Next


            txtToilet.Text = ds.Tables(0).Rows(0).Item("PM_PPT_TOT_TOI")  'IIf(txtToilet.Text = "", 0, txtToilet.Text)
            ddlPropertyType.SelectedValue = ds.Tables(0).Rows(0).Item("PM_PPT_TYPE")
            txtPropIDName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_NAME")
            If (ds.Tables(0).Rows(0).Item("PM_PPT_ESTD").ToString() <> "") Then
                txtESTD.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ESTD")
            End If
            'txtESTD.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ESTD") 'IIf(txtESTD.Text = "", "", Convert.ToString(txtESTD.Text))
            txtAge.Text = ds.Tables(0).Rows(0).Item("PM_PPT_AGE")   'IIf(txtAge.Text = "", "", Convert.ToString(txtAge.Text))
            txtPropDesc.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ADDRESS")
            txtSignageLocation.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SIGN_LOC")

            txtSocityName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SOC_NAME")
            txtlat.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LAT")
            txtlong.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LONG")
            txtOwnScopeWork.Text = ds.Tables(0).Rows(0).Item("PM_PPT_OWN_SCOPE")
            If (ds.Tables(0).Rows(0).Item("PM_PPT_RECOMMENDED").ToString() = "") Then
                ddlRecommended.SelectedValue = ""
            Else
                ddlRecommended.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_PPT_RECOMMENDED") = True, 1, 0)
            End If
            'ddlRecommended.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_RECOMMENDED")).Selected = True
            If ddlRecommended.SelectedValue = 1 Then
                txtRecmRemarks.Visible = True
                txtRecmRemarks.Text = ds.Tables(0).Rows(0).Item("PM_PPT_RECO_REM")
            Else
                divRecommanded.Visible = False
            End If

            'Owner Details
            txtownrname.Text = ds.Tables(0).Rows(0).Item("PM_OWN_NAME")
            txtphno.Text = ds.Tables(0).Rows(0).Item("PM_OWN_PH_NO")
            txtPrvOwnName.Text = ds.Tables(0).Rows(0).Item("PM_PREV_OWN_NAME")
            txtPrvOwnPhNo.Text = ds.Tables(0).Rows(0).Item("PM_PREV_OWN_PH_NO")

            txtOwnEmail.Text = ds.Tables(0).Rows(0).Item("PM_OWN_EMAIL")

            'Area Details
            txtCarpetArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_CARPET_AREA"))
            txtBuiltupArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_BUA_AREA"))
            txtCommonArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_COM_AREA"))
            txtRentableArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_RENT_AREA"))

            txtUsableArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_USABEL_AREA"))
            txtSuperBulArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_SBU_AREA"))
            txtPlotArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_PLOT_AREA"))
            txtCeilingHight.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_FTC_HIGHT"))

            txtBeamBottomHight.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_FTBB_HIGHT"))
            txtMaxCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_MAX_CAP"))
            txtOptCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_OPT_CAP"))
            txtSeatingCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_SEATING_CAP"))
            If (ds.Tables(0).Rows(0).Item("PM_AR_FLOOR_TYPE").ToString() <> "") Then
                ddlFlooringType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_AR_FLOOR_TYPE")).Selected = True
            End If

            If (ds.Tables(0).Rows(0).Item("PM_AR_FSI").ToString() = "") Then
                ddlFSI.SelectedValue = ""
            Else
                ddlFSI.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_AR_FSI") = True, 1, 0)
            End If

            txtFSI.Text = ds.Tables(0).Rows(0).Item("PM_AR_FSI_RATIO")
            txtEfficiency.Text = ds.Tables(0).Rows(0).Item("PM_AR_PREF_EFF")

            'PURCHASE DETAILS
            txtPurPrice.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_PUR_PRICE"), 2)
            txtPurDate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PUR_DATE"))
            txtMarketValue.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_PUR_MARKET_VALUE"), 2)

            'GOVT DETAILS
            txtIRDA.Text = ds.Tables(0).Rows(0).Item("PM_GOV_IRDA")
            txtPCcode.Text = ds.Tables(0).Rows(0).Item("PM_GOV_PC_CODE")
            txtGovtPropCode.Text = ds.Tables(0).Rows(0).Item("PM_GOV_PROP_CODE")
            txtUOM_CODE.Text = ds.Tables(0).Rows(0).Item("PM_GOV_UOM_CODE")

            'INSURANCE DETAILS
            If (ds.Tables(0).Rows(0).Item("PM_INS_TYPE").ToString() <> "") Then
                ddlInsuranceType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_INS_TYPE")).Selected = True
            End If

            txtInsuranceVendor.Text = ds.Tables(0).Rows(0).Item("PM_INS_VENDOR")
            txtInsuranceAmt.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_INS_AMOUNT"), 2)
            txtInsurancePolNum.Text = ds.Tables(0).Rows(0).Item("PM_INS_PNO")

            If (ds.Tables(0).Rows(0).Item("PM_INS_START_DT").ToString() <> "") Then
                txtInsuranceStartdate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_INS_START_DT"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_INS_END_DT").ToString() <> "") Then
                txtInsuranceEnddate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_INS_END_DT"))
            End If
            txtInspection.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_BY")
            If (ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT").ToString() <> "") Then
                txtsdate.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT")
            End If

            If (ds.Tables(0).Rows(0).Item("PM_OWN_LANDLINE").ToString() <> "") Then
                txtllno.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OWN_LANDLINE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_POA").ToString() = "") Then
                ddlAgreementbyPOA.SelectedValue = "--Select--"
            Else
                ddlAgreementbyPOA.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_POA")).Selected = True
            End If
            'physical condition
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_WALLS").ToString() <> "") Then
                txtWalls.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_WALLS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_ROOF").ToString() <> "") Then
                txtRoof.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_ROOF"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_CEILING").ToString() <> "") Then
                txtCeiling.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_CEILING"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_WINDOWS").ToString() <> "") Then
                txtwindows.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_WINDOWS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_DAMAGE").ToString() <> "") Then
                txtDamage.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_DAMAGE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_SEEPAGE").ToString() <> "") Then
                txtSeepage.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_SEEPAGE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_OTHR_TNT").ToString() <> "") Then
                txtOthrtnt.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_OTHR_TNT"))
            End If


            'LANDLORD SCOPE OF WORK
            If (ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED").ToString() = "") Then
                ddlvitrified.SelectedValue = "--Select--"
            Else
                ddlvitrified.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED")).Selected = True
            End If
            txtVitriRemarks.Text = ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED_RMKS")
            If (ds.Tables(0).Rows(0).Item("PM_LLS_WASHROOMS").ToString() = "") Then
                ddlWashroom.SelectedValue = "--Select--"
            Else
                ddlWashroom.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_WASHROOMS")).Selected = True
            End If
            txtWashroom.Text = ds.Tables(0).Rows(0).Item("PM_LLS_WASH_RMKS")
            If (ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY").ToString() = "") Then
                ddlPantry.SelectedValue = "--Select--"
            Else
                ddlPantry.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY")).Selected = True
            End If
            txtPantry.Text = ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY_RMKS")
            If (ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER").ToString() = "") Then
                ddlShutter.SelectedValue = "--Select--"
            Else
                ddlShutter.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER")).Selected = True
            End If
            txtShutter.Text = ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER_RMKS")
            If (ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS").ToString() = "") Then
                ddlOthers.SelectedValue = "--Select--"
            Else
                ddlOthers.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS_RMKS").ToString() <> "") Then
                txtOthers.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS_RMKS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_WORK_DAYS").ToString() <> "") Then
                txtllwork.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_LLS_WORK_DAYS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_EXISTING").ToString() <> "") Then
                txtElectricEx.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_EXISTING"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_REQUIRED").ToString() <> "") Then
                txtElectricRq.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_REQUIRED"))
            End If
            'COST DETAILS
            If (ds.Tables(0).Rows(0).Item("PM_COST_RENT").ToString() <> "") Then
                txtRent.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_RENT"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_RENT_SFT").ToString() <> "") Then
                txtsft.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_RENT_SFT"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_RATIO").ToString() <> "") Then
                txtRatio.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_RATIO"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_OWN_SHARE").ToString() <> "") Then
                txtownshare.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_OWN_SHARE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_SECDEPOSIT").ToString() <> "") Then
                txtsecdepmonths.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_SECDEPOSIT"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_COST_GST").ToString() = "") Then
                ddlgst.SelectedValue = "--Select--"
            Else
                ddlgst.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_COST_GST")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_MAINTENANCE").ToString() <> "") Then
                txtmaintenance.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_MAINTENANCE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_ESC_RENTALS").ToString() <> "") Then
                txtesc.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_ESC_RENTALS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_RENT_FREE").ToString() <> "") Then
                txtRentFree.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_RENT_FREE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_STAMP").ToString() = "") Then
                ddlStampReg.SelectedValue = "--Select--"
            Else
                ddlStampReg.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_COST_STAMP")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_AGREEMENT_PERIOD").ToString() <> "") Then
                txtAgree.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_AGREEMENT_PERIOD"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_FLOORING").ToString() = "") Then
                ddlFlooring.SelectedValue = "--Select--"
            Else
                ddlFlooring.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_OTHR_FLOORING")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_FLRG_RMKS").ToString() <> "") Then
                txtFlooring.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_FLRG_RMKS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_WASHRM_EXISTING").ToString() <> "") Then
                txtWashExisting.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_WASHRM_EXISTING"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_WASHRM_REQUIRED").ToString() <> "") Then
                txtWashRequirement.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_WASHRM_REQUIRED"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_POTABLE_WTR").ToString() = "") Then
                ddlPotableWater.SelectedValue = "--Select--"
            Else
                ddlPotableWater.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_OTHR_POTABLE_WTR")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_SIG_LENGTH").ToString() <> "") Then
                txtsiglength.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_SIG_LENGTH"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_SIG_WIDTH").ToString() <> "") Then
                txtsigwidth.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_SIG_WIDTH"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_AC_OUTDOOR").ToString() <> "") Then
                txtACOutdoor.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_AC_OUTDOOR"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_GSB").ToString() <> "") Then
                txtGSB.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_GSB"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_TITLE").ToString() = "") Then
                ddlTitle.SelectedValue = "--Select--"
            Else
                ddlTitle.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_TITLE")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_TITLLE_RMKS").ToString() <> "") Then
                txtTitleRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_TITLLE_RMKS"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_DOC_OCCUPANCY").ToString() = "") Then
                ddlOccupancy.SelectedValue = "--Select--"
            Else
                ddlOccupancy.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_OCCUPANCY")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_OCC_RMKS").ToString() <> "") Then
                txtOccupancyRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_OCC_RMKS"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_DOC_BUILD").ToString() = "") Then
                ddlBuilding.SelectedValue = "--Select--"
            Else
                ddlBuilding.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_BUILD")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_OCC_RMKS").ToString() <> "") Then
                txtBuildingRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_BUILD_RMKS"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_DOC_PAN").ToString() = "") Then
                ddlPAN.SelectedValue = "--Select--"
            Else
                ddlPAN.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_PAN")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_PAN_RMKS").ToString() <> "") Then
                txtPANRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_PAN_RMKS"))
            End If


            If (ds.Tables(0).Rows(0).Item("PM_DOC_TAX").ToString() = "") Then
                ddlTax.SelectedValue = "--Select--"
            Else
                ddlTax.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_TAX")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_TAX_RMKS").ToString() <> "") Then
                txtTaxRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_TAX_RMKS"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_DOC_OTHER_INFO").ToString() <> "") Then
                txtOtherInfo.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_OTHER_INFO"))
            End If



            '   If ds.Tables(1).Rows.Count > 0 Then
            gvPropdocs.DataSource = ds.Tables(1)
            gvPropdocs.DataBind()
            'End If

        End If
    End Sub

    Protected Sub gvPropdocs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPropdocs.RowCommand
        Try
            If e.CommandName = "Download" Then
                Response.ContentType = ContentType
                Dim imgPath As String = "../../images/Property_Images/" + e.CommandArgument
                Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(imgPath)))
                Response.WriteFile(imgPath)
                Response.End()
            Else
                'DELETE
                Dim gvr As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                'gvPropdocs.DeleteRow(gvr.RowIndex)
                gvPropdocs.Rows(gvr.RowIndex).Visible = False
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_DELETE_DOCS")
                sp.Command.AddParameter("@DOC_ID", e.CommandArgument, DbType.String)
                sp.Command.AddParameter("@TYPE", "PROPERTY", DbType.String)
                sp.ExecuteScalar()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvPropdocs_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvPropdocs.RowDeleting

    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/PropertyManagement/Views/frmEditPropertyDetails.aspx?back=" + txtReqID.Text)
    End Sub
End Class
