Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmSurrenderLease
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet
    Dim RentRevision As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then
            scriptManager.RegisterPostBackControl(btnApprove)
            'scriptManager.RegisterPostBackControl(DocLink)
            ''scriptManager.RegisterPostBackControl(Documentsbindinggrid)
            scriptManager.RegisterPostBackControl(grdDocs)
            scriptManager.RegisterPostBackControl(btnReject)

        End If
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            BindGrid()
            panel.Visible = False
            panel2.Visible = False
            panel3.Visible = False
            PaymentMode()
            ''BindGridSurrenderDocuments()
            ''BindDocuments()
        End If
        txtTdate.Attributes.Add("readonly", "readonly")

    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SURRENDER_LEASE_APPROVE_GRID")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        Dim ds As DataSet
        ds = sp.GetDataSet()
        ViewState("reqDetails") = ds
        gvItems.DataSource = ViewState("reqDetails")
        gvItems.DataBind()

    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        gvItems.DataSource = ViewState("reqDetails")
        gvItems.DataBind()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "Surrender" Then

            Dim lnkSurrender As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkSurrender.NamingContainer, GridViewRow)
            Dim lblLseName As Label = DirectCast(gvRow.FindControl("lblLeaseName"), Label)
            Dim lblLseSno As Label = DirectCast(gvRow.FindControl("lblsno"), Label)
            txtLeaseId.Text = lblLseName.Text
            txtlblsno.Text = lblLseSno.Text


            Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SURRENDER_LEASE_FILL_DETAILS")
            SP.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            SP.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
            SP.Command.AddParameter("@LEASE_ID", lblLseName.Text, DbType.String)
            SP.Command.AddParameter("@LEASE_SNO", txtlblsno.Text, DbType.String)
            Dim dr As SqlDataReader = SP.GetReader()
            If dr.Read() Then

                txtsurrender.Text = dr("SURRENDER_DT")
                txtrefund.Text = dr("PM_SL_REFUND_DT")
                txtAmtPaidLesse.Text = dr("LESSE_AMT")
                txtAmtPaidLessor.Text = dr("LESSOR_AMT")
                txtSDamount.Text = dr("SECURITY_AMT")
                txtOutAmt.Text = dr("OUT_AMT")
                txtDmg.Text = dr("PM_SL_DAMAGES")
                txtDmgAmt.Text = dr("DAMAGE_AMT")
                ''DocLink.Text = dr("DOC_NAME")
                txttotamount.Text = dr("pm_les_basic_rent")


            End If
            panel.Visible = True

            BindDocuments()

        Else
            panel.Visible = False

        End If

    End Sub

    'Private Sub BindGridSurrenderDocuments()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[PM_SURRENDER_LEASE_Documents]")
    '    sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
    '    sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
    '    Dim ds As DataSet
    '    ds = sp.GetDataSet()
    '    ViewState("reqDetails1") = ds
    '    Documentsbindinggrid.DataSource = ViewState("reqDetails1")
    '    Documentsbindinggrid.DataBind()

    'End Sub
    'Protected Sub Documentsbindinggrid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Documentsbindinggrid.PageIndexChanging
    '    Documentsbindinggrid.PageIndex = e.NewPageIndex()
    '    Documentsbindinggrid.DataSource = ViewState("reqDetails1")
    '    Documentsbindinggrid.DataBind()
    'End Sub
    'Protected Sub Documentsbindinggrid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Documentsbindinggrid.RowCommand
    '    If e.CommandName = "Document" Then

    '        Dim lnkSurrender As LinkButton = DirectCast(e.CommandSource, LinkButton)
    '        Dim Documentsbindinggrid As GridViewRow = DirectCast(lnkSurrender.NamingContainer, GridViewRow)
    '        Dim lblLseName As Label = DirectCast(Documentsbindinggrid.FindControl("lblLeaseid"), Label)

    '        Dim orgfilename As String = lnkSurrender.Text
    '        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & orgfilename
    '        If filePath <> "" Then
    '            ''Dim path As String = Server.MapPath(filePath)
    '            Dim file As System.IO.FileInfo = New System.IO.FileInfo(filePath)

    '            If file.Exists Then
    '                Response.Clear()
    '                Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
    '                Response.AddHeader("Content-Length", file.Length.ToString())
    '                Response.ContentType = "application/octet-stream"
    '                Response.WriteFile(file.FullName)
    '                Response.[End]()
    '            Else
    '                Response.Write("This file does not exist.")
    '            End If
    '        End If
    '    End If

    'End Sub
    Protected Sub ddlPaymentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub PaymentMode()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PAYMENT_MODE")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlPaymentType.DataSource = sp.GetDataSet()
        ddlPaymentType.DataTextField = "NAME"
        ddlPaymentType.DataValueField = "CODE"
        ddlPaymentType.DataBind()
        ddlPaymentType.Items.Insert(0, "--Select--")
    End Sub

    'Private Sub DocLink_Click(sender As Object, e As EventArgs) Handles DocLink.Click
    '    Dim orgfilename As String = DocLink.Text
    '    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & orgfilename
    '    If filePath <> "" Then
    '        ''Dim path As String = Server.MapPath(filePath)
    '        Dim file As System.IO.FileInfo = New System.IO.FileInfo(filePath)

    '        If file.Exists Then
    '            Response.Clear()
    '            Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
    '            Response.AddHeader("Content-Length", file.Length.ToString())
    '            Response.ContentType = "application/octet-stream"
    '            Response.WriteFile(file.FullName)
    '            Response.[End]()
    '        Else
    '            Response.Write("This file does not exist.")
    '        End If
    '    End If
    'End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SURRENDER_LEASE_REJECT")
            sp.Command.AddParameter("@LEASE_ID", txtLeaseId.Text, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
            sp.Command.AddParameter("@LEASE_SNO", txtlblsno.Text, DbType.String)
            sp.ExecuteScalar()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        BindGrid()
        panel.Visible = False
        lblMsg.Visible = True
        lblMsg.Text = "Surrender Lease Rejected Successfully"

    End Sub
    Public Class ImageClas
        Private _fn As String
        Public Property Filename() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String
        Public Property FilePath() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property

    End Class
    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Dim orgfilename As String = BrowsePossLtr.FileName
        Dim filename As String = ""

        'If orgfilename = "" Then
        '    filename = DocLink.Text
        'End If


        'Try
        '    If (BrowsePossLtr.HasFile) Then
        '        Dim fileExt As String
        '        fileExt = System.IO.Path.GetExtension(BrowsePossLtr.FileName)
        '        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & orgfilename
        '        BrowsePossLtr.PostedFile.SaveAs(filePath)
        '        filename = orgfilename
        '        'lblMsg.Visible = True
        '        'lblMsg.Text = "File upload successfully"

        '    End If
        'Catch ex As Exception
        '    Response.Write(ex.Message)
        'End Try
        Dim SurrenderList As List(Of ImageClas) = New List(Of ImageClas)
        Dim IC As ImageClas
        Dim i As Int32 = 0
        Dim fileSize As Int64 = 0
        Try
            If BrowsePossLtr.PostedFiles IsNot Nothing Then
                Dim count = BrowsePossLtr.PostedFiles.Count
                While (i < count)
                    fileSize = BrowsePossLtr.PostedFiles(i).ContentLength + fileSize
                    i = i + 1
                End While
                If (fileSize > 20971520) Then
                    lblMsg.Text = "Upload files size could not be greater than 20 MB."
                    Exit Sub
                End If
                For Each File In BrowsePossLtr.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC = New ImageClas()
                        IC.Filename = File.FileName
                        IC.FilePath = Upload_Time & "_" & File.FileName
                        SurrenderList.Add(IC)

                    End If
                Next
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            Dim param As SqlParameter() = New SqlParameter(13) {}
            param(0) = New SqlParameter("@LEASE_ID", DbType.String)
            param(0).Value = txtLeaseId.Text
            param(1) = New SqlParameter("@APPRV_REMARKS", DbType.String)
            param(1).Value = txtRemarks.Text
            param(2) = New SqlParameter("@PAY_MODE", DbType.String)
            param(2).Value = ddlPaymentType.SelectedItem.Text
            param(3) = New SqlParameter("@PAY_DATE", DbType.Date)
            param(3).Value = txtTdate.Text
            param(4) = New SqlParameter("@AUR_ID", DbType.String)
            param(4).Value = Session("UID")
            param(5) = New SqlParameter("@COMPANYID", DbType.Int32)
            param(5).Value = Session("COMPANYID")
            param(6) = New SqlParameter("@LEASE_SNO", DbType.String)
            param(6).Value = txtlblsno.Text
            If ddlPaymentType.SelectedItem.Value = "2" Then
                param(7) = New SqlParameter("@ACC_SERIAL_NO", DbType.Decimal)
                param(7).Value = txtAccNo.Text
                param(8) = New SqlParameter("@BANK_NAME", DbType.String)
                param(8).Value = txtBankName.Text
                param(9) = New SqlParameter("@CHEQUENO", DbType.String)
                param(9).Value = txtcheqno.Text
                param(10) = New SqlParameter("@CHEQUEDT", DbType.String)
                param(10).Value = txtchedddt.Text
                param(11) = New SqlParameter("@BRANCH", DbType.String)
                param(11).Value = ""
                param(12) = New SqlParameter("@IFSC", DbType.String)
                param(12).Value = ""
            ElseIf ddlPaymentType.SelectedItem.Value = "3" Then
                param(7) = New SqlParameter("@ACC_SERIAL_NO", DbType.Decimal)
                param(7).Value = txtAccNo.Text
                param(8) = New SqlParameter("@BANK_NAME", DbType.String)
                param(8).Value = txtBankName.Text
                param(9) = New SqlParameter("@CHEQUENO", DbType.String)
                param(9).Value = ""
                param(10) = New SqlParameter("@CHEQUEDT", DbType.String)
                param(10).Value = ""
                param(11) = New SqlParameter("@BRANCH", DbType.String)
                param(11).Value = txtbrnch.Text
                param(12) = New SqlParameter("@IFSC", DbType.String)
                param(12).Value = txtIFSC.Text
            ElseIf ddlPaymentType.SelectedItem.Value = "4" Then
                param(7) = New SqlParameter("@ACC_SERIAL_NO", DbType.Decimal)
                param(7).Value = txtAccNo.Text
                param(8) = New SqlParameter("@BANK_NAME", DbType.String)
                param(8).Value = txtBankName.Text
                param(9) = New SqlParameter("@CHEQUENO", DbType.String)
                param(9).Value = txtcheqno.Text
                param(10) = New SqlParameter("@CHEQUEDT", DbType.String)
                param(10).Value = txtchedddt.Text
                param(11) = New SqlParameter("@BRANCH", DbType.String)
                param(11).Value = ""
                param(12) = New SqlParameter("@IFSC", DbType.String)
                param(12).Value = ""
            Else
                param(7) = New SqlParameter("@ACC_SERIAL_NO", DbType.Decimal)
                param(7).Value = ""
                param(8) = New SqlParameter("@BANK_NAME", DbType.String)
                param(8).Value = ""
                param(9) = New SqlParameter("@CHEQUENO", DbType.String)
                param(9).Value = ""
                param(10) = New SqlParameter("@CHEQUEDT", DbType.String)
                param(10).Value = ""
                param(11) = New SqlParameter("@BRANCH", DbType.String)
                param(11).Value = ""
                param(12) = New SqlParameter("@IFSC", DbType.String)
                param(12).Value = ""
            End If
            param(13) = New SqlParameter("@DOC_NAME", SqlDbType.Structured)
            param(13).Value = UtilityService.ConvertToDataTable(SurrenderList)
            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_SURRENDER_LEASE_APPROVE", param)
            'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SURRENDER_LEASE_APPROVE")
            'sp.Command.AddParameter("@LEASE_ID", txtLeaseId.Text, DbType.String)
            'sp.Command.AddParameter("@APPRV_REMARKS", txtRemarks.Text, DbType.String)
            'sp.Command.AddParameter("@PAY_MODE", ddlPaymentType.SelectedItem.Text, DbType.String)
            'sp.Command.AddParameter("@PAY_DATE", txtTdate.Text, DbType.Date)
            'sp.Command.AddParameter("@DOC_NAME", filename, DbType.String)
            'sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            'sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
            'sp.Command.AddParameter("@LEASE_SNO", txtlblsno.Text, DbType.String)
            'If ddlPaymentType.SelectedItem.Value = "2" Then
            '    sp.Command.AddParameter("@ACC_SERIAL_NO", txtAccNo.Text, DbType.String)
            '    sp.Command.AddParameter("@BANK_NAME", txtBankName.Text, DbType.String)
            '    sp.Command.AddParameter("@CHEQUENO", txtcheqno.Text, DbType.String)
            '    sp.Command.AddParameter("@CHEQUEDT", txtchedddt.Text, DbType.Date)
            '    sp.Command.AddParameter("@BRANCH", "", DbType.String)
            '    sp.Command.AddParameter("@IFSC", "", DbType.String)
            'ElseIf ddlPaymentType.SelectedItem.Value = "3" Then
            '    sp.Command.AddParameter("@ACC_SERIAL_NO", txtAccTwo.Text, DbType.String)
            '    sp.Command.AddParameter("@BANK_NAME", txtBankTwo.Text, DbType.String)
            '    'sp.Command.AddParameter("@CHEQUEDT", txtchedddt.Text, DbType.Date)
            '    sp.Command.AddParameter("@BRANCH", txtbrnch.Text, DbType.String)
            '    sp.Command.AddParameter("@IFSC", txtIFSC.Text, DbType.String)
            'ElseIf ddlPaymentType.SelectedItem.Value = "4" Then
            '    sp.Command.AddParameter("@ACC_SERIAL_NO", txtAccNo.Text, DbType.String)
            '    sp.Command.AddParameter("@BANK_NAME", txtBankName.Text, DbType.String)
            '    sp.Command.AddParameter("@CHEQUENO", txtcheqno.Text, DbType.String)
            '    sp.Command.AddParameter("@CHEQUEDT", txtchedddt.Text, DbType.Date)
            '    sp.Command.AddParameter("@BRANCH", "", DbType.String)
            '    sp.Command.AddParameter("@IFSC", "", DbType.String)
            'End If
            'sp.ExecuteScalar()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        BindGrid()
        panel.Visible = False
        lblMsg.Visible = True
        disply.Visible = False
        lblMsg.Text = "Surrender Lease Approved Successfully"

    End Sub

    Protected Sub ddlPaymentType_SelectedIndexChanged1(sender As Object, e As EventArgs) Handles ddlPaymentType.SelectedIndexChanged
        If ddlPaymentType.SelectedIndex > 0 Then
            If ddlPaymentType.SelectedItem.Value = "1" Then
                panel2.Visible = False
                panel3.Visible = False
            ElseIf ddlPaymentType.SelectedItem.Value = "2" Then
                panel2.Visible = True
                panel3.Visible = False
            ElseIf ddlPaymentType.SelectedItem.Value = "3" Then
                panel2.Visible = False
                panel3.Visible = True
            ElseIf ddlPaymentType.SelectedItem.Value = "4" Then
                panel2.Visible = True
                panel3.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnsearch_Click(sender As Object, e As EventArgs) Handles btnsearch.Click
        Try
            fillgridOnSearch()
            panel.Visible = False
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub fillgridOnSearch()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SURRENDER_LEASE_APPROVE_GRID_BY_SEARCH")
            sp2.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            sp2.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
            sp2.Command.AddParameter("@SEARCH", txtReqId.Text, DbType.String)
            Dim ds As DataSet
            ds = sp2.GetDataSet()
            ViewState("reqDetails") = ds
            gvItems.DataSource = ViewState("reqDetails")
            gvItems.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtReqId.Text = ""
        BindGrid()
        panel.Visible = False
    End Sub

    Public Sub BindDocuments()
        Dim dtDocs As New DataTable("Documents")
        param = New SqlParameter(1) {}
        param(0) = New SqlParameter("@PM_SL_SNO", DbType.String)
        param(0).Value = txtlblsno.Text
        param(1) = New SqlParameter("@COMPANYID", DbType.Int32)
        param(1).Value = Session("COMPANYID")
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("GET_SURRENDER_Documents", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdDocs.DataSource = ds
            grdDocs.DataBind()
            tblGridDocs.Visible = True
            lblDocsMsg.Text = ""
            disply.Visible = True
        Else
            disply.Visible = True
            tblGridDocs.Visible = False
            lblMsg.Visible = True
            lblDocsMsg.Text = "No Documents Available"
        End If
        dtDocs = Nothing
    End Sub

    Private Sub grdDocs_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.DeleteCommand
        If grdDocs.EditItemIndex = -1 Then
            Dim Bid As Integer
            Bid = grdDocs.DataKeys(e.Item.ItemIndex)
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@pm_sl_sno", SqlDbType.NVarChar, 50)
            param(0).Value = Bid
            ObjSubSonic.GetSubSonicDataSet("DELETE_SURRENDER_Documents", param)
            BindDocuments()
        End If
    End Sub

    Private Sub grdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.ItemCommand
        If e.CommandName = "Download" Then
            Dim pm_ldoc_sno = grdDocs.DataKeys(e.Item.ItemIndex)
            e.Item.BackColor = Drawing.Color.LightSteelBlue
            Dim filePath As String
            filePath = grdDocs.Items(e.Item.ItemIndex).Cells(1).Text
            filePath = "~\UploadFiles\" & filePath
            Response.ContentType = "application/CSV"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "~\UploadFiles", "") & """")
            Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If
    End Sub

End Class
