﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewLeasePayments.aspx.cs" Inherits="PropertyManagement_Views_ViewLeasePayments" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <script src="../../../../Scripts/moment.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="form-group" style="padding-top: 10px;">
            <asp:GridView ID="gvLandlordPay" runat="server" EmptyDataText="No Payments Done."
                AllowSorting="false" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped" AlternatingRowStyle-CssClass="alt"
                 AllowPaging="true" PageSize="5" OnPageIndexChanging="gvLandlordPay_PageIndexChanging">
                <PagerSettings Mode="NumericFirstLast" />
                <Columns>
                    <asp:TemplateField HeaderText="Request Id">
                        <ItemTemplate>
                            <asp:Label ID="lblPropType" runat="server" Text='<%#Eval("LEASE_ID")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location">
                        <ItemTemplate>
                            <asp:Label ID="lblProplcm" runat="server" Text='<%#Eval("LCM_NAME")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Landlord Name">
                        <ItemTemplate>
                            <asp:Label ID="lblllname" runat="server" Text='<%#Eval("LL_NAME")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Total Landlord Rent">
                        <ItemTemplate>
                            <asp:Label ID="lblProprent" runat="server" Text='<%#Eval("PM_LL_MON_RENT_PAYABLE")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="WithHold Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblPropwthd" runat="server" Text='<%#Eval("PM_LPD_LL_WITHHOLD_AMT")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Due Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblPropdue" runat="server" Text='<%#Eval("PM_LPD_LL_DUE_AMT")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Month">
                        <ItemTemplate>
                            <asp:Label ID="lblPropCode" runat="server" Text='<%#Eval("MONTHS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                   
                    <asp:TemplateField HeaderText="Year">
                        <ItemTemplate>
                            <asp:Label ID="lblProptear" runat="server" Text='<%#Eval("Year")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="STATUS">
                        <ItemTemplate>
                            <asp:Label ID="lblProsstats" runat="server" Text='<%#Eval("STATUS")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
                <HeaderStyle />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </form>
     <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
