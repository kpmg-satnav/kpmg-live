Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmViewTenantDetails
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Flag As Integer
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If ddlproptype.SelectedIndex > 0 And ddlCity.SelectedIndex > 0 Then
            gvPropType.Visible = True
            fillgrid(3)
            GetTenantSearchDetails(3)
            If gvPropType.Rows.Count > 0 Then
                btnexporttoexcel.Enabled = True
                btnexporttoexcel.Visible = True
            Else
                btnexporttoexcel.Enabled = False
            End If
        Else
            gvPropType.Visible = False
        End If

    End Sub
    Public Sub GetTenantSearchDetails(Flag)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_TENANT_DETAILS")
        sp2.Command.AddParameter("@FLAG", Flag, DbType.Int32)
        sp2.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedValue, DbType.String)
        sp2.Command.AddParameter("@BDG_ADM_CODE", ddlCity.SelectedValue, DbType.String)
        sp2.Command.AddParameter("@PROPERTY_TYPE", ddlproptype.SelectedValue, DbType.String)
        Session("dataset") = sp2.GetDataSet()
        gvPropType.DataSource = Session("dataset")
        gvPropType.DataBind()
        gvPropType.PageIndex = Convert.ToInt32(Session("CurrentPageIndex"))
        LBTN1.Visible = False
    End Sub
    Public Sub fillgrid(Flag)
        Try
            Val = "B"
            Session("value") = Val
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_TENANT_DETAILS")
            sp2.Command.AddParameter("@FLAG", Flag, DbType.Int32)
            sp2.Command.AddParameter("@COMPANY", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            Session("dataset") = sp2.GetDataSet()
            gvPropType.DataSource = Session("dataset")
            gvPropType.DataBind()
            gvPropType.PageIndex = Convert.ToInt32(Session("CurrentPageIndex"))
            LBTN1.Visible = False
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
           ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            BindPropType()
            BindCity()
            Session("CurrentPageIndex") = 0
            fillgrid(1)
            ddlLocation.Items.Insert(0, New ListItem("Select Location", "0"))
        End If
    End Sub
    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_TENANT_CITY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@USR_ID", Session("UID"), DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("Select City", "0"))
        LBTN1.Visible = False
        btnexporttoexcel.Enabled = False
        'tab.Visible = False
    End Sub
    Private Sub BindCityLoc()
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
            sp1.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
            sp1.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
            ddlLocation.DataSource = sp1.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("Select Location", "0"))

        Catch ex As Exception

        End Try

    End Sub
    Private Sub BindPropType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("Select Property Type", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            fillgridOnTenantCodeSearch(2)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        btnexporttoexcel.Enabled = True

    End Sub
    Dim Val As String
    Private Sub SearchFn(Val)
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_TENANT_DETAILS")
            sp2.Command.AddParameter("@TEN_CODE", txtSearch.Text, DbType.String)
            sp2.Command.AddParameter("@BDG_ADM_CODE", String.Empty, DbType.String)
            sp2.Command.AddParameter("@PN_NAME", String.Empty, DbType.String)
            sp2.Command.AddParameter("@PROPERTY_TYPE", String.Empty, DbType.String)
            sp2.Command.AddParameter("@USER", Session("uid"), DbType.String)
            sp2.Command.AddParameter("@GET_ALL_REC", Val, DbType.String)
            sp2.Command.AddParameter("@FLAG", 3, DbType.Int32)
            gvPropType.Visible = True
            gvPropType.DataSource = sp2.GetDataSet()
            gvPropType.DataBind()
            gvPropType.PageIndex = Convert.ToInt32(Session("CurrentPageIndex"))
            LBTN1.Visible = True
            If gvPropType.Rows.Count <> 0 Then
                btnexporttoexcel.Visible = True
            Else
                btnexporttoexcel.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub fillgridOnTenantCodeSearch(Flag)
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_TENANT_DETAILS")
            sp2.Command.AddParameter("@FLAG", Flag, DbType.Int32)
            sp2.Command.AddParameter("@TEN_CODE", txtSearch.Text, DbType.String)
            Session("dataset") = sp2.GetDataSet()
            gvPropType.DataSource = Session("dataset")
            gvPropType.DataBind()
            'gvPropType.DataSource = sp2.GetDataSet()
            'gvPropType.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub gvPropType_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPropType.PageIndexChanging
        gvPropType.PageIndex = e.NewPageIndex()
        gvPropType.DataSource = Session("dataset")
        gvPropType.DataBind()
        'gvPropType.PageIndex = e.NewPageIndex
        'SearchFn(Session("value").ToString())
        'Session("CurrentPageIndex") = e.NewPageIndex
        ' tab.Visible = True
    End Sub
    Protected Sub LBTN1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBTN1.Click
        gvPropType.Visible = False
        If Not IsPostBack Then
            gvPropType.PageIndex = 0
        End If
        gvPropType.Visible = True
        'fillgrid()
        txtSearch.Text = String.Empty
        Val = "A"
        Session("value") = Val
        SearchFn(Val)
        gvPropType.Visible = True
        ' tab.Visible = True
        'txtSearch.Text = ""
        btnexporttoexcel.Enabled = True
    End Sub
    Protected Sub btnexporttoexcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexporttoexcel.Click
        exportpanel1.ExportType = ControlFreak.ExportPanel.AppType.Excel
        exportpanel1.FileName = "Tenant Details Report"
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        Try
            gvPropType.Visible = False
            btnexporttoexcel.Visible = False
            txtSearch.Text = String.Empty
            If ddlCity.SelectedIndex > 0 And ddlproptype.SelectedIndex > 0 Then
                BindCityLoc()
                'BindProp()
                gvPropType.DataSource = Nothing
                gvPropType.DataBind()
            Else
                'ddlProp.Items.Clear()
                'ddlProp.Items.Insert(0, New ListItem("--Select--", "0"))
            End If


        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlproptype.SelectedIndex > 0 And ddlCity.SelectedIndex > 0 Then
            'BindProp()
            gvPropType.DataSource = Nothing
            gvPropType.DataBind()

        Else
            'ddlProp.Items.Clear()
            'ddlProp.Items.Insert(0, New ListItem("--Select--", "0"))
        End If
    End Sub
    Protected Sub ddlproptype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ' txtSearch.Text = String.Empty
        'btnexporttoexcel.Visible = False
        'gvPropType.Visible = False

    End Sub
    Protected Sub gvPropType_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvPropType.RowDeleting

    End Sub
    Public Sub Tenant_Rowcommand(ByVal intStatus As Integer, ByVal Serial As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_UPDATE_TENANT_STATUS")
        If intStatus = 1 Then
            intStatus = 0
        ElseIf intStatus = 0 Then
            intStatus = 1
        End If
        sp2.Command.AddParameter("@STATUS", intStatus, DbType.Int32)
        sp2.Command.AddParameter("@SNO", Serial, DbType.Int32)
        sp2.ExecuteScalar()
        If (Session("value").ToString() = "B") Then
            fillgrid(1)
        Else
            SearchFn(Session("value").ToString())
        End If
    End Sub
    Protected Sub lnkStauts_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim lnk As LinkButton = CType(sender, LinkButton)
        Dim Status As String = lnk.Text
        If Status = "Active" Then
            Status = 1
        Else
            Status = 0
        End If
        Dim StatusInt As Integer = Status
        Dim intStatus As Integer = StatusInt
        Dim strDept As String = lnk.CommandArgument
        Try
            Tenant_Rowcommand(intStatus, strDept)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASDepartment", "lnkStauts_Click", exp)
        End Try
    End Sub
End Class
