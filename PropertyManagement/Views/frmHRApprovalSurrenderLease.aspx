<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmHRApprovalSurrenderLease.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmSurrenderLease" Title="Approve Surrender Lease" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
        function showPopWin(id) {
            $("#modelcontainer").load("frmViewLeaseDetailsuser.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
                $("#myModal").modal().fadeIn();
            });
        }
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="View Or Modify Maintenance Contract" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Approve Surrender Lease</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel runat="server" ID="upfelren">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label"></label>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtstore" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 10px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-10 control-label">Search By Lease ID / Property Name / Code / City / Location <span style="color: red;">*</span></label>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                                        Display="none" ErrorMessage="Please Search By Property / Lease / Location Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:Button ID="btnsearch" CssClass="btn btn-default btn-primary" runat="server" Text="Search" ValidationGroup="Val1"
                                                        CausesValidation="true" TabIndex="2" />
                                                    <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reset" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 10px">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" SelectedRowStyle-VerticalAlign="Top"
                                            AllowPaging="True" PageSize="5" EmptyDataText=" No Surrender Records Found" CssClass="table table-condensed table-bordered table-hover table-striped"
                                            Style="font-size: 12px;">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Lease Name" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLeaseName" runat="server" Text='<%#Eval("LEASE_ID")%>'></asp:Label>
                                                        <asp:Label ID="lblsno" runat="server" Text='<%#Eval("PM_LES_SNO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Id">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lblLseName" runat="server" Text='<%#Eval("LEASE_ID")%>' CommandName="Surrender"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Property Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPC" runat="server" Text='<%#Eval("PM_PPT_CODE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Property Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPN" runat="server" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="City">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCity" runat="server" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLCM" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Start Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLsdate" runat="server" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Expiry Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLedate" runat="server" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Surrender Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstatitle" runat="server" Text='<%#Eval("STA_TITLE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>

                                <div id="panel" runat="Server" style="padding-top: 10px;">
                                    <div class="row">

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Lease Id</label>
                                                <asp:Label ID="txtlblsno" runat="server" CssClass="form-control" Visible="false"></asp:Label>
                                                <asp:TextBox ID="txtLeaseId" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Surrender Date<span style="color: red;">*</span></label>
                                                <div onmouseover="Tip('Please Click on the TextBox to select Date')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtsurrender" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Refund Date<span style="color: red;">*</span></label>
                                                <div onmouseover="Tip('Please Click on the TextBox to select Date')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtrefund" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Total Rent<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="Server" ControlToValidate="txtSDamount"
                                                    ErrorMessage="Please Enter Security Deposit" Display="None" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txttotamount" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Security Deposit<span style="color: red;">*</span></label>
                                                <asp:RegularExpressionValidator ID="revrent" runat="server" ControlToValidate="txtSDamount"
                                                    ErrorMessage="Please Enter Valid Security Deposit " Display="None" ValidationGroup="Val2"
                                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="Server" ControlToValidate="txtSDamount"
                                                    ErrorMessage="Please Enter Security Deposit" Display="None" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtSDamount" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Damages</label>
                                                <asp:TextBox ID="txtDmg" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Damage Amount</label>
                                                <asp:TextBox ID="txtDmgAmt" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Outstanding Amount</label>
                                                <asp:TextBox ID="txtOutAmt" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Amount To Be Paid To Lesse</label>
                                                <asp:TextBox ID="txtAmtPaidLesse" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Amount To Be Paid To Lessor</label>
                                                <asp:TextBox ID="txtAmtPaidLessor" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                            </div>
                                        </div>
                                        <%--      <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Payee Name</label>
                <asp:TextBox ID="txtPayeeName" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
            </div>
        </div>--%>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Payment Mode<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvpaytype" runat="server" ControlToValidate="ddlPaymentType"
                                                    Display="none" ErrorMessage="Please Select Payment Mode" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true"
                                                    ToolTip="--Select--">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Payment Date<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtTdate"
                                                    Display="None" ErrorMessage="Please Select Payment Date" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <div class='input-group date' id='effdate'>
                                                    <asp:TextBox ID="txtTdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('effdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="panel2" runat="Server">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Account/Serial Number <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                                        Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revAccno" Display="None" runat="server" ControlToValidate="txtAccNo"
                                                        ErrorMessage="Enter Valid Account Number" ValidationExpression="^[a-zA-Z0-9, ]*$"
                                                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtAccNo" runat="server" CssClass="form-control">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Bank Name <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                                        Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revBankName" Display="None" runat="server" ControlToValidate="txtBankName"
                                                        ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9, ]*$" ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Cheque/Draft Number <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rvfcheqno" runat="server" ControlToValidate="txtcheqno"
                                                        Display="None" ErrorMessage="Please Enter Cheque/Draft Number" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ControlToValidate="txtAccNo"
                                                        ErrorMessage="Enter Valid Cheque/Draft Number" ValidationExpression="^[a-zA-Z0-9, ]*$"
                                                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtcheqno" runat="server" CssClass="form-control">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Draft/Cheque Date <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="ddlcheque" runat="server" ControlToValidate="txtchedddt"
                                                        Display="None" ErrorMessage="Please Select Draft/Cheque Date" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                    <div class='input-group date' id='cheqdt'>
                                                        <asp:TextBox ID="txtchedddt" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('cheqdt')"></span>
                                                        </span>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div id="panel3" runat="Server" visible="false">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Account / Serial Number <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAccTwo"
                                                        Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                                        ControlToValidate="txtAccTwo" ErrorMessage="Enter Valid Account Number" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtAccTwo" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Bank Name <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfDeposited" runat="server" ControlToValidate="txtBankTwo"
                                                        Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revDeposited" Display="None" runat="server" ControlToValidate="txtBankTwo"
                                                        ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtBankTwo" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Branch Name <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvl3brnch" runat="server" ControlToValidate="txtbrnch"
                                                        Display="None" ErrorMessage="Please Enter Branch Name"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revl3brnch" Display="None" runat="server" ControlToValidate="txtbrnch"
                                                        ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtbrnch" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>IFSC Code <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvIFsc" runat="server" ControlToValidate="txtIFSC"
                                                        Display="None" ErrorMessage="Please Enter IFSC Code" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="REVIFsc" Display="None" runat="server" ControlToValidate="txtIFSC"
                                                        ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                        onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtIFSC" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <%-- <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Download Possession Letter</label>
                                                        <asp:LinkButton ID="DocLink" runat="server" CssClass="form-control">hello</asp:LinkButton>
                                                    </div>
                                                </div>--%>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Upload Possession Letter <a href="#" data-toggle="tooltip" title="Upload File Type:All and size should not be more than 20MB">?</a></label>
                                                <asp:RegularExpressionValidator ID="rfvupserv" Display="None" ControlToValidate="BrowsePossLtr"
                                                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                                                    ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$">                        
                                                </asp:RegularExpressionValidator>
                                                <div class="btn-default">
                                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                                    <asp:FileUpload ID="BrowsePossLtr" runat="Server" onchange="showselectedfiles(this)" Width="90%" AllowMultiple="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Approver Remarks<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvtxtremrk" runat="server" ControlToValidate="txtremarks" Display="None" ValidationGroup="Val2"
                                                    ErrorMessage="Please Enter Remarks"></asp:RequiredFieldValidator>
                                                <div onmouseover="Tip('Please Enter Remarks')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="multiLine" MaxLength="1000" Height="30%"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                            <div class="form-group">
                                                <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-default btn-primary" ValidationGroup="Val2" CausesValidation="true" />
                                                <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-default btn-primary" />
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <%-- <div class="row" style="padding-top: 20px">
                                            <div class="col-md-12">
                                                <h4 style="font: bold">Surrender Uploaded Document</h4>
                                                <asp:GridView ID="Documentsbindinggrid" runat="server" AutoGenerateColumns="False" SelectedRowStyle-VerticalAlign="Top"
                                                    AllowPaging="True" PageSize="5" EmptyDataText=" No Surrender Records Found" CssClass="table table-condensed table-bordered table-hover table-striped"
                                                    Style="font-size: 12px;">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Lease Id">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeaseName" runat="server" Text='<%#Eval("LEASE_ID")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Property Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPC" runat="server" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Lease Name" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeaseid" runat="server" Text='<%#Eval("LEASE_ID")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Surrender Status">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeasestatus" runat="server" Text='<%#Eval("STA_TITLE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Surrender Lease Document">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lblLseN" runat="server" Text='<%#Eval("DOC_NAME")%>' CommandName="Document"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                    <PagerStyle CssClass="pagination-ys" />
                                                </asp:GridView>
                                            </div>
                                        </div>--%>
                                        </br>
                                          </br>     
                                       
                                        <div class="row" runat="server" id="disply" visible="false">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <div class="form-group">
                                                        <h5 style="font: bold">Surrender Uploaded Document</h5>
                                                        <%--<strong>Documents</strong>--%>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <div class="form-group">
                                                        <asp:Label ID="lblDocsMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-3 col-xs-12">
                                                <div class="form-group">
                                                    <div id="tblGridDocs" runat="server">
                                                        <asp:DataGrid ID="grdDocs" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" DataKeyField="PM_SL_ID"
                                                            EmptyDataText="No Documents Found." AutoGenerateColumns="False" PageSize="5">
                                                            <Columns>
                                                                <asp:BoundColumn Visible="False" DataField="PM_SL_ID" HeaderText="ID"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="pm_sl_upl_path" HeaderText="Document Name">
                                                                    <HeaderStyle></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="STA_DESC" HeaderText="Property Status">
                                                                    <HeaderStyle></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="PM_PPT_NAME" HeaderText="Property Name">
                                                                    <HeaderStyle></HeaderStyle>
                                                                </asp:BoundColumn>
                                                                <asp:ButtonColumn Text="Download" CommandName="Download">
                                                                    <HeaderStyle></HeaderStyle>
                                                                </asp:ButtonColumn>
                                                                <asp:ButtonColumn Text="Delete" CommandName="Delete">
                                                                    <HeaderStyle></HeaderStyle>
                                                                </asp:ButtonColumn>
                                                            </Columns>
                                                            <HeaderStyle ForeColor="white" BackColor="Black" />
                                                            <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                <div class="modal fade" id="myModal" tabindex='-1'>
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Lease Application form</h4>
                                            </div>
                                            <div class="modal-body" id="modelcontainer">
                                                <%-- Content loads here --%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        function refreshSelectpicker() {
            $("#<%=ddlPaymentType.ClientID%>").selectpicker();

        }
        refreshSelectpicker();

    </script>
</body>
</html>
