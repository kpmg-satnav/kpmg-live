﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic

Partial Class WorkSpace_SMS_Webfiles_TenantRenewalApproval
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            ddlLtype.SelectedValue = "2"
            ddlLtype.Enabled = False
            'divTermsDates.Visible = False
            BindGrid()
        End If
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
    End Sub


    Private Sub BindGrid()
        Try
            lblMsg.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_RENEWAL_DETAILS")
            sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
            sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)
            gvLDetails_Lease.DataSource = sp.GetDataSet()
            gvLDetails_Lease.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If txtempid.Text <> "" Then
            BindGrid()
        Else
            lblMsg.Text = "Please Enter Tenant ID"
        End If
    End Sub
    Protected Sub gvLDetails_Lease_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLDetails_Lease.PageIndexChanging
        gvLDetails_Lease.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
End Class
