<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LeaseExtension.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_LeaseExtension" Title="Lease Extension" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="View Or Modify Maintenance Contract" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Lease Extension</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel runat="server" ID="upfelren" UpdateMode="Conditional">
                            <ContentTemplate>
                                <%-- <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />--%>
                                <asp:ValidationSummary ID="ValidationSummary3" runat="server" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg2" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="panel1" runat="server">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-md-10 control-label">Search By Lease ID / Property Name / Code / City / Location </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                                            Display="none" ErrorMessage="Please Search By Property Name / Lease / Location Name" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <asp:Button ID="btnsearch" CssClass="btn btn-default btn-primary" runat="server" Text="Search" ValidationGroup="Val2"
                                                            CausesValidation="true" TabIndex="2" />
                                                        <asp:Button ID="btnReset" runat="server" CausesValidation="false" CssClass="btn btn-primary custom-button-color" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" style="padding-top: 10px">
                                        <asp:GridView ID="gvitems" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                            AllowPaging="True" PageSize="5" EmptyDataText="No Records Found." CssClass="table table-condensed table-bordered table-hover table-striped"
                                            Style="font-size: 12px;">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Lease Name" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbllname" runat="server" Text='<%#Eval("LEASE_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbllsno" runat="server" Text='<%#Eval("PM_LES_SNO")%>' Visible="false"></asp:Label>
                                                        <asp:LinkButton ID="lblextend" runat="server" CssClass="lblCODE" Text='<%#Eval("LEASE_ID")%>' CommandName="Extension"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Property Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCode" runat="server" CssClass="lblCODE" Text='<%#Eval("PM_PPT_CODE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Property Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" CssClass="lblName" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="City">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcity" runat="server" CssClass="lblcity" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbllcm" runat="server" CssClass="lblLCM" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Start Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsdate" runat="server" CssClass="lblstartdate" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Expiry Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEdate" runat="server" CssClass="lblExpiryDate" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:TemplateField HeaderText="Created By">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Lbluser" runat="server" CssClass="lbluser" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="true" ShowValidationErrors="true" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                                <div id="pnl1" runat="Server">

                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label style="font-weight: bold">Property Name <b>:</b></label>
                                                <asp:Label ID="propname" runat="server" CssClass="control-label" Font-Bold="true" ForeColor="#209e91"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label style="font-weight: bold">Lease Start Date <b>:</b></label>
                                                <asp:Label ID="lblstrtdate" runat="server" CssClass="control-label" Font-Bold="true" ForeColor="#209e91"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label style="font-weight: bold">Lease End Date <b>:</b></label>
                                                <asp:Label ID="lblenddate" runat="server" CssClass="control-label" Font-Bold="true" ForeColor="#209e91"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Lease Id</label>
                                                <asp:TextBox ID="txtsno" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                                <asp:TextBox ID="txtstore" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Lease Start Date</label>
                                                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Ext. Lease End Date<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="Server" ControlToValidate="txttodate"
                                                    ErrorMessage="Please Select Lease End Date" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <div class='input-group date' id='effdate'>
                                                    <div onmouseover="Tip('Please Click on the TextBox to select Date')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txttodate" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('effdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Rent Amount</label>
                                                <asp:RequiredFieldValidator ID="rfvrent" runat="Server" ControlToValidate="txtrent"
                                                    ErrorMessage="Please Enter Rent Amount for Lease Extension" Display="None" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revrent" runat="server" ControlToValidate="txtrent"
                                                    ErrorMessage="Please Enter Rent Amount in Decimals" Display="None" ValidationGroup="Val2"
                                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <asp:TextBox ID="txtrent" runat="server" CssClass="form-control" TabIndex="2" MaxLength="10" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Security Deposit</label>
                                                <asp:RequiredFieldValidator ID="rfvdep" runat="Server" ControlToValidate="txtsdep"
                                                    ErrorMessage="Please Enter Security Deposit for Lease Extension" Display="None"
                                                    ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revdep" runat="server" ControlToValidate="txtsdep"
                                                    ErrorMessage="Please Enter Security Deposit in Decimals" Display="None" ValidationGroup="Val2"
                                                    ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Please Enter Security Deposit in Decimals Only')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtsdep" runat="server" CssClass="form-control" TabIndex="3" MaxLength="10" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Remarks<span style="color: red;">*</span></label>
                                                <asp:RegularExpressionValidator ID="RegExpRem" runat="server" ControlToValidate="txtremarks"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Remarks upto 500 Characters, special characters (-/_@$)(&*,space) are allowed">
                                                </asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="rfvtxtremrk" runat="server" ControlToValidate="txtremarks" Display="None" ValidationGroup="Val1"
                                                    ErrorMessage="Please Enter Remarks"></asp:RequiredFieldValidator>
                                                <div onmouseover="Tip('Please Enter Remarks')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control" Height="30% " TextMode="multiLine" MaxLength="500"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Upload Possession Letter <a href="#" data-toggle="tooltip" title="Upload File Type:All and size should not be more than 20MB">?</a></label>
                                                <asp:RegularExpressionValidator ID="rfvupserv" Display="None" ControlToValidate="BrowsePossLtr"
                                                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                                                    ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$">                        
                                                </asp:RegularExpressionValidator>
                                                <div class="btn-default">
                                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                                    <asp:FileUpload ID="BrowsePossLtr" runat="Server" onchange="showselectedfiles(this)" Width="90%" AllowMultiple="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                            <div class="form-group">
                                                <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-default btn-primary" CausesValidation="true"
                                                    Text="Submit" TabIndex="5" ValidationGroup="Val1" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Lease Application form</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin(id) {
            $("#modelcontainer").load("frmViewLeaseDetailsuser.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
                $("#myModal").modal().fadeIn();
            });
        }
    </script>
</body>
</html>
