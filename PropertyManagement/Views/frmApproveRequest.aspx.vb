Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmApproveRequest
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            getrequest()
        End If
    End Sub
    Public Sub getrequest()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_APPRREQ")
        sp.Command.AddParameter("@User", Session("uid"), DbType.String)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)
        ddlWorkRequest.DataSource = sp.GetDataSet()
        ddlWorkRequest.DataTextField = "PN_WORKREQUEST_REQ"
        ddlWorkRequest.DataBind()
        ddlWorkRequest.Items.Insert(0, New ListItem("--Select WorkRequest--", "0"))
    End Sub
    Public Sub WorkRequest_Change()
        If ddlWorkRequest.SelectedIndex > 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_WR")
            sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtCity.Text = ds.Tables(0).Rows(0).Item("PN_BLDG_CODE")
                txtproperty.Text = ds.Tables(0).Rows(0).Item("PN_LOC_ID")
                txtproptype.Text = ds.Tables(0).Rows(0).Item("PROP_TYPE")
                txtLocation.Text = ds.Tables(0).Rows(0).Item("PN_CTY_CODE")
                txtWorkTitle.Text = ds.Tables(0).Rows(0).Item("WORK_TITLE")
                txtWorkSpec.Text = ds.Tables(0).Rows(0).Item("WORK_SPECIFICATIONS")
                txtamount.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("ESTIMATED_AMOUNT"), 2, MidpointRounding.AwayFromZero)
                txtVendorName.Text = ds.Tables(0).Rows(0).Item("VENDOR_NAME")
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("Remarks")
                Dim status As Integer = ds.Tables(0).Rows(0).Item("REQUEST_STATUS")
                If status = 5010 Then
                    txtstatus.Text = "Pending"
                Else
                    txtstatus.Text = "Approved"
                End If
            End If
            btnApprove.Enabled = True
            btnReject.Enabled = True
        Else
            txtCity.Text = ""
            txtproperty.Text = ""
            txtWorkTitle.Text = ""
            txtWorkSpec.Text = ""
            txtamount.Text = ""
            txtVendorName.Text = ""
            txtRemarks.Text = ""
            txtstatus.Text = ""
            txtproptype.Text = ""
        End If
    End Sub
    Public Sub ValidateWorkRequest()
        lblmsg.Text = ""
        Dim ds As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_VALIDATE_REQUEST")
        sp.Command.AddParameter("@VALIDATE_WR", ddlWorkRequest.SelectedValue, DbType.String)
        ds = sp.GetDataSet
        If ds.Tables(0).Rows(0).Item("VALIDATE_COUNT") = 1 Then
            lblmsg.Text = "Work Order Has Already Been Approved/ Rejected"
            btnApprove.Enabled = False
            btnReject.Enabled = False
        Else
            WorkRequest_Change()
        End If
    End Sub
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Approve")
        sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@ApprovedBy", Session("Uid"), DbType.String)
        sp.ExecuteScalar()
        'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=11")
        lblmsg.Text = "Work Request Approved Succesfully"
        Cleardata()

    End Sub
    Protected Sub ddlWorkRequest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWorkRequest.SelectedIndexChanged
        ValidateWorkRequest()
    End Sub
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Reject")
        sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp.ExecuteScalar()
        'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=12")
        lblmsg.Text = "Work Request Rejected Successfully"
        Cleardata()

    End Sub
    Public Sub Cleardata()
        getrequest()
        ddlWorkRequest.SelectedValue = 0
        ' txtBuilding.Text = ""
        txtproperty.Text = ""
        txtWorkTitle.Text = ""
        txtWorkSpec.Text = ""
        txtamount.Text = ""
        txtVendorName.Text = ""
        txtstatus.Text = ""
        txtRemarks.Text = ""
        btnApprove.Enabled = False
        btnReject.Enabled = False
    End Sub
End Class
