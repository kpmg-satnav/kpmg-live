﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LeaseExtensionAppRej.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_LeaseExtensionAppRej" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Approve Lease Extension" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Approve/Reject Lease Extension</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="pnl1" runat="Server">

                            <h3 class="panel-title panel-heading-qfms-title">Lease Extension Details</h3>

                            <hr />
                            <div class="row">
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label style="font-weight: bold">Lease Start Date <b>:</b></label>
                                        <asp:Label ID="lblstrtdate" runat="server" CssClass="control-label" Font-Bold="true" ForeColor="#209e91"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label style="font-weight: bold">Lease End Date <b>:</b></label>
                                        <asp:Label ID="lblenddate" runat="server" CssClass="control-label" Font-Bold="true" ForeColor="#209e91"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Lease Id</label>
                                        <asp:TextBox ID="txtstore" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Lease Start Date</label>
                                        <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Ext. Lease End Date<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="Server" ControlToValidate="txttodate"
                                            ErrorMessage="Please Select Lease End Date" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class='input-group date' id='effdate'>
                                            <div onmouseover="Tip('Please Click on the TextBox to select Date')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar" onclick="setup('effdate')"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Rent Amount</label>
                                        <asp:RequiredFieldValidator ID="rfvrent" runat="Server" ControlToValidate="txtrent"
                                            ErrorMessage="Please Enter Rent Amount for Lease Extension" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revrent" runat="server" ControlToValidate="txtrent"
                                            ErrorMessage="Please Enter Rent Amount in Decimals" Display="None" ValidationGroup="Val1"
                                            ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                        <asp:TextBox ID="txtrent" runat="server" CssClass="form-control" TabIndex="2" MaxLength="10" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <%-- <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Download Lease Eetension Documents</label>
                                                <asp:LinkButton ID="DocLink" runat="server" CssClass="form-control">hello</asp:LinkButton>
                                            </div>
                                        </div>--%>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Security Deposit</label>
                                        <asp:RequiredFieldValidator ID="rfvdep" runat="Server" ControlToValidate="txtsdep"
                                            ErrorMessage="Please Enter Security Deposit for Lease Extension" Display="None"
                                            ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revdep" runat="server" ControlToValidate="txtsdep"
                                            ErrorMessage="Please Enter Security Deposit in Decimals" Display="None" ValidationGroup="Val1"
                                            ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                        <div onmouseover="Tip('Please Enter Security Deposit in Decimals Only')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtsdep" runat="server" CssClass="form-control" TabIndex="3" MaxLength="10" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Approver Remarks</label>
                                        <asp:RegularExpressionValidator ID="RegExpRem" runat="server" ControlToValidate="txtremarks"
                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Remarks upto 500 Characters, special characters (-/_@$)(&*,space) are allowed">
                                        </asp:RegularExpressionValidator>
                                        <div onmouseover="Tip('Please Enter Remarks')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control" Height="30% " TextMode="multiLine" MaxLength="500"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                    <div class="form-group">
                                        <asp:Button ID="btnapprove" runat="server" CssClass="btn btn-default btn-primary" CausesValidation="true"
                                            Text="Approve" TabIndex="5" ValidationGroup="Val1" />
                                        <asp:Button ID="btnreject" runat="server" CssClass="btn btn-default btn-primary" CausesValidation="true"
                                            Text="Reject" TabIndex="6" />
                                        <asp:Button ID="btnback" runat="server" CssClass="btn btn-default btn-primary" CausesValidation="true"
                                            Text="Back" TabIndex="7" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        </br>
                                          </br>     
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <div class="form-group">
                                                    <h5 style="font: bold">Lease Extention Uploaded Document</h5>
                                                    <%--<strong>Documents</strong>--%>
                                                </div>
                                            </div>
                                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDocsMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                    </asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div id="tblGridDocs" runat="server">
                                        <asp:DataGrid ID="grdDocs" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" DataKeyField="PM_LEDOC_SNO"
                                            EmptyDataText="No Documents Found." AutoGenerateColumns="False" PageSize="5">
                                            <Columns>
                                                <asp:BoundColumn Visible="False" DataField="PM_LEDOC_SNO" HeaderText="ID"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="PM_LEDOC_PATH" HeaderText="Document Name">
                                                    <HeaderStyle></HeaderStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="PM_LEDOC_CREATED_DT" HeaderText="Created Date">
                                                    <HeaderStyle></HeaderStyle>
                                                </asp:BoundColumn>
                                                <%--       <asp:BoundColumn DataField="PM_PPT_NAME" HeaderText="Property Name">
                                                                    <HeaderStyle></HeaderStyle>
                                                                </asp:BoundColumn>--%>
                                                <asp:ButtonColumn Text="Download" CommandName="Download">
                                                    <HeaderStyle></HeaderStyle>
                                                </asp:ButtonColumn>
                                                <asp:ButtonColumn Text="Delete" CommandName="Delete">
                                                    <HeaderStyle></HeaderStyle>
                                                </asp:ButtonColumn>
                                            </Columns>
                                            <HeaderStyle ForeColor="white" BackColor="Black" />
                                            <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Lease Application form</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin(id) {
            $("#modelcontainer").load("frmViewLeaseDetailsuser.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
                $("#myModal").modal().fadeIn();
            });
        }
    </script>
</body>
</html>
