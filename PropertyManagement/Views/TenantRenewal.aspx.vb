﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic

Partial Class WorkSpace_SMS_Webfiles_TenantRenewal
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            ddlLtype.SelectedValue = "2"
            ddlLtype.Enabled = False
            fillgrid()
            'BindRejectedDetails()
        End If
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
    End Sub

    Private Sub BindLease_Type()
        Try
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETACTIVE_LEASETYPE")
            sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
            ddlLtype.DataSource = sp3.GetDataSet()
            ddlLtype.DataTextField = "PN_LEASE_TYPE"
            ddlLtype.DataValueField = "PN_LEASE_ID"
            ddlLtype.DataBind()
            ddlLtype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub fillgrid()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_TENANT_RENEWAL_DETAILS")
            sp2.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)
            Session("dataset") = sp2.GetDataSet()
            gvLDetails_Lease.DataSource = Session("dataset")
            gvLDetails_Lease.DataBind()
            gvLDetails_Lease.PageIndex = Convert.ToInt32(Session("CurrentPageIndex"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    'Private Sub BindRejectedDetails()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_BIND_REJECTED_DETAILS")
    '        sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
    '        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)
    '        Session("RejectedData") = sp.GetDataSet()
    '        gvrejected.DataSource = Session("RejectedData")
    '        gvrejected.DataBind()
    '        gvrejected.DataBind()
    '        gvrejected.PageIndex = Convert.ToInt32(Session("CurrentPageIndex"))
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    Protected Sub gvLDetails_Lease_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLDetails_Lease.PageIndexChanging
        gvLDetails_Lease.PageIndex = e.NewPageIndex()
        fillgrid()
    End Sub

    'Protected Sub gvrejected_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvrejected.PageIndexChanging
    '    gvrejected.PageIndex = e.NewPageIndex()
    '    fillgrid()
    'End Sub
    Public Sub fillgridOnTenantCodeSearch()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_TENANT_FILTER_DETAILS")
            sp2.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
            sp2.Command.AddParameter("@TEN_CODE", txtSearch.Text, DbType.String)
            Session("dataset") = sp2.GetDataSet()
            gvLDetails_Lease.DataSource = Session("dataset")
            gvLDetails_Lease.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            fillgridOnTenantCodeSearch()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
