﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SubSonic; 

public partial class PropertyManagement_Views_TenExtensionApproval : System.Web.UI.Page
{
    clsSubSonicCommonFunctions ObjSubSonic = new clsSubSonicCommonFunctions();
    SqlParameter[] param;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_TENANT_DETAILS");
            sp.Command.AddParameter("@USER", Session["uid"], DbType.String);
            sp.Command.AddParameter("@COMPANY", Session["COMPANYID"], DbType.String);
            sp.Command.AddParameter("@FLAG", 8, DbType.Int32);
            gvLDetails_Lease.DataSource = sp.GetDataSet();
            gvLDetails_Lease.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    protected void gvLDetails_Lease_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "ViewDetails")
            {
                // CALL SP AND FILL 3 TAB VALUES
                panel1.Visible = true;
                   GridViewRow gvRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int rowIndex = gvRow.RowIndex;
                hdnExtSNO.Value = ((Label)gvLDetails_Lease.Rows[rowIndex].FindControl("lblExtSNO")).Text;
                hdnExtFrmDt.Value = ((Label)gvLDetails_Lease.Rows[rowIndex].FindControl("lblExtsdate")).Text;
                hdnExtToDt.Value = ((Label)gvLDetails_Lease.Rows[rowIndex].FindControl("lblExtEdate")).Text;
                BindDocuments(hdnExtSNO.Value);
                hdnPropCode.Value = Convert.ToString(e.CommandArgument);
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_TENANT_DETAILS");
                sp.Command.AddParameter("@USER", Session["uid"], DbType.String);
                sp.Command.AddParameter("@PN_NAME", e.CommandArgument, DbType.String);
                sp.Command.AddParameter("@FLAG", 9, DbType.Int32);
                sp.Command.AddParameter("@COMPANY", Session["COMPANYID"], DbType.Int32);
                DataSet ds = sp.GetDataSet();

                lblproptype.Text = Convert.ToString(ds.Tables[0].Rows[0]["PN_PROPERTYTYPE"]);
                lblcity.Text = Convert.ToString(ds.Tables[0].Rows[0]["CTY_NAME"]);
                lblLocation.Text = Convert.ToString(ds.Tables[0].Rows[0]["LCM_NAME"]);
                lblProperty.Text = Convert.ToString(ds.Tables[0].Rows[0]["PROPERTY_NAME"]);
                lblOccArea.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TEN_OCCUP_AREA"]);
                lblTenFromDt.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TEN_FRM_DT"]);
                lblTenToDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TEN_TO_DT"]);

                lblTenant.Text = Convert.ToString(ds.Tables[0].Rows[0]["TENANT"]);
                lblTenCode.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TEN_CODE"]);
                txtNoofparking.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TEN_NO_OF_PARKING"]);

                txtRent.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_RENT"]);
                txtSecurityDeposit.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_SECURITY_DEPOSIT"]);

                if (Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_JOIN_DT"]) == "01/01/1900 00:00:00")
                {
                    txtDate.Text = "";
                }
                else
                {
                    txtDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_JOIN_DT"]);
                }            

             
                txtpayterms.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_PT_NAME"]);
                txtfees.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_ADDITION_PARKING_FEE"]);
                txtcar.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_MAINT_FEES"]);
                txtamount.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TD_TOT_RENT"]);
                txtReqRemarks.Text = Convert.ToString(ds.Tables[0].Rows[0]["PM_TE_REMARKS"]);
            }
            else
            {
                panel1.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void txtfees_TextChanged(object sender, EventArgs e)
    {
        if (txtRent.Text != null & txtfees.Text != null)
        {
            txtamount.ReadOnly = false;
            txtamount.Text = Convert.ToString(Convert.ToDecimal(txtRent.Text) + Convert.ToDecimal(txtfees.Text));
            txtamount.ReadOnly = true;
        }
        else
        {
            txtamount.Text = null;
        }
    }

    protected void txtRent_TextChanged(object sender, EventArgs e)
    {
        if (txtfees.Text != null & txtRent.Text != null)
        {
            txtamount.ReadOnly = false;
            txtamount.Text = Convert.ToString(Convert.ToDecimal(txtfees.Text) + Convert.ToDecimal(txtRent.Text));
            txtamount.ReadOnly = true;
        }
        else
        {
            txtamount.Text = null;
        }
    }

    protected void gvLDetails_Lease_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        gvLDetails_Lease.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        Extension_AppRej(5002);
        lblmsg.Text = "Tenant Extension Approved Successfully";
    }

    protected void btnReject_Click(object sender, EventArgs e)
    {
        Extension_AppRej(5003);
        lblmsg.Text = "Tenant Extension Rejected Successfully";
    }

    private void Extension_AppRej(int status)
    {
        SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_TEN_EXTENSION_APPROVAL");
        sp3.Command.AddParameter("@TEN_SNO", hdnPropCode.Value, DbType.String);
        sp3.Command.AddParameter("@EXTSNO", hdnExtSNO.Value, DbType.String);
        sp3.Command.AddParameter("@EXT_FROMDATE", hdnExtFrmDt.Value, DbType.Date);
        sp3.Command.AddParameter("@EXT_TODATE", hdnExtToDt.Value, DbType.Date);
        sp3.Command.AddParameter("@RENT", Convert.ToDecimal(txtRent.Text), DbType.Decimal);
        sp3.Command.AddParameter("@SEC_DEPOSIT", Convert.ToDecimal(txtSecurityDeposit.Text), DbType.String);
        sp3.Command.AddParameter("@ADD_FEE", Convert.ToDecimal(txtcar.Text), DbType.Decimal);
        sp3.Command.AddParameter("@MAIN_FEE", Convert.ToDecimal(txtfees.Text), DbType.Decimal);
        sp3.Command.AddParameter("@REMS", txtRemarks.Text, DbType.String);
        sp3.Command.AddParameter("@STATUS", status, DbType.String);
        sp3.Command.AddParameter("@CREATED_BY", Session["uid"], DbType.String);
        sp3.Command.AddParameter("@CMP_ID", Session["COMPANYID"], DbType.String);
        sp3.ExecuteScalar();
        BindGrid();
        panel1.Visible = false;
    }


    public void BindDocuments(string Sno)
    {
        DataTable dtDocs = new DataTable("Documents");
        param = new SqlParameter[2];
        param[0] = new SqlParameter("@PM_LDOC_SNO", SqlDbType.NVarChar, 50);
        param[0].Value = Sno;
        param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[1].Value = Session["COMPANYID"];
        DataSet ds = new DataSet();
        ds = (DataSet)ObjSubSonic.GetSubSonicDataSet("PM_GET_TENANT_EXT_DOCS", param);
        if (ds.Tables[0].Rows.Count > 0)
        {
            grdDocs.DataSource = ds;
            grdDocs.DataBind();
            tblGridDocs.Visible = true;
            lblDocsMsg.Text = "";
        }
        else
        {
            tblGridDocs.Visible = false;
            lblmsg.Visible = true;
            lblDocsMsg.Text = "No Documents Available";
        }
        dtDocs = null;
    }

   
    protected void grdDocs_ItemCommand1(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Download")
        {
            dynamic pm_ldoc_sno = grdDocs.DataKeys[e.Item.ItemIndex];
            e.Item.BackColor = System.Drawing.Color.LightSteelBlue;
            string filePath = null;
            string FileName;
            FileName = grdDocs.Items[e.Item.ItemIndex].Cells[1].Text;
            filePath = "~\\Images\\Property_Images\\" + FileName;
            Response.ContentType = "application/CSV";
            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + FileName + "\"");
            Response.TransmitFile(Server.MapPath(filePath));
            Response.End();
        }
    }
    protected void grdDocs_DeleteCommand1(object source, DataGridCommandEventArgs e)
    {
        if (grdDocs.EditItemIndex == -1)
        {
            object Bid = 0;
            Bid = grdDocs.DataKeys[e.Item.ItemIndex];
            param = new SqlParameter[1];
            param[0] = new SqlParameter("@PM_LDOC_SNO", SqlDbType.NVarChar, 50);
            param[0].Value = Bid;
            ObjSubSonic.GetSubSonicDataSet("PM_DELETE_TENANT_DOCS", param);
            BindDocuments(hdnExtSNO.Value);
        }
    }
}