Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports SqlHelper
Imports System.Drawing

Partial Class PropertyManagement_Views_LeaseL1Approval
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet
    Dim RentRevision As DataTable

    Public Class L1Approval

        Public Property Lease_ID() As String
            Get
                Return m_Lease_ID
            End Get
            Set(value As String)
                m_Lease_ID = value
            End Set
        End Property
        Private m_Lease_ID As String

        Public Property Lease_SNO() As Integer
            Get
                Return m_Lease_SNO
            End Get
            Set(value As Integer)
                m_Lease_SNO = value
            End Set
        End Property
        Private m_Lease_SNO As Integer

        Public Property L1_REMARKS() As String
            Get
                Return m_L1_REMARKS
            End Get
            Set(value As String)
                m_L1_REMARKS = value
            End Set
        End Property
        Private m_L1_REMARKS As String

    End Class

    Public Class ImageClas
        Private _fn As String

        Public Property Filename() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String

        Public Property FilePath() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property

    End Class

    Private Sub BindProperty()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_BIND_PROPERTIES_FOR_LEASE")
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@FLAG", 1, DbType.String)
        ddlproperty.DataSource = sp.GetDataSet()
        ddlproperty.DataTextField = "PN_NAME"
        ddlproperty.DataValueField = "PM_PPT_SNO"
        ddlproperty.DataBind()
        ddlproperty.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindRentRevision()

        Dim rowCount As Integer = 0
        rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
        If rowCount > 1 Then
            RentRevisionPanel.Visible = True
            Dim RR_lst As New List(Of RentRevision)()
            Dim rr_obj As New RentRevision()
            For i As Integer = 1 To rowCount - 1
                rr_obj = New RentRevision()
                rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
                rr_obj.RR_Percentage = "0"
                RR_lst.Add(rr_obj)
            Next
            Repeater2.DataSource = RentRevision
            Repeater2.DataBind()
        Else
            RentRevisionPanel.Visible = False
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then
            scriptManager.RegisterPostBackControl(grdDocs)
            scriptManager.RegisterPostBackControl(lldocgrid)
            scriptManager.RegisterPostBackControl(gvlandlordItems)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            BindGrid()
            BindProperty()
            BindPropType()
            BindCity()
            BindPayMode()
            BindFlooringTypes()
            BindStatus()
            BindTenure()
            BindEntity()
            txtsdate.Text = DateTime.Now.ToString("MM/dd/yyyy")
            txtedate.Text = DateTime.Now.AddYears(1).ToString("MM/dd/yyyy")
        End If
    End Sub
    Private Sub BindStatus()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PCL_GET_PROJECTTYPES")
        ddlstatus.DataSource = sp3.GetDataSet()
        ddlstatus.DataTextField = "PROJECTNAM"
        ddlstatus.DataValueField = "PROJECTCD"
        ddlstatus.DataBind()
        ddlstatus.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITY")
        sp.Command.AddParameter("@dummy", 0, DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Private Sub BindPayMode()
        ObjSubSonic.Binddropdown(ddlpaymentmode, "PM_GET_PAYMENT_MODE", "NAME", "CODE")
    End Sub

    Private Sub BindLeaseExpences()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_EXPENSES_OF_LEASE")
        sp.Command.AddParameter("@PM_LES_SNO", hdnLSNO.Value, DbType.String)
        gvLeaseExpences.DataSource = sp.GetDataSet()
        gvLeaseExpences.DataBind()
    End Sub

    Private Sub BindTenure()
        ObjSubSonic.Binddropdown(ddlTenure, "PM_GET_PAYMENT_TERMS", "PM_PT_NAME", "PM_PT_SNO")
        ddlTenure.Items.RemoveAt(0)
    End Sub

    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_LEVEL1_APROVAL")
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.Int32)
            ds = sp.GetDataSet()
            ViewState("reqDetails") = ds
            gvLeases.DataSource = ViewState("reqDetails")
            If ds.Tables(0).Rows.Count > 0 Then
                gvLeases.DataSource = ds
                gvLeases.DataBind()
                pnlBulk.Visible = True
            Else
                gvLeases.DataSource = ds
                gvLeases.DataBind()
                pnlBulk.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_LEVEL1_APROVAL_BY_SEARCH")
        sp.Command.AddParameter("@SEARCH_BY", txtSearch.Text, Data.DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), Data.DbType.String)
        sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.Int32)
        ds = sp.GetDataSet()
        ViewState("reqDetails") = ds
        gvLeases.DataSource = ViewState("reqDetails")
        If ds.Tables(0).Rows.Count > 0 Then
            gvLeases.DataSource = ds
            gvLeases.DataBind()
            pnlBulk.Visible = True
            updatepanel.Visible = False
        Else
            gvLeases.DataSource = ds
            gvLeases.DataBind()
            pnlBulk.Visible = False
            updatepanel.Visible = False
        End If
    End Sub
    Private Sub BindEntity()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ENTITY_TYPES")
        ddlentity.DataSource = sp3.GetDataSet()
        ddlentity.DataTextField = "CHE_NAME"
        ddlentity.DataValueField = "CHE_CODE"
        ddlentity.DataBind()
        ddlentity.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub
    Protected Sub gvLeases_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLeases.RowCommand
        Try
            If e.CommandName = "GetLeaseDetails" Then
                Dim gvr As GridViewRow = CType(((CType(e.CommandSource, LinkButton)).NamingContainer), GridViewRow)
                Dim RowIndex1 As Integer = gvr.RowIndex
                For Each row1 As GridViewRow In gvLeases.Rows
                    If row1.RowIndex = RowIndex1 Then
                        gvLeases.Rows(RowIndex1).BackColor = System.Drawing.Color.SkyBlue
                    Else
                        gvLeases.Rows(row1.RowIndex).BackColor = Color.Empty
                    End If
                Next
                pnlBulk.Visible = False
                hdnLSNO.Value = e.CommandArgument
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim RowIndex As Integer = row.RowIndex
                Dim ReqID As String = DirectCast(row.FindControl("lblReqID"), LinkButton).Text.ToString()
                Session("REQ_ID") = ReqID
                GetSelectedLeaseDetails(hdnLSNO.Value)
                BindDocuments()
            Else
                pnlBulk.Visible = True
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GetSelectedLeaseDetails(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GRID_VIEW_MODIFY")
        sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@FLAG", 2, DbType.String)
        sp.Command.AddParameter("@HDN_VALUE", Reqid, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvlandlordItems.DataSource = ds.Tables(1)
        gvlandlordItems.DataBind()
        SetLeaseValuesToUpdate(ds)
        updatepanel.Visible = True
    End Sub

    Private Sub SetLeaseValuesToUpdate(ByVal leaseAllSections As DataSet)
        Dim LeaseNAreaNDetails As DataTable
        Dim charges As DataTable
        Dim agreementDetails As DataTable
        Dim brokerageDt As DataTable
        Dim UtilityDt As DataTable
        Dim OtherServicesDt As DataTable
        Dim OtherDetailsDt As DataTable
        Dim LeaseDetailsDt As DataTable
        Dim LeaseExpDt As DataTable
        Dim POA As DataTable
        Dim LeaseEscalationDt As DataTable
        Dim AreaDetails As DataTable
        Dim LLScope As DataTable

        LeaseNAreaNDetails = leaseAllSections.Tables(0)
        charges = leaseAllSections.Tables(2)
        agreementDetails = leaseAllSections.Tables(3)
        brokerageDt = leaseAllSections.Tables(4)
        UtilityDt = leaseAllSections.Tables(5)
        OtherServicesDt = leaseAllSections.Tables(6)
        OtherDetailsDt = leaseAllSections.Tables(7)
        LeaseDetailsDt = leaseAllSections.Tables(8)
        LeaseEscalationDt = leaseAllSections.Tables(9)
        LeaseExpDt = leaseAllSections.Tables(10)
        POA = leaseAllSections.Tables(11)
        ''RentRevision = leaseAllSections.Tables(11)
        AreaDetails = leaseAllSections.Tables(12)
        LLScope = leaseAllSections.Tables(13)

        BindLeaseExpences()
        ddlproperty.ClearSelection()
        'Lease Details Section Start
        Dim leaseproperty As String = LeaseNAreaNDetails.Rows(0)("PM_PPT_SNO")
        ddlproperty.Items.FindByValue(leaseproperty).Selected = True

        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@PROP_CODE", SqlDbType.VarChar, 200)
        param(0).Value = ddlproperty.SelectedValue
        ds = New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("BIND_DETAILS_BY_PROP", param)
        If ds.Tables(0).Rows.Count > 0 Then
            txtPropAddedBy.Text = ds.Tables(0).Rows(0).Item("ADDED_BY").ToString()
            txtApprovedBy.Text = ds.Tables(0).Rows(0).Item("ADDED_BY").ToString()
            ddlentity.ClearSelection()
            ddlentity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CHE_CODE").ToString()).Selected = True
            txtInspection.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_BY").ToString()
            If (ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT").ToString() <> "") Then
                txtInspecteddate.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT")
            End If
            ''txtInspecteddate.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT").ToString()
            ddlPprtNature.ClearSelection()
            ddlPprtNature.Items.FindByValue(ds.Tables(0).Rows(0).Item("pm_ppt_nature").ToString()).Selected = True
            'txtaptno.Text = ds.Tables(0).Rows(0).Item("PM_PPT_APT_NO").ToString()
            'txtstaffincharge.Text = ds.Tables(0).Rows(0).Item("PM_STAFFINCHARGE").ToString()
            'txtownername.Text = ds.Tables(0).Rows(0).Item("PM_OWN_NAME").ToString()
        End If
        txtPropCode.Text = LeaseNAreaNDetails.Rows(0)("PM_PPT_CODE")
        'Lease Details Section End
        ddlstatus.ClearSelection()
        Dim STATUS As String = LeaseNAreaNDetails.Rows(0)("PM_LES_STATUS")
        If (STATUS <> "") Then
            ddlstatus.Items.FindByValue(STATUS).Selected = True
        End If

        ddlPropertyType.ClearSelection()
        Dim PropertyType As String = LeaseNAreaNDetails.Rows(0)("PM_PPT_TYPE")
        If (PropertyType <> "") Then
            ddlPropertyType.Items.FindByValue(PropertyType).Selected = True
        End If
        'Area & Cost Details Section start
        ddlSecurityDepMonths.ClearSelection()
        Dim secrtyDepositMonths As String = LeaseNAreaNDetails.Rows(0)("PM_LES_SEC_DEP_MONTHS")
        ddlSecurityDepMonths.Items.FindByValue(secrtyDepositMonths).Selected = True
        ddlTenure.ClearSelection()
        Dim tenure As String = LeaseNAreaNDetails.Rows(0)("PM_LES_TENURE")
        ddlTenure.Items.FindByValue(tenure).Selected = True
        Dim costType As String = LeaseNAreaNDetails.Rows(0)("PM_LES_COST_TYPE")
        rblCostType.Items.FindByValue(costType).Selected = True
        If String.Equals(costType, "Seat") Then
            Costype1.Visible = False
            Costype2.Visible = True
        Else
            Costype1.Visible = True
            Costype2.Visible = False
        End If
        txtSeatCost.Text = IIf(LeaseNAreaNDetails.Rows(0)("PM_LES_SEAT_COST") = 0, 0, Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_SEAT_COST")))
        txtRentPerSqftCarpet.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_PER_SQFT_CARPET"))
        txtRentPerSqftBUA.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_PER_SQFT_BUA"))
        txtLnumber.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_CTS_NO")
        modifiedremrks.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_REMARKS")
        txtentitle.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_ENTITLED_AMT"))
        txtInvestedArea.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_BASIC_RENT"))
        txtpay.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_SEC_DEPOSIT"))
        txtRentFreePeriod.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_FREE_PERIOD")
        txtInteriorCost.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_INTERIOR_COST"))
        'Area & Cost Details Section end

        'Charges Section start
        txtregcharges.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_REGS_CHARGES"))
        txtsduty.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_STMP_DUTY_CHARGES"))
        txtfurniture.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_FF_CHARGES"))
        txtbrokerage.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_CONSLBROKER_CHARGES"))
        txtpfees.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_PROF_CHARGES"))
        txtmain1.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_MAINT_CHARGES"))
        ddlmaintpaid.ClearSelection()
        ddlmaintpaid.Items.FindByValue(charges.Rows(0)("PM_LC_MAINT_PAIDBY")).Selected = True
        ddlamenpaid.ClearSelection()
        ddlamenpaid.Items.FindByValue(charges.Rows(0)("PM_LC_AMENT_PAIDBY")).Selected = True
        'txtservicetax.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_SERVICE_TAX"))
        txtservicetax.ClearSelection()
        Dim GSTTax As String = charges.Rows(0)("PM_LC_GST").ToString()
        If (GSTTax <> "") Then
            txtservicetax.Items.FindByValue(GSTTax).Selected = True
        End If
        txtproptax.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_PROPERTY_TAX"))
        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
        'Charges Section end

        'Agreement Details Section start
        If (agreementDetails.Rows(0)("PM_LAD_START_DT_AGREEMENT").ToString() <> "") Then
            txtstartagreDT.Text = agreementDetails.Rows(0)("PM_LAD_START_DT_AGREEMENT")
        End If
        txtsdate.Text = agreementDetails.Rows(0)("PM_LAD_EFFE_DT_AGREEMENT")
        txtedate.Text = agreementDetails.Rows(0)("PM_LAD_EXP_DT_AGREEMENT")
        txtrentcommencementdate.Text = agreementDetails.Rows(0)("PM_LAD_RENT_DATE")
        txtlock.Text = agreementDetails.Rows(0)("PM_LAD_LOCK_INPERIOD")

        If (agreementDetails.Rows(0)("PM_LAD_LOCK_INPEDATE").ToString() <> "") Then
            Txtlockinpeamonth.Text = agreementDetails.Rows(0)("PM_LAD_LOCK_INPEDATE")
        End If
        txtLeasePeiodinYears.Text = agreementDetails.Rows(0)("PM_LAD_LEASE_PERIOD")
        txtNotiePeriod.Text = agreementDetails.Rows(0)("PM_LAD_NOTICE_PERIOD")
        ddlAgreementbyPOA.ClearSelection()
        Dim AgreementbyPOA As String = agreementDetails.Rows(0)("PM_LES_SIGNED_POA")
        ddlAgreementbyPOA.Items.FindByValue(AgreementbyPOA).Selected = True
        If ddlAgreementbyPOA.SelectedValue = "Yes" Then
            panPOA.Visible = True
            txtPOAName.Text = POA.Rows(0)("PM_POA_NAME")
            txtPOAAddress.Text = POA.Rows(0)("PM_POA_ADDRESS")
            txtPOAMobile.Text = POA.Rows(0)("PM_POA_PHONE_NO")
            txtPOAEmail.Text = POA.Rows(0)("PM_POA_EMAIL")
            txtLLtype.Text = POA.Rows(0)("PM_POA_LL_TYPE")
        Else
            panPOA.Visible = False
        End If

        'Agreement Details Section end
        BindRentRevision()

        'Brokerage Section start
        txtbrkamount.Text = Convert.ToDouble(brokerageDt.Rows(0)("PM_LBD_PAID_AMOUNT"))
        txtbrkname.Text = brokerageDt.Rows(0)("PM_LBD_NAME")
        txtbrkpan.Text = brokerageDt.Rows(0)("PM_LBD_PAN_NO")
        txtbrkmob.Text = brokerageDt.Rows(0)("PM_LBD_PHONE_NO")
        txtbrkremail.Text = brokerageDt.Rows(0)("PM_LBD_EMAIL")
        txtbrkaddr.Text = brokerageDt.Rows(0)("PM_LBD_ADDRESS")
        'Brokerage Section end
        If (LLScope.Rows.Count > 0) Then
            'landlord scope start
            txtvitrified.Text = LLScope.Rows(0)("PM_LLS_VITRIFIED")
            txtvitriremarks.Text = LLScope.Rows(0)("PM_LLS_VITRIFIED_RMKS")
            txtwashroom.Text = LLScope.Rows(0)("PM_LLS_WASHROOMS")
            txtwashroomremarks.Text = LLScope.Rows(0)("PM_LLS_WASH_RMKS")
            txtpantry.Text = LLScope.Rows(0)("PM_LLS_PANTRY")
            txtpantryremarks.Text = LLScope.Rows(0)("PM_LLS_PANTRY_RMKS")
            txtshutter.Text = LLScope.Rows(0)("PM_LLS_SHUTTER")
            txtshutterremarks.Text = LLScope.Rows(0)("PM_LLS_SHUTTER_RMKS")
            txtothers.Text = LLScope.Rows(0)("PM_LLS_OTHERS")
            txtothersremarks.Text = LLScope.Rows(0)("PM_LLS_OTHERS_RMKS")
            txtllwork.Text = LLScope.Rows(0)("PM_LLS_WORK_DAYS")
            txtelectricex.Text = LLScope.Rows(0)("PM_LLS_ELEC_EXISTING")
            txtelectricrq.Text = LLScope.Rows(0)("PM_LLS_ELEC_REQUIRED")
            'landlord scope end
        End If
        'Utility/Power back up Details Section start
        ddlDgSet.ClearSelection()
        Dim dgSetVal As String = UtilityDt.Rows(0)("PM_LUP_DGSET")
        ddlDgSet.Items.FindByValue(dgSetVal).Selected = True
        If ddlDgSet.SelectedValue = "Landlord" Then
            Dgset.Visible = True
        Else
            Dgset.Visible = False
        End If
        txtDgSetPerUnit.Text = UtilityDt.Rows(0)("PM_LUP_DGSET_COMMER_PER_UNIT")
        txtDgSetLocation.Text = UtilityDt.Rows(0)("PM_LUP_DGSET_LOCATION")
        txtSpaceServoStab.Text = UtilityDt.Rows(0)("PM_LUP_SPACE_SERVO_STABILIZER")

        ddlElectricalMeter.ClearSelection()
        Dim eleMeter As String = UtilityDt.Rows(0)("PM_LUP_ELE_METER")
        ''ddlElectricalMeter.Items.FindByValue(eleMeter).Selected = True
        If (eleMeter <> "") Then
            ddlElectricalMeter.Items.FindByValue(eleMeter).Selected = True
        End If

        If ddlElectricalMeter.SelectedValue = "Yes" Then
            Meter.Visible = True
            txtMeterLocation.Text = ""
        Else
            Meter.Visible = False
            txtMeterLocation.Text = ""
        End If
        'ddlElectricalMeter.ClearSelection()
        'Dim eleMeter As String = UtilityDt.Rows(0)("PM_LUP_ELE_METER")
        'ddlElectricalMeter.Items.FindByValue(eleMeter).Selected = True
        ''ele meter yes start

        'If ddlElectricalMeter.SelectedValue = "Yes" Then
        '    Meter.Visible = True
        '    txtMeterLocation.Text = ""
        'Else
        '    Meter.Visible = False
        '    txtMeterLocation.Text = ""
        'End If

        txtMeterLocation.Text = UtilityDt.Rows(0)("PM_LUP_MET_LOCATION")
        txtEarthingPit.Text = UtilityDt.Rows(0)("PM_LUP_EARTHING_PIT")
        txtAvailablePower.Text = UtilityDt.Rows(0)("PM_LUP_AVAIL_POWER")
        txtAdditionalPowerKWA.Text = UtilityDt.Rows(0)("PM_LUP_ADDITIONAL_POWER")
        txtPowerSpecification.Text = UtilityDt.Rows(0)("PM_LUP_POWER_SPECIFICATION")
        ' ele meter yes end

        'Utility/Power back up Details Section end

        'Other Services Section start
        txtNoOfTwoWheelerParking.Text = OtherServicesDt.Rows(0)("PM_LO_NO_TWO_PARK")
        txtNoOfCarsParking.Text = OtherServicesDt.Rows(0)("PM_LO_NO_CAR_PARK")
        txtDistanceFromAirPort.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_AIRPT")
        txtDistanceFromRailwayStation.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_RAIL")
        txtDistanceFromBustop.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_BUS")
        txtadditionalparking.Text = Convert.ToDouble(OtherServicesDt.Rows(0)("PM_LO_ADDI_PARK_CHRG"))
        'Other Services Section end


        'Other Details Section start
        txtCompetitorsVicinity.Text = OtherDetailsDt.Rows(0)("PM_LO_COMP_VICINITY")
        ddlRollingShutter.ClearSelection()
        Dim rolShutter As String = OtherDetailsDt.Rows(0)("PM_LO_ROL_SHUTTER")
        ddlRollingShutter.Items.FindByValue(rolShutter).Selected = True
        txtOfficeEquipments.Text = OtherDetailsDt.Rows(0)("PM_LO_OFF_EQUIPMENTS")
        'Other Details Section end


        If (LeaseEscalationDt.Rows.Count > 0) Then
            ddlesc.ClearSelection()
            Dim leaseEsc As String = LeaseEscalationDt.Rows(0)("PM_LES_ESC")
            If (leaseEsc = "Yes") Then
                LandlordEscalation.Visible = True
                EscalationType.Visible = True
                ''EscalationType1.Visible = True
            End If
            ''ddlesc.Items.FindByValue(leaseEsc).Selected = True
            If (leaseEsc <> "") Then
                ddlesc.Items.FindByValue(leaseEsc).Selected = True
            End If

            rblEscalationType.ClearSelection()
            rblEscalationType.SelectedValue = LeaseEscalationDt.Rows(0)("PM_LES_ESC_ON")
            Dim RblEscType As String = LeaseEscalationDt.Rows(0)("PM_LES_ESC_ON")
            If (RblEscType = "Lease") Then

                EscalationType1.Visible = True
                EscalationType2.Visible = True

                RentRevisionPanel.Visible = True
                Dim RR_lst As New List(Of RentRevision)()
                Dim rr_obj As New RentRevision()
                'For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To months1 - Convert.ToInt32(txtIntervaldu.Text)
                Dim lbtxt As TextBox
                Dim lbmnth As TextBox
                Dim lbday As TextBox
                Dim lbesc As TextBox
                Dim lbtxtr As TextBox
                For i As Integer = 0 To leaseAllSections.Tables(9).Rows.Count - 1
                    rr_obj = New RentRevision()
                    'd = Convert.ToDateTime(txtInterval).AddMonths(Convert.ToInt32(txtIntervaldu.Text))
                    rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(i + 1) + ":"
                    rr_obj.RR_Year = leaseAllSections.Tables(9).Rows(i)("PM_LES_ESC_INT_DATE")
                    rr_obj.RR_YEARS = leaseAllSections.Tables(9).Rows(i)("PM_LES_ESC_YEARS")
                    rr_obj.RR_MONTHS = leaseAllSections.Tables(9).Rows(i)("PM_LES_ESC_MONTHS")
                    rr_obj.RR_DAYS = leaseAllSections.Tables(9).Rows(i)("PM_LES_ESC_DAYS")
                    If LeaseEscalationDt.Rows(0)("PM_LES_ESC_AMOUNTIN") = "Value" Then
                        rr_obj.RR_Percentage = leaseAllSections.Tables(9).Rows(i)("PM_LES_ESC_AMTVAL")
                    Else
                        rr_obj.RR_Percentage = leaseAllSections.Tables(9).Rows(i)("PM_LES_ESC_AMTPER")
                    End If
                    rr_obj.pm_les_esc_id = leaseAllSections.Tables(9).Rows(i)("pm_les_esc_id")
                    rr_obj.validat = "1"
                    RR_lst.Add(rr_obj)

                Next
                Repeater2.DataSource = RR_lst
                Repeater2.DataBind()
                Dim Intervaltype1 As String = LeaseEscalationDt.Rows(0)("PM_LES_ESC_INT_TYPE")
                Dim coun As Integer = 0
                If (Intervaltype1 = "Flexy") Then
                    For Each item As RepeaterItem In Repeater2.Items
                        If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                            Dim lbl = CType(item.FindControl("lblRevYear"), Label)
                            lbtxtr = CType(item.FindControl("txtRevision"), TextBox)
                            lbtxt = CType(item.FindControl("ddlYear"), TextBox)
                            lbmnth = CType(item.FindControl("ddlMonth"), TextBox)
                            lbday = CType(item.FindControl("ddlDay"), TextBox)
                            lbesc = CType(item.FindControl("TextBox1"), TextBox)
                            'lbl.Visible = False
                            lbtxtr.Visible = False
                            If RR_lst(coun).validat = 1 Then
                                lbtxt.ReadOnly = True
                                lbmnth.ReadOnly = True
                                lbday.ReadOnly = True
                                lbesc.ReadOnly = True
                            End If
                            coun = coun + 1
                        End If
                    Next
                    Escflex.Visible = True
                    Escduration.Visible = False
                    txtnofEscltions.Text = LeaseEscalationDt.Rows(0)("PM_LES_ESC_INT_DUR")
                Else
                    For Each item As RepeaterItem In Repeater2.Items
                        If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                            lbtxtr = CType(item.FindControl("txtRevision"), TextBox)
                            Dim lbl = CType(item.FindControl("lblEscno"), Label)
                            lbtxt = CType(item.FindControl("ddlYear"), TextBox)
                            lbmnth = CType(item.FindControl("ddlMonth"), TextBox)
                            lbday = CType(item.FindControl("ddlDay"), TextBox)
                            lbesc = CType(item.FindControl("TextBox1"), TextBox)
                            Dim lbyr = CType(item.FindControl("lbyear"), Label)
                            Dim lbmth = CType(item.FindControl("lbmnt"), Label)
                            Dim lbdy = CType(item.FindControl("lbldy"), Label)
                            Dim lble = CType(item.FindControl("lbldf"), Label)
                            'lbl.Visible = False
                            lbtxt.Visible = False
                            lbmnth.Visible = False
                            lbesc.Visible = False
                            lbday.Visible = False
                            lbyr.Visible = False
                            lbmth.Visible = False
                            lbdy.Visible = False
                            lble.Visible = False
                            Escduration.Visible = True
                            Escflex.Visible = False
                            If RR_lst(coun).validat = 1 Then
                                lbtxtr.ReadOnly = True
                            End If
                            coun = coun + 1
                        End If
                    Next
                End If
            Else
                RentRevisionPanel.Visible = False
            End If
            ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
            If (RblEscType <> "") Then
                rblEscalationType.Items.FindByValue(RblEscType).Selected = True
            End If
            ddlEscalation.ClearSelection()
            Dim leaseEscType As String = LeaseEscalationDt.Rows(0)("PM_LES_ESC_TYPE")
            ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
            If (leaseEscType <> "") Then
                ddlEscalation.Items.FindByValue(leaseEscType).Selected = True
            End If
            ddlintervaltype.ClearSelection()
            Dim Intervaltype As String = LeaseEscalationDt.Rows(0)("PM_LES_ESC_INT_TYPE")
            ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
            If (Intervaltype <> "") Then
                ddlintervaltype.Items.FindByValue(Intervaltype).Selected = True
            End If
            ddlamount.ClearSelection()
            Dim Amountin As String = LeaseEscalationDt.Rows(0)("PM_LES_ESC_AMOUNTIN")
            ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
            If (Amountin <> "") Then
                ddlamount.Items.FindByValue(Amountin).Selected = True
            End If

            txtIntervaldu.Text = LeaseEscalationDt.Rows(0)("PM_LES_ESC_INT_DUR")
            'txtComments.Text = LeaseEscalationDt.Rows(0)("PM_LD_LES_COMMENTS")
            'ddlDueDilegence.ClearSelection()
            'Dim dueDilegence As String = LeaseDetailsDt.Rows(0)("PM_LD_DUE_DILIGENCE_CERT")
            '''ddlDueDilegence.Items.FindByValue(dueDilegence).Selected = True
            'If (dueDilegence <> "") Then
            '    ddlDueDilegence.Items.FindByValue(dueDilegence).Selected = True
            'End If


            'Lease Escalation Details Section start
            'ddlesc.ClearSelection()
            'Dim leaseEsc As String = LeaseDetailsDt.Rows(0)("PM_LD_LES_ESCALATON")
            'ddlesc.Items.FindByValue(leaseEsc).Selected = True
            'ddlLeaseEscType.ClearSelection()
            'Dim leaseEscType As String = LeaseDetailsDt.Rows(0)("PM_LD_ESCALTION_TYPE")
            'ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
            'txtLeaseHoldImprovements.Text = LeaseDetailsDt.Rows(0)("PM_LD_LES_IMPROV")
            'txtComments.Text = LeaseDetailsDt.Rows(0)("PM_LD_LES_COMMENTS")
            'ddlDueDilegence.ClearSelection()
            'Dim dueDilegence As String = LeaseDetailsDt.Rows(0)("PM_LD_DUE_DILIGENCE_CERT")
            'ddlDueDilegence.Items.FindByValue(dueDilegence).Selected = True
        End If


        Dim reminderBefore As String
        reminderBefore = LeaseDetailsDt.Rows(0).Item("PM_LRE_REMINDER_BEFORE")
        Dim parts As String() = reminderBefore.Split(New Char() {","c})
        For i As Integer = 0 To parts.Length - 1
            For j As Integer = 0 To ReminderCheckList.Items.Count - 1
                If ReminderCheckList.Items(j).Value = parts(i) Then
                    ReminderCheckList.Items(j).Selected = True
                End If
            Next
        Next

        txtBuiltupArea.Text = AreaDetails.Rows(0)("PM_AR_BUA_AREA")
        txtSuperBulArea.Text = AreaDetails.Rows(0)("PM_AR_SBU_AREA")
        txtCarpetArea.Text = AreaDetails.Rows(0)("PM_AR_CARPET_AREA")
        txtCommonArea.Text = AreaDetails.Rows(0)("PM_AR_COM_AREA")
        txtRentableArea.Text = AreaDetails.Rows(0)("PM_AR_RENT_AREA")
        txtUsableArea.Text = AreaDetails.Rows(0)("PM_AR_USABEL_AREA")
        txtPlotArea.Text = AreaDetails.Rows(0)("PM_AR_PLOT_AREA")
        txtCeilingHight.Text = AreaDetails.Rows(0)("PM_AR_FTC_HIGHT")
        txtBeamBottomHight.Text = AreaDetails.Rows(0)("PM_AR_FTBB_HIGHT")
        txtMaxCapacity.Text = AreaDetails.Rows(0)("PM_AR_MAX_CAP")
        txtOptCapacity.Text = AreaDetails.Rows(0)("PM_AR_OPT_CAP")
        txtSeatingCapacity.Text = AreaDetails.Rows(0)("PM_AR_SEATING_CAP")

        ddlFlooringType.SelectedValue = AreaDetails.Rows(0)("PM_AR_FLOOR_TYPE")
        If (AreaDetails.Rows(0)("PM_AR_FSI").ToString() = "") Then
            ddlFSI.SelectedValue = ""
        Else
            ddlFSI.SelectedValue = IIf(AreaDetails.Rows(0).Item("PM_AR_FSI") = True, 1, 0)
        End If
        'ddlFSI.SelectedValue = AreaDetails.Rows(0)("PM_AR_FSI")
        txtEfficiency.Text = AreaDetails.Rows(0)("PM_AR_PREF_EFF")
        txtFSI.Text = AreaDetails.Rows(0)("PM_AR_FSI_RATIO")

    End Sub

    Private Sub BindFlooringTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_FLOORING_TYPES")
        ddlFlooringType.DataSource = sp3.GetDataSet()
        ddlFlooringType.DataTextField = "PM_FT_TYPE"
        ddlFlooringType.DataValueField = "PM_FT_SNO"
        ddlFlooringType.DataBind()
        ddlFlooringType.Items.Insert(0, New ListItem("--Select Flooring Type--", 0))
    End Sub
    Protected Sub ddlFSI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFSI.SelectedIndexChanged
        divFSI.Visible = IIf(ddlFSI.SelectedValue = "1", True, False)
    End Sub

    Protected Sub gvLeases_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLeases.PageIndexChanging
        gvLeases.PageIndex = e.NewPageIndex()
        gvLeases.DataSource = ViewState("reqDetails")
        gvLeases.DataBind()
    End Sub

    Protected Sub gvlldata_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvlandlordItems.RowCommand
        Try
            If e.CommandName = "GetLandlordDetails" Then
                Landlord.Visible = True
                hdnLandlordSNO.Value = e.CommandArgument
                GetSelectedLandlordDetails(hdnLandlordSNO.Value)
            Else
                panel1.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GetSelectedLandlordDetails(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LANDLORD_DETAILS_BY_ID")
        sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@FLAG", 2, DbType.String)
        sp.Command.AddParameter("@HDN_LL_VALUE", Reqid, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        updatepanel.Visible = True
        Landlord.Visible = True
        SetLandlordValsToUpdate(ds)
    End Sub

    Private Sub SetLandlordValsToUpdate(ByVal landlorddt As DataSet)

        txtName.Text = landlorddt.Tables(0).Rows(0)("PM_LL_NAME")

        txtAddress.Text = landlorddt.Tables(0).Rows(0)("PM_LL_ADDRESS1")
        txtAddress2.Text = landlorddt.Tables(0).Rows(0)("PM_LL_ADDRESS2")
        txtAddress3.Text = landlorddt.Tables(0).Rows(0)("PM_LL_ADDRESS3")
        txtL1State.Text = landlorddt.Tables(0).Rows(0)("PM_LL_STATE")

        ddlCity.ClearSelection()
        Dim selectedCity As String = landlorddt.Tables(0).Rows(0)("PM_LL_CITY_CODE")
        ddlCity.Items.FindByValue(selectedCity).Selected = True
        txtld1Pin.Text = landlorddt.Tables(0).Rows(0)("PM_LL_PINCODE")
        txtPAN.Text = landlorddt.Tables(0).Rows(0)("PM_LL_PAN")
        txtldemail.Text = landlorddt.Tables(0).Rows(0)("PM_LL_EMAIL")
        ddlServiceTaxApplicable.ClearSelection()
        Dim serviceTax As String = landlorddt.Tables(0).Rows(0)("PM_LL_GST").ToString()
        If (serviceTax <> "") Then
            ddlServiceTaxApplicable.Items.FindByValue(serviceTax).Selected = True
        End If

        txtServiceTaxlnd.Text = landlorddt.Tables(0).Rows(0)("PM_LL_GST_NO").ToString()
        ddlPropertyTaxApplicable.ClearSelection()
        Dim propertyTax As String = landlorddt.Tables(0).Rows(0)("PM_LL_PPTAX_APPLICABLE")
        If (propertyTax <> "") Then
            ddlPropertyTaxApplicable.Items.FindByValue(propertyTax).Selected = True
        End If
        ddltds.ClearSelection()
        Dim TDS As String = landlorddt.Tables(0).Rows(0)("PM_LL_TDS")
        If (TDS <> "") Then
            ddltds.Items.FindByValue(TDS).Selected = True
            txttdsper.Text = landlorddt.Tables(0).Rows(0)("PM_LL_TDS_PERCENTAGE")
        End If
        txtPropertyTax.Text = landlorddt.Tables(0).Rows(0)("PM_LL_PROPERTY_TAX")
        txtContactDetails.Text = landlorddt.Tables(0).Rows(0)("PM_LL_PHONE_NO")
        ddlAmountIn.ClearSelection()
        Dim amountIn As String = landlorddt.Tables(0).Rows(0)("PM_LL_AMOUNT_IN")
        If (amountIn <> "") Then
            ddlAmountIn.Items.FindByValue(amountIn).Selected = True
        End If
        txtpmonthrent.Text = Convert.ToDouble(landlorddt.Tables(0).Rows(0)("PM_LL_MON_RENT_PAYABLE"))
        txtpsecdep.Text = Convert.ToDouble(landlorddt.Tables(0).Rows(0)("PM_LL_SECURITY_DEPOSIT"))

        'ddlpaymentmode.ClearSelection()
        'If Not IsDBNull(landlorddt.Tables(0).Rows(0)("PM_LL_PAYMENT_MODE")) Then
        '    sdr2.Value = dt.Rows(0)("name")
        'End If
        ddlpaymentmode.ClearSelection()
        Dim paymentMode As String = landlorddt.Tables(0).Rows(0)("PM_LL_PAYMENT_MODE")
        If (paymentMode <> "") Then
            ddlpaymentmode.Items.FindByValue(paymentMode).Selected = True
        End If
        txtBankName.Text = landlorddt.Tables(0).Rows(0)("PM_LL_BANK")
        txtAccNo.Text = landlorddt.Tables(0).Rows(0)("PM_LL_ACC_NO")
        'NEFT Transfer
        txtNeftBank.Text = landlorddt.Tables(0).Rows(0)("PM_LL_BANK")
        txtNeftAccNo.Text = landlorddt.Tables(0).Rows(0)("PM_LL_ACC_NO")
        txtNeftBrnch.Text = landlorddt.Tables(0).Rows(0)("PM_LL_BRANCH")
        txtNeftIFSC.Text = landlorddt.Tables(0).Rows(0)("PM_LL_IFSC")
        txtNeftAccNo.Text = landlorddt.Tables(0).Rows(0)("PM_LL_ACC_NO")
        txtcheqno.Text = Convert.ToString(landlorddt.Tables(0).Rows(0)("PM_LL_CHEQDDNO"))
        txtchedddt.Text = landlorddt.Tables(0).Rows(0)("PM_LL_CHEQDDDT")
        txtllmaint.Text = landlorddt.Tables(0).Rows(0)("PM_LL_MAINT_CHARGES")
        txtllAments.Text = landlorddt.Tables(0).Rows(0)("PM_LL_AMENITIES_CHARGES")
        ddlrenttype.ClearSelection()
        Dim renttype As String = landlorddt.Tables(0).Rows(0)("PM_LL_RENTTYPE")
        If (renttype <> "") Then
            ddlrenttype.Items.FindByValue(renttype).Selected = True
        End If
        If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
            panel1.Visible = True
            panel2.Visible = False
        ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
            panel1.Visible = False
            panel2.Visible = True
        Else
            panel1.Visible = False
            panel2.Visible = False
        End If

        llesctiondetils.Visible = False
        'Lease Escalation Details Section start

        If (landlorddt.Tables(1).Rows.Count > 0) Then

            If (landlorddt.Tables(1).Rows(0)("PM_LES_ESC_ON") = "LandLoard") Then
                llesctiondetils.Visible = True
                ddllandloardescalation.ClearSelection()
                Dim LLleaseEsc As String = landlorddt.Tables(1).Rows(0)("PM_LES_ESC")
                'If (LLleaseEsc = "Yes") Then
                '    LandlordEscalation.Visible = True
                '    EscalationType.Visible = True
                '    ''EscalationType1.Visible = True
                'End If
                ''ddlesc.Items.FindByValue(leaseEsc).Selected = True
                If (LLleaseEsc <> "") Then
                    ddllandloardescalation.Items.FindByValue(LLleaseEsc).Selected = True
                End If

                'rblEscalationType.ClearSelection()
                'rblEscalationType.SelectedValue = landlorddt.Tables(0).Rows(0)("PM_LES_ESC_ON")
                'Dim RblEscType As String = landlorddt.Tables(0).Rows(0)("PM_LES_ESC_ON")
                'If (RblEscType = "Lease") Then

                '    EscalationType1.Visible = True
                '    EscalationType2.Visible = True
                'End If
                '''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
                'If (RblEscType <> "") Then
                '    rblEscalationType.Items.FindByValue(RblEscType).Selected = True
                'End If
                ddllandloardescaltype.ClearSelection()
                Dim LLleaseEscType As String = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_TYPE")
                ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
                If (LLleaseEscType <> "") Then
                    ddllandloardescaltype.Items.FindByValue(LLleaseEscType).Selected = True
                End If
                ddllandloardinterval.ClearSelection()
                Dim LLIntervaltype As String = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_INT_TYPE")
                ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
                If (LLIntervaltype <> "") Then
                    ddllandloardinterval.Items.FindByValue(LLIntervaltype).Selected = True
                End If
                ddllandloardescAmount.ClearSelection()
                Dim LLAmountin As String = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_AMOUNTIN")
                ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
                If (LLAmountin <> "") Then
                    ddllandloardescAmount.Items.FindByValue(LLAmountin).Selected = True
                End If

                'txtescduration.Text = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_INT_DUR")


                Dim RblEscType As String = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_ON")
                If (RblEscType = "LandLord") Then
                    Landloardescalation.Visible = True
                    Landloardescalation1.Visible = True
                    LandloardRentRevisionPanel.Visible = True
                    Dim RR_lst As New List(Of RentRevision)()
                    Dim rr_obj As New RentRevision()
                    ''For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To months1 - Convert.ToInt32(txtIntervaldu.Text)
                    For i As Integer = 0 To landlorddt.Tables(1).Rows.Count - 1
                        rr_obj = New RentRevision()
                        'd = Convert.ToDateTime(txtInterval).AddMonths(Convert.ToInt32(txtIntervaldu.Text))
                        rr_obj.RR_Year = landlorddt.Tables(1).Rows(i)("PM_LES_ESC_INT_DATE")
                        rr_obj.RR_YEARS = landlorddt.Tables(1).Rows(i)("PM_LES_ESC_YEARS")
                        rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(i + 1) + ":"
                        rr_obj.RR_MONTHS = landlorddt.Tables(1).Rows(i)("PM_LES_ESC_MONTHS")
                        rr_obj.RR_DAYS = landlorddt.Tables(1).Rows(i)("PM_LES_ESC_DAYS")
                        If ddllandloardescAmount.SelectedValue = "Value" Then
                            rr_obj.RR_Percentage = landlorddt.Tables(1).Rows(i)("PM_LES_ESC_AMTVAL")
                        Else
                            rr_obj.RR_Percentage = landlorddt.Tables(1).Rows(i)("PM_LES_ESC_AMTPER")
                        End If
                        rr_obj.pm_les_esc_id = landlorddt.Tables(1).Rows(i)("pm_les_esc_id")
                        RR_lst.Add(rr_obj)
                    Next
                    Repeater1.DataSource = RR_lst
                    Repeater1.DataBind()
                    Dim Intervaltype1 As String = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_INT_TYPE")
                    If (Intervaltype1 = "Flexy") Then
                        For Each item As RepeaterItem In Repeater1.Items
                            If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                                Dim lbl = CType(item.FindControl("lblRevYear"), Label)
                                Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
                                lbl.Visible = False
                                lbtxt.Visible = False
                            End If
                        Next
                        Landloardescalation1.Visible = True
                        LLescduration.Visible = False
                        txtnoescduration.Text = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_INT_DUR")
                    Else
                        For Each item As RepeaterItem In Repeater1.Items
                            If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
                                'Dim lbl = CType(item.FindControl("lblEscno"), Label)
                                Dim lbtxt = CType(item.FindControl("ddlYear"), TextBox)
                                Dim lbmnth = CType(item.FindControl("ddlMonth"), TextBox)
                                Dim lbday = CType(item.FindControl("ddlDay"), TextBox)
                                Dim lbesc = CType(item.FindControl("TextBox1"), TextBox)
                                Dim lbyr = CType(item.FindControl("lbyear"), Label)
                                Dim lbmth = CType(item.FindControl("lbmnt"), Label)
                                Dim lbdy = CType(item.FindControl("lbldy"), Label)
                                Dim lble = CType(item.FindControl("lbldf"), Label)
                                'lbl.Visible = False
                                lbtxt.Visible = False
                                lbmnth.Visible = False
                                lbesc.Visible = False
                                lbday.Visible = False
                                lbyr.Visible = False
                                lbmth.Visible = False
                                lbdy.Visible = False
                                lble.Visible = False
                                LLescduration.Visible = True
                                Landloardescalation1.Visible = False
                            End If
                        Next
                        txtllescdurtn.Text = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_INT_DUR")
                    End If
                End If
            End If
        End If

    End Sub

    Protected Sub rblCostType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblCostType.SelectedIndexChanged
        If rblCostType.SelectedValue = "Sqft" Then
            Costype1.Visible = True
            Costype2.Visible = False
            txtSeatCost.Text = ""
        ElseIf rblCostType.SelectedValue = "Seat" Then
            Costype1.Visible = False
            Costype2.Visible = True
            txtRentPerSqftCarpet.Text = ""
            txtRentPerSqftBUA.Text = ""
        End If
    End Sub

    Protected Sub ddlAgreementbyPOA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAgreementbyPOA.SelectedIndexChanged
        If ddlAgreementbyPOA.SelectedValue = "Yes" Then
            panPOA.Visible = True
        Else
            panPOA.Visible = False
        End If
    End Sub

    Protected Sub ddlpaymentmode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlpaymentmode.SelectedIndexChanged
        If ddlpaymentmode.SelectedIndex > 0 Then
            If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
                panel1.Visible = True
                panel2.Visible = False
            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                panel1.Visible = False
                panel2.Visible = True
            Else
                panel1.Visible = False
                panel2.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtSearch.Text = String.Empty
        updatepanel.Visible = False
        BindGrid()
    End Sub

    Private Sub MultiApproval()
        Try
            Dim LES_APPR_lst As New List(Of L1Approval)()
            Dim LES_APPR_obj As New L1Approval()
            For Each row As GridViewRow In gvLeases.Rows
                LES_APPR_obj = New L1Approval()
                Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                Dim lblLeaseID As Label = DirectCast(row.FindControl("lblLeaseID"), Label)
                Dim lblLesSno As Label = DirectCast(row.FindControl("lblLesSno"), Label)
                If chkselect.Checked = True Then
                    LES_APPR_obj.Lease_ID = lblLeaseID.Text
                    LES_APPR_obj.Lease_SNO = lblLesSno.Text
                    LES_APPR_obj.L1_REMARKS = txtRemarks.Text
                    LES_APPR_lst.Add(LES_APPR_obj)
                End If
            Next

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@LEASEAPPRLIST", SqlDbType.Structured)
            param(0).Value = UtilityService.ConvertToDataTable(LES_APPR_lst)
            param(1) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(1).Value = Session("Uid").ToString
            param(2) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(2).Value = Session("COMPANYID")

            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_APPROVE_MULTI_LEASES_L1", param)
                While sdr.Read()
                    If sdr("result").ToString() = "SUCCESS" Then

                        Dim mailParam(5) As SqlParameter
                        For i As Integer = 0 To LES_APPR_lst.Count - 1
                            mailParam(0) = New SqlParameter("@APR_AUR_ID", SqlDbType.VarChar)
                            mailParam(0).Value = Session("UID").ToString()
                            mailParam(1) = New SqlParameter("@APPREJ", SqlDbType.VarChar)
                            mailParam(1).Value = "Approved"
                            mailParam(2) = New SqlParameter("@LEVEL", SqlDbType.VarChar)
                            mailParam(2).Value = "Level1 "
                            mailParam(3) = New SqlParameter("@REMARKS", SqlDbType.VarChar)
                            mailParam(3).Value = txtRemarks.Text
                            mailParam(4) = New SqlParameter("@COMPANYID", SqlDbType.Int)
                            mailParam(4).Value = Session("COMPANYID")
                            mailParam(5) = New SqlParameter("@LEASEID", SqlDbType.VarChar)
                            mailParam(5).Value = LES_APPR_lst(i).Lease_ID
                            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_SENDMAIL_LEASE_APPREJ", mailParam)
                        Next

                        lblMsg.Text = "Lease Request Approved Successfully "
                        txtRemarks.Text = ""
                    Else
                        lblMsg.Text = "Something went wrong. Please try again later."
                    End If
                End While
                sdr.Close()
            End Using

            BindGrid()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnAppall_Click(sender As Object, e As EventArgs) Handles btnAppall.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        MultiApproval()
    End Sub

    Private Sub MultiReject()
        Try
            Dim LES_APPR_lst As New List(Of L1Approval)()
            Dim LES_APPR_obj As New L1Approval()
            For Each row As GridViewRow In gvLeases.Rows
                LES_APPR_obj = New L1Approval()
                Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                Dim lblLeaseID As Label = DirectCast(row.FindControl("lblLeaseID"), Label)
                Dim lblLesSno As Label = DirectCast(row.FindControl("lblLesSno"), Label)
                If chkselect.Checked = True Then
                    LES_APPR_obj.Lease_ID = lblLeaseID.Text
                    LES_APPR_obj.Lease_SNO = lblLesSno.Text
                    LES_APPR_obj.L1_REMARKS = txtRemarks.Text
                    LES_APPR_lst.Add(LES_APPR_obj)
                End If
            Next

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@LEASEAPPRLIST", SqlDbType.Structured)
            param(0).Value = UtilityService.ConvertToDataTable(LES_APPR_lst)
            param(1) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(1).Value = Session("Uid").ToString
            param(2) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(2).Value = Session("COMPANYID")

            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_REJECT_MULTI_LEASES_L1", param)
                While sdr.Read()
                    If sdr("result").ToString() = "SUCCESS" Then
                        Dim mailParam(5) As SqlParameter
                        For i As Integer = 0 To LES_APPR_lst.Count - 1
                            mailParam(0) = New SqlParameter("@APR_AUR_ID", SqlDbType.VarChar)
                            mailParam(0).Value = Session("UID").ToString()
                            mailParam(1) = New SqlParameter("@APPREJ", SqlDbType.VarChar)
                            mailParam(1).Value = "Rejected"
                            mailParam(2) = New SqlParameter("@LEVEL", SqlDbType.VarChar)
                            mailParam(2).Value = "Level1 "
                            mailParam(3) = New SqlParameter("@REMARKS", SqlDbType.VarChar)
                            mailParam(3).Value = txtRemarks.Text
                            mailParam(4) = New SqlParameter("@COMPANYID", SqlDbType.Int)
                            mailParam(4).Value = Session("COMPANYID")
                            mailParam(5) = New SqlParameter("@LEASEID", SqlDbType.VarChar)
                            mailParam(5).Value = LES_APPR_lst(i).Lease_ID
                            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_SENDMAIL_LEASE_APPREJ", mailParam)
                        Next
                        lblMsg.Text = "Lease Request Rejected Successfully "
                        txtRemarks.Text = ""
                    Else
                        lblMsg.Text = "Something went wrong. Please try again later."
                    End If
                End While
                sdr.Close()
            End Using

            BindGrid()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnRejAll_Click(sender As Object, e As EventArgs) Handles btnRejAll.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        MultiReject()
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Landlord.Visible = False
    End Sub

    Private Sub SingleApproval()
        Try
            Dim param(4) As SqlParameter
            param(0) = New SqlParameter("@L1_REMARKS", SqlDbType.VarChar)
            param(0).Value = txtRemarksSingle.Text
            param(1) = New SqlParameter("@REQ_ID", SqlDbType.VarChar)
            param(1).Value = Session("REQ_ID").ToString
            param(2) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(2).Value = Session("Uid").ToString
            param(3) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(3).Value = Session("COMPANYID")
            param(4) = New SqlParameter("@LES_SNO", SqlDbType.Int)
            param(4).Value = hdnLSNO.Value


            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_APPROVE_SINGLE_LEASES_L1", param)
                While sdr.Read()
                    If sdr("result").ToString() = "SUCCESS" Then
                        Dim mailParam(5) As SqlParameter
                        mailParam(0) = New SqlParameter("@APR_AUR_ID", SqlDbType.VarChar)
                        mailParam(0).Value = Session("UID").ToString()
                        mailParam(1) = New SqlParameter("@APPREJ", SqlDbType.VarChar)
                        mailParam(1).Value = "Approved"
                        mailParam(2) = New SqlParameter("@LEVEL", SqlDbType.VarChar)
                        mailParam(2).Value = "Level1 "
                        mailParam(3) = New SqlParameter("@REMARKS", SqlDbType.VarChar)
                        mailParam(3).Value = txtRemarksSingle.Text
                        mailParam(4) = New SqlParameter("@COMPANYID", SqlDbType.Int)
                        mailParam(4).Value = Session("COMPANYID")
                        mailParam(5) = New SqlParameter("@LEASEID", SqlDbType.VarChar)
                        mailParam(5).Value = Session("REQ_ID")
                        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_SENDMAIL_LEASE_APPREJ", mailParam)
                        lblMsg.Text = "Lease Request Approved Successfully: " + Session("REQ_ID").ToString
                        txtRemarksSingle.Text = ""
                        updatepanel.Visible = False
                        pnlSingle.Visible = False
                        pnlBulk.Visible = True
                    Else
                        lblMsg.Text = "Something went wrong. Please try again later."
                    End If
                End While
                sdr.Close()
            End Using

            BindGrid()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        SingleApproval()
    End Sub

    Private Sub SingleReject()
        Try
            Dim param(4) As SqlParameter
            param(0) = New SqlParameter("@L1_REMARKS", SqlDbType.VarChar)
            param(0).Value = txtRemarksSingle.Text
            param(1) = New SqlParameter("@REQ_ID", SqlDbType.VarChar)
            param(1).Value = Session("REQ_ID").ToString
            param(2) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(2).Value = Session("Uid").ToString
            param(3) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(3).Value = Session("COMPANYID")
            param(4) = New SqlParameter("@LES_SNO", SqlDbType.Int)
            param(4).Value = hdnLSNO.Value

            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_REJECT_SINGLE_LEASES_L1", param)
                While sdr.Read()
                    If sdr("result").ToString() = "SUCCESS" Then
                        Dim mailParam(5) As SqlParameter
                        mailParam(0) = New SqlParameter("@APR_AUR_ID", SqlDbType.VarChar)
                        mailParam(0).Value = Session("UID").ToString()
                        mailParam(1) = New SqlParameter("@APPREJ", SqlDbType.VarChar)
                        mailParam(1).Value = "Rejected"
                        mailParam(2) = New SqlParameter("@LEVEL", SqlDbType.VarChar)
                        mailParam(2).Value = "Level1 "
                        mailParam(3) = New SqlParameter("@REMARKS", SqlDbType.VarChar)
                        mailParam(3).Value = txtRemarksSingle.Text
                        mailParam(4) = New SqlParameter("@COMPANYID", SqlDbType.Int)
                        mailParam(4).Value = Session("COMPANYID")
                        mailParam(5) = New SqlParameter("@LEASEID", SqlDbType.VarChar)
                        mailParam(5).Value = Session("REQ_ID")
                        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_SENDMAIL_LEASE_APPREJ", mailParam)
                        lblMsg.Text = "Lease Request Rejected Successfully: " + Session("REQ_ID").ToString
                        txtRemarksSingle.Text = ""
                        updatepanel.Visible = False
                        pnlSingle.Visible = False
                        pnlBulk.Visible = True
                    Else
                        lblMsg.Text = "Something went wrong. Please try again later."
                    End If
                End While
                sdr.Close()
            End Using

            BindGrid()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        SingleReject()
    End Sub

    Private Sub grdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.ItemCommand
        If e.CommandName = "Download" Then
            Dim pm_ldoc_sno = grdDocs.DataKeys(e.Item.ItemIndex)
            e.Item.BackColor = Drawing.Color.LightSteelBlue
            Dim filePath As String
            filePath = grdDocs.Items(e.Item.ItemIndex).Cells(1).Text
            filePath = "~\Images\Property_Images\" & filePath
            Response.ContentType = "application/CSV"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "~\Images\Property_Images", "") & """")
            Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If
    End Sub


    Public Sub BindDocuments()
        Dim dtDocs As New DataTable("Documents")
        param = New SqlParameter(1) {}
        param(0) = New SqlParameter("@PM_LDOC_PM_LES_SNO", SqlDbType.NVarChar, 50)
        param(0).Value = hdnLSNO.Value
        param(1) = New SqlParameter("@COMPANYID", SqlDbType.Int)
        param(1).Value = Session("COMPANYID")
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("PM_GET_LEASE_DOCS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdDocs.DataSource = ds
            grdDocs.DataBind()
            tblGridDocs.Visible = True
            lblDocsMsg.Text = ""
        Else
            tblGridDocs.Visible = False
            lblMsg.Visible = True
            lblDocsMsg.Text = "No Documents Available"
        End If
        dtDocs = Nothing
    End Sub
    Protected Sub gvlandlordItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvlandlordItems.RowCommand
        Try
            If e.CommandName = "GetLandlordDetails" Then
                Landlord.Visible = True
                hdnLandlordSNO.Value = e.CommandArgument
                GetSelectedLandlordDetails(hdnLandlordSNO.Value)

                BindLLDocuments()
            Else
                panel1.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ddltds_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddltds.SelectedIndexChanged

        '' TDSDIV.Visible = IIf(ddltds.SelectedValue = "1", True, False)
        If ddltds.SelectedValue = "1" Then
            TDSDIV.Visible = True
        Else
            TDSDIV.Visible = False
            txttdsper.Text = 0
        End If
    End Sub
    Public Sub BindLLDocuments()
        Dim dtDocs As New DataTable("Documents")
        param = New SqlParameter(1) {}
        param(0) = New SqlParameter("@HDN_LL_VALUE", SqlDbType.NVarChar, 50)
        param(0).Value = hdnLandlordSNO.Value
        param(1) = New SqlParameter("@COMPANY_ID", SqlDbType.Int)
        param(1).Value = Session("COMPANYID")
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("PM_GET_LEASE_LL_DOCS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lldocgrid.DataSource = ds
            lldocgrid.DataBind()
            Div3.Visible = True
            lblmesge.Text = ""
        Else
            Div3.Visible = False
            lblMsg.Visible = True
            lblmesge.Text = "No Documents Available"
        End If
        dtDocs = Nothing
    End Sub

    Private Sub lldocgrid_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lldocgrid.DeleteCommand
        If lldocgrid.EditItemIndex = -1 Then
            Dim Bid As Integer
            Bid = lldocgrid.DataKeys(e.Item.ItemIndex)
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@PM_LL_DOC_SNO", SqlDbType.NVarChar, 50)
            param(0).Value = Bid
            ObjSubSonic.GetSubSonicDataSet("PM_DELETE_LEASE_LL_DOCS", param)
            BindLLDocuments()
        End If
    End Sub

    Private Sub lldocgrid_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lldocgrid.ItemCommand
        If e.CommandName = "Download" Then
            Dim pm_ldoc_sno = lldocgrid.DataKeys(e.Item.ItemIndex)
            e.Item.BackColor = Drawing.Color.LightSteelBlue
            Dim filePath As String
            filePath = lldocgrid.Items(e.Item.ItemIndex).Cells(1).Text
            filePath = "~\Images\Property_Images\" & filePath
            Response.ContentType = "application/CSV"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "~\Images\Property_Images", "") & """")
            Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If
    End Sub
End Class
