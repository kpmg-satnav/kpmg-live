<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddTenants.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_AddTenants" Title="Add Tenants" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>

    <script lang="javascript" type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Add Tenant" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Add Tenant
                                <a href="#" class="btn btn-default closeall pull-right" style="padding-bottom: initial;" title="Collapse All"><i class="fa fa-minus" aria-hidden="true"></i></a>
                        <a href="#" class="btn btn-default openall pull-right" style="padding-bottom: initial;" title="Expand All"><i class="fa fa-plus" aria-hidden="true"></i></a>

                    </h3>
                </div>

                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" DisplayMode="List" Style="padding-bottom: 20px" ForeColor="Red" />
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="panel panel-default " role="tab" runat="server" id="div0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-multiselectable="true">Property Details</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body color">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Property Type <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                                                            Display="None" ErrorMessage="Please Select Property Type" ValidationGroup="Val1"
                                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                        <div class="col-md-12">
                                                            <asp:DropDownList ID="ddlproptype" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">City <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                        <div class="col-md-12">
                                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Location <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                        <div class="col-md-12">
                                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true">
                                                                <asp:ListItem Value="">--Select--</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Property <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="cvbuilding" runat="server" ControlToValidate="ddlBuilding"
                                                            Display="None" ErrorMessage="Please Select Property" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                        <div class="col-md-12">
                                                            <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                                <asp:ListItem Value="">--Select--</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Tenant Occupied Area (Sqft)<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfarea" runat="server" ControlToValidate="txtTenantOccupiedArea"
                                                            Display="None" ErrorMessage="Please Enter Tenant Occupied Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revarea" runat="server" ControlToValidate="txtTenantOccupiedArea"
                                                            ErrorMessage="Please Enter Valid Tenant Occupied Area in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                                                            Display="None" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                                        <div class="col-md-12">
                                                            <div onmouseover="Tip('Enter Tenant Occupied Area in numbers upto 2 decimal places.')"
                                                                onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtTenantOccupiedArea" runat="server" CssClass="form-control"
                                                                    MaxLength="20"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Subject of Agreement</label>
                                                        <div class="col-md-12">
                                                            <asp:TextBox ID="txtSubject" Rows="3" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Purpose of Agreement</label>
                                                        <div class="col-md-12">
                                                            <asp:TextBox ID="txtPurpose" runat="server" Rows="3" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Tenant From Date <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TntFrm"
                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Tenant From Date"></asp:RequiredFieldValidator>
                                                        <div class="col-md-12">
                                                            <div class='input-group date' id='TFrm'>
                                                                <asp:TextBox ID="TntFrm" runat="server" CssClass="form-control"></asp:TextBox>
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar" onclick="setup('TFrm')"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Tenant End Date <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvPayDate" runat="server" ControlToValidate="txtPayableDate"
                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Tenant End Date"></asp:RequiredFieldValidator>
                                                        <div class="col-md-12">
                                                            <div class='input-group date' id='Payabledate'>
                                                                <asp:TextBox ID="txtPayableDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar" onclick="setup('Payabledate')"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default " role="tab" runat="server" id="div1">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Tenant Details</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse in">
                                        <div class="panel-body color">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Tenant <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="cvuser" runat="server" ControlToValidate="ddluser"
                                                            Display="None" ErrorMessage="Please Select User" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                        <div class="col-md-12">
                                                            <asp:DropDownList ID="ddluser" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Tenant Code </label>
                                                        <%--<span style="color: red;">*</span>--%>
                                                        <%--<asp:RequiredFieldValidator ID="rfvtcode" runat="server" ControlToValidate="txttcode"
                                                            Display="None" ErrorMessage="Please Enter Tenant Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                        <div class="col-md-12">
                                                            <asp:TextBox ID="txttcode" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Parking Spaces</label>
                                                        <asp:RegularExpressionValidator ID="revNoofParking" ValidationGroup="Val1" runat="server"
                                                            Display="none" ControlToValidate="txtNoofparking" ErrorMessage="Please Enter Valid Number of Parking"
                                                            ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                        <div class="col-md-12">
                                                            <div onmouseover="Tip('Enter numbers only with maximum length 20')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtNoofparking" runat="server" CssClass="form-control"
                                                                    MaxLength="20"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default " role="tab" runat="server" id="div2">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Payment Details</a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse in">
                                        <div class="panel-body color">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Tenant Rent <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfRent" runat="server" ControlToValidate="txtRent"
                                                            Display="None" ErrorMessage="Please Enter Tenant Rent" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revRent" runat="server" ControlToValidate="txtRent"
                                                            Display="None" ErrorMessage="Please Enter Valid Tenant Rent in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                                                            ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                                        <div class="col-md-12">
                                                            <div onmouseover="Tip('Enter tenant rent in numbers upto 2 decimal places.')"
                                                                onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtRent" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="20"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Security Deposit</label>
                                                        <%-- <asp:RequiredFieldValidator ID="rfdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                                                            Display="None" ErrorMessage="Please Enter Security Deposit" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                        <asp:RegularExpressionValidator ID="revdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                                                            Display="None" ErrorMessage="Please Enter Valid Security Deposit in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                                                            ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                                        <div class="col-md-12">
                                                            <div onmouseover="Tip('Enter Security Deposit only in numbers upto 2 decimal places.')"
                                                                onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtSecurityDeposit" runat="server" CssClass="form-control"
                                                                    MaxLength="20"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Joining Date</label>
                                                        <%--  <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Joining Date"></asp:RequiredFieldValidator>--%>
                                                        <div class="col-md-12">
                                                            <div class='input-group date' id='fromdate'>
                                                                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Payment Terms <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvpayment" runat="server" ControlToValidate="ddlPaymentTerms"
                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Payment Terms"
                                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                        <div class="col-md-12">
                                                            <asp:DropDownList ID="ddlPaymentTerms" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                                <asp:ListItem>--Select--</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Society Maintenance / CAM</label>
                                                        <asp:RegularExpressionValidator ID="revfees" runat="server" ControlToValidate="txtfees"
                                                            Display="none" ErrorMessage="Please Enter Valid Maintenance fees in Numbers or Decimal Number with 2 Decimal Places." ValidationExpression="((\d+)((\.\d{1,2})?))$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                        <div class="col-md-12">
                                                            <div onmouseover="Tip('Enter Maintenance Fees in numbers upto 2 decimal places.')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtfees" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="20"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Additional Car Parking Fees</label>
                                                        <div class="col-md-12">
                                                            <asp:TextBox ID="txtcar" runat="server" AutoPostBack="true" CssClass="form-control" Rows="3" MaxLength="500"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Total Rent Amount <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvamt" runat="server" ControlToValidate="txtamount"
                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Total Rent Amount."></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="regexpamount" runat="server" ControlToValidate="txtamount" Display="None"
                                                            ValidationGroup="Val1" ErrorMessage="Please Enter Total Rent Amount in Numbers or Decimal Number with 2 Decimal Places" ValidationExpression="((\d+)((\.\d{1,2})?))$">

                                                        </asp:RegularExpressionValidator>
                                                        <div class="col-md-12">
                                                            <div onmouseover="Tip('Enter Total Rent Amount in numbers upto 2 decimal places.')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label">Remarks <span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                                            Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                        <div class="col-md-12">
                                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%"
                                                                Rows="3" MaxLength="500"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default " role="tab" runat="server" id="div3">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Tenant Reminder</a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse in">
                                        <div class="panel-body color">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Tenant Reminders For</label>
                                                    <div class="col-md-12" style="width: 500px;">
                                                        <div class="bootstrap-tagsinput">
                                                            <asp:CheckBoxList ID="TntRem" runat="server" CellPadding="25" CellSpacing="100" RepeatColumns="6" RepeatLayout="Table" RepeatDirection="Vertical" CausesValidation="True">
                                                                <asp:ListItem Value="10">10 Days</asp:ListItem>
                                                                <asp:ListItem Value="3">3 Days</asp:ListItem>
                                                                <asp:ListItem Value="7">7 Days</asp:ListItem>
                                                                <asp:ListItem Value="2">2 Days</asp:ListItem>
                                                                <asp:ListItem Value="5">5 Days</asp:ListItem>
                                                                <asp:ListItem Value="1">1 Day</asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-right" style="padding-top: 5px">
                                    <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript">
        $(document).ready(function () {
            //$("#txtRent").change(function () {  // this is your event
            //    alert('hi');
            //    $('.panel-collapse collapse out').addclass('panel-collapse collapse in');
            //    $("#collapseThree").addClass("panel-collapse collapse in");     // here your adding the new class
            //});
            $("#txtRent").change(function () {
                $('.openall').click(function () {
                    $('.panel-collapse:not(".in")')
                      .collapse('show');
                });
            });
        });
    </script>
    <script>
        $('.closeall').click(function () {
            $('.panel-collapse.in')
              .collapse('hide');
        });
        $('.openall').click(function () {
            $('.panel-collapse:not(".in")')
              .collapse('show');
        });
    </script>
    <script>
        function refreshSelectpicker() {
            $("#<%=ddlBuilding.ClientID%>").selectpicker();
              $("#<%=ddlCity.ClientID%>").selectpicker();
              $("#<%=ddlLocation.ClientID%>").selectpicker();
              $("#<%=ddlPaymentTerms.ClientID%>").selectpicker();
              $("#<%=ddlproptype.ClientID%>").selectpicker();
              $("#<%=ddluser.ClientID%>").selectpicker();

          }
          refreshSelectpicker();


    </script>
</body>
</html>


