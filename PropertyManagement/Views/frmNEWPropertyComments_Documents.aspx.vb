Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_frmNEWPropertyComments_Documents
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then
            scriptManager.RegisterPostBackControl(btnSubmit)
        End If
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            Try
                BindPropType()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select Property Type--", "0"))
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        AddRemarks()
        BindInnerGridView()
    End Sub

    Private Sub AddRemarks()
        Dim UploadFilesName As String
        'UploadFilesName = UploadFiles()
        Dim filePath As String = ""
        Dim FILE_PATH As String = ""
        Dim orgfilename As String = ""
        Dim repdocdatetime As String = ""
        Try
            If (fpBrowseDoc.PostedFile IsNot Nothing) Then
                orgfilename = fpBrowseDoc.FileName
                repdocdatetime = orgfilename
                Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                filePath = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Upload_Time & "_" & repdocdatetime
                'filePath = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetime
                fpBrowseDoc.PostedFile.SaveAs(filePath)
                FILE_PATH = Upload_Time & "_" & repdocdatetime
            End If
            UploadFilesName = repdocdatetime
            lblMsg.Text = "Document Uploaded Successfully"
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@PROPERTY_ID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlProperty.SelectedItem.Value
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("uid")
        param(2) = New SqlParameter("@COMMENTS", SqlDbType.NVarChar, 200)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@FilesName", SqlDbType.NVarChar, 200)
        param(3).Value = FILE_PATH
        param(4) = New SqlParameter("@COMPANYID", SqlDbType.Int)
        param(4).Value = Session("COMPANYID")
        'param(5) = New SqlParameter("@FILEPATH", SqlDbType.NVarChar, 800)
        'param(5).Value = FILE_PATH
        ObjSubSonic.GetSubSonicExecuteScalar("PM_UPLOAD_PROPERTY", param)
    End Sub

    Protected Sub ddlPropertyType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPropertyType.SelectedIndexChanged
        gvRemarks.Visible = False
        lblMsg.Text = ""
        txtRemarks.Text = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTIES")
        sp.Command.AddParameter("@PROPERTYTYPE", ddlPropertyType.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.Int32)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        ddlProperty.DataSource = ds
        ddlProperty.DataTextField = "PM_PPT_NAME"
        ddlProperty.DataValueField = "PM_PPT_SNO"
        ddlProperty.DataBind()
        ddlProperty.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlProperty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProperty.SelectedIndexChanged
        BindInnerGridView()
    End Sub

    Private Sub BindInnerGridView()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@PROPERTY", SqlDbType.NVarChar, 200)
        param(0).Value = ddlProperty.SelectedItem.Value
        ObjSubSonic.BindGridView(gvRemarks, "PM_GET_PROPERTY_DOCUMENTS", param)
        If gvRemarks.Rows.Count > 0 Then
            gvRemarks.Visible = True
        Else
            gvRemarks.Visible = False
        End If
    End Sub

    Protected Sub gvRemarks_RowCommand1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        If e.CommandName = "Delete" Then
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@COM_ID", SqlDbType.NVarChar, 200)
            param(0).Value = CInt(e.CommandArgument.ToString())
            ObjSubSonic.GetSubSonicExecuteScalar("DELETE_PROP_DOCS", param)
            BindInnerGridView()
        End If
    End Sub

    Protected Sub gvRemarks_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)

    End Sub
    Protected Sub gvRemarks_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRemarks.PageIndexChanging
        gvRemarks.PageIndex = e.NewPageIndex()
        BindInnerGridView()
    End Sub
    Protected Sub gvRemarks_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvRemarks.RowEditing

    End Sub

End Class