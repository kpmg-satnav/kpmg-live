<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmSurrenderProperty.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmSurrenderProperty" Title="Surrender Property" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Tenant Renewal" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Surrender Property</h3>
                </div>

                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="90%" placeholder="Filter By Any..."></asp:TextBox>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" />
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-bordered table-hover table-striped"
                                    SelectedRowStyle-VerticalAlign="Top" AllowPaging="True" Width="100%" PageSize="5" EmptyDataText="No Surrender Property Found.">
                                    <Columns>
                                        <asp:TemplateField Visible="false" HeaderText="SNO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsno" runat="server" Text='<%#Eval("SNO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpropcode" runat="server" Text='<%#Eval("PROPERTY_CODE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Property Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpropname" runat="server" Text='<%#Eval("PROPERTY_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCity" runat="server" Text='<%#Eval("CTY_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLoc" runat="server" Text='<%#Eval("LCM_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tenant Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltenant" runat="server" Text='<%#Eval("TENANT_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LOB">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLOB" runat="server" Text='<%#Eval("LOB")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Rent">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRent" runat="server" Text='<%#Eval("PM_TD_RENT", "{0:c2}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tenant Start Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsdate" runat="server" Text='<%#Eval("PM_TEN_FRM_DT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tenant Expiry Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEdate" runat="server" Text='<%#Eval("PM_TEN_TO_DT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Surrender">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnksurrender" runat="server" Text="Surrender" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Surrender"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="View Details">
                                            <ItemTemplate>
                                                <a href="#" onclick="showPopWin('<%# Eval("SNO") %>')">
                                                    <asp:Label ID="lbltencode" runat="server" Text="View Details"></asp:Label>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <br />

                        <div id="panel1" runat="server">
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Property Code <span style="color: red;">*</span></label>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtpropertycode" ReadOnly="true" runat="server" Width="97%" CssClass="form-control"></asp:TextBox>
                                            <asp:HiddenField ID="hfsno" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Property Name <span style="color: red;">*</span></label>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtpropname" ReadOnly="true" runat="server" Width="97%" CssClass="form-control"></asp:TextBox>
                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Tenant Code<span style="color: red;">*</span></label>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txttcode" ReadOnly="true" runat="server" Width="97%" CssClass="form-control"></asp:TextBox>
                                            <asp:HiddenField ID="HiddenField2" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Tenant <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="cvuser" runat="server" ControlToValidate="ddluser"
                                            Display="None" ErrorMessage="Please Select User" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddluser" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="HiddenField3" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-top: 10px">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Surrendered Date <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvDesgcode" runat="server" ControlToValidate="txtSdate" Display="None" ErrorMessage="Please Pick Surrendered Date"
                                            ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtSdate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Total Rent Amount</label>
                                        <div class="col-md-12">
                                            <div>
                                                <asp:TextBox ID="txtTotRent" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Rent Amount Paid</label>
                                        <div class="col-md-12">
                                            <div>
                                                <asp:TextBox ID="RentAmount" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Security Deposit</label>
                                        <div class="col-md-12">
                                            <div>
                                                <asp:TextBox ID="Security" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-top: 10px">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Damages</label>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtDmg" runat="server" CssClass="form-control" MaxLength="300"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Damage Amount</label>

                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtDmgAmt" runat="server" CssClass="form-control" AutoPostBack="true">0</asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Remaining Amount To Be Paid</label>
                                        <div class="col-md-12">
                                            <div>
                                                <asp:TextBox ID="txtResultAmount" runat="server" CssClass="form-control" MaxLength="20">0</asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Amount To Be Paid To Owner</label>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtAmtPaidLesse" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-top: 10px">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Amount To Be Paid By Owner</label>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtAmtPaidLessor" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Upload Files/Images</label>
                                        <div class="btn btn-primary btn-mm">
                                            <i class="fa fa-folder-open-o fa-lg"></i>
                                            <asp:FileUpload ID="fu1" runat="Server" Width="90%" AllowMultiple="True" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 text-right" style="padding-top: 5px">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" CausesValidation="true" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View Tenant Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <iframe id="modalcontentframe" src="#" width="100%" height="450px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>

    <script>
        function showPopWin(id) {
            $("#modalcontentframe").attr("src", "../../WorkSpace/SMS_Webfiles/frmTenantDetails.aspx?tenant=" + id);
            $("#myModal").modal().fadeIn();
        }
    </script>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
