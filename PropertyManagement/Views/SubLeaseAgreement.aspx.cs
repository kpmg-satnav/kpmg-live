﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SubSonic;
using System.IO;
using System.Net;
using SubSonic;
using System.Drawing;
public partial class WorkSpace_SMS_Webfiles_SubLeaseAgreement : System.Web.UI.Page
{
    clsSubSonicCommonFunctions ObjSubSonic = new clsSubSonicCommonFunctions();
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "anything", "refreshSelectpicker();", true);
        string path = HttpContext.Current.Request.Url.AbsolutePath;
        string host = HttpContext.Current.Request.Url.Host;
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 50);
        param[0].Value = Session["UID"];
        param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
        param[1].Value = path;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
        {
            if (Session["UID"].ToString() == "")
            {
                Response.Redirect(Application["FMGLogout"].ToString());
            }
            else
            {
                if (sdr.HasRows)
                {
                }
                else
                {
                    Response.Redirect(Application["FMGLogout"].ToString());
                }
            }
        }
        revAccno.ValidationExpression = User_Validation.GetValidationExpressionForNumber().VAL_EXPR;
        revBankName.ValidationExpression = User_Validation.GetValidationExpressionForNormalText().VAL_EXPR;
        RegularExpressionValidator3.ValidationExpression = User_Validation.GetValidationExpressionForNormalText().VAL_EXPR;
        revDeposited.ValidationExpression = User_Validation.GetValidationExpressionForNormalText().VAL_EXPR;
        revl3brnch.ValidationExpression = User_Validation.GetValidationExpressionForNormalText().VAL_EXPR;
        REVIFsc.ValidationExpression = User_Validation.GetValidationExpressionForNormalText().VAL_EXPR;
        if (!IsPostBack)
        {
            BindGrid();
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = false;
            GetSubGroup();
            GetPaymentMode();
            GetPaymentTerms();
            GetAgreementype();
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_SEARCH_SUB_LEASE_AGREEMENT_DATA");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        sp.Command.AddParameter("@LEASE_ID", txtReqId.Text, DbType.String);
        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        ViewState["reqDetails"] = ds;
        gvItems.DataSource = ViewState["reqDetails"];
        gvItems.DataBind();
    }
    protected void txtreset_Click(object sender, EventArgs e)
    {
        BindGrid();
        txtReqId.Text = "";
    }
    private void BindGrid()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_SUB_LEASE_AGREEMENT_DATA");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        ViewState["reqDetails"] = ds;
        gvItems.DataSource = ViewState["reqDetails"];
        gvItems.DataBind();
    }

    protected void gvItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        gvItems.PageIndex = e.NewPageIndex;
        gvItems.DataSource = ViewState["reqDetails"];
        gvItems.DataBind();
        lblMsg.Text = "";
    }
    protected void gvItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "SubLeaseAgreement")
        {
            GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            int RowIndex = gvr.RowIndex;
            foreach (GridViewRow row in gvItems.Rows)
            {
                if (row.RowIndex == RowIndex)
                {
                    gvItems.Rows[RowIndex].BackColor = System.Drawing.Color.SkyBlue;
                }
                else
                {
                    gvItems.Rows[row.RowIndex].BackColor = Color.Empty;
                }
            }

            LinkButton lnkLease = (LinkButton)e.CommandSource;
            GridViewRow gvRow = (GridViewRow)lnkLease.NamingContainer;
            Label lblLseName = (Label)gvRow.FindControl("lblLseName");
            Label lblLsesno = (Label)gvRow.FindControl("lblLseSno");
            txtleaseid.Text = lblLseName.Text;
            ldlsno.Text = lblLsesno.Text;
            LeaseDetails();
            panel1.Visible = true;
        }
        else
        {
            panel1.Visible = false;
        }

    }
    protected void GetPaymentMode()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_PAYMENT_MODE");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ddlPaymentType.DataSource = sp.GetDataSet();
        ddlPaymentType.DataTextField = "NAME";
        ddlPaymentType.DataValueField = "CODE";
        ddlPaymentType.DataBind();
        ddlPaymentType.Items.Insert(0, "--Select--");
    }
    protected void GetPaymentTerms()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_PAYMENT_TERMS");
        ddlPaymentTerm.DataSource = sp.GetDataSet();
        ddlPaymentTerm.DataTextField = "PM_PT_NAME";
        ddlPaymentTerm.DataValueField = "PM_PT_SNO";
        ddlPaymentTerm.DataBind();
        ddlPaymentTerm.Items.Insert(0, "--Select--");
    }
    //protected void GetSubGroup()
    //{
    //    SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_SUB_COMPANIES");
    //    sp.Command.AddParameter("@dummy", 1, DbType.Int32);
    //    ddlSubGrp.DataSource = sp.GetDataSet();
    //    ddlSubGrp.DataTextField = "CNP_NAME";
    //    ddlSubGrp.DataValueField = "CNP_ID";
    //    ddlSubGrp.DataBind();
    //    ddlSubGrp.Items.Insert(0, "--Select--");
    //}


    protected void GetSubGroup()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_ENTITY_TYPES");
        //sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ddlSubGrp.DataSource = sp.GetDataSet();
        ddlSubGrp.DataTextField = "CHE_NAME";
        ddlSubGrp.DataValueField = "CHE_CODE";
        ddlSubGrp.DataBind();
        ddlSubGrp.Items.Insert(0, "--Select--");
    }

    protected void GetAgreementype()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_LEASE_AGGREMENTTYPE");
        sp.Command.AddParameter("@dummy", 1, DbType.Int32);
        ddlagrementType.DataSource = sp.GetDataSet();
        ddlagrementType.DataTextField = "PM_LES_AGR_TYPE";
        ddlagrementType.DataValueField = "PM_LES_ID";
        ddlagrementType.DataBind();
        ddlagrementType.Items.Insert(0, "--Select--");
    }

    protected void LeaseDetails()
    {

        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LEASE_DETAILS");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        sp.Command.AddParameter("@LEASEID", ldlsno.Text, DbType.String);
        IDataReader dr;
        dr = sp.GetReader();
        if (dr.Read())
        {
            txtPropCd.Text = Convert.ToString(dr["PM_PPT_CODE"]);
            txtPropName.Text = Convert.ToString(dr["PM_PPT_NAME"]);
            txtCountry.Text = Convert.ToString(dr["CNY_NAME"]);
            txtCity.Text = Convert.ToString(dr["CTY_NAME"]);
            txtLocation.Text = Convert.ToString(dr["LCM_NAME"]);
            txtLseStrDt.Text = Convert.ToDateTime(dr["LEASE_ST_DT"]).ToString("MM/dd/yyyy");
            txtLseEndDt.Text = Convert.ToDateTime(dr["LEASE_END_DT"]).ToString("MM/dd/yyyy");
            txtLnumber.Text = Convert.ToString(dr["PM_LES_CTS_NO"]).ToString();
            txtentitle.Text = Convert.ToDecimal(dr["PM_LES_ENTITLED_AMT"]).ToString();
            txtBasicRent.Text = Convert.ToDecimal(dr["PM_LES_BASIC_RENT"]).ToString();
            txtSD.Text = Convert.ToDecimal(dr["PM_LES_SEC_DEPOSIT"]).ToString();
            txtSDMonths.Text = Convert.ToInt32(dr["PM_LES_SEC_DEP_MONTHS"]).ToString();
            txtRentFreePeriod.Text = Convert.ToString(dr["PM_LES_RENT_FREE_PERIOD"]);
            txtRentPerSqftCarpet.Text = Convert.ToDecimal(dr["PM_LES_RENT_PER_SQFT_CARPET"]).ToString();
            txtRentPerSqftBUA.Text = Convert.ToDecimal(dr["PM_LES_RENT_PER_SQFT_BUA"]).ToString();
            txtInteriorCost.Text = Convert.ToDecimal(dr["PM_LES_INTERIOR_COST"]).ToString();
            txtCarArea.Text = Convert.ToDecimal(dr["PM_AR_CARPET_AREA"]).ToString();
            txtBUA.Text = Convert.ToDecimal(dr["PM_AR_BUA_AREA"]).ToString();
            txtRemain.Text = Convert.ToDecimal(dr["REMAINING_AREA_SEAT"]).ToString();


        }

    }

    protected void ddlPaymentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPaymentType.SelectedIndex > 0)
        {
            if (ddlPaymentType.SelectedItem.Value == "1")
            {
                panel2.Visible = false;
                panel3.Visible = false;
            }
            else if (ddlPaymentType.SelectedItem.Value == "2")
            {
                panel2.Visible = true;
                panel3.Visible = false;
            }
            else if (ddlPaymentType.SelectedItem.Value == "3")
            {
                panel2.Visible = false;
                panel3.Visible = true;
            }
            else if (ddlPaymentType.SelectedItem.Value == "4")
            {
                panel2.Visible = true;
                panel3.Visible = false;
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string SubLeaseId = "";
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@LEASE_ID", SqlDbType.NVarChar, 250);
            param[0].Value = txtleaseid.Text;

            SubLeaseId = ObjSubSonic.GetSubSonicExecuteScalar("PM_GET_LEASE_ID", param).ToString();

            SubLeaseId = txtleaseid.Text + "_SUB" + SubLeaseId;


            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_INSERT_SUB_LEASE_DETAILS");
            sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
            sp.Command.AddParameter("@SUB_LEASE_ID", SubLeaseId, DbType.String);
            sp.Command.AddParameter("@LEASE_ID", txtleaseid.Text, DbType.String);
            sp.Command.AddParameter("@SUB_GROUP", ddlSubGrp.SelectedItem.Value, DbType.String);
            sp.Command.AddParameter("@AGREE_START_DT", AgreeStartDate.Text, DbType.String);
            sp.Command.AddParameter("@AGREE_END_DT", AgreeEndDate.Text, DbType.String);
            sp.Command.AddParameter("@AREA_SEAT", txtSeat.Text, DbType.Decimal);
            sp.Command.AddParameter("@COST", txtCost.Text, DbType.Decimal);
            sp.Command.AddParameter("@MAIN_CHRG", txtMaintChrg.Text, DbType.Decimal);
            sp.Command.AddParameter("@PAY_TERMS", ddlPaymentTerm.SelectedItem.Text, DbType.String);
            sp.Command.AddParameter("@PAY_DATE", PaymentDate.Text, DbType.String);
            sp.Command.AddParameter("@PAY_TYPE", ddlPaymentType.SelectedItem.Text, DbType.String);
            sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String);
            sp.Command.AddParameter("@AGRREMENT_TYPE", ddlagrementType.SelectedItem.Value, DbType.String);
            sp.Command.AddParameter("@AGREEMANT_DATE", txtagredate.Text, DbType.String);
            sp.Command.AddParameter("@AGREEMENT_ADDRESS", txtaddress.Text, DbType.String);
            sp.Command.AddParameter("@SECURITY_DEPOSITE", txtsecdep.Text, DbType.String);

            if (ddlPaymentType.SelectedItem.Value == "1")
            {
                sp.Command.AddParameter("@ACC_NO", "", DbType.String);
                sp.Command.AddParameter("@BANK", "", DbType.String);
                sp.Command.AddParameter("@BRANCH", "", DbType.String);
                sp.Command.AddParameter("@IFSC", "", DbType.String);

            }

            else if (ddlPaymentType.SelectedItem.Value == "2")
            {
                sp.Command.AddParameter("@ACC_NO", txtAccNo.Text, DbType.String);
                sp.Command.AddParameter("@BANK", txtBankName.Text, DbType.String);
                sp.Command.AddParameter("@BRANCH", "", DbType.String);
                sp.Command.AddParameter("@IFSC", "", DbType.String);

            }
            else if (ddlPaymentType.SelectedItem.Value == "3")
            {
                sp.Command.AddParameter("@ACC_NO", txtAccTwo.Text, DbType.String);
                sp.Command.AddParameter("@BANK", txtBankTwo.Text, DbType.String);
                sp.Command.AddParameter("@BRANCH", txtbrnch.Text, DbType.String);
                sp.Command.AddParameter("@IFSC", txtIFSC.Text, DbType.String);

            }
            else if (ddlPaymentType.SelectedItem.Value == "4")
            {
                sp.Command.AddParameter("@ACC_NO", txtAccNo.Text, DbType.String);
                sp.Command.AddParameter("@BANK", txtBankName.Text, DbType.String);
                sp.Command.AddParameter("@BRANCH", "", DbType.String);
                sp.Command.AddParameter("@IFSC", "", DbType.String);
            }

            sp.Command.AddParameter("@COST_TYPE", rblCostType.SelectedValue, DbType.String);
            sp.Command.AddParameter("@SEAT_COST", txtSeatCost.Text == "" ? "0" : txtSeatCost.Text, DbType.Decimal);
            sp.Command.AddParameter("@RENT_SFT_CARPET", txtRentPerSqftCarpet.Text == "" ? "0" : txtRentPerSqftCarpet.Text, DbType.Decimal);
            sp.Command.AddParameter("@RENT_SFT_BUA", txtRentPerSqftBUA.Text == "" ? "0" : txtRentPerSqftBUA.Text, DbType.Decimal);

            if (Convert.ToDateTime(AgreeStartDate.Text) < Convert.ToDateTime(txtLseStrDt.Text))
            {
                //            Response.Write("<script language=javascript>alert(""From Date Should Be within in the Contract Period "")</script>")

                lblMsg.Text = "Sub Lease Agreement should be within Master Lease Agreement";
                return;
            }
            else if (Convert.ToDateTime(AgreeStartDate.Text) > Convert.ToDateTime(txtLseEndDt.Text))
            {
                // Response.Write("<script language=javascript>alert(""From Date Should Be within in the Contract Period "")</script>")
                lblMsg.Text = "Sub Lease Agreement should be within Master Lease Agreement";
                return;
            }
            else if (Convert.ToDateTime(AgreeEndDate.Text) > Convert.ToDateTime(txtLseEndDt.Text))
            {
                //Response.Write("<script language=javascript>alert(""To Date Should Be within in the Contract Period "")</script>")

                lblMsg.Text = "Sub Lease Agreement should be within Master Lease Agreement";
                return;
            }
            else if (Convert.ToDateTime(AgreeEndDate.Text) < Convert.ToDateTime(txtLseStrDt.Text))
            {
                //Response.Write("<script language=javascript>alert(""To Date Should Be within in the Contract Period "")</script>")

                lblMsg.Text = "Sub Lease Agreement should be within Master Lease Agreement";
                return;
            }
            sp.ExecuteScalar();

            lblMsg.Visible = true;
            panel1.Visible = false;
            lblMsg.Text = "Sub Lease Agreement Created Successfully" +txtleaseid.Text; 
            //if (Convert.ToDecimal(txtSeat.Text) > Convert.ToDecimal(txtRemain.Text))
            //{
            //    lblMsg.Text = "Area / No. of Seating should not be greater than Remaining Area / No. Of Seating";
            //}
            //else
            //{
            //    sp.ExecuteScalar();

            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Sub Lease Agreement Created Successfully";
            //}
        }

        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
    protected void txtSeat_TextChanged(object sender, EventArgs e)
    {
        Decimal Area = Convert.ToDecimal(txtSeat.Text);
        //Decimal Total = Convert.ToDecimal(txtTotal.Text);
        //Decimal MainChrg = Convert.ToDecimal(txtMaintChrg.Text);
        //Decimal Rent = Convert.ToDecimal(txtRentPerSqftCarpet.Text);
        //Decimal Cost = Convert.ToDecimal(txtCost.Text);

        //txtCost.Text = (Area * Rent).ToString();
        //txtTotal.Text = ((Area * Rent) + MainChrg).ToString();

        //txtTotal.Text = (Cost + MainChrg).ToString();
    }

    protected void txtCost_TextChanged(object sender, EventArgs e)
    {

        Decimal Total = Convert.ToDecimal(txtTotal.Text);
        Decimal MainChrg = Convert.ToDecimal(txtMaintChrg.Text);
        //Decimal Rent = Convert.ToDecimal(txtRentPerSqftCarpet.Text);
        Decimal Cost = Convert.ToDecimal(txtCost.Text);
        txtTotal.Text = (Cost + MainChrg).ToString();
    }
    protected void txtMaintChrg_TextChanged(object sender, EventArgs e)
    {
        //Decimal Area = Convert.ToDecimal(txtSeat.Text);
        Decimal Total = Convert.ToDecimal(txtTotal.Text);
        Decimal MainChrg = Convert.ToDecimal(txtMaintChrg.Text);
        // Decimal Rent = Convert.ToDecimal(txtRentPerSqftCarpet.Text);
        Decimal Cost = Convert.ToDecimal(txtCost.Text);

        txtTotal.Text = (Cost + MainChrg).ToString();
    }
    protected void rblCostType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblCostType.SelectedValue == "Sqft")
        {
            Costype1.Visible = true;
            Costype2.Visible = false;
            txtSeatCost.Text = "";
        }
        else if (rblCostType.SelectedValue == "Seat")
        {
            Costype1.Visible = false;
            Costype2.Visible = true;
            txtRentPerSqftCarpet.Text = "";
            txtRentPerSqftBUA.Text = "";
        }
    }
}