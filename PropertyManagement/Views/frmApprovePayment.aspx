<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmApprovePayment.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmApprovePayment" Title="Approve Payment" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('.date').datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Add Tenant" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Approve Work Request Payment</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Work Request</label>
                                    <div class="col-md-12">
                                        <asp:DropDownList ID="ddlWorkRequest" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Work Title</label>
                                    <div class="col-md-12">
                                        <asp:TextBox ID="txtWorkTitle" runat="server" CssClass="form-control"
                                            MaxLength="50" Enabled="False"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Work Specifications</label>
                                    <div class="col-md-12">
                                        <asp:TextBox ID="txtWorkSpec" runat="server" CssClass="form-control" Height="30%" TextMode="MultiLine" MaxLength="250" Enabled="False"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Estimated Amount</label>
                                    <div class="col-md-12">
                                        <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Start Date <span style="color: red;">*</span></label>
                                    <div class="col-md-12">
                                        <div class='input-group date' id='fromdate'>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtStartDate"
                                                Display="none" ErrorMessage="Please Enter Start Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control" MaxLength="10" Enabled="false"> </asp:TextBox>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Expected End Date <span style="color: red;">*</span></label>
                                    <div class="col-md-12">
                                        <div class='input-group date' id='todate'>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtExpiryDate"
                                                Display="none" ErrorMessage="Please Enter Expected End Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="form-control" MaxLength="10" Enabled="false"> </asp:TextBox>
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Paid Amount</label>
                                    <div class="col-md-12">
                                        <asp:TextBox ID="txtpamount" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Outstanding Amount</label>
                                    <div class="col-md-12">
                                        <asp:TextBox ID="txtoamount" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Work Status</label>
                                    <div class="col-md-12">
                                        <asp:DropDownList ID="ddlwstatus" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="False"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Work Condition<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                                        Display="none" ValidationGroup="Val1" ErrorMessage="Please Enter Work Condition"></asp:RequiredFieldValidator>
                                    <div class="col-md-12">
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Enabled="false" Height="30%" MaxLength="250"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Remarks</label>
                                    <div class="col-md-12">
                                        <asp:TextBox ID="Remarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%" MaxLength="250"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 19px; padding-left: 30px">
                                <asp:Button ID="btnApprove" CssClass="btn btn-primary custom-button-color" runat="server" ValidationGroup="Val1" Text="Approve" />
                                <asp:Button ID="btnReject" CssClass="btn btn-primary custom-button-color" runat="server" ValidationGroup="Val1" Text="Reject" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>



