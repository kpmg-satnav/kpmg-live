Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmApproveLeaseExtension
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet
    Dim RentRevision As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then
            scriptManager.RegisterPostBackControl(btnAppall)
            ''scriptManager.RegisterPostBackControl(DocLink)
            'scriptManager.RegisterPostBackControl(Documentsbindinggrid)
            scriptManager.RegisterPostBackControl(btnRejAll)

        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            RegExpRem.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
            ddlLtype.SelectedValue = "2"
            ddlLtype.Enabled = False
            BindGrid()
            ''BindGridSurrenderDocuments()
            'BindLeaseHistory()


            If gvLDetailsLease.Rows.Count > 0 Then
                pnlbutton.Visible = True
            Else
                pnlbutton.Visible = False
            End If
        End If
    End Sub

    Private Sub BindGrid()
        Try
            lblMsg.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_EXTENSION_APPROVAL_GRID")
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
            sp.Command.AddParameter("@LEASE_ID", txtLeaseId.Text, DbType.String)
            Dim ds As DataSet
            ds = sp.GetDataSet()
            ViewState("reqDetails") = ds
            gvLDetailsLease.DataSource = ViewState("reqDetails")
            gvLDetailsLease.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        BindGrid()
    End Sub

    Protected Sub gvLDetailsLease_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLDetailsLease.PageIndexChanging
        gvLDetailsLease.PageIndex = e.NewPageIndex()
        gvLDetailsLease.DataSource = ViewState("reqDetails")
        gvLDetailsLease.DataBind()
    End Sub

    'Protected Sub gvitems1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems1.PageIndexChanging
    '    gvitems1.PageIndex = e.NewPageIndex()
    '    BindLeaseHistory()
    'End Sub
    Private Sub UpdateLEAll(ByVal STA_ID As Integer)
        Try
            Dim ds As New DataSet
            For Each row As GridViewRow In gvLDetailsLease.Rows
                Dim chkselect As CheckBox = DirectCast(row.FindControl("chkItem"), CheckBox)
                Dim lblreqid As Label = DirectCast(row.FindControl("lbllname"), Label)
                If chkselect.Checked = True Then
                    Dim param As SqlParameter() = New SqlParameter(4) {}
                    param(0) = New SqlParameter("@LEASEID", lblreqid.Text)
                    param(1) = New SqlParameter("@STA_ID", STA_ID)
                    param(2) = New SqlParameter("@APPRV_REMARKS", txtRemarks.Text)
                    param(3) = New SqlParameter("@AUR_ID", Session("UID").ToString())
                    param(4) = New SqlParameter("@COMPANYID", Session("COMPANYID"))
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_LEASE_EXTENSION_APPROVE_REJECT", param)
                Else
                End If
            Next
            BindGrid()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnAppall_Click(sender As Object, e As EventArgs) Handles btnAppall.Click

        For Each row As GridViewRow In gvLDetailsLease.Rows
            Dim chkselect As CheckBox = DirectCast(row.FindControl("chkItem"), CheckBox)
            If chkselect.Checked = True Then
                UpdateLEAll(4018)
                lblMsg.Visible = True
                lblMsg.Text = "Lease Extension Approved Successfully"
                'Else
                '    lblMsg.Text = "Please Select Atleast One Checkbox"
            End If
        Next

    End Sub

    Protected Sub btnRejAll_Click(sender As Object, e As EventArgs) Handles btnRejAll.Click

        For Each row As GridViewRow In gvLDetailsLease.Rows
            Dim chkselect As CheckBox = DirectCast(row.FindControl("chkItem"), CheckBox)
            If chkselect.Checked = True Then
                UpdateLEAll(4019)
                lblMsg.Visible = True
                lblMsg.Text = "Lease Extension Rejected Successfully"
                'Else
                '    lblMsg.Text = "Please Select Atleast One Checkbox"
            End If
        Next

    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtLeaseId.Text = ""
        BindGrid()
    End Sub

    'Private Sub BindGridSurrenderDocuments()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[PM_LEASE_EXTENSION_VIEW_UPLOADdOCUMENTS]")
    '    sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
    '    sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
    '    sp.Command.AddParameter("@LEASE_ID", txtSearch.Text, DbType.String)
    '    Dim ds As DataSet
    '    ds = sp.GetDataSet()
    '    ViewState("reqDetails1") = ds
    '    Documentsbindinggrid.DataSource = ViewState("reqDetails1")
    '    Documentsbindinggrid.DataBind()

    'End Sub
    'Protected Sub Documentsbindinggrid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Documentsbindinggrid.PageIndexChanging
    '    Documentsbindinggrid.PageIndex = e.NewPageIndex()
    '    Documentsbindinggrid.DataSource = ViewState("reqDetails1")
    '    Documentsbindinggrid.DataBind()
    'End Sub
    'Protected Sub Documentsbindinggrid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Documentsbindinggrid.RowCommand
    '    If e.CommandName = "Document" Then

    '        Dim lnkSurrender As LinkButton = DirectCast(e.CommandSource, LinkButton)
    '        Dim Documentsbindinggrid As GridViewRow = DirectCast(lnkSurrender.NamingContainer, GridViewRow)
    '        Dim lblLseName As Label = DirectCast(Documentsbindinggrid.FindControl("lblLeaseid"), Label)

    '        Dim orgfilename As String = lnkSurrender.Text
    '        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & orgfilename
    '        If filePath <> "" Then
    '            ''Dim path As String = Server.MapPath(filePath)
    '            Dim file As System.IO.FileInfo = New System.IO.FileInfo(filePath)

    '            If file.Exists Then
    '                Response.Clear()
    '                Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
    '                Response.AddHeader("Content-Length", file.Length.ToString())
    '                Response.ContentType = "application/octet-stream"
    '                Response.WriteFile(file.FullName)
    '                Response.[End]()
    '            Else
    '                Response.Write("This file does not exist.")
    '            End If
    '        End If
    '    End If

    'End Sub

    'Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
    '    BindGridSurrenderDocuments()
    'End Sub

    'Private Sub BtnClear_Click(sender As Object, e As EventArgs) Handles BtnClear.Click
    '    txtSearch.Text = ""
    '    BindGridSurrenderDocuments()
    'End Sub



End Class