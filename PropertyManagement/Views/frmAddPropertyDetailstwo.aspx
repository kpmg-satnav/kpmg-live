<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddPropertyDetailstwo.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmAddPropertyDetails"
    MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <%--   <style>
        .panel > .panel-heading {
            height: 39px;
        }
    </style>--%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        /*body
        {
            color: #6a6c6f;
            background-color: #f1f3f6;
            margin-top: 30px;
        }*/

        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>
</head>

<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Country Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Add Property                                
                                <a href="#" class="btn btn-default openall pull-right" style="padding-bottom: initial;" title="Expand All"><i class="fa fa-minus" aria-hidden="true"></i></a>
                        <a href="#" class="btn btn-default closeall pull-right" style="padding-bottom: initial;" title="Collapse All"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    </h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                        <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                    </Triggers>
                                    <ContentTemplate>--%>

                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                    </asp:Label>
                                </div>
                            </div>
                        </div>
                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" />
                        <div class="panel panel-default " role="tab" id="div0">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#General">General Details</a>
                                </h4>
                            </div>
                            <div id="General" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Request ID</label>
                                                <asp:TextBox ID="txtReqID" runat="server" CssClass="form-control" BorderColor="White" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Request Type<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvddlReqType" runat="server" ControlToValidate="ddlReqType"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Request Type"
                                                    InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlReqType" runat="server" CssClass="form-control selectpicker with-search" onchange="clearMsg()" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Property Nature<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvddlPprtNature" runat="server" ControlToValidate="ddlPprtNature"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Property Nature"
                                                    InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlPprtNature" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                    <asp:ListItem Value="1">Lease</asp:ListItem>
                                                    <asp:ListItem Value="2">Leave & Licence</asp:ListItem>
                                                    <asp:ListItem Value="3">Own</asp:ListItem>
                                                    <%--<asp:ListItem Value="3">Farm House</asp:ListItem>
                                                                    <asp:ListItem Value="3">Software Park</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Acquisition Through<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvddlAcqThr" runat="server" ControlToValidate="ddlAcqThr"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Acquisition Through"
                                                    InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlAcqThr" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>City <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Location <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true"
                                                    data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Tower <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvddlTower" runat="server" ControlToValidate="ddlTower"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Tower" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlTower" runat="server" CssClass="form-control selectpicker with-search" AutoPostBack="true"
                                                    data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Total No. Of Floors</label>
                                                <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control" BorderColor="White" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Floor <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvddlFloor" runat="server" ControlToValidate="ddlFloor"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Floor" InitialValue="0"></asp:RequiredFieldValidator>
                                                <%--  <asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control selectpicker with-search" multiple data-live-search="true"></asp:DropDownList>--%>
                                                <asp:ListBox ID="ddlFloor" runat="server" SelectionMode="Multiple" CssClass="form-control selectpicker" data-live-search="true"></asp:ListBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Apartment Number</label>
                                                <asp:TextBox ID="txtApartmentno" runat="SERVER" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>No. Of Restrooms</label>
                                                <asp:RegularExpressionValidator ID="revtxtToilet" runat="server" ControlToValidate="txtToilet"
                                                    Display="None" ErrorMessage="Invalid No. Of Toilet Blocks" ValidationExpression="^[A-Za-z0-9//\s/-]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div>
                                                        <asp:TextBox ID="txtToilet" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>No. of BedRooms</label>
                                                <asp:TextBox ID="txtNoOfBedrooms" runat="SERVER" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Property Type<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rvpropertyType" runat="server" ControlToValidate="ddlPropertyType"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Property Type"
                                                    InitialValue="0"></asp:RequiredFieldValidator>
                                                <div>
                                                    <asp:DropDownList ID="ddlPropertyType" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Property Name<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="revPropIDName" runat="server" ControlToValidate="txtPropIDName"
                                                    Display="None" ErrorMessage="Please Enter Property Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revpropertyname" runat="server" ControlToValidate="txtPropIDName"
                                                    Display="None" ErrorMessage="Invalid Property Name" ValidationExpression="^[A-Za-z0-9//\s/-]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('No Special characters allowed except / ')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtPropIDName" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Signage Location</label>
                                                <div>
                                                    <div onmouseover="Tip('Enter Signage Location with maximum 500 characters')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtSignageLocation" runat="server" CssClass="form-control"
                                                            Rows="3" TextMode="Multiline" MaxLength="500" Height="30%"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>ESTD(Year)</label>
                                                <asp:RegularExpressionValidator ID="rgetxtESTD" runat="server" ControlToValidate="txtESTD"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid ESTD." ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter Numericals With Maximum length 4')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtESTD" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="4"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Age</label>
                                                <div>
                                                    <div>
                                                        <asp:TextBox ID="txtAge" runat="server" CssClass="form-control" MaxLength="4" BorderColor="White" Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Property Address<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ControlToValidate="txtPropDesc"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Property Address"></asp:RequiredFieldValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Address with maximum 500 characters')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtPropDesc" runat="server" CssClass="form-control" Height="30%"
                                                            Rows="3" TextMode="Multiline" MaxLength="500"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Society Name</label>
                                                <div>
                                                    <div>
                                                        <asp:TextBox ID="txtSocityName" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Latitude<span style="color: red;"></span></label>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtlat"
                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Latitude"></asp:RequiredFieldValidator>--%>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtlat" ValidationGroup="Val1"
                                                    Display="None" ErrorMessage="Invalid Latitude" ValidationExpression="^[0-9\.\-\/]+$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <asp:TextBox ID="txtlat" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Longitude<span style="color: red;"></span></label>
                                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtlong"
                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Longitude"></asp:RequiredFieldValidator>--%>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtlong" ValidationGroup="Val1"
                                                    Display="None" ErrorMessage="Invalid Longitude" ValidationExpression="^[0-9\.\-\/]+$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <asp:TextBox ID="txtlong" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Owner Scope Of Work</label>
                                                <div>
                                                    <asp:TextBox ID="txtOwnScopeWork" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Recommended/Priority</label>
                                                <asp:DropDownList ID="ddlRecommended" runat="server" AutoPostBack="true" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12" runat="server" id="divRecommanded" visible="false">
                                            <div class="form-group">
                                                <label>Recommended/Priority Remarks<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvtxtRecmRemarks" runat="server" ControlToValidate="txtRecmRemarks"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Recommended/Priority Remarks"></asp:RequiredFieldValidator>
                                                <div onmouseover="Tip('Enter Remarks with maximum 500 characters')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtRecmRemarks" runat="server" CssClass="form-control" Height="30%"
                                                        Rows="3" TextMode="Multiline" MaxLength="500"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default " role="tab" id="div1">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#Owner">Owner Details</a>
                                </h4>
                            </div>
                            <div id="Owner" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Owner Name <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvownname" runat="server" ControlToValidate="txtownrname"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Owner Name"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtownrname" runat="SERVER" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Phone Number<span style="color: red;">*</span> </label>
                                                <asp:RegularExpressionValidator ID="REVPHNO" runat="server" ControlToValidate="txtphno"
                                                    ErrorMessage="Please enter valid Phone Number" ValidationGroup="Val1" Display="None"
                                                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtphno"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Valid Phone Number"></asp:RequiredFieldValidator>
                                                <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtphno" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Representative Name </label>
                                                <asp:TextBox ID="txtPrvOwnName" runat="SERVER" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Representative Phone Number</label>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="txtPrvOwnPhNo" Display="None"
                                                    ErrorMessage="Please enter valid Phone Number" ValidationGroup="Val1" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtPrvOwnPhNo" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Owner Email<span style="color: red;">*</span></label>
                                                <asp:RegularExpressionValidator ID="rfv" runat="server" ControlToValidate="txtOwnEmail"
                                                    ValidationGroup="Val1" ErrorMessage="Please Enter Valid Email" Display="None"
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                </asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtOwnEmail"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Valid Email Id"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtOwnEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Staff Incharge</label>
                                                <asp:TextBox ID="Txtstaffincharge" runat="SERVER" CssClass="form-control"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Staff Incharge Email</label>
                                                <asp:TextBox ID="txtstaffinchargemail" runat="SERVER" CssClass="form-control"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" role="tab" id="div6">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#BankDetails">Owner Bank Details</a>
                                </h4>
                            </div>
                            <div id="BankDetails" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Bank Name </label>
                                                <asp:TextBox ID="txtBankName" runat="SERVER" CssClass="form-control"></asp:TextBox>
                                            </div>

                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Bank Address</label>
                                                <asp:TextBox ID="txtBankAddress" runat="server" CssClass="form-control" Height="20%" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>IFSC Code</label>
                                                <asp:TextBox ID="txtISFCCode" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Account number</label>
                                                <asp:TextBox ID="txtAcctnumber" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default " role="tab" id="div2">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#Area">Area Details</a>
                                </h4>
                            </div>
                            <div id="Area" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Super Built Up Area(Sqft) <span style="color: red;"></span></label>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ControlToValidate="txtUsableArea"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Super Built Up Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtSuperBulArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Built Up Area(Sqft) <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvBArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                    Display="None" ErrorMessage="Please Enter Built Up Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <%--<asp:CompareValidator ID="cmpbuiltuparea" runat="server" ControlToValidate="txtBuiltupArea" ControlToCompare="txtSuperBulArea" Type="Integer" ValidationGroup="Val1" Display="None" ></asp:CompareValidator>--%>
                                                <%-- <asp:RegularExpressionValidator ID="revBuiltupArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                            Display="None" ErrorMessage="Invalid Builtup Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>--%>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtBuiltupArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Carpet Area(Sqft) <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfCarpetarea" runat="server" ControlToValidate="txtCarpetArea"
                                                    Display="None" ErrorMessage="Please Enter Carpet Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="cmpcarpetarea" runat="server" ControlToValidate="txtCarpetArea" ControlToCompare="txtBuiltupArea" Type="Integer" Operator="LessThanEqual" ValidationGroup="Val1" Display="None" ErrorMessage="Carpet Area must be less than or equal to BuiltUp Up Area"></asp:CompareValidator>
                                                <asp:RegularExpressionValidator ID="revCarpetarea" runat="server" ControlToValidate="txtOptCapacity"
                                                    Display="None" ErrorMessage="Invalid Carpet Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtCarpetArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Common Area(Sqft) <span style="color: red;"></span></label>

                                                <asp:RegularExpressionValidator ID="revCommon" runat="server" ControlToValidate="txtCommonArea"
                                                    Display="None" ErrorMessage="Invalid Common Area" ValidationExpression="^[0-9]*\.?[0-9]*$"
                                                    ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtCommonArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Rentable Area(Sqft) <span style="color: red;"></span></label>

                                                <asp:RegularExpressionValidator ID="revRent" runat="server" ControlToValidate="txtRentableArea"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Rentable Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtRentableArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Usable Area(Sqft) <span style="color: red;"></span></label>

                                                <asp:RegularExpressionValidator ID="revUsable" runat="server" ControlToValidate="txtUsableArea"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Usable Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtUsableArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Plot Area(Sqft) <span style="color: red;"></span></label>

                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" ControlToValidate="txtUsableArea"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Plot Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtPlotArea" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Floor to Ceiling Height(ft) </label>
                                                <asp:RegularExpressionValidator ID="rfvtxtCeilingHight" runat="server" ControlToValidate="txtCeilingHight"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Floor to Ceiling Hight" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtCeilingHight" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Floor to Beam Bottom Height(ft) </label>
                                                <asp:RegularExpressionValidator ID="rfvtxtBeamBottomHight" runat="server" ControlToValidate="txtBeamBottomHight"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Floor to Beam Bottom Height" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtBeamBottomHight" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Maximum Capacity <span style="color: red;"></span></label>

                                                <asp:RegularExpressionValidator ID="revmaxcapacity" runat="server" ControlToValidate="txtMaxCapacity"
                                                    Display="None" ErrorMessage="Invalid Maximum Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Numericals With Maximum length 6')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtMaxCapacity" runat="server" CssClass="form-control" MaxLength="6">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Optimum Capacity </label>
                                                <asp:RegularExpressionValidator ID="revOptCapacity" runat="server" ControlToValidate="txtOptCapacity"
                                                    Display="None" ErrorMessage="Invalid Optimum Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Numericals With Maximum length 6')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtOptCapacity" runat="Server" CssClass="form-control" MaxLength="6">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Seating Capacity</label>
                                                <asp:RegularExpressionValidator ID="rfvtxtSeatingCapacity" runat="server" ControlToValidate="txtSeatingCapacity"
                                                    Display="None" ErrorMessage="Invalid Seating Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Numericals With Maximum length 6')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtSeatingCapacity" runat="Server" CssClass="form-control" MaxLength="6">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Flooring Type</label>
                                                <div>
                                                    <asp:DropDownList ID="ddlFlooringType" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>FSI (Floor Space Index)</label>
                                                <div>
                                                    <asp:DropDownList ID="ddlFSI" runat="server" AutoPostBack="true" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group col-sm-3 col-xs-6" runat="server" id="divFSI" visible="false">
                                            <div class="form-group">
                                                <label>FSI Ratio(Sqft) <span style="color: red;"></span></label>

                                                <asp:RegularExpressionValidator ID="revtxtFSI" runat="server" ControlToValidate="txtFSI"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid FSI Ratio" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtFSI" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Preferred Efficiency(%) </label>
                                                <asp:RegularExpressionValidator ID="revtxtEfficiency" runat="server" ControlToValidate="txtEfficiency"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Preferred Efficiency" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtEfficiency" runat="server" CssClass="form-control" MaxLength="14">0</asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Interior Type</label>
                                                    <%--<asp:TextBox ID="txtfurnished" runat="SERVER" CssClass="form-control"></asp:TextBox>--%>
                                                    <asp:DropDownList ID="ddlinreriortype" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="6">
                                                        <asp:ListItem Value="0">--Select-- </asp:ListItem>
                                                        <asp:ListItem Value="1">Furnished </asp:ListItem>
                                                        <asp:ListItem Value="2">Semi Furnished</asp:ListItem>
                                                        <asp:ListItem Value="3">UnFurnished</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default " role="tab" id="div3">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#Purchase">Management Details</a>
                                </h4>
                            </div>
                            <div id="Purchase" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Management Fee </label>
                                                <asp:RegularExpressionValidator ID="revtxtPurPrice" runat="server" ControlToValidate="txtPurPrice"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Management Fee" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtPurPrice" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Management Date</label>
                                                <div>
                                                    <div class='input-group date' id='PurDate'>
                                                        <asp:TextBox ID="txtPurDate" runat="server" CssClass="form-control"> </asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('PurDate')"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Market Value </label>
                                                <asp:RegularExpressionValidator ID="revtxtMarketValue" runat="server" ControlToValidate="txtMarketValue"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Market Value" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtMarketValue" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default " role="tab" id="div4">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#Govt">Regulator Details</a>
                                </h4>
                            </div>
                            <div id="Govt" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Regulator Name</label>
                                                <div>
                                                    <asp:TextBox ID="txtIRDA" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Branch Code</label>
                                                <div>
                                                    <asp:TextBox ID="txtPCcode" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Government Property Code</label>
                                                <asp:RegularExpressionValidator ID="revgovt" runat="server" ControlToValidate="txtGovtPropCode"
                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Government Property Code"
                                                    ValidationExpression="^[A-Za-z0-9//(/)\s/-]*$"></asp:RegularExpressionValidator>
                                                <div>
                                                    <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtGovtPropCode" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>UOM CODE (Unit Of Measure)</label>
                                                <div>
                                                    <asp:TextBox ID="txtUOM_CODE" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default " role="tab" id="div5">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#Insurance">Insurance Details</a>
                                </h4>
                            </div>
                            <div id="Insurance" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Type</label>
                                                <div>
                                                    <asp:DropDownList ID="ddlInsuranceType" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Vendor</label>
                                                <div>
                                                    <asp:TextBox ID="txtInsuranceVendor" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Amount</label>
                                                <div>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtInsuranceAmt"
                                                        ErrorMessage="Please enter Insurance Amount in numericals with decimals only" ValidationGroup="Val1" Display="None"
                                                        ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                    <asp:TextBox ID="txtInsuranceAmt" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Policy Number</label>
                                                <div>
                                                    <asp:TextBox ID="txtInsurancePolNum" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Start Date</label>
                                                <div>
                                                    <div class='input-group date' id='fromdate'>
                                                        <asp:TextBox ID="txtInsuranceStartdate" runat="server" CssClass="form-control"> </asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>End Date</label>
                                                <div>
                                                    <div class='input-group date' id='todate'>
                                                        <asp:TextBox ID="txtInsuranceEnddate" runat="server" CssClass="form-control"> </asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label for="txtcode">Upload Property Images <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                                    <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fu1"
                                                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only jpg,jpeg, png, gif files are allowed"
                                                        ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$">
                                                    </asp:RegularExpressionValidator>

                                                    <div class="btn btn-primary btn-mm">
                                                        <i class="fa fa-folder-open-o fa-lg"></i>
                                                        <asp:FileUpload ID="fu1" runat="Server" Width="90%" AllowMultiple="True" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-right" style="padding-top: 5px">
                            <asp:Button ID="btnSubmit" runat="server" Class="btn btn-primary btn-mm" Text="Submit" CausesValidation="true"
                                ValidationGroup="Val1" />
                            <asp:Button ID="btnAddMore" runat="server" Class="btn btn-primary btn-mm" Text="Submit & Add More Properties" CausesValidation="true" ValidationGroup="Val1" />

                        </div>
                        <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
                        <%--   </ContentTemplate>
                                </asp:UpdatePanel>--%>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

<script>
    $('.closeall').click(function () {
        $('.panel-collapse.in')
            .collapse('hide');
    });
    $('.openall').click(function () {
        $('.panel-collapse:not(".in")')
            .collapse('show');
    });

    function clearMsg() {
        document.getElementById("lblmsg").innerHTML = '';
    }
    function maxLength(s, args) {
        if (args.Value.length >= 500)
            args.IsValid = false;
    }
    function CheckDate() {
        var dtFrom = document.getElementById("txtInsuranceStartdate").Value;
        var dtTo = document.getElementById("txtInsuranceEnddate").Value;
        if (dtFrom < dtTo) {
            alert("Invalid Dates");
        }
    }
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
</script>
