Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmGeneratePaymentAdvise
    Inherits System.Web.UI.Page
    Public Sub Cleardata()
        getrequest()
        ddlWorkRequest.SelectedValue = 0
        txtBuilding.Text = ""
        txtproperty.Text = ""
        txtWorkTitle.Text = ""
        txtWorkSpec.Text = ""
        ddlpay.SelectedIndex = -1
        txtamount.Text = ""
        txtVendorName.Text = ""
        txtRemarks.Text = ""
        ddlwstatus.SelectedIndex = -1
        txtCheque.Text = ""
        txtBankName.Text = ""
        txtAccNo.Text = ""
        txtIBankName.Text = ""
        txtDeposited.Text = ""
    End Sub
    Public Sub GetWorkStatus()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_WORK_STATUS")
        ddlwstatus.DataSource = sp.GetDataSet()
        ddlwstatus.DataTextField = "STA_NAME"
        ddlwstatus.DataValueField = "STA_CODE"
        ddlwstatus.DataBind()
        ddlwstatus.Items.Insert(0, New ListItem("--Select Work Status--", "0"))
    End Sub
    Public Sub getrequest()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_UPDATED_GETWORK1REQ")
        sp.Command.AddParameter("@User", Session("UID"), DbType.String)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)
        ddlWorkRequest.DataSource = sp.GetDataSet()
        ddlWorkRequest.DataTextField = "PN_WORKREQUEST_REQ"
        ddlWorkRequest.DataBind()
        ddlWorkRequest.Items.Insert(0, New ListItem("--Select WorkRequest--", "0"))
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            getrequest()
            GetWorkStatus()
            ddlwstatus.Enabled = False
            panel1.Visible = False
            panel2.Visible = False
        End If
    End Sub
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GEN_PAY")
        sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@PAYMENT_MODE", ddlpay.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CREATED_BY", Session("Uid"), DbType.String)

        If ddlpay.SelectedItem.Value = "1" Then
            sp.Command.AddParameter("@ChequeNo", txtCheque.Text, DbType.String)
            sp.Command.AddParameter("@IssuingBank", txtBankName.Text, DbType.String)
            sp.Command.AddParameter("@DepositedBank", "", DbType.String)
            sp.Command.AddParameter("@AccountNumber", txtAccNo.Text, DbType.String)
            sp.Command.AddParameter("@IFSC", "", DbType.String)
        ElseIf ddlpay.SelectedItem.Value = "3" Then
            sp.Command.AddParameter("@ChequeNo", "", DbType.String)
            sp.Command.AddParameter("@IssuingBank", txtIBankName.Text, DbType.String)
            sp.Command.AddParameter("@DepositedBank", txtDeposited.Text, DbType.String)
            sp.Command.AddParameter("@AccountNumber", "", DbType.String)
            sp.Command.AddParameter("@IFSC", txtIFCB.Text, DbType.String)
        Else
            sp.Command.AddParameter("@ChequeNo", "", DbType.String)
            sp.Command.AddParameter("@IssuingBank", "", DbType.String)
            sp.Command.AddParameter("@DepositedBank", "", DbType.String)
            sp.Command.AddParameter("@AccountNumber", "", DbType.String)
            sp.Command.AddParameter("@IFSC", "", DbType.String)
        End If
        sp.Command.AddParameter("@Remarks", txtRemarks.Text, DbType.String)
        sp.ExecuteScalar()
        'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=15")
        lblmsg.Text = "Payment Advise Generated Successfully"
        Cleardata()
    End Sub
    Protected Sub ddlpay_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlpay.SelectedIndexChanged
        If ddlpay.SelectedIndex > 0 Then
            If ddlpay.SelectedItem.Value = "1" Then
                panel1.Visible = True
                panel2.Visible = False
            ElseIf ddlpay.SelectedItem.Value = "2" Then
                panel1.Visible = False
                panel2.Visible = False
            ElseIf ddlpay.SelectedItem.Value = "3" Then
                panel1.Visible = False
                panel2.Visible = True
            End If
        End If
    End Sub
    Protected Sub ddlWorkRequest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWorkRequest.SelectedIndexChanged
        If ddlWorkRequest.SelectedIndex > 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_WR_PAYMENT_DETAILS")
            sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                If Not String.IsNullOrEmpty(dr("PN_BLDG_CODE")) Then
                    Dim PlantArea As String = dr("PN_BLDG_CODE")
                    txtBuilding.Text = PlantArea
                End If
                If Not String.IsNullOrEmpty(dr("PN_LOC_ID")) Then
                    Dim Building As String = dr("PN_LOC_ID")
                    txtproperty.Text = Building
                End If
                If Not String.IsNullOrEmpty(dr("WORK_TITLE")) Then
                    Dim WRT As String = dr("WORK_TITLE")
                    txtWorkTitle.Text = WRT
                End If
                If Not String.IsNullOrEmpty(dr("WORK_SPECIFICATIONS")) Then
                    Dim Spc As String = dr("WORK_SPECIFICATIONS")
                    txtWorkSpec.Text = Spc
                End If
                If Not String.IsNullOrEmpty(dr("ESTIMATED_AMOUNT")) Then
                    Dim Amt As String = dr("ESTIMATED_AMOUNT")
                    txtamount.Text = Decimal.Round(Amt, 2, MidpointRounding.AwayFromZero)
                End If
                If Not String.IsNullOrEmpty(dr("VENDOR_NAME")) Then
                    Dim Vendor As String = dr("VENDOR_NAME")
                    txtVendorName.Text = Vendor
                End If
                GetWorkStatus()
                ddlwstatus.Enabled = False
                If Not String.IsNullOrEmpty(dr("REQUEST_STATUS")) Then
                    ddlwstatus.ClearSelection()
                    ddlwstatus.Items.FindByValue(dr("REQUEST_STATUS")).Selected = True
                    ddlwstatus.Enabled = False
                End If
            End If
            'Dim ds As New DataSet()
            'ds = sp.GetDataSet()
            'If ds.Tables(0).Rows.Count > 0 Then
            '    txtBuilding.Text = ds.Tables(0).Rows(0).Item("PN_BLDG_CODE")
            '    txtproperty.Text = ds.Tables(0).Rows(0).Item("PN_LOC_ID")
            '    txtWorkTitle.Text = ds.Tables(0).Rows(0).Item("WORK_TITLE")
            '    txtWorkSpec.Text = ds.Tables(0).Rows(0).Item("WORK_SPECIFICATIONS")
            '    txtamount.Text = ds.Tables(0).Rows(0).Item("ESTIMATED_AMOUNT")
            '    txtVendorName.Text = ds.Tables(0).Rows(0).Item("VENDOR_NAME")
            '    getrequest()
            '    ddlwstatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("REQUEST_STATUS")).Selected = True
            '    ddlwstatus.Enabled = True
            'End If
        Else
            txtBuilding.Text = ""
            txtproperty.Text = ""
            txtWorkTitle.Text = ""
            txtWorkSpec.Text = ""
            txtamount.Text = ""
            txtVendorName.Text = ""
            txtRemarks.Text = ""
            ddlwstatus.SelectedIndex = -1
            txtCheque.Text = ""
            txtBankName.Text = ""
            txtAccNo.Text = ""
            txtIBankName.Text = ""
            txtDeposited.Text = ""
        End If
    End Sub
End Class
