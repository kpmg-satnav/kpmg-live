<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPropertyLevelApproval.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmPropertyLevelApproval" Title="Property Details Approval " %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

            <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvrReqProperties.ClientID %>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Country Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Approve/Reject Property</h3>
                </div>
                <div class="panel-body">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" />

                                <div class="row">
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <asp:TextBox ID="txtSrch" runat="server" CssClass="form-control" Width="90%" placeholder="Search By Request Id/Property Name"></asp:TextBox>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
                                            <asp:Button ID="btnsrch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" />
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row form-inline">
                                    <div class="form-group col-md-12">
                                        <asp:GridView ID="gvReqs" runat="server" EmptyDataText="No Records Found." AllowPaging="True" PageSize="5" AutoGenerateColumns="false"
                                            CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Request Id">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnksurrender" runat="server" Text='<%#Eval("PM_REQ_ID")%>' CommandArgument='<%#Eval("PM_REQ_ID")%>'
                                                            CommandName="GetProperties"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPropType" runat="server" Text='<%#Eval("PM_CREATED_DT")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Property Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpropname" runat="server" Text='<%#Eval("PM_PPT_NAME")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Property Address">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpropadd" runat="server" Text='<%#Eval("PM_PPT_ADDRESS")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested By">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPropCode" runat="server" Text='<%#Eval("PM_CREATED_BY")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <asp:HiddenField ID="hdnReqid" runat="server" />
                                <div id="panel1" runat="Server" visible="false">

                                    <div class="row" style="padding-top: 10px;">
                                        <div class="col-md-5 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Search by Property Name/Location/Recommended/Owner Name</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:TextBox ID="txtsearch" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:Button ID="btnSearch" runat="server" Class="btn btn-primary btn-mm" Text="Search" />
                                            </div>
                                        </div>
                                    </div>


                                    <asp:HiddenField ID="hdnSno" runat="server" />
                                    <div class="row form-inline">
                                        <div class="horizontal-scroll">
                                            <div class="form-group col-md-12">
                                                <asp:GridView ID="gvrReqProperties" runat="server" EmptyDataText="No Property Details Found."
                                                    AllowSorting="false" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Request Id" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblID" runat="server" Text='<%#Eval("PM_PPT_SNO")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                Select All
                                                                        <input id="chkSelect1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkselect', this.checked)">
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Request Id">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblrequestId" runat="server" Text='<%#Eval("PM_REQ_ID")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Req By">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblReqBy" runat="server" Text='<%#Eval("PM_PPT_CREATED_BY")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Req Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblcreatedBy" runat="server" Text='<%#Eval("PM_PPT_CREATED_DT")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Request Type">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblReqType" runat="server" Text='<%#Eval("PM_RT_TYPE")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Property Nature">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPropNature" runat="Server" Text='<%#Eval("PM_PPT_NATURE")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Property Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPropName" runat="Server" Text='<%#Eval("PM_PPT_NAME")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Entity">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEntityName" runat="Server" Text='<%#Eval("CHE_NAME")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Acquisition Through">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblacqthr" runat="Server" Text='<%#Eval("PM_PPT_ACQ_THR")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Carpet Area(Sqft)">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblcarArea" runat="Server" Text='<%#Eval("PM_AR_CARPET_AREA")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Property Address">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPropDesc" runat="Server" Text='<%#Eval("PM_PPT_ADDRESS")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="City">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCity" runat="Server" Text='<%#Eval("CTY_NAME")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Location">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLcm" runat="Server" Text='<%#Eval("LCM_NAME")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Owner Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOwnName" runat="Server" Text='<%#Eval("PM_OWN_NAME")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Phone No">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPhno" runat="Server" Text='<%#Eval("PM_OWN_PH_NO")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Recomm - ended">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRecomm" runat="Server" Text='<%#Eval("PM_PPT_RECOMMENDED")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Email">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEmail" runat="Server" Text='<%#Eval("PM_OWN_EMAIL")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Request Status">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblReqSts" runat="Server" Text='<%#Eval("STA_TITLE")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkSno" runat="server" Text='View Details' CommandArgument='<%#Eval("PM_PPT_SNO") %>'
                                                                    CommandName="ViewDetails"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle />
                                                    <PagerStyle CssClass="pagination-ys" />
                                                </asp:GridView>

                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="form-group col-sm-4 col-xs-6"></div>
                                        <div class="col-md-5 col-sm-8 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-md-12">Remarks<span style="color: red;">*</span> </label>
                                                <div class="col-md-12">
                                                    <asp:TextBox ID="txtMultiRemarks" CssClass="form-control" Width="100%" Height="30%"
                                                        runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-8 col-xs-12" style="margin-top: 20px">
                                            <div class="form-group">
                                                <asp:Button ID="btnApprove" runat="server" Class="btn btn-primary btn-mm" Text="Approve" OnClientClick="javascript:return validateCheckBoxesMyReq();" />
                                                <asp:Button ID="btnReject" runat="server" Class="btn btn-primary btn-mm" Text="Reject"
                                                    OnClientClick="return confirm('Are you sure you want to delete this Property?');" />
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                </div>
                                <div id="panel2" runat="Server" visible="false">
                                    <div class="panel panel-default " role="tab" id="div0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#General">General Details</a>
                                            </h4>
                                        </div>
                                        <div id="General" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Request ID</label>
                                                            <asp:TextBox ID="txtReqID" runat="server" CssClass="form-control" BorderColor="White" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="fprm-group col-sm-3 col-xs-6">
                                                        <div class="form-group">
                                                            <label>Request Type<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvddlReqType" runat="server" ControlToValidate="ddlReqType"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Request Type"
                                                                InitialValue="0"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlReqType" runat="server" CssClass="form-control selectpicker with-search" Enabled="false" onchange="clearMsg()" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="fprm-group col-sm-3 col-xs-6">
                                                        <div class="form-group">
                                                            <label>Property Nature<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvddlPprtNature" runat="server" ControlToValidate="ddlPprtNature"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Property Nature"
                                                                InitialValue="0"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlPprtNature" runat="server" CssClass="form-control selectpicker with-search" Enabled="false" data-live-search="true">
                                                                <asp:ListItem Value="0">--Select Property Nature--</asp:ListItem>
                                                                <asp:ListItem Value="1">Lease</asp:ListItem>
                                                                <asp:ListItem Value="2">Own</asp:ListItem>
                                                                <asp:ListItem Value="3">Leave & Licence</asp:ListItem>
                                                                <%--<asp:ListItem Value="3">Farm House</asp:ListItem>
                                                                <asp:ListItem Value="3">Software Park</asp:ListItem>--%>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="fprm-group col-sm-3 col-xs-6">
                                                        <div class="form-group">
                                                            <label>Acquisition Through<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvddlAcqThr" runat="server" ControlToValidate="ddlAcqThr"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Acquisition Through"
                                                                InitialValue="0"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlAcqThr" runat="server" CssClass="form-control selectpicker with-search" Enabled="false" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>City <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="0"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker with-search" Enabled="false" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Location <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker with-search" Enabled="false" AutoPostBack="true"
                                                                data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Tower <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvddlTower" runat="server" ControlToValidate="ddlTower"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Tower" InitialValue="0"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlTower" runat="server" CssClass="form-control selectpicker with-search" Enabled="false" AutoPostBack="true"
                                                                data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Total No. Of Floors</label>
                                                            <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control" BorderColor="White" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Floor <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvddlFloor" runat="server" ControlToValidate="ddlFloor"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Floor" InitialValue="0"></asp:RequiredFieldValidator>
                                                            <%-- <asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>--%>

                                                            <asp:ListBox ID="ddlFloor" runat="server" Enabled="false" SelectionMode="Multiple" CssClass="form-control selectpicker" data-live-search="true"></asp:ListBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>No. Of Toilet Blocks</label>
                                                            <asp:RegularExpressionValidator ID="revtxtToilet" runat="server" ControlToValidate="txtToilet"
                                                                Display="None" ErrorMessage="Invalid No. Of Toilet Blocks" ValidationExpression="^[A-Za-z0-9//\s/-]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('No Special characters allowed except / ')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtToilet" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Property Type<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rvpropertyType" runat="server" ControlToValidate="ddlPropertyType"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Property Type"
                                                                InitialValue="0"></asp:RequiredFieldValidator>
                                                            <div>
                                                                <asp:DropDownList ID="ddlPropertyType" Enabled="false" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Property Name<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="revPropIDName" runat="server" ControlToValidate="txtPropIDName"
                                                                Display="None" ErrorMessage="Please Enter Property Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revpropertyname" runat="server" ControlToValidate="txtPropIDName"
                                                                Display="None" ErrorMessage="Invalid Property Name" ValidationExpression="^[A-Za-z0-9//\s/-]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>

                                                            <div onmouseover="Tip('No Special characters allowed except / ')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtPropIDName" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>ESTD(Year)<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="ReqtxtESTD" runat="server" ControlToValidate="txtESTD"
                                                                Display="None" ErrorMessage="Please Enter ESTD(Year)" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <div class='input-group date' id='PurDate'>
                                                                <asp:TextBox ID="txtESTD" runat="server" CssClass="form-control"> </asp:TextBox>
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar" enabled="false" onclick="setup('PurDate')"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Age</label>
                                                            <asp:TextBox ID="txtAge" runat="server" Enabled="false" CssClass="form-control" MaxLength="4" BorderColor="White"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Property Address<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ControlToValidate="txtPropDesc"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Property Address"></asp:RequiredFieldValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Address with maximum 250 characters')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtPropDesc" runat="server" CssClass="form-control"
                                                                        Rows="3" TextMode="Multiline" Enabled="false" MaxLength="500"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Signage Location</label>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Signage Location with maximum 250 characters')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtSignageLocation" runat="server" CssClass="form-control"
                                                                        Rows="3" TextMode="Multiline" Enabled="false" MaxLength="500"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Society Name</label>
                                                            <div>
                                                                <div>
                                                                    <asp:TextBox ID="txtSocityName" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Latitude<span style="color: red;"></span></label>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtlat"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Latitude"></asp:RequiredFieldValidator>--%>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtlat" ValidationGroup="Val1"
                                                                Display="None" ErrorMessage="Invalid Latitude" ValidationExpression="^[0-9\.\-\/]+$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <asp:TextBox ID="txtlat" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Longitude<span style="color: red;"></span></label>
                                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtlong"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Longitude"></asp:RequiredFieldValidator>--%>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtlong" ValidationGroup="Val1"
                                                                Display="None" ErrorMessage="Invalid Longitude" ValidationExpression="^[0-9\.\-\/]+$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <asp:TextBox ID="txtlong" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Owner Scope Of Work</label>
                                                            <div>
                                                                <asp:TextBox ID="txtOwnScopeWork" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Recommended/Priority</label>
                                                            <div>
                                                                <asp:DropDownList ID="ddlRecommended" Enabled="false" runat="server" AutoPostBack="true" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12" runat="server" id="divRecommanded">
                                                        <div class="form-group">
                                                            <label>Recommended/Priority Remarks<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtRecmRemarks" runat="server" ControlToValidate="txtRecmRemarks"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Recommended/Priority Remarks"></asp:RequiredFieldValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Remarks with maximum 500 characters')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtRecmRemarks" runat="server" CssClass="form-control"
                                                                        Rows="3" TextMode="Multiline" Enabled="false" MaxLength="500"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Entity</label>
                                                            <div>
                                                                <asp:TextBox ID="txtentity" runat="SERVER" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Inspection By<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvInspection" runat="server" ControlToValidate="txtInspection"
                                                                Display="None" ErrorMessage="Please Enter Inspection By" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revInspection" runat="server" ControlToValidate="txtInspection"
                                                                Display="None" ErrorMessage="Invalid Inspection By" ValidationExpression="^[A-Za-z0-9//\s/-]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('No Special characters allowed except / ')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtInspection" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Inspection Date<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvsdate" runat="server" ControlToValidate="txtsdate"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Inspection Date"></asp:RequiredFieldValidator>
                                                            <div class='input-group date' id='effdate'>
                                                                <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtsdate" Enabled="false" runat="server" CssClass="form-control" TabIndex="55" AutoPostBack="true"></asp:TextBox>
                                                                </div>
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar" onclick="setup('effdate')"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="panel panel-default " role="tab" id="div1">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Owner">Owner Details</a>
                                            </h4>
                                        </div>
                                        <div id="Owner" class="panel-collapse collapse in">
                                            <div class="panel-body">

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Owner Name <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvownname" runat="server" ControlToValidate="txtownrname"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Owner Name"></asp:RequiredFieldValidator>
                                                            <asp:TextBox ID="txtownrname" runat="SERVER" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Mobile Number </label>
                                                            <asp:RegularExpressionValidator ID="REVPHNO" runat="server" ControlToValidate="txtphno"
                                                                ErrorMessage="Please enter valid Phone Number" ValidationGroup="Val1" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtphno" runat="server" Enabled="false" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Landline Number </label>
                                                            <asp:RegularExpressionValidator ID="REVLLNO" runat="server" ControlToValidate="txtllno"
                                                                ErrorMessage="Please enter valid Landline Number" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtllno" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Previous Owner Name </label>
                                                            <asp:TextBox ID="txtPrvOwnName" runat="SERVER" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Previous Owner Phone Number</label>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="txtPrvOwnPhNo" Display="None"
                                                                ErrorMessage="Please enter valid Phone Number" ValidationGroup="Val1" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtPrvOwnPhNo" runat="server" Enabled="false" CssClass="form-control" MaxLength="15"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Owner Email</label>
                                                            <asp:RegularExpressionValidator ID="rfv" runat="server" ControlToValidate="txtOwnEmail"
                                                                ValidationGroup="Val1" ErrorMessage="Please Enter Valid Email" Display="None"
                                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                            </asp:RegularExpressionValidator>
                                                            <asp:TextBox ID="txtOwnEmail" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Agreement To be Signed By POA<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvddlAgreementbyPOA" runat="server" ControlToValidate="ddlAgreementbyPOA" Display="None" ValidationGroup="Val1"
                                                                ErrorMessage="Please Select Agreement To be Signed By POA" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlAgreementbyPOA" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="panPOA" runat="server" visible="false" class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapsepanPOA">Power of Attorney Details</a>
                                            </h4>
                                        </div>
                                        <div id="collapsepanPOA" class="panel-collapse collapse in">
                                            <div class="panel-body color">
                                                <div class="row">

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Name<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvPOAName" runat="server" ControlToValidate="txtPOAName"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Name of Power of Attorney"></asp:RequiredFieldValidator>
                                                            <asp:TextBox ID="txtPOAName" runat="server" Enabled="false" CssClass="form-control" TabIndex="32" MaxLength="50"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Address<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvPOAAddress" runat="server" ControlToValidate="txtPOAAddress"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Address of Power of Attorney"></asp:RequiredFieldValidator>
                                                            <asp:TextBox ID="txtPOAAddress" runat="server" Enabled="false" CssClass="form-control" MaxLength="1000" TextMode="MultiLine" Rows="4" TabIndex="33"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Contact Details<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvPOAMobile" runat="server" ControlToValidate="txtPOAMobile"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Contact Details of Power of Attorney"></asp:RequiredFieldValidator>
                                                            <cc1:FilteredTextBoxExtender ID="ftetxtPOAMobile" runat="server" TargetControlID="txtPOAMobile" FilterType="Numbers" ValidChars="0123456789" />
                                                            <asp:TextBox ID="txtPOAMobile" runat="server" Enabled="false" CssClass="form-control" MaxLength="12" TabIndex="34"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Email-ID</label>
                                                            <asp:RegularExpressionValidator ID="revPOAEmail" runat="server" ControlToValidate="txtPOAEmail"
                                                                ErrorMessage="Please Enter valid Email of Power of Attorney" Display="None" ValidationGroup="Val1"
                                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                            <asp:TextBox ID="txtPOAEmail" Enabled="false" runat="server" CssClass="form-control" MaxLength="1000" TabIndex="35"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Type of Landlord</label>
                                                            <asp:TextBox ID="txtLLtype" Enabled="false" runat="SERVER" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default " role="tab" id="div2">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Area">Area Details</a>
                                            </h4>
                                        </div>
                                        <div id="Area" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Super Built Up Area(Sqft) <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtSuperBulArea" runat="server" ControlToValidate="txtSuperBulArea"
                                                                Display="None" ErrorMessage="Please Enter Super Built Up Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ControlToValidate="txtUsableArea"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Super Built Up Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtSuperBulArea" Enabled="false" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Built Up Area(Sqft) <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvBArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                                Display="None" ErrorMessage="Please Enter Built Up Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="cmpbuiltuparea" runat="server" ControlToValidate="txtBuiltupArea" ControlToCompare="txtSuperBulArea" Type="Integer" Operator="LessThanEqual" ValidationGroup="Val1" Display="None" ErrorMessage="BuiltUp Area must be less than or equal to SuperBuilt Up Area"></asp:CompareValidator>
                                                            <asp:RegularExpressionValidator ID="revBuiltupArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                                Display="None" ErrorMessage="Invalid Builtup Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtBuiltupArea" Enabled="false" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Carpet Area(Sqft) <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfCarpetarea" runat="server" ControlToValidate="txtCarpetArea"
                                                                Display="None" ErrorMessage="Please Enter Carpet Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="cmpcarpetarea" runat="server" ControlToValidate="txtCarpetArea" ControlToCompare="txtBuiltupArea" Type="Integer" Operator="LessThanEqual" ValidationGroup="Val1" Display="None" ErrorMessage="Carpet Area must be less than or equal to BuiltUp Up Area"></asp:CompareValidator>
                                                            <asp:RegularExpressionValidator ID="revCarpetarea" runat="server" ControlToValidate="txtOptCapacity"
                                                                Display="None" ErrorMessage="Invalid Carpet Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtCarpetArea" runat="server" Enabled="false" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Common Area(Sqft) <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvCommonarea" runat="server" ControlToValidate="txtCommonArea"
                                                                Display="None" ErrorMessage="Please Enter Common Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revCommon" runat="server" ControlToValidate="txtCommonArea"
                                                                Display="None" ErrorMessage="Invalid Common Area" ValidationExpression="^[0-9]*\.?[0-9]*$"
                                                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtCommonArea" runat="server" Enabled="false" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Rentable Area(Sqft) <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvRentable" runat="server" ControlToValidate="txtRentableArea"
                                                                Display="None" ErrorMessage="Please Enter Rentable Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revRent" runat="server" ControlToValidate="txtRentableArea"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Rentable Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtRentableArea" runat="server" Enabled="false" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Usable Area(Sqft) <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvUsable" runat="server" ControlToValidate="txtUsableArea"
                                                                Display="None" ErrorMessage="Please Enter Usable Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revUsable" runat="server" ControlToValidate="txtUsableArea"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Usable Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtUsableArea" runat="server" Enabled="false" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Plot Area(Sqft) <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtPlotArea" runat="server" ControlToValidate="txtPlotArea"
                                                                Display="None" ErrorMessage="Please Enter Plot Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" ControlToValidate="txtUsableArea"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Plot Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtPlotArea" runat="server" Enabled="false" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Floor to Ceiling Height(ft) </label>
                                                            <asp:RegularExpressionValidator ID="rfvtxtCeilingHight" runat="server" ControlToValidate="txtCeilingHight"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Floor to Ceiling Hight" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtCeilingHight" runat="server" Enabled="false" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Floor to Beam Bottom Height(ft) </label>
                                                            <asp:RegularExpressionValidator ID="rfvtxtBeamBottomHight" runat="server" ControlToValidate="txtBeamBottomHight"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Floor to Beam Bottom Height" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtBeamBottomHight" runat="server" Enabled="false" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Maximum Capacity <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvMaxCapacity" runat="server" ControlToValidate="txtMaxCapacity"
                                                                Display="None" ErrorMessage="Please Enter Maximum Capacity" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revmaxcapacity" runat="server" ControlToValidate="txtMaxCapacity"
                                                                Display="None" ErrorMessage="Invalid Maximum Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 6')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtMaxCapacity" runat="server" Enabled="false" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Optimum Capacity </label>
                                                            <asp:RegularExpressionValidator ID="revOptCapacity" runat="server" ControlToValidate="txtOptCapacity"
                                                                Display="None" ErrorMessage="Invalid Optimum Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 6')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtOptCapacity" runat="Server" Enabled="false" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Seating Capacity</label>
                                                            <asp:RegularExpressionValidator ID="rfvtxtSeatingCapacity" runat="server" ControlToValidate="txtSeatingCapacity"
                                                                Display="None" ErrorMessage="Invalid Seating Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 6')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtSeatingCapacity" runat="Server" Enabled="false" CssClass="form-control" MaxLength="6"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Flooring Type</label>
                                                            <div>
                                                                <asp:DropDownList ID="ddlFlooringType" runat="server" Enabled="false" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>FSI (Floor Space Index)</label>
                                                            <div>
                                                                <asp:DropDownList ID="ddlFSI" runat="server" Enabled="false" AutoPostBack="true" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-sm-3 col-xs-6" runat="server" id="divFSI">
                                                        <div class="form-group">
                                                            <label>FSI Ratio(Sqft) <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvtxtFSI" runat="server" ControlToValidate="txtFSI"
                                                                Display="None" ErrorMessage="Please Enter FSI Ratio" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revtxtFSI" runat="server" ControlToValidate="txtFSI"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid FSI Ratio" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtFSI" runat="server" Enabled="false" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Preferred Efficiency(%) </label>
                                                            <asp:RegularExpressionValidator ID="revtxtEfficiency" runat="server" ControlToValidate="txtEfficiency"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Preferred Efficiency" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtEfficiency" runat="server" Enabled="false" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                    <div class="panel panel-default " role="tab" id="div8">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Condition">Physical Condition of premise</a>
                                            </h4>
                                        </div>
                                        <div id="Condition" class="panel-collapse collapse in">
                                            <div class="panel-body">

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Walls</label>
                                                            <div>
                                                                <asp:TextBox ID="txtWalls" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Roof</label>
                                                            <div>
                                                                <asp:TextBox ID="txtRoof" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Ceiling</label>
                                                            <div>
                                                                <asp:TextBox ID="txtCeiling" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Windows</label>
                                                            <div>
                                                                <asp:TextBox ID="txtwindows" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Damage</label>
                                                            <div>
                                                                <asp:TextBox ID="txtDamage" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Seepage</label>
                                                            <div>
                                                                <asp:TextBox ID="txtSeepage" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Surrounding Offices/Other Tenants</label>
                                                            <div>
                                                                <asp:TextBox ID="txtOthrtnt" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default " role="tab" id="div9">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Landlord">Landlord's Scope of work</a>
                                            </h4>
                                        </div>
                                        <div id="Landlord" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Vitrified Flooring as per specifications<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvVitrified" runat="server" ControlToValidate="ddlvitrified" Display="None" ValidationGroup="Val1"
                                                                ErrorMessage="Please Select Vitrified Flooring as per specifications" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlvitrified" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remarks <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvvitriremarks" runat="server" ControlToValidate="txtVitriRemarks"
                                                                Display="None" ErrorMessage="Please enter Vitrified Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <div>
                                                                <asp:TextBox ID="txtVitriRemarks" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Two Washrooms<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvwashrooms" runat="server" ControlToValidate="ddlWashroom" Display="None" ValidationGroup="Val1"
                                                                ErrorMessage="Please Select Two Washrooms" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlWashroom" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remarks<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvWashroomremarks" runat="server" ControlToValidate="txtWashroom"
                                                                Display="None" ErrorMessage="Please enter Two Washrooms Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <div>
                                                                <asp:TextBox ID="txtWashroom" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Pantry<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvpantry" runat="server" ControlToValidate="ddlPantry" Display="None" ValidationGroup="Val1"
                                                                ErrorMessage="Please Select Pantry" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlPantry" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remarks<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvpantryremarks" runat="server" ControlToValidate="txtPantry"
                                                                Display="None" ErrorMessage="Please enter Pantry Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <div>
                                                                <asp:TextBox ID="txtPantry" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Rolling Shutter with boxing<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvShutterremarks" runat="server" ControlToValidate="ddlShutter" Display="None" ValidationGroup="Val1"
                                                                ErrorMessage="Please Select Rolling Shutter with boxing" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlShutter" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remarks<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvShutter" runat="server" ControlToValidate="txtShutter"
                                                                Display="None" ErrorMessage="Please enter Rolling Shutter remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <div>
                                                                <asp:TextBox ID="txtShutter" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Others<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvOthers" runat="server" ControlToValidate="ddlOthers" Display="None" ValidationGroup="Val1"
                                                                ErrorMessage="Please Select Two Washrooms" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlOthers" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remarks<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvothremarks" runat="server" ControlToValidate="txtOthers"
                                                                Display="None" ErrorMessage="Please enter Others remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <div>
                                                                <asp:TextBox ID="txtOthers" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>LL Work Completion Time(Days)<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvllwork" runat="server" ControlToValidate="txtllwork"
                                                                Display="None" ErrorMessage="Please enter LL Work Completion Time" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revllwork" runat="server" ControlToValidate="txtllwork"
                                                                ErrorMessage="Please enter valid LL Work Completion Time" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <asp:TextBox ID="txtllwork" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Electric Load & Meter Existing</label>
                                                            <asp:RegularExpressionValidator ID="refElectricex" runat="server" ControlToValidate="txtElectricEx"
                                                                ErrorMessage="Please enter Electric Load & Meter Existing" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Only Numeric values')" onmouseout="UnTip()">
                                                                <div>
                                                                    <asp:TextBox ID="txtElectricEx" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Electric Load & Meter Requirement</label>
                                                            <asp:RegularExpressionValidator ID="refElectricrq" runat="server" ControlToValidate="txtElectricRq"
                                                                ErrorMessage="Please enter Electric Load & Meter Requirement" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Only Numeric values')" onmouseout="UnTip()">
                                                                <div>
                                                                    <asp:TextBox ID="txtElectricRq" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default " role="tab" id="div3">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Purchase">Purchase Details</a>
                                            </h4>
                                        </div>
                                        <div id="Purchase" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Purchase Price </label>
                                                            <asp:RegularExpressionValidator ID="revtxtPurPrice" runat="server" ControlToValidate="txtPurPrice"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Purchase Price" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtPurPrice" runat="server" Enabled="false" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Purchase Date</label>
                                                            <div>
                                                                <div class='input-group date' id='PurDate1'>
                                                                    <asp:TextBox ID="txtPurDate" runat="server" Enabled="false" CssClass="form-control"> </asp:TextBox>
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar" onclick="setup('PurDate1')"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Market Value </label>
                                                            <asp:RegularExpressionValidator ID="revtxtMarketValue" runat="server" ControlToValidate="txtMarketValue"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Market Value" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtMarketValue" Enabled="false" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default " role="tab" id="div4">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Govt">Regulator Details</a>
                                            </h4>
                                        </div>
                                        <div id="Govt" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Regulator Name</label>
                                                            <div>
                                                                <asp:TextBox ID="txtIRDA" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Branch Code</label>
                                                            <div>
                                                                <asp:TextBox ID="txtPCcode" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Government Property Code</label>
                                                            <asp:RegularExpressionValidator ID="revgovt" runat="server" ControlToValidate="txtGovtPropCode"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Government Property Code"
                                                                ValidationExpression="^[A-Za-z0-9//(/)\s/-]*$"></asp:RegularExpressionValidator>
                                                            <div>
                                                                <div onmouseover="Tip('No Special characters allowed except / - , )(')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtGovtPropCode" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>UOM CODE (Unit Of Measure)</label>
                                                            <div>
                                                                <asp:TextBox ID="txtUOM_CODE" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default " role="tab" id="div5">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Insurance">Insurance Details</a>
                                            </h4>
                                        </div>
                                        <div id="Insurance" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Type</label>
                                                            <div>
                                                                <asp:DropDownList ID="ddlInsuranceType" runat="server" Enabled="false" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Vendor</label>
                                                            <div>
                                                                <asp:TextBox ID="txtInsuranceVendor" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Amount</label>
                                                            <div>
                                                                <asp:TextBox ID="txtInsuranceAmt" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Policy Number</label>
                                                            <div>
                                                                <asp:TextBox ID="txtInsurancePolNum" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Start Date</label>
                                                            <div>
                                                                <div class='input-group date' id='fromdate'>
                                                                    <asp:TextBox ID="txtInsuranceStartdate" Enabled="false" runat="server" CssClass="form-control"> </asp:TextBox>
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>End Date</label>
                                                            <div>
                                                                <div class='input-group date' id='todate'>
                                                                    <asp:TextBox ID="txtInsuranceEnddate" Enabled="false" runat="server" CssClass="form-control"> </asp:TextBox>
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default " role="tab" id="div10">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Cost">Cost Details</a>
                                            </h4>
                                        </div>
                                        <div id="Cost" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Expected Rent per month</label>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtRent"
                                                                ErrorMessage="Please enter valid Expected Rent per month" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Only Numeric values')" onmouseout="UnTip()">
                                                                <div>
                                                                    <asp:TextBox ID="txtRent" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Rent per sqft</label>
                                                            <asp:RegularExpressionValidator ID="revsft" runat="server" ControlToValidate="txtsft"
                                                                ErrorMessage="Please enter valid Rent per sqft" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Only Numeric values')" onmouseout="UnTip()">
                                                                <div>
                                                                    <asp:TextBox ID="txtsft" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Rent payment ratio if any</label>
                                                            <div>
                                                                <asp:TextBox ID="txtRatio" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Owner's Share (%)</label>
                                                            <div>
                                                                <asp:TextBox ID="txtownshare" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Security Deposit: No of Months</label>
                                                            <asp:RegularExpressionValidator ID="refsecdepmonths" runat="server" ControlToValidate="txtsecdepmonths"
                                                                ErrorMessage="Please enter valid Security Deposit: No of Months" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Only Numeric values')" onmouseout="UnTip()">
                                                                <div>
                                                                    <asp:TextBox ID="txtsecdepmonths" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>GST Registered</label>
                                                            <asp:DropDownList ID="ddlgst" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Maintenance Charges</label>
                                                            <div>
                                                                <asp:TextBox ID="txtmaintenance" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Escalation in rentals</label>
                                                            <div>
                                                                <asp:TextBox ID="txtesc" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Fit out(rent free) period</label>
                                                            <asp:RegularExpressionValidator ID="revrentfree" runat="server" ControlToValidate="txtRentFree"
                                                                ErrorMessage="Please enter valid Fit out(rent free) period" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Only Numeric values')" onmouseout="UnTip()">
                                                                <div>
                                                                    <asp:TextBox ID="txtRentFree" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Stamp Duty & Registration</label>
                                                            <asp:DropDownList ID="ddlStampReg" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Lessor">Lessor</asp:ListItem>
                                                                <asp:ListItem Value="50/50">50/50</asp:ListItem>
                                                                <asp:ListItem Value="Lessee">Lessee</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Lease Agreement period(Months)</label>
                                                            <asp:RegularExpressionValidator ID="revAgree" runat="server" ControlToValidate="txtAgree"
                                                                ErrorMessage="Please enter valid Lease Agreement period" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Only Numeric values')" onmouseout="UnTip()">
                                                                <div>
                                                                    <asp:TextBox ID="txtAgree" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="panel panel-default " role="tab" id="div11">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Other">Other Details</a>
                                            </h4>
                                        </div>
                                        <div id="Other" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Flooring: (2x2 Vitrified)</label>
                                                            <asp:DropDownList ID="ddlFlooring" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div id="flrRmks" visible="false" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Flooring Remarks</label>
                                                            <div>
                                                                <asp:TextBox ID="txtFlooring" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Exclusive Washrooms Existing</label>
                                                            <asp:RegularExpressionValidator ID="revexisting" runat="server" ControlToValidate="txtWashExisting"
                                                                ErrorMessage="Please enter valid Exclusive Washrooms Existing" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Only Numeric values')" onmouseout="UnTip()">
                                                                <div>
                                                                    <asp:TextBox ID="txtWashExisting" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Exclusive Washrooms Requirement</label>
                                                            <asp:RegularExpressionValidator ID="refRequirement" runat="server" ControlToValidate="txtWashRequirement"
                                                                ErrorMessage="Please enter valid Exclusive Washrooms Requirement" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Only Numeric values')" onmouseout="UnTip()">
                                                                <div>
                                                                    <asp:TextBox ID="txtWashRequirement" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Potable Water</label>
                                                            <asp:DropDownList ID="ddlPotableWater" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">


                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Signages Space Length</label>
                                                            <asp:RegularExpressionValidator ID="revsiglength" runat="server" ControlToValidate="txtsiglength"
                                                                ErrorMessage="Please enter valid Signages Space Length" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Only Numeric values')" onmouseout="UnTip()">
                                                                <div>
                                                                    <asp:TextBox ID="txtsiglength" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Signages Space Width</label>
                                                            <asp:RegularExpressionValidator ID="revsigwidth" runat="server" ControlToValidate="txtsigwidth"
                                                                ErrorMessage="Please enter valid Signages Space Width" ValidationGroup="Val2" Display="None"
                                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                                            <div onmouseover="Tip('Only Numeric values')" onmouseout="UnTip()">
                                                                <div>
                                                                    <asp:TextBox ID="txtsigwidth" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>AC Outdoor unit space</label>
                                                            <div>
                                                                <asp:TextBox ID="txtACOutdoor" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Space for GSB</label>
                                                            <div>
                                                                <asp:TextBox ID="txtGSB" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default " role="tab" id="div12">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Documents">Documents available for Legal Req</a>
                                            </h4>
                                        </div>
                                        <div id="Documents" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Title Doucment</label>
                                                            <%--         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlvitrified" Display="None" ValidationGroup="Val1"
                                                            ErrorMessage="Please Select Vitrified Flooring as per specifications" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                                            <asp:DropDownList ID="ddlTitle" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remarks </label>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtVitriRemarks"
                                                            Display="None" ErrorMessage="Please enter Vitrified Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                            <div>
                                                                <asp:TextBox ID="txtTitleRemarks" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Occupancy & Completion Certificate</label>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlWashroom" Display="None" ValidationGroup="Val1"
                                                            ErrorMessage="Please Select Two Washrooms" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                                            <asp:DropDownList ID="ddlOccupancy" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remarks</label>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtWashroom"
                                                            Display="None" ErrorMessage="Please enter Two Washrooms Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                            <div>
                                                                <asp:TextBox ID="txtOccupancyRemarks" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Building Sanction Plan</label>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlPantry" Display="None" ValidationGroup="Val1"
                                                            ErrorMessage="Please Select Pantry" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                                            <asp:DropDownList ID="ddlBuilding" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remarks</label>
                                                            <%--        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPantry"
                                                            Display="None" ErrorMessage="Please enter Pantry Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                            <div>
                                                                <asp:TextBox ID="txtBuildingRemarks" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Copy of PAN Card of landlord/s</label>
                                                            <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlShutter" Display="None" ValidationGroup="Val1"
                                                            ErrorMessage="Please Select Rolling Shutter with boxing" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                                            <asp:DropDownList ID="ddlPAN" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remarks</label>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtShutter"
                                                            Display="None" ErrorMessage="Please enter Rolling Shutter remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                            <div>
                                                                <asp:TextBox ID="txtPANRemarks" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Latest tax paid receipt</label>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlOthers" Display="None" ValidationGroup="Val1"
                                                            ErrorMessage="Please Select Two Washrooms" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                                            <asp:DropDownList ID="ddlTax" runat="server" Enabled="false" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true">
                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remarks</label>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtOthers"
                                                            Display="None" ErrorMessage="Please enter Others remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                            <div>
                                                                <asp:TextBox ID="txtTaxRemarks" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Other information if any</label>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtOthers"
                                                            Display="None" ErrorMessage="Please enter Others remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                            <div>
                                                                <asp:TextBox ID="txtOtherInfo" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default " role="tab" id="div6">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#PropDocs">Property Images</a>
                                            </h4>
                                        </div>
                                        <div id="PropDocs" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row form-inline">
                                                    <div class="form-group col-md-12">
                                                        <asp:GridView ID="gvPropdocs" runat="server" EmptyDataText="No Property Images Found." AllowPaging="True"
                                                            PageSize="10" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                                            <PagerSettings Mode="NumericFirstLast" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Document Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblImgNam" runat="server" Text='<%#Eval("PM_IMG_NAME")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Details">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblImgType" runat="server" Text='<%#Eval("PM_IMG_TYPE")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkDownload" runat="server" Text="Download" CommandArgument='<%#Eval("PM_IMG_PATH")%>'
                                                                            CommandName="Download" CausesValidation="false"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle />
                                                            <PagerStyle CssClass="pagination-ys" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fu1"
                                                                ValidationGroup="Val1" runat="Server" ErrorMessage="Only jpg,jpeg, png, gif files are allowed"
                                                                ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$">
                                                            </asp:RegularExpressionValidator>
                                                            <asp:FileUpload ID="fu1" runat="Server" Width="90%" Visible="false" AllowMultiple="True" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default " role="tab" id="div7">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#EPD">Existing Property Details</a>
                                            </h4>
                                        </div>
                                        <div id="EPD" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Location</label>
                                                            <div>
                                                                <%--  <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control" MaxLength="14"></asp:TextBox>--%>
                                                                <asp:Label ID="lblLoc" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>No of Properties</label><br />
                                                            <asp:Label ID="txtpptsCount" runat="server"></asp:Label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Current Month Rent</label><br />
                                                            <asp:Label ID="txtMonth" runat="server"></asp:Label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Total Sqft</label><br />
                                                            <asp:Label ID="txtsqft" runat="server"></asp:Label>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <%--                                    <div class="row">
                                        <div class="form-group col-sm-6 col-xs-6"></div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span>Remarks</label>
                                                <div>
                                                    <div onmouseover="Tip('Enter Remarks with maximum 500 characters')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtIndiRemarks" runat="server" CssClass="form-control"
                                                            Rows="3" TextMode="Multiline" MaxLength="500"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-3 col-xs-6 text-right">
                                            <br />
                                            <br />
                                            <br />
                                            <asp:Button ID="btnIndApprove" runat="server" Class="btn btn-primary btn-mm" Text="Approve" CausesValidation="true" ValidationGroup="Val1" />
                                            <asp:Button ID="btnIndReject" runat="server" Class="btn btn-primary btn-mm" Text="Reject" CausesValidation="true" ValidationGroup="Val1" />
                                        </div>
                                    </div>--%>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
