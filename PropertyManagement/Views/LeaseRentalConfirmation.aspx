﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LeaseRentalConfirmation.aspx.cs" Inherits="PropertyManagement_LeaseRentalConfirmation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<%--<html lang="en" data-ng-app="QuickFMS">--%>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <script src="../../../../Scripts/moment.min.js"></script>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        #divScroll {
            height: 200px;
            overflow-y: auto;
            width: auto !important;
        }
    </style>
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }
        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= this.gvLandLord.ClientID%>');
            var TargetChildControl = "chkSelect";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvLandLord.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {

                    return true;

                }
            }
            alert("Please select atleast one checkbox");
            return false;

        }
        function UploadFile() {
            event.preventDefault();
            var param = new FormData($('form')[0]);
            console.log(param);
            debugger;
            $.ajax({
                type: "POST",
                url: 'http://localhost:55730/api/LeaseRentalReport/UploadproprtyleaseRent',    // CALL WEB API TO SAVE THE FILES.
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                cache: false,
                data: param,
                dataType: "json",
                //success: OnSuccess,
                success: function (response) {
                    console.log(response.data)

                    var tbl_body = '<div style="margin-left: 50px;width: 249px;" id="divScroll"><table class="table table-fixed" id="example"><thead><tr><th class="col-md-2 col-sm-6 col-xs-12">Sno</th><th class="col-md-2 col-sm-6 col-xs-12">Lease Id</th><th class="col-md-2 col-sm-6 col-xs-12">State</th><th class="col-md-2 col-sm-6 col-xs-12">Location</th><th class="col-md-2 col-sm-6 col-xs-12">Landlord Name</th><th class="col-md-2 col-sm-6 col-xs-12">Landlord Number</th><th class="col-md-2 col-sm-6 col-xs-12">Landlord Amount</th><th class="col-md-2 col-sm-6 col-xs-12">Status</th><th class="col-md-2 col-sm-6 col-xs-12">Month</th><th class="col-md-2 col-sm-6 col-xs-12">Company</th></tr></thead><tbody>';

                    for (i = 0; i < response.data.length; i++) {
                        tbl_body += '<tr>';

                        tbl_body += '<td class="col-md-3 col-sm-6 col-xs-12 " >';
                        tbl_body += response.data[i]["SNO"];
                        tbl_body += '</td>';

                        tbl_body += '<td class="col-md-3 col-sm-6 col-xs-12" >';
                        tbl_body += response.data[i]["LEASE_ID"];
                        tbl_body += '</td>';
                        tbl_body += '<td class="col-md-3 col-sm-6 col-xs-12" >';
                        //console.log(response.data.Table[i]["STATE"])
                        tbl_body += response.data[i]["STATE"];
                        tbl_body += '</td>';
                        tbl_body += '<td class="col-md-3 col-sm-6 col-xs-12" >';
                        tbl_body += response.data[i]["Location"];
                        tbl_body += '</td>';
                        tbl_body += '<td class="col-md-3 col-sm-6 col-xs-12" >';
                        tbl_body += response.data[i]["Landlord_Name"];
                        tbl_body += '</td>';
                        tbl_body += '<td class="col-md-3 col-sm-6 col-xs-12" >';
                        tbl_body += response.data[i]["Landlord_Number"];
                        tbl_body += '</td>';
                        tbl_body += '<td class="col-md-3 col-sm-6 col-xs-12" >';
                        tbl_body += response.data[i]["LANDLORDAMOUNT"];
                        tbl_body += '</td>';
                        tbl_body += '<td class="col-md-3 col-sm-6 col-xs-12" >';
                        tbl_body += response.data[i]["Status"];
                        tbl_body += '</td>';
                        tbl_body += '<td class="col-md-3 col-sm-6 col-xs-12" >';
                        tbl_body += response.data[i]["MONTH"];
                        tbl_body += '</td>';
                        tbl_body += '<td class="col-md-3 col-sm-6 col-xs-12" >';
                        tbl_body += response.data[i]["Company"];
                        tbl_body += '</td>';

                        tbl_body += '</td>';
                        tbl_body += '</tr>';

                    }
                    tbl_body += '</tbody></table></div>';
                    $('#tableScoreDiv').html(tbl_body);
                    $('#tableScoreDiv').show();
                    $('#gvItems').hide();
                    return false;
                },
                error: function (xhr, textStatus, errorThrown) {

                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        }
    </script>
    <style type="text/css">
        .table-fixed {
            width: 100%;
            background-color: #f3f3f3;
        }



        /*table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            float: left;
            /*background-color: #f39c12;*/
        /*border-color: #e67e22;*/
    </style>
    <style type="text/css">
        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .modalPopup {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 800px;
            height: 500px;
        }

        .btnpopup {
            display: none;
        }
    </style>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Rental Confirmation" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Rental Payment </h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
                        <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
                        <asp:UpdatePanel ID="CountryPanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />

                                <%-- <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="true" ShowValidationErrors="true" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />--%>
                                <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowSummary="true" ShowValidationErrors="true" ForeColor="Red" ValidationGroup="Val3" DisplayMode="List" />

                                <div id="AddLeaseDetails" runat="server">
                                    <div class=" panel panel-default" runat="server" id="divspace">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Upload Rental Payment</a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body color">

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>From Date<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="startdate" runat="server" ControlToValidate="FROM_DATE"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Select from Date"></asp:RequiredFieldValidator>
                                                            <div class='input-group date' id='strtdate'>
                                                                <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="FROM_DATE" runat="server" CssClass="form-control" TabIndex="55" AutoPostBack="true"></asp:TextBox>
                                                                </div>
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar" onclick="setup('strtdate')"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>To Date<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TO_DATE"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Select from Date"></asp:RequiredFieldValidator>
                                                            <div class='input-group date' id='todate'>
                                                                <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="TO_DATE" runat="server" CssClass="form-control" TabIndex="55" AutoPostBack="true"></asp:TextBox>
                                                                </div>
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="control-label">Company</label>
                                                            <asp:DropDownList ID="ddlcompany" runat="server" CssClass="selectpicker" data-live-search="true"
                                                                ToolTip="--Select--" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group" style="padding-top: 10%">
                                                            <asp:Button ID="btndownload" runat="server" Text="Download" CssClass="btn btn-default btn-primary" OnClick="btndownload_click"></asp:Button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <label class="col-md-3 control-label">Upload Document  (Only Excel ) <span style="color: red;">*</span></label>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="None" ErrorMessage="Please Select File"
                                                                            ControlToValidate="fpBrowseDoc" ValidationGroup="Val3"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" ControlToValidate="fpBrowseDoc"
                                                                            ValidationGroup="Val3" runat="Server" ErrorMessage="Only Excel file allowed"
                                                                            ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> </asp:RegularExpressionValidator>
                                                                        <div class="col-md-3">
                                                                            <div class="btn btn-default">
                                                                                <i class="fa fa-folder-open-o fa-lg"></i>
                                                                                <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4  control-label">
                                                                            <asp:Button ID="btnbrowse" runat="Server" CssClass="btn btn-primary custom-button-color" class="col-md-3" Text="Upload" ValidationGroup="Val3" OnClientClick="UploadFile()" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnbrowse" />
                                                    </Triggers>
                                                </asp:UpdatePanel>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row" style="padding-top: 10px">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-10 control-label">Filter By Lease ID / Property Code / Name/ City/ Location<span style="color: red;">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                                        Display="none" ErrorMessage="Filter By Property Name / Lease Id / Location Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-md-5">
                                                    <asp:Button ID="btnsearch" CssClass="btn btn-default btn-primary" runat="server" Text="Search" ValidationGroup="Val1"
                                                        CausesValidation="true" TabIndex="2" OnClick="btnsearch_Click" />
                                                    <asp:Button ID="txtreset" CssClass="btn btn-default btn-primary" runat="server" Text="Reset"
                                                        CausesValidation="false" TabIndex="2" OnClick="txtreset_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tableScoreDiv" class="form-group" runat="server" visible="true">
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" AllowSorting="True" Visible="true"
                                        AllowPaging="True" PageSize="5" EmptyDataText="No Records Found." CssClass="table table-condensed table-bordered table-hover table-striped"
                                        Style="font-size: 12px;" OnPageIndexChanging="gvItems_PageIndexChanging" OnRowCommand="gvItems_RowCommand">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Lease Name" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllname" runat="server" Text='<%#Eval("LEASE_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblsno" runat="server" Text='<%#Eval("pm_les_sno") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Id">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblextend" runat="server" CssClass="lblCODE" Text='<%#Eval("LEASE_ID")%>' CommandName="LeaseRent"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Code">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCode" runat="server" CssClass="lblCODE" Text='<%#Eval("PM_PPT_CODE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblName" runat="server" CssClass="lblName" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Entity">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEntityType" Text='<%#Eval("CHE_NAME")%>' runat="server" CssClass="lblEntityType"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="City">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblcity" runat="server" CssClass="lblcity" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllcm" runat="server" CssClass="lblLCM" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Start Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsdate" runat="server" CssClass="lblstartdate" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Expiry Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEdate" runat="server" CssClass="lblExpiryDate" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lease Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStatus" runat="server" CssClass="lbluser" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Rent" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRentRent" Text='<%#Eval("PM_LES_TOT_RENT","{0:c2}")%>' runat="server" CssClass="lblRentRent"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Rent" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRent" Text='<%#Eval("PM_LES_TOT_RENT")%>' runat="server" CssClass="lblRent"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Service Tax" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSertax" Text='<%#Eval("PM_LC_SERVICE_TAX")%>' runat="server" CssClass="lblSertax"></asp:Label>
                                                    <asp:Label ID="lblgst" Text='<%#Eval("PM_LC_GST")%>' runat="server" CssClass="lblgst"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Payment Term" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPayTerm" runat="server" CssClass="lbluser" Text='<%#Eval("PM_PT_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                                <asp:ValidationSummary ID="ValidationSummary4" runat="server" ShowSummary="true" ShowValidationErrors="true" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>

                                    <%--<div class="row col-md-3 col-sm-12 col-xs-12 pull-right text-align-last">
                                                 <a href="#" ID="LinkButton1" Visible="false" onclick="showPopWin('+txtLeaseId.text+')">Click here to View Pending Payments</a>
                                                 <asp:LinkButton  runat="server" Style="font-size: small" OnClick="LinkButton1_Click">Click here to View Pending Payments</asp:LinkButton>
                                            </div>
                                            <br />--%>

                                    <%-- <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>--%>
                                    <%-- <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="gvLandlordPay" TargetControlID="LinkButton1"
                                                CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                                            </cc1:ModalPopupExtender>
                                    --%>
                                    <%--  <asp:Panel ID="Panel2" runat="server" CssClass="modalPopup" align="center" Style="display: none">
                                                <asp:GridView ID="gvLandlordPay" runat="server" EmptyDataText="No Payments Done."
                                                    AllowSorting="false" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped" AlternatingRowStyle-CssClass="alt"
                                                    PagerStyle-CssClass="pgr" AllowPaging="true" PageSize="5">
                                                    <PagerSettings Mode="NumericFirstLast" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Request Id">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPropType" runat="server" Text='<%#Eval("LEASE_ID")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="STATUS">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblProsstats" runat="server" Text='<%#Eval("STATUS")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="MONTH">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPropCode" runat="server" Text='<%#Eval("MONTHS")%>'>
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle />
                                                    <PagerStyle CssClass="pagination-ys" />
                                                </asp:GridView>
                                                <br />
                                                <asp:Button ID="btnClose" runat="server" Text="Close" />
                                            </asp:Panel>--%>
                                </div>

                                <div id="panel1" runat="Server">
                                    <div class="row" style="padding-top: 10px">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Lease Id</label>
                                                <input type="hidden" id="llsno" name="abcName" runat="server" />
                                                <%--<asp:TextBox runat="server" ID="llsno" Visible="false"></asp:TextBox>--%>
                                                <asp:TextBox ID="txtLeaseId" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Payment Terms</label>
                                                <asp:TextBox ID="txtPaymentTerm" runat="server" CssClass="form-control" Enabled="false">                                                    
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Months</label>
                                                <asp:RequiredFieldValidator ID="rfvmonth" runat="server" ControlToValidate="ddlMonths"
                                                    Display="none" ErrorMessage="Please Select Month" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <%-- <asp:DropDownList ID="ddlMonths" runat="server" CssClass="selectpicker" data-live-search="true"
                                                            ToolTip="--Select--" OnSelectedIndexChanged="ddlMonths_SelectedIndexChanged" AutoPostBack="true">
                                                        </asp:DropDownList>--%>
                                                <asp:ListBox ID="ddlMonths" runat="server" SelectionMode="Multiple" CssClass="selectpicker" data-live-search="true"
                                                    OnSelectedIndexChanged="ddlMonths_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Years</label>
                                                <asp:RequiredFieldValidator ID="rfvyear" runat="server" ControlToValidate="ddlYears"
                                                    Display="none" ErrorMessage="Please Select Year" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlYears" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    ToolTip="--Select--" OnSelectedIndexChanged="ddlYears_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Payment Date</label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="PaymentDate"
                                                    Display="none" ErrorMessage="Please Select Payment Date" ValidationGroup="Val2">
                                                </asp:RequiredFieldValidator>
                                                <div class='input-group date' id='PayDate'>
                                                    <asp:TextBox ID="PaymentDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('PayDate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Rent Amount</label>
                                                <asp:TextBox ID="txtCost" TextMode="Number" runat="server" CssClass="form-control" Enabled="false">0</asp:TextBox>
                                            </div>
                                        </div>
                                        <%-- <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Service Tax (%)</label>
                                                        <asp:TextBox ID="txtSerTax" TextMode="Number" min="0" runat="server" CssClass="form-control" Enabled="TRUE" OnTextChanged="txtSerTax_TextChanged" AutoPostBack="True">0</asp:TextBox>
                                                    </div>
                                                </div>--%>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>GST</label>
                                                <asp:RequiredFieldValidator ID="rfvgst" runat="server" ControlToValidate="ddlGST"
                                                    Display="none" ErrorMessage="Please Select GST" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlGST" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    ToolTip="--Select--" OnSelectedIndexChanged="ddlGST_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="">--Select--</asp:ListItem>
                                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                    <asp:ListItem Value="No">No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="clearfix" id="divgst" runat="server">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>GST (%)</label>
                                                    <asp:TextBox ID="txtSerTax" TextMode="Number" min="0" runat="server" CssClass="form-control" Enabled="TRUE" OnTextChanged="txtSerTax_TextChanged" AutoPostBack="True">0</asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <div class="form-group">
                                                        <label>Withhold (%)</label>
                                                        <asp:TextBox ID="txtWithhold" runat="server" TextMode="Number" min="0" data-live-search="true" AutoPostBack="True" OnTextChanged="txtWithhold_TextChanged">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>--%>

                                    <div class="row col-md-3 col-sm-12 col-xs-12 pull-right text-align-last">
                                        <a href="#" id="LinkButton1" visible="false" onclick="showPopWin(document.getElementById('txtLeaseId').value,document.getElementById('llsno').value)">Click here to View Pending Payments</a>
                                    </div>

                                    <br />
                                    <div class="row" style="overflow-x: auto; width: auto">
                                        <div class="col-md-12">
                                            <asp:GridView ID="gvLandLord" runat="server" AllowPaging="True" AutoGenerateColumns="false" ShowFooter="true"
                                                EmptyDataText="No Records Found." CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10"
                                                OnPageIndexChanging="gvLandLord_PageIndexChanging" OnRowDataBound="gvLandLord_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                                                ToolTip="Click to check all" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                                        <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Landlord Number" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LL_NUM" runat="server" Text='<%#Eval("NUMBER") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Lease Id" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="requisition_Sno" runat="server" Visible="false" Text='<%#Eval("PM_LL_PM_LES_SNO") %>'></asp:Label>
                                                            <asp:Label ID="lblLeaseId" runat="server" Text='<%#Eval("LEASE_ID") %>' CommandName="LandlordRent"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Landlord Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="llId" runat="server" Visible="false" Text='<%#Eval("SNO") %>'></asp:Label>
                                                            <asp:Label ID="lblLseName" runat="server" Text='<%#Eval("PM_LL_NAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Month">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblmonth" Text='<%#Eval("MONTHS")%>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Address" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAdd" Text='<%#Eval("PM_LL_ADDRESS1")%>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Basic Rent Amount">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRentAmt" Text='<%#Eval("PM_LL_MON_RENT_PAYABLE")%>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Maintainance">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblmaintcrgs" Text='<%#Eval("PM_LL_MAINT_CHARGES")%>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Amenities">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAmenties" Text='<%#Eval("PM_LL_AMENITIES_CHARGES")%>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="TDS (%)">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtTDSAmt" TextMode="Number" min="0" runat="server" OnTextChanged="txtTDSAmt_TextChanged" AutoPostBack="true"
                                                                Enabled='<%# Eval("PM_LL_TDS").ToString() == "1" ? true : false %>'>0</asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Due Amount">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDue" Text='<%#Eval("PM_LP_DUE_AMT")%>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="ddlstatus" runat="server" Text='<%#Eval("STATUS")%>'>
                                                                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                                        <asp:ListItem Value="1">Release</asp:ListItem>
                                                                        <asp:ListItem Value="2">Hold</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                    <asp:TemplateField HeaderText="Payment Type">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlrentpay" runat="server" AutoPostBack="True" OnTextChanged="ddlrentpay_TextChanged" Text='<%#Eval("STATUS")%>'>
                                                                <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Rent+Maint+Amenit">Rent+Maint+Amenit</asp:ListItem>
                                                                <asp:ListItem Value="Rent">Rent</asp:ListItem>
                                                                <asp:ListItem Value="Maintainance">Maintainance</asp:ListItem>
                                                                <asp:ListItem Value="Amenities">Amenities</asp:ListItem>
                                                                <asp:ListItem Value="Rent+Maint">Rent+Maint</asp:ListItem>
                                                                <asp:ListItem Value="Rent+Ameni">Rent+Amenities</asp:ListItem>
                                                                <asp:ListItem Value="Maint+Amenit">Maint+Amenit</asp:ListItem>
                                                                <asp:ListItem Value="Hold">Hold</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Maintainance Paid by" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="mntpiadby" Text='<%#Eval("PM_LC_MAINT_PAIDBY")%>' runat="server"></asp:Label>
                                                            <asp:Label ID="amenpidby" Text='<%#Eval("PM_LC_AMENT_PAIDBY")%>' runat="server"></asp:Label>
                                                            <asp:Label ID="paidrent" Text='<%#Eval("STATUS")%>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Total Landlord Rent">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTLLRent" Text="0" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Withold (%)">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtWithhold" TextMode="Number" runat="server" data-live-search="true" AutoPostBack="True" OnTextChanged="txtWithhold_TextChanged" min="0">0</asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Withhold Amount">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblWHAmt" runat="server">0</asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblTotal" Text="Total" runat="server" Font-Bold="True" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total Payable">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPayable" runat="server">0</asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblTotalRent" runat="server" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-top: 10px;" id="holdrent" runat="server">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Invoice No.<span style="color: red;">*</span></label>
                                                <div>
                                                    <asp:RequiredFieldValidator ID="rfcinvc" runat="server" ControlToValidate="txtinvno"
                                                        Display="none" ErrorMessage="Please Enter Invoice No." ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="txtinvno" runat="server" CssClass="form-control" MaxLength="500"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Invoice Date<span style="color: red;">*</span></label>
                                                <div>
                                                    <asp:RequiredFieldValidator ID="rfcinvdt" runat="server" ControlToValidate="txtinvdt"
                                                        Display="none" ErrorMessage="Please Enter Invoice Date " ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                    <div class='input-group date' id='invdate'>
                                                        <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtinvdt" runat="server" CssClass="form-control" MaxLength="500"></asp:TextBox>
                                                        </div>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('invdate')"></span>
                                                        </span>
                                                    </div>
                                                    <%--<asp:RequiredFieldValidator ID="rfcinvdt" runat="server" ControlToValidate="txtRemarks"
                                                                Display="none" ErrorMessage="Please Enter Invoice Date " ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                            <asp:TextBox ID="txtinvdt" runat="server" CssClass="form-control" MaxLength="500"></asp:TextBox>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>PaymentMode<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvddlpaymentmode" runat="server" ControlToValidate="ddlpaymentmode" Display="None" ValidationGroup="Val2"
                                                    ErrorMessage="Please Select Payment Mode" InitialValue="0"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlpaymentmode" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" TabIndex="48"
                                                    OnTextChanged="ddlpaymentmode_selectedindexchanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Remarks<span style="color: red;">*</span></label>
                                                <div>
                                                    <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="txtRemarks"
                                                        Display="none" ErrorMessage="Please Enter Remarks !" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Height="30%" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row" id="panel3" runat="server">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Bank Name <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                                    Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revBankName" Display="None" ValidationGroup="Val2" runat="server" ControlToValidate="txtBankName"
                                                    ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtBankName" runat="server" TabIndex="49" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Account Number<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                                    Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="ftetxtAccNo" runat="server" TargetControlID="txtAccNo" FilterType="Numbers" ValidChars="0123456789" />
                                                <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtAccNo" runat="server" TabIndex="50" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <%--<div class="row" id="Div1" runat="server">--%>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Cheque/DD Number<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvChequeNo" runat="server" ControlToValidate="txtChequeNo"
                                                    Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Cheque/DD Number"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="ftetxtChequeNo" runat="server" TargetControlID="txtChequeNo" FilterType="Numbers" ValidChars="0123456789" />
                                                <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtChequeNo" runat="server" TabIndex="50" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Date<span style="color: red;">*</span></label>
                                                <div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtChequedate"
                                                        Display="none" ErrorMessage="Please Enter Date " ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                    <div class='input-group date' id='Chequdate'>
                                                        <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtChequedate" runat="server" CssClass="form-control" MaxLength="500"></asp:TextBox>
                                                        </div>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('Chequdate')"></span>
                                                        </span>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <%--</div>--%>
                                    </div>

                                    <div class="row" id="panel2" runat="server">
                                        <div class="row" style="padding-left: 11px; padding-right: 11px;">

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Bank Name <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtNeftBank" runat="server" ControlToValidate="txtNeftBank"
                                                        Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revtxtNeftBank" Display="None" ValidationGroup="Val2" runat="server" ControlToValidate="txtNeftBank"
                                                        ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtNeftBank" runat="server" TabIndex="51" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Account Number <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtNeftAccNo" runat="server" ControlToValidate="txtNeftAccNo"
                                                        Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                                    <cc1:FilteredTextBoxExtender ID="ftetxtNeftAccNo" runat="server" TargetControlID="txtNeftAccNo" FilterType="Numbers" ValidChars="0123456789" />
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtNeftAccNo" runat="server" TabIndex="52" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Branch Name <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtNeftBrnch" runat="server" ControlToValidate="txtNeftBrnch"
                                                        Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Branch Name"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revtxtNeftBrnch" Display="None" ValidationGroup="Val2" runat="server" ControlToValidate="txtNeftBrnch"
                                                        ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtNeftBrnch" runat="server" TabIndex="53" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>IFSC Code <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvtxtNeftIFSC" runat="server" ControlToValidate="txtNeftIFSC"
                                                        Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter IFSC Code"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revtxtNeftIFSC" Display="None" ValidationGroup="Val2"
                                                        runat="server" ControlToValidate="txtNeftIFSC" ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtNeftIFSC" runat="server" TabIndex="54" CssClass="form-control" MaxLength="16"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-left: 11px; padding-right: 11px;">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>NEFT Number<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtNEFTNo"
                                                        Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter NEFT Number"></asp:RequiredFieldValidator>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtNEFTNo" FilterType="Numbers" ValidChars="0123456789" />
                                                    <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtNEFTNo" runat="server" TabIndex="50" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Date<span style="color: red;">*</span></label>
                                                    <div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtNEFTdate"
                                                            Display="none" ErrorMessage="Please Enter Date " ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                        <div class='input-group date' id='NEFTdate'>
                                                            <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtNEFTdate" runat="server" CssClass="form-control" MaxLength="500"></asp:TextBox>
                                                            </div>
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('NEFTdate')"></span>
                                                            </span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px; padding-right: 10px;">

                                        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                            <div class="form-group">
                                                <asp:Button ID="btnSubmit" CssClass="btn btn-default btn-primary" runat="server" Text="Pay"
                                                    ValidationGroup="Val2" OnClientClick="javascript:return validateCheckBoxesMyReq();" OnClick="btnSubmit_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Lease Rental Pending Payments</h3>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <iframe id="modalcontentframe" src="#" width="100%" height="300px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <%--  <script>
        var app = angular.module('QuickFMS', ["agGrid"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();
        });

        function setDateVals() {
            $('#FROM_DATE').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#TO_DATE').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FROM_DATE').datepicker('setDate', new Date(moment().subtract(0, 'month').startOf('month').format('DD-MMM-YYYY')));
            $('#TO_DATE').datepicker('setDate', new Date(moment().subtract(0, 'month').endOf('month').format('DD-MMM-YYYY')));
        }

        function refreshSelectpicker() {
            $("#<%=ddlMonths.ClientID%>").selectpicker();
            $("#<%=ddlYears.ClientID%>").selectpicker();
            $("#<%=ddlGST.ClientID%>").selectpicker();
            $("#<%=ddlpaymentmode.ClientID%>").selectpicker();
        }
        refreshSelectpicker();


    </script>
    <script>
        function showPopWin(id, id2) {
            $("#modalcontentframe").attr("src", "../../PropertyManagement/Views/ViewLeasePayments.aspx?Lease=" + id + " &lsno=" + id2);
            $("#myModal").modal().fadeIn();
        }
    </script>
    <%-- <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/LeaseRentalConfirmation.js"></script>--%>
</body>
</html>
