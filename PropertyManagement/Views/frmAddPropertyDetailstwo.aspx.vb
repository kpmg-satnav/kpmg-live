Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic

Partial Class WorkSpace_SMS_Webfiles_frmAddPropertyDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindAcquisitionthrough()
            BindRequestTypes()
            BindPropType()
            BindInsuranceType()
            BindUserCities()
            BindFlooringTypes()
            ddlFSI.SelectedIndex = 1
            ddlRecommended.SelectedIndex = 1
            If Request.QueryString("Rid") <> Nothing Then   'Getting Rid from View Property Details
                txtReqID.Text = Request.QueryString("Rid")
            Else
                BindReqId()
            End If
        End If
    End Sub

    Public Sub insertnewrecord()
        Try
            Dim param As SqlParameter() = New SqlParameter(66) {}

            'General Details
            param(0) = New SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
            param(0).Value = txtReqID.Text
            param(1) = New SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar)
            param(1).Value = ddlReqType.SelectedValue
            param(2) = New SqlParameter("@PROP_NATURE", SqlDbType.VarChar)
            param(2).Value = ddlPprtNature.SelectedValue
            param(3) = New SqlParameter("@ACQ_TRH", SqlDbType.VarChar)
            param(3).Value = ddlAcqThr.SelectedValue

            param(4) = New SqlParameter("@CITY", SqlDbType.VarChar)
            param(4).Value = ddlCity.SelectedValue
            param(5) = New SqlParameter("@LOCATION", SqlDbType.VarChar)
            param(5).Value = ddlLocation.SelectedValue
            param(6) = New SqlParameter("@TOWER", SqlDbType.VarChar)
            param(6).Value = ddlTower.SelectedValue

            param(7) = New SqlParameter("@FLOOR", SqlDbType.VarChar)
            'param(7).Value = ddlFloor.SelectedValue
            param(7).Value = ([String].Join(",", ddlFloor.Items.OfType(Of ListItem)().Where(Function(r) r.Selected).[Select](Function(r) r.Value)))
            param(8) = New SqlParameter("@TOI_BLKS", SqlDbType.Int)
            param(8).Value = IIf(txtToilet.Text = "", 0, txtToilet.Text)
            param(9) = New SqlParameter("@PROP_TYPE", SqlDbType.VarChar)
            param(9).Value = ddlPropertyType.SelectedValue
            param(10) = New SqlParameter("@PROP_NAME", SqlDbType.VarChar)
            param(10).Value = txtPropIDName.Text

            param(11) = New SqlParameter("@ESTD_YR", SqlDbType.VarChar)
            param(11).Value = IIf(txtESTD.Text = "", "", Convert.ToString(txtESTD.Text))
            param(12) = New SqlParameter("@AGE", SqlDbType.VarChar)
            param(12).Value = IIf(txtAge.Text = "", "", Convert.ToString(txtAge.Text))
            param(13) = New SqlParameter("@PROP_ADDR", SqlDbType.VarChar)
            param(13).Value = txtPropDesc.Text
            param(14) = New SqlParameter("@SING_LOC", SqlDbType.VarChar)
            param(14).Value = txtSignageLocation.Text

            param(15) = New SqlParameter("@SOC_NAME", SqlDbType.VarChar)
            param(15).Value = txtSocityName.Text
            param(16) = New SqlParameter("@LATITUDE", SqlDbType.VarChar)
            param(16).Value = txtlat.Text
            param(17) = New SqlParameter("@LONGITUDE", SqlDbType.VarChar)
            param(17).Value = txtlong.Text
            param(18) = New SqlParameter("@SCOPE_WK", SqlDbType.VarChar)
            param(18).Value = txtOwnScopeWork.Text

            param(19) = New SqlParameter("@RECM_PROP", SqlDbType.VarChar)
            param(19).Value = ddlRecommended.SelectedValue
            param(20) = New SqlParameter("@RECM_PROP_REM", SqlDbType.VarChar)
            param(20).Value = txtRecmRemarks.Text
            param(21) = New SqlParameter("@APT_NO", SqlDbType.VarChar)
            param(21).Value = txtApartmentno.Text
            param(22) = New SqlParameter("@NOOF_BEDRMS", SqlDbType.VarChar)
            param(22).Value = txtNoOfBedrooms.Text
            param(23) = New SqlParameter("@STAFF_INCHARGE", SqlDbType.VarChar)
            param(23).Value = Txtstaffincharge.Text
            param(24) = New SqlParameter("@STAFF_INCHARGE_EMAIL", SqlDbType.VarChar)
            param(24).Value = txtstaffinchargemail.Text
            param(25) = New SqlParameter("@FUR_SEMIFURNISHED", SqlDbType.VarChar)
            param(25).Value = ddlinreriortype.SelectedValue

            'Owner Details
            param(26) = New SqlParameter("@OWN_NAME", SqlDbType.VarChar)
            param(26).Value = txtownrname.Text
            param(27) = New SqlParameter("@OWN_PH", SqlDbType.VarChar)
            param(27).Value = txtphno.Text
            param(28) = New SqlParameter("@PREV_OWN_NAME", SqlDbType.VarChar)
            param(28).Value = txtPrvOwnName.Text
            param(29) = New SqlParameter("@PREV_OWN_PH", SqlDbType.VarChar)
            param(29).Value = txtPrvOwnPhNo.Text

            param(30) = New SqlParameter("@OWN_EMAIL", SqlDbType.VarChar)
            param(30).Value = txtOwnEmail.Text

            'Area Details
            param(31) = New SqlParameter("@CARPET_AREA", SqlDbType.Decimal)
            param(31).Value = Convert.ToDecimal(txtCarpetArea.Text)
            param(32) = New SqlParameter("@BUILTUP_AREA", SqlDbType.Decimal)
            param(32).Value = Convert.ToDecimal(txtBuiltupArea.Text)
            param(33) = New SqlParameter("@COMMON_AREA", SqlDbType.Decimal)
            param(33).Value = Convert.ToDecimal(txtCommonArea.Text)
            param(34) = New SqlParameter("@RENTABLE_AREA", SqlDbType.Decimal)
            param(34).Value = Convert.ToDecimal(txtRentableArea.Text)

            param(35) = New SqlParameter("@USABLE_AREA", SqlDbType.Decimal)
            param(35).Value = Convert.ToDecimal(txtUsableArea.Text)
            param(36) = New SqlParameter("@SUPER_BLT_AREA", SqlDbType.Decimal)
            param(36).Value = Convert.ToDecimal(txtSuperBulArea.Text)
            param(37) = New SqlParameter("@PLOT_AREA", SqlDbType.Decimal)
            param(37).Value = Convert.ToDecimal(txtPlotArea.Text)
            param(38) = New SqlParameter("@FTC_HIGHT", SqlDbType.Decimal)
            param(38).Value = IIf(txtCeilingHight.Text = "", 0, txtCeilingHight.Text)

            param(39) = New SqlParameter("@FTBB_HIGHT", SqlDbType.Decimal)
            param(39).Value = IIf(txtBeamBottomHight.Text = "", 0, txtBeamBottomHight.Text)
            param(40) = New SqlParameter("@MAX_CAPACITY", SqlDbType.Int)
            param(40).Value = Convert.ToInt32(txtMaxCapacity.Text)
            param(41) = New SqlParameter("@OPTIMUM_CAPACITY", SqlDbType.Int)
            param(41).Value = IIf(txtOptCapacity.Text = "", 0, txtOptCapacity.Text)
            param(42) = New SqlParameter("@SEATING_CAPACITY", SqlDbType.Int)
            param(42).Value = IIf(txtSeatingCapacity.Text = "", 0, txtSeatingCapacity.Text)

            param(43) = New SqlParameter("@FLR_TYPE", SqlDbType.VarChar)
            param(43).Value = ddlFlooringType.SelectedValue
            param(44) = New SqlParameter("@FSI", SqlDbType.VarChar)
            param(44).Value = ddlFSI.SelectedValue
            param(45) = New SqlParameter("@FSI_RATIO", SqlDbType.Decimal)
            param(45).Value = IIf(txtFSI.Text = "", 0, txtFSI.Text)
            param(46) = New SqlParameter("@PFR_EFF", SqlDbType.VarChar)
            param(46).Value = txtEfficiency.Text

            'PURCHASE DETAILS
            param(47) = New SqlParameter("@PUR_PRICE", SqlDbType.Decimal)
            param(47).Value = IIf(txtPurPrice.Text = "", 0, txtPurPrice.Text)
            param(48) = New SqlParameter("@PUR_DATE", SqlDbType.Date)
            param(48).Value = IIf(txtPurDate.Text = "", DBNull.Value, txtPurDate.Text)
            param(49) = New SqlParameter("@MARK_VALUE", SqlDbType.Decimal)
            param(49).Value = IIf(txtMarketValue.Text = "", 0, txtMarketValue.Text)

            'GOVT DETAILS
            param(50) = New SqlParameter("@IRDA", SqlDbType.VarChar)
            param(50).Value = txtIRDA.Text
            param(51) = New SqlParameter("@PC_CODE", SqlDbType.VarChar)
            param(51).Value = txtPCcode.Text
            param(52) = New SqlParameter("@PROP_CODE", SqlDbType.VarChar)
            param(52).Value = txtGovtPropCode.Text
            param(53) = New SqlParameter("@UOM_CODE", SqlDbType.VarChar)
            param(53).Value = txtUOM_CODE.Text

            'INSURANCE DETAILS
            param(54) = New SqlParameter("@IN_TYPE", SqlDbType.VarChar)
            param(54).Value = ddlInsuranceType.SelectedValue
            param(55) = New SqlParameter("@IN_VENDOR", SqlDbType.VarChar)
            param(55).Value = txtInsuranceVendor.Text
            param(56) = New SqlParameter("@IN_AMOUNT", SqlDbType.Decimal)
            param(56).Value = IIf(txtInsuranceAmt.Text = "", 0, txtInsuranceAmt.Text)
            param(57) = New SqlParameter("@IN_POLICY_NUMBER", SqlDbType.VarChar)
            param(57).Value = txtInsurancePolNum.Text

            param(58) = New SqlParameter("@IN_SDATE", SqlDbType.VarChar)
            param(58).Value = IIf(txtInsuranceStartdate.Text = "", DBNull.Value, txtInsuranceStartdate.Text)
            param(59) = New SqlParameter("@IN_EDATE", SqlDbType.VarChar)
            param(59).Value = IIf(txtInsuranceEnddate.Text = "", DBNull.Value, txtInsuranceEnddate.Text)

            param(60) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(60).Value = Session("uid")
            param(61) = New SqlParameter("@CMP_ID", SqlDbType.VarChar)
            param(61).Value = Session("COMPANYID")
            param(62) = New SqlParameter("@OBD_BANK_NAME", SqlDbType.VarChar)
            param(62).Value = txtBankName.Text
            param(63) = New SqlParameter("@OBD_BANK_ADDRESS", SqlDbType.VarChar)
            param(63).Value = txtBankAddress.Text
            param(64) = New SqlParameter("@OBD_ISFC_CODE", SqlDbType.VarChar)
            param(64).Value = txtISFCCode.Text
            param(65) = New SqlParameter("@OBD_ACCOUNT_NUM", SqlDbType.VarChar)
            param(65).Value = txtAcctnumber.Text

            ' PROPERTY IMAGES UPLOAD
            Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC As ImageClas

            If fu1.PostedFiles IsNot Nothing Then
                For Each File In fu1.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC = New ImageClas()
                        IC.Filename = File.FileName
                        IC.FilePath = "~/images/Property_Images/" & Upload_Time & "_" & File.FileName
                        Imgclass.Add(IC)
                    End If
                Next
            End If

            param(66) = New SqlParameter("@IMAGES", SqlDbType.Structured)
            param(66).Value = UtilityService.ConvertToDataTable(Imgclass)

            'Owner Bank Details


            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_ADD_PROPERTY_DETAILS_FORM2", param)
            If res = "SUCCESS" Then
                lblmsg.Text = "Property Added Successfully"
            Else
                lblmsg.Text = "Something went wrong. Please try again later."
            End If
            Cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)

        End Try
    End Sub

    Public Class ImageClas
        Private _fn As String
        Public Property Filename() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String
        Public Property FilePath() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property

    End Class

    Private Sub BindFlooringTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_FLOORING_TYPES")
        ddlFlooringType.DataSource = sp3.GetDataSet()
        ddlFlooringType.DataTextField = "PM_FT_TYPE"
        ddlFlooringType.DataValueField = "PM_FT_SNO"
        ddlFlooringType.DataBind()
        ddlFlooringType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindReqId()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GENERATE_REQUESTID")
        sp3.Command.Parameters.Add("@REQ_TYPE", "ADDPROPERTY", DbType.String)
        Dim dt = DateTime.Now.ToString("MMddyyyyHHmm")
        txtReqID.Text = dt + "/PRPREQ/" + Convert.ToString(sp3.ExecuteScalar())
    End Sub

    Private Sub BindRequestTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_REQUEST_TYPES")
        ddlReqType.DataSource = sp3.GetDataSet()
        ddlReqType.DataTextField = "PM_RT_TYPE"
        ddlReqType.DataValueField = "PM_RT_SNO"
        ddlReqType.DataBind()
        ddlReqType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindUserCities()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp3.Command.AddParameter("@DUMMY", "", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp3.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub
    Private Sub BindAcquisitionthrough()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACQISITION_THROUGH")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlAcqThr.DataSource = sp3.GetDataSet()
        ddlAcqThr.DataTextField = "PN_ACQISITION_THROUGH"
        ddlAcqThr.DataValueField = "PN_ACQISITION_ID"
        ddlAcqThr.DataBind()
        ddlAcqThr.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindInsuranceType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_INSURANCE_TYPE")
        ddlInsuranceType.DataSource = sp.GetDataSet()
        ddlInsuranceType.DataTextField = "INSURANCE_TYPE"
        ddlInsuranceType.DataValueField = "ID"
        ddlInsuranceType.DataBind()
        ddlInsuranceType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If CInt(txtCarpetArea.Text) > CInt(txtBuiltupArea.Text) Then
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be more than builtup area');</SCRIPT>", False)
        ElseIf CInt(txtCommonArea.Text) > CInt(txtCarpetArea.Text) Or CInt(txtCommonArea.Text) > CInt(txtBuiltupArea.Text) Then
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Common Area cannot be more than builtup area and Carpet');</SCRIPT>", False)

        ElseIf CInt(txtRentableArea.Text) > CInt(txtBuiltupArea.Text) Or CInt(txtRentableArea.Text) > CInt(txtCarpetArea.Text) Or CInt(txtRentableArea.Text) > CInt(txtUsableArea.Text) Then
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Rentable area cannot be more than built up, carpet area and usable area');</SCRIPT>", False)
        Else
            insertnewrecord()
            Cleardata()
            BindReqId()
        End If
    End Sub

    Protected Sub btnAddMore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddMore.Click
        If CInt(txtCarpetArea.Text) > CInt(txtBuiltupArea.Text) Then
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be more than builtup area');</SCRIPT>", False)
        ElseIf CInt(txtCommonArea.Text) > CInt(txtCarpetArea.Text) Or CInt(txtCommonArea.Text) > CInt(txtBuiltupArea.Text) Then
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Common Area cannot be more than builtup area and Carpet');</SCRIPT>", False)

        ElseIf CInt(txtRentableArea.Text) > CInt(txtBuiltupArea.Text) Or CInt(txtRentableArea.Text) > CInt(txtCarpetArea.Text) Or CInt(txtRentableArea.Text) > CInt(txtUsableArea.Text) Then
            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Rentable area cannot be more than built up, carpet area and usable area');</SCRIPT>", False)
        Else
            insertnewrecord()
            Cleardata()
        End If
    End Sub


    Private Sub Cleardata()
        ddlReqType.SelectedIndex = 0
        ddlPprtNature.SelectedIndex = 0
        ddlAcqThr.SelectedIndex = 0
        ddlCity.SelectedIndex = 0
        ddlLocation.Items.Clear()
        ddlTower.Items.Clear()
        txtFloor.Text = ""
        ddlFloor.Items.Clear()
        txtToilet.Text = ""
        ddlPropertyType.SelectedIndex = 0
        txtPropIDName.Text = ""
        txtESTD.Text = ""
        txtAge.Text = ""
        txtPropDesc.Text = ""
        txtSignageLocation.Text = ""
        txtSocityName.Text = ""
        txtlat.Text = ""
        txtlong.Text = ""
        txtOwnScopeWork.Text = ""
        txtRecmRemarks.Text = ""

        txtownrname.Text = ""
        txtphno.Text = ""
        txtPrvOwnName.Text = ""
        txtPrvOwnPhNo.Text = ""
        txtOwnEmail.Text = ""

        txtBankName.Text = ""
        txtBankAddress.Text = ""
        txtISFCCode.Text = ""
        txtAcctnumber.Text = ""

        txtCarpetArea.Text = ""
        txtBuiltupArea.Text = ""
        txtCommonArea.Text = ""
        txtRentableArea.Text = ""
        txtUsableArea.Text = ""
        txtSuperBulArea.Text = ""
        txtPlotArea.Text = ""
        txtCeilingHight.Text = ""
        txtBeamBottomHight.Text = ""
        txtMaxCapacity.Text = ""
        txtOptCapacity.Text = ""
        txtSeatingCapacity.Text = ""
        ddlFlooringType.SelectedIndex = 0
        txtFSI.Text = ""
        txtEfficiency.Text = ""

        txtPurPrice.Text = ""
        txtPurDate.Text = ""
        txtMarketValue.Text = ""

        txtIRDA.Text = ""
        txtPCcode.Text = ""
        txtGovtPropCode.Text = ""
        txtUOM_CODE.Text = ""

        ddlInsuranceType.SelectedIndex = 0
        txtInsuranceVendor.Text = ""
        txtInsuranceAmt.Text = ""
        txtInsurancePolNum.Text = ""
        txtInsuranceStartdate.Text = ""
        txtInsuranceEnddate.Text = ""
        txtApartmentno.Text = ""
        txtNoOfBedrooms.Text = ""
        txtstaffinchargemail.Text = ""
        Txtstaffincharge.Text = ""
        'txtfurnished.Text = ""
        ddlinreriortype.SelectedIndex = 0
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
            sp2.Command.AddParameter("@CITY", ddlCity.SelectedValue)
            sp2.Command.AddParameter("@USR_ID", Session("uid"))
            ddlLocation.DataSource = sp2.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlTower.Items.Clear()
        Else
            ddlLocation.Items.Clear()
        End If
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex > 0 Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWERSBYLCMID")
            sp2.Command.AddParameter("@LCMID", ddlLocation.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            ddlTower.DataSource = sp2.GetDataSet()
            ddlTower.DataTextField = "TWR_NAME"
            ddlTower.DataValueField = "TWR_CODE"
            ddlTower.DataBind()
            ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))
        Else
            ddlTower.Items.Clear()
        End If
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        If ddlTower.SelectedIndex > 0 Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_getFloors_masters")
            sp2.Command.AddParameter("@TWR_ID", ddlTower.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@FLR_LOC_ID", ddlLocation.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@FLR_CITY_CODE", ddlCity.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ddlFloor.DataSource = sp2.GetDataSet()
            ddlFloor.DataTextField = "FLR_NAME"
            ddlFloor.DataValueField = "FLR_CODE"
            ddlFloor.DataBind()
            ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
            txtFloor.Text = ddlFloor.Items.Count - 1
        Else
            ddlFloor.Items.Clear()
        End If
    End Sub


    Protected Sub txtESTD_TextChanged(sender As Object, e As EventArgs) Handles txtESTD.TextChanged
        If Integer.TryParse(txtESTD.Text, 0) Then
            txtAge.Text = DateTime.Now.Year - Convert.ToInt32(txtESTD.Text)
        Else
            txtAge.Text = ""
        End If
    End Sub

    Protected Sub ddlRecommended_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRecommended.SelectedIndexChanged
        divRecommanded.Visible = IIf(ddlRecommended.SelectedValue = "1", True, False)
    End Sub

    Protected Sub ddlFSI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFSI.SelectedIndexChanged
        divFSI.Visible = IIf(ddlFSI.SelectedValue = "1", True, False)
    End Sub

End Class
