﻿app.service("TenantReportService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    this.GetTenantDetails = function (TenantRep) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/TenantReport/GetTenantDetails', TenantRep)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);
app.controller('TenantReportController', ['$scope', '$q', '$http', 'UtilityService', 'TenantReportService','$timeout', function ($scope, $q, $http, UtilityService, TenantReportService, $timeout) {
    $scope.TenantRep = {};
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.Loclist = [];
    $scope.PrpTypelist = [];
    $scope.PrpNamelist = [];


    $scope.columnDefs = [
           { headerName: "Tenant-Property Type", field: "TEN_PRPTYPE", width: 350, cellClass: 'grid-align', },
           { headerName: "Tenant Code", field: "TEN_CODE", cellClass: 'grid-align', width: 350, },
           { headerName: "Tenant Name", field: "TEN_NAME", cellClass: 'grid-align', width: 350 },
           { headerName: "Tenant Email", field: "TEN_EMAIL", cellClass: 'grid-align', width: 350 },
           { headerName: "Occupied Area(Sqft.)", field: "OCC_AREA", cellClass: 'grid-align', width: 350 },
           { headerName: "Tenant Rent", field: "TEN_RENT", cellClass: 'grid-align', width: 350 },
           { headerName: "Maintenance Fee", field: "MAINT_FEE", cellClass: 'grid-align', width: 350 },
           { headerName: "Total Rent Amount", field: "TOT_RENT_AMT", cellClass: 'grid-align', width: 350 }
    ];

    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Loclist = response.data;
            $scope.Loclist.ticked = true;
            angular.forEach($scope.Loclist, function (value, key) {
                value.ticked = true;
            });
        }
    });



    UtilityService.getPropertyTypes().then(function (response) {
        if (response.data != null) {
            $scope.PrpTypelist = response.data;
            $scope.PrpTypelist.ticked = true;
            angular.forEach($scope.PrpTypelist, function (value, key) {
                value.ticked = true;
            });
        }
    });

    $scope.LcmChanged = function () {
        var params =
   {
       selectedLoc: $scope.TenantRep.selectedLoc,
       selectedPrpType: $scope.TenantRep.selectedPrpType,
       selectedPrpName: $scope.TenantRep.selectedPrpName,
       CNP_NAME: $scope.TenantRep.CNP_NAME[0].CNP_ID
   }
        UtilityService.getPropertyNames(params).then(function (response) {
            if (response.data != null) {
                $scope.PrpNamelist = response.data;
                $scope.PrpNamelist.ticked = true;
                angular.forEach($scope.PrpNamelist, function (value, key) {
                    value.ticked = true;
                });
            }
            else
                $scope.PrpNamelist = [];
        });

    }
    $scope.LocChangeAll = function () {
        var params =
{
    selectedLoc: $scope.Loclist,
    selectedPrpType: $scope.TenantRep.selectedPrpType,
    selectedPrpName: $scope.TenantRep.selectedPrpName,
    CNP_NAME: $scope.TenantRep.CNP_NAME[0].CNP_ID
}
        UtilityService.getPropertyNames(params).then(function (response) {
            if (response.data != null) {
                $scope.PrpNamelist = response.data;
                $scope.PrpNamelist.ticked = true;
                angular.forEach($scope.PrpNamelist, function (value, key) {
                    value.ticked = true;
                });
            }
            else
                $scope.PrpNamelist = [];
        });
    }

    $scope.PrpChanged = function () {
        var params =
   {
       selectedLoc: $scope.TenantRep.selectedLoc,
       selectedPrpType: $scope.TenantRep.selectedPrpType
   }
        UtilityService.getPropertyNames(params).then(function (response) {
            if (response.data != null) {
                $scope.PrpNamelist = response.data;
                angular.forEach($scope.PrpNamelist, function (value, key) {
                    value.ticked = true;
                });
            }
            else
                $scope.PrpNamelist = [];
        });
    }

    $scope.PrpChangeAll = function () {
        var params =
{
    selectedLoc: $scope.Loclist,
    selectedPrpType: $scope.PrpTypelist
    //selectedPrpName: $scope.TenantRep.selectedPrpName,
    //CNP_NAME: $scope.TenantRep.CNP_NAME[0].CNP_ID
}
        UtilityService.getPropertyNames(params).then(function (response) {
            if (response.data != null) {
                $scope.PrpNamelist = response.data;
                $scope.PrpNamelist.ticked = true;
                angular.forEach($scope.PrpNamelist, function (value, key) {
                    value.ticked = true;
                });
            }
            else
                $scope.PrpNamelist = [];
        });
    }


    setTimeout(function () {
        progress(0, 'Loading...', true);
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.TenantRep.CNP_NAME = parseInt(CompanySessionId);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySessionId) });
                    a.ticked = true;
                });
                if (CompanySessionId == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }
            progress(0, 'Loading...', false);
        });
    }, 500);

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();
        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                var params =
                {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    selectedLoc: $scope.TenantRep.selectedLoc,
                    selectedPrpType: $scope.TenantRep.selectedPrpType,
                    selectedPrpName: $scope.TenantRep.selectedPrpName,

                    CNP_NAME: $scope.TenantRep.CNP_NAME[0].CNP_ID
                }
                progress(0, 'Loading...', true);
                TenantReportService.GetTenantDetails(params).then(function (data) {
                    $scope.gridata = data.data;
                    if ($scope.gridata == null) {
                        $("#btNext").attr("disabled", true);
                        $scope.gridOptions.api.setRowData([]);
                        $scope.GridVisiblity = false;
                    }
                    else {
                        $scope.GridVisiblity = true;
                        $scope.gridOptions.api.setRowData([]);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                    }
                    progress(0, 'Loading...', false);
                }, function (error) {
                    console.log(error);
                });
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };


    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    };

    $scope.GenReport = function (TenantRep, Type) {
        var searchval = $("#filtertxt").val();
        progress(0, 'Loading...', true);
        woobj = {};
        angular.copy(TenantRep, woobj);
        woobj.CNP_NAME = $scope.TenantRep.CNP_NAME[0].CNP_ID;
        woobj.Type = Type;
        woobj.SearchValue = searchval;
        woobj.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        woobj.PageSize = $scope.gridata[0].OVERALL_COUNT;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/TenantReport/GetGrid',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'TenantReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);

        var columns = [{ title: "Tenant-Property Type", key: "TEN_PRPTYPE" }, { title: "Tenant Code ", key: "TEN_CODE" }, { title: "Tenant Name", key: "TEN_NAME" },
                        { title: "Tenant Email", key: "TEN_EMAIL" }, { title: "Occupied Area(Sqft.)", key: "OCC_AREA" }, { title: "Tenant Rent", key: "TEN_RENT" },
                        { title: "Maintenance Fee", key: "MAINT_FEE" }, { title: "Total Rent Amount", key: "TOT_RENT_AMT" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("TenantReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "TenantReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }



    setTimeout(function () {
        var params =
     {
         selectedLoc: $scope.TenantRep.selectedLoc,
         selectedPrpType: $scope.TenantRep.selectedPrpType,
         selectedPrpName: $scope.TenantRep.selectedPrpName,
         CNP_NAME: $scope.TenantRep.CNP_NAME[0].CNP_ID
     }
        UtilityService.getPropertyNames(params).then(function (response) {
            console.log(response);
            if (response.data != null) {
                $scope.PrpNamelist = response.data;
                console.log($scope.PrpNamelist);
                $scope.PrpNamelist.ticked = true;
                angular.forEach($scope.PrpNamelist, function (value, key) {
                    value.ticked = true;
                });
            }
        });
        setTimeout(function () {
            $scope.LoadData();
        }, 500);
    }, 1000);
}]);