﻿app.service('ExpHeadService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    var deferred = $q.defer();

    this.SaveData = function (Ddata) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ExpenseHead/SaveData', Ddata)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.UpdateExpenseHeadData = function (Ddata) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ExpenseHead/UpdateExpenseHeadData', Ddata)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.BindExpenseHeadGrid = function (Ddata) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ExpenseHead/BindExpenseHeadGrid', Ddata)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

}]);

app.controller('ExpHeadController', ['$scope', '$q', 'ExpHeadService', 'UtilityService', '$timeout', function ($scope, $q, ExpHeadService, UtilityService, $timeout) {

    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.ExpHead = {};
    $scope.ParentEntityList = [];
    $scope.ActionStatus = 0;
    $scope.GridVisible = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;


    //to Save the data
    $scope.SaveData = function () {
        if ($scope.IsInEdit) {
            ExpHeadService.UpdateExpenseHeadData($scope.ExpHead).then(function (repeat) {
                var updatedobj = {};
                angular.copy($scope.ExpHead, updatedobj)
                $scope.gridata.unshift(updatedobj);
                $scope.ShowMessage = true;
                $scope.Success = "Expense Head Updated Successfully";

                ExpHeadService.BindExpenseHeadGrid().then(function (data) {
                    $scope.gridata = data;
                    //$scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                    console.log(error);
                });

                $scope.IsInEdit = false;
                $scope.ClearData();
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
            }, function (error) {
                console.log(error);
            })
        }
        else {
            $scope.ExpHead.EXP_STATUS = "1";
            ExpHeadService.SaveData($scope.ExpHead).then(function (response) {
                $scope.ShowMessage = true;
                $scope.Success = "Expense Head Inserted Successfully";
                var savedobj = {};
                angular.copy($scope.ExpHead, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
                $scope.ExpHead = {};

            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    }
    //for GridView
    var columnDefs = [
       { headerName: "Expense Head Code", field: "EXP_CODE", width: 100, cellClass: 'grid-align' },
       { headerName: "Expense Head Name", field: "EXP_NAME", width: 150, cellClass: 'grid-align' },
       //{ headerName: "Entity Admin", template: "{{data.ADM_Code}} / {{data.ADM_Name}}", width: 300, cellClass: 'grid-align' },
       { headerName: "Status", template: "{{ShowStatus(data.EXP_STATUS)}}", width: 100, cellClass: 'grid-align' },
       { headerName: "Edit", width: 40, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        ExpHeadService.BindExpenseHeadGrid().then(function (data) {
            console.log(data);
            $scope.gridata = data;
            //$scope.createNewDatasource();
            $scope.gridOptions.api.setRowData(data);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 700);
        }, function (error) {
            console.log(error);
        });
        //$scope.StaDet[0].Name = "Active";
    }


    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        suppressHorizontalScroll: true,
        enableFilter: true,

        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };
    $timeout($scope.LoadData, 1000);
    $scope.EditFunction = function (data) {
        $scope.ExpHead = {};
        angular.copy(data, $scope.ExpHead);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
    }
    $scope.ClearData = function () {
        $scope.ExpHead = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frmExpensesHead.$submitted = false;
    }
    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
}]);