﻿app.service('LocUtilityService', ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    var deferred = $q.defer();

    this.SaveData = function (Ddata) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LocUtility/SaveData', Ddata)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.BindLocation = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/LocUtility/BindLocation')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.BindExpenseHead = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/LocUtility/BindExpenseHead')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.BindDepartment = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/LocUtility/BindDepartment')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.BindUtilityExpenseGrid = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LocUtility/BindUtilityExpenseGrid')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetUtilityDetails = function (update) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LocUtility/GetUtilityDetails', update)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.UpdateExpUtilityDetails = function (modify) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LocUtility/UpdateExpUtilityDetails', modify)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);
this.ExcelDownload = function (assetinfo) {
    //console.log($scope.AssetRequisition.astlst);

    var deferred = $q.defer();

    return $http.post(UtilityService.path + '/api/LocUtility/ExcelDownload', assetinfo)
     .then(function (response) {
         //console.log(response.data);
         deferred.resolve(response.data);
         return deferred.promise;
     }, function (response) {
         deferred.reject(response);
         return deferred.promise;
     });
};

app.controller('LocUtilityController', ['$scope', '$q', 'LocUtilityService', 'UtilityService', '$timeout','$http', function ($scope, $q, LocUtilityService, UtilityService, $timeout, $http) {

    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.SaveExpUtility = {};
    $scope.LocUtilityList = [];
    $scope.ExpenseUtility = [];
    $scope.UserList = [];
    $scope.ActionStatus = 0;
    $scope.GridVisible = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;
    $scope.SaveExpUtility.STATUS = 'New';

    $scope.LoadData = function () {
       
        progress(0, 'Loading...', true);
       
        LocUtilityService.BindLocation().then(function (Ldata) {
            $scope.LocUtilityList = Ldata;
            LocUtilityService.BindExpenseHead().then(function (data) {
                $scope.ExpenseUtility = data;
                LocUtilityService.BindDepartment().then(function (Udata) {
                    $scope.UserList = Udata;
                    LocUtilityService.BindUtilityExpenseGrid().then(function (Gdata) {
                        $scope.gridata = Gdata;
                        //$scope.createNewDatasource();
                        $scope.gridOptions.api.setRowData(Gdata);                        
                        setTimeout(function () {
                            $('.selectpicker').selectpicker('refresh');
                            progress(0, ' ', false);
                        }, 700);
                    });
                });
            });
        });
    }

    $scope.$watch('SaveExpUtility.FromDate', function () {
        if ($scope.SaveExpUtility.FromDate != undefined)
            $scope.GetUtilityDetails();
    });

    $scope.GetUtilityDetails = function () {        
        if ($scope.SaveExpUtility.LCM_CODE != "" && $scope.SaveExpUtility.LCM_CODE != undefined) {
            if ($scope.SaveExpUtility.EXP_CODE != "" && $scope.SaveExpUtility.EXP_CODE != undefined) {
                    if ($scope.SaveExpUtility.FromDate != undefined) {
                    LocUtilityService.GetUtilityDetails($scope.SaveExpUtility).then(function (response) {
                        //console.log(response);                       
                        $scope.SaveExpUtility = response.LocWiseUtilityModel;
                        setTimeout(function () {
                            $('#DEP_CODE').selectpicker('refresh');
                        }, 100);
                        
                        //$scope.GetUtilityDetails();
                    });
                }                
            }            
        }        
    }
    //TO SAVE THE DATA
    $scope.SubmitData = function () {
     
        LocUtilityService.SaveData($scope.SaveExpUtility).then(function (response) {
            if (response.data != null) {
                $scope.ShowMessage = true;
                $scope.Success = "Data Inserted Successfully";
                var savedobj = {};
                angular.copy($scope.SaveExpUtility, savedobj)
                showNotification('success', 8, 'bottom-right', response.Message);
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                LocUtilityService.BindUtilityExpenseGrid().then(function (Gdata) {
                    $scope.gridata = Gdata;                   
                    $scope.gridOptions.api.setRowData(Gdata);
                   
                });
             
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
                              
                $scope.ClearData();
            }
            else {
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        }, function (error) {
            showNotification('error', 8, 'bottom-right', error);
        });
    }
    $scope.UploadFile = function () {
        
        if ($('#File3', $('#form1')).val()) {
            progress(0, 'Loading...', true);
            var filetype = $('#File3', $('#form1')).val().substring($('#File3', $('#form1')).val().lastIndexOf(".") + 1);
            if (filetype == "xlsx" || filetype == "xls") {
                if (!window.FormData) {
                    redirect(); // if IE8
                }
                else {

                    var formData = new FormData();
                    var UplFile = $('#File3')[0];
                    var CurrObj = $scope.SaveExpUtility;
                    formData.append("ExUpload", UplFile.files[0]);
                    formData.append("CurrObj", JSON.stringify(CurrObj));
                    $.ajax({
                        url: UtilityService.path + "/api/LocUtility/UploadExcel",
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {

                            if (response.data != null) {

                                $scope.$apply(function () {
                                    $scope.gridata = response.data;
                                    $scope.gridUploadExcel.api.setRowData([]);
                                    $scope.gridUploadExcel.api.setRowData($scope.gridata);
                                    $scope.gridUploadExcel.api.refreshHeader();
                                    $scope.tempAssetsobj = response.data;
                                    progress(0, '', false);
                                });

                                progress(0, '', false);
                                showNotification('success', 8, 'bottom-right', response.Message);
                            }
                            else {

                                progress(0, '', false);

                            }
                        }
                    });
                }

            }
            else
                progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', 'Please Upload only Excel File(s)');
        }
        else
            showNotification('error', 8, 'bottom-right', 'Please Select File To Upload');

    }
    $scope.ExcelDownload = function ()
    {
        $http({
            url: UtilityService.path + '/api/LocUtility/ExcelDownload',
            method: 'POST',
            responseType: 'arraybuffer',
            data: {}, //this is your json data string
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            }
        }).success(function (data) {
            console.log(data);
            if (data.byteLength > 27) {
                var blob = new Blob([data], {
                    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                });
                save(blob, 'UtilityExpenses.xlsx');

            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "No data Found");

            }
        }).error(function () {
            progress(0, '', false);

        });
    }
    function save(blob, fileName) {
        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(blob, fileName);
            progress(0, '', false);
        } else {
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.click();
            window.URL.revokeObjectURL(link.href);
            $scope.ToggleDiv = !$scope.ToggleDiv;
            progress(0, '', false);
        }
    }

    //ToUpdate


    $scope.UpdateExpUtilityDetails = function () {

        LocUtilityService.UpdateExpUtilityDetails($scope.SaveExpUtility).then(function (response) {

            if (response.data != null) {
                $scope.ShowMessage = true;
                $scope.Success = "Data Updated Successfully";
                var updateobj = {};
                angular.copy($scope.SaveExpUtility, updateobj)
                showNotification('success', 8, 'bottom-right', response.Message);
                $scope.gridata.unshift(updateobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                LocUtilityService.BindUtilityExpenseGrid().then(function (Gdata) {
                    $scope.gridata = Gdata;
                    $scope.gridOptions.api.setRowData(Gdata);

                });
               
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
               
                $scope.ClearData();
               
            }
            else {
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        }, function (error) {
            showNotification('error', 8, 'bottom-right', error);
        });
    }

    //for GridView
    var columnDefs = [
       { headerName: "Expense Head", field: "EXP_NAME", width: 150, cellClass: 'grid-align' },
       { headerName: "Location", field: "LCM_NAME", width: 150, cellClass: 'grid-align' },
       { headerName: "Bill Date", field: "FromDate", width: 150, cellClass: 'grid-align' },
       { headerName: "Bill No", field: "BILL_NO", width: 100, cellClass: 'grid-align' },
       { headerName: "Bill Invoice", field: "BILL_INVOICE", width: 100, cellClass: 'grid-align' },
       { headerName: "Amount", field: "AMT", width: 100, cellClass: 'grid-align' },
       { headerName: "Department", field: "DEP_NAME", width: 150, cellClass: 'grid-align' },
       { headerName: "Vendor", field: "VENDOR", width: 150, cellClass: 'grid-align' },
    { headerName: "Vendor Email", field: "VEN_MAIL", width: 150, cellClass: 'grid-align' }

    ];
    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        suppressHorizontalScroll: true,
        enableFilter: true,

        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.ClearData = function () {
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.SaveExpUtility = {};
        $scope.SaveExpUtility.STATUS = 'New';
        $scope.SaveExpUtility.LCM_CODE = '';
        $scope.frmLocationUtility.$submitted = false;
        $('.selectpicker').selectpicker('val', '');

        //$scope.LocUtilityList = [];
        //$scope.ExpenseUtility = [];

    }

    setTimeout(function () {
        $scope.LoadData();
    }, 1000);
}]);