﻿app.service("ExpenseUtilityReportService", ['$http', '$q', 'LocUtilityService','UtilityService', function ($http, $q, LocUtilityService, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ExpenseUtilityReport/GetCustomizedDetails', Customized)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);
app.controller('ExpenseUtilityReportController', ['$scope', '$q', '$http', 'LocUtilityService', 'ExpenseUtilityReportService', 'UtilityService', '$timeout','$filter', function ($scope, $q, $http, LocUtilityService, ExpenseUtilityReportService, UtilityService, $timeout, $filter) {
    $scope.Customized = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.GridVisiblity2 = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.Locations = [];
    $scope.Expense = [];    
    $scope.Company = [];

    $scope.Pageload = function () {
        LocUtilityService.BindLocation().then(function (response) {
            if (response != null) {
                $scope.Locations = response;
                angular.forEach($scope.Locations, function (value, key) {
                    value.ticked = true;
                });

                LocUtilityService.BindExpenseHead().then(function (response) {
                    if (response != null) {
                        $scope.Expense = response;
                        angular.forEach($scope.Expense, function (value, key) {
                            value.ticked = true;
                        });
                        setTimeout(function () { $scope.LoadData(1); }, 500);
                     
                    }
                });

            }
        });
    }
    setTimeout(function () {
        progress(0, 'Loading...', true);
        UtilityService.GetCompanies().then(function (response) {            
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.Customized.CNP_NAME = parseInt(CompanySessionId);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySessionId) });
                    a.ticked = true;
                });
                if (CompanySessionId == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }
            progress(0, 'Loading...', false);
        });
    }, 500);

    $scope.locSelectAll = function () {
        $scope.Customized.Locations = $scope.Locations;
        $scope.LocationChange();
    }

    $scope.ExpSelectAll = function () {
        $scope.Customized.Expense = $scope.Expense;
        $scope.ExpenseChange();
    }

    $scope.LocationChange = function () {

        angular.forEach($scope.Locations, function (value, key) {
            var loc = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (loc != undefined && value.ticked == true) {
                loc.ticked = true;
            }
        });

    }

    $scope.ExpenseChange = function () {

        angular.forEach($scope.Expense, function (value, key) {
            var Exp = _.find($scope.Expense, { EXP_CODE: value.EXP_CODE });
            if (Exp != undefined && value.ticked == true) {
                Exp.ticked = true;
            }
        });

    }

    $scope.Cols = [

                { COL: "Location", value: "LCM_NAME", ticked: false },
                { COL: "Expense Head", value: "EXP_NAME", ticked: false },
                { COL: "Bill Date", value: "FromDate", ticked: false },
                { COL: "Bill No.", value: "BILL_NO", ticked: false },
                { COL: "Invoice/PO No.", value: "BILL_INVOICE", ticked: false },
                { COL: "Amount", value: "AMT", ticked: false },
                { COL: "Vendor Name", value: "VENDOR", ticked: false },
                { COL: "Vendor Email", value: "VEN_MAIL", ticked: false },
                { COL: "Vendor Ph.No.", value: "VEN_PHNO", ticked: false },
                { COL: "Department", value: "DEP_NAME", ticked: false },
                { COL: "Remarks", value: "REM", ticked: false },

    ];

    $scope.columnDefs = [
        { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 130, },
        { headerName: "Expense Head", field: "EXP_NAME", cellClass: 'grid-align', width: 130 },
         { headerName: "Department", field: "DEP_NAME", cellClass: 'grid-align', width: 130 },
        { headerName: "Bill Date", field: "FromDate", template: '<span>{{data.FromDate | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: true, },
        { headerName: "Amount", field: "AMT", cellClass: 'grid-align', width: 100, suppressMenu: true, aggFunc: 'sum' },
        { headerName: "Bill No.", field: "BILL_NO", width: 100, cellClass: 'grid-align', width: 100, suppressMenu: true },
        { headerName: "Invoice/PO No.", field: "BILL_INVOICE", cellClass: 'grid-align', width: 100, suppressMenu: true },
        { headerName: "Vendor Name", field: "VENDOR", cellClass: 'grid-align', width: 130, },
        { headerName: "Vendor Email", field: "VEN_MAIL", cellClass: 'grid-align', width: 130, suppressMenu: true },
        { headerName: "Vendor Ph.No.", field: "VEN_PHNO", cellClass: 'grid-align', width: 130, suppressMenu: true, },
        { headerName: "Remarks", field: "REM", cellClass: 'grid-align', width: 130 },

    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Location", field: "LOCATION",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function groupAggFunction(rows) {
        var sums = {
            AMOUNT: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.AMOUNT += parseFloat((data.AMOUNT));
        });
        return sums;
    }
    $scope.LoadData = function (req_type) {
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {
            rowCount: null,

            getRows: function (params) {
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    explst: $scope.Customized.Expense,
                    loclst: $scope.Customized.Locations,
                    Request_Type: req_type,
                    FromDate: $scope.Customized.FromDate,
                    ToDate: $scope.Customized.ToDate,
                    CNP_NAME: $scope.Customized.CNP_NAME[0].CNP_ID
                };
                console.log(params);
                ExpenseUtilityReportService.GetGriddata(params).then(function (data) {
                    $scope.gridata = data.data;
                    if ($scope.gridata == null) {
                        $("#btNext").attr("disabled", true);
                        $scope.GridVisiblity = false;
                        $scope.GridVisiblity2 = true;
                        $scope.gridOptions.api.setRowData([]);

                    }
                    else {
                        progress(0, 'Loading...', true);
                        $scope.GridVisiblity = true;
                        $scope.GridVisiblity2 = true;

                        $scope.gridOptions.api.refreshHeader();
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        var cols = [];
                        var unticked = _.filter($scope.Cols, function (item) {
                            return item.ticked == false;
                        });
                        var ticked = _.filter($scope.Cols, function (item) {
                            return item.ticked == true;
                        });
                        for (i = 0; i < unticked.length; i++) {
                            cols[i] = unticked[i].value;
                        }
                        $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
                        cols = [];
                        for (i = 0; i < ticked.length; i++) {
                            cols[i] = ticked[i].value;
                        }
                        $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
                    }
                    progress(0, '', false);
                });

            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Location", key: "LCM_NAME" }, { title: "Expense Head", key: "EXP_NAME" }, { title: "Department", key: "DEP_NAME" },
            { title: "Bill Date", key: "FromDate" }, { title: "Amount", key: "AMT" }, { title: "Bill No", key: "BILL_NO" }, { title: "Invoice/PO No.", key: "BILL_INVOICE" },
            { title: "Vendor Name", key: "VENDOR" }, { title: "Vendor Email", key: "VEN_MAIL" }, { title: "Vendor Ph.No.", key: "VEN_PHNO" }, { title: "Remarks", key: "REM" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("ExpenseUtilityReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "ExpenseUtilityReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Customized, Type) {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        Customized.SearchValue = searchval;
        Customized.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        Customized.PageSize = $scope.gridata[0].OVERALL_COUNT;
        Customized.Type = Type;
        Customized.Request_Type = 2;
        Customized.loclst = Customized.Locations;
        Customized.explst = Customized.Expense;
        Customized.CNP_NAME = $scope.Customized.CNP_NAME[0].CNP_ID

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/ExpenseUtilityReport/GetCustomizedData',
                method: 'POST',
                data: Customized,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'ExpenseUtilityReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.selVal = "THISYEAR";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.Customized.FromDate = moment().format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Customized.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Customized.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Customized.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Customized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'THISYEAR':
                $scope.Customized.FromDate = moment().startOf('year').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('year').format('MM/DD/YYYY');
                break;
        }
    }

    $scope.Pageload();

}]);