﻿app.service('PropInsValidityService', ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    
    this.GetGriddata = function (Ddata) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Prop_Ins_Validity/GetGriddata', Ddata)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    //this.GetGrid = function (Ddata) {
    //    var deferred = $q.defer();
    //    return $http.post(UtilityService.path + '/api/Prop_Ins_Validity/GetGrid', Ddata)
    //      .then(function (response) {
    //          deferred.resolve(response.data);
    //          return deferred.promise;
    //      }, function (response) {
    //          deferred.reject(response);
    //          return deferred.promise;
    //      });
    //};

}]);

app.controller('PropInsValidityController', ['$scope', '$q', 'PropInsValidityService', 'UtilityService','$http', function ($scope, $q, PropInsValidityService, UtilityService, $http) {
    $scope.ActionStatus = 0;
    $scope.GridVisible = 0;
    $scope.Company = [];
    $scope.CompanyVisible = 0;
    $scope.Ddata = {};
    $scope.InsValidity = {};
    //for GridView
    var columnDefs = [
       { headerName: "Property Type", field: "PPT_TYPE", width: 130, cellClass: 'grid-align' },
       { headerName: "Property Name", field: "PPT_NAME", width: 130, cellClass: 'grid-align' },
       { headerName: "Location", field: "LCM_NAME", width: 150, cellClass: 'grid-align' },
       { headerName: "Address", field: "PPT_ADDR", width: 180, cellClass: 'grid-align' },
       { headerName: "Owner", field: "OWNER", width: 100, cellClass: 'grid-align' },
       { headerName: "Phone Number", field: "PHNO", width: 100, cellClass: 'grid-align' },
       { headerName: "Insurance Type", field: "INS_TYPE", width: 120, cellClass: 'grid-align' },
       { headerName: "Insurance Vendor", field: "INS_VENDOR", width: 130, cellClass: 'grid-align' },
       { headerName: "Insurance Amount", field: "INS_AMOUNT", width: 120, cellClass: 'grid-align' },
       { headerName: "Insurance Policy Number", field: "INS_PNO", width: 150, cellClass: 'grid-align' },
       { headerName: "Insurance Start Date", template: '<span>{{data.INS_START_DT | date: "dd-MM-yyyy"}}</span>', field: "INS_START_DT", width: 120, cellClass: 'grid-align' },
       { headerName: "Insurance End Date", template: '<span>{{data.INS_END_DT | date: "dd-MM-yyyy"}}</span>', field: "INS_END_DT", width: 120, cellClass: 'grid-align' },
    ];
    
    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        //suppressHorizontalScroll: true,
    };

    $scope.LoadGrid = function () {
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();
        var dataSource = {
            rowCount: null,

            getRows: function (params) {
                var obj = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    CNP_NAME: $scope.InsValidity.Company[0].CNP_ID,
                    FROM_DATE: $scope.InsValidity.FromDate,
                    TO_DATE: $scope.InsValidity.ToDate,
                }
             
                progress(0, 'Loading...', false);
                PropInsValidityService.GetGriddata(obj).then(function (response) {
                    $scope.gridata = response.data;
                    //$scope.createNewDatasource();
                    if (response.data == null) {
                        $("#btNext").attr("disabled", true);
                        $scope.gridOptions.api.setRowData([]);
                    }
                    else {
                        $scope.gridOptions.api.setRowData(response.data);
                    }
                    progress(0, 'Loading...', false);
                }, function (error) {
                    console.log(error);
                });
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };
    setTimeout(function () {
        progress(0, 'Loading...', true);
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.InsValidity.Company = [];
                console.log(CompanySession);
                $scope.Company = response.data;
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.InsValidity.Company.push(a);
                });
                if (CompanySession == "1") { $scope.CompanyVisible = 1; }
                else { $scope.CompanyVisible = 0; }
                $scope.LoadGrid();
            }
        });
    }, 500);

    $scope.GenReport = function (ReqDet, Type) {
        var searchval = $("#filtertxt").val();
        progress(0, 'Loading...', true);
        
        $scope.InsValidity.CNP_NAME = CompanySession;
        var ReqDet = {
            SearchValue: searchval,
            PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
            PageSize: $scope.gridata[0].OVERALL_COUNT,
            CNP_NAME: $scope.InsValidity.Company[0].CNP_ID,
            FROM_DATE: $scope.InsValidity.FromDate,
            TO_DATE: $scope.InsValidity.ToDate,
           Type: $scope.InsValidity.Type = Type
        }

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            //console.log(ReqDet);
            $http({
                url: UtilityService.path + '/api/Prop_Ins_Validity/GetGrid', 
                method: 'POST',
                data: ReqDet,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'InsuranceValidity.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);

                var columns = [{ title: "Property Type", key: "PPT_TYPE" }, { title: "Property Name", key: "PPT_NAME" }, { title: "Location", key: "LCM_NAME" },
                        { title: "Address", key: "PPT_ADDR" }, { title: "Owner)", key: "OWNER" }, { title: "Phone Number", key: "PHNO" },
                        { title: "Insurance Type", key: "INS_TYPE" }, { title: "Insurance Vendor", key: "INS_VENDOR" },
                        { title: "Insurance Amount", key: "INS_AMOUNT" }, { title: "Insurance Policy Number", key: "INS_PNO" },
                        { title: "Insurance Start Date", key: "INS_START_DT" }, { title: "Insurance End Date", key: "INS_END_DT" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("PropInsuValidityReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "PropInsuValidityReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


}]);