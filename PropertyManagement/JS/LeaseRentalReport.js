﻿app.service("LeaseRentalReportService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseRentalReport/GetGriddata', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetGrid = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseRentalReport/GetGrid', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };

    
}]);


app.controller('LeaseRentalReportController', ['$scope', '$q', '$http', 'LeaseRentalReportService', 'UtilityService','$timeout', function ($scope, $q, $http, LeaseRentalReportService, UtilityService, $timeout) {

    $scope.GridVisiblity = false;

    $scope.Type = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    $scope.DocTypeVisible = 0;
    $scope.Country = [];
    $scope.City = [];
    $scope.Locations = [];

    UtilityService.getCountires(2).then(function (response) {
        if (response.data != null) {
            $scope.Country = response.data;
            angular.forEach($scope.Country, function (value, key) {
                value.ticked = true;
            });
            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.City = response.data;
                    angular.forEach($scope.City, function (value, key) {
                        value.ticked = true;
                    });
                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Locations = response.data;
                            angular.forEach($scope.Locations, function (value, key) {
                                value.ticked = true;
                            });
                        }
                    });
                }
            });

        }
    });

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.LeaseRep.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.LeaseRep.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.LeaseRep.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.LeaseRep.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.LeaseRep.City = $scope.City;
        $scope.getLocationsByCity();
    }


    $scope.locSelectAll = function () {
        $scope.LeaseRep.Locations = $scope.Locations;
        $scope.LocationChange();
    }

    $scope.LocationChange = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.LeaseRep.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.LeaseRep.City[0] = cty;
            }
        });
    }

    $scope.columnDefs = [
        { headerName: "Location", field: "LCM_NAME", width: 100, cellClass: 'grid-align', width: 100, },
        { headerName: "Property Name", field: "PM_PPT_NAME", width: 100, cellClass: 'grid-align', width: 100, },
        { headerName: "Child Entity", field: "CHE_CODE", width: 100, cellClass: 'grid-align', width: 100, },
        { headerName: "Invoice No.", field: "INVOICE_NO", width: 100, cellClass: 'grid-align', width: 100, },
        { headerName: "Invoice Date", field: "INVOICE_DT", template: '<span>{{data.INVOICE_DT | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 150, },
        { headerName: "Property Address", field: "PM_PPT_ADDRESS", cellClass: 'grid-align', width: 150, },
        { headerName: "Branch Code", field: "PM_PPT_LOC_CODE", cellClass: 'grid-align', width: 80, },
        { headerName: "Lease ID", field: "PM_LPD_REQ_ID", cellClass: 'grid-align', width: 100, },
        { headerName: "Landlord Name", field: "PM_LPD_LL_NAME", cellClass: 'grid-align', width: 200, },
        { headerName: "Landlord Rent", field: "PM_LPD_LL_BASIC_RENT", cellClass: 'grid-align', width: 100 },
        { headerName: "Payment Term", field: "PM_LPD_LL_PAY_TERM", cellClass: 'grid-align', width: 100 },
        { headerName: "Payment Month", field: "PM_LPD_LL_PAY_MONTH", cellClass: 'grid-align', width: 100, },
        { headerName: "Payment Year", field: "PM_LPD_LL_PAY_YEAR", cellClass: 'grid-align', width: 100 },
        { headerName: "Withhold Amount", field: "withhold", cellClass: 'grid-align', width: 100 },
        { headerName: "Due Amount", field: "dueamnt", cellClass: 'grid-align', width: 100 },
        { headerName: "Rent Paid", field: "RentPaid", cellClass: 'grid-align', width: 100 },
        { headerName: "Status", field: "PM_LPD_LL_STATUS", cellClass: 'grid-align', width: 100, },
        { headerName: "Payment Date", field: "PM_LPD_CREATED_DT", cellClass: 'grid-align', width: 150, },
        { headerName: "Payment Mode", field: "PAY_MODE", cellClass: 'grid-align', width: 150, },
        { headerName: "Account NO.", field: "PM_LP_ACC_NO", cellClass: 'grid-align', width: 150, },
        { headerName: "Neft branch", field: "PM_LP_NEFT_BRNCH", cellClass: 'grid-align', width: 150, },
        { headerName: "Bank Name", field: "PM_LP_BANK_NAME", cellClass: 'grid-align', width: 150, },
        { headerName: "IFSC Code", field: "PM_LP_IFSC", cellClass: 'grid-align', width: 150, },

        { headerName: "Agreement Start Date", field: "PM_LAD_EFFE_DT_AGREEMENT", template: '<span>{{data.PM_LAD_EFFE_DT_AGREEMENT | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 150, },
        { headerName: "Agreement End Date", field: "PM_LAD_EXP_DT_AGREEMENT", template: '<span>{{data.PM_LAD_EXP_DT_AGREEMENT | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 150, },


    ];

    setTimeout(function () {
        progress(0, 'Loading...', true);
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {                
                $scope.Company = response.data;
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.LeaseRep.CNP_NAME.push(a);
                });
                if (CompanySession == "1") { $scope.CompanyVisible = 0; }
                else { $scope.CompanyVisible = 1; }
                $scope.LoadData();
            }
            progress(0, 'Loading...', false);
        });

    }, 500);


    $scope.LoadData = function () {
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {
            rowCount: null,

            getRows: function (params) {
                progress(0, 'Loading...', true);
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    CNP_NAME: $scope.LeaseRep.CNP_NAME[0].CNP_ID,
                    FromDate: $scope.LeaseRep.FromDate,
                    ToDate: $scope.LeaseRep.ToDate,
                    loclst: $scope.LeaseRep.Locations
                };

                LeaseRentalReportService.GetGriddata(params).then(function (data) {
                    $scope.gridata = data.data;
                    if ($scope.gridata == null) {
                        $("#btNext").attr("disabled", true);
                        $scope.GridVisiblity = false;
                        $scope.gridOptions.api.setRowData([]);
                    }
                    else {
                        $scope.GridVisiblity = true;
                        $scope.gridOptions.api.setRowData($scope.gridata);
                    }
                    progress(0, 'Loading...', false);
                }, function (error) {
                    console.log(error);
                });
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);

    };


    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})


    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: false,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        //onReady: function () {
        //    $scope.gridOptions.api.sizeColumnsToFit()
        //},
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };


    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Location", key: "LCM_NAME" }, { title: "Property Name", key: "PM_PPT_NAME" }, { title: "Property Address", key: "PM_PPT_ADDRESS" },
            { title: "Lease ID", key: "PM_LPD_REQ_ID" }, { title: "Landlord Name", key: "PM_LPD_LL_NAME" }, { title: "Landlord Rent", key: "PM_LPD_LL_BASIC_RENT" },
            { title: "Payment Term", key: "PM_LPD_LL_PAY_TERM" }, { title: "Payment Month", key: "PM_LPD_LL_PAY_MONTH" }, { title: "Payment Year", key: "PM_LPD_LL_PAY_YEAR" },
            { title: "Payment Date", key: "LAND_ADDR" }, { title: "Agreement Start Date", key: "PM_LAD_EFFE_DT_AGREEMENT" }, { title: "Agreement End Date", key: "PM_LAD_EXP_DT_AGREEMENT" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("LeaseRentalReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "LeaseReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


    $scope.GenReport = function (LeaseRep, Type) {
        var searchval = $("#filtertxt").val();
        progress(0, 'Loading...', true);
        LeaseRep.SearchValue = searchval;
        LeaseRep.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        LeaseRep.PageSize = $scope.gridata[0].OVERALL_COUNT;
        LeaseRep.Type = Type;
        LeaseRep.loclst = $scope.LeaseRep.Locations;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (LeaseRep.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/LeaseRentalReport/GetGrid',
                method: 'POST',
                data: LeaseRep,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

               
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'LeaseRentalReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

}]);