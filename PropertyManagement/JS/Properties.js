﻿app.service("PropertiesService", ['$http', '$q','UtilityService',function ($http, $q, UtilityService) {

    this.GetGriddata = function (Property) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Properties/GetProperties', Property)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetMarkers = function (params) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Properties/GetMarkers', params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('PropertiesController', ['$scope', '$q', '$http', 'PropertiesService', 'UtilityService', '$timeout','$filter', function ($scope, $q, $http, PropertiesService, UtilityService, $timeout, $filter) {
    $scope.Property = {};
    $scope.GridVisiblity = false;
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Markers = [];
    $scope.Company = [];
    $scope.columnDefs = [
        { headerName: "Property Type", field: "PN_PROPERTYTYPE", width: 110, cellClass: 'grid-align' },
        { headerName: "Property Name", field: "PM_PPT_NAME", width: 100, cellClass: 'grid-align' },
        { headerName: "Entity", field: "CHE_NAME", width: 100, cellClass: 'grid-align'},
        { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Property Address", field: "PM_PPT_ADDRESS", cellClass: 'grid-align', width: 150, },
        { headerName: "Carpet Area(Sqft)", field: "PM_AR_CARPET_AREA", cellClass: 'grid-align', width: 150 },
        { headerName: "Build-up Area (Sqft)", field: "PM_AR_BUA_AREA", cellClass: 'grid-align', width: 150 },
        { headerName: "Owner Name", field: "PM_OWN_NAME", cellClass: 'grid-align', width: 150, },
        { headerName: "Company Name", field: "CNP_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Phone Number", field: "AUR_RES_NUMBER", cellClass: 'grid-align', width: 150, },
        { headerName: "Email Id", field: "AUR_EMAIL", cellClass: 'grid-align', width: 150, },
        { headerName: "Property Status", field: "STA_DESC", cellClass: 'grid-align', width: 150, },
        { headerName: "Property Remarks", field: "PM_PROPERTY_REMARK", cellClass: 'grid-align', width: 150, },
        //{ headerName: "Insurance Type", field: "PM_IT_NAME", cellClass: 'grid-align', width: 150, },
        //{ headerName: "Insurance Vendor", field: "PM_INS_VENDOR", cellClass: 'grid-align', width: 150, },
        //{ headerName: "Insurance Amount", field: "PM_INS_AMOUNT", cellClass: 'grid-align', width: 150, },
        //{ headerName: "Insurance Policy Number", field: "PM_INS_PNO", cellClass: 'grid-align', width: 180, },
        //{ headerName: "Insurance Start Date", field: "PM_INS_START_DT", template: '<span>{{data.PM_INS_START_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, },
        //{ headerName: "Insurance End Date", field: "PM_INS_END_DT", template: '<span>{{data.PM_INS_END_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true }
    ];

    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.Property.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });
    }, 500);
   
    $scope.LoadData = function () {
        deleteMarkers();
        $scope.citylst = [];
        $scope.totalsum = 0;
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();
      
        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    CNP_NAME: $scope.Property.CNP_NAME[0].CNP_ID
                };
                progress(0, 'Loading...', true);
                PropertiesService.GetGriddata(params).then(function (data) {
                    $scope.gridata = data;
                    if ($scope.gridata.data == null) {
                       $("#btNext").attr("disabled", true);
                        $scope.gridOptions.api.setRowData([]);
                      
                    }
                    else {
                        $scope.GridVisiblity = true;
                        $scope.gridOptions.api.setRowData($scope.gridata.data);

                    }
                    progress(0, 'Loading...', false);
                });
               
                PropertiesService.GetMarkers(params).then(function (response) {
                    if (response.data != undefined && response.data != null) {
                       
                        $scope.citylst = response.data.Table1;
                        $scope.totalsum = _.sumBy($scope.citylst, 'CNT');
                        $scope.markerlst = response.data.Table;


                    }

                });
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: false,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        pagination: true,
        paginationPageSize: 10,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ",",
            fileName: "Properties.csv"
        }; $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


    $scope.GenReport = function (Property, Type) {
        var searchval = $("#filtertxt").val();
         Property.Type = Type;
        //var flrObj = { flrlst: response.data.SELSPACES.flrlst };
        progress(0, 'Loading...', true);
        woobj = {};
        angular.copy(Property, woobj);
        woobj.CNP_NAME = $scope.Property.CNP_NAME[0].CNP_ID;
        woobj.SearchValue= searchval;
        woobj.PageNumber= $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        woobj.PageSize = $scope.gridata.data[0].OVERALL_COUNT;
        woobj.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Property.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/Properties/GetAllProperties',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Properties.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $("#table2").fadeIn();

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
                //alert('hi');
                $scope.initMap();
                setTimeout(function () {
                    angular.forEach($scope.markerlst, function (value, key) {
                        addMarker(value);
                    });
                }, 200);
            });
        }
    });

    setTimeout(function () {
        $scope.LoadData();
    }, 700);
    $scope.initMap = function () {
        var haightAshbury = { lat: 23.0885976, lng: 86.1615858 };

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: haightAshbury,
            mapTypeId: 'terrain'
        });
        // This event listener will call addMarker() when the map is clicked.
        //map.addListener('click', function (event) {
        //    addMarker(event.latLng);
        //});

        // Adds a marker at the center of the map.
        //addMarker(haightAshbury);
    }

    var infowindow = new google.maps.InfoWindow();

    // Adds a marker to the map and push to the array.
    function addMarker(data) {
        
        var marker = new google.maps.Marker({
            position: { lat: parseFloat(data.LATITUDE), lng: parseFloat(data.LONGITUDE) },
            map: map,
            title: data.PN_NAME
        });
        marker.CTY_CODE = data.CTY_CODE;
        marker.LCM_CODE = data.LCM_CODE;
        marker.CTY_NAME = data.CTY_NAME;
        marker.LCM_NAME = data.LCM_NAME;

        var content = '<font style=font-family:Arial, Helvetica, sans-serif, serif size=-1 > ' + data.PN_NAME + ' (' + data.PN_CODE + ')  <br> <b>Total Seats:-</b> : ' + data.TOTAL_SEATS + ' <br/><b>Property Description:-</b> : ' + data.PPT_DESC + ' <br/><b>Property Type:-</b> : ' + data.PPT_TYPE + ' </font>'
        marker.addListener('click', function () {
            if (infowindow)
                infowindow.close();
            infowindow.setContent(content);
            infowindow.open(map, marker);
        });

        $scope.Markers.push(marker);
    }

    $scope.CTYClick = function (city) {
        clearMarkers();
        var markers = $filter('filter')($scope.Markers, { CTY_CODE: city.CTY_CODE });
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < $scope.Markers.length; i++) {
            $scope.Markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    $scope.showMarkers = function () {
        setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        $scope.Markers = [];
    }


}]);