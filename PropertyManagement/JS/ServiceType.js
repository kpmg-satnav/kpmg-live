﻿app.service("ServiceTypeService", ['$http', '$q', function ($http, $q) {
    var deferred = $q.defer();
    this.getServiceType = function () {
        deferred = $q.defer();
        return $http.get('../../../api/ServiceType')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //SAVE
    this.saveServiceType = function (mncCat) {
        deferred = $q.defer();
        return $http.post('../../../api/ServiceType/InsertNUpdate', mncCat)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    //UPDATE BY ID
    this.updateServiceType = function (repeat) {
        deferred = $q.defer();
        return $http.post('../../../api/ServiceType/Update/', repeat)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    //Bind Grid
    this.GetServiceTypeBindGrid = function () {
        deferred = $q.defer();
        return $http.get('../../../api/ServiceType/ServiceTypeBindGrid')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('ServiceTypeController', ['$scope', '$q', 'ServiceTypeService', '$timeout', '$http', function ($scope, $q, ServiceTypeService, $timeout, $http) {

 
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    

    //$scope.StaDet[Id] = 1;
    $scope.ServiceType = {};
    $scope.repeatCategorylist = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;

    //to Save the data
    $scope.Save = function () {
        if ($scope.IsInEdit) {
            $scope.ServiceType.FLAG = "UPDATE";
            ServiceTypeService.saveServiceType($scope.ServiceType).then(function (response) {
                console.log(response);
                $scope.ShowMessage = true;
                $scope.Success = response.data.Message;
                $scope.LoadData();
                var savedobj = {};
                $scope.ClearData();
                $scope.IsInEdit = false;
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
                $scope.ServiceType = {};

            }, function (error) {
                console.log(error);
            })
        }
        else {
            $scope.ServiceType.PM_STATUS = "1";
            $scope.ServiceType.FLAG = "INSERT";
            ServiceTypeService.saveServiceType($scope.ServiceType).then(function (response) {
                $scope.ShowMessage = true;
                $scope.Success = response.data.Message;
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
                $scope.LoadData();
                $scope.ServiceType = {};

            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    }
    //for GridView
    var columnDefs = [
        { headerName: "Service Type Name", field: "PM_ST_NAME", width: 160, cellClass: 'grid-align' },
        { headerName: "Status", template: "{{ShowStatus(data.PM_STATUS)}}", width: 100, cellClass: 'grid-align' },//template: "{{ShowStatus(data.PM_STATUS)}}"
        {
            headerName: "Action", width: 70, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align',
            onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true
        }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        ServiceTypeService.GetServiceTypeBindGrid().then(function (data) {
            $scope.gridata = data;
            //$scope.createNewDatasource();
            $scope.gridOptions.api.setRowData(data);
            progress(0, '', false);
        }, function (error) {
            console.log(error);
        });
    }


    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };
    $timeout($scope.LoadData, 1000);
    $scope.EditFunction = function (data) {
        $scope.ServiceType = {};
        $scope.EditCategory = data;
        angular.copy(data, $scope.ServiceType);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        $('#RT_CODE').prop('readonly', true);
    }
    $scope.ClearData = function () {
        $scope.ServiceType = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $('#RT_CODE').prop('readonly', false);
    }
    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

}]);