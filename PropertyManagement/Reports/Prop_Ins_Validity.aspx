﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>

<body data-ng-controller="PropInsValidityController" class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Property Insurance Validity" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Property Insurance Validity</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form role="form" id="form1" name="frminsvalidity" data-valid-submit="LoadGrid()" novalidate>
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12" ng-show="CompanyVisible == 1">
                                <div class="form-group">
                                    <label class="control-label">Company</label>
                                    <div isteven-multi-select data-input-model="Company" data-output-model="InsValidity.Company" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                        item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="InsValidity.Company" name="CNP_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frminsvalidity.$submitted && frminsvalidity.Company.$invalid" style="color: red">Please Select Company </span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">From Date</label>
                                    <div class="input-group date" id='FromDate'>
                                        <input type="text" class="form-control" data-ng-model="InsValidity.FromDate" id="Text1" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('FromDate')"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">To Date</label>
                                    <div class="input-group date" id='ToDate'>
                                        <input type="text" class="form-control" data-ng-model="InsValidity.ToDate" id="Text2" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('ToDate')"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer text-left" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px; padding-top: 21px;">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                    <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="col-md-12" id="table2" data-ng-show="gridata.length>0">
                                    <br />
                                    <a data-ng-click="GenReport(InsValidity,'doc')"><i id="word" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(InsValidity,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(InsValidity,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                            </div>
                            <div class="row" style="padding-left: 20px;">
                                <%--<input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />--%>
                                <div class="input-group" style="width: 20%">
                                    <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary custom-button-color" type="submit">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div data-ag-grid="gridOptions" style="height: 250px; width: 98%;" class="ag-blue"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"] %>';
    </script>
    <script src="../../SMViews/Utility.min.js"></script>
    <script src="../JS/Prop_Ins_Validity.min.js"></script>
    <script type="text/javascript">
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().startOf('year').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();
        });
    </script>
</body>
</html>
