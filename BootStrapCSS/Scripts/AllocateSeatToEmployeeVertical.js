﻿//console.log('dr');
var cty = '';
var vercode = '';
var PrjCode = '';
var deptname = '';
var spacetyp = 0;
var bdgid;
var hypSpcId = '';

$("#divemployeedetails").hide();

var spcid = null;

$(document).ready(function () {
    //var loading = $("#divloading");
    $("#divloading").hide();
    //Allocate to Vertical btn functionality
    $("#Button1").click(function () {
        var verticalcode = $("#ddlVertical").val();

        if ($("#txtFrmDate").val() != "") {
            if ($("#txtToDate").val() != "") {
                if (verticalcode != "--Select--") {
                    //spacetyp = 2 for Sharable
                    if (spacetyp == "2") {
                        if ($("#ddlshift").val() == "--Select--") {
                            notifymsg('Please Select Shift');
                            return false;
                        }
                        if ($("#starttimehr").val() == "Hr") {
                            notifymsg('Please Enter From Time');
                            return false;
                        }
                        if ($("#starttimemin").val() == "Min") {
                            notifymsg('Please Enter From Minutes');
                            return false;
                        }
                        if ($("#endtimehr").val() == "Hr") {
                            notifymsg('Please Enter To Time');
                            return false;
                        }
                        if ($("#endtimemin").val() == "Hr") {
                            notifymsg('Please Enter To Minutes');
                            return false;
                        }
                    }

                    Functionshow();

                    gdavailBind('', '');
                    if ($('#gdavail td').length == 0) {
                        notifymsg('Time slot not available.');
                        functionhide();
                        return;
                    }
                    else {
                        //alert("Button1 Len " + data.length);
                        //fnBindSA();
                        //var empcode = $("#txtEmpName").val().split(" ");


                        if ($('#gdavail').length > 0) {
                            // SubmitAllocationOccupied(ddlVertical.SelectedItem.Value, Session("uid"), 6)

                            $.ajax({
                                type: "POST",
                                url: '../api/AllSeattoEmpVertAPI/SubmitAllocationOccupied',
                                data: "=" + verticalcode,
                                dataType: "json",
                                async: false,
                                success: function (data) {
                                    var resl = data.split("@");

                                    fnsubmitAO(verticalcode, resl[2], 6, resl[0], resl[1], resl[3], "1");

                                }
                            });
                        }
                        else {

                        }
                        //else{}
                    }
                }
                else {
                    notifymsg('Please select ' + lblparent);
                }
            }
            else {
                notifymsg('Please Enter To Date');
            }
        }
        else {
            notifymsg('Please Enter From Date');
        }

        functionhide();
        return false;
    });

    $("#btntest").click(function () {
        Functionshow();
        functionhide();
    });

    //Allocate to employee
    $("#btnSubmit").click(function () {
       
        var verticalcode = $("#ddlVertical").val();
        if (verticalcode != "--Select--") {
            var empcode = $("#txtEmpName").val().split(" ");

            if ($('#gdavail td').length == 0) {
                notifymsg('Time slot not available.');
               // functionhide();
                return;
            }
        
            $.ajax({
                type: "POST",
                url: '../api/AllSeattoEmpVertAPI/VacantEmpToAllocateNewSeat',
                data: { 'reqid': hypSpcId, 'AurId': empcode[0], 'DeptName': deptname },
                dataType: "json",
                async: false,
                success: function (data1) {                   
                    $.ajax({
                        type: "POST",
                        url: '../api/AllSeattoEmpVertAPI/SubmitAllocationOccupied',
                        data: "=" + verticalcode,
                        dataType: "json",
                        async: false,
                        success: function (data) {
                            Functionshow();
                            var resl = data.split("@");
                            fnsubmitAO(verticalcode, empcode[0], 7, resl[0], resl[1], resl[3], "2");                           
                        }
                    });                   
                }
            });
        }
        else {
            notifymsg('Please select ' + lblparent);
        }
        functionhide();
        //return false;
    });

    //Spacerelease gvSharedEmployee Employee Occ
    $("#gvSharedEmployee").on("click", 'button[type="button"]', function (e) {
        var Aurcode = $(this).closest('tr').find('td:eq(0)').text();
        var Spaceid = $(this).closest('tr').find('td:eq(3)').text();
        //spacetyp            
        Functionshow();
        $.ajax({
            type: "POST",
            url: '../api/AllSeattoEmpVertAPI/SharedEmployeeSpaceRelease',
            data: { 'spaceid': Spaceid, 'AurId': Aurcode, 'spacetype': spacetyp, 'spaceid': spcid },
            dataType: "json",
            async: false,
            success: function (data) {
                fnBindSA();
                clearvalues();
                $("#divemployeedetails").hide();
                notifymsgsuccess('Employee ' + Aurcode + ' Released');
            }
        });
        functionhide();
    });

    //gvAllocatedSeats release click   Vertical Release
    $("#gvAllocatedSeats").on("click", 'button[type="button"]', function (e) {
        var reqid = $(this).parents('tr').find('#hdnreqid').val();
        var Vert = $(this).closest('tr').find('td:eq(1)').text();
        var Spid = $(this).closest('tr').find('td:eq(2)').text();
        var fromdatet = $(this).closest('tr').find('td:eq(3)').text();
        var todatet = $(this).closest('tr').find('td:eq(4)').text();
        var fromtym = $(this).closest('tr').find('td:eq(5)').text();
        var totym = $(this).closest('tr').find('td:eq(6)').text();
        //alert(reqid);
        var employeeid = $("#txtEmpName").val().split(" ", 1);
        Functionshow();
        $.ajax({
            type: "POST",
            url: '../api/AllSeattoEmpVertAPI/AllocatedSeatsRelease',
            data: { 'VerticalCode': Vert, 'spaceid': Spid, 'fromdate': fromdatet, 'todate': todatet, 'fromtime': fromtym, 'totime': totym, 'spacetype': spacetyp, 'DeptName': deptname, 'AurId': employeeid, 'reqid': reqid },
            dataType: "json",
            async: false,
            success: function (data) {
                fnBindSA();
                clearvalues();
                $("#divemployeedetails").hide();
                notifymsgsuccess('Vertical - ' + Vert + ' Released');
            }
        });
        functionhide();
    });

    //check employee details btn- tab2 
    $("#btnallocate").click(function () {
        Functionshow();
        btnalloc();
        functionhide();
        //alert("LenS- " + $('#gdavail tr').length);
        if ($('#gdavail tr').length > 0) {
            $("#btnSubmit").show();
        }
        else {
            $("#btnSubmit").hide();
        }
        return false;
    });

    //
    $("#ddlVertical").on('change', function () {
        $('#txtEmpName').val('');
    });

    $("#ddlshift").on('change', function () {
        var dataObject = { flr_id: $('#ddlfloors').val(), shift_id: $('#ddlshift').val() };
        $.post('../api/AllSeattoEmpVertAPI/GetShiftTimings', dataObject, function (result) {
            $.each(result, function (key, value) {
                $('#starttimehr').val(value.SH_FRM_HRS).attr('disabled', true);
                $('#starttimemin').val(value.SH_FRM_MINS).attr('disabled', true);
                $('#endtimehr').val(value.SH_TO_HRS).attr('disabled', true);
                $('#endtimemin').val(value.SH_TO_MINS).attr('disabled', true);
            });
            $('#starttimehr').selectpicker('refresh');
            $('#starttimemin').selectpicker('refresh');
            $('#endtimehr').selectpicker('refresh');
            $('#endtimemin').selectpicker('refresh');
        });
    });
});
//$("#btnSubmit").hide();

// Vertical/Cost center labels
//GetlblVertical();

////ddlshift binding
//GetURL();

////ddlvertical binding
//GetVerticals();

////
//fnBindSA()

////Auto complete empid
//SearchText();

//$("#btnSubmit").hide();

////gvAssets  binding

//fnbindgvAssets();
function startendtimehrs() {
    var hours = 23;
    var option = '';

    for (var i = 0; i <= hours; i++) {
        if (i <= 9) {
            i = '0' + i;
        }
        option += '<option value="' + i + '">' + i + '</option>';
    }
    $("#starttimehr").append(option);
    $("#starttimehr").selectpicker('refresh');
    $("#endtimehr").append(option);
    $("#endtimehr").selectpicker('refresh');
}

//gvAssets binding function 
function fnbindgvAssets() {
    $.ajax({
        type: "POST",
        url: "../api/AllSeattoEmpVertAPI/GetgvAssets",
        data: "=" + spcid,
        dataType: "json",
        success: function (data) {
            var table = $('#gvAssets');
            table.find("tr:gt(0)").remove();
            for (var i = 0; i < data.length; i++) {
                $("#gvAssets").append("<tr>" +
                    "<td>" + data[i].AAT_CODE + "</td>" +
                     "<td>" + data[i].AAT_NAME + "</td>" +
                     "<td>" + data[i].SPC_NAME + "</td>" + "</tr>");
            }
        },
        failure: function (response) {

        }
    });
}

var lblparent;
//Vertical/Cost center labels
function GetlblVertical() {
    $.ajax({
        type: "get",
        url: "../api/AllSeattoEmpVertAPI/GetlblVerticalCC",
        data: {},
        contentType: "application/json; charset=utf-8",
        // async:false,
        success: function (data) {
            $("label[for='lblSelVertical']").html(data[0]);
            $("label[for='lblSelVertical1']").html(data[0]);
            $("label[for='lblSelProject']").html(data[1]);
            lblparent = data[0];
        },
        failure: function (response) {
        }
    });
}

//url post and fill ddlshift 
function GetURL() {
    $("#ddlshift").find('option').remove();
    $("#ddlshift").append('<option value="--Select--">--Select--</option>');
    var url = $(location).attr('search');
    var val = url.split('/', 1);
    $.ajax({
        type: "POST",
        url: '../api/AllSeattoEmpVertAPI/GetURL',
        data: { 'URL': $('#ddlfloors').val() },
        dataType: "json",
        success: function (data) {
            var item = JSON.stringify(data);
            var $select = $('#ddlshift');
            $('#ddlshift option').remove();
            $select.append('<option >--Select--</option>');
            $.each(data, function (key, val) {
                $select.append('<option value="' + val.SH_CODE + '">' + val.SH_NAME + '</option>');
            })
            $("#ddlshift").selectpicker('refresh');
        }
    });
}

//fill ddlVertical
function GetVerticals() {
    $("#ddlVertical").find('option').remove();
    $("#ddlVertical").append('<option value="--Select--">--Select--</option>');
    $.ajax({
        type: "GET",
        url: '../api/AllSeattoEmpVertAPI/GetVerticals',
        data: {},
        dataType: "json",
        success: function (data) {
            var item = JSON.stringify(data);
            var $select = $('#ddlVertical');
            $.each(data, function (key, val) {
                $select.append('<option value="' + val.VER_CODE + '">' + val.VER_NAME + '</option>');
            })
            $("#ddlVertical").selectpicker('refresh');
        }
    });
}

//AUTO COMPLETE EMPID-NAME
function SearchText() {
    $("#txtEmpName").autocomplete({
        source: function (request, response) {
            var vertcode = $('#ddlVertical').val();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                //url: "AllocateSeatToEmployeeVertical.aspx/SearchCustomers",
                url: '../api/AllSeattoEmpVertAPI/SearchCustomers',
                data: "{'AurId':'" + document.getElementById('txtEmpName').value + "', 'VerticalCode':'" + $('#ddlVertical').val() + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.length == 0)
                        notifymsg('No Records Found');
                    else
                        response(data);
                },
                error: function (result) {
                }
            });
        }
    });
}

//btnallocate check emp details tab - 2 Binding
function btnalloc() {
    $("label[for='lblMsg']").html('');
    $("#divemployeedetails").hide();
    if ($("#txtFrmDate").val() != "") {
        if ($("#txtToDate").val() != "") {
            if ($("#txtEmpName").val() != "") {

                if (spacetyp == "2") {
                    if ($("#ddlshift").val() == "--Select--") {
                        notifymsg('Please Select Shift');
                        return false;
                    }
                    if ($("#starttimehr").val() == "Hr") {
                        notifymsg('Please Enter From Time');
                        return false;
                    }
                    if ($("#starttimemin").val() == "Min") {
                        notifymsg('Please Enter From Minutes');
                        return false;
                    }
                    if ($("#endtimehr").val() == "Hr") {
                        notifymsg('Please Enter To Time');
                        return false;
                    }
                    if ($("#endtimemin").val() == "Hr") {
                        notifymsg('Please Enter To Minutes');
                        return false;
                    }
                }

                var employeeid = $("#txtEmpName").val().split(" ", 1);
                //fnGetSpaceType();
                $.ajax({
                    type: "POST",
                    url: '../api/AllSeattoEmpVertAPI/GetEmpDetails',
                    data: { 'EmployeeID': employeeid },
                    dataType: "json",
                    async: false,
                    success: function (data) {
                        if (data.length > 0) {

                            $("label[for='lblEmpSearchId']").html(data[0].AUR_ID);
                            $("label[for='lblEmpName']").html(data[0].AUR_FIRST_NAME);
                            $("label[for='lblEmpEmailId']").html(data[0].AUR_EMAIL);
                            $("label[for='lblReportingTo']").html(data[0].REPORTTO);
                            $("label[for='lblAUR_BDG_ID']").html(data[0].AUR_BDG_ID);
                            $("label[for='lblAUR_VERT_CODE']").html(data[0].VER_NAME);
                            cty = data[0].AUR_CITY;
                            vercode = data[0].AUR_VERT_CODE;
                            $("label[for='lblCity']").html(data[0].AUR_CITY);
                            $("label[for='lblDepartment']").html(data[0].DEPARTMENT);
                            deptname = data[0].AUR_DEP_ID;
                            $("label[for='lblDesignation']").html(data[0].AUR_DESGN_ID);
                            $("label[for='lblEmpExt']").html(data[0].AUR_EXTENSION);
                            $("label[for='lblEmpFloor']").html(data[0].AUR_FLOOR);
                            $("label[for='lblPrjCode']").html(data[0].AUR_PRJ_CODE);
                            PrjCode = data[0].AUR_PRJ_CODE;
                            $("label[for='lblResNumber']").html(data[0].AUR_RES_NUMBER);
                            $("label[for='lblState']").html(data[0].AUR_STATE);
                            $("label[for='lblEmpTower']").html(data[0].AUR_TOWER);
                            $("label[for='lblDoj']").html(data[0].AUR_DOJ);
                            // $("label[for='hypSpaceId']").html(data[0].AUR_LAST_NAME);
                            $("#hypSpaceId").text(data[0].AUR_LAST_NAME);
                            hypSpcId = data[0].AUR_LAST_NAME;

                            // alert("hyspcid- " + hypSpcId +"leng- " + hypSpcId.length);

                            //$('#ddlVertical').prop('selectedIndex', 0);
                            //$("#ddlVertical").selectpicker('refresh');

                            if (hypSpcId.length == 0) {
                                $("label[for='hypSpaceId']").html("Space not allocated to " + data[0].AUR_FIRST_NAME);
                            }
                            else {
                                $("label[for='hypSpaceId']").html(hypSpcId);
                            }

                            var verticl = data[0].VER_NAME;

                            if ($("select[id$='ddlVertical'] option:contains(" + verticl + ")").length > 0) {
                                $("#ddlVertical option:contains(" + verticl + ")").attr('selected', true);

                                $("#ddlVertical").selectpicker('refresh');
                            }
                            else {
                                var str = new Array[3];
                                arr = verticl.Split("/");
                                if (arr.Length > 0) {
                                    var $select = $('#ddlVertical');
                                    $select.append('<option id="' + $('#ddlVertical').children('option').length + '">' + arr + '</option>');
                                }
                            }
                        }
                        else {
                            $("label[for='lblmsg']").html("");
                        }
                    }
                });


                gdavailBind(cty, vercode);
                if ($('#gdavail td').length == 0) {
                    notifymsg('Time slot not available.');
                    return;
                }
                else {
                    fnBindSA();
                    $("#divemployeedetails").show();
                }
                //gdavail bind
            }
            else {
                notifymsg('Please select employee to search')
            }
        }
        else {
            notifymsg('Please Enter To Date');
        }
    }
    else {
        notifymsg('Please Enter From Date');
    }
}

// load() functinality in aspx.cs, Binding gdavail  
function gdavailBind(city, vercode) {
    //var url = $(location).attr('search');

    var ftime = "";
    var ttime = "";
    var StartHr = "00";
    var EndHr = "00";
    var StartMM = "00";
    var EndMM = "00";

    if ($("#starttimehr").val() != 'Hr') {
        StartHr = $("#starttimehr").val();
    }
    if ($("#starttimemin").val() != "Min") {
        StartMM = $("#starttimemin").val();
    }
    if ($("#endtimehr").val() != "Hr") {
        EndHr = $("#endtimehr").val();
    }
    if ($("#endtimemin").val() != "Min") {
        EndMM = $("#endtimemin").val();
    }

    if (spacetyp == "1") {
        ftime = "00:00"
        ttime = "23:59"
    }
    else {
        ftime = StartHr + ":" + StartMM
        ttime = EndHr + ":" + EndMM
    }

    var fromdt = $("#txtFrmDate").val();
    var todt = $("#txtToDate").val();

    $.ajax({
        type: "POST",
        url: '../api/AllSeattoEmpVertAPI/GetSeattoAllocateOccupied',
        data: { location: bdgid, floor: $('#ddlfloors').val(), fromdate: fromdt, todate: todt, fromtime: ftime, totime: ttime, spacetype: spacetyp, spaceid: spcid, mode: 0 },
        dataType: "json",
        async: false,
        success: function (data) {
            var table = $('#gdavail');
            table.find("tr:gt(0)").remove();

            for (var i = 0; i < data.length; i++) {
                table.append("<tr>" +
                    "<td>" + data[i].SPC_ID + "</td>" +
                     "<td>" + data[i].SPC_BDG_ID + "</td>" +
                    "</tr>");
            }
            if (data.length > 0) {
                $("#btnSubmit").show();
            }
        }
    });
}

//get spacetype and Binding gvSharedEmployee/gvAllocatedSeats
function fnBindSA() {
    $.ajax({
        type: "POST",
        url: '../api/AllSeattoEmpVertAPI/GetSpaceType',
        data: "=" + spcid,
        dataType: "json",
        async: true,
        success: function (data) {
            spacetyp = data.Table[0].SPACE_TYPE;
            bdgid = data.Table[0].SPC_BDG_ID;

            if (spacetyp == "2") {
                $("#trshift").show();
                $("#trTimeSlot").show();
            }
            else {
                $("#trshift").hide();
                $("#trTimeSlot").hide();
            }
            //gvSharedEmployee Binding
            $('#gvSharedEmployee tbody').remove();
            var table = $('#gvSharedEmployee');
            //table.find("tr:gt(0)").remove();
            if (data.Table1.length == 0) {
                table.append("<tr>" + "<td colspan='9' align='left'> Not Occupied By Any Employee.</td>" + "</tr>");
            }
            else {
                for (var i = 0; i < data.Table1.length; i++) {
                    table.append("<tr>" + "<td>" + data.Table1[i].AUR_ID + "</td>" +
                          "<td>" + data.Table1[i].AUR_FIRST_NAME + "</td>" +
                          "<td>" + data.Table1[i].AUR_EMAIL + "</td>" +
                         "<td>" + data.Table1[i].AUR_LAST_NAME + "</td>" +
                          "<td>" + data.Table1[i].SSA_FROM_DATE + "</td>" +
                          "<td>" + data.Table1[i].SSA_TO_DT + "</td>" +
                         "<td>" + data.Table1[i].FROM_TIME + "</td>" +
                          "<td>" + data.Table1[i].TO_TIME + "</td>" +
                         "<td>" + "<button type='button' class='btn btn-primary custom-button-color' data-toggle='modal' >Release</button>" + "</td>" + "</tr>");
                }
            }

            //gvAllocatedSeats Binding
            $('#gvAllocatedSeats tbody').remove();
            var tableAS = $('#gvAllocatedSeats');
            //tableAS.find("tr:gt(0)").remove();
            if (data.Table2.length == 0) {
                tableAS.append("<tr>" + "<td colspan='8' align='left'> Not Allocated To Any Vertical.</td>" + "</tr>");
                $("#searchemppnl").show();
                $("#divgrid").hide();
            }
            else {
                for (var i = 0; i < data.Table2.length; i++) {

                    //alert($("#hfid").val());
                    tableAS.append("<tr>" +
                         "<td  style='display:none;'><input type='hidden' id='hdnreqid' value='" + data.Table2[i].SSA_SRNREQ_ID + "'></td>" +
                        "<td>" + data.Table2[i].SSA_VERTICAL + "</td>" +
                         "<td>" + data.Table2[i].SSA_SPC_ID + "</td>" +
                          "<td>" + data.Table2[i].SSA_FROM_DATE + "</td>" +
                         "<td>" + data.Table2[i].SSA_TO_DT + "</td>" +
                          "<td>" + data.Table2[i].FROM_TIME + "</td>" +
                         "<td>" + data.Table2[i].TO_TIME + "</td>" +
                           "<td>" + "<button type='button' class='btn btn-primary custom-button-color' data-toggle='modal' >Release</button>" + "</td>" + "</tr>");
                }
                $("#searchemppnl").hide();
                $("#divgrid").show();
                //fnrelease1();

                if (data.Table1.length >= 1)
                    //$('#gvAllocatedSeats td').eq(7).html('');                    
                    $("#gvAllocatedSeats tr th:nth-child(8), #gvAllocatedSeats tr td:nth-child(8)").hide();
                else
                    $("#gvAllocatedSeats tr th:nth-child(8), #gvAllocatedSeats tr td:nth-child(8)").show();
            }
            if (data.Table1.length == 0 && data.Table2.length != 0) {

                $("#searchemppnl").show();
                $("#divgrid").show();
            }
            if (spacetyp == 2 && (data.Table1.length != 0 || data.Table2.length != 0)) {
                $("#searchemppnl").show();
                $("#divgrid").show();
            }
        }
    });
}

//SubmitAllocationOccupied('','','') in aspx.cs
function fnsubmitAO(verc, aurid, sta, reqid, osdate, tmpreqid, type) {    
    var verticalreqid = reqid;
    var cntWst = 0
    var cntHCB = 0
    var cntFCB = 0
    var twr = ""
    var flr = ""
    var wng = ""
    var intCount = 0
    var ftime = ""
    var ttime = ""
    var StartHr = "00"
    var EndHr = "00"
    var StartMM = "00"
    var EndMM = "00"

    if ($("#starttimehr").val() != 'Hr') {
        StartHr = $("#starttimehr").val();
    }
    if ($("#starttimemin").val() != "Min") {
        StartMM = $("#starttimemin").val();
    }
    if ($("#endtimehr").val() != "Hr") {
        EndHr = $("#endtimehr").val();
    }
    if ($("#endtimemin").val() != "Min") {
        EndMM = $("#endtimemin").val();
    }

    if (spacetyp == "1") {
        ftime = "00:00"
        ttime = "23:30"
    }
    else {
        ftime = StartHr + ":" + StartMM
        ttime = EndHr + ":" + EndMM
    }

    var fromdatee = new Date($("#txtFrmDate").val());
    var todatee = new Date($("#txtToDate").val());
    //var frmdtc = ((fromdatee.getMonth() + 1) + '/' + fromdatee.getDate() + '/' + fromdatee.getFullYear());
    //var todtc = ((todatee.getMonth() + 1) + '/' + todatee.getDate() + '/' + todatee.getFullYear());
    var todtc2 = new Date(todatee.getFullYear(), (todatee.getMonth() + 1), todatee.getDate());
    var frmdtc2 = new Date(fromdatee.getFullYear(), (fromdatee.getMonth() + 1), fromdatee.getDate());
    
    var osdt = new Date(osdate);
    if ((fromdatee.getMonth() + 1) < (osdt.getMonth() + 1) & todatee.getFullYear() < osdt.getFullYear()) {
        notifymsg('You cant request for the past month');
        return false;
    }
    else if ((fromdatee.getMonth() + 1) < (osdt.getMonth() + 1) & todatee.getFullYear() == osdt.getFullYear()) {
        notifymsg('You cant request for the past month');
        return false;
    }
    else if ((fromdatee.getMonth() + 1) > (todatee.getMonth() + 1) & todatee.getFullYear() == osdt.getFullYear()) {
        notifymsg('Please Select Valid Date. To Date Cannot be less than From Date');
        return false;
    }      

    else if ((fromdatee.getMonth() + 1) == (osdt.getMonth() + 1) && todatee.getFullYear() == osdt.getFullYear()) {        
        if (fromdatee.getDate() < osdt.getDate()) {
            notifymsg('Please Select Valid Date. From Date Cannot be less than Current Date');
            return false;
        }
        if (fromdatee.getDate() > todatee.getDate() && (todatee.getMonth() + 1) == (osdt.getMonth() + 1)) {
            //alert((fromdatee.getMonth() + 1) + " " + (osdt.getMonth() + 1)+ "" +(todatee.getMonth() + 1));
            notifymsg('Please Select Valid Date. To Date Cannot be less than From Date');
            return false;
        }
    }

    else if ((fromdatee.getMonth() + 1) == (osdt.getMonth() + 1) && todatee.getFullYear() > osdt.getFullYear()) {
        if (fromdatee.getDate() < osdt.getDate()) {
            notifymsg('Please Select Valid Date. From Date Cannot be less than Current Date');
            return false;
        }  
    }
    
    else if (frmdtc2 < osdt || todtc2 < frmdtc2) {
        notifymsg('Selected To Date Cannot be less than From Date');
        return false;
    }

    var cntWst = 0
    var cntHCB = 0
    var cntFCB = 0
    var sptype = '';
    //alert($("#gdavail").closest('tr').find('td:eq(1)').val());
    var table = $("#gdavail");

    table.find('tr').each(function (i) {
        if (i != 0) {
            var $tds = $(this).find('td'),
                //Id = $tds.eq(0).text(),
                stype = $tds.eq(1).text();
            sptype = stype;
            if (stype == "WORK STATION") {
                cntWst += 1;
            }
            if (stype == "HALF CABIN") {
                cntHCB += 1;
            }
            if (stype == "FULL CABIN") {
                cntFCB += 1;
            }
            //spacetyp

        }
    });

    $.ajax({
        type: "POST",
        url: '../api/AllSeattoEmpVertAPI/BlockSeatsRequestPart1_Updated',
        data: { 'reqid': reqid, 'VerticalCode': $('#ddlVertical').val(), 'cntWst': cntWst, 'cntFCB': cntFCB, 'cntHCB': cntHCB, 'fromdate': $("#txtFrmDate").val(), 'todate': $("#txtToDate").val(), 'fromtime': ftime, 'totime': ttime, 'spacetype': spacetyp, 'costcenter': PrjCode, 'spaceid': spcid, 'AurId': aurid, 'BDGID': bdgid, 'city': cty, 'Statusid': sta, 'shift': $('#ddlshift').val(), 'DeptName': deptname, 'Messege': $("label[for='lblMsg']").text(), 'TmpReqseqid': tmpreqid },
        dataType: "json",
        async: false,
        success: function (data) {
            //if (obj.Messege == "") {                       
            //    HttpContext.Current.Response.Redirect("AllocateSeatToEmployeeVertical.html?id=" + HttpContext.Current.Request.QueryString[obj.spaceid]);
            //}
            //else {

            //}

            var verticalcde = $('#ddlVertical option:selected').text();            
            fnBindSA();
            clearvalues();
            $("#divgrid").show();

            if (type == "1") {
                notifymsgsuccess('Allocated to Vertical - ' + verticalcde);
            }
            else {
                notifymsgsuccess('Occupied By Employee ' + aurid);
            }
        },
        failure: function (data) {
        }
    });
}

function setup(id) {
    $('#' + id).datepicker({
        format: 'mm/dd/yyyy'
        //autoclose: true
    }).on('changeDate', function (e) {
        $(this).datepicker('hide');
    });
};

// clear values
function clearvalues() {
    $('#divemployeedetails').hide();
    $('#txtFrmDate').val('');
    $('#txtToDate').val('');
    $('#ddlVertical').val('');
    $('#ddlVertical').selectpicker('refresh');
    $('#txtEmpName').val('');
    $("#extdate").datepicker('setDate', null);
    $("#indate").datepicker('setDate', null);
    $('#ddlshift').val('');
    $('#ddlshift').selectpicker('refresh');
    $('#starttimehr').val('');
    $('#starttimehr').selectpicker('refresh');
    $('#starttimemin').val('');
    $('#starttimemin').selectpicker('refresh');
    $('#endtimehr').val('');
    $('#endtimehr').selectpicker('refresh');
    $('#endtimemin').val('');
    $('#endtimemin').selectpicker('refresh');
}

function notifymsg(displaytext) {
    $.notify({
        title: '<strong>Heads up!</strong>',
        message: displaytext
    }, {
        type: 'danger',
        delay:500,
        timer: 50
    });

}
function notifymsgsuccess(displaytext) {
    $.notify({
        title: '<strong>Success!</strong>',
        message: displaytext
    }, {        
        type: 'success',
        delay: 500,
        timer: 50
    });

}

function Functionshow() {
    if (navigator.userAgent.search("Firefox") >= 0) {
        $("#divloading").show();
    }
    else if (navigator.userAgent.search("Chrome") > 0) {        
        setTimeout(function () { $("#divloading").show(); }, 200);
    }    
}

function functionhide() {

    if (navigator.userAgent.search("Firefox") >= 0) {
        $("#divloading").hide();
    }
    else if (navigator.userAgent.search("Chrome") > 0) {        
        setTimeout(function () { $("#divloading").hide(); }, 300);
    }    
}