﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AssetBreakdownReport.aspx.cs" Inherits="DrillDownReports_AssetBreakdownReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>

        function setup(id) {
            $('.date').datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };

    </script>
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Maintenance Consolidated Report" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Asset Breakdown Report</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Location<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="rfvLocName" InitialValue="--Select--"
                                        runat="server" ControlToValidate="ddlLName" ErrorMessage="Please Select Location"
                                        ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="ddlLName" runat="server" CssClass="selectpicker" data-live-search="true"
                                        ToolTip="Select Location Name">
                                        <asp:ListItem>--Select--</asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Vendor<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlVendor"
                                        ErrorMessage="Please Select Vendor" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="ddlVendor" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Vendor">
                                        <asp:ListItem>--Select--</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>From Date<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtFromDate" ErrorMessage="Please Select From Date"
                                        SetFocusOnError="True" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <div class='input-group date' id='fromdate'>
                                        <%-- <asp:CompareValidator ID="CV1" runat="server"  ControlToValidate="txtmnfdate"  ControlToCompare="txtExpdate" Type="Date" Operator="GreaterThanEqual"></asp:CompareValidator>--%>

                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control" MaxLength="10" ClientIDMode="Static"> </asp:TextBox>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>To Date<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate" ErrorMessage="Please Select To Date"
                                        SetFocusOnError="True" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <div class='input-group date' id='Div1'>

                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                        </span>

                                    </div>
                                    <asp:CompareValidator ID="CV1" runat="server" ControlToValidate="txtToDate" ControlToCompare="txtFromDate" Type="Date" Operator="GreaterThanEqual" ValidationGroup="Val1" ErrorMessage="Todate should be greater than Fromdate"></asp:CompareValidator>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right" style="padding-top: 17px">
                                <div class="form-group">
                                    <asp:Button ID="btnSearch" CssClass="btn btn-primary" runat="server" Text="Search" ValidationGroup="Val1" OnClick="btnSearch_Click" />
                                    <asp:Button ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back" />
                                </div>
                            </div>
                        </div>
                        <br />
                        <label id="lblMsg" runat="server"></label>

                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <div class="row table table table-condensed table-responsive">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" ShowExportControls="true" ShowPrintButton="true" ShowRefreshButton="False" ShowZoomControl="False" ShowFindControls="true"></rsweb:ReportViewer>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
