﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Property_Count.aspx.cs" Inherits="DrillDownReports_Property_Count" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />

    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        /*body {
            color: #6a6c6f;
            background-color: #f1f3f6;
            margin-top: 30px;
        }*/

        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Add Lease" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Property Consolidated Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" class="form-horizontal" runat="server">
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1"
                                    ForeColor="Red" />
                                <div class="row">
                                      <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" Visible="false" runat="server" CssClass="col-md-6 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="row" runat="server">
                                                <label class="col-md-5 control-label">Company  <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlCompany"
                                                    Display="None" ErrorMessage="Please Select Company" InitialValue="--Select--" ValidationGroup="Val1"> </asp:RequiredFieldValidator>

                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="row">
                                                        <div class="col-md-5 control-label">
                                                            <asp:Button ID="btnSubmit" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" OnClick="btnSubmit_Click"></asp:Button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row table table table-condensed table-responsive" runat="server" id="RptViewer">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" ShowBackButton="False" ShowExportControls="False" ShowFindControls="False" ShowPageNavigationControls="False" ShowPrintButton="False" ShowRefreshButton="False" ShowZoomControl="False"></rsweb:ReportViewer>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


