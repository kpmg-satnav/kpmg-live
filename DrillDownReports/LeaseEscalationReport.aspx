﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LeaseEscalationReport.aspx.cs" Inherits="DrillDownReports_LeaseEscalationReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
            });
        };
    </script>
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Ast Loc Report" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Lease Escalation Report </h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1" ForeColor="Red" />

                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>

                        <%--                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Location</label>
                                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                ToolTip="Select Location" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>City</label>
                                            <asp:DropDownList ID="ddlcity" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                ToolTip="Select City" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Tower</label>
                                            <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                ToolTip="Select Floor">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Floor</label>
                                            <asp:DropDownList ID="ddlfloor" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                ToolTip="Select Floor">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    

                                </div>--%>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Property Name</label>
                                    <asp:DropDownList ID="ddlproperty" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="True"
                                        ToolTip="Select Property Name">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>From Date</label>
                                    <div class='input-group date' id='Div1'>
                                        <asp:TextBox ID="FromDate" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy" MaxLength="10"> </asp:TextBox>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('Div1')"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>To Date</label>
                                    <div class='input-group date' id='Div4'>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy" MaxLength="10"> </asp:TextBox>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('Div4')"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="Button1" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" OnClick="Button1_Click" />
                                </div>
                            </div>
                        </div>

                        <div class="row table table table-condensed table-responsive" runat="server" id="RptViewer">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" ShowBackButton="False" ShowExportControls="true" ShowFindControls="False" ShowPageNavigationControls="true" ShowPrintButton="true" ShowRefreshButton="False" ShowZoomControl="False"></rsweb:ReportViewer>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtToDate").val(moment().format('MM-DD-YYYY'));
            $("#FromDate").val(moment().subtract(30, 'days').format('MM-DD-YYYY'));
            //rangeChanged();
        });
    </script>
    <script src="../Scripts/moment.min.js"></script>

</body>
</html>



