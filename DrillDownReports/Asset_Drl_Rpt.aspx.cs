﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DrillDownReports_Asset_Drl_Rpt : System.Web.UI.Page
{
    clsSubSonicCommonFunctions ObjSubsonic = new clsSubSonicCommonFunctions();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect(Convert.ToString(Application["FMGLogout"]));
        }

        if (!IsPostBack)
        {
            FillCompanies();
            BindLocation();
            getassetcategory();
            (ddlAssetCategory as IPostBackDataHandler).RaisePostDataChangedEvent();
            LoadReport();
        }
    }

    private void FillCompanies()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_COMPANIES");
        ddlCompany.DataSource = sp.GetReader();
        ddlCompany.DataTextField = "CNP_NAME";
        ddlCompany.DataValueField = "CNP_ID";
        ddlCompany.DataBind();
    }

    public void LoadReport()
    {
        try
        {
            string AstCat = "";
            if (ddlAssetCategory.SelectedValue == "--All--")
            {
                AstCat = "";
            }
            else
            {
                AstCat = ddlAssetCategory.SelectedValue;
            }

            string AstSubCat = "";
            if (ddlAstSubCat.SelectedValue == "--All--")
            {
                AstSubCat = "";
            }
            else
            {
                AstSubCat = ddlAstSubCat.SelectedValue;
            }

            string AstBrand = "";
            if (ddlAstBrand.SelectedValue == "--All--")
            {
                AstBrand = "";
            }
            else
            {
                AstBrand = ddlAstBrand.SelectedValue;
            }

            string AstModel = "";
            if (ddlModel.SelectedValue == "--All--")
            {
                AstModel = "";
            }
            else
            {
                AstModel = ddlModel.SelectedValue;
            }

            string Location = "";
            if (ddlLocation.SelectedValue == "--All--")
            {
                Location = "";
            }
            else
            {
                Location = ddlLocation.SelectedValue;
            }
           
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@Category", SqlDbType.NVarChar, 200);
            param[0].Value = AstCat;
            param[1] = new SqlParameter("@SubCategory", SqlDbType.NVarChar, 200);
            param[1].Value = AstSubCat;
            param[2] = new SqlParameter("@Brand", SqlDbType.NVarChar, 200);
            param[2].Value = AstBrand;
            param[3] = new SqlParameter("@Model", SqlDbType.NVarChar, 200);
            param[3].Value = AstModel;
            param[4] = new SqlParameter("@LOCCODE", SqlDbType.NVarChar, 200);
            param[4].Value = Location;
            param[5] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200);
            param[5].Value = Convert.ToString(Session["Uid"]);
            param[6] = new SqlParameter("@COMPANY", SqlDbType.NVarChar, 5);
            param[6].Value = ddlCompany.SelectedValue;

            DataSet ds = new DataSet();
            ds =(DataSet)ObjSubsonic.GetSubSonicDataSet("AM_AST_TOTAL_ASSETS_COUNT_MAPPED_DRL", param);
           
            ReportDataSource rds1 = new ReportDataSource();
            rds1.Name = "MappedDS";
            rds1.Value = ds.Tables[0];

            SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AM_AST_ASSET_TOTAL_COUNT_DRL");
            sp2.Command.Parameters.Add("@COMPANY", ddlCompany.SelectedValue, DbType.Int32);
            DataSet ds2 = new DataSet();
            ds2 = sp2.GetDataSet();
            ReportDataSource rds2 = new ReportDataSource();
            rds2.Name = "TotalAssetsDS";
            rds2.Value = ds2.Tables[0];

            SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AM_AST_IT_NONIT_TOTAL_COUNT_DRL");
            sp3.Command.Parameters.Add("@COMPANY", ddlCompany.SelectedValue, DbType.Int32);
            DataSet ds3 = new DataSet();
            ds3 = sp3.GetDataSet();
            ReportDataSource rds3 = new ReportDataSource();
            rds3.Name = "CapitalDS";
            rds3.Value = ds3.Tables[0];

            SubSonic.StoredProcedure sp4 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AM_AST_TOTAL_DISPOSE_REQUESTS");
            sp4.Command.Parameters.Add("@COMPANY", ddlCompany.SelectedValue, DbType.Int32);
            DataSet ds4 = new DataSet();
            ds4 = sp4.GetDataSet();
            ReportDataSource rds4 = new ReportDataSource();
            rds4.Name = "DisposeDS";
            rds4.Value = ds4.Tables[0];

            SubSonic.StoredProcedure sp5 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AM_AST_TOTAL_SURRENDER_REQUESTS_DRL");
            sp5.Command.Parameters.Add("@COMPANY", ddlCompany.SelectedValue, DbType.Int32);
            DataSet ds5 = new DataSet();
            ds5 = sp5.GetDataSet();
            ReportDataSource rds5 = new ReportDataSource();
            rds5.Name = "SurrenderDS";
            rds5.Value = ds5.Tables[0];

            SubSonic.StoredProcedure sp6 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AM_AST_TOTAL_INWARD_REQUESTS_DRL");
            sp6.Command.Parameters.Add("@COMPANY", ddlCompany.SelectedValue, DbType.Int32);
            DataSet ds6 = new DataSet();
            ds6 = sp6.GetDataSet();
            ReportDataSource rds6 = new ReportDataSource();
            rds6.Name = "InwardDS";
            rds6.Value = ds6.Tables[0];

            SubSonic.StoredProcedure sp7 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AM_AST_TOTAL_OUTWARD_REQUESTS_DRL");
            sp7.Command.Parameters.Add("@COMPANY", ddlCompany.SelectedValue, DbType.Int32);
            DataSet ds7 = new DataSet();
            ds7 = sp7.GetDataSet();
            ReportDataSource rds7 = new ReportDataSource();
            rds7.Name = "OutwardDS";
            rds7.Value = ds7.Tables[0];

            ReportViewer1.Reset();
            ReportViewer1.LocalReport.DataSources.Add(rds1);
            ReportViewer1.LocalReport.DataSources.Add(rds2);
            ReportViewer1.LocalReport.DataSources.Add(rds3);
            ReportViewer1.LocalReport.DataSources.Add(rds4);
            ReportViewer1.LocalReport.DataSources.Add(rds5);
            ReportViewer1.LocalReport.DataSources.Add(rds6);
            ReportViewer1.LocalReport.DataSources.Add(rds7);

            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/DrillDownReports/DDL_RDLC/AssetDRLReport.rdlc");
            ReportViewer1.SizeToReportContent = true;
            ReportViewer1.Visible = true;
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.LocalReport.EnableHyperlinks = true;
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }


    private void getassetcategory()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "USP_GET_ASSETCATEGORIESSALL");
        sp.Command.AddParameter("@dummy", 1, DbType.String);
        ddlAssetCategory.DataSource = sp.GetDataSet();
        ddlAssetCategory.DataTextField = "VT_TYPE";
        ddlAssetCategory.DataValueField = "VT_CODE";
        ddlAssetCategory.DataBind();
        ddlAssetCategory.Items.Insert(0, new ListItem("--ALL--", "ALL"));
    }
    private void getsubcategorybycat(string categorycode)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AM_USP_GET_ASSETSUBCATBYASSET");
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String);
        ddlAstSubCat.DataSource = sp.GetDataSet();
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME";
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE";
        ddlAstSubCat.DataBind();
        ddlAstSubCat.Items.Insert(0, new ListItem("--ALL--", "ALL"));
    }

    private void getbrandbycatsubcat(string category, string subcategory)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AM_AST_GET_MAKEBYCATSUBCAT");
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String);
        sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String);
        ddlAstBrand.DataSource = sp.GetDataSet();
        ddlAstBrand.DataTextField = "manufacturer";
        ddlAstBrand.DataValueField = "manufactuer_code";
        ddlAstBrand.DataBind();
        ddlAstBrand.Items.Insert(0, new ListItem("--ALL--", "ALL"));
        (ddlAstBrand as IPostBackDataHandler).RaisePostDataChangedEvent();
    }

    private void getmakebycatsubcat()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AM_AST_GET_MAKEBYCATSUBCATVEND");
        sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String);
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String);
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String);
        ddlModel.DataSource = sp.GetDataSet();
        ddlModel.DataTextField = "AST_MD_NAME";
        ddlModel.DataValueField = "AST_MD_CODE";
        ddlModel.DataBind();
        ddlModel.Items.Insert(0, new ListItem("--ALL--", "ALL"));

    }
    private void BindLocation()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "usp_getActiveLocation");
        sp.Command.AddParameter("@USER_ID", Session["Uid"], DbType.String);
        ddlLocation.DataSource = sp.GetReader();
        ddlLocation.DataTextField = "LCM_NAME";
        ddlLocation.DataValueField = "LCM_CODE";
        ddlLocation.DataBind();
        ddlLocation.Items.Insert(0, new ListItem("--All--", "ALL"));
    }

    protected void ddlAstBrand_SelectedIndexChanged(object sender, EventArgs e)
    {
        getmakebycatsubcat();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        LoadReport();
    }

    protected void ddlAssetCategory_SelectedIndexChanged1(object sender, EventArgs e)
    {
        ddlAstBrand.Items.Clear();
        ddlModel.Items.Clear();
        getsubcategorybycat(ddlAssetCategory.SelectedItem.Value);
        (ddlAstSubCat as IPostBackDataHandler).RaisePostDataChangedEvent();
    }

    protected void ddlAstSubCat_SelectedIndexChanged1(object sender, EventArgs e)
    {
        ddlModel.Items.Clear();
        getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value);
    }

    protected void btnSubmit_Click1(object sender, EventArgs e)
    {
        LoadReport();
    }
}