﻿<%@ Page Language="VB" MasterPageFile="~/KotakMasterPage.master" AutoEventWireup="false" CodeFile="frmDetailsKotak.aspx.vb" Inherits="WebFiles_frmDetailsKotak" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .ScrollStyle {
            max-height: 530px;
            overflow-y: scroll;
        }
        .al-main
{
        margin-left: -44px !important;
}
            .ScrollStyle::-webkit-scrollbar {
                width: 12px;
            }
            /* Track */
            .ScrollStyle::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(99,187,178,0.3);
                -webkit-border-radius: 10px;
                border-radius: 10px;
            }

            /* Handle */
            .ScrollStyle::-webkit-scrollbar-thumb {
                -webkit-border-radius: 10px;
                border-radius: 10px;
                background: rgba(32, 158, 145, 1);
                -webkit-box-shadow: inset 0 0 6px rgba(99,187,178,0.5);
            }

        /*.active1 {
            background-color: #209e91;
            border-right: 4px solid #209e91;
            color: #FFFFFF !important;
        }*/

        /*a:visited {
            color: #209e91;
            background-color: #209e91;
        }*/
    </style>
    <div class="row">
    <%--    <ba-sidebar id="mysidebar">
                <aside class="al-sidebar">
                     <nav class="sidebar-nav">
                            <ul id="menu" class="ScrollStyle">
                                <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                            </ul>
                        </nav>
                </aside>
        </ba-sidebar>--%>
        <a id="main">
        <div class="al-main">
            <div class="al-content" id="Mapdiv">
                <content-top></content-top>
                
            </div>
        </div>
           
        <%--<div class="al-footer">
            <div class="pull-left" style="padding-left: 45px">
                Other Links : 
                <a href="http://www.quickfms.com/" target="_blank">www.quickfms.com</a>
            </div>
            <div class="pull-right">
                <%Response.Write("&copy;" & Year(Now))%>&nbsp;<%Response.Write("QuickFMS v3.1")%>
            </div>
        </div>--%>
             </a>
        <%--<back-top></back-top>--%>
    </div>

    <script src="../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script defer src="../Scripts/Intercom/Intercom.js"></script>
    <script defer type="text/javascript">
        
         var table_body = "<iframe id='container' src=../../SMViews/Map/KotakUserMapFloorList.aspx width='100%' frameborder='0' height=1000px;></iframe>";
        $('#Mapdiv').append(table_body);

         function GetParameterValues(param) {
        var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return urlparam[1];
        }
    }
}
  

        //TO AVOID FADE IN EFFECT IN IE
        var ms_ie = false;
        var ua = window.navigator.userAgent;
        var old_ie = ua.indexOf('MSIE ');
        var new_ie = ua.indexOf('Trident/');
        var mozilla = ua.indexOf('Firefox/');

        if ((old_ie > -1) || (new_ie > -1)) {
            ms_ie = true;
        } else if (mozilla > -1) {
            mozilla = true;
        }

        function setFrameSource(src) {
            if (!(ms_ie || mozilla)) {
                $("#container").hide();
            }
            $("#container").attr("src", src);
            Intercom('update', { "Link": src });
            return false;
        }

        $(document).ready(function () {
            //$(window).on('blur', function () { $('.dropdown-toggle').parent().removeClass('open'); });            
            $('#container').css('height', $(window).height() - 100 + 'px');
            $('.sidebar').css('height', $(window).height() - 100 + 'px');
            $("#page-wrapper").css('min-height', $(window).height() + 'px');
            var viewportWidth = $(window).width();
                if (viewportWidth < 800) {
                    $('main').addClass('menu-collapsed');               
                    $('#time').css('display', 'none');                    
                    $('.ie8-pngcss').css({ 'height': 'auto', 'max-width': '115px' });
                    $('.ie9-pngcss').css({ 'height': 'auto', 'width': '30px' });
                    
                } else {
                    $("main").removeClass("menu-collapsed")
                    $('#time').css("display", "block");
                    $('.ie8-pngcss').css({ "height": "50px", "max-width": "none" });
                    $('.ie9-pngcss').css({ "height": "40px", "width": "40px" });
                    
                }
            //if (!ms_ie) {
            $("#container").load(function (e) {
                $("#container").fadeIn("slow");
            });
            //}
            $('.collapse-menu-link').on('click', function () {
                $('main').toggleClass('menu-collapsed');
            });

            $(window).resize(function () {
                var viewportWidth = $(window).width();
                if (viewportWidth < 800) {
                    $('main').addClass('menu-collapsed');                   
                    $('#time').css("display", "none");                    
                    $('.ie8-pngcss').css({ 'height': 'auto', 'max-width': '115px' });
                    $('.ie9-pngcss').css({ 'height': 'auto', 'width': '30px' });
                    
                }
                else {
                    $("main").removeClass("menu-collapsed")                   
                    $('#time').css("display", "block");
                    $('.ie8-pngcss').css({ "height": "50px", "max-width": "none" });
                    $('.ie9-pngcss').css({ "height": "40px", "width": "40px" });                  
                }
            });
        });
        $('#main').on('click', function () {
            $("main").removeClass("menu-collapsed")
        });

        window.intercomSettings = {
            app_id: "qy20hva4",
            name: '<%=Session("uid")%>', // Full name
            email: '<%=Session("uemail")%>', // Email address
            created_at: '<%=Session("LoginTime")%>',// Signup date as a Unix timestamp
            "Link": 'frmDetails.aspx',
            tenant_id: '<%=Session("TENANT")%>'
        };
        $(document).ready(function () {
            //$('a').click(function () {
            //    $('a').removeClass("active1");
            //    $(this).addClass("active1");
            //});
        });

    </script>
    <script defer>
        function close() {
            document.getElementById("mysidebar").style.display = "none";
        }
        </script>
</asp:Content>

