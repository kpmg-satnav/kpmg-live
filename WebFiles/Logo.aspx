﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Logo.aspx.vb" Inherits="FAM_FAM_Webfiles_Logo" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Upload Image" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" runat="server">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label class="control-label">Select Image<span style="color: red;"> *</span></label>
                                    <asp:RequiredFieldValidator ID="rfvdoc" runat="server" Display="None" ErrorMessage="Please Select Image"
                                        ControlToValidate="fpBrowseDoc" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <div class="col-sm-4">
                                        <i class="fa fa-folder-open-o fa-lg"></i>
                                        <asp:FileUpload ID="fpBrowseDoc" runat="Server" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color pull-right" Text="Submit" ValidationGroup="Val1" />
                                    <asp:Label ID="lblMsg" runat="server" CssClass="control-label" ForeColor="Red"></asp:Label>
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger pull-left" Style="top: 15px; position: relative; color: red" ShowSummary="true" ValidationGroup="Val1" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer type="text/javascript">
        $("#btnSubmit").click(function (e) {
            $("#lblMsg").text(' ');
        })

        $("#fpBrowseDoc").click(function (e) {
            $("#lblMsg").text(' ');
        })

    </script>


</body>
</html>
