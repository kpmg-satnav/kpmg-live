﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMyProfile.aspx.vb" Inherits="WebFiles_frmMyProfile" %>

<html>
<head>
    <title>My Profile</title>
    <link href="../BlurScripts/BlurCss/MyProfileStyler.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-default " runat="server" id="divspace" visible="false">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Today Seat Booking                            
                                <i class="fa fa-bell pull-right" data-content="<%=popupVal%>" id="spacePopOver" data-container="body" data-placement="right" data-trigger="hover"></i></a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body color">
                            <div class="row">
                                <div class="col-md-5">
                                    <i class="fa fa-wheelchair"></i><b><i>&nbsp Allocated Space Id:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:HyperLink
                                        runat="server"
                                        ID="hlDividents"
                                        >
                                    </asp:HyperLink>
                                    <%--<asp:HiddenField ID="twr_code" runat="server" />--%>
                                   
                                   <%-- <asp:Label runat="server" ID="lblSpaceId"></asp:Label>--%>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <i class="fa fa-map-marker"></i><b><i>&nbsp Location:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblLocation"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <i class="fa fa-building"></i><b><i>&nbsp Floor:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblfloor"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <i class="fa fa-cube" ></i><b><i>&nbsp Vertical:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblvertical"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <i class="fa fa-book" ></i><b><i>&nbsp Cost Center:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblcostcenter"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <i class="fa fa-calendar"></i><b><i>&nbsp From Date:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblFromDate"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <i class="fa fa-calendar"></i><b><i>&nbsp To Date:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblToDate"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <i class="fa fa-clock-o"></i><b><i>&nbsp Shift Timings:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblshifttime"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <i class="fa fa-phone"></i><b><i>&nbsp Extension Number:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblextension"></asp:Label>
                                </div>
                            </div>
                            <div class="row" hidden>
                                <div class="col-md-5">
                                    <b><i> Asset Details:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblAsset" ></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" runat="server" id="propertydiv" visible="false">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">My Property Management<i class="fa fa-bell pull-right" id="propertyPopOver" data-container="body" data-placement="right" data-trigger="hover" data-content="<%=propertypopup%>"></i></a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body color">
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Property Types:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="lblpt"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>No of Properties:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="lblpptscount"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Lease Properties:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="lblleasecount"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Tenant Properties:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="lbltenantcount"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Properties Under Lease & Tenant:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="lblLseTnt"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" runat="server" id="assetdiv" visible="false">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">My Asset Management<i class="fa fa-bell pull-right" id="assetPopOver" data-container="body" data-placement="right" data-trigger="hover"
                                data-content="<%=Assetpopup%>"></i></a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body color">
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>No of Assets Mapped(Capital):</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="lblastcount"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" runat="server" id="maintenancediv" visible="false">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">My Maintenance Management<i class="fa fa-bell pull-right" id="maintenancePopOver" data-container="body" data-placement="right" data-trigger="hover"
                                data-content="<%=maintenancepopup%>"></i></a>
                        </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse">
                        <div class="panel-body color">
                            <div class="row">
                                <div class="col-md-5">
                                    <b><i>Assets under AMC:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblamcassets"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" runat="server" id="helpdeskdiv" visible="false">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">My Helpdesk Management<i class="fa fa-bell pull-right" id="helpDeskPopOver" data-container="body" data-placement="right" data-trigger="hover"
                                data-content="<%=HelpDeskpopup%>"></i></a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body color">
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Total Requests Raised: </i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="lblreqraised"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Total Requests In-progress:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="lblinprogreq"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Total Requests On Hold:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="lblonhold"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Total Requests Closed:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="lblclosed"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Total Requests Rejected:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="lblrejected"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default" runat="server" id="gh" visible="false">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">My Helpdesk Management<i class="fa fa-bell pull-right" id="I1" data-container="body" data-placement="right" data-trigger="hover"
                                data-content="<%=HelpDeskpopup%>"></i></a>
                        </h4>
                    </div>
                    <div id="Div2" class="panel-collapse collapse">
                        <div class="panel-body color">
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Total Requests Raised: </i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="Label3"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Total Requests In-progress:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="Label4"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Total Requests On Hold:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="Label5"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Total Requests Closed:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="Label6"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Total Requests Rejected:</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="Label7"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default" runat="server" id="conferencediv" visible="false">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">My Reservation Management<i class="fa fa-bell pull-right" id="conferencePopOver" data-container="body" data-placement="right" data-trigger="hover"
                                data-content="<%=Conferencepopup%>"></i></a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body color">
                            <div class="row">
                                <div class="col-md-5">
                                    <b><i>Reservation Name:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblconfname"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <b><i>Location:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblconfloc"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <b><i>Floor:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblconsfloor"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <b><i>Booked Date:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblbkdate"></asp:Label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <b><i>Booked Time:</i></b>
                                </div>
                                <div class="col-md-7">
                                    <asp:Label runat="server" ID="lblbktime"></asp:Label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default" runat="server" id="Businessid" visible="false">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight">My Business Card Management<i class="fa fa-bell pull-right" id="BusinessPop" data-container="body" data-placement="right" data-trigger="hover"
                                data-content="<%=BusinessPop%>"></i></a>
                        </h4>
                    </div>
                    <div id="collapseEight" class="panel-collapse collapse">
                        <div class="panel-body color">
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Total No.of Requests</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="Label1"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7">
                                    <b><i>Total Closed Requests</i></b>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label runat="server" ID="Label2"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-7">
                <div class="profile">
                    <div class="col-sm-12">
                        <div class="col-xs-12 col-sm-8">
                            <h2><i class="fa fa-user" aria-hidden="true"></i>
                                <asp:Label ID="lblempname" runat="server"></asp:Label></h2>
                            <br />
                            <h5><i class="fa fa-credit-card" aria-hidden="true"></i>&nbsp;&nbsp;User ID:&nbsp;<asp:Label ID="lblemployeeID" runat="server"></asp:Label></h5>
                            <br />
                            <h5><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;Email:&nbsp;<asp:Label ID="lblemail" runat="server"></asp:Label></h5>
                            <br />
                            <h5><i class="fa fa-tags" aria-hidden="true"></i>&nbsp;&nbsp;Designation: 
                                <asp:Label ID="lbldesig" runat="server"></asp:Label></h5>
                            <br />
                            
                                <h5>
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                &nbsp;&nbsp;Department:&nbsp;<asp:Label ID="lbldept" runat="server" Visible="false"></asp:Label>
                            </h5>
                            <br />
                            <h5><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Reporting To:&nbsp;<asp:Label ID="lblreporting" runat="server"></asp:Label></h5>
                            <br />
                                <h5>
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                &nbsp;&nbsp;SkipLevel Reporting To:&nbsp;<asp:Label ID="lblskiplevel" runat="server" Visible="false"></asp:Label>
                            </h5>

                        </div>
                        <div runat="server" class="col-xs-12 col-md-4 text-center">
                            <a href="#">
                                <asp:Image ID="img" runat="server" ToolTip="Click to change profile picture" ImageUrl="../Userprofiles/default-user-icon-profile.jpg"
                                    Height="120px" Width="120px" AlternateText="" CssClass="img-circle img-responsive" />
                                <input type="file" id="fup" name="fileUp" class="hidden" accept="image/*">
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>

    <script defer>
        $(function () {
            //$("#spacePopOver").popover({ title: 'Twitter Bootstrap Popover', content: "It's so simple to create a tooltop for my website!" });
            $("#spacePopOver").popover();
            $("#propertyPopOver").popover();
            $("#assetPopOver").popover();
            $("#conferencePopOver").popover();
            $("#maintenancePopOver").popover();
            $("#helpDeskPopOver").popover();
            $("#BusinessPop").popover();
        });

        $("#img").click(function () {
            var ofd = document.getElementById("fup");
            ofd.click();
            return false;
        });
        $(":file").change(function (e) {
            var fileUpload = $("#fup").get(0);
            var files = fileUpload.files;
            var data = new FormData();
            for (var i = 0; i < files.length; i++) {
                data.append(files[i].name, files[i]);
                var filetype = files[i].type;
            }
            if (filetype == "image/jpeg" || filetype == "image/jpg" || filetype == "image/png") {
                $.ajax({
                    url: "../api/MyprofileAPI/SaveImagetoDB",
                    type: "POST",
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (result) { $.notify(result, { color: '#fff', background: '#20D67B', close: true, icon: "check" }); },
                    error: function (err) {
                        $.notify('Failed to upload please check again !', { color: "#fff", background: "#D44950", close: true });
                    }
                });
                e.preventDefault();
                $("#img").attr("src", "../Userprofiles/" + files[0].name);
            }
            else {
                alert('Please Select JPG/JPEG/PNG Image File');

            }
        });

        $(document).ready(function () {
            getModulesByUserID();
        });

        function getModulesByUserID() {
            $.ajax({
                url: '../api/AdminfunctionsAPI/GetModulesByUserID',
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "get",
                dataType: 'json',
                success: function (data) {

                    for (var i = 0; i < data.length; i++) {

                        if (data[i].CLS_ID == "108") {
                            $("#assetdiv").removeAttr("style");
                        }
                        else if (data[i].CLS_ID == "195") {
                            $("#conferencediv").removeAttr("style");
                        }
                        else if (data[i].CLS_ID == "234") {
                            $("#helpdeskdiv").removeAttr("style");
                        }
                        else if (data[i].CLS_ID == "218") {
                            $("#maintenancediv").removeAttr("style");
                        }
                        else if (data[i].CLS_ID == "62" || data[i].CLS_ID == "83") {
                            $("#propertydiv").removeAttr("style");
                        }
                        else if (data[i].CLS_ID == "248") {
                            $("#divspace").removeAttr("style");
                        }
                        else if (data[i].CLS_ID == "1403") {
                            $("#Businessid").removeAttr("style");
                        }





                    }



                }
            });
        }

    </script>
</body>
</html>
