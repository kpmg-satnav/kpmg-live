﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.IO
Partial Class WebFiles_frmDetailsKotak
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If

        If Not IsPostBack Then
            If isMobileBrowser() = True Then
                Dim script As String = String.Format("alert('{0}');", "hello")
                Page.ClientScript.RegisterClientScriptBlock(Page.[GetType](), "alert", script, True)
                Response.Redirect("~/frmDetails.aspx?id=" + Session("UID") + "&CompanyId=" + Session("TENANT"))
            Else
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_MENU_SCRIPT1_BOOTSTRAP")
                sp.Command.AddParameter("@AURID", Session("uid").ToString(), Data.DbType.String)
                'litMenu.Text = sp.ExecuteScalar
            End If

        End If
    End Sub
    Public Function isMobileBrowser() As Boolean
        Dim context As HttpContext = HttpContext.Current

        If context.Request.Browser.IsMobileDevice Then
            Return True
        End If

        If context.Request.ServerVariables("HTTP_X_WAP_PROFILE") IsNot Nothing Then
            Return True
        End If

        If context.Request.ServerVariables("HTTP_ACCEPT") IsNot Nothing AndAlso context.Request.ServerVariables("HTTP_ACCEPT").ToLower().Contains("wap") Then
            Return True
        End If

        If context.Request.ServerVariables("HTTP_USER_AGENT") IsNot Nothing Then
            Dim mobiles As String() = {"midp", "j2me", "avant", "docomo", "novarra", "palmos", "palmsource", "240x320", "opwv", "chtml", "pda", "windows ce", "mmp/", "blackberry", "mib/", "symbian", "wireless", "nokia", "hand", "mobi", "phone", "cdm", "up.b", "audio", "SIE-", "SEC-", "samsung", "HTC", "mot-", "mitsu", "sagem", "sony", "alcatel", "lg", "eric", "vx", "NEC", "philips", "mmm", "xx", "panasonic", "sharp", "wap", "sch", "rover", "pocket", "benq", "java", "pt", "pg", "vox", "amoi", "bird", "compal", "kg", "voda", "sany", "kdd", "dbt", "sendo", "sgh", "gradi", "jb", "dddi", "moto", "iphone"}

            For Each s As String In mobiles

                If context.Request.ServerVariables("HTTP_USER_AGENT").ToLower().Contains(s.ToLower()) Then
                    Return True
                End If
            Next
        End If

        Return False
    End Function
End Class
