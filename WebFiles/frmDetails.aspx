<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmDetails.aspx.vb" Inherits="WebFiles_frmDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        #menu {
            max-height: 530px;
            overflow-y: scroll;
        }

            #menu::-webkit-scrollbar {
                width: 7px;
            }
            /* Track */
            #menu::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(11,28,55,1);
                -webkit-border-radius: 5px;
                border-radius: 5px;
            }

            /* Handle */
            #menu::-webkit-scrollbar-thumb {
                -webkit-border-radius: 5px;
                border-radius: 5px;
                background: rgba(32, 158, 145, 1);
                -webkit-box-shadow: inset 0 0 6px rgba(11,28,55,1);
            }

        /*.active1 {
            background-color: #209e91;
            border-right: 4px solid #209e91;
            color: #FFFFFF !important;
        }*/

        /*a:visited {
            color: #209e91;
            background-color: #209e91;
        }*/
    </style>
    <%--<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>--%>
    <script src="assets/jquery/Jquery-3.3.1.js"></script>
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--%>
    <link href="BootStrapCSS/Scripts/bootstrap4.0.css" rel="stylesheet" />
    <%--<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>--%>
    <script src="assets/js/gijgo1.9.13.js"></script>
    <%--<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />--%>
    <link href="BootStrapCSS/font-awesome/css/gijgo-1.9.13.css" rel="stylesheet" />
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--%>
    <link href="BootStrapCSS/font-awesome/css/font-awesome4.7.2.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/skincolors.css">
    <div class="page-wrapper chiller-theme toggled">
        <div class="SideNavPanel">
            <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
                <i class="fa fa-arrow-right"></i>
            </a>
            <nav id="sidebar" class="sidebar-wrapper">
                <div class="sidebar-content">
                    <div class="sidebar-brand">
                        <div id="close-sidebar">
                            <i class="fa fa-bars"></i>
                        </div>
                        <div class="brand-logo" hidden="false">
                            <a href="frmDetails.aspx">
                                <img src="../BootStrapCSS/images/MySeatlogo.png" alt=""></a>
                        </div>

                    </div>
                    <div class="sidebar-menu">
                        <ul id="menu" class="side-menu-list-container">
                            <div class="side-menu-list">
                                <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                            </div>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        <main class="page-content">
            <div class="container-fluid page-content-inner">
                <iframe id="container" src="../Dashboard/DashboardV2.aspx" width="100%" frameborder="0"></iframe>
            </div>
        </main>
        <div id="footer" class="login-page-footer-text" width="100%">
            <%--<div class="float-left">
                Other Links : 
                <a href="http://www.quickfms.com/" target="_blank">www.quickfms.com</a>
            </div>
            <div class="float-right"><%Response.Write("&copy;" & Year(Now))%>&nbsp;<%Response.Write("QuickFMS v3.2")%></div>--%>
        </div>

    </div>

    <script src="../BootStrapCSS/Scripts/jquery.min.js"></script>

    <script src="../Scripts/Intercom/Intercom.js"></script>
    <noscript>Your browser does not support JavaScript!</noscript>
    <script defer type="text/javascript">



        //TO AVOID FADE IN EFFECT IN IE
        var ms_ie = false;
        var ua = window.navigator.userAgent;
        var old_ie = ua.indexOf('MSIE ');
        var new_ie = ua.indexOf('Trident/');
        var mozilla = ua.indexOf('Firefox/');

        if ((old_ie > -1) || (new_ie > -1)) {
            ms_ie = true;
        } else if (mozilla > -1) {
            mozilla = true;
        }

        function setFrameSource(src) {
            if (!(ms_ie || mozilla)) {
                $("#container").hide();
            }
            $("#container").attr("src", src);
            Intercom('update', { "Link": src });

            if (src == "/logout.aspx") {
                window.location.href = "/logout.aspx";
            }
            return false;
        }
        $(document).ready(function () {
            //$(window).on('blur', function () { $('.dropdown-toggle').parent().removeClass('open'); });            
            $('#container').css('height', $(window).height() - 100 + 'px');
            $('.sidebar').css('height', $(window).height() - 100 + 'px');
            $("#page-wrapper").css('min-height', $(window).height() + 'px');
            var viewportWidth = $(window).width();
            if (viewportWidth < 800) {
                $('main').addClass('menu-collapsed');
                $('#time').css('display', 'none');
                $('.ie8-pngcss').css({ 'height': 'auto', 'max-width': '115px' });
                $('.ie9-pngcss').css({ 'height': 'auto', 'width': '30px' });

            } else {
                $("main").removeClass("menu-collapsed")
                $('#time').css("display", "block");
                $('.ie8-pngcss').css({ "height": "50px", "max-width": "none" });
                $('.ie9-pngcss').css({ "height": "40px", "width": "40px" });

            }
            //if (!ms_ie) {
            $("#container").load(function (e) {
                $("#container").fadeIn("slow");
            });
            //}
            $('.collapse-menu-link').on('click', function () {
                $('main').toggleClass('menu-collapsed');
            });

            $(window).resize(function () {
                var viewportWidth = $(window).width();
                if (viewportWidth < 800) {
                    $('main').addClass('menu-collapsed');
                    $('#time').css("display", "none");
                    $('.ie8-pngcss').css({ 'height': 'auto', 'max-width': '115px' });
                    $('.ie9-pngcss').css({ 'height': 'auto', 'width': '30px' });

                }
                else {
                    $("main").removeClass("menu-collapsed")
                    $('#time').css("display", "block");
                    $('.ie8-pngcss').css({ "height": "50px", "max-width": "none" });
                    $('.ie9-pngcss').css({ "height": "40px", "width": "40px" });
                }
            });
                var div = document.getElementById("footer");
                if (window.innerWidth <= 780) {
                   div.style.display = "none";
                }
                
                
        
        });
        $('#main').on('click', function () {
            $("main").removeClass("menu-collapsed")
        });

        window.intercomSettings = {
            app_id: "w7eauak2",
            name: '<%=Session("uid")%>', // Full name
            email: '<%=Session("uemail")%>', // Email address
            created_at: '<%=Session("LoginTime")%>',// Signup date as a Unix timestamp
            "Link": 'frmDetails.aspx',
            tenant_id: '<%=Session("TENANT")%>'
        };
        $(document).ready(function () {
            //$('a').click(function () {
            //    $('a').removeClass("active1");
            //    $(this).addClass("active1");
            //});
        });

    </script>
    <script type="text/javascript">
        var rumMOKey = 'c006b44a81a882a219b0b33dd470ce03';
        (function () {
            if (window.performance && window.performance.timing && window.performance.navigation) {
                var site24x7_rum_beacon = document.createElement('script');
                site24x7_rum_beacon.async = true;
                site24x7_rum_beacon.setAttribute('src', '//static.site24x7rum.com/beacon/site24x7rum-min.js?appKey=' + rumMOKey);
                document.getElementsByTagName('head')[0].appendChild(site24x7_rum_beacon);
            }
        })(window)
    </script>
    <script>
        function close() {
            document.getElementById("mysidebar").style.display = "none";
        }
    </script>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>--%>
    <script src="assets/js/bootstrap-4.3.1.js"></script>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>--%>
    <script src="assets/js/cdnjspopper-1.14.7.js"></script>
    <script src="../assets/js/scripts.js"></script>
    <script src="../assets/js/tooltip-hover-scripts.js"></script>
    <script src="../assets/js/timepicki.js"></script>
    <script src="../assets/jquery/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.bundle.min.js"></script>

</asp:Content>
