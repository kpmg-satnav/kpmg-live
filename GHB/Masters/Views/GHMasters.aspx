﻿<%@ Page Language="C#" %>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-5b02d1ea3b.css" rel="stylesheet" />



    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <%--  <style>
        .btn {
            border-radius: 4px;
            background-color: #3A618F;
        }
    </style>--%>
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title=" Masters" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 id="Title" class="panel-title panel-heading-qfms-title"></h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <div class="box-body">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-12 col-xs-12 ">

                                    <asp:HyperLink ID="HyperLink4" runat="server" class="btn btn-block btn-primary" role="button" NavigateUrl="~/GHB/Masters/Views/ReservationType.aspx">Add Facility Type</asp:HyperLink>

                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">

                                    <asp:HyperLink ID="HyperLink11" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/GHB/Masters/Views/AddFacility.aspx">Add Facility & Room</asp:HyperLink>

                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/GHB/Masters/Views/GHBusinessSpecific.aspx">Business Specific</asp:HyperLink>
                                </div>
                            </div>
                            <br />
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript" defer>
        var GHT = '<%= Session["GHT"]%>';
        $(document).ready(function () {
            $("#Title").append(GHT).append(" Masters");
        });

    </script>
</body>
</html>

