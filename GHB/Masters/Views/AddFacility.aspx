﻿<%@ Page Language="VB" AutoEventWireup="true" %>

<%--CodeFile="~/GHB/Masters/Views/AddFacility.aspx.vb" Inherits="GHB_Masters_Views_AddFacility"--%>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'>   
    <![endif]-->


    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

    </script>
</head>
<body data-ng-controller="AddFacilityController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Add Facility & Room" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Add Facility & Room </h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form role="form" id="frm" name="frm" data-valid-submit="UploadFile()" novalidate>
                        <span ng-click="Add()" data-ng-show="ShowAddFacilityGrid" class="btn btn-primary   custom-button-color glyphicon glyphicon-plus"></span>
                        <br />
                        <div data-ng-show="ShowAddFacility">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.RT_SNO.$invalid}">
                                        <label for="txtcode">Facility Type <span style="color: red;">*</span></label>
                                        <select id="Select3" name="RT_SNO" class="form-control selectpicker" data-live-search="true" data-ng-model="ReservationRoom.RT_SNO"
                                            required>
                                            <option id="Option3" value=''>--Select--</option>
                                            <option data-ng-repeat="type in RTlst" value="{{type.RT_SNO}}">{{type.RT_NAME}}</option>
                                        </select>
                                        <span class="error" data-ng-show="frm.$submitted && frm.RT_SNO.$invalid" style="color: red">Please select Facility Type </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.RF_NAME.$invalid}">
                                        <label for="txtcode">Facility Name <span style="color: red;">*</span></label>
                                        <input type="text" id="RF_NAME" name="RF_NAME" data-ng-model="ReservationRoom.RF_NAME" class="form-control" required="" />
                                        <span id="Span2" class="error" data-ng-show="frm.$submitted && frm.RF_NAME.$invalid" style="color: red">Please enter Facility Name </span>
                                    </div>

                                </div>


                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.RF_EMAIL.$invalid}">
                                        <label for="txtcode">Email <span style="color: red;">*</span></label>
                                        <input type="text" id="RF_EMAIL" name="RF_EMAIL" data-ng-model="ReservationRoom.RF_EMAIL" class="form-control" required="" />
                                        <span id="Span1" class="error" data-ng-show="frm.$submitted && frm.RF_EMAIL.$invalid" style="color: red">Please enter valid Email Id</span>

                                    </div>

                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.RF_CNY_CODE.$invalid}">
                                        <label for="txtcode">Country <span style="color: red;">*</span></label>
                                        <select id="Select1" name="RF_CNY_CODE" class="form-control selectpicker" data-live-search="true" data-ng-model="ReservationRoom.RF_CNY_CODE"
                                            required
                                            ng-change="CountryChanged()">
                                            <option id="Option1" value=''>--Select--</option>
                                            <option data-ng-repeat="cny in Cnylst" value="{{cny.CNY_CODE}}">{{cny.CNY_NAME}}</option>
                                        </select>
                                        <span class="error" data-ng-show="frm.$submitted && frm.RF_CNY_CODE.$invalid" style="color: red">Please select Country </span>
                                    </div>
                                </div>

                            </div>


                            <div class="clearfix">

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.RF_CTY_CODE.$invalid}">
                                        <label for="txtcode">City <span style="color: red;">*</span></label>
                                        <select id="ddlMain" name="RF_CTY_CODE" data-ng-model="ReservationRoom.RF_CTY_CODE"
                                            class="form-control selectpicker" data-live-search="true"
                                            required
                                            ng-change="CityChanged()">
                                            <option id="ddlsub" value=''>--Select--</option>
                                            <option data-ng-repeat="city in Citylst" value="{{city.CTY_CODE}}">{{city.CTY_NAME}}</option>
                                        </select>
                                        <span class="error" data-ng-show="frm.$submitted && frm.RF_CTY_CODE.$invalid" style="color: red">Please select City </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.RF_LOC_CODE.$invalid}">
                                        <label for="txtcode">Location <span style="color: red;">*</span></label>
                                        <select id="Select2" name="RF_LOC_CODE" data-ng-model="ReservationRoom.RF_LOC_CODE"
                                            class="form-control selectpicker" data-live-search="true"
                                            required>
                                            <option id="Option2" value=''>--Select--</option>
                                            <option data-ng-repeat="loc in Locationlst" value="{{loc.LCM_CODE}}">{{loc.LCM_NAME}}</option>
                                        </select>
                                        <span class="error" data-ng-show="frm.$submitted && frm.RF_LOC_CODE.$invalid" style="color: red">Please select Location </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Latitude </label>
                                        <input id="Text9" name="RF_LAT" type="text" data-ng-model="ReservationRoom.RF_LAT" class="form-control" />
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Longitude </label>
                                        <input id="Text10" name="RF_LONG" type="text" data-ng-model="ReservationRoom.RF_LONG" class="form-control" />
                                    </div>
                                </div>

                            </div>

                            <div class="clearfix">

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Charges Per Room Per Night </label>
                                        <input type="number" id="txtCost" name="RF_COST" data-ng-model="ReservationRoom.RF_COST" class="form-control" />
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Remarks </label>
                                        <textarea id="Textarea1" name="RF_REMARKS" data-ng-model="ReservationRoom.RF_REMARKS" class="form-control"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.RF_NO_OF_ROOMS.$invalid}">
                                        <label for="txtcode">No. Of Rooms </label>
                                        <input id="RF_NO_OF_ROOMS" ng-change="renderTable()" name="RF_NO_OF_ROOMS" type="text" readonly data-ng-model="ReservationRoom.RF_NO_OF_ROOMS" maxlength="15" required="" class="form-control" />
                                        <%--<label id="RF_NO_OF_ROOMS" ng-change="renderTable()" name="RF_NO_OF_ROOMS"  data-ng-model="ReservationRoom.RF_NO_OF_ROOMS" class="form-control" ></label>--%>
                                        <span class="error" data-ng-show="frm.$submitted && frm.RF_NO_OF_ROOMS.$invalid" style="color: red">Please Enter No. Of Rooms </span>
                                    </div>

                                </div>


                                <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="ActionStatus==1">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.RF_STATUS.$invalid}">
                                        <label for="txtcode">Status <span style="color: red;">*</span></label>
                                        <select id="staid" name="status" data-ng-model="ReservationRoom.RF_STATUS" class="form-control selectpicker" data-live-search="true">
                                            <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Upload File </label>
                                    </div>
                                    <label class="custom-file">
                                        <input type="file" name="UPLFILE" id="FileUpl" required="" class="custom-file-input">
                                        <span class="custom-file-control"></span>
                                    </label>
                                    <a id="downloadfile" href="../../../GHB/UploadImages/{{ReservationRoom.RF_FILE_NAME}}" ng-show="ShowDownloadFile==1" target="_blank">Download File</a>
                                </div>
                            </div>

                            <div class="clearfix" data-ng-show="ShowTable==1">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <legend>Add Room</legend>
                                </div>

                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="display: none"></th>
                                            <th>S.No.</th>
                                            <th>Room Number/Name <span style="color: red;">*</span></th>
                                            <th>Room Capacity <span style="color: red;">*</span></th>
                                            <th>Other Facilities</th>

                                            <th ng-show="ShowIsActive==1">Is Active</th>
                                            <th>Add / Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="room in ReservationRoom.addroom">
                                            <td style="display: none">
                                                <input type="hidden" ng-model="room.RR_SNO" /></td>
                                            <td>{{$index+1}}</td>
                                            <td>
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.RR_NAME.$invalid}">
                                                    <input type="text" name="RR_NAME" ng-model="room.RR_NAME" class="form-control" ng-model-onblur required ng-change="FindDuplicate($index)" />
                                                    <span class="error" data-ng-show="frm.$submitted && frm.RR_NAME.$invalid" style="color: red">Please Enter Room Name </span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.RR_CAPCITY.$invalid}">
                                                    <input type="number" name="RR_CAPCITY" ng-model="room.RR_CAPCITY" class="form-control" required />
                                                    <span class="error" data-ng-show="frm.$submitted && frm.RR_CAPCITY.$invalid" style="color: red">Please Enter Capacity</span>
                                                </div>
                                                <td>
                                                    <input type="text" class="form-control" ng-model="room.RR_FACILITIES" /></td>

                                            <td ng-show="ShowIsActive==1">
                                                <label class="btn btn-default">
                                                    <input type="checkbox" ng-model="room.IsActive" ng-true-value="1" ng-false-value="0"
                                                        autocomplete="off" /></td>

                                            </label>
                                                   
                                                <td>
                                                    <span ng-click="addNew($index)" class="btn btn-info btn-sm glyphicon glyphicon-plus"></span>
                                                    <span ng-click="remove($index)" class="btn btn-danger btn-sm   glyphicon glyphicon-minus"></span>
                                                </td>

                                        </tr>
                                    </tbody>
                                </table>



                            </div>

                            <br />
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                    <input type="reset" value="Clear" class='btn btn-primary custom-button-color' data-ng-click="EraseData()" />
                                    <a class='btn btn-primary custom-button-color' data-ng-click="Close()">Close</a>
                                    <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>

                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row" data-ng-show="ShowAddFacilityGrid">

                            <div class="col-md-8 col-sm-12 col-xs-12">

                                <input id="filtertxt" placeholder="Filter..." type="text" style="width: 26%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 329px; width: 150%"></div>

                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <%--<script src="../../../SMViews/Utility.js"></script>--%>
    <%--<script src="../Js/AddFacility.js"></script>--%>
    <script src="../Js/AddFacility.min.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
</body>
</html>
