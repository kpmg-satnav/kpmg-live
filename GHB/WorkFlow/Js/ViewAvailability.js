﻿app.service("ViewAvailabilityService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (vabData) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewAvailability/GetViewAvailabilityDetails', vabData)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


});
app.controller('ViewAvailabilityController', function ($scope, $q, $http, ViewAvailabilityService, UtilityService, $timeout, $filter) {
    $scope.Customized = {};
    $scope.Request_Type = [];
    $scope.rtlist = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.BsmDet = { Parent: "", Child: "" };
    $scope.RFlst = [];

    //UtilityService.getBussHeirarchy().then(function (response) {
    //    if (response.data != null) {
    //        $scope.BsmDet = response.data;
    //        $scope.Cols[7].COL = $scope.BsmDet.Parent;
    //        $scope.Cols[8].COL = $scope.BsmDet.Child;
    //    }
    //});

    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getReservationTypes(2).then(function (response) {
            $scope.RTlst = response.data;
            angular.forEach($scope.RTlst, function (value, key) {
                value.ticked = true;
            })
            angular.forEach($scope.RTlst, function (value, key) {
                var cny = _.find($scope.RTlst, { RT_SNO: value.RT_SNO });
                if (cny != undefined && value.ticked == true) {
                    cny.ticked = true;
                    $scope.Customized.RT_SNO = cny;
                }
            });
        }, function (error) {
            console.log(error);
        });
        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Locations = response.data;
                angular.forEach($scope.Locations, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.GetFacilityNamesbyType(1, $scope.RTlst).then(function (response) {
            $scope.RFlst = response.data;
            angular.forEach($scope.RFlst, function (value, key) {
                value.ticked = true;
            })

        }, function (error) {
            console.log(error);
        });
    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Customized.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.Customized.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customized.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.Customized.City = $scope.City;
        $scope.getLocationsByCity();
    }

  
  

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.Customized.Locations, 2).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.Customized.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.Customized.Towers, 1).then(function (response) {
            if (response.data != null)
                $scope.Floors = response.data;
            else
                $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });
    }

  
    $scope.LocationChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Customized.Locations[0] = lcm;
            }
        });

        //angular.forEach($scope.Floors, function (value, key) {
        //    var twr = _.find($scope.Towers, { TWR_CODE: value.TWR_CODE });
        //    if (twr != undefined && value.ticked == true) {
        //        twr.ticked = true;
        //        $scope.Customized.Towers[0] = twr;
        //    }
        //});

    }
  
    $scope.LocationSelectNone = function () {
        $scope.Locationlst = [];
        $scope.LocationChange();
    }

  

    $scope.FTSelectAll = function () {
        //$scope.Customized.RTlst = $scope.RFlst;
        //angular.forEach($scope.RFlst, function (value, key) {
        //    value.ticked = false;
        //});
        $scope.FTChange();
    }

    $scope.FTChange = function () {

        angular.forEach($scope.RFlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.RTlst, function (value, key) {
            var cny = _.find($scope.RFlst, { RT_SNO: value.RT_SNO });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.RT_SNO[0] = cny;
            }
        });

        //UtilityService.GetFacilityNamesbyType(1, $scope.RTlst).then(function (response) {
        //    $scope.RFlst = response.data;
        //    angular.forEach($scope.RFlst, function (value, key) {
        //        value.ticked = true;
        //    })

        //}, function (error) {
        //    console.log(error);
        //});


    }

    $scope.getFacilityNamesbyType = function () {
        console.log($scope.RTlst);
        UtilityService.GetFacilityNamesbyType(0, $scope.RTlst).then(function (response) {
            console.log(response.data);
            $scope.RFlst = response.data;
           
        }, function (error) {
            console.log(error);
        });
        
    }

    //$scope.FTChange = function () {



    //    $scope.getFacilityNamebyType();

    //}
    $scope.FTSelectNone = function () {
        $scope.RFlst = [];
        //  $scope.FTChange();
    }


    // fecility name change multiselect

    $scope.FNSelectAll = function () {
        $scope.Customized.RTlst = $scope.RFlst;


        angular.forEach($scope.RFlst, function (value, key) {
            var cny = _.find($scope.RTlst, { RT_SNO: value.RT_SNO });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.RT_SNO[0] = cny;
            }
        });

        //angular.forEach($scope.RTlst, function (value, key) {
        //    value.ticked = true;
        //});
    }
    $scope.FNChange = function () {
        angular.forEach($scope.RTlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.RFlst, function (value, key) {
            var cny = _.find($scope.RTlst, { RT_SNO: value.RT_SNO });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.RT_SNO = cny;
            }
        });
    }
    $scope.FNSelectNone = function () {
        //$scope.RTlst = [];
        $scope.FNChange();
    }



   
    $scope.columnDefs = [
                            
             { headerName: "Facility Type", field: "RT_NAME", cellClass: 'grid-align', width: 150 },
             { headerName: "Facility Name", field: "RF_NAME", cellClass: 'grid-align', width: 150 },
             { headerName: "Room Name/Number", field: "RR_NAME", cellClass: 'grid-align', width: 150 },
             { headerName: "Country", field: "COUNTRY", width: 100, cellClass: 'grid-align', width: 80 },
             { headerName: "City", field: "CITY", cellClass: 'grid-align', width: 100 },
             { headerName: "Location", field: "LOCATION", cellClass: 'grid-align', width: 180 },
             //{
             //     headerName: "View Images", field: "", width: 200, cellClass: 'grid-align', cellRenderer: ghimages,
              
             //     width: 120, suppressMenu: true

             // },
    ];
  
    function createImageSpan(image) {
        var resultElement = document.createElement("span");
        for (var i = 0; i < 1; i++) {
            var imageElement = document.createElement("img");
            imageElement.src = "/GHB/UploadImages/" + image;
            imageElement.height = 70;
            imageElement.width = 100;
            resultElement.appendChild(imageElement);
        }
        return resultElement;
    }

    function ghimages(params) {
        return createImageSpan(params.data.RRDT_FILE_NAME);
    }
    
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
       // showToolPanel: true,
        //groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    //function groupAggFunction(rows) {
    //    var sums = {
    //        Requisition_Id: 0
    //    };
    //    rows.forEach(function (row) {
    //        var data = row.data;
    //        sums.Requisition_Id += parseFloat((data.Requisition_Id).toFixed(2));
    //    });
    //    return sums;
    //}
    $scope.LoadData = function (stat, ReqType) {
        //if (stat == 1) {
        //    $scope.rptDateRanges();
        //}
        //if (stat == 1) {
        //    ReqType = $scope.Customized.Request_Type;
        //}

        var params = {

            loclst: $scope.Customized.Locations,
            FromDate: $scope.Customized.FromDate,
            ToDate: $scope.Customized.ToDate,
            FromTime: $scope.Customized.FromTime,
            ToTime: $scope.Customized.ToTime,
            STAT: stat,
            rflist: $scope.Customized.RFlst
        };
        
        ViewAvailabilityService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            progress(0, 'Loading...', true);
          
            if ($scope.gridata == null) {
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            else {
                showNotification('', 8, 'bottom-right', '');
                $scope.GridVisiblity = true;
                //$scope.gridOptions.columnApi.getColumn("VERTICAL").colDef.headerName = $scope.BsmDet.Parent;
                //$scope.gridOptions.columnApi.getColumn("COSTCENTER").colDef.headerName = $scope.BsmDet.Child;
                //$scope.gridOptions.api.refreshHeader();
                $scope.gridOptions.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);

                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });
                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }

                $scope.gridOptions.columnApi.setColumnsVisible(cols, false);                cols = [];                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }                 $scope.gridOptions.columnApi.setColumnsVisible(cols, true);

            }
            //progress(0, '', false);
        });

    }, function (error) {
        console.log(error);
    }
    $scope.CustmPageLoad = function () {


    }, function (error) {
        console.log(error);
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Facility Type", key: "RT_NAME" }, { title: "Facility Name", key: "RF_NAME" }, { title: "Room Name/Number", key: "RR_NAME" },
            { title: "Country", key: "COUNTRY" }, { title: "City", key: "CITY" },
            { title: "Location", key: "LOCATION" }, //, { title: "View Image", key: "RRDT_FILE_NAME" }

];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("ViewAvailability.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });
    setTimeout(function () {
        $scope.LoadData(1, 'All');
    }, 1000);

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "ViewAvailability.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Customized, Type) {
        progress(0, 'Loading...', true);
        Customized.Type = Type;
        Customized.loclst = Customized.Locations;
        Customized.rflist = Customized.RFlst;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/ViewAvailability/GetViewAvailabilityData',
                method: 'POST',
                data: Customized,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'ViewAvailability.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    //$timeout($scope.LoadData, 500);
    $scope.Pageload();
    $scope.Customized.Request_Type = "All";
 //   $scope.gridOptions.rowHeight = 70;



    // quick select
    $scope.selVal = "TODAY";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.Customized.FromDate = moment().format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Customized.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Customized.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Customized.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Customized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'NEXTMONTH':
                $scope.Customized.FromDate = moment().add(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().add(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }
 
});
