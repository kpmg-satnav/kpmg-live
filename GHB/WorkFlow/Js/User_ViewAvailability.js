﻿app.service("ViewAvailabilityService", function ($http, $q, UtilityService) {

    this.GetAvailableNBookedRoomsCount = function (vabData) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewAvailability/GetAvailableNBookedRoomsCount', vabData)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


});
app.controller('ViewAvailabilityController', function ($scope, $q, $http, ViewAvailabilityService, UtilityService, $timeout, $filter) {
    $scope.Customized = {};
    $scope.Request_Type = [];
    $scope.rtlist = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.BsmDet = { Parent: "", Child: "" };
    $scope.RFlst = [];


    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;

                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                });

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;

                        angular.forEach($scope.City, function (value, key) {
                            value.ticked = true;
                        });


                    }

                    UtilityService.getLocations(2).then(function (response) {
                        console.log(response.data);
                        if (response.data != null) {
                            $scope.Locations = response.data;
                            angular.forEach($scope.Locations, function (value, key) {
                                value.ticked = true;
                            });
                            setTimeout(function () {
                                getFacilityTypesandNames();
                            }, 200
                            )

                            Get_GH_Timings();
                        }

                    });
                });
            }
        });




    }
    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Customized.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.Customized.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customized.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.Customized.City = $scope.City;
        $scope.getLocationsByCity();
    }



    $scope.locSelectAll = function () {
        $scope.Customized.Locations = $scope.Locations;
        $scope.LocationChange();
    }

    $scope.LocationChange = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });

        //angular.forEach($scope.Locations, function (value, key) {
        //    var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
        //    if (lcm != undefined && value.ticked == true) {
        //        lcm.ticked = true;
        //        $scope.Customized.Locations[0] = lcm;
        //    }
        //});

        getFacilityTypesandNames();
    }

    $scope.LocationSelectNone = function () {
        $scope.Locationlst = [];
        $scope.LocationChange();
    }
    $scope.loadData = function (stat, ReqType) {
        var params = {
            loclst: $scope.Locations,
            FromDate: $scope.Customized.FromDate,
            ToDate: $scope.Customized.ToDate,
            FromTime: $scope.Customized.FromTime,
            ToTime: $scope.Customized.ToTime,
            STAT: stat,
            rflist: $scope.Customized.RFlst
        };
        console.log(params);
        if (moment($scope.Customized.FromDate) > moment($scope.Customized.ToDate)) {
            progress(0, '', false);
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        } else {
            ViewAvailabilityService.GetAvailableNBookedRoomsCount(params).then(function (data) {
                progress(0, 'Loading...', true);
                $('#bookedCount').text(0);
                console.log(data);
                var data = data.data;
                var available = data.Table[0].AVAILABLE;
                $('#availableCount').text(available);
                var booked = data.Table1[0].BOOKED;
                $('#bookedCount').text(booked);
                $scope.GridVisiblity = true;
                progress(0, '', false);
            });
        }

    }



    setTimeout(function () {
        $scope.loadData(2, 'All');
    }, 1000);




    $scope.Pageload();
    $scope.Customized.Request_Type = "All";


    // quick select
    $scope.selVal = "TODAY";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.Customized.FromDate = moment().format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().add(1, 'days').format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Customized.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Customized.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Customized.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Customized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'NEXTMONTH':
                $scope.Customized.FromDate = moment().add(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().add(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }


    function getFacilityTypesandNames() {

        UtilityService.GetFacilityNamesbyLocation(2, $scope.Customized.Locations).then(function (response) {
            $scope.RFlst = [];
            $scope.RFlst = response.data;

            console.log($scope.RFlst);



            angular.forEach($scope.RFlst, function (value, key) {
                value.ticked = true;
            });

            angular.forEach($scope.RFlst, function (value, key) {
                var cny = _.find($scope.RTlst, { RT_SNO: value.RT_SNO });

                if (cny != undefined && value.ticked == true) {
                    cny.ticked = true;

                }
            });

            getFacilityTypes();
        })
    }

    function getFacilityTypes() {
        UtilityService.GetReservationTypesByFacility(2, $scope.RFlst).then(function (response) {
            $scope.RTlst = response.data;

            angular.forEach($scope.RTlst, function (value, key) {
                value.ticked = true;
            });

        }, function (error) {
            console.log(error);
        });
    }

    function Get_GH_Timings() {
        UtilityService.Get_GH_Timings().then(function (response) {
            if (response.data != null) {
                $scope.Customized.FromTime = response.data[0].SYSP_VAL1;
                $scope.Customized.ToTime = response.data[0].SYSP_VAL2;

            }
        });
    }

    $scope.FNSelectAll = function () {
        $scope.Customized.RTlst = $scope.RFlst;


        angular.forEach($scope.RFlst, function (value, key) {
            var cny = _.find($scope.RTlst, { RT_SNO: value.RT_SNO });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.RT_SNO[0] = cny;
            }
        });

    }
    $scope.FNChange = function () {
        angular.forEach($scope.RTlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.RFlst, function (value, key) {
            var cny = _.find($scope.RTlst, { RT_SNO: value.RT_SNO });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.RT_SNO = cny;
            }
        });

        angular.forEach($scope.RFlst, function (value, key) {
            var lcmName = _.find($scope.Locations, { LCM_CODE: value.TVP_LCM_CODE });
            if (lcmName != undefined && value.ticked == true) {
                angular.forEach($scope.Locations, function (value, key) {
                    value.ticked = false;
                });
                lcmName.ticked = true;
            }

            var ctyName = _.find($scope.City, { CTY_CODE: value.TVP_CTY_CODE });
            if (ctyName != undefined && value.ticked == true) {
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = false;
                });
                ctyName.ticked = true;
            }

        });

    }
    $scope.FNSelectNone = function () {
        $scope.FNChange();
    }


    $scope.FTSelectAll = function () {
        $scope.FTChange();
    }

    $scope.FTChange = function () {

        angular.forEach($scope.RFlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.RTlst, function (value, key) {
            var cny = _.find($scope.RFlst, { RT_SNO: value.RT_SNO });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.RT_SNO[0] = cny;
            }
        });
    }

    $scope.Reset = function () {
        $scope.Pageload();
        $scope.selVal = 'TODAY';
        $scope.rptDateRanges();
        $("#ddlRange").val("TODAY");
        $('#ddlRange').selectpicker('refresh');
        setTimeout(function () {
            $scope.loadData(2, 'All');
        }, 1000);
    }
});
