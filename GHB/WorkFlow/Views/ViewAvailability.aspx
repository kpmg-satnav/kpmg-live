﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewAvailability.aspx.cs" Inherits="GHB_Masters_Views_ViewAvailability" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/bootstrap-timepicker.css" rel="stylesheet" />

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="ViewAvailabilityController" class="amantra">
    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Admin View Availability</legend>
                        <div class="clearfix">
                            <div class="box-footer text-right">
                                <span style="color: red;">*</span>  Mandatory field  &nbsp; &nbsp;    <span style="color: red;">**</span>  Select to auto fill the data
                            </div>
                        </div>
                    </fieldset>
                    <div class="well">
                        <form id="form1" name="ViewAvailability" data-valid-submit="LoadData(2,'All')" novalidate>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': ViewAvailability.$submitted && ViewAvailability.CNY_NAME.$invalid}">

                                        <label>Country <span style="color: red;">*</span></label>

                                        <div isteven-multi-select data-input-model="Country" data-output-model="Customized.Country" data-button-label="icon CNY_NAME"
                                            data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                            data-tick-property="ticked" data-max-labels="2">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="ViewAvailability.$submitted && ViewAvailability.CNY_NAME.$invalid" style="color: red">Please select country </span>


                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': ViewAvailability.$submitted && ViewAvailability.CTY_NAME.$invalid}">

                                        <label>City<span style="color: red;">**</span></label>

                                        <div isteven-multi-select data-input-model="City" data-output-model="Customized.City" data-button-label="icon CTY_NAME"
                                            data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.City[0]" name="CTY_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="ViewAvailability.$submitted && ViewAvailability.CTY_NAME.$invalid" style="color: red">Please select city </span>


                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': ViewAvailability.$submitted && ViewAvailability.LCM_NAME.$invalid}">

                                        <label>Location <span style="color: red;">**</span></label>

                                        <div isteven-multi-select data-input-model="Locations" data-output-model="Customized.Locations" data-button-label="icon LCM_NAME"
                                            data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="LocationSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="ViewAvailability.$submitted && ViewAvailability.LCM_NAME.$invalid" style="color: red">Please select location </span>

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': ViewAvailability.$submitted && ViewAvailability.RT_NAME.$invalid}">

                                        <label>Facility Type<span style="color: red;">*</span></label>

                                        <div isteven-multi-select data-input-model="RTlst" data-output-model="Customized.RTlst" button-label="icon RT_NAME" item-label="RT_NAME maker" tick-property="ticked"
                                            data-on-item-click="getFacilityNamesbyType()" data-on-select-all="FTSelectAll()" data-on-select-none="FTSelectNone1()" data-max-labels="2">
                                        </div>
                                        <input type="text" data-ng-model="Customized.RTlst[0]" name="RT_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="ViewAvailability.$submitted && ViewAvailability.RT_NAME.$invalid" style="color: red">Please Select Reservation Type </span>


                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': ViewAvailability.$submitted && ViewAvailability.RF_NAME.$invalid}">

                                        <label>Facility Name<span style="color: red;">**</span></label>

                                        <div isteven-multi-select data-input-model="RFlst" data-output-model="Customized.RFlst" button-label="icon RF_NAME" item-label="RF_NAME maker" tick-property="ticked"
                                            data-on-item-click="FNChange()" data-on-select-all="FNSelectAll()" data-on-select-none="FNSelectNone()" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.RFlst[0]" name="RF_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="ViewAvailability.$submitted && ViewAvailability.RF_NAME.$invalid" style="color: red">Please Select Facility Name </span>


                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Quick Select <span style="color: red;">**</span></label>
                                        <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                            <option value="TODAY">Today</option>
                                            <%-- <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>--%>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="NEXTMONTH">Next Month</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">

                                        <label>From Date</label>

                                        <div class="input-group date" id='fromdate'>
                                            <input type="text" class="form-control" data-ng-model="Customized.FromDate" id="Text1" name="FromDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                            </span>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">

                                        <label>To Date</label>

                                        <div class="input-group date" id='todate'>
                                            <input type="text" class="form-control" data-ng-model="Customized.ToDate" id="Text2" name="ToDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                            </span>
                                        </div>

                                    </div>
                                </div>




                            </div>

                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <%-- <div class="form-group">

                                        <label>From Time</label>

                                        <div class="input-group bootstrap-timepicker timepicker">
                                            <input id="txtFromTime" name="FromTime" type="text" data-ng-model="Customized.FromTime" maxlength="15" class="form-control" />
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                        </div>


                                    </div>--%>
                                    <div class="form-group">
                                        <div class="bootstrap-timepicker">
                                            <div class="form-group">
                                                <label>From Time</label>
                                                <div class="input-group">
                                                    <input type="text" id="Text4" name="FromTime" class="form-control timepicker" data-ng-model="Customized.FromTime"
                                                        required="" />
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="bootstrap-timepicker">
                                            <div class="form-group">
                                                <label>To Time</label>
                                                <div class="input-group">
                                                    <input type="text" id="Text3" name="ToTime" class="form-control timepicker" data-ng-model="Customized.ToTime"
                                                        required="" />
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>








                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12"></div>

                                <div class="col-md-2 col-sm-6 col-xs-12"></div>
                                <div class="col-md-3 col-sm-6 col-xs-12"></div>
                                <div class="col-md-3 col-sm-6 col-xs-12"></div>
                                <div class="col-md-1 col-sm-6 col-xs-12">


                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" />

                                </div>
                            </div>
                        </form>
                        <form id="form2">
                            <div data-ng-show="GridVisiblity">
                                <br />
                                <div class="row">
                                    <a data-ng-click="GenReport(Customized,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(Customized,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(Customized,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                                <div class="row" style="padding-left: 30px">
                                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>


    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../Scripts/bootstrap-timepicker.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>


    <%-- <script src="../../Scripts/moment.min.js"></script>--%>
    <%-- <script src="../../../Scripts/Calender/src/services/moment.js"></script>--%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

            </script>
    <script src="../../../SMViews/Utility.js" defer></script>
    <script src="../../WorkFlow/Js/ViewAvailability.js" defer></script>
    <%--<script src="../../Utility.js"></script>--%><%--<script src="../../WorkFlow/Js/HDMViewAvailability.js"></script>--%>
    <%--<script src="../../HDM/HDM_Webfiles/Reports/Js/HDMViewAvailability.js"></script>--%>


    <script type="text/javascript" defer>

        function setDateVals() {

            $('#Text1').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#Text2').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            //$('#Text1').datepicker('setDate', new Date(moment().subtract(0, 'month').startOf('month').format('MM/DD/YYYY')));
            //$('#Text2').datepicker('setDate', new Date(moment().subtract(0, 'month').endOf('month').format('MM/DD/YYYY')));

            $('#Text1').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));
            $('#Text2').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false,
                showMeridian: false
            });
            //$scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
            //$scope.Customized.ToDate = moment().format('MM/DD/YYYY');
        }
            </script>

    <script type="text/javascript" defer>
        $(document).ready(function () {
          setDateVals();
      });
            </script>
</body>
</html>
