﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/bootstrap-timepicker.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="CheckInAndoutController" onload="setDateVals()" class="amantra">

    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Check In/Out" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Check In/Out</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <div data-ng-show="Viewstatus==0">
                        <form role="form" id="CheckInOut" name="CheckInOut" novalidate>
                            <div class="row" style="padding-left: 15px">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Quick Select <span style="color: red;">**</span></label>
                                        <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                            <option value="TODAY">Today</option>
                                            <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="NEXTMONTH">Next Month</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">

                                        <label>From Date</label>

                                        <div class="input-group date" id='fromdate'>
                                            <input type="text" class="form-control" data-ng-model="Customized.FromDate" id="Text1" name="FromDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                            </span>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">

                                        <label>To Date</label>

                                        <div class="input-group date" id='todate'>
                                            <input type="text" class="form-control" data-ng-model="Customized.ToDate" id="Text2" name="ToDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                            </span>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 20px">
                                    <input type="submit" ng-click="getData()" value="Submit" class="btn btn-primary custom-button-color" />
                                </div>
                            </div>

                            <div class="row" style="padding-left: 30px">
                                <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </form>
                    </div>
                    <div data-ng-show="Viewstatus==1">
                        <form role="form" id="ChkInOut" name="ChkInOut" novalidate>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Requisition ID</b></label>
                                        <div>{{ViewReqData.RB_REQ_ID}}</div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Reserved By </b></label>
                                        <div>{{ViewReqData.RESERVED_BY}}</div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Reserved by Email </b></label>
                                        <div>{{ViewReqData.RESERVED_BY_EMAIL}}</div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Reserved Date </b></label>
                                        <div>{{ViewReqData.RESERVED_DT| date: dateformat}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Facility Type </b></label>
                                        <div>{{ViewReqData.RT_NAME}}</div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Facility Name </b></label>
                                        <div>{{ViewReqData.RF_NAME}}</div>
                                    </div>
                                </div>
                                <%--  <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label><b>Room Number/Name </b> </label>
                                          <div>  {{ViewReqData.RR_NAME}}</div>
                                        </div>
                                    </div>--%>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>City </b></label>
                                        <div>{{ViewReqData.CTY_NAME}}</div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Location </b></label>
                                        <div>{{ViewReqData.LCM_NAME}}</div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix">

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>From Date </b></label>
                                        <div>{{ViewReqData.RB_FROM_DATE | date: dateformat}}</div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>To Date </b></label>
                                        <div>{{ViewReqData.RB_TO_DATE | date: dateformat}}</div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Check In Date </b></label>
                                        <div>{{ViewReqData.RB_FROM_DATE | date: dateformat}}</div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12" ng-show="enableCheckOutDate==1">
                                    <div class="form-group">
                                        <label><b>Check Out Date</b></label>
                                        <div class="input-group date" id='fromdatebooking'>
                                            <input type="text" class="form-control" data-ng-model="ViewReqData.RB_CHK_OUT_DATE" id="checkoutdate" name="FromDate" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdatebooking')"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12" ng-show="enableCheckOutDate==0">
                                    <div class="form-group">
                                        <label><b>Check Out Date </b></label>
                                        <div>{{ViewReqData.RB_TO_DATE | date: dateformat}}</div>
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><b>Mobile Number </b></label>
                                        <div>{{ViewReqData.RB_MOBILE_NUMBER}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <table class="table table-bordered table-striped">
                                    <thead style="background-color: #366599; color: #E2E2E2">
                                        <tr>
                                            <th>Room Name</th>
                                            <th>Employee Name</th>
                                            <th>Type</th>
                                            <th>Email</th>
                                            <th>Check In</th>
                                            <th>Check Out</th>
                                            <th>Remarks</th>
                                            <th>Edit/Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="s in selectedRows " ng-include="getTemplate(s)">
                                        </tr>
                                    </tbody>
                                </table>

                                <script type="text/ng-template" id="display">
                                            <td>{{s.RR_NAME}}</td>
                                            <td>{{s.RB_EMP_NAME}}</td>
                                            <td>{{ getEmpType(s.RB_EMP_TYPE)}}</td>
                                            <td>{{s.RB_EMP_EMAIL }}</td>
                                            <td> {{s.RB_CHK_IN_TIME}}</td>
                                            <td> {{s.RB_CHK_OUT_TIME}}</td>
                                            <td>{{s.RB_REMARKS}}</td>
                                            <td>
                                            <button type="button" ng-click="editContact(s)" class="btn-xs btn-primary"><i class="fa fa-pencil fa-2" aria-hidden="true"></i></button>
                                                                                
                                            </td>
                                </script>
                                <script type="text/ng-template" id="edit">
                                                    <td>  <label  >{{model.selected.RR_NAME}}</label></td>
                                                    <td>
                                                    <div angucomplete-alt
                                                    id="ex7"
                                                    placeholder="Search Employee"
                                                    pause="500"
                                                    selected-object="residentSelected"
                                                    remote-url="../../../api/Utility/GetEmployeeNameAndID"
                                                    remote-url-request-formatter="remoteUrlRequestFn"
                                                    remote-url-data-field="items"
                                                    title-field="NAME"
                                                    minlength="2"
                                                    input-class="form-control form-control-small"
                                                    ng-model="model.selected.RB_EMP_NAME"

                                                    match-class="highlight" >
                                                    </div>
                                                    </td>
                                                    <td>
                                                    <select ng-model="model.selected.RB_EMP_TYPE" >
                                                    <option selected="selected">--Select--</option>
                                                    <option  ng-repeat="type in EmpTypes"   value="{{type.ET_SNO}}">{{type.ET_NAME}}</option>
                                                    </select>
                                                    </td>
                                                    <td><textarea rows="2" ng-model="model.selected.RB_EMP_EMAIL" ></textarea></td>
                                                    <td> 
                                        
                                                <select   data-ng-model="model.selected.RB_CHK_IN_TIME" class="chkintimeSelect">
                                                      <option  ng-repeat="chkin in CHKINOUTTIME"   value="{{chkin.TIME}}">{{chkin.TIME}}</option>
                                                </select>
                                        
                                        </td>
                                                    <td> 
                                       
                                           <select   data-ng-model="model.selected.RB_CHK_OUT_TIME" class="chkouttimeSelect">
                                 <option  ng-repeat="type in CHKINOUTTIME"   value="{{type.TIME}}">{{type.TIME}}</option>
                                                </select>
                                        </td>
                                                    <td><textarea rows="2"  ng-model="model.selected.RB_REMARKS" ></textarea></td>
                                                    <td>
                                                    <button ng-click="saveContact($index,'CHECKIN')" class="btn-xs btn-info" ng-hide='model.selected.RB_CHK_IN_OUT_STA_ID==1'>CheckIn</button>
                                                    <button ng-click="saveContact($index,'CHECKOUT')" class="btn-xs btn-info" ng-hide='model.selected.RB_CHK_IN_OUT_STA_ID==0'>CheckOut</button>
                                                    <button ng-click="reset()" class="btn-xs btn-warning">Cancel</button>
                                                    </td>
                                </script>
                            </div>




                            <div class="clearfix">
                                <div class="box-footer text-right" data-ng-show="Viewstatus==1">
                                    <input type="button" value="Back" class="btn btn-primary custom-button-color" data-ng-click="back()">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../Scripts/bootstrap-timepicker.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "angucomplete-alt"]);

        function setDateVals() {
            $('#Text1').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            $('#Text1').datepicker('setDate', new Date(moment().startOf('month').format('MM/DD/YYYY')));
            $('#Text2').datepicker('setDate', new Date(moment().endOf('month').format('MM/DD/YYYY')));

            $(".timepicker").timepicker({
                showInputs: false,
                showMeridian: false
            });
        }

    </script>
    <script type="text/javascript" defer>
        $(document).ready(function () {
            setDateVals();
        });
    </script>
    <script src="../../../SMViews/Utility.js" defer></script>
    <script src="../Js/CheckInAndOut.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>

</body>
</html>
