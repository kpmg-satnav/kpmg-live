﻿app.service("UserGuestHouseUltReportService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    this.path = window.location.origin;
    this.GetGriddata = function (Dataobj) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserGuestHouseUltReport/GetGuesthousedetails', Dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetGHStatusDT = function () {
        var deferred = $q.defer();        
        $http.get(this.path + '/api/UserGuestHouseUltReport/GetGHStatusDetails')
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

}]);

app.controller('UserGuestHouseUltReportController', ['$scope', '$q', '$http', 'UserGuestHouseUltReportService', 'UtilityService', '$timeout','$filter', function ($scope, $q, $http, UserGuestHouseUltReportService, UtilityService, $timeout, $filter) {
    $scope.UserGuestHouse = {};
    $scope.GridVisiblity = true;
    $scope.DocTypeVisible = 0;

    var datetimenow = moment().format('YYYY-MM-DD h:mm') + " ";
    $scope.UserGuestHouse.Country = [];
    $scope.UserGuestHouse.Locations = [];

    $scope.columnDefs = [

{ headerName: "Reference Id", field: "RB_REFERENCE_ID", cellClass: 'grid-align', width: 150 },
{ headerName: "Reserved By", field: "RB_CREATEDBY", cellClass: 'grid-align', width: 200,  },
{ headerName: "Reserved For", field: "RB_RESERVED_FOR", cellClass: 'grid-align', width: 200 },
{ headerName: "Room Number/Name", field: "RR_NAME", cellClass: 'grid-align', width: 150 },
{ headerName: "Utilization", field: "UTILIZATION", cellClass: 'grid-align', width: 170 , suppressMenu: true, },
{ headerName: "Facility Type", field: "RT_NAME", width: 150, cellClass: 'grid-align'},
{ headerName: "Facility Name", field: "RF_NAME", cellClass: 'grid-align', width: 150 },
//{ headerName: "Capcity", field: "RR_CAPCITY", cellClass: 'grid-align', width: 150 },
{ headerName: "City", field: "RBS_CTY_CODE", cellClass: 'grid-align', width: 100 },
{ headerName: "Location", field: "RBS_LOC_CODE", cellClass: 'grid-align', width: 150 },
{ headerName: "Cost Center", field: "Cost_Center_Name", cellClass: 'grid-align', width: 150 },
{ headerName: "Reserved On", field: "RB_CREATEDON", template: '<span>{{data.RB_CREATEDON | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true, },
{ headerName: "Reserved For Email", field: "RESVERED_FOR_EMAIL", cellClass: 'grid-align', width: 200},
{ headerName: "Reserved By Email", field: "RESVERED_BY_EMAIL", cellClass: 'grid-align', width: 200},
{ headerName: "Check In Date", field: "RB_CHK_IN_DATE", template: '<span>{{data.RB_CHK_IN_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
{ headerName: "Check In Time", field: "RB_CHK_IN_TIME", cellClass: 'grid-align', width: 100, suppressMenu: true, },
{ headerName: "Check Out Date", field: "RB_CHK_OUT_DATE", template: '<span>{{data.RB_CHK_OUT_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
{ headerName: "Check Out Time", field: "RB_CHK_OUT_TIME", cellClass: 'grid-align', width: 100, suppressMenu: true, },

{ headerName: "Band", field: "band", cellClass: 'grid-align', width: 100, },
{ headerName: "Cost", field: "rf_cost", cellClass: 'grid-align', width: 100, },
{ headerName: "Type", field: "bt_name", cellClass: 'grid-align', width: 100, },
{ headerName: "Remarks", field: "rb_remarks", cellClass: 'grid-align', width: 100, suppressMenu: true, },
          
{ headerName: "No.Of Night", field: "noofnights", cellClass: 'grid-align', width: 100, },
{ headerName: "Total Of Room Rent", field: "totalroomrent", cellClass: 'grid-align', width: 100, },
        //  { headerName: "Status", field: "STA_TITLE", cellClass: 'grid-align', width: 100 },
    ];

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.Pageload = function () {
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                    $scope.UserGuestHouse.Country.push(value);
                });
            }
        });

        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = true;
                });
            }
        });

        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Locations = response.data;
                angular.forEach($scope.Locations, function (value, key) {
                    value.ticked = true;
                    $scope.UserGuestHouse.Locations.push(value);
                });
            }
        });

        UserGuestHouseUltReportService.GetGHStatusDT().then(function (response) {
            if (response.data != null) {
                $scope.Status = response.data;
            }
        });
    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.UserGuestHouse.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.UserGuestHouse.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UserGuestHouse.Country[0] = cny;
            }
        });
    }
    $scope.cnySelectAll = function () {
        $scope.UserGuestHouse.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }
    $scope.ctySelectAll = function () {
        $scope.UserGuestHouse.City = $scope.City;
        $scope.getLocationsByCity();
    }
    $scope.getTowerByLocation = function () {
        $scope.UserGuestHouse.Locations = $scope.Locations;

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.UserGuestHouse.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.UserGuestHouse.City[0] = cty;
            }
        });
    }
    $scope.locSelectAll = function () {
        $scope.UserGuestHouse.Locations = $scope.Locations;
        $scope.getTowerByLocation();
    }        
    $scope.lcmSelectNone = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
    }
    $scope.Pageload();

    $scope.selVal = "30";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.UserGuestHouse.FromDate = moment().format('MM/DD/YYYY');
                $scope.UserGuestHouse.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.UserGuestHouse.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.UserGuestHouse.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.UserGuestHouse.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.UserGuestHouse.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.UserGuestHouse.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.UserGuestHouse.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.UserGuestHouse.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.UserGuestHouse.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.UserGuestHouse.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.UserGuestHouse.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }

    $scope.UserGuestHouse.Request_Type = "ALL";
    var OVERALL_COUNT;
    $scope.LoadData = function (std) {
        var searchval;
        if (std == 0) {
            searchval = "";
            $("#filtertxt").val("");
        }
        else {
            searchval = $("#filtertxt").val();
        }
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                var Dataobj = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
            FromDate: $scope.UserGuestHouse.FromDate,
            ToDate: $scope.UserGuestHouse.ToDate,
            lcmlst: $scope.UserGuestHouse.Locations,
            Status: $scope.UserGuestHouse.Request_Type,
            CompanyID: $('#ddlCompany').val()
        };

        var fromdate = moment($scope.UserGuestHouse.FromDate);
        var todate = moment($scope.UserGuestHouse.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);
            $scope.GridVisiblity = true;
            UserGuestHouseUltReportService.GetGriddata(Dataobj).then(function (data) {
                console.log(data);
                $scope.gridata = data;
                if ($scope.gridata.length==0) {
                    $("#btNext").attr("disabled", true);
                    $scope.gridOptions.api.setRowData([]);
                }
                else {
                    OVERALL_COUNT = $scope.gridata[0].OVERALL_COUNT
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }            
                progress(0, 'Loading...', false);
            }, function (error) {
                console.log(error);
            });
        }
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    setTimeout(function () {
        $scope.LoadData();
    }, 2000);

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Requisition Id", key: "REQ_ID" }, { title: "Reservation Type", key: "RT_NAME" }, { title: "Reservation Code", key: "RR_CODE" },
            { title: "Reservation Name", key: "RR_NAME" }, { title: "City", key: "RBS_CTY_CODE" }, { title: "Location", key: "RBS_LOC_CODE" }, { title: "Location", key: "LCM_NAME" },
            { title: "From Date", key: "RB_FROM_DATE" }, { title: "To Date", key: "RB_TO_DATE" }, { title: "Department", key: "DEP_NAME" }, { title: "Reserved By", key: "RB_CREATEDBY" },
            { title: "Reserved On", key: "RB_CREATEDON" }, { title: "Reserved For", key: "RB_RESERVED_FOR" }, { title: "Reserved For Email", key: "RESVERED_FOR_EMAIL" }, { title: "Reserved By Email", key: "RESVERED_BY_EMAIL" }, { title: "Check In", key: "RBC_CHK_IN_TIME" }, { title: "Check Out", key: "RBC_CHK_OUT_TIME" },
            { title: "Status", key: "RB_TITLE" }];
        
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        console.log($scope.gridOptions.api.isAnyFilterPresent($scope.gridOptions.api.getModel()));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save(datetimenow + "GuestHouseUtilizationReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: datetimenow + "GuestHouseUtilizationReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


    $scope.GenReport = function (ghudata, Type) {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        ghudata.Type = Type;
      

        var Dataobj = {
            SearchValue: searchval,
            PageNumber: 1,
            PageSize:OVERALL_COUNT,
            FromDate: ghudata.FromDate,
            ToDate: ghudata.ToDate,
            lcmlst: ghudata.Locations,
            Status: ghudata.Request_Type,
            Type: ghudata.Type,
            CompanyID: $('#ddlCompany option:selected').val(),
        };

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (ghudata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {            
            $http({
                url: UtilityService.path + '/api/UserGuestHouseUltReport/GetUserGHDetailsReportdata',
                method: 'POST',
                data: Dataobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);       
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = datetimenow + 'GuestHouseUtilizationReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    //$("#filtertxt").change(function () {
    //    onReqFilterChanged($(this).val());
    //}).keydown(function () {
    //    onReqFilterChanged($(this).val());
    //}).keyup(function () {
    //    onReqFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onReqFilterChanged($(this).val());
    //});
}]);