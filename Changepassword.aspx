﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ChangepasswordMaster.master" AutoEventWireup="true" CodeFile="Changepassword.aspx.cs" Inherits="Changepassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .ScrollStyle {
            max-height: 530px;
            overflow-y: scroll;
        }

            .ScrollStyle::-webkit-scrollbar {
                width: 12px;
            }
            /* Track */
            .ScrollStyle::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(99,187,178,0.3);
                -webkit-border-radius: 10px;
                border-radius: 10px;
            }

            /* Handle */
            .ScrollStyle::-webkit-scrollbar-thumb {
                -webkit-border-radius: 10px;
                border-radius: 10px;
                background: rgba(99,187,178,0.8);
                -webkit-box-shadow: inset 0 0 6px rgba(99,187,178,0.5);
            }

        .active1 {
            background-color: #1C2B36;
            /*border-right: 4px solid #FFFFFF;*/
            color: #FFFFFF !important;
        }

        a:visited {
            color: #FFFFFF;
            background-color: #209E91;
        }

        .notify.top {
            top: 130px;
        }
    </style>

    <div class="row">
        <ba-sidebar>
        <aside class="al-sidebar">
             <nav class="sidebar-nav">
                    <ul id="menu" class="ScrollStyle">
                        <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                    </ul>
                </nav>
        </aside>
        </ba-sidebar>

        <div class="al-footer">
            <div class="pull-left" style="padding-left: 45px">
                Other Links : 
                <a href="http://www.quickfms.com/" target="_blank">www.quickfms.com</a>
            </div>
            <div class="pull-right">
                <%--<%Response.Write("&copy;" & Year(Now))%>&nbsp;<%Response.Write("QuickFMS v3.1")%>--%>
            </div>
        </div>

        <%--<back-top></back-top>--%>
    </div>
    <div class="animsition" data-ng-controller="FloorLstController">
       <div ba-panel ba-panel-title="Change Password" ba-panel-class="with-scroll">
           <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                            <h3 class="panel-title panel-heading-qfms-title">Change Password</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <%--<form id="form1" runat="server">--%>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">User Id</label>
                                        <asp:TextBox ID="txtUsrId" runat="server" ReadOnly="True" CssClass="form-control">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Employee Name</label>
                                        <asp:TextBox ID="txtEmpName" runat="server" ReadOnly="True" CssClass="form-control">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Email Id</label>
                                        <asp:RegularExpressionValidator ID="revemail" runat="server" ControlToValidate="txtmailid"
                                            Display="None" ErrorMessage="Enter Valid Email Id" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                        </asp:RegularExpressionValidator>
                                        <asp:TextBox ID="txtmailid" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Enter Old Password<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvOldPwd" runat="server" ControlToValidate="txtOldPwd" ValidationGroup="Group1"
                                            Display="None" ErrorMessage="Please Enter Old Password" SetFocusOnError="True">
                                        </asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtOldPwd" runat="server" CssClass="form-control" MaxLength="15"
                                            TextMode="Password">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Enter New Password<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvNewPwd" runat="server" ControlToValidate="txtpwd"
                                            Display="None" ErrorMessage="Please Enter New Password" SetFocusOnError="True" ValidationGroup="Group1">
                                        </asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtpwd" runat="server" CssClass="form-control" MaxLength="15" TextMode="Password"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Confirm New Password<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvCpwd" runat="server" ControlToValidate="txtcpwd" ValidationGroup="Group1"
                                            Display="None" ErrorMessage="Please Enter Confirm New Password" SetFocusOnError="True">
                                        </asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvPwd" runat="server" ControlToCompare="txtpwd" ControlToValidate="txtcpwd"
                                            Display="None" ErrorMessage="Passwords do not match" SetFocusOnError="True">
                                        </asp:CompareValidator>
                                        <asp:TextBox ID="txtcpwd" runat="server" CssClass="form-control" MaxLength="15" TextMode="Password">                                      
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" ValidationGroup="Group1" Text="Submit" OnClick="btnSubmit_Click" />
                                        <asp:Label ID="lblPwd" runat="server" CssClass="clsnote" Visible="False"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <h5>
                                                <label class="col-md-12 control-label" style="text-align: center;">
                                                    <span style="color: red;">Note:</span> Password Should Contain Minimum of 8 characters, 2 Numerics, 1 Special Character
                                                </label>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--</form>--%>
                        </div>
                   
                </div>
            </div>
        </div>
    
    <%--<script defer src="../BootStrapCSS/Scripts/jquery.min.js"></script>--%>
    <%--<script defer src="../BlurScripts/BlurJs/notify.js"></script>--%>
    <link href="BlurScripts/BlurCss/Notify1.css" rel="stylesheet" />
    <%--<link href="BlurScripts/BlurCss/notify.css" rel="stylesheet" />--%>
    <script src="BootStrapCSS/Scripts/jquery.min.js"></script>
    <script defer src="BlurScripts/BlurJs/notify.js"></script>
    <%--<script defer src="BootStrapCSS/Scripts/bootstrap-notify.min.js"></script>--%>
    <script defer src="BlurScripts/BlurJs/prettify.js"></script>
    
    <script src="Scripts/Intercom/Intercom.js"></script>
    <link href="BootStrapCSS/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <%-- <script defer src="Dashboard/C3/d3.v3.min.js"></script>
    <script defer src="Dashboard/C3/c3.min.js"></script>
    <script defer src="Dashboard/C3/jshint.js"></script>--%>

    <script defer type="text/javascript">

        //TO AVOID FADE IN EFFECT IN IE
        var ms_ie = false;
        var ua = window.navigator.userAgent;
        var old_ie = ua.indexOf('MSIE ');
        var new_ie = ua.indexOf('Trident/');
        var mozilla = ua.indexOf('Firefox/');

        if ((old_ie > -1) || (new_ie > -1)) {
            ms_ie = true;
        } else if (mozilla > -1) {
            mozilla = true;
        }

        function setFrameSource(src) {
            if (!(ms_ie || mozilla)) {
                $("#container").hide();
            }
            $("#container").attr("src", src);
            Intercom('update', { "Link": src });
            return false;
        }

        <%--$(document).ready(function () {
            debugger;
            $.notify('&nbsp  Welcome <% =Session["LoginUser"] %> , Kindly update the current system generated password ', { color: '#fff', background: '#20d67b', close: true, icon: "user", position: 'middle', delay: '50000' });
            //$(window).on('blur', function () { $('.dropdown-toggle').parent().removeClass('open'); });            
            $('#container').css('height', $(window).height() - 100 + 'px');
            $('.sidebar').css('height', $(window).height() - 100 + 'px');
            $("#page-wrapper").css('min-height', $(window).height() + 'px');

            //if (!ms_ie) {
            $("#container").load(function (e) {
                $("#container").fadeIn("slow");
            });
            //}
        });--%>

        function Validate() {
            var Isvalid = false;
            Isvalid = Page_ClientValidate("Group1");
            if (Isvalid == "false") {
                return false;
            }
        }

        //$.notify("Hello World");
        window.intercomSettings = {
            <%--app_id: "qy20hva4",
            name: '<%=Session("uid")%>', // Full name
            email: '<%=Session("uemail")%>', // Email address
            created_at: '<%=Session("LoginTime")%>',// Signup date as a Unix timestamp
            "Link": 'frmDetails.aspx',
            tenant_id: '<%=Session("TENANT")%>'--%>
        };
        $(document).ready(function ($) {
            $('a').click(function () {
                $('a').removeClass("active1");
                $(this).addClass("active1");
            });
            //$.notify('Hello World');
            <%--  <%--  debugger;--%>
            
            $.notify('&nbsp  Welcome <% =Session["LoginUser"] %> , Kindly update the current system generated password. Until then you will not have the access to the screens in QuickFMS tool', { color: '#fff', background: '#FF0000', close: true, icon: "user", position: 'middle', delay: '100000', height:'130px' });
            //$(window).on('blur', function () { $('.dropdown-toggle').parent().removeClass('open'); });            
            //$('#container').css('height', $(window).height() - 100 + 'px');
            //$('.sidebar').css('height', $(window).height() - 100 + 'px');
            //$("#page-wrapper").css('min-height', $(window).height() + 'px');

            ////if (!ms_ie) {
            //$("#container").load(function (e) {
            //    $("#container").fadeIn("slow");
            //});
        });

    </script>

</asp:Content>

