﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 35px;
            height: 20px;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 12px;
            width: 12px;
            left: 4.5px;
            bottom: 4.5px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #1C2B36;
            -webkit-transition: .4s;
            transition: .4s;
        }


        input:checked + .slider {
            background-color: #63bbb2;
        }

            input:checked + .slider:before {
                -webkit-transform: translateX(12px);
                -ms-transform: translateX(12px);
                transform: translateX(12px);
            }

        .slider.round:before {
            border-radius: 45%;
        }
        /* Rounded sliders */
        .slider.round {
            border-radius: 15px;
        }
    </style>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }



        .align-left {
            float: left;
            width: 50%;
        }

        .align-right {
            float: right;
            width: 50%;
        }
    </style>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body data-ng-controller="ProjectCheckListReport" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="CheckList Report" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms" style="height: 41px;">
                    <div class='container'>
                        <div class="align-left">
                            <h3 class="panel-title panel-heading-qfms-title">Project CheckList Report</h3>
                        </div>
                        <div class="align-right">

                            <label style="font-weight: normal; font-size: smaller;">Image Download  </label>
                            <label class="switch" style="font-size: medium">
                                <input type="checkbox" id="myCheck" ng-click="myFunction()">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="panel-body" style="padding-right: 10px;" id="text">

                    <form id="PCL_Report" name="frmCheckList">
                        <div class="row">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.LCM_NAME.$invalid}">
                                        <label class="control-label">Location <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Location" data-output-model="PCL_Report.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                            data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="PCL_Report.Location" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" style="color: red;" data-ng-show="frmCheckList.$submitted && frmCheckList.LCM_NAME.$invalid">Please select location </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Quick Select</label>
                                        <br />
                                        <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                            <option value="TODAY">Today</option>
                                            <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="LASTMONTH">Last Month</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.FromDate.$invalid}">
                                        <label class="control-label">From Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" style="width: 150px" id='fromdate'>
                                            <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="Text1" name="FromDate" ng-model="PCL_Report.FromDate" required />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                            </span>
                                        </div>
                                        <span class="error" style="color: red;" data-ng-show="frmCheckList.$submitted && frmCheckList.FromDate.$invalid" style="color: red"></span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.ToDate.$invalid}">
                                        <label class="control-label">To Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" style="width: 150px" id='todate'>
                                            <input type="text" id="Text2" class="form-control" required="" placeholder="mm/dd/yyyy" name="ToDate" ng-model="PCL_Report.ToDate" />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                            </span>
                                        </div>
                                        <span class="error" style="color: red;" data-ng-show="frmCheckList.$submitted && frmCheckList.ToDate.$invalid" style="color: red"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Company<span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="PCL_Report.CNP_NAME" button-label="icon CNP_NAME"
                                            item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="PCL_Report.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                        <span class="error" style="color: red;" data-ng-show="PCL_Report.$submitted && PCL_Report.CNP_NAME.$invalid" style="color: red">Please Select Company </span>
                                    </div>
                                </div>
                                <div class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-top: 26px">
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="Pageload()" />
                                </div>
                            </div>
                        </div>

                        <div class="row" style="padding-right: 18px">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12" id="table2">
                                <br />
                                <a data-ng-click="GenReport(PCL_Report,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(PCL_Report,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(PCL_Report,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                            </div>
                        </div>
                        <br />
                        <div id="Tabular">
                            <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>
                    </form>
                </div>

                <div>
                    <div class="panel-body" id="text1" style="display: none">
                        <form id="twoform">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.LCM_NAME.$invalid}">
                                        <label class="control-label">Location <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Location" data-output-model="PCL_Report.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                            data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="PCL_Report.Location" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" style="color: red;" data-ng-show="frmCheckList.$submitted && frmCheckList.LCM_NAME.$invalid">Please select location </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.INSPECTOR.$invalid}">
                                        <label class="control-label">Inspection By <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Inspection" data-output-model="PCL_Report.InspctdBy" data-button-label="icon INSPECTOR" data-item-label="icon INSPECTOR"
                                            data-tick-property="ticked" data-max-labels="1" selection-mode="multiple">
                                        </div>
                                        <input type="text" data-ng-model="PCL_Report.InspctdBy" name="INSPECTOR" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.INSPECTOR.$invalid" style="color: red">Please select Inspection By  </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.SVR_FROM_DATE.$invalid}">
                                        <label class="control-label">Visit Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" style="width: 150px" id='InspctdDT'>
                                            <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="Text3" name="SVR_FROM_DATE" ng-model="PCL_Report.InspctdDT" required />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar" onclick="setup('InspctdDT')"></span>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.SVR_FROM_DATE.$invalid" style="color: red">Please select from date</span>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="LoadImg()" style="float: right" />
                            </div>
                            <br>
                            <br>
                            <div class="clearfix">
                                <br>
                                <br>

                                <div class="row">
                                    <div data-ag-grid="gridOptions1" class="ag-blue" style="height: 310px; width: auto"></div>
                                    <%--<div id="myGrid" style="height: 310px; width: auto " visible="false" class="ag-blue"></div>--%>
                                </div>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js" defer></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../JS/ProjectCheckListReport.js" defer></script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script type="text/javascript" defer>
        $(document).ready(function () {
            setDateVals();
        });
        var CompanySession = '<%= Session["COMPANYID"]%>';
        function setDateVals() {

            $('#Text1').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#Text2').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#Text3').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#Text1').datepicker('setDate', new Date(moment().startOf('month').format('MM/DD/YYYY')));
            $('#Text2').datepicker('setDate', new Date(moment().endOf('month').format('MM/DD/YYYY')));
        }
    </script>
</body>
</html>
