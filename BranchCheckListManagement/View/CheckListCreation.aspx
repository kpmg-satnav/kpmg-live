﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js" defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <script type="text/javascript" defer>

        $(document).ready(function () {
            $("input[type=file]").click(function () {
                $(this).val("");
            });

            $("input[type=file]").change(function () {
                alert($(this).val());
            });
        });

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: 'today',
                maxDate: 'today'
            });
        };

        function OnlyNumeric(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }



    </script>
    <style>
        .col-xs-3 selected {
            border-color: blue;
        }

        .container {
            width: 432% !important;
        }

        .has-error2 {
            border-style: solid;
            border-color: #ff0000;
        }

        .has-error3 {
        }

        .mystyle {
            border-color: red !important;
            border-width: 2px !important;
        }



        .highlight {
            background-color: red;
        }

        input[type='radio'], label {
            margin: 10px;
        }

        .clearBoth {
            clear: both;
        }

        .Fonts {
            font-size: 14px;
        }

        input {
            height: 35px;
            font-size: 15px;
        }




        .table-bordered th {
            color: black;
            background: #99CCFF;
            font-size: 15px;
        }

        .table-bordered td {
            border: 1px solid black;
            float: left;
        }

        /*th {
            color: black;
            background: #99CCFF;
            font-size: 15px;
        }*/

        /*table, th, td {
            border: 1px solid black;
            float: left;
        }*/

        .panel {
            box-shadow: 0 5px 5px 0 rgba(0,0,0,.25);
        }

        .panel-heading {
            border-bottom: 1px solid rgba(0,0,0,.12);
        }

        .list-inline {
            display: block;
        }

            .list-inline li {
                display: inline-block;
            }

                .list-inline li:after {
                    content: '|';
                    margin: 0 10px;
                }
    </style>
</head>
<body data-ng-controller="CheckListCreationController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Branch Checklist Creation  <a href="#ex1" rel="modal:open" style="margin-left: 800px; color: green;">Saved Drafts</a></h3>
                </div>

                <div class="panel-body" style="padding-right: 10px;">
                    <div class="clearfix">
                        <div class="box-footer text-right">

                            <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                        </div>
                    </div>
                    <br />
                    <form id="Form1" name="frmCheckList" data-valid-submit="Creation()" novalidate>
                        <div class="row">

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.CNY_NAME.$invalid}">
                                    <label class="control-label">Country <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Country" data-output-model="CheckList.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                        data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="CheckList.Country" name="CNY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.CTY_NAME.$invalid}">
                                    <label class="control-label">City <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="City" data-output-model="CheckList.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                        data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="CheckList.City" name="CTY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.CTY_NAME.$invalid" style="color: red">Please select city </span>

                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.LCM_NAME.$invalid}">
                                    <label class="control-label">Location <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Location" data-output-model="CheckList.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                        data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="CheckList.Location" name="LCM_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.INSPECTOR.$invalid}">
                                    <label class="control-label">Inspection By <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Inspection" data-output-model="CheckList.Inspection" data-button-label="icon INSPECTOR" data-item-label="icon INSPECTOR"
                                        data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="CheckList.Inspection" name="INSPECTOR" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.INSPECTOR.$invalid" style="color: red">Please select Inspection By  </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.SVR_FROM_DATE.$invalid}">
                                    <label class="control-label">Visit Date <span style="color: red;">*</span></label>
                                    <div class="input-group date" style="width: 150px" id='fromdate'>
                                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="SVR_FROM_DATE" name="SVR_FROM_DATE" ng-model="CheckList.SVR_FROM_DATE" required />
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.SVR_FROM_DATE.$invalid" style="color: red">Please select from date</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.CNP_NAME.$invalid}">
                                    <label class="control-label">Company<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="CNP_NAME" data-output-model="CheckList.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                        item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="CheckList.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.CNP_NAME.$invalid" style="color: red">Please Select Company </span>
                                </div>
                            </div>
                        </div>
                        <br />
                        <%-- <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-24">
                                        <div class="form-group">
                                            <ul class="list-inline">
                                                <li data-ng-repeat="dat in scorelist">{{dat.BCL_SCORE_CODE}} - {{dat.BCL_SCORE_NAME}}</li>

                                            </ul>
                                        </div>
                                    </div>
                                </div> --%>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div id="tableDiv" style="margin-top: 1px;">
                                        Table will generate here.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div style="margin-top: 1px;">
                                        <label for="txtcode" class="custom-file"><strong>Overall Comments</strong></label>
                                        <textarea name="OVERALL_CMTS" id="OVERALL_CMTS" class="form-control" data-ng-model="CheckList.OVERALL_CMTS" cols="40" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <label for="txtcode" class="custom-file">Upload Images/Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                <input multiple type="file" name="UPLFILE" data-ng-model="CheckList.UPLFILE[0]" id="UPLFILE" accept=".png,.jpg,.jpeg" class="custom-file-input" onchange="angular.element(this).scope().fileNameChanged(this)">
                                <span class="custom-file-control"></span>
                            </div>
                        </div>
                        <div class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-top: 26px">
                            <input type="button" value="Save as Draft" class="btn btn-primary custom-button-color" data-ng-click="Save()" />
                            <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="Submit()" />
                        </div>

                        <div class="clearfix">
                            <div class="col-md-12">
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: 47%"></div>
                            </div>
                        </div>

                        <div id="ex1" class="modal">

                            <div class="clearfix">
                                <div class="col-md-12">
                                    <h4>Saved Items</h4>
                                </div>
                                <div class="col-md-12 table-responsive">
                                    <table class="table table-bordered">

                                        <tr>
                                            <td style="width: 100px;">Location Code
                                            </td>
                                            <td style="width: 198px">Location Name
                                            </td>
                                            <td style="width: 110px">Inspected Date
                                            </td>
                                        </tr>
                                        <tr data-ng-repeat="items in CheckList.SaveList">
                                            <td style="width: 100px; height: 55px;">
                                                <a href data-ng-bind="items.LCM_CODE" class="control-label" data-ng-model="items.LCM_CODE" ng-click="GetLocationCode(items)" rel="modal:close"></a>
                                            </td>
                                            <td style="width: 198px; height: 55px;">
                                                <label data-ng-bind="items.LCM_NAME" class="control-label" data-ng-model="items.LCM_NAME">
                                                </label>
                                            </td>

                                            <td style="width: 110px; height: 55px;">
                                                <label data-ng-bind="items.BCL_SELECTED_DT| date:'MM/dd/yyyy'" class="control-label" data-ng-model="items.BCL_SELECTED_DT">
                                                </label>
                                            </td>

                                        </tr>

                                        <%--<thead>
                                                    <tr>
                                                        <th>Location Code</th>
                                                        <th>Location Name</th>
                                                    </tr>
                                                </thead>--%>
                                        <%--<tbody>
                                                    <tr data-ng-repeat="items in CheckList.SaveList">
                                                        <td>
                                                            <a href data-ng-bind="items.LCM_CODE" class="control-label" data-ng-model="items.LCM_CODE" ng-click="GetLocationCode(items.LCM_CODE)"></a>
                                                        </td>
                                                        <td>
                                                            <label data-ng-bind="items.LCM_NAME" class="control-label" data-ng-model="items.LCM_NAME">
                                                            </label>
                                                        </td>
                                                    </tr>
                                                </tbody>--%>
                                    </table>
                                </div>
                            </div>
                            <a href="#" rel="modal:close">Close</a>
                        </div>

                    </form>

                </div>

            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../BootStrapCSS/Scripts/UnderScoreJs.js" defer></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../JS/CheckListCreation.js" defer></script>
</body>


</html>
