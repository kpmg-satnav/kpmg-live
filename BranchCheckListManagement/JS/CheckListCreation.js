﻿
app.service("CheckListCreationService", function ($http, $q, UtilityService) {

    this.getInspectors = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckListCreation/getInspectors')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getCompany = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckListCreation/getCompany')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };


    this.GetGriddata = function (SelectedDraftLocation) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/GetGriddata', SelectedDraftLocation)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetScore = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckListCreation/GetScore')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.InsertCheckList = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/InsertCheckList', dataobject)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
    this.getLocations = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckListCreation/getLocations')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getSavedList = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListCreation/getSavedList')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.getSavedListItems = function (LcmCode) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckListCreation/getSavedListItems?LcmCode=' + LcmCode + ' ')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.LocationByCity = function (citylst) {
        var deferred = $q.defer();
        $http.post(UtilityService.path + '/api/CheckListCreation/LocationByCity', citylst)
          .then(function (response) {
              deferred.resolve(response.data);
          }, function (response) {
              deferred.reject(response);
          });
        return deferred.promise;
    };

});
app.controller('CheckListCreationController', function ($scope, $q, $location, CheckListCreationService, UtilityService, $filter) {
    $scope.CheckList = {};
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Inspection = [];
    $scope.CheckList.CNP_NAME = [];
    $scope.CheckList.Country = [];
    $scope.CheckList.City = [];
    $scope.CheckList.Location = [];
    $scope.CheckList.Inspection = [];
    $scope.CheckList.OVERALL_CMTS = [];
    $scope.SelectedValues = [];
    $scope.TotalData = [];
    $scope.DraftData = [];
    $scope.SelectedDraftLocation = {};
    rowData = [];
    MultiUploadData = [];
    var count = 0;

    ///////////File Upload Grid

    $scope.columnDefs = [
        { headerName: "Selected File", field: "Name", cellClass: "grid-align", width: 220 },
        //{ headerName: "Image", field: "file", cellClass: "grid-align", width: 250 },
        {
            headerName: "Download", field: "Name", width: 130, cellRenderer: function (params) {
                return '<a download=' + params.data.Name + ' href="' + params.data.FilePath + '"><span class="glyphicon glyphicon-download"></span></a>';
            }
        },
        { headerName: "Remove", template: '<a ng-click="Remove(this)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 130 }
    ];

    var rowData = [];
    var data;
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        rowData: rowData,
        angularCompileRows: true
    };

    $scope.Remove = function (node) {
        $('#UPLFILE').val("");
        var SelectedRow = _.find(rowData, function (x) { if (x.Name == node.data.Name) return x });
        var SelectedIndex = _.indexOf(rowData, SelectedRow);
        rowData.splice(SelectedIndex, 1);
        SelectedRow = _.find(MultiUploadData, function (x) { if (x.Imagepath == node.data.Name) return x });
        SelectedIndex = _.indexOf(MultiUploadData, SelectedRow);
        MultiUploadData.splice(SelectedIndex, 1);
        if ($scope.gridOptions.rowData.length > 0) {
            var Files = $scope.gridOptions.rowData.length + " Files";
            $('#UPLFILE').text(Files); 
        }
        $scope.gridOptions.api.setRowData(rowData);
    };

    ///////////////////////////

    $scope.clear = function () {

        $("input:radio").removeAttr("checked");
        $("input:text").val("");
        $("textarea").each(function () {
            $(this).val("");
        });
        $scope.CheckList.SVR_FROM_DATE = "";
        angular.forEach($scope.CNP_NAME, function (CNP_NAME) {
            CNP_NAME.ticked = false;
        });

        angular.forEach($scope.Country, function (Country) {
            Country.ticked = false;
        });
        angular.forEach($scope.City, function (City) {
            City.ticked = false;
        });
        angular.forEach($scope.Location, function (Location) {
            Location.ticked = false;
        });
        angular.forEach($scope.Inspection, function (Inspection) {
            Inspection.ticked = false;
        });
        angular.forEach($scope.SelectedValues, function (SelectedValues) {
            SelectedValues.ticked = false;
        });
        $scope.SelectedValues = [];

        rowData = [];
        MultiUploadData = [];
        $scope.gridOptions.api.setRowData(rowData);
        $('#UPLFILE').val("");
    }

    var MultiUploadData = [];

    $scope.fileNameChanged = function (fu) {
        if (rowData.length === 0) {
            rowData = [];
        }
        data = new FormData($('form')[0]);
        for (i = 0; i < fu.files.length; i++) {
            var gridLength = $scope.gridOptions.rowData.length;
            if (gridLength > 0) {
                for (j = 1; j <= gridLength; j++) {
                    //rowData.splice(0, 1);
                    $scope.gridOptions.api.setRowData(rowData);
                }
            }
            rowData.push({ Name: fu.files[i].name, file: fu.files[i] });
            MultiUploadData.push({ Imagepath: fu.files[i].name });
            $scope.gridOptions.api.setRowData(rowData);
        }
    };

    CheckListCreationService.getCompany().then(function (response) {
        if (response.data != null) {
            $scope.CNP_NAME = response.data;
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });


    CheckListCreationService.getLocations().then(function (response) {
        if (response.data != null) {
            $scope.Location = response.data;
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });

    $scope.GetDrafts = function () {
        CheckListCreationService.getSavedList().then(function (response) {
            if (response.data != null) {
                $scope.CheckList.SaveList = response.data;

            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }


    $scope.GetLocationCode = function (items) {
        $scope.clear();
        $scope.SelectedDraftLocation = { LcmCode: items.LCM_CODE, InspectdDate: items.BCL_SELECTED_DT };
        $scope.LoadData();
        $scope.CheckList.SVR_FROM_DATE = $filter('date')(items.BCL_SELECTED_DT, "yyyy-MM-dd");

        //rowData = [];
        //MultiUploadData = [];
        //for (var g = 0; g < data.data[1].length; g++) {
        //    rowData.push({
        //        Name: $scope.UploadedData[g].Name, FilePath: "https://live.quickfms.com/UploadFiles/" + tenant + "/ProjectCheckList/" + $scope.UploadedData[g].Name
        //    });
        //    MultiUploadData.push({ Imagepath: $scope.UploadedData[g].Name });
        //}
        //$scope.gridOptions.api.setRowData(rowData);
        

        var lcm = _.find($scope.Location, { LCM_CODE: items.LCM_CODE });
        if (lcm != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    lcm.ticked = true;
                    $scope.CheckList.Location.push(lcm);
                });
            }, 100)
        }


        var cny = _.find($scope.Country, { CNY_CODE: items.CNY_CODE });

        if (cny != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    cny.ticked = true;
                    $scope.CheckList.Country.push(cny);
                });
            }, 100)
        }

        var cty = _.find($scope.City, { CTY_CODE: items.CTY_CODE });
        if (cty != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    cty.ticked = true;
                    $scope.CheckList.City.push(cty);
                });
            }, 100)
        }

        var CNP = _.find($scope.CNP_NAME, { CNP_ID: items.LCM_CNP });
        if (CNP != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    CNP.ticked = true;
                    $scope.CheckList.CNP_NAME.push(CNP);
                });
            }, 100)
        }

        var CMTS = items.BCL_OVERALL_CMTS  /*_.find($scope.OVERALL_CMTS, { OVERALL_CMTS: items.BCL_OVERALL_CMTS });*/
        if (CMTS != undefined) {
            //setTimeout(function () {
            //    $scope.$apply(function () { CMTS.ticked = true;
            //        $scope.CheckList.OVERALL_CMTS.push(CMTS);
            //    });
            //}, 100)
            $scope.CheckList.OVERALL_CMTS = CMTS;
        }
        console.log($scope.CheckList);
    }

    UtilityService.getCountires(2).then(function (response) {
        if (response.data != null) {
            $scope.Country = response.data;

            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.City = response.data;

                }
            });
        }
    });


    $scope.CnyChangeAll = function () {
        $scope.CheckList.Country = $scope.Country;
        $scope.CnyChanged();
    }

    $scope.CnySelectNone = function () {
        $scope.CheckList.Country = [];
        $scope.CnyChanged();
    }

    $scope.CnyChanged = function () {
        if ($scope.CheckList.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.CheckList.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.CheckList.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.CheckList.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        $scope.CheckList.Country = [];

        console.log($scope.CheckList.City);
        CheckListCreationService.LocationByCity($scope.CheckList.City).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        //UtilityService.getLocationsByCity($scope.CheckList.City, 2).then(function (response) {
        //    if (response.data != null)
        //        $scope.Location = response.data;
        //    else
        //        $scope.Location = [];
        //});

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.CheckList.Country.push(cny);
            }
        });
    }

    /// Location Events
    $scope.LcmChangeAll = function () {
        $scope.CheckList.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.CheckList.Location = [];
        $scope.LcmChanged();
    }
    $scope.LcmChanged = function () {

        $scope.CheckList.Country = [];
        $scope.CheckList.City = [];

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.CNP_NAME, function (value, key) {
            value.ticked = false;
        })

        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.CheckList.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.CheckList.City.push(cty);
            }
        });

        angular.forEach($scope.Location, function (value, key) {
            var cnp = _.find($scope.CNP_NAME, { CNP_ID: value.LCM_CNP });
            if (cnp != undefined && value.ticked == true && cnp.ticked == false) {
                cnp.ticked = true;
                $scope.CheckList.CNP_NAME.push(cnp);
            }
        });

        angular.forEach($scope.Location, function (value, key) {
            var CMTS = _.find($scope.OVERALL_CMTS, { OVERALL_CMTS: value.OVERALL_CMTS });
            if (CMTS != undefined && value.ticked == true && CMTS.ticked == false) {
                CMTS.ticked = true;
                $scope.CheckList.OVERALL_CMTS.push(CMTS);
            }
        });
    }

    CheckListCreationService.getInspectors().then(function (response) {
        if (response.data != null) {
            $scope.Inspection = response.data;
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });

    var sb;
    $scope.LoadData = function () {

        CheckListCreationService.GetGriddata($scope.SelectedDraftLocation).then(function (response) {

            var Ins = _.find($scope.Inspection, { INSPECTOR: response.data[0].BCL_INSPECTED_BY });
            if (Ins != undefined) {
                setTimeout(function () {
                    $scope.$apply(function () {
                        Ins.ticked = true;
                        $scope.CheckList.Inspection.push(Ins);
                    });
                }, 100)
            }
            console.log(response);

            var z = 1;
            if (response != null) {
                $scope.TotalData = response.data;
                var table_body = '<div class="container"><div class="table-responsive"><table class="table table-bordered" id="example"><thead><tr><th class="col-md-3 col-sm-6 col-xs-12"><b>Category</th></b><th class="col-md-3 col-sm-6 col-xs-12"><b>SubCategory</b></th><th class="col-md-3 col-sm-6 col-xs-12"><b>Working Condition</b></th></tr></thead><tbody>';

                for (i = 0; i < response.data.length; i++) {
                    var array = response.data[i]["SCORE"].split(",");
                    var multiplearray = '';
                    var multiplescorecode = '';
                    if (response.data[i]["BCLD_SCORE_NAME"] != null && response.data[i]["BCLD_SCORE_NAME"] != "") {
                        multiplearray = response.data[i]["BCLD_SCORE_NAME"].split(",");
                        multiplescorecode = response.data[i]["BCLD_SCORE_CODE"].split(",");
                    }

                    if (response.data[i]["BCLD_DATE"] != null) {
                        var multiplescoredate = response.data[i]["BCLD_DATE"].split(",");
                    }
                    table_body += '<tr>';

                    table_body += '<td style="display:none;" class="col-xs-3">';
                    table_body += response.data[i]["BCL_MC_CODE"];
                    table_body += '</td>';

                    table_body += '<td class="col-xs-3 Fonts">';
                    table_body += response.data[i]["BCL_MC_NAME"];
                    table_body += '</td>';

                    table_body += '<td style="display:none;" class="col-xs-3">';
                    table_body += response.data[i]["BCL_CH_SUB_CODE"];
                    table_body += '</td>';

                    table_body += '<td class="col-xs-3 Fonts">';
                    table_body += response.data[i]["BCL_SUB_NAME"];
                    table_body += '</td>';

                    table_body += '<td style="display:none;" class="col-xs-3">';
                    table_body += response.data[i]["BCL_SCORE_FLAG"];
                    table_body += '</td>';


                    table_body += '<td class="col-xs-6" id="txtRadio">';
                    $.each(array, function (j) {
                        if (response.data[i]["BCL_SUB_TYPE"] == 'Radio') {
                            table_body += '<input type="' + response.data[i]["BCL_SUB_TYPE"] + '" id=Score' + i + ' name=Score' + i + '  value="' + array[j] + '" ';

                            if (response.data[i].BCLD_SCORE_CODE == array[j] && response.data[i].BCLD_FLAG == 1) {
                                table_body += 'checked="checked">' + array[j];
                                sb = response.data[i].BCLD_SUB_SCORE
                                $("#Score" + i + "").data('events', response.data[i].BCLD_SCORE_CODE).trigger("click");

                            }

                            else {
                                table_body += '>' + array[j];

                            }
                        }
                        else if (response.data[i]["BCL_SUB_TYPE"] == 'Multiple') {
                            count = 0;
                            var textboxid = '<ul class="list-group"><li class="list-group-item">' + array[j] + '<input type="Text" id=' + i + 'textbox' + j + '  name="' + array[j] + '" style="width:30px;"'
                            var dateid = ' Expiry Date <b>:</b> <input type="Date"  id=' + i + 'Date' + j + '  name=Date' + z + ' style="width:139px;"'
                            if (multiplearray != undefined && multiplearray != '') {
                                if (j < array.length && multiplearray.length >= 1) {
                                    $.each(multiplearray, function (k) {

                                        if (array[j] == multiplearray[k] && response.data[i].BCLD_FLAG == 1) {
                                            table_body += textboxid + 'value="' + multiplescorecode[k] + '"/>'
                                            $("#" + i + "textbox" + j + "").data('events', multiplescorecode[k]).trigger("change");
                                            table_body += dateid + 'value="' + $filter('date')(multiplescoredate[k], "yyyy-MM-dd") + '"/>'
                                            $("#" + i + "Date" + j + "").data('events', multiplescoredate[k]).trigger("change");
                                            table_body += '<input type="Radio" id="rdScore" value="Green" name=Score' + z + '/>Green'
                                            table_body += '<input type="Radio" id="rdScore" value="Red" name=Score' + z + '/>Red' + '</li></ul>'
                                            z++;
                                            count = count + 1;
                                        }
                                       
                                    });
                                    if (count == "0") {
                                        table_body += textboxid + '/>'
                                        table_body += dateid + '/>'
                                        table_body += '<input type="Radio" id="rdScore" value="Green" name=Score' + z + '/>Green'
                                        table_body += '<input type="Radio" id="rdScore" value="Red" name=Score' + z + '/>Red' + '</li></ul>'
                                        z++;
                                    }
                                }

                            }
                            else {
                                table_body += textboxid + '/>'
                                table_body += dateid + '/>'
                                table_body += '<input type="Radio" id="rdScore" value="Green" name=Score' + z + '/>Green'
                                table_body += '<input type="Radio" id="rdScore" value="Red" name=Score' + z + '/>Red' + '</li></ul>'
                                z++;
                            }
                        }
                        else if (response.data[i]["BCL_SUB_TYPE"] == 'File') {
                            table_body += '<input type="' + response.data[i]["BCL_SUB_TYPE"] + '" id="AttachFile" name=Score' + i + '  value="' + array[j] + '" accept=".png,.jpg,.xlsx,.pdf,.docx">' + array[j]
                        }
                        else {
                            count = 0;
                            var table_Input = '<ul><div class="row"><div class="col-md-3"><li>' + array[j] + '<b>  :</b></div><div class="col-md-6">' + '<input type="Text" onkeypress="AlphaNumeric(event)" id=textbox1' + i + ' name="' + array[j] + '"'
                            if (multiplearray != undefined && multiplearray != '') {
                                if (j < array.length && multiplearray.length >= 1) {
                                    $.each(multiplearray, function (l) {
                                        if (array[j] == multiplearray[l] && response.data[i].BCLD_FLAG == 1) {
                                            table_body += table_Input + 'value="' + multiplescorecode[l] + '">'
                                            table_body += '</li></div></div></ul>'
                                            $("#textbox1" + i + "").data('events', response.data[i].BCLD_SCORE_CODE).trigger("change");
                                            count = count + 1;
                                        }
                                        //else {
                                        //    if (count == "0") {
                                        //        table_body += table_Input + '></li></div></div></ul>'
                                        //        count = count + 1;
                                        //    }
                                        //}
                                    });

                                    if (count == "0") {
                                        table_body += table_Input + '></li></div></div></ul>'
                                    }
                                }
                                //count = count + 1;
                            }

                            else {
                                table_body += table_Input + '></li></div></div></ul>'
                            }
                            //}
                        }
                    });
                    if (response.data[i]["BCL_SUB_TYPE"] == 'Radio') {
                        table_body += '<textarea id="textarea' + i + '" name=color' + i + ''
                        if (response.data[i].BCLD_FLAG == 1 && response.data[i].BCLD_TEXTDATA != "") {
                            table_body += '>' + response.data[i].BCLD_TEXTDATA + '</textarea>'
                            $("#textarea" + i + "").data('events', response.data[i].BCLD_TEXTDATA).trigger("change");

                        }
                        else {
                            table_body += '></textarea>'

                        }


                    }
                    table_body += '</td>';
                    table_body += '<td style="display:none;" class="col-xs-3">';
                    table_body += response.data[i]["BCL_SUB_TYPE"];
                    table_body += '</td>';

                    table_body += '</tr>';

                }
                table_body += '</tbody></table></div>';
                $('#tableDiv').html(table_body);

            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            progress(0, '', false);
        }, function (error) {
            console.log(error);
        });
    }
    $scope.LoadData();

    var ScoreCode = ''
    var ScoreName = '';
    var SubScore = ''
    var txtdata = ''
    var Date = ''
    var SubScoreName = ''
    var SubDateName = ''
    var FilePath = ''
    var data;
    var rowData = [];
    $('#Form1').on('change', 'input[type=File]', function (fu) {
        var newDate = new window.Date();
        var time = newDate.getHours() + ":" + newDate.getMinutes() + ":" + newDate.getSeconds();

        data = new FormData($('form')[0]);
        for (i = 0; i < fu.target.files.length; i++) {
            rowData.push({ Name: fu.target.files[i].name, file: fu.target.files[i] });
            var currentRow = $(this).closest("tr");
            var Index = currentRow[0].rowIndex;
            var CatCode = currentRow.find("td:eq(0)").text();
            var SubcatCode = currentRow.find("td:eq(2)").text();
            var Subtype = currentRow.find("td:eq(6)").text();
            var FilePath = "~/UploadFiles/AdityaBirla.dbo/" + fu.target.files[i].name;
            var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index) return x });
            var ExistingIndex = _.indexOf($scope.SelectedValues, Exists);
            if (Exists != undefined) {
                $scope.SelectedValues.splice(ExistingIndex, 1);
                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": txtdata, "Date": Date, "SubScore": SubScore, "FilePath": FilePath, "ticked": "true", "Subtype": Subtype });
            }
            else
                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": txtdata, "Date": Date, "SubScore": SubScore, "FilePath": FilePath, "ticked": "true", "Subtype": Subtype });
        }

    })

    $scope.TextBoxChange = function (e, thisval) {

        var TextValue = thisval.data('events');
        var currentRow = thisval.closest("tr");
        var Index = currentRow[0].rowIndex;
        var CatCode = currentRow.find("td:eq(0)").text();
        var SubcatCode = currentRow.find("td:eq(2)").text();
        var Subtype = currentRow.find("td:eq(6)").text();
        var ScoreCode = thisval.val();

        if (TextValue != '' && TextValue != undefined) {
            ScoreCode = TextValue;
        }
        var ScoreName = e.target.name
        var SubScore = ''

        if (SubcatCode == "S21")
            var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index && x.ScoreName == ScoreName) return x });
        else
            var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index) return x });
        if (Exists != undefined) {

            var S = _.find($scope.SelectedValues, function (x) { if (x.row == Index && (x.ScoreName == ScoreName)) return x });
            var ExistingIndex = _.indexOf($scope.SelectedValues, S);
            if (ExistingIndex != -1)
                $scope.SelectedValues.splice(ExistingIndex, 1);


            if (S != undefined) {
                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": '', "Date": S.Date, "SubScore": S.SubScore, "SubScoreName": S.SubScoreName, "SubDateName": S.SubDateName, "ticked": "true", "Subtype": Subtype });
            }
            else {
                var T = _.find($scope.SelectedValues, function (x) { if (x.row == Index && (x.ScoreName == "" && (x.SubScoreName != "" || x.SubDateName != ""))) return x });
                var ExistingIndex = _.indexOf($scope.SelectedValues, T);
                if (ExistingIndex != -1)
                    $scope.SelectedValues.splice(ExistingIndex, 1);

                if (T != undefined) {
                    $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": '', "Date": T.Date, "SubScore": T.SubScore, "SubScoreName": T.SubScoreName, "SubDateName": T.SubDateName, "ticked": "true", "Subtype": Subtype });
                }


                else if (T == undefined) {
                    $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": '', "Date": '', "SubScore": SubScore, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });
                }

            }

        }
        else {
            $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": '', "Date": '', "SubScore": SubScore, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });
        }
        console.log($scope.SelectedValues)
    }


    $('#Form1').on('change', 'input[type=Text]', function (e) {

        $scope.TextBoxChange(e, $(this));

    });

    $scope.TextareaChange = function (e, thisval) {

        var Textarea = thisval.data('events');
        var currentRow = thisval.closest("tr");
        var Index = currentRow[0].rowIndex;
        var CatCode = currentRow.find("td:eq(0)").text();
        var SubcatCode = currentRow.find("td:eq(2)").text();
        var Subtype = currentRow.find("td:eq(6)").text();
        var ScoreCode = ''

        if (Textarea != '' && Textarea != undefined) {
            ScoreCode = Textarea;
        }
        txtdata = thisval.val();
        var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index) return x });
        var ExistingIndex = _.indexOf($scope.SelectedValues, Exists);
        if (Exists != undefined) {
            $scope.SelectedValues.splice(ExistingIndex, 1);
            $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": Exists.ScoreCode, "ScoreName": Exists.ScoreName, "txtdata": txtdata, "Date": Exists.Date, "SubScore": SubScore, "SubScoreName": Exists.SubScoreName, "SubDateName": Exists.SubDateName, "ticked": "true", "Subtype": Subtype });
        }
        else {
            $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": '', "txtdata": txtdata, "Date": Date, "SubScore": SubScore, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });
        }
        console.log($scope.SelectedValues)
    }

    $('#Form1').on('change', 'textarea', function (e) {

        $scope.TextareaChange(e, $(this));

    });

    $scope.DateChange = function (e, thisval) {

        var Datedfield = thisval.data('events');
        var currentRow = thisval.closest("tr");
        var Index = currentRow[0].rowIndex;
        var CatCode = currentRow.find("td:eq(0)").text();
        var SubcatCode = currentRow.find("td:eq(2)").text();
        var Subtype = currentRow.find("td:eq(6)").text();
        var SubScore = ''
        if (Datedfield != '' && Datedfield != undefined) {
            ScoreCode = Datedfield;
        }
        var SubDateName = ""
        Date = thisval.val();
        var SubDateName = e.target.name


        var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index) return x });

        if (Exists != undefined) {
            var EX = _.find($scope.SelectedValues, function (x) { if (x.row == Index && (x.SubDateName == SubDateName)) return x });
            var ExistingIndex = _.indexOf($scope.SelectedValues, EX);
            if (EX != undefined) {
                if (ExistingIndex != -1) {
                    $scope.SelectedValues.splice(ExistingIndex, 1);
                    $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": EX.ScoreCode, "ScoreName": EX.ScoreName, "txtdata": EX.txtdata, "Date": Date, "SubScore": EX.SubScore, "SubScoreName": EX.SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });;
                }
            }
            else {
                var EX1 = _.find($scope.SelectedValues, function (x) { if (x.row == Index && (x.SubDateName == "" && (x.SubScoreName != "" || x.ScoreName != ""))) return x });
                var ExistingIndex = _.indexOf($scope.SelectedValues, EX1);
                if (ExistingIndex != -1) {
                    $scope.SelectedValues.splice(ExistingIndex, 1);
                }
                if (EX1 != undefined) {
                    $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": EX1.ScoreCode, "ScoreName": EX1.ScoreName, "txtdata": EX1.txtdata, "Date": Date, "SubScore": EX1.SubScore, "SubScoreName": EX1.SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });;
                }
                else {
                    $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": txtdata, "Date": Date, "SubScore": SubScore, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });;
                }

            }

        }
        else {
            $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": txtdata, "Date": Date, "SubScore": SubScore, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });
        }
        console.log($scope.SelectedValues)
    }

    $('#Form1').on('change', 'input[type=Date]', function (e) {

        $scope.DateChange(e, $(this));

    });

    $scope.RadioButtonChange = function (e, thisval) {

        var subscoreval = thisval.data('events');
        var currentRow = thisval.closest("tr");
        var Index = currentRow[0].rowIndex;
        var CatCode = currentRow.find("td:eq(0)").text();
        var SubcatCode = currentRow.find("td:eq(2)").text();
        var Subtype = currentRow.find("td:eq(6)").text();
        var ScoreCode = thisval.val();
        if (subscoreval != '' && subscoreval != undefined) {
            ScoreCode = subscoreval;
        }
        SubScoreName = "";
        if (SubcatCode == 'S55' || SubcatCode == 'S14') {
            var SubScoreName = e.target.name
        }
        if (SubcatCode == 'S65' || SubcatCode == 'S66') {
            var ScoreName = e.target.name
        }
        var Existsubradio = _.find($scope.SelectedValues, function (x) { if (x.row == Index) return x });

        if (Existsubradio != undefined && SubScoreName != "ABC") {
            $('#LblRed1').remove();
            $('#LblRed2').remove();

        }

        if (SubcatCode == 'S14' && SubScoreName != "ABC") {
            if (sb == 'Yes') {
                $(thisval).closest("td").append('<label id="LblRed1"><input type="Radio" id="Red1" value="Yes"name="ABC" checked="checked">Yes</label>');
                $(thisval).closest("td").append('<label id="LblRed2"><input type="Radio" id="Red2" value="No" name="ABC">No</label>');

            }
            else if (sb == 'No') {
                $(thisval).closest("td").append('<label id="LblRed1"><input type="Radio" id="Red1" value="Yes"name="ABC">Yes</label>');
                $(thisval).closest("td").append('<label id="LblRed2"><input type="Radio" id="Red2" value="No" name="ABC" checked="checked">No</label>');

            }
            else {
                $(thisval).closest("td").append('<label id="LblRed1"><input type="Radio" id="Red1" value="Yes"name="ABC">Yes</label>');
                $(thisval).closest("td").append('<label id="LblRed2"><input type="Radio" id="Red2" value="No" name="ABC">No</label>');
            }

        }
        else
            if (SubcatCode == 'S65' && ScoreCode == 'Yes') {
                $(thisval).closest("td").append('<label id="YesDate"><input type="Date" id="DateS65"  name="Validity">Validity</label>');
            }
            else if (SubcatCode == 'S66' && ScoreCode == 'Yes') {
                $(thisval).closest("td").append('<label id="YesDate1"><input type="Date" id="Date1S66" name="Validity">Validity</label>');
            }
            else {
                if (SubcatCode == 'S65' && ScoreCode == 'No') {
                    $('#YesDate').remove();
                }
                if (SubcatCode == 'S66' && ScoreCode == 'No') {
                    $('#YesDate1').remove();
                }

            }
        if (SubcatCode == 'S55')
            var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index && x.SubScoreName == SubScoreName) return x });
        else
            var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index) return x });
        var ExistingIndex = _.indexOf($scope.SelectedValues, Exists);
        if (Exists != undefined) {
            if (ExistingIndex != -1)
                $scope.SelectedValues.splice(ExistingIndex, 1);
            if (e.target.id == "Red1" || e.target.id == "Red2" || SubcatCode == "S55") {
                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": Exists.ScoreCode, "ScoreName": Exists.ScoreName, "txtdata": txtdata, "Date": Exists.Date, "SubScore": ScoreCode, "SubScoreName": SubScoreName, "SubDateName": Exists.SubDateName, "ticked": "true", "Subtype": Subtype });
            }
            else {
                if (ScoreCode == "Yes")
                    $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": Exists.txtdata, "Date": Exists.Date, "SubScore": "", "SubScoreName": SubScoreName, "SubDateName": Exists.SubDateName, "ticked": "true", "Subtype": Subtype });
                else
                    $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": Exists.txtdata, "Date": "", "SubScore": "", "SubScoreName": SubScoreName, "SubDateName": Exists.SubDateName, "ticked": "true", "Subtype": Subtype });
            }

        }
        else {
            var Exists = _.find($scope.SelectedValues, function (x) { if (x.row == Index && (x.SubScoreName == "" && (x.ScoreName != "" || x.SubDateName != ""))) return x });
            var ExistingIndex = _.indexOf($scope.SelectedValues, Exists);
            if (Exists != undefined) {
                if (ExistingIndex != -1)
                    $scope.SelectedValues.splice(ExistingIndex, 1);
                $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": Exists.ScoreCode, "ScoreName": Exists.ScoreName, "txtdata": '', "Date": Exists.Date, "SubScore": ScoreCode, "SubScoreName": SubScoreName, "SubDateName": Exists.SubDateName, "ticked": "true", "Subtype": Subtype });
            }
            else {
                if (SubcatCode == "S55") {
                    $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": '', "ScoreName": '', "txtdata": '', "Date": '', "SubScore": ScoreCode, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });
                }
                else {
                    $scope.SelectedValues.push({ "row": Index, "CatCode": CatCode, "SubcatCode": SubcatCode, "ScoreCode": ScoreCode, "ScoreName": ScoreName, "txtdata": '', "Date": '', "SubScore": SubScore, "SubScoreName": SubScoreName, "SubDateName": SubDateName, "ticked": "true", "Subtype": Subtype });
                }

            }


        }

        console.log($scope.SelectedValues)

    }


    $('#Form1').on('change', 'input[type=Radio]', function (e) {


        $scope.RadioButtonChange(e, $(this));

    })

    $(document).ready(function () {
        $("#tableDiv").on("change", "textarea,input,input[type=Date]", function () {
            //var $selects = $(this).closest('tr').find('td select'),
            var currentRow = $(this).closest("tr");
            var SubcatCode = currentRow.find("td:eq(2)").text();
            var curr = $(this).val();
            var $textareas = $(this).closest('tr').find('td  textarea'),
            $cells = $(this).closest("tr").find("td input");
            $dates = $(this).closest('tr').find("td input[type=Date]")
            $cells.removeClass("has-error3");
            $textareas.removeClass("has-error2");
            //$selects.removeClass("has-error");
            $cells.each(function () {
                if ($(this).val().trim() !== '') {
                    $(this).addClass("has-error3");
                }
            });
            //$selects.each(function () {
            //    console.log($(this).val() == 'NA');
            //    if ($(this).val() == 'NA') {
            //        $(this).addClass("has-error");
            //    }
            //});

            $textareas.each(function () {
                if ($(this).val().trim() === '' && (curr === 'Needs work' || curr === 'New Required' || (curr === 'No' && SubcatCode != 'S68' && SubcatCode != 'S15') || (curr === 'Yes' && SubcatCode === 'S68') || (curr === 'Yes' && SubcatCode === 'S15'))) {
                    $(this).addClass("has-error2");
                }
            });


        });
    });

    $scope.Submit = function () {

        debugger;
        var table = document.getElementById('example'),
           rows = table.getElementsByTagName('tr'),
           i, j, cells, customerId, flagchk;

        for (i = 0, j = rows.length; i < j; ++i) {
            flagchk = true;
            cells = rows[i].getElementsByTagName('td');
            if (!cells.length) {
                continue;
            }
            for (var k = 0; k < $scope.SelectedValues.length; k++) {

                if ($scope.SelectedValues[k].Subtype == "Multiple" && (($scope.SelectedValues[k].SubScore == "") || ($scope.SelectedValues[k].Date == ""))) {
                    cells[5].classList.add("mystyle");
                    flagchk = true;
                    //$scope.SelectedValues.splice(k, 1);

                }
                else if ((($scope.SelectedValues[k].SubcatCode == "S65") || ($scope.SelectedValues[k].SubcatCode == "S66")) && ($scope.SelectedValues[k].Date == '' && $scope.SelectedValues[k].ScoreCode == "Yes")) {

                    cells[5].classList.add("mystyle");
                    flagchk = true;
                    // $scope.SelectedValues.splice(k, 1);

                }
                else if ((($scope.SelectedValues[k].SubcatCode == "S14") && ($scope.SelectedValues[k].SubScore == ""))) {

                    cells[5].classList.add("mystyle");
                    flagchk = true;
                    //$scope.SelectedValues.splice(k, 1);

                }
                else if ($scope.SelectedValues[k].row == i && $scope.SelectedValues.length != 0) {
                    cells[5].classList.remove("mystyle");
                    flagchk = false;
                }
            }
            if (flagchk) {
                cells[5].classList.add("mystyle");
            }

        }

        angular.forEach($scope.SelectedValues, function (x) {
            if (x.SubcatCode == "S65" || x.SubcatCode == "S66")
                x.ScoreName = "";

        });

        console.log($scope.SelectedValues.length);//70
        console.log($scope.TotalData.length);//69
        console.log(flagchk);//true

        if ($scope.TotalData.length <= $scope.SelectedValues.length && flagchk == false) {
            $scope.data = { LCMLST: $scope.CheckList.Location, InspectdBy: $scope.CheckList.Inspection[0].INSPECTOR, date: $scope.CheckList.SVR_FROM_DATE, SelCompany: $scope.CheckList.CNP_NAME[0].CNP_ID, Seldata: $scope.SelectedValues, Flag: 2 };


            CheckListCreationService.InsertCheckList($scope.data).then(function (response) {
                console.log(response);
                if (response != null) {
                    $.ajax({
                        type: "POST",
                        url: 'https://live.quickfms.com/api/CheckListCreation/UploadFiles',    // CALL WEB API TO SAVE THE FILES.                        
                        contentType: false,
                        processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                        cache: false,
                        data: data, 		        // DATA OR FILES IN THIS CONTEXT.
                        success: function (data, textStatus, xhr) {
                            showNotification('success', 8, 'bottom-right', response.Message);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert(textStatus + ': ' + errorThrown);
                        }
                    });
                    setTimeout(function () {
                        showNotification('success', 8, 'bottom-right', response.data);
                    }, 500);
                    $scope.clear();
                }
                progress(0, '', false);
            }, function (error) {
                console.log(error);
            });
        }
        else {
            showNotification('error', 8, 'bottom-right', "Please Enter  All The Fields ");
        }
    }
    $scope.Save = function () {
        console.log($scope.SelectedValues)
        angular.forEach($scope.SelectedValues, function (x) {
            if (x.SubcatCode == "S65" || x.SubcatCode == "S66")
                x.ScoreName = "";
        });
        $scope.data = {
            LCMLST: $scope.CheckList.Location,
            InspectdBy: $scope.CheckList.Inspection[0].INSPECTOR,
            date: $scope.CheckList.SVR_FROM_DATE,
            SelCompany: $scope.CheckList.CNP_NAME[0].CNP_ID,
            Seldata: $scope.SelectedValues,
            Flag: 1,
            OVERALL_CMTS: $scope.CheckList.OVERALL_CMTS,
            //OVERALL_CMTS: $scope('#OVER_CMTS').val(),
            imagesList: MultiUploadData
        };
        console.log($scope.data);
        CheckListCreationService.InsertCheckList($scope.data).then(function (response) {
            console.log(response);
            if (response != null) {
                $.ajax({
                    type: "POST",
                    url: 'https://live.quickfms.com/api/CheckListCreation/UploadFiles',    // CALL WEB API TO SAVE THE FILES.                        
                    contentType: false,
                    processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                    cache: false,
                    data: data, 		        // DATA OR FILES IN THIS CONTEXT.
                    success: function (data, textStatus, xhr) {
                        showNotification('success', 8, 'bottom-right', response.Message);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus + ': ' + errorThrown);
                    }
                });
                setTimeout(function () {
                    showNotification('success', 8, 'bottom-right', response.data);
                }, 500);
                $scope.clear();
            }
            progress(0, '', false);
        }, function (error) {
            console.log(error);
        });
        $scope.GetDrafts();
    }
    $scope.GetDrafts();
});