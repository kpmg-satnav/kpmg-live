﻿app.service("CheckListReportService", function ($http, $q, UtilityService) {
    this.getInspectors = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CheckListReport/getInspectors')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
    this.getImage = function (param) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListReport/GetImage', param)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    };
    this.LoadGrid = function (BCLDetails) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CheckListReport/LoadGrid', BCLDetails)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});
app.controller('CheckListReport', function ($scope, $q, $http, CheckListReportService, UtilityService, CheckListCreationService, $timeout, $filter) {
    $scope.BCL_Report = {};
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.Location = [];
    $scope.Country = [];
    $scope.City = [];
    UtilityService.getCountires(2).then(function (response) {
        if (response.data != null) {
            $scope.Country = response.data;
            angular.forEach($scope.Country, function (value, key) {
                value.ticked = true;
            });
            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.City = response.data;
                    angular.forEach($scope.City, function (value, key) {
                        value.ticked = true;
                    });
                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Location = response.data;
                            angular.forEach($scope.Locations, function (value, key) {
                                value.ticked = true;
                            });
                        }
                    });
                }
            });
        }
    });
    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.BCL_Report.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.cnySelectAll = function () {
        $scope.BCL_Report.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }
    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.BCL_Report.City, 2).then(function (response) {
            $scope.Location = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.BCL_Report.Country[0] = cny;
            }
        });
    }
    //$scope.ctySelectAll = function () {
    //    $scope.BCL_Report.City = $scope.City;
    //    $scope.getLocationsByCity();
    //}

    //$scope.cnySelectNone = function () {
    //    $scope.City = [];
    //    $scope.Location = [];
    //}
    //$scope.ctySelectNone = function () {
    //    $scope.Location = [];
    //}
    CheckListCreationService.getInspectors().then(function (response) {
        if (response.data != null) {
            $scope.Inspection = response.data;
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });
    UtilityService.getLocations(1).then(function (response) {
        if (response.data != null) {
            $scope.Location = response.data;
            angular.forEach($scope.Location, function (value, key) {
                value.ticked = true;
            })
        }
    });
    $scope.Pageload = function () {
        var params = {
            LCMLST: $scope.BCL_Report.Location,
            CNPLST: $scope.BCL_Report.CNP_NAME,
            FromDate: $scope.BCL_Report.FromDate,
            ToDate: $scope.BCL_Report.ToDate
        };
        CheckListReportService.LoadGrid(params).then(function (data) {
            $scope.gridata = data;
            if ($scope.gridata == null) {
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData($scope.gridata);
                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });
                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, false);                cols = [];                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }                $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
            }
            progress(0, '', false);
            $scope.LoadAllImg();
        });
    }
    setTimeout(function () {
        CheckListCreationService.getCompany().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.BCL_Report.CNP_NAME = $scope.Company;
                angular.forEach($scope.Company, function (value, key) {
                    value.ticked = true;
                })
                $scope.Pageload();
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });

    }, 1000);

    $scope.selVal = "30";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'THISMONTH':
                $scope.BCL_Report.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.BCL_Report.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'TODAY':
                $scope.BCL_Report.FromDate = moment().format('MM/DD/YYYY');
                $scope.BCL_Report.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.BCL_Report.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.BCL_Report.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.BCL_Report.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.BCL_Report.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.BCL_Report.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.BCL_Report.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.BCL_Report.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.BCL_Report.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }
    $scope.columnDefs = [
            { headerName: "Country", field: "CNY_NAME", width: 70, cellClass: 'grid-align', width: 210 },
            { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align', width: 210 },
            { headerName: "Location Code", field: "LCM_CODE", cellClass: 'grid-align', width: 200 },
            { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 200 },
            { headerName: "Inspected Company", field: "SELECTED_CNP", cellClass: 'grid-align', width: 200 },
            { headerName: "Inspected By", field: "BCL_INSPECTED_BY", cellClass: 'grid-align', width: 200 },
            { headerName: "Inspection Date", field: "BCL_SELECTED_DT", template: '<span>{{data.BCL_SELECTED_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 200 },
            { headerName: "Main Category", field: "BCL_MC_NAME", cellClass: 'grid-align', width: 200 },
            { headerName: "Sub Category", field: "BCL_SUB_NAME", cellClass: 'grid-align', width: 200 },
            { headerName: "Field", field: "BCLD_SCORE_NAME", cellClass: 'grid-align', width: 200 },
            { headerName: "Score", field: "BCLD_SCORE_CODE", cellClass: 'grid-align', width: 200 },
            { headerName: "Comments", field: "BCLD_TEXTDATA", cellClass: 'grid-align', width: 200 },
            { headerName: "Validity", field: "BCLD_DATE", template: '<span>{{data.BCLD_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 200 },
            { headerName: "Created Date", field: "BCL_CREATED_DT", template: '<span>{{data.BCL_CREATED_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 200 },
            { headerName: "Created By", field: "BCL_CREATED_BY", cellClass: 'grid-align', width: 200 }];

    $scope.columnDefs1 = [
                { headerName: "Country", field: "CNY_NAME", width: 150, cellClass: 'grid-align' },
                { headerName: "City", field: "CTY_NAME", width: 150, cellClass: 'grid-align' },
                { headerName: "Location", field: "LCM_NAME", width: 250, cellClass: 'grid-align' },
                { headerName: "Inspected By", field: "BCL_INSPECTED_BY", width: 100, cellClass: 'grid-align' },
                { headerName: "Inspected DT", field: "INSPECTED_DT", width: 100, cellClass: 'grid-align' },
                { headerName: "Image/File Name", field: "BCLD_FILE_UPLD", suppressMenu: true, width: 200, cellClass: 'grid-align' }
    ];
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [
            { title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" },
            { title: "Location Code", key: "LCM_CODE" },
            { title: "Main Category", key: "BCL_MC_NAME" }, { title: "Sub Category", key: "BCL_SUB_NAME" },
         { title: "Score", key: "BCL_SCORE_NAME" }, { title: "Inspected By", key: "BCL_INSPECTED_BY" }, { title: "Inspection Date", key: "BCL_SELECTED_DT" },
         { title: "Inspected Company", key: "SELECTED_CNP" }, { title: "Created Date", key: "BCL_CREATED_DT" }, { title: "Created By", key: "BCL_CREATED_BY" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("CheckListReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions1 = {
        columnDefs: $scope.columnDefs1,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        groupHideGroupColumns: true,
        suppressHorizontalScroll: true,
        onReady: function () {
            $scope.gridOptions1.api.sizeColumnsToFit()
        }
    };
      $scope.myFunction=function() {
        var checkBox = document.getElementById("myCheck");
        var text = document.getElementById("text");
        if (checkBox.checked == true) {
            text.style.display = "none";
            text1.style.display = "block";
            //$scope.LoadAllImg();
        } else {

            text.style.display = "block";
            text1.style.display = "none";
        }
    }
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupHideGroupColumns: true,

        groupColumnDef: {
            headerName: "Country", field: "CNY_NAME",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };
    $scope.LoadAllImg = function () {
        var param1 = {
            LCMLST: $scope.BCL_Report.Location,
            INSPCTED_BY: 1,
            INSPCTED_DT: '01/01/1900'
        };
        console.log(param1)
        CheckListReportService.getImage(param1).then(function (data) {
            console.log(data);
            $scope.gridata1 = data;
            if ($scope.gridata1 == null) {
                $scope.GridVisiblity = true;
                $scope.gridOptions1.api.setRowData([]);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions1.api.setRowData($scope.gridata1);
                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });
                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions1.columnApi.setColumnsVisible(cols, false);                cols = [];                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }                $scope.gridOptions1.columnApi.setColumnsVisible(cols, true);
            }
            progress(0, '', false);
        });
    }
    $scope.LoadImg = function () {
        var param = {
            LCMLST: $scope.BCL_Report.Location,
            INSPCTED_BY: $scope.BCL_Report.InspctdBy[0].INSPECTOR,
            INSPCTED_DT: $scope.BCL_Report.InspctdDT
        };
        CheckListReportService.getImage(param).then(function (data) {
            console.log(data);
            $scope.gridata1 = data;
            if ($scope.gridata1 == null) {
                $scope.GridVisiblity = true;
                $scope.gridOptions1.api.setRowData([]);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions1.api.setRowData($scope.gridata1);
                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });
                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions1.columnApi.setColumnsVisible(cols, false);                cols = [];                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }                $scope.gridOptions1.columnApi.setColumnsVisible(cols, true);
            }
            progress(0, '', false);
        });
    }

    $scope.GenReport = function (BCL_Report, Type) {
        progress(0, 'Loading...', true);
        var dataobj = {
            LCMLST: $scope.BCL_Report.Location,
            CNPLST: $scope.BCL_Report.CNP_NAME,
            FromDate: $scope.BCL_Report.FromDate,
            ToDate: $scope.BCL_Report.ToDate,
            Type: Type
        };
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            console.log(dataobj)
            $http({
                url: UtilityService.path + '/api/CheckListReport/ExportGrid',
                method: 'POST',
                data: dataobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'CheckListReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "CheckListReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
});