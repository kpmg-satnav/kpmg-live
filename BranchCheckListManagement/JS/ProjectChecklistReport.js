﻿app.service("ProjectCheckListReportService", function ($http, $q, UtilityService) {
    this.getInspectors = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ProjectCheckListReport/getInspectors')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
    this.LoadGrid = function (PCLDetails) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ProjectCheckListReport/LoadGrid', PCLDetails)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getImage = function (param) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ProjectCheckListReport/GetImage', param)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    };
});

app.controller('ProjectCheckListReport', function ($scope, $q, $http, ProjectCheckListReportService, UtilityService, $timeout, $filter) {
    $scope.PCL_Report = {};
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.Location = [];
    UtilityService.getLocations(1).then(function (response) {
        if (response.data != null) {
            $scope.Location = response.data;
            angular.forEach($scope.Location, function (value, key) {
                value.ticked = true;
            })
        }
    });
    setTimeout(function () {
        progress(0, 'Loading...', true);
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                console.log(CompanySession);
                $scope.Company = response.data;
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.PCL_Report.CNP_NAME.push(a);
                });
                if (CompanySession == "1") { $scope.CompanyVisible = 0; }
                else { $scope.CompanyVisible = 1; }
                $scope.Pageload();
            }
            progress(0, 'Loading...', false);
        });
    }, 500);
    $scope.myFunction = function () {
        var checkBox = document.getElementById("myCheck");
        var text = document.getElementById("text");
        if (checkBox.checked == true) {
            text.style.display = "none";
            text1.style.display = "block";
            $scope.LoadAllImg();
        } else {

            text.style.display = "block";
            text1.style.display = "none";
        }
    }

    $scope.selVal = "THISMONTH";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'THISMONTH':
                $scope.PCL_Report.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.PCL_Report.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'TODAY':
                $scope.PCL_Report.FromDate = moment().format('MM/DD/YYYY');
                $scope.PCL_Report.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.PCL_Report.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.PCL_Report.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.PCL_Report.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.PCL_Report.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.PCL_Report.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.PCL_Report.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.PCL_Report.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.PCL_Report.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }
    $scope.columnDefs = [
        { headerName: "Country", field: "CNY_NAME", width: 70, cellClass: 'grid-align', width: 210 },
        { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align', width: 210 },
        { headerName: "Location Code", field: "LCM_CODE", cellClass: 'grid-align', width: 200 },
        { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Project Name", field: "PM_CL_MAIN_PRJ_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Inspected By", field: "PM_CL_MAIN_INSP_BY", cellClass: 'grid-align', width: 200 },
        { headerName: "Inspection Date", field: "PM_CL_MAIN_VISIT_DT", template: '<span>{{data.PM_CL_MAIN_VISIT_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 200 },
        { headerName: "Main Category", field: "PM_CL_CAT_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Sub Category", field: "PM_CL_SUBCAT_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Status", field: "PM_CL_CHILDCAT_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Comments", field: "PM_CLD_CHILDCAT_VALUE", cellClass: 'grid-align', width: 200 },
        { headerName: "No. of Quantity", field: "PM_CL_SCC_NAME", cellClass: 'grid-align', width: 200 }
    ];
        $scope.columnDefs1 = [
                { headerName: "Country", field: "CNY_NAME", width: 150, cellClass: 'grid-align' },
                { headerName: "City", field: "CTY_NAME", width: 150, cellClass: 'grid-align' },
                { headerName: "Location", field: "LCM_NAME", width: 250, cellClass: 'grid-align' },
                { headerName: "Inspected By", field: "BCL_INSPECTED_BY", width: 100, cellClass: 'grid-align' },
                { headerName: "Inspected DT", field: "INSPECTED_DT", width: 100, cellClass: 'grid-align' },
                { headerName: "Image/File Name", field: "BCLD_FILE_UPLD", suppressMenu: true, width: 200, cellClass: 'grid-align' }
        ];

        $scope.GenerateFilterPdf = function () {
            progress(0, 'Loading...', true);
            var columns = [
                { title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" },
                { title: "Location Code", key: "LCM_CODE" }, { title: "Project Name", key: "PM_CL_MAIN_PRJ_NAME" },
                { title: "Main Category", key: "PM_CL_CAT_NAME" }, { title: "Sub Category", key: "PM_CL_SUBCAT_NAME" },
                { title: "Child Category", key: "PM_CL_CHILDCAT_NAME" }, { title: "Inspected By", key: "PM_CL_MAIN_INSP_BY" }, { title: "Inspection Date", key: "PM_CL_MAIN_VISIT_DT" },
                { title: "Child Category Value", key: "PM_CLD_CHILDCAT_VALUE" }, { title: "Sub Child Category", key: "PM_CL_SCC_NAME" }];
            var model = $scope.gridOptions.api.getModel();
            var data = [];
            model.forEachNodeAfterFilter(function (node) {
                data.push(node.data);
            });
            var jsondata = JSON.parse(JSON.stringify(data));
            var doc = new jsPDF("landscape", "pt", "a4");
            doc.autoTable(columns, jsondata);
            doc.save("CheckListReport.pdf");
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions1 = {
        columnDefs: $scope.columnDefs1,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        groupHideGroupColumns: true,
        suppressHorizontalScroll: true,
        onGridReady: function sizeToFit() {
            gridOptions1.api.sizeColumnsToFit();

        }
    };
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "CNY_NAME",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };
    ProjectCheckListReportService.getInspectors().then(function (response) {
        if (response.data != null) {
            $scope.Inspection = response.data;
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });
    $scope.LoadAllImg = function () {
        var param1 = {
            LCMLST: $scope.PCL_Report.Location,
            INSPCTED_BY: 1,
            INSPCTED_DT: '01/01/1900'
        };
        ProjectCheckListReportService.getImage(param1).then(function (data) {
            console.log(data);
            $scope.gridata1 = data;
            if ($scope.gridata1 == null) {
                $scope.GridVisiblity = true;
                $scope.gridOptions1.api.setRowData([]);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions1.api.setRowData($scope.gridata1);
                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });
                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions1.columnApi.setColumnsVisible(cols, false);                cols = [];                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }                $scope.gridOptions1.columnApi.setColumnsVisible(cols, true);
            }
            progress(0, '', false);
        });
   } 
    $scope.LoadImg = function () {
        var param = {
            LCMLST: $scope.PCL_Report.Location,
            INSPCTED_BY: $scope.PCL_Report.InspctdBy[0].INSPECTOR,
            INSPCTED_DT: $scope.PCL_Report.InspctdDT
        };
        ProjectCheckListReportService.getImage(param).then(function (data) {
            console.log(data);
            $scope.gridata1 = data;
            if ($scope.gridata1 == null) {
                $scope.GridVisiblity = true;
                $scope.gridOptions1.api.setRowData([]);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions1.api.setRowData($scope.gridata1);
                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });
                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions1.columnApi.setColumnsVisible(cols, false);                cols = [];                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }                $scope.gridOptions1.columnApi.setColumnsVisible(cols, true);
            }
            progress(0, '', false);
        });
    }

    $scope.Pageload = function () {
        var params = {
            LCMLST: $scope.PCL_Report.Location,
            CNPLST: $scope.PCL_Report.CNP_NAME,
            FromDate: $scope.PCL_Report.FromDate,
            ToDate: $scope.PCL_Report.ToDate
        };
        ProjectCheckListReportService.LoadGrid(params).then(function (data) {
            $scope.gridata = data;
            if ($scope.gridata == null) {
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData($scope.gridata);
                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });
                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
                cols = [];
                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
            }
            progress(0, '', false);
        });
    }
   
    $scope.GenReport = function (PCL_Report, Type) {
        progress(0, 'Loading...', true);
        var dataobj = {
            LCMLST: $scope.PCL_Report.Location,
            CNPLST: $scope.PCL_Report.CNP_NAME,
            FromDate: $scope.PCL_Report.FromDate,
            ToDate: $scope.PCL_Report.ToDate,
            Type: Type
        };

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            console.log(dataobj)
            $http({
                url: UtilityService.path + '/api/ProjectCheckListReport/ExportGrid',
                method: 'POST',
                data: dataobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'ProjectCheckListReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }


    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "ProjectCheckListReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
});