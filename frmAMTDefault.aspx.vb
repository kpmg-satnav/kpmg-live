Imports System.Data
Imports System.Data.SqlClient
Imports System.Security
Imports System.Web.Security
Imports System.Configuration.ConfigurationManager
Imports System.Exception
'Imports AmantraAxis
Imports SqlHelper

Partial Class frmAMTDefault
    Inherits System.Web.UI.Page

#Region "Variables"

    Dim objdata As SqlDataReader
    Dim strTpwd, uid, pwd As String
    Dim strSql As String = String.Empty
    Dim strADID As String = String.Empty
    Dim strUsername As String = String.Empty
    Dim strclass As clsQueries
    Dim idrole, roles As String
    Dim strlogged As String = String.Empty
    Dim strQuery As String
    Dim iQuery As Integer
    Dim iQuery1 As Integer
    Dim COUNT As Integer

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                Dim UID As String = Session("uid")
            End If


            ' Session("uname") = "ManNay"
            If Session("uname") <> String.Empty Then
                Label1.Text = Session("uname").ToString()
                strQuery = "select count(aur_id) from " & Session("TENANT") & "." & "amantra_user where ad_id =  '" & Label1.Text.Trim() & "'"
                iQuery1 = SqlHelper.ExecuteScalar(CommandType.Text, strQuery)
                If iQuery1 > 0 Then
                    strQuery = "select aur_id from " & Session("TENANT") & "." & "amantra_user where ad_id =  '" & Label1.Text.Trim & "'"
                    objdata = SqlHelper.ExecuteReader(CommandType.Text, strQuery)
                    Do While objdata.Read
                        strADID = objdata("aur_id").ToString()
                        Session("uid") = strADID
                    Loop
                    objdata.Close()
                    strQuery = "Select USR_ACTIVE from " & Session("TENANT") & "." & "[USER]" & " WHERE USR_ID='" & strADID & "'"
                    objdata = SqlHelper.ExecuteReader(CommandType.Text, strQuery)
                    Dim strActive As String = ""
                    While (objdata.Read())
                        strActive = objdata("USR_ACTIVE").ToString()
                    End While
                    objdata.Close()
                    If (strActive = "N" Or strActive = "n") Then
                        Label1.Text = "De-Activated User ID!"
                        Exit Sub
                    End If
                    If Session("uid") <> String.Empty Then 'And Session("user") <> String.Empty Then
                        If validateUser(Session("uid").ToString()) Then
                            'If isMobileBrowser() = True Then
                            '    Dim script As String = String.Format("alert('{0}');", "hello")
                            '    Page.ClientScript.RegisterClientScriptBlock(Page.[GetType](), "alert", script, True)
                            '    strSql = ("WebFiles/MobileDetect.aspx?UserId=" + Session("UID") + "&CompanyId=" + Session("TENANT"))

                            'Else
                            Dim sp2 As SubSonic.StoredProcedure
                            Dim reader As IDataReader
                            sp2 = New SubSonic.StoredProcedure((HttpContext.Current.Session("TENANT")) & "." & "GET_MAP_FOR_EMP")
                            sp2.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session("UID"), DbType.String)
                            reader = sp2.GetReader()
                            If reader.Read Then
                                If reader("URL_ROL_ID") = 6 And reader("SYS_PRF") = 1 Then
                                    Response.Redirect(String.Format("WebFiles/frmDetailsKotak.aspx"))
                                Else
                                    strSql = ("WebFiles/frmDetails.aspx")
                                End If
                            Else
                                Response.Redirect("/ErrorMap.aspx")
                                'Response.Write("User " + HttpContext.Current.Session("UID") + " with this id not mapped to any floor")
                            End If
                            'End If
                        Else
                            'Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1))
                            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
                            'Response.Cache.SetNoStore()
                            'Session.Abandon()
                            strSql = ("login.aspx")
                        End If
                    End If
                Else
                    ' strSql = ("login.aspx")
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Default", "Load", exp)
        End Try
        If strSql <> String.Empty Then
            Response.Redirect(strSql)
        End If


    End Sub

    Public Function isMobileBrowser() As Boolean
        Dim context As HttpContext = HttpContext.Current

        If context.Request.Browser.IsMobileDevice Then
            Return True
        End If

        If context.Request.ServerVariables("HTTP_X_WAP_PROFILE") IsNot Nothing Then
            Return True
        End If

        If context.Request.ServerVariables("HTTP_ACCEPT") IsNot Nothing AndAlso context.Request.ServerVariables("HTTP_ACCEPT").ToLower().Contains("wap") Then
            Return True
        End If

        If context.Request.ServerVariables("HTTP_USER_AGENT") IsNot Nothing Then
            Dim mobiles As String() = {"midp", "j2me", "avant", "docomo", "novarra", "palmos", "palmsource", "240x320", "opwv", "chtml", "pda", "windows ce", "mmp/", "blackberry", "mib/", "symbian", "wireless", "nokia", "hand", "mobi", "phone", "cdm", "up.b", "audio", "SIE-", "SEC-", "samsung", "HTC", "mot-", "mitsu", "sagem", "sony", "alcatel", "lg", "eric", "vx", "NEC", "philips", "mmm", "xx", "panasonic", "sharp", "wap", "sch", "rover", "pocket", "benq", "java", "pt", "pg", "vox", "amoi", "bird", "compal", "kg", "voda", "sany", "kdd", "dbt", "sendo", "sgh", "gradi", "jb", "dddi", "moto", "iphone"}

            For Each s As String In mobiles

                If context.Request.ServerVariables("HTTP_USER_AGENT").ToLower().Contains(s.ToLower()) Then
                    Return True
                End If
            Next
        End If


        Return False
    End Function
    Public Function validateUser(ByVal strUser As String) As Boolean
        Dim strlogged As String = ""
        If strUser <> String.Empty Then
            strQuery = "select count(aur_id) from " & Session("TENANT") & "." & "amantra_user where aur_id =  '" & strUser & "'"
            iQuery = SqlHelper.ExecuteScalar(CommandType.Text, strQuery)
            Dim sp As New SqlParameter("@VC_USERID", SqlDbType.NVarChar, 50)
            sp.Value = Session("uid")
            objdata = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "USER_SP_LOGIN_CHECKLOGIN", sp)
            Do While objdata.Read
                strlogged = objdata("Usr_logged").ToString
                Session("uemail") = objdata("AUR_EMAIL").ToString
                ' Session("uname") = objdata("AUR_first_name").ToString & " " & objdata("AUR_middle_name").ToString & " " & objdata("AUR_last_name").ToString
                Session("bdg") = objdata("aur_bdg_id").ToString
                Dim uTime As Integer
                uTime = (DateTime.UtcNow - New DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds
                Session("LoginTime") = uTime
            Loop
            objdata.Close()
            If strlogged = "Y" Then
                Label1.Text = "User Already Logged In."
                Exit Function
            End If
            If iQuery = 0 Then
                Label1.Text = "Invalid Login ID.Please Login and try once again"
            Else

                validateUser = True
            End If
        Else
            Label1.Text = "Invalid Login ID.Please Login and try once again"
        End If
    End Function
End Class
