﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<style>
    hr {
        display: block;
        margin-top: 0.5em;
        margin-bottom: 0.5em;
        margin-left: auto;
        margin-right: auto;
        border-style: inset;
        border-width: 1px;
    }
</style>
<body>
    <form id="form1" runat="server">
        <div class="row">
            <h5 class="col-md-9">Help Desk Management</h5>
        </div>
        <hr />
        <div class="row" style="padding-top: 10px;">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Company</label>
                    <select id="ddlHDMCompany" class="form-control"></select>
                </div>
            </div>

            <div class="col-md-9 col-sm-12 col-xs-12" align="right">
                <div class="form-group">
                    <button type="button" class="btn btn-default btn-sm" onclick="Print('ConsHD')" style="color: rgba(58, 156, 193, 0.87)">
                        <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: green"></span><span style="color: rgba(58, 156, 193, 0.87)"></span>
                    </button>
                </div>
            </div>
        </div>

        <div id="ConsHD">

            <div class="row">
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-bar-chart fa-fw"></i>Requests Raised for last 7 days
                        </div>
                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('helpdeskcontainer')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>
                        </div>
                        <div class="panel-body">
                            <div id="helpdeskcontainer"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-md-6">
                    <div class="row">
                        <div class="col-md-9 text-center">
                            <button type="button" id="ancSLAexceed" class="alert alert-info" style="width: 230px;">
                                <i class="fa fa-tasks pull-left" aria-hidden="true"></i>SLA Exceeded Request Count  
                            <span class="badge pull-right" id="lblslacount"></span>
                            </button>
                        </div>
                    </div>
                </div>



                <div class="col-md-3 col-md-6">
                    <div class="row">
                        <div class="col-md-9 text-center">
                            <%--<label class="huge" id="lblreqserper"></label>
                                    <div></div>
                                    <label style="font-size: 11px;" class="small">% of Requests Served to Request Raised</label>--%>
                            <button type="button" id="ancCompletedRequests" class="alert alert-success" style="width: 230px;">
                                <i class="fa fa-tasks pull-left" aria-hidden="true"></i>% of Requests Served to Request Raised  
                            <span class="badge pull-right" id="lblreqserper"></span>
                            </button>
                        </div>
                    </div>
                </div>
                <%-- <a href="#" id="ancCompletedRequests">
                    <div class="panel-footer" role="alert" data-toggle="modal" data-target="#closedRequests">
                        <span class="hvr-icon-wobble-horizontal">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>--%>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                            <i class="fa fa-bar-chart fa-fw"></i>Location-Wise Pending Services 
                        </div>
                        <div align="right">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('helpdeskdonutCointainer')" style="color: rgba(58, 156, 193, 0.87)">
                                <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                            </button>
                        </div>
                        <div class="panel-body">
                            <div id="helpdeskdonutCointainer"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="modal fade" id="closedRequests" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="H5">Completed Requests</h4>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-3 pull-left">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('tblClosedRequests')">
                                <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                            </button>
                        </div>
                        <table id="tblClosedRequests" class="table table-condensed table-bordered table-hover table-striped">
                            <tr>
                                <th>Req. Id</th>
                                <th>Requested Date</th>
                                <th>Req. By</th>
                                <th>Assigned To</th>
                                <th>Contact No</th>
                                <th>Status</th>
                                <th>Remarks</th>
                                <th>Last Updated On</th>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="SLAExceededRequests" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="H1">SLA Exceeded Requests</h4>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-3 pull-left">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('tblExceededRequests')">
                                <span class="glyphicon glyphicon-print hvr-icon-bounce"></span>Print
                            </button>
                        </div>
                        <table id="tblExceededRequests" class="table table-condensed table-bordered table-hover table-striped">
                            <tr>
                                <th>Req. Id</th>
                                <th>Requested Date</th>
                                <th>Req. By</th>
                                <th>Assigned To</th>
                                <th>Contact No</th>
                                <th>Status</th>
                                <th>Remarks</th>
                                <th>Last Updated On</th>

                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="divBarGraph" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="divBarGraphHeading"></h4>
                    </div>
                    <div class="modal-body">
                        <div class=" col-md-3 pull-left">
                            <button type="button" class="btn btn-default btn-sm" onclick="Print('tblBarGraph')">
                                <span class="glyphicon glyphicon-print"></span>Print
                            </button>
                        </div>
                        <table id="tblBarGraph" class="table table-condensed table-bordered table-hover table-striped">
                            <tr>
                                <th>Req. Id</th>
                                <th>Requested Date</th>
                                <th>Req. By</th>
                                <th>Assigned To</th>
                                <th>Contact No</th>
                                <th>Status</th>
                                <th>Remarks</th>
                                <th>Last Updated On</th>

                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
<script type="text/javascript" defer>

    $(document).ready(function () {

        var CompanySessionId = '<%= Session("COMPANYID")%>';
        var year;
        year = (new Date().getFullYear()).toString();

        fillPropertyCompany();
        $("#ddlHDMCompany").change(function () {
            CompanySessionId = $(this).val();

            setTimeout(function () {
                funGetHDreqWeekly(CompanySessionId);
            }, 700);
            fnfillSLA(CompanySessionId);
            funfillCategories(CompanySessionId);
        });

        //Requests raised in last 7 days

        function fnfillSLA(CompanySessionId) {
            $.ajax({
                url: '../api/HelpDeskDBAPI/GetSLACount/' + CompanySessionId,
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    $("#lblslacount").html(data.SLACount);
                    $("#lblreqserper").html(data.SLAratio);
                },
                error: function (result) {
                }
            });
        }

        //function funGetHDreqWeekly(CompanySessionId) {
        //    $("#helpdeskcontainer").empty();
        //    //[['x','2015-04-01','2015-04-10','2015-04-23'],['Date',3,1,2]]
        //    var chart = c3.generate({
        //        data: {
        //            x: 'x',
        //            columns: [],
        //            empty: { label: { text: "No Data Available" } },
        //        },
        //        axis: {
        //            x: {
        //                type: 'timeseries',
        //                tick: {
        //                    format: '%Y-%m-%d',
        //                    rotate: 90,
        //                    multiline: false
        //                },
        //            },
        //            y: {
        //                show: true,
        //                label: {
        //                    text: 'No of Requests',
        //                    position: 'outer-middle'
        //                }
        //            },
        //        }
        //    });

        //    $.ajax({
        //        url: '../api/HelpDeskDBAPI/GetHDreqWeekly/' + CompanySessionId,
        //        type: 'GET',
        //        success: function (result) {
        //            chart.load(
        //                {
        //                    columns: result //[['x', '2013-01-01', '2013-01-02', '2013-01-03', '2013-01-04', '2013-01-05', '2013-01-06'], ['data1', 30, 200, 100, 400, 150, 250]]
        //                });
        //        }
        //    });
        //    $("#helpdeskcontainer").append(chart.element);
        //}

        function funGetHDreqWeekly() {
            $("#helpdeskcontainer").empty();
            $.ajax({
                url: '../api/HelpDeskDBAPI/GetHDreqWeekly/' + CompanySessionId,
                type: 'GET',
                success: function (result) {
                    console.log(result);
                    var chart = c3.generate({
                        data: {
                            x: 'x',
                            columns: result,
                            type: 'spline'
                        },
                        axis: {
                            x: {
                                type: 'timeseries',
                                tick: {
                                    format: '%Y-%m-%d'
                                },
                                show: true,
                                label: {
                                    text: 'Date',
                                    position: 'outer-center'
                                }
                            },
                            y: {
                                show: true,
                                label: {
                                    text: 'Total Requests',
                                    position: 'outer-middle'
                                }
                            },
                        },
                    });
                    $("#helpdeskcontainer").append(chart.element);
                },
                failure: function (response) {

                }
            });
        }

        function funfillCategories(CompanySessionId) {
            $("#helpdeskdonutCointainer").empty();
            $.ajax({
                url: '../api/HelpDeskDBAPI/BindHDCategories/' + CompanySessionId,
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    var chart2 = c3.generate({
                        data: {
                            json: JSON.parse(data.locVal),
                            keys: {
                                x: 'name',
                                value: JSON.parse(data.services),
                            },
                            type: 'bar',
                            empty: { label: { text: "No Data Available" } },
                            onclick: function (e) {
                                $("#divBarGraph").modal("show");
                                var selectedService = this.categories()[e.index]
                                $('#divBarGraphHeading').html(e.id + "  - " + "(" + selectedService + ")");
                                GetRequestDetailsbySelectedServiceAndLocation(e.id, selectedService, CompanySessionId);
                            }
                        },
                        axis: {
                            x: {
                                type: 'category'
                            },
                            y: {
                                show: true,
                                label: {
                                    text: 'Count',
                                    position: 'outer-middle'
                                }
                            },
                        }
                    });
                    $("#helpdeskdonutCointainer").append(chart2.element);

                },
                error: function (result) {
                }
            });
        }

        function GetRequestDetails(BType) {
            $.ajax({
                url: "../api/HelpDeskDBAPI/GetRequestDetails/" + CompanySessionId,
                data: {},
                contentType: "application/json; charset=utf-8",
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    //alert("tot " + data.Table.length + " closed" + data.Table1.length + " pending" + data.Table2.length);
                    if (BType == "1") {
                        var table = $('#tblExceededRequests');
                        $('#tblExceededRequests td').remove();
                        for (var i = 0; i < data.Table.length; i++) {
                            table.append("<tr>" +
                            "<td>" + data.Table[i].SER_ID + "</td>" +
                                  "<td>" + data.Table[i].REQUESTEDON + "</td>" +
                            "<td>" + data.Table[i].REQ_BY + "</td>" +
                            "<td>" + data.Table[i].ASSIGNED_TO + "</td>" +
                            "<td>" + data.Table[i].CONTACT_NO + "</td>" +
                            "<td>" + data.Table[i].STA_TITLE + "</td>" +
                            "<td>" + data.Table[i].REMARKS + "</td>" +
                            "<td>" + data.Table[i].LAST_UPDATED_ON + "</td>" +

                                "</tr>");
                        }
                        if (data.Table.length == 0) {
                            table.append("<tr>" + "<td colspan='6' align='center'>No Records Found.</td>" + "</tr>");
                        }
                    }
                    //Total WR
                    if (BType == "2") {
                        var table = $('#tblClosedRequests');
                        $('#tblClosedRequests td').remove();
                        for (var i = 0; i < data.Table1.length; i++) {
                            table.append("<tr>" +
                            "<td>" + data.Table1[i].SER_ID + "</td>" +
                             "<td>" + data.Table1[i].REQUESTEDON + "</td>" +
                            "<td>" + data.Table1[i].REQ_BY + "</td>" +
                             "<td>" + data.Table1[i].ASSIGNED_TO + "</td>" +
                             "<td>" + data.Table1[i].CONTACT_NO + "</td>" +
                             "<td>" + data.Table1[i].STA_TITLE + "</td>" +
                            "<td>" + data.Table1[i].REMARKS + "</td>" +
                            "<td>" + data.Table1[i].LAST_UPDATED_ON + "</td>" +
                             "</tr>");
                        }
                        if (data.Table1.length == 0) {
                            table.append("<tr>" + "<td colspan='6' align='center'> No Records Found.</td>" + "</tr>");
                        }
                    }

                },
                error: function (result) {
                }
            });
        }

        funGetHDreqWeekly(CompanySessionId);
        fnfillSLA(CompanySessionId);
        funfillCategories(CompanySessionId);
        //SLA Exceeded requests
        $("#ancSLAexceed").click(function (e) {
            e.preventDefault();
            $("#SLAExceededRequests").modal("show");
            GetRequestDetails("1");
        });

        //completed requests
        $("#ancCompletedRequests").click(function (e) {
            e.preventDefault();
            $("#closedRequests").modal("show");
            GetRequestDetails("2");
        });

        function GetRequestDetailsbySelectedServiceAndLocation(category, location, CompanySessionId) {

            CompanySessionId = $("#ddlHDMCompany").val();
            p = {
                location: location,
                category: category,
                company: CompanySessionId
            }
            $.ajax({
                url: "../api/HelpDeskDBAPI/GetRequestDetailsbySelectedServiceAndLocation",
                data: p,
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    var table = $('#tblBarGraph');
                    $('#tblBarGraph td').remove();
                    for (var i = 0; i < data.length; i++) {
                        table.append("<tr>" +
                        "<td>" + data[i].SER_ID + "</td>" +
                              "<td>" + data[i].REQUESTEDON + "</td>" +
                        "<td>" + data[i].REQ_BY + "</td>" +
                        "<td>" + data[i].ASSIGNED_TO + "</td>" +
                        "<td>" + data[i].CONTACT_NO + "</td>" +
                        "<td>" + data[i].STA_TITLE + "</td>" +
                        "<td>" + data[i].REMARKS + "</td>" +
                        "<td>" + data[i].LAST_UPDATED_ON + "</td>" +

                            "</tr>");
                    }
                    if (data.length == 0) {
                        table.append("<tr>" + "<td colspan='6' align='center'>No Records Found.</td>" + "</tr>");
                    }
                },
                error: function (result) {
                }
            });
        }

        //GET COMPANIES
        function fillPropertyCompany() {
            $.each(Companies, function (key, value) {
                $("#ddlHDMCompany").append($("<option></option>").val(value.CNP_ID).html(value.CNP_NAME));
            });
        }

        setTimeout(function () { enableDisableCompanyDDL(CompanySessionId) }, 1000);
    });

    function enableDisableCompanyDDL(compid) {
        if (compid == "1") {
            $("#ddlHDMCompany").prop('disabled', false);
        }
        else {
            $("#ddlHDMCompany").prop('disabled', true);
            $('#ddlHDMCompany option[value="' + compid + '"]').attr("selected", "selected");
        }
    }


</script>
</html>
