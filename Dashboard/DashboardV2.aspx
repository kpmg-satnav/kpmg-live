﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DashboardV2.aspx.vb" Inherits="DashboardV2" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard</title>
    <%--<%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>--%>
    <link href="../BlurScripts/BlurCss/bootstrap.css" rel="stylesheet" />
    <link href="../BootStrapCSS/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../BootStrapCSS/FMS_UI/css/flaticon.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/notify.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/prettify.css" rel="stylesheet" />
    
    <link href="../BlurScripts/BlurCss/DashboardMaster.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="al-content">
        <div class="widgets">
            <div ba-panel ba-panel-title="Dashboard" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
                <div class="panel">
                    <div class="panel-body">
                        <ul class="nav nav-tabs " role="tablist">
                            <li class="active"><a href="#profile" data-toggle="tab"><i class="flaticon-people"></i>
                                <br>
                                User Profile</a></li>
                            <li id="propertytab"><a href="#property" data-toggle="tab"><i class="flaticon-home"></i>
                                <br>
                                Properties</a></li>
                            <li id="spacetab"><a href="#space" data-toggle="tab" ><i class="flaticon-office"></i>
                                <br>
                                Space</a></li>
                            <li id="assettab"><a href="#asset" data-toggle="tab"><i class="flaticon-gold"></i>
                                <br>
                                Assets</a></li>
                            <li id="maintenancetab"><a href="#maintenance" data-toggle="tab"><i class="flaticon-settings-work-tool"></i>
                                <br>
                                Maintenance</a></li>
                            <li  id="helpdesktab"><a href="#helpdesk" data-toggle="tab"><i class="flaticon-life-preserver"></i>
                                <br>
                                Help Desk</a></li>
                            <li id="conferencetab"><a href="#conference" data-toggle="tab"><i class="flaticon-businessmen-having-a-group-conference"></i>
                                <br>
                                Reservation</a></li>
                            <li id="businesscardtab"><a href="#businesscard" data-toggle="tab"><i class="flaticon-newspaper"></i>
                                <br>
                                Business Card</a></li>
                            <li id="GuestHousetab"><a href="#GuestHouse" data-toggle="tab" id="ghTitle"><i class="flaticon-home"></i>
                                <br />
                                Guest House
                            </a>
                            </li>
                             <li id="Energytab"><a href="#Energy" data-toggle="tab"><i class="flaticon-lightning-electric-energy"></i>
                                <br />
                                Energy Management
                            </a>
                            </li>
                        </ul>
                        <%--<a href="#top" class="scrollToTop pull-left"></a>--%>

                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="profile"></div>
                            <div class="tab-pane fade" id="property" runat="server"></div>
                            <div class="tab-pane fade" id="space" runat="server"></div>
                            <div class="tab-pane fade" id="asset" runat="server"></div>
                            <div class="tab-pane fade" id="maintenance" runat="server"></div>
                            <div class="tab-pane fade" id="helpdesk" runat="server"></div>
                            <div class="tab-pane fade" id="conference" runat="server"></div>
                            <div class="tab-pane fade" id="businesscard" runat="server"></div>
                            <div class="tab-pane fade" id="GuestHouse" runat="server"></div>
                            <div class="tab-pane fade" id="Energy" runat="server"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <%--C3 charts start here--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../BlurScripts/BlurJs/notify.js" defer></script>
    <script src="../BlurScripts/BlurJs/prettify.js" defer></script>
    <script src="C3/d3.v3.min.js" defer></script>
    <link href="C3/c3.min.css" rel="stylesheet" />
    <script src="C3/c3.min.js" defer></script>
    <script src="C3/jshint.js" defer></script>


    <script type="text/javascript">
        $(document).ready(function () {
            var space = '<%=Session("space")%>';
            var conference = '<%=Session("conference")%>';
            var businesscard = '<%=Session("businesscard")%>';
            var GuestHouse = '<%=Session("GuestHouse")%>';
            var Energy = '<%=Session("Energy")%>';
            var maintenance = '<%=Session("maintenance")%>';
            var helpdesk = '<%=Session("helpdesk")%>';
            var property = '<%=Session("property")%>';
            var assets = '<%=Session("Assets")%>';

           <%-- $.notify('&nbsp  Welcome <% =Session("LoginUser") %> ', { color: '#fff', background: '#20d67b', close: true, icon: "user", position: 'middle' });--%>
            fillCompanyDet();
            validateUserFirstLogin();
            if (space == 1) {
                $("#spacetab").show();
            } else {

                $("#spacetab").hide();

            }
            if (conference == 1) {
                $("#conferencetab").show();
            } else {
                $("#conferencetab").hide();
            }
            if (businesscard == 1) {
                $("#businesscardtab").show();
            } else {
                $("#businesscardtab").hide();
            }
            if (GuestHouse == 1) {
                $("#GuestHousetab").show();
            } else {
                $("#GuestHousetab").hide();
            }
            if (Energy == 1) {
                $("#Energytab").show();
            } else {
                $("#Energytab").hide();
            }
            if (maintenance == 1) {
                $("#maintenancetab").show();
            } else {
                $("#maintenancetab").hide();
            }
            if (property == 1) {
                $("#propertytab").show();
            } else {
                $("#propertytab").hide();
            }
            if (helpdesk == 0) {
                $("#helpdesktab").show();
            } else {
                $("#helpdesktab").hide();
            }
            if (assets == 1) {
                $("#assettab").show();
            } else {
                $("#assettab").hide();
            }
        });
        var Companies = [];

        function fillCompanyDet(valcompany) {

            $.ajax({
                url: '../api/Utility/GetCompanies',
                type: 'GET',
                success: function (result) {
                    Companies = result.data;
                }
            });
        }


         function validateUserFirstLogin(valstatus) {

            $.ajax({
                url: '../api/Utility/ValidateUserFirstLogin',
                type: 'GET',
                success: function (result) {
                    Status = result.data[0].AUR_ID;
                    //var sta = _.find($scope.Country, { AUR_ID: value.AUR_ID });
                    //console.log(sta)
                 <%--   if (result.data[0].AUR_ID == 1) {
                        $.notify('&nbsp  Welcome <% =Session("LoginUser") %> , Kindly update the current system generated password ', { color: '#fff', background: '#20d67b', close: true, icon: "user", position: 'middle', delay: '50000' });
                    }
                    else
                    {--%>
                          $.notify('&nbsp  Welcome <% =Session("LoginUser") %> ', { color: '#fff', background: '#20d67b', close: true, icon: "user", position: 'middle' });
                    //}
                }
            });
        }

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        function activaTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };
        $(function () {
            $('a[title]').tooltip();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href")
                if (target != "#profile")
                    $(target).load(target.substring(1) + "Dashboard.aspx");
                else
                    $("#profile").load("../WebFiles/frmMyProfile.aspx?dummy=" + Math.random());
            });
            if (getParameterByName("back") != "") {
                //activaTab("maintenance");
                var dbname = getParameterByName("back");
                activaTab(dbname);
            }
            else
                $("#profile").load("../WebFiles/frmMyProfile.aspx?dummy=" + Math.random());
        });
    </script>
    <script type="text/javascript">
        var GHT = '<%= Session("GHT")%>';
        $(document).ready(function () {

            $("#ghTitle").append(GHT);
        });

    </script>
    <%--C3 charts end here--%>
    <script src="../Scripts/jQuery.print.js"></script>
    <script src="../BlurScripts/BlurJs/DashboardPrint.js"></script>
</body>
</html>
