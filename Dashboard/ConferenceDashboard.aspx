﻿<style>
    /*#spce .c3 path, .c3 line {
        stroke: #F0AE7E !important;
    }*/

    hr {
        margin-top: 0px;
        margin-bottom: 0px;
    }
</style>
<div id="page-wrapper">
    <form id="form1" runat="server">
        <div class="row">
            <h5 class="col-md-2">Reservation Dashboard</h5>
        </div>
        <hr />
        <div class="row" style="padding-top: 10px;">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Company</label>
                    <select id="ddlCONFCompany" class="form-control selectpicker with-search" data-live-search="true"></select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                        <i class="fa fa-bar-chart fa-fw"></i>Reservation Utilization for last 7 days
                    </div>
                    <div align="right">
                        <button type="button" class="btn btn-default btn-sm" onclick="Print('confUtlContainer')" style="color: rgba(58, 156, 193, 0.87)">
                            <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                        </button>
                    </div>
                    <div class="panel-body">
                        <div id="confUtlContainer"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                        City-Wise Reservation Count
                    </div>
                    <div align="right">
                        <button type="button" class="btn btn-default btn-sm" onclick="Print('donutCointainer')" style="color: rgba(58, 156, 193, 0.87)">
                            <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                        </button>
                    </div>
                    <div class="panel-body">
                        <div id="confCountContainer"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading" style="padding-top: 12px; padding-left: 10px;">
                        City-Wise Reservation Utilization (Current Month) 
                    </div>
                    <div align="right">
                        <button type="button" class="btn btn-default btn-sm" onclick="Print('donutCointainer')" style="color: rgba(58, 156, 193, 0.87)">
                            <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                        </button>
                    </div>
                    <div class="panel-body">
                        <div id="donutCointainer"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" defer>
    var year
    $(document).ready(function () {
        year = (new Date().getFullYear()).toString();
    });
    $(document).ready(function () {

        $("#ddlCONFCompany").change(function () {
            Cmpy = $(this).val();
            var CompanyId = Cmpy;
            funcitywiseconfCount(CompanyId);
            funConfUtilization(CompanyId);
            funcitywiseconfUtil(CompanyId);
        });

        function funfillCompany(valcompany) {

            $.each(Companies, function (key, value) {
                $("#ddlCONFCompany").append($("<option></option>").val(value.CNP_ID).html(value.CNP_NAME));
            });

        }
        var Cmpy = '<%= Session("COMPANYID")%>';
        funfillCompany(Cmpy);
        funcitywiseconfCount(Cmpy);
        funConfUtilization(Cmpy);
        funcitywiseconfUtil(Cmpy);

        function funcitywiseconfCount(CompanyId) {
            var param = { companyid: parseInt(CompanyId) };
            var chart = c3.generate({
                data: {
                    columns: [],
                    type: 'pie',
                },
                pie: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value);
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });

            $.ajax({
                url: '../api/ConferenceDBAPI/GetcitywiseconfCount',
                type: 'POST',
                data: param,
                success: function (result) {
                    chart.load({ columns: result });
                }
            });
            $("#confCountContainer").empty();
            $("#confCountContainer").append(chart.element);
            $("#confCountContainer").load();
        }

        function funcitywiseconfUtil(CompanyId) {
            var param = { companyid: parseInt(CompanyId) };
            var chart = c3.generate({
                data: {
                    columns: [],
                    type: 'donut',
                },
                donut: {
                    expand: false,
                    label: {
                        format: function (value, ratio, id) {
                            return d3.format('')(value) + ' hr(s)';
                        }
                    }

                },
                axis: {
                    x: {
                        position: 'outer-center',
                    },
                }
            });

            $.ajax({
                url: '../api/ConferenceDBAPI/GetcitywiseUtil',
                data: param,
                type: 'POST',
                success: function (result) {
                    chart.load({ columns: result });
                }
            });
            $("#donutCointainer").empty();
            $("#donutCointainer").append(chart.element);
            $("#donutCointainer").load();
        }

        function funConfUtilization(CompanyId) {
            var param = { companyid: parseInt(CompanyId) };
            $.ajax({
                url: '../api/ConferenceDBAPI/GetConfUtil',
                type: 'POST',
                data: param,
                success: function (result) {
                    var chart = c3.generate({
                        data: {
                            x: 'x',
                            columns: result,
                            type: 'spline'
                        },
                        axis: {
                            x: {
                                type: 'timeseries',
                                tick: {
                                    format: '%Y-%m-%d'
                                },
                                show: true,
                                label: {
                                    text: 'Date',
                                    position: 'outer-center'
                                }
                            },
                            y: {
                                show: true,
                                label: {
                                    text: 'Hours Utilized',
                                    position: 'outer-middle'
                                }
                            },
                        },
                    });
                    $("#confUtlContainer").html('');
                    $("#confUtlContainer").append(chart.element);
                },
                failure: function (response) {

                }
            });
        }

        function enableDisableCompanyDDL(compid) {
            if (compid == "1") {
                $("#ddlCONFCompany").prop('disabled', false);
            }
            else {
                $("#ddlCONFCompany").prop('disabled', true);
                $('#ddlCONFCompany option[value="' + compid + '"]').attr("selected", "selected");
            }
        }
        function Print(divName) {
            $('#' + divName).print({
                globalStyles: true,
                mediaPrint: false,
                stylesheet: "http://fonts.googleapis.com/css?family=Inconsolata",
                iframe: true,
                prepend: "<h3>QuickFMS</h3> <hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-bottom: 0.5em;''margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'>" + "<br/>",
                append: "<hr style = ' display: block;' 'margin-top: 0.5em;' 'margin-left: auto;''margin-right: auto;''border-style: inset;''border-width: 1px;'> <h6 align='center'>© QuickFMS " + year + " </h6>"
            });

        }
        setTimeout(function () { enableDisableCompanyDDL(Cmpy) }, 1000);

    });
</script>
