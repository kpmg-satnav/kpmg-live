Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class ESP_ESP_Webfiles_frmLiveAtt
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            BindGrid1()
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_LIVE_ATTENDANCE")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvIn.DataSource = sp.GetDataSet()
            gvIn.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvIn_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvIn.PageIndexChanging
        gvIn.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvOut_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOut.PageIndexChanging
        gvOut.PageIndex = e.NewPageIndex()
        BindGrid1()
    End Sub
    Private Sub BindGrid1()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_LIVE_ATTENDANCE_OUT")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            gvOut.DataSource = sp.GetDataSet()
            gvOut.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
