<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewAccesCard.aspx.vb" Inherits="ESP_ESP_Webfiles_ViewAccesCard" Title="View Access Card Register" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>View Access Card Register
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div class="row">
                              <div class="col-md-12">
                                    <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10" EmptyDataText="No Access Card Register Found.">
                                        <Columns>
                                            <asp:TemplateField HeaderText="AD_ID">
                                                <ItemStyle Width="10%" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbladid" runat="server" CssClass="bodyText" Text='<%#Eval("AD_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Employee No">
                                                <ItemStyle Width="5%" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblempno" runat="server" CssClass="bodyText" Text='<%#Eval("AUR_NO") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemStyle Width="25%" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblname" runat="server" CssClass="bodyText" Text='<%#Eval("AUR_KNOWN_AS") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email">
                                                <ItemStyle Width="25%" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblemail" runat="Server" CssClass="bodyText" Text='<%#Eval("AUR_EMAIL") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Department">
                                                <ItemStyle Width="10%" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldep" runat="Server" CssClass="bodyText" Text='<%#Eval("AUR_DEP_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Designation">
                                                <ItemStyle Width="30%" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldesg" runat="Server" CssClass="bodyText" Text='<%#Eval("AUR_DESGN_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

