Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO

Partial Class EFM_EFM_Webfiles_MaintenanceReport1
    Inherits System.Web.UI.Page

    Dim sqlString As String
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        If Not IsPostBack() Then
            BindListItem()
            BindTimings1()
            txtfromdate.Text = getoffsetdatetime(DateTime.Now).AddYears(-1).Date
            txttodate.Text = getoffsetdatetime(DateTime.Now).Date
            BindGrid()
        End If
    End Sub

    Private Sub BindTimings1()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TIMINGS_2")
        sp.Command.AddParameter("@TIMING", 0, DbType.Int32)
        cbohr.DataSource = sp.GetDataSet()
        cbohr.DataTextField = "VALUE"
        cbohr.DataValueField = "VALUE"
        cbohr.DataBind()
        cbohr.Items.Insert(0, New ListItem("--HH--", "--HH--"))
    End Sub

    Private Sub BindListItem()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_COLUMNS")
        Dim dummy As String = sp.Command.CommandSql
        liitems.DataSource = sp.GetDataSet()
        liitems.DataTextField = "COLUMN_NAME"
        liitems.DataValueField = "COLUMN_VALUE"
        liitems.DataBind()
        'liitems.SelectMethod
        For Each li As ListItem In liitems.Items
            li.Selected = True
        Next
        'liitems.Items.Insert(0, New ListItem("--Select--", "-1"))
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If CDate(txtfromdate.Text) > CDate(txttodate.Text) Then
            lblMsg.Text = "To Date Should be Greater than From Date"

        Else
            'txtstore.Text = ""
            'For Each li As ListItem In liitems.Items
            '    If li.Selected = True Then
            '        txtstore.Text = txtstore.Text + li.Value.ToString() + ","
            '    End If
            'Next
            'txtstore.Text = txtstore.Text.Remove(txtstore.Text.Length - 1)
            'sqlString = txtstore.Text
            BindGrid()
        End If
    End Sub

    Private Sub BindGrid()
        txtstore.Text = ""
        For Each li As ListItem In liitems.Items
            If li.Selected = True Then
                txtstore.Text = txtstore.Text + li.Value.ToString() + " as [" + li.Text.ToString() + "] ,"
            End If
        Next
        txtstore.Text = txtstore.Text.Remove(txtstore.Text.Length - 1)
        sqlString = txtstore.Text
        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@REQ", SqlDbType.NVarChar, 2000)
        param(0).Value = sqlString
        param(1) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        param(1).Value = CDate(txtfromdate.Text)
        param(2) = New SqlParameter("@TODATE", SqlDbType.DateTime)
        param(2).Value = CDate(txttodate.Text)
        param(3) = New SqlParameter("@FROMTIME", SqlDbType.NVarChar)
        param(4) = New SqlParameter("@TOTIME", SqlDbType.NVarChar)
        If cbohr.SelectedItem.Text = "--HH--" Then
            param(3).Value = "0"
            param(4).Value = "23"
        Else
            param(3).Value = cbohr.SelectedValue
            param(4).Value = cboToHr.SelectedValue
        End If
        Dim ds As New DataSet()
        ds = objsubsonic.GetSubSonicDataSet("HDM_GET_COLUMN_WISE_REPORT1", param)
        gvitems.DataSource = ds
        gvitems.DataBind()
    End Sub

    Protected Sub cbohr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbohr.SelectedIndexChanged
        If cbohr.SelectedIndex > 0 And txtfromdate.Text <> "" And txttodate.Text <> "" Then
            BindTimings2()
        Else
            lblMsg.Text = "Please enter mandatory fields"

            cboToHr.Items.Clear()
        End If
    End Sub

    Private Sub BindTimings2()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_TIMINGS_REPORT")
        sp.Command.AddParameter("@FROM_dATE", txtfromdate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_dATE", txttodate.Text, DbType.Date)
        sp.Command.AddParameter("@FROM_TIME", cbohr.SelectedValue, DbType.String)
        cboToHr.DataSource = sp.GetDataSet()
        cboToHr.DataTextField = "VALUE"
        cboToHr.DataValueField = "VALUE"
        cboToHr.DataBind()

    End Sub

    Protected Sub gvitems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

End Class
