Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class ESP_ESP_Webfiles_AddVehicle
    Inherits System.Web.UI.Page

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim Req As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMdd")
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMANTRA_SP_ADD_VEHICLE")
            sp.Command.AddParameter("@VISITOR_REQ", Req, DbType.String)
            sp.Command.AddParameter("@VISITOR_NAME", txtvisitorName.Text, DbType.String)
            sp.Command.AddParameter("@VISITOR_EMAIL", txtemail.Text, DbType.String)
            sp.Command.AddParameter("@VISITOR_MOBILE", ddltype.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@VISITOR_ADDRESS", txtaddress.Text, DbType.String)
            sp.Command.AddParameter("@VISITOR_MEET", txtMeet.Text, DbType.String)
            sp.ExecuteScalar()
            lblMsg.Text = "Vehicle Added Successfully"
            ClearData()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("VehicleEntry.aspx")
    End Sub
    Private Sub ClearData()
        txtvisitorName.Text = ""
        txtemail.Text = ""
       
        txtaddress.Text = ""
        txtMeet.Text = ""

    End Sub
End Class
