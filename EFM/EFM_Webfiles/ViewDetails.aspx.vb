Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO
Imports SENDSMS
Partial Class EFM_EFM_Webfiles_ViewDetails
    Inherits System.Web.UI.Page
    Dim lbl As Integer = 1

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_USER_STATUS")
        sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        sp.Command.AddParameter("@UPDATED_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Comments", txtcomments1.Text, DbType.String)
        sp.Command.AddParameter("@STATUS", ddlstatus1.SelectedItem.Value, DbType.Int32)
        sp.ExecuteScalar()
        txtcomments1.Text = ""

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Dim validID As Integer
            validID = VALIDATEREQUESTID()
            If validID = 0 Then
                BindCity()
                BindService_request_category(ddlType, 0)
                BindDetails()
                BindUsers()
                BindAssignDetails()
                If radlstactions.Items("0").Selected = True Then
                    Mobile.Visible = True
                ElseIf radlstactions.Items("0").Selected = False Then
                    Mobile.Visible = False
                End If
            Else
                Response.Redirect("~/WorkSpace/SMS_Webfiles/ErrorStatus.aspx")
            End If
        End If
        txtconvdate.Attributes.Add("onClick", "displayDatePicker('" + txtconvdate.ClientID + "')")
        txtconvdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
    End Sub


    Private Sub BindService_request_category(ByRef ddl As DropDownList, ByVal idparent As Integer)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@IDPARENT", SqlDbType.Int)
        param(0).Value = idparent
        ObjSubSonic.Binddropdown(ddl, "GET_CATEGORIES", "CATEGORYNAME", "IDCATEGORY", param)
    End Sub
    Private Function VALIDATEREQUESTID()
        Dim valid As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_REQUEST")
        sp.Command.AddParameter("@REQUEST", Request.QueryString("id"), DbType.String)
        valid = sp.ExecuteScalar()
        Return valid
    End Function
    Private Sub BindUsers()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASSIGNED_USERS_SERVICE_TYPE")
        sp.Command.AddParameter("@SER_TYPE", ddlReq.SelectedValue, DbType.String)
        sp.Command.AddParameter("@REQ_TYPE", ddlreqtype.SelectedValue, DbType.String)
        ddluser.DataSource = sp.GetDataSet()
        ddluser.DataTextField = "AUR_KNOWN_AS"
        ddluser.DataValueField = "AUR_ID"
        ddluser.DataBind()
        ddluser.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindAssignDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ASSIGNED_USER_DETAILS")
            sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                ddluser.ClearSelection()
                ddluser.Items.FindByValue(ds.Tables(0).Rows(0).Item("ASSIGNED_TO")).Selected = True
                txtcomments.Text = ds.Tables(0).Rows(0).Item("COMMENTS")
                Dim stat As Integer = ds.Tables(0).Rows(0).Item("SER_STATUS")

                'ddlreqstatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_STATUS")).Selected = True
                ddlstatus1.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_STATUS")).Selected = True
            End If
        Catch ex As Exception
            'Response.Write("BindAssignDetails" & ex.Message)
        End Try
    End Sub


    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CITY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_HELP_DESK_DETAILS")
            sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                rbActions.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_TYPE")).Selected = True
                If rbActions.SelectedItem.Value = "Employee" Then
                    temp.Visible = True
                    tothers.Visible = False
                    tabcomm.Visible = True
                    personal.Visible = True
                    txtempid.Text = ds.Tables(0).Rows(0).Item("SER_AUR_ID")
                    txtescontact.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")

                ElseIf rbActions.SelectedItem.Value = "Others" Then
                    txtempid.Text = ""
                    temp.Visible = False
                    tothers.Visible = True
                    tabcomm.Visible = True
                    personal.Visible = False
                    txtname.Text = ds.Tables(0).Rows(0).Item("NAME1")
                    txtemail.Text = ds.Tables(0).Rows(0).Item("EMAIL")
                    txtcontact.Text = ds.Tables(0).Rows(0).Item("CONTACT")

                End If
                BindService_request_category(ddlReq, ds.Tables(0).Rows(0).Item("REQ"))
                ddlReq.ClearSelection()
                ddlReq.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_TYPE")).Selected = True
                ddlType.ClearSelection()
                ddlType.Items.FindByValue(ds.Tables(0).Rows(0).Item("REQ")).Selected = True

                ddlreqtype.Items.Clear()
                BindRequest(ddlReq.SelectedValue)
                BindService_request_category(ddlreqtype, ddlReq.SelectedItem.Value)
                ddlreqtype.ClearSelection()
                ddlreqtype.Items.FindByValue(ds.Tables(0).Rows(0).Item("REQ_TYPE")).Selected = True
                ddlCity.ClearSelection()
                ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_CTY_ID")).Selected = True
                BindLocation(ddlCity.SelectedValue)
                'ddlloc.ClearSelection()
                'ddlloc.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_BDG_ID")).Selected = True
                'ddltwr.Items.Clear()
                BindTower(ddlloc.SelectedValue)
                'ddltwr.ClearSelection()
                'ddltwr.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_TWR_ID")).Selected = True
                'ddlfloor.Items.Clear()
                BindFloor(ddltwr.SelectedValue, ddlloc.SelectedValue)
                'ddlfloor.ClearSelection()
                'ddlfloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_FLR_ID")).Selected = True
                'ddlwing.Items.Clear()
                BindWing(ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
                'ddlwing.ClearSelection()
                'ddlwing.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_WNG_ID")).Selected = True
                If ddlType.SelectedItem.Text = "Others" Then
                    Loc.Visible = True
                    Twr.Visible = True
                    flr.Visible = True
                    wng.Visible = True
                    ddlSpace.Items.Clear()
                    BindRoom(ddlwing.SelectedValue, ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
                    ddlSpace.ClearSelection()
                    ddlSpace.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_SPC_ID")).Selected = True
                Else
                    Loc.Visible = False
                    Twr.Visible = False
                    flr.Visible = False
                    wng.Visible = False
                    ddlSpace.Items.Clear()
                    BindSpace(ddlType.SelectedItem.Text)
                    ddlSpace.ClearSelection()
                    ddlSpace.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_SPC_ID")).Selected = True
                End If
                txtProbDesc.Text = ds.Tables(0).Rows(0).Item("SER_DESCRIPTION")
                txtSMSMobile.Text = ds.Tables(0).Rows(0).Item("SMS")
                txtSMSEmail.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
                Dim response As String = ds.Tables(0).Rows(0).Item("RESPONSE_BY")
                radlstactions.Items.FindByValue(ds.Tables(0).Rows(0).Item("RESPONSE_BY")).Selected = True
                txtesname.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
                txtdept.Text = ds.Tables(0).Rows(0).Item("DEP")
                txtdesig.Text = ds.Tables(0).Rows(0).Item("DESIGNATION")
                txtescontact.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
                lblreqid.Text = Request.QueryString("id")
                lblreqby.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
                lblreqdate.Text = ds.Tables(0).Rows(0).Item("SER_UPDATED_ON")
                txtconvdate.Text = ds.Tables(0).Rows(0).Item("CONV_DATE").ToString()
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQ_TYPE_MAINTENANCE")
                sp1.Command.AddParameter("@req_id", ddlReq.SelectedValue, DbType.String)
                Dim ds1 As New DataSet()
                ds1 = sp1.GetDataSet()
                If ds1.Tables(0).Rows.Count > 0 Then
                    lbl = ds1.Tables(0).Rows(0).Item("TYPE")
                End If
               
                        BindTimings12(lbl)
                  

                    cboHr.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONV_TIME")).Selected = True
                End If

        Catch ex As Exception
            Response.Write("BindDetails" & ex.Message)
        End Try
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        If rbActions.SelectedItem.Value = "Others" Then
            ddlSpace.Items.Clear()
            BindLocation(ddlCity.SelectedValue)
            If ddlloc.Items.Count = 2 Then
                ddlloc.Items(1).Selected = True
                BindTower(ddlloc.SelectedValue)
                If ddltwr.Items.Count = 2 Then
                    ddltwr.SelectedIndex = 1
                    BindFloor(ddltwr.SelectedValue, ddlloc.SelectedValue)
                    If ddlfloor.Items.Count = 2 Then
                        ddlfloor.SelectedIndex = 1
                        BindWing(ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
                        If ddlwing.Items.Count = 2 Then
                            ddlwing.SelectedIndex = 1
                            BindRoom(ddlwing.SelectedValue, ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
                            If ddlSpace.Items.Count = 2 Then
                                ddlSpace.SelectedIndex = 1
                            Else
                                ddlSpace.SelectedIndex = 0
                            End If
                        Else
                            ddlwing.SelectedIndex = 0
                            ddlSpace.Items.Clear()
                        End If
                    Else
                        ddlfloor.SelectedIndex = 0
                        ddlwing.Items.Clear()
                        ddlSpace.Items.Clear()
                    End If
                Else
                    ddltwr.SelectedIndex = 0
                    ddlfloor.Items.Clear()
                    ddlwing.Items.Clear()
                    ddlSpace.Items.Clear()
                End If
            Else
                ddlloc.SelectedIndex = 0
                ddltwr.Items.Clear()
                ddlfloor.Items.Clear()
                ddlwing.Items.Clear()
                ddlSpace.Items.Clear()


            End If
        Else
            If ddlCity.SelectedIndex = 0 Then
                ddlSpace.Items.Clear()
            Else
                BindSpace(ddlCity.SelectedItem.Value)
            End If
        End If
    End Sub
    Private Sub BindRoom(ByVal wing As String, ByVal flr As String, ByVal tow As String, ByVal loc As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        sp.Command.AddParameter("@TOWER", tow, DbType.String)
        sp.Command.AddParameter("@FLOOR", flr, DbType.String)
        sp.Command.AddParameter("@WING", wing, DbType.String)
        sp.Command.AddParameter("@SPC_TYPE", "", DbType.String)
        ddlSpace.DataSource = sp.GetDataSet()
        ddlSpace.DataTextField = "SPC_VIEW_NAME"
        ddlSpace.DataValueField = "SPC_ID"
        ddlSpace.DataBind()
        ddlSpace.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub
    Private Sub BindLocation(ByVal city As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCATION_CITY")
        sp.Command.AddParameter("@CITY", city, DbType.String)
        ddlloc.DataSource = sp.GetDataSet()
        ddlloc.DataTextField = "LCM_NAME"
        ddlloc.DataValueField = "LCM_CODE"
        ddlloc.DataBind()
        ddlloc.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindTower(ByVal loc As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TOWER_LOC_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        ddltwr.DataSource = sp.GetDataSet()
        ddltwr.DataTextField = "TWR_NAME"
        ddltwr.DataValueField = "TWR_CODE"
        ddltwr.DataBind()
        ddltwr.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindFloor(ByVal twr As String, ByVal loc As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_FLOOR_TOW_LOC_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        sp.Command.AddParameter("@TOWER", twr, DbType.String)
        ddlfloor.DataSource = sp.GetDataSet()
        ddlfloor.DataTextField = "FLR_NAME"
        ddlfloor.DataValueField = "FLR_CODE"
        ddlfloor.DataBind()
        ddlfloor.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindWing(ByVal floor As String, ByVal twr As String, ByVal loc As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_WING_FLR_TOW_LOC_CITY")
        sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
        sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        sp.Command.AddParameter("@TOWER", twr, DbType.String)
        sp.Command.AddParameter("@FLOOR", floor, DbType.String)
        ddlwing.DataSource = sp.GetDataSet()
        ddlwing.DataTextField = "WNG_NAME"
        ddlwing.DataValueField = "WNG_CODE"
        ddlwing.DataBind()
        ddlwing.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindSpace(ByVal SPACE_TYPE As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_CITY")
        sp.Command.AddParameter("@SPC_CTY_ID", ddlCity.SelectedValue, DbType.String)
        If rbActions.SelectedItem.Value = "Employee" Then
            sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)
        Else
            sp.Command.AddParameter("@AUR_ID", "", DbType.String)
        End If
        sp.Command.AddParameter("@SPACE_TYPE", SPACE_TYPE, DbType.String)
        ddlSpace.DataSource = sp.GetDataSet()
        ddlSpace.DataTextField = "SPC_VIEW_NAME"
        ddlSpace.DataValueField = "SPC_ID"
        ddlSpace.DataBind()
        ddlSpace.Items.Insert(0, New ListItem("--Select--", "--Select--"))
        If ddlSpace.Items.Count = 2 Then
            ddlSpace.SelectedIndex = 1
        End If
    End Sub

    Protected Sub mnuTabs_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles mnuTabs.MenuItemClick
        Dim index As Integer = Int32.Parse(e.Item.Value)
        multiTabs.ActiveViewIndex = index
    End Sub

    Protected Sub btnModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModify.Click

       


        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"MODIFY_HELP_DESK_REQUEST1")
        sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        sp.Command.AddParameter("@AUR_TYPE", rbActions.SelectedItem.Value, DbType.String)
        If rbActions.SelectedItem.Value = "Employee" Then
            sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)
        Else
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        End If
        sp.Command.AddParameter("@Name", txtname.Text, DbType.String)
        sp.Command.AddParameter("@Email", txtemail.Text, DbType.String)
        sp.Command.AddParameter("@Contact", txtcontact.Text, DbType.String)
        sp.Command.AddParameter("@CITY", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SER_TYPE", ddlType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQ_RAISED_FOR", ddlReq.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQ_TYPE", ddlreqtype.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SER_SPC_ID", ddlSpace.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@DESCRIPTION", txtProbDesc.Text, DbType.String)
        sp.Command.AddParameter("@MOBILE", "", DbType.String)
        sp.Command.AddParameter("@RESPONSE_BY", radlstactions.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@RAISED_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQ", ddlType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SMS", txtSMSMobile.Text, DbType.String)
        sp.Command.AddParameter("@AUR_EMAIL", txtSMSEmail.Text, DbType.String)
        If txtconvdate.Text = "" Then
            sp.Command.AddParameter("@CONV_DATE", DBNull.Value, DbType.DateTime)
            sp.Command.AddParameter("@CONV_TIME", "-HH--", DbType.String)
            sp.Command.AddParameter("@CONV_TIME1", "-HH--", DbType.String)
        Else
            sp.Command.AddParameter("@CONV_DATE", txtconvdate.Text, DbType.DateTime)
            sp.Command.AddParameter("@CONV_TIME", cboHr.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@CONV_TIME1", cboHr.SelectedItem.Text, DbType.String)
        End If
        sp.Command.AddParameter("@CONREMARKS", "NA", DbType.String)
        sp.Command.AddParameter("@COMMENTS", txtcomments.Text, DbType.String)
        sp.Command.AddParameter("@ASSIGN_TO", ddluser.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks1.aspx?id=58&Reqid=" & Request.QueryString("id"))
    End Sub
    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedIndex > 0 And ddlCity.SelectedIndex > 0 Then
            If ddlType.SelectedItem.Text = "Others" Then
                Getdetails()
                loc.Visible = True
                Twr.Visible = True
                flr.Visible = True
                wng.Visible = True
            Else
                loc.Visible = False
                Twr.Visible = False
                flr.Visible = False
                wng.Visible = False
            End If
            BindSpace(ddlType.SelectedItem.Text)
        End If
    End Sub
    Private Sub Getdetails()
        ddlloc.Items.Clear()
        ddltwr.Items.Clear()
        ddlfloor.Items.Clear()
        ddlwing.Items.Clear()
        ddlSpace.Items.Clear()
        BindCity()
        If ddlCity.Items.Count = 2 Then
            BindLocation(ddlCity.SelectedValue)
            If ddlloc.Items.Count = 2 Then
                ddlloc.Items(1).Selected = True
                BindTower(ddlloc.SelectedValue)
                If ddltwr.Items.Count = 2 Then
                    ddltwr.SelectedIndex = 1
                    BindFloor(ddltwr.SelectedValue, ddlloc.SelectedValue)
                    If ddlfloor.Items.Count = 2 Then
                        ddlfloor.SelectedIndex = 1
                        BindWing(ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
                        If ddlwing.Items.Count = 2 Then
                            ddlwing.SelectedIndex = 1
                            BindRoom(ddlwing.SelectedValue, ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
                            If ddlSpace.Items.Count = 2 Then
                                ddlSpace.SelectedIndex = 1
                            Else
                                ddlSpace.SelectedIndex = 0
                            End If
                        Else
                            ddlwing.SelectedIndex = 0
                            ddlSpace.Items.Clear()
                        End If
                    Else
                        ddlfloor.SelectedIndex = 0
                        ddlwing.Items.Clear()
                        ddlSpace.Items.Clear()
                    End If
                Else
                    ddltwr.SelectedIndex = 0
                    ddlfloor.Items.Clear()
                    ddlwing.Items.Clear()
                    ddlSpace.Items.Clear()
                End If
            Else
                ddlloc.SelectedIndex = 0
                ddltwr.Items.Clear()
                ddlfloor.Items.Clear()
                ddlwing.Items.Clear()
                ddlSpace.Items.Clear()
            End If
        Else
            ddlloc.Items.Clear()
            ddltwr.Items.Clear()
            ddlfloor.Items.Clear()
            ddlwing.Items.Clear()
            ddlSpace.Items.Clear()
        End If
    End Sub
    Protected Sub ddlReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReq.SelectedIndexChanged
        'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQ_TYPE_MAINTENANCE")
        'sp1.Command.AddParameter("@req_id", ddlReq.SelectedItem.Value, DbType.String)
        'Dim ds1 As New DataSet()
        'ds1 = sp1.GetDataSet()
        'If ds1.Tables(0).Rows.Count > 0 Then
        '    lbl = ds1.Tables(0).Rows(0).Item("TYPE")
        'End If
        'If rbActions.SelectedItem.Value = "Others" Then
        '    txtSMSEmail.Text = txtemail.Text
        'End If
        'If ddlReq.SelectedIndex > 0 Then
        '    If txtconvdate.Text <> "" Then
        '        If CDate(txtconvdate.Text) = getoffsetdate(Date.Today) Then
        '            BindTimings(lbl)
        '        Else
        '            BindTimings1(lbl)
        '        End If
        '    Else
        '        BindTimings1(lbl)
        '    End If
        'End If
        BindRequest()
        If ddlreqtype.Items.Count = 2 Then
            ddlreqtype.SelectedIndex = 1
            BindTimings()
        Else
            ddlreqtype.SelectedIndex = 0
        End If
    End Sub
    Private Sub BindRequest()
        Dim count As Integer = 0
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_REQUEST_TYPE_COUNT")
        sp1.Command.AddParameter("@REQ_TYPE", ddlReq.SelectedItem.Value, DbType.String)
        Dim ds1 As New DataSet
        ds1 = sp1.GetDataSet()
        If ds1.Tables(0).Rows.Count > 0 Then
            count = ds1.Tables(0).Rows(0).Item("COUNT")
        End If
        count = count + 1
        BindService_request_category(ddlreqtype, ddlReq.SelectedItem.Value)
    End Sub

  
    Private Sub BindRequest(ByVal req As String)
        Dim count As Integer = 0
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_REQUEST_TYPE_COUNT")
        sp1.Command.AddParameter("@REQ_TYPE", req, DbType.String)
        Dim ds1 As New DataSet
        ds1 = sp1.GetDataSet()
        If ds1.Tables(0).Rows.Count > 0 Then
            count = ds1.Tables(0).Rows(0).Item("COUNT")
        End If
        count = count + 1
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_NAME1")

        sp.Command.AddParameter("@REQ_TYPE", ddlReq.SelectedItem.Value, DbType.String)
        ddlreqtype.DataSource = sp.GetDataSet()
        ddlreqtype.DataTextField = "REQ_NAME"
        ddlreqtype.DataValueField = "REQ_ID"
        ddlreqtype.DataBind()
        ddlreqtype.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub
    Private Sub BindTimings12(ByVal lbl As Integer)

        cboHr.Items.Clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TIMINGS_21")
        sp.Command.AddParameter("@req_id", ddlreqtype.SelectedValue, DbType.String)
        sp.Command.AddParameter("@date", txtconvdate.Text, DbType.Date)
        cboHr.DataSource = sp.GetDataSet()
        cboHr.DataTextField = "TIMINGS"
        cboHr.DataValueField = "VALUE"
        cboHr.DataBind()
        cboHr.Items.Insert(0, New ListItem("-HH--", "-HH--"))

    End Sub
    
    
    Protected Sub radlstactions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radlstactions.SelectedIndexChanged


        If rbActions.SelectedValue = "Employee" Then
            ClearSMSdetails()
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_MOBILE_DETAILS")
            sp.Command.AddParameter("@AUR_ID", txtempid.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtSMSMobile.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
                txtSMSEmail.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
            End If
        Else
            ClearSMSdetails()
            txtSMSEmail.Text = txtemail.Text

        End If
        If radlstactions.Items("0").Selected = True Then
            Mobile.Visible = True
        ElseIf radlstactions.Items("0").Selected = False Then
            Mobile.Visible = False
        End If
    End Sub
    Private Sub ClearSMSdetails()
        txtSMSMobile.Text = ""
        txtSMSEmail.Text = ""
    End Sub

    Protected Sub ddlreqtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlreqtype.SelectedIndexChanged
        BindUsers()
        'If ddlreqtype.SelectedIndex > 0 And txtconvdate.Text <> "" Then
        '    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CMS_TIMINGS")
        '    sp1.Command.AddParameter("@req_id", ddlreqtype.SelectedItem.Value, DbType.String)
        '    sp1.Command.AddParameter("@date", txtconvdate.Text, DbType.Date)
        '    cboHr.DataSource = sp1.GetDataSet()
        '    cboHr.DataTextField = "TIMINGS"
        '    cboHr.DataValueField = "VALUE"
        '    cboHr.DataBind()
        '    cboHr.Items.Insert(0, New ListItem("-HH--", "-HH--"))

        'End If
    End Sub

    Protected Sub ddlloc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlloc.SelectedIndexChanged

        If ddlType.SelectedItem.Text = "Others" Then
            If ddlCity.SelectedIndex > 0 And ddlloc.SelectedIndex > 0 Then
                BindTower(ddlloc.SelectedValue)
                If ddltwr.Items.Count = 2 Then
                    ddltwr.SelectedIndex = 1
                    BindFloor(ddltwr.SelectedValue, ddlloc.SelectedValue)
                    If ddlfloor.Items.Count = 2 Then
                        ddlfloor.SelectedIndex = 1
                        BindWing(ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
                        If ddlwing.Items.Count = 2 Then
                            ddlwing.SelectedIndex = 1
                            BindRoom(ddlwing.SelectedValue, ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
                            If ddlSpace.Items.Count = 2 Then
                                ddlSpace.SelectedIndex = 1
                            Else
                                ddlSpace.SelectedIndex = 0
                            End If
                        Else
                            ddlwing.SelectedIndex = 0
                            ddlSpace.Items.Clear()
                        End If
                    Else
                        ddlfloor.SelectedIndex = 0
                        ddlwing.Items.Clear()
                        ddlSpace.Items.Clear()
                    End If
                Else
                    ddltwr.SelectedIndex = 0
                    ddlfloor.Items.Clear()
                    ddlwing.Items.Clear()
                    ddlSpace.Items.Clear()
                End If
            Else
                ddlloc.SelectedIndex = 0
                ddltwr.Items.Clear()
                ddlfloor.Items.Clear()
                ddlwing.Items.Clear()
                ddlSpace.Items.Clear()
            End If
        Else
            ddlSpace.Items.Clear()
            If ddlloc.SelectedIndex > 0 Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_LOCATION")
                sp.Command.AddParameter("@LOCATION", ddlloc.SelectedItem.Value, DbType.String)
                ddlSpace.DataSource = sp.GetDataSet()
                ddlSpace.DataTextField = "SPC_VIEW_NAME"
                ddlSpace.DataValueField = "SPC_ID"
                ddlSpace.DataBind()
                ddlSpace.Items.Insert(0, New ListItem("--Select--", "--Select--"))
            End If
        End If

    End Sub

    Protected Sub ddltwr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddltwr.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 And ddlloc.SelectedIndex > 0 And ddltwr.SelectedIndex > 0 Then
            BindFloor(ddltwr.SelectedValue, ddlloc.SelectedValue)
            If ddlfloor.Items.Count = 2 Then
                ddlfloor.SelectedIndex = 1
                BindWing(ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
                If ddlwing.Items.Count = 2 Then
                    ddlwing.SelectedIndex = 1
                    BindRoom(ddlwing.SelectedValue, ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
                    If ddlSpace.Items.Count = 2 Then
                        ddlSpace.SelectedIndex = 1
                    Else
                        ddlSpace.SelectedIndex = 0
                    End If
                Else
                    ddlwing.SelectedIndex = 0
                    ddlSpace.Items.Clear()
                End If
            Else
                ddlfloor.SelectedIndex = 0
                ddlwing.Items.Clear()
                ddlSpace.Items.Clear()
            End If
        End If
    End Sub

    Protected Sub ddlfloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlfloor.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 And ddlloc.SelectedIndex > 0 And ddltwr.SelectedIndex > 0 And ddlfloor.SelectedIndex > 0 Then
            BindWing(ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
            If ddlwing.Items.Count = 2 Then
                ddlwing.SelectedIndex = 1
                BindRoom(ddlwing.SelectedValue, ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
                If ddlSpace.Items.Count = 2 Then
                    ddlSpace.SelectedIndex = 1
                Else
                    ddlSpace.SelectedIndex = 0
                End If
            Else
                ddlwing.SelectedIndex = 0
                ddlSpace.Items.Clear()
            End If
        End If
    End Sub

    Protected Sub ddlwing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlwing.SelectedIndexChanged



        If ddlwing.SelectedIndex > 0 And ddlfloor.SelectedIndex > 0 And ddltwr.SelectedIndex > 0 And ddlloc.SelectedIndex > 0 And ddlCity.SelectedIndex > 0 Then
            BindRoom(ddlwing.SelectedValue, ddlfloor.SelectedValue, ddltwr.SelectedValue, ddlloc.SelectedValue)
            If ddlSpace.Items.Count = 2 Then
                ddlSpace.SelectedIndex = 1
            Else
                ddlSpace.SelectedIndex = 0
            End If
        End If


    End Sub

 

    Protected Sub txtconvdate_TextChanged(sender As Object, e As EventArgs) Handles txtconvdate.TextChanged
        BindTimings()
    End Sub

    Private Sub BindTimings()
        If ddlreqtype.SelectedIndex > 0 And txtconvdate.Text <> "" Then

            If txtconvdate.Text = getoffsetdate(Date.Today) And cboHr.SelectedItem.Value < getoffsetdatetime(DateTime.Now).Hour() Then
                lblMsg.Text = "Convinient time should be more than current time "
                Exit Sub
            End If


            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CMS_TIMINGS")
            sp1.Command.AddParameter("@req_id", ddlreqtype.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@date", txtconvdate.Text, DbType.Date)
            cboHr.DataSource = sp1.GetDataSet()
            cboHr.DataTextField = "TIMINGS"
            cboHr.DataValueField = "VALUE"
            cboHr.DataBind()
            cboHr.Items.Insert(0, New ListItem("-HH--", "-HH--"))

        End If
    End Sub
End Class
