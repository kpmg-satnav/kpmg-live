<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="HRApprovalRaiseRequest.aspx.vb" Inherits="ESP_ESP_Webfiles_HRApprovalRaiseRequest" title="HR Approval Raise Request" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="100%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
       
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>HR Approval for Raise Request</strong></td>
            <td style="width: 17px">
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
            <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
               
                <table id="table1" cellspacing="0" cellpadding="1" width="100%" border="1">
                    <tr>
                        <td align="left" style="width: 25%; height: 26px;">
                           ID Card Type
                          
                        </td>
                        <td align="left" style="width: 25%; height: 26px;">
                            <asp:DropDownList ID="ddlIdType" runat="server" CssClass="clsComboBox" Width="97%">
                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                <asp:ListItem Value="Associate">Associate ID-Card</asp:ListItem>
                                <asp:ListItem Value="Business">Business Card</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    
                </table>
                <asp:Panel ID="pnl1" runat="server" Width="100%">
                    <table id="tass" runat="server" cellpadding="2" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" style="width: 25%; height: 26px;">
                                Associate Fist Name
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtAssociateFirstName" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                Associate Middle Name
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtAssociateMiddleName" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%; height: 26px;">
                                Associate SurName
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtAssociatesurName" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                Associate No
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtAssociateNo" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%; height: 26px;">
                                Department
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtdep" runat="Server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                D.O.J
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtdoj" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%; height: 26px;">
                                Blood Group
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtBG" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 100%; height: 26px;" colspan="4">
                                
                                <asp:CheckBox ID="chkterms" runat="Server" />
                                &nbsp;
                                <asp:LinkButton ID="lbtnterms" runat="server" Text="I Accept to the Terms and Conditions"></asp:LinkButton>
                            </td>
                        </tr>
                       
                       
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnl2" runat="server" Width="100%">
                    <table id="tabbus" runat="server" cellpadding="2" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" style="width: 25%; height: 26px;">
                                Associate Name
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtassname" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                Designation
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtdesig" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%; height: 26px;">
                                Phone
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtphone" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                Mobile
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtmobile" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%; height: 26px;">
                                Email
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                Quantity 
                               <%-- <asp:RequiredFieldValidator ID="rfvqty" runat="server" Display="none" ErrorMessage="Please Enter Quantity"
                                    ControlToValidate="txtqty" ValidationGroup="Val3"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revqty" runat="Server" Display="none" ErrorMessage="Please Enter Valid Quantity"
                                    ControlToValidate="txtqty" ValidationExpression="^[0-9]+$" ValidationGroup="Val3"></asp:RegularExpressionValidator>--%>
                            </td>
                            <td align="left" style="width: 25%; height: 26px;">
                                <asp:TextBox ID="txtqty" runat="server" CssClass="clsTextField" Width="97%" MaxLength="5"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 100%; height: 26px;" colspan="4">
                               
                                
                                <asp:CheckBox ID="chkbusterms" runat="Server" />
                                <asp:LinkButton ID="lbtnbus1terms" runat="server" Text="I Accepts to the Terms and Conditions"></asp:LinkButton>
                            </td>
                        </tr>
                       
                      
                    </table>
                     </asp:Panel>
                    <table id="t" cellpadding="0" cellspacing="0" runat="Server" width="100%">
                    <tr>
                    <td align="center">
                    <asp:Button ID="btnapprove" runat="server" CssClass="button" Text="Approve" />
                     &nbsp;
                    &nbsp;
                    <asp:Button ID="btnReject" runat="server" CssClass="button" Text="Reject" />
                    </td>
                    </tr>
                    </table>
                   <%-- <table id="tab" cellpadding="0" cellspacing="0" runat="Server" width="100%">
                    <tr>
                    <td align="center" >
                    <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" />
                    &nbsp;
                    &nbsp;
                    <asp:Button ID="btnReject" runat="Server" CssClass="button" Text="Reject" />
                    </td>
                    </tr>
                    </table>--%>
               
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px; width: 524px;" background="../../Images/table_bot_mid_bg.gif">
                <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
</div>
</asp:Content>

