Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports System.Configuration
Imports Microsoft.Reporting.WebForms
Partial Class EFM_EFM_Webfiles_MaintenanceReport2
    Inherits System.Web.UI.Page
    Dim lbl As Integer = 1
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        txtfromdate.Attributes.Add("readonly", "readonly")
        txttodate.Attributes.Add("readonly", "readonly")
        If Not IsPostBack() Then
            txtfromdate.Text = getoffsetdatetime(DateTime.Now).AddYears(-1).Date
            txttodate.Text = getoffsetdatetime(DateTime.Now).Date
            GetLocations()
            getcategory(ddlType.SelectedItem.Value)
            getservicetype(ddlType.SelectedItem.Value, ddlReq.SelectedItem.Value)
            BindTimings1()

            getcategory(ddlType.SelectedValue)
            ddlReq.Items.Insert(0, "--All--")
            ddlreqtype.Items.Insert(0, "--All--")
            BindGridView()
        End If
    End Sub
    Public Sub GetLocations()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_Location_GetAll")
        ddlType.DataSource = sp.GetDataSet()
        ddlType.DataTextField = "LCM_NAME"
        ddlType.DataValueField = "LCM_CODE"
        ddlType.DataBind()
        ddlType.Items.Insert(0, "--All--")
    End Sub

    Private Sub BindTimings1()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TIMINGS_2")
        sp.Command.AddParameter("@TIMING", 0, DbType.Int32)
        cbohr.DataSource = sp.GetDataSet()
        cbohr.DataTextField = "VALUE"
        cbohr.DataValueField = "VALUE"
        cbohr.DataBind()
        cbohr.Items.Insert(0, New ListItem("--HH--", "--HH--"))
    End Sub
    Private Sub BindRequestCategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_REQUEST_TYPE")
        sp.Command.AddParameter("@TYPE", ddlType.SelectedItem.Value, DbType.Int32)
        ddlReq.DataSource = sp.GetDataSet()
        ddlReq.DataTextField = "REQUEST_TYPE"
        ddlReq.DataValueField = "SNO"
        ddlReq.DataBind()
        ddlReq.Items.Insert(0, "--All--")
        'ddlReq.Items.Insert(1, New ListItem("All", "All"))
    End Sub

    Private Sub BindService_request_category(ByRef ddl As DropDownList, ByVal idparent As Integer)

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@IDPARENT", SqlDbType.Int)
        param(0).Value = idparent
        ObjSubSonic.Binddropdown(ddl, "GET_CATEGORIES", "CATEGORYNAME", "IDCATEGORY", param)
        ddl.Items.Insert(1, New ListItem("All", "All"))
    End Sub

    Protected Sub ddlReq_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReq.SelectedIndexChanged
        ReportViewer1.Visible = False
        ddlreqtype.Items.Clear()
        getservicetype(ddlType.SelectedItem.Value, ddlReq.SelectedItem.Value)
        ddlreqtype.Items(0).Text = "--All--"
        If ddlreqtype.Items.Count = 2 Then
            ddlreqtype.SelectedIndex = 0
        End If
    End Sub
    Private Sub BindService_request_category(ByVal idparent As Integer)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@IDPARENT", SqlDbType.Int)
        param(0).Value = idparent
        ObjSubSonic.Binddropdown(ddlReq, "GET_CATEGORIES", "CATEGORYNAME", "IDCATEGORY", param)
        ddlReq.Items.Insert(1, New ListItem("All", "All"))
    End Sub
    Protected Sub cbohr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbohr.SelectedIndexChanged
        If cbohr.SelectedIndex > 0 And txtfromdate.Text <> "" And txttodate.Text <> "" Then
            BindTimings2()
        Else
            cboToHr.Items.Clear()
        End If
    End Sub
    Private Sub BindTimings2()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_TIMINGS_REPORT")
        sp.Command.AddParameter("@FROM_dATE", txtfromdate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_dATE", txttodate.Text, DbType.Date)
        sp.Command.AddParameter("@FROM_TIME", cbohr.SelectedValue, DbType.Int32)
        cboToHr.DataSource = sp.GetDataSet()
        cboToHr.DataTextField = "VALUE"
        cboToHr.DataValueField = "VALUE"
        cboToHr.DataBind()

    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        ReportViewer1.Visible = False
        'ddlReq.Items.Clear()
        getcategory(ddlType.SelectedItem.Value)
        ddlReq.Items(0).Text = "--All--"
        If ddlReq.Items.Count = 2 Then
            ddlReq.SelectedIndex = 0
        End If
        getservicetype(ddlType.SelectedItem.Value, ddlReq.SelectedItem.Value)
        ddlreqtype.Items(0).Text = "--All--"
        If ddlreqtype.Items.Count = 2 Then
            ddlreqtype.SelectedIndex = 0
        End If

    End Sub

    Private Sub getcategory(lcm_code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GETACTIVESERREQUESTTYPE")
        sp.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
        ddlReq.DataSource = sp.GetDataSet()
        ddlReq.DataTextField = "SER_NAME"
        ddlReq.DataValueField = "SER_CODE"
        ddlReq.DataBind()
        ddlReq.Items.Insert(0, "--All--")
    End Sub
    Private Sub getservicetype(lcm_code As String, ser_code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_ALL_SERVICETYPE")
        sp.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
        sp.Command.AddParameter("@SER_CODE", ser_code, DbType.String)
        ddlreqtype.DataSource = sp.GetDataSet()
        ddlreqtype.DataTextField = "SER_TYPE_NAME"
        ddlreqtype.DataValueField = "SER_TYPE_CODE"
        ddlreqtype.DataBind()
        ddlreqtype.Items.Insert(0, "--All--")

    End Sub


    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        BindGridView()

    End Sub
    Public Sub BindGridView()


        Dim loc As String = ""
        Dim ctgry As String = ""
        Dim serreq As String = ""
        Dim sta As String = ""

        If ddlType.SelectedItem.Value = "--All--" Then
            loc = ""
        Else
            loc = ddlType.SelectedItem.Value
        End If

        If ddlReq.SelectedItem.Value = "--All--" Then
            ctgry = ""
        Else
            ctgry = ddlReq.SelectedItem.Value
        End If

        If ddlreqtype.SelectedItem.Value = "--All--" Then
            serreq = ""
        Else
            serreq = ddlreqtype.SelectedItem.Value
        End If

        If ddlstatus.SelectedItem.Value = "--All--" Then
            sta = ""
        Else
            sta = ddlstatus.SelectedItem.Value
        End If

        Dim param(12) As SqlParameter
        param(0) = New SqlParameter("@FDATE", SqlDbType.DateTime)
        param(0).Value = txtfromdate.Text
        param(1) = New SqlParameter("@TDATE", SqlDbType.DateTime)
        param(1).Value = txttodate.Text
        param(2) = New SqlParameter("@REQ_TYPE", SqlDbType.NVarChar, 200)
        param(2).Value = serreq
        param(3) = New SqlParameter("@REQ", SqlDbType.NVarChar, 200)
        param(3).Value = ctgry
        param(4) = New SqlParameter("@FTIME", SqlDbType.NVarChar, 200)
        param(5) = New SqlParameter("@TTIME", SqlDbType.NVarChar, 200)
        If cbohr.SelectedItem.Text = "--HH--" Then
            param(4).Value = "0"

            param(5).Value = "23"
        Else
            param(4).Value = cbohr.SelectedItem.Value

            param(5).Value = cboToHr.SelectedItem.Value
        End If
        param(6) = New SqlParameter("@SER_STATUS", SqlDbType.NVarChar, 200)
        param(6).Value = sta
        param(7) = New SqlParameter("@type", SqlDbType.NVarChar, 200)
        param(7).Value = loc
        param(8) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(8).Value = 1
        param(9) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(9).Value = "REQUESTED_DATE"
        param(10) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(10).Value = "ASC"
        param(11) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(11).Value = 100

        param(12) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(12).Value = 0
        param(12).Direction = ParameterDirection.Output
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("HDM_GET_MAINTENANCE_REPORT2", param)
        Dim rds As New ReportDataSource()
        rds.Name = "HelpByCategoryDS"
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/HelpByCategory.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
    End Sub
End Class
