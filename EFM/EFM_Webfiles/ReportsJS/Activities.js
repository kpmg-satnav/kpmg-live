﻿// JScript File

function dg_maintenance_Activities(User)
{ 

   $("#UsersGrid").jqGrid(
   {    url: '../../Generics_Handler/Activities.ashx?MUser='+ User +'',
        datatype: 'json',
        height: 250,
        
        colNames: ['Assigned To','Requested Time','Assigned Time','Comments','Closed Time','Total Time','Delayed Time','Request Status','Alert'],
        colModel: [
                           
                           { name: 'ASSIGNED_TO',  width: 130,   sortable: true },
                           { name: 'REQUESTED_TIME', width: 130, sortable: true },
                           { name: 'ASSIGNED_TIME', width: 130, sortable: true },
                           { name: 'COMMENTS', width: 200, sortable: true }, 
                           { name: 'CLOSED_TIME', width: 130, sortable: true },
                           { name: 'TOTAL_TIME', width: 130, sortable: true }, 
                           { name: 'DELAYED_TIME', width: 130, sortable: true }, 
                           { name: 'REQUEST_STATUS',  width: 90,   sortable: true },
                           { name: 'Alert',  width: 50,   sortable: true }
                           
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#UsersGridPager'),
        sortname: 'ASSIGNED_TO',
        viewrecords: true,
        sortorder: 'asc'
        //caption: 'Report'
        }).navGrid('#UsersGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
					
					// add custom button to export the data to excel
			jQuery("#UsersGrid").jqGrid('navButtonAdd','#UsersGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/MaintenanceActivities.aspx?Muser='+ User +''; jQuery("#UsersGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 