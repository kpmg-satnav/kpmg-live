<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="MaintenanceOthers.aspx.vb" Inherits="EFM_EFM_Webfiles_MaintenanceOthers"
    Title="Maintenance Report for Others" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
<script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js" defer></script>

   <%-- <script type="text/javascript">
     
    function dg_reportc()
             {
		        var FromDate = $('#<%= txtfromdate.ClientID %>').val();
		        var toDate = $('#<%= txttodate.ClientID %>').val();
		        var FromTime = $('#<%= cbohr.ClientID %>').val();
		        var ToTime = $('#<%= cboToHr.ClientID %>').val();
		        
		       
		        $("#UserGrid").setGridParam({url:'../../Generics_Handler/MaintenanceOthers.ashx?Mfromdate='+FromDate+'&Mtodate='+toDate+'&Mfromtime='+FromTime+'&Mtotime='+ToTime});
		        dg_manage_stores(FromDate,toDate,FromTime,ToTime);
		        $("#UserGrid").trigger("reloadGrid");
		      }

    </script>--%>
     <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
 <ContentTemplate>

    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Maintenance Report for others
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="100%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td colspan="3" align="left">
                <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Maintenance Report for others</strong></td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                <br />
                <asp:Label ID="lblMsg" runat="Server" CssClass="clsMessage"></asp:Label>
                <br />
                <table id="tab" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                    <tr>
                        <td align="left" style="height: 26px; width: 50%">
                            Select From Date<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvfromdate" runat="server" ControlToValidate="txtfromdate"
                                Display="None" ErrorMessage="Please Enter From Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 26px; width: 50%">
                            <asp:TextBox ID="txtfromdate" runat="server" CssClass="clsTextField" Width="60%"></asp:TextBox>
                           <asp:Image ID="Image1" runat="server" Height="21px" ImageUrl="~/images/calen1.gif.PNG" />
                           <%-- <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                TargetControlID="txtfromdate" PopupButtonID="Image1">
                            </cc1:CalendarExtender>--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%">
                            Select To Date<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txttodate"
                                Display="None" ErrorMessage="Please Enter To Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 26px; width: 50%">
                            <asp:TextBox ID="txttodate" runat="server" CssClass="clsTextField" Width="60%"></asp:TextBox>
                           <asp:Image ID="Image2" runat="server" Height="21px" ImageUrl="~/images/calen1.gif.PNG" />
                           <%-- <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MMM-yyyy"
                                TargetControlID="txttodate" PopupButtonID="Image2">
                            </cc1:CalendarExtender>--%>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%">
                            Select From Time
                        </td>
                        <td align="left" style="height: 26px; width: 50%">
                            <asp:DropDownList ID="cbohr" runat="server" CssClass="clsComboBox" Width="97%" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%">
                            Select To Time
                        </td>
                        <td align="left" style="height: 26px; width: 50%">
                            <asp:DropDownList ID="cboToHr" runat="server" CssClass="clsComboBox" Width="97%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit" CausesValidation="true" ValidationGroup="Val1" />
                        </td>
                    </tr>
                </table>
            </td>
           
          
            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
    <%-- </div>--%>
</asp:Content>
