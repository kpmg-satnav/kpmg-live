﻿
app.service("VlanServices", function ($http, $q, UtilityService) {
    //var deferred = $q.defer();
    //this.saveVLAN = function () {
    //    deferred = $q.defer(); 
    //    return $http.post(UtilityService.path + '/api/Vlan/InsertNUpdate', dataobj)
    //        .then(function (response) {
    //            deferred.resolve(response.data);
    //            return deferred.promise;
    //        }, function (response) {
    //            deferred.reject(response);
    //            return deferred.promise;
    //        });
    //};
    //SAVE
    this.saveVLAN = function (mncCat) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/VlanAPI/InsertUpdate', mncCat)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    ////UPDATE BY ID
    //this.updateVlanServices = function (repeat) {
    //    deferred = $q.defer();
    //    return $http.post('../../../API/VLAN/InsertNUpdate', repeat)
    //        .then(function (response) {
    //            deferred.resolve(response);
    //            return deferred.promise;
    //        }, function (response) {
    //            deferred.reject(response);
    //            return deferred.promise;
    //        });
    //};
    //Bind Grid
    this.GetVlanTypeBindGrid = function () {
     
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/VlanAPI/VlanTypeBindGrid')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});

app.controller('VLANController', function ($scope, VlanServices, UtilityService, $timeout,$http) {
    $scope.StaDet = [{ Id: 1, Name: 'active' }, { Id:0, Name:'inactive' }];
    var obj = { VL_Code: '', VL_Name: '', VL_Email: '' }
    $scope.pgVar = { VLAN: angular.copy(obj) };

    //$scope.StaDet[Id] = 1;
    $scope.frmVLAN = {};
    $scope.repeatCategorylist = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;

    //to Save the data
    $scope.Save = function () {
        debugger;
        if ($scope.IsInEdit) {
            console.log($scope.VLAN.VL_Status);
            $scope.pgVar.VLAN.VL_STATUS = $scope.VLAN.VL_Status;
            $scope.pgVar.VLAN.FLAG = "UPDATE";

            VlanServices.saveVLAN($scope.pgVar.VLAN).then(function (response) {
                console.log(response);
                $scope.ShowMessage = true;
                $scope.Success = response.data.Message;
                $scope.LoadData();
                var savedobj = {};
                $scope.ClearData();
                $scope.IsInEdit = false;
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
                $scope.VLAN = {};

            }, function (error) {
                console.log(error);
            })
        }
        else {
            $scope.pgVar.VLAN.VL_STATUS = "1";
            $scope.pgVar.VLAN.FLAG = "INSERT";
            VlanServices.saveVLAN($scope.pgVar.VLAN).then(function (response) {
                $scope.ShowMessage = true;
                $scope.Success = response.data.Message;
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        getdata();
                    });
                }, 700);
                $scope.LoadData();
                $scope.VLAN = {};

            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    }

    //for GridView
    var columnDefs = [
        { headerName: "Code", field: "VL_Code", width: 160, cellClass: 'grid-align' },
        { headerName: "Name", field: "VL_Name", width: 160, cellClass: 'grid-align' },
        { headerName: "Email", field: "VL_Email", width: 160, cellClass: 'grid-align' },
        { headerName: "Status", template: "{{ShowStatus(data.VL_STATUS)}}", width: 100, cellClass: 'grid-align' },
        {
            headerName: "Action", width: 70, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align',
            onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true
        }
    ];
    // To display grid row data
    $scope.LoadData = function () {
        getdata();
    }

    function getdata() {
        VlanServices.GetVlanTypeBindGrid().then(function (data) {
            $scope.gridata = data;
            $scope.gridOptions.api.setRowData(data);
            progress(0, '', false);
        }, function (error) {
            console.log(error);
        });
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };
    $timeout($scope.LoadData, 1000);
    $scope.EditFunction = function (data) {
      
        $scope.VLAN = {};
        $scope.pgVar.VLAN.VL_Code = data;
        angular.copy(data, $scope.pgVar.VLAN);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        $('#RT_CODE').prop('readonly', true);
    }
    $scope.ClearData = function () {
        $scope.VLAN = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $('#RT_CODE').prop('readonly', false);
    }
    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

});