﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->

    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="VLANController" class="amantra">
    <%-- <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Add Service Type</legend>
                    </fieldset>
                    <div class="well">--%>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Add Service Type" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">VLAN Master</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form role="form" id="form1" name="frmVLANMaster" data-valid-submit="Save()" novalidate>
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmVLAN.$submitted && frmVLAN.VL_Code.$invalid}">

                                    <label>Code<span style="color: red;">*</span></label>
                                    <input id="VL_Code" type="text" name="Code" maxlength="50" data-ng-model="pgVar.VLAN.VL_Code" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" class="form-control" required="" />
                                    <span class="error" data-ng-show="frmVLANMaster.$submitted && frmVLANMaster.VL_Code.$invalid" style="color: red">Please Enter Code</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmVLAN.$submitted && frmVLAN.VL_Name.$invalid}">

                                    <label>Name<span style="color: red;">*</span></label>

                                    <input id="VL_Name" type="text" name="Name" maxlength="50" data-ng-model="pgVar.VLAN.VL_Name" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" class="form-control" required="" />
                                    <span class="error" data-ng-show="frmVLANMaster.$submitted && frmVLANMaster.VL_Name.$invalid" style="color: red">Please Enter Name</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmVLAN.$submitted && frmVLAN.VL_Email.$invalid}">

                                    <label>Email<span style="color: red;">*</span></label>
                                    <%--//{{VLAN.VL_Email}}--%>
                                    <input id="VL_Email" type="text" name="Email" maxlength="50" data-ng-model="pgVar.VLAN.VL_Email" class="form-control" required="" />
                                    <span class="error" data-ng-show="frmVLANMaster.$submitted && frmVLANMaster.VL_Email.$invalid" style="color: red">Please Enter Email</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div data-ng-show="ActionStatus==1">
                                        <label>Status</label>

                                        <div>
                                            <select id="VL_Status" name="VL_Status" data-ng-model="VLAN.VL_Status" class="form-control">
                                                <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" data-ng-click="Save()" />
                                    <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                    <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                    <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div data-ag-grid="gridOptions" style="height: 329px; width: 100%;" class="ag-blue"></div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>

    <script src="VLAN.js"></script>
    <script src="../SMViews/Utility.js"></script>
    <%--<script src="../../../SMViews/Utility.js"></script>--%>
    <%--<script src="../Js/ServiceType.js"></script>--%>
    <%--<script src="../JS/ServiceType.min.js"></script>--%>
</body>

</html>
