﻿<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmPropertyAuthority.aspx.vb" Inherits="Masters_Configuration_frmPropertyAuthority"
    Title="Property Authority" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>    
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Property Authority
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ShowSummary="true"
                            ForeColor="Red" ValidationGroup="Val1" />

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">City<span style="color: red;">*</span></label>

                                        <asp:CompareValidator ID="cvcity" runat="server" ErrorMessage="Please Select City"
                                            ControlToValidate="ddlCity" Display="None" ValidationGroup="Val1" Operator="NotEqual" ValueToCompare="0"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select City" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location<span style="color: red;">*</span></label>

                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Please Select Location"
                                            ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1" Operator="NotEqual" ValueToCompare="0"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Location">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Admin ID<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Admin ID"
                                            ControlToValidate="txtEmpCode" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revadminid" Display="None" ValidationGroup="Val1"
                                        runat="server" ControlToValidate="txtEmpCode" ErrorMessage="Enter Valid Admin ID"
                                        ValidationExpression="^[A-Za-z0-9]+"></asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtEmpCode" runat="server" CssClass="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Status<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please Select Status"
                                            ControlToValidate="ddlstatus" Display="None" ValidationGroup="Val1" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlstatus" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Status">
                                                <asp:ListItem Text="--Select--"></asp:ListItem>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="0">Inactive</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Add" ValidationGroup="Val1"></asp:Button>
                                    <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reset" CausesValidation="false"></asp:Button>
                                    <asp:Button ID="btnback" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-md-12">                            

                                    <asp:GridView ID="gvitems" runat="server" AllowPaging="true" AllowSorting="true"
                                        AutoGenerateColumns="false" EmptyDataText="No Property Authority Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>
                                            <asp:TemplateField HeaderText="City">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbllcode" runat="server" Text='<%#Eval("CITY_CODE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLoc" runat="server" Text='<%#Eval("LOCATION_CODE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Admin Id" SortExpression="ADMIN_ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmpId" runat="server" Text='<%#Eval("ADMIN_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("STATS") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkSpaceRelease" CausesValidation="false" CommandArgument='<%#Eval("ID")%>'
                                                        CommandName="Edit" runat="server">Edit</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                               
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
