Imports System.Data
Imports System.Data.SqlClient
Partial Class Masters_Mas_Webfiles_frmEditUser
    Inherits System.Web.UI.Page

    Dim obj As New clsMasters
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            lblMsg.Visible = False
            If Not IsPostBack Then
                obj.bindemp(cmbAurId, Me.Page)
                If cmbAurId.Items.Count = 2 Then
                    cmbAurId.SelectedIndex = 1
                    indexchanged()
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmEditUser", "Load", exp)
        End Try

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            If cmbAurId.SelectedIndex = 0 Then
                'Response.Write("<script language=javascript>alert(""Please Enter Employee Id !"")</script>")
                lblMsg.Text = "Please Enter Employee Id !"
                lblMsg.Visible = True
                Exit Sub
            End If
            If (cmbrepman.SelectedIndex <= 0) Then
                lblMsg.Text = "Please Select Valid Reporting Manager!"
                lblMsg.Visible = True
                Exit Sub
            End If

            modifyemp()
            'PopUpMessage("Updated Sucessfully")
            lblMsg.Text = "Updated Sucessfully !"
            lblMsg.Visible = True
            clear()
            If cmbAurId.Items.Count = 2 Then
                cmbAurId.SelectedIndex = 1
                indexchanged()
            End If
            'cmbAurId.SelectedIndex = 0
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmEditUser", "Button1_Click", exp)
        End Try

    End Sub
    Public Sub modifyemp()
        obj.updateemp(cmbdg, txtEmpName, cmbrepman, cmbAurId, Me, txtEmail.Text)
        obj.bindemp(cmbAurId, Me)
    End Sub
#Region " Display POPUP Messages "

    Private Sub PopUpMessage(ByVal strText As String)
        Dim strScript As String
        strScript = "<script language='javascript'>alert('" & strText & "')</script>"
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "strScript", strScript)
        Return
    End Sub

#End Region
    Protected Sub cmbAurId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAurId.SelectedIndexChanged
        Try
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            If cmbAurId.SelectedItem.Text = "--Select--" Then
                clear()
            Else
                indexchanged()

            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmEditUser", "cmbAurId_SelectedIndexChanged", exp)
        End Try
    End Sub
    Public Sub indexchanged()
        obj.useridselectedindexchanged(txtEmail, txtEmpName, cmbAurId, cmbrepman, txttoday, cmbdg)
        'obj.bindemp(cmbAurId, Me)
    End Sub

    Private Sub clear()
        txtEmpName.Text = ""
        cmbdg.SelectedIndex = 0
        cmbrepman.SelectedIndex = 0
        cmbrepman.SelectedIndex = 0
        txtext.Text = ""
        txtphres.Text = ""
        txtEmail.Text = ""
    End Sub
End Class
