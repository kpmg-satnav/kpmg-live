﻿Imports System.Data
Imports System.Data.SqlClient


Partial Class Masters_Mas_Webfiles_frmMASGeography
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Private Sub Cleardata()
        txtGCode.Text = String.Empty
        txtGName.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlGRY.SelectedIndex = 0
    End Sub

    Private Sub Insertdata()
        obj.getcode = txtGCode.Text
        obj.getname = txtGName.Text
        obj.getRemarks = txtRemarks.Text
        Dim iStaus As Integer = obj.InsertGeography(Me)
        If iStaus = 1 Then
            lblMsg.Text = "Geography Code Already Exists "
            lblMsg.Visible = True
        ElseIf iStaus = 0 Then
            lblMsg.Text = "Geography Inserted Successfully "
            lblMsg.Visible = True


            Cleardata()
            obj.Geography_LoadGrid(gvItem)
        End If
    End Sub

    Private Sub Modifydata()
        obj.getname = txtGName.Text
        obj.getRemarks = txtRemarks.Text
        If (obj.ModifyGeography(ddlGRY, Me) > 0) Then
            Cleardata()
            lblMsg.Text = "Geography Updated Successfully "
            lblMsg.Visible = True
        Else
            lblMsg.Text = "Failed to Update "
            lblMsg.Visible = True
        End If
        obj.Geography_LoadGrid(gvItem)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/Masters/MAS_Webfiles/frmMASMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If Session("UID") = "" Then
                    Response.Redirect(Application("FMGLogout"))
                Else
                    If sdr.HasRows Then
                    Else
                        Response.Redirect(Application("FMGLogout"))
                    End If
                End If
            End Using
            revCode.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
            revName.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
            lblMsg.Text = ""
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            lblMsg.Visible = False



            If Not Page.IsPostBack Then
                obj.Geography_LoadGrid(gvItem)
                Cleardata()
                trCName.Visible = False
                rbActions.Checked = True
                obj.BindGeography(ddlGRY)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmGeography", "Load", exp)
        End Try
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        Try
            If rbActions.Checked = True Then
                trCName.Visible = False
                txtGCode.ReadOnly = False
                btnSubmit.Text = "Submit"
                Cleardata()
            Else
                Cleardata()
                trCName.Visible = True
                txtGCode.ReadOnly = True
                btnSubmit.Text = "Modify"
                Cleardata()
                obj.BindGeography(ddlGRY)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmGeography", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                Insertdata()
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                Modifydata()
                obj.BindGeography(ddlGRY)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmGeography", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlGRY_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGRY.SelectedIndexChanged
        Try
            If ddlGRY.SelectedValue <> "--Select--" Then
                obj.Geography_SelectedIndex_Changed(ddlGRY)
                txtGCode.Text = obj.getcode
                txtGName.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
            Else
                Cleardata()
                obj.BindGeography(ddlGRY)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmGeography", "ddlGRY_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            obj.Geography_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmGeography", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                Dim iStatus As Integer = obj.Geography_Rowcommand(gvItem, index)
                If iStatus = 0 Then
                    gvItem.HeaderRow.Cells(2).Visible = False
                    gvItem.HeaderRow.Cells(3).Visible = False
                    For i As Integer = 0 To gvItem.Rows.Count - 1
                        gvItem.Rows(i).Cells(2).Visible = False
                        gvItem.Rows(i).Cells(3).Visible = False
                    Next
                    'PopUpMessage("First inactivate all the Function under this Geography ", Me)
                    lblMsg.Text = "First inactivate all the Function under this Geography "
                    lblMsg.Visible = True
                    Exit Sub
                Else
                    obj.BindGeography(ddlGRY)
                    obj.Geography_LoadGrid(gvItem)
                    Cleardata()
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmGeography", "gvItem_RowCommand", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub
End Class