<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmLocationAvailableEdit.aspx.vb" Inherits="Masters_Mas_Webfiles_frmLocationAvailableEdit"
    Title="Modify Available and Occupied Locations " %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table id="table5" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Modify Available and Occupied Locations
             <hr align="center" width="60%" /></asp:Label></td>
            </tr>
        </table>
        <table id="table3" cellspacing="0" cellpadding="0" width="85%" align="center" border="0">
            <tr>
                <td colspan="3" align="left">
                    <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 36px">
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Available Locations Master</strong>&nbsp;</td>
                <td>
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif" style="width: 36px">
                </td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" ValidationGroup="Val1" />
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <table id="tab" runat="Server" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="center" style="height: 28px; width: 25%">
                                Search by Location
                            </td>
                            <td align="left" style="height: 28px; width: 25%">
                                <asp:TextBox ID="txtloc" runat="server" CssClass="clstextField" Width="100%"></asp:TextBox>
                            </td>
                            <td align="left" style="height: 28px">
                                <asp:Button ID="btnSubmit" runat="Server" CssClass="button" Text="Submit" />
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="pnlgrid" runat="Server" Width="100%">
                        <asp:GridView ID="gvitems" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" Width="100%">
                            <Columns>
                                <asp:TemplateField Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblloc" runat="server" Text='<%#Eval("LOCATION_CODE") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="LOCATION">
                                    <ItemTemplate>
                                        <asp:Label ID="lbllocname" runat="server" Text='<%#Eval("LOCATION_NAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Available WT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblawt" runat="server" Text='<%#Eval("AVAILABLE_WT") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Occupied WT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblowt" runat="server" Text='<%#Eval("OCCUPIED_WT") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Available WI">
                                    <ItemTemplate>
                                        <asp:Label ID="lblawi" runat="server" Text='<%#Eval("AVAILABLE_WI") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Occupied WI">
                                    <ItemTemplate>
                                        <asp:Label ID="lblowi" runat="server" Text='<%#Eval("OCCUPIED_WI") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Available WBPO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblawbpo" runat="server" Text='<%#Eval("AVAILABLE_WBPO") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Occupied WBPO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblowbpo" runat="server" Text='<%#Eval("OCCUPIED_WBPO") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Available WSTL">
                                    <ItemTemplate>
                                        <asp:Label ID="lblawstl" runat="server" Text='<%#Eval("AVAILABLE_WSTL") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Occupied WBPO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblowstl" runat="server" Text='<%#Eval("OCCUPIED_WSTL") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:ButtonField Text="DELETE" CommandName="DELETE" />
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px; width: 861px;" background="../../Images/table_bot_mid_bg.gif">
                    <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
