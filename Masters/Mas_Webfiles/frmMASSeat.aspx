<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmMASSeat.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASSeat" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script language="javascript" type="text/javascript" defer> 
    function noway(go)
    {
        if(document.all)
        {
            if (event.button == 2)
            {
                alert('For Security reasons Right click option was disabled,Inconvenience regreted!');
                return false;
            }
        }
        if (document.layers) 
        {
            if (go.which == 3)
            {
                alert('For Security reasons Right click option was disabled,,Inconvenience regreted!');
                return false;
            }
        }
    } 
     if (document.layers) 
     {
        document.captureEvents(Event.MOUSEDOWN); 
     }
     document.onmousedown=noway;
    </script>
<div>
 
              <table id="table1" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center"> <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False" ForeColor="Black">Seat Master
             <hr align="center" width="60%" /></asp:Label></td>
                        </tr>
                        </table>
<asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
           
           <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align:top;" width="95%"  align="center" border="0">
                    <tr>
						<td><img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
						<td width="100%"  class="tableHEADER" align="left"  >&nbsp;<strong>Seat Details</strong> </td>
						<td><img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
					</tr>
								
                <tr>
            			<td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
            			<td align="center"><br />
               
                       
                                   <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="1">
        
        <tr>
          
        </tr>
        <tr>
          <td align="center" colspan="2" style="height: 43px"><asp:RadioButtonList ID="rbActions" runat="server" CssClass="clsRadioButton" RepeatDirection="Horizontal"
									AutoPostBack="True">
              <asp:ListItem Value="0" Selected="True">Add</asp:ListItem>
              <asp:ListItem Value="1">Modify</asp:ListItem>
          </asp:RadioButtonList></td>
        </tr>
                                       <tr id="trLName" runat="server">
                                           <td align="left" class="clslabel" style="height: 11px; width: 50%;">
                                               <asp:Label ID="lblSeatname" runat="server" Text="Select Seat" Font-Bold="False"></asp:Label>
                                               <span style="font-size: 6pt; color: #ff0000">*<asp:RequiredFieldValidator
                                                   ID="rfvLAN" runat="server" ControlToValidate="ddlSeat" Display="None" EnableTheming="False"
                                                   ErrorMessage="Please Select Seat"></asp:RequiredFieldValidator></span></td>
                                           <td align="left" style="width: 43%; height: 11px" >
                                               <asp:DropDownList ID="ddlSeat" tabIndex="1"  runat="server" Width="97%" AutoPostBack="True" CssClass="clsComboBox">
                                               <asp:ListItem Value="1">--Select--</asp:ListItem>
                                               </asp:DropDownList></td>
                                       </tr>
        <tr>
          <td align="left" class="clslabel" style="width: 50%; height: 26px;">
              Seat Code
          <font class="clsNote">*</font>
[Enter code in alphanumerics and upto 15 characters allowed] <asp:RequiredFieldValidator
              ID="rfvLC" runat="server" ControlToValidate="txtSeatcode" Display="None"
              ErrorMessage="Please Enter Seat Code"></asp:RequiredFieldValidator>
              <asp:RegularExpressionValidator ID="regCod" runat="server" ControlToValidate="txtSeatcode"
                  Display="None" ErrorMessage="Please Enter Seat Code in alphanumerics only"
                  ValidationExpression="^[0-9a-zA-Z]+"></asp:RegularExpressionValidator></td>
          <td align="left" style="width: 43%; height: 26px;">
              <asp:TextBox ID="txtSeatcode" runat="server" width="97%" CssClass="clsTextField" MaxLength="15"></asp:TextBox></td>
        </tr>
        <tr>
          <td align="left" class="clslabel" style="height: 16px; width: 50%;">
              Seat Name<font class="clsNote">*</font>
[Enter Name in alphanumerics and (space,-,_ ,(,),\ allowed) and upto 50 characters allowed]<asp:RequiredFieldValidator ID="rfvLN" runat="server" ControlToValidate="txtSeatName"
                  Display="None" ErrorMessage="Please Enter Seat Name"></asp:RequiredFieldValidator>
              <asp:RegularExpressionValidator ID="regNam" runat="server" ControlToValidate="txtSeatName"
                  Display="None" ErrorMessage="Please Enter Seat Name in alphanumerics and (space,-,_ ,(,) allowed)"
                  ValidationExpression="^[0-9a-zA-Z-_\() ]+"></asp:RegularExpressionValidator></td>
          <td align="left" style="height: 16px; width: 43%;">
              <asp:TextBox ID="txtSeatName" runat="server" Width="97%" CssClass="clsTextField" MaxLength="50"></asp:TextBox></td>
        </tr>
                                       <tr>
                                           <td align="left" class="clslabel" style="width: 50%; height: 16px">
                                               Block Name<span style="font-size: 6pt; color: #ff0000">*<asp:RequiredFieldValidator
                                                   ID="rfvBN" runat="server" ControlToValidate="ddlBlock" Display="None" ErrorMessage="Please Select Block"></asp:RequiredFieldValidator>
                                               </span>
                                           </td>
                                           <td align="left" style="width: 43%; height: 16px">
                                               <asp:DropDownList ID="ddlBlock" tabIndex="1"  runat="server" Width="97%" AutoPostBack="True" CssClass="clsComboBox">
                                                   <asp:ListItem Value="1">--Select--</asp:ListItem>
                                               </asp:DropDownList></td>
                                       </tr>
                                       <tr>
                                               <td align="left" class="clslabel" style="width: 50%; height: 16px">
                                               Floor Name <span style="font-size: 6pt; color: #ff0000">*</span><asp:RequiredFieldValidator
                                                   ID="rfvFV" runat="server" ControlToValidate="ddlFloor" Display="None" ErrorMessage="Please Enter Floor"></asp:RequiredFieldValidator></td>
                                               <td align="left" style="width: 43%; height: 16px">
                                               <asp:DropDownList ID="ddlFloor" tabIndex="1"  runat="server" Width="97%" AutoPostBack="True" CssClass="clsComboBox">
                                                   <asp:ListItem Value="1">--Select--</asp:ListItem>
                                               </asp:DropDownList></td>
                                           </tr>  
                                       <tr>
                                           <td align="left" class="clslabel" style="width: 50%; height: 16px">
                                               Tower Name <span style="font-size: 6pt; color: #ff0000">*
                                                   <asp:RequiredFieldValidator ID="rfvTN" runat="server" ControlToValidate="ddlTower" Display="None"
                                                       ErrorMessage="Please Select Tower"></asp:RequiredFieldValidator></span></td>
                                           <td align="left" style="width: 43%; height: 16px">
                                               <asp:DropDownList ID="ddlTower" tabIndex="1"  runat="server" Width="97%" AutoPostBack="True" CssClass="clsComboBox">
                                                   <asp:ListItem Value="1">--Select--</asp:ListItem>
                                               </asp:DropDownList></td>
                                       </tr>
                                       <tr>
                                           <td align="left" class="clslabel" style="width: 50%; height: 16px">
                                               Location Name <span style="font-size: 6pt; color: #ff0000">*</span><asp:RequiredFieldValidator
                                                   ID="rfvLname" runat="server" ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please Enter Location"></asp:RequiredFieldValidator></td>
                                           <td align="left" style="width: 43%; height: 16px">
                                               <asp:DropDownList ID="ddlLocation" tabIndex="1"  runat="server" Width="97%" AutoPostBack="True" CssClass="clsComboBox">
                                                   <asp:ListItem Value="1">--Select--</asp:ListItem>
                                               </asp:DropDownList></td>
                                       </tr>
                                       <tr>
                                           <td align="left" class="clslabel" style="width: 50%; height: 16px">
                                               City Name<span style="font-size: 6pt; color: #ff0000">*<asp:RequiredFieldValidator
                                                   ID="rfvCname" runat="server" ControlToValidate="ddlCity" Display="None" EnableTheming="False"
                                                   ErrorMessage="Please Select City"></asp:RequiredFieldValidator></span></td>
                                           <td align="left" style="width: 43%; height: 16px">
                                               <asp:DropDownList ID="ddlCity" tabIndex="1"  runat="server" Width="97%" AutoPostBack="True" CssClass="clsComboBox">
                                               <asp:ListItem Value="1">--Select--</asp:ListItem>
                                               </asp:DropDownList></td>
                                       </tr>
                                       <tr>
                                           <td align="left" class="clslabel" style="width: 50%; height: 16px">
                                               Country Name <span style="font-size: 6pt; color: #ff0000">*
                                                   <asp:RequiredFieldValidator ID="rfvCN" runat="server" ControlToValidate="ddlCountry"
                                                       Display="None" ErrorMessage="Please Select  Country"></asp:RequiredFieldValidator></span></td>
                                           <td align="left" style="width: 43%; height: 16px">
                                               <asp:DropDownList ID="ddlCountry" tabIndex="1"  runat="server" Width="97%" AutoPostBack="True" CssClass="clsComboBox">
                                               <asp:ListItem Value="1">--Select--</asp:ListItem>
                                           </asp:DropDownList></td>
                                       </tr>
                                       <tr>
                                           <td align="left" class="clslabel" style="width: 50%; height: 16px">
                                               Remarks<font class="clsNote">*</font>[Enter Remarks upto 500 characters.Space,AlphaNumerics and special characters allowed]
                                               <asp:RequiredFieldValidator ID="rfvSeat" runat="server" ControlToValidate="txtRemarks"
                                                   Display="None" ErrorMessage="Please Enter Remarks"></asp:RequiredFieldValidator></td>
                                           <td align="left" style="width: 43%; height: 16px">
                                               <asp:TextBox ID="txtRemarks" runat="server" CssClass="clsTextField" TextMode="MultiLine"
                                                   Width="97%" Height="35px" MaxLength="500"></asp:TextBox></td>
                                       </tr>
                                       
      </table>
      <table class="table" style="HEIGHT: 22px" width="100%" border="1">
													<tr>
														<td align="center" style="height: 20px">
                                                            &nbsp;<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                                                ShowSummary="False" />
															<asp:button id="btnSubmit" runat="server" Width="76px" CssClass="button" Text="Submit"></asp:button></td>
													</tr>
          <tr>
              <td align="center" style="height: 20px">
                  <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" Width="100%" PageSize="20">
                      <Columns>
                          <asp:BoundField DataField="SPC_NAME" ItemStyle-HorizontalAlign="Left"  HeaderText="Seat Name" />
                          <asp:ButtonField HeaderText="Status"  ItemStyle-HorizontalAlign="Left" CommandName="Status" Text="Button" />
                          <asp:BoundField DataField="SPC_STA_ID" ItemStyle-HorizontalAlign="Left"  HeaderText="STID" />
                          <asp:BoundField DataField="SPC_CODE" ItemStyle-HorizontalAlign="Left"  HeaderText="Seat Code" />
                      </Columns>
                  </asp:GridView>
              </td>
          </tr>
	  </table>
												<br />
 </td>
               <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        &nbsp;</td>
                 </tr>
	          <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>   
           
		          
               
              </table>
           
        </asp:Panel>
       
      
    </div>
   </asp:Content>