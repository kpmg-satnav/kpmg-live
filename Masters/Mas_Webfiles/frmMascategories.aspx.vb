﻿Imports System.Data
Partial Class Masters_Mas_Webfiles_frmMascategories
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Bindcategories()
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CATEGORIES_GRID")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        gvItem.DataSource = sp.GetDataSet()
        gvItem.DataBind()
    End Sub
    Private Sub Bindcategories()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_CLASSIFICATION1")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlLinkName.DataSource = sp.GetDataSet()
        ddlLinkName.DataTextField = "CategoryName"
        ddlLinkName.DataValueField = "idcategory"
        ddlLinkName.DataBind()
        ddlLinkName.Items.Insert(0, New ListItem("--Parent ID--", "0"))
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        UpdateRequest()
        lblMsg.Text = "Request Item Added Succesfully"
        lblMsg.Visible = True
        Cleardata()
        Bindcategories()
        BindGrid()
    End Sub
    Private Sub updateRequest()
        Try

            Dim StartHr As String = "00"
            Dim EndHr As String = "00"
            Dim StartMM As String = "00"
            Dim EndMM As String = "00"
            Dim ftime As String
            Dim ttime As String

            If starttimehr.SelectedItem.Value <> "Hr" Then
                StartHr = starttimehr.SelectedItem.Text
            End If
            If starttimemin.SelectedItem.Value = "Min" Then
                StartMM = starttimemin.SelectedItem.Text
            End If
            If endtimehr.SelectedItem.Value <> "Hr" Then
                EndHr = endtimehr.SelectedItem.Text
            End If
            If endtimemin.SelectedItem.Value = "Min" Then
                EndMM = endtimemin.SelectedItem.Text
            End If

            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_CATEGORIES")
            sp.Command.AddParameter("@cls_Description", txtLinkName.Text, DbType.String)
            sp.Command.AddParameter("@CLS_ID", ddlLinkName.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@UPDATED_BY", Session("UID"), DbType.String)
            sp.Command.AddParameter("@STAT", ddlstatus.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@FROM_TIME", ftime, DbType.DateTime)
            sp.Command.AddParameter("@TO_TIME", ttime, DbType.DateTime)
            sp.ExecuteScalar()
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub cleardata()
        ddlLinkName.SelectedIndex = -1
        txtLinkName.Text = ""
        txtURL.Text = ""
    End Sub

    Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
End Class
