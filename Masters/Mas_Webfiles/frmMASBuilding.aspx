<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmMASBuilding.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASBuilding"
    Title="Building Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <div>
        <table id="table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Building Master
             <hr align="center" width="60%" /></asp:Label>
                </td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
            <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp; Building Details </strong>
                    </td>
                    <td>
                        <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="divmessagebackground"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <table id="tab" cellspacing="1" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td align="center" colspan="2" style="height: 43px">
                                    <asp:RadioButtonList ID="rbActions" runat="server" CssClass="clsRadioButton" RepeatDirection="Horizontal"
                                        AutoPostBack="True">
                                        <asp:ListItem Value="0" Selected="True">Add</asp:ListItem>
                                        <asp:ListItem Value="1">Modify</asp:ListItem>
                                    </asp:RadioButtonList></td>
                            </tr>
                        </table>
                        <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="1">
                            <tr>
                                <td align="left" width="25%">
                                    <asp:Label ID="lbllocation" runat="server" CssClass="bodytext" Text="Select Location"></asp:Label>
                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvlocation" runat="server" ControlToValidate="ddlLocation"
                                        Display="Dynamic" ErrorMessage="Select Location" ValidationGroup="Val1" InitialValue="--Select Location--"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" width="25%">
                                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="dropdown" Width="99%">
                                    </asp:DropDownList>
                                    <br />
                                </td>
                                <td align="left" width="25%">
                                    <asp:Label ID="lblBuilding" runat="server" CssClass="bodytext" Text="Building Name"></asp:Label>
                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBuilding"
                                        Display="Dynamic" ErrorMessage="Required Building Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" width="25%">
                                    <asp:TextBox ID="txtBuilding" CssClass="textbox" runat="server" Width="97%"></asp:TextBox>
                                   
                                     <asp:DropDownList ID="ddlbdgname" runat="server" CssClass="dropdown" Width="99%" Visible="false" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                
                            </tr>
                            <tr>
                                <td align="left" width="25%">
                                    <asp:Label ID="lblBuildingCode" runat="server" CssClass="bodytext" Text="Building Code"></asp:Label>
                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBuildingCode"
                                        Display="Dynamic" ErrorMessage="Required Building Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" width="25%">
                                    <asp:TextBox ID="txtBuildingCode" CssClass="textbox" runat="server" MaxLength="50"
                                        Width="97%"></asp:TextBox>
                                    <br />
                                </td>
                                <td align="left" width="25%">
                                    <asp:Label ID="lblPreQID" runat="server" CssClass="bodytext" Text="Building PREQ ID"></asp:Label>
                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvID" runat="server" ControlToValidate="TXTPREQID"
                                        Display="Dynamic" ErrorMessage="Required Building PREQID" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" width="25%">
                                    <asp:TextBox ID="TXTPREQID" CssClass="textbox" runat="server" Width="97%"></asp:TextBox>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="25%">
                                    <asp:Label ID="lblGSCID" runat="server" CssClass="bodytext" Text="Building GSCID"></asp:Label>
                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RFVGSCID" runat="server" ControlToValidate="txtGSCID"
                                        Display="Dynamic" ErrorMessage="Required Building GSC ID" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revgscid" runat="server" ControlToValidate="txtGSCID"
                                        ErrorMessage="Please enter valid GSCID" ValidationExpression="^[0-9]*$" ValidationGroup="Val1">
                                    </asp:RegularExpressionValidator>
                                </td>
                                <td align="left" width="25%">
                                    <div onmouseover="Tip('Enter ID in numbers only)" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtGSCID" CssClass="textbox" runat="server" Width="97%"></asp:TextBox>
                                        <br />
                                </td>
                                <td align="left" width="25%">
                                    <asp:Label ID="lblAddress" runat="server" CssClass="bodytext" Text="Building Address"></asp:Label>
                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress"
                                        Display="Dynamic" ErrorMessage="Required Building Address" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" width="25%">
                                    <asp:TextBox ID="txtAddress" CssClass="textbox" runat="server" Width="97%" TextMode="MultiLine"
                                        Rows="5"></asp:TextBox>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="25%">
                                    <asp:Label ID="Label1" runat="server" CssClass="bodytext" Text="Select Building Category"></asp:Label>
                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlcategory"
                                        Display="Dynamic" ErrorMessage="Select Building Category" ValidationGroup="Val1"
                                        InitialValue="--Select Category--"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" width="25%">
                                    <asp:DropDownList ID="ddlcategory" runat="server" CssClass="dropdown" Width="99%">
                                    </asp:DropDownList>
                                    <br />
                                </td>
                                <td align="left" width="25%">
                                    <asp:Label ID="lblStatus" runat="server" CssClass="bodytext" Text="Select Status"></asp:Label>
                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlstatus"
                                        Display="Dynamic" ErrorMessage="Please Select Status" ValidationGroup="Val1"
                                        InitialValue="--Select Status--"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" width="25%">
                                    <asp:DropDownList ID="ddlstatus" runat="server" CssClass="dropdown" Width="99%">
                                        <asp:ListItem>--Select Status--</asp:ListItem>
                                        <asp:ListItem Value="0">InActive</asp:ListItem>
                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                            </tr>
                            <tr>
                                <td align="left" width="25%">
                                    <asp:Label ID="lbCpArea" runat="server" CssClass="bodytext" Text="CarpetArea"></asp:Label>
                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvCarpetArea" runat="server" ControlToValidate="txtcparea"
                                        Display="Dynamic" ErrorMessage="Required Building Carpet-Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtcparea"
                                        ErrorMessage="Please enter valid Carpet Area" ValidationGroup="Val1" Type="double"
                                        Operator="DataTypeCheck"></asp:CompareValidator>
                                </td>
                                <td align="left" width="25%">
                                    <div onmouseover="Tip('Enter CarpetArea in numbers and decimals)" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtcparea" CssClass="textbox" runat="server" Width="97%" MaxLength="5"></asp:TextBox>
                                        <br />
                                </td>
                                <td align="left" width="25%">
                                    <asp:Label ID="lblBUArea" runat="server" CssClass="bodytext" Text="Built-Up Area"></asp:Label>
                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvBuiltuparea" runat="server" ControlToValidate="txtBuiltuparea"
                                        Display="Dynamic" ErrorMessage="Required Building Carpet-Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtBuiltuparea"
                                        ErrorMessage="Please enter valid Builtup Area" ValidationGroup="Val1" Type="double"
                                        Operator="DataTypeCheck"></asp:CompareValidator>
                                </td>
                                <td align="left" width="25%">
                                    <div onmouseover="Tip('Enter BuiltupArea in numbers and decimals)" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtBuiltuparea" CssClass="textbox" runat="server" Width="97%" AutoPostBack="True"
                                            MaxLength="5"></asp:TextBox>
                                        <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="25%">
                                    <asp:Label ID="lblPMSID" runat="server" CssClass="bodytext" Text="Building PMS_ID"></asp:Label>
                                    <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RFVPMSID" runat="server" ControlToValidate="txtPMSID"
                                        Display="Dynamic" ErrorMessage="Required Building PMSID" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" width="25%">
                                    <asp:TextBox ID="txtPMSID" CssClass="textbox" runat="server" MaxLength="500" Width="97%"></asp:TextBox>
                                </td>
                                <td align="left" width="25%">
                                    <asp:Label ID="lblLatLng" runat="server" CssClass="bodytext" Text="Enter Latitude Longitude for Building"></asp:Label>
                                    <font class="clsNote">*</font>
                                </td>
                                <td align="left" width="50%">
                                    <asp:TextBox ID="txtLat" CssClass="textbox" runat="server" MaxLength="8" Width="45%"></asp:TextBox>
                                    <asp:TextBox ID="txtLon" CssClass="textbox" runat="server" MaxLength="8" Width="45%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="25%">
                                    <asp:Label ID="lblBldgDesc" runat="server" CssClass="bodytext" Text="Building Description"></asp:Label>
                                    <font class="clsNote">*</font>
                                </td>
                                <td align="left" width="25%">
                                    <asp:TextBox ID="txtBldgDesc" CssClass="textbox" runat="server" MaxLength="500" TextMode="MultiLine"
                                        Width="97%" Rows="5"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <table class="table" style="height: 22px" width="100%" border="1">
                            <tr>
                                <td align="center" style="height: 10px">
                                    &nbsp;
                                    <asp:Button ID="btnSubmit" runat="server" Width="76px" CssClass="button" Text="Submit"
                                        ValidationGroup="Val1"></asp:Button>&nbsp;
                                    <asp:Button ID="btnback" runat="server" CssClass="button" Text="Back" Width="76px" /></td>
                            </tr>
                            <tr>
                                <td align="center" style="height: 20px">
                                    <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        Width="100%" PageSize="20" EmptyDataText="No Records Found">
                                        <Columns>
                                         <asp:TemplateField HeaderText="Building Code">
                                         <ItemTemplate>
                                         <asp:Label ID="lblbcode" runat="Server" Text='<% #bind("BDG_ADM_CODE") %>'></asp:Label>
                                         </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Building Name">
                                         <ItemTemplate>
                                         <asp:Label ID="lblbname" runat="Server" Text='<% #bind("BDG_NAME") %>'></asp:Label>
                                         </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Location Name">
                                         <ItemTemplate>
                                         <asp:Label ID="lblblname" runat="Server" Text='<% #bind("BDG_LOC_ID") %>'></asp:Label>
                                         </ItemTemplate>
                                         </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                         <ItemTemplate>
                                          <asp:LinkButton ID="lnkStatus" runat="server" Text='<%#BIND("BDG_STA_ID")%>' CommandArgument='<%#BIND("BDG_STA_ID")%>'
                                        CommandName="Status"></asp:LinkButton>
                                         </ItemTemplate>
                                         </asp:TemplateField>
 
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
