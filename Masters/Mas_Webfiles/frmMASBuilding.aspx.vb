Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class Masters_Mas_Webfiles_frmMASBuilding
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            lblMsg.Visible = False
            
            If Not Page.IsPostBack Then
                Bdg_LoadGrid()
                getbdg_Location()
                getbdg_Category()
                getbdgname()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASFloor", "Load", exp)
        End Try

    End Sub
    Private Sub getbdgname()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTBDG")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlbdgname.DataSource = sp.GetDataSet()
        ddlbdgname.DataTextField = "BDG_NAME"
        ddlbdgname.DataValueField = "BDG_ADM_CODE"
        ddlbdgname.DataBind()
        ddlbdgname.Items.Insert(0, New ListItem("--Select Building--", "0"))
    End Sub
    Private Sub getbdg_Location()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_BDG_LOCATION")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))
    End Sub
    Private Sub getbdg_Category()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CATEGORY_TYPE")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlcategory.DataSource = sp.GetDataSet()
        ddlcategory.DataTextField = "PN_PROPERTYTYPE"
        ddlcategory.DataValueField = "PN_TYPEID"
        ddlcategory.DataBind()
        ddlcategory.Items.Insert(0, New ListItem("--Select Category--", "0"))
    End Sub
    Private Sub Bdg_LoadGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_BDGGRID")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        gvItem.DataSource = sp.GetDataSet()
        gvItem.DataBind()
        For i As Integer = 0 To gvItem.Rows.Count - 1
            Dim lnkStatus As LinkButton = CType(gvItem.Rows(i).FindControl("lnkStatus"), LinkButton)
            If lnkStatus.Text = "1" Then
                lnkStatus.Text = "Active"
            Else
                lnkStatus.Text = "InActive"
            End If
        Next
        'sp.ExecuteScalar()
    End Sub
    Private Sub Cleardata()

        ddlLocation.SelectedValue = 0
        txtBuildingCode.Text = ""
        txtBuilding.Text = ""
        TXTPREQID.Text = ""
        txtGSCID.Text = ""
        txtAddress.Text = ""
        ddlcategory.SelectedValue = 0
        ddlstatus.SelectedIndex = -1
        txtcparea.Text = ""
        txtBuiltuparea.Text = ""
        txtPMSID.Text = ""
        txtLat.Text = ""
        txtLon.Text = ""
        txtBldgDesc.Text = ""
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.SelectedItem.Text = "Add" Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                If txtBldgDesc.Text.Length > 500 Then
                    lblMsg.Text = "Please Enter remarks in less than or equal to 500 characters"
                    lblMsg.Visible = True
                Else
                    insertdata()
                    getbdgname()
                    Bdg_LoadGrid()
                End If
            ElseIf rbActions.SelectedItem.Text = "Modify" Then
               
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If txtBldgDesc.Text.Length > 500 Then
                    lblMsg.Text = "Please Enter remarks in less than or equal to 500 characters"
                    lblMsg.Visible = True
                Else
                    Modifydata()

                    Bdg_LoadGrid()
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmMASBuilding", "btnSubmit_Click", exp)
        End Try
    End Sub
    Private Sub insertdata()
        Dim ValidateCode As Integer
        ValidateCode = ValidateBuildingCode()
        If ValidateCode = 0 Then
            lblMsg.Text = "Building Code already exist please Enter another code"
        ElseIf ValidateCode = 1 Then
            Try
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_BUILDING")
                sp.Command.AddParameter("@BDG_ADM_CODE", txtBuildingCode.Text, DbType.String)
                sp.Command.AddParameter("@BDG_NAME", txtBuilding.Text, DbType.String)
                sp.Command.AddParameter("@BDG_DESC", txtBldgDesc.Text, DbType.String)
                sp.Command.AddParameter("@BDG_PREQ_ID", TXTPREQID.Text, DbType.String)
                sp.Command.AddParameter("@BDG_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@BDG_GSC_ID", txtGSCID.Text, DbType.Int32)
                sp.Command.AddParameter("@BDG_ADDRESS", txtAddress.Text, DbType.String)
                sp.Command.AddParameter("@BDG_CATEGORY", ddlcategory.SelectedItem.Text, DbType.String)
                sp.Command.AddParameter("@BDG_STA_ID", ddlstatus.SelectedItem.Value, DbType.Int32)
                sp.Command.AddParameter("@BDG_UPT_BY", Session("UID"), DbType.String)
                sp.Command.AddParameter("@BDG_CPAREA", txtcparea.Text, DbType.String)
                sp.Command.AddParameter("@BDG_BUAREA", txtBuiltuparea.Text, DbType.String)
                sp.Command.AddParameter("@BDG_PMS_ID", txtPMSID.Text, DbType.Int32)
                sp.Command.AddParameter("@X_LAT", txtLat.Text, DbType.Decimal)
                sp.Command.AddParameter("@Y_LON", txtLon.Text, DbType.Decimal)
                sp.ExecuteScalar()
                lblMsg.Text = "New Building Added Succesfully"
                lblMsg.Visible = True
                Cleardata()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If
    End Sub
    Private Sub Modifydata()
        Dim ValidateCode1 As Integer
        ValidateCode1 = ValidateBuildingCode1()
        If ValidateCode1 = 0 Then
            lblMsg.Text = "Building Code already exist please Enter another code"
            lblMsg.Visible = True
        ElseIf ValidateCode1 = 1 Then
            Try
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_BUILDING")

                sp.Command.AddParameter("@BDG_ADM_CODE", txtBuildingCode.Text, DbType.String)
                sp.Command.AddParameter("@BDG_NAME", ddlbdgname.SelectedItem.Text, DbType.String)
                sp.Command.AddParameter("@BDG_DESC", txtBldgDesc.Text, DbType.String)
                sp.Command.AddParameter("@BDG_PREQ_ID", TXTPREQID.Text, DbType.String)
                sp.Command.AddParameter("@BDG_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@BDG_GSC_ID", txtGSCID.Text, DbType.Int32)
                sp.Command.AddParameter("@BDG_ADDRESS", txtAddress.Text, DbType.String)
                sp.Command.AddParameter("@BDG_CATEGORY", ddlcategory.SelectedItem.Text, DbType.String)
                sp.Command.AddParameter("@BDG_STA_ID", ddlstatus.SelectedItem.Value, DbType.Int32)
                sp.Command.AddParameter("@BDG_UPT_BY", Session("UID"), DbType.String)
                sp.Command.AddParameter("@BDG_CPAREA", txtcparea.Text, DbType.String)
                sp.Command.AddParameter("@BDG_BUAREA", txtBuiltuparea.Text, DbType.String)
                sp.Command.AddParameter("@BDG_PMS_ID", txtPMSID.Text, DbType.Int32)
                sp.Command.AddParameter("@X_LAT", txtLat.Text, DbType.Decimal)
                sp.Command.AddParameter("@Y_LON", txtLon.Text, DbType.Decimal)
                sp.ExecuteScalar()
                lblMsg.Text = "Property Modified Succesfully"
                lblMsg.Visible = True
                ddlbdgname.SelectedValue = 0
                Cleardata()

            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If
    End Sub
    Private Function ValidateBuildingCode1()
        Dim ValidateCode1 As Integer
        Dim BDG_ADM_CODE As String = txtBuildingCode.Text
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_BDG1_CODE")
        sp.Command.AddParameter("@BDG_ADM_CODE", BDG_ADM_CODE, DbType.String)
        ValidateCode1 = sp.ExecuteScalar()
        Return ValidateCode1
    End Function
    Private Function ValidateBuildingCode()
        Dim ValidateCode As Integer
        Dim BDG_ADM_CODE As String = txtBuildingCode.Text
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_BDG_CODE")
        sp1.Command.AddParameter("@BDG_ADM_CODE", BDG_ADM_CODE, DbType.String)
        ValidateCode = sp1.ExecuteScalar()
        Return ValidateCode
    End Function

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            Bdg_LoadGrid()
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASFloor", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim lnkStatus As LinkButton = DirectCast(e.CommandSource, LinkButton)
                Dim gvRow As GridViewRow = DirectCast(lnkStatus.NamingContainer, GridViewRow)
                Dim status As Integer = CInt(e.CommandArgument)
                Dim lblbcode As Label = DirectCast(gvRow.FindControl("lblbcode"), Label)

                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPT_BDG_STATUS")
                sp.Command.AddParameter("@BDG_ADM_CODE", lblbcode.Text, DbType.String)

                sp.Command.AddParameter("@BDG_STA_ID", 1 - status, DbType.Int32)
                sp.ExecuteScalar()
            End If
            Bdg_LoadGrid()
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASBuilding", "gvItem_RowCommand", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub

    Protected Sub ddlbdgname_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlbdgname.SelectedIndexChanged
        'Try

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_BUILDING_DETAILS")
        sp.Command.AddParameter("@BDG_ADM_CODE", ddlbdgname.SelectedItem.Value, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()

        If ds.Tables(0).Rows.Count > 0 Then

            Dim li As ListItem = Nothing
            li = ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_LOC_ID"))
            If Not li Is Nothing Then
                ddlLocation.ClearSelection()
                li.Selected = True
            End If

            txtBuildingCode.Text = ds.Tables(0).Rows(0).Item("BDG_ADM_CODE")
            'txtBuilding.Text = ds.Tables(0).Rows(0).Item("BDG_NAME")
            TXTPREQID.Text = ds.Tables(0).Rows(0).Item("BDG_PREQ_ID")
            txtGSCID.Text = ds.Tables(0).Rows(0).Item("BDG_GSC_ID")
            txtAddress.Text = ds.Tables(0).Rows(0).Item("BDG_ADDRESS")

            Dim li2 As ListItem = Nothing
            li2 = ddlcategory.Items.FindByText(ds.Tables(0).Rows(0).Item("BDG_CATEGORY"))
            If Not li2 Is Nothing Then
                ddlcategory.ClearSelection()
                li2.Selected = True
            End If

            Dim li1 As ListItem = Nothing
            li1 = ddlstatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("BDG_STA_ID"))
            If Not li1 Is Nothing Then
                ddlstatus.ClearSelection()
                li1.Selected = True
            End If
            txtcparea.Text = ds.Tables(0).Rows(0).Item("BDG_CPAREA")
            txtBuiltuparea.Text = ds.Tables(0).Rows(0).Item("BDG_BUAREA")
            txtPMSID.Text = ds.Tables(0).Rows(0).Item("BDG_PMS_ID")
            txtLat.Text = ds.Tables(0).Rows(0).Item("X_LAT")
            txtLon.Text = ds.Tables(0).Rows(0).Item("Y_LON")
            txtBldgDesc.Text = ds.Tables(0).Rows(0).Item("BDG_DESC")
        End If
        'Catch exp As System.Exception
        '    Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASFloor", "gvItem_PageIndexChanging", exp)
        'End Try

    End Sub

    
    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.SelectedIndexChanged
        If rbActions.SelectedItem.Text = "Modify" Then

            txtBuilding.Visible = False
            ddlbdgname.Visible = True
            txtBuildingCode.Enabled = False
        ElseIf rbActions.SelectedItem.Text = "Add" Then

            txtBuilding.Visible = True
            ddlbdgname.Visible = False
            txtBuildingCode.Enabled = True
            Cleardata()
        End If
    End Sub
End Class
