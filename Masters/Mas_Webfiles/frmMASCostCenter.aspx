<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMASCostCenter.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASCostCenter" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Cost Center Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">
                        <asp:Label ID="lblHeader" runat="server" /></h3>
                </div>
                <div class="panel-content">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="CityPanel1" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val2" />
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ForeColor="Red" ValidationGroup="Val1" />
                                <asp:TextBox ID="txtCCode" runat="server" CssClass="form-control"
                                    TabIndex="8" ValidationGroup="Get" MaxLength="150" Visible="False"></asp:TextBox>
                                <asp:Button ID="btnGet" runat="server" CssClass="btn btn-primary custom-button-color" Text="Get" ValidationGroup="Get"
                                    TabIndex="9" Visible="False" />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <asp:Label ID="lblError" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Visible="False">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:HyperLink ID="hyp" runat="server" Text=" Click here to View the Template" NavigateUrl="~/Masters/Mas_Webfiles/CostCenter.xls"></asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">

                                            <label class="col-md-4 control-label">Upload Document (Only Excel)<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                                                ControlToValidate="fpBrowseDoc" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseDoc"
                                                ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                                                ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                                            </asp:RegularExpressionValidator>
                                            <div class="col-md-4">
                                                <div class="btn-default">
                                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                                    <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-3 col-xs-6">
                                                <div class="form-group">
                                                    <asp:Button ID="btnbrowse" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Upload" ValidationGroup="Val2" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="Radio-btn-s">
                                            <label class="btn">
                                                <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                                    ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                                                Add</label>
                                            <label class="btn">
                                                <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                                    ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                                                Modify
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div id="trCC" runat="server" class="row">
                                    <div class="col-md-3 col-sm-3 col-xs-6" >
                                        <div class="form-group">
                                            <asp:Label ID="lblProcess" runat="server" ForeColor ="Black" ><span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="ddlCostCenter"
                                                runat="server" Display="None" InitialValue="0" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlCostCenter" runat="server" CssClass="selectpicker" data-live-search="true"
                                                TabIndex="2" AutoPostBack="True">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-inline">
                                   
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblPrssNme" runat="server" ForeColor ="Black" ><span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtProjectName"
                                                runat="server" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtProjectName" MaxLength="150" runat="server" CssClass="form-control"
                                                    TabIndex="2"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Parent Entity<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvparent" runat="server" ControlToValidate="ddlParent"
                                                Display="None" InitialValue="--Select--" ValidationGroup="Val1" ErrorMessage="Please Select Parent Entity"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlParent" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"
                                                TabIndex="3">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <label>Child Entity<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvchild" runat="server" ControlToValidate="ddlChild"
                                                Display="None" InitialValue="--Select--" ValidationGroup="Val1" ErrorMessage="Please Select Child Entity"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlChild" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"
                                                TabIndex="3">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                     <div class="col-md-3 col-sm-6 col-xs-12" visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="lblPrssCde" runat="server" ForeColor ="Black" Visible="false" ><span style="color: red;">*</span></asp:Label>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtProjectCode"
                                                runat="server" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtProjectCode" MaxLength="15" runat="server" CssClass="form-control" TabIndex="1" Visible="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblVertical" runat="server" ForeColor ="Black" ><span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvVertical" runat="server" ControlToValidate="ddlVertical"
                                                Display="None" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlVertical" runat="server" CssClass="selectpicker" data-live-search="true"
                                                TabIndex="3">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Status<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlstatus"
                                                Display="None" ErrorMessage="Please Select Status" InitialValue="--Select--" ValidationGroup="Val1">
                                            </asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlstatus" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="9">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="0">Inactive</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Remarks<span style="color: red;">*</span></label>
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="maxLength"
                                                ControlToValidate="txtRemarks" Display="None" ErrorMessage="Remarks Shoud be less than 500 characters !"></asp:CustomValidator><asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                                    Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%"
                                                    MaxLength="500" TabIndex="10"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit"
                                                TabIndex="11" ValidationGroup="Val1"></asp:Button>
                                            <asp:Button ID="btnback" runat="server"
                                                CssClass="btn btn-primary custom-button-color" Text="Back" TabIndex="12" CausesValidation="False"></asp:Button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12">
                                        <div style="overflow: scroll">
                                            <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" EmptyDataText="No Cost Center Found."
                                                PageSize="20" AllowPaging="True" CssClass="table table-condensed table-bordered table-hover table-striped">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="CostCenter Code" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblCostCode"  Text='<% #Bind("COST_CENTER_CODE")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="CostCenter Name">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblProjName" Text='<% #Bind("COST_CENTER_NAME")%>'>  </asp:Label>
                                                            <%--<asp:Label runat="server" ID="lblProjCode" Text='<% #Bind("COST_CENTER_CODE")%>'></asp:Label>--%>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Vertical Name">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblVerName" Text='<% #Bind("VER_NAME")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Parent Entity Name">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblPeName" Text='<% #Bind("PE_NAME")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Child Entity Name">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblCheName" Text='<% #Bind("CHE_NAME")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <asp:LinkButton runat="server" ID="lnkStatus" Text='<% #Bind("status")%>' CommandArgument='<%#BIND("COST_STA_ID") %>'
                                                                OnClick="lnkStatus_Click" Visible="false" CausesValidation="False"></asp:LinkButton><asp:Label runat="server" ID="Label2" Text='<% #Bind("status")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>
        function ShowHelpWindow() {
            window.open('frmPrjHead.aspx', 'Window', 'toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=690,height=450');
            return false;
        }
    </script>
    <script type="text/javascript" defer>
        $("#btnSubmit").click(function () {
            $('#lblMsg').text("")
        });

        function refreshSelectpicker() {
            $("#<%=ddlVertical.ClientID%>").selectpicker();
            $("#<%=ddlParent.ClientID%>").selectpicker();
            $("#<%=ddlChild.ClientID%>").selectpicker();
            $("#<%=ddlCostCenter.ClientID%>").selectpicker();
            $("#<%=ddlstatus.ClientID%>").selectpicker();

        }
        refreshSelectpicker();


    </script>
</body>
</html>


