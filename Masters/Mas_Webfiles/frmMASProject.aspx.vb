Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Web.Services
Imports System.Collections.Generic

Partial Class Masters_Mas_Webfiles_frmMASProduct
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters
    Dim stQuery As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            lblError.Visible = True
            lblError.Text = ""
            If Not Page.IsPostBack Then
                obj.project_LoadGrid(gvItem)
                obj = New clsMasters()
               
                trProjCode.Visible = False
                rbActions.Items(0).Selected = True
                obj.BindVertical(ddlVertical)
                ' obj.binduser(ddlProjecthead)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "Load", exp)
        End Try
    End Sub

    Private Sub Cleardata()
        txtProjectCode.Text = String.Empty
        txtProjectName.Text = String.Empty
        txtRemarks.Text = String.Empty
        txtEmployee.Text = String.Empty
        txtProjC.Text = ""
        ddlVertical.SelectedIndex = 0
        ddlCostCenter.SelectedIndex = 0
    End Sub

    Private Sub Modifydata()
        obj.getname = txtProjectName.Text.Trim().Replace("'", "''")
        obj.getRemarks = txtRemarks.Text.Trim().Replace("'", "''")
        obj.getcode = txtProjectCode.Text.Trim().Replace("'", "''")
        Dim strEmp As String = txtEmployee.Text.Substring(0, txtEmployee.Text.IndexOf("/"))
        If (obj.ModifyProject(ddlVertical, strEmp, ddlCostCenter.SelectedValue.Trim(), Me) = 4) Then
            lblError.Text = "Record has been modified"
            Cleardata()
        Else
            lblError.Text = "Failed to modify"
        End If
        obj.project_LoadGrid(gvItem)

        'obj.binduser(ddlProjecthead)
        obj.BindVertical(ddlVertical)
    End Sub

    <WebMethod()> _
       <System.Web.Script.Services.ScriptMethod()> _
       Public Shared Function GetEmployeeList(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Try
            Dim cKey As String = ""
            If count = 0 Then
                count = 12
            End If
            Dim s As String = prefixText
            Dim dtEle As New DataTable()
            'strSQL = "select AUR_ID from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID like '" & prefixText & "%' order by AUR_ID"
            'dtEle = SqlHelper.ExecuteDatatable(CommandType.Text, strSQL)
            Dim sp1 As New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)
            sp1.Value = prefixText
            dtEle = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GETUSERS_NAME", sp1)
            Dim dtcount As Integer = CInt(dtEle.Rows.Count)
            'count = dtcount
            If dtcount <> 0 Then
                Dim items As New List(Of String)(count)
                Dim total As Integer = IIf((dtEle.Rows.Count < count), dtEle.Rows.Count, count)
                For i As Integer = 0 To total - 1
                    Dim dr As DataRow = dtEle.Rows(i)
                    items.Add(dr(0))
                Next
                Return items.ToArray()
            Else
                Return New String(-1) {}
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "GetEmployeeList", exp)
        End Try
    End Function

    <WebMethod()> _
       <System.Web.Script.Services.ScriptMethod()> _
       Public Shared Function GetProjects(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Try
            Dim cKey As String = ""
            If count = 0 Then
                count = 12
            End If
            Dim s As String = prefixText
            Dim dtEle As New DataTable()
            'strSQL = "select AUR_ID from  " & Session("TENANT") & "."  & "AMT_AMANTRAUSER_VW where AUR_ID like '" & prefixText & "%' order by AUR_ID"
            'dtEle = SqlHelper.ExecuteDatatable(CommandType.Text, strSQL)
            Dim sp1 As New SqlParameter("@VC_CC", SqlDbType.NVarChar, 50)
            sp1.Value = prefixText
            dtEle = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_PROJECTS_AUTO", sp1)
            Dim dtcount As Integer = CInt(dtEle.Rows.Count)
            'count = dtcount
            If dtcount <> 0 Then
                Dim items As New List(Of String)(count)
                Dim total As Integer = IIf((dtEle.Rows.Count < count), dtEle.Rows.Count, count)
                For i As Integer = 0 To total - 1
                    Dim dr As DataRow = dtEle.Rows(i)
                    items.Add(dr(0))
                Next
                Return items.ToArray()
            Else
                Return New String(-1) {}
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "GetEmployeeList", exp)
        End Try
    End Function

    Private Sub Insertdata()
        obj.getcode = txtProjectCode.Text
        obj.getname = txtProjectName.Text
        obj.getRemarks = txtRemarks.Text
        Dim strEmp As String = txtEmployee.Text.Substring(0, txtEmployee.Text.IndexOf("/"))
        Dim intStatus As Integer = obj.InsertProject(ddlVertical, strEmp, ddlCostCenter.SelectedValue.Trim(), Me)
        If intStatus = 1 Then
            lblError.Text = "The Project Code Already Exists, Please Modify Code."
        ElseIf intStatus = 2 Then
            lblError.Text = "The Project Name Already Exists, Please Modify Name."
        ElseIf intStatus = 3 Then
            lblError.Text = "Record has been inserted."
            Cleardata()
        End If
        obj.project_LoadGrid(gvItem)
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.SelectedIndexChanged
        Try
            Dim i As Integer
            For i = 0 To rbActions.Items.Count - 1
                If rbActions.Items(i).Selected = True Then
                    If rbActions.Items(i).Text = "Add" Then
                        trProjCode.Visible = False
                        txtProjectCode.ReadOnly = False
                        btnSubmit.Text = "Submit"
                        Cleardata()
                    ElseIf rbActions.Items(i).Text = "Modify" Then
                        obj = New clsMasters()
                        Dim dtProject As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_PROJECTS")
                        ddlProjectName.DataSource = dtProject
                        ddlProjectName.DataTextField = "PRJ_NAME"
                        ddlProjectName.DataValueField = "PRJ_CODE"
                        ddlProjectName.DataBind()
                        ddlProjectName.Items.Insert(0, "--Select--")
                        txtProjectCode.ReadOnly = True
                        trProjCode.Visible = True
                        btnSubmit.Text = "Modify"
                        obj.BindVertical(ddlVertical)
                        Cleardata()
                    End If
                End If
            Next
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try

            Dim sp1 As New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)
            If Not txtEmployee.Text.Contains("/") Then
                sp1.Value = txtEmployee.Text
            Else
                sp1.Value = txtEmployee.Text.Substring(0, txtEmployee.Text.IndexOf("/"))
            End If
            Dim dtEmp As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GETUSERS_NAME", sp1)
            If dtEmp.Rows.Count = 0 Then
                lblError.Text = "Please enter valid Project Head ID"
                Exit Sub
            Else
                txtEmployee.Text = dtEmp.Rows(0)(0).ToString()
            End If

            Dim sp As New SqlParameter("@VC_CC", SqlDbType.NVarChar, 50)
            If Not txtProjC.Text.Contains("/") Then
                sp.Value = txtProjC.Text
            Else
                sp.Value = ddlProjectName.SelectedItem.Value
                'txtProjC.Text.Substring(0, txtProjC.Text.IndexOf("/"))
            End If
            dtEmp = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_PROJECTS_AUTO", sp)
            If dtEmp.Rows.Count = 0 Then
                lblError.Text = "Please enter valid Project Code"
                Exit Sub
            Else
                txtProjC.Text = dtEmp.Rows(0)(0).ToString()
            End If
            If rbActions.SelectedItem.Text = "Add" Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                If txtProjectCode.Text = String.Empty Or txtProjectName.Text = String.Empty Or txtRemarks.Text = String.Empty Or ddlVertical.SelectedItem.Text = "--Select--" Then
                    lblError.Text = "Please Enter Mandatory fields"
                ElseIf txtRemarks.Text.Length > 500 Then
                    lblError.Text = "Please Enter remarks in less than or equal to 500 characters"
                Else
                    Insertdata()
                End If
            ElseIf rbActions.SelectedItem.Text = "Modify" Then
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If txtProjectCode.Text = String.Empty Or txtProjectName.Text = String.Empty Or txtRemarks.Text = String.Empty Or ddlVertical.SelectedItem.Text = "--Select--" Then
                    lblError.Text = "Please Enter Mandatory fields"
                ElseIf txtRemarks.Text.Length > 500 Then
                    lblError.Text = "Please Enter remarks in less than or equal to 500 characters"
                Else
                    Modifydata()
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmMASProject", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            'Commented By Venkat
            'Commented Date 02/01/09

            ' gvItem.Columns(2).Visible = True
            ' gvItem.Columns(3).Visible = True

            'Commented By Venkat
            'Commented Date 02/01/09
            obj.project_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    'Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
    '    Try
    '        If e.CommandName = "Status" Then
    '            Dim index As Integer = CType(e.CommandArgument, Integer)
    '            Dim iStatus As Integer = obj.project_Rowcommand(gvItem, index)
    '            obj.project_LoadGrid(gvItem)
    '            Cleardata()
    '        End If

    '        obj.BindProject(ddlProject)
    '        obj.BindVertical(ddlVertical)
    '        obj.binduser(ddlProjecthead)

    '        ddlProjecthead.SelectedIndex = 0
    '        ddlProject.SelectedIndex = 0
    '        ddlVertical.SelectedIndex = 0

    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try

    'End Sub

    Protected Sub lnkStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnk As LinkButton = CType(sender, LinkButton)
            Dim intStatus As Integer = lnk.CommandArgument

            If intStatus = 1 Then
                intStatus = 2
            Else
                intStatus = 1
            End If
            Try
                For i As Integer = 0 To gvItem.Rows.Count - 1
                    Dim lnkStatus As LinkButton = CType(gvItem.Rows(i).FindControl("lnkStatus"), LinkButton)
                    If lnk.ClientID = lnkStatus.ClientID Then
                        Dim lblProjCode As Label = CType(gvItem.Rows(i).FindControl("lblProjCode"), Label)
                        'strSQL = "update " & Session("TENANT") & "."  & "PROJECT set PRJ_STA_ID='" & intStatus & "' where PRJ_CODE='" & lblProjCode.Text.Trim() & "' "

                        Dim spStatus As New SqlParameter("@STAUTS", SqlDbType.Int)
                        Dim spPrjCode As New SqlParameter("@PRJ_CODE", SqlDbType.NVarChar, 50)
                        spStatus.Value = intStatus
                        spPrjCode.Value = lblProjCode.Text.Trim()
                        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_PROJECT_STATUS", spStatus, spPrjCode)
                        Exit For
                    End If
                Next
                lblError.Text = "Record has been modified"
            Catch ex As Exception
                lblError.Text = ex.Message
            End Try

            Cleardata()

            obj.BindVertical(ddlVertical)
            'obj.binduser(ddlProjecthead)
            obj.project_LoadGrid(gvItem)


            ddlVertical.SelectedIndex = 0
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASProject", "lnkStatus_Click", exp)
        End Try
    End Sub

    Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVertical.SelectedIndexChanged
        Dim spVerticalCode As New SqlParameter("@VERTICAL_CODE", SqlDbType.NVarChar, 200)
        spVerticalCode.Value = ddlVertical.SelectedValue
        Dim dtCC As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_COSTCENTER_PROJECT", spVerticalCode)
        ddlCostCenter.DataSource = dtCC
        ddlCostCenter.DataTextField = "COST_CENTER_NAME"
        ddlCostCenter.DataValueField = "COST_CENTER_CODE"
        ddlCostCenter.DataBind()
        ddlCostCenter.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub btnGet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGet.Click
        Try
            Dim strCC As String = String.Empty
            If txtProjC.Text.Contains("/") Then
                strCC = txtProjC.Text.Substring(0, txtProjC.Text.IndexOf("/"))
            Else
                strCC = txtProjC.Text
            End If
            obj.BindVertical(ddlVertical)
            ' obj.project_SelectedIndex_Changed(strCC, ddlVertical, txtEmployee, ddlCostCenter, ddlProjHead)
            If obj.getcode = "" Then
                lblError.Text = "Please enter valid Project Code/Name"
                Cleardata()
                Exit Sub
            End If
            txtProjectCode.Text = obj.getcode
            txtProjectName.Text = obj.getname
            txtRemarks.Text = obj.getRemarks

        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "ddlProject_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub ddlProjectName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProjectName.SelectedIndexChanged
        Try
            If ddlProjectName.SelectedIndex > 0 Then
                Dim strCC As String = String.Empty
                txtProjC.Text = ddlProjectName.SelectedItem.Value
                'If txtProjC.Text.Contains("/") Then
                '    strCC = txtProjC.Text.Substring(0, txtProjC.Text.IndexOf("/"))
                'Else
                '    strCC = txtProjC.Text
                'End If
                strCC = txtProjC.Text
                obj.BindVertical(ddlVertical)
                obj.project_SelectedIndex_Changed(strCC, ddlVertical, txtEmployee, ddlCostCenter, txtEmployee)
                If obj.getcode = "" Then
                    lblError.Text = "Please enter valid Project Code/Name"
                    Cleardata()
                    Exit Sub
                End If
                txtProjectCode.Text = obj.getcode
                txtProjectName.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "ddlProject_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub lnkHelp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkHelp.Click
        Response.Write("<script language=javascript>javascript:window.open('frmPrjHead.aspx','Window','toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=690,height=450')</script>")

        'Response.Write("<script language=javascript>javascript:window.open('frmPrjHead.aspx','Window','toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=690,height=450')</script>")
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub
End Class
