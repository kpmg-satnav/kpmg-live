Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic

Partial Class Masters_Mas_Webfiles_frmMASDesignition
    Inherits System.Web.UI.Page
    Dim obj As New clsMasters


    Private Sub Cleardata()
        txtDesigCode.Text = String.Empty
        txtDesigName.Text = String.Empty
        txtRemarks.Text = String.Empty
    End Sub
    Dim intStatus As Integer
    Dim intInsertModifyStatus As Integer

    Private Sub insertdata()
        lblMsg.Visible = True
        obj.getcode = txtDesigCode.Text
        obj.getname = txtDesigName.Text
        obj.getRemarks = txtRemarks.Text
        intInsertModifyStatus = 1
        intStatus = obj.InsertDesig(Me, intInsertModifyStatus)
        If intStatus = 1 Then
            lblMsg.Text = "The Designation Code Already Exists, Please Modify Code."
        ElseIf intStatus = 2 Then
            lblMsg.Text = "Designation Inserted Successfully"
        ElseIf intStatus = 3 Then
            lblMsg.Text = "Designation Inserted Successfully"
            Cleardata()
        End If
        obj.Desig_LoadGrid(gvItem)
    End Sub

    Private Sub Modifydata()
        obj.getname = txtDesigName.Text
        obj.getRemarks = txtRemarks.Text
        intInsertModifyStatus = 2
        obj.getcode = txtDesigCode.Text
        intStatus = obj.InsertDesig(Me, intInsertModifyStatus)
        If intStatus = 4 Then
            lblMsg.Visible = True
            lblMsg.Text = "Designation Updated Successfully."
            Cleardata()
        Else
            lblMsg.Text = "Failed to modify."
        End If
        obj.Desig_LoadGrid(gvItem)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/Masters/MAS_Webfiles/frmMASMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If Session("UID") = "" Then
                    Response.Redirect(Application("FMGLogout"))
                Else
                    If sdr.HasRows Then
                    Else
                        Response.Redirect(Application("FMGLogout"))
                    End If
                End If
            End Using
            RegExpCode.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
            RegExpName.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
            'RegularExpressionValidator2.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
            'RegExpName.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
            'RegExpNumber.ValidationExpression = User_Validation.GetValidationExpressionForPhone.VAL_EXPR()
            'If Session("uid") = "" Then
            '    Response.Redirect(Application("FMGLogout"))
            'End If
            lblMsg.Visible = False

            If Not Page.IsPostBack Then
                obj.Desig_LoadGrid(gvItem)
                trCName.Visible = False
                rbActions.Checked = True
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASDesignition", "Load", exp)
        End Try
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        Try
            If rbActions.Checked = True Then
                trCName.Visible = False
                txtDesigCode.ReadOnly = False
                btnSubmit.Text = "Submit"
                trCName.Visible = False
                Cleardata()
            Else
                Cleardata()
                trCName.Visible = True
                txtDesigCode.ReadOnly = True
                btnSubmit.Text = "Modify"
                obj.BindDesig1(ddlDesignition)
            End If

        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASDesignition", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                insertdata()
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                Modifydata()
                obj.BindDesig1(ddlDesignition)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmMASDesignition", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlDesignition_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDesignition.SelectedIndexChanged
        Try
            If ddlDesignition.SelectedValue <> "--Select--" Then
                obj.Desig_SelectedIndex_Changed(ddlDesignition)
                txtDesigCode.Text = obj.getcode
                txtDesigName.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
            Else
                Cleardata()
                obj.BindDesig1(ddlDesignition)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASDesignition", "ddlDesignition_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            obj.Desig_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASDesignition", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                obj.Desig_Rowcommand("fdg", index)
            End If
            obj.BindDesig1(ddlDesignition)
            obj.Desig_LoadGrid(gvItem)
            Cleardata()
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASDesignition", "gvItem_RowCommand", exp)
        End Try
    End Sub

    Protected Sub lnkStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnk As LinkButton = CType(sender, LinkButton)
            Dim intSt As Integer = lnk.AccessKey
            Dim strDesgCode As String = lnk.CommandArgument
            obj.Desig_Rowcommand(intSt, strDesgCode)
            obj.BindDesig1(ddlDesignition)
            obj.Desig_LoadGrid(gvItem)
            Cleardata()
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASDesignition", "lnkStatus_Click", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub
End Class
