<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmNewUser.aspx.vb" Inherits="Masters_Mas_Webfiles_frmNewUser" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <%--<script type="text/javascript" src="../../Scripts/Cal.js">--%>
    <script type="text/javascript" defer>
        function setup(id) {
            $('.date').datepicker({
                //$('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                orientation: 'bottom'
            });
        };
    </script>
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Add New User" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">
                        <asp:Label ID="lblHead" runat="server"></asp:Label></h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server" autocomplete="off">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <%--<div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 17px">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="Label1" ForeColor="Red" CssClass="col-md-12 control-label" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>                                
                            </div>--%>

                        <div id="PNLCONTAINER" runat="server">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                ValidationGroup="Val1" ForeColor="Red" />
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 17px">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lbl1" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Visible="False">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Title<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cvtitle" runat="server" ValueToCompare="0" Operator="NotEqual"
                                            ControlToValidate="ddltitle" Display="None" ErrorMessage="Please Select Title"
                                            ValidationGroup="Val1"></asp:CompareValidator>
                                        <asp:DropDownList ID="ddltitle" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="1">
                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                            <asp:ListItem Value="1">Mr.</asp:ListItem>
                                            <asp:ListItem Value="2">Mrs.</asp:ListItem>
                                            <asp:ListItem Value="3">Ms.</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">First Name<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="Comptxtfname" runat="server" Operator="NotEqual" ControlToValidate="txtfname"
                                            Display="None" ErrorMessage="Please Enter only characters" ValidationGroup="Val1"></asp:CompareValidator>
                                        <asp:RegularExpressionValidator ID="RegExptxtfname" runat="server" ControlToValidate="txtfname"
                                            Display="None" ErrorMessage="Please Enter First Name only in Characters" ValidationExpression="^[a-zA-Z ]*"
                                            ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvfname" runat="server" Width="48px" ControlToValidate="txtfname"
                                            Display="None" ErrorMessage="Please Enter First Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div onmouseover="Tip('Enter first name in alphabets,space and 50 characters allowed')"
                                            onmouseout="UnTip()">
                                            <asp:TextBox ID="txtfname" runat="server" CssClass="form-control" MaxLength="50"
                                                TabIndex="1"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Middle Name</label>
                                        <asp:RegularExpressionValidator ID="Regexptxtmname" runat="server" ControlToValidate="txtmname"
                                            Display="None" ErrorMessage="Please Enter Middle Name only in Characters"
                                            ValidationExpression="^[a-zA-Z ]*" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <div onmouseover="Tip('Enter middle name in alphabets,space and 50 characters allowed')"
                                            onmouseout="UnTip()">
                                            <asp:TextBox ID="txtmname" runat="server" CssClass="form-control" MaxLength="50"
                                                TabIndex="2"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Last Name<span style="color: red;">*</span></label>
                                        <asp:RegularExpressionValidator ID="RegexpLname" runat="server" ControlToValidate="txtlname"
                                            Display="None" ErrorMessage="Please Enter Last Name only in Characters" ValidationExpression="^[a-zA-Z ]*"
                                            ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <asp:CompareValidator ID="Comptxtlname" runat="server" Operator="NotEqual" ControlToValidate="txtlname"
                                            Display="None" ErrorMessage="Please Enter lastname only characters" ValidationGroup="Val1"></asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="rfvLname" runat="server" Width="48px" ControlToValidate="txtlname"
                                            Display="None" ErrorMessage="Please Enter Last Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div onmouseover="Tip('Enter last name in alphabets and 50 characters allowed')"
                                            onmouseout="UnTip()">
                                            <asp:TextBox ID="txtlname" runat="server" CssClass="form-control" MaxLength="50"
                                                TabIndex="3"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Email Id<span style="color: red;"></span></label>
                                        <asp:RegularExpressionValidator ID="revemail" runat="server" ControlToValidate="txtmailid"
                                            Display="None" ErrorMessage="Please Enter Valid Email Id" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <%--<asp:RequiredFieldValidator ID="rfvmail" runat="server" Width="48px" ControlToValidate="txtmailid"
                                                    Display="None" ErrorMessage="Please Enter Email ID" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                        <div onmouseover="Tip('Enter email id')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtmailid" runat="server" CssClass="form-control" TabIndex="4"
                                                MaxLength="250"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">User/Employee ID<span style="color: red;">*</span></label>
                                        <asp:RegularExpressionValidator ID="revaurid" runat="server" ControlToValidate="txtaurid"
                                            Display="None" ErrorMessage="Please Enter User Id only in Alphanumerics"
                                            ValidationExpression="^[a-zA-Z0-9 -_]*" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvaurid" runat="server" Width="48px" ControlToValidate="txtaurid"
                                            Display="None" ErrorMessage="Please Enter User Id" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div onmouseover="Tip('Enter UserID in alphabets,numbers and _,-')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtaurid" runat="server" CssClass="form-control" MaxLength="20"
                                                TabIndex="5"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Country<span style="color: red;">*</span></label>

                                        <asp:CompareValidator ID="CompareValidator3" runat="server" ValueToCompare="--Select--"
                                            Operator="NotEqual" ControlToValidate="ddlcountry" Display="None" ErrorMessage="Please Select Country"
                                            ValidationGroup="Val1"></asp:CompareValidator>
                                        <asp:DropDownList ID="ddlcountry" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                            TabIndex="6" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Time Zone<span style="color: red;">*</span></label>

                                        <asp:RequiredFieldValidator ID="RFVtimezone" runat="server" ControlToValidate="ddlTimeZone"
                                            Display="None" ErrorMessage="Please Select Time Zone" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlTimeZone" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                            TabIndex="7">
                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">City<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="Reqtxtcity" runat="server" ControlToValidate="ddlcity"
                                            Display="None" ErrorMessage="Please Select City" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlcity" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="8" AutoPostBack="True" OnSelectedIndexChanged="ddlcity_SelectedIndexChanged">
                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Location<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="reqLocation" runat="server" ControlToValidate="ddldg"
                                            Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                        <%-- <asp:CompareValidator ID="cvbdg" runat="server" ValueToCompare="--Select--" Operator="NotEqual"
                                                    ControlToValidate="ddldg" Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1"></asp:CompareValidator>--%>
                                        <asp:DropDownList ID="ddldg" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="9">
                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Address</label>

                                        <div onmouseover="Tip('Enter Address in 200 characters allowed')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="200"
                                                TabIndex="10"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Mobile No  </label>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtmob"
                                            Display="None" ErrorMessage="Please Enter Mobile Number in Digits" ValidationExpression="^[0-9-]*"
                                            ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <asp:RegularExpressionValidator ID="revmobile" runat="server"
                                            ControlToValidate="txtmob" ErrorMessage="Please Enter Valid Mobile No." Display="None" ValidationGroup="Val1"
                                            ValidationExpression="^[0-9]{10}$"></asp:RegularExpressionValidator>

                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtmob"
                                                    Display="None" ErrorMessage="Please Enter Mobile Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                        <div onmouseover="Tip('Enter mobile number in digits')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtmob" runat="server" CssClass="form-control" MaxLength="15" AutoCompleteType="None"
                                                TabIndex="11"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Grade     </label>
                                        <asp:DropDownList ID="ddlGrade" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                            TabIndex="12">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Company   </label>
                                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                            TabIndex="13">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Department</label>

                                        <asp:DropDownList ID="ddldept" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                            TabIndex="14">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Designation   </label>
                                        <asp:DropDownList ID="ddldesn" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                            TabIndex="15">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <%--<div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Reporting Manager  </label>      
                                                <asp:DropDownList ID="ddlrepmgr" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                    TabIndex="16">
                                                </asp:DropDownList>
                                            </div>
                                        </div>--%>

                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Reporting Manager  </label>
                                        <%-- <asp:DropDownList ID="ddlrepmgr" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                    TabIndex="16">
                                                </asp:DropDownList>--%>
                                        <asp:TextBox ID="ddlrepmgr" runat="server" OnTextChanged="ddlrepmgr_TextChanged" CssClass="form-control" Width="200px" AutoPostBack="true"></asp:TextBox>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="ddlrepmgr" MinimumPrefixLength="2" EnableCaching="false"
                                            CompletionSetCount="10" CompletionInterval="10" ServiceMethod="GetDetails" ServicePath="~/Autocompletetype.asmx">
                                        </asp:AutoCompleteExtender>
                                    </div>
                                </div>


                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label ID="lblVertical" runat="server" CssClass="control-label"></asp:Label>
                                        <asp:DropDownList ID="ddlVertical" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                            TabIndex="17" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label ID="lblProcess" runat="server" CssClass="control-label"></asp:Label>
                                        <asp:DropDownList ID="ddlCostcenter" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                            TabIndex="18">
                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Enter Password<span style="color: red;">*</span></label>

                                        <asp:RequiredFieldValidator ID="refpwd" runat="server" Width="48px" ControlToValidate="txtpwd"
                                            Display="None" ErrorMessage="Please Enter Password" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div onmouseout="UnTip()">
                                            <asp:TextBox ID="txtpwd" runat="server" CssClass="form-control" MaxLength="20"
                                                TextMode="Password" TabIndex="19" autocomplete="off"></asp:TextBox>
                                            <input type="checkbox" onchange="document.getElementById('txtpwd').type = this.checked ? 'text' : 'password'">
                                            Show password
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Remarks<span style="color: red;"></span></label>
                                    <%-- <asp:RequiredFieldValidator ID="refdesc" runat="server" ErrorMessage="Please Enter the Remarks"
                                                        ControlToValidate="RemarksDesc" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    <asp:TextBox ID="RemarksDesc" Rows="4" runat="server" CssClass="form-control" Height="10%" TextMode="MultiLine">                               
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <%-- <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Confirm Password<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvcpwd" runat="server" Width="48px" ControlToValidate="txtcpwd"  Display="None" ErrorMessage="Please Enter Confirm Password" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtp" runat="server" Visible="False" Width="0px"></asp:TextBox>
                                                <asp:TextBox ID="txtcpwd" runat="server" CssClass="form-control" MaxLength="20"
                                                    TextMode="Password" TabIndex="11" autocomplete="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>--%>

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 text-right" style="padding-top: 17px">
                                <div class="form-group">
                                    <asp:TextBox ID="txttoday" runat="server" Width="0px" Visible="False"></asp:TextBox>
                                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" TabIndex="20" ValidationGroup="Val1"></asp:Button>
                                    <asp:Button ID="btnclear" CssClass="btn btn-primary custom-button-color" runat="server" Text="Clear" CausesValidation="false" />
                                    <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" TabIndex="19"></asp:Button>
                                </div>
                            </div>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

