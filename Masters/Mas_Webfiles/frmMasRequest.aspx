<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmMasRequest.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMasRequest"
    Title="Request Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tbody>
                        <tr>
                            <td align="center" width="100%">
                                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" ForeColor="Black" Font-Underline="False" Width="86%">SLA Master <hr align="center" width="60%" /></asp:Label>
                                &nbsp;
                                <br />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                    <tbody>
                        <tr>
                            <td>
                                <img height="27" alt="" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                            <td class="tableHEADER" align="left" width="100%">&nbsp;<strong>Request</strong></td>
                            <td style="WIDTH: 17px">
                                <img height="27" alt="" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                        </tr>
                        <tr>
                            <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                            <td align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage" ForeColor="" ValidationGroup="Val1"></asp:ValidationSummary>
                                <br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                                <asp:TextBox ID="txtstore" runat="server" Visible="false"></asp:TextBox>
                                <asp:TextBox ID="txtcode" runat="server" Visible="false"></asp:TextBox>
                                <table id="table4" cellspacing="0" cellpadding="5" width="100%" border="1">
                                    <%--
                                <tr>
                        <td align="left" width="50%">
                           Select City
                
                          <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="reg1" runat="server" ControlToValidate="ddlCty"
                                Display="None" ErrorMessage="Please Select City" ValidationGroup="Val1"
                                InitialValue="--Select--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlCty" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True" TabIndex="2">
                          
                            </asp:DropDownList>
                        </td>
                    </tr>
            
                    <tr>
                        <td align="left" width="50%">
                         Select Location
                
                          <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvBDG" runat="server" ControlToValidate="ddlBDG"
                                Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1"
                                InitialValue="--Select--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:DropDownList ID="ddlBDG" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True" TabIndex="3">
                               
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            Select Tower
                
                           <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvtwr" runat="server" ControlToValidate="ddlTWR"
                                Display="None" ErrorMessage="Please Select Tower" ValidationGroup="Val1" InitialValue="--Select--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:DropDownList ID="ddlTWR" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True" TabIndex="4">
                              
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%">
                            Select Floor
                
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvFLR" runat="server" ControlToValidate="ddlFLR"
                                Display="None" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="--Select--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="width: 50%">
                            <asp:DropDownList ID="ddlFLR" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True" TabIndex="5">
                              
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" width="50%" style="height: 26px">
                           Select Wing
                
                          <font class="clsNote">*</font>
                           
                        </td>
                        <td align="left" style="width: 50%; height: 26px;">
                        <asp:ListBox id="lstwing"  runat="Server" tabindex="6" SelectionMode="Multiple" width="50%"></asp:ListBox>
                        
                        
                        </td>
                    </tr>--%><tbody><tr>
                        <td>Service Raised For<font class="clsNote">*</font>
                            
                            <asp:RequiredFieldValidator ID="rfvlstatus" runat="server" ValidationGroup="Val1" InitialValue="--Select--" ControlToValidate="ddlType" Display="None" ErrorMessage="Select Service"></asp:RequiredFieldValidator>
                        </td>
                        <td align="left" width="50%">
                            <asp:DropDownList ID="ddlType" runat="server" CssClass="clsComboBox" Width="97%" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                        <tr>
                            <td>Select City<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvcity" runat="server" ValidationGroup="Val1" InitialValue="--Select--" ControlToValidate="ddlcity" Display="None" ErrorMessage="Select City"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlcity" runat="server" AutoPostBack="True" Width="97%" CssClass="clsComboBox"></asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" width="50%">Select Building
                
                          <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvBDG" runat="server" ControlToValidate="liitems"
                                    Display="None" ErrorMessage="Please Select Building" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%">
                                <asp:ListBox ID="liitems" runat="server" SelectionMode="Multiple" Width="100%"></asp:ListBox>
                            </td>
                        </tr>



                        <tr>
                            <td>Select Service Category <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvreqtype" runat="server" ValidationGroup="Val1" InitialValue="--Select--" ControlToValidate="ddlreqtype" Display="None" ErrorMessage="Select Request Type"> </asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlreqtype" TabIndex="7" runat="server" CssClass="clsComboBox" Width="97%" AutoPostBack="True"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>Select Service Type <font class="clsNote">*</font> </td>
                            <td>
                                <asp:DropDownList ID="ddlRequests" TabIndex="7" runat="server" CssClass="clsComboBox" Width="97%" __designer:wfdid="w1" AutoPostBack="True"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>L1 <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvl1" runat="server" ValidationGroup="Val1" ControlToValidate="txtL1" Display="none" ErrorMessage="Please enter L1"></asp:RequiredFieldValidator>&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
                                <div>
                                    <asp:RegularExpressionValidator ID="revl1" runat="server" ValidationGroup="Val1" ControlToValidate="txtL1" Display="none" ErrorMessage="Please enter L1 in Numerics" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                    <asp:TextBox ID="txtL1" runat="server" CssClass="clsTextField" Width="25%" MaxLength="5"> </asp:TextBox>
                                    <asp:Label ID="lbll1ds" runat="server" CssClass="bodytext" Text="(in minutes)"></asp:Label>
                                </div>
                            </td>
                            <td>
                                User 1 <font class="clsNote">*</font> &nbsp;
                                <asp:RequiredFieldValidator ID="rfvusr1" runat="server" ValidationGroup="Val1" ControlToValidate="txtUser1Contact" Display="None" ErrorMessage="Select User 1" ValidationExpression="^[0-9, ]*$"> </asp:RequiredFieldValidator>
                                <div>
                                    <%--<asp:TextBox ID="txtUser1Contact" runat="server" CssClass="clsTextField" Width="40%" AutoPostBack="True" Rows="3" Visible="True" MaxLength="50" TextMode="Multiline" AutoCompleteType="Disabled" AutoComplete="off"></asp:TextBox>--%>
                                    <asp:TextBox ID="txtUser1Contact" runat="server" CssClass="clsTextField" Width="40%" AutoPostBack="True" Visible="True" MaxLength="50" AutoCompleteType="Disabled" AutoComplete="off" Enabled="False"></asp:TextBox>
                                </div>                                
                                <%-- <cc1:AutoCompleteExtender id="autoCompleteEx1" runat="server" BehaviorID="AutoCompleteEx1" CompletionSetCount="5" MinimumPrefixLength="1" TargetControlID="txtUser1Contact" ServicePath="~/SearchUsers.asmx" ServiceMethod="SearchUser" ContextKey="0" CompletionInterval="1000" DelimiterCharacters=";,">
                    </cc1:AutoCompleteExtender> --%> 
                                <div style="clear: both;">
                                    Mobile1 Number <font class="clsNote">*</font> &nbsp;
                                    <asp:RequiredFieldValidator ID="rfvmob1" runat="server" ValidationGroup="Val1" ControlToValidate="txtmob1" Display="none" ErrorMessage="Please enter Mobile1 Number"> </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revmob1" runat="server" ValidationGroup="Val1" ControlToValidate="txtmob1" Display="none" ErrorMessage="Please enter Mobile Number in Numerics Separated by commas" ValidationExpression="^[0-9, ]*$"></asp:RegularExpressionValidator>                                    
                                </div>
                                <div>
                                    <%--<asp:TextBox ID="txtmob1" runat="server" CssClass="clsTextField" Width="50%" Rows="3" MaxLength="50" TextMode="Multiline">
                                    </asp:TextBox>--%>
                                    <asp:TextBox ID="txtmob1" runat="server" CssClass="clsTextField" Width="50%" MaxLength="50" Enabled="False"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>L2 <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvL2" runat="server" ValidationGroup="Val1" ControlToValidate="txtL2" Display="none" ErrorMessage="Please enter L2"> </asp:RequiredFieldValidator>
                                <div>
                                    <asp:RegularExpressionValidator ID="revl2" runat="server" ValidationGroup="Val1" ControlToValidate="txtL2" Display="none" ErrorMessage="Please enter L2 in Numerics" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                    <asp:TextBox ID="txtL2" runat="server" CssClass="clsTextField" Width="25%" MaxLength="5"> </asp:TextBox>
                                    <asp:Label ID="lblL1D" runat="server" CssClass="bodytext" Text="(in minutes)"></asp:Label>
                                </div>                                
                            </td>
                            <td>
                                User 2 <font class="clsNote">*</font> &nbsp;
                                <asp:RequiredFieldValidator ID="rfvU1" runat="server" ValidationGroup="Val1" ControlToValidate="txtuser2" Display="none" ErrorMessage="Select User 2" ValidationExpression="^[0-9, ]*$"> </asp:RequiredFieldValidator>
                                <div>
                                    <%--<asp:TextBox ID="txtuser2" runat="server" CssClass="clsTextField" Width="40%" AutoPostBack="True" Rows="3" MaxLength="50" TextMode="Multiline" AutoCompleteType="Disabled" AutoComplete="off"></asp:TextBox>--%>
                                    <asp:TextBox ID="txtuser2" runat="server" CssClass="clsTextField" Width="40%" AutoPostBack="True" MaxLength="50" AutoCompleteType="Disabled" AutoComplete="off" Enabled="False"></asp:TextBox>
                                </div>                                
                                <%--<cc1:AutoCompleteExtender id="AutoCompleteExtender2" runat="server" BehaviorID="AutoCompleteEx2" CompletionSetCount="5" MinimumPrefixLength="1" TargetControlID="txtuser2" ServicePath="~/SearchUsers.asmx" ServiceMethod="SearchUser" ContextKey="0" CompletionInterval="1000" DelimiterCharacters=";,">
                    </cc1:AutoCompleteExtender> --%>
                                <div style="clear: both;">
                                    Mobile2 Number <font class="clsNote">*</font> &nbsp;
                                    <asp:RequiredFieldValidator ID="rfvmob2" runat="server" ValidationGroup="Val1" ControlToValidate="txtmob2" Display="none" ErrorMessage="Please enter Mobile2 Number"> </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revmob2" runat="server" ValidationGroup="Val1" ControlToValidate="txtmob2" Display="none" ErrorMessage="Please enter Mobile2 Number in Numerics Separated by commas" ValidationExpression="^[0-9, ]*$"></asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <%--<asp:TextBox ID="txtmob2" runat="server" CssClass="clsTextField" Width="50%" Rows="3" MaxLength="50" TextMode="Multiline">
                                    </asp:TextBox>--%>
                                    <asp:TextBox ID="txtmob2" runat="server" CssClass="clsTextField" Width="50%" MaxLength="50" Enabled="False"></asp:TextBox>
                                </div>                                
                            </td>
                        </tr>
                        <tr>
                            <td>L3 <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvL3" runat="server" ValidationGroup="Val1" ControlToValidate="txtL3" Display="none" ErrorMessage="Please enter L3"> </asp:RequiredFieldValidator>
                                <div>
                                    <asp:RegularExpressionValidator ID="revl3" runat="server" ValidationGroup="Val1" ControlToValidate="txtL3" Display="none" ErrorMessage="Please enter L2 in Numerics" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                    <asp:TextBox ID="txtL3" runat="server" CssClass="clsTextField" Width="25%" MaxLength="5"> </asp:TextBox>
                                    <asp:Label ID="lblLD3" runat="server" CssClass="bodytext" Text="(in minutes)"></asp:Label>
                                </div>                                
                            </td>
                            <td>
                                User 3 <font class="clsNote">*</font> &nbsp;
                                <asp:RequiredFieldValidator ID="rfvUsr3" runat="server" ValidationGroup="Val1" ControlToValidate="txtuser3" Display="none" ErrorMessage="Select User 3" ValidationExpression="^[0-9, ]*$"> </asp:RequiredFieldValidator>
                                <div>
                                    <%--<asp:TextBox ID="txtuser3" runat="server" CssClass="clsTextField" Width="40%" AutoPostBack="True" Rows="3" MaxLength="50" TextMode="Multiline" AutoCompleteType="Disabled" AutoComplete="off"></asp:TextBox>--%>
                                    <asp:TextBox ID="txtuser3" runat="server" CssClass="clsTextField" Width="40%" AutoPostBack="True" MaxLength="50" AutoCompleteType="Disabled" AutoComplete="off" Enabled="False"></asp:TextBox>
                                </div>                                
                                <%--<cc1:AutoCompleteExtender id="AutoCompleteExtender3" runat="server" BehaviorID="AutoCompleteEx3" CompletionSetCount="5" MinimumPrefixLength="1" TargetControlID="txtuser3" ServicePath="~/SearchUsers.asmx" ServiceMethod="SearchUser" ContextKey="0" CompletionInterval="1000" DelimiterCharacters=";,">
                    </cc1:AutoCompleteExtender>--%>
                                <div style="clear: both;">
                                    Mobile3 Number <font class="clsNote">*</font> &nbsp;
                                    <asp:RequiredFieldValidator ID="rfvmob3" runat="server" ValidationGroup="Val1" ControlToValidate="txtmob3" Display="none" ErrorMessage="Please enter Mobile3 Number"> </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revmob3" runat="server" ValidationGroup="Val1" ControlToValidate="txtmob3" Display="none" ErrorMessage="Please enter Mobile3 Number in Numerics Separated by commas" ValidationExpression="^[0-9, ]*$"></asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <%--<asp:TextBox ID="txtmob3" runat="server" CssClass="clsTextField" Width="50%" Rows="3" MaxLength="50" TextMode="Multiline">
                                    </asp:TextBox>--%>
                                    <asp:TextBox ID="txtmob3" runat="server" CssClass="clsTextField" Width="50%" MaxLength="50" Enabled="False"></asp:TextBox>
                                </div>                                
                            </td>
                        </tr>
                        <tr>
                            <td>Remarks <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvrem" runat="server" ValidationGroup="Val1" ControlToValidate="txtRem" Display="none" ErrorMessage="Please Enter Remarks!"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtRem" runat="server" CssClass="clsTextField" Width="97%" Rows="3" Height="70px" TextMode="MultiLine" MaxLength="1000"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Status <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ValidationGroup="Val1" InitialValue="--Select Status--" ControlToValidate="ddlStatus" Display="none" ErrorMessage="Please Select Status"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="clsComboBox" Width="50%">
                                    <asp:ListItem>--Select Status--</asp:ListItem>
                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                    <asp:ListItem Value="0">InActive</asp:ListItem>

                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">&nbsp;<asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                <asp:Button ID="btnModify" runat="Server" CssClass="button" Text="Modify" ValidationGroup="Val1"></asp:Button>
                                <asp:Button ID="btnBack" runat="server" CssClass="button" Text="Back" Width="76px" PostBackUrl="~/Workspace/Sms_Webfiles/helpmasters.aspx"></asp:Button>
                            </td>
                        </tr>
                    </tbody>
                                </table>
                                <asp:GridView ID="gvitems" TabIndex="9" runat="server" Width="100%" EmptyDataText="No Records Found" AllowPaging="True" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblreq" runat="Server" Text='<%#Eval("REQ_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Request Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSerType" runat="Server" Text='<%#Eval("REQUEST_TYPE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Request Desc">
                                            <ItemTemplate>
                                                <asp:Label ID="lbldesc" runat="server" Text='<%#Eval("REQ_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblloc" runat="server" Text='<%#Eval("LOCATION") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:ButtonField Text="EDIT" CommandName="EDIT" />
                                        <asp:ButtonField Text="Delete" CommandName="DELETE" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td background="../../Images/table_right_mid_bg.gif">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                            <td background="../../images/table_bot_mid_bg.gif">
                                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                            <td>
                                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
