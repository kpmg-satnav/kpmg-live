<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmMASMasters.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASMasters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-5b02d1ea3b.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Primary Masters" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Primary Masters</h3>
                </div>
                <div class="panel-content">
                    <form id="form1" runat="server">
                        <div class="box-body">

                            <br />
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12" id="CountryHLdiv" runat="server">
                                    <asp:HyperLink ID="CountryHL" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASCountry.aspx"> Country </asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="CityHLdiv" runat="server">
                                    <asp:HyperLink ID="CityHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASCity.aspx"> City</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="LocationHLdiv" runat="server">
                                    <asp:HyperLink ID="LocationHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASLocation.aspx"> Location Master</asp:HyperLink>
                                </div>

                            </div>
                            <br />
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12" id="TowerHLdiv" runat="server">
                                    <asp:HyperLink ID="TowerHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASTower.aspx"> Tower Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="FloorHLdiv" runat="server">
                                    <asp:HyperLink ID="FloorHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASFloor.aspx">Floor Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="DesignatioHLdiv" runat="server">
                                    <asp:HyperLink ID="DesignatioHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASDesignition.aspx">Designation Master</asp:HyperLink>
                                </div>
                            </div>
                            <br />
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12" id="DepartmentHLdiv" runat="server">
                                    <asp:HyperLink ID="DepartmentHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASDepartment.aspx">Department Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="UplaodMasterDataHLdiv" runat="server">
                                    <asp:HyperLink ID="UplaodMasterDataHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/uploadmasterdata.aspx">Upload Master Data</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="UploadHRMSDataHLdiv" runat="server">
                                    <asp:HyperLink ID="UploadHRMSDataHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMasUploadHRMSData.aspx">Upload HRMS Data</asp:HyperLink>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12" id="GeographHLdiv" runat="server">
                                    <asp:HyperLink ID="GeographyHL" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASGeography.aspx"> Geography </asp:HyperLink>
                                </div>
                                  <div class="col-md-4 col-sm-12 col-xs-12" id="FunctionHLDiv" runat="server">
                                    <asp:HyperLink ID="FunctionHL" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASFunction.aspx"> Function </asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink3" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Workspace/sms_Webfiles/frmEntityAdmin.aspx">Sub Function</asp:HyperLink>
                                </div>
                                </div>
                            <br />
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink4" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Workspace/sms_Webfiles/frmEntity.aspx">Sub Function1</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink11" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASVertical.aspx">
                                        <%-- Vertical Master<asp:Label ID="lblSelVertical" runat="server" Text=""></asp:Label>--%>
                                        <asp:Label ID="lblSelVertical" runat="server" Text=""></asp:Label>
                                    </asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink5" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASCostCenter.aspx">
                                        <%--  CostCenter Master<asp:Label ID="Label1" runat="server" Text=""></asp:Label>--%>
                                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                    </asp:HyperLink>
                                </div>  
                                </div> 
                            <br />
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="hprBSM" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/MAS_Webfiles/frmMasBusinessSpecificMaster.aspx">Business Specific Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink2" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/FAM/FAM_Webfiles/frmAMGVendorNewRecord1.aspx">Add Vendor</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="FloorSectionHLdiv" runat="server">
                                    <asp:HyperLink ID="FloorSectionHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASFloorSection.aspx">Floor Section Master</asp:HyperLink>
                                </div>
                               <%--  <div class="col-md-4 col-sm-12 col-xs-12" id="GeographHLdiv" runat="server">
                                    <asp:HyperLink ID="GeographyHL" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASGeography.aspx"> Geography </asp:HyperLink>
                                </div>
                                  <div class="col-md-4 col-sm-12 col-xs-12" id="FunctionHLDiv" runat="server">
                                    <asp:HyperLink ID="FunctionHL" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASFunction.aspx"> Function </asp:HyperLink>
                                </div>--%>
                            </div>
                            <br />
                            <div class="clearfix">
                                   <%--<div class="col-md-4 col-sm-12 col-xs-12" id="GeographHLdiv" runat="server">
                                    <asp:HyperLink ID="GeographyHL" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASGeography.aspx"> Geography </asp:HyperLink>
                                </div>--%>
                                  <%--<div class="col-md-4 col-sm-12 col-xs-12" id="FunctionHLDiv" runat="server">
                                    <asp:HyperLink ID="FunctionHL" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASFunction.aspx"> Function </asp:HyperLink>
                                </div>--%>
                                 <%--<div class="col-md-4 col-sm-12 col-xs-12" id="FloorSectionHLdiv" runat="server">
                                    <asp:HyperLink ID="FloorSectionHL" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASFloorSection.aspx">Floor Section Master</asp:HyperLink>
                                </div>--%>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
