Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Web.Services
Imports System.Collections.Generic
Imports System.Data.OleDb
Imports System.IO

Partial Class Masters_Mas_Webfiles_frmMASCostCenter
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UID") = "" Then
        '    Response.Redirect(Application("FMGLogout"))
        'End If
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = "/Masters/MAS_Webfiles/frmMASMasters.aspx"
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using

        'RegExpCode.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
        'RegExpName.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()


        lblError.Visible = True
        lblError.Text = ""
        lblHeader.Text = Session("Child") + " Master" '+ " <span style='color: red;'>*</span>"
        lblProcess.Text = "Select " + Session("Child") + " <span style='color: red;'>*</span>"
        lblPrssCde.Text = Session("Child") + " Code" + " <span style='color: red;'>*</span>"
        ' RequiredFieldValidator1.ErrorMessage = "Please Enter " + Session("Child") + " Code"
        RequiredFieldValidator5.ErrorMessage = "Please Select " + Session("Child")
        lblPrssNme.Text = Session("Child") + " Name" + " <span style='color: red;'>*</span>"
        RequiredFieldValidator2.ErrorMessage = "Please Select " + Session("Child") + " Code"
        lblVertical.Text = Session("Parent") + " Name" + " <span style='color: red;'>*</span>"
        rfvVertical.ErrorMessage = "Please Select " + Session("Parent") + " Name"
        If Not Page.IsPostBack Then
            BindParentEntity()
            LoadCostcenterGrid(gvItem)
            'obj.costCenter_LoadGrid(gvItem)
            'obj.BindVertical(ddlVertical)
            BindCostcenter()
            trCC.Visible = False
        End If
    End Sub
    Private Sub BindCostcenter()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_COSTCENTER")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_COSTCENTER_DATA")
        ddlCostCenter.DataSource = sp.GetDataSet()
        ddlCostCenter.DataTextField = "Cost_Center_Name"
        ddlCostCenter.DataValueField = "COST_CENTER_CODE"
        ddlCostCenter.DataBind()
        ddlCostCenter.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub
    Private Sub BindParentEntity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_ACTIVE_PARENT_ENTITY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlParent.DataSource = sp.GetDataSet()
        ddlParent.DataTextField = "PE_NAME"
        ddlParent.DataValueField = "PE_CODE"
        ddlParent.DataBind()
        ddlParent.Items.Insert(0, "--Select--")
    End Sub
    Private Sub BindChildEntityByParent()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CHILD_ENTITY_BYPARENT")
        sp.Command.AddParameter("@PRNT_CODE", ddlParent.SelectedValue, DbType.String)
        ddlChild.DataSource = sp.GetDataSet()
        ddlChild.DataTextField = "CHE_NAME"
        ddlChild.DataValueField = "CHE_CODE"
        ddlChild.DataBind()
        ddlChild.Items.Insert(0, "--Select--")

    End Sub
    Private Sub BindVerticalByChild()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_VERTICAL_BY_CHILD")
        sp.Command.AddParameter("@VER_CHE_ID", ddlChild.SelectedValue, DbType.String)
        ddlVertical.DataSource = sp.GetDataSet()
        ddlVertical.DataTextField = "VER_NAME"
        ddlVertical.DataValueField = "VER_CODE"
        ddlVertical.DataBind()
        ddlVertical.Items.Insert(0, "--Select--")

    End Sub


    Private Sub Cleardata()
        txtProjectCode.Text = String.Empty
        txtProjectName.Text = String.Empty
        txtRemarks.Text = String.Empty
        txtCCode.Text = ""
        ddlParent.SelectedIndex = 0
        ddlChild.SelectedIndex = 0
        ddlVertical.SelectedIndex = 0
    End Sub

    Private Sub Modifydata()
        obj.getname = txtProjectName.Text
        obj.getRemarks = txtRemarks.Text
        Dim intstatus As Integer = obj.Modifycostcenter(ddlCostCenter.SelectedItem.Value, txtProjectName.Text.Trim().Replace("'", "''"), ddlParent.SelectedValue, ddlChild.SelectedValue, ddlVertical.SelectedValue, "", "", txtRemarks.Text.Trim().Replace("'", "''"), 2, ddlstatus.SelectedItem.Value, Me)
        If intstatus = 4 Then
            lblError.Text = Session("Child") + " has been modified"
            Cleardata()
            ddlCostCenter.SelectedIndex = 0
            ddlParent.SelectedIndex = 0
            ddlstatus.SelectedIndex = 0
        ElseIf intstatus = 5 Then
            lblError.Text = "We can't modify Business unit old transactions are effected."
        Else
            lblError.Text = "Failed to modify " + Session("Parent")
        End If
        'obj.costCenter_LoadGrid(gvItem)
        'obj.BindVertical(ddlVertical)
        LoadCostcenterGrid(gvItem)
        BindParentEntity()
    End Sub

    <WebMethod()>
    <System.Web.Script.Services.ScriptMethod()>
    Public Shared Function GetEmployeeList(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Try
            Dim cKey As String = ""
            If count = 0 Then
                count = 12
            End If
            Dim s As String = prefixText
            Dim dtEle As New DataTable()
            Dim sp1 As New SqlParameter("@usr_id", SqlDbType.NVarChar, 50)
            sp1.Value = prefixText
            dtEle = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GETUSERS_NAME", sp1)
            Dim dtcount As Integer = CInt(dtEle.Rows.Count)
            If dtcount <> 0 Then
                Dim items As New List(Of String)(count)
                Dim total As Integer = IIf((dtEle.Rows.Count < count), dtEle.Rows.Count, count)
                For i As Integer = 0 To total - 1
                    Dim dr As DataRow = dtEle.Rows(i)
                    items.Add(dr(0))
                Next
                Return items.ToArray()
            Else
                Return New String(-1) {}
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "GetEmployeeList", exp)
        End Try
    End Function

    <WebMethod()>
    <System.Web.Script.Services.ScriptMethod()>
    Public Shared Function GetCostCenters(ByVal prefixText As String, ByVal count As Integer, ByVal contextKey As String) As String()
        Try
            Dim cKey As String = ""
            If count = 0 Then
                count = 12
            End If
            Dim s As String = prefixText
            Dim dtEle As New DataTable()
            Dim sp1 As New SqlParameter("@VC_CC", SqlDbType.NVarChar, 50)
            sp1.Value = prefixText
            dtEle = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "USP_GET_COSTCENTERS", sp1)
            Dim dtcount As Integer = CInt(dtEle.Rows.Count)
            If dtcount <> 0 Then
                Dim items As New List(Of String)(count)
                Dim total As Integer = IIf((dtEle.Rows.Count < count), dtEle.Rows.Count, count)
                For i As Integer = 0 To total - 1
                    Dim dr As DataRow = dtEle.Rows(i)
                    items.Add(dr(0))
                Next
                Return items.ToArray()
            Else
                Return New String(-1) {}
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "GetEmployeeList", exp)
        End Try
    End Function

    Private Sub Insertdata()
        obj.getcode = txtProjectCode.Text
        obj.getname = txtProjectName.Text
        obj.getRemarks = txtRemarks.Text

        Dim intStatus As Integer = obj.InsertModifyCostCenter(txtProjectCode.Text.Trim().Replace("'", "''"), txtProjectName.Text.Trim().Replace("'", "''"), ddlParent.SelectedValue, ddlChild.SelectedValue, ddlVertical.SelectedValue, "", "", txtRemarks.Text.Trim().Replace("'", "''"), 1, ddlstatus.SelectedItem.Value, Me)
        If intStatus = 1 Then
            lblError.Text = "Cost Center Code Already Exists, Please Modify Code."
        ElseIf intStatus = 2 Then
            lblError.Text = Session("Child") + " Inserted Successfully..."
        ElseIf intStatus = 3 Then
            lblError.Text = Session("Child") + " Inserted Successfully...."
            Cleardata()
        End If
        'obj.costCenter_LoadGrid(gvItem)
        LoadCostcenterGrid(gvItem)
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        Try
            If rbActions.Checked = True Then
                BindCostcenter()
                trCC.Visible = False
                txtProjectCode.ReadOnly = False
                btnSubmit.Text = "Submit"
                Cleardata()
            Else
                BindCostcenter()
                trCC.Visible = True
                txtProjectCode.ReadOnly = True
                btnSubmit.Text = "Modify"
                ' obj.BindVertical(ddlVertical)
                BindParentEntity()
                Cleardata()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                If txtProjectName.Text = String.Empty Or ddlVertical.SelectedItem.Text = "--Select--" Then
                    lblError.Text = "Please Enter Mandatory fields"
                ElseIf txtRemarks.Text.Length > 500 Then
                    lblError.Text = "Please Enter remarks in less than or equal to 500 characters"
                Else
                    Insertdata()
                End If
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If txtProjectCode.Text = String.Empty Or txtProjectName.Text = String.Empty Or ddlVertical.SelectedItem.Text = "--Select--" Then
                    lblError.Text = "Please Enter Mandatory fields"
                ElseIf txtRemarks.Text.Length > 500 Then
                    lblError.Text = "Please Enter remarks in less than or equal to 500 characters"
                Else
                    Modifydata()
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmMASProject", "btnSubmit_Click", exp)
        End Try
    End Sub
    Public Shared dt As DataTable
    Public Sub LoadCostcenterGrid(ByVal gv As GridView)

        dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GET_COSTCENTER_DATA")
        gv.DataSource = dt
        gv.DataBind()

    End Sub
    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex

            'obj.costCenter_LoadGrid(gvItem)
            LoadCostcenterGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub lnkStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnk As LinkButton = CType(sender, LinkButton)
            Dim intStatus As Integer = lnk.CommandArgument

            If intStatus = 1 Then
                intStatus = 2
            Else
                intStatus = 1
            End If
            Try
                For i As Integer = 0 To gvItem.Rows.Count - 1
                    Dim lnkStatus As LinkButton = CType(gvItem.Rows(i).FindControl("lnkStatus"), LinkButton)
                    If lnk.ClientID = lnkStatus.ClientID Then
                        Dim lblProjCode As Label = CType(gvItem.Rows(i).FindControl("lblProjCode"), Label)
                        Dim spStatus As New SqlParameter("@STAUTS", SqlDbType.Int)
                        Dim spPrjCode As New SqlParameter("@COSTCENTER_CODE", SqlDbType.NVarChar, 50)
                        spStatus.Value = intStatus
                        spPrjCode.Value = lblProjCode.Text.Trim()
                        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_COSTCENTER_STATUS", spStatus, spPrjCode)
                        Exit For
                    End If
                Next
                lblError.Text = Session("Child") + " has been modified"
            Catch ex As Exception
                lblError.Text = ex.Message
            End Try
            Cleardata()
            'obj.BindVertical(ddlVertical)
            BindParentEntity()
            'obj.costCenter_LoadGrid(gvItem)
            LoadCostcenterGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASProject", "lnkStatus_Click", exp)
        End Try
    End Sub

    Protected Sub btnGet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGet.Click

    End Sub

    Protected Sub ddlCostCenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCostCenter.SelectedIndexChanged
        Try
            If ddlCostCenter.SelectedIndex > 0 Then
                txtCCode.Text = ddlCostCenter.SelectedItem.Value
                Dim strCC As String = String.Empty
                If txtCCode.Text.Contains("/") Then
                    strCC = txtCCode.Text.Substring(0, txtCCode.Text.IndexOf("/"))
                Else
                    strCC = txtCCode.Text
                End If
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_COSTCENTER_CC")
                sp.Command.AddParameter("@COST_CENTER_CODE", ddlCostCenter.SelectedItem.Value, DbType.String)
                Dim ds As DataSet = sp.GetDataSet()
                If ds.Tables(0).Rows.Count > 0 Then
                    txtProjectCode.Text = ds.Tables(0).Rows(0).Item("COST_CENTER_CODE")
                    txtProjectName.Text = ds.Tables(0).Rows(0).Item("COST_CENTER_NAME")
                    txtRemarks.Text = ds.Tables(0).Rows(0).Item("REMARKS")
                    'obj.BindVertical(ddlVertical)
                    BindParentEntity()
                    ddlParent.ClearSelection()
                    ddlParent.Items.FindByValue(ds.Tables(0).Rows(0).Item("PE_CODE")).Selected = True
                    BindChildEntityByParent()
                    ddlChild.ClearSelection()
                    ddlChild.Items.FindByValue(ds.Tables(0).Rows(0).Item("CHE_CODE")).Selected = True
                    BindVerticalByChild()
                    ddlVertical.ClearSelection()
                    ddlVertical.Items.FindByValue(ds.Tables(0).Rows(0).Item("VER_CODE")).Selected = True
                    ddlstatus.ClearSelection()
                    ddlstatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("COST_STA_ID")).Selected = True
                End If
            Else
                Cleardata()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASProject", "ddlProject_SelectedIndexChanged", exp)
        End Try
    End Sub


    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub

    Protected Sub btnbrowse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnbrowse.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile = True Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                msheet = listSheet(0).ToString()
                mfilename = msheet
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    sheetname = mfilename
                    str = "Select * from [" & sheetname & "]"
                End If
                Dim snocnt As Integer = 1
                Dim pro_codecnt As Integer = 1
                Dim pro_namecnt As Integer = 1
                Dim clnt_codecnt As Integer = 1
                Dim clnt_namecnt As Integer = 1
                Dim bfm_idcnt As Integer = 1
                Dim dmn_idcnt As Integer = 1
                Dim pe_codecnt As Integer = 1
                Dim che_codecnt As Integer = 1
                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If LCase(ds.Tables(0).Columns(0).ToString) <> "sno" Then
                            snocnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be SNO"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "cc_code" Then
                            pro_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be CC_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(2).ToString) <> "cc_name" Then
                            pro_namecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be CC_NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(3).ToString) <> "ver_code" Then
                            clnt_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Ver_Code"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "ver_name" Then
                            clnt_namecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be VER_NAME"
                            Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(5).ToString) <> "bfm_id" Then
                            '    bfm_idcnt = 0
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be BFM_ID"
                            '    Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(6).ToString) <> "domain_head_id" Then
                            '    dmn_idcnt = 0
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be DOMAIN_HEAD_ID"
                            '    Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(5).ToString) <> "pe_code" Then
                            pe_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be PE_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(6).ToString) <> "che_code" Then
                            che_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be CHE_CODE"
                            Exit Sub
                        End If
                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "cc_code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "CC Code is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "cc_name" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "CC Name is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "ver_code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "Vertical Code is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "ver_name" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "Vertical Name is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        'If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "bfm_id" Then
                        '    If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                        '        lblMsg.Text = "BFM Id Code is null or empty in the uploaded excel"
                        '        lblMsg.Visible = True
                        '        Exit Sub
                        '    End If
                        'End If
                        'If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "domain_head_id" Then
                        '    If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                        '        lblMsg.Text = "Domain Head Id Code is null or empty in the uploaded excel"
                        '        lblMsg.Visible = True
                        '        Exit Sub
                        '    End If
                        'End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "pe_code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "PE Code is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "che_code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "CHE Code is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_INSERT_MODIFY_COSTCENTER_TEST")
                    sp.Command.AddParameter("@COST_CENTER_CODE", ds.Tables(0).Rows(i).Item("CC_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@COST_CENTER_NAME", ds.Tables(0).Rows(i).Item("CC_NAME").ToString, DbType.String)
                    sp.Command.AddParameter("@VERTICAL_CODE", ds.Tables(0).Rows(i).Item("VER_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@VERTICAL_DESC", ds.Tables(0).Rows(i).Item("VER_NAME").ToString, DbType.String)
                    'sp.Command.AddParameter("@BFM_ID", ds.Tables(0).Rows(i).Item("BFM_ID").ToString, DbType.String)
                    'sp.Command.AddParameter("@DOMAIN_HEAD_ID", ds.Tables(0).Rows(i).Item("DOMAIN_HEAD_ID").ToString, DbType.String)
                    sp.Command.AddParameter("@COST_PE_CODE", ds.Tables(0).Rows(i).Item("PE_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@COST_CHE_CODE", ds.Tables(0).Rows(i).Item("CHE_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@CREATED_BY", Session("UID"), DbType.String)
                    sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)
                    sp.Command.AddParameter("@REMARKS", "", DbType.String)
                    sp.Command.AddParameter("@STATUS", 1, DbType.Int32)
                    sp.Command.AddParameter("@i_Status", 1, DbType.Int32)
                    sp.Command.AddParameter("@i_Op", 1, DbType.Int32)
                    sp.ExecuteScalar()
                Next
                lblMsg.Visible = True
                lblMsg.Text = "Data uploaded successfully..."
                'obj.costCenter_LoadGrid(gvItem)
                LoadCostcenterGrid(gvItem)
            Else
                lblMsg.Text = ""
            End If
        Catch ex As Exception
            lblMsg.Visible = True
            lblMsg.Text = "Input excel was not in a correct format..!!!"
            'Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItem_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvItem.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            'e.Row.Cells(0).Text = Session("Child") + " Name"

            e.Row.Cells(0).Text = Session("Child") + " Code"
            e.Row.Cells(1).Text = Session("Child") + " Name"
            e.Row.Cells(2).Text = Session("Parent") + " Name"
        End If
    End Sub

    Protected Sub ddlParent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlParent.SelectedIndexChanged
        BindChildEntityByParent()
    End Sub

    Protected Sub ddlChild_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlChild.SelectedIndexChanged
        BindVerticalByChild()
    End Sub
End Class
