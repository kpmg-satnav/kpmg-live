<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmEditUser.aspx.vb" Inherits="Masters_Mas_Webfiles_frmEditUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div>
<table id="Table1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                    <tr>
                    <td align="center" width="100%">
           <asp:Label ID="label4" runat="server" CssClass="clsHead" Width="95%"> Edit User</asp:Label>
         
                    </td>
                    </tr>
 <tr>
                    <td align="left" width="100%">
            <asp:TextBox ID="txttoday" runat="server" Visible="False" Width="0px"></asp:TextBox>
                    </td>
                    </tr>
                
                    <tr>
                    <td align="left" width="100%">
          <asp:TextBox ID="Textbox1" runat="server" Width="0px" Visible="False"></asp:TextBox>
                    </td>
                    </tr>   
                    </table>
           
     
           
                
                
                
            <asp:Panel ID="panelmain" runat="server" Width="95%" Height="100%">
           
                <table id="Table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
               <tr>
                    <td width="100%" align="left" colspan="3">  <asp:Label ID="LBLNOTE" runat="server" CssClass="clsNote" Height="8px" Width="128px">(*) Mandatory Fields. </asp:Label> </td>
                      </tr>
                    <tr>
                        <td style="width: 10px">
                            <img alt="" height="27" src="../../Images/table_left_top_corner.gif"/></td>
                        <td class="tableHEADER" align="left">
                            <strong>&nbsp; Edit User </strong>
                        </td>
                        <td>
                            <img alt="" height="27" src="../../Images/table_right_top_corner.gif"/></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
                        </td>
                        <td align="center">
                            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="1">
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                                            ForeColor=""></asp:ValidationSummary>
                                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage" Width="95%"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td width="450" align="left">
                                        &nbsp;Employee Id<font class="clsNote">*</font>
                                        <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Selection of Employee ID from the List is required !"
                                            Display="None" ControlToValidate="cmbAurId" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                                    </td>
                                    <td align="left" style="width: 449px">
                                        <asp:DropDownList ID="cmbAurId" runat="server" CssClass="clsComboBox" Width="97%"
                                            DESIGNTIMEDRAGDROP="492" AutoPostBack="True">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" width="450">
                                        &nbsp;Employee Name</td>
                                    <td style="width: 449px">
                                        <asp:TextBox ID="txtEmpName" runat="server" CssClass="clsTextField" MaxLength="50"
                                            Width="97%"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" width="450">
                                        &nbsp;E-mail</td>
                                    <td style="width: 449px">
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="clsTextField" MaxLength="250"
                                            Width="97%"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td width="450" align="left" style="height: 20px">
                                        &nbsp;Reporting Manager
                                    </td>
                                    <td style="width: 449px; height: 20px;">
                                        <asp:DropDownList ID="cmbrepman" runat="server" CssClass="clsComboBox" Width="97%">
                                        </asp:DropDownList></td>
                                </tr>
                        
                                <tr runat="server" id="trdept" visible="false">
                                    <td width="450" align="left">
                                        &nbsp;Department<font class="clsNote">*</font>
                                        <asp:CompareValidator ID="cvdept" runat="server" ErrorMessage="Select Department !"
                                            Display="None" ControlToValidate="cmbdept" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                                    </td>
                                    <td align="left" style="width: 449px">
                                        <asp:DropDownList ID="cmbdept" runat="server" CssClass="clsComboBox" Width="97%">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td width="450" align="left">
                                        &nbsp;Location <font class="clsNote">*</font>
                                        <asp:CompareValidator ID="cvbdg" runat="server" ErrorMessage="Select Location !"
                                            Display="None" ControlToValidate="cmbdg" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                                    </td>
                                    <td align="left" style="width: 449px">
                                        <asp:DropDownList ID="cmbdg" runat="server" CssClass="clsComboBox" Width="97%">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr runat="server" id="trdesg" visible="false">
                                    <td width="450" align="left" style="height: 26px">
                                        &nbsp;Designation<font class="clsNote">*</font>
                                        <asp:CompareValidator ID="cvdesig" runat="server" ErrorMessage="Select Your Designation !"
                                            Display="None" ControlToValidate="cmbdesg" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                                    </td>
                                    <td align="left" style="height: 26px; width: 449px;">
                                        <asp:DropDownList ID="cmbdesg" runat="server" CssClass="clsComboBox" Width="97%">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr runat="server"  visible="false">
                                    <td width="450" align="left" visible="false">
                                        &nbsp;Office Extension Number
                                        </td>
                                    <td align="left" style="width: 449px; height: 31px;">
                                        <div onmouseover="Tip('Enter phone number in numbers')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtext" runat="server" CssClass="clsTextField" Width="97%" MaxLength="10"></asp:TextBox></div>
                                    </td>
                                </tr>
                                <tr runat="server"  visible="false">
                                    <td width="451" align="left">
                                        &nbsp;Phone Number(Residence)
                                        </td>
                                    <td align="left" style="width: 449px">
                                        <div onmouseover="Tip('Enter residence phone no in numbers and upto 15 characters allowed')"
                                            onmouseout="UnTip()">
                                            <asp:TextBox ID="txtphres" runat="server" CssClass="clsTextField" Width="97%" MaxLength="15"></asp:TextBox></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" width="50%" colspan="2" style="height: 29px">
                                        <asp:Button ID="Button1" runat="server" CssClass="button" Text="Submit"></asp:Button><br />
                                        <asp:Label ID="lbl1" runat="server" CssClass="clsMessage" Height="12px" Visible="False"
                                            Width="90%">Label</asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="height: 100%;">
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <img alt="" src="../../Images/table_left_bot_corner.gif" /></td>
                        <td style="height: 17px;" background="../../Images/table_bot_mid_bg.gif">
                            <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" /></td>
                        <td style="height: 17px;">
                            <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" /></td>
                    </tr>
                </table>
            
            </asp:Panel>
            
        </div>
   </asp:Content>