﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmMasBusinessSpecificMaster.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMasBusinessSpecificMaster" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Business Specific Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Business Specific Master</h3>
                </div>
                <div class="panel-content">
                    <form id="form1" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--  <div class="row" style="padding-bottom: 20px">
                                    <div class="col-md-6 text-right">
                                        <label class="col-md-2 btn pull-right">
                                            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                                ToolTip="Please Select Add to add new Business Specific and Select Modify to modify the existing Business Specific" />
                                            Add</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-md-2 btn pull-left">
                                            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                                ToolTip="Please Select Add to add new Business Specific and Select Modify to modify the existing Business Specific" />
                                            Modify
                                        </label>
                                    </div>
                                </div>--%>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Business Specific Parent<span style="color: red;">*</span></label>
                                    <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtParent"
                                        Display="none" ErrorMessage="Please Enter Business Specific Parent" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtParent" runat="server" CssClass="form-control" Width="99%"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Business Specific Child<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtChild"
                                        Display="none" ErrorMessage="Please Enter Business Specific Child" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtChild" runat="server" CssClass="form-control" Width="99%"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Update" ValidationGroup="Val1" />
                                    <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" PostBackUrl="~/Masters/Mas_Webfiles/frmMasBusinessSpecificMaster.aspx" CausesValidation="False" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvLocation" runat="server" AllowPaging="True" AllowSorting="False" EmptyDataText="No Business Specific Found."
                                    RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" Width="100%"
                                    PageSize="10" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("AMT_BSM_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Specific Parent">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("AMT_BSM_PARENT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Specific Child">
                                            <ItemTemplate>
                                                <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("AMT_BSM_CHILD")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript" defer>
        $("#btnSubmit").click(function () {
            $('#lblMsg').text("")
        });
    </script>
</body>
</html>
