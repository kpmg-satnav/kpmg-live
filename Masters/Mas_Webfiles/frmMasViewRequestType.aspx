<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmMasViewRequestType.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMasViewRequestType"
    Title="View RequestTypes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table id="table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">View Request Type
             <hr align="center" width="60%" /></asp:Label>
                </td>
            </tr>
        </table>
        <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td colspan="3" align="left">
                    <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                <td width="100%" class="tableHEADER" align="left">
                    <strong>&nbsp; View Request Type </strong>
                </td>
                <td>
                    <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="divmessagebackground"
                        ForeColor="" ValidationGroup="Val1" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <table id="t1" runat="server" width="80%" cellpadding="3" cellspacing="0" align="center"
                        border="0">
                        <tr>
                            <td align="left" style="height: 30px" width="30%" valign="top">
                                Enter Request Type to find Particular Record
                            </td>
                            <td align="left" style="height: 30px" width="25%" valign="top">
                                <asp:TextBox ID="txtfindcode" runat="server" CssClass="textBox"></asp:TextBox>&nbsp;<br />
                                &nbsp;
                                <asp:RequiredFieldValidator ID="rfvcode" runat="server" ControlToValidate="txtfindcode"
                                    ErrorMessage="Please enter Requesttype !" ValidationGroup="Val1" SetFocusOnError="True"
                                    Display="None"></asp:RequiredFieldValidator></td>
                            <td align="left" style="height: 30px" width="5%" valign="top">
                                <asp:Button ID="btnfincode" runat="server" CssClass="button" Text="GO" ValidationGroup="Val1" />
                            </td>
                            <td align="LEFT" style="height: 30px" width="20%" valign="top">
                                <asp:LinkButton ID="lbtn1" runat="Server" Text="Get All Records"></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                    <table id="tab1" runat="server" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="center" style="height: 20px">
                                <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                    Width="100%" PageSize="10" EmptyDataText="Sorry! No Records Found">
                                    <Columns>
                                        <asp:TemplateField Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsno" runat="server" CssClass="lblsno" Text='<%#Eval("sno")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Request Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReqType" runat="server" CssClass="lblReqType" Text='<%#Eval("REQUEST_TYPE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" CssClass="lblStatus" Text='<%#Eval("STA_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href='frmMasEditRequestType.aspx?Type=<%#Eval("REQUEST_TYPE")%>&id=<%#Eval("sno")%>'>EDIT</a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:ButtonField Text="DELETE" CommandName="DELETE" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px">
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
