﻿
Imports System.Data
Imports System.Data.SqlClient
Partial Class Masters_MAS_WebFiles_frmMASFunction
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Private Sub Cleardata()
        txtFunctioncode.Text = String.Empty
        txtFunctionName.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlFunction.SelectedIndex = 0
        ddlGeography.SelectedIndex = 0
    End Sub

    Private Sub Modifydata()
        obj.getname = txtFunctionName.Text
        obj.getRemarks = txtRemarks.Text
        If (obj.ModifyFunction(ddlGeography, ddlFunction, Me) > 0) Then
            Cleardata()
            lblMsg.Text = "Function Updated Successfully "
        End If
        obj.Function_LoadGrid(gvItem)
        obj.BindFunction(ddlFunction)
    End Sub

    Private Sub Insertdata()
        obj.getcode = txtFunctioncode.Text
        obj.getname = txtFunctionName.Text
        obj.getRemarks = txtRemarks.Text
        Dim iStatus As Integer = obj.InsertFunction(ddlGeography, Me)
        If iStatus = 1 Then
            lblMsg.Text = "Function Code Already Exists "
            lblMsg.Visible = True
        ElseIf iStatus = 2 Then
            lblMsg.Text = "Function Inserted Successfully "
            lblMsg.Visible = True
            Cleardata()
        ElseIf iStatus = 0 Then
            lblMsg.Text = "Function Inserted Successfully "
            lblMsg.Visible = True
            Cleardata()
        End If
        obj.Function_LoadGrid(gvItem)
        obj.BindFunction(ddlFunction)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
        Try
            'If Session("UID") = "" Then
            '    Response.Redirect(Application("FMGLogout"))
            'End If

            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/Masters/MAS_Webfiles/frmMASMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If Session("UID") = "" Then
                    Response.Redirect(Application("FMGLogout"))
                Else
                    If sdr.HasRows Then
                    Else
                        Response.Redirect(Application("FMGLogout"))
                    End If
                End If
            End Using
            revFunctionCode.ValidationExpression = User_Validation.GetValidationExpressionForCode.VAL_EXPR()
            revNmae.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()

            If Not Page.IsPostBack Then
                obj.Function_LoadGrid(gvItem)
                trCName.Visible = False
                rbActions.Checked = True
                obj.BindFunction(ddlFunction)
                obj.BindGeography(ddlGeography)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmGeography", "Page_Load", exp)
        End Try
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        Try
            If rbActions.Checked = True Then
                trCName.Visible = False
                txtFunctioncode.ReadOnly = False
                btnSubmit.Text = "Submit"
                Cleardata()
            Else
                trCName.Visible = True
                txtFunctioncode.ReadOnly = True
                btnSubmit.Text = "Modify"
                obj.BindFunction(ddlFunction)
                obj.BindGeography(ddlGeography)
                ddlGeography.Enabled = False
                Cleardata()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmGeography", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                Insertdata()
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                Modifydata()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmGeography", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlFunction_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFunction.SelectedIndexChanged
        Try
            If (ddlFunction.SelectedIndex <> 0) Then
                obj.BindGeography(ddlGeography)
                obj.Function_SelectedIndex_Changed(ddlFunction, ddlGeography)
                txtFunctioncode.Text = obj.getcode
                txtFunctionName.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
            Else
                Cleardata()
                obj.BindFunction(ddlFunction)
                obj.BindGeography(ddlGeography)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmGeography", "ddlFunction_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            obj.Function_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmGeography", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                Dim iStatus As Integer = obj.Function_Rowcommand(gvItem, index)
                If iStatus = 0 Then
                    gvItem.HeaderRow.Cells(3).Visible = False
                    gvItem.HeaderRow.Cells(4).Visible = False
                    For i As Integer = 0 To gvItem.Rows.Count - 1
                        gvItem.Rows(i).Cells(3).Visible = False
                        gvItem.Rows(i).Cells(4).Visible = False
                    Next
                    lblMsg.Text = "First Inactivate all the Location(s) under this Function"
                    Exit Sub
                End If
                obj.Function_LoadGrid(gvItem)
                Cleardata()
            End If
            obj.BindFunction(ddlFunction)
            obj.BindGeography(ddlGeography)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmGeography", "gvItem_RowCommand", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("~/Masters/Mas_Webfiles/frmMASMasters.aspx")
    End Sub



    'Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItem.PageIndexChanging
    '    gvItem.PageIndex = e.NewPageIndex()
    '    obj.Function_LoadGrid(gvItem)

    'End Sub


End Class