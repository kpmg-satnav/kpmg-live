﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMASGeography.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASGeography" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Geography Master" ba-panel-class="with-scroll" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Geography Master</h3>
                </div>
                <div class="panel-content">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="CityPanel1" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" CssClass="alert alert-danger" />
                                <div class="row">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12" ForeColor="Red" Style="padding-left: 45px;">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="Radio-btn-s">
                                            <label class="btn">
                                                <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                                    ToolTip="Please Select Add to add new Geography and Select Modify to modify the existing Geography" />
                                                Add</label>
                                            <label class="btn">
                                                <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                                    ToolTip="Please Select Add to add new Geography and Select Modify to modify the existing Geography" />
                                                Modify
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <%-- <div class="col-md-6">
                                        <label class="col-md-2 btn pull-right">
                                            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                                ToolTip="Please Select Add to add new Geography and Select Modify to modify the existing Geography" />
                                            Add</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-md-2 btn pull-left">
                                            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                                ToolTip="Please Select Add to add new Geography and Select Modify to modify the existing Geography" />
                                            Modify
                                        </label>
                                    </div>--%>

                                <div class="row">
                                    <div class="form-group col-sm-12 col-xs-6">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row" id="trCName" runat="server">
                                                    <label for="txtcode">Select Geography <span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvctry" runat="server" ErrorMessage="Please Select Geography "
                                                        InitialValue="--Select--" ControlToValidate="ddlGRY" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlGRY" runat="server" AutoPostBack="True" BorderColor="White" CssClass="selectpicker" data-live-search="true"
                                                        ToolTip="Select Geography Name">
                                                        <asp:ListItem Value="">--Select--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">Geography Code<span style="color: red;">*</span></label>
                                            <asp:RegularExpressionValidator ID="revCode" runat="server" ControlToValidate="txtGCode"
                                                Display="None" ErrorMessage="Please enter Geography code in alphabets and numbers, upto 15 characters allowed"
                                                ValidationExpression="^[A-Za-z0-9]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="rfvcode" runat="server" ControlToValidate="txtGCode"
                                                Display="None" ErrorMessage="Please Enter Geography Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtGCode" runat="server" MaxLength="15" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">Geography Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvname" runat="server" ControlToValidate="txtGName"
                                                Display="None" ErrorMessage="Please Enter Geography Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revName" runat="server" ControlToValidate="txtGName"
                                                Display="None" ErrorMessage="Please Enter Geography Name in alphabets and numbers and (space,-,_ ,(,) allowed), upto 50 characters allowed"
                                                ValidationExpression="^[0-9a-zA-Z-_\/(), ]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <div onmouseover="Tip('Enter Geography Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtGName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">Remarks</label>
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Height="30%" MaxLength="500"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                        <div class="form-group">
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                            <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary" Text="Back"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row form-inline">
                                    <div class="form-group col-md-12">
                                        <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Geography Found."
                                            CssClass="table table-bordered table-hover table-striped" PageSize="5">
                                            <Columns>
                                                <asp:BoundField DataField="GRY_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="Geography Name">
                                                    <ItemStyle HorizontalAlign="Center" ForeColor="Black" />
                                                </asp:BoundField>
                                                <asp:ButtonField HeaderText="Status" ItemStyle-HorizontalAlign="Left" CommandName="Status"
                                                    Text="Button"></asp:ButtonField>
                                                <asp:BoundField DataField="GRY_STA_ID" ItemStyle-HorizontalAlign="Left" HeaderText="STID" HeaderStyle-ForeColor="#000099">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="GRY_CODE" ItemStyle-HorizontalAlign="Left" HeaderText="Geography Code">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                            <HeaderStyle />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/ecmascript" defer>
        $("#btnSubmit").click(function () {
            $('#lblMsg').text("")
        });

        function refreshSelectpicker() {
            $("#<%=ddlGRY.ClientID%>").selectpicker();

        }
        refreshSelectpicker();
    </script>


</body>
</html>