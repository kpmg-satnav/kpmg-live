<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMASTower.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASTower" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />

   

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

   <%-- <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function noway(go) {
            if (document.all) {
                if (event.button == 2) {
                    alert('For Security reasons Right click option was disabled,Inconvenience regreted!');
                    return false;
                }
            }
            if (document.layers) {
                if (go.which == 3) {
                    alert('For Security reasons Right click option was disabled,,Inconvenience regreted!');
                    return false;
                }
            }
        }
    </script>--%>
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Tower Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Tower Master</h3>
                </div>
                <div class="panel-content">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="CityPanel1" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" />
                                <div class="row">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Style="padding-left: 45px;">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="Radio-btn-s">
                                            <label class="btn">
                                                <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                                    ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                                                Add</label>
                                            <label class="btn">
                                                <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                                    ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                                                Modify
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-inline">
                                    <div class="form-group col-sm-12 col-xs-6">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row" id="trLName" runat="server">
                                                    <asp:Label ID="lblTowername" runat="server" Text="Select Tower"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlTName"
                                                        Display="None" ErrorMessage="Please Select Tower " InitialValue="--Select--" ValidationGroup="Val1">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlTName" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                        ToolTip="Select Tower Name">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                   <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Tower Code <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtTowercode"
                                                Display="None" ErrorMessage="Please Enter Tower Code " ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtTowercode" MaxLength="15" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>--%>
                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Tower Code<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtTowercode"
                                                Display="None" ErrorMessage="Please Enter Tower Code " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtTowercode" MaxLength="30" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Tower Name<span style="color: red;">*</span></label>
                                            <asp:RegularExpressionValidator ID="regNam" runat="server" ControlToValidate="txtTowerName"
                                                Display="None" ErrorMessage="Please Enter Tower Name in alphanumerics and (space,-,_ ,(,),/,\,, allowed)"
                                                ValidationExpression="^[0-9a-zA-Z-_/\(), ]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtTowerName"
                                                Display="None" ErrorMessage="Please Enter Tower Name " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/,, allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtTowerName" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                   <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Location Name <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvLocName"
                                                runat="server" ControlToValidate="ddlLName" Display="None" ErrorMessage="Please Select Location Name "
                                                InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlLName" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Location Name">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>--%>
                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Location Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvLocName" runat="server" ControlToValidate="ddlLName" Display="None" ErrorMessage="Please Select Location Name "
                                                InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlLName" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>City Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvCityName"
                                                runat="server" ControlToValidate="ddlCityname" Display="None" ErrorMessage="Please Select City Name "
                                                InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlCityname" runat="server" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select City Name">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Country Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvCnyName" runat="server" ControlToValidate="ddlCountryName"
                                                Display="None" ErrorMessage="Please Select Country Name " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlCountryName" runat="server" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Country Name">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Remarks</label>
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" MaxLength="500" Height="30%"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <label for="txtcode">Upload Images <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                                <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fu1"
                                                    ValidationGroup="Val1" runat="Server" ErrorMessage="Only jpg,jpeg, png, gif files are allowed"
                                                    ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.jpeg)$">
                                                </asp:RegularExpressionValidator>
                                                <div class="btn btn-primary btn-mm">
                                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                                    <asp:FileUpload ID="fu1" runat="Server" Width="90%" AllowMultiple="True" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12" for="txtcode"></label>
                                            <label class="col-md-12" for="txtcode"></label>
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" ToolTip="Click to Submit or Modify"></asp:Button>
                                            <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Tower Found."
                                            CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:BoundField DataField="TWR_NAME" HeaderText="Tower Name">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="lcm_name" HeaderText="Location">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="cty_name" HeaderText="City">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="cny_name" HeaderText="Country">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:ButtonField HeaderText="Status" CommandName="Status" Text="Button">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="TWR_STA_ID" HeaderText="STID">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TWR_CODE" HeaderText="Tower Code">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="View Image">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkViewImage" Text="Image" runat="server" OnClick="lnkViewImage_Click" CommandArgument='<%#Eval("TWR_CODE")%>'>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div id="dialog" visible="false" runat="server">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:GridView ID="grdTowerImages" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Image Found."
                                                CssClass="table table-condensed table-bordered table-hover table-striped">
                                                <Columns>
                                                    <asp:BoundField DataField="TWI_FILENAME" HeaderText="Image Name" />
                                                    <asp:ImageField DataImageUrlField="TWI_UPLOAD_PATH" ControlStyle-Width="100" ControlStyle-Height="100" HeaderText="Preview Image" />
                                                    <asp:TemplateField HeaderText="Download">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDownload" Text="Download" CommandArgument='<%# Eval("TWI_UPLOAD_PATH")%>' runat="server" OnClick="DownloadFile"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDelete" Text="Delete" CommandArgument='<%# Eval("TWI_UPLOAD_PATH")%>' runat="server" OnClick="DeleteFile" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <div style="display: none;" id="divModal">
                                    <iframe id="myIframe" src="SomeValidURL" width="1100" height="800" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/ecmascript" defer>
        $("#btnSubmit").click(function () {
            $('#lblMsg').text("")
        });
        $("#lnkDownload").click(function () {
            alert("hi");
        });
        function refreshSelectpicker() {
            $("#<%=ddlLName.ClientID%>").selectpicker();
            $("#<%=ddlCityname.ClientID%>").selectpicker();
            $("#<%=ddlCountryName.ClientID%>").selectpicker();
            $("#<%=ddlTName.ClientID%>").selectpicker();
        }
        refreshSelectpicker();


    </script>
</body>
</html>
