Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class Masters_Mas_Webfiles_frmMasRequestType
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            lblMsg.Visible = False
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim validatecode As Integer
            validatecode = ValidateRequesttype()
            If validatecode = 0 Then
                lblMsg.Text = "RequestType Already there Please enter another RequestType"
                lblMsg.Visible = True
            Else
                lblMsg.Visible = False
                InsertNewRecord()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function ValidateRequesttype()
        Dim validatecode As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_REQUESTTYPE")
        sp.Command.AddParameter("@REQTYPE", txtReqType.Text, DbType.String)
        validatecode = sp.ExecuteScalar()
        Return validatecode
    End Function

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMasMainMasters.aspx")
    End Sub
    Private Sub InsertNewRecord()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"Add_PN_MAINTENANCE_REQUEST_TYPE")
            sp.Command.AddParameter("@REQTYTPE", txtReqType.Text, DbType.String)
            sp.Command.AddParameter("@STA_ID", ddlstatus.SelectedItem.Value, DbType.Int32)
            sp.ExecuteScalar()
            Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=33")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
