.<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMASVertical.aspx.vb" Inherits="Masters_Mas_Webfiles_frm" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        // function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
        //    re = new RegExp(aspCheckBoxID)
        //    for (i = 0; i < form1.elements.length; i++) {
        //        elm = document.forms[0].elements[i]
        //        if (elm.type == 'checkbox') {
        //            if (re.test(elm.name)) {
        //                if (elm.disabled == false)
        //                    elm.checked = checkVal
        //            }
        //        }
        //    }
        //}
    </script>
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Country Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">
                        <asp:Label ID="lblHeader" runat="server" /></h3>
                </div>
                <div class="panel-content">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="CityPanel1" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" />
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ForeColor="Red" ValidationGroup="Val2" />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:HyperLink ID="hyp" runat="server" Text=" Click here to View the Template" NavigateUrl="~/Masters/Mas_Webfiles/VerticalMaster.xls"></asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Upload Document  (Only Excel) <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Select File"
                                                    ControlToValidate="fpBrowseDoc" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ControlToValidate="fpBrowseDoc"
                                                    ValidationGroup="Val2" runat="Server" ErrorMessage="Only Excel file allowed"
                                                    ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                                                </asp:RegularExpressionValidator>
                                                <div class="col-md-4">
                                                    <div class="btn-default">
                                                        <i class="fa fa-folder-open-o fa-lg"></i>
                                                        <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                                                    </div>
                                                </div>
                                                <div class="form-group col-sm-3 col-xs-6">
                                                    <div class="form-group">
                                                        <asp:Button ID="btnbrowse" runat="Server" CssClass="btn btn-primary custom-button-color" Text="Upload" CausesValidation="true" ValidationGroup="Val2" />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="Radio-btn-s">
                                            <label class="btn">
                                                <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                                    ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                                                Add</label>
                                            <label class="btn">
                                                <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                                    ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                                                Modify
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row" id="trVerName" runat="server">
                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <asp:Label ID="lblLocationname" runat="server" ForeColor ="Black"><span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvvertical1" runat="server" ControlToValidate="ddlVertical"
                                                Display="None" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlVertical" runat="server" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true" TabIndex="1">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <%--<div class="row form-inline">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblverticalCode" runat="server" ForeColor ="Black" ><span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvVerCode" runat="server" ControlToValidate="txtVerticalCode"
                                                Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtVerticalCode" runat="server" CssClass="form-control" TabIndex="2" MaxLength="15"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblverticalName" runat="server" ForeColor ="Black"><span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvVeName" runat="server" ControlToValidate="txtVerticalName"
                                                Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtVerticalName" runat="server" CssClass="form-control"
                                                    TabIndex="3" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    </div>--%>
                                 <div class="row form-inline">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <%--<label>Geography <span style="color: red;">*</span></label>--%>
                                            <asp:HyperLink ID="HyperLink1" runat="server" Text="Geography" Font-Bold="true" NavigateUrl="~/Masters/Mas_Webfiles/frmMASGeography.aspx"></asp:HyperLink>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlGeography"
                                                Display="None" InitialValue="--Select--" ValidationGroup="Val1" ErrorMessage="Please Select Geography "></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlGeography" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"
                                                TabIndex="3">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                     <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <%--<label >Function <span style="color: red;">*</span></label>--%>
                                            <asp:HyperLink ID="FunctionHL" runat="server" Text="Function" Font-Bold="true" NavigateUrl="~/Masters/Mas_Webfiles/frmMASFunction.aspx"></asp:HyperLink>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlFunction"
                                                Display="None" InitialValue="--Select--" ValidationGroup="Val1" ErrorMessage="Please Select Function ">
                                            </asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlFunction" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="3">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                     </div> 
                                 <div class="row form-inline">
                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <%--<label>Sub Function <span style="color: red;">*</span></label>--%>
                                            <asp:HyperLink ID="HyperLink2" runat="server" Text="Sub Function" Font-Bold="true" NavigateUrl="~/Workspace/sms_Webfiles/frmEntityAdmin.aspx"></asp:HyperLink>
                                            <asp:RequiredFieldValidator ID="rfvparent" runat="server" ControlToValidate="ddlParent"
                                                Display="None" InitialValue="--Select--" ValidationGroup="Val1" ErrorMessage="Please Select Parent Entity"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlParent" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"
                                                TabIndex="3">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="form-group">
                                            <%--<label>Sub Function1<span style="color: red;">*</span></label>--%>
                                            <asp:HyperLink ID="HyperLink3" runat="server" Text="Sub Function 1" Font-Bold="true" NavigateUrl="~/Workspace/sms_Webfiles/frmEntity.aspx"></asp:HyperLink>
                                            <asp:RequiredFieldValidator ID="rfvchild" runat="server" ControlToValidate="ddlChild"
                                                Display="None" InitialValue="--Select--" ValidationGroup="Val1" ErrorMessage="Please Select Child Entity">
                                            </asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlChild" runat="server" CssClass="selectpicker" data-live-search="true" TabIndex="3">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                     </div> 
                                 <div class="row form-inline">
                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Location Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvLocName" runat="server" ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please Select Location Name "
                                                InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Du Lead<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlDulead" Display="None" ErrorMessage="Please Select Du Lead "
                                                InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlDulead" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    </div>
                                 <div class="row form-inline">
                                      <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Project Spoc<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlProjectSpoc" Display="None" ErrorMessage="Please Select Project Spoc "
                                                InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlProjectSpoc" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList> 
                                        </div>
                                    </div>
                                      <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server">BayType <span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" InitialValue="--Select--" ControlToValidate="ddlBayType"
                                                Display="None" ErrorMessage="Please Select Bay Type" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlBayType" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Restricted</asp:ListItem>
                                                <asp:ListItem Value="0">Un Restricted</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    </div>
                                <div class="row form-inline">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblverticalCode" runat="server" ForeColor ="Black" ><span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvVerCode" runat="server" ControlToValidate="txtVerticalCode"
                                                Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtVerticalCode" runat="server" CssClass="form-control" TabIndex="2" MaxLength="15"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblverticalName" runat="server" ForeColor ="Black"><span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvVeName" runat="server" ControlToValidate="txtVerticalName"
                                                Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtVerticalName" runat="server" CssClass="form-control"
                                                    TabIndex="3" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                  <%--   <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                Bay Type
                                                                        <input id="chkSelect1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkselect', this.checked)">
                                                                            </HeaderTemplate>
                                                                         <%--   <ItemTemplate>
                                                                                <asp:CheckBox ID="CheckBox2" runat="server" />
                                                                            </ItemTemplate>--%>
                                                                           <%-- <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>--%>
                                     <%-- <ItemTemplate>
                                          <asp:CheckBox ID="chkSelect" Checked='<%#Bind("ticked") %>' runat="server" ToolTip="Click to check" OnCheckedChanged="chkSelect_CheckedChanged" AutoPostBack="true" />
                                                    </ItemTemplate>--%>
                                     
                                    <%-- <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <input id="Checkbox1" type="checkbox" onclick="CheckAllBYTYPEREQCheckBoxes('chkselect', this.checked)">
                                                            </HeaderTemplate>
                                                            <ItemStyle Width="1px" />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkselect" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                       
                                    <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>CostCenter Name <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlCostCenter"
                                                Display="None" ErrorMessage="Please Select CostCenter Name " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlCostCenter" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select CostCenter">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>--%>
                                

                                <div Class="row">
                                    <div Class="col-md-3 col-sm-6 col-xs-12">
                                        <div Class="form-group">
                                            <Label> Remarks<span style="color: red;"></span></label>
                                            <%--<ASP:RequiredFieldValidator ID = "rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                                Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                            <asp:CustomValidator ID = "CustomValidator1" runat="server" ClientValidationFunction="maxLength"
                                                    ControlToValidate="txtRemarks" Display="None" ErrorMessage="Remarks Shoud be less than 500 characters " ValidationGroup="Val1"></asp:CustomValidator>
                                            <div onmouseover = "Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <ASP:TextBox ID = "txtRemarks" runat="server" CssClass="form-control" Height="10%"
                                                    TextMode="MultiLine" MaxLength="200" TabIndex="6"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div Class="col-md-12 text-right">
                                        <div Class="form-group">
                                            <ASP:Button ID = "btnSubmit" runat="server" Width="76px" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" TabIndex="7"></asp:Button>
                                            <ASP:Button ID = "btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" TabIndex="8"></asp:Button>

                                        </div>
                                    </div>
                                </div>

                                <div Class="row">
                                    <div Class="col-md-12">
                                        <div style = "overflow: scroll" >
                                            <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Vertical Found."
                                                CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="20">
                                                <Columns>
                                                    <asp:BoundField DataField="VER_CODE" HeaderText="Vertica" ItemStyle-HorizontalAlign="Left"/>
                                                    <asp:BoundField DataField="VER_NAME" HeaderText="Vertical Name" ItemStyle-HorizontalAlign="Left"/>

                                                    <asp:BoundField DataField="PE_NAME" HeaderText="Sub Function" ItemStyle-HorizontalAlign="Left"/>
                                                    <asp:BoundField DataField="CHE_NAME" HeaderText="Sub Function1" ItemStyle-HorizontalAlign="Left"/>
                                                    <asp:BoundField DataField="GRY_NAME" HeaderText="Geography  Name" ItemStyle-HorizontalAlign="Left"/>
                                                    <asp:BoundField DataField="Fun_NAME" HeaderText="Function  Name" ItemStyle-HorizontalAlign="Left"/>
                                                    <asp:BoundField DataField="LCM_NAME" HeaderText="Location  Name" ItemStyle-HorizontalAlign="Left"/>
                                                    <asp:BoundField DataField="DU_LEAD" HeaderText="Du Lead" ItemStyle-HorizontalAlign="Left"/>
                                                    <asp:BoundField DataField="Project_Spoc" HeaderText="Project Spoc" ItemStyle-HorizontalAlign="Left"/>
                                                    <asp:BoundField DataField="VER_BAY_TYPE" HeaderText="BayType" ItemStyle-HorizontalAlign="Left"/>
                                                    <%--<asp:BoundField DataField="Cost_Center_NAME" HeaderText="CostCenter Name" ItemStyle-HorizontalAlign="Left"/>--%>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkStatus" CausesValidation="false" runat="Server" Text='<% # Bind("Status")%>'
                                                                OnClick="lnkStatus_Click" CommandArgument='<% #Bind("VER_CODE")%>' AccessKey='<% #Bind("VER_STA_ID")%>'></asp:LinkButton><asp:Label Visible="false" runat="server" ID="lblVerticalID" Text='<% # Bind("VER_CODE")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        function ShowHelpWindow() {
            window.open('frmPrjHead.aspx', 'Window', 'toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=690,height=450');
            return false;
        }
    </script>
    <script type="text/javascript" defer>
        $("#btnSubmit").click(function () {
            $('#lblMsg').text("")
        });

        function refreshSelectpicker() {
            $("#<%=ddlVertical.ClientID%>").selectpicker();
            $("#<%=ddlGeography.ClientID%>").selectpicker();
            $("#<%=ddlFunction.ClientID%>").selectpicker();
           $("#<%=ddlParent.ClientID%>").selectpicker();
            $("#<%=ddlChild.ClientID%>").selectpicker();
            $("#<%=ddlLocation.ClientID%>").selectpicker();
            $("#<%=ddlDulead.ClientID%>").selectpicker();
            $("#<%=ddlProjectSpoc.ClientID%>").selectpicker();
            $("#<%=ddlBayType.ClientID%>").selectpicker();
           <%-- $("#<%=ddlCostCenter.ClientID%>").selectpicker();--%>
        }
        refreshSelectpicker();

    </script>
</body>
</html>
