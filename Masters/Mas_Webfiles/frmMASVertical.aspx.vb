Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports System.IO
Imports System.Collections.Generic

Partial Class Masters_Mas_Webfiles_frm
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim obj As New clsMasters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblMsg.Text = ""

            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
            Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
            Dim host As String = HttpContext.Current.Request.Url.Host
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
            param(0).Value = Session("UID")
            param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
            param(1).Value = "/Masters/MAS_Webfiles/frmMASMasters.aspx"
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
                If Session("UID") = "" Then
                    Response.Redirect(Application("FMGLogout"))
                Else
                    If sdr.HasRows Then
                    Else
                        Response.Redirect(Application("FMGLogout"))
                    End If
                End If
            End Using


            'If Session("UID") = "" Then
            '    Response.Redirect(Application("FMGLogout"))
            'End If
            lblMsg.Text = ""
            lblLocationname.Text = "Select " + Session("Parent") + " <span style='color: red;'>*</span>"
            lblHeader.Text = Session("Parent") + " Master"
            lblverticalCode.Text = Session("Parent") + " Code <span style='color: red;'>*</span>"
            rfvVerCode.ErrorMessage = "Please Enter " + Session("Parent") + " Code"
            rfvvertical1.ErrorMessage = "Please Select " + Session("Parent")
            lblverticalName.Text = Session("parent") + " Name <span style='color: red;'>*</span>"
            rfvVeName.ErrorMessage = "Please Enter " + Session("parent") + " Name"


            If Not Page.IsPostBack Then
                BindParentEntity()
                BindGeographyEntity()
                ' BindCostCenterByvertical()
                LoadVerticalGrid(gvItem)
                trVerName.Visible = False
                rbActions.Checked = True
                'obj.BindVertical(ddlVertical)
                BindVerticals()
                obj.Bindlocation(ddlLocation)
                obj.BindDuLead(ddlDulead)
                obj.BindProject_Spoc(ddlProjectSpoc)

            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASVertical", "Page_Load", exp)
        End Try
    End Sub

    Private Sub Cleardata()
        txtVerticalCode.Text = String.Empty
        txtVerticalName.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlVertical.SelectedIndex = 0
        ddlParent.SelectedIndex = 0
        ddlChild.SelectedIndex = 0
        ddlGeography.SelectedIndex = 0
        ddlFunction.SelectedIndex = 0
        ddlLocation.SelectedIndex = -1
        ddlDulead.SelectedIndex = -1
        ddlProjectSpoc.SelectedIndex = -1
        ddlBayType.SelectedIndex = 0
        'ddlCostCenter.SelectedIndex = 0
    End Sub
    Private Sub BindVerticals()
        BindCombo("GET_VERTICAL_DATA", ddlVertical, "VER_NAME", "VER_CODE")
        ddlVertical.SelectedIndex = 0
    End Sub
    Private Sub BindParentEntity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_ACTIVE_PARENT_ENTITY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlParent.DataSource = sp.GetDataSet()
        ddlParent.DataTextField = "PE_NAME"
        ddlParent.DataValueField = "PE_CODE"
        ddlParent.DataBind()
        ddlParent.Items.Insert(0, "--Select--")
    End Sub
    Private Sub BindChildEntityByParent()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CHILD_ENTITY_BYPARENT")
        sp.Command.AddParameter("@PRNT_CODE", ddlParent.SelectedValue, DbType.String)
        ddlChild.DataSource = sp.GetDataSet()
        ddlChild.DataTextField = "CHE_NAME"
        ddlChild.DataValueField = "CHE_CODE"
        ddlChild.DataBind()
        ddlChild.Items.Insert(0, "--Select--")

    End Sub
    Private Sub BindGeographyEntity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_ACTIVE_GEOGRAPHY_ENTITY")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlGeography.DataSource = sp.GetDataSet()
        ddlGeography.DataTextField = "GRY_NAME"
        ddlGeography.DataValueField = "GRY_CODE"
        ddlGeography.DataBind()
        ddlGeography.Items.Insert(0, "--Select--")
    End Sub
    Private Sub BindFunctionEntityByGeography()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_FUNCTION_ENTITY_BYGEOGRAPHY")
        sp.Command.AddParameter("@GRY_CODE", ddlGeography.SelectedValue, DbType.String)
        ddlFunction.DataSource = sp.GetDataSet()
        ddlFunction.DataTextField = "Fun_NAME"
        ddlFunction.DataValueField = "Fun_CODE"
        ddlFunction.DataBind()
        ddlFunction.Items.Insert(0, "--Select--")

    End Sub
    'Private Sub BindCostCenterByVertical()

    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CostCenter_DATA")
    '    sp.Command.AddParameter("@VER_ID", ddlVertical.SelectedValue, DbType.String)
    '    ddlCostCenter.DataSource = sp.GetDataSet()
    '    ddlCostCenter.DataTextField = "Cost_Center_NAME"
    '    ddlCostCenter.DataValueField = "Cost_Center_CODE"
    '    ddlCostCenter.DataBind()
    '    ddlCostCenter.Items.Insert(0, "--Select--")
    'End Sub
    Private Sub insertdata()
        obj.getcode = txtVerticalCode.Text
        obj.getname = txtVerticalName.Text
        obj.getRemarks = txtRemarks.Text.Replace("'", "''")
        obj.getparententity = ddlParent.SelectedItem.Value
        obj.getchildentity = ddlChild.SelectedItem.Value
        obj.getgeographyentity = ddlGeography.SelectedItem.Value
        obj.getfunctionentity = ddlFunction.SelectedItem.Value
        obj.getLocation = ddlLocation.SelectedItem.Value
        obj.getDuLead = ddlDulead.SelectedItem.Value
        obj.getProject_Spoc = ddlProjectSpoc.SelectedItem.Value
        obj.getBayType = ddlBayType.SelectedItem.Value

        Dim intStatus As Integer = obj.InsertVertical(Me, "INSERT")
        If intStatus = 1 Then
            lblMsg.Visible = True
            lblMsg.Text = "The Vertical Code Already Exists, Please Modify Code."
        ElseIf intStatus = 2 Then
            lblMsg.Visible = True
            lblMsg.Text = "The Vertical Name Already Exists, Please Modify Name."
        ElseIf intStatus = 3 Then
            lblMsg.Visible = True
            lblMsg.Text = "Vertical Inserted Successfully..."
            Cleardata()
        End If
        LoadVerticalGrid(gvItem)
    End Sub

    Private Sub Modifydata()
        obj.getcode = txtVerticalCode.Text.Trim()
        obj.getname = txtVerticalName.Text
        obj.getRemarks = txtRemarks.Text.Replace("'", "''")
        obj.getparententity = ddlParent.SelectedValue
        obj.getchildentity = ddlChild.SelectedValue
        obj.getgeographyentity = ddlGeography.SelectedValue
        obj.getfunctionentity = ddlFunction.SelectedValue
        obj.getLocation = ddlLocation.SelectedValue
        obj.getDuLead = ddlDulead.SelectedValue
        obj.getProject_Spoc = ddlProjectSpoc.SelectedValue
        obj.getBayType = ddlBayType.SelectedValue
        Dim intStatus As Integer = obj.InsertVertical(Me, "MODIFY")
        If intStatus = 4 Then
            lblMsg.Visible = True
            lblMsg.Text = "Vertical Modified Successfully..."
            Cleardata()
        ElseIf intStatus = 5 Then
            lblMsg.Visible = True
            lblMsg.Text = "We can't modify Vertical Code old transactions are effected."
        Else
            lblMsg.Text = "Failed to modify."
        End If
        LoadVerticalGrid(gvItem)
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        Try
            If rbActions.Checked = True Then
                trVerName.Visible = False
                txtVerticalCode.ReadOnly = False
                btnSubmit.Text = "Submit"
                Cleardata()
            Else
                Cleardata()
                trVerName.Visible = True
                txtVerticalCode.ReadOnly = True
                btnSubmit.Text = "Modify"
                Cleardata()
                BindVerticals()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASVertical", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                insertdata()
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If ddlVertical.SelectedItem.Text = "--Select--" Or txtVerticalCode.Text = String.Empty Or txtVerticalName.Text = String.Empty Then
                    lblMsg.Text = "Enter mandatory fields"
                ElseIf txtRemarks.Text.Length > 500 Then
                    lblMsg.Text = "Enter Remarks in less than or equal to 500 characters"
                Else
                    Modifydata()
                    BindVerticals()
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmMASVertical", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVertical.SelectedIndexChanged
        Try
            Dim strsql As String = String.Empty
            'Dim dr As SqlDataReader
            Dim iQry As Integer = 0
            If ddlVertical.SelectedValue <> "--Select--" Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_DROPDOWN_BY_VERTICAL")
                sp.Command.AddParameter("@VER_CODE", ddlVertical.SelectedItem.Value, DbType.String)
                Dim Ds As New DataSet
                Ds = sp.GetDataSet

                txtVerticalCode.Text = Ds.Tables(0).Rows(0)("VER_CODE")
                txtVerticalName.Text = Ds.Tables(0).Rows(0)("VER_NAME")
                BindParentEntity()
                ddlParent.SelectedValue = Ds.Tables(0).Rows(0)("VER_PE_CODE")
                BindChildEntityByParent()
                ddlChild.SelectedValue = Ds.Tables(0).Rows(0)("VER_CHE_CODE")
                BindGeographyEntity()
                ddlGeography.SelectedValue = Ds.Tables(0).Rows(0)("VER_GRY_CODE")
                BindFunctionEntityByGeography()
                ddlFunction.SelectedValue = Ds.Tables(0).Rows(0)("VER_Fun_CODE")
                obj.Bindlocation(ddlLocation)
                ddlLocation.SelectedValue = Ds.Tables(0).Rows(0)("VER_LOC")
                obj.BindDuLead(ddlDulead)
                ddlDulead.SelectedValue = Ds.Tables(0).Rows(0)("VER_DU_LEAD")
                obj.BindProject_Spoc(ddlProjectSpoc)
                ddlProjectSpoc.SelectedValue = Ds.Tables(0).Rows(0)("VER_PROJECT_SPOC")
                txtRemarks.Text = Ds.Tables(0).Rows(0)("VER_REM")


                'obj.Vertical_SelectedIndex_Changed(ddlVertical)
                'txtVerticalCode.Text = obj.getcode
                'txtVerticalName.Text = obj.getname
                'txtRemarks.Text = obj.getRemarks



                'strsql = "Select VER_PE_CODE, VER_CHE_CODE from " & Session("TENANT") & ".VERTICAL where VER_CODE='" & ddlVertical.SelectedItem.Value & "'"
                'dr = SqlHelper.ExecuteReader(CommandType.Text, strsql)
                'If (dr.Read()) Then
                '    BindParentEntity()
                '    ddlParent.SelectedValue = dr("VER_PE_CODE").ToString()
                '    BindChildEntityByParent()
                '    ddlChild.SelectedValue = dr("VER_CHE_CODE").ToString()
                'End If
            Else
                Cleardata()
                BindVerticals()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASVertical", "ddlVertical_SelectedIndexChanged", exp)
        End Try
    End Sub
    Public Shared dt As DataTable
    Public Sub LoadVerticalGrid(ByVal gv As GridView)

        dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GET_VERTICAL_DATA")
        gv.DataSource = dt
        gv.DataBind()

    End Sub
    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            LoadVerticalGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASVertical", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub lnkStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnk As LinkButton = CType(sender, LinkButton)
        Dim Status As String = lnk.AccessKey
        Dim verticalCode As String = lnk.CommandArgument
        Try
            obj.Vertical_Rowcommand(verticalCode, Status)
            BindVerticals()
            LoadVerticalGrid(gvItem)
            Cleardata()
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASVertical", "lnkStatus_Click", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASMasters.aspx")
    End Sub

    Protected Sub btnbrowse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnbrowse.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile = True Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename
                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                msheet = listSheet(0).ToString()
                mfilename = msheet
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    sheetname = mfilename
                    str = "Select * from [" & sheetname & "]"
                End If
                Dim snocnt As Integer = 1
                Dim ver_codecnt As Integer = 1
                Dim ver_namecnt As Integer = 1
                Dim pe_codecnt As Integer = 1
                Dim che_codecnt As Integer = 1
                Dim gry_codecnt As Integer = 1
                Dim fun_codecnt As Integer = 1
                Dim loc_codecnt As Integer = 1
                Dim du_lead_codecnt As Integer = 1
                Dim project_spoc_codecnt As Integer = 1
                Dim rm_idcnt As Integer = 1
                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If LCase(ds.Tables(0).Columns(0).ToString) <> "sno" Then
                            snocnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be SNO"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "ver_code" Then
                            ver_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be VER_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(2).ToString) <> "ver_name" Then
                            ver_namecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be VER_NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(3).ToString) <> "pe_code" Then
                            pe_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be PE_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "che_code" Then
                            che_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be CHE_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(3).ToString) <> "gry_code" Then
                            gry_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be GRY_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "fun_code" Then
                            fun_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Fun_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "loc_code" Then
                            loc_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be LOC_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "du_lead_code" Then
                            du_lead_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be du_lead_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "project_spoc_code" Then
                            project_spoc_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be project_spoc_code"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(5).ToString) <> "rm_id" Then
                            rm_idcnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be RM_ID"
                            Exit Sub
                        End If
                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "ver_code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "Vertical Code is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "ver_name" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "Vertical Name is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "rm_id" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                lblMsg.Text = "RM Id is null or empty in the uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_INSERT_MODIFY_VERTICAL")
                    sp.Command.AddParameter("@VER_CODE", ds.Tables(0).Rows(i).Item("VER_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_NAME", ds.Tables(0).Rows(i).Item("VER_NAME").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_PE_CODE", ds.Tables(0).Rows(i).Item("PE_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_CH_CODE", ds.Tables(0).Rows(i).Item("CHE_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_GRY_CODE", ds.Tables(0).Rows(i).Item("GRY_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_Fun_CODE", ds.Tables(0).Rows(i).Item("Fun_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_LOC", ds.Tables(0).Rows(i).Item("LOC_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_DU_LEAD", ds.Tables(0).Rows(i).Item("DU_LEAD_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_PROJECT_SPOC", ds.Tables(0).Rows(i).Item("PROJECT_SPOC_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_REM", ds.Tables(0).Rows(i).Item("VER_NAME").ToString, DbType.String)
                    sp.Command.AddParameter("@VER_UPT_BY", Session("Uid"), DbType.String)
                    sp.Command.AddParameter("@VER_VRM", ds.Tables(0).Rows(i).Item("RM_ID").ToString, DbType.String)
                    sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)
                    sp.Command.AddParameter("@i_Status", 1, DbType.Int32)
                    sp.Command.AddParameter("@i_Op", 1, DbType.Int32)
                    sp.ExecuteScalar()
                Next
                lblMsg.Visible = True
                lblMsg.Text = "Data uploaded successfully..."
                'Else
                '    lblMsg.Text = "Please Upload Proper file...."
                LoadVerticalGrid(gvItem)
            Else
                lblMsg.Text = ""
            End If
        Catch ex As Exception
            lblMsg.Visible = True
            lblMsg.Text = "Input excel was not in a correct format..!!!"
            'Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItem_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvItem.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then

            e.Row.Cells(0).Text = Session("Parent") + " Code"
            e.Row.Cells(1).Text = Session("Parent") + " Name"
            e.Row.Cells(2).Text = Session("geography") + "Code"
            e.Row.Cells(3).Text = Session("geography") + "Name"

        End If
    End Sub

    Protected Sub ddlParent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlParent.SelectedIndexChanged
        BindChildEntityByParent()
    End Sub
    Protected Sub ddlGeography_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGeography.SelectedIndexChanged
        BindFunctionEntityByGeography()
    End Sub
    'Protected Sub ddlCostCenter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCostCenter.SelectedIndexChanged
    '    BindCostCenterByVertical()
    'End Sub
End Class
