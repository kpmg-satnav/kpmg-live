<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="seattype.aspx.vb" Inherits="Masters_Mas_Webfiles_seattype" title="Seat Type Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <script language="javascript" type="text/javascript" defer> 
     function maxLength(s,args)
     { 
     if(args.Value.length >= 500)
     args.IsValid=false;
     }
   
    </script>


    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript" defer></script>

   <div>
   <table id="table1" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center">
    <asp:Label ID="lblHead" runat="server" CssClass="clsHead"  Width="86%" Font-Underline="False" ForeColor="Black">Seat Type Master 
    <hr align="center" width="60%" />
            </asp:Label>
            </td>
            </tr>
           
            </table>
            
            <asp:Panel ID="PNLCONTAINER"  runat="server" Width="85%" Height="100%">
             
                <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
                    align="center" border="0">
                     <tr>
                        <td width="100%" align="left" colspan="3">

            <asp:Label  ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
             
            </td>
            </tr>
                    <tr>
                        <td>
                            <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                        <td width="100%" class="tableHEADER" align="left">
                            &nbsp;<strong>Seat Type Master </strong>
                        </td>
                        <td>
                            <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">
                            &nbsp;</td>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server"  CssClass="clsMessage"
                                ForeColor="" ValidationGroup="Val1" /><br />
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                            <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="1">
                                <tr>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" style="height: 43px">
                                        <asp:RadioButtonList ID="rbActions" runat="server" CssClass="clsRadioButton" RepeatDirection="Horizontal"
                                            AutoPostBack="True">
                                            <asp:ListItem Value="0" Selected="True">Add</asp:ListItem>
                                            <asp:ListItem Value="1">Modify</asp:ListItem>
                                        </asp:RadioButtonList></td>
                                </tr>
                                <tr id="trCName" runat="server">
                                    <td align="left" class="clslabel" style="height: 11px; width: 50%;">
                                        Select Seat Type
                                        <span style="font-size: 8pt; color: #ff0000">*</span></td>
                                    <td align="left" style="width: 43%; height: 11px">
                                        <asp:DropDownList ID="ddlseattype" runat="server" Width="97%" AutoPostBack="True" CssClass="clsComboBox">
                                            
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" class="clslabel" style="width: 50%; height: 26px;">
                                        &nbsp;Seat Type code<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtseattypecode"
                                            Display="None" ErrorMessage="Please Enter Seat type Code " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="width: 43%; height: 26px;">
                                                                                   <asp:TextBox ID="txtseattypecode" MaxLength="15" runat="server" Width="97%" CssClass="clsTextField"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="clslabel" style="height: 16px; width: 50%;">
                                        &nbsp;Seat Type Name<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtseatname"
                                            Display="None" ErrorMessage="Please Enter Seat type Name " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                       </td>
                                    <td align="left" style="height: 16px; width: 43%;">
                                        
                                            <asp:TextBox ID="txtseatname" MaxLength="50" runat="server" Width="97%" CssClass="clsTextField"></asp:TextBox>
                                        
                                    </td>
                                </tr>
                              
                                <tr>
                                    <td align="left" class="clslabel" style="width: 50%; height: 16px">
                                        &nbsp;Remarks<font class="clsNote">*</font>
                                      
                                        <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                            Display="None" ErrorMessage="Please enter Remarks " ValidationGroup="Val1"></asp:RequiredFieldValidator></td>
                                    <td align="left" style="width: 43%; height: 16px">
                                       
                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="clsTextField" TextMode="MultiLine"
                                                Width="97%" MaxLength="1000" Height="35px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <table class="table" style="height: 22px" width="100%" border="1">
                                <tr>
                                    <td align="center" style="height: 20px">
                                        &nbsp;
                                        <asp:Button ID="btnSubmit" runat="server" Width="76px" CssClass="button" Text="Submit" ValidationGroup="Val1">
                                        </asp:Button>&nbsp;<asp:Button ID="btnback" runat="server" Width="76px" CssClass="button" Text="Back" >
                                        </asp:Button></td>
                                </tr>
                                <tr>
                                    <td align="center" style="height: 20px">
                                        <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                            Width="100%" PageSize="20">
                                            <Columns>
                                                <asp:BoundField DataField="SEAT_TYPE_CODE" HeaderText="Seat Type Code" >
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SEAT_TYPE" HeaderText="Seat Type Name" >
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SEAT_REMARKS" HeaderText="Seat Type Remarks" >
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                               
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
         
            </asp:Panel>
           
           
        </div>
</asp:Content>

