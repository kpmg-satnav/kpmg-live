﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmMasSpaceMasters.aspx.vb" Inherits="Masters_frmMasSpaceMasters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>



    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
        function noway(go) {
            if (document.all) {
                if (event.button == 2) {
                    alert('For Security reasons Right click option was disabled,Inconvenience regreted!');
                    return false;
                }
            }
            if (document.layers) {
                if (go.which == 3) {
                    alert('For Security reasons Right click option was disabled,,Inconvenience regreted!');
                    return false;
                }
            }
        }
        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);
        }
        document.onmousedown = noway;
    </script>
    <style>
        .btn {
            border-radius: 4px;
        }
    </style>
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Space Masters" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Space Masters</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <div class="box-body">
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/WorkSpace/SMS_Webfiles/frmSpaceTypeMaster.aspx">Space Type Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink2" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/WorkSpace/SMS_Webfiles/SpaceSubTypeMaster.aspx">Space Sub Type Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink14" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/workspace/sms_webfiles/Vertical_Escalation.aspx">
                                        <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                    </asp:HyperLink>
                                </div>
                            </div>
                            <br />
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink17" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Workspace/sms_Webfiles/space_matrix.aspx">Space Matrix</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink8" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/Mas_Webfiles/frmMASShift.aspx">Shift Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink18" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/SMViews/Masters/UploadSpaceAllocation.aspx">Upload Space Allocation Data</asp:HyperLink>
                                </div>
                            </div>
                            <br />
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink6" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/MAS_Webfiles/frmMASspacetypeGradeMapping.aspx">Space Type Grade Master</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink3" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Workspace/sms_Webfiles/SpaceTypeCost.aspx">Space Type Wise Cost</asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink4" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/SMViews/Masters/SpaceDateBookingRestriction.aspx">Space Booking Restriction</asp:HyperLink>
                                </div>

                            </div>
                            <br />
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <%--<asp:HyperLink ID="HyperLink5" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/SMViews/Masters/VLAN.aspx">VLAN</asp:HyperLink>--%>
                                    <a href="../VLAN.aspx"  class="btn btn-block btn-primary">VLAN</a>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="HyperLink7" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/SMViews/Masters/CovidDistancingMaster.aspx">Configure Social Distancing</asp:HyperLink>
                                </div>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
