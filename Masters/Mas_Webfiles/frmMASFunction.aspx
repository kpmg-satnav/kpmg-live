﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMASFunction.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASFunction" %>


<!DOFunPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

    </script>

</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Function Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Function Master</h3>
                </div>
                <div class="panel-content">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="FunctionPanel1" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" />
                                <div class="row">
                                    <div class="form-group">
                                        <div class="row">

                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Style="padding-left: 45px;">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="Radio-btn-s">
                                            <label class="btn">
                                                <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                                    ToolTip="Please Select Add to add new Geography and Select Modify to modify the existing Geography" />
                                                Add</label>
                                            <label class="btn">
                                                <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                                    ToolTip="Please Select Add to add new Geography and Select Modify to modify the existing Geography" />
                                                Modify
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-sm-12 col-xs-6">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row" id="trCName" runat="server">
                                                    <label>Select Function<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvFunction" runat="server" ErrorMessage="Please Select Function "
                                                        InitialValue="--Select--" ControlToValidate="ddlFunction" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:DropDownList ID="ddlFunction" runat="server" AutoPostBack="True" CssClass="selectpicker">
                                                        <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Function Code <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtFunctioncode"
                                                Display="None" ErrorMessage="Please Enter Function Code " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revFunctionCode" runat="server" ControlToValidate="txtFunctioncode"
                                                Display="None" ErrorMessage="Please enter Function code in alphabets and numbers, upto 15 characters allowed"
                                                ValidationExpression="^[A-Za-z0-9]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtFunctioncode" MaxLength="15" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Function Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtFunctionName"
                                                Display="None" ErrorMessage="Please Enter Function Name " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revNmae" runat="server" ControlToValidate="txtFunctionName"
                                                Display="None" ErrorMessage="Please Enter Function Name in alphabets and numbers and (space,-,_ ,(,),, allowed)"
                                                ValidationExpression="^[0-9a-zA-Z-_\/(), ]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/,, allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtFunctionName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Geography Name <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlGeography"
                                                Display="None" ErrorMessage="Please Select Geography Name " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlGeography" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select Geography">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Remarks</label>
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" MaxLength="500" Height="30%"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 text-right" style="padding-top: 17px">
                                    <div class="form-group ">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                        <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary" Text="Back"></asp:Button>
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No Function Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:BoundField DataField="Fun_NAME" HeaderText="Function Name">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="GRY_name" HeaderText="Geography Name">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:ButtonField HeaderText="Status" CommandName="Status"
                                                    Text="Button">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="Fun_STA_ID" HeaderText="STID">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Fun_CODE" HeaderText="Function Code">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/ecmascript" defer>
        $("#btnSubmit").click(function () {
            $('#lblMsg').text("")
        });
        function refreshSelectpicker() {
            $("#<%=ddlGeography.ClientID%>").selectpicker();
            $("#<%=ddlFunction.ClientID%>").selectpicker();

        }
        refreshSelectpicker();
    </script>
</body>
</html>