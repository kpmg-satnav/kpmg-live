Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager

Partial Class Masters_Mas_Webfiles_frmMASBlock
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters

    Private Sub cleardata()
        txtBlockcode.Text = String.Empty
        txtBlockName.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlBName.SelectedIndex = 0
        ddlFloor.SelectedIndex = 0
        ddlTower.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0
        ddlCity.SelectedIndex = 0
        ddlCountry.SelectedIndex = 0
    End Sub

    Private Sub Insertdata()
        obj.getcode = txtBlockcode.Text
        obj.getname = txtBlockName.Text
        obj.getRemarks = txtRemarks.Text
        Dim iStatus As Integer = obj.insertBlock(ddlFloor, ddlTower, ddlLocation, ddlCity, ddlCountry, Me)
        If iStatus = 1 Then
            lblMsg.Text = "Wing Code Already Exists"
            lblMsg.Visible = True
        ElseIf iStatus = 2 Then
            lblMsg.Text = "Wing Inserted Successfully"
            lblMsg.Visible = True
            cleardata()
        ElseIf iStatus = 0 Then
            lblMsg.Text = "Wing Inserted Successfully"
            lblMsg.Visible = True
            cleardata()
        End If
        obj.BindBlock(ddlBName)
        obj.BindFloor2(ddlFloor)
        obj.BindTower(ddlTower)
        obj.Bindlocation(ddlLocation)
        obj.BindCity(ddlCity)
        obj.BindCountry(ddlCountry)
        obj.Block_LoadGrid(gvItem)
    End Sub

    Private Sub Modifydata()
        obj.getname = txtBlockName.Text
        obj.getRemarks = txtRemarks.Text
        If (obj.ModifyBlock(ddlBName, ddlFloor, ddlTower, ddlLocation, ddlCity, ddlCountry, Me) > 0) Then
            lblMsg.Text = "Wing Updated Successfully"
            lblMsg.Visible = True
            cleardata()
        End If
        obj.BindBlock(ddlBName)
        obj.BindFloor2(ddlFloor)
        obj.BindTower(ddlTower)
        obj.Bindlocation(ddlLocation)
        obj.BindCity(ddlCity)
        obj.BindCountry(ddlCountry)
        obj.Block_LoadGrid(gvItem)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            lblMsg.Visible = False
            If Not Page.IsPostBack Then
                obj.Block_LoadGrid(gvItem)
                trLName.Visible = False
                rbActions.Checked = True
                obj.BindBlock(ddlBName)
                obj.BindFloor2(ddlFloor)
                obj.BindTower(ddlTower)
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASBlock", "Page_Load", exp)
        End Try
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        ddlFloor.Enabled = True
        ddlTower.Enabled = True
        ddlLocation.Enabled = True
        ddlCity.Enabled = True
        ddlCountry.Enabled = True
        Try
            If rbActions.Checked = True Then
                txtBlockcode.ReadOnly = False
                btnSubmit.Text = "Submit"
                trLName.Visible = False
                cleardata()
            Else
                trLName.Visible = True
                txtBlockcode.ReadOnly = True
                ddlFloor.Enabled = False
                ddlTower.Enabled = False
                ddlLocation.Enabled = False
                ddlCity.Enabled = False
                ddlCountry.Enabled = False
                btnSubmit.Text = "Modify"
                obj.BindBlock(ddlBName)
                obj.BindFloor2(ddlFloor)
                obj.BindTower(ddlTower)
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
                cleardata()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASBlock", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                Insertdata()
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                Modifydata()
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmMASBlock", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlBName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBName.SelectedIndexChanged
        Try
            If ddlBName.SelectedItem.Value <> "--Select--" Then
                obj.BindFloor2(ddlFloor)
                obj.BindTower(ddlTower)
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
                obj.Block_SelectedIndex_ChangedWing(ddlBName, ddlFloor, ddlTower, ddlLocation, ddlCity, ddlCountry)
                txtBlockcode.Text = obj.getcode
                txtBlockName.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
            Else
                cleardata()
                obj.BindBlock(ddlBName)
                obj.BindFloor2(ddlFloor)
                obj.BindTower(ddlTower)
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASBlock", "ddlBName_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            obj.Block_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASBlock", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                Dim iStatus As Integer = obj.Block_Rowcommand(gvItem, index)
                If iStatus = 0 Then
                    gvItem.HeaderRow.Cells(4).Visible = False
                    gvItem.HeaderRow.Cells(5).Visible = False
                    For i As Integer = 0 To gvItem.Rows.Count - 1
                        gvItem.Rows(i).Cells(4).Visible = False
                        gvItem.Rows(i).Cells(5).Visible = False
                    Next
                    lblMsg.Text = "First inactivate all the seats under this Block"
                    lblMsg.Visible = True
                    Exit Sub
                End If
                obj.Block_LoadGrid(gvItem)
            End If
            cleardata()
            obj.BindBlock(ddlBName)
            obj.BindFloor2(ddlFloor)
            obj.BindTower(ddlTower)
            obj.Bindlocation(ddlLocation)
            obj.BindCity(ddlCity)
            obj.BindCountry(ddlCountry)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASBlock", "gvItem_RowCommand", exp)
        End Try
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFloor.SelectedIndexChanged
        Try
            If (ddlFloor.SelectedIndex <> 0) Then
                Dim str As String = ddlFloor.SelectedValue.ToString().Trim()
                Dim strFlrCode As String = ddlFloor.SelectedValue.ToString().Trim().Remove(ddlFloor.SelectedValue.ToString().Trim().IndexOf("/"))
                Dim strTwrCode As String = str.Substring(str.IndexOf("/") + 1)

                Dim dr As SqlDataReader = obj.block_getflr_detls(strFlrCode, strTwrCode)
                If (dr.Read) Then
                    ddlTower.SelectedValue = dr("flr_twr_id").ToString().Trim()
                    ddlLocation.SelectedValue = dr("flr_loc_id").ToString().Trim()
                    ddlCity.SelectedValue = dr("flr_cty_id").ToString().Trim()
                    ddlCountry.SelectedValue = dr("flr_cny_id").ToString().Trim()
                End If
            Else
                ddlTower.SelectedIndex = 0
                ddlLocation.SelectedIndex = 0
                ddlCity.SelectedIndex = 0
                ddlCountry.SelectedIndex = 0

            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASBlock", "ddlFloor_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("~/Masters/Mas_Webfiles/frmMASMasters.aspx")
    End Sub
End Class
