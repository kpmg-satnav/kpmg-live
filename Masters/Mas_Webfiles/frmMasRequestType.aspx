<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmMasRequestType.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMasRequestType"
    Title="Maintenance Request" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table id="table1" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">Add Request Type
             <hr align="center" width="60%" /></asp:Label>
                </td>
            </tr>
        </table>
        <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td colspan="3" align="left">
                    <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                <td width="100%" class="tableHEADER" align="left">
                    <strong>&nbsp; Add Request Type </strong>
                </td>
                <td>
                    <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="divmessagebackground"
                        ForeColor="" ValidationGroup="Val1" /><br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="1">
                        <tr>
                            <td align="left" class="clslabel" style="height: 16px; width: 50%;">
                                &nbsp;Request Type<font class="clsNote">*</font>
                                <asp:RegularExpressionValidator ID="revReqType" runat="server" Display="None" ErrorMessage="Please Enter Request Type in alphanumerics and (space,-,_ ,(,),, allowed)"
                                    ValidationExpression="^[0-9a-zA-Z-_\/(), ]+" ControlToValidate="txtReqType"
                                    ValidationGroup="Val1">
                                </asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="rfvreqtype" runat="server" ControlToValidate="txtReqType"
                                    Display="None" ErrorMessage="Please Enter RequestType " ValidationGroup="Val1">
                                </asp:RequiredFieldValidator></td>
                            <td align="left" style="height: 16px; width: 43%;">
                                <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/,, allowed) and upto 50 characters allowed')"
                                    onmouseout="UnTip()">
                                    <asp:TextBox ID="txtReqType" MaxLength="50" runat="server" Width="97%" CssClass="clsTextField">
                                    </asp:TextBox>
                            </td>
                        </tr>
                        <tr id="trLName" runat="server">
                            <td align="left" class="clslabel" style="height: 11px; width: 50%;">
                                &nbsp;<asp:Label ID="lblStatus" runat="server" Text="Select Status" Font-Bold="False"></asp:Label>
                                <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlstatus"
                                    Display="None" ErrorMessage="Please Select Status " ValidationGroup="Val1" initialvalue="--Select--">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 43%; height: 11px">
                                <asp:DropDownList ID="ddlstatus" runat="server" Width="97%" AutoPostBack="False"
                                    CssClass="clsComboBox" ToolTip="Select Status">
                                   <asp:ListItem>--Select--</asp:ListItem>
                                   <asp:ListItem value="1" text="Active"></asp:ListItem>
                                   <asp:ListItem value="0" text="InActive"></asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                    <table class="table" style="height: 22px" width="100%" border="1">
                        <tr>
                            <td align="center" style="height: 10px">
                                &nbsp;
                                <asp:Button ID="btnSubmit" runat="server" Width="76px" CssClass="button" Text="Submit"
                                    ValidationGroup="Val1"></asp:Button>&nbsp;
                                <asp:Button ID="btnback" runat="server" CssClass="button" Text="Back" Width="76px" /></td>
                        </tr>
                    </table>
                </td>
                <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td style="height: 17px">
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
        </div>
</asp:Content>
