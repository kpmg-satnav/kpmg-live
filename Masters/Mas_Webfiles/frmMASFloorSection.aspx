﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMASFloorSection.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASFloorSection" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="FloorSection Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">FloorSection Master</h3>
                </div>
                <div class="panel-content">
                    <form runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="CityPanel1" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Val1" ForeColor="Red" />

                                <div class="row">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Style="padding-left: 45px;">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="Radio-btn-s">
                                            <label class="btn">
                                                <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                                    ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                                                Add</label>
                                            <label class="btn">
                                                <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                                    ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                                                Modify
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-inline">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" id="trLName" runat="server">
                                            <label>Select FloorSection<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlFloorSection"
                                                Display="None" ErrorMessage="Please Select FloorSection " InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlFloorSection" runat="server" AutoPostBack="True" ToolTip="Select the FloorSection" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>FloorSection Code<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFloorSectioncode"
                                                Display="None" ErrorMessage="Please Enter FloorSection Code " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtFloorSectioncode" MaxLength="30" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>FloorSection Name<span style="color: red;">*</span></label>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="None"
                                                ErrorMessage="Please Enter FloorSection Name in alphanumerics and (space,-,_ ,(,),, allowed)"
                                                ValidationExpression="^[0-9a-zA-Z-_\/(), ]+" ControlToValidate="txtFloorSectionName" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtFloorSectionName"
                                                Display="None" ErrorMessage="Please Enter FloorSection Name " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/,, allowed) and upto 50 characters allowed')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtFloorSectionName" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                      <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Floor Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvFloor" runat="server" ControlToValidate="ddlFloor"
                                                Display="None" ErrorMessage="Please Select Floor Name " InitialValue="--Select--" ValidationGroup="Val1">
                                            </asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlFloor" runat="server" AutoPostBack="True" class="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Tower Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvTower" runat="server" ControlToValidate="ddlTower"
                                                Display="None" ErrorMessage="Please Select Tower Name " InitialValue="--Select--" ValidationGroup="Val1">
                                            </asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlTower" runat="server" AutoPostBack="True" class="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server">FloorSection BayType <span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" InitialValue="--Select--" ControlToValidate="ddlFloorSectionBayType"
                                                Display="None" ErrorMessage="Please Select FloorSection BayType" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlFloorSectionBayType" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Restricted</asp:ListItem>
                                                <asp:ListItem Value="0">Un Restricted</asp:ListItem>
                                                <asp:ListItem Value="2">Open</asp:ListItem>
                                                <asp:ListItem Value="3">Closed</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Location Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="ddlLocation" Display="None" ErrorMessage="Please Select Location Name "
                                                InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" class="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row ">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>City Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvCity" runat="server"
                                                ControlToValidate="ddlCity" Display="None" ErrorMessage="Please Select City Name "
                                                InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" class="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Country Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry"
                                                Display="None" ErrorMessage="Please Select Country Name " InitialValue="--Select--" ValidationGroup="Val1">
                                            </asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" class="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Remarks</label>
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Height="30%" MaxLength="500" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12" for="txtcode"></label>
                                            <label class="col-md-12 control-label"></label>
                                            <label class="col-md-12 control-label"></label>
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="Val1" CssClass="btn btn-primary custom-button-color"></asp:Button>
                                            <asp:Button ID="btnback" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="overflow: scroll">
                                            <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="No FloorSection Found."
                                                CssClass="table table-condensed table-bordered table-hover table-striped">
                                                <Columns>
                                                    <asp:BoundField DataField="FLR_SEC_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="FloorSection Name">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundField>
                                                     <asp:BoundField DataField="Flr_name" ItemStyle-HorizontalAlign="Left" HeaderText="Floor Name">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="twr_name" ItemStyle-HorizontalAlign="Left" HeaderText="Tower Name">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundField>
                                                     <asp:BoundField DataField="LCM_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="Location Name">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="FLR_SEC_BAY_TYPE" ItemStyle-HorizontalAlign="Left" HeaderText="FloorSectionBayType Name">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CTY_NAME" ItemStyle-HorizontalAlign="Left" HeaderText="City Name">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:ButtonField HeaderText="Status" ItemStyle-HorizontalAlign="Left" CommandName="Status" Text="Button">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="FLR_SEC_STA_ID" ItemStyle-HorizontalAlign="Left" HeaderText="STID">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="FLR_SEC_CODE" ItemStyle-HorizontalAlign="Left" HeaderText="FloorSection Code">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblFlrID" Text='<% #Bind("FLR_SEC_flr_id")%>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript" defer>
        $("#btnSubmit").click(function () {
            $('#lblMsg').text("")
        });

        function refreshSelectpicker() {
            $("#<%=ddlFloorSection.ClientID%>").selectpicker();
            $("#<%=ddlFloor.ClientID%>").selectpicker();
            $("#<%=ddlTower.ClientID%>").selectpicker();
            $("#<%=ddlFloorSectionBayType.ClientID%>").selectpicker();
            $("#<%=ddlLocation.ClientID%>").selectpicker();
            $("#<%=ddlCity.ClientID%>").selectpicker();
            $("#<%=ddlCountry.ClientID%>").selectpicker();
        }
        refreshSelectpicker();

    </script>
</body>
</html>