<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmMASMainMasters.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMASMainMasters"
    Title="Maintenance Masters" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Masters
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
            <tr>
                <td style="width: 10px">
                    <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Service Masters</strong></td>
                <td style="width: 17px">
                    <img alt="" height="27" src="../../images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../images/table_left_mid_bg.gif" style="width: 10px">
                    &nbsp;</td>
                <td align="left">
                    <br />
                    <table id="table2" cellpadding="5" width="100%" border="1" style="border-collapse: collapse">
                        <tr>
                            <td align="left" class="label" style="height: 26px; width: 50%;">
                                &nbsp;<asp:HyperLink ID="hplreqtype" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMasRequestType.aspx">Add Request Type</asp:HyperLink>
                            </td>
                            <td align="left" class="label" style="height: 26px; width: 50%;">
                                &nbsp;<asp:HyperLink ID="hplviewreqtypes" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMasViewRequestType.aspx">View Request Types</asp:HyperLink>
                            </td>
                        </tr>
                        <tr id="trLName" runat="server">
                            <td align="left" class="label" style="height: 26px; width: 50%;">
                                &nbsp;<asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMasServiceType.aspx">Service Type Master </asp:HyperLink></td>
                            <td align="left" class="label" style="height: 26px; width: 50%;">
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Masters/Mas_Webfiles/frmMasService.aspx">Service Master</asp:HyperLink></td>
                        </tr>
                    </table>
                    <br />
                </td>
                <td background="../../images/table_right_mid_bg.gif">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px">
                    <img height="17" src="../../images/table_left_bot_corner.gif" width="9" /></td>
                <td background="../../images/table_bot_mid_bg.gif">
                    <img height="17" src="../../images/table_bot_mid_bg.gif" width="25" /></td>
                <td>
                    <img height="17" src="../../images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
