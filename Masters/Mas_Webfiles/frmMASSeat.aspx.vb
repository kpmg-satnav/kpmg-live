Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager

Partial Class Masters_Mas_Webfiles_frmMASSeat
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If

            obj.Seat_LoadGrid(gvItem)
            If Not Page.IsPostBack Then
                trLName.Visible = False
                rbActions.Items(0).Selected = True
                obj.BindSeat(ddlSeat)
                obj.BindBlock(ddlBlock)
                obj.BindFloor(ddlFloor)
                obj.BindTower(ddlTower)
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASSeat", "Load", exp)
        End Try
    End Sub
    Private Sub cleardata()
        txtSeatcode.Text = String.Empty
        txtSeatName.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlBlock.SelectedIndex = 0
        ddlFloor.SelectedIndex = 0
        ddlTower.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0
        ddlCity.SelectedIndex = 0
        ddlCountry.SelectedIndex = 0
        ddlSeat.SelectedIndex = 0
    End Sub
    Private Sub Insertdata()
        obj.getcode = txtSeatcode.Text
        obj.getname = txtSeatName.Text
        obj.getRemarks = txtRemarks.Text
        If (obj.insertSeat(ddlBlock, ddlFloor, ddlTower, ddlLocation, ddlCity, ddlCountry, Me) > 0) Then
            cleardata()
        End If
        obj.Seat_LoadGrid(gvItem)
    End Sub
    Private Sub Modifydata()
        obj.getname = txtSeatName.Text
        obj.getRemarks = txtRemarks.Text
        If (obj.ModifyBlock(ddlBlock, ddlFloor, ddlTower, ddlLocation, ddlCity, ddlCountry, Me) > 0) Then
            cleardata()
        End If
        obj.Seat_LoadGrid(gvItem)
    End Sub
    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.SelectedIndexChanged
        Try
            Dim i As Integer
            For i = 0 To rbActions.Items.Count - 1
                If rbActions.Items(i).Selected = True Then
                    If rbActions.Items(i).Text = "Add" Then
                        trLName.Visible = False
                        txtSeatcode.ReadOnly = False
                        btnSubmit.Text = "Submit"
                        cleardata()
                    ElseIf rbActions.Items(i).Text = "Modify" Then
                        trLName.Visible = True
                        txtSeatcode.ReadOnly = True
                        btnSubmit.Text = "Modify"
                        obj.BindSeat(ddlSeat)
                        obj.BindBlock(ddlBlock)
                        obj.BindFloor(ddlFloor)
                        obj.BindTower(ddlTower)
                        obj.Bindlocation(ddlLocation)
                        obj.BindCity(ddlCity)
                        obj.BindCountry(ddlCountry)
                        cleardata()
                    End If
                End If
            Next
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASSeat", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.SelectedItem.Text = "Add" Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                If txtSeatcode.Text = String.Empty Or txtSeatName.Text = String.Empty Or txtRemarks.Text = String.Empty Or ddlFloor.SelectedItem.Text = "--Select--" Or ddlTower.SelectedItem.Text = "--Select--" Or ddlLocation.SelectedItem.Text = "--Select--" Or ddlCity.SelectedItem.Text = "--Select--" Or ddlCountry.SelectedItem.Text = "--Select--" Or ddlBlock.SelectedItem.Text = "--Select--" Then
                    PopUpMessage("Please Enter Mandatory fields", Me)
                ElseIf txtRemarks.Text.Length > 500 Then
                    PopUpMessage("Please Enter remarks in less than or equal to 500 characters", Me)
                Else
                    Insertdata()
                End If
            ElseIf rbActions.SelectedItem.Text = "Modify" Then
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If txtSeatcode.Text = String.Empty Or txtSeatName.Text = String.Empty Or txtRemarks.Text = String.Empty Or ddlFloor.SelectedItem.Text = "--Select--" Or ddlTower.SelectedItem.Text = "--Select--" Or ddlLocation.SelectedItem.Text = "--Select--" Or ddlCity.SelectedItem.Text = "--Select--" Or ddlCountry.SelectedItem.Text = "--Select--" Or ddlBlock.SelectedItem.Text = "--Select--" Or ddlSeat.SelectedItem.Text = "--Select--" Then
                    PopUpMessage("Please Enter Mandatory fields", Me)
                ElseIf txtRemarks.Text.Length > 500 Then
                    PopUpMessage("Please Enter remarks in less than or equal to 500 characters", Me)
                Else
                    Modifydata()
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "frmMASSeat", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlSeat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSeat.SelectedIndexChanged
        Try
            If ddlSeat.SelectedItem.Value <> "--Select--" Then
                obj.BindBlock(ddlBlock)
                obj.BindFloor(ddlFloor)
                obj.BindTower(ddlTower)
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
                obj.Seat_SelectedIndex_Changed(ddlSeat, ddlBlock, ddlFloor, ddlTower, ddlLocation, ddlCity, ddlCountry)
                txtSeatcode.Text = obj.getcode
                txtSeatName.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
            Else
                cleardata()
                obj.BindSeat(ddlSeat)
                obj.BindBlock(ddlBlock)
                obj.BindFloor(ddlFloor)
                obj.BindTower(ddlTower)
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASSeat", "ddlSeat_SelectedIndexChanged", exp)
        End Try
    End Sub
    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            gvItem.Columns(2).Visible = True
            gvItem.Columns(3).Visible = True
            obj.Seat_LoadGrid(gvItem)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASSeat", "gvItem_PageIndexChanging", exp)
        End Try
    End Sub
    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                obj.Seat_Rowcommand(gvItem, index)
                obj.Seat_LoadGrid(gvItem)
            End If
            cleardata()
            obj.BindSeat(ddlSeat)
            obj.BindBlock(ddlBlock)
            obj.BindFloor(ddlFloor)
            obj.BindTower(ddlTower)
            obj.Bindlocation(ddlLocation)
            obj.BindCity(ddlCity)
            obj.BindCountry(ddlCountry)
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASSeat", "gvItem_RowCommand", exp)
        End Try
    End Sub

    Protected Sub ddlBlock_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBlock.SelectedIndexChanged
        Try
            If (ddlBlock.SelectedIndex <> 0) Then
                Dim dr As SqlDataReader = obj.seat_getwng_detls(ddlBlock.SelectedValue)
                If (dr.Read) Then
                    ddlFloor.SelectedValue = dr("wng_flr_id")
                    ddlTower.SelectedValue = dr("wng_twr_id")
                    ddlLocation.SelectedValue = dr("wng_lcn_id")
                    ddlCity.SelectedValue = dr("wng_cty_id")
                    ddlCountry.SelectedValue = dr("wng_cry_id")
                End If
            Else
                ddlBlock.SelectedIndex = 0
                ddlFloor.SelectedIndex = 0
                ddlTower.SelectedIndex = 0
                ddlLocation.SelectedIndex = 0
                ddlCity.SelectedIndex = 0
                ddlCountry.SelectedIndex = 0
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASSeat", "ddlBlock_SelectedIndexChanged", exp)
        End Try
    End Sub

End Class
