<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPrjHead.aspx.vb" Inherits="Masters_Mas_Webfiles_frmPrjHead" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amantra</title>
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/bootstrap.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/bootstrap-select.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/amantra.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/font-awesome/css/font-awesome.min.css")%>' rel="stylesheet" />
    <link rel="stylesheet" href='<%= Page.ResolveUrl("~/BootStrapCSS/datepicker.css")%>' />
    

    <script language="javascript" type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

    </script>
</head>
<body>   
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Project </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">

                                        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" Visible="False">
                                            <asp:ListItem>-- Select --</asp:ListItem>
                                            <asp:ListItem>10</asp:ListItem>
                                            <asp:ListItem>20</asp:ListItem>
                                            <asp:ListItem Value="30"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                      <div class="row" style="margin-top: 10px">
                            <div class="col-md-11 pull-center">
                                <div class="form-group">
                                    <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False"  
                                        CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="True" PageSize="30">
                                        <Columns>
                                            <asp:BoundField HeaderText="Employee Name" DataField="AUR_NAME" />
                                            <asp:BoundField HeaderText="Employee ID" DataField="AUR_ID" />
                                        </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/jquery.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap.min.js")%>' type="text/javascript" defer></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-select.min.js")%>' type="text/javascript" defer></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-datepicker.js") %>' type="text/javascript" defer></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/ObjectKeys.js")%>' defer></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/wz_tooltip.js")%>' type="text/javascript" defer></script>
    <script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js" temp_src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js" defer></script>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" temp_src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" temp_src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</body>
</html>
