Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.Configuration.ConfigurationManager
Partial Class frmForgetPwd
    Inherits System.Web.UI.Page

#Region "Variable Declaration"
    Dim strSQL As String
    Dim objDR As SqlDataReader
    Dim strMessage As String = String.Empty
    Dim aur_name As String = String.Empty
    Dim aur_Email As String = String.Empty
    Dim strPassword As String = String.Empty
    Dim msg As String = String.Empty
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Session("TENANT") = tenantID.Text
        Try
            Dim Tenant_Id As String = ""
            If Tenant_Id <> "0" Then
                txtUsrId.Text = txtUsrId.Text.Replace("'", "")
                If txtUsrId.Text <> "" Then
                    Tenant_Id = ValidateTenant(tenantID.Text)
                    Session("TENANT") = Tenant_Id
                    If Tenant_Id <> "0" Then
                        Dim validatecode As String = validateempmail()
                        If validatecode = "1" Then
                            GetForgotPassword()
                        Else
                            lblMsg.Text = "Please Enter Valid Email Id"
                        End If
                    Else
                        lblMsg.Text = "Invalid Company Id!"
                    End If
                Else
                    strMessage = "This is not a valid user id"
                    lblMsg.Text = strMessage
                End If
            Else
                lblMsg.Text = "Invalid Company Id!"
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while Updating data", "Forgot password", "Load", ex)
        End Try
    End Sub

    Public Function ValidateTenant(ByVal Tenant_Name As String) As String
        Dim Valid_Tenant_Id As String = ""
        Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "CHECK_USER_TENANT_ISVALD")
        sp.Command.AddParameter("@TENANT_NAME", Tenant_Name, DbType.String)
        Session("useroffset") = "+05:30"
        Valid_Tenant_Id = sp.ExecuteScalar
        Return Valid_Tenant_Id
    End Function

    Public Function validateempmail() As String
        Dim validatecode As String
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_USER_EMAILID")
        Session("useroffset") = "+05:30"
        sp.Command.AddParameter("@USER_MAIL", txtEmail.Text, DbType.String)
        validatecode = sp.ExecuteScalar
        Return validatecode
    End Function
    Public Sub GetForgotPassword()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_FORGOT_PASSWORD")
        Session("useroffset") = "+05:30"
        sp.Command.AddParameter("@USER_MAIL", txtEmail.Text, DbType.String)
        sp.Command.AddParameter("@USER_ID", txtUsrId.Text, DbType.String)
        sp.ExecuteScalar()
        lblMsg.Text = "Password has been sent to your mail id"
    End Sub

End Class








