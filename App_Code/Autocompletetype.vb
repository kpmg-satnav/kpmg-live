﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration
Imports System.Web.Script.Services

<WebService> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService> _
Public Class Autocompletetype
    Inherits System.Web.Services.WebService
    <WebMethod(EnableSession:=True)>
    <System.Web.Script.Services.ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)>
    Public Function GetDetails(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_EMP_DETAILS_GRID")
        sp.Command.AddParameter("@SEARCH_CRITERIA", prefixText)

        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = sp.GetReader()
        While sdr.Read
            customers.Add(sdr("AUR_KNOWN_AS").ToString)
        End While

        Return customers

    End Function
    <WebMethod(EnableSession:=True)>
    <System.Web.Script.Services.ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)>
    Public Function GetintAttendees(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_INT_ATTENDESS")
        sp.Command.AddParameter("@SEARCH_CRITERIA", prefixText)

        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = sp.GetReader()
        While sdr.Read
            customers.Add(sdr("AUR_EMAIL").ToString)
        End While

        Return customers

    End Function

    <WebMethod(EnableSession:=True)>
    <System.Web.Script.Services.ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)>
    Public Function GetDepartment(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_DEPARTMENT_DETAILS")
        sp.Command.AddParameter("@SEARCH_CRITERIA", prefixText)

        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = sp.GetReader()
        While sdr.Read
            customers.Add(sdr("DEP_NAME").ToString)
        End While

        Return customers

    End Function

    <WebMethod(EnableSession:=True)>
    <System.Web.Script.Services.ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)>
    Public Function GetDesignation(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_DESIGNATION_DETAILS")
        sp.Command.AddParameter("@SEARCH_CRITERIA", prefixText)

        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = sp.GetReader()
        While sdr.Read
            customers.Add(sdr("DEP_NAME").ToString)
        End While

        Return customers

    End Function
    <WebMethod(EnableSession:=True)>
    <System.Web.Script.Services.ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)>
    Public Function GetLocations(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCATION_DETAILS")
        sp.Command.AddParameter("@SEARCH_CRITERIA", prefixText)

        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = sp.GetReader()
        While sdr.Read
            customers.Add(sdr("LCM_NAME").ToString)
        End While

        Return customers

    End Function
End Class
'</webmethod()>