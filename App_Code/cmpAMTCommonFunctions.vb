Imports System.Data.SqlClient
Imports System.Data
Imports System.ApplicationException
Imports System.Configuration.ConfigurationManager
Imports System.Web
Imports System.Web.UI.WebControls


Public Module cmpAMTCommonFunctions
    Public strSQL, strSQL1, str As String
    Public ObjDR As SqlDataReader
    Public ObjCon, objcon1 As New SqlConnection()
    Public ObjComm, objcomm1 As New SqlCommand()
    Public Sub BindGrid(ByVal strqry As String, ByRef gdGrid As GridView, ByVal ParamArray params() As SqlParameter)
        If (strqry.Contains("select")) Then
            gdGrid.DataSource = SqlHelper.ExecuteDataset(CommandType.Text, strqry)
        Else
            gdGrid.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, strqry, params)
        End If
        gdGrid.DataBind()
    End Sub

    Public Sub BindGrid(ByVal strqry As String, ByRef gdGrid As GridView)
        If (strqry.Contains("select")) Then
            gdGrid.DataSource = SqlHelper.ExecuteDataset(CommandType.Text, strqry)
        Else
            gdGrid.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, strqry)
        End If
        gdGrid.DataBind()
    End Sub

    Public Sub BindDetailsView(ByVal strqry As String, ByRef gdGrid As DetailsView)
        If (strqry.Contains("select")) Then
            gdGrid.DataSource = SqlHelper.ExecuteDataset(CommandType.Text, strqry)
        Else
            gdGrid.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, strqry)
        End If
        gdGrid.DataBind()
    End Sub
    Public Sub BindGridSet(ByVal strqry As String, ByRef gdGrid As GridView)
        If (strqry.Contains("select")) Then
            gdGrid.DataSource = SqlHelper.ExecuteDataset(CommandType.Text, strqry)
        Else
            gdGrid.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, strqry)
        End If
        gdGrid.DataBind()
    End Sub

    Public Function DataReader(ByVal strSQL As String, ByVal ParamArray params() As SqlParameter) As SqlDataReader
        If (strSQL.Contains("select") Or strSQL.Contains("SELECT")) Then
            DataReader = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        Else
            DataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, strSQL, params)
        End If
    End Function
    Public Function DataReader(ByVal strSQL As String) As SqlDataReader
        DataReader = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
    End Function

    Public Function ExecuteQry(ByVal strsql As String) As Boolean
        Return SqlHelper.ExecuteNonQuery(CommandType.Text, strsql)
    End Function
    Public Sub BindCombos(ByVal strqry As String, ByRef cboCombo As DropDownList, ByVal txtField As String, ByVal valField As String)
        cboCombo.DataSource = SqlHelper.ExecuteReader(CommandType.Text, strqry)
        cboCombo.DataTextField = txtField
        cboCombo.DataValueField = valField
        cboCombo.DataBind()
        cboCombo.Items.Insert(0, "--Select--")
    End Sub

    Public Sub BindComboXtra(ByVal strqry As String, ByRef cboCombo As DropDownList, ByVal txtField As String, ByVal valField As String)

        cboCombo.DataSource = SqlHelper.ExecuteReader(CommandType.Text, strqry)
        cboCombo.DataTextField = txtField
        cboCombo.DataValueField = valField
        cboCombo.DataBind()
        cboCombo.Items.Insert(0, "--Select--")
        cboCombo.Items.Insert(1, "--All--")
        cboCombo.Items.Insert(2, "--New--")

    End Sub
    Public Sub BindComboAddNew(ByVal strqry As String, ByRef cboCombo As DropDownList, ByVal txtField As String, ByVal valField As String)

        cboCombo.DataSource = SqlHelper.ExecuteReader(CommandType.Text, strqry)
        cboCombo.DataTextField = txtField
        cboCombo.DataValueField = valField
        cboCombo.DataBind()
        cboCombo.Items.Insert(0, "--Add New--")

    End Sub
    Public Function ExecuteDataset(ByVal connection As SqlConnection, _
                                                    ByVal commandText As String) As DataSet
        Dim cmd As New SqlCommand
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        cmd.Connection = connection
        cmd.CommandText = commandText

        da = New SqlDataAdapter(cmd)

        'fill the DataSet using default values for Datatable names, etc.
        da.Fill(ds)
        da.Dispose()
        cmd.Parameters.Clear()
        Return ds

    End Function


    Public Sub ClearCombos(ByVal ParamArray cboSrc() As DropDownList)
        Dim iLoop As DropDownList
        For Each iLoop In cboSrc
            iLoop.Items.Clear()
            iLoop.Items.Insert(0, "--Select--")
            iLoop.SelectedIndex = 0
        Next
        'iLoop.Dispose()
    End Sub
    Public Sub LoadDetails(ByVal str As String, ByVal cboDest As DropDownList, Optional ByVal cboSrc As DropDownList = Nothing, Optional ByVal iId As Integer = Nothing)

        Dim strTxtFld As String = String.Empty
        Dim strValFld As String = String.Empty

        Select Case str
            Case "Country"
                strSQL = "SELECT DISTINCT CNY_ID,CNY_NAME FROM  " & HttpContext.Current.Session("TENANT") & "." & "AMT_CNTTOLVL_VW ORDER BY CNY_NAME"
                strTxtFld = "CNY_NAME"
                strValFld = "CNY_ID"

            Case "Region"
                strSQL = "SELECT DISTINCT RGN_NAME,RGN_ID FROM  " & HttpContext.Current.Session("TENANT") & "." & "AMT_CNTTOLVL_VW WHERE CNY_ID=" & cboSrc.SelectedItem.Value _
                & " ORDER BY RGN_NAME"
                strTxtFld = "RGN_NAME"
                strValFld = "RGN_ID"

            Case "State"
                strSQL = "Select DISTINCT STE_ID,STE_NAME FROM  " & HttpContext.Current.Session("TENANT") & "." & "AMT_CNTTOLVL_VW WHERE RGN_ID=" & cboSrc.SelectedItem.Value _
               & " ORDER BY STE_NAME"
                strTxtFld = "STE_NAME"
                strValFld = "STE_ID"

            Case "City"
                strSQL = "Select DISTINCT CTY_NAME,CTY_ID FROM  " & HttpContext.Current.Session("TENANT") & "." & "AMT_CNTTOLVL_VW WHERE CNY_ID=" & cboSrc.SelectedItem.Value _
                & " ORDER BY CTY_NAME"
                strTxtFld = "CTY_NAME"
                strValFld = "CTY_ID"

            Case "Location"
                strSQL = "Select DISTINCT LOC_ID,LOC_NAME FROM  " & HttpContext.Current.Session("TENANT") & "." & "AMT_CNTTOLVL_VW WHERE CTY_ID=" & cboSrc.SelectedItem.Value _
               & " ORDER BY LOC_NAME"
                strTxtFld = "LOC_NAME"
                strValFld = "LOC_ID"

            Case "Building"
                strSQL = "select DISTINCT BDG_ID,BDG_NAME FROM  " & HttpContext.Current.Session("TENANT") & "." & "BUILDING WHERE BDG_STA_ID=1 and BDG_CTY_ID=" & cboSrc.SelectedItem.Value & " ORDER BY BDG_DESC"
                strTxtFld = "BDG_NAME"
                strValFld = "BDG_ID"

            Case "Tower"
                strSQL = "Select DISTINCT TWR_NAME, TWR_ID FROM  " & HttpContext.Current.Session("TENANT") & "." & "Tower ORDER BY TWR_NAME"
                strTxtFld = "TWR_NAME"
                strValFld = "TWR_ID"

            Case "Towers"
                strSQL = "Select DISTINCT TWR_NAME,TWR_ID FROM  " & HttpContext.Current.Session("TENANT") & "." & "Tower WHERE TWR_BDG_ID=" & cboSrc.SelectedItem.Value & " ORDER BY TWR_NAME"
                strTxtFld = "TWR_NAME"
                strValFld = "TWR_ID"

            Case "Floor"
                strSQL = "Select DISTINCT FLR_NAME, FLR_CODE FROM  " & HttpContext.Current.Session("TENANT") & "." & "Floor ORDER BY FLR_NAME"
                strTxtFld = "FLR_NAME"
                strValFld = "FLR_CODE"

            Case "Wing"
                strSQL = "Select DISTINCT WNG_NAME,WNG_ID FROM  " & HttpContext.Current.Session("TENANT") & "." & "Wing ORDER BY WNG_NAME"
                strTxtFld = "WNG_NAME"
                strValFld = "WNG_ID"


            Case "Level"
                strSQL = "Select DISTINCT LVL_NAME,LVL_ID FROM  " & HttpContext.Current.Session("TENANT") & "." & "LEVELS ORDER BY LVL_NAME"
                strTxtFld = "LVL_NAME"
                strValFld = "LVL_ID"


            Case "Vendor"
                strSQL = "Select DISTINCT AVR_NAME,AVR_ID FROM " & HttpContext.Current.Session("TENANT") & "." & "AMT_VENDOR ORDER BY AVR_NAME"
                strTxtFld = "AVR_NAME"
                strValFld = "AVR_ID"

            Case "Cat"
                strSQL = "Select DISTINCT ACY_ID,ACY_NAME FROM " & HttpContext.Current.Session("TENANT") & "." & "AMT_CATEGORY ORDER BY ACY_NAME"
                strTxtFld = "ACY_NAME"
                strValFld = "ACY_ID"

            Case "Channel"
                strSQL = "Select DISTINCT ACL_ID,ACL_NAME FROM " & HttpContext.Current.Session("TENANT") & "." & "AMT_CHANNEL ORDER BY ACL_NAME"
                strTxtFld = "ACL_NAME"
                strValFld = "ACL_ID"

            Case "SubCat"
                strSQL = "Select DISTINCT ASC_NAME,ASC_ID FROM " & HttpContext.Current.Session("TENANT") & "." & "AMT_SUB_CATEGORY WHERE " _
                & " ASC_ACY_ID=" & cboSrc.SelectedItem.Value & " ORDER BY ASC_NAME"
                strTxtFld = "ASC_NAME"
                strValFld = "ASC_ID"

        End Select

        Dim objds As New DataSet
        objds = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
        cboDest.Items.Clear()
        cboDest.DataSource = objds
        cboDest.DataTextField = strTxtFld
        cboDest.DataValueField = strValFld
        cboDest.DataBind()
        cboDest.Items.Insert(0, "--Select--")
        cboDest.SelectedIndex = 0
        objds.Dispose()

    End Sub

End Module