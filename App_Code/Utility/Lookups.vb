#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports SubSonic
Imports System.Web

Namespace Commerce.Common

    Public Class Lookups
        ''' <summary>
        ''' This is a simple function to use with lookup tables
        ''' that are not represented with a business object.
        ''' </summary>
        ''' <param name="tableName">Name of the table to query. All results will be returned</param>
        ''' <returns>IDataReader</returns>
        Private Sub New()
        End Sub
        Public Shared Function GetList(ByVal tableName As String) As IDataReader

            Return New Query(HttpContext.Current.Session("TENANT").ToString() & "." & tableName).ExecuteReader()

        End Function

        ''' <summary>
        ''' This is a simple function to use with lookup tables
        ''' that are not represented with a business object.
        ''' You can supply a parameter name and value
        ''' </summary>
        ''' <param name="tableName">The name of the table</param>
        ''' <param name="paramField">The column to use </param>
        ''' <param name="paramValue">The column value</param>
        ''' <returns></returns>
        Public Shared Function GetListByIntParam(ByVal tableName As String, ByVal paramField As String, ByVal paramValue As Integer) As IDataReader
            Return New Query(HttpContext.Current.Session("TENANT").ToString() & "." & tableName).AddWhere(paramField, paramValue).ExecuteReader()

        End Function
        ''' <summary>
        ''' This is a simple function to use with lookup tables
        ''' that are not represented with a business object.
        ''' You can supply a parameter name and value
        ''' </summary>
        ''' <param name="tableName">The name of the table</param>
        ''' <param name="paramField">The column to use </param>
        ''' <param name="paramValue">The column value</param>
        ''' <returns></returns>
        Public Shared Function GetListByStringParam(ByVal tableName As String, ByVal paramField As String, ByVal paramValue As String) As IDataReader
            Return New Query(HttpContext.Current.Session("TENANT").ToString() & "." & tableName).AddWhere(paramField, paramValue).ExecuteReader()

        End Function

        Public Shared Sub QuickAdd(ByVal tableName As String, ByVal paramField As String, ByVal paramValue As String)

            Dim cmd As QueryCommand = New QueryCommand("INSERT INTO " & HttpContext.Current.Session("TENANT").ToString() & "." & tableName & " (" & paramField & ") VALUES(@p1) ")
            cmd.AddParameter("@p1", paramValue)
            DataService.ExecuteQuery(cmd)

        End Sub

    End Class
End Namespace
