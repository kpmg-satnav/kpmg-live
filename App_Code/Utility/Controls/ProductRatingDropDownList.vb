#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Namespace Commerce.Web.UI.Controls
	''' <summary>
	''' Summary description for ProductRatingDropDownList
	''' </summary>
	Public Class ProductRatingDropDownList
		Inherits DropDownList
		Public Sub New()
			Items.Add(New ListItem("-", "0"))
			Items.Add(New ListItem("5 Stars", "5"))
			Items.Add(New ListItem("4 Stars", "4"))
			Items.Add(New ListItem("3 Stars", "3"))
			Items.Add(New ListItem("2 Stars", "2"))
			Items.Add(New ListItem("1 Star", "1"))
		End Sub
	End Class
End Namespace
