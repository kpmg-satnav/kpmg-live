#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common

Namespace Commerce.Web.UI.Controls
	''' <summary>
	''' Summary description for RatingImage
	''' </summary>
	Public Class RatingImage
		Inherits PlaceHolder
		Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
			Dim image As HtmlImage = New HtmlImage()
			image.Border = 0

			SetRatingImage(image)

			image.RenderControl(writer)
		End Sub

		#Region "Properties"

		''' <summary>
		''' Rating image path.
		''' </summary>
		Private imagePath_Renamed As String = Nothing
		Public Property ImagePath() As String
			Get
				If Utility.IsNullOrEmpty(imagePath_Renamed) Then
					imagePath_Renamed = Utility.GetSiteRoot() & "/images/rating/"
				End If

				If imagePath_Renamed.EndsWith("/") Then
					Return (imagePath_Renamed)
				Else
					Return (String.Format("{0}{1}", imagePath_Renamed, "/"))
				End If
			End Get
			Set
				imagePath_Renamed = Value
			End Set
		End Property

		''' <summary>
		''' Rating value.
		''' </summary>
		Private rating_Renamed As Double = 0.0
		Public Property Rating() As Double
			Get
				If Not ViewState Is Nothing AndAlso Not ViewState("Rating") Is Nothing Then
					rating_Renamed = CDbl(ViewState("Rating"))
				End If

				Return rating_Renamed
			End Get
			Set
				rating_Renamed = Value

				If Not ViewState Is Nothing Then
					ViewState("Rating") = rating_Renamed
				End If
			End Set
		End Property

		Private totalVotes_Renamed As Integer = 0
		Public Property TotalVotes() As Integer
			Get
				If Not ViewState Is Nothing AndAlso Not ViewState("TotalVotes") Is Nothing Then
					totalVotes_Renamed = CInt(Fix(ViewState("TotalVotes")))
				End If

				Return totalVotes_Renamed

			End Get
			Set
				totalVotes_Renamed = Value

				If Not ViewState Is Nothing Then
					ViewState("TotalVotes") = totalVotes_Renamed
				End If
			End Set
		End Property

		Private showTooltip_Renamed As Boolean = False
		Public Property ShowTooltip() As Boolean
			Get
				If Not ViewState Is Nothing AndAlso Not ViewState("ShowTooltip") Is Nothing Then
					showTooltip_Renamed = CBool(ViewState("ShowTooltip"))
				End If

				Return showTooltip_Renamed
			End Get
			Set
				showTooltip_Renamed = Value

				If Not ViewState Is Nothing Then
					ViewState("ShowTooltip") = showTooltip_Renamed
				End If
			End Set
		End Property

		#End Region

		#Region "Helpers"

		''' <summary>
		''' Formats rating image path.
		''' </summary>
		Protected Function BuildImagePath(ByVal imageName As String) As String
			Return String.Format("{0}{1}", ImagePath, imageName)
		End Function

		''' <summary>
		''' Selects the right rating image to be displayed.
		''' </summary>
		Protected Sub SetRatingImage(ByRef ratingImage As HtmlImage)
			If ratingImage Is Nothing Then
				Return
			End If

			Dim text As String = ""

			If Rating <= 0.25 Then
				ratingImage.Src = BuildImagePath("star_0.gif")
				text = "Terrible"
			ElseIf Rating <= 0.5 Then
				ratingImage.Src = BuildImagePath("star_0h.gif")
				text = "Terrible"
			ElseIf Rating <= 1 Then
				ratingImage.Src = BuildImagePath("star_1.gif")
				text = "Poor"
			ElseIf Rating <= 1.5 Then
				ratingImage.Src = BuildImagePath("star_1h.gif")
				text = "Poor"
			ElseIf Rating <= 2 Then
				ratingImage.Src = BuildImagePath("star_2.gif")
				text = "Fair"
			ElseIf Rating <= 2.5 Then
				ratingImage.Src = BuildImagePath("star_2h.gif")
				text = "Fair"
			ElseIf Rating <= 3 Then
				ratingImage.Src = BuildImagePath("star_3.gif")
				text = "Average"
			ElseIf Rating <= 3.5 Then
				ratingImage.Src = BuildImagePath("star_3h.gif")
				text = "Average"
			ElseIf Rating <= 4 Then
				ratingImage.Src = BuildImagePath("star_4.gif")
				text = "Good"
			ElseIf Rating <= 4.5 Then
				ratingImage.Src = BuildImagePath("star_4h.gif")
				text = "Good"
			Else
				ratingImage.Src = BuildImagePath("star_5.gif")
				text = "Excellent"
			End If

			If (Not ShowTooltip) Then
				Return
			End If

			If Rating = 0 Then
				ratingImage.Alt = "Not rated"
				'ratingImage.Alt = string.Format("Rated {0} [{1} out of 5]", text, "0");
			Else
				ratingImage.Alt = String.Format("Rated {0} [{1} out of 5 / rated {2} time(s)]", text, Rating.ToString("#.##"), TotalVotes)
			End If
		End Sub

		#End Region
	End Class
End Namespace
