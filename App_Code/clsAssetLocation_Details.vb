Imports Microsoft.VisualBasic

Public Class clsAssetLocation_Details
    Private mASTCOUNT As String

    Public Property ASTCOUNT() As String
        Get
            Return mASTCOUNT
        End Get
        Set(ByVal value As String)
            mASTCOUNT = value
        End Set
    End Property
    Private mLCM_NAME As String

    Public Property LCM_NAME() As String
        Get
            Return mLCM_NAME
        End Get
        Set(ByVal value As String)
            mLCM_NAME = value
        End Set
    End Property
    Private mPRODUCTNAME As String

    Public Property PRODUCTNAME() As String
        Get
            Return mPRODUCTNAME
        End Get
        Set(ByVal value As String)
            mPRODUCTNAME = value
        End Set
    End Property
    Private mPRODUCTID As String

    Public Property PRODUCTID() As String
        Get
            Return mPRODUCTID
        End Get
        Set(ByVal value As String)
            mPRODUCTID = value
        End Set
    End Property
    Private mLCM_CODE As String

    Public Property LCM_CODE() As String
        Get
            Return mLCM_CODE
        End Get
        Set(ByVal value As String)
            mLCM_CODE = value
        End Set
    End Property
End Class
