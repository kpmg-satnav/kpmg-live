Imports Microsoft.VisualBasic

Public Class clsAssetsforStockUpdate
    Private mAIM_CODE As String

    Public Property AIM_CODE() As String
        Get
            Return mAIM_CODE
        End Get
        Set(ByVal value As String)
            mAIM_CODE = value
        End Set
    End Property
    Private mAIM_NAME As String

    Public Property AIM_NAME() As String
        Get
            Return mAIM_NAME
        End Get
        Set(ByVal value As String)
            mAIM_NAME = value
        End Set
    End Property
End Class
