Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Web.SessionState.HttpSessionState
Imports System.IO
Imports System.Security.Cryptography
Imports System.Web.HttpApplicationState
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Web.UI
Imports System.Web


Public Class clsSubSonicCommonFunctions

#Region "DropDown Functions"

    'Public sub Binddropdown(ByRef ddl As DropDownList, ByVal SP_Name As String, ByVal TxtField As String, ByVal ValField As String, ByVal param As SqlParameter()) 
    'added by praveen changed and added retrun type for flag on 13/02/2014
    Public Function Binddropdown(ByRef ddl As DropDownList, ByVal SP_Name As String, ByVal TxtField As String, ByVal ValField As String, ByVal param As SqlParameter()) As Integer
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SP_Name)
        For i As Integer = 0 To param.Length - 1
            sp.Command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next

        Dim reader As IDataReader = sp.GetReader()
        ddl.Items.Insert(0, "--Select--")
        While (reader.Read())
            Dim listitem = New ListItem()
            listitem.Text = reader.GetValue(reader.GetOrdinal(TxtField))
            listitem.Value = reader.GetValue(reader.GetOrdinal(ValField))
            ddl.Items.Add(listitem)
        End While


        'Dim ds As New DataSet
        'ds = sp.GetDataSet
        Dim flag As Integer = 0
        'If ds.Tables(0).Rows.Count > 0 Then
        '    ddl.DataSource = ds
        '    ddl.DataTextField = TxtField
        '    ddl.DataValueField = ValField
        '    ddl.DataBind()
        '    ddl.Items.Insert(0, "--Select--")
        '    flag = 1
        'Else
        '    flag = 0
        'End If
        Return flag
    End Function

    Public Sub Binddropdown(ByRef cboCombo As DropDownList, ByVal SP_Name As String, ByVal TxtField As String, ByVal ValField As String)
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SP_Name)
        Dim dummy As String = sp.Command.CommandSql

        'For i As Integer = 0 To Param.Length - 1
        '    sp.Command.Parameters.Add(Param(i).ParameterName, Param(i).Value)
        'Next

        Dim reader As IDataReader = sp.GetReader()
        cboCombo.Items.Insert(0, "--Select--")
        While (reader.Read())
            Dim listitem = New ListItem()
            listitem.Text = reader.GetValue(reader.GetOrdinal(TxtField))
            listitem.Value = reader.GetValue(reader.GetOrdinal(ValField))
            cboCombo.Items.Add(listitem)
        End While

        'Dim ds As New DataSet
        'ds = sp.GetDataSet
        'cboCombo.DataSource = ds
        'cboCombo.DataTextField = TxtField
        'cboCombo.DataValueField = ValField
        'cboCombo.DataBind()
        'cboCombo.Items.Insert(0, "--Select--")

    End Sub



    Public Sub BindListBox(ByRef lstBox As ListBox, ByVal SP_Name As String, ByVal TxtField As String, ByVal ValField As String)
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SP_Name)
        Dim dummy As String = sp.Command.CommandSql

        Dim ds As New DataSet
        ds = sp.GetDataSet
        lstBox.DataSource = ds
        lstBox.DataTextField = TxtField
        lstBox.DataValueField = ValField
        lstBox.DataBind()


    End Sub


    Public Sub BindListBox(ByRef lstBox As ListBox, ByVal SP_Name As String, ByVal TxtField As String, ByVal ValField As String, ByVal param As SqlParameter())
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SP_Name)
        For i As Integer = 0 To param.Length - 1
            sp.Command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next
        Dim ds As New DataSet
        ds = sp.GetDataSet
        lstBox.DataSource = ds
        lstBox.DataTextField = TxtField
        lstBox.DataValueField = ValField
        lstBox.DataBind()

    End Sub

#End Region

    Dim strsql As String
    Public Sub BindGridView(ByRef gv As GridView, ByVal SP_Name As String)
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SP_Name)

        Dim ds As New DataSet
        ds = sp.GetDataSet
        gv.DataSource = ds
        gv.DataBind()
    End Sub
    Public Sub BindDataGrid(ByRef gv As DataGrid, ByVal SP_Name As String)
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SP_Name)

        Dim ds As New DataSet
        ds = sp.GetDataSet
        gv.DataSource = ds
        gv.DataBind()
    End Sub
    Public Sub BindDataGrid(ByRef gv As DataGrid, ByVal SP_Name As String, ByVal param As SqlParameter())
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SP_Name)
        For i As Integer = 0 To param.Length - 1
            sp.Command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gv.DataSource = ds
        gv.DataBind()
    End Sub
    Public Sub BindGridViewGET_PLAN_DETAILS(ByRef gv As GridView, ByVal SP_Name As String, ByVal param As SqlParameter())
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SP_Name)
        For i As Integer = 0 To param.Length - 1
            sp.Command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next
        '  
        Dim ds As New DataSet
        ds = sp.GetDataSet
        Dim dt1 As New DataTable
        dt1 = ds.Tables(0)
        Dim dt2 As New DataTable
        dt2 = ds.Tables(1)
        dt1.Merge(dt2)

        ds.Merge(dt1)
        gv.DataSource = ds
        gv.DataBind()
    End Sub
    Public Sub BindDataGridGET_PLAN_DETAILS(ByRef gv As DataGrid, ByVal SP_Name As String, ByVal param As SqlParameter())
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SP_Name)
        For i As Integer = 0 To param.Length - 1
            sp.Command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next
        '  
        Dim ds As New DataSet
        ds = sp.GetDataSet
        Dim dt1 As New DataTable
        dt1 = ds.Tables(0)
        Dim dt2 As New DataTable
        dt2 = ds.Tables(1)
        dt1.Merge(dt2)

        ds.Merge(dt1)
        gv.DataSource = ds
        gv.DataBind()
    End Sub
    Public Sub BindGridView(ByRef gv As GridView, ByVal SP_Name As String, ByVal param As SqlParameter())
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SP_Name)
        For i As Integer = 0 To param.Length - 1
            sp.Command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next
        Dim ds As New DataSet
        ds = sp.GetDataSet
        HttpContext.Current.Session("dataset") = ds
        If ds.Tables.Count > 0 Then
            gv.DataSource = ds
            gv.DataBind()
        Else
            gv.DataSource = Nothing
            gv.DataBind()
        End If

    End Sub

    Public Function GetSubSonicDataReader(ByVal SPName As String)
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SPName)
        Return sp.GetReader()
    End Function

    Public Function GetSubSonicDataReader(ByVal SPName As String, ByVal param As SqlParameter())
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SPName)
        For i As Integer = 0 To param.Length - 1
            sp.Command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next
        Return sp.GetReader()
    End Function

    Public Function GetSubSonicDataSet(ByVal SPName As String, ByVal param As SqlParameter())
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SPName)
        'Dim sp As New SubSonic.StoredProcedure("T1" & "." & SPName)
        For i As Integer = 0 To param.Length - 1
            sp.Command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next
        Return sp.GetDataSet()
    End Function

    Public Function GetSubSonicDataSet(ByVal SPName As String)
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SPName)
        Return sp.GetDataSet()
    End Function
    Public Function GetSubSonicExecute(ByVal SPName As String, ByVal param As SqlParameter())
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SPName)
        For i As Integer = 0 To param.Length - 1
            sp.Command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next
        Return sp.ExecuteScalar()
    End Function

    Public Function GetSubSonicExecuteScalar(ByVal SPName As String, ByVal param As SqlParameter())
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SPName)
        For i As Integer = 0 To param.Length - 1
            sp.Command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next
        Return sp.ExecuteScalar()
    End Function

    Public Sub SubSonicExecuteScalar(ByVal SPName As String, ByVal param As SqlParameter())
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & SPName)
        For i As Integer = 0 To param.Length - 1
            sp.Command.Parameters.Add(param(i).ParameterName, param(i).Value)
        Next
        sp.ExecuteScalar()
    End Sub

    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ/" & RID
        Return RID
    End Function

#Region "Invoke Popup Message"
    Public Shared Sub PopUpMessage(ByVal strText As String, ByVal page As Page)
        ScriptManager.RegisterStartupScript(page, page.[GetType](), "ajaxscript", "alert('" & strText & "')", True)
        Return
    End Sub
    Public Shared Sub PopUpMessageNormal(ByVal strText As String, ByVal page As Page)
        Dim strScript As String = String.Empty
        strScript = "<script language='javascript'>alert('" & strText & "')</script>"
        page.ClientScript.RegisterStartupScript(page.[GetType](), "strScript", strScript)

        Return
    End Sub
#End Region





#Region " REQUEST ID GENARATION(Req)"
    Public Function REQGENARATION_REQ(ByVal Strfield As String, ByVal countfield As String, ByVal DataBase As String, ByVal Table As String, Optional ByVal Wherecondiction As String = "1=1", Optional ByVal field1 As String = "0", Optional ByVal field2 As String = "0") As String
        Dim TmpReqseqid As String
        Dim Reqseqid As Integer

        strsql = "SELECT isnull(MAX(" & countfield & "),0)+1 FROM " & DataBase + Table & " where " & Wherecondiction
        Reqseqid = SqlHelper.ExecuteScalar(CommandType.Text, strsql)

        If Reqseqid < 10 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/000000" & Reqseqid
        ElseIf Reqseqid < 100 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/00000" & Reqseqid
        ElseIf Reqseqid < 1000 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/0000" & Reqseqid
        ElseIf Reqseqid < 10000 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/000" & Reqseqid
        ElseIf Reqseqid < 100000 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/00" & Reqseqid
        ElseIf Reqseqid < 1000000 Then
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/0" & Reqseqid
        Else
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year & "/" & Strfield & "/" & Reqseqid
        End If
        Return TmpReqseqid
    End Function
#End Region




End Class
