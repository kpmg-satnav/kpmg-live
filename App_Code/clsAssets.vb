Imports Microsoft.VisualBasic

Public Class clsAssets
    Private mPRODUCTNAME As String

    Public Property PRODUCTNAME() As String
        Get
            Return mPRODUCTNAME
        End Get
        Set(ByVal value As String)
            mPRODUCTNAME = value
        End Set
    End Property
    Private mPRODUCTID As String

    Public Property PRODUCTID() As String
        Get
            Return mPRODUCTID
        End Get
        Set(ByVal value As String)
            mPRODUCTID = value
        End Set
    End Property
End Class
