Imports Microsoft.VisualBasic
Imports System.Security.Cryptography
Imports System.Text
Imports System

Public Class clsSecurity
    Public Shared Function Encrypt(ByVal strToEncrypt As String) As String
        Try
            'Return Encrypt(strToEncrypt, "A-mantraMD5ForAmantraAxis")
            Return strToEncrypt
        Catch ex As Exception
            Return "Wrong Input. " + ex.Message
        End Try
    End Function
    Public Shared Function Decrypt(ByVal strEncrypted As String) As String
        Try
            'Return Decrypt(strEncrypted, "A-mantraMD5ForAmantraAxis")
            Return strEncrypted
        Catch ex As Exception
            Return "Wrong Input. " + ex.Message
        End Try
    End Function
    Public Shared Function Encrypt1(ByVal stringToEncrypt As String) As String
        Dim sEncryptionKey As String = "!#$a54?3"
        Dim key As Byte() = {}
        Dim IV As Byte() = {10, 20, 30, 40, 50, 60, _
        70, 80}
        Dim inputByteArray As Byte()
        'Convert.ToByte(stringToEncrypt.Length) 
        Try
            key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8))
            Dim des As New DESCryptoServiceProvider()
            inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt)
            Dim ms As New IO.MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch ex As System.Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function Decrypt1(ByVal stringToDecrypt As String) As String
        Dim sEncryptionKey As String = "!#$a54?3"
        Dim key As Byte() = {}
        Dim IV As Byte() = {10, 20, 30, 40, 50, 60, _
        70, 80}
        'Private IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF} 
        'if (stringToDecrypt.Contains(" ")) 
        stringToDecrypt = stringToDecrypt.Replace(" "c, "+"c)
        Dim inputByteArray As Byte() = New Byte(stringToDecrypt.Length - 1) {}
        Try
            key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8))
            Dim des As New DESCryptoServiceProvider()
            inputByteArray = Convert.FromBase64String(stringToDecrypt)

            Dim ms As New IO.MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()

            Dim encoding__1 As Encoding = Encoding.UTF8
            Return encoding__1.GetString(ms.ToArray())
        Catch ex As System.Exception
            Return ex.Message
        End Try
    End Function
    'Public Shared Function Encrypt(ByVal strToEncrypt As String, ByVal strKey As String) As String
    '    Try
    '        Dim objDESCrypto As New TripleDESCryptoServiceProvider()
    '        Dim objHashMD5 As New MD5CryptoServiceProvider()
    '        Dim byteHash As Byte(), byteBuff As Byte()
    '        Dim strTempKey As String = strKey
    '        byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey))
    '        objHashMD5 = Nothing
    '        objDESCrypto.Key = byteHash
    '        objDESCrypto.Mode = CipherMode.ECB
    '        'CBC, CFB 
    '        byteBuff = ASCIIEncoding.ASCII.GetBytes(strToEncrypt)
    '        Return Convert.ToBase64String(objDESCrypto.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length))
    '    Catch ex As Exception
    '        Return "Wrong Input. " + ex.Message
    '    End Try
    'End Function
    'Public Shared Function Decrypt(ByVal strEncrypted As String, ByVal strKey As String) As String
    '    Try
    '        Dim objDESCrypto As New TripleDESCryptoServiceProvider()
    '        Dim objHashMD5 As New MD5CryptoServiceProvider()
    '        Dim byteHash As Byte(), byteBuff As Byte()
    '        Dim strTempKey As String = strKey
    '        byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey))
    '        objHashMD5 = Nothing
    '        objDESCrypto.Key = byteHash
    '        objDESCrypto.Mode = CipherMode.ECB
    '        'CBC, CFB 
    '        byteBuff = Convert.FromBase64String(strEncrypted)
    '        Dim strDecrypted As String = ASCIIEncoding.ASCII.GetString(objDESCrypto.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length))
    '        objDESCrypto = Nothing
    '        Return strDecrypted
    '    Catch ex As Exception
    '        Return "Wrong Input. " + ex.Message
    '    End Try
    'End Function
    Public Shared Function checkPassword(ByVal strPassword As String) As Boolean
        Dim intNCount As Int16 = 0
        Dim intSCount As Int16 = 0
        Dim intUpper As Int16 = 0
        Dim strSpecial As String = "!@#$%^&*()-_{}[];':,./<>?"""
        ' 1st condition Minimum of 8chars
        If strPassword.Length < 8 Then
            Return False
        End If
        For Each c As Char In strPassword
            If Char.IsDigit(c) Then
                intNCount = intNCount + 1
            End If
            If strSpecial.Contains(c) Then
                intSCount = intSCount + 1
            End If
            If Char.IsUpper(c) Then
                intUpper = intUpper + 1
            End If
        Next
        ' 2nd condition Minimum of 2 Numerics
        If intNCount < 2 Then
            Return False
        End If
        ' 3rd condition Minimum of 1 Speciacl char
        If intSCount < 1 Then
            Return False
        End If
        '4th condition Minimum of 1 Uppercase char
        If intUpper < 1 Then
            Return False
        End If
        Return True
    End Function

    Public Shared Function UniCodeEncrypt(ByVal data As Byte(), ByVal parameters As RSAParameters) As Byte()
        Dim provider As New RSACryptoServiceProvider()
        provider.ImportParameters(parameters)
        Return provider.Encrypt(data, False)
    End Function

    Public Shared Function UniCodeDecrypt(ByVal data As Byte(), ByVal parameters As RSAParameters) As Byte()
        Dim provider As New RSACryptoServiceProvider()
        provider.ImportParameters(parameters)
        Return provider.Decrypt(data, False)
    End Function

End Class
