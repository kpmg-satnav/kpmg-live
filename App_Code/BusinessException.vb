Imports System
Imports System.Collections.Generic
Imports System.Text

Namespace Amantra.Exception
    Public Class BusinessException
        Inherits BaseException

        Public Sub New(ByVal message As String, ByVal className As String, ByVal methodName As String, ByVal objException As System.Exception)
            MyBase.New(message, className, methodName, objException)
        End Sub
    End Class
End Namespace