#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

''' <summary>
''' Summary description for PayPalProSettings
''' </summary>
Public Class PayPalProSettings
	Inherits ConfigurationSection

  <ConfigurationProperty("apiUserName", DefaultValue := "")> _
  Public Property APIUserName() As String
	Get
	  Return CStr(MyBase.Item("apiUserName"))
	End Get
	Set
	  MyBase.Item("apiUserName") = Value
	End Set
  End Property
  <ConfigurationProperty("apiPassword", DefaultValue := "")> _
  Public Property APIPassword() As String
	Get
	  Return CStr(MyBase.Item("apiPassword"))
	End Get
	Set
	  MyBase.Item("apiPassword") = Value
	End Set
  End Property

  <ConfigurationProperty("signature", DefaultValue := "")> _
  Public Property Signature() As String
	Get
	  Return CStr(MyBase.Item("signature"))
	End Get
	Set
	  MyBase.Item("signature") = Value
	End Set
  End Property

  '[ConfigurationProperty("merchantId", DefaultValue = "")]
  'public string MerchantId {
  '  get {
  '    return (string)base["merchantId"];
  '  }
  '  set {
  '    base["merchantId"] = value;
  '  }
  '}

  '[ConfigurationProperty("defaultCurrencyCode", DefaultValue = "USD")]
  'public string DefaultCurrencyCode {
  '  get {
  '    return (string)base["defaultCurrencyCode"];
  '  }
  '  set {
  '    base["defaultCurrencyCode"] = value;
  '  }
  '}

  <ConfigurationProperty("isActive", DefaultValue := "false")> _
  Public Property IsActive() As Boolean
	Get
	  Return CBool(MyBase.Item("isActive"))
	End Get
	Set
	  MyBase.Item("isActive") = Value
	End Set
  End Property

  <ConfigurationProperty("isLive", DefaultValue := "false")> _
  Public Property IsLive() As Boolean
	Get
	  Return CBool(MyBase.Item("isLive"))
	End Get
	Set
	  MyBase.Item("isLive") = Value
	End Set
  End Property

End Class
