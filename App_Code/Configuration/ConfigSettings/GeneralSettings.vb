#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

''' <summary>
''' Summary description for GeneralSettings
''' </summary>
Public Class GeneralSettings
	Inherits ConfigurationSection
	<StringValidator(MinLength := 3, MaxLength := 3), ConfigurationProperty("currencyCode", DefaultValue := "USD")> _
	Public Property CurrencyCode() As String
		Get
			Return CStr(MyBase.Item("currencyCode"))
		End Get
		Set
			MyBase.Item("currencyCode") = Value
		End Set
	End Property
	<StringValidator(MinLength := 1), ConfigurationProperty("loginRequirement", DefaultValue := "checkout")> _
	Public Property LoginRequirement() As String
		Get
			Return CStr(MyBase.Item("loginRequirement"))
		End Get
		Set
			MyBase.Item("loginRequirement") = Value
		End Set
	End Property
	<ConfigurationProperty("authorizeSaleOnly", DefaultValue := False)> _
	Public Property AuthorizeSaleOnly() As Boolean
		Get
			Return CBool(MyBase.Item("authorizeSaleOnly"))
		End Get
		Set
			MyBase.Item("authorizeSaleOnly") = Value
		End Set
	End Property
End Class
