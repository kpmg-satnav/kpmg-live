#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Web.Configuration
Imports Commerce.Common
Imports Commerce.Providers
Imports System.IO

''' <summary>
''' Summary description for SiteConfig
''' </summary>
Public Class SiteConfig
  Private Shared generalSettings As GeneralSettings
  Private Shared ppProSettings As PayPalProSettings
  Private Shared ppStandardSettings As PayPalStandardSettings
  Public Shared CreditCardSettings As PaymentServiceSection
  Public Shared TaxServiceSettings As TaxServiceSection
  Private Shared shippingSettings As FulfillmentServiceSection

  Private Sub New()
  End Sub
  Public Shared Sub Load()
	' Get the current configuration file.
	Dim config As System.Configuration.Configuration = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath)
	generalSettings = CType(config.GetSection("GeneralSettings"), GeneralSettings)
	ppProSettings = CType(config.GetSection("PayPalProSettings"), PayPalProSettings)
	ppStandardSettings = CType(config.GetSection("PayPalStandardSettings"), PayPalStandardSettings)
	CreditCardSettings = CType(config.GetSection("PaymentService"), PaymentServiceSection)
	TaxServiceSettings = CType(config.GetSection("TaxService"), TaxServiceSection)
	shippingSettings = CType(config.GetSection("FulfillmentService"), FulfillmentServiceSection)
    End Sub
    Public Shared Function ReadHitCounter() As Long
        Try
            Dim ct As String = ""
            Dim Cnt As Long = 5111333
            Dim CounterPage As String = HttpContext.Current.Server.MapPath("~/counters/hitcounter.txt")
            'Reading Counter value from text file.
            Dim objStreamReader As StreamReader
            objStreamReader = File.OpenText(CounterPage)
            ct = objStreamReader.ReadToEnd()
            If Long.TryParse(ct, Cnt) Then
            Else
                Cnt = 50000
            End If
            objStreamReader.Close()
            Return Cnt
        Catch ex As Exception
            Return 5111333
        End Try
    End Function
    Public Shared Sub WriteHitCounter(ByVal Cnt As Long)
        Try
            'Writing Counter value to text file.
            Dim CounterPage As String = HttpContext.Current.Server.MapPath("~/counters/hitcounter.txt")
            Dim objFileWriter As StreamWriter
            objFileWriter = File.CreateText(CounterPage)
            objFileWriter.Write(Cnt.ToString)
            objFileWriter.Close()
        Catch ex As Exception
        End Try
    End Sub

  #Region "Publics"
  Public Shared ReadOnly Property AcceptCreditCards() As Boolean
	Get
	  Return CreditCardSettings.AcceptCreditCards
	End Get
  End Property

  Public Shared ReadOnly Property DimensionUnit() As String
	Get
	  Return shippingSettings.DimensionUnit
	End Get
  End Property

  Public Shared ReadOnly Property BusinessEmail() As String
	Get
	  Return ppStandardSettings.BusinessEmail
	End Get
  End Property

  Public Shared ReadOnly Property RequireShipping() As Boolean
	Get
	  Return shippingSettings.UseShipping
	End Get
  End Property

  Public Shared ReadOnly Property RequireLogin() As String
	Get
	  Return generalSettings.LoginRequirement
	End Get
  End Property

  Public Shared ReadOnly Property ShipFromZip() As String
	Get
	  Return shippingSettings.ShipFromZip
	End Get
  End Property

  Public Shared ReadOnly Property UsePPStandardSandbox() As Boolean
	Get
	  Return ppStandardSettings.UseSandbox
	End Get
  End Property

  Public Shared ReadOnly Property UsePayPalPaymentsStandard() As Boolean
	Get
	  Return ppStandardSettings.IsActive
	End Get
  End Property

  Public Shared ReadOnly Property UsePayPalExpressCheckout() As Boolean
	Get
	  Return ppProSettings.IsActive
	End Get
  End Property

  Public Shared ReadOnly Property UsePPProSandbox() As Boolean
	Get
	  Return Not ppProSettings.IsLive
	End Get
  End Property

  Public Shared ReadOnly Property CurrencyCode() As CurrencyCode
	Get
	  If (Not System.Enum.IsDefined(GetType(Commerce.Common.CurrencyCode), generalSettings.CurrencyCode)) Then
		Throw New InvalidOperationException(String.Format("Provided currency code is not a valid Commerce.Common.CurrencyCode. Currency Code: {0}.", generalSettings.CurrencyCode))
	  End If
	  Return CType(System.Enum.Parse(GetType(Commerce.Common.CurrencyCode), generalSettings.CurrencyCode), CurrencyCode)
	End Get
  End Property

  Public Shared ReadOnly Property PayPalAPIUserName() As String
	Get
	  Return ppProSettings.APIUserName
	End Get
  End Property

  Public Shared ReadOnly Property PayPalAPIPassword() As String
	Get
	  Return ppProSettings.APIPassword
	End Get
  End Property

  Public Shared ReadOnly Property PayPalAPISignature() As String
	Get
	  Return ppProSettings.Signature
	End Get
  End Property

  Public Shared ReadOnly Property PayPalAPIIsLive() As Boolean
	Get
	  Return ppProSettings.IsLive
	End Get
  End Property

  Public Shared ReadOnly Property PayPalPDTID() As String
	Get
	  Return ppStandardSettings.PDTID
	End Get
  End Property

  Public Shared ReadOnly Property ShipPackagingBuffer() As Decimal
	Get
	  Return shippingSettings.ShipPackagingBuffer
	End Get
  End Property

  Public Shared ReadOnly Property ShipFromCountryCode() As String
	Get
	  Return shippingSettings.ShipFromCountryCode
	End Get
  End Property

  Public Shared ReadOnly Property AuthorizeSaleOnly() As Boolean
	Get
	  Return generalSettings.AuthorizeSaleOnly
	End Get
  End Property

  #End Region
End Class
