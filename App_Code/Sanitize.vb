Imports System.IO
Imports System.Text.RegularExpressions
Public Class Sanitize
    Public Shared Function SanitizeInput(ByVal input As String) As String
        Dim badCharReplace As New Regex("([<>""'%;()&])")
        Dim goodChars As String = badCharReplace.Replace(input, "")
        Return goodChars
    End Function
End Class
