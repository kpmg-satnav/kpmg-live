﻿Imports Microsoft.VisualBasic

Public Class RentRevision
    Private m_RR_Year As String
    Private M_RR_YEARS As String
    Private M_RR_DAYS As String
    Private M_RR_MONTHS As String
    Private m_RR_NOESC As String
    Private m_RR_Percentage As String
    Private m_RR_Value As String
    Private M_pm_les_esc_id As String
    Private M_validat As Integer


    Public Property RR_Year() As String
        Get
            Return m_RR_Year
        End Get
        Set(value As String)
            m_RR_Year = value
        End Set
    End Property

    Public Property pm_les_esc_id() As String
        Get
            Return M_pm_les_esc_id
        End Get
        Set(value As String)
            M_pm_les_esc_id = value
        End Set
    End Property

    Public Property RR_Percentage() As String
        Get
            Return m_RR_Percentage
        End Get
        Set(value As String)
            m_RR_Percentage = value
        End Set
    End Property

    Public Property RR_Value() As String
        Get
            Return m_RR_Value
        End Get
        Set(value As String)
            m_RR_Value = value
        End Set
    End Property
    Public Property RR_YEARS() As String
        Get
            Return M_RR_YEARS
        End Get
        Set(value As String)
            M_RR_YEARS = value
        End Set
    End Property
    Public Property RR_MONTHS() As String
        Get
            Return M_RR_MONTHS
        End Get
        Set(value As String)
            M_RR_MONTHS = value
        End Set
    End Property
    Public Property RR_DAYS() As String
        Get
            Return M_RR_DAYS
        End Get
        Set(value As String)
            M_RR_DAYS = value
        End Set
    End Property
    Public Property RR_NOESC() As String
        Get
            Return m_RR_NOESC
        End Get
        Set(value As String)
            m_RR_NOESC = value
        End Set
    End Property
    Public Property validat() As String
        Get
            Return M_validat
        End Get
        Set(value As String)
            M_validat = value
        End Set
    End Property

End Class
