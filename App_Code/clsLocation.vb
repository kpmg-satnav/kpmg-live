Imports Microsoft.VisualBasic

Public Class clsLocation
    Private mLCM_CODE As String

    Public Property LCM_CODE() As String
        Get
            Return mLCM_CODE
        End Get
        Set(ByVal value As String)
            mLCM_CODE = value
        End Set
    End Property
    Private mLCM_NAME As String

    Public Property LCM_NAME() As String
        Get
            Return mLCM_NAME
        End Get
        Set(ByVal value As String)
            mLCM_NAME = value
        End Set
    End Property
End Class
