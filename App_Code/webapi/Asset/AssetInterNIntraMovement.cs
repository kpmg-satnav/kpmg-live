﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class AssetInterNIntraMovementController : ApiController
{
    AssetInterNIntraMovementService ais = new AssetInterNIntraMovementService();
    [HttpPost]
    public DataTable GetInterMovementsData([FromBody] MovementVM.ExtensionViewModel obj)
    {
        return ais.GetInterMovementsData(obj);
    }
}
