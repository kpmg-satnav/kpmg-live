﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;

public class ASTCustomizedReportController : ApiController
{
    EquipmentCustomizedReportService ECRS = new EquipmentCustomizedReportService();


    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(EquipmentVM Equipment)
    {
        var obj = ECRS.GetCustomizedObject(Equipment);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public async Task<HttpResponseMessage> GetCustomizedData([FromBody]EquipmentVM data)
    {
        ReportGenerator<EquipmentCustomizedData> reportgen = new ReportGenerator<EquipmentCustomizedData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Asset_Mgmt/ASTCustomizableReport.rdlc"),
            DataSetName = "AssetCustomizableReport",
            ReportType = "Asset Customizable Report"
        };

        ECRS = new EquipmentCustomizedReportService();
        var reportdata =  ECRS.SearchAllData(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/ASTCustomizableReport." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "AssetCustomizableReport." + data.Type;
        return result;
    }
    //[HttpPost]
    //public HttpResponseMessage SearchAllData(EquipmentVM Equipment)
    //{
    //    var obj = ECRS.GetSearchAllData(Equipment);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}
}
