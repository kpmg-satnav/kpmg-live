﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using System.Drawing;
using System.IO;
using QRCoder;

public class QRCodeGeneratorService
{
    SubSonic.StoredProcedure sp;
    List<QRCodeGeneratorModel> QRCodeLst;
    QRCodeGeneratorModel QRCode;
    DataSet ds;
    static byte[] byteImage;
    public object GetQRCodeDetails(QRCodeGeneratorModel data)
    {

        QRCodeLst = new List<QRCodeGeneratorModel>();
        try
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["UID"];
            param[1] = new SqlParameter("@Location", SqlDbType.NVarChar);
            param[1].Value = data.LCM_CODE;
            param[2] = new SqlParameter("@CompanyId", SqlDbType.NVarChar);
            param[2].Value =data.CompanyId;           
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "ASSET_QR_CODE_READER", param);
            return new { griddata = ds.Tables[0]};
          
        }
        catch(Exception ex)
        {
            return QRCodeLst;
        }
    }
}