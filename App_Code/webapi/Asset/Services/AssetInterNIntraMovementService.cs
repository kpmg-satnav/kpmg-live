﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
/// <summary>
/// Summary description for AssetInterNIntraMovement
/// </summary>
public class AssetInterNIntraMovementService
{
    SubSonic.StoredProcedure sp;
    DataSet ds = new DataSet();
    public DataTable GetInterMovementsData(MovementVM.ExtensionViewModel obj)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AST_GETASSETS_INSTOCKINTER");
        sp.Command.AddParameter("@AssetCode", obj.AssetCode, DbType.String);
        sp.Command.AddParameter("@REQ_ID", obj.RequisitionID, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
}