﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class QRCodeGeneratorModel
{
    public string AAT_CODE { get; set; }
    public string QrCode { get; set; }
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string AAT_NAME { get; set; }
    public string AAT_AAB_CODE { get; set; }
    public string AST_MD_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string CompanyId { get; set; }

}
