﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for ProjectCheckListReportController
/// </summary>
public class ProjectCheckListReportController : ApiController
{
    ProjectCheckListReportService Report = new ProjectCheckListReportService();
    [HttpGet]
    public Object getInspectors()
    {
        return Report.getInspectors();
    }
    [HttpPost]
    public HttpResponseMessage LoadGrid(CustParams param)
    {
        var obj = Report.LoadGrid(param);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetImage(PrjImgParams PrjImgParams)
    {
        var obj = Report.GetImage(PrjImgParams);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public async Task<HttpResponseMessage> ExportGrid([FromBody]CustParams data)
    {
        ReportGenerator<ProjectCheckListReportModel> reportgen = new ReportGenerator<ProjectCheckListReportModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/BranchCheckList_Mgmt/ProjectChecklistReport.rdlc"),
            DataSetName = "ProjectChecklistReport",
            ReportType = "CheckList Report"
        };

        Report = new ProjectCheckListReportService();
        List<ProjectCheckListReportModel> reportdata = Report.LoadGrid(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/ProjectCheckListReport." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "ProjectCheckListReport." + data.Type;
        return result;
    }
}
