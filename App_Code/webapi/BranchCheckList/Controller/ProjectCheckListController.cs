﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.IO;

public class ProjectCheckListController:ApiController
{
    ProjectCheckListService pcs = new ProjectCheckListService();

    [HttpGet]
    public Object getInspectors()
    {
        return pcs.getInspectors();
    }
    [HttpGet]
    public Object getProjects()
    {
        return pcs.getProjects();
    }
    [HttpPost]
    public HttpResponseMessage GetGriddata([FromUri] int pType)
    {
        var obj = pcs.GetGriddata(pType);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);

        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetSavedDraftsdata(GetSavedDraftsList savedDraftsList)
    {
        var obj = pcs.GetSavedDraftsdata(savedDraftsList);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage getSavedList()
    {
        var obj = pcs.getSavedList();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage InsertCheckList(SaveProjectChecklist seldt)
    {
        var obj = pcs.InsertCheckList(seldt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public string UploadFiles()
    {
        int iUploadedCnt = 0;

        // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
        string sPath = "";
        sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/" + HttpContext.Current.Session["TENANT"] + "/ProjectCheckList/");

        System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

        // CHECK THE FILE COUNT.
        for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        {
            System.Web.HttpPostedFile hpf = hfc[iCnt];
            if (hpf.ContentLength > 0)
            {
                // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                //if (!File.Exists(sPath + Path.GetFileName(hpf.FileName)))
                //{
                // SAVE THE FILES IN THE FOLDER.
                hpf.SaveAs(sPath + Path.GetFileName(hpf.FileName));
                iUploadedCnt = iUploadedCnt + 1;
                //}
            }
        }

        // RETURN A MESSAGE.
        if (iUploadedCnt > 0)
        {
            return iUploadedCnt + " Files Uploaded Successfully";
        }
        else
        {
            return "Upload Failed";
        }
    }
}