﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.IO;

public class CheckListCreationController : ApiController    
{
    CheckListCreationService ccs = new CheckListCreationService();

    [HttpGet]
    public Object getInspectors()
    {
      
        return ccs.getInspectors();
    }
    [HttpGet]
    public Object getCompany()
    {

        return ccs.getCompany();
    }
    [HttpGet]
    public Object getLocations()
    {

        return ccs.getLocations();
    }


    [HttpPost]
    public HttpResponseMessage GetGriddata(list lst)
    {
        //var response = ccs.GetGriddata(lst);
        //return response;
        var obj = ccs.GetGriddata(lst);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public object GetScore()
    {
        var response = ccs.GetScore();
        return response;
    }

    [HttpPost]
    public HttpResponseMessage InsertCheckList(selecteddata seldt)
    {
        var obj = ccs.InsertCheckList(seldt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage LocationByCity(List<CityList> citylst)
    {
        var obj = ccs.LocationByCity(citylst);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage getSavedList()
    {
        var obj = ccs.getSavedList();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage getSavedListItems([FromUri] string LcmCode)
    {
        var obj = ccs.getSavedListItems(LcmCode);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //var response = getSavedListItems(LcmCode);
        //return response;
    }


    [HttpPost]
    public string UploadFiles()
    {
        int iUploadedCnt = 0;

        // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
        string sPath = "";
        sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadFiles/AdityaBirla.dbo/");

        System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

        // CHECK THE FILE COUNT.
        for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        {
            System.Web.HttpPostedFile hpf = hfc[iCnt];
            if (hpf.ContentLength > 0)
            {
                // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                //if (!File.Exists(sPath + Path.GetFileName(hpf.FileName)))
                //{
                    // SAVE THE FILES IN THE FOLDER.
                    hpf.SaveAs(sPath + Path.GetFileName(hpf.FileName));
                    iUploadedCnt = iUploadedCnt + 1;
                //}
            }
        }

        // RETURN A MESSAGE.
        if (iUploadedCnt > 0)
        {
            return iUploadedCnt + " Files Uploaded Successfully";
        }
        else
        {
            return "Upload Failed";
        }
    }
}