﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;


public class selecteddata
{
    public List<LoclstHirachy> LCMLST { get; set; }
    public string InspectdBy { get; set; }
    public string date { get; set; }
    public string SelCompany { get; set; }   
    public List<SelRadio> Seldata { get; set; }
    public List<FileProperties> PostedFiles { get; set; }
    public int Flag { get; set; }
    public ObservableCollection<ImagesList> imagesList { get; set; }
    public string OVERALL_CMTS { get; set; }
}

public class CityList
{
    public string CTY_CODE { get; set; }
    public string CTY_NAME { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class LoclstHirachy
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }  
    public bool ticked { get; set; }
}

public class LocationByCity
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public int LCM_CNP { get; set; }
    public bool ticked { get; set; }
}
public class SelRadio
{
    //public string row { get; set; }
    public string CatCode { get; set; }
    public string SubcatCode { get; set; }
    public string ScoreCode { get; set; }
    public string ScoreName { get; set; }
    public string txtdata { get; set; }
    public string Date { get; set; }
    public string SubScore { get; set; }
    public string FilePath { get; set; }
}

public class list
{
    public string LcmCode { get; set; }
    public DateTime? InspectdDate { get; set; }
}

//public class FileProperties
//{
//    public string name { get; set; }
//    public string size { get; set; }
//    public string type { get; set; }
//    public string SaveAs { get; set; }
//    public string Length { get; set; }
//}
