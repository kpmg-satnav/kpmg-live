﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class CustParams
{
    public List<LoclstHirachy> LCMLST { get; set; }
    public List<Companylst> CNPLST { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string Type { get; set; }
}
public class ImgParams
{
    public List<LoclstHirachy> LCMLST { get; set; }
    public string INSPCTED_BY { get; set; }
    public DateTime INSPCTED_DT { get; set; }
}
public class CheckListReportModel
{
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string BCL_MC_NAME { get; set; }
    public string BCL_SUB_NAME { get; set; }
    public string BCLD_SCORE_NAME { get; set; }
    public string BCLD_SCORE_CODE { get; set; }
    public string BCLD_TEXTDATA { get; set; }
    public DateTime? BCLD_DATE { get; set; }   
    public string BCL_INSPECTED_BY { get; set; }
    public DateTime? BCL_SELECTED_DT { get; set; }
    public string SELECTED_CNP { get; set; }
    public DateTime? BCL_CREATED_DT { get; set; }
    public string BCL_CREATED_BY { get; set; }
  
}