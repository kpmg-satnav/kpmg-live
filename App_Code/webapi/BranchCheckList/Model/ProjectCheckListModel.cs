﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProjectCheckListModel
/// </summary>
public class ProjectCheckListModel
{   
    public string Category { get; set; }

    public string SubCategory { get; set; }
    public string ChildCategory { get; set; }
    public string childfiled { get; set; }
    public string Mandatory { get; set; }
    public string SubChildCategory { get; set; }
    public string Subchildfiled { get; set; }
    public string SubChildCategory1 { get; set; }
    public string PM_SCC1_R_C_T { get; set; }
    public string ChildCatCode { get; set; }
}
public class SaveProjectChecklist
{
    public string Country { get; set; }
    public string City { get; set; }
    public string Location { get; set; }
    public string BusinessUnit { get; set; }
    public string Inspectionby { get; set; }
    public string ProjectType { get; set; }
    public string FromDate { get; set; }
    public string ProjectName { get; set; }
    public int Flag { get; set; }
    public List<Details> ChecklistDetails { get; set; }
    public ObservableCollection<ImagesList> imagesList { get; set; }
    public string OVERALL_CMTS { get; set; }
}
public class Details
{
    public string CatCode { get; set; }
    public string SubCatCode { get; set; }
    public string ChildCode { get; set; }
    public string ChildSubCode { get; set; }
    public string ChildCatValue { get; set; }
    public string ChildSubValue { get; set; }
    public bool ticked { get; set; }
}

public class GetSavedDraftsList
{
    public string LCM_CODE { get; set; }
    public string PM_CL_MAIN_INSP_BY { get; set; }
    public DateTime PM_CL_MAIN_VISIT_DT { get; set; }
    public string CompanyId { get; set; }
    public string AUR_ID { get; set; }
    public int Flag { get; set; }
}
public class GetGridBasedOnPType
{
    public int Project_Id { get; set; }
}