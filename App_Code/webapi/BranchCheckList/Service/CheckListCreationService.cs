﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;
using System.ComponentModel;


public class CheckListCreationService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object getInspectors()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_INSPECTORS");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getCompany()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_COMPANIES");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object getLocations()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_LOCATIONS");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetGriddata(list lst)
    {
        DataSet ds = new DataSet();
        if (lst!=null)
        {
            
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CATEGORIES");
            sp.Command.Parameters.Add("@LOC_ID", lst.LcmCode, DbType.String);
            sp.Command.Parameters.Add("@InspectedDate", lst.InspectdDate, DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
        }
        else
        {
            
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_CATEGORIES");
            sp.Command.Parameters.Add("@LOC_ID","", DbType.String);
            sp.Command.Parameters.Add("@InspectedDate","", DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
        }
       
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }


    public object GetScore()
    {
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_SCORE");
        DataSet ds = sp.GetDataSet();
        return new { data = ds.Tables[0] };
    }


    public object getSavedList()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_SAVED_LIST");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);      
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getSavedListItems(string LcmCode)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "BCL_GET_SAVED_LIST_ITEMS");
        sp.Command.Parameters.Add("@LCM_CODE",LcmCode, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object LocationByCity(List<CityList> citylst)
    {
        List<LocationByCity> loclst = new List<LocationByCity>();
        LocationByCity loc;
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CITYLST", SqlDbType.Structured);
        if (citylst == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(citylst);
        }    
       

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_LOCATION_BY_CITY", param))
        {
            while (sdr.Read())
            {
                loc = new LocationByCity();
                loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                loc.CNY_CODE = sdr["CNY_CODE"].ToString();    
                loc.LCM_CNP = Convert.ToInt32(sdr["LCM_CNP"]);
                loc.ticked = false;
                loclst.Add(loc);
            }
        }
        if (loclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = loclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public static DataTable ConvertToDataTable<T>(List<T> data)
    {
        DataTable dataTable = new DataTable();
        PropertyDescriptorCollection propertyDescriptorCollection =
            TypeDescriptor.GetProperties(typeof(T));
        for (int i = 0; i < propertyDescriptorCollection.Count; i++)
        {
            PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
            Type type = propertyDescriptor.PropertyType;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                type = Nullable.GetUnderlyingType(type);


            dataTable.Columns.Add(propertyDescriptor.Name, type);
        }
        object[] values = new object[propertyDescriptorCollection.Count];
        foreach (T iListItem in data)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
            }
            dataTable.Rows.Add(values);
        }
        return dataTable;
    }

    public object InsertCheckList(selecteddata dataobj)
    {
        for (int i = 0; i < dataobj.imagesList.Count; i++)
        {
            SqlParameter[] param1 = new SqlParameter[7];
            param1[0] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            param1[0].Value = UtilityService.ConvertToDataTable(dataobj.LCMLST);
            param1[1] = new SqlParameter("@INSP_BY", SqlDbType.NVarChar);
            param1[1].Value = dataobj.InspectdBy;
            param1[2] = new SqlParameter("@INSP_DT", SqlDbType.NVarChar);
            param1[2].Value = dataobj.date;
            param1[3] = new SqlParameter("@UPLOAD_PATH", SqlDbType.NVarChar);
            param1[3].Value = dataobj.imagesList[i].Imagepath;
            param1[4] = new SqlParameter("@FILENAME", SqlDbType.NVarChar);
            param1[4].Value = dataobj.imagesList[i].Imagepath;
            param1[5] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param1[5].Value = HttpContext.Current.Session["UID"];
            param1[6] = new SqlParameter("@REM", SqlDbType.NVarChar);
            param1[6].Value = "Uploaded From Web";
            SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "API_HD_INSERT_CHECKLIST_FILES", param1);
        }
        List<selecteddata> HDSEM = new List<selecteddata>();
        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@LCM_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(dataobj.LCMLST);
        param[1] = new SqlParameter("@InspectdBy", SqlDbType.NVarChar);
        param[1].Value = dataobj.InspectdBy;
        param[2] = new SqlParameter("@date", SqlDbType.NVarChar);
        param[2].Value = dataobj.date;
        param[3] = new SqlParameter("@SelCompany", SqlDbType.NVarChar); 
        param[3].Value = dataobj.SelCompany;
        param[4] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
        param[4].Value = UtilityService.ConvertToDataTable(dataobj.Seldata);
        param[5] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[5].Value = HttpContext.Current.Session["UID"];
        param[6] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[6].Value = HttpContext.Current.Session["COMPANYID"];
        param[7] = new SqlParameter("@FLAG", SqlDbType.Int);
        param[7].Value = dataobj.Flag;
        param[8] = new SqlParameter("@OVER_CMTS", SqlDbType.NVarChar);
        param[8].Value = dataobj.OVERALL_CMTS;
        Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "BCL_INSERT_CHECKLIST", param);        
        int sem_id = (int)o;
        string msg;
        if (sem_id == 0)
            msg = "Saved as Draft";
        else
            msg = "CheckList Submitted Succesfully";
        return new { data = msg, Message = msg };
    }
}