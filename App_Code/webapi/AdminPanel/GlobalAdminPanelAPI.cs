﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

public class GlobalAdminPanelAPIController : ApiController
{
  
    GlobalAdminPanelService service = new GlobalAdminPanelService();
    // GET api/<controller>
       
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["UID"])))
        {
            if (string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["UID"])))
            {
                //Response.Redirect(Application("FMGLogout"))
                //HttpContext.Current.Response.Redirect(Application("FMGLogout"));   
            }
        }       
    }

    public object[][] GetAllCountryCustomers()
    {        
       
        return service.GetAllCountryCustomers();
    }

    public object[][] GetCustomerwiseempcount()
    {
        
        return service.GetCustomerwiseempcount();
    }

    public DataTable GetCustomertranscationcount()
    {
        return service.GetCustomertranscationcount();
    }
    
    //total Customers Details
    public DataTable GettotalCustDetails()
    {
        return service.GettotalCustDetails();
    }

    //14-Day Trial Version Customers Details
    public DataTable Get14DaysTrialCustDetails()
    {
        return service.Get14DaysTrialCustDetails();
    }
    
    //14- day trial expired Customers Details
    public DataTable Get14daystrialexpcustDetails()
    {
        return service.Get14daystrialexpcustDetails();
    }

    //Subs Active Customers Details
    public DataTable GetSubscribedCustDetails()
    {
        return service.GetSubscribedCustDetails();
    }

    //Subs INActive Customers Details
    public DataTable GetSubscribedInactCustDetails()
    {
        return service.GetSubscribedInactCustDetails();
    }
   
    //Customer Employee Count
    public DataTable GetCustomeremplimit()
    {
        return service.GetCustomeremplimit();
    }
      

    //update expiry date

    [HttpPost]
    public void UpdatetrialversExpCustDetails([FromBody] GlobalAdminPanelModel.ExtensionViewModel obj)
    {
        service.UpdatetrialversExpCustDetails(obj);
    }

    //update Tenant Employee count
    [HttpPost]
    public void UpdateCustEmployeeCount([FromBody] GlobalAdminPanelModel.ExtensionEmpCountModel obj)
    {
        service.UpdateCustEmployeeCount(obj);
    }

    //Get Total Customers Module-Wise
    public DataTable GetTotalCustomersModuleWise()
    {
        return service.GetTotalCustomersModuleWise();
    }

    //Get All Customers for Modules
    public DataTable GetCustomersByModules()
    {
        return service.GetCustomersByModules();
    }

    
    // GET api/<controller>/5
    public string Get(int id)
    {
        return "value";
    }

    // POST api/<controller>
    public void Post([FromBody]string value)
    {
    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]string value)
    {
    }

    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }    
}



