﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


public class BusinessCardCustomizableController : ApiController
{
    BusinessCardCustomizableService Bservice = new BusinessCardCustomizableService();
    [HttpPost]
    public HttpResponseMessage GetBCDetails(CustomParams param)
    {
        var obj = Bservice.GetBusinessCardobject(param);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage LoadBCDetails(CustomParams param)
    {
        var obj = Bservice.LoadCustomizableDetails(param);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public async Task<HttpResponseMessage> GetCustomizedData([FromBody]CustomParams data)
    {
        ReportGenerator<BusinessCardCustomizableModel> reportgen = new ReportGenerator<BusinessCardCustomizableModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/BusinessCard/BusinessCardCustomizable.rdlc"),
            DataSetName = "CustomizableReport",
            ReportType = "Business Card Customized Report"
        };

        Bservice = new BusinessCardCustomizableService();
        List<BusinessCardCustomizableModel> reportdata = Bservice.GetCustomData(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/Customizable." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "BusinessCard_Customization." + data.Type;
        return result;
    }
}
