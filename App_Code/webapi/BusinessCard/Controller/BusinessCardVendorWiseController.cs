﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class BusinessCardVendorWiseController : ApiController
{
    BusinessCardVendorWiseService BCService = new BusinessCardVendorWiseService();

    [HttpPost]
    public object GetVendorWiseDetails([FromBody] BusinessCardsParams obj)
    {
        var response = BCService.GetVendorWiseDetails(obj);
        //HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetVendorWiseReport([FromBody] BusinessCardsParams obj)
    {
        ReportGenerator<BusinessCardVendorWiseModel> reportgen = new ReportGenerator<BusinessCardVendorWiseModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/BusinessCard/BusinessCardVendorWise.rdlc"),
            DataSetName = "VendorWiseReq",
            ReportType = "Vendor Wise Request Report"
        };
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/VendorWiseRequest." + obj.Type);
        SubSonic.StoredProcedure sp;
        List<BusinessCardVendorWiseModel> VMlist;
        BusinessCardVendorWiseModel rptVMobj;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BC_VENDOR_WISE_REQUEST_REPORT");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@FROMDATE", obj.FromDate.ToString(), DbType.String);
        sp.Command.AddParameter("@TODATE", obj.ToDate.ToString(), DbType.String);
        sp.Command.AddParameter("@CompanyId", obj.CompanyId, DbType.Int32);
        sp.Command.AddParameter("@SEARCHVAL", obj.SearchValue, DbType.String);
        sp.Command.AddParameter("@PAGENUM", obj.PageNumber, DbType.String);
        sp.Command.AddParameter("@PAGESIZE", obj.PageSize, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            VMlist = new List<BusinessCardVendorWiseModel>();
            while (sdr.Read())
            {
                rptVMobj = new BusinessCardVendorWiseModel();
                rptVMobj.ADDRESS = sdr["ADDRESS"].ToString();
                rptVMobj.DEPARTMENT = sdr["DEPARTMENT"].ToString();
                rptVMobj.LOCATION = sdr["LOCATION"].ToString();
                rptVMobj.LCM_CODE = sdr["LCM_CODE"].ToString();
                rptVMobj.CNP_NAME = sdr["CNP_NAME"].ToString();
                rptVMobj.VENDOR = sdr["VENDOR"].ToString();
                rptVMobj.REQ = Convert.ToInt32(sdr["REQ"]);
                rptVMobj.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                VMlist.Add(rptVMobj);
            }
            sdr.Close();
        }
        await reportgen.GenerateReport(VMlist, filePath, obj.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "VendorWiseRequest." + obj.Type;
        return result;
    }
}
