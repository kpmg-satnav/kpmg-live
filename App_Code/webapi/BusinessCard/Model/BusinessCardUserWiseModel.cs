﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class BusinessCardUserWiseModel
{
    public string EMPID { get; set; }
    public int CARDS{ get; set; }
    public string DEPARTMENT { get; set; }
    public string DESIGNATION { get; set; }
    public string LOCATION { get; set; }
    public string LCM_CODE { get; set; }
    public string CNP_NAME { get; set; }
    public string ADDRESS { get; set; }
    public string REMARKS { get; set; }   
    public string STATUS { get; set; }
    public string BC_FAX { get; set; }
    public string BC_PHONE_NO { get; set; }
    public string BC_LAND_NO { get; set; }
    public string BC_EMAIL { get; set; }
    public string BC_CREATED_BY { get; set; }
    public string BC_CREATED_DT { get; set; }
    public string OVERALL_COUNT { get; set; }
}
public class UserWiseparams 
{
    public DateTime? FromDate { get; set; }
    public DateTime? ToDate { get; set; }
    public int CompanyId{ get; set; }
    public string Type { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}