﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ReservationTypeController : ApiController
{
    ReservationTypeService service = new ReservationTypeService();

    [HttpPost]
    public HttpResponseMessage Create(ReservationTypeVM RtType)
    {
        if (service.Save(RtType) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "Facility Type Added Successfully");
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Facility Type Name already Exists");
    }


    [HttpPost]
    public HttpResponseMessage Update(ReservationTypeVM update)
    {

        if (service.Update(update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "Facility Type Updated Successfully");
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }

    [HttpGet]
    public HttpResponseMessage ReservationTypeBindGrid()
    {
        IEnumerable<ReservationTypeVM> mncCatlist = service.GetReservationTypeBindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, mncCatlist);
        return response;
    }

}
