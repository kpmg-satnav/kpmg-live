﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Net.Http.Headers;

public class CustomizableReportController : ApiController
{

    CustomizableReportService custrptservice = new CustomizableReportService();


    [HttpPost]
    public async Task<HttpResponseMessage> GetCustomizableReportdata([FromBody]CustomizableRptVM CustRpt)
    {
        ReportGenerator<CustomizableRpteDetails> reportgen = new ReportGenerator<CustomizableRpteDetails>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/GuestHouse_Mgmt/CustomizableRpt.rdlc"),
            DataSetName = "CustomizableRpt",
            ReportType = "Customizable Report"
        };

        List<CustomizableRpteDetails> reportdata = custrptservice.Getdetails(CustRpt);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/CustomizableRpt." + CustRpt.Type);
        await reportgen.GenerateReport(reportdata, filePath, CustRpt.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "CustomizableRpt." + CustRpt.Type;
        return result;
    }

    [HttpPost]
    public HttpResponseMessage GetCustomizabledetails(CustomizableRptVM CustRpt)
    {
        var obj = custrptservice.Getdetails(CustRpt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
