﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using UtiltiyVM;
/// <summary>
/// Summary description for AddReservationService
/// </summary>
public class AddFacilityService
{
    SubSonic.StoredProcedure sp;

    public IEnumerable<AddFacilityVM> GetGridData()
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[0].Value = HttpContext.Current.Session["UID"];

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_RESERVATION_MASTER_GRID", param))
            {
                List<AddFacilityVM> resrvlist = new List<AddFacilityVM>();
                while (reader.Read())
                {
                    resrvlist.Add(new AddFacilityVM()
                    {
                        RT_SNO = reader["RT_SNO"].ToString(),
                        RF_SNO = Convert.ToInt32(reader["RF_SNO"]),
                        RR_SNO = Convert.ToInt32(reader["RR_SNO"]),
                        RT_NAME = reader["RT_NAME"].ToString(),
                        RF_NAME = reader["RF_NAME"].ToString(),
                        // RR_NAME = reader["RR_NAME"].ToString(),
                        CNY_NAME = reader["CNY_NAME"].ToString(),
                        CTY_NAME = reader["CTY_NAME"].ToString(),
                        LCM_NAME = reader["LCM_NAME"].ToString(),
                        RF_STATUS = reader["RF_STATUS"].ToString(),
                        RF_REMARKS = reader["RF_REMARKS"].ToString(),
                        RF_NO_OF_ROOMS = reader["RF_NO_OF_ROOMS"].ToString(),
                        RF_CNY_CODE = reader["RF_CNY_CODE"].ToString(),
                        RF_CTY_CODE = reader["RF_CTY_CODE"].ToString(),
                        RF_LOC_CODE = reader["RF_LOC_CODE"].ToString(),
                        RF_EMAIL = reader["RF_EMAIL"].ToString(),
                        RF_LAT = reader["RF_LAT"].ToString(),
                        RF_LONG = reader["RF_LONG"].ToString(),
                        RF_COST = Convert.ToInt32(reader["RF_COST"].ToString()),
                        RF_FILE_NAME = reader["RF_FILE_NAME"].ToString()

                    });
                }
                reader.Close();
                return resrvlist;
            }
        }
        catch
        {
            throw;
        }
    }

    public IEnumerable<AddRoomVM> GetRoomDetails(AddFacilityVM obj)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@RF_SNO", SqlDbType.Int);
            param[0].Value = obj.RF_SNO;

            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_GET_ROOM_DETAILS", param))
            {
                List<AddRoomVM> rmlist = new List<AddRoomVM>();
                while (dr.Read())
                {
                    rmlist.Add(new AddRoomVM()
                    {
                        RR_SNO = Convert.ToInt32(dr["RR_SNO"]),
                        RR_NAME = dr["RR_NAME"].ToString(),
                        RR_CAPCITY = Convert.ToInt32(dr["RR_CAPCITY"]),
                        RR_FACILITIES = dr["RR_FACILITIES"].ToString(),
                        IsActive = Convert.ToInt32(dr["RR_STATUS"]),
                        FileName = dr["imgfilename"].ToString(),
                        ImageName = dr["RR_IMAGE_NAME"].ToString()
                    });
                }
                dr.Close();
                return rmlist;
            }
        }
        catch
        {
            throw;
        }
    }

   

    //public void SaveImage(AddRoomVM avm)
    //{
    //    string convert = avm.dataImg.Replace("data:image/jpeg;base64,", String.Empty).Replace("data:image/png;base64,", String.Empty);
    //    using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(convert)))
    //    {
    //        using (System.Drawing.Bitmap bm2 = new Bitmap(ms))
    //        {
    //            var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "GHB\\UploadImages\\" + avm.ImageName);
    //            bm2.Save(filePath);
    //        }
    //    }
    //}

    public object Create(AddFacilityVM model)
    {
        try
        {

            //List<AddRoomVM> list = model.addroom;

            //foreach (AddRoomVM item in list) // Loop through List with foreach
            //{
            //    if(item.dataImg != null && item.dataImg!="")
            //    SaveImage(item);
            //}

            SqlParameter[] param = new SqlParameter[17];


            param[0] = new SqlParameter("@RT_SNO", SqlDbType.Int);
            param[0].Value = model.RT_SNO;
            param[1] = new SqlParameter("@RF_NAME", SqlDbType.NVarChar);
            param[1].Value = model.RF_NAME;
            param[2] = new SqlParameter("@RF_EMAIL", SqlDbType.NVarChar);
            param[2].Value = model.RF_EMAIL;
            param[3] = new SqlParameter("@RF_CNY_CODE", SqlDbType.NVarChar);
            param[3].Value = model.RF_CNY_CODE;

            param[4] = new SqlParameter("@RF_CTY_CODE", SqlDbType.NVarChar);
            param[4].Value = model.RF_CTY_CODE;

            param[5] = new SqlParameter("@RF_LOC_CODE", SqlDbType.NVarChar);
            param[5].Value = model.RF_LOC_CODE;

           
            if (model.RF_LONG != "")
            {
                param[6] = new SqlParameter("@RF_LONG", SqlDbType.Decimal);
                param[6].Value = model.RF_LONG;
            }
            else
            {
                param[6] = new SqlParameter("@RF_LONG", SqlDbType.Decimal);
                param[6].Value = null;
            }
            if (model.RF_LAT != "")
            {
                param[7] = new SqlParameter("@RF_LAT", SqlDbType.Decimal);
                param[7].Value = model.RF_LAT;
            }
            else
            {
                param[7] = new SqlParameter("@RF_LAT", SqlDbType.Decimal);
                param[7].Value = null;
            }

            param[8] = new SqlParameter("@RF_NO_OF_ROOMS", SqlDbType.Int);
            param[8].Value = model.RF_NO_OF_ROOMS;

            param[9] = new SqlParameter("@RF_REMARKS", SqlDbType.NVarChar);
            param[9].Value = model.RF_REMARKS;

            param[10] = new SqlParameter("@RF_CREATEDBY", SqlDbType.NVarChar);
            param[10].Value = HttpContext.Current.Session["UID"];

            param[11] = new SqlParameter("@TVP_ADD_ROOM", SqlDbType.Structured);
            param[11].Value = UtilityService.ConvertToDataTable(model.addroom);

            param[12] = new SqlParameter("@RF_STATUS", SqlDbType.Int);
            param[12].Value = model.RF_STATUS;

            param[13] = new SqlParameter("@FLAG", SqlDbType.Int);
            param[13].Value = model.Flag;

            param[14] = new SqlParameter("@RF_SNO", SqlDbType.Int);
            param[14].Value = model.RF_SNO;

            param[15] = new SqlParameter("@RF_SNO", SqlDbType.Int);
            param[15].Value = model.RF_COST;


            param[16] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[16].Value = HttpContext.Current.Session["COMPANYID"];

            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_ADD_RESERVATION_ROOM", param))
            {
                if (dr.Read())
                {
                    if ((int)dr["FLAG"] == 1)
                    {
                      
                            return new { Message = MessagesVM.AddFacility_Inserted, data = dr["MSG"].ToString() };
                       
                    }
                    else if ((int)dr["FLAG"] == 2)
                    {

                        return new { Message = MessagesVM.AddFacility_Updated, data = dr["MSG"].ToString() };
                    }
                    else
                        return new { Message = MessagesVM.AddFacility_Exists + " :- " + dr["MSG"].ToString(), data = (object)null };
                }
            }

            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
           

        }
        catch
        {
            throw;
        }
    }

    public object UploadTemplate(HttpRequest httpRequest)
    {
        try
        {
            String jsonstr = httpRequest.Params["CurrObj"];
            FailityDet model = (FailityDet)Newtonsoft.Json.JsonConvert.DeserializeObject<FailityDet>(jsonstr);


            SqlParameter[] param = new SqlParameter[18];


            param[0] = new SqlParameter("@RT_SNO", SqlDbType.Int);
            param[0].Value = model.AddFacilityVM.RT_SNO;
            param[1] = new SqlParameter("@RF_NAME", SqlDbType.NVarChar);
            param[1].Value = model.AddFacilityVM.RF_NAME;
            param[2] = new SqlParameter("@RF_EMAIL", SqlDbType.NVarChar);
            param[2].Value = model.AddFacilityVM.RF_EMAIL;
            param[3] = new SqlParameter("@RF_CNY_CODE", SqlDbType.NVarChar);
            param[3].Value = model.AddFacilityVM.RF_CNY_CODE;

            param[4] = new SqlParameter("@RF_CTY_CODE", SqlDbType.NVarChar);
            param[4].Value = model.AddFacilityVM.RF_CTY_CODE;

            param[5] = new SqlParameter("@RF_LOC_CODE", SqlDbType.NVarChar);
            param[5].Value = model.AddFacilityVM.RF_LOC_CODE;



            if (model.AddFacilityVM.RF_LONG != "")
            {
                param[6] = new SqlParameter("@RF_LONG", SqlDbType.Decimal);
                param[6].Value = model.AddFacilityVM.RF_LONG;
            }
            else
            {
                param[6] = new SqlParameter("@RF_LONG", SqlDbType.Decimal);
                param[6].Value = null;
            }
            if (model.AddFacilityVM.RF_LAT != "")
            {
                param[7] = new SqlParameter("@RF_LAT", SqlDbType.Decimal);
                param[7].Value = model.AddFacilityVM.RF_LAT;
            }
            else
            {
                param[7] = new SqlParameter("@RF_LAT", SqlDbType.Decimal);
                param[7].Value = null;
            }




            param[8] = new SqlParameter("@RF_NO_OF_ROOMS", SqlDbType.Int);
            param[8].Value = model.AddFacilityVM.RF_NO_OF_ROOMS;

            param[9] = new SqlParameter("@RF_REMARKS", SqlDbType.NVarChar);
            param[9].Value = model.AddFacilityVM.RF_REMARKS;

            param[10] = new SqlParameter("@RF_CREATEDBY", SqlDbType.NVarChar);
            param[10].Value = HttpContext.Current.Session["UID"];

            param[11] = new SqlParameter("@TVP_ADD_ROOM", SqlDbType.Structured);
            param[11].Value = UtilityService.ConvertToDataTable(model.AddFacilityVM.addroom);

            param[12] = new SqlParameter("@RF_STATUS", SqlDbType.Int);
            param[12].Value = model.AddFacilityVM.RF_STATUS;

            param[13] = new SqlParameter("@FLAG", SqlDbType.Int);
            param[13].Value = model.AddFacilityVM.Flag;

            param[14] = new SqlParameter("@RF_SNO", SqlDbType.Int);
            param[14].Value = model.AddFacilityVM.RF_SNO;


           

            string Upload_Time = DateTime.Now.ToString("ddMMyyyyhhmm");
            string fileName = "";
            if (httpRequest.Files.Count != 0)
            {
                var postedFile = httpRequest.Files[0];
                fileName = Upload_Time + postedFile.FileName;
                var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "GHB\\UploadImages\\" + fileName);
                postedFile.SaveAs(filePath);
            }


            param[15] = new SqlParameter("@RF_FILE_NAME", SqlDbType.VarChar);
            param[15].Value = fileName;



            param[16] = new SqlParameter("@RF_COST", SqlDbType.Int);
            param[16].Value = model.AddFacilityVM.RF_COST;


            param[17] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[17].Value = HttpContext.Current.Session["COMPANYID"];

            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_ADD_RESERVATION_ROOM", param))
            {
                if (dr.Read())
                {
                    if ((int)dr["FLAG"] != 0)
                    {

                        //string imgs = "";
                        //if (model.AddFacilityVM.Flag == 1)
                        //{
                        //    //imgs = "Insert";
                        //    //bool imgstatus = InsertImages(httpRequest, (int)dr["FLAG"], imgs);
                        //}
                        //else
                        //{
                        //    imgs = "Update";
                        //    bool imgstatus = InsertImages(httpRequest, model.AddFacilityVM.RF_SNO, imgs);
                        //}

                        if (model.AddFacilityVM.Flag == 1)
                            return new { Message = MessagesVM.AddFacility_Inserted, data = dr["MSG"].ToString() };
                        else
                            return new { Message = MessagesVM.AddFacility_Updated, data = dr["MSG"].ToString() };
                    }
                    else
                    {
                        return new { Message = dr["MSG"].ToString(), data = (object)null };
                    }
                }
            }

            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public bool InsertImages(HttpRequest httpRequest, int fksno, string Type)
    {
        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "GHB\\UploadImages\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_ADD_RESERVATION_ROOM_IMAGE");
        if (Type == "Insert")
        {
            sp.Command.AddParameter("@FLAG", 1, DbType.Int32);
        }
        else if (Type == "Update")
        {
            sp.Command.AddParameter("@FLAG", 2, DbType.Int32);
        }
        sp.Command.AddParameter("@RRDT_RR_SNO", fksno, DbType.Int32);
        sp.Command.AddParameter("@RRDT_FILE_NAME", postedFile.FileName, DbType.String);
        sp.Command.AddParameter("@RRDT_FILE_PATH", filePath, DbType.String);
        sp.Command.AddParameter("@RRDT_STATUS", 1, DbType.Int32);
        sp.Command.AddParameter("@RRDT_CREATEDBY", HttpContext.Current.Session["UID"], DbType.String);
        int flag = (int)sp.ExecuteScalar();
        return true;
    }

}