﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class AdminBookingDetails
{
    public List<Locationlst> loclst { get; set; }
    public List<FacilityNamelst> rflist { get; set; }
    public string Request_Type { get; set; }
    public int STAT { get; set; }
}

public class AdminBookingVM
{
    public ABMain obj { get; set; }
    public List<ABDetailslst> objList { get; set; }
    public string ScreenType { get; set; }
    public string LocationCode { get; set; }
    public string start { get; set; }
    public string end { get; set; }

}



public class ABMain
{
    public string BM_REQ_ID { get; set; }
    public int BM_TYPE { get; set; }
    public string BM_TITLE { get; set; }
    public string BM_RESERVED_FOR { get; set; }
    public string BM_REFRERENCE_ID { get; set; }
    public string BM_FROM_DATE { get; set; }
    public string BM_TO_DATE { get; set; }
    public string BM_FROM_TIME { get; set; }
    public string BM_TO_TIME { get; set; }
    public string BM_REMARKS { get; set; }
    public int BM_STA_ID { get; set; }
    public string BM_LCM_CODE { get; set; }
    public string BM_CTY_CODE { get; set; }
    public string BM_CNY_CODE { get; set; }

}

public class ABDetailslst
{
    public string BD_BM_SNO { get; set; }
    public string BD_REQ_ID { get; set; }
    public string BD_RT_SNO { get; set; }
    public string BD_RF_SNO { get; set; }
    public string BD_RR_SNO { get; set; }
    public string BD_STA_ID { get; set; }
    public string BD_RESERVED_FOR { get; set; }
    public string BD_EMP_TYPE { get; set; }
    public string BD_EMP_EMAIL { get; set; }
    public string BD_REMARKS { get; set; }
    public string BD_EMP_NAME { get; set; }
    public string BD_MOBILE_NUMBER { get; set; }

}

public class ABGrid
{
    public string COUNTRY { get; set; }
    public string CITY { get; set; }
    public string LOCATION { get; set; }
    public string RT_NAME { get; set; }
    public string RF_NAME { get; set; }
    public string RR_NAME { get; set; }
    public string RRDT_FILE_NAME { get; set; }
    public string RR_RT_SNO { get; set; }
    public string RR_RF_SNO { get; set; }
    public string RR_SNO { get; set; }

}


public class AdminBookingGrid
{
    public string COUNTRY { get; set; }
    public string CITY { get; set; }
    public string LOCATION { get; set; }

    public string RT_NAME { get; set; }
    public string RF_NAME { get; set; }
    public string RR_NAME { get; set; }
    public string RRDT_FILE_NAME { get; set; }
    public string RF_COST { get; set; }
    public int RB_TYPE { get; set; }
    
    public string Title { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }
    public string FromTime { get; set; }
    public string ToTime { get; set; }
    public string ReferenceID { get; set; }
    public string ToWhom { get; set; }
    public string Remarks { get; set; }

    public string RR_RT_SNO { get; set; }
    public string RR_RF_SNO { get; set; }
    public string RR_SNO { get; set; }

    public string RB_REQ_ID { get; set; }
    public int RB_STA_ID { get; set; }

}




