﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserGuestHouseUltVM
/// </summary>
public class UserGuestHouseUltVM
{
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public List<Locationlst> lcmlst { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<Cnylst> cnylst { get; set; }
    public string Status { get; set; }
    public string Type { get; set; }
    public string CompanyID { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }

}

public class Locationlst
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}
public class MAINCATlst
{
    public string MNC_CODE { get; set; }
    public string MNC_NAME { get; set; }
   public bool ticked { get; set; }
}
public class LCMlst
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public bool ticked { get; set; }
}

public class VER_LST
{
    public string VER_CODE { get; set; }
    public string VER_NAME { get; set; }
    public bool ticked { get; set; }
}
public class SUBCATlst
{
    public string SUBC_CODE { get; set; }
    public string SUBC_NAME { get; set; }
    public string MNC_CODE { get; set; }
    public bool ticked { get; set; }
}
public class CHILDCATlst
{
    public string CHC_TYPE_CODE { get; set; }
    public string CHC_TYPE_NAME { get; set; }
    public string CHC_TYPE_SUBC_CODE { get; set; }
    public string CHC_TYPE_MNC_CODE { get; set; }
    public bool ticked { get; set; }
}
public class STATUSlst
{
    public string STA_ID { get; set; }
    public string STA_TITLE { get; set; }
    public bool ticked { get; set; }
}
public class Designationlst
{
    public string DSG_CODE { get; set; }
    public string DSG_NAME { get; set; }
    public bool ticked { get; set; }
}
public class Types
{
    public string VT_CODE { get; set; }
    public string VT_TYPE { get; set; }
    public bool ticked { get; set; }
}

public class Cnylst
{
    public string CNY_CODE { get; set; }
    public string CNY_NAME { get; set; }
    public bool ticked { get; set; }
}

public class Citylst
{
    public string CTY_CODE { get; set; }
    public string CTY_NAME { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class UserGuestHouseDetails
{
    public string RB_REQ_ID { get; set; }
    public string RT_NAME { get; set; }
    public DateTime RB_FROM_DATE { get; set; }
    public DateTime RB_TO_DATE { get; set; }
    public string RF_NAME { get; set; }
    public string RR_NAME { get; set; }
    public string RR_CAPCITY { get; set; }
    public string RBS_CTY_CODE { get; set; }
    public string RBS_LOC_CODE { get; set; }
    public string DEP_NAME { get; set; }
    public string RB_CREATEDBY { get; set; }
    public DateTime RB_CREATEDON { get; set; }
    public string RB_RESERVED_FOR { get; set; }
  
    public string STA_TITLE { get; set; }
    public string RESVERED_FOR_EMAIL { get; set; }
    public string RESVERED_BY_EMAIL { get; set; }


    public Nullable<DateTime> RB_CHK_IN_DATE { get; set; }
    public string RB_CHK_IN_TIME { get; set; }

    public Nullable<DateTime> RB_CHK_OUT_DATE { get; set; }
    public string RB_CHK_OUT_TIME { get; set; }
    public string UTILIZATION { get; set; }
    public string RB_REFERENCE_ID { get; set; }
    public string Cost_Center_Name { get; set; }

    public string ET_NAME { get; set; }

    public string band { get; set; }
    public string rf_cost { get; set; }
    public string bt_name { get; set; }
    public string rb_remarks { get; set; }

    public string noofnights { get; set; }
    public string totalroomrent { get; set; }
    public string OVERALL_COUNT { get; set; }


}

public class GHStatusDT
{
    public string STA_ID { get; set; }
    public string STA_TITLE { get; set; }
    public int STA_STA_ID { get; set; }
}