﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PerVSOfficalBookingVM
/// </summary>
public class PerVSOfficalBookingVM
{
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public List<PeroffLoclst> lcmlst { get; set; }
    public string Status { get; set; }
    public string Type { get; set; }
    public string CompanyID { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }

}

public class PeroffLoclst
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_CODE { get; set; }
    public string CNY_CODE { get; set; }
    public bool ticked { get; set; }
}

public class PervsoffDetails
{
    public string RT_NAME { get; set; }    
    public string RF_NAME { get; set; }
    public string RR_NAME { get; set; }
    public string RR_CAPCITY { get; set; }
    public string RF_CTY_CODE { get; set; }
    public string RF_LOC_CODE { get; set; }
    public string PERSONAL { get; set; }
    public string OFFICIAL { get; set; }
    public int RF_COST { get; set; }
    public int TOTALCOST { get; set; }
    public DateTime RB_FROM_DATE { get; set; }
    public DateTime RB_TO_DATE { get; set; }
    public string OVERALL_COUNT { get; set; }
}