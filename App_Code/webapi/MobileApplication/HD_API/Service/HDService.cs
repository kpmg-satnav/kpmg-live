﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Web.Script.Serialization;
using System.ComponentModel;
using System.Collections;

public partial class HDService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    #region API Core Logics
    string DatabaseURL;
    SecureAPIKey Secure = new SecureAPIKey();

    //Space Module

    public object GetEmployeeSpaceDetails(GetSpaceDetails main)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = main.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "HD_GET_SPACE_DETAILS");
                sp.Command.AddParameter("@AUR_ID", main.AURID, DbType.String);
                sp.Command.AddParameter("@FROM_DATE", main.FromDate, DbType.String);
                sp.Command.AddParameter("@FROM_TIME", main.FromTime, DbType.String);
                sp.Command.AddParameter("@TO_DATE", main.ToDate, DbType.String);
                sp.Command.AddParameter("@TO_TIME", main.ToTime, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
               
                if (ds.Tables.Count != 0)
                    return new { data = ds.Tables[0] };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    public object SEAT_STATUS_CHECK(SEAT_CHECK SC)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = SC.COMPANY_ID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("Seat Check" + JsonConvert.SerializeObject(SC));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "HD_SEAT_STATUS_CHECK");
                sp.Command.Parameters.Add("@AUR_ID", SC.AUR_ID, DbType.String);
                sp.Command.Parameters.Add("@SPC_ID", SC.SPC_ID, DbType.String);
                sp.Command.Parameters.Add("@COMPANYID", 1, DbType.Int32);
                sp.Command.Parameters.Add("@FROM_DATE", SC.FROM_DATE, DbType.DateTime);
                sp.Command.Parameters.Add("@TO_DATE", SC.TO_DATE, DbType.DateTime);
                ds = sp.GetDataSet();
                return ds.Tables[0];
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object GetMarkers(GetSpaceDetails svm)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = svm.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("GetMarkers" + JsonConvert.SerializeObject(svm));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "SMS_GET_MARKER_ALL");
                sp.Command.Parameters.Add("@AUR_ID", svm.AURID, DbType.String);
                sp.Command.Parameters.Add("@FLR_ID", svm.Floor_Id, DbType.String);
                sp.Command.Parameters.Add("@COMPANYID", 1, DbType.Int32);
                ds = sp.GetDataSet();
                return ds.Tables[0];
            }
        }
        catch(Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object GetFloorDetails(Maplistfloors main)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = main.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "GETUSERFLOORLIST");
                sp.Command.AddParameter("@AUR_ID", main.AURID, DbType.String);
                // sp.Command.AddParameter("@COMPANYID", main.COMPANYID, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();

                if (ds.Tables.Count != 0)
                    return new { data = ds.Tables[0] };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    public object GetSpaceDetailsByREQID(GetSpaceDetails svm)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = svm.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("GetSpaceDetailsByREQID" + JsonConvert.SerializeObject(svm));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "SMS_GET_SPACEDETAILS_BYREQID");
                sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
                sp.Command.Parameters.Add("@SPC_ID", svm.category, DbType.String);
                sp.Command.Parameters.Add("@REQ_ID", svm.subitem, DbType.String);
                sp.Command.Parameters.Add("@AUR_ID", svm.AURID, DbType.String);
                ds = sp.GetDataSet();
                return ds.Tables[0];

               
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object GetEmpAllocSeat(GetSpaceDetails svm)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = svm.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("GetEmpAllocSeat" + JsonConvert.SerializeObject(svm));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "SMS_MAP_EMP_ALLOC_SEAT_DETAILS");
                sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
                sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
                sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
                sp.Command.Parameters.Add("@AUR_ID", svm.Item, DbType.String);
                sp.Command.Parameters.Add("@MODE", svm.mode, DbType.String);
                ds = sp.GetDataSet();
                return ds.Tables[0];
               
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object AllocateSeats(HD_SPACE_ALLOCATE_DETAILS_LST allocDetLst)
    {
        try
        {
            HDModel mo = new HDModel();

            mo.CompanyId = allocDetLst.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("AllocateSeats" + JsonConvert.SerializeObject(allocDetLst.LST));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
               
                List<HD_SPACE_ALLOCATE_DETAILS> hdl = new List<HD_SPACE_ALLOCATE_DETAILS>();
                hdl.Add(allocDetLst.LST);

                int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
                var details = hdl.Where(x => arr.Contains(x.STACHECK)).ToList();

                String proc_name = DB + ".HD_SMS_SPACE_ALLOC_MAP";
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(details);
                param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[1].Value = allocDetLst.AUR_ID;
                param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[2].Value = 1;
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
                string Result;
                return ds.Tables[0];
                //List<HD_SPACE_ALLOCATE_DETAILS> spcdet = new List<HD_SPACE_ALLOCATE_DETAILS>();
                //HD_SPACE_ALLOCATE_DETAILS spc;
                
                //    using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, DB+"."+"SMS_SPACE_ALLOC_MAP", param))
                //{
                //    while (sdr.Read())
                //    {
                //        string REQ_ID = sdr["SSA_SRNREQ_ID"].ToString();
                //        spc = new HD_SPACE_ALLOCATE_DETAILS();
                //        spc.AUR_ID = sdr["AUR_ID"].ToString();
                //        spc.VERTICAL = sdr["VERTICAL"].ToString();
                //        spc.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                //        spc.FROM_DATE = Convert.ToDateTime(sdr["FROM_DATE"]);
                //        spc.TO_DATE = Convert.ToDateTime(sdr["TO_DATE"]);
                //        spc.SH_CODE = sdr["SH_CODE"].ToString();
                //        spc.SPC_ID = sdr["SPC_ID"].ToString();
                //        spc.SSAD_SRN_REQ_ID = sdr["SSAD_SRN_REQ_ID"].ToString();
                //        spc.SSA_SRNREQ_ID = sdr["SSA_SRNREQ_ID"].ToString();
                //        spc.STACHECK = (int)RequestState.Modified;
                //        spc.STATUS = Convert.ToInt16(sdr["STATUS"]);
                //        spc.ticked = true;
                //        spc.SHIFT_TYPE = sdr["SPACE_TYPE"].ToString();
                //        spc.emp = sdr["emp"].ToString();
                //        spcdet.Add(spc);

                //        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_EMPLOYEE_ALLOCATION_ACTION_SM");
                //        sp.Command.AddParameter("@REQID", REQ_ID, DbType.String);
                //        sp.Command.AddParameter("@CREATED_BY", spc.AUR_ID, DbType.String);
                //        sp.Command.AddParameter("@COMPANY_ID", 1, DbType.Int32);
                //        sp.ExecuteScalar();
                //    }


                //}
                //foreach (HD_SPACE_ALLOCATE_DETAILS obj in hdl.Where(x => x.STACHECK == (int)RequestState.Deleted).ToList())
                //{
                //    spc = new HD_SPACE_ALLOCATE_DETAILS();
                //    spc.SPC_ID = obj.SPC_ID;
                //    spc.STATUS = obj.STATUS;
                //    spc.ticked = true;
                //    spcdet.Add(spc);
                //}
                //if (spcdet.Count != 0)
                //    return new { Message = MessagesVM.UM_OK, data = spcdet };
                //else
                //    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

            }
            
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object GET_VERTICAL_WISE_ALLOCATIONS(GetSpaceDetails svm)
    {
        DataSet ds = new DataSet();
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = svm.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(svm));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "GET_VERTICAL_WISE_ALLOCATIONS");
                sp.Command.AddParameter("@FLR_CODE", svm.Floor_Id, DbType.String);
                ds = sp.GetDataSet();
                List<object> verticals = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[0]).Distinct().ToList();
                return new { data = verticals, data1 = ds.Tables[0], data2 = ds.Tables[1] };
            }
           
        }
        catch (Exception ex)
        {
            return new { data = (object)null };
        }
    }

    public object SubmitSpaceDetails(SubmitSpaceDetails main)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = main.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "HD_SUBMIT_SPACE_DETAILS" +"");
                sp.Command.AddParameter("@AUR_ID", main.AURID, DbType.String);
                sp.Command.AddParameter("@FROM_DATE", main.FromDate, DbType.String);
                sp.Command.AddParameter("@FROM_TIME", main.FromTime, DbType.String);
                sp.Command.AddParameter("@TO_DATE", main.ToDate, DbType.String);
                sp.Command.AddParameter("@TO_TIME", main.ToTime, DbType.String);
                sp.Command.AddParameter("@SPC_ID", main.SPC_ID, DbType.String);
                sp.Command.AddParameter("@CMP_ID", main.COMPANYID, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();

                if (ds.Tables.Count != 0)
                    return new { data = ds.Tables[0] };
                else
                    return new { Message = MessagesVM.MOBil_SPACE, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    //QRCODE Scanner Service

    public object GetAssetCodeDetails(QrCodeVariable QrCode)
    {
        try
        {
            List<QrCodeVariable> QList = new List<QrCodeVariable>();
            QrCodeVariable Qt;
            HDModel login = new HDModel();
            login.CompanyId = "GMR";
            string DB = _ReturnDB(login);
            int cnt;
            int Expiry;
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(QrCode.AssetCode));
                sp = new SubSonic.StoredProcedure(DB + "." + "API_QR_GET_ASSET_DETAILS");
                sp.Command.AddParameter("@AssetCode", QrCode.AssetCode, DbType.String);
                using (IDataReader sdr = sp.GetReader())
                {
                    while (sdr.Read())
                    {
                        Qt = new QrCodeVariable();
                        Qt.AssetID = sdr["AssetID"].ToString();
                        Qt.AssetName = sdr["AssetName"].ToString();
                        Qt.Location = sdr["Location"].ToString();
                        Qt.Brand = sdr["Brand"].ToString();
                        Qt.Model = sdr["Model"].ToString();
                        Qt.State = sdr["STATE"].ToString();
                        Qt.SerialNumber = sdr["AAT_AST_SERIALNO"].ToString();
                        Qt.AssignedTo = sdr["AAT_TAG_STATUS"].ToString();
                        Qt.ActucalCost = sdr["ActucalCost"].ToString();
                        Qt.AdditionalCost = sdr["AdditionalCost"].ToString();
                        Qt.LastPPMDate = sdr["LastPPMDate"].ToString();
                        Qt.LabourCost = sdr["LabourCost"].ToString();
                        Qt.SparePartsCost = sdr["SparePartsCost"].ToString();
                        Qt.BreakdownStatus = sdr["BreakdownStatus"].ToString();
                        Qt.BreakdownNumbers = sdr["BreakdownNumbers"].ToString();
                        Qt.ImagePath = sdr["ImagePath"].ToString();
                        QList.Add(Qt);
                    }
                }
            }
            return new { Status = "true", Data = QList };


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    // Get Mobile Menu

    public object GetMainMenu(HDModel main)
    {
        try
        {
            List<MainMenuItems> MList = new List<MainMenuItems>();
            MainMenuItems Mt;
            SubMenuItems st;
            //HDModel login = new HDModel();
            //login.CompanyId = "ABC_UAT";
            string DB = _ReturnDB(main);
            int cnt;
            int Expiry;
            ErrorHandler err2 = new ErrorHandler();
            err2._WriteErrorLog_string("inside" + JsonConvert.SerializeObject(main.UserId));
            ErrorHandler err = new ErrorHandler();
            err._WriteErrorLog_string("DatabaseCheck" + JsonConvert.SerializeObject(DB));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
                sp = new SubSonic.StoredProcedure(DB + "." + "GET_MAIN_MENU_ITEMS");
                //sp.Command.AddParameter("@AssetCode", main.Aurid, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Mt = new MainMenuItems();
                    Mt.MOD_ID = ds.Tables[0].Rows[i]["MOD_ID"].ToString();
                    Mt.MOD_NAME = ds.Tables[0].Rows[i]["MOD_NAME"].ToString();
                    Mt.ImagePath = ds.Tables[0].Rows[i]["MOD_IMG"].ToString();
                    Mt.SubMenu = new List<SubMenuItems>();
                    for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                    {

                        if (ds.Tables[0].Rows[i]["MOD_ID"].ToString() == ds.Tables[1].Rows[j]["MAIN_MOD_ID"].ToString())
                        {
                            st = new SubMenuItems();

                            st.SUB_MOD_ID = ds.Tables[1].Rows[j]["SUB_MOD_ID"].ToString();
                            st.SUB_MOD_NAME = ds.Tables[1].Rows[j]["SUB_MOD_NAME"].ToString();
                            st.SUB_MOD_IMG = ds.Tables[1].Rows[j]["SUB_MOD_IMG"].ToString();
                            st.SUB_MOD_PAGE_PATH = ds.Tables[1].Rows[j]["SUB_MOD_PAGE_PATH"].ToString();
                            st.MAIN_MOD_ID = ds.Tables[1].Rows[j]["MAIN_MOD_ID"].ToString();

                            Mt.SubMenu.Add(st);
                        }
                    }
                    MList.Add(Mt);
                }
                //using (IDataReader sdr = sp.GetReader())
                //{
                //    while (sdr.Read())
                //    {
                //        Mt = new MainMenuItems();
                //        Mt.MOD_ID = sdr["MOD_ID"].ToString();
                //        Mt.MOD_NAME = sdr["MOD_NAME"].ToString();
                //        Mt.ImagePath = sdr["MOD_IMG"].ToString();
                //        if (sdr["SUB_MOD_ID"].ToString() != "")
                //        {
                //            st = new SubMenuItems();
                //            st.SUB_MOD_ID = sdr["SUB_MOD_ID"].ToString();
                //            st.SUB_MOD_NAME = sdr["SUB_MOD_NAME"].ToString();
                //            st.SUB_MOD_IMG = sdr["SUB_MOD_IMG"].ToString();
                //            st.MAIN_MOD_ID = sdr["MOD_ID"].ToString();
                //            Mt.SubMenu = new List<SubMenuItems>();
                //            Mt.SubMenu.Add(st);
                //        }
                //        //Qt.Location = sdr["Location"].ToString();
                //        //Qt.Brand = sdr["Brand"].ToString();
                //        //Qt.Model = sdr["Model"].ToString();
                //        MList.Add(Mt);
                //    }
                //}
            }
            return new { Status = "true", Data = MList };


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //Get Menu By Role
    public object GetMenuByRole(MenuByRole main)
    {
        try
        {
            List<MainMenuItems> MList = new List<MainMenuItems>();
            MainMenuItems Mt;
            SubMenuItems st;
            HDModel login = new HDModel();
            login.CompanyId = main.CompanyId;
            string DB = _ReturnDB(login);
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "GET_ROLE_BASED_MENU");
                sp.Command.AddParameter("@Rol_id", main.RoleId, DbType.String);
                sp.Command.AddParameter("@MOD_ID", main.MOD_ID, DbType.Int32);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                return new { MenuListByRole = ds.Tables[0] };
            }



        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    //Get Main Menu By Role
    public object GetMainMenuByRole(MenuByRole main)
    {
        try
        {
            List<MainMenuItems> MList = new List<MainMenuItems>();
            MainMenuItems Mt;
            SubMenuItems st;
            HDModel login = new HDModel();
            login.CompanyId = main.CompanyId;
            string DB = _ReturnDB(login);
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "GET_ROLE_BASED_MAIN_MENU");
                sp.Command.AddParameter("@Rol_id", main.RoleId, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                return new { MenuListByRole = ds.Tables[0] };
            }



        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    //Multiple Image Save
    public object UploadMultipleImages(HttpRequest httpRequest)
    {
        try
        {
            for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
            {
                HDModel hd = new HDModel();
                hd.CompanyId = httpRequest.Params["CompanyId"];
                string DB = _ReturnDB(hd);
                String LCM_CODE = httpRequest.Params["LCM_CODE"];
                String INSP_BY = httpRequest.Params["INSP_BY"];
                String INSP_DT = httpRequest.Params["INSP_DT"];
                String AUR_ID = httpRequest.Params["AUR_ID"];
                var req = httpRequest.Files[i];
                var fn = req.FileName;

                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(DB + "." + "API_HD_INSERT_CHECKLIST_FILES");
                sp2.Command.AddParameter("@LCM_CODE", LCM_CODE, DbType.String);
                sp2.Command.AddParameter("@INSP_BY", INSP_BY, DbType.String);
                sp2.Command.AddParameter("@INSP_DT", INSP_DT, DbType.String);
                sp2.Command.AddParameter("@FILENAME", fn, DbType.String);
                sp2.Command.AddParameter("@UPLOAD_PATH", (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn, DbType.String);
                sp2.Command.AddParameter("@AUR_ID", AUR_ID, DbType.String);
                sp2.ExecuteScalar();
                var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "BranchCheckList\\CheckListCreation\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);
                req.SaveAs(filePath);
            }
            string Result;
            return new { Result = "Success" };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    //Insert Contact Details


    //public object InsertContactDetails(HD_INSERT_CONTACT_DETAILS reldet)
    //{
    //    try
    //    {

    //        HDModel hd = new HDModel();
    //        hd.CompanyId = reldet.CompanyId;
    //        string DB = _ReturnDB(hd);
    //        SqlParameter[] param = new SqlParameter[4];
    //        param[0] = new SqlParameter("@FNAME", SqlDbType.Structured);
    //        param[0].Value = reldet.Full_Name;
    //        param[1] = new SqlParameter("@CNAME", SqlDbType.NVarChar);
    //        param[1].Value = reldet.Company_Name;
    //        param[2] = new SqlParameter("@EMAIL", SqlDbType.NVarChar);
    //        param[2].Value = reldet.Email;
    //        param[3] = new SqlParameter("@COUNTRY", SqlDbType.NVarChar);
    //        param[3].Value = reldet.Country;
    //        param[4] = new SqlParameter("@PHONENUM", SqlDbType.NVarChar);
    //        param[4].Value = reldet.PhoneNo;

    //        int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, DB + ".HD_INSERT_CONTACT_DET", param);

    //        if (RETVAL == 1)
    //            return new { Message = MessagesVM.UM_OK, data = reldet.Email };
    //        else
    //            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorHandler erhndlr = new ErrorHandler();
    //        erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };

    //    }


    //}
    //Space Release Details

    public object ReleaseSelectedseat(HD_SPACE_REL_DETAILS reldet)
    {
        try
        {
           
            HDModel hd = new HDModel();
            hd.CompanyId = reldet.CompanyId;
            string DB = _ReturnDB(hd);
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@REL_DET", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(reldet.sad);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value =reldet.User_Id;
            param[2] = new SqlParameter("@RELTYPE", SqlDbType.NVarChar);
            param[2].Value = reldet.reltype;
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[3].Value =1;

            int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, DB+".SMS_SPACE_REL_MAP", param);

            if (RETVAL == 1)
                return new { Message = MessagesVM.UM_OK, data = reldet.reltype };
            else
                return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
        catch(Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };

        }


    }
    public Object GetBookedSeatsByUser(HD_SPACE_ALLOCATE_DETAILS_LST svm)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = svm.COMPANYID;
            string DB = _ReturnDB(mo);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "MOB_SPACE_SEAT_BOOKING_HISTORY");
            sp.Command.Parameters.Add("@USER", svm.AUR_ID, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    //Space Map Details
    public object GetBindMapDetails(SpaceMapDet svm)
    {
        try
        {
            HDModel hd = new HDModel();
            hd.CompanyId = svm.CompanyId;
            string DB = _ReturnDB(hd);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(hd.UserId));
            DataSet ds = new DataSet();
            if (svm.key_value == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "SMS_GET_FLOORMAPS_View_In_Map");
                sp.Command.Parameters.Add("@AUR_ID", svm.User_Id, DbType.String);
                sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
                ds = sp.GetDataSet();
                Hashtable sendData = new Hashtable();

                foreach (DataRow drIn in ds.Tables[1].Rows)
                {
                    sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
                }

                return new { mapDetails = ds.Tables[0], FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[2] };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "HD_SMS_GET_FLOORMAPS");
                sp.Command.Parameters.Add("@AUR_ID", svm.User_Id, DbType.String);
                sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
                ds = sp.GetDataSet();
                Hashtable sendData = new Hashtable();

                foreach (DataRow drIn in ds.Tables[2].Rows)
                {
                    sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
                }

                return new { mapDetails = ds.Tables[0], mapDetails2 = ds.Tables[1], FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[3] };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    // ProjectCheckList

    public object GetProjectType(HDModel main)
    {
        try
        {
            string DB = _ReturnDB(main);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "PCL_GET_PROJECTTYPES");
                ds = sp.GetDataSet();
                if (ds.Tables.Count != 0)
                    return new { ProjectType = ds.Tables[0] };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public List<Modules> GetModuleList(HDModel main)
    {
        List<Modules> ModList = new List<Modules>();
        string DB = _ReturnDB(main);
        Modules MOD;
        int i = 0;
        try

        {
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_ASSET_MODULE_CHECK_FOR_USER1");

            sp.Command.AddParameter("@TENANT_ID", main.CompanyId + ".dbo", DbType.String);

            DataSet ds = new DataSet();
            ds = sp.GetDataSet();

            for (i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                MOD = new Modules();
                MOD.TNAME = ds.Tables[0].Rows[i]["TNAME"].ToString();
                MOD.T_MODULE = ds.Tables[0].Rows[i]["T_MODULE"].ToString();
                ModList.Add(MOD);
            }
            return ModList;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return ModList;
        }
    }
    public object GetProjectCheckList(GetProjectList main)
    {
        try
        {
            int i = 0;
            List<MainMenuItems> MList = new List<MainMenuItems>();
            List<ProjectCheckList> PCList = new List<ProjectCheckList>();
            ProjectCheckList PCL;
            List<childList> childlist = new List<childList>();
            childList cl;
            List<Subchildcategory> scorelist = new List<Subchildcategory>();
            Subchildcategory sc;
            List<subchildcat1> subchildlist = new List<subchildcat1>();
            subchildcat1 scc1;
            MainMenuItems Mt;
            SubMenuItems st;
            TextBoxValues txb;

            HDModel login = new HDModel();
            login.CompanyId = main.CompanyId;
            string DB = _ReturnDB(login);
            int cnt;
            int Expiry;
            ErrorHandler err2 = new ErrorHandler();
            err2._WriteErrorLog_string("inside" + JsonConvert.SerializeObject(main.UserId));
            ErrorHandler err = new ErrorHandler();
            err._WriteErrorLog_string("DatabaseCheck" + JsonConvert.SerializeObject(DB));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
                sp = new SubSonic.StoredProcedure(DB + "." + "GET_PROJECT_CHECKLIST_DETAILS");
                sp.Command.AddParameter("@PROJECTID", main.PTCode, DbType.Int32);
                //sp.Command.AddParameter("@InspectedDate", "", DbType.String);
                //sp.Command.AddParameter("@AUR_ID", main.UserId, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                int childcount = 0;
                PCL = new ProjectCheckList();
                for (i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (i != 0)
                    {
                        if (ds.Tables[0].Rows[i]["Subcategory_Code"].ToString() != ds.Tables[0].Rows[i - 1]["Subcategory_Code"].ToString())
                        {
                            PCList.Add(PCL);
                            childlist = new List<childList>();
                            PCL = new ProjectCheckList();
                        }
                    }
                    PCL.Category = ds.Tables[0].Rows[i]["Category"].ToString();
                    PCL.CatCode = ds.Tables[0].Rows[i]["Category_code"].ToString();
                    PCL.SubCatCode = ds.Tables[0].Rows[i]["Subcategory_Code"].ToString();
                    PCL.SubCategory = ds.Tables[0].Rows[i]["SubCategory"].ToString();
                    PCL.Mandatory = ds.Tables[0].Rows[i]["Mandatory"].ToString();
                    if (i == 0)
                    {
                        if (childcount == 0)
                        {
                            childlist = new List<childList>();
                            childcount++;
                        }
                        cl = new childList();
                        cl.ChildCode = ds.Tables[0].Rows[i]["ChildCategoryCode"].ToString();
                        cl.ChildCategory = ds.Tables[0].Rows[i]["ChildCategory"].ToString();
                        cl.childfiled = ds.Tables[0].Rows[i]["childfiled"].ToString();

                        scorelist = new List<Subchildcategory>();
                        sc = new Subchildcategory();

                        subchildlist = new List<subchildcat1>();


                        string a = ds.Tables[0].Rows[i]["PM_CL_SCC_CODE"].ToString();
                        string b = ds.Tables[0].Rows[i]["PM_CL_SCC_NAME"].ToString();
                        string c = ds.Tables[0].Rows[i]["PM_CL_SCC1_CODE"].ToString();
                        string d = ds.Tables[0].Rows[i]["PM_CL_SCC1_NAME"].ToString();
                        string e = ds.Tables[0].Rows[i]["PM_SCC1_R_C_T"].ToString();
                        string[] authorsList = a.Split(',');
                        string[] authorsList1 = b.Split(',');
                        string[] subchildcatList = c.Split(',');
                        string[] subchildcatListName = d.Split(',');
                        string[] subchildcatListfield = e.Split(',');
                        int j = 0, h = 0;
                        foreach (string author in authorsList)
                        {
                            sc = new Subchildcategory();
                            sc.ScoreCode = author;
                            sc.ScoreName = authorsList1[j];
                            sc.ScoreText = ds.Tables[0].Rows[i]["PM_CL_SCC_RC"].ToString();
                            int z = 0;
                            if (ds.Tables[0].Rows[i]["PM_CL_SCC1_CODE"].ToString() != "")
                            {
                                for (int ss = 0; ss < 2; ss++)
                                {
                                    scc1 = new subchildcat1();
                                    scc1.SubchildcatCode = subchildcatList[h];
                                    scc1.Subchildcatname = subchildcatListName[h];
                                    scc1.Subchildcatfield = subchildcatListfield[h];
                                    subchildlist.Add(scc1);
                                    h++;
                                }
                            }

                            sc.subchildcat1 = subchildlist;
                            scorelist.Add(sc);
                            j++;
                        }
                        cl.Subchildcategory = scorelist;
                        childlist.Add(cl);
                        PCL.childlist = childlist;
                    }
                    else if (ds.Tables[0].Rows[i]["Subcategory_Code"].ToString() == ds.Tables[0].Rows[i - 1]["Subcategory_Code"].ToString())
                    {
                        if (childcount == 0)
                        {
                            PCL.childlist = new List<childList>();
                            childcount++;
                        }
                        cl = new childList();
                        cl.ChildCode = ds.Tables[0].Rows[i]["ChildCategoryCode"].ToString();
                        cl.ChildCategory = ds.Tables[0].Rows[i]["ChildCategory"].ToString();
                        cl.childfiled = ds.Tables[0].Rows[i]["childfiled"].ToString();

                        scorelist = new List<Subchildcategory>();
                        sc = new Subchildcategory();

                        subchildlist = new List<subchildcat1>();
                        scc1 = new subchildcat1();
                        string a = ds.Tables[0].Rows[i]["PM_CL_SCC_CODE"].ToString();
                        string b = ds.Tables[0].Rows[i]["PM_CL_SCC_NAME"].ToString();
                        string c = ds.Tables[0].Rows[i]["PM_CL_SCC1_CODE"].ToString();
                        string d = ds.Tables[0].Rows[i]["PM_CL_SCC1_NAME"].ToString();
                        string e = ds.Tables[0].Rows[i]["PM_SCC1_R_C_T"].ToString();
                        string[] authorsList = a.Split(',');
                        string[] authorsList1 = b.Split(',');
                        string[] subchildcatList = c.Split(',');
                        string[] subchildcatListName = d.Split(',');
                        string[] subchildcatListfield = e.Split(',');
                        int j = 0, h = 0;
                        foreach (string author in authorsList)
                        {
                            sc = new Subchildcategory();
                            sc.ScoreCode = author;
                            sc.ScoreName = authorsList1[j];
                            sc.ScoreText = ds.Tables[0].Rows[i]["PM_CL_SCC_RC"].ToString();
                            int z = 0;
                            if (ds.Tables[0].Rows[i]["PM_CL_SCC1_CODE"].ToString() != "")
                            {
                                for (int ss = 0; ss < 2; ss++)
                                {
                                    if (subchildcatList.Length != h)
                                    {
                                        scc1 = new subchildcat1();
                                        scc1.SubchildcatCode = subchildcatList[h];
                                        scc1.Subchildcatname = subchildcatListName[h];
                                        scc1.Subchildcatfield = subchildcatListfield[h];
                                        subchildlist.Add(scc1);
                                        h++;
                                    }
                                }
                            }

                            sc.subchildcat1 = subchildlist;
                            scorelist.Add(sc);
                            subchildlist = new List<subchildcat1>();
                            j++;
                        }
                        cl.Subchildcategory = scorelist;
                        childlist.Add(cl);
                        PCL.childlist = childlist;
                    }
                    else
                    {
                        childcount = 0;
                        if (childcount == 0)
                        {
                            PCL.childlist = new List<childList>();
                            childcount++;
                        }
                        cl = new childList();
                        cl.ChildCode = ds.Tables[0].Rows[i]["ChildCategoryCode"].ToString();
                        cl.ChildCategory = ds.Tables[0].Rows[i]["ChildCategory"].ToString();
                        cl.childfiled = ds.Tables[0].Rows[i]["childfiled"].ToString();
                        scorelist = new List<Subchildcategory>();
                        sc = new Subchildcategory();

                        subchildlist = new List<subchildcat1>();
                        scc1 = new subchildcat1();
                        string a = ds.Tables[0].Rows[i]["PM_CL_SCC_CODE"].ToString();
                        string b = ds.Tables[0].Rows[i]["PM_CL_SCC_NAME"].ToString();
                        string c = ds.Tables[0].Rows[i]["PM_CL_SCC1_CODE"].ToString();
                        string[] authorsList = a.Split(',');
                        string[] authorsList1 = b.Split(',');
                        string[] subchildcatList = c.Split(',');
                        int j = 0;
                        foreach (string author in authorsList)
                        {
                            sc = new Subchildcategory();
                            sc.ScoreCode = author;
                            sc.ScoreName = authorsList1[j];
                            sc.ScoreText = ds.Tables[0].Rows[i]["PM_CL_SCC_RC"].ToString();
                            scorelist.Add(sc);
                            j++;
                        }
                        cl.Subchildcategory = scorelist;
                        childlist.Add(cl);
                        PCL.childlist = childlist;
                    }
                }
                if (i == ds.Tables[0].Rows.Count)
                {
                    PCList.Add(PCL);
                }
                return new { Status = "true", Data = PCList };
            }


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object InsertPPSaveAsDraftData(ProposalDraftDetails dataobj)
    {
        try
        {
            List<ImageClas> Imgclass = new List<ImageClas>();
            List<ImageClas> Imgclass1 = new List<ImageClas>();
            List<ImageClas> Imgclass2 = new List<ImageClas>();
            List<ImageClas> Imgclass3 = new List<ImageClas>();
            HDModel hd = new HDModel();
            hd.CompanyId = dataobj.CompanyId;
            string DB = _ReturnDB(hd);
            
                String proc_name = DB + ".PM_PROPERTY_SAVEASDRAFT_DETAILS_MOBILE";
            SqlParameter[] param = new SqlParameter[132];
            param[0] = new SqlParameter("@REQUEST_ID", SqlDbType.VarChar);
            param[0].Value = dataobj.PM_PPT_PM_REQ_ID;
            param[1] = new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar);
            param[1].Value = dataobj.PM_PPT_REQ_TYPE;
            param[2] = new SqlParameter("@PROP_NATURE", SqlDbType.VarChar);
            param[2].Value = dataobj.PM_PPT_NATURE;
            param[3] = new SqlParameter("@ACQ_TRH", SqlDbType.VarChar);
            param[3].Value = dataobj.PM_PPT_ACQ_THR;
            param[4] = new SqlParameter("@CITY", SqlDbType.VarChar);
            param[4].Value = dataobj.PM_PPT_CTY_CODE;               
            param[5] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
            param[5].Value = dataobj.PM_PPT_LOC_CODE;
            param[6] = new SqlParameter("@TOWER", SqlDbType.VarChar);
            param[6].Value = dataobj.PM_PPT_TOW_CODE;
            param[7] = new SqlParameter("@FLOOR", SqlDbType.VarChar);
            param[7].Value = dataobj.PM_PPT_FLR_CODE; 
            param[8] = new SqlParameter("@TOI_BLKS", SqlDbType.Int);
            param[8].Value = dataobj.PM_PPT_TOT_TOI;
            param[9] = new SqlParameter("@PROP_TYPE", SqlDbType.VarChar);
            param[9].Value = dataobj.PM_PPT_TYPE;
            param[10] = new SqlParameter("@PROP_NAME", SqlDbType.VarChar);
            param[10].Value = dataobj.PM_PPT_NAME;
            param[11] = new SqlParameter("@ESTD_YR", SqlDbType.Date);
            param[11].Value = dataobj.PM_PPT_ESTD;
            param[12] = new SqlParameter("@AGE", SqlDbType.VarChar);
            param[12].Value = dataobj.PM_PPT_AGE;
            param[13] = new SqlParameter("@PROP_ADDR", SqlDbType.VarChar);
            param[13].Value = dataobj.PM_PPT_ADDRESS;
            param[14] = new SqlParameter("@SING_LOC", SqlDbType.VarChar);
            param[14].Value = dataobj.PM_PPT_SIGN_LOC;
            param[15] = new SqlParameter("@SOC_NAME", SqlDbType.VarChar);
            param[15].Value = dataobj.PM_PPT_SOC_NAME;
            param[16] = new SqlParameter("@LATITUDE", SqlDbType.VarChar);
            param[16].Value = dataobj.PM_PPT_LAT;
            param[17] = new SqlParameter("@LONGITUDE", SqlDbType.VarChar);
            param[17].Value = dataobj.PM_PPT_LONG;
            param[18] = new SqlParameter("@SCOPE_WK", SqlDbType.VarChar);
            param[18].Value = dataobj.PM_PPT_OWN_SCOPE;
            param[19] = new SqlParameter("@RECM_PROP", SqlDbType.VarChar);
            param[19].Value = dataobj.PM_PPT_RECOMMENDED;
            param[20] = new SqlParameter("@RECM_PROP_REM", SqlDbType.VarChar);
            param[20].Value = dataobj.PM_PPT_RECO_REM;
            //Owner Details
            param[21] = new SqlParameter("@OWN_NAME", SqlDbType.VarChar);
            param[21].Value = dataobj.PM_OWN_NAME;
            param[22] = new SqlParameter("@OWN_PH", SqlDbType.VarChar);
            param[22].Value = dataobj.PM_OWN_PH_NO;
            param[23] = new SqlParameter("@PREV_OWN_NAME", SqlDbType.VarChar);
            param[23].Value = dataobj.PM_PREV_OWN_NAME;
            param[24] = new SqlParameter("@PREV_OWN_PH", SqlDbType.VarChar);
            param[24].Value = dataobj.PM_PREV_OWN_PH_NO;
            param[25] = new SqlParameter("@OWN_EMAIL", SqlDbType.VarChar);
            param[25].Value = dataobj.PM_OWN_EMAIL;          
            //Area Details
            param[26] = new SqlParameter("@CARPET_AREA", SqlDbType.Decimal);
            param[26].Value = Convert.ToDecimal(dataobj.PM_AR_CARPET_AREA);
            param[27] = new SqlParameter("@BUILTUP_AREA", SqlDbType.Decimal);
            param[27].Value = Convert.ToDecimal(dataobj.PM_AR_BUA_AREA);
            param[28] = new SqlParameter("@COMMON_AREA", SqlDbType.Decimal);
            param[28].Value = Convert.ToDecimal(dataobj.PM_AR_COM_AREA);
            param[29] = new SqlParameter("@RENTABLE_AREA", SqlDbType.Decimal);
            param[29].Value = Convert.ToDecimal(dataobj.PM_AR_RENT_AREA);
            param[30] = new SqlParameter("@USABLE_AREA", SqlDbType.Decimal);
            param[30].Value = Convert.ToDecimal(dataobj.PM_AR_USABEL_AREA);
            param[31] = new SqlParameter("@SUPER_BLT_AREA", SqlDbType.Decimal);
            param[31].Value = Convert.ToDecimal(dataobj.PM_AR_SBU_AREA);
            param[32] = new SqlParameter("@PLOT_AREA", SqlDbType.Decimal);
            param[32].Value = Convert.ToDecimal(dataobj.PM_AR_PLOT_AREA);
            param[33] = new SqlParameter("@FTC_HIGHT", SqlDbType.Decimal);
            param[33].Value = dataobj.PM_AR_FTC_HIGHT;
            param[34] = new SqlParameter("@FTBB_HIGHT", SqlDbType.Decimal);
            param[34].Value = dataobj.PM_AR_FTBB_HIGHT;
            param[35] = new SqlParameter("@MAX_CAPACITY", SqlDbType.Int);
            param[35].Value = Convert.ToInt32(dataobj.PM_AR_MAX_CAP);
            param[36] = new SqlParameter("@OPTIMUM_CAPACITY", SqlDbType.Int);
            param[36].Value = dataobj.PM_AR_OPT_CAP;
            param[37] = new SqlParameter("@SEATING_CAPACITY", SqlDbType.Int);
            param[37].Value = dataobj.PM_AR_SEATING_CAP;
            param[38] = new SqlParameter("@FLR_TYPE", SqlDbType.VarChar);
            param[38].Value = dataobj.PM_AR_FLOOR_TYPE;
            param[39] = new SqlParameter("@FSI", SqlDbType.VarChar);
            param[39].Value = dataobj.PM_AR_FSI;
            param[40] = new SqlParameter("@FSI_RATIO", SqlDbType.Decimal);
            param[40].Value = dataobj.PM_AR_FSI_RATIO;
            param[41] = new SqlParameter("@PFR_EFF", SqlDbType.VarChar);
            param[41].Value = dataobj.PM_AR_PREF_EFF;
            //PURCHASE DETAILS
            param[42] = new SqlParameter("@PUR_PRICE", SqlDbType.Decimal);
            param[42].Value = dataobj.PM_PUR_PRICE;
            param[43] = new SqlParameter("@PUR_DATE", SqlDbType.Date);
            param[43].Value = dataobj.PM_PUR_DATE;
            param[44] = new SqlParameter("@MARK_VALUE", SqlDbType.Decimal);
            param[44].Value = dataobj.PM_PUR_MARKET_VALUE;
            //GOVT DETAILS
            param[45] = new SqlParameter("@IRDA", SqlDbType.VarChar);
            param[45].Value = dataobj.PM_GOV_IRDA;
            param[46] = new SqlParameter("@PC_CODE", SqlDbType.VarChar);
            param[46].Value = dataobj.PM_GOV_PC_CODE;
            param[47] = new SqlParameter("@PROP_CODE", SqlDbType.VarChar);
            param[47].Value = dataobj.PM_GOV_PROP_CODE;
            param[48] = new SqlParameter("@UOM_CODE", SqlDbType.VarChar);
            param[48].Value = dataobj.PM_GOV_UOM_CODE;
            //INSURANCE DETAILS
            param[49] = new SqlParameter("@IN_TYPE", SqlDbType.VarChar);
            param[49].Value = dataobj.PM_INS_TYPE;
            param[50] = new SqlParameter("@IN_VENDOR", SqlDbType.VarChar);
            param[50].Value = dataobj.PM_INS_VENDOR;
            param[51] = new SqlParameter("@IN_AMOUNT", SqlDbType.Decimal);
            param[51].Value = dataobj.PM_INS_AMOUNT;
            param[52] = new SqlParameter("@IN_POLICY_NUMBER", SqlDbType.VarChar);
            param[52].Value = dataobj.PM_INS_PNO;
            param[53] = new SqlParameter("@IN_SDATE", SqlDbType.VarChar);
            param[53].Value = dataobj.PM_INS_START_DT;
            param[54] = new SqlParameter("@IN_EDATE", SqlDbType.VarChar);
            param[54].Value = dataobj.PM_INS_END_DT;
            param[55] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[55].Value = dataobj.AUR_ID;
            param[56] = new SqlParameter("@CMP_ID", SqlDbType.VarChar);
            param[56].Value = 1;
            param[57] = new SqlParameter("@OWN_LANDLINE", SqlDbType.VarChar);
            param[57].Value = dataobj.PM_OWN_LANDLINE;
            param[58] = new SqlParameter("@POA_SIGNED", SqlDbType.VarChar);
            param[58].Value = dataobj.PM_POA_SIGN;
            //POA Details
            param[59] = new SqlParameter("@POA_NAME", SqlDbType.VarChar);
            param[59].Value = dataobj.PM_POA_NAME;
            param[60] = new SqlParameter("@POA_ADDRESS", SqlDbType.VarChar);
            param[60].Value = dataobj.PM_POA_ADDRESS;
            param[61] = new SqlParameter("@POA_MOBILE", SqlDbType.VarChar);
            param[61].Value = dataobj.PM_POA_MOBILE;
            param[62] = new SqlParameter("@POA_EMAIL", SqlDbType.VarChar);
            param[62].Value = dataobj.PM_POA_EMAIL;
            param[63] = new SqlParameter("@POA_LLTYPE", SqlDbType.VarChar);
            param[63].Value = dataobj.PM_POA_LL_TYPE;
            param[64] = new SqlParameter("@IMAGES", SqlDbType.VarChar);
            param[64].Value = "";
            param[65] = new SqlParameter("@ENTITY", SqlDbType.VarChar);
            param[65].Value = dataobj.PM_PPT_ENTITY;
            //Physical Condition
            param[66] = new SqlParameter("@PHYCDTN_WALLS", SqlDbType.VarChar);
            param[66].Value = dataobj.PM_PHYCDTN_WALLS;
            param[67] = new SqlParameter("@PHYCDTN_ROOF", SqlDbType.VarChar);
            param[67].Value = dataobj.PM_PHYCDTN_ROOF;
            param[68] = new SqlParameter("@PHYCDTN_CEILING", SqlDbType.VarChar);
            param[68].Value = dataobj.PM_PHYCDTN_CEILING;
            param[69] = new SqlParameter("@PHYCDTN_WINDOWS", SqlDbType.VarChar);
            param[69].Value = dataobj.PM_PHYCDTN_WINDOWS;
            param[70] = new SqlParameter("@PHYCDTN_DAMAGE", SqlDbType.VarChar);
            param[70].Value = dataobj.PM_PHYCDTN_DAMAGE;
            param[71] = new SqlParameter("@PHYCDTN_SEEPAGE", SqlDbType.VarChar);
            param[71].Value = dataobj.PM_PHYCDTN_SEEPAGE;                                                   
            param[72] = new SqlParameter("@PHYCDTN_OTHRTNT", SqlDbType.VarChar);
            param[72].Value = dataobj.PM_PHYCDTN_OTHR_TNT;
        
            //Landlord's Scope of work
            param[73] = new SqlParameter("@LL_VITRIFIED", SqlDbType.VarChar);
            param[73].Value = dataobj.PM_LLS_VITRIFIED;
            param[74] = new SqlParameter("@LL_VITRIFIED_RMKS", SqlDbType.VarChar);
            param[74].Value = dataobj.PM_LLS_VITRIFIED_RMKS;
            param[75] = new SqlParameter("@LL_WASHRMS", SqlDbType.VarChar);
            param[75].Value = dataobj.PM_LLS_WASHROOMS;
            param[76] = new SqlParameter("@LL_WASHRM_RMKS", SqlDbType.VarChar);
            param[76].Value = dataobj.PM_LLS_WASH_RMKS;
            param[77] = new SqlParameter("@LL_PANTRY", SqlDbType.VarChar);
            param[77].Value = dataobj.PM_LLS_PANTRY;
            param[78] = new SqlParameter("@LL_PANTRY_RMKS", SqlDbType.VarChar);
            param[78].Value = dataobj.PM_LLS_PANTRY_RMKS;
            param[79] = new SqlParameter("@LL_SHUTTER", SqlDbType.VarChar);
            param[79].Value = dataobj.PM_LLS_SHUTTER;
            param[80] = new SqlParameter("@LL_SHUTTER_RMKS", SqlDbType.VarChar);
            param[80].Value = dataobj.PM_LLS_SHUTTER_RMKS;
            param[81] = new SqlParameter("@LL_OTHERS", SqlDbType.VarChar);
            param[81].Value = dataobj.PM_LLS_OTHERS;
            param[82] = new SqlParameter("@LL_OTHERS_RMKS", SqlDbType.VarChar);
            param[82].Value = dataobj.PM_LLS_OTHERS_RMKS;
            param[83] = new SqlParameter("@LL_WORK_DAYS", SqlDbType.VarChar);
            param[83].Value = dataobj.PM_LLS_WORK_DAYS;
            param[84] = new SqlParameter("@LL_ELEC_EXISTING", SqlDbType.VarChar);
            param[84].Value = dataobj.PM_LLS_ELEC_EXISTING;
            param[85] = new SqlParameter("@LL_ELEC_REQUIRED", SqlDbType.VarChar);
            param[85].Value = dataobj.PM_LLS_ELEC_REQUIRED;
            //Other Details
            param[86] = new SqlParameter("@DOC_TAX", SqlDbType.VarChar);
            param[86].Value = dataobj.PM_DOC_TAX;
            param[87] = new SqlParameter("@DOC_TAX_RMKS", SqlDbType.VarChar);
            param[87].Value = dataobj.PM_DOC_TAX_RMKS;
            param[88] = new SqlParameter("@DOC_OTHR_INFO", SqlDbType.VarChar);
            param[88].Value = dataobj.PM_DOC_OTHER_INFO;
            param[89] = new SqlParameter("@OTHR_FLOORING", SqlDbType.VarChar);
            param[89].Value = dataobj.PM_OTHR_FLOORING;
            param[90] = new SqlParameter("@OTHR_FLRG_RMKS", SqlDbType.VarChar);
            param[90].Value = dataobj.PM_OTHR_FLRG_RMKS;
            param[91] = new SqlParameter("@OTHR_WASH_EXISTING", SqlDbType.VarChar);
            param[91].Value = dataobj.PM_OTHR_WASHRM_EXISTING;
            param[92] = new SqlParameter("@OTHR_WASH_REQUIRED", SqlDbType.VarChar);
            param[92].Value = dataobj.PM_OTHR_WASHRM_REQUIRED;
            param[93] = new SqlParameter("@OTHR_POTABLE_WTR", SqlDbType.VarChar);
            param[93].Value = dataobj.PM_OTHR_POTABLE_WTR;
           
            //Cost Details
            param[94] = new SqlParameter("@COST_RENT", SqlDbType.VarChar);
            param[94].Value = dataobj.PM_COST_RENT;
            param[95] = new SqlParameter("@COST_RENT_SFT", SqlDbType.VarChar);
            param[95].Value = dataobj.PM_COST_RENT_SFT;
            param[96] = new SqlParameter("@COST_RATIO", SqlDbType.VarChar);
            param[96].Value = dataobj.PM_COST_RATIO;
            param[97] = new SqlParameter("@COST_OWN_SHARE", SqlDbType.VarChar);
            param[97].Value = dataobj.PM_COST_OWN_SHARE;
            param[98] = new SqlParameter("@COST_SECDEP", SqlDbType.VarChar);
            param[98].Value = dataobj.PM_COST_SECDEPOSIT;
            param[99] = new SqlParameter("@COST_GST", SqlDbType.VarChar);
            param[99].Value = dataobj.PM_COST_GST;
            param[100] = new SqlParameter("@COST_MAINTENANCE", SqlDbType.VarChar);
            param[100].Value = dataobj.PM_COST_MAINTENANCE;
            param[101] = new SqlParameter("@COST_ESC", SqlDbType.VarChar);
            param[101].Value = dataobj.PM_COST_ESC_RENTALS;
            param[102] = new SqlParameter("@COST_RENT_FREE", SqlDbType.VarChar);
            param[102].Value = dataobj.PM_COST_RENT_FREE;
            param[103] = new SqlParameter("@COST_STAMP", SqlDbType.VarChar);
            param[103].Value = dataobj.PM_COST_STAMP;
            param[104] = new SqlParameter("@COST_AGREE", SqlDbType.VarChar);
            param[104].Value = dataobj.PM_COST_AGREEMENT_PERIOD;
            //DOCUMENTS VAILABLE
            param[105] = new SqlParameter("@DOC_TITLE", SqlDbType.VarChar);
            param[105].Value = dataobj.PM_DOC_TITLE;
            param[106] = new SqlParameter("@DOC_TITLE_RMKS", SqlDbType.VarChar);
            param[106].Value = dataobj.PM_DOC_TITLLE_RMKS;
            param[107] = new SqlParameter("@DOC_OCCUP", SqlDbType.VarChar);
            param[107].Value = dataobj.PM_DOC_OCCUPANCY;
            param[108] = new SqlParameter("@DOC_OCCUP_RMKS", SqlDbType.VarChar);
            param[108].Value = dataobj.PM_DOC_OCC_RMKS;
            param[109] = new SqlParameter("@DOC_BUILD", SqlDbType.VarChar);
            param[109].Value = dataobj.PM_DOC_BUILD;
            param[110] = new SqlParameter("@DOC_BUILD_RMKS", SqlDbType.VarChar);
            param[110].Value = dataobj.PM_DOC_BUILD_RMKS;
            param[111] = new SqlParameter("@DOC_PAN", SqlDbType.VarChar);
            param[111].Value = dataobj.PM_DOC_PAN;
            param[112] = new SqlParameter("@DOC_PAN_RMKS", SqlDbType.VarChar);
            param[112].Value = dataobj.PM_DOC_PAN_RMKS;
           
            param[113] = new SqlParameter("@SIGNAGEIMAGES", SqlDbType.VarChar);
            param[113].Value = "";
            param[114] = new SqlParameter("@SIG_LENGTH", SqlDbType.VarChar);
            param[114].Value = dataobj.PM_PPT_SIG_LENGTH;
            param[115] = new SqlParameter("@SIG_WIDTH", SqlDbType.VarChar);
            param[115].Value = dataobj.PM_PPT_SIG_WIDTH;
            param[116] = new SqlParameter("@AC_OUTDOOR", SqlDbType.VarChar);
            param[116].Value = dataobj.PM_PPT_AC_OUTDOOR;
            param[117] = new SqlParameter("@GSB", SqlDbType.VarChar);
            param[117].Value = dataobj.PM_PPT_GSB;
            param[118] = new SqlParameter("@AC_OUTDOOR_IMAGE", SqlDbType.VarChar);
            param[118].Value = ""; 
            param[119] = new SqlParameter("@GSB_IMAGE", SqlDbType.VarChar);
            param[119].Value ="";
            param[120] = new SqlParameter("@INSPECTION_DATE", SqlDbType.DateTime);
            param[120].Value = dataobj.PM_PPT_INSPECTED_DT;
            param[121] = new SqlParameter("@INSPECTION_BY", SqlDbType.VarChar);
            param[121].Value = dataobj.PM_PPT_INSPECTED_BY;
            param[122] = new SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar);
            param[122].Value = dataobj.PM_PPT_TOT_FLRS;
            param[123] = new SqlParameter("@RELOCATION", SqlDbType.VarChar);
            param[123].Value = dataobj.PM_PPT_RELOC_NAME;
            param[124] = new SqlParameter("@RELOCADDRS", SqlDbType.VarChar);
            param[124].Value = dataobj.PM_PPT_RELOC_ADDRESS;
            param[125] = new SqlParameter("@OWN_ADDRESS", SqlDbType.VarChar);
            param[125].Value = dataobj.PM_OWN_ADDRESS;
            param[126] = new SqlParameter("@PHYCDTN_OTHRBLDG", SqlDbType.VarChar);
            param[126].Value = dataobj.PM_PHYCDTN_OTHRBU;
            param[127] = new SqlParameter("@OTHR_POTABLE_WTR_RMKS", SqlDbType.VarChar);
            param[127].Value = dataobj.PM_OTHR_POTABLE_WTR_REMARKS;
            param[128] = new SqlParameter("@OTHR_IT_INSTALL", SqlDbType.VarChar);
            param[128].Value = dataobj.PM_OTHR_IT_INSTALL;
            param[129] = new SqlParameter("@OTHR_IT_INSTALL_RMKS", SqlDbType.VarChar);
            param[129].Value = dataobj.PM_OTHR_IT_INSTALL_REMARKS;
            param[130] = new SqlParameter("@OTHR_DG_INSTALL", SqlDbType.VarChar);
            param[130].Value = dataobj.PM_OTHR_DG_INSTALL;
            param[131] = new SqlParameter("@OTHR_DG_INSTALL_RMKS", SqlDbType.VarChar);
            param[131].Value = dataobj.PM_OTHR_DG_INSTALL_RMKS;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            string Result;
            return new { Result = ds.Tables[0].Rows[0]["result"].ToString() };
        }

        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object InsertCheckList(SaveProjectChecklistInsert dataobj)
    {
        try
        {
            List<BCLLocations> BCLLocationsList = new List<BCLLocations>();
            BCLLocations BCLLoc = new BCLLocations();
            HDModel hd = new HDModel();
            hd.CompanyId = dataobj.CompanyId;
            string DB = _ReturnDB(hd);
            String proc_name = DB + ".SAVE_PROJECTCHECKLIST_DETAILS_MOBILE";
            SqlParameter[] param = new SqlParameter[12];
            param[0] = new SqlParameter("@ProjectChecklistDetails", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(dataobj.SelectedLists);
            param[1] = new SqlParameter("@Country", SqlDbType.NVarChar);
            param[1].Value = dataobj.CNY_CODE;
            param[2] = new SqlParameter("@City", SqlDbType.VarChar);
            param[2].Value = dataobj.CTY_CODE;
            param[3] = new SqlParameter("@Location", SqlDbType.VarChar);
            param[3].Value = dataobj.LCM_CODE;
            param[4] = new SqlParameter("@BusinessUnit", SqlDbType.VarChar);
            param[4].Value = dataobj.VER_CODE;
            param[5] = new SqlParameter("@Inspectionby", SqlDbType.VarChar);
            param[5].Value = dataobj.INSPECTOR;
            param[6] = new SqlParameter("@ProjectType", SqlDbType.VarChar);
            param[6].Value = dataobj.PT_CODE;
            param[7] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            param[7].Value = dataobj.VISITDATE;
            param[8] = new SqlParameter("@ProjectName", SqlDbType.VarChar);
            param[8].Value = dataobj.PROJECTNAME;
            param[9] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[9].Value = dataobj.AUR_ID;
            //param[10] = new SqlParameter("@COMPANY", SqlDbType.Int);
            //param[10].Value = dataobj.CompanyId;
            param[10] = new SqlParameter("@FLAG", SqlDbType.Int);
            param[10].Value = dataobj.FLAG;
            param[11] = new SqlParameter("@OVER_ALL_CMTS", SqlDbType.VarChar);
            param[11].Value = dataobj.OVERALL_CMTS;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            string Result;
            return new { Result = ds.Tables[0].Rows[0]["result"].ToString() };
        }

        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    /// <summary>
    /// SAVE MULTPLE PROJECT CHECKLIST IMAGES
    /// </summary>
    /// <param name="httpRequest"></param>
    /// <returns></returns>
     public object UploadPCLMultipleImages(HttpRequest httpRequest)
    {
        try
        {
            for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
            {
                HDModel hd = new HDModel();
                hd.CompanyId = httpRequest.Params["CompanyId"];
                string DB = _ReturnDB(hd);
                String LCM_CODE = httpRequest.Params["LCM_CODE"];
                String INSP_BY = httpRequest.Params["INSP_BY"];
                String INSP_DT = httpRequest.Params["INSP_DT"];
                String AUR_ID = httpRequest.Params["AUR_ID"];
                var req = httpRequest.Files[i];
                var fn = req.FileName;

                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(DB + "." + "API_HD_INSERT_PCL_CHECKLIST_FILES");
                sp2.Command.AddParameter("@LCM_CODE", LCM_CODE, DbType.String);
                sp2.Command.AddParameter("@INSP_BY", INSP_BY, DbType.String);
                sp2.Command.AddParameter("@INSP_DT", INSP_DT, DbType.String);
                sp2.Command.AddParameter("@FILENAME", fn, DbType.String);
                sp2.Command.AddParameter("@UPLOAD_PATH", (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn, DbType.String);
                sp2.Command.AddParameter("@AUR_ID", AUR_ID, DbType.String);
                sp2.ExecuteScalar();
                var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\AdityaBirla.dbo\\ProjectCheckList\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);
                req.SaveAs(filePath);
            }
            string Result;
            return new { Result = "Success" };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }

    // Get Saved List ProjectChecklist
    public object getSavedList(HDModel main)
    {
        try
        {
            HDModel hd = new HDModel();
            hd.CompanyId = main.CompanyId;
            string DB = _ReturnDB(hd);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "PCL_GET_SAVED_LIST");
            sp.Command.Parameters.Add("@AUR_ID", main.UserId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object getDashboardList(HDModel main)
    {
        try
        {
            HDModel hd = new HDModel();
            hd.CompanyId = main.CompanyId;
            string DB = _ReturnDB(hd);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_CHECKLIST_DASHBOARD");
            sp.Command.Parameters.Add("@AUR_ID", main.UserId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    //GetSavedDraftsData
    public object GetSavedDraftsdata(GetSavedDraftsList savedDraftsList)
    {
        try
        {
            HDModel hd = new HDModel();
            hd.CompanyId = savedDraftsList.CompanyId;
            string DB = _ReturnDB(hd);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(savedDraftsList.AUR_ID));
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_SAVED_PCLLIST_ITEMS_MOBILE");
            sp.Command.Parameters.Add("@LCM_CODE", savedDraftsList.LCM_CODE, DbType.String);
            sp.Command.Parameters.Add("@INSPECTEDDATE", savedDraftsList.PM_CL_MAIN_VISIT_DT, DbType.String);
            sp.Command.Parameters.Add("@FLAG", savedDraftsList.Flag, DbType.Int32);
            ds = sp.GetDataSet();
            ErrorHandler err12 = new ErrorHandler();
            err12._WriteErrorLog_string("data" + JsonConvert.SerializeObject(ds.Tables[0]));
            ErrorHandler err13 = new ErrorHandler();
            err13._WriteErrorLog_string("SelectedLists" + JsonConvert.SerializeObject(ds.Tables[0]));
            ErrorHandler err14 = new ErrorHandler();
            err14._WriteErrorLog_string("ImageList" + JsonConvert.SerializeObject(ds.Tables[1]));
            if (ds.Tables.Count != 0)
               
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], SelectedLists = ds.Tables[0], ImageList = ds.Tables[1] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object GetPropertyProposalSavedDraft(ProSavedDraft savedDraftsList)
    {
        try
        {
            HDModel hd = new HDModel();
            hd.CompanyId = savedDraftsList.CompanyId;
            string DB = _ReturnDB(hd);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(savedDraftsList.AUR_ID));
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "PM_GET_PROPERTYSAVEASDRAFTBY_REQS");
            sp.Command.Parameters.Add("@CMP_ID", 1, DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", savedDraftsList.AUR_ID, DbType.String);
           
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object GetPPSavedDetails(ProposalDraftDetails savedDraftsList)
    {
        try
        {
            HDModel hd = new HDModel();
            hd.CompanyId = savedDraftsList.CompanyId;
            string DB = _ReturnDB(hd);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(savedDraftsList.AUR_ID));
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "PM_GET_PRPTYSAVEASDRAFT_DETAILS");            
            sp.Command.Parameters.Add("@PROP_ID", savedDraftsList.PM_PPT_PM_REQ_ID, DbType.String);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    // Get Bussiness Unit 
    public object GetBussinessUnit(HDModel main)
    {
        try
        {
            List<Verticallst> verticallst = new List<Verticallst>();
            Verticallst verticals = new Verticallst();
            string DB = _ReturnDB(main);
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "Get_Vertical_ADM");
                sp.Command.Parameters.Add("@MODE", 1, DbType.Int16);
                sp.Command.Parameters.Add("@AUR_ID", main.UserId, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    verticals = new Verticallst();
                    verticals.VER_CODE = ds.Tables[0].Rows[i]["VER_CODE"].ToString();
                    verticals.VER_NAME = ds.Tables[0].Rows[i]["VER_NAME"].ToString();
                    verticallst.Add(verticals);
                }

                return new { verticallst = verticallst };
            }


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    // BranchCheckList
    public object GetBranchCheckList(HDModel main)
    {
        try
        {
            List<MainMenuItems> MList = new List<MainMenuItems>();
            List<BranchChecklist> BCList = new List<BranchChecklist>();
            MainMenuItems Mt;
            SubMenuItems st;
            BranchChecklist BCL;
            Score sc;
            TextBoxValues txb;
            //HDModel login = new HDModel();
            //login.CompanyId = "ABC_UAT";
            string DB = _ReturnDB(main);
            int cnt;
            int Expiry;
            ErrorHandler err2 = new ErrorHandler();
            err2._WriteErrorLog_string("inside" + JsonConvert.SerializeObject(main.UserId));
            ErrorHandler err = new ErrorHandler();
            err._WriteErrorLog_string("DatabaseCheck" + JsonConvert.SerializeObject(DB));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
                sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_CATEGORIES");
                sp.Command.AddParameter("@LOC_ID", "", DbType.String);
                sp.Command.AddParameter("@InspectedDate", "", DbType.String);
                sp.Command.AddParameter("@AUR_ID", main.UserId, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    BCL = new BranchChecklist();
                    BCL.BCL_MC_CODE = ds.Tables[0].Rows[i]["BCL_MC_CODE"].ToString();
                    BCL.BCL_MC_NAME = ds.Tables[0].Rows[i]["BCL_MC_NAME"].ToString();
                    BCL.BCL_CH_SUB_CODE = ds.Tables[0].Rows[i]["BCL_CH_SUB_CODE"].ToString();
                    BCL.BCL_SUB_NAME = ds.Tables[0].Rows[i]["BCL_SUB_NAME"].ToString();
                    BCL.BCL_SUB_TYPE = ds.Tables[0].Rows[i]["BCL_SUB_TYPE"].ToString();
                    BCL.Score = new List<Score>();
                    BCL.TextBoxValues = new List<TextBoxValues>();
                    if (BCL.BCL_SUB_TYPE == "Radio")
                    {
                        string a = ds.Tables[0].Rows[i]["SCORE"].ToString();
                        string[] authorsList = a.Split(',');
                        foreach (string author in authorsList)
                        {
                            sc = new Score();
                            sc.ScoreValues = author;
                            BCL.Score.Add(sc);
                        }
                    }
                    else if (BCL.BCL_SUB_TYPE == "Text" || BCL.BCL_SUB_TYPE == "Multiple")
                    {
                        string a = ds.Tables[0].Rows[i]["SCORE"].ToString();
                        string[] authorsList = a.Split(',');
                        foreach (string author in authorsList)
                        {
                            txb = new TextBoxValues();
                            txb.Names = author;
                            BCL.TextBoxValues.Add(txb);
                        }
                    }


                    BCList.Add(BCL);

                }

                return new { Status = "true", Data = BCList };
            }


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }


    //Branch CheckList Inspectors

    public object GetInspectors(Inspectors main)
    {
        try
        {
            List<Inspectors> InspectorsList = new List<Inspectors>();
            Inspectors inspect = new Inspectors();
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = main.CompanyId;
            string DB = _ReturnDB(hDModel);
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_INSPECTORS");
                sp.Command.Parameters.Add("@PRJCKL", main.PRJCKL, DbType.Int16);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    inspect = new Inspectors();
                    inspect.INSPECTOR = ds.Tables[0].Rows[i]["INSPECTOR"].ToString();
                    InspectorsList.Add(inspect);
                }

                return new { InspectorsList = InspectorsList };
            }


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //Branch CheckList Locations

    public object GetLocations(HDModel main)
    {
        try
        {
            List<BCLLocations> BCLLocationsList = new List<BCLLocations>();
            BCLLocations BCLLoc = new BCLLocations();
            string DB = _ReturnDB(main);
            int cnt;
            int Expiry;
            ErrorHandler err2 = new ErrorHandler();
            err2._WriteErrorLog_string("inside" + JsonConvert.SerializeObject(main.UserId));
            ErrorHandler err = new ErrorHandler();
            err._WriteErrorLog_string("DatabaseCheck" + JsonConvert.SerializeObject(DB));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
                sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_LOCATIONS");
                sp.Command.AddParameter("@AUR_ID", main.UserId, DbType.String);
                //sp.Command.AddParameter("@InspectedDate", "", DbType.String);
                //sp.Command.AddParameter("@AUR_ID", main.UserId, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    BCLLoc = new BCLLocations();
                    BCLLoc.LCM_CODE = ds.Tables[0].Rows[i]["LCM_CODE"].ToString();
                    BCLLoc.LCM_NAME = ds.Tables[0].Rows[i]["LCM_NAME"].ToString();
                    BCLLoc.CTY_CODE = ds.Tables[0].Rows[i]["CTY_CODE"].ToString();
                    BCLLoc.CNY_CODE = ds.Tables[0].Rows[i]["CNY_CODE"].ToString();
                    BCLLoc.LCM_CNP = ds.Tables[0].Rows[i]["LCM_CNP"].ToString();
                    BCLLocationsList.Add(BCLLoc);
                }

                return new { LocationsList = BCLLocationsList };
            }


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //Branch CheckList Insert/Save As Draft

    public object BCLInsert(BranchCheckListInsert main)
    {
        try
        {
            List<BCLLocations> BCLLocationsList = new List<BCLLocations>();
            BCLLocations BCLLoc = new BCLLocations();
            HDModel hd = new HDModel();
            hd.CompanyId = main.CompanyId;
            string DB = _ReturnDB(hd);
            int cnt;
            int Expiry;

            //String proc_name = "[" + main.CompanyId + "].[dbo].BCL_INSERT_CHECKLIST_MOBILE";
            String proc_name = DB + ".BCL_INSERT_CHECKLIST_MOBILE";

            SqlParameter[] param = new SqlParameter[11];
            param[0] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(main.SelectedLists);
            param[1] = new SqlParameter("@InspectdBy", SqlDbType.NVarChar);
            param[1].Value = main.INSPECTOR;
            param[2] = new SqlParameter("@date", SqlDbType.NVarChar);
            param[2].Value = main.VISITDATE;
            param[3] = new SqlParameter("@SelCompany", SqlDbType.NVarChar);
            param[3].Value = main.LCM_CNP;
            param[4] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            param[4].Value = main.LCM_CODE;
            param[5] = new SqlParameter("@CTY_CODE", SqlDbType.NVarChar);
            param[5].Value = main.CTY_CODE;
            param[6] = new SqlParameter("@CNY_CODE", SqlDbType.NVarChar);
            param[6].Value = main.CNY_CODE;
            param[7] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[7].Value = main.AUR_ID;
            param[8] = new SqlParameter("@COMPANY", SqlDbType.Int);
            param[8].Value = 1;
            param[9] = new SqlParameter("@FLAG", SqlDbType.Int);
            param[9].Value = main.FLAG;
            param[10] = new SqlParameter("@OVER_CMTS", SqlDbType.NVarChar);
            param[10].Value = main.OVERALL_CMTS;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            string Result;
            return new { Result = ds.Tables[0].Rows[0]["Result"].ToString() };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //Saved Drafts List  ---BranchCheckList Creation

    public object BCLGetSavedDrafts(HDModel main)
    {
        try
        {
            List<BranchCheckListSavedDrafts> BCLSavedDraftsList = new List<BranchCheckListSavedDrafts>();
            BranchCheckListSavedDrafts BCLSavedDrafts = new BranchCheckListSavedDrafts();

            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
            HDModel hd = new HDModel();
            hd.CompanyId = main.CompanyId;
            string DB = _ReturnDB(hd);
            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_SAVED_LIST");
            sp.Command.AddParameter("@AUR_ID", main.UserId, DbType.String);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                BCLSavedDrafts = new BranchCheckListSavedDrafts();
                BCLSavedDrafts.LCM_CODE = ds.Tables[0].Rows[i]["LCM_CODE"].ToString();
                BCLSavedDrafts.LCM_NAME = ds.Tables[0].Rows[i]["LCM_NAME"].ToString();
                BCLSavedDrafts.CTY_CODE = ds.Tables[0].Rows[i]["CTY_CODE"].ToString();
                BCLSavedDrafts.CNY_CODE = ds.Tables[0].Rows[i]["CNY_CODE"].ToString();
                BCLSavedDrafts.LCM_CNP = ds.Tables[0].Rows[i]["LCM_CNP"].ToString();
                BCLSavedDrafts.VISITDATE = ds.Tables[0].Rows[i]["BCL_SELECTED_DT"].ToString();
                BCLSavedDrafts.SavedCount = ds.Tables[0].Rows[i]["SavedCount"].ToString();
                BCLSavedDraftsList.Add(BCLSavedDrafts);
            }

            return new { SavedDraftsList = BCLSavedDraftsList };



        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //Saved Drafts List Location-wise  ---BranchCheckList Saved Drafts

    public object BCLGetSavedDraftsBasedOnLoc(BranchCheckListInsert main)
    {
        try
        {
            List<BranchCheckListInsert> BCLSavedDraftsListByLoc = new List<BranchCheckListInsert>();
            BranchCheckListInsert BCLSavedLoc = new BranchCheckListInsert();
            SelectedList selectedList = new SelectedList();
            ImagesList images = new ImagesList();

            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.AUR_ID));
            HDModel hd = new HDModel();
            hd.CompanyId = main.CompanyId;
            string DB = _ReturnDB(hd);
            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_SAVED_LIST_ITEMS_MOBILE");
            sp.Command.AddParameter("@LCM_CODE", main.LCM_CODE, DbType.String);
            sp.Command.AddParameter("@AUR_ID", main.AUR_ID, DbType.String);
            sp.Command.AddParameter("@INSPECTEDDATE", main.VISITDATE, DbType.String);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();

            BCLSavedLoc = new BranchCheckListInsert();
            BCLSavedLoc.CNY_CODE = ds.Tables[0].Rows[0]["BCL_CNY_CODE"].ToString();
            BCLSavedLoc.LCM_CODE = ds.Tables[0].Rows[0]["BCL_LOC_CODE"].ToString();
            BCLSavedLoc.CTY_CODE = ds.Tables[0].Rows[0]["BCL_CTY_CODE"].ToString();
            BCLSavedLoc.INSPECTOR = ds.Tables[0].Rows[0]["BCL_INSPECTED_BY"].ToString();
            BCLSavedLoc.VISITDATE = ds.Tables[0].Rows[0]["BCL_SELECTED_DT"].ToString();
            BCLSavedLoc.LCM_CNP = ds.Tables[0].Rows[0]["BCL_SELECTED_CNP"].ToString();
            BCLSavedLoc.OVERALL_CMTS = ds.Tables[0].Rows[0]["BCL_OVERALL_CMTS"].ToString();
            BCLSavedLoc.SelectedLists = new List<SelectedList>();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                selectedList = new SelectedList();
                selectedList.CatCode = ds.Tables[0].Rows[i]["BCLD_CAT_CODE"].ToString();
                selectedList.SubcatCode = ds.Tables[0].Rows[i]["BCLD_SUBCAT_CODE"].ToString();
                selectedList.ScoreCode = ds.Tables[0].Rows[i]["BCLD_SCORE_CODE"].ToString();
                selectedList.ScoreName = ds.Tables[0].Rows[i]["BCLD_SCORE_NAME"].ToString();
                selectedList.txtdata = ds.Tables[0].Rows[i]["BCLD_TEXTDATA"].ToString();
                selectedList.Date = ds.Tables[0].Rows[i]["BCLD_DATE"].ToString();
                selectedList.SubScore = ds.Tables[0].Rows[i]["BCLD_SUB_SCORE"].ToString();
                selectedList.FilePath = ds.Tables[0].Rows[i]["BCLD_FILE_UPLD"].ToString();
                BCLSavedLoc.SelectedLists.Add(selectedList);
            }
            BCLSavedLoc.imagesList = new List<ImagesList>();
            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
            {
                images = new ImagesList();
                images.Imagepath = ds.Tables[1].Rows[i]["BCL_UPL_PATH"].ToString();
                BCLSavedLoc.imagesList.Add(images);
            }


            BCLSavedDraftsListByLoc.Add(BCLSavedLoc);

            return new { SavedDraftsList = BCLSavedDraftsListByLoc };



        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    #endregion

    #region Database Configuration
    //Return DataBase
    public string _ReturnDB(HDModel Tenant)
    {
        try
        {
            DataSet ds = new DataSet();
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "CHECK_USER_TENANT");
            sp.Command.AddParameter("@TENANT_NAME", Tenant.CompanyId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                DatabaseURL = ds.Tables[0].Rows[0]["TENANT_ID"].ToString();
                HttpContext.Current.Session["useroffset"] = ds.Tables[0].Rows[0]["OFFSET"].ToString();
                if (ds.Tables[0].Rows[0]["TENANT_ID"].ToString() == "" && ds.Tables[0].Rows[0]["OFFSET"].ToString() == "")
                { return (string)null; }
                else if (ds.Tables[0].Rows.Count == 0)
                { return (string)null; }
                else
                { return DatabaseURL; }
            }
            else { return MessagesVM.Err; }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    public string _ReturnDBByGmailId(string Tenant)
    {
        try
        {
            DataSet ds = new DataSet();
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "CHECK_USER_TENANT");
            sp.Command.AddParameter("@TENANT_NAME", Tenant, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                DatabaseURL = ds.Tables[0].Rows[0]["TENANT_ID"].ToString();
                HttpContext.Current.Session["useroffset"] = ds.Tables[0].Rows[0]["OFFSET"].ToString();
                if (ds.Tables[0].Rows[0]["TENANT_ID"].ToString() == "" && ds.Tables[0].Rows[0]["OFFSET"].ToString() == "")
                { return (string)null; }
                else if (ds.Tables[0].Rows.Count == 0)
                { return (string)null; }
                else
                { return DatabaseURL; }
            }
            else { return MessagesVM.Err; }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    #endregion

    #region MileStone #02
    //Return UserName
    public string _GetUserName(string UID, string DB)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_GET_USERNAME");
            sp.Command.AddParameter("@UID", UID, DbType.String);
            return (string)sp.ExecuteScalar();
        }
        catch (Exception)
        {
            return MessagesVM.Err;
        }
    }
    public string _GetUserNameByEmailId(string EMAIL_ID, string DB)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_GET_USERNAME_BY_EMAIL_ID");
            sp.Command.AddParameter("@EMAIL_ID", EMAIL_ID, DbType.String);
            return (string)sp.ExecuteScalar();
        }
        catch (Exception)
        {
            return MessagesVM.Err;
        }
    }
    //User Role
    public int _GetRoles(string UID, string DB)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_GET_USER_ROLE");
            sp.Command.AddParameter("@UID", UID, DbType.String);
            DataSet ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                int flag = Convert.ToInt32(ds.Tables[0].Rows[0]["ROL_ID"].ToString());
                return flag;
            }
            else
            {
                return 0;
            }
        }
        catch (SqlException ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex);
            return 0;
        }
    }

    //Get Role By EmailID

    public int _GetRolesByEmailId(string EMAIL_ID, string DB)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_GET_USER_ROLE_BY_EMAIL_ID");
            sp.Command.AddParameter("@EMAIL_ID", EMAIL_ID, DbType.String);
            DataSet ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                int flag = Convert.ToInt32(ds.Tables[0].Rows[0]["ROL_ID"].ToString());
                return flag;
            }
            else
            {
                return 0;
            }
        }
        catch (SqlException ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex);
            return 0;
        }
    }
    //Sign In
    public object _LoginValidate(HDModel Login)
    {
        try
        {
            string DB = _ReturnDB(Login);
            int cnt;
            int Invldcnt;
            int Expiry;
            if (DB == null)
            { return new { Message = MessagesVM.InvalidCompany, data = (object)null }; }
            else
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_VALIDATE_USER");
                sp.Command.AddParameter("@COMPANY", Login.CompanyId, DbType.String);
                sp.Command.AddParameter("@USR_ID", Login.UserId, DbType.String);
                sp.Command.AddParameter("@USR_LOGIN_PASSWORD", Login.Password, DbType.String);
                cnt = (int)sp.ExecuteScalar();
                string UNAME = _GetUserName(Login.UserId, DB);
                int ROLE = _GetRoles(Login.UserId, DB);
                // List<Module> modules = new List<Module>();
                List<Modules> modules = GetModuleList(Login);
                if (cnt == 1)
                {
                    string Key = Guid.NewGuid().ToString();
                    SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "LOGIN_HISTORY");
                    sp1.Command.AddParameter("@COMPANY", Login.CompanyId, DbType.String);
                    sp1.Command.AddParameter("@USR_ID", Login.UserId, DbType.String);
                    sp1.Command.AddParameter("@KEY", Key, DbType.String);
                    Expiry = (int)sp1.ExecuteScalar();
                    if (Expiry == 1)
                    {
                        return new { Status = "false", Message = MessagesVM.License, LicensePeriodExpired = Expiry };
                    }
                    else
                    {
                        return new { Status = "true", Message = MessagesVM.LoginSuccess, data = Key, User = UNAME, EmployeeRole = ROLE, Modules = modules };
                    }
                }
                else
                {
                    SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(DB + "." + "USR_INVALID_CNT");
                    sp1.Command.AddParameter("@USR_ID", Login.UserId, DbType.String);
                    Invldcnt = (int)sp1.ExecuteScalar();
                    if (Invldcnt >= 3)
                    {
                        return new { Status = "false", Message = MessagesVM.moreattempts, data = (object)null };
                    }
                    else
                    {
                        return new { Status = "false", Message = MessagesVM.InvalidLogin, data = (object)null };
                    }
                }
            }
        }
        catch (SqlException ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex);
            return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex);
            return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
    }
    public object _LoginValidateWithADFS(HDModel ADFSLogin)
    {
        try
        {
            string DB = _ReturnDB(ADFSLogin);
            int cnt;
            int Expiry;
            if (DB == null)
            { return new { Message = MessagesVM.InvalidCompany, data = (object)null }; }
            else
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_VALIDATE_USER_ADFS");
                sp.Command.AddParameter("@COMPANY", ADFSLogin.CompanyId, DbType.String);
                sp.Command.AddParameter("@USR_ID", ADFSLogin.UserId, DbType.String);

                cnt = (int)sp.ExecuteScalar();
                string UNAME = _GetUserName(ADFSLogin.UserId, DB);
                int ROLE = _GetRoles(ADFSLogin.UserId, DB);
                List<Modules> modules = GetModuleList(ADFSLogin);
                if (cnt == 1)
                {
                    string Key = Guid.NewGuid().ToString();
                    SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "LOGIN_HISTORY");
                    sp1.Command.AddParameter("@COMPANY", ADFSLogin.CompanyId, DbType.String);
                    sp1.Command.AddParameter("@USR_ID", ADFSLogin.UserId, DbType.String);
                    sp1.Command.AddParameter("@KEY", Key, DbType.String);
                    Expiry = (int)sp1.ExecuteScalar();
                    if (Expiry == 1)
                    {
                        return new { Status = "false", Message = MessagesVM.License, LicensePeriodExpired = Expiry };
                    }
                    else
                    {
                        return new { Status = "true", Message = MessagesVM.LoginSuccess, data = Key, User = UNAME, EmployeeRole = ROLE, Modules = modules };
                    }
                }
                else { return new { Status = "false", Message = MessagesVM.InvalidLogin, data = (object)null }; }
            }
        }
        catch (SqlException ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex);
            return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex);
            return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
    }

    //Sign In With Gmail
    //public object SignInWithGmail()
    //{
    //    try
    //    {
    //        GoogleConnect.ClientId = "140086361902-h8balga4l6fhaq5ip6r4ip8e0278o1mc.apps.googleusercontent.com";
    //        GoogleConnect.ClientSecret = "gnQK3yUuUAVgfJHzNRefXPxs";
    //        GoogleConnect.RedirectUri = HttpContext.Current.Request.Url.AbsoluteUri.Split('?')[0];
    //        if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["code"]))
    //        {

    //            string json = GoogleConnect.Fetch("me", HttpContext.Current.Request.QueryString["code"]);
    //            GoogleProfile profile = new JavaScriptSerializer().Deserialize<GoogleProfile>(json);
    //            var EMAIL_ID = profile.Emails.Find(email => email.Type == "account").Value;
    //            string DB = _ReturnDBByGmailId(EMAIL_ID);
    //            int cnt;
    //            int Expiry;
    //            if (DB == null)
    //            { return new { Message = MessagesVM.InvalidCompany, data = (object)null }; }
    //            else
    //            {

    //                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_VALIDATE_USER_BY_EMAIL_ID");
    //                //sp.Command.AddParameter("@COMPANY", Login.CompanyId, DbType.String);
    //                sp.Command.AddParameter("@EMAIL_ID", EMAIL_ID, DbType.String);
    //                //sp.Command.AddParameter("@USR_LOGIN_PASSWORD", Login.Password, DbType.String);
    //                cnt = (int)sp.ExecuteScalar();
    //                string UNAME = _GetUserNameByEmailId(EMAIL_ID, DB);
    //                int ROLE = _GetRolesByEmailId(EMAIL_ID, DB);
    //                if (cnt == 1)
    //                {
    //                    string Key = Guid.NewGuid().ToString();
    //                    SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "LOGIN_HISTORY_BY_EMAIL_ID");

    //                    sp1.Command.AddParameter("@EMAIL_ID", EMAIL_ID, DbType.String);
    //                    sp1.Command.AddParameter("@KEY", Key, DbType.String);
    //                    Expiry = (int)sp1.ExecuteScalar();
    //                    if (Expiry == 1)
    //                    {
    //                        return new { Status = "false", Message = MessagesVM.License, LicensePeriodExpired = Expiry };
    //                    }
    //                    else
    //                    {
    //                        return new { Status = "true", Message = MessagesVM.LoginSuccess, data = Key, User = UNAME, EmployeeRole = ROLE, UserImage = profile.Image.Url };
    //                    }
    //                }
    //                else
    //                {
    //                    return new { Status = "false", Message = MessagesVM.InvalidLogin, data = (object)null };
    //                }

    //            }

    //        }
    //        else
    //        {
    //            GoogleConnect.Authorize("profile", "email");
    //            return new { Status = "false", Message = MessagesVM.InvalidLogin, data = (object)null };
    //        }

    //    }
    //    catch (SqlException ex)
    //    {
    //        ErrorHandler erhndlr = new ErrorHandler();
    //        erhndlr._WriteErrorLog(ex);
    //        return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorHandler erhndlr = new ErrorHandler();
    //        erhndlr._OtherExceptionsInDBErrorLog(ex);
    //        return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
    //    }
    //}
    //Forgot Password
    public object _PasswordForget(HDModel Password)
    {
        try
        {
            string TenantValidate = _ReturnDB(Password);
            if (TenantValidate != null | TenantValidate != "")
            {
                if (Password.UserId != null | Password.UserId != "")
                {
                    SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(TenantValidate + "." + "GN_PASSWORD_FORGET");
                    sp.Command.AddParameter("@USR_ID", Password.UserId, DbType.String);
                    int vldpwd = (int)sp.ExecuteScalar();
                    if (vldpwd == 1)
                    {
                        return new { Status = "true", Message = MessagesVM.ForgotPassword };
                    }
                    else if (vldpwd == 2)
                    {
                        return new { Status = "false", Message = MessagesVM.InvalidUserID };
                    }
                    else
                    {
                        return new { Status = "false", Message = MessagesVM.Err };
                    }
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            else { return new { Status = "false", Message = MessagesVM.Err }; }
        }
        catch (SqlException Sql)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(Sql); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
    }
    //User Profile
    public object _GetDashboardProfile(HDModel Profile)
    {
        if (!(Profile.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(Profile.APIKey);
                if (VALDB.AURID != null)
                {
                    DashboardProfile Dashboard = new DashboardProfile();
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_GET_EMP_DETAILS");
                    sp.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            Dashboard.EMP_ID = sdr["EMP_ID"].ToString();
                            Dashboard.EMP_NAME = sdr["EMP_NAME"].ToString();
                            Dashboard.EMP_EMAIL = sdr["EMP_EMAIL"].ToString();
                            Dashboard.EMP_DESIG = sdr["DESIGNATION"].ToString();
                            Dashboard.DEPARTMENT = sdr["DEPARTMENT"].ToString();
                            Dashboard.REPORTING = sdr["REPORTING"].ToString();
                            Dashboard.DOJ = sdr["DOJ"].ToString();
                            Dashboard.DOB = sdr["DOB"].ToString();
                            Dashboard.EXTENSION = sdr["EXTENSION"].ToString();
                            Dashboard.STATUS = sdr["STATUS"].ToString();
                            Dashboard.EMP_REPNAME = sdr["REPORTING"].ToString();
                            Dashboard.AUR_RES_NUMBER = sdr["AUR_RES_NUMBER"].ToString();
                            Dashboard.EMP_IMG = sdr["EMP_IMG"].ToString();
                        }
                    }
                    if (Dashboard.EMP_ID != null)
                        return new { Status = "true", data = Dashboard };
                    else
                        return new { Status = "false", data = (object)null };
                }
                else
                {
                    return new { Status = "false", Message = MessagesVM.Err };
                }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(e);
                return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }
        }
        else
        {
            return new { Status = "false", Message = MessagesVM.Err };
        }
    }
    //Update User Profile
    public object _UpdateProfile(Profile Prof)
    {
        try
        {
            int ValidateProfile;
            ValidateKey VALDB = Secure._ValidateAPIKey(Prof.APIKey);
            if (VALDB.AURID != null)
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_UPDATE_USER_PROFILE");
                sp.Command.AddParameter("@ID", VALDB.AURID, DbType.String);
                sp.Command.AddParameter("@AUR_FIRST_NAME", Prof.Name, DbType.String);
                sp.Command.AddParameter("@AUR_EMAIL", Prof.Email, DbType.String);
                sp.Command.AddParameter("@AUR_DEP_ID", Prof.Department, DbType.String);
                sp.Command.AddParameter("@AUR_STA_ID", Prof.Status, DbType.Int32);
                if (!((Prof.Phone) == null | (Prof.Phone) == ""))
                {
                    sp.Command.AddParameter("@AUR_RES_NUMBER", Prof.Phone, DbType.String);
                }
                else { sp.Command.AddParameter("@AUR_RES_NUMBER", "", DbType.String); }
                if (!((Prof.Extension) == null | (Prof.Extension) == ""))
                {
                    sp.Command.AddParameter("@AUR_EXTENSION", Prof.Extension, DbType.String);
                }
                else { sp.Command.AddParameter("@AUR_EXTENSION", "", DbType.String); }
                sp.Command.AddParameter("@AUR_DOB", Prof.DOB, DbType.Date);
                sp.Command.AddParameter("@AUR_DOJ", Prof.DOJ, DbType.Date);
                sp.Command.AddParameter("@AUR_REPORTING_TO", Prof.ReportingManager, DbType.String);
                ValidateProfile = (int)sp.ExecuteScalar();
                if (ValidateProfile == 1) { return new { Status = "true", Message = MessagesVM.ProfileUpdated }; } else { return new { Status = "false", Message = MessagesVM.Err, data = (object)null }; }
            }
            else
            {
                return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }
        }
        catch (SqlException ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
        catch (Exception e)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
    }
    //Change Password
    public object _ChangePasswod(ChangePwd ChangePwd)
    {
        if (!(ChangePwd.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(ChangePwd.APIKey);
                if (VALDB.AURID != null)
                {
                    //Validate Old Password & Then Update To New One
                    int Passwodcnt;
                    SubSonic.StoredProcedure sp0 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_VALIDATE_OLD");
                    sp0.Command.AddParameter("@USER", VALDB.AURID, DbType.String);
                    sp0.Command.AddParameter("@OLDPWD", ChangePwd.OldPwd, DbType.String);
                    Passwodcnt = (int)sp0.ExecuteScalar();
                    if (Passwodcnt == 1)
                    {
                        try
                        {
                            int cnt;
                            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_CHANGE_PWD");
                            sp.Command.AddParameter("@USER", VALDB.AURID, DbType.String);
                            sp.Command.AddParameter("@PWD", ChangePwd.NewPwd, DbType.String);
                            cnt = (int)sp.ExecuteScalar();
                            if (cnt == 1)
                            {
                                return new { Status = "true", Message = MessagesVM.ChangePassword };
                            }
                            else { return new { Status = "false", Message = MessagesVM.Err }; }
                        }
                        catch (SqlException E)
                        {
                            ErrorHandler erhndlr = new ErrorHandler();
                            erhndlr._WriteErrorLog(E); return E.ToString();
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler erhndlr = new ErrorHandler();
                            erhndlr._OtherExceptionsInDBErrorLog(ex); return ex.ToString();
                        }
                    }
                    else { return new { Status = "false", Message = MessagesVM.InvalidOldPassword }; }
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (SqlException sq)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(sq); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }
        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }

    #region Dropdown Binding Methods
    //Departments
    public object _GetDepartments(Key AKey)
    {
        if (!(AKey.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(AKey.APIKey);
                if (VALDB.AURID != null)
                {
                    try
                    {
                        List<Department> verlst = new List<Department>();
                        Department ver;
                        sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "GET_DEPARTMENTS");
                        using (IDataReader sdr = sp.GetReader())
                        {
                            while (sdr.Read())
                            {
                                ver = new Department();
                                ver.DEP_NAME = sdr["DEP_NAME"].ToString();
                                ver.DEP_CODE = sdr["DEP_CODE"].ToString();
                                ver.ticked = false;
                                verlst.Add(ver);
                            }
                        }
                        if (verlst.Count != 0)
                            return new { Status = "true", data = verlst };
                        else
                            return new { Status = "false", data = (object)null };
                    }
                    catch (Exception e)
                    {
                        ErrorHandler erhndlr = new ErrorHandler();
                        erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
                    }
                }
                else { return new { Status = "false", Message = MessagesVM.Err, data = (object)null }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(e); return new { Status = "false", Message = MessagesVM.Err };
            }
        }
        else { return new { Status = "false", Message = MessagesVM.Err, data = (object)null }; }

    }
    //Countries
    public object _GetCountries(Key AKey)
    {
        if (!(AKey.APIKey == null))
        {
            ValidateKey VALDB = Secure._ValidateAPIKey(AKey.APIKey);
            if (VALDB.AURID != null)
            {
                try
                {
                    List<Countrylst> cnylst = new List<Countrylst>();
                    Countrylst cny;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "Get_Countries_ADM");
                    sp.Command.AddParameter("@MODE", 1, DbType.Int32);
                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            cny = new Countrylst();
                            cny.CNY_CODE = sdr["CNY_CODE"].ToString();
                            cny.CNY_NAME = sdr["CNY_NAME"].ToString();
                            cny.ticked = false;
                            cnylst.Add(cny);
                        }
                    }
                    if (cnylst.Count != 0)
                        return new { Status = "true", data = cnylst };
                    else
                        return new { Status = "false", data = (object)null };
                }
                catch (Exception e)
                {
                    ErrorHandler erhndlr = new ErrorHandler();
                    erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
                }
            }
            else
            { return new { Status = "false", Message = MessagesVM.Err }; }
        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }
    //Side Menu Count
    public object _HDCount(HDModel HDCount)
    {
        if (!(HDCount.APIKey == null))
        {
            ValidateKey VALDB = Secure._ValidateAPIKey(HDCount.APIKey);
            if (VALDB.AURID != null)
            {
                try
                {
                    List<HDDashboardCount> cntlst = new List<HDDashboardCount>();
                    HDDashboardCount Dshcnt = new HDDashboardCount();
                    SqlParameter[] param = new SqlParameter[3];
                    param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                    param[0].Value = VALDB.AURID;
                    param[1] = new SqlParameter("@CMP_ID", SqlDbType.NVarChar);
                    param[1].Value = VALDB.COMPANYID;
                    param[2] = new SqlParameter("@ROLE_ID", SqlDbType.Int);
                    param[2].Value = HDCount.Role_id;
                    using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, VALDB.TENANT_ID + "." + "API_HD_GET_HELPDESK_STS_DASHBOARD", param))
                    {
                        if (dr.Read())
                        {
                            Dshcnt.TOTAL = dr["TOTAL"].ToString();
                            Dshcnt.PENDING = dr["PENDING"].ToString();
                            Dshcnt.INPROGRESS = dr["INPROGRESS"].ToString();
                            Dshcnt.CLOSED = dr["CLOSED"].ToString();
                            Dshcnt.ONHOLD = dr["ONHOLD"].ToString();
                            Dshcnt.REJECTED = dr["REJECTED"].ToString();
                            Dshcnt.SLA_VIOLATION = dr["VIOLATION"].ToString();
                            return new { Status = "true", data = Dshcnt };
                        }
                        else
                        {
                            return new { Status = "false", data = (object)null };
                        }
                    }
                }
                catch (Exception e)
                {
                    ErrorHandler erhndlr = new ErrorHandler();
                    erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
                }
            }
            else
            { return new { Status = "false", Message = MessagesVM.Err }; }
        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }

    //Reporting To 
    public object _ReportingManagers(HDModel name)
    {
        if (!(name.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(name.APIKey);
                if (VALDB.AURID != null)
                {
                    List<EmployeeNameID> emplst = new List<EmployeeNameID>();
                    EmployeeNameID emp;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "HD_API_GET_REPORTING_MANAGERS");
                    sp.Command.AddParameter("@NAME", name.UserId, DbType.String);

                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            emp = new EmployeeNameID();
                            emp.AUR_ID = sdr["id"].ToString();
                            emp.NAME = sdr["name"].ToString();
                            emp.ticked = false;
                            emplst.Add(emp);
                        }
                    }
                    return new { Status = "true", items = emplst, total_count = emplst.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }
    #endregion

    #endregion

    #region MileStone #03

    //Get Total No.Of HD Request Count
    public string GetTotalHDReqCount(string SKey)
    {
        try
        {
            string cnt = "0";
            DataSet ds = new DataSet();
            ValidateKey VALDB = Secure._ValidateAPIKey(SKey);
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "GET_TODAYS_REQUEST_ID");
            sp.Command.AddParameter("@dummy", 1, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                cnt = Convert.ToInt32((ds.Tables[0].Rows[0]["cnt"])).ToString();
                return cnt;
            }
            else { return Convert.ToInt32("0").ToString(); }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    //Service Type 
    public object _ServiceType(RaiseRequest SType)
    {
        if (!(SType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(SType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<ServiceType> SList = new List<ServiceType>();
                    ServiceType St;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_GET_CHILD_CATEGORY");
                    sp.Command.AddParameter("@CHILD", SType.ServiceType, DbType.String);
                    sp.Command.AddParameter("@PageNo", SType.PageNo, DbType.String);
                    sp.Command.AddParameter("@PageSize", SType.PageSize, DbType.String);
                    sp.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.Int32);

                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            St = new ServiceType();
                            St.SERVICE_TYPE_CODE = sdr["CCAT_CODE"].ToString();
                            St.SERVICE_TYPE_NAME = sdr["CCAT_NAME"].ToString();
                            SList.Add(St);
                        }
                    }
                    return new { Status = "true", items = SList, total_count = SList.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }


    // Location Type

    public object _LocationType(RaiseRequest LType)
    {
        if (!(LType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(LType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<LocationType> SList = new List<LocationType>();
                    LocationType St;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_GET_LOCATION");
                    sp.Command.AddParameter("@USER_ID", VALDB.AURID, DbType.String);
                    sp.Command.AddParameter("@LOCATION", LType.LocationType, DbType.String);
                    sp.Command.AddParameter("@PageNo", LType.PageNo, DbType.String);
                    sp.Command.AddParameter("@PageSize", LType.PageSize, DbType.String);
                    sp.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.Int32);

                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            St = new LocationType();
                            St.LCM_CODE = sdr["LCM_CODE"].ToString();
                            St.LCM_NAME = sdr["LCM_NAME"].ToString();
                            SList.Add(St);
                        }
                    }
                    return new { Status = "true", items = SList, total_count = SList.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }

    // Request Type

    public object _RequestType(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<RequestType> RList = new List<RequestType>();
                    RequestType Rt;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "PM_GET_REQUEST_TYPES");
                    sp.Command.AddParameter("@RequestType", RType.RequestType, DbType.String);
                   

                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {                              
                            Rt = new RequestType();
                            Rt.PM_RT_SNO = sdr["PM_RT_SNO"].ToString();
                            Rt.PM_RT_TYPE = sdr["PM_RT_TYPE"].ToString();
                            RList.Add(Rt);
                        }
                    }
                    return new { Status = "true", items = RList, total_count = RList.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }


    //Aquisition Through

    public object _AquisitionThr(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<AquisitionThr> ATL = new List<AquisitionThr>();
                    AquisitionThr At;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "PM_GET_ACQISITION_THROUGH");
                    sp.Command.AddParameter("@AquisitionThr", RType.AcqThr, DbType.String);


                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            At = new AquisitionThr();
                            At.PN_ACQISITION_ID = sdr["PN_ACQISITION_ID"].ToString();
                            At.PN_ACQISITION_THROUGH = sdr["PN_ACQISITION_THROUGH"].ToString();
                            ATL.Add(At);
                        }
                    }
                    return new { Status = "true", items = ATL, total_count = ATL.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }


    //Entity Type

    public object _EntityType(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<EntityType> ETL = new List<EntityType>();
                    EntityType Et;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "PM_GET_ENTITY_TYPES");
                    sp.Command.AddParameter("@EntityType", RType.EntityType, DbType.String);


                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            Et = new EntityType();
                            Et.CHE_CODE = sdr["CHE_CODE"].ToString();
                            Et.CHE_NAME = sdr["CHE_NAME"].ToString();
                            ETL.Add(Et);
                        }
                    }
                    return new { Status = "true", items = ETL, total_count = ETL.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }


    //Floor 

    public object _FloorDetails(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<FloorDetails> FLT = new List<FloorDetails>();
                    FloorDetails FL;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "USP_getFloors_masters");
                    sp.Command.AddParameter("@Floor", RType.Floor, DbType.String);


                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            FL = new FloorDetails();
                            FL.FLR_NAME = sdr["FLR_NAME"].ToString();
                            FL.FLR_CODE = sdr["FLR_CODE"].ToString();
                            FLT.Add(FL);
                        }
                    }
                    return new { Status = "true", items = FLT, total_count = FLT.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }
    public object _PropertyType(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<PropType> PRL = new List<PropType>();
                    PropType PT;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "PM_GET_ACTPROPTYPE");
                    sp.Command.AddParameter("@PropertyType", RType.PropertyType, DbType.String);


                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            PT = new PropType();
                            PT.PN_PROPERTYTYPE = sdr["PN_PROPERTYTYPE"].ToString();
                            PT.PN_TYPEID = sdr["PN_TYPEID"].ToString();
                            PRL.Add(PT);
                        }
                    }
                    return new { Status = "true", items = PRL, total_count = PRL.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }
    public object _FlooringType(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<FlooringType> FTL = new List<FlooringType>();
                    FlooringType FT;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "PM_GET_FLOORING_TYPES");
                    sp.Command.AddParameter("@FlooringType", RType.FlooringType, DbType.String);


                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            FT = new FlooringType();
                            FT.PM_FT_SNO = sdr["PM_FT_SNO"].ToString();
                            FT.PM_FT_TYPE = sdr["PM_FT_TYPE"].ToString();
                            FTL.Add(FT);
                        }
                    }
                    return new { Status = "true", items = FTL, total_count = FTL.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }
    public object _InsuranceType(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<InsuranceType> ITL = new List<InsuranceType>();
                    InsuranceType IT;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "GET_INSURANCE_TYPE");
                    sp.Command.AddParameter("@InsuranceType", RType.InsurenceType, DbType.String);


                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            IT = new InsuranceType();
                            IT.ID = sdr["ID"].ToString();
                            IT.INSURANCE_TYPE = sdr["INSURANCE_TYPE"].ToString();
                            ITL.Add(IT);
                        }
                    }
                    return new { Status = "true", items = ITL, total_count = ITL.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }
    //Return Categories
    public Categories _ReturnCategories(string SType, string Key)
    {
        Categories Categ = new Categories();
        if (!(Key == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(Key);
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_GET_CATEGORIES");
                sp.Command.AddParameter("@CCAT", SType, DbType.String);
                sp.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp.GetReader())
                {
                    while (sdr.Read())
                    {
                        Categ.MAINCAT = sdr["MAINCAT"].ToString();
                        Categ.SUBCAT = sdr["SUBCAT"].ToString();
                        Categ.CHILDCAT = sdr["CHILDCAT"].ToString();
                    }
                }
                return Categ;
            }
            catch (SqlException ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(ex);
                return Categ;
            }
        }
        else
        {
            return Categ;
        }
    }
    //View SLA
    public object _ViewSLADetails(RaiseRequest K)
    {
        if (!(K.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
                Categories VALTYPES = _ReturnCategories(K.ServiceType, K.APIKey);
                if (VALDB.AURID != null)
                {
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_GET_SLA_DETAILS_BY_LOCATION");
                    sp.Command.AddParameter("@LOC", VALDB.AUR_LOCATION, DbType.String);
                    sp.Command.AddParameter("@MCAT", VALTYPES.MAINCAT, DbType.String);
                    sp.Command.AddParameter("@SCAT", VALTYPES.SUBCAT, DbType.String);
                    sp.Command.AddParameter("@CCAT", VALTYPES.CHILDCAT, DbType.String);
                    sp.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.Int32);
                    ds = sp.GetDataSet();

                    return new { Status = "true", data = new { SLA = ds.Tables[0], Mapping = ds.Tables[1] } };
                }
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        return new { Status = "false", Message = MessagesVM.Err };
    }

    //Get SLA
    public object GetSLA(GETSLA K)
    {
        if (!(K.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
                Categories VALTYPES = _ReturnCategories(K.ChildCategory, K.APIKey);
                if (VALDB.AURID != null)
                {
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "CHECK_SERVICE_INCHARGE");
                    sp.Command.AddParameter("@LOC", K.Location, DbType.String);
                    sp.Command.AddParameter("@MAIN_CATEGORY", VALTYPES.MAINCAT, DbType.String);
                    sp.Command.AddParameter("@SUB_CATEGORY", VALTYPES.SUBCAT, DbType.String);
                    sp.Command.AddParameter("@CHILD_CATEGORY", VALTYPES.CHILDCAT, DbType.String);
                    sp.Command.AddParameter("@CMP_ID", VALDB.COMPANYID, DbType.Int32);
                    //ds = sp.GetDataSet();
                    int i = (int)sp.ExecuteScalar();
                    return new { Status = i };
                }
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        return new { Status = "false", Message = MessagesVM.Err };
    }

    //Submit Raise Request
    public object _RaiseHDRequest(HttpRequest httpRequest)
    {
        try
        {
            String Key = httpRequest.Params["APIKey"];
            String LType = httpRequest.Params["LocationType"];
            String SType = httpRequest.Params["ServiceType"];
            String Prb = httpRequest.Params["Problem"];
            String RCall = httpRequest.Params["RepeatCall"];
            String ICall = httpRequest.Params["ImpactType"];
            String UCall = httpRequest.Params["UrgencyType"];
            String UPhone = httpRequest.Params["PhoneNo"];

            var cnt = GetTotalHDReqCount(Key);
            string HelDeskReq = "";
            HelDeskReq = (DateTime.Now.ToString("ddMMyyyy") + '/' + cnt).ToString();

            ValidateKey VALDB = Secure._ValidateAPIKey(Key);
            Categories VALTYPES = _ReturnCategories(SType, Key);
            if (httpRequest.Files.AllKeys.Length == 0)
            {
                if (VALDB.AURID != null)
                {
                    SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_RAISE_HELP_DESK_REQUEST");
                    sp.Command.AddParameter("@REQ_ID", HelDeskReq, DbType.String);
                    sp.Command.AddParameter("@STATUS", 1, DbType.String);
                    sp.Command.AddParameter("@LOC_CODE", LType, DbType.String);
                    sp.Command.AddParameter("@MNC_CODE", VALTYPES.MAINCAT, DbType.String);
                    sp.Command.AddParameter("@SUB_CAT", VALTYPES.SUBCAT, DbType.String);
                    sp.Command.AddParameter("@CHILD_CAT", VALTYPES.CHILDCAT, DbType.String);
                    sp.Command.AddParameter("@REPEATCALL", RCall, DbType.String);
                    sp.Command.AddParameter("@PROB_DESC", Prb, DbType.String);
                    sp.Command.AddParameter("@IMPACT", ICall, DbType.String);
                    sp.Command.AddParameter("@URGENCY", UCall, DbType.String);
                    sp.Command.AddParameter("@SER_CALL_LOG_BY", VALDB.AURID, DbType.String);
                    sp.Command.AddParameter("@SER_REQ_TYPE", 0, DbType.String);
                    sp.Command.AddParameter("@RAISED_BY", VALDB.AURID, DbType.String);
                    sp.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.Int32);
                    sp.Command.AddParameter("@PHONENO", UPhone, DbType.String);
                    sp.ExecuteScalar();
                    return new { Status = "true", Message = MessagesVM.RequestRaised, RequestId = HelDeskReq };
                }
            }
            else
            {
                string obj = InsertUploadImages(httpRequest, HelDeskReq, VALDB.TENANT_ID, VALDB.AURID);
                if (obj == "1")
                {
                    try
                    {
                        if (VALDB.AURID != null)
                        {
                            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_RAISE_HELP_DESK_REQUEST");
                            sp.Command.AddParameter("@REQ_ID", HelDeskReq, DbType.String);
                            sp.Command.AddParameter("@STATUS", 1, DbType.String);
                            sp.Command.AddParameter("@LOC_CODE", LType, DbType.String);
                            sp.Command.AddParameter("@MNC_CODE", VALTYPES.MAINCAT, DbType.String);
                            sp.Command.AddParameter("@SUB_CAT", VALTYPES.SUBCAT, DbType.String);
                            sp.Command.AddParameter("@CHILD_CAT", VALTYPES.CHILDCAT, DbType.String);
                            sp.Command.AddParameter("@REPEATCALL", RCall, DbType.String);
                            sp.Command.AddParameter("@PROB_DESC", Prb, DbType.String);
                            sp.Command.AddParameter("@IMPACT", ICall, DbType.String);
                            sp.Command.AddParameter("@URGENCY", UCall, DbType.String);
                            sp.Command.AddParameter("@SER_CALL_LOG_BY", VALDB.AURID, DbType.String);
                            sp.Command.AddParameter("@SER_REQ_TYPE", 0, DbType.String);
                            sp.Command.AddParameter("@RAISED_BY", VALDB.AURID, DbType.String);
                            sp.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.Int32);
                            sp.Command.AddParameter("@PHONENO", UPhone, DbType.String);
                            sp.ExecuteScalar();
                            return new { Status = "true", Message = MessagesVM.RequestRaised, RequestId = HelDeskReq };
                        }
                        else
                        {
                            return new { Status = "false", data = (object)null };
                        }
                    }
                    catch (SqlException SqlExcp)
                    {
                        ErrorHandler erhndlr = new ErrorHandler();
                        erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler erhndlr = new ErrorHandler();
                        erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
                    };
                }
                else
                {
                    return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
                }
            }
            return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    //Upload Image
    public string InsertUploadImages(HttpRequest httpRequest, string RequestId, string DB, string User)
    {
        try
        {
            for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
            {
                var req = httpRequest.Files[i];
                var fn = req.FileName;

                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(DB + "." + "API_HD_INSERT_UPLOADED_FILES");
                sp2.Command.AddParameter("@REQ_ID", RequestId, DbType.String);
                sp2.Command.AddParameter("@STATUS_ID", 1, DbType.Int32);
                sp2.Command.AddParameter("@FILENAME", fn, DbType.String);
                sp2.Command.AddParameter("@UPLOAD_PATH", (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn, DbType.String);
                sp2.Command.AddParameter("@AUR_ID", User, DbType.String);
                sp2.ExecuteScalar();

                var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "HelpdeskManagement\\HDImages\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);
                req.SaveAs(filePath);
            }
            return "1";
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    //View Requests
    public object _ViewRequests(Pagination K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        ViewReq req;
        List<ViewReq> reqlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_VIEW_REQUISITIONS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@pageNum", K.PageNo, DbType.Int32);
                sp2.Command.AddParameter("@pageSize", K.PageSize, DbType.Int32);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_STATUS = sdr["STA_ID"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        reqlst.Add(req);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }

    //View&Update Requests
    public object _ViewUpdateRequestList(Pagination K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        ViewReq req;
        List<ViewReq> reqlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_UPDATE_REQUISITIONS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@pageNum", K.PageNo, DbType.Int32);
                sp2.Command.AddParameter("@pageSize", K.PageSize, DbType.Int32);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_STATUS = sdr["STA_ID"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        reqlst.Add(req);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Search Requests
    public object _SearchRequests(string Key, string FilterParam)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        ViewReq req;
        List<ViewReq> Srchlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_FILTER_REQUISITIONS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@FILTER", FilterParam, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_STATUS = sdr["STA_ID"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        Srchlst.Add(req);
                    }
                }
                return new { Status = "true", data = Srchlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Get Status
    public object _Status(string Key, int Type)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        HDStatus Sts;
        List<HDStatus> Stalst = new List<HDStatus>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_STATUS");
                sp2.Command.AddParameter("@TYPE", Type, DbType.Int32);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        Sts = new HDStatus();
                        Sts.STA_ID = Convert.ToInt32(sdr["STA_ID"].ToString());
                        Sts.STA_TITLE = sdr["STA_DESC"].ToString();
                        Stalst.Add(Sts);
                    }
                }
                return new { Status = "true", data = Stalst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Cancel Request
    public object _CancelRequest(HDParams Param)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_CANCEL_VIEW_REQUISITON");
                sp.Command.AddParameter("@SER_ID", Param.RequestID, DbType.String);
                sp.Command.AddParameter("@SER_STATUS", 3, DbType.String);
                sp.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp.Command.AddParameter("@SER_PROB_DESC", Param.ProblemDesc, DbType.String);
                int pcnt = (int)sp.ExecuteScalar();
                if (pcnt == 1)
                {
                    return new { Status = "true", Message = MessagesVM.CancelRequest };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Modify Request
    public object _ModifyRequest(ModifyViewReq MReq)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(MReq.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_MODIFY_REQUEST");
                sp2.Command.AddParameter("@REQ_ID", MReq.RequestId, DbType.String);
                sp2.Command.AddParameter("@STATUS_ID", 2, DbType.Int32);
                sp2.Command.AddParameter("@IMPACT", MReq.Impact, DbType.String);
                sp2.Command.AddParameter("@URGENCY", MReq.Urgency, DbType.String);
                sp2.Command.AddParameter("@REPEAT", MReq.Repeat, DbType.String);
                sp2.Command.AddParameter("@REMARKS", MReq.Remarks, DbType.String);
                sp2.Command.AddParameter("@PROBLEM", MReq.Problem, DbType.String);
                sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@FEEDBACK", MReq.FeedBack, DbType.String);
                sp2.ExecuteScalar();
                return new { Status = "true", Message = MessagesVM.UPDATE_REQ };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Submit Feedback
    public object _SubmitFeedback(HDParams Param)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_INSERT_USER_FEEDBACK");
                sp.Command.AddParameter("@REQ_ID", Param.RequestID, DbType.String);
                sp.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp.Command.AddParameter("@FBD_CODE", Param.Feedback, DbType.String);
                sp.Command.AddParameter("@REMARKS", Param.Remarks, DbType.String);
                sp.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                int pcnt = (int)sp.ExecuteScalar();
                if (pcnt == 1)
                {
                    return new { Status = "true", Message = MessagesVM.FeedbackRequest };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Reassign Users
    public object _ReAssign(RaiseRequest Param)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
        Categories VALTYPES = _ReturnCategories(Param.ServiceType, Param.APIKey);
        ReAssignName ReAsg;
        List<ReAssignName> ReAsglst = new List<ReAssignName>();
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_BIND_INCHARGES");
                sp.Command.AddParameter("@SEM_LOC_CODE", Param.LocationType, DbType.String);
                sp.Command.AddParameter("@SEM_MNC_CODE", VALTYPES.MAINCAT, DbType.String);
                sp.Command.AddParameter("@SEM_SUBC_CODE", VALTYPES.SUBCAT, DbType.String);
                sp.Command.AddParameter("@SEM_CHC_CODE", VALTYPES.CHILDCAT, DbType.String);
                sp.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp.GetReader())
                {

                    while (sdr.Read())
                    {
                        ReAsg = new ReAssignName();
                        ReAsg.REASSING_EMP_ID = sdr["AUR_ID"].ToString();
                        ReAsg.REASSING_EMP_NAME = sdr["ASSIGNED_TO"].ToString();
                        ReAsglst.Add(ReAsg);
                    }

                }
                return new { Status = "true", data = ReAsglst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Update Picture
    public object _UpdatePicture(HttpRequest httpRequest)
    {
        String Key = httpRequest.Params["APIKey"];
        String RID = httpRequest.Params["RequestId"];

        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
                {
                    var req = httpRequest.Files[i];
                    var fn = req.FileName;

                    SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_UPDATE_PICTURE");
                    sp2.Command.AddParameter("@REQ_ID", RID, DbType.String);
                    sp2.Command.AddParameter("@FILENAME", fn, DbType.String);
                    sp2.Command.AddParameter("@STATUS_ID", 1, DbType.Int32);
                    sp2.Command.AddParameter("@UPLOAD_PATH", (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn, DbType.String);
                    sp2.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                    sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                    sp2.ExecuteScalar();

                    var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "HelpdeskManagement\\HDImages\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);
                    req.SaveAs(filePath);
                }
                return new { Status = "true" };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Remove Picture
    public object _RemovePicture(Image Img)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Img.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_REMOVE_UPLOADED_FILES");
                sp2.Command.AddParameter("@ID", Img.IMG_ID, DbType.String);
                sp2.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                sp2.ExecuteScalar();
                return new { Status = "true" };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Get Request Details
    public object _ViewDetails(HDParams Params)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Params.APIKey);
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_VIEW_DETAILS");
                sp2.Command.AddParameter("@REQ_ID", Params.RequestID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                ds = sp2.GetDataSet();
                return new { Status = "true", data = new { RequestDetails = ds.Tables[0], Images = ds.Tables[1] } };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Dropdown List Impact, Urgency, Repeat , Feedback,Status
    public object _ConsolidatedDDL(string Key)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_CONSOLIDATED_DDLIST");
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                DataSet cds = sp2.GetDataSet();
                return new { Status = "true", data = new { Impact = cds.Tables[0], Urgency = cds.Tables[1], Repeat = cds.Tables[2], Feedback = cds.Tables[3], Status = cds.Tables[4] } };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    #endregion

    #region MileStone #04
    //Get Update Request
    public object _UpdateViewRequests(Pagination K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        ViewReq req;
        List<ViewReq> reqlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_GET_PENDING_REQUESTS_INCHARGE");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@pageNum", K.PageNo, DbType.String);
                sp2.Command.AddParameter("@pageSize", K.PageSize, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.REQ_STATUS = sdr["SER_STATUS"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        reqlst.Add(req);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Search Update Request
    public object _SearchUpdateRequests(string Key, string FilterParam)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        ViewReq req;
        List<ViewReq> Srchlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_FILTER_UPDATE_REQUISITIONS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@FILTER", FilterParam, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_STATUS = sdr["STA_ID"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        Srchlst.Add(req);
                    }
                }
                return new { Status = "true", data = Srchlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Search ReAssign Request
    public object _SearchReAssignRequests(string Key, string FilterParam)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        ViewReq req;
        List<ViewReq> Srchlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_FILTER_REASSIGN_REQUISITIONS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@FILTER", FilterParam, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_STATUS = sdr["STA_ID"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        Srchlst.Add(req);
                    }
                }
                return new { Status = "true", data = Srchlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Multiple Request Approval
    public object _UpdateMultipleRequest(ListHDParams Param)
    {

        ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                foreach (var values in Param.HdParmsList)
                {
                    SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_VIEW_UPDATE_REQ_UPDATE");
                    sp2.Command.AddParameter("@REQ_ID", values.RequestID, DbType.String);
                    sp2.Command.AddParameter("@STA_ID", values.Status, DbType.Int32);
                    sp2.Command.AddParameter("@REMARKS", values.Remarks, DbType.String);
                    sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                    sp2.Command.AddParameter("@COST", values.Cost, DbType.String);
                    sp2.ExecuteScalar();
                }
                return new { Status = "true", Message = MessagesVM.UPDATE_REQ };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", Message = MessagesVM.Err };
        }
    }
    //Single Request Approval
    public object _UpadteSingleRequest(HttpRequest httpRequest)
    {

        String Key = httpRequest.Params["APIKey"];
        String RID = httpRequest.Params["RequestId"];
        String RMKS = httpRequest.Params["Remarks"];
        String STS = httpRequest.Params["Status"];
        string CST = httpRequest.Params["Cost"];
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_VIEW_UPDATE_REQ_UPDATE");
                sp2.Command.AddParameter("@REQ_ID", RID, DbType.String);
                sp2.Command.AddParameter("@STA_ID", STS, DbType.Int32);
                sp2.Command.AddParameter("@REMARKS", RMKS, DbType.String);
                sp2.Command.AddParameter("@COST", CST, DbType.String);

                sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                sp2.ExecuteScalar();
                for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
                {
                    var req = httpRequest.Files[i];
                    var fn = req.FileName;

                    string obj = InsertUploadImages(httpRequest, RID, VALDB.TENANT_ID, VALDB.AURID);
                    if (obj == "1")
                    {

                        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "HelpdeskManagement\\HDImages\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);
                        req.SaveAs(filePath);
                    }
                }
                return new { Status = "true", Message = MessagesVM.UPDATE_REQ };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Get ReAssign Details
    public object _ReAssignViewRequests(Pagination K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        ViewReq req;
        List<ViewReq> reqlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_VIEW_REQUISITIONS_TO_ASSIGN");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@pageNum", K.PageNo, DbType.String);
                sp2.Command.AddParameter("@pageSize", K.PageSize, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.MAIN_CAT_CODE = sdr["MAINCAT_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBCAT_CODE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHILDCAT_CODE"].ToString();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.REQ_STATUS = sdr["SER_STATUS"].ToString();
                        req.ASSIGN = sdr["ASSIGNED_TO"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.REQ_STATUS_NAME = sdr["STATUS"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.ASSIGNED_TO_NAME = sdr["ASSIGNED_TO_NAME"].ToString();
                        reqlst.Add(req);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //ReAssign Single and Multiple Requests Approval
    public object _ReAssignMultipleRequest(ListHDParams Param)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                foreach (var values in Param.HdParmsList)
                {
                    SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_ASSIGN_REQUEST");
                    sp2.Command.AddParameter("@REQ_ID", values.RequestID, DbType.String);
                    sp2.Command.AddParameter("@ASSIGN_TO", values.AssignTo, DbType.String);
                    sp2.Command.AddParameter("@UPDATE_REMARKS", values.Remarks, DbType.String);
                    sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                    sp2.ExecuteScalar();
                }
                return new { Status = "true", Message = MessagesVM.REQ_REASSIGN_SUBMIT };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", Message = MessagesVM.Err };
        }
    }

    //Reopen View Request Details
    public object _ReOpenViewRequests(Pagination K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        ViewReq req;
        List<ViewReq> reqlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_GET_CLOSED_TICKET");
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@pageNum", K.PageNo, DbType.String);
                sp2.Command.AddParameter("@pageSize", K.PageSize, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHILD_CAT_CODE"].ToString();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.ASSIGN = sdr["ASSIGNED_TO"].ToString();
                        req.REQ_STATUS = sdr["SER_STATUS"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        reqlst.Add(req);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Reopen Ticket
    public object _ReOpenRequest(HDParams Param)
    {

        ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_MODIFY_REQ_REOPEN_TICKET");
                sp2.Command.AddParameter("@REQ_ID", Param.RequestID, DbType.String);
                sp2.Command.AddParameter("@REMARKS", Param.Remarks, DbType.String);
                sp2.Command.AddParameter("@SER_CALL_TYPE", "Y", DbType.String);
                sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                sp2.ExecuteScalar();
                return new { Status = "true", Message = MessagesVM.UM_RE_OPEN };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", Message = MessagesVM.Err };
        }
    }
    //Search Reopen
    public object _SearchReOpenRequests(string Key, string FilterParam)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        ViewReq req;
        List<ViewReq> Srchlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_FILTER_REOPEN_REQUISITIONS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@FILTER", FilterParam, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_STATUS = sdr["STA_ID"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        Srchlst.Add(req);
                    }
                }
                return new { Status = "true", data = Srchlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    #endregion

    #region MileStone #05
    public object _GetReport(ReportParams Params)
    {
        ReportDataSet RP;
        List<ReportDataSet> RPLST = new List<ReportDataSet>();
        ValidateKey VALDB = Secure._ValidateAPIKey(Params.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_GET_REPORT_BY_USER");
                sp2.Command.AddParameter("@FROMDATE", Params.FromDate, DbType.DateTime);
                sp2.Command.AddParameter("@TODATE", Params.ToDate, DbType.DateTime);
                sp2.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@FLAG", 1, DbType.String);
                DataSet ds = new DataSet();
                ds = sp2.GetDataSet();
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        RP = new ReportDataSet();
                        RP.REQUEST_ID = sdr["RequisitionId"].ToString();
                        RP.SERVICE_TYPE = sdr["ChildCategory"].ToString();
                        RP.LOCATION = sdr["LocName"].ToString();
                        RP.REQ_BY = sdr["REQUESTEDBY_NAME"].ToString();
                        RP.STATUS = sdr["STA_TITLE"].ToString();
                        RP.SLA = sdr["DEFINED_TAT"].ToString();
                        RP.TAT = sdr["TAT"].ToString();
                        RP.REQ_DATE = sdr["CallLogDate"].ToString();
                        RP.ASSIGNED_TO = sdr["Helpdesk_InchargeName"].ToString();
                        RPLST.Add(RP);
                    }
                    sdr.Close();
                }
                return new { Status = "true", data = RPLST };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", Message = MessagesVM.Err };
        }
    }

    public object _SearchReport(ReportFilterParams FilterParam)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(FilterParam.APIKey);
        ReportDataSet RP;
        List<ReportDataSet> RPLST = new List<ReportDataSet>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_GET_REPORT_BY_USER");
                sp2.Command.AddParameter("@FILTER", FilterParam.Filter, DbType.String);
                sp2.Command.AddParameter("@FROMDATE", FilterParam.FromDate, DbType.DateTime);
                sp2.Command.AddParameter("@TODATE", FilterParam.ToDate, DbType.DateTime);
                sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@FLAG", 2, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        RP = new ReportDataSet();
                        RP.REQUEST_ID = sdr["SER_REQ_ID"].ToString();
                        RP.SERVICE_TYPE = sdr["CHC_TYPE_NAME"].ToString();
                        RP.LOCATION = sdr["LCM_NAME"].ToString();
                        RP.REQ_BY = sdr["AUR_KNOWN_AS"].ToString();
                        RP.STATUS = sdr["STA_TITLE"].ToString();
                        RP.SLA = sdr["SLA"].ToString();
                        RP.TAT = sdr["TAT_SLA"].ToString();
                        RP.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        RP.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        RP.MOBILE_NO = sdr["SER_MOBILE"].ToString();
                        RPLST.Add(RP);
                    }
                }
                return new { Status = "true", data = RPLST };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }

    public object _GetTotalDashboardDetails(DashboardDetails K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        ViewReq req;
        List<ViewReq> reqlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {

                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "GN_TICKETS_DETAILS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@STATUS", K.Status, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.REQ_STATUS = sdr["REQ_STATUS"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_NAME"].ToString();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        reqlst.Add(req);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }

    public object _SLAViolation(Key K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        SLAViolation data;
        List<SLAViolation> reqlst = new List<SLAViolation>();
        if (VALDB.AURID != null)
        {
            try
            {

                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "GN_SLA_VIOLATION");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        data = new SLAViolation();
                        data.REQUEST_ID = sdr["SERE_SER_REQ_ID"].ToString();
                        data.MAIN_CATEGORY = sdr["MNC_NAME"].ToString();
                        data.SUB_CATEGORY = sdr["SUBC_NAME"].ToString();
                        data.SERVICE_TYPE = sdr["CHC_TYPE_NAME"].ToString();
                        data.PROBLEM_DESC = sdr["SER_PROB_DESC"].ToString();
                        data.STATUS = sdr["STA_TITLE"].ToString();
                        reqlst.Add(data);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    #endregion

    #region Device Token Update

    public object _UpdateDeviceToken(DeviceToken Tkn)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "UPDATE_DEVICE_TOKEN");
            sp.Command.AddParameter("@API", Tkn.APIKey, DbType.String);
            sp.Command.AddParameter("@TOKEN", Tkn.Token, DbType.String);
            DataSet ds = sp.GetDataSet();
            if (ds.Tables[0].Rows[0]["TKN"].ToString() == "1")
            {
                return new { Status = "true", Message = MessagesVM.UPDATE_TOKEN };
            }
            else
            {
                return new { Status = "false", Message = MessagesVM.Err };
            }
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };

    }
    #endregion

    #region Emergency Request
    public object _Emergency(Key K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        Emergency EM;
        List<Emergency> EMLST = new List<Emergency>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_GET_EMERGENCY_REQUEST");
                sp2.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        EM = new Emergency();
                        EM.SERVICE_TYPE_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        EM.SERVICE_TYPE_NAME = sdr["CHC_TYPE_NAME"].ToString();
                        EM.SERVICE_TYPE_IMAGE = sdr["CHC_TYPE_IMAGE"].ToString();
                        EMLST.Add(EM);
                    }
                }
                return new { Status = "true", data = EMLST };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    #endregion

    #region Notifications

    public object _NotificationList(string APIKey)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(APIKey);
        NotificationHistory NT;
        List<NotificationHistory> NTLST = new List<NotificationHistory>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_NOTIFICATION_LIST");
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        NT = new NotificationHistory();
                        NT.SNO = sdr["SNO"].ToString();
                        NT.NOTIF_BODY = sdr["NOTIF_BODY"].ToString();
                        NT.NOTIF_RESULT = sdr["NOTIF_RESULT"].ToString();
                        NT.NOTIF_REQUEST_ID_STATUS = sdr["NOTIF_REQUEST_ID_STATUS"].ToString();
                        NT.NOTIF_REQUEST_ID = sdr["NOTIF_REQUEST_ID"].ToString();
                        NTLST.Add(NT);
                    }
                }
                //return new { Status = "Ok", data = NTLST };
                return new { data = NTLST };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }

    public object _UpdateNotificationStatus(StatusUpdate Tkn)
    {
        try
        {
            ValidateKey VALDB = Secure._ValidateAPIKey(Tkn.APIKey);
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_UPDATE_NOTIF_STATUS");
            sp.Command.AddParameter("@RID", Tkn.RequestId, DbType.String);
            sp.Command.AddParameter("@SNO", Tkn.Sno, DbType.String);
            DataSet ds = sp.GetDataSet();
            if (ds.Tables[0].Rows[0]["STS"].ToString() == "1")
            {
                return new { Status = "true", Message = MessagesVM.StatusUpdated };
            }
            else
            {
                return new { Status = "false", Message = MessagesVM.Err };
            }
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };

    }

    //public object _ValidateGoogleEmail() { 

    //}
    #endregion

    #region Add Property

    //General Details
    public object GetGeneralDetails(HDModel main)
    {
        try
        {
            List<GetGeneralDetails> GDList = new List<GetGeneralDetails>();
            GetGeneralDetails GD = new GetGeneralDetails();
            List<RequestTypes> RequestList = new List<RequestTypes>();
            RequestTypes Request = new RequestTypes();
            List<PropertyNature> PMList = new List<PropertyNature>();
            PropertyNature PM = new PropertyNature();
            List<AcquistionThrgh> AQTList = new List<AcquistionThrgh>();
            AcquistionThrgh AQT = new AcquistionThrgh();
            List<Entity> EntityList = new List<Entity>();
            Entity Entity = new Entity();
            List<Locations> LocationsList = new List<Locations>();
            Locations Locations = new Locations();
            List<PropertyType> PropertyTypeList = new List<PropertyType>();
            PropertyType PropertyType = new PropertyType();
            List<Recommened> RecommenedList = new List<Recommened>();
            Recommened Recommened = new Recommened();
            List<FlooringTypes> FloorsList = new List<FlooringTypes>();
            FlooringTypes Floors = new FlooringTypes();
            List<InsuranceTypes> InsuranceTypesList = new List<InsuranceTypes>();
            InsuranceTypes InsuranceTypes = new InsuranceTypes();
            List<StampDuty> StampDutyList = new List<StampDuty>();
            StampDuty StampDuty = new StampDuty();

            string DB = _ReturnDB(main);
            ErrorHandler err = new ErrorHandler();
            err._WriteErrorLog_string("DatabaseCheck" + JsonConvert.SerializeObject(DB));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
                sp = new SubSonic.StoredProcedure(DB + "." + "PM_GET_GENERAL_DETAILS_MOBILE");
                sp.Command.AddParameter("@USR_ID", main.UserId, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Request = new RequestTypes();
                    Request.PM_RT_SNO = Convert.ToInt32(ds.Tables[0].Rows[i]["PM_RT_SNO"].ToString());
                    Request.PM_RT_TYPE = ds.Tables[0].Rows[i]["PM_RT_TYPE"].ToString();
                    RequestList.Add(Request);
                }
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    PM = new PropertyNature();
                    PM.PM_PN_ID = Convert.ToInt32(ds.Tables[1].Rows[i]["PM_PN_ID"].ToString());
                    PM.PM_PN_NAME = ds.Tables[1].Rows[i]["PM_PN_NAME"].ToString();
                    PMList.Add(PM);
                }
                for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                {
                    AQT = new AcquistionThrgh();
                    AQT.PN_ACQISITION_ID = Convert.ToInt32(ds.Tables[2].Rows[i]["PN_ACQISITION_ID"].ToString());
                    AQT.PN_ACQISITION_THROUGH = ds.Tables[2].Rows[i]["PN_ACQISITION_THROUGH"].ToString();
                    AQTList.Add(AQT);
                }
                for (int i = 0; i < ds.Tables[3].Rows.Count; i++)
                {
                    Entity = new Entity();
                    Entity.CHE_CODE = ds.Tables[3].Rows[i]["CHE_CODE"].ToString();
                    Entity.CHE_NAME = ds.Tables[3].Rows[i]["CHE_NAME"].ToString();
                    EntityList.Add(Entity);
                }
                for (int i = 0; i < ds.Tables[4].Rows.Count; i++)
                {
                    Locations = new Locations();
                    Locations.LCM_CODE = ds.Tables[4].Rows[i]["LCM_CODE"].ToString();
                    Locations.LCM_NAME = ds.Tables[4].Rows[i]["LCM_NAME"].ToString();
                    Locations.LCM_CNY_ID = ds.Tables[4].Rows[i]["LCM_CNY_ID"].ToString();
                    Locations.LCM_CTY_ID = ds.Tables[4].Rows[i]["LCM_CTY_ID"].ToString();
                    LocationsList.Add(Locations);
                }
                for (int i = 0; i < ds.Tables[5].Rows.Count; i++)
                {
                    PropertyType = new PropertyType();
                    PropertyType.PN_TYPEID = Convert.ToInt32(ds.Tables[5].Rows[i]["PN_TYPEID"].ToString());
                    PropertyType.PN_PROPERTYTYPE = ds.Tables[5].Rows[i]["PN_PROPERTYTYPE"].ToString();
                    PropertyTypeList.Add(PropertyType);
                }
                for (int i = 0; i < ds.Tables[6].Rows.Count; i++)
                {
                    Recommened = new Recommened();
                    Recommened.PM_RCMD_ID = Convert.ToInt32(ds.Tables[6].Rows[i]["PM_RCMD_ID"].ToString());
                    Recommened.PM_RCMD_NAME = ds.Tables[6].Rows[i]["PM_RCMD_NAME"].ToString();
                    RecommenedList.Add(Recommened);
                }
                for (int i = 0; i < ds.Tables[7].Rows.Count; i++)
                {
                    Floors = new FlooringTypes();
                    Floors.PM_FT_SNO = Convert.ToInt32(ds.Tables[7].Rows[i]["PM_FT_SNO"].ToString());
                    Floors.PM_FT_TYPE = ds.Tables[7].Rows[i]["PM_FT_TYPE"].ToString();
                    FloorsList.Add(Floors);
                }
                for (int i = 0; i < ds.Tables[8].Rows.Count; i++)
                {
                    InsuranceTypes = new InsuranceTypes();
                    InsuranceTypes.ID = Convert.ToInt32(ds.Tables[8].Rows[i]["ID"].ToString());
                    InsuranceTypes.INSURANCE_TYPE = ds.Tables[8].Rows[i]["INSURANCE_TYPE"].ToString();
                    InsuranceTypesList.Add(InsuranceTypes);
                }
                for (int i = 0; i < ds.Tables[9].Rows.Count; i++)
                {
                    StampDuty = new StampDuty();
                    StampDuty.PM_STP_NAME = ds.Tables[9].Rows[i]["PM_STP_NAME"].ToString();
                    StampDutyList.Add(StampDuty);
                }
                GD.REQ_ID = DateTime.Now.ToString("MMddyyyy") + "/PRPREQ/" + ds.Tables[10].Rows[0]["REQ"].ToString();
                GD.RequestTypes = RequestList;
                GD.PropertyNature = PMList;
                GD.AcquistionThrgh = AQTList;
                GD.Entity = EntityList;
                GD.Locations = LocationsList;
                GD.PropertyType = PropertyTypeList;
                GD.Recommened = RecommenedList;
                GD.FlooringTypes = FloorsList;
                GD.InsuranceTypes = InsuranceTypesList;
                GD.StampDuty = StampDutyList;
                GDList.Add(GD);

                return new { GeneralDetailsList = GDList };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //Submit Propert Details
    public object SubmitPropertyDetails(SavePropertDetails main)
    {
        try
        {

            HDModel mn = new HDModel();
            mn.CompanyId = main.CompanyId;
            string DB = _ReturnDB(mn);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
            //sp = new SubSonic.StoredProcedure(DB + "." + "PM_GET_GENERAL_DETAILS_MOBILE");
            String proc_name = DB+"."+ "PM_PROPERTY_SAVEASDRAFT_DETAILS_MOBILE";
            SqlParameter[] param = new SqlParameter[125];

            // General Details
            param[0] = new SqlParameter("@REQUEST_ID", SqlDbType.VarChar);
            param[0].Value = main.REQUEST_ID;
            param[1] = new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar);
            param[1].Value = main.REQUEST_TYPE;
            param[2] = new SqlParameter("@PROP_NATURE", SqlDbType.VarChar);
            param[2].Value = main.PROP_NATURE;
            param[3] = new SqlParameter("@ACQ_TRH", SqlDbType.VarChar);
            param[3].Value = main.ACQ_TRH;
            param[4] = new SqlParameter("@CITY", SqlDbType.VarChar);
            param[4].Value = main.CITY;
            if (main.REQUEST_TYPE == "New")
            {

                param[122] = new SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar);
                param[122].Value = main.PM_PPT_TOT_FLRS;
                param[5] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
                param[5].Value = main.LOCATION;
                param[6] = new SqlParameter("@TOWER", SqlDbType.VarChar);
                param[6].Value = main.TOWER;
                 param[7] = new SqlParameter("@FLOOR", SqlDbType.VarChar);
                param[7].Value = main.FLOOR;
                param[123] = new SqlParameter("@RELOCATION", SqlDbType.VarChar);
                param[123].Value = "";
                param[124] = new SqlParameter("@RELOCADDRS", SqlDbType.VarChar);
                param[124].Value = "";
            }
            else if (main.REQUEST_TYPE == "Relocation")
            {
                param[5] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
                param[5].Value = main.LOCATION;
                param[122] = new SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar);
                param[122].Value = main.PM_PPT_TOT_FLRS;
                param[123] = new SqlParameter("@RELOCATION", SqlDbType.VarChar);
                param[123].Value = main.RELOCATION;
                param[124] = new SqlParameter("@RELOCADDRS", SqlDbType.VarChar);
                param[124].Value = main.RELOCADDRS;
                param[6] = new SqlParameter("@TOWER", SqlDbType.VarChar);
                param[6].Value = main.TOWER;
                param[7] = new SqlParameter("@FLOOR", SqlDbType.VarChar);
                param[7].Value = main.FLOOR;
            }
            else
            {
                // param[4] = new SqlParameter("@CITY", SqlDbType.VarChar)
                // param[4].Value = main.ddlCity.SelectedValue
                param[122] = new SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar);
                param[122].Value = main.PM_PPT_TOT_FLRS;
                param[123] = new SqlParameter("@RELOCATION", SqlDbType.VarChar);
                param[123].Value = "";
                param[124] = new SqlParameter("@RELOCADDRS", SqlDbType.VarChar);
                param[124].Value = "";
                param[5] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
                param[5].Value = main.LOCATION;
                param[6] = new SqlParameter("@TOWER", SqlDbType.VarChar);
                param[6].Value = main.TOWER;
                param[7] = new SqlParameter("@FLOOR", SqlDbType.VarChar);
                // param[7].Value = main.ddlFloor.SelectedValue
                param[7].Value = main.FLOOR;
            }

            param[8] = new SqlParameter("@TOI_BLKS", SqlDbType.Int);
            param[8].Value = main.TOI_BLKS;
            param[9] = new SqlParameter("@PROP_TYPE", SqlDbType.VarChar);
            param[9].Value = main.PROP_TYPE;
            param[10] = new SqlParameter("@PROP_NAME", SqlDbType.VarChar);
            param[10].Value = main.PROP_NAME;

            param[11] = new SqlParameter("@ESTD_YR", SqlDbType.DateTime);
            param[11].Value = main.ESTD_YR;
            param[12] = new SqlParameter("@AGE", SqlDbType.Int);
            param[12].Value = main.AGE;
            param[13] = new SqlParameter("@PROP_ADDR", SqlDbType.VarChar);
            param[13].Value = main.PROP_ADDR;
            param[14] = new SqlParameter("@SING_LOC", SqlDbType.VarChar);
            param[14].Value = main.SING_LOC;

            param[15] = new SqlParameter("@SOC_NAME", SqlDbType.VarChar);
            param[15].Value = main.SOC_NAME;
            param[16] = new SqlParameter("@LATITUDE", SqlDbType.VarChar);
            param[16].Value = main.LATITUDE;
            param[17] = new SqlParameter("@LONGITUDE", SqlDbType.VarChar);
            param[17].Value = main.LONGITUDE;
            param[18] = new SqlParameter("@SCOPE_WK", SqlDbType.VarChar);
            param[18].Value = main.SCOPE_WK;

            param[19] = new SqlParameter("@RECM_PROP", SqlDbType.VarChar);
            param[19].Value = main.RECM_PROP;
            param[20] = new SqlParameter("@RECM_PROP_REM", SqlDbType.VarChar);
            param[20].Value = main.RECM_PROP_REM;

            // Owner Details
            param[21] = new SqlParameter("@OWN_NAME", SqlDbType.VarChar);
            param[21].Value = main.OWN_NAME;
            param[22] = new SqlParameter("@OWN_PH", SqlDbType.VarChar);
            param[22].Value = main.OWN_PH;
            param[23] = new SqlParameter("@PREV_OWN_NAME", SqlDbType.VarChar);
            param[23].Value = main.PREV_OWN_NAME;
            param[24] = new SqlParameter("@PREV_OWN_PH", SqlDbType.VarChar);
            param[24].Value = main.PREV_OWN_PH;
            param[25] = new SqlParameter("@OWN_EMAIL", SqlDbType.VarChar);
            param[25].Value = main.OWN_EMAIL;

            // Area Details
            param[26] = new SqlParameter("@CARPET_AREA", SqlDbType.Decimal);
            param[26].Value = Convert.ToDecimal(main.CARPET_AREA);
            param[27] = new SqlParameter("@BUILTUP_AREA", SqlDbType.Decimal);
            param[27].Value = Convert.ToDecimal(main.BUILTUP_AREA);
            param[28] = new SqlParameter("@COMMON_AREA", SqlDbType.Decimal);
            param[28].Value = Convert.ToDecimal(main.COMMON_AREA);
            param[29] = new SqlParameter("@RENTABLE_AREA", SqlDbType.Decimal);
            param[29].Value = Convert.ToDecimal(main.RENTABLE_AREA);

            param[30] = new SqlParameter("@USABLE_AREA", SqlDbType.Decimal);
            param[30].Value = Convert.ToDecimal(main.USABLE_AREA);
            param[31] = new SqlParameter("@SUPER_BLT_AREA", SqlDbType.Decimal);
            param[31].Value = Convert.ToDecimal(main.SUPER_BLT_AREA);
            param[32] = new SqlParameter("@PLOT_AREA", SqlDbType.Decimal);
            param[32].Value = Convert.ToDecimal(main.PLOT_AREA);
            param[33] = new SqlParameter("@FTC_HIGHT", SqlDbType.Decimal);
            param[33].Value = Convert.ToDecimal(main.FTC_HIGHT);

            param[34] = new SqlParameter("@FTBB_HIGHT", SqlDbType.Decimal);
            param[34].Value = Convert.ToDecimal(main.FTBB_HIGHT);
            param[35] = new SqlParameter("@MAX_CAPACITY", SqlDbType.Int);
            param[35].Value = Convert.ToInt32(main.MAX_CAPACITY);
            param[36] = new SqlParameter("@OPTIMUM_CAPACITY", SqlDbType.Int);
            param[36].Value = main.OPTIMUM_CAPACITY;
            param[37] = new SqlParameter("@SEATING_CAPACITY", SqlDbType.Int);
            param[37].Value = main.SEATING_CAPACITY;

            param[38] = new SqlParameter("@FLR_TYPE", SqlDbType.VarChar);
            param[38].Value = main.FLR_TYPE;
            param[39] = new SqlParameter("@FSI", SqlDbType.VarChar);
            param[39].Value = main.FSI;
            param[40] = new SqlParameter("@FSI_RATIO", SqlDbType.Decimal);
            param[40].Value = Convert.ToDecimal(main.FSI_RATIO);
            param[41] = new SqlParameter("@PFR_EFF", SqlDbType.VarChar);
            param[41].Value = main.PFR_EFF;

            // PURCHASE DETAILS
            param[42] = new SqlParameter("@PUR_PRICE", SqlDbType.Decimal);
            param[42].Value =Convert.ToDecimal(main.PUR_PRICE);
            param[43] = new SqlParameter("@PUR_DATE", SqlDbType.Date);
            param[43].Value = Convert.ToDateTime(main.PUR_DATE);
            param[44] = new SqlParameter("@MARK_VALUE", SqlDbType.Decimal);
            param[44].Value = Convert.ToDecimal(main.MARK_VALUE);

            // GOVT DETAILS
            param[45] = new SqlParameter("@IRDA", SqlDbType.VarChar);
            param[45].Value = main.IRDA;
            param[46] = new SqlParameter("@PC_CODE", SqlDbType.VarChar);
            param[46].Value = main.PC_CODE;
            param[47] = new SqlParameter("@PROP_CODE", SqlDbType.VarChar);
            param[47].Value = main.PROP_CODE;
            param[48] = new SqlParameter("@UOM_CODE", SqlDbType.VarChar);
            param[48].Value = main.UOM_CODE;

            // INSURANCE DETAILS
            param[49] = new SqlParameter("@IN_TYPE", SqlDbType.VarChar);
            param[49].Value = main.IN_TYPE;
            param[50] = new SqlParameter("@IN_VENDOR", SqlDbType.VarChar);
            param[50].Value = main.IN_VENDOR;
            param[51] = new SqlParameter("@IN_AMOUNT", SqlDbType.Decimal);
            param[51].Value =Convert.ToDecimal(main.IN_AMOUNT);
            param[52] = new SqlParameter("@IN_POLICY_NUMBER", SqlDbType.VarChar);
            param[52].Value = main.IN_POLICY_NUMBER;

            param[53] = new SqlParameter("@IN_SDATE", SqlDbType.VarChar);
            param[53].Value = main.IN_SDATE;
            param[54] = new SqlParameter("@IN_EDATE", SqlDbType.VarChar);
            param[54].Value = main.IN_EDATE;

            param[55] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[55].Value = main.AUR_ID;
            param[56] = new SqlParameter("@CMP_ID", SqlDbType.VarChar);
            param[56].Value = 1;
            param[57] = new SqlParameter("@OWN_LANDLINE", SqlDbType.VarChar);
            param[57].Value = main.OWN_LANDLINE;
            param[58] = new SqlParameter("@POA_SIGNED", SqlDbType.VarChar);
            param[58].Value = main.POA_SIGNED;
            // POA Details
            param[59] = new SqlParameter("@POA_NAME", SqlDbType.VarChar);
            param[59].Value = main.POA_NAME;
            param[60] = new SqlParameter("@POA_ADDRESS", SqlDbType.VarChar);
            param[60].Value = main.POA_ADDRESS;
            param[61] = new SqlParameter("@POA_MOBILE", SqlDbType.VarChar);
            param[61].Value = main.POA_MOBILE;
            param[62] = new SqlParameter("@POA_EMAIL", SqlDbType.VarChar);
            param[62].Value = main.POA_EMAIL;
            param[63] = new SqlParameter("@POA_LLTYPE", SqlDbType.VarChar);
            param[63].Value = main.POA_LLTYPE;

            param[64] = new SqlParameter("@IMAGES", SqlDbType.VarChar);
            param[64].Value ="";

            param[65] = new SqlParameter("@ENTITY", SqlDbType.VarChar);
            param[65].Value = main.ENTITY;

            // Physical Condition

            param[66] = new SqlParameter("@PHYCDTN_WALLS", SqlDbType.VarChar);
            param[66].Value = main.PHYCDTN_WALLS;
            param[67] = new SqlParameter("@PHYCDTN_ROOF", SqlDbType.VarChar);
            param[67].Value = main.PHYCDTN_ROOF;
            param[68] = new SqlParameter("@PHYCDTN_CEILING", SqlDbType.VarChar);
            param[68].Value = main.PHYCDTN_CEILING;
            param[69] = new SqlParameter("@PHYCDTN_WINDOWS", SqlDbType.VarChar);
            param[69].Value = main.PHYCDTN_WINDOWS;
            param[70] = new SqlParameter("@PHYCDTN_DAMAGE", SqlDbType.VarChar);
            param[70].Value = main.PHYCDTN_DAMAGE;
            param[71] = new SqlParameter("@PHYCDTN_SEEPAGE", SqlDbType.VarChar);
            param[71].Value = main.PHYCDTN_SEEPAGE;
            param[72] = new SqlParameter("@PHYCDTN_OTHRTNT", SqlDbType.VarChar);
            param[72].Value = main.PHYCDTN_OTHRTNT;
            // Landlord's Scope of work 
            param[73] = new SqlParameter("@LL_VITRIFIED", SqlDbType.VarChar);
            param[73].Value = main.LL_VITRIFIED;
            param[74] = new SqlParameter("@LL_VITRIFIED_RMKS", SqlDbType.VarChar);
            param[74].Value = main.LL_VITRIFIED_RMKS;
            param[75] = new SqlParameter("@LL_WASHRMS", SqlDbType.VarChar);
            param[75].Value = main.LL_WASHRMS;
            param[76] = new SqlParameter("@LL_WASHRM_RMKS", SqlDbType.VarChar);
            param[76].Value = main.LL_WASHRM_RMKS;
            param[77] = new SqlParameter("@LL_PANTRY", SqlDbType.VarChar);
            param[77].Value = main.LL_PANTRY;
            param[78] = new SqlParameter("@LL_PANTRY_RMKS", SqlDbType.VarChar);
            param[78].Value = main.LL_PANTRY_RMKS;
            param[79] = new SqlParameter("@LL_SHUTTER", SqlDbType.VarChar);
            param[79].Value = main.LL_SHUTTER;
            param[80] = new SqlParameter("@LL_SHUTTER_RMKS", SqlDbType.VarChar);
            param[80].Value = main.LL_SHUTTER_RMKS;
            param[81] = new SqlParameter("@LL_OTHERS", SqlDbType.VarChar);
            param[81].Value = main.LL_OTHERS;
            param[82] = new SqlParameter("@LL_OTHERS_RMKS", SqlDbType.VarChar);
            param[82].Value = main.LL_OTHERS_RMKS;
            param[83] = new SqlParameter("@LL_WORK_DAYS", SqlDbType.VarChar);
            param[83].Value = main.LL_WORK_DAYS;
            param[84] = new SqlParameter("@LL_ELEC_EXISTING", SqlDbType.VarChar);
            param[84].Value = main.LL_ELEC_EXISTING;
            param[85] = new SqlParameter("@LL_ELEC_REQUIRED", SqlDbType.VarChar);
            param[85].Value = main.LL_ELEC_REQUIRED;
            // Other Details
            param[89] = new SqlParameter("@OTHR_FLOORING", SqlDbType.VarChar);
            param[89].Value = main.OTHR_FLOORING;
            param[90] = new SqlParameter("@OTHR_FLRG_RMKS", SqlDbType.VarChar);
            param[90].Value = main.OTHR_FLRG_RMKS;
            param[91] = new SqlParameter("@OTHR_WASH_EXISTING", SqlDbType.VarChar);
            param[91].Value = main.OTHR_WASH_EXISTING;
            param[92] = new SqlParameter("@OTHR_WASH_REQUIRED", SqlDbType.VarChar);
            param[92].Value = main.OTHR_WASH_REQUIRED;
            param[93] = new SqlParameter("@OTHR_POTABLE_WTR", SqlDbType.VarChar);
            param[93].Value = main.OTHR_POTABLE_WTR;
            // Cost Details
            param[94] = new SqlParameter("@COST_RENT", SqlDbType.VarChar);
            param[94].Value = main.COST_RENT;
            param[95] = new SqlParameter("@COST_RENT_SFT", SqlDbType.VarChar);
            param[95].Value = main.COST_RENT_SFT;
            param[96] = new SqlParameter("@COST_RATIO", SqlDbType.VarChar);
            param[96].Value = main.COST_RATIO;
            param[97] = new SqlParameter("@COST_OWN_SHARE", SqlDbType.VarChar);
            param[97].Value = main.COST_OWN_SHARE;
            param[98] = new SqlParameter("@COST_SECDEP", SqlDbType.VarChar);
            param[98].Value = main.COST_SECDEP;
            param[99] = new SqlParameter("@COST_GST", SqlDbType.VarChar);
            param[99].Value = main.COST_GST;
            param[100] = new SqlParameter("@COST_MAINTENANCE", SqlDbType.VarChar);
            param[100].Value = main.COST_MAINTENANCE;
            param[101] = new SqlParameter("@COST_ESC", SqlDbType.VarChar);
            param[101].Value = main.COST_ESC;
            param[102] = new SqlParameter("@COST_RENT_FREE", SqlDbType.VarChar);
            param[102].Value = main.COST_RENT_FREE;
            param[103] = new SqlParameter("@COST_STAMP", SqlDbType.VarChar);
            param[103].Value = main.COST_STAMP;
            param[104] = new SqlParameter("@COST_AGREE", SqlDbType.VarChar);
            param[104].Value = main.COST_AGREE;
            // DOCUMENTS VAILABLE
            param[105] = new SqlParameter("@DOC_TITLE", SqlDbType.VarChar);
            param[105].Value = main.DOC_TITLE;
            param[106] = new SqlParameter("@DOC_TITLE_RMKS", SqlDbType.VarChar);
            param[106].Value = main.DOC_TITLE_RMKS;
            param[107] = new SqlParameter("@DOC_OCCUP", SqlDbType.VarChar);
            param[107].Value = main.DOC_OCCUP;
            param[108] = new SqlParameter("@DOC_OCCUP_RMKS", SqlDbType.VarChar);
            param[108].Value = main.DOC_OCCUP_RMKS;
            param[109] = new SqlParameter("@DOC_BUILD", SqlDbType.VarChar);
            param[109].Value = main.DOC_BUILD;
            param[110] = new SqlParameter("@DOC_BUILD_RMKS", SqlDbType.VarChar);
            param[110].Value = main.DOC_BUILD_RMKS;
            param[111] = new SqlParameter("@DOC_PAN", SqlDbType.VarChar);
            param[111].Value = main.DOC_PAN;
            param[112] = new SqlParameter("@DOC_PAN_RMKS", SqlDbType.VarChar);
            param[112].Value = main.DOC_PAN_RMKS;
            param[86] = new SqlParameter("@DOC_TAX", SqlDbType.VarChar);
            param[86].Value = main.DOC_TAX;
            param[87] = new SqlParameter("@DOC_TAX_RMKS", SqlDbType.VarChar);
            param[87].Value = main.DOC_TAX_RMKS;
            param[88] = new SqlParameter("@DOC_OTHR_INFO", SqlDbType.VarChar);
            param[88].Value = main.DOC_OTHR_INFO;

            param[113] = new SqlParameter("@SIGNAGEIMAGES", SqlDbType.VarChar);
            param[113].Value ="";
            param[114] = new SqlParameter("@SIG_LENGTH", SqlDbType.VarChar);
            param[114].Value = main.SIG_LENGTH;
            param[115] = new SqlParameter("@SIG_WIDTH", SqlDbType.VarChar);
            param[115].Value = main.SIG_WIDTH;
            param[116] = new SqlParameter("@AC_OUTDOOR", SqlDbType.VarChar);
            param[116].Value = main.AC_OUTDOOR;
            param[117] = new SqlParameter("@GSB", SqlDbType.VarChar);
            param[117].Value = main.GSB;
            param[118] = new SqlParameter("@AC_OUTDOOR_IMAGE", SqlDbType.VarChar);
            param[118].Value = "";
            param[119] = new SqlParameter("@GSB_IMAGE", SqlDbType.VarChar);
            param[119].Value = "";
            param[120] = new SqlParameter("@INSPECTION_DATE", SqlDbType.DateTime);
            param[120].Value = main.INSPECTION_DATE;
            // 'param[120].Value = main.Convert.ToDateTime(txtsdate.Text)
            param[121] = new SqlParameter("@INSPECTION_BY", SqlDbType.VarChar);
            param[121].Value = main.INSPECTION_BY;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            string Result;
            return new { Result = ds.Tables[0].Rows[0]["Result"].ToString() };
            
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    #endregion
}
#region API Key Validation and Getting DB


public partial class SecureAPIKey
{
    public ValidateKey _ValidateAPIKey(string API)
    {
        ValidateKey ver = new ValidateKey();
        if (!(API == null))
        {

            try
            {

                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "GN_VALIDATE_API");
                sp.Command.AddParameter("@API", API, DbType.String);
                using (IDataReader sdr = sp.GetReader())
                {
                    while (sdr.Read())
                    {
                        ver.TENANT_ID = sdr["TENANT_ID"].ToString();
                        ver.COMPANYID = Convert.ToInt32(sdr["COMPANYID"]);
                        HttpContext.Current.Session["useroffset"] = sdr["TENANT_DT_OFFSET"].ToString();
                        ver.AURID = sdr["USER_ID"].ToString();
                        ver.AUR_LOCATION = sdr["AUR_LOCATION"].ToString();
                    }
                }
                return ver;
            }
            catch (SqlException ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(ex);
                return ver;
            }
        }
        else
        {
            return ver;
        }
    }
}

public partial class ErrorHandler
{
    #region Error Log Logic
    public void _WriteErrorLog(Exception ex)
    {
        string errorLogFilename = "ErrorLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
        string path = HttpContext.Current.Server.MapPath("~/ErrorLogFiles/" + errorLogFilename);
        if (File.Exists(path))
        {
            using (StreamWriter stwriter = new StreamWriter(path, true))
            {
                stwriter.WriteLine("Error Log Start as on " + DateTime.Now.ToString("hh:mm tt"));
                stwriter.WriteLine("Message:" + ex.ToString());
                stwriter.WriteLine("End as on " + DateTime.Now.ToString("hh:mm tt"));
            }
        }
        else
        {
            StreamWriter stwriter = File.CreateText(path);
            stwriter.WriteLine("Error Log Start as on " + DateTime.Now.ToString("hh:mm tt"));
            stwriter.WriteLine("Message: " + ex.ToString());
            stwriter.WriteLine("End as on " + DateTime.Now.ToString("hh:mm tt"));
            stwriter.Close();
        }
    }
    #endregion
    #region Inserting Other Error's In DB Error Log
    public void _OtherExceptionsInDBErrorLog(Exception ex)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "ERROR_LOG");
            sp.Command.AddParameter("@ERROR", ex.ToString(), DbType.String);
            sp.ExecuteScalar();
        }
        catch (SqlException e)
        {
            _WriteErrorLog(e);
        }
    }
    #endregion

    public void _WriteErrorLog_string(String ex)
    {
        string errorLogFilename = "ErrorLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
        string path = HttpContext.Current.Server.MapPath("~/ErrorLogFiles/" + errorLogFilename);
        if (File.Exists(path))
        {
            using (StreamWriter stwriter = new StreamWriter(path, true))
            {
                stwriter.WriteLine("Error Log Start as on " + DateTime.Now.ToString("hh:mm tt"));
                stwriter.WriteLine("Message:" + ex.ToString());
                stwriter.WriteLine("End as on " + DateTime.Now.ToString("hh:mm tt"));
            }
        }
        else
        {
            StreamWriter stwriter = File.CreateText(path);
            stwriter.WriteLine("Error Log Start as on " + DateTime.Now.ToString("hh:mm tt"));
            stwriter.WriteLine("Message: " + ex.ToString());
            stwriter.WriteLine("End as on " + DateTime.Now.ToString("hh:mm tt"));
            stwriter.Close();
        }
    }

}

#endregion

