﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Newtonsoft.Json;
using Microsoft.Reporting.WebForms;


/// <summary>
/// Summary description for CategoryRoleMappingService
/// </summary>
public class CategoryRoleMappingService
{
    SubSonic.StoredProcedure sp;
    List<EquipmentCustomizedData> Cust;
    EquipmentCustomizedData Ecustm;
    DataSet ds;
    public object GetCustomizedObject(EquipmentRL Equipment)
    {
        try
        {
            DataTable dt = GetCustomizedDetails(Equipment);
            if (dt.Rows.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = dt };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public DataTable GetCustomizedDetails(EquipmentRL Equipment)
    {

        List<EquipmentCustomizedData> ECData = new List<EquipmentCustomizedData>();
        DataTable DT = new DataTable();
        try
        {
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@CAT_LIST", SqlDbType.Structured);
            if (Equipment.Categorylst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(Equipment.Categorylst);
            }

            param[1] = new SqlParameter("@BRND_LIST", SqlDbType.Structured);
            if (Equipment.Brandlst == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(Equipment.Brandlst);
            }

            param[2] = new SqlParameter("@MODEL_LIST", SqlDbType.Structured);
            if (Equipment.Modellst == null)
            {
                param[2].Value = null;
            }
            else
            {
                param[2].Value = UtilityService.ConvertToDataTable(Equipment.Modellst);
            }

            param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"];
            param[4] = new SqlParameter("@SUB_CAT_LIST", SqlDbType.Structured);
            if (Equipment.Subcatlst == null)
            {
                param[4].Value = null;
            }
            else
            {
                param[4].Value = UtilityService.ConvertToDataTable(Equipment.Subcatlst);
            }
            param[5] = new SqlParameter("@CNP_LST", SqlDbType.Structured);
            if (Equipment.CNP_NAME == null)
            {
                param[5].Value = null;
            }
            else
            {
                param[5].Value = UtilityService.ConvertToDataTable(Equipment.CNP_NAME);
            }
            param[6] = new SqlParameter("@ROL_LST", SqlDbType.Structured);
            if (Equipment.rolelsts == null)
            {
                param[6].Value = null;
            }
            else
            {
                param[6].Value = UtilityService.ConvertToDataTable(Equipment.rolelsts);
            }
            param[7] = new SqlParameter("@ROLBSE", SqlDbType.NVarChar);
            param[7].Value = Equipment.ROLBASE;
            param[8] = new SqlParameter("@COMBSE", SqlDbType.NVarChar);
            param[8].Value = Equipment.COMPBASE;
            param[9] = new SqlParameter("@STATS", SqlDbType.NVarChar);
            param[9].Value = Equipment.Status;
            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "INSERT_ROL_ASSET_CATEGORY_MAPPING", param);
            HttpContext.Current.Session["AssetCustomizedGrid"] = DT;
            return DT;


        }
        catch (Exception ex)
        {
            return DT;
        }
    }
    public object GetGridRoleData()
    {
        try
        {
            DataTable dt = GetGridRoleDetails();
            if (dt.Rows.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = dt };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public DataTable GetGridRoleDetails()
    {

        List<EquipmentCustomizedData> ECData = new List<EquipmentCustomizedData>();
        DataTable DT = new DataTable();
        try
        {
           
            DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "GET_ASSET_CATEGORY_MAPPING");
            HttpContext.Current.Session["AssetCustomizedGrid"] = DT;
            return DT;


        }
        catch (Exception ex)
        {
            return DT;
        }
    }
}