﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using UtiltiyVM;

/// <summary>
/// Summary description for CompanyService
/// </summary>
public class CompanyService
{
    SubSonic.StoredProcedure sp;

    public object GetModules()
    {
        List<ModuleVM> modlst = new List<ModuleVM>();
        ModuleVM Modl;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_MODULES_COMPANY");
        //sp.Command.AddParameter("@TENANT_ID", HttpContext.Current.Session["TENANT"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Modl = new ModuleVM();
                Modl.MOD_CODE = sdr["MOD_CODE"].ToString();
                Modl.MOD_NAME = sdr["MOD_NAME"].ToString();
                Modl.ticked = false;
                modlst.Add(Modl);
            }
        }
        if (modlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = modlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetCompanyList()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "ADM_GET_COMPANY_DETAILS").GetReader())
        {
            List<CompanyVM> EditLst = new List<CompanyVM>();
            List<CompanyVM> CNPList = new List<CompanyVM>();
            List<CompanyVM> GrpCNPList = new List<CompanyVM>();
            while (reader.Read())
            {
                CNPList.Add(new CompanyVM()
                {
                    CNP_ID = (int)reader["CNP_ID"],
                    CNP_NAME = reader["CNP_NAME"].ToString(),
                    CNP_REMARKS = reader["CNP_REMARKS"].ToString(),
                    CNP_ADDRESS = reader["CNP_ADDRESS"].ToString(),
                    CNP_PARENT_ID = (int)reader["CNP_PARENT_ID"],
                    CNP_MODULE = reader["CNPD_MODULE"].ToString(),
                });
            }

            if (reader.NextResult())
            {
                while (reader.Read())
                {
                    GrpCNPList.Add(new CompanyVM()
                    {
                        CNP_ID = (int)reader["CNP_ID"],
                        CNP_NAME = reader["CNP_NAME"].ToString(),
                        CNP_REMARKS = reader["CNP_REMARKS"].ToString(),
                        CNP_ADDRESS = reader["CNP_ADDRESS"].ToString(),
                        CNP_PARENT_ID = (int)reader["CNP_PARENT_ID"],
                        CNP_MODULE = reader["CNPD_MODULE"].ToString(),
                    });
                }
            }

            reader.Close();
            //return CNPList;
            return new { CNPDetails = CNPList, GRPCNPDetails = GrpCNPList };
        }
    }

    public object Save(CompanyVM CNPdetails)
    {
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@CNP_NAME", SqlDbType.NVarChar);
        param[0].Value = CNPdetails.CNP_NAME;
        param[1] = new SqlParameter("@CNP_ADDRESS", SqlDbType.NVarChar);
        param[1].Value = string.IsNullOrEmpty(CNPdetails.CNP_ADDRESS) ? "" : CNPdetails.CNP_ADDRESS.ToString();
        param[2] = new SqlParameter("@CNP_REMARKS", SqlDbType.NVarChar);
        param[2].Value = string.IsNullOrEmpty(CNPdetails.CNP_REMARKS) ? "" : CNPdetails.CNP_REMARKS.ToString();
        param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["UID"];
        param[4] = new SqlParameter("@CNP_PARENT_ID", SqlDbType.NVarChar);
        param[4].Value = 0;
        param[5] = new SqlParameter("@TENANT_ID", SqlDbType.NVarChar);
        param[5].Value = HttpContext.Current.Session["TENANT"];

        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "ADM_SAVE_COMPANY_DETAILS", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    return new { Message = MessagesVM.CNP_INSERTED, data = (int)dr["CNP_ID"] };
                }
                else
                    return new { Message = MessagesVM.CNP_EXIST, data = (object)null };
            }
            else
                return new { Message = MessagesVM.CNP_ERROR, data = (object)null };
        }
    }

    public object GRPSave(CompanyVM GRPCNPdetails)
    {
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@CNP_NAME", SqlDbType.NVarChar);
        param[0].Value = GRPCNPdetails.CNP_NAME;
        param[1] = new SqlParameter("@CNP_ADDRESS", SqlDbType.NVarChar);
        param[1].Value = string.IsNullOrEmpty(GRPCNPdetails.CNP_ADDRESS) ? "" : GRPCNPdetails.CNP_ADDRESS.ToString();
        param[2] = new SqlParameter("@CNP_REMARKS", SqlDbType.NVarChar);
        param[2].Value = string.IsNullOrEmpty(GRPCNPdetails.CNP_REMARKS) ? "" : GRPCNPdetails.CNP_REMARKS.ToString();
        param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["UID"];
        param[4] = new SqlParameter("@CNP_PARENT_ID", SqlDbType.NVarChar);
        param[4].Value = GRPCNPdetails.CNP_PARENT_ID;
        param[5] = new SqlParameter("@MODL_LIST", SqlDbType.Structured);
        param[5].Value = UtilityService.ConvertToDataTable(GRPCNPdetails.MODlst);

        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "ADM_SAVE_GRP_COMPANY_DETAILS", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    return new { Message = MessagesVM.GRPCNP_INSERTED, data = new { CNP_ID = (int)dr["CNP_ID"], CNP_MODULE = dr["CNP_MODULE"].ToString() } };
                }
                else
                    return new { Message = MessagesVM.CNP_EXIST, data = (object)null };
            }
            else
                return new { Message = MessagesVM.CNP_ERROR, data = (object)null };
        }
    }

    public object Modify(CompanyVM CNPdetails)
    {
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@CNP_NAME", SqlDbType.NVarChar);
        param[0].Value = CNPdetails.CNP_NAME;
        param[1] = new SqlParameter("@CNP_ADDRESS", SqlDbType.NVarChar);
        param[1].Value = string.IsNullOrEmpty(CNPdetails.CNP_ADDRESS) ? "" : CNPdetails.CNP_ADDRESS.ToString();
        param[2] = new SqlParameter("@CNP_REMARKS", SqlDbType.NVarChar);
        param[2].Value = string.IsNullOrEmpty(CNPdetails.CNP_REMARKS) ? "" : CNPdetails.CNP_REMARKS.ToString();
        param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["UID"];
        param[4] = new SqlParameter("@CNP_PARENT_ID", SqlDbType.NVarChar);
        param[4].Value = 0;
        param[5] = new SqlParameter("@CNP_ID", SqlDbType.Int);
        param[5].Value = CNPdetails.CNP_ID;

        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "ADM_UPDATE_COMPANY_DETAILS", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    return new { Message = MessagesVM.CNP_MODIFY, data = (int)dr["CNP_ID"] };
                }
                else
                    return new { Message = MessagesVM.CNP_EXIST, data = (object)null };
            }
            else
                return new { Message = MessagesVM.CNP_ERROR, data = (object)null };
        }
    }

    public object GRPModify(CompanyVM GRPCNPdetails)
    {
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@CNP_NAME", SqlDbType.NVarChar);
        param[0].Value = GRPCNPdetails.CNP_NAME;
        param[1] = new SqlParameter("@CNP_ADDRESS", SqlDbType.NVarChar);
        param[1].Value = string.IsNullOrEmpty(GRPCNPdetails.CNP_ADDRESS) ? "" : GRPCNPdetails.CNP_ADDRESS.ToString();
        param[2] = new SqlParameter("@CNP_REMARKS", SqlDbType.NVarChar);
        param[2].Value = string.IsNullOrEmpty(GRPCNPdetails.CNP_REMARKS) ? "" : GRPCNPdetails.CNP_REMARKS.ToString();
        param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["UID"];
        param[4] = new SqlParameter("@CNP_PARENT_ID", SqlDbType.NVarChar);
        param[4].Value = GRPCNPdetails.CNP_PARENT_ID;
        param[5] = new SqlParameter("@CNP_ID", SqlDbType.Int);
        param[5].Value = GRPCNPdetails.CNP_ID;
        param[6] = new SqlParameter("@MODL_LIST", SqlDbType.Structured);
        param[6].Value = UtilityService.ConvertToDataTable(GRPCNPdetails.MODlst);

        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "ADM_UPDATE_GRP_COMPANY_DETAILS", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    return new { Message = MessagesVM.GRPCNP_MODIFY, data = new { CNP_ID = (int)dr["CNP_ID"], CNP_MODULE = dr["CNP_MODULE"].ToString() } };
                }
                else
                    return new { Message = MessagesVM.GRPCNP_EXIST, data = (object)null };
            }
            else
                return new { Message = MessagesVM.CNP_ERROR, data = (object)null };
        }
    }

    public object GRPEdit(CompanyVM GRPCNPdetails)
    {
        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@CNP_ID", SqlDbType.Int);
        param[0].Value = GRPCNPdetails.CNP_ID;

        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "ADM_GET_GRP_COMPANY_DETAILS_BY_ID", param))
        {
            List<ModuleVM> modlst = new List<ModuleVM>();
            ModuleVM Modl;
            while (reader.Read())
            {
                Modl = new ModuleVM();
                Modl.MOD_CODE = reader["CNPD_MODULE"].ToString();
                Modl.MOD_NAME = reader["CNPD_MODULE"].ToString();
                Modl.ticked = true;
                modlst.Add(Modl);
            }

            if (modlst.Count != 0)
                return new { Message = "", data = modlst };
            else
                return new { Message = "", data = (object)null };
        }
    }
}