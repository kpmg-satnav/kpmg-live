﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for PrivilegesModelService
/// </summary>
public class PrivilegesService
{
    SubSonic.StoredProcedure sp;
    public object Get(string id)
    {
        try
        {
            SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PRIVILEGES_GetPrivilegesByRoleId");
            sp3.Command.AddParameter("@ParentId", 0, DbType.Int32);
            sp3.Command.AddParameter("@RolId", id, DbType.Int32);
            IDataReader reader = sp3.GetReader();
            List<PrivilegesModel> prvglist = new List<PrivilegesModel>();
            while (reader.Read())
            {
                prvglist.Add(new PrivilegesModel()
                {
                    CLS_ID = Convert.ToInt32(reader["CLS_ID"]),
                    CLS_DESCRIPTION = reader["CLS_DESCRIPTION"].ToString(),
                    CLS_PARENT_ID = Convert.ToInt32(reader["CLS_PARENT_ID"]),
                    Cls_Parent_Name = reader["cls_parent_name"].ToString(),
                    CLSLEVEL = Convert.ToInt32(reader["CLSLEVEL"]),
                    selected = false,
                    isAssigned = Convert.ToInt32(reader["isAssigned"]),

                });
            }
            foreach (var item in prvglist.Where(selected => selected.isAssigned != 0))
            {
                item.selected = true;
            }
            reader.Close();
            Dictionary<string, object> objdict = new Dictionary<string, object>();
            foreach (PrivilegesModel cls0 in prvglist.Where(lvl0 => lvl0.CLSLEVEL == 0))
            {
                Dictionary<string, object> objlvl1 = new Dictionary<string, object>();
                foreach (PrivilegesModel cls1 in prvglist.Where(lvl1 => lvl1.CLS_PARENT_ID == cls0.CLS_ID))
                {
                    Dictionary<string, object> objlvl2 = new Dictionary<string, object>();
                    foreach (PrivilegesModel cls2 in prvglist.Where(lvl2 => lvl2.CLS_PARENT_ID == cls1.CLS_ID))
                    {
                        Dictionary<string, object> objlvl3 = new Dictionary<string, object>();
                        foreach (PrivilegesModel cls3 in prvglist.Where(lvl3 => lvl3.CLS_PARENT_ID == cls2.CLS_ID))
                        {
                            objlvl3.Add(cls3.CLS_DESCRIPTION, cls3);
                        }
                        objlvl2.Add(cls2.CLS_DESCRIPTION, new { link = cls2.CLS_DESCRIPTION, cls2.CLS_ID, cls2.selected, data = objlvl3.ToArray() });
                    }
                    objlvl1.Add(cls1.CLS_DESCRIPTION, new { link = cls1.CLS_DESCRIPTION, cls1.CLS_ID, cls1.selected, data = objlvl2.ToArray() });
                }
                objdict.Add(cls0.CLS_DESCRIPTION, new { link = cls0.CLS_DESCRIPTION, cls0.CLS_ID, cls0.selected, data = objlvl1.ToArray() });
            }
            return new { Message = MessagesVM.AF_OK, data = objdict.ToArray() };
        }
        catch(Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }

    }
    public object SavePrivileges(RoleVM dataobject)
    {
     
        List<Main> mainlist = dataobject.main.ToList();

        List<RoleValues> rolelist = new List<RoleValues>();
        RoleValues rv;
        try
        {
            deleteExistingPrivileges(dataobject.ROL_ID);
            foreach (var lis in mainlist)
            {

                if (lis.Value.selected == true)
                {
                    rv = new RoleValues();
                    rv.PRV_ROL_ID = dataobject.ROL_ID;
                    rv.PRV_CLS_ID = lis.Value.CLS_ID;
                    rolelist.Add(rv);

                    List<Main> mainlist1 = lis.Value.data.ToList();
                    foreach (var lis1 in mainlist1)
                    {
                        if (lis1.Value.selected == true)
                        {
                            rv = new RoleValues();
                            rv.PRV_ROL_ID = dataobject.ROL_ID;
                            rv.PRV_CLS_ID = lis1.Value.CLS_ID;
                            rolelist.Add(rv);

                            List<Main> mainlist2 = lis1.Value.data.ToList();
                            foreach (var lis2 in mainlist2)
                            {
                                if (lis2.Value.selected == true)
                                {
                                    rv = new RoleValues();
                                    rv.PRV_ROL_ID = dataobject.ROL_ID;
                                    rv.PRV_CLS_ID = lis2.Value.CLS_ID;
                                    rolelist.Add(rv);
                                }
                            }
                        }

                    }
                }


            }
            insertRoles(rolelist);
        }
        catch(Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
        return new { Message = MessagesVM.AF_SUCCESS, data = rolelist };
    }
    public void insertRoles(List<RoleValues> rolelist)
    {
        try
        {
            foreach (var rol in rolelist)
            {
                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "ADD_PRVGLS");
                sp.Command.AddParameter("@PrvRolId", rol.PRV_ROL_ID, DbType.Int32);
                sp.Command.AddParameter("@PrvClsId", rol.PRV_CLS_ID, DbType.Int32);
                sp.Command.AddParameter("@User_id", HttpContext.Current.Session["UID"], DbType.String);
                sp.Execute();

            }
        }
        catch (Exception)
        {
            throw;
        }

    }
    public void deleteExistingPrivileges(int roleID)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AMT_DelPrivileges_SP");
            sp.Command.AddParameter("@PrvRolId", roleID, DbType.Int32);
            sp.Execute();
        }
        catch(Exception)
        {
            throw;
        }
    }


    public object getPrivilegesByRoleID(string id)
    {
        try
        {
            SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GET_PRV_DATA");
            sp3.Command.AddParameter("@ParentId", 0, DbType.Int32);
            sp3.Command.AddParameter("@RolId", id, DbType.Int32);
            IDataReader reader1 = sp3.GetReader();

            List<PrivilegesModel> permissions = new List<PrivilegesModel>();
            List<grantedPRVM> grantedPermissions = new List<grantedPRVM>();
            while (reader1.Read())
            {
                permissions.Add(new PrivilegesModel()
                {
                    CLS_ID = Convert.ToInt32(reader1["CLS_ID"]),
                    name = reader1["name"].ToString(),
                    CLS_PARENT_ID = Convert.ToInt32(reader1["CLS_PARENT_ID"]),
                    parentName = reader1["parentName"].ToString(),
                    CLSLEVEL = Convert.ToInt32(reader1["CLS_LEVEL"]),
                    selected = false,
                    isGrantedByDefault = Convert.ToInt32(reader1["isGrantedByDefault"]),

                });
            }
            foreach (var item in permissions.Where(selected => selected.isGrantedByDefault != 0))
            {
                grantedPRVM pm = new grantedPRVM();
                pm.name = item.name;
                pm.id = item.CLS_ID;
                grantedPermissions.Add(pm);
            }
            return new { Status = "Success", permissions = permissions.ToArray(), grantedPermissionNames = grantedPermissions };
        }
        catch (Exception ex)
        {
            return new { Status = "Fail", Message = ex.Message, data = (object)null };
        }
    }

    public object SaveGrantedPermissions(saveGrantedPermissions paramsList)
    {
        try
        {
            List<int> list = new List<int>(paramsList.permissions);
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@PRV_LIST", SqlDbType.Structured);
            param[0].Value = ConvertListToDataTable(list);
            param[1] = new SqlParameter("@ROLE_ID", SqlDbType.Int);
            param[1].Value = paramsList.RoleID;
            SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "Insert_PRV_Data", param);
            return new { Status = "Success", data = paramsList.permissions };
        }
        catch (Exception ex)
        {
            return new { Status = "Fail", Message = ex.Message, data = (object)null };
        }
    }

    static DataTable ConvertListToDataTable(List<int> list)
    {
        DataTable table = new DataTable();
        table.Columns.Add("CLS_ID", typeof(Int32));
        foreach (var privName in list)
        {
            table.Rows.Add(privName);
        }

        return table;
    }

}