﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtiltiyVM;
using System.Web;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using System.Data;
using System.Globalization;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;

public class UserRoleMappingController : ApiController
{
    UserRoleMappingService UserRoleService = new UserRoleMappingService();
    [HttpPost]
    public async Task<HttpResponseMessage> GetEmployeeData([FromBody]UserRoleMappingVM EmpData)
    {
        ReportGenerator<UserRoleMapping> GenEmployeeData = new ReportGenerator<UserRoleMapping>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/EmployeeData.rdlc"),
            DataSetName = "EmployeeData",
            ReportType = "Employee Data"
        };
        UserRoleService = new UserRoleMappingService();
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/EmployeeData." + EmpData.Type);
        List<UserRoleMapping> reportdata = UserRoleService.EmployeeData();
        await GenEmployeeData.GenerateReport(reportdata, filePath, EmpData.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "EmployeeData." + EmpData.Type;
        return result;
    }

    //Grid View
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetUserRoleDetails()
    {
        var URMlist = UserRoleService.GetUserRoleDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }

    //Grid View Array
    [HttpGet]
    //public HttpResponseMessage GetUserRoleDetailsArray([FromUri] int StartCount, [FromUri] int EndCount)
    public HttpResponseMessage GetUserRoleDetailsArray()
    {
        var URMlist = UserRoleService.GetUserRoleDetailsArray();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetUserOnSelection(String Id)
    {
        var URMlist = UserRoleService.GetUserOnSelection(Id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }

    //for  Module Dropdown
    [HttpGet]
    public HttpResponseMessage Getmodules()
    {
        var modulelist = UserRoleService.Getmodules();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, modulelist);
        return response;
    }

    //Inactive Spaces
    [HttpPost]
    public HttpResponseMessage InactiveSpaces(InactiveSpace user)
    {
        var modulelist = UserRoleService.InactiveSpaces(user);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, modulelist);
        return response;
    }

    //For Access CheckBox By Module change
    [HttpPost]
    public HttpResponseMessage GetAccessByModule(UserCompanyModules ucm)
  // public HttpResponseMessage GetAccessByModule(string id)
    {
        var Rolelist = UserRoleService.GetAccessByModule(ucm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Rolelist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveDetails(UserRoleMappingVM dataobject)
    {
        var user = UserRoleService.SaveDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage DeleteSeat(UserCompanyModules dataobject)
    {
        var user = UserRoleService.DeleteEmployee(dataobject.AurId);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage UploadTemplate()
    {
        var httpRequest = HttpContext.Current.Request;
        HttpPostedFile file = HttpContext.Current.Request.Files[0];
        string Extension;
        string[] ExtensionList = file.FileName.Split('.');
        Extension = ExtensionList.Last();
        dynamic retobj = "";
        if (Extension.Equals("xlsx") || Extension.Equals("xls"))
        {
            retobj = UserRoleService.UploadTemplate(httpRequest);
        }
        else
        {
            var response1 = Request.CreateResponse(HttpStatusCode.OK);
            response1.Content = new StringContent("Invalid File Format", Encoding.UTF8, "text/plain");
            response1.Content.Headers.ContentType = new MediaTypeWithQualityHeaderValue(@"text/html");
            return response1;
        }
        string retContent = "";
        if (retobj.data != null)
            retContent = JsonConvert.SerializeObject(retobj);
        else
            retContent = JsonConvert.SerializeObject(retobj);

        var response = Request.CreateResponse(HttpStatusCode.OK);
        response.Content = new StringContent(retContent, Encoding.UTF8, "text/plain");
        response.Content.Headers.ContentType = new MediaTypeWithQualityHeaderValue(@"text/html");
        return response;
    }

    //Get User Emp Details
    [HttpGet]
    public HttpResponseMessage GetUserEmpDetails(String Id)
    {
        var URMlist = UserRoleService.GetUserEmpDetails(Id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }

    // Get User Modules and Roles
    [HttpGet]
    public HttpResponseMessage GetUserModRolesDetails(String Id)
    {
        var URMlist = UserRoleService.GetUserModRolesDetails(Id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }

    //save Emp Details
    [HttpPost]
    public HttpResponseMessage SaveEmpDetails(UserRoleMapping dataobject)
    {
        var user = UserRoleService.SaveEmpDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    //Save Role/Module details
    [HttpPost]
    public HttpResponseMessage SaveRoleModDetls(UserRoleMappingVM dataobject)
    {
        var user = UserRoleService.SaveRoleModDetls(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    //Save Mapping details
    [HttpPost]
    public HttpResponseMessage SaveMappingDetails(UserRoleMappingVM dataobject)
    {
        var URMlist = UserRoleService.SaveMappingDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }

    //Step2 -> get role/module details
    [HttpGet]
    public HttpResponseMessage GetRMDetails_Step2(String Id)
    {
        var URMlist = UserRoleService.GetRMDetails_Step2(Id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }

    //Step2 -> get role/module details
    [HttpGet]
    public HttpResponseMessage GetMappingDetails_Step3(String Id)
    {
        var URMlist = UserRoleService.GetMappingDetails_Step3(Id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, URMlist);
        return response;
    }
}
