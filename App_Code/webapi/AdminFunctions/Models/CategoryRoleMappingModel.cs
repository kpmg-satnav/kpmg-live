﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;



public class EquipmentRL
{
    public List<Categorylst> Categorylst { get; set; }
    public List<SubCategorylst> Subcatlst { get; set; }

    public List<Modellst> Modellst { get; set; }
    public List<Brandlst> Brandlst { get; set; }
    public List<GetCompanies> CNP_NAME { get; set; }
    public List<Rolelst> rolelsts { get; set; }
    public string ROLBASE { get; set; }
    public string COMPBASE { get; set; }
    public string Status { get; set; }
}


