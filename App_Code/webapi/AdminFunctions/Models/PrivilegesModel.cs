﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PrivilegesModel
/// </summary>
/// 



public class RoleVM
{
    public int ROL_ID { get; set; }
    public string PRV_CREATED_BY { get; set; }
    public List<Main> main { get; set; }
}
public class Main
{
    public object Key { get; set; }
    public Sub Value { get; set; }

}

public class Sub
{
    public int CLS_ID { get; set; }
    public List<Main> data { get; set; }
    public Boolean selected { get; set; }
}
public class PrivilegesModel
{

    public int CLS_ID { get; set; }
    public string CLS_DESCRIPTION { get; set; }
    public int CLS_PARENT_ID { get; set; }
    public string Cls_Parent_Name { get; set; }
    public int CLSLEVEL { get; set; }
    public int isAssigned { get; set; }
    public Boolean selected { get; set; }

    //

    public string name { get; set; }
    public string parentName { get; set; }
    public int isGrantedByDefault { get; set; } 
  

}

public class grantedPRVM
{
    public string name { get; set; }
    public int id { get; set; }
}
public class RoleValues
{
    public int PRV_ROL_ID { get; set; }
    public int PRV_CLS_ID { get; set; }
    public string PRV_CREATED_BY { get; set; }
    public string PRV_CREATED_DT { get; set; }

}

public class grantedPermissions
{
    public int id { get; set; }
}

public class saveGrantedPermissions
{
    public int[] permissions { get; set; }
    public int RoleID {get;set;}
}

