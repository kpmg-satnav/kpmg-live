﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.Web.Http;

/// <summary>
/// Summary description for XpandService
/// </summary>
public class XpandService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    string DatabaseURL;

    public object _EmployeeList(EMP_ID_LIST detls)
    {

        try
        {
            List<EMP_ID_LIST> emplst = new List<EMP_ID_LIST>();
            EMP_ID_LIST emp;
            emp = new EMP_ID_LIST();
            emp.EMP_ID = detls.EMP_ID;
            emp.FIRST_NAME = detls.FIRST_NAME;
            emp.MIDDLE_NAME = detls.MIDDLE_NAME;
            emp.LAST_NAME = detls.LAST_NAME;
            emp.Email_ID = detls.Email_ID;
            emp.RM_ID = detls.RM_ID;
            emp.ROLL_TYPE = detls.ROLL_TYPE;
            emp.LCM_NAME = detls.LCM_NAME;
            emp.DSN = detls.DSN;
            emp.DEPT = detls.DEPT;
            emp.DOB = detls.DOB;
            emp.DOJ = detls.DOJ;
            emp.CNY = detls.CNY;
            emp.CTY = detls.CTY;
            emp.GRADE = detls.GRADE;
            emp.PARENT_ENTITY = detls.PARENT_ENTITY;
            emp.CHILD_ENTITY = detls.CHILD_ENTITY;
            emp.BU = detls.BU;
            emp.LOB = detls.LOB;
            emp.EOD = detls.EOD;
            emplst.Add(emp);
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@Emp_list", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(emplst);
            DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "[TCL].[dbo].XPAND_INTEGRATION_STEP1", param);
            return new { Message = MessagesVM.UAD_UPLOK, data = dt };

        }
        catch (Exception ex)
        {

            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

}

