﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class FacilityController:ApiController
{
    FacilityService service = new FacilityService();
    [HttpPost]
    public HttpResponseMessage Create(FacilityModel facility)
    {
        if (service.Save(facility) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, facility);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already Exists");
    }
    [HttpPost]
    public HttpResponseMessage UpdateFacilityData(FacilityModel update)
    {
        if (service.Update(update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }

    [HttpGet]
    public HttpResponseMessage GetFacilityGrid()
    {
        IEnumerable<FacilityModel> faclist = service.GetFacilityBindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, faclist);
        return response;
    }  
}