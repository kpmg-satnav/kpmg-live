﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class UrgencyController : ApiController
{
    UrgencyService service = new UrgencyService();

    [HttpPost]
    public HttpResponseMessage Create(UrgencyModel urgency)
    {
        if (service.Save(urgency)==0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, urgency);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code Already Exists");
        }
    }

    [HttpPost]
    public HttpResponseMessage UpdateUrgencyMaster(UrgencyModel updt)
    {

        if (service.Update(updt))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, updt);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest);

    }

    [HttpGet]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<UrgencyModel> Urgencylist = service.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Urgencylist);
        return response;
    }
}
