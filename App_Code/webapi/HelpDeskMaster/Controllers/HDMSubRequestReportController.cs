﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


/// <summary>
/// Summary description for HDMSubRequestReportController
/// </summary>
public class HDMSubRequestReportController : ApiController
{
    HDMSubRequestReportService viewSubReqReport = new HDMSubRequestReportService();
    [HttpGet]
    public HttpResponseMessage GetReportGrid()
    {
        var obj = viewSubReqReport.GetReportGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetGriddataBySearch(BindGridDetailsBySearch bindGridDetailsBySearch)
    {
        var obj = viewSubReqReport.GetGriddataBySearch(bindGridDetailsBySearch);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid([FromBody]BindReportDetails data)
    {
        try
        {
            ReportGenerator<BindReportDetails> reportgen = new ReportGenerator<BindReportDetails>()
            {
                ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/SubRequestReport.rdlc"),
                DataSetName = "SubRequestReport",
                ReportType = "Sub Request Report"
            };

            viewSubReqReport = new HDMSubRequestReportService();
            List<BindReportDetails> reportdata = viewSubReqReport.GetReportGridrdlc();
            string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SubRequestReport." + data.Type);
            await reportgen.GenerateReport(reportdata, filePath, data.Type);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = "SubRequestReport." + data.Type;
            return result;
        }
        catch (Exception)
        {
            throw;
        }
    }

}