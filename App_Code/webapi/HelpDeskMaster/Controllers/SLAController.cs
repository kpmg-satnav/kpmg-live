﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;

public class SLAController : ApiController
{
    SLAService SLAService = new SLAService();

    //country
    [HttpGet]
    public HttpResponseMessage GetCountries()
    {
        IEnumerable<Country> countrylist = SLAService.GetActiveCountries();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, countrylist);
        return response;
    }
    //city by country
    [HttpGet]
    public HttpResponseMessage GetCityByCountry(String id)
    {
        IEnumerable<City> citylist = SLAService.GetActiveCityByCountry(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, citylist);
        return response;
    }
    //location by city
    [HttpGet]
    public HttpResponseMessage GetLocationByCity(String id)
    {
        IEnumerable<Location> locationlist = SLAService.GetActiveLocationByCity(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, locationlist);
        return response;
    }

    //main cat dropdowm
    [HttpGet]
    public HttpResponseMessage GetMaincategory()
    {
        IEnumerable<MainCategoryModel> maincategorylist = SLAService.GetActiveMainCategories();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, maincategorylist);
        return response;
    }
    //sub dropdown by main
    [HttpGet]
    public HttpResponseMessage GetSubCategoryByMain(String id)
    {
        IEnumerable<SubCategoryModel> subcategorylist = SLAService.GetActiveSubCategoriesByMain(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, subcategorylist);
        return response;
    }
    //Child dropdown by sub
    [HttpGet]
    public HttpResponseMessage GetChildCategoryBySubCat(String id)
    {
        IEnumerable<ChildCategoryModel> childcategorylist = SLAService.GetActiveChildCategoriesBySub(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, childcategorylist);
        return response;
    }


    //For Status/Role view
    [HttpGet]
    public object GetSLATime(int id)
    {
        return SLAService.GetSLATime(id);
    }
    //Grid View
    [HttpGet]
    public HttpResponseMessage GetSLAGrid()
    {
        IEnumerable<SLAModel> SLAlist = SLAService.GetSLAList();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SLAlist);
        return response;
    }

    //To save records

    [HttpPost]
    public HttpResponseMessage SaveDetails(SLAMasterVM dataobject)
    {
        var obj = SLAService.SaveDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage AddSla(SLAMasterVM dataobject)
    {
        var obj = SLAService.AddSla(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //edit
    [HttpPost]
    public object EditSLA(EditDetails Ed)
    {
        return SLAService.EditSLADetails(Ed);

    }

    //To Update records
    [HttpPost]
    public HttpResponseMessage UpdateDetails(SLAMasterVM dataobject)
    {
        var obj = SLAService.UpdateDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
