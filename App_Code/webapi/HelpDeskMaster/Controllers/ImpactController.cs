﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
public class ImpactController : ApiController
{
    ImpactService service = new ImpactService();

    [HttpPost]
    public HttpResponseMessage Create(ImpactModel impact)
    {

        if (service.Save(impact)==0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, impact);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code Already Exists");
        }
        
    }

    [HttpPost]
    public HttpResponseMessage UpdateImpactData(ImpactModel updt)
    {

        if (service.UpdateImpactData(updt))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, updt);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest);
    }

    [HttpGet]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<ImpactModel> impactlist = service.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, impactlist);
        return response;
    }
}