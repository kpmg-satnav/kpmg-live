﻿using System;
using QuickFMS.API.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SubCategoryController : ApiController
{
    SubCategoryService service = new SubCategoryService();

    [HttpPost]
    public HttpResponseMessage Create(HDMSubCategoryModel subcat)
    {

        if (service.Save(subcat) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, subcat);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code Already Exists");
        }

    }
    [HttpPost]
    public HttpResponseMessage UpdateSubcatData(HDMSubCategoryModel updt)
    {

        if (service.UpdateSubcatData(updt))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, updt);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest);
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetMainCategoryByModule(HDMSubCategoryModel com)
    {

        IEnumerable<MainCategoryModel> maincategorylist = service.GetMainCategoryByModule(com);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, maincategorylist);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetMaincategory()
    {
        IEnumerable<MainCategoryModel> maincategorylist = service.GetActiveMainCategories();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, maincategorylist);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<HDMSubCategoryModel> subcatlist = service.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, subcatlist);
        return response;
    }
}
