﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.IO;
using HDMRaiseSubRequestVM;


public class HDMRaiseSubRequestController : ApiController
{
    HDMRaiseSubRequestService SubReq = new HDMRaiseSubRequestService();
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetMainCat()
    {
        var obj = SubReq.GetMainCat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Getsubcat()
    {
        var obj = SubReq.Getsubcat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Getchildcat()
    {
        var obj = SubReq.Getchildcat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetAssertLocations()
    {
        var obj = SubReq.GetAssertLocations();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetSubCategories(GetSubCategories getSubCategories)
    {
        var obj = SubReq.GetSubCategories(getSubCategories);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetChildCategories(GetChildCategories getChildCategories)
    {
        var obj = SubReq.GetChildCategories(getChildCategories);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetApprovals()
    {
        var obj = SubReq.GetApprovals();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage getClientPrefixId([FromUri] string LcmCode)
    {
        var obj = SubReq.getClientPrefixId(LcmCode);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //var response = getSavedListItems(LcmCode);
        //return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveEmpDetails(SaveReqDetails dataobject)
    {


        var user = SubReq.SaveEmpDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [HttpPost]
        public string UploadFiles()
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/OLivaUploadFiles/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists(sPath + Path.GetFileName(hpf.FileName)))
                    {
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + Path.GetFileName(hpf.FileName));
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            // RETURN A MESSAGE.
            if (iUploadedCnt > 0) {
                return iUploadedCnt + " Files Uploaded Successfully";
            }
            else {
                return "Upload Failed";
            }
        }

}
