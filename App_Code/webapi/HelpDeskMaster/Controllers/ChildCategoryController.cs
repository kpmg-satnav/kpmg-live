﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ChildCategoryController : ApiController
{
    ChildCategoryService service = new ChildCategoryService();
    
    [HttpPost]
    public HttpResponseMessage Create(HDMChildCategoryModel subcat)
    {
        if (service.Save(subcat) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, subcat);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code Already Exists");
        }
    }

    [HttpPost]
    public HttpResponseMessage UpdateChildcatData(HDMChildCategoryModel updt)
    {

        if (service.UpdateSubcatData(updt))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, updt);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest);
    }
    //sub category
    [HttpGet]
    public HttpResponseMessage GetSubCategory()
    {
        IEnumerable<SubCategoryModel> subcategorylist = service.GetActiveSubCategories();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, subcategorylist);
        return response;
    }

    //
    [HttpPost]
    public HttpResponseMessage GetSubCategoryByModule(HDMChildCategoryModel Com)
    {
        IEnumerable<SubCategoryModel> subcategorylist = service.GetSubCategoryByModule(Com);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, subcategorylist);
        return response;
    }
    //main by sub
    [HttpGet]
    public HttpResponseMessage GetMaincategoryBySub(string id)
    {
        IEnumerable<MainCategoryModel> maincategorylist = service.GetActiveMainCategoriesBySub(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, maincategorylist);
        return response;
    }
    //main
    [HttpGet]
    public HttpResponseMessage GetMaincategory()
    {
        IEnumerable<MainCategoryModel> maincategorylist = service.GetActiveMainCategories();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, maincategorylist);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<HDMChildCategoryModel> subcatlist = service.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, subcatlist);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetCompanyModules()
    {
        var obj = service.GetCompanyModules();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
