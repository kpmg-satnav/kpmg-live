﻿using QuickFMS.API.Filters;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;

/// <summary>
/// Summary description for HDMViewModifyFeedbackController
/// </summary>
public class HDMViewModifyFeedbackController:ApiController
{
    HDMViewModifyFeedbackService ViewModifyreq = new HDMViewModifyFeedbackService();
    [HttpPost]
    public HttpResponseMessage GetDetailsByRequestId(GetFDBCKDetails getDetailsByRequestId)
    {
        var obj = ViewModifyreq.GetDetailsByRequestId(getDetailsByRequestId);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage ModifyFDBckDetails(saveFDBCKDetails dataobject)
    {
        var user = ViewModifyreq.ModifyFDBckDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }
}