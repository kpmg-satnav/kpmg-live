﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class PopulationClassificationController : ApiController
{
    PopulationClassificationService service = new PopulationClassificationService();

    [HttpPost]
    public HttpResponseMessage Create(HDMPopulationClassificationModel popcls)
    {
        if (service.Save(popcls) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, popcls);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already Exists");
    }


    [HttpPost]
    public HttpResponseMessage UpdatePopulationClassificationData(HDMPopulationClassificationModel update)
    {

        if (service.Update(update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }

    [HttpGet]
    public HttpResponseMessage GetPopulationClassificationBindGrid()
    {
        IEnumerable<HDMPopulationClassificationModel> popclslist = service.MainCategoryBindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, popclslist);
        return response;
    }
}
