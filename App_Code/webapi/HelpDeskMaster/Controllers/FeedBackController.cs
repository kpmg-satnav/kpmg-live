﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class FeedBackController : ApiController
{
    FeedBackService service = new FeedBackService();

    [HttpPost]
    public HttpResponseMessage Create(FeedBackModel assetloc)
    {
        if (service.Save(assetloc) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, assetloc);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already Exists");
    }


    [HttpPost]
    public HttpResponseMessage UpdateFeedBackData(FeedBackModel update)
    {

        if (service.Update(update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }

    [HttpGet]
    public HttpResponseMessage GetFeedBackBindGrid()
    {
        IEnumerable<FeedBackModel> assetLocationlist = service.FeedbackBindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, assetLocationlist);
        return response;
    }
}
