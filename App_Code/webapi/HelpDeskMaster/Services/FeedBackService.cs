﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

public class FeedBackService
{
    SubSonic.StoredProcedure sp;

    public int Save(FeedBackModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_FEEDBACK_DETAILS");
            sp.Command.AddParameter("@FBD_CODE", model.FBD_CODE, DbType.String);
            sp.Command.AddParameter("@FBD_NAME", model.FBD_NAME, DbType.String);
            sp.Command.AddParameter("@FBD_STA_ID", model.FBD_STA_ID, DbType.String);
            sp.Command.AddParameter("@FBD_REM", model.FBD_REM, DbType.String);
            sp.Command.Parameters.Add("@FBD_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch
        {
            throw;
        }
    }


    public Boolean Update(FeedBackModel upd)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_UPDATE_FEEDBACK_DETAILS");
            sp.Command.AddParameter("@FBD_CODE", upd.FBD_CODE, DbType.String);
            sp.Command.AddParameter("@FBD_NAME", upd.FBD_NAME, DbType.String);
            sp.Command.AddParameter("@FBD_STA_ID", upd.FBD_STA_ID, DbType.Int32);
            sp.Command.AddParameter("@FBD_REM", upd.FBD_REM, DbType.String);
            sp.Command.Parameters.Add("@FBD_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            sp.Execute();
            return true;
        }
        catch
        {
            throw;
        }
    }



    public IEnumerable<FeedBackModel> FeedbackBindGrid()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_FEEDBACK_DETAILS_BINDGRID").GetReader())
            {
                List<FeedBackModel> fbdlist = new List<FeedBackModel>();
                while (reader.Read())
                {
                    fbdlist.Add(new FeedBackModel()
                    {
                        FBD_CODE = reader.GetValue(0).ToString(),
                        FBD_NAME = reader.GetValue(1).ToString(),
                        FBD_STA_ID = reader.GetValue(2).ToString(),
                        FBD_REM = reader.GetValue(3).ToString()
                    });
                }
                reader.Close();
                return fbdlist;
            }
        }
        catch
        {
            throw;
        }
    }
}

