﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for HDMconsolidatedReportService
/// </summary>
public class HDMReqStatusconsolidatedReportService
{
    SubSonic.StoredProcedure sp;
    public object BindGrid(HDMReqStatusconsolidatedReportVM Params)
    {

        dynamic hdmconslst;
        if (Params.FLAG == 1)
            hdmconslst = GetReportList(Params);
        else        
            hdmconslst = GetReportList_zone(Params);     

        if (hdmconslst != null)
            return new { Message = MessagesVM.UM_OK, data = hdmconslst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //public object GetReportList()
    public List<HDMReqStatusconsolidatedReportVM> GetReportList(HDMReqStatusconsolidatedReportVM Params)
    {
        List<HDMReqStatusconsolidatedReportVM> hdmconslst = new List<HDMReqStatusconsolidatedReportVM>();
        HDMReqStatusconsolidatedReportVM hdmcons;

        
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HELPDESK_CONSOLIDATED_RPT");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANY", Params.CNP_NAME, DbType.Int32);
        sp.Command.AddParameter("@HIS_STATUS", Params.HSTSTATUS, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                hdmcons = new HDMReqStatusconsolidatedReportVM();
                hdmcons.CNY_NAME = sdr["CNY_NAME"].ToString();
                hdmcons.CTY_NAME = sdr["CTY_NAME"].ToString();
                hdmcons.LCM_NAME = sdr["LCM_NAME"].ToString();
                hdmcons.LCM_CODE = sdr["LCM_CODE"].ToString();
                hdmcons.CNP_NAME = sdr["CNP_NAME"].ToString();
                hdmcons.Total_Requests = Convert.ToInt32(sdr["Total_Requests"]);
                hdmcons.Pending_Requests = Convert.ToInt32(sdr["Pending_Requests"]);
                hdmcons.In_Progress_Requests = Convert.ToInt32(sdr["In_Progress_Requests"]);
                hdmcons.Closed_Requests = Convert.ToInt32(sdr["Closed_Requests"]);
                hdmcons.Canceled_Requests = Convert.ToInt32(sdr["Canceled_Requests"]);
                hdmconslst.Add(hdmcons);
            }
            sdr.Close();
        }       
            return hdmconslst;
                
    }


    public List<HDMReqStatusconsolidatedReportVM> GetReportList_zone(HDMReqStatusconsolidatedReportVM Params)
    {
        List<HDMReqStatusconsolidatedReportVM> hdmconslst = new List<HDMReqStatusconsolidatedReportVM>();
        HDMReqStatusconsolidatedReportVM hdmcons;


       
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HELPDESK_ZONE_WISE_CONSOLIDATED_RPT");
            sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANY", Params.CNP_NAME, DbType.Int32);
            sp.Command.AddParameter("@HIS_STATUS", Params.HSTSTATUS, DbType.Int32);
            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    hdmcons = new HDMReqStatusconsolidatedReportVM();
                    hdmcons.LCM_ZONE = sdr["LCM_ZONE"].ToString();
                    hdmcons.Total_Requests = Convert.ToInt32(sdr["Total_Requests"]);
                    hdmcons.Pending_Requests = Convert.ToInt32(sdr["Pending_Requests"]);
                    hdmcons.In_Progress_Requests = Convert.ToInt32(sdr["In_Progress_Requests"]);
                    hdmcons.Closed_Requests = Convert.ToInt32(sdr["Closed_Requests"]);
                    hdmcons.Canceled_Requests = Convert.ToInt32(sdr["Canceled_Requests"]);
                    hdmconslst.Add(hdmcons);
                }
                sdr.Close();

            }
            return hdmconslst;
       
        
    }

    public object HDMconsolidatedChart(int CompanyID)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HELPDESK_CONSOLIDATED_CHART_RPT");
        sp.Command.AddParameter("@USER", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANY", CompanyID, DbType.Int32);
        DataSet ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();

        if (arr.Length != 0)
            return new { Message = MessagesVM.UM_OK, data = arr };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
}