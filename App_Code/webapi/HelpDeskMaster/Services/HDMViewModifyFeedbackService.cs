﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System;
using System.Collections.ObjectModel;
using UtiltiyVM;
using System.IO;
using HDMUtilityVM;

/// <summary>
/// Summary description for HDMViewModifyFeedbackService
/// </summary>
public class HDMViewModifyFeedbackService
{
    public object GetDetailsByRequestId(GetFDBCKDetails user)
    {
        try
        {


            string jsonstring;
            SqlParameter[] udParams = new SqlParameter[2];
            udParams[0] = new SqlParameter("@REQID", SqlDbType.VarChar);
            udParams[0].Value = user.RequestId;
            udParams[1] = new SqlParameter("@AURID", SqlDbType.VarChar);
            udParams[1].Value = HttpContext.Current.Session["UID"];
            //DataSet dset = new DataSet();
            //dset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_VIEW_SUB_REQUISITION_BY_REQ_ID", udParams);
            List<FileUploaddetails> fileUploaddetails = new List<FileUploaddetails>();
            List<GetDetailsByRequestId> GridLst = new List<GetDetailsByRequestId>();
            using (SqlDataReader readers = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_FDBK_FILES", udParams))
            {
                while (readers.Read())
                {
                    fileUploaddetails.Add(new FileUploaddetails()
                    {
                        Name = readers.GetString(0),
                        Path = "/OLivaUploadFiles/FeedbackUploads/" + readers.GetString(0),
                        //Path = HttpContext.Current.Server.MapPath("~/OLiva") + @"UploadFiles\" + HttpContext.Current.Session["TENANT"] + @"\" + readers.GetString(1),
                        UplTimeName = readers.GetString(1)
                    });
                }
                readers.Close();
                jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(fileUploaddetails);

            }
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_VIEW_FDBK_REQUISITION_BY_REQ_ID", udParams);
            if (ds.Tables[0].Rows.Count > 0)
            {
                user.LCM_CODE = ds.Tables[0].Rows[0]["LCM_CODE"].ToString();
                user.MAIN_CAT = ds.Tables[0].Rows[0]["MNC_CODE"].ToString();
                user.SUB_CAT = ds.Tables[0].Rows[0]["SUBC_CODE"].ToString();
                user.CLINIC_MGR_ID = ds.Tables[0].Rows[0]["CLINIC_MGR_ID"].ToString();
                user.CLINIC_MGR_NAME = ds.Tables[0].Rows[0]["CLINIC_MGR_NAME"].ToString();
                user.AOM_ID = ds.Tables[0].Rows[0]["AOM_ID"].ToString();
                user.AOM_NAME = ds.Tables[0].Rows[0]["AOM_NAME"].ToString();
                user.OTH_ID = ds.Tables[0].Rows[0]["OTH_ID"].ToString();
                user.OTHERS_NAME = ds.Tables[0].Rows[0]["OTHERS_NAME"].ToString();
                user.CLIENTNAME = ds.Tables[0].Rows[0]["FDBK_CLNT_NAME"].ToString();
                user.COMPLAINT_DATE = ds.Tables[0].Rows[0]["FDBK_CMP_REV_DT"].ToString();
                user.MAIN_STATUS = ds.Tables[0].Rows[0]["MAIN_STATUS"].ToString();
                user.MAIN_STATUS_ID = ds.Tables[0].Rows[0]["MAIN_ID"].ToString();
                user.MODE_ID = ds.Tables[0].Rows[0]["MODE_ID"].ToString();
                user.MODE_NAME = ds.Tables[0].Rows[0]["MODE_NAME"].ToString();
                user.MODECOMMENTS = ds.Tables[0].Rows[0]["FDBK_MODE_CMTS"].ToString();
                user.SUB_STATUS_NAME = ds.Tables[0].Rows[0]["SUB_STATUS"].ToString();
                user.SUB_STATUS_ID = ds.Tables[0].Rows[0]["SUB_STATUS_ID"].ToString();
                user.STATUSCOMMENTS = ds.Tables[0].Rows[0]["FDBK_STATUS_CMTS"].ToString();
                user.CREATED_BY = ds.Tables[0].Rows[0]["FDBK_CREATED_BY"].ToString();
                user.CONTACT = ds.Tables[0].Rows[0]["FDBK_CNT_NO"].ToString();
                //user.STA_IS_BTN_ENB = ds.Tables[0].Rows[0]["STA_IS_BTN_ENB"].ToString();
                //user.ROLE_ID = ds.Tables[0].Rows[0]["ROLE_ID"].ToString();
                user.jsonstring = jsonstring;
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                user.lcmlst = new List<LCMlst>();
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    user.lcmlst.Add(new LCMlst
                    {
                        LCM_CODE = Convert.ToString(dr["LCM_CODE"]),
                        LCM_NAME = Convert.ToString(dr["LCM_NAME"]),
                        ticked = true
                    });
                }
            }
            if (ds.Tables[2].Rows.Count > 0)
            {
                user.SubLst = new List<SUBCATlst>();
                foreach (DataRow dr in ds.Tables[2].Rows)
                {
                    user.SubLst.Add(new SUBCATlst
                    {
                        SUBC_CODE = Convert.ToString(dr["SUBC_CODE"]),
                        SUBC_NAME = Convert.ToString(dr["SUBC_NAME"]),
                        MNC_CODE = Convert.ToString(dr["SUBC_MNC_CODE"]),
                        ticked = true
                    });
                }
            }
            if (ds.Tables[3].Rows.Count > 0)
            {
                user.Modes = new List<Modes>();
                foreach (DataRow dr in ds.Tables[3].Rows)
                {
                    user.Modes.Add(new Modes
                    {
                        MODE_ID = Convert.ToString(dr["MODE_ID"]),
                        MODE_NAME = Convert.ToString(dr["MODE_NAME"]),
                        ticked = true
                    });
                }
            }

            if (ds.Tables[4].Rows.Count > 0)
            {
                user.StatusList = new List<ApprovalStat>();
                foreach (DataRow dr in ds.Tables[4].Rows)
                {
                    user.StatusList.Add(new ApprovalStat
                    {
                        SER_APP_STA_REMARKS_ID = Convert.ToString(dr["SER_APP_STA_REMARKS_ID_SUB1"]),
                        SER_APP_STA_REMARKS_NAME = Convert.ToString(dr["SER_APP_STA_REMARKS_NAME_SUB1"]),
                        ticked = true
                    });
                }
            }

            if (ds.Tables[5].Rows.Count > 0)
            {
                user.SubStatusList = new List<ApprovalStat>();
                foreach (DataRow dr in ds.Tables[5].Rows)
                {
                    user.SubStatusList.Add(new ApprovalStat
                    {
                        SER_APP_STA_REMARKS_ID = Convert.ToString(dr["SER_APP_STA_REMARKS_ID_SUB1"]),
                        SER_APP_STA_REMARKS_NAME = Convert.ToString(dr["SER_APP_STA_REMARKS_NAME_SUB1"]),
                        ticked = true
                    });
                }
            }

            return new { Message = MessagesVM.AF_OK, data = user };
            

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public DateTime getoffsetdatetime(DateTime date)
    {
        Object O = HttpContext.Current.Session["useroffset"];
        if (O != null)
        {
            string Temp = O.ToString().Trim();
            if (!Temp.Contains("+") && !Temp.Contains("-"))
                Temp = Temp.Insert(0, "+");
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            DateTime startTime = DateTime.Parse(DateTime.UtcNow.ToString());
            DateTime _now = DateTime.Parse(DateTime.UtcNow.ToString());
            foreach (TimeZoneInfo timeZoneInfo__1 in timeZones)
            {
                if (timeZoneInfo__1.ToString().Contains(Temp))
                {
                    // Response.Write(timeZoneInfo.ToString());
                    // string _timeZoneId = "Pacific Standard Time";
                    TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id);
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
                    break;
                }
            }
            return _now;
        }
        else
            return DateTime.Now;
    }

    public object ModifyFDBckDetails(saveFDBCKDetails user)
    {
        try
        {
           
            SqlParameter[] udParams = new SqlParameter[16];
            udParams[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            udParams[0].Value = HttpContext.Current.Session["UID"];
            udParams[1] = new SqlParameter("@COMPLAINT_DATE", SqlDbType.VarChar);
            udParams[1].Value = user.COMPLAINT_DATE;
            udParams[2] = new SqlParameter("@LCM_CODE", SqlDbType.VarChar);
            udParams[2].Value = user.LCM_CODE;
            udParams[3] = new SqlParameter("@CLIENTNAME", SqlDbType.VarChar);
            udParams[3].Value = user.CLIENTNAME;
            udParams[4] = new SqlParameter("@CONTACT", SqlDbType.VarChar);
            udParams[4].Value = user.CONTACT;
            udParams[5] = new SqlParameter("@MAIN_CAT", SqlDbType.VarChar);
            udParams[5].Value = user.MAIN_CAT;
            udParams[6] = new SqlParameter("@SUB_CAT", SqlDbType.VarChar);
            udParams[6].Value = user.SUB_CAT;
            udParams[7] = new SqlParameter("@CLINIC_MGR_ID", SqlDbType.NVarChar);
            udParams[7].Value = user.CLINIC_MGR_ID;
            udParams[8] = new SqlParameter("@AOM_ID", SqlDbType.VarChar);
            udParams[8].Value = user.AOM_ID;
            udParams[9] = new SqlParameter("@STATUS", SqlDbType.Int);
            udParams[9].Value = user.STATUS;
            udParams[10] = new SqlParameter("@MODECOMMENTS", SqlDbType.NVarChar);
            udParams[10].Value = user.MODECOMMENTS;
            udParams[11] = new SqlParameter("@MODE", SqlDbType.Int);
            udParams[11].Value = user.MODE;
            udParams[12] = new SqlParameter("@STATUSCOMMENTS", SqlDbType.NVarChar);
            udParams[12].Value = user.STATUSCOMMENTS;
            udParams[13] = new SqlParameter("@STATUSCOM", SqlDbType.NVarChar);
            udParams[13].Value = user.STATUSCOM;
            udParams[14] = new SqlParameter("@REQ_ID", SqlDbType.VarChar);
            udParams[14].Value = user.RequestId;
            udParams[15] = new SqlParameter("@OTHERS", SqlDbType.VarChar);
            udParams[15].Value = user.OTH_ID;
            DataSet dset = new DataSet();
            dset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_UPDATE_FDBK_REQ", udParams);

            string MaintenanceReq = user.RequestId;
            string MaintenanceReq_Upl = MaintenanceReq.Replace("/", "_");
            DataSet dset2 = new DataSet();
            foreach (var File in user.PostedFiles)
            {
                //if (user.PostedFiles[fucount].Equals(File.name))
                //{
                long intsize = System.Convert.ToInt64(File.size);
                string strFileName;
                string strFileExt;
                if (intsize <= 30971520)
                {
                    string Upload_Time = getoffsetdatetime(DateTime.Now).ToString("hhmmss");
                    strFileName = System.IO.Path.GetFileName(File.name);
                    strFileExt = System.IO.Path.GetExtension(File.name);
                    SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_FDBK_UPLOADED_FILES");
                    sp2.Command.AddParameter("@REQ_ID", MaintenanceReq, DbType.String);
                    sp2.Command.AddParameter("@FILENAME", strFileName, DbType.String);
                    sp2.Command.AddParameter("@UPLOAD_PATH", MaintenanceReq_Upl + "_" + Upload_Time + "_" + strFileName, DbType.String);
                    //sp2.Command.AddParameter("@REMARKS", user.ProbDesc, DbType.String);
                    sp2.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["Uid"], DbType.String);
                    sp2.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);
                    //sp2.ExecuteScalar();

                    dset2 = sp2.GetDataSet();
                }

            }
            return new { Message = "Service Requisition(" + MaintenanceReq + ") Modified successfully" };

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
}