﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System;
using System.Collections.ObjectModel;
using UtiltiyVM;
using System.IO;
using HDMUtilityVM;
/// <summary>
/// Summary description for HDMViewModifySubRequesitionsService
/// </summary>
public class HDMViewModifySubRequesitionsService
{
    SubSonic.StoredProcedure sp;

    public object GetMainCat()
    {
        List<MAINCATlst> MAIN_lst = new List<MAINCATlst>();
        MAINCATlst MAIN;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_MAIN_CAT_SUB");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                MAIN = new MAINCATlst();
                MAIN.MNC_CODE = sdr["MNC_CODE"].ToString();
                MAIN.MNC_NAME = sdr["MNC_NAME"].ToString();
                MAIN.ticked = false;
                MAIN_lst.Add(MAIN);
            }
        }
        if (MAIN_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = MAIN_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object Getsubcat()
    {
        List<SUBCATlst> SUB_lst = new List<SUBCATlst>();
        SUBCATlst SUB;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SUB_CAT_SUB");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                SUB = new SUBCATlst();
                SUB.SUBC_CODE = sdr["SUBC_CODE"].ToString();
                SUB.SUBC_NAME = sdr["SUBC_NAME"].ToString();
                SUB.MNC_CODE = sdr["SUBC_MNC_CODE"].ToString();
                SUB.ticked = false;
                SUB_lst.Add(SUB);
            }
        }
        if (SUB_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SUB_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object Getchildcat()
    {
        List<CHILDCATlst> CHILD_lst = new List<CHILDCATlst>();
        CHILDCATlst CHILD;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CHILD_CAT_SUB");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                CHILD = new CHILDCATlst();
                CHILD.CHC_TYPE_CODE = sdr["CHC_TYPE_CODE"].ToString();
                CHILD.CHC_TYPE_NAME = sdr["CHC_TYPE_NAME"].ToString();
                CHILD.CHC_TYPE_SUBC_CODE = sdr["CHC_TYPE_SUBC_CODE"].ToString();
                CHILD.CHC_TYPE_MNC_CODE = sdr["CHC_TYPE_MNC_CODE"].ToString();
                CHILD.ticked = false;
                CHILD_lst.Add(CHILD);
            }
        }
        if (CHILD_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = CHILD_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetDetailsByRequestId(GetDetailsByRequestId user)
    {
        try
        {

            
            string jsonstring;
            SqlParameter[] udParams = new SqlParameter[2];
            udParams[0] = new SqlParameter("@REQID", SqlDbType.VarChar);
            udParams[0].Value = user.RequestId;
            udParams[1] = new SqlParameter("@AURID", SqlDbType.VarChar);
            udParams[1].Value = HttpContext.Current.Session["UID"];
            //DataSet dset = new DataSet();
            //dset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_VIEW_SUB_REQUISITION_BY_REQ_ID", udParams);
            List<FileUploaddetails> fileUploaddetails = new List<FileUploaddetails>();
            List<GetDetailsByRequestId> GridLst = new List<GetDetailsByRequestId>();
            using (SqlDataReader readers = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_SUB_FILES", udParams))
            {
                //if (ds.Tables.Count > 0)
                //{
                //    GridLst.Add(new GetDetailsByRequestId()
                //    {
                //        Name = ds.Tables[0].Rows[0]["HRU_UPL_NAME_SUB1"].ToString(),
                //        Path = "/ UploadFiles / " + HttpContext.Current.Session["TENANT"] + " / " + ds.Tables[0].Rows[0]["HRU_UPL_PATH_SUB1"].ToString(),
                //        UplTimeName = ds.Tables[0].Rows[0]["HRU_UPL_PATH_SUB1"].ToString()
                //    });
                //}

                while (readers.Read())
                {
                    fileUploaddetails.Add(new FileUploaddetails()
                    {
                        Name = readers.GetString(0),
                        Path = "/OLivaUploadFiles/" + readers.GetString(0), 
                        //Path = HttpContext.Current.Server.MapPath("~/OLiva") + @"UploadFiles\" + HttpContext.Current.Session["TENANT"] + @"\" + readers.GetString(1),
                        UplTimeName = readers.GetString(1)
                    });
                }
                readers.Close();
                jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(fileUploaddetails);
                
            }
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_VIEW_SUB_REQUISITION_BY_REQ_ID", udParams);
            if (ds.Tables[0].Rows.Count > 0)
            {
                user.Mobile = ds.Tables[0].Rows[0]["MOBILE"].ToString();
                user.CustID = ds.Tables[0].Rows[0]["SER_CUST_ID_SUB1"].ToString();
                user.CustName = ds.Tables[0].Rows[0]["SER_CUST_NAME_SUB1"].ToString();
                user.MAIN_CATEGORY = ds.Tables[0].Rows[0]["MAIN_CATEGORY"].ToString();
                user.SUB_CATEGORY = ds.Tables[0].Rows[0]["SUB_CATEGORY"].ToString();
                user.CHILD_CATEGORY = ds.Tables[0].Rows[0]["CHILD_CATEGORY"].ToString();
                user.SER_LOC_CODE = ds.Tables[0].Rows[0]["SER_LOC_CODE"].ToString();
                user.CHC_TYPE_NAME = ds.Tables[0].Rows[0]["CHC_TYPE_NAME"].ToString();
                user.BilledBy = ds.Tables[0].Rows[0]["SER_BIL_BY_SUB1"].ToString();
                user.InvoiceDate = ds.Tables[0].Rows[0]["SER_INV_DATE_SUB1"].ToString();
                user.InvoiceNo = ds.Tables[0].Rows[0]["SER_INV_NO_SUB1"].ToString();
                user.REQUESTED_BY = ds.Tables[0].Rows[0]["REQUESTED_BY"].ToString();
                user.ASSIGNED_TO = ds.Tables[0].Rows[0]["ASSIGNED_TO"].ToString();
                user.CONTACT_NO = ds.Tables[0].Rows[0]["CONTACT_NO"].ToString();
                user.SER_UPDATED_DT_SUB1 = ds.Tables[0].Rows[0]["SER_UPDATED_DT_SUB1"].ToString();
                user.SER_UPDATED_BY_SUB1 = ds.Tables[0].Rows[0]["SER_UPDATED_BY_SUB1"].ToString();
                user.ProbDesc = ds.Tables[0].Rows[0]["REMARKS"].ToString();
                user.STATUS = ds.Tables[0].Rows[0]["STATUS_ID"].ToString();
                user.REQUESTED_DATE = ds.Tables[0].Rows[0]["REQUESTED_DATE"].ToString();
                user.STA_IS_BTN_ENB = ds.Tables[0].Rows[0]["STA_IS_BTN_ENB"].ToString();
                user.ROLE_ID = ds.Tables[0].Rows[0]["ROLE_ID"].ToString();
                user.jsonstring = jsonstring;
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                user.lcmlst = new List<Locationlst>();
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    user.lcmlst.Add(new Locationlst
                    {
                        LCM_CODE = Convert.ToString(dr["LCM_CODE"]),
                        LCM_NAME = Convert.ToString(dr["LCM_NAME"]),
                        CTY_CODE = Convert.ToString(dr["CTY_CODE"]),
                        CNY_CODE = Convert.ToString(dr["CNY_CODE"]),
                        ticked = true
                    });
                }
            }
            if (ds.Tables[2].Rows.Count > 0)
            {
                user.ChildCatlst = new List<ChildCatlst>();
                foreach (DataRow dr in ds.Tables[2].Rows)
                {
                    user.ChildCatlst.Add(new ChildCatlst
                    {
                        CHC_TYPE_CODE = Convert.ToString(dr["CHC_TYPE_CODE"]),
                        CHC_TYPE_NAME = Convert.ToString(dr["CHC_TYPE_NAME"]),
                        CHC_TYPE_SUBC_CODE = Convert.ToString(dr["CHC_TYPE_SUBC_CODE"]),
                        CHC_TYPE_MNC_CODE = Convert.ToString(dr["CHC_TYPE_MNC_CODE"]),
                        ticked = true
                    });
                }
            }
            if (ds.Tables[3].Rows.Count > 0)
            {
                user.GridDetails = new List<GridDetails>();
                foreach (DataRow dr in ds.Tables[3].Rows)
                {
                    user.GridDetails.Add(new GridDetails
                    {
                        REQUESTED_DATE = Convert.ToString(dr["SER_CAL_LOG_DT_SUB1"]),
                        REQUESTED_BY = Convert.ToString(dr["REQUESTED_BY"]),
                        SER_UPDATED_DT_SUB1 = Convert.ToString(dr["UPDATED_DT"]),
                        SER_UPDATED_BY_SUB1 = Convert.ToString(dr["UPDATED_BY"]),
                        ProbDesc = Convert.ToString(dr["SERH_COMMENTS_SUB1"]),
                        STATUS = Convert.ToString(dr["STA_TITLE"]),
                    });
                }
            }

            // HIDING
            //using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_VIEW_SUB_REQUISITION_BY_REQ_ID", udParams))
            //{

            //    //List<UserRoleMapping> URMlist = new List<UserRoleMapping>();
            //    while (reader.Read())
            //    {
            //        GridLst.Add(new GetDetailsByRequestId()
            //        {
            //Mobile = reader["MOBILE"].ToString(),
            //            CustID = reader["SER_CUST_ID_SUB1"].ToString(),
            //            CustName = reader["SER_CUST_NAME_SUB1"].ToString(),
            //            MAIN_CATEGORY = reader["MAIN_CATEGORY"].ToString(),
            //            SUB_CATEGORY = reader["SUB_CATEGORY"].ToString(),
            //            CHILD_CATEGORY = reader["CHILD_CATEGORY"].ToString(),
            //            SER_LOC_CODE = reader["SER_LOC_CODE"].ToString(),
            //            CHC_TYPE_NAME = reader["CHC_TYPE_NAME"].ToString(),
            //            BilledBy = reader["SER_BIL_BY_SUB1"].ToString(),
            //            InvoiceDate = reader["SER_INV_DATE_SUB1"].ToString(),
            //            InvoiceNo = reader["SER_INV_NO_SUB1"].ToString(),
            //            REQUESTED_BY = reader["REQUESTED_BY"].ToString(),
            //            ASSIGNED_TO = reader["ASSIGNED_TO"].ToString(),
            //            CONTACT_NO = reader["CONTACT_NO"].ToString(),
            //            SER_UPDATED_DT_SUB1 = reader["SER_UPDATED_DT_SUB1"].ToString(),
            //            SER_UPDATED_BY_SUB1 = reader["SER_UPDATED_BY_SUB1"].ToString(),
            //            ProbDesc = reader["REMARKS"].ToString(),
            //            STATUS = reader["STA_TITLE"].ToString(),
            //            REQUESTED_DATE = reader["REQUESTED_DATE"].ToString(),
            //            jsonstring = jsonstring,
            //    });
            //    }
            //    reader.Close();

            //}
            //HIDE LAST
            return new { Message = MessagesVM.AF_OK, data = user };
            //DataSet dset2 = new DataSet();
            //foreach (var File in user.PostedFiles)
            //{
            //    //if (user.PostedFiles[fucount].Equals(File.name))
            //    //{
            //    long intsize = System.Convert.ToInt64(File.size);
            //    string strFileName;
            //    string strFileExt;
            //    if (intsize <= 30971520)
            //    {
            //        string Upload_Time = getoffsetdatetime(DateTime.Now).ToString("hhmmss");
            //        strFileName = System.IO.Path.GetFileName(File.name);
            //        strFileExt = System.IO.Path.GetExtension(File.name);
            //        string filePath = HttpContext.Current.Server.MapPath("~/OLiva") + @"UploadFiles\" + HttpContext.Current.Session["TENANT"] + @"\" + MaintenanceReq_Upl + "_" + Upload_Time + "_" + strFileName; // & "." & strFileExt
            //        File.SaveAs = filePath;

            //        SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_UPLOADED_FILES_SUB1");
            //        sp2.Command.AddParameter("@REQ_ID", MaintenanceReq, DbType.String);
            //        sp2.Command.AddParameter("@STATUS_ID", 1, DbType.Int32);
            //        sp2.Command.AddParameter("@FILENAME", strFileName, DbType.String);
            //        sp2.Command.AddParameter("@UPLOAD_PATH", MaintenanceReq_Upl + "_" + Upload_Time + "_" + strFileName, DbType.String);
            //        sp2.Command.AddParameter("@REMARKS", user.ProbDesc, DbType.String);
            //        sp2.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["Uid"], DbType.String);
            //        sp2.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);
            //        //sp2.ExecuteScalar();

            //        dset2 = sp2.GetDataSet();
            //    }

            //}
            //else
            //{
            //return new { Message = MessagesVM.ErrorMessage, data = dset };
            //}

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public DateTime getoffsetdatetime(DateTime date)
    {
        Object O = HttpContext.Current.Session["useroffset"];
        if (O != null)
        {
            string Temp = O.ToString().Trim();
            if (!Temp.Contains("+") && !Temp.Contains("-"))
                Temp = Temp.Insert(0, "+");
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            DateTime startTime = DateTime.Parse(DateTime.UtcNow.ToString());
            DateTime _now = DateTime.Parse(DateTime.UtcNow.ToString());
            foreach (TimeZoneInfo timeZoneInfo__1 in timeZones)
            {
                if (timeZoneInfo__1.ToString().Contains(Temp))
                {
                    // Response.Write(timeZoneInfo.ToString());
                    // string _timeZoneId = "Pacific Standard Time";
                    TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id);
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
                    break;
                }
            }
            return _now;
        }
        else
            return DateTime.Now;
    }

    public object ModifyEmpDetails(GetDetailsByRequestId user)
    {
        try
        {
            //int len = user.length;
            DataSet ds = new DataSet();
            SqlParameter[] udParams = new SqlParameter[13];
            udParams[0] = new SqlParameter("@AURID", SqlDbType.VarChar);
            udParams[0].Value = HttpContext.Current.Session["UID"];
            udParams[1] = new SqlParameter("@LCMLst", SqlDbType.Structured);
            udParams[1].Value = UtilityService.ConvertToDataTable(user.LCMlst);
            udParams[2] = new SqlParameter("@CHILDlst", SqlDbType.Structured);
            udParams[2].Value = UtilityService.ConvertToDataTable(user.CHILDlst);
            udParams[3] = new SqlParameter("@CustID", SqlDbType.VarChar);
            udParams[3].Value = user.CustID;
            udParams[4] = new SqlParameter("@CustName", SqlDbType.VarChar);
            udParams[4].Value = user.CustName;
            udParams[5] = new SqlParameter("@STA_ID", SqlDbType.VarChar);
            udParams[5].Value = 2;
            udParams[6] = new SqlParameter("@InvoiceNo", SqlDbType.VarChar);
            udParams[6].Value = user.InvoiceNo;
            udParams[7] = new SqlParameter("@SER_MOBILE", SqlDbType.VarChar);
            udParams[7].Value = user.Mobile == null ? "" : user.Mobile;
            udParams[8] = new SqlParameter("@SER_PROB_DESC", SqlDbType.NVarChar);
            udParams[8].Value = user.ProbDesc;
            //udParams[9] = new SqlParameter("@STATUS", SqlDbType.Int);
            //udParams[9].Value = status;
            udParams[9] = new SqlParameter("@InvoiceDate", SqlDbType.VarChar);
            udParams[9].Value = user.InvoiceDate;
            udParams[10] = new SqlParameter("@REQID", SqlDbType.NVarChar);
            udParams[10].Value = user.RequestId;
            //udParams[11] = new SqlParameter("@SER_CALL_LOG_BY", SqlDbType.NVarChar);
            //udParams[11].Value = HttpContext.Current.Session["UID"];
            udParams[11] = new SqlParameter("@BilledBy", SqlDbType.NVarChar);
            udParams[11].Value = user.BilledBy;
            udParams[12] = new SqlParameter("@Comments", SqlDbType.NVarChar);
            udParams[12].Value = user.Comments == null ? user.ProbDesc : user.Comments;
            DataSet dset = new DataSet();
            dset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_MODIFY_SUB_REQUISITION_BY_REQID", udParams);
            //HDM_MODIFY_SUB_REQUISITION_BY_REQ_ID
            string MaintenanceReq = user.RequestId;
            string MaintenanceReq_Upl = MaintenanceReq.Replace("/", "_");
            DataSet dset2 = new DataSet();
            int i = 1;
            foreach (var File in user.PostedFiles)
            {
                //if (user.PostedFiles[fucount].Equals(File.name))
                //{
                if (i == 1)
                {
                    SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_DELETE_UPLOADED_FILES_SUB_REQ_ID");
                    sp1.Command.AddParameter("@REQ_ID", user.RequestId, DbType.String);
                    sp1.ExecuteScalar();
                    i++;
                }
                long intsize = System.Convert.ToInt64(File.size);
                string strFileName;
                string strFileExt;
                if (intsize <= 30971520)
                {
                    string Upload_Time = getoffsetdatetime(DateTime.Now).ToString("hhmmss");
                    strFileName = System.IO.Path.GetFileName(File.name);
                    strFileExt = System.IO.Path.GetExtension(File.name);
                    string filePath = HttpContext.Current.Server.MapPath("~/UploadFiles") + @"\" + HttpContext.Current.Session["TENANT"] + @"\" + MaintenanceReq_Upl + "_" + Upload_Time + "_" + strFileName; // & "." & strFileExt
                    File.SaveAs = filePath;
                    SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_MODIFY_SUB_UPLOADED_FILES");
                    sp2.Command.AddParameter("@REQ_ID", user.RequestId, DbType.String);
                    sp2.Command.AddParameter("@STATUS_ID", 1, DbType.Int32);
                    sp2.Command.AddParameter("@FILENAME", strFileName, DbType.String);
                    sp2.Command.AddParameter("@UPLOAD_PATH", MaintenanceReq_Upl + "_" + Upload_Time + "_" + strFileName, DbType.String);
                    sp2.Command.AddParameter("@REMARKS", user.ProbDesc, DbType.String);
                    sp2.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["Uid"], DbType.String);
                    //sp2.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);
                    //sp2.ExecuteScalar();

                    dset2 = sp2.GetDataSet();
                }

            }
            //if (dset2.Tables.Count > 0)
            //{
            //    return new { Message = "Service Requisition(" + MaintenanceReq + ") raised successfully" };
            //}
            //else if (dset.Tables[0].Rows[0]["Result"].ToString() == "1")
            //{
            //    return new { Message = "Service Requisition(" + MaintenanceReq + ") raised successfully" };
            //}
            //else
            //{
            return new { Message = "Service Requisition(" + MaintenanceReq + ") Modified successfully" };
            //}

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object ApproveEmployeeDetails(GetDetailsByRequestId user)
    {
        try
        {
            DataSet ds = new DataSet();
            SqlParameter[] udParams = new SqlParameter[4];
            udParams[0] = new SqlParameter("@AURID", SqlDbType.VarChar);
            udParams[0].Value = HttpContext.Current.Session["UID"];
            udParams[1] = new SqlParameter("@REQID", SqlDbType.NVarChar);
            udParams[1].Value = user.RequestId;
            udParams[2] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            udParams[2].Value = user.ApprovalStatus;
            udParams[3] = new SqlParameter("@COMMENTS", SqlDbType.NVarChar);
            udParams[3].Value = user.Comments == null?"":user.Comments;
            DataSet dset = new DataSet();
            dset = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "APPROVE_SUB_REQUEST", udParams);
            return new { Message = "Service Requisition Approved successfully" };


        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object CancelRequest(GetDetailsByRequestId user)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_CANCEL_VIEW_REQUISITON_SUB ");
            sp.Command.AddParameter("@SER_ID", user.RequestId, DbType.String);
            sp.Command.AddParameter("@SER_STATUS", 3, DbType.String);
            sp.Command.AddParameter("@AURID", HttpContext.Current.Session["Uid"], DbType.String);
            sp.Command.AddParameter("@SER_PROB_DESC", user.ProbDesc, DbType.String);
            sp.Execute();
            return new { Message = "Service Requisition(" + user.RequestId + ") Cancelled successfully" };

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object SaveFileUploads(HttpRequest httpRequest)
    {

        //String Key = httpRequest.Params["APIKey"];
        //String RID = httpRequest.Params["RequestId"];
        //String RMKS = httpRequest.Params["Remarks"];
        //String STS = httpRequest.Params["Status"];
        //ValidateKey VALDB = Secure._ValidateAPIKey(Key);

        try
        {
            for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
            {
                var req = httpRequest.Files[i];
                var fn = req.FileName;
                //SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_VIEW_UPDATE_REQ_UPDATE");
                //sp2.Command.AddParameter("@REQ_ID", RID, DbType.String);
                //sp2.Command.AddParameter("@STA_ID", STS, DbType.Int32);
                //sp2.Command.AddParameter("@REMARKS", RMKS, DbType.String);
                //sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                //sp2.ExecuteScalar();
                var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "HelpdeskManagement\\HDImages\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);
                req.SaveAs(filePath);

            }
            return new { Status = "true", Message = MessagesVM.UM_OK };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }

    public object GetApprovalStatus()
    {
        try
        {
            DataSet ds = new DataSet();
            SqlParameter[] udParams = new SqlParameter[1];
            udParams[0] = new SqlParameter("@AURID", SqlDbType.VarChar);
            udParams[0].Value = HttpContext.Current.Session["UID"];
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_APPROVALS_STATUS", udParams))
            {
                List<ApprovalStat> AppStatuslist = new List<ApprovalStat>();
                while (reader.Read())
                {
                    AppStatuslist.Add(new ApprovalStat()
                    {
                        SER_APP_STA_REMARKS_ID = reader["SER_APP_STA_REMARKS_ID_SUB1"].ToString(),
                        SER_APP_STA_REMARKS_NAME = reader["SER_APP_STA_REMARKS_NAME_SUB1"].ToString(),
                        ticked = false,

                    });
                }
                reader.Close();
                return new { Message = MessagesVM.AF_OK, data = AppStatuslist };
            }



        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    

}