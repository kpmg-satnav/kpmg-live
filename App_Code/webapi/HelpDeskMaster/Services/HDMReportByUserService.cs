﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for HDMReportByUserService
/// </summary>
public class HDMReportByUserService
{
    SubSonic.StoredProcedure sp;
    List<HDMReportByUserVM> rptByUserlst;
    HDMReport_Bar_Graph rptBarGph;
    DataSet ds;
    public object GetReportByUser(HDMReportByUser hdmUsr)
    {
        rptByUserlst = GetHDMReportByUser(hdmUsr);

        if (rptByUserlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = rptByUserlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public List<HDMReportByUserVM> GetHDMReportByUser(HDMReportByUser HDM)
    {

        List<HDMReportByUserVM> rptByUserlst = new List<HDMReportByUserVM>();
        HDMReportByUserVM rptByUser;
        TimeSpan ts = new TimeSpan(23, 59, 0);
        HDM.ToDate = HDM.ToDate + ts;


        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_REPORT_BY_USER");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@FromDate", HDM.FromDate, DbType.DateTime);
        sp.Command.AddParameter("@ToDate", HDM.ToDate, DbType.DateTime);
        sp.Command.AddParameter("@CompanyId", HDM.CompanyId, DbType.Int32);
        sp.Command.AddParameter("@PageNumber", HDM.PageNumber, DbType.Int32);
        sp.Command.AddParameter("@PageSize", HDM.PageSize, DbType.Int32);
        sp.Command.AddParameter("@SEARCHVAL", HDM.SEARCHVAL, DbType.String);
        

        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByUser = new HDMReportByUserVM();
                rptByUser.SER_REQ_ID = sdr["SER_REQ_ID"].ToString();
                rptByUser.AUR_ID = sdr["AUR_ID"].ToString();
                rptByUser.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                rptByUser.LCM_NAME = sdr["LCM_NAME"].ToString();
                //rptByUser.TWR_NAME = sdr["TWR_NAME"].ToString();
                rptByUser.MNC_NAME = sdr["MNC_NAME"].ToString();
                rptByUser.SUBC_NAME = sdr["SUBC_NAME"].ToString();
                rptByUser.CHC_TYPE_NAME = sdr["CHC_TYPE_NAME"].ToString();
                rptByUser.NoOfEscalations = sdr["NoOfEscalations"].ToString();
                rptByUser.DEFINED_TAT = sdr["DEFINED_TAT"].ToString();
                rptByUser.TAT = sdr["TAT"].ToString();
                rptByUser.SLA = sdr["SLA"].ToString();
                rptByUser.TAT_SLA = sdr["TAT_SLA"].ToString();
                rptByUser.DELAYED_TIME = sdr["DELAYED_TIME"].ToString();
                rptByUser.DELAYED_SLA = sdr["DELAYED_SLA"].ToString();
                rptByUser.TOTAL = sdr["TOTAL"].ToString();
                rptByUser.SER_SUB_CAT_CODE = sdr["SER_SUB_CAT_CODE"].ToString();
                rptByUser.STATUS = sdr["STA_TITLE"].ToString();
                //rptByUser.SERH_CREATED_DT = sdr["SERH_CREATED_DT"].ToString();
                rptByUser.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                rptByUserlst.Add(rptByUser);
            }
            sdr.Close();
        }
        if (rptByUserlst.Count != 0)
            //return new { Message = MessagesVM.HDM_UM_OK, data = rptByUserlst };
            return rptByUserlst;
        else
            return null;
    }

    public object GetHistory(HDMReportByUserVM data)
    {
        List<HDMReport_Req_History> historyList = new List<HDMReport_Req_History>();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@REQID", SqlDbType.NVarChar);
        param[0].Value = data.SER_REQ_ID;
        HDMReport_Req_History req_history;
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_REQ_HISTORY_DETAILS", param))
        {
            while (reader.Read())
            {

                req_history = new HDMReport_Req_History();
                req_history.SERH_SER_ID = reader["SERH_SER_ID"].ToString();
                req_history.SERH_STA_TITLE = reader["STA_TITLE"].ToString();
                req_history.SERH_COMMENTS = reader["SERH_COMMENTS"].ToString();
                req_history.CREATEDON = Convert.ToDateTime(reader["SERH_CREATED_DT"]);
                req_history.CREATEDBY = reader["AUR_KNOWN_AS"].ToString();

                historyList.Add(req_history);

            }
            reader.Close();
        }
        return new { Message = MessagesVM.UM_OK, data = historyList };
    }

    public object GetLocationWiseCount(HDMReportByUser data)
    {
        List<HDMReport_Bar_Graph> rptBarGphLst = new List<HDMReport_Bar_Graph>();
        TimeSpan ts = new TimeSpan(23, 59, 0);
        data.ToDate = data.ToDate + ts;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_REQCNT_BY_LOCATIONS");
        sp.Command.AddParameter("@FROM_DATE", data.FromDate, DbType.DateTime);
        sp.Command.AddParameter("@TO_DATE", data.ToDate, DbType.DateTime);
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANY", data.CompanyId, DbType.String);

        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return arr;
    }

    public object GetSubCatWiseCount(HDMReportByUser data)
    {
        List<HDMReport_Bar_Graph> rptBarGphLst = new List<HDMReport_Bar_Graph>();
        TimeSpan ts = new TimeSpan(23, 59, 0);
        data.ToDate = data.ToDate + ts;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_REQCNT_BY_SUB_CAT");
        sp.Command.AddParameter("@FROM_DATE", data.FromDate, DbType.DateTime);
        sp.Command.AddParameter("@TO_DATE", data.ToDate, DbType.DateTime);
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANY", data.CompanyId, DbType.String);
        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return arr;
    }


    //public DataTable GetHDMReportDetails(HDMReportByUser Details)
    //{
    //    try
    //    {
    //        List<HDMReportByUserVM> CData = new List<HDMReportByUserVM>();
    //        SqlParameter[] param = new SqlParameter[2];
    //        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
    //        param[0].Value = HttpContext.Current.Session["UID"];
    //        if (Details.CNP_NAME == null)
    //        {
    //            param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
    //            param[1].Value = HttpContext.Current.Session["COMPANYID"];

    //        }
    //        else
    //        {
    //            param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
    //            param[1].Value = Details.CNP_NAME;

    //        }


    //        DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "HDM_GET_REPORT_BY_USER_EXPORTTOEXCEL", param);
    //        return dt;

    //    }
    //    catch
    //    {
    //        throw;
    //    }
    //}
    //public object SearchAllData(HDMReportByUser Det)
    //{
    //    List<HDMReportByUserVM> CData = new List<HDMReportByUserVM>();
    //    DataTable DT = new DataTable();
    //    try
    //    {
    //        SqlParameter[] param = new SqlParameter[5];
    //        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
    //        param[0].Value = HttpContext.Current.Session["UID"];
    //        param[1] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
    //        param[1].Value = HttpContext.Current.Session["COMPANYID"];
    //        param[2] = new SqlParameter("@PageNumber", SqlDbType.Int);
    //        param[2].Value = Det.PageNumber;
    //        param[3] = new SqlParameter("@PageSize", SqlDbType.Int);
    //        param[3].Value = Det.PageSize;
    //        param[4] = new SqlParameter("@SearchValue", SqlDbType.VarChar);
    //        param[4].Value = Det.SearchValue;

    //        DT = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "HDM_CONSOLIDATED_REPORT_SURCHGRID", param);
    //        return DT;
    //    }
    //    catch (Exception ex) { return DT; }
    //}

}