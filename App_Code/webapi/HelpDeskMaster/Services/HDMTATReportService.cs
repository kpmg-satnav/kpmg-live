﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for HDMTATReportService
/// </summary>
public class HDMTATReportService
{
    SubSonic.StoredProcedure sp;
    HDMTAT_Details TATdtls;
    DataSet ds;

    public List<HDMTAT_Details> GetSpaceReportDetails(HDMTAT_Params Params)
    {
        try
        {
            List<HDMTAT_Details> Spcdata = new List<HDMTAT_Details>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_REQUISITIONS_ALL_TAT");
            sp.Command.AddParameter("@FROM_DATE", Params.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", Params.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@REQTYP", Params.Request_Type, DbType.String);
            sp.Command.AddParameter("@CompanyId", Params.CompanyId, DbType.Int32);
            sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            sp.Command.AddParameter("@SEARCHVAL", Params.SearchValue, DbType.String);
            sp.Command.AddParameter("@PAGENUM", Params.PageNumber, DbType.String);
            sp.Command.AddParameter("@PAGESIZE", Params.PageSize, DbType.String);
            ds = sp.GetDataSet();

            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    TATdtls = new HDMTAT_Details();
                    TATdtls.REQ_ID = reader["SER_REQ_ID"].ToString();
                    TATdtls.DESCRIPTION = reader["DESCRIPTION"].ToString();
                    TATdtls.STA_TITLE = reader["STA_TITLE"].ToString();
                    TATdtls.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    Spcdata.Add(TATdtls);
                }
                reader.Close();
            }
            return Spcdata;
        }
        catch
        {
            throw;
        }
    }

    public object GetRequestTypes()
    {
        List<Type_Data> Statuslst = new List<Type_Data>();
        Type_Data Status;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_REQUEST_TYPE_STATUS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Status = new Type_Data();
                Status.CODE = sdr["STA_ID"].ToString();
                Status.NAME = sdr["STA_TITLE"].ToString();
                Statuslst.Add(Status);
            }
        }
        if (Statuslst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Statuslst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetReqDetails(HDMTAT_Details data)
    {
        try
        {
            List<HDMTAT_Details> VMlist = new List<HDMTAT_Details>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_REPORT_BY_TAT");
            sp.Command.AddParameter("@REQ_ID", data.REQ_ID);
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);

            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {

                    TATdtls = new HDMTAT_Details();
                    TATdtls.REQ_ID = reader["SER_REQ_ID"].ToString();
                    TATdtls.REQUESTED_BY = reader["REQUESTED_BY"].ToString();
                    TATdtls.REQUESTED_DATE = reader["REQUESTED_DATE"].ToString();
                    TATdtls.ASSIGNED_TO = reader["ASSIGNED_TO"].ToString();
                    TATdtls.DESCRIPTION = reader["SERH_COMMENTS"].ToString();
                    TATdtls.CLOSED_TIME = reader["CLOSED_TIME"].ToString();
                    TATdtls.TOTAL_TIME = reader["TOTAL_TIME"].ToString();
                    TATdtls.DEFINED_TAT = reader["DEFINED_TAT"].ToString();
                    TATdtls.DELAYED_TIME = reader["DELAYED_TIME"].ToString();
                    TATdtls.REQ_STATUS = reader["REQ_STATUS"].ToString();
                    VMlist.Add(TATdtls);
                }
                reader.Close();
            }
            return new { Message = MessagesVM.UM_OK, data = VMlist };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

}