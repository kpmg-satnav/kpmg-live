﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class HolidayService
{
    SubSonic.StoredProcedure sp;

    public int Save(HolidayModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_HOLIDAY");
            sp.Command.AddParameter("@HOL_REASON", model.HOL_REASON, DbType.String);
            sp.Command.AddParameter("@HOL_DATE", model.HOL_DATE, DbType.DateTime);
            sp.Command.AddParameter("@HOL_CITY_CODE", model.HOL_CITY_CODE, DbType.String);
            sp.Command.AddParameter("@HOL_STA_ID", model.HOL_STA_ID, DbType.String);
            sp.Command.AddParameter("@HOL_REM", model.HOL_REM, DbType.String);
            sp.Command.Parameters.Add("@HOL_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.String);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch {
            throw;
        }
    }

    public Boolean Update(HolidayModel upd)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_UPDATE_HOLIDAYS");
            sp.Command.AddParameter("@HOL_REASON", upd.HOL_REASON, DbType.String);
            sp.Command.AddParameter("@HOL_DATE", upd.HOL_DATE, DbType.DateTime);
            sp.Command.AddParameter("@HOL_CITY_CODE", upd.HOL_CITY_CODE, DbType.String);
            sp.Command.AddParameter("@HOL_STA_ID", upd.HOL_STA_ID, DbType.String);
            sp.Command.AddParameter("@HOL_REM", upd.HOL_REM, DbType.String);
            sp.Command.Parameters.Add("@HOL_USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@HOL_PREVDT", upd.HOL_PREVDATE, DbType.DateTime);
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.String);
            sp.Execute();
            return true;
        }
        catch {
            throw;
        }
    }

    public IEnumerable<City> GetActiveCities()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GET_HOLIDAY_CITY").GetReader())
            {
                List<City> Citylist = new List<City>();
                while (reader.Read())
                {
                    Citylist.Add(new City()
                    {
                        CTY_CODE = reader.GetValue(0).ToString(),
                        CTY_NAME = reader.GetValue(1).ToString()
                    });
                }
                reader.Close();
                return Citylist;
            }
        }
        catch {
            throw;
        }
    }


    public IEnumerable<HolidayModel> HolidaysBindGrid()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_HOLIDAYS_BINDGRID").GetReader())
            {
                List<HolidayModel> hldlist = new List<HolidayModel>();
                while (reader.Read())
                {
                    hldlist.Add(new HolidayModel()
                    {

                        HOL_REASON = reader.GetValue(0).ToString(),
                        HOL_DATE = (DateTime)reader.GetValue(1),
                        HOL_CITY_CODE = reader.GetValue(2).ToString(),
                        HOL_CITY_NAME = reader.GetValue(3).ToString(),
                        HOL_STA_ID = reader.GetValue(4).ToString(),
                        HOL_REM = reader.GetValue(5).ToString()
                    });
                }
                reader.Close();
                return hldlist;
            }
        }
        catch {
            throw;
        }
    }

    public List<HolidayModelUpload> GetDataTableFrmFile(HttpRequest httpRequest)
    {
        DataTable dt = new DataTable();

        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);

        //var uplst = CreateExcelFile.ReadAsList(filePath, "all");
        var hlst = ReadAsListHoliday(filePath);
        if (hlst.Count != 0)
            hlst.RemoveAt(0);
        return hlst;

    }
    public object UploadTemplate(HttpRequest httpRequest)
    {
        try
        {
            List<HolidayModelUpload> uadmlst = GetDataTableFrmFile(httpRequest);

            //String jsonstr = httpRequest.Params["CurrObj"];

            //HolidayModel UVM = (HolidayModel)Newtonsoft.Json.JsonConvert.DeserializeObject<HolidayModel>(jsonstr);

            string str = "INSERT INTO " + HttpContext.Current.Session["TENANT"] + "." + "TEMP_HDM_HOLIDAY (HOL_DATE,HOL_REASON,HOL_CTY_CODE,HOL_STA_ID,HOL_REM,HOL_CREATED_BY,HOL_CREATED_DT,COMPANYID) VALUES ";
            foreach (HolidayModelUpload uadm in uadmlst)
            {
                str = str + string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}'),",
                          uadm.HOL_DATE,
                          uadm.HOL_REASON,
                          uadm.HOL_CITY_CODE,
                          "1",
                          "",
                          HttpContext.Current.Session["UID"],
                          System.DateTime.Now,
                          HttpContext.Current.Session["COMPANYID"]
                      
                        );
            }
            str = str.Remove(str.Length - 1, 1);
            int retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str);
            if (retval != -1)
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                param[0].Value = HttpContext.Current.Session["UID"];


                DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "HDM_HOLIDAY_UPLOAD", param);
                return new { Message = MessagesVM.UAD_UPLOK, data = dt };
            }
            else
                return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = "Input excel was not in a correct format...", Info = ex.InnerException, data = (object)null };
        }
    }

    public static List<HolidayModelUpload> ReadAsListHoliday(string Path)
    {
        List<HolidayModelUpload> uplst = new List<HolidayModelUpload>();

        string fileName = Path;
        SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileName, false);

        WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
        IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
        string relationshipId = sheets.First().Id.Value;
        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
        Worksheet workSheet = worksheetPart.Worksheet;
        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
        IEnumerable<Row> rows = sheetData.Descendants<Row>();
        HolidayModelUpload uadv;
        foreach (Row row in rows)
        {
            uadv = new HolidayModelUpload();
            uadv.HOL_CITY_CODE = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(1));
            uadv.HOL_DATE = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(2));

            uadv.HOL_REASON = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(3));
       //     uadv.HOL_STA_ID = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(3));
         //   uadv.HOL_REM = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(4));
            //uadv.HOL_UPL_BY = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(5));
            //uadv.HOL_UPL_DT = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(6));
            //uadv.HOL_COMPANY_ID = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(7));
        
            uplst.Add(uadv);
        }
        spreadSheetDocument.Close();
        return uplst;
    }

    private static string GetCellValue(SpreadsheetDocument document, Cell cell)
    {
        SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
        string value = "";
        if (cell.CellValue != null)
            value = Convert.ToString(cell.CellValue.InnerXml);
        else
            value = "";

        if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
        {
            return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
        }
        else
        {
            return value;
        }
    }

}