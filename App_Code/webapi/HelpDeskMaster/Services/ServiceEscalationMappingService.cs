﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;
using System.IO;

/// <summary>
/// Summary description for ServiceEscalationMappingService
/// </summary>
public class ServiceEscalationMappingService
{
    SubSonic.StoredProcedure sp;

    //country
    public IEnumerable<Country> GetActiveCountries()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GETCOUNTRY").GetReader())
        {
            List<Country> countrylist = new List<Country>();
            while (reader.Read())
            {
                countrylist.Add(new Country()
                {
                    CNY_CODE = reader["CNY_CODE"].ToString(),
                    CNY_NAME = reader["CNY_NAME"].ToString(),
                });
            }
            reader.Close();
            return countrylist;
        }
    }

    //get city by country
    public IEnumerable<City> GetActiveCityByCountry(string id)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_City_By_Country");
        sp.Command.Parameters.Add("@CTY_CNY_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<City> citylist = new List<City>();
            while (reader.Read())
            {
                citylist.Add(new City()
                {
                    CTY_CODE = reader["CTY_CODE"].ToString(),
                    CTY_NAME = reader["CTY_NAME"].ToString()
                });
            }
            reader.Close();
            return citylist;
        }
    }

    //GET location by city
    public IEnumerable<Location> GetActiveLocationByCity(string id)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_location_By_City");
        sp.Command.Parameters.Add("@LCM_CTY_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<Location> locationlist = new List<Location>();
            while (reader.Read())
            {
                locationlist.Add(new Location()
                {
                    LCM_CODE = reader["LCM_CODE"].ToString(),
                    LCM_NAME = reader["LCM_NAME"].ToString(),
                });
            }
            reader.Close();
            return locationlist;
        }
    }

    //GET tower by loc
    public IEnumerable<Tower> GetActiveTowerByLocation(string id)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_Tower_By_location");
        sp.Command.Parameters.Add("@TWR_LOC_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<Tower> locationlist = new List<Tower>();
            while (reader.Read())
            {
                locationlist.Add(new Tower()
                {
                    TWR_CODE = reader["TWR_CODE"].ToString(),
                    TWR_NAME = reader["TWR_NAME"].ToString(),
                });
            }
            reader.Close();
            return locationlist;
        }
    }


    //Main Category
    public IEnumerable<MainCategoryModel> GetActiveMainCategories()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ACTIVE_MAIN_CATEGORIES").GetReader())
        {
            List<MainCategoryModel> maincategorylist = new List<MainCategoryModel>();
            while (reader.Read())
            {
                maincategorylist.Add(new MainCategoryModel()
                {
                    MNC_CODE = reader["MNC_CODE"].ToString(),
                    MNC_NAME = reader["MNC_NAME"].ToString(),
                });
            }
            reader.Close();
            return maincategorylist;
        }
    }

    //sub category
    public IEnumerable<SubCategoryModel> GetActiveSubCategoriesByMain(string id)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_SUBCAT_BY_MAINCAT");
        sp.Command.Parameters.Add("@SUBC_MNC_CODE", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<SubCategoryModel> subcategorylist = new List<SubCategoryModel>();
            while (reader.Read())
            {
                subcategorylist.Add(new SubCategoryModel()
                {
                    SUBC_CODE = reader["SUBC_CODE"].ToString(),
                    SUBC_NAME = reader["SUBC_NAME"].ToString(),
                });
            }
            reader.Close();
            return subcategorylist;
        }
    }
    //child category
    public IEnumerable<ChildCategoryModel> GetActiveChildCategoriesBySub(string id)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_CHILDCAT_BY_SUBCAT");
        sp.Command.Parameters.Add("@CHC_TYPE_SUBC_CODE", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<ChildCategoryModel> childcategorylist = new List<ChildCategoryModel>();
            while (reader.Read())
            {
                childcategorylist.Add(new ChildCategoryModel()
                {
                    CHC_TYPE_CODE = reader["CHC_TYPE_CODE"].ToString(),
                    CHC_TYPE_NAME = reader["CHC_TYPE_NAME"].ToString(),
                });
            }
            reader.Close();
            return childcategorylist;
        }
    }

    //Grid View
    public IEnumerable<ServiceEscalationMappingModel> GetSEMDetails(int id)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_SERVICE_ESCALATION_MAPPING_DETAILS");
        sp.Command.Parameters.Add("@COMPANY_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<ServiceEscalationMappingModel> Semlist = new List<ServiceEscalationMappingModel>();
            while (reader.Read())
            {
                Semlist.Add(new ServiceEscalationMappingModel()
                {

                    SEM_ID = (int)reader["SEM_ID"],
                    CNY_NAME = reader["CNY_NAME"].ToString(),
                    CTY_NAME = reader["CTY_NAME"].ToString(),
                    LCM_NAME = reader["LCM_NAME"].ToString(),
                    TWR_NAME = reader["TWR_NAME"].ToString(),
                    MNC_NAME = reader["MNC_NAME"].ToString(),
                    SUBC_NAME = reader["SUBC_NAME"].ToString(),
                    CHC_TYPE_NAME = reader["CHC_TYPE_NAME"].ToString(),
                    SEM_CNY_CODE = reader["SEM_CNY_CODE"].ToString(),
                    SEM_CTY_CODE = reader["SEM_CTY_CODE"].ToString(),
                    SEM_LOC_CODE = reader["SEM_LOC_CODE"].ToString(),
                    SEM_TWR_CODE = reader["SEM_TWR_CODE"].ToString(),
                    SEM_MNC_CODE = reader["SEM_MNC_CODE"].ToString(),
                    SEM_SUBC_CODE = reader["SEM_SUBC_CODE"].ToString(),
                    SEM_CHC_CODE = reader["SEM_CHC_CODE"].ToString(),
                    SEM_MAP_TYPE = reader["SEM_MAP_TYPE"].ToString(),
                    CNP_NAME = reader["COMPANYID"].ToString(),
                    HDM_MAIN_MOD_ID = reader["HDM_MAIN_MOD_ID"].ToString(),
                });
            }
            reader.Close();
            return Semlist;
        }
    }
    public IEnumerable<ServiceEscalationMappingDetails> GetRoleUserDetails(int id)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_Role_User_Details");
        sp.Command.Parameters.Add("@COMPANY_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<ServiceEscalationMappingDetails> Semlist = new List<ServiceEscalationMappingDetails>();
            while (reader.Read())
            {
                Semlist.Add(new ServiceEscalationMappingDetails()
                {
                    ROL_ACRONYM = reader["ROL_ACRONYM"].ToString(),
                    AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString(),
                    ROL_ID = (int)reader["ROL_ID"],
                    AD_ID = reader["AUR_ID"].ToString(),
                    AUR_EMAIL = reader["AUR_EMAIL"].ToString(),
                    ROL_ORDER = (int)reader["ROL_ORDER"],

                });
            }
            reader.Close();
            return Semlist;
        }
    }

    public IEnumerable<ServiceEscalationMappingDetails> GetRoleUserListByModule(int id)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Get_Role_User_Details_By_Module");
        sp.Command.Parameters.Add("@MODULE_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<ServiceEscalationMappingDetails> Semlist = new List<ServiceEscalationMappingDetails>();
            while (reader.Read())
            {
                Semlist.Add(new ServiceEscalationMappingDetails()
                {
                    ROL_ACRONYM = reader["ROL_ACRONYM"].ToString(),
                    AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString(),
                    ROL_ID = (int)reader["ROL_ID"],
                    AD_ID = reader["AUR_ID"].ToString(),
                    AUR_EMAIL = reader["AUR_EMAIL"].ToString(),
                    ROL_ORDER = (int)reader["ROL_ORDER"],

                });
            }
            reader.Close();
            return Semlist;
        }
    }

    // save
    public object Save(ServiceEscalationMappingVM dataobj)
    {
        List<ServiceEscalationMappingVM> HDSEM = new List<ServiceEscalationMappingVM>();
        var SEM = new ServiceEscalationMappingModel();
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@TWR_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(dataobj.Twrlst);
        param[1] = new SqlParameter("@CHC_LIST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(dataobj.SEM);
        param[2] = new SqlParameter("@SEM_USER_ID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["UID"];
        param[3] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];
        int HDIcount = dataobj.SEMD.Where(x => x.ROL_ID == 22).Count();
        if (dataobj.SEMD.Count == 1)
        {
            if (HDIcount == 1)
                param[4] = new SqlParameter("@SEM_MAP_TYPE", SqlDbType.NVarChar);
            param[4].Value = "SINGLE";
        }
        else
        {
            param[4] = new SqlParameter("@SEM_MAP_TYPE", SqlDbType.NVarChar);
            param[4].Value = "MULTIPLE";
        }
        param[5] = new SqlParameter("@SEM_ACTION_TYPE", SqlDbType.NVarChar);
        param[5].Value = "SCREEN";
        param[6] = new SqlParameter("@SEMD", SqlDbType.Structured);
        param[6].Value = UtilityService.ConvertToDataTable(dataobj.SEMD);
        Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "HDM_INSERT_SERVICE_ESCALATION_MAPPING", param);
        int sem_id = (int)o;
        string msg;
        if (sem_id == 0)
            msg = "Data Already Exists With Same Details. Please Edit and Modify.";
        else
            msg = "Data Inserted Succesfully";
        return new { data = sem_id, Message = msg };
    }

    //edit
    public IEnumerable<ServiceEscalationMappingDetails> EditSEMDetails(int id)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Edit_Service_Escalation_Mapping");
        sp.Command.Parameters.Add("@SEMD_SEM_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<ServiceEscalationMappingDetails> SERVICElist = new List<ServiceEscalationMappingDetails>();
            while (reader.Read())
            {
                SERVICElist.Add(new ServiceEscalationMappingDetails()
                {
                    ROL_ID = (int)reader["SEMD_ROL_ID"],
                    AD_ID = reader["SEMD_EMP_ID"].ToString(),
                    AUR_EMAIL = reader["SEMD_EMAIL_ID"].ToString(),
                    ROL_ACRONYM = reader["ROL_ACRONYM"].ToString(),
                    AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString()

                });
            }
            reader.Close();
            return SERVICElist;
        }
    }

    public object UpdateSEMDetails(ServiceEscalationMappingVM model)
    {

        List<ServiceEscalationMappingVM> HDSEM = new List<ServiceEscalationMappingVM>();
        var SEM = new ServiceEscalationMappingModel();
        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@TWR_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(model.Twrlst);
        param[1] = new SqlParameter("@CHC_LIST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(model.SEM);
        param[2] = new SqlParameter("@SEM_USER_ID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["UID"];
        param[3] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];
        int HDIcount = model.SEMD.Where(x => x.ROL_ID == 22).Count();
        if (model.SEMD.Count == 1)
        {
            if (HDIcount == 1)
                param[4] = new SqlParameter("@SEM_MAP_TYPE", SqlDbType.NVarChar);
            param[4].Value = "SINGLE";
        }
        else
        {
            param[4] = new SqlParameter("@SEM_MAP_TYPE", SqlDbType.NVarChar);
            param[4].Value = "MULTIPLE";
        }
        param[5] = new SqlParameter("@SEM_ACTION_TYPE", SqlDbType.NVarChar);
        param[5].Value = "SCREEN";
        param[6] = new SqlParameter("@SEMD", SqlDbType.Structured);
        param[6].Value = UtilityService.ConvertToDataTable(model.SEMD);
        param[7] = new SqlParameter("@SEM_ID", SqlDbType.NVarChar);
        param[7].Value = model.SEM_ID;
        Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "HDM_Update_Service_Escalation", param);
        int sem_id = (int)o;
        string msg;
        if (sem_id == 0)
            msg = "Data Already Exists With Same Details.";
        else
            msg = "Data Updated Succesfully";
        return new { data = sem_id, Message = msg };
        //var SEM = new ServiceEscalationMappingModel();
        //List<ServiceEscalationMappingVM> Semlist = new List<ServiceEscalationMappingVM>();
        //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_Update_Service_Escalation");
        //sp.Command.AddParameter("@SEM_ID", model.SEM.SEM_ID, DbType.String);


        //    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_DELETE_SERVICE_ESCALATION_MAPPING_DETAILS");
        //    sp.Command.AddParameter("@SEMD_SEM_ID", model.SEM.SEM_ID, DbType.String);
        //    sp.Execute();
        //    List<ServiceEscalationMappingDetails> Semlists = model.SEMD.ToList();

        //    foreach (var lis in Semlists)
        //    {
        //        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_SERVICE_ESCALATION_MAPPING_DETAILS");
        //        sp.Command.AddParameter("@SEMD_SEM_ID", model.SEM.SEM_ID, DbType.Int32);
        //        sp.Command.AddParameter("@SEMD_ROL_ID", lis.ROL_ID, DbType.String);
        //        sp.Command.AddParameter("@SEMD_EMP_ID", lis.AD_ID, DbType.String);
        //        sp.Command.AddParameter("@SEMD_EMAIL_ID", lis.AUR_EMAIL, DbType.String);
        //        sp.Command.AddParameter("@SEMD_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        //        sp.Command.AddParameter("@SEMD_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        //        sp.Command.AddParameter("@SEMD_ACTION_TYPE", "SCREEN");
        //        sp.Execute();
        //    }
        //    SEM.SEM_ID = sem_id;
    }

    public List<ServiceEscalationMappingVM> DownloadTemplate(ServiceEscalationMappingVM sermap)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@TWR_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(sermap.Twrlst);
        param[1] = new SqlParameter("@CHC_LIST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(sermap.SEM);
        param[2] = new SqlParameter("@SEM_USER_ID", SqlDbType.VarChar);
        param[2].Value = HttpContext.Current.Session["UID"];
        param[3] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];

        ServiceEscalationMappingVM dwndata;
        List<ServiceEscalationMappingVM> dwndatalst = new List<ServiceEscalationMappingVM>();
        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_DWLND_SERVICE_MAPPING", param);

            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_DWLND_SERVICE_MAPPING", param))
            {

                while (sdr.Read())
                {
                    dwndata = new ServiceEscalationMappingVM();
                    dwndata.Country = sdr["Country"].ToString();
                    dwndata.City = sdr["City"].ToString();
                    dwndata.Location = sdr["location"].ToString();
                    dwndata.Tower = sdr["Tower"].ToString();
                    dwndata.Main_Category = sdr["Main_Category"].ToString();
                    dwndata.Sub_Category = sdr["Sub_Category"].ToString();
                    dwndata.Child_Category = sdr["Child_Category"].ToString();
                    //dwndata.Company = sdr["Entity"].ToString();
                    dwndata.HelpDesk_InCharge = sdr["Help_desk_InCharge"].ToString();
                    dwndata.L1_Manager = sdr["L1Manager"].ToString();
                    dwndata.L2_Manager = sdr["L2Manager"].ToString();
                    dwndata.L3_Manager = sdr["L3Manager"].ToString();
                    dwndata.L4_Manager = sdr["L4Manager"].ToString();
                    dwndata.L5_Manager = sdr["L5Manager"].ToString();
                    dwndata.L6_Manager = sdr["L6Manager"].ToString();
                    dwndatalst.Add(dwndata);
                }
                sdr.Close();
            }
            return dwndatalst;
        }

        catch (Exception ex)
        {
            throw;
        }
    }

    public object UploadTemplate(HttpRequest httpRequest)
    {
        try
        {
            List<ServiceEscalationMappingVM> uadmlst = GetDataTableFrmReq(httpRequest);

            //String jsonstr = httpRequest.Params["CurrObj"];

            //UPLTYPEVM UVM = (UPLTYPEVM)Newtonsofkt.Json.JsonConvert.DeserializeObject<UPLTYPEVM>(jsonstr);

            string str = "INSERT INTO " + HttpContext.Current.Session["TENANT"] + "." + "TEMP_HDM_SERVICE_ESCALATION_MAPPING (TEMP_HDMSER_COUNTRY,TEMP_HDMSER_CITY,TEMP_HDMSER_LOCATION,TEMP_HDMSER_TOWER,TEMP_HDMSER_MNC,TEMP_HDMSER_SUBC,TEMP_HDMSER_CHC,TEMP_HDMSER_HDINCHARGE,TEMP_HDMSER_HDL1,TEMP_HDMSER_HDL2,TEMP_HDMSER_HDL3,TEMP_HDMSER_HDL4,TEMP_HDMSER_HDL5,TEMP_HDMSER_HDL6,TEMP_HDMSER_UPLOADED_BY) VALUES ";
            string str1 = "";
            int retval, cnt = 1;
            foreach (ServiceEscalationMappingVM uadm in uadmlst)
            {


                if ((cnt % 1000) == 0)
                {
                    str1 = str1.Remove(str1.Length - 1, 1);
                    retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                    if (retval == -1)
                    {
                        return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
                    }
                    else
                    {
                        str1 = "";
                        cnt = 1;
                    }

                }

                str1 = str1 + string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}'),",
                          uadm.Country.Replace("'", "''"),
                          uadm.City.Replace("'", "''"),
                          uadm.Location.Replace("'", "''"),
                          uadm.Tower.Replace("'", "''"),
                          uadm.Main_Category.Replace("'", "''"),
                          uadm.Sub_Category.Replace("'", "''"),
                          uadm.Child_Category.Replace("'", "''"),
                          uadm.HelpDesk_InCharge,
                          uadm.L1_Manager,
                          uadm.L2_Manager,
                          uadm.L3_Manager,
                          uadm.L4_Manager,
                          uadm.L5_Manager,
                          uadm.L6_Manager,
                          HttpContext.Current.Session["Uid"]

                        );
                cnt = cnt + 1;
            }
            str1 = str1.Remove(str1.Length - 1, 1);
            retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
            if (retval != -1)
            {
                SqlParameter[] param = new SqlParameter[2];
                //param[0] = new SqlParameter("@UPLTYPE", SqlDbType.VarChar);
                //param[0].Value = UVM.UplAllocType;
                param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                param[0].Value = HttpContext.Current.Session["UID"];
                //param[2] = new SqlParameter("@UPLOPTIONS", SqlDbType.VarChar);
                //param[2].Value = UVM.UplOptions;
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
                param[1].Value = HttpContext.Current.Session["COMPANYID"];
                DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "HDM_UPLOAD_SERVICE_ESCALATION_MAPPING", param);
                if (dt.Rows.Count > 0)
                    return new { Message = MessagesVM.UAD_UPLOK, data = dt };
                else
                    return new { Message = MessagesVM.UAD_UPLVALDAT, data = dt };
            }
            else
                return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
        }
        catch (Exception e)
        {

            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(e);

            return new { Message = MessagesVM.UAD_UPLVALDAT, Info = e.InnerException, data = (object)null };

        }
    }

    public List<ServiceEscalationMappingVM> GetDataTableFrmReq(HttpRequest httpRequest)
    {
        DataTable dt = new DataTable();

        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);

        //var uplst = CreateExcelFile.ReadAsList(filePath, "all");
        var uplist = CreateExcelFile.ReadHdmSerAsList(filePath);
        if (uplist.Count != 0)
            uplist.RemoveAt(0);
        return uplist;

    }


}