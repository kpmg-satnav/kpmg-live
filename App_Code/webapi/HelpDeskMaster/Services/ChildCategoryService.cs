﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

public class ChildCategoryService
{
    SubSonic.StoredProcedure sp;

    public int Save(HDMChildCategoryModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_SAVE_CHILD_CATEGORY_DATA");
            sp.Command.AddParameter("@C_CODE", model.Code, DbType.String);
            sp.Command.AddParameter("@C_NAME", model.Name, DbType.String);
            sp.Command.AddParameter("@C_SUBCAT", model.SubCode, DbType.String);
            sp.Command.AddParameter("@C_MAINCAT", model.MainCode, DbType.String);
            sp.Command.AddParameter("@C_ID", model.Status, DbType.String);
            sp.Command.AddParameter("@C_REMARKS", model.Remarks, DbType.String);
            sp.Command.AddParameter("@C_CRTBY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            if (model.Company != null)
            {
                sp.Command.Parameters.Add("@MODULE", model.Company, DbType.String);
            }
            else
            {
                sp.Command.Parameters.Add("@MODULE", 1, DbType.String);
            }
        }
        catch
        {
            throw;
        }
        int flag = (int)sp.ExecuteScalar();
        return flag;
    }
    public Boolean UpdateSubcatData(HDMChildCategoryModel updt)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_UPDATE_CHILD_CATEGORY_MASTER");
            sp.Command.AddParameter("@C_ID", updt.Code, DbType.String);
            sp.Command.AddParameter("@C_NAME", updt.Name, DbType.String);
            sp.Command.AddParameter("@C_SUBCAT", updt.SubCode, DbType.String);
            sp.Command.AddParameter("@C_MAINCAT", updt.MainCode, DbType.String);
            sp.Command.AddParameter("@C_STA_ID", updt.Status, DbType.String);
            sp.Command.AddParameter("@C_REMARKS", updt.Remarks, DbType.String);
            sp.Command.AddParameter("@C_UPDBY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            if (updt.Company != null)
            {
                sp.Command.Parameters.Add("@MODULE", updt.Company, DbType.String);
            }
            else
            {
                sp.Command.Parameters.Add("@MODULE", 1, DbType.String);
            }
            sp.Execute();
        }
        catch
        {
            throw;
        }
        return true;
    }

    public IEnumerable<SubCategoryModel> GetSubCategoryByModule(HDMChildCategoryModel getSub)
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Company", SqlDbType.NVarChar);
        param[0].Value = getSub.CompanyId;
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_ACTIVE_SUB_CATEGORIES_BY_MODULE", param))
        {
            try
            {
                List<SubCategoryModel> subcategorylist = new List<SubCategoryModel>();
                while (reader.Read())
                {
                    subcategorylist.Add(new SubCategoryModel()
                    {
                        SUBC_CODE = reader.GetValue(0).ToString(),
                        SUBC_NAME = reader.GetValue(1).ToString()
                    });
                }
                reader.Close();
                return subcategorylist;
            }
            catch
            {
                throw;
            }
        }

    }

    public IEnumerable<SubCategoryModel> GetActiveSubCategories()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ACTIVE_SUB_CATEGORIES").GetReader())
        {
            try
            {
                List<SubCategoryModel> subcategorylist = new List<SubCategoryModel>();
                while (reader.Read())
                {
                    subcategorylist.Add(new SubCategoryModel()
                    {
                        SUBC_CODE = reader.GetValue(0).ToString(),
                        SUBC_NAME = reader.GetValue(1).ToString()
                    });
                }
                reader.Close();
                return subcategorylist;
            }
            catch
            {
                throw;
            }
        }

    }

    public IEnumerable<MainCategoryModel> GetActiveMainCategories()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ACTIVE_MAIN_CATEGORIES").GetReader())
        {
            try
            {
                List<MainCategoryModel> maincategorylist = new List<MainCategoryModel>();
                while (reader.Read())
                {
                    maincategorylist.Add(new MainCategoryModel()
                    {
                        MNC_CODE = reader.GetValue(0).ToString(),
                        MNC_NAME = reader.GetValue(1).ToString()
                    });
                }
                reader.Close();
                return maincategorylist;
            }
            catch
            {
                throw;
            }
        }
    }

    public IEnumerable<MainCategoryModel> GetActiveMainCategoriesBySub(string id)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_MAINCAT_BY_SUBCAT");
        sp.Command.Parameters.Add("@MNC_SUBC_CODE", id);
        using (IDataReader reader = sp.GetReader())
        {
            try
            {
                List<MainCategoryModel> subcategorylist = new List<MainCategoryModel>();
                while (reader.Read())
                {
                    subcategorylist.Add(new MainCategoryModel()
                    {
                        MNC_CODE = reader.GetValue(0).ToString(),
                        MNC_NAME = reader.GetValue(1).ToString()
                    });
                }
                reader.Close();
                return subcategorylist;
            }
            catch
            {
                throw;
            }
        }
    }
    public IEnumerable<HDMChildCategoryModel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_BIND_CHILD_CATEGORY_GRIDDATA").GetReader())
        {
            try
            {
                List<HDMChildCategoryModel> catlist = new List<HDMChildCategoryModel>();
                while (reader.Read())
                {
                    catlist.Add(new HDMChildCategoryModel()
                    {
                        SubCode = reader.GetValue(0).ToString(),
                        SubName = reader.GetValue(1).ToString(),
                        MainCode = reader.GetValue(2).ToString(),
                        MainName = reader.GetValue(3).ToString(),
                        Status = reader.GetValue(4).ToString(),
                        Remarks = reader.GetValue(5).ToString(),
                        Code = reader.GetValue(6).ToString(),
                        Name = reader.GetValue(7).ToString(),
                        Company = reader.GetValue(9).ToString(),
                        CompanyId = reader.GetValue(10).ToString()
                    });
                }
                reader.Close();
                return catlist;
            }
            catch
            {
                throw;
            }
        }
    }

    public object GetCompanyModules()
    {
        List<GetCompanyModules> getCompanyModules = new List<GetCompanyModules>();
        GetCompanyModules app;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_HELPDESK_MODULES_SUB");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                app = new GetCompanyModules();
                app.HDM_MAIN_MOD_ID = sdr["HDM_MAIN_MOD_ID"].ToString();
                app.HDM_MAIN_MOD_NAME = sdr["HDM_MAIN_MOD_NAME"].ToString();
                app.ticked = false;
                getCompanyModules.Add(app);
            }
        }
        if (getCompanyModules.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = getCompanyModules };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

}