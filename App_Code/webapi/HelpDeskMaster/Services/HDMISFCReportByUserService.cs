﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for HDMISFCReportByUserService
/// </summary>
public class HDMISFCReportByUserService
{
	SubSonic.StoredProcedure sp;
    List<HDMISFCReportByUserVM> rptByUserlst;
    HDMReport_Bar_Graph rptBarGph;
    DataSet ds;
    public object GetReportByUser(HDMReportByUser hdmUsr)
    {
        rptByUserlst = GetHDMReportByUser(hdmUsr);

        if (rptByUserlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = rptByUserlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetStatusTypes()
    {
        List<GetRequestTypes> status = new List<GetRequestTypes>();
        GetRequestTypes req;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_REQ_STATUS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                req = new GetRequestTypes();
                req.SCODE = sdr["STA_ID"].ToString();
                req.SNAME = sdr["STA_TITLE"].ToString();
                status.Add(req);
            }
        }
        if (status.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = status };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public List<HDMISFCReportByUserVM> GetHDMReportByUser(HDMReportByUser HDM)
    {

        List<HDMISFCReportByUserVM> rptByUserlst = new List<HDMISFCReportByUserVM>();
        HDMISFCReportByUserVM rptByUser;
        TimeSpan ts = new TimeSpan(23, 59, 0);
        HDM.ToDate = HDM.ToDate + ts;


        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_REPORT_BY_USER_NEW");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@FromDate", HDM.FromDate, DbType.DateTime);
        sp.Command.AddParameter("@ToDate", HDM.ToDate, DbType.DateTime);
        sp.Command.AddParameter("@CompanyId", HDM.CompanyId, DbType.Int32);
        sp.Command.AddParameter("@Status", HDM.Status, DbType.String);
        //sp.Command.AddParameter("@Rejected", HDM.Rejected, DbType.String);
        

        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                rptByUser = new HDMISFCReportByUserVM();
                rptByUser.SER_REQ_ID = sdr["SER_REQ_ID"].ToString();
                rptByUser.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                rptByUser.LCM_NAME = sdr["LCM_NAME"].ToString();
                rptByUser.TOTAL_AMOUNT = (int)sdr["TOTAL_AMOUNT"];
                rptByUser.MNC_NAME = sdr["MNC_NAME"].ToString();
                rptByUser.SUBC_NAME = sdr["SUBC_NAME"].ToString();
                rptByUser.CHC_TYPE_NAME = sdr["CHC_TYPE_NAME"].ToString();

                rptByUser.NoOfEscalations = sdr["NoOfEscalations"].ToString();
                rptByUser.DEFINED_TAT = sdr["DEFINED_TAT"].ToString();
                rptByUser.TAT = sdr["TAT"].ToString();
                rptByUser.SLA = sdr["SLA"].ToString();
                rptByUser.TAT_SLA = sdr["TAT_SLA"].ToString();
                rptByUser.DELAYED_TIME = sdr["DELAYED_TIME"].ToString();
                rptByUser.DELAYED_SLA = sdr["DELAYED_SLA"].ToString();
                rptByUser.TOTAL = sdr["TOTAL"].ToString();
                rptByUser.SER_SUB_CAT_CODE = sdr["SER_SUB_CAT_CODE"].ToString();
                rptByUser.STATUS = sdr["HD_STATUS"].ToString();
                rptByUser.NO_OF_APPROVALS = sdr["NoOfApprovals"].ToString();
                rptByUser.L1_Approved = sdr["L1_APP_BY"].ToString();
                rptByUser.L1_APPR_DT = sdr["L1_APP_DT"].ToString();
                rptByUser.L1_COMMENTS = sdr["L1_APP_REM"].ToString();
                rptByUser.L2_Approved = sdr["L2_APP_BY"].ToString();
                rptByUser.L2_APPR_DT = sdr["L2_APP_DT"].ToString();
                rptByUser.L2_COMMENTS = sdr["L2_APP_REM"].ToString();
                rptByUser.L3_Approved = sdr["L3_APP_BY"].ToString();
                rptByUser.L3_APPR_DT = sdr["L3_APP_DT"].ToString();
                rptByUser.L3_COMMENTS = sdr["L3_APP_REM"].ToString();
                rptByUser.L4_Approved = sdr["L4_APP_BY"].ToString();
                rptByUser.L4_APPR_DT = sdr["L4_APP_DT"].ToString();
                rptByUser.L4_COMMENTS = sdr["L4_APP_REM"].ToString();
                rptByUser.L5_Approved = sdr["L5_APP_BY"].ToString();
                rptByUser.L5_APPR_DT = sdr["L5_APP_DT"].ToString();
                rptByUser.L5_COMMENTS = sdr["L5_APP_REM"].ToString();
                rptByUser.L6_Approved = sdr["L6_APP_BY"].ToString();
                rptByUser.L6_APPR_DT = sdr["L6_APP_DT"].ToString();
                rptByUser.L6_COMMENTS = sdr["L6_APP_REM"].ToString();
                rptByUser.L7_Approved = sdr["L7_APP_BY"].ToString();
                rptByUser.L7_APPR_DT = sdr["L7_APP_DT"].ToString();
                rptByUser.L7_COMMENTS = sdr["L7_APP_REM"].ToString();
                rptByUser.L8_Approved = sdr["L8_APP_BY"].ToString();
                rptByUser.L8_APPR_DT = sdr["L8_APP_DT"].ToString();
                rptByUser.L8_COMMENTS = sdr["L8_APP_REM"].ToString();
                rptByUser.L9_Approved = sdr["L9_APP_BY"].ToString();
                rptByUser.L9_APPR_DT = sdr["L9_APP_DT"].ToString();
                rptByUser.L9_COMMENTS = sdr["L9_APP_REM"].ToString();
                rptByUser.L10_Approved = sdr["L10_APP_BY"].ToString();
                rptByUser.L10_APPR_DT = sdr["L10_APP_DT"].ToString();
                rptByUser.L10_COMMENTS = sdr["L10_APP_REM"].ToString();
                rptByUser.SER_PROB_DESC = sdr["SER_PROB_DESC"].ToString();
                rptByUser.SER_UPDATED_DT = sdr["SER_UPDATED_DT"].ToString();
                rptByUser.SER_CLAIM_AMT = sdr["SER_CLAIM_AMT"].ToString();
                rptByUser.SER_APPR_AMOUNT = sdr["SER_APPR_AMOUNT"].ToString();
                rptByUser.HRU_UPL_PATH = sdr["HRU_UPL_PATH"].ToString();
                rptByUserlst.Add(rptByUser);
            }
            sdr.Close();
        }
        if (rptByUserlst.Count != 0)
            //return new { Message = MessagesVM.HDM_UM_OK, data = rptByUserlst };
            return rptByUserlst;
        else
            return null;
    }

    public object GetHistory(HDMReportByUserVM data)
    {
        List<HDMReport_Req_History> historyList = new List<HDMReport_Req_History>();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@REQID", SqlDbType.NVarChar);
        param[0].Value = data.SER_REQ_ID;
        HDMReport_Req_History req_history;
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_REQ_HISTORY_DETAILS", param))
        {
            while (reader.Read())
            {

                req_history = new HDMReport_Req_History();
                req_history.SERH_SER_ID = reader["SERH_SER_ID"].ToString();
                req_history.SERH_STA_TITLE = reader["STA_TITLE"].ToString();
                req_history.SERH_COMMENTS = reader["SERH_COMMENTS"].ToString();
                req_history.CREATEDDATE = reader["SERH_CREATED_DT"].ToString();
                req_history.CREATEDBY = reader["AUR_KNOWN_AS"].ToString();

                historyList.Add(req_history);

            }
            reader.Close();
        }
        return new { Message = MessagesVM.UM_OK, data = historyList };
    }

    public object GetLocationWiseCount(HDMReportByUser data)
    {
        List<HDMReport_Bar_Graph> rptBarGphLst = new List<HDMReport_Bar_Graph>();
        TimeSpan ts = new TimeSpan(23, 59, 0);
        data.ToDate = data.ToDate + ts;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_REQCNT_BY_LOCATIONS");
        sp.Command.AddParameter("@FROM_DATE", data.FromDate, DbType.DateTime);
        sp.Command.AddParameter("@TO_DATE", data.ToDate, DbType.DateTime);
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANY", data.CompanyId, DbType.String);

        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return arr;
    }

    public object GetSubCatWiseCount(HDMReportByUser data)
    {
        List<HDMReport_Bar_Graph> rptBarGphLst = new List<HDMReport_Bar_Graph>();
        TimeSpan ts = new TimeSpan(23, 59, 0);
        data.ToDate = data.ToDate + ts;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_REQCNT_BY_SUB_CAT");
        sp.Command.AddParameter("@FROM_DATE", data.FromDate, DbType.DateTime);
        sp.Command.AddParameter("@TO_DATE", data.ToDate, DbType.DateTime);
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANY", data.CompanyId, DbType.String);
        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return arr;
    }
    public object DloadFiles(HDMReportByUser data)
    {

        var filepath = HttpContext.Current.Server.MapPath("~/UploadFiles/[ISFC].dbo/");
        //var filepath = HttpContext.Current.Request.Url.Host;
        var directory = new DirectoryInfo(filepath);
        DateTime from_date = Convert.ToDateTime(data.FromDate);
        DateTime to_date = Convert.ToDateTime(data.ToDate);
        var files1 = directory.GetFiles()
          .Where(file => file.LastWriteTime >= from_date && file.LastWriteTime <= to_date);

        var i = files1.Count();
        int j = 0;
        List<string> str = new List<string>();
        //string[] a = new string[];
        foreach (FileInfo file in files1)
        {
            var fname = file.Name;
            str.Add(fname);
            j++;
        }

        return str;

    }
}