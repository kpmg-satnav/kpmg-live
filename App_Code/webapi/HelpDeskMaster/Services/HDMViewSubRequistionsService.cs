﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

/// <summary>
/// Summary description for HDMViewSubRequistionsService
/// </summary>
public class HDMViewSubRequistionsService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object GetMainCat()
    {
        List<MAINCATlst> MAIN_lst = new List<MAINCATlst>();
        MAINCATlst MAIN;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_MAIN_CAT_SUB");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                MAIN = new MAINCATlst();
                MAIN.MNC_CODE = sdr["MNC_CODE"].ToString();
                MAIN.MNC_NAME = sdr["MNC_NAME"].ToString();
                MAIN.ticked = false;
                MAIN_lst.Add(MAIN);
            }
        }
        if (MAIN_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = MAIN_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object Getsubcat()
    {
        List<SUBCATlst> SUB_lst = new List<SUBCATlst>();
        SUBCATlst SUB;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SUB_CAT_SUB");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                SUB = new SUBCATlst();
                SUB.SUBC_CODE = sdr["SUBC_CODE"].ToString();
                SUB.SUBC_NAME = sdr["SUBC_NAME"].ToString();
                SUB.MNC_CODE = sdr["SUBC_MNC_CODE"].ToString();
                SUB.ticked = false;
                SUB_lst.Add(SUB);
            }
        }
        if (SUB_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SUB_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object Getchildcat()
    {
        List<CHILDCATlst> CHILD_lst = new List<CHILDCATlst>();
        CHILDCATlst CHILD;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CHILD_CAT_SUB");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                CHILD = new CHILDCATlst();
                CHILD.CHC_TYPE_CODE = sdr["CHC_TYPE_CODE"].ToString();
                CHILD.CHC_TYPE_NAME = sdr["CHC_TYPE_NAME"].ToString();
                CHILD.CHC_TYPE_SUBC_CODE = sdr["CHC_TYPE_SUBC_CODE"].ToString();
                CHILD.CHC_TYPE_MNC_CODE = sdr["CHC_TYPE_MNC_CODE"].ToString();
                CHILD.ticked = false;
                CHILD_lst.Add(CHILD);
            }
        }
        if (CHILD_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = CHILD_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetGriddata()
    {
        //List<AssertLocations> asserts_Lst = new List<AssertLocations>();
        //AssertLocations assert;
        ds = new DataSet();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@AURID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        //ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_VIEW_SUB_REQUISITIONS", param);
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_VIEW_SUB_REQUISITIONS", param))
        {
            List<BindGridDetails> GridLst = new List<BindGridDetails>();
            //List<UserRoleMapping> URMlist = new List<UserRoleMapping>();
            while (reader.Read())
            {
                GridLst.Add(new BindGridDetails()
                {
                    REQUEST_ID = reader["REQUEST_ID"].ToString(),
                    REQUESTED_DATE = reader["REQUESTED_DATE"].ToString(),
                    REQUESTED_BY = reader["REQUESTED_BY"].ToString(),
                    LOCATION = reader["LOCATION"].ToString(),
                    MAIN_CATEGORY = reader["MAIN_CATEGORY"].ToString(),
                    SUB_CATEGORY = reader["SUB_CATEGORY"].ToString(),
                    CHILD_CATEGORY = reader["CHILD_CATEGORY"].ToString(),
                    //ASSIGNED_TO = HttpContext.Current.Session["UID"].ToString(),
                    CONTACT_NO = reader["CONTACT_NO"].ToString(),
                    REQ_STATUS = reader["REQ_STATUS"].ToString(),
                    //ESC_COUNT = reader["ESC_COUNT"].ToString(),
                    TOTAL_TIME = reader["TOTAL_TIME"].ToString(),
                    UPDATED_COM = reader["SER_UPDATED_COMMENTS_SUB1"].ToString(),
                    ROLID = reader["URL_ROL_ID"].ToString()
                });
            }
            reader.Close();
            return new { Message = MessagesVM.AF_OK, data = GridLst };
        }
    }


    public object GetGriddataBySearch(BindGridDetailsBySearch bindGridDetailsBySearch)
    {
        //List<AssertLocations> asserts_Lst = new List<AssertLocations>();
        //AssertLocations assert;
        //ds = new DataSet();
        SqlParameter[] udParams = new SqlParameter[9];
        udParams[0] = new SqlParameter("@AURID", SqlDbType.NVarChar);
        udParams[0].Value = HttpContext.Current.Session["UID"];
        udParams[1] = new SqlParameter("@LCMLst", SqlDbType.Structured);
        udParams[1].Value = UtilityService.ConvertToDataTable(bindGridDetailsBySearch.LCMlst);
        udParams[2] = new SqlParameter("@CHILDlst", SqlDbType.Structured);
        udParams[2].Value = UtilityService.ConvertToDataTable(bindGridDetailsBySearch.CHILDlst);
        udParams[3] = new SqlParameter("@STATUSlst", SqlDbType.Structured);
        udParams[3].Value = UtilityService.ConvertToDataTable(bindGridDetailsBySearch.STATUSlst);
        udParams[4] = new SqlParameter("@HSTSTATUS", SqlDbType.VarChar);
        udParams[4].Value = bindGridDetailsBySearch.HSTSTATUS;
        udParams[5] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
        udParams[5].Value = bindGridDetailsBySearch.FromDate.ToString();
        udParams[6] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
        udParams[6].Value = bindGridDetailsBySearch.ToDate.ToString();
        udParams[7] = new SqlParameter("@REQID", SqlDbType.NVarChar);
        udParams[7].Value = bindGridDetailsBySearch.RequestID;
        udParams[8] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        udParams[8].Value = HttpContext.Current.Session["COMPANYID"];
        ////if (len != 0)
        ////{
        //udParams[5] = new SqlParameter("@ASSERTLst", SqlDbType.Structured);
        //udParams[5].Value = UtilityService.ConvertToDataTable(user.ASSERTlst);

        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_SEARCH_VIEW_REQUISITIONS_SUB1", udParams);
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_SEARCH_VIEW_REQUISITIONS_SUB1", udParams))
        {
            List<BindGridDetails> GridLst = new List<BindGridDetails>();
            //List<UserRoleMapping> URMlist = new List<UserRoleMapping>();
            while (reader.Read())
            {
                GridLst.Add(new BindGridDetails()
                {
                    REQUEST_ID = reader["REQUEST_ID"].ToString(),
                    REQUESTED_DATE = reader["REQUESTED_DATE"].ToString(),
                    REQUESTED_BY = reader["REQUESTED_BY"].ToString(),
                    LOCATION = reader["LOCATION"].ToString(),
                    MAIN_CATEGORY = reader["MAIN_CATEGORY"].ToString(),
                    SUB_CATEGORY = reader["SUB_CATEGORY"].ToString(),
                    CHILD_CATEGORY = reader["CHILD_CATEGORY"].ToString(),
                    ASSIGNED_TO = reader["ASSIGNED_TO"].ToString(),
                    CONTACT_NO = reader["CONTACT_NO"].ToString(),
                    REQ_STATUS = reader["REQ_STATUS"].ToString(),
                    ESC_COUNT = reader["ESC_COUNT"].ToString(),
                    TOTAL_TIME = reader["TOTAL_TIME"].ToString(),
                    UPDATED_COM = reader["SER_UPDATED_COMMENTS_SUB1"].ToString()
                });
            }
            reader.Close();
            return new { Message = MessagesVM.AF_OK, data = GridLst };
        }
    }

    public object GetStatusList()
    {
        List<STATUSlst> STATUS_lst = new List<STATUSlst>();
        STATUSlst STATUS;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SUB_STATUS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                STATUS = new STATUSlst();
                STATUS.STA_ID = sdr["STA_ID"].ToString();
                STATUS.STA_TITLE = sdr["STA_TITLE"].ToString();
                STATUS.ticked = false;
                STATUS_lst.Add(STATUS);
            }
        }
        if (STATUS_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = STATUS_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
}