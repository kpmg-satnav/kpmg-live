﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

/// <summary>
/// Summary description for HDMSubRequestReportService
/// </summary>
public class HDMSubRequestReportService
{
    DataSet ds;
    public object GetReportGrid()
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@AURID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_SUB_REQ_REPORT",param))
        {
            List<BindReportDetails> GridLst = new List<BindReportDetails>();
            while (reader.Read())
            {
                GridLst.Add(new BindReportDetails()
                {
                    REQUEST_ID = reader["SER_REQ_ID_SUB1"].ToString(),
                    REQUESTED_DATE = reader["SER_CREATED_DT_SUB1"].ToString(),
                    REQUESTED_BY = reader["SER_CREATED_BY_SUB1"].ToString(),
                    LOCATION = reader["LCM_NAME"].ToString(),
                    MAIN_CATEGORY = reader["MNC_NAME"].ToString(),
                    SUB_CATEGORY = reader["SUBC_NAME"].ToString(),
                    CHILD_CATEGORY = reader["CHC_TYPE_NAME"].ToString(),
                    UPDATED_BY = reader["UPDATED_BY"].ToString(),
                    UPDATED_DT = reader["UPDATED_DT"].ToString(),
                    STA_TITLE = reader["STA_TITLE"].ToString(),
                    BILLED_BY = reader["SER_BIL_BY_SUB1"].ToString(),
                    CUST_ID = reader["SER_CUST_ID_SUB1"].ToString(),
                    CUST_NAME = reader["SER_CUST_NAME_SUB1"].ToString(),
                    INV_NO = reader["SER_INV_NO_SUB1"].ToString(),
                    SER_PROB_DESC_SUB1 = reader["SER_PROB_DESC_SUB1"].ToString(),
                    UPDATED_COM = reader["SER_UPDATED_COMMENTS_SUB1"].ToString(),
                });
            }
            reader.Close();
            return new { Message = MessagesVM.AF_OK, data = GridLst };
        }
    }

    public object GetGriddataBySearch(BindGridDetailsBySearch bindGridDetailsBySearch)
    {
        //List<AssertLocations> asserts_Lst = new List<AssertLocations>();
        //AssertLocations assert;
        //ds = new DataSet();
        SqlParameter[] udParams = new SqlParameter[9];
        udParams[0] = new SqlParameter("@AURID", SqlDbType.NVarChar);
        udParams[0].Value = HttpContext.Current.Session["UID"];
        udParams[1] = new SqlParameter("@LCMLst", SqlDbType.Structured);
        udParams[1].Value = UtilityService.ConvertToDataTable(bindGridDetailsBySearch.LCMlst);
        udParams[2] = new SqlParameter("@CHILDlst", SqlDbType.Structured);
        udParams[2].Value = UtilityService.ConvertToDataTable(bindGridDetailsBySearch.CHILDlst);
        udParams[3] = new SqlParameter("@STATUSlst", SqlDbType.Structured);
        udParams[3].Value = UtilityService.ConvertToDataTable(bindGridDetailsBySearch.STATUSlst);
        udParams[4] = new SqlParameter("@HSTSTATUS", SqlDbType.VarChar);
        udParams[4].Value = bindGridDetailsBySearch.HSTSTATUS;
        udParams[5] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
        udParams[5].Value = bindGridDetailsBySearch.FromDate.ToString();
        udParams[6] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
        udParams[6].Value = bindGridDetailsBySearch.ToDate.ToString();
        udParams[7] = new SqlParameter("@REQID", SqlDbType.NVarChar);
        udParams[7].Value = bindGridDetailsBySearch.RequestID;
        udParams[8] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        udParams[8].Value = HttpContext.Current.Session["COMPANYID"];
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_SEARCH_VIEW_REQUISITIONS_REPORT_SUB1", udParams))
        {
            List<BindReportDetails> GridLst = new List<BindReportDetails>();
            while (reader.Read())
            {
                GridLst.Add(new BindReportDetails()
                {
                    REQUEST_ID = reader["SER_REQ_ID_SUB1"].ToString(),
                    REQUESTED_DATE = reader["SER_CREATED_DT_SUB1"].ToString(),
                    REQUESTED_BY = reader["SER_CREATED_BY_SUB1"].ToString(),
                    LOCATION = reader["LCM_NAME"].ToString(),
                    MAIN_CATEGORY = reader["MNC_NAME"].ToString(),
                    SUB_CATEGORY = reader["SUBC_NAME"].ToString(),
                    CHILD_CATEGORY = reader["CHC_TYPE_NAME"].ToString(),
                    UPDATED_BY = reader["UPDATED_BY"].ToString(),
                    UPDATED_DT = reader["UPDATED_DT"].ToString(),
                    STA_TITLE = reader["STA_TITLE"].ToString(),
                    BILLED_BY = reader["SER_BIL_BY_SUB1"].ToString(),
                    CUST_ID = reader["SER_CUST_ID_SUB1"].ToString(),
                    CUST_NAME = reader["SER_CUST_NAME_SUB1"].ToString(),
                    INV_NO = reader["SER_INV_NO_SUB1"].ToString(),
                    SER_PROB_DESC_SUB1 = reader["SER_PROB_DESC_SUB1"].ToString(),
                    UPDATED_COM = reader["SER_UPDATED_COMMENTS_SUB1"].ToString(),
                });
            }
            reader.Close();
            return new { Message = MessagesVM.AF_OK, data = GridLst };
        }
    }
    public List<BindReportDetails> GetReportGridrdlc()
    {
        ds = new DataSet();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@AURID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_SUB_REQ_REPORT",param))
        {
            List<BindReportDetails> GridLst = new List<BindReportDetails>();
            while (reader.Read())
            {
                GridLst.Add(new BindReportDetails()
                {
                    REQUEST_ID = reader["SER_REQ_ID_SUB1"].ToString(),
                    REQUESTED_DATE = reader["SER_CREATED_DT_SUB1"].ToString(),
                    REQUESTED_BY = reader["SER_CREATED_BY_SUB1"].ToString(),
                    LOCATION = reader["LCM_NAME"].ToString(),
                    MAIN_CATEGORY = reader["MNC_NAME"].ToString(),
                    SUB_CATEGORY = reader["SUBC_NAME"].ToString(),
                    CHILD_CATEGORY = reader["CHC_TYPE_NAME"].ToString(),
                    UPDATED_BY = reader["UPDATED_BY"].ToString(),
                    UPDATED_DT = reader["UPDATED_DT"].ToString(),
                    STA_TITLE = reader["STA_TITLE"].ToString(),
                    BILLED_BY = reader["SER_BIL_BY_SUB1"].ToString(),
                    CUST_ID = reader["SER_CUST_ID_SUB1"].ToString(),
                    CUST_NAME = reader["SER_CUST_NAME_SUB1"].ToString(),
                    INV_NO = reader["SER_INV_NO_SUB1"].ToString(),
                    SER_PROB_DESC_SUB1 = reader["SER_PROB_DESC_SUB1"].ToString(),
                    UPDATED_COM = reader["SER_UPDATED_COMMENTS_SUB1"].ToString(),
                });
            }
            reader.Close();
            return GridLst;
        }
    }
}