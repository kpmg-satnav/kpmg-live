﻿using HDMUtilityVM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class CatDsgService
{
    SubSonic.StoredProcedure sp;

    public object getDesignations()
    {
        List<Dsglst> dsglst = new List<Dsglst>();
        Dsglst dsg;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ACTIVE_DESIGNATIONS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                dsg = new Dsglst();
                dsg.DSN_CODE = sdr["DSN_CODE"].ToString();
                dsg.DSN_AMT_TITLE = sdr["DSN_AMT_TITLE"].ToString();
                dsg.ticked = false;
                dsglst.Add(dsg);
            }
            sdr.Close();
        }
        if (dsglst.Count != 0)
            return new { Message = MessagesVM.HDM_UM_OK, data = dsglst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public int Save(CatDsgList model)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@ChildCatlst", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(model.ChildCatlst);
            param[1] = new SqlParameter("@Dsglst", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(model.Dsglst);
            param[2] = new SqlParameter("@HDM_CREATED_BY", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            //param[3] = new SqlParameter("@COMPANY", SqlDbType.NVarChar);
            //param[3].Value = HttpContext.Current.Session["COMPANYID"];

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_INSERT_MAPPING_DETAILS", param);

            return 0;
        }
        catch
        {
            throw;
        }
    }
    //UPDATE

    public Boolean Update(CatDsgList model)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@ChildCatlst", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(model.ChildCatlst);
            param[1] = new SqlParameter("@Dsglst", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(model.Dsglst);
            param[2] = new SqlParameter("@HDM_CREATED_BY", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            //param[3] = new SqlParameter("@COMPANY", SqlDbType.NVarChar);
            //param[3].Value = HttpContext.Current.Session["COMPANYID"];

            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_UPDATE_MAPPING_DETAILS", param);

            return true;
        }
        catch
        {
            throw;
        }
    }
    //Grid View
    public object GetMappingDetails()
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_MAPPING_DETAILS");
            //sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.String);
            using (IDataReader reader = sp.GetReader())
            {
                List<GridData> Grid = new List<GridData>();
                while (reader.Read())
                {
                    Grid.Add(new GridData()
                    {
                        DSN_AMT_TITLE = reader["DSN_AMT_TITLE"].ToString(),
                        CHC_TYPE_NAME = reader["CHC_TYPE_NAME"].ToString(),
                        HDM_DSN_CODE = reader["HDM_DSN_CODE"].ToString(),

                    });
                }
                reader.Close();
                return new { data = Grid };
            }
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    //edit

    public object editData(string Id)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "EDIT_MAPPING_DETAILS");
            sp.Command.AddParameter("@HDM_DSN_CODE", Id, DbType.String);


            CatDsgList Userlist = new CatDsgList();
            DataSet ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Userlist.Dsglst = new List<Dsglst>();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Userlist.Dsglst.Add(new Dsglst { DSN_CODE = Convert.ToString(dr["HDM_DSN_CODE"]), DSN_AMT_TITLE = Convert.ToString(dr["DSN_AMT_TITLE"]), ticked = true });
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    Userlist.ChildCatlst = new List<ChildCatlst>();
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        Userlist.ChildCatlst.Add(new ChildCatlst { CHC_TYPE_CODE = Convert.ToString(dr["HDM_CHC_CODE"]), CHC_TYPE_NAME = Convert.ToString(dr["CHC_TYPE_NAME"]), CHC_TYPE_SUBC_CODE = Convert.ToString(dr["HDM_SUB_CODE"]), CHC_TYPE_MNC_CODE = Convert.ToString(dr["HDM_MN_CODE"]), ticked = true });
                    }
                }

            }  return new { Message = MessagesVM.ErrorMessage, data = Userlist };

        }


        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }

    }
}