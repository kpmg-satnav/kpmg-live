﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;

public class HDMcustomizedReportService
{
    SubSonic.StoredProcedure sp;
    List<HDMcustomizedData> Cust;
    HDMcustomizedData Custm;

    public object GetCustomizedObject(HDMcustomizedDetails Det)
    {
        try
        {
            //col names in ddl
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_GET_ESCALTION_ROLES");

            var lst = GetCustomizedDetails(Det);
            if (lst != null)
            {
                return new { data = new { lst }, Message = MessagesVM.SER_OK };
            }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }

        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public object GetCustomizedDetails(HDMcustomizedDetails Details)
    {

        try
        {
            List<HDMcustomizedData> CData = new List<HDMcustomizedData>();
            DataTable DT = new DataTable();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            ExportClass exportcls;
            List<ExportClass> lstexportcls = new List<ExportClass>();

            SqlParameter[] param = new SqlParameter[11];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["UID"];
            param[1] = new SqlParameter("@LCMlst", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Details.LCMlst);
            param[2] = new SqlParameter("@CHILDlst", SqlDbType.Structured);
            param[2].Value = UtilityService.ConvertToDataTable(Details.CHILDlst);
            param[3] = new SqlParameter("@STATUSlst", SqlDbType.Structured);
            param[3].Value = UtilityService.ConvertToDataTable(Details.STATUSlst);
            param[4] = new SqlParameter("@FROMDATE", SqlDbType.NVarChar);
            param[4].Value = Details.FromDate.ToString();
            param[5] = new SqlParameter("@TODATE", SqlDbType.NVarChar);
            param[5].Value = Details.ToDate.ToString();
            param[6] = new SqlParameter("@COMPANY", SqlDbType.NVarChar);
            param[6].Value = Details.CompanyId;
            param[7] = new SqlParameter("@HIS_STATUS", SqlDbType.NVarChar);
            param[7].Value = Details.HSTSTATUS;
            param[8] = new SqlParameter("@SEARCHVAL", SqlDbType.NVarChar);
            param[8].Value = Details.SearchValue;
            param[9] = new SqlParameter("@PAGENUM", SqlDbType.NVarChar);
            param[9].Value = Details.PageNumber;
            param[10] = new SqlParameter("@PAGESIZE", SqlDbType.NVarChar);
            param[10].Value = Details.PageSize;

            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_CUSTOMIZED_RPT_MAIN", param);
            HttpContext.Current.Session["CustomizedGrid"] = ds;

            List<GetCustomizedGrid> customizedGridList = new List<GetCustomizedGrid>();
            GetCustomizedGrid customizedGrid = new GetCustomizedGrid();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(ds.Tables[0]);
            customizedGridList = JsonConvert.DeserializeObject<List<GetCustomizedGrid>>(JSONString);
            return new { griddata = customizedGridList };
        }
        catch (Exception Ex)
        {
            var err = Ex.Message;
            throw;
        }
    }
    public List<GetCustomizedGrid> GetCustomizedDetailsExport(HDMcustomizedDetails Details)
    {

        try
        {
            DataSet ds = new DataSet();
            ds = (DataSet)HttpContext.Current.Session["CustomizedGrid"];
            List<GetCustomizedGrid> customizedGridList = new List<GetCustomizedGrid>();
            GetCustomizedGrid customizedGrid = new GetCustomizedGrid();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(ds.Tables[0]);
            customizedGridList = JsonConvert.DeserializeObject<List<GetCustomizedGrid>>(JSONString);
            return customizedGridList;  //, Coldef = gridcols, exportCols = lstexportcls
        }
        catch (Exception Ex)
        {
            var err = Ex.Message;
            throw;
        }
    }
}