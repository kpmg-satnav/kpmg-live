﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class FacilityService
{

    SubSonic.StoredProcedure sp;
    public int Save(FacilityModel model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_INSERT_FACILITY");
            sp.Command.AddParameter("@FD_CODE", model.FD_CODE, DbType.String);
            sp.Command.AddParameter("@FD_NAME", model.FD_Name, DbType.String);
            sp.Command.AddParameter("@FD_LOCATION", model.FD_LOCATION, DbType.String);
            sp.Command.AddParameter("@COST", model.COST, DbType.String);
            sp.Command.AddParameter("@FD_STA_ID", model.FD_STA_ID, DbType.String);
            sp.Command.AddParameter("@FD_REM", model.FD_REM, DbType.String);
            sp.Command.Parameters.Add("@FD_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch
        {
            throw;
        }
    }

    public Boolean Update(FacilityModel upd)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "HDM_UPDATE_FACILITY");
            sp.Command.AddParameter("@FD_CODE", upd.FD_CODE, DbType.String);
            sp.Command.AddParameter("FD_NAME", upd.FD_Name, DbType.String);
            sp.Command.AddParameter("@FD_LOCATION", upd.FD_LOCATION, DbType.String);
            sp.Command.AddParameter("@COST", upd.COST, DbType.String);
            sp.Command.AddParameter("FD_STA_ID", upd.FD_STA_ID, DbType.Int32);
            sp.Command.AddParameter("FD_REM", upd.FD_REM, DbType.String);
            sp.Command.Parameters.Add("@FD_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            sp.Execute();
            return true;
        }
        catch
        {
            throw;
        }
    }

    public IEnumerable<FacilityModel> GetFacilityBindGrid()
    {
        try
        {
            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "HDM_GET_ALL_FACILITY_GRID"))
            {
                List<FacilityModel> faclist = new List<FacilityModel>();
                while (dr.Read())
                {
                    faclist.Add(new FacilityModel()
                    {
                        FD_CODE = dr["FD_CODE"].ToString(),
                        FD_Name = dr["FD_Name"].ToString(),
                        FD_LOCATION = dr["FD_LOCATION"].ToString(),
                        COST = float.Parse((dr["COST"]).ToString()),
                        FD_STA_ID = dr["FD_STA_ID"].ToString(),
                        FD_REM = dr["FD_REM"].ToString(),
                        
                    });
                }
                dr.Close();
                return faclist;
            }
        }
        catch
        {
            throw;
        }
    }
}