﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class HDMPopulationClassificationModel
{
    public string PCN_Code { get; set; }
    public string PCN_Name { get; set; }
    public string PCN_Status_Id { get; set; }
    public string PCN_REM { get; set; }
}