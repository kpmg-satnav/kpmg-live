﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class AssetLocationModel
{
    public string ASSETLOC_Code { get; set; }
    public string ASSETLOC_Name { get; set; }
    public string ASSETLOC_Status_Id { get; set; }
    public string ASSETLOC_REM { get; set; }

}