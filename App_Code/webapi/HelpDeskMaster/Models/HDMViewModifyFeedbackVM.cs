﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class GetFDBCKDetails
{
    
    public string LCM_CODE { get; set; }
    public string COMPLAINT_DATE { get; set; }
    public string CLIENTNAME { get; set; }
    public string CONTACT { get; set; }
    public string MAIN_CAT { get; set; }
    public string SUB_CAT { get; set; }
    public string AOM_ID { get; set; }
    public string MAIN_STATUS { get; set; }
    public string MAIN_STATUS_ID { get; set; }
    public string CLINIC_MGR_ID { get; set; }
    public string MODECOMMENTS { get; set; }
    public string MODE_ID { get; set; }
    public string MODE_NAME { get; set; }
    public string STATUSCOMMENTS { get; set; }
    public string SUB_STATUS_ID { get; set; }
    public string SUB_STATUS_NAME { get; set; }
    public List<FileProperties> PostedFiles { get; set; }
    public string RequestId { get; set; }
    public string CLINIC_MGR_NAME { get; set; }
    public string jsonstring { get; set; }
    public string AOM_NAME { get; set; }
    public string OTH_ID { get; set; }
    public string OTHERS_NAME { get; set; }

    public bool ticked { get; set; }
    public string CREATED_BY { get; set; }
    public List<LCMlst> lcmlst { get; set; }
    public List<SUBCATlst> SubLst { get; set; }
    public List<Modes> Modes { get; set; }
    public List<ApprovalStat> StatusList { get; set; }
    public List<ApprovalStat> SubStatusList { get; set; }
}