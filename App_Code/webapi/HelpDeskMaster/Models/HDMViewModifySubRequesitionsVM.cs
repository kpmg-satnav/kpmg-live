﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HDMUtilityVM;

public class FileProperties
{
    public string name { get; set; }
    public string size { get; set; }
    public string type { get; set; }
    public string SaveAs { get; set; }
    public string Length { get; set; }
}

public class ApprovalStat
{
    public string SER_APP_STA_REMARKS_ID { get; set; }
    public string SER_APP_STA_REMARKS_NAME { get; set; }
    public bool ticked { get; set; }
}

public class GridDetails {
    public string REQUESTED_DATE { get; set; }
    public string REQUESTED_BY { get; set; }
    public string SER_UPDATED_DT_SUB1 { get; set; }
    public string SER_UPDATED_BY_SUB1 { get; set; }
    public string ProbDesc { get; set; }
    public string STATUS { get; set; }
}

public class GetDetailsByRequestId
{
    public string AUR_ID { get; set; }
    public string RequestId { get; set; }
    public string STATUS { get; set; }
    public string STA_IS_BTN_ENB { get; set; }
    public string CustID { get; set; }
    public string BillDate { get; set; }
    public string BillNo { get; set; }
    public string InvoiceDate { get; set; }
    public string ROLE_ID { get; set; }
    public string CustName { get; set; }
    public string InvoiceNo { get; set; }
    public string Mobile { get; set; }
    public string BilledBy { get; set; }
    public string ProbDesc { get; set; }
    public string CALL_LOG_BY { get; set; }
    public List<CHILDCATlst> CHILDlst { get; set; }
    public List<LCMlst> LCMlst { get; set; }
    public string FileName { get; set; }
    public List<FileProperties> PostedFiles { get; set; }
    public int length { get; set; }

    public string REQUESTED_BY { get; set; }
    public string REQUESTED_DATE { get; set; }
    public string ASSIGNED_TO { get; set; }
    public string CONTACT_NO { get; set; }
    public string SER_UPDATED_DT_SUB1 { get; set; }
    public string SER_UPDATED_BY_SUB1 { get; set; }
    public string MAIN_CATEGORY { get; set; }
    public string SUB_CATEGORY { get; set; }
    public string CHILD_CATEGORY { get; set; }
    public string SER_LOC_CODE { get; set; }
    public string CHC_TYPE_NAME { get; set; }
    public string jsonstring { get; set; }
    public string Comments { get; set; }
    public string ApprovalStatus { get; set; }
    public List<Locationlst> lcmlst { get; set; }
    public List<ChildCatlst> ChildCatlst { get; set; }
    public List<GridDetails> GridDetails { get; set; }
}
public class FileUploaddetails {
    public string Name { get; set; }
    public string Path { get; set; }
    public string UplTimeName { get; set; }
}


