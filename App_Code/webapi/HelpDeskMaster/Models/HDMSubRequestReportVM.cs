﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMSubRequestReportVM
/// </summary>

public class BindReportDetails
{
    public string REQUEST_ID { get; set; }
    public string REQUESTED_DATE { get; set; }
    public string REQUESTED_BY { get; set; }
    public string LOCATION { get; set; }
    public string MAIN_CATEGORY { get; set; }
    public string SUB_CATEGORY { get; set; }
    public string CHILD_CATEGORY { get; set; }
    public string UPDATED_BY { get; set; }
    public string UPDATED_DT { get; set; }
    public string STA_TITLE { get; set; }
    public string ESC_COUNT { get; set; }
    public string TOTAL_TIME { get; set; }
    public string BILLED_BY { get; set; }
    public string CUST_ID { get; set; }
    public string CUST_NAME { get; set; }
    public string INV_NO { get; set; }
    public string SER_PROB_DESC_SUB1 { get; set; }
    public string UPDATED_COM { get; set; }
    public string Type { get; set; }
}
