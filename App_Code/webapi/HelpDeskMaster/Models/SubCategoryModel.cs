﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SubCategoryModel
/// </summary>
public class SubCategoryModel
{
    public string SUBC_CODE { get; set; }
    public string SUBC_NAME { get; set; }
}