﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMconsolidatedReportVM
/// </summary>
public class HDMReqStatusconsolidatedReportVM
{
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string LCM_ZONE { get; set; }
    public string LCM_CODE { get; set; }
    public string CNP_NAME { get; set; }
    public Int32 Total_Requests { get; set; }
    public Int32 Pending_Requests { get; set; }
    public Int32 In_Progress_Requests { get; set; }
    public Int32 Closed_Requests { get; set; }
    public Int32 Canceled_Requests { get; set; }
    public int FLAG { get; set; }
    public string DocType { get; set; }
    public string HSTSTATUS { get; set; }
}

public class HDMconsolidatedReportGraphVM
{
    public string Status_Type { get; set; }
    public int RequestsCount { get; set; }
}

public class HDMconsolidatedParameters
{
  
    public int CompanyId { get; set; }
}