﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Tower
/// </summary>
public class Tower
{
    public string TWR_CODE { get; set; }
    public string TWR_NAME { get; set; }
    public Boolean ticked { get; set; }
}