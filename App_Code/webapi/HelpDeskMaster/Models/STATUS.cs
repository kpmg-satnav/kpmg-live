﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for STATUS
/// </summary>
public class STATUS
{
    public int STA_ID { get; set; }
    public string STA_MODULE { get; set; }
    public string STA_CODE { get; set; }
    public string STA_DESC { get; set; }
    public string STA_REM { get; set; }
    public Nullable<int> STA_STA_ID { get; set; }
    public Nullable<int> STA_IS_SLA { get; set; }
    public string STA_CREATED_BY { get; set; }
    public Nullable<System.DateTime> STA_CREATED_DT { get; set; }
    public string STA_UPDATED_BY { get; set; }
    public Nullable<System.DateTime> STA_UPDATED_DT { get; set; }
}