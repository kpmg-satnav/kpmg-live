﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMConsolidatedReportVM
/// </summary>
public class HDMConsolidatedReportVM
{
    public string SER_REQ_ID { get; set; }
    public string MNC_NAME { get; set; }
    public string SUBC_NAME { get; set; }
    public string CHC_TYPE_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string LCM_CODE { get; set; }
    
    public string SER_PROB_DESC { get; set; }
    public string REQUESTED_BY_ID { get; set; }
    public string REQUESTED_BY_NAME { get; set; }
    public string SER_CREATED_DT { get; set; }
    public string SER_UPDATED_DT { get; set; }
    public string ASSIGNED_DATE { get; set; }
    public string SERH_ASSIGN_TO { get; set; }
    public string ASSIGN_TO_NAME { get; set; }
    public string CONTACT_NO { get; set; }
    public string TOTAL_TIME { get; set; }
    public string STA_TITLE { get; set; }
    public string LABOUR_COST { get; set; }
    public string SPARE_COST { get; set; }
    public string ADDITIONAL_COST { get; set; }
    public string CLOSED_TIME { get; set; }
    public string DEFINED_TAT { get; set; }   
    public string RESPONSE_TAT { get; set; }   
    public string DELAYED_TAT { get; set; }
    public string FEEBACK { get; set; }
    public string COMMENTS { get; set; }
    public string CNP_NAME { get; set; }
    public string VERTICAL { get; set; }
    public string COSTCENTER { get; set; }
    public string HELPDESK { get; set; }
    public string L1Manager { get; set; }
    public string L2Manager { get; set; }
    public string L3Manager { get; set; }
    public DateTime? Helpdesk_Incharge { get; set; }
    public DateTime? L1_Manager { get; set; }
    public DateTime? L2_Manager { get; set; }
    public DateTime? L3_Manager { get; set; }
    public string ZONE { get; set; }
    public DateTime? SER_CREATED_DT1 { get; set; }
    public DateTime? SER_UPDATED_DT1 { get; set; }
    public DateTime? ASSIGNED_DATE1 { get; set; }
    public DateTime? CLOSED_TIME1 { get; set; }
    public string ser_claim_amt { get; set; }
    public string FeedBack { get; set; }

}