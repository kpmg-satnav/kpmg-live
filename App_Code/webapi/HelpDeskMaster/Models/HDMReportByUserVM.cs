﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMReportByUserVM
/// </summary>
public class HDMReportByUserVM
{
    public string SER_REQ_ID { get; set; }
    public string AUR_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public string MNC_NAME { get; set; }
    public string SUBC_NAME { get; set; }
    public string CHC_TYPE_NAME { get; set; }
    public string NoOfEscalations { get; set; }
    public string DEFINED_TAT { get; set; }
    public string TAT { get; set; }
    public string SLA { get; set; }
    public string TAT_SLA { get; set; }
    public string DELAYED_TIME { get; set; }
    public string DELAYED_SLA { get; set; }
    public string TOTAL { get; set; }
    public string SER_SUB_CAT_CODE { get; set; }
    public string STATUS { get; set; }
    public string SERH_CREATED_DT { get; set; }
    public string SER_CLAIM_AMT { get; set; }
    public string SERH_AMOUNT { get; set; }
    public string SearchValue { get; set; }
    public string OVERALL_COUNT { get; set; }
}
public class InMemoryFile
{
    public string FileName { get; set; }
    public byte[] Content { get; set; }
}
public class FileModel
{
    public string FileName { get; set; }
    public string FilePath { get; set; }
    public bool IsSelected { get; set; }
}
public class HDMReportByUser
{
    public Nullable<DateTime> FromDate { get; set; }
    public Nullable<DateTime> ToDate { get; set; }
    public string DocType { get; set; }
    public string CompanyId { get; set; }
    public string Status { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
    public string CNP_NAME { get; set; }
    public string SearchValue { get; set; }
    public string SEARCHVAL { get; set; }
    //public string Rejected { get; set; }


}
public class HDMReport_Req_History
{
    public string SERH_SER_ID { get; set; }
    public string SERH_STA_TITLE { get; set; }
    public string SERH_COMMENTS { get; set; }
    public Nullable<DateTime> CREATEDON { get; set; }
    public string CREATEDDATE { get; set; }
    public string CREATEDBY { get; set; }
}
public class HDMReport_Bar_Graph
{
    public int REQ_COUNT { get; set; }
    public string LCM_NAME { get; set; }
}
public class LocationwiseCount
{
    public List<HDMReportByUserVM> griddata { get; set; }
    public List<HDMReport_Bar_Graph> graphdata { get; set; }
}

public class HDMISFCReportByUserVM
{
    public string SER_REQ_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public int TOTAL_AMOUNT { get; set; }
    public string MNC_NAME { get; set; }
    public string SUBC_NAME { get; set; }
    public string CHC_TYPE_NAME { get; set; }
    public string NoOfEscalations { get; set; }
    public string DEFINED_TAT { get; set; }
    public string TAT { get; set; }
    public string SLA { get; set; }
    public string TAT_SLA { get; set; }
    public string DELAYED_TIME { get; set; }
    public string DELAYED_SLA { get; set; }
    public string TOTAL { get; set; }
    public string SER_SUB_CAT_CODE { get; set; }
    public string STATUS { get; set; }
    public string NO_OF_APPROVALS { get; set; }
    public string L1_Approved { get; set; }
    public string L1_COMMENTS { get; set; }
    public string L1_APPR_DT { get; set; }
    public string L2_Approved { get; set; }
    public string L2_COMMENTS { get; set; }
    public string L2_APPR_DT { get; set; }
    public string L3_Approved { get; set; }
    public string L3_COMMENTS { get; set; }
    public string L3_APPR_DT { get; set; }
    public string L4_Approved { get; set; }
    public string L4_COMMENTS { get; set; }
    public string L4_APPR_DT { get; set; }
    public string L5_Approved { get; set; }
    public string L5_COMMENTS { get; set; }
    public string L5_APPR_DT { get; set; }
    public string L6_Approved { get; set; }
    public string L6_COMMENTS { get; set; }
    public string L6_APPR_DT { get; set; }
    public string L7_Approved { get; set; }
    public string L7_COMMENTS { get; set; }
    public string L7_APPR_DT { get; set; }
    public string L8_Approved { get; set; }
    public string L8_COMMENTS { get; set; }
    public string L8_APPR_DT { get; set; }
    public string L9_Approved { get; set; }
    public string L9_COMMENTS { get; set; }
    public string L9_APPR_DT { get; set; }
    public string L10_Approved { get; set; }
    public string L10_COMMENTS { get; set; }
    public string L10_APPR_DT { get; set; }
    public string SER_PROB_DESC { get; set; }
    public string SER_UPDATED_DT { get; set; }
    public string SER_CLAIM_AMT { get; set; }
    public string SER_APPR_AMOUNT { get; set; }
    public string HRU_UPL_PATH { get; set; }

}
public class GetRequestTypes
{
    public string SCODE { get; set; }
    public string SNAME { get; set; }
    public bool ticked { get; set; }
}