﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class HDMReportByCategory
{
    public string MAIN_CATEGORY { get; set; }
    public string SUB_CATEGORY { get; set; }
    public string CHILD_CATEGORY { get; set; }
    public string LOCATION { get; set; }
    public string REQUESTED_BY_ID { get; set; }
    public string REQUESTED_BY_NAME { get; set; }
    public string REQUESTED_DATE { get; set; }

    public string SER_PROB_DESC { get; set; }
    public string ASSIGNED_TO { get; set; }
    public string ASSIGNED_TO_NAME { get; set; }
    public string CONTACT_NO { get; set; }
    public string REQ_STATUS { get; set; }
    public string TOTAL_TIME { get; set; }
    public string CLOSED_TIME { get; set; }
    public string DEFINED_TAT { get; set; }
    public string SER_REQ_ID { get; set; }
    //public string TAT { get; set; }
    public string DELAYED_TIME { get; set; }
    public string OVERALL_COUNT { get; set; }


}

public class HDMReportByCatParams
{
    public Nullable<DateTime> FromDate { get; set; }
    public Nullable<DateTime> ToDate { get; set; }
    public string DocType { get; set; }
    public string CompanyId { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
    public string CNP_NAME { get; set; }
    public string SEARCHVAL { get; set; }
}