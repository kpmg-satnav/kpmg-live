using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//public class GetEmployeeList
//{
//    public List<GetEmployeeData> EMPHRMSlist { get; set; }

//}
    public class GetEmployeeData
{
    public string EmployeeID { set; get; }
    public string EmployeeName { set; get; }
    public string EmpEmail { set; get; }
    public string Gender { set; get; }
    public string Grade { set; get; }
    public string Level { set; get; }
    public string Designation { set; get; }
    public string MobileNo { set; get; }
    public string FunctionName { set; get; }
    public string SubDepartment { set; get; }
    public string Department { set; get; }
    public string DepartmentCostCenter { set; get; }
    public string FunctionCostCenter { set; get; }
    public string City { set; get; }
    public string State { set; get; }
    public string Location { set; get; }
    public string LocationCostCenter { set; get; }
    public string Category { set; get; }
    public DateTime JoiningDate { set; get; }
    public string EmployeeType { set; get; }
    public string Status { set; get; }
    public DateTime DateofResign { set; get; }
    public DateTime LastWorkingDate { set; get; }
    public DateTime EffectiveDate { set; get; }
    public string RAECode { set; get; }
    public string HODECode { set; get; }
    public string NHODECode { set; get; }    
    public string WorkLocationType { set; get; }



}
