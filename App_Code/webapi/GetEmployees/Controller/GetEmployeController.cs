
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using Newtonsoft.Json.Linq;

namespace BasicAuthentication.Controllers
{
    public class GetEmployeController : ApiController
    {
        GetEmployeService Agnrex = new GetEmployeService();

        [BasicAuthentication]
        [HttpPost]

        //public async Task GetBulkEmployeeData(HttpRequestMessage request)
        //{
        //    var jObject = await request.Content.ReadAsAsync<JObject>();

        //    GetEmployeeList sample = JsonConvert.DeserializeObject<GetEmployeeList>(jObject.ToString());
        //    var obj = Agnrex.GetEmployees(sample);
        //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        //}

        public HttpResponseMessage GetBulkEmployeeData([FromBody] List<GetEmployeeData> getEmployeeData)
        {
            var obj = Agnrex.GetEmployees(getEmployeeData);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
            return response;
        }



    }
}