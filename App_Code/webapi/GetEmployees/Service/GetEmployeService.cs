using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ContractMasterService
/// </summary>
public class GetEmployeService
{
    SubSonic.StoredProcedure sp;


    public object GetEmployees(List<GetEmployeeData> getEmployeeData)
    {
        System.Web.HttpContext.Current.Session["useroffset"] = "+05:30";
        System.Web.HttpContext.Current.Session["TENANT"] = "Bajaj.dbo";
        try
        {
            
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@HRMSLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(getEmployeeData);

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_HRMS_DATA_BAJAJ", param);
            return new { Message = "Data Uploaded Successfully" };

            //ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_HRMS_DATA_BAJAJ", param);
            //return new { data = ds.Tables[0] };
            //param[0] = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            //param[0].Value = GetEmployees.EmployeeID;
            //param[1] = new SqlParameter("@EmployeeName", SqlDbType.VarChar);
            //param[1].Value = GetEmployees.EmployeeName;
            //param[2] = new SqlParameter("@EmpEmail", SqlDbType.VarChar);
            //param[2].Value = GetEmployees.EmpEmail;
            //param[3] = new SqlParameter("@Gender", SqlDbType.VarChar);
            //param[3].Value = GetEmployees.Gender;
            //param[4] = new SqlParameter("@Grade", SqlDbType.VarChar);
            //param[4].Value = GetEmployees.Grade;
            //param[5] = new SqlParameter("@Level", SqlDbType.VarChar);
            //param[5].Value = GetEmployees.Level;
            //param[6] = new SqlParameter("@Designation", SqlDbType.VarChar);
            //param[6].Value = GetEmployees.Designation;
            //param[7] = new SqlParameter("@MobileNo", SqlDbType.VarChar);
            //param[7].Value = GetEmployees.MobileNo;
            //param[8] = new SqlParameter("@Function", SqlDbType.VarChar);
            //param[8].Value = GetEmployees.FunctionName;
            //param[9] = new SqlParameter("@SubDepartment", SqlDbType.VarChar);
            //param[9].Value = GetEmployees.SubDepartment;
            //param[10] = new SqlParameter("@Department", SqlDbType.VarChar);
            //param[10].Value = GetEmployees.Department;
            //param[11] = new SqlParameter("@DepartmentCostCenter", SqlDbType.VarChar);
            //param[11].Value = GetEmployees.DepartmentCostCenter;
            //param[12] = new SqlParameter("@FunctionCostCenter", SqlDbType.VarChar);
            //param[12].Value = GetEmployees.FunctionCostCenter;
            //param[13] = new SqlParameter("@City", SqlDbType.VarChar);
            //param[13].Value = GetEmployees.City;
            //param[14] = new SqlParameter("@State", SqlDbType.VarChar);
            //param[14].Value = GetEmployees.State;
            //param[15] = new SqlParameter("@Location", SqlDbType.VarChar);
            //param[15].Value = GetEmployees.Location;
            //param[16] = new SqlParameter("@LocationCostCenter", SqlDbType.VarChar);
            //param[16].Value = GetEmployees.LocationCostCenter;
            //param[17] = new SqlParameter("@Category", SqlDbType.VarChar);
            //param[17].Value = GetEmployees.Category;
            //param[18] = new SqlParameter("@JoiningDate", SqlDbType.DateTime);
            //param[18].Value = GetEmployees.JoiningDate;
            //param[19] = new SqlParameter("@EmployeeType", SqlDbType.VarChar);
            //param[19].Value = GetEmployees.EmployeeType;
            //param[20] = new SqlParameter("@Status", SqlDbType.VarChar);
            //param[20].Value = GetEmployees.Status;
            //param[21] = new SqlParameter("@DateofResign", SqlDbType.DateTime);
            //param[21].Value = GetEmployees.DateofResign;
            //param[22] = new SqlParameter("@LastWorkingDate", SqlDbType.DateTime);
            //param[22].Value = GetEmployees.LastWorkingDate;
            //param[23] = new SqlParameter("@EffectiveDate", SqlDbType.DateTime);
            //param[23].Value = GetEmployees.EffectiveDate;
            //param[24] = new SqlParameter("@RAECode", SqlDbType.VarChar);
            //param[24].Value = GetEmployees.RAECode;
            //param[25] = new SqlParameter("@HODECode", SqlDbType.VarChar);
            //param[25].Value = GetEmployees.HODECode;
            //param[26] = new SqlParameter("@NHODECode", SqlDbType.VarChar);
            //param[26].Value = GetEmployees.NHODECode;



        }
        catch (Exception Ex)
        {

            return new { Message = "Invalid Input! Please try again", data = (object)null, ErrorMessage = Ex.InnerException };
        }
    }

    
}