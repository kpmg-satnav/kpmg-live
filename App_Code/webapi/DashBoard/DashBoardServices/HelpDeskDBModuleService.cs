﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for HelpDeskDBModuleService
/// </summary>
public class HelpDeskDBModuleService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    //public object GetHDreqWeekly(int CmpId)
    //{
    //    sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_HD_REQ_WEEKLY_DASHBOARD");
    //    sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
    //    sp.Command.AddParameter("@CMP_ID", CmpId, DbType.Int32);
    //    ds = sp.GetDataSet();

    //    //HD Requestsed Dates        
    //    List<object> ReqDates = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (r.ItemArray[1])).ToList();
    //    ReqDates.Insert(0, "x");
    //    //No of requests raised/day count
    //    List<object> ReqCount = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (string.IsNullOrEmpty(r.ItemArray[0].ToString()) ? 0 : r.ItemArray[0])).ToList();
    //    ReqCount.Insert(0, "Date");

    //    List<Object> list = new List<object>();
    //    list.Add(ReqDates);
    //    list.Add(ReqCount);
    //    return list;
    //}

    public object GetHDreqWeekly(int CmpId)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_HD_REQ_WEEKLY_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@CMP_ID", CmpId, DbType.Int32);
        ds = sp.GetDataSet();

        List<object> ReqDate = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("RAISEDDATE") != null)).Select(r => r.ItemArray[1]).Distinct().ToList();
        List<object> parentlist = new List<object>();
        parentlist.Add(ReqDate);
        List<object> distStatus = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (r.ItemArray[0])).Distinct().ToList();
        List<object> list = new List<object>();

        foreach (string sts in distStatus)
        {
            list.Add(sts);
            foreach (string reqD in ReqDate)
            {
                var reqcount = ds.Tables[0].Rows.Cast<DataRow>().Where(x => x.Field<string>("RAISEDDATE") == reqD && x.Field<string>("STA_TITLE") == sts).Select(x => (x.Field<int>("TOTAL"))).FirstOrDefault();
                list.Add(reqcount == null ? 0 : reqcount);
            }
            parentlist.Add(list.ToList());
            list.Clear();
        }
        ReqDate.Insert(0, "x");
        return parentlist;
    }

    public HelpDeskDBModel.SLADetails GetSLACount(int CmpId)
    {
        HelpDeskDBModel.SLADetails obj = new HelpDeskDBModel.SLADetails();
        //string[] str = new string[2];
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_HELPDESK_STS_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@CMP_ID", CmpId, DbType.Int32);
        ds = sp.GetDataSet();

        obj.SLACount = Convert.ToInt32(ds.Tables[6].Rows[0]["TOTAL"] == "" ? 0 : ds.Tables[6].Rows[0]["TOTAL"]);
        decimal tot = Convert.ToDecimal(ds.Tables[0].Rows[0]["REQUESTS RAISED"]);// + Convert.ToDecimal(ds.Tables[1].Rows[0]["PENDING"]) + Convert.ToDecimal(ds.Tables[2].Rows[0]["INPROGRESS"]);
        decimal divby = Convert.ToDecimal(ds.Tables[3].Rows[0]["CLOSED"]);
        tot = tot == 0 ? 1 : tot;
        obj.SLAratio = Convert.ToDecimal(Math.Round((divby / tot) * 100, 2));
        return obj;
    }

    public static dynamic GetDynamicObject(Dictionary<string, object> properties)
    {
        var dynamicObject = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
        foreach (var property in properties)
        {
            dynamicObject.Add(property.Key, property.Value);
        }
        return dynamicObject;
    }

    public object BindHDCategories(int CmpId)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_HD_CATEGORIES_STATUS_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@CMP_ID", CmpId, DbType.Int32);
        ds = sp.GetDataSet();

        List<object> locs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[2]).Distinct().ToList();
        List<dynamic> list = new List<dynamic>();

        foreach (string location in locs)
        {
            Dictionary<String, Object> properties = new Dictionary<string, object>();
            properties.Add("name", location);

            var strCategory = ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("LCM_NAME") == location))
                       .Select(r => new { ServiceName = r.Field<string>("SER_NAME"), PendingCount = r.Field<int>("PENDING") }).ToList();
            for (int i = 0; i < strCategory.Count; i++)
            {
                properties.Add(strCategory[i].ServiceName, strCategory[i].PendingCount);
            }
            dynamic obj = GetDynamicObject(properties);
            list.Add(obj);
        }     

        List<object> serviceslist = ds.Tables[1].Rows.Cast<DataRow>().Select(r => (r.ItemArray[0])).ToList();
        //  return new { shift = listParent, ttlshift = totalcount };
        return new { locVal = Newtonsoft.Json.JsonConvert.SerializeObject(list), services = Newtonsoft.Json.JsonConvert.SerializeObject(serviceslist) };       
    }

    //details
    public object GetRequestDetails(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_HELPDESK_REQUESTDETAILS");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@CMP_ID", company, DbType.Int32);
        ds = sp.GetDataSet();
        return ds;
        //return new { ChartData = new dynamic[] { new { TotalReq = ds.Tables[0].Rows[0]["Total"] }, new { PendingReq = ds.Tables[0].Rows[1]["Total"] }, new { WRExpenses = ds.Tables[0].Rows[2]["Total"] } } };
    }

    public DataTable GetUserReqs_StatusWise(string statusId)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_HDM_USER_REQS_BYSTATUS");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@STATUS", statusId, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetRequestDetailsbySelectedServiceAndLocation(string location, string category, int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_HELPDESK_REQUESTDETAILS_BY_LOCATION_SERVICE_CATEGORY");
        sp.Command.AddParameter("@CATEGORY", category, DbType.String);
        sp.Command.AddParameter("@Location_name", location, DbType.String);
        sp.Command.AddParameter("@ADMIN_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@CMP_ID", company, DbType.Int32);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable BindSupport()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "[Support_Customer_Matrix]");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
}