﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AssetDBModuleService
/// </summary>
public class AssetDBModuleService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object[] GetAssetTypes(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_TOTAL_ASSETS_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@companyid", company);
        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();        
        return arr;
    }

    public object[] GetMapunmapassets(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_TOTAL_ASSETS_BYLOCATION_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@companyid", company);
        ds = sp.GetDataSet();
        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return arr;
    }

    public DataSet GetMap_umMapAssets_DB(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_TOTAL_ASSETS_BYLOCATION_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@companyid", company);
        ds = sp.GetDataSet();
        return ds;
    }

    public DataTable GetAssetType_Categories(int company,string category)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_ASSET_TYPESLIST_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@CAT_CODE", category, DbType.String);
        sp.Command.AddParameter("@companyid", company);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    public object GetTotalAssets_by_Sub_category(DateTime From_Date, DateTime To_Date)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_TOTAL_COUNT_BY_SUB_CAT_DASHBOARD");

            sp.Command.AddParameter("@FRM_DATE", From_Date, DbType.DateTime);
            sp.Command.AddParameter("@TO_DATE", To_Date, DbType.DateTime);
            ds = sp.GetDataSet();

            List<object> Total = new List<object>();
            List<object> CONSUMED = new List<object>();
            List<object> Available = new List<object>();
            List<object> Subcat = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[0]).Distinct().ToList();

            // List<object> Dynamiccols = new List<object>();
            // var random = new Random();

            foreach (var i in Subcat)
            {
                Total.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("SUBCATEGORY") == i)).Sum(x => int.Parse(x.Field<string>("TOTAL"))));
                CONSUMED.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("SUBCATEGORY") == i)).Sum(x => (x.Field<int>("CONSUMED"))));
                Available.Add(ds.Tables[0].AsEnumerable().Where(x => (x.Field<string>("SUBCATEGORY") == i)).Sum(x => (x.Field<int>("Available"))));

            }
            Subcat.Insert(0, "x");
            Total.Insert(0, "Total Assets");
            CONSUMED.Insert(0, "Consumed Assets");
            Available.Insert(0, "Available Assets");
            return new { Subcat = Subcat, Total = Total, CONSUMED = CONSUMED, Available = Available };
        }
        catch (Exception ex)
        {
            return new { data = (object)null };
        }
    }
}