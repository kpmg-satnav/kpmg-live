﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ConferenceDBModuleService
/// </summary>
public class ConferenceDBModuleService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    public object[] GetcitywiseconfCount(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_CONF_COUNT_CTY_DASHBOARD");
        sp.Command.AddParameter("@companyid", company);
        ds = sp.GetDataSet();

        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return arr;
    }


    public object[] GetcitywiseUtil(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_CONF_UTILIZATION_DASHBOARD");
        sp.Command.AddParameter("@companyid", company);
        ds = sp.GetDataSet();

        object[] arr = ds.Tables[1].Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray();
        return arr;
    }

    public object GetConfUtil(int company)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_CONF_UTILIZATION_DASHBOARD");
        sp.Command.AddParameter("@companyid", company);
        ds = sp.GetDataSet();

        List<object> Bookeddates = ds.Tables[0].Rows.Cast<DataRow>().Where(x => (x.Field<string>("SSA_FROM_DATE") != null)).Select(r => r.ItemArray[1]).Distinct().ToList();
        List<object> parentlist = new List<object>();
        parentlist.Add(Bookeddates);
        List<object> distconfs = ds.Tables[0].Rows.Cast<DataRow>().Select(r => (r.ItemArray[0])).Distinct().ToList();
        List<object> list = new List<object>();

        foreach (string conf in distconfs)
        {            
            list.Add(conf);
            foreach (string Bkdate in Bookeddates)
            {
                var hrs = ds.Tables[0].Rows.Cast<DataRow>().Where(x => x.Field<string>("SSA_FROM_DATE") == Bkdate && x.Field<string>("CONFERENCE_ROOM_NAME") == conf).Select(x => (x.Field<int>("TOTAL"))).FirstOrDefault();                
                //list.Add(Bkdate);
                list.Add(hrs == null ? 0 : hrs);
            }
            parentlist.Add(list.ToList());
            list.Clear();
        }
        Bookeddates.Insert(0,"x");        
        return parentlist;
    }
}