﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GuestHouseDBModuleService
/// </summary>
public class GuestHouseDBModuleService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    public object[] GetLocationwiseFacilitiesCount(int companyid)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GH_GET_FACILITIES_COUNT_BY_LOCATION_DASHBOARD");
        sp.Command.AddParameter("@companyid", companyid);
        ds = sp.GetDataSet();

        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
        return arr;
    }


    public object[] GetTotalReqCount(int companyid)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_TOTAL_REQUESTS_COUNT_GH_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@companyid", companyid);
        ds = sp.GetDataSet();

        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray();
        return arr;
    }


    public DataSet GetTotalReqDetails(int companyid)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_GH_REQUESTS_DETAILS_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@companyid", companyid);
        ds = sp.GetDataSet();

        return ds;
    }

    public object[] GetWithHoldCount(int companyid)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_WITHHOLD_REQUESTS_COUNT_GH_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@companyid", companyid);
        ds = sp.GetDataSet();

        object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray();
        return arr;
    }

    public DataSet GetWithHoldDetails(int companyid)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_GH_WITHHOLD_REQUESTS_DETAILS_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@companyid", companyid);
        ds = sp.GetDataSet();

        
        return ds;
    }
    public static dynamic GetDynamicObject(Dictionary<string, object> properties)
    {
        var dynamicObject = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
        foreach (var property in properties)
        {
            dynamicObject.Add(property.Key, property.Value);
        }
        return dynamicObject;
    }
    public object GetGHUtilization(int companyid)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_GH_UTILIZATION_DASHBOARD");
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@companyid", companyid);
        ds = sp.GetDataSet();
        List<object> serviceslist = ds.Tables[1].Rows.Cast<DataRow>().Select(r => (r.ItemArray[0])).ToList();
        return new { locVal = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]), services = Newtonsoft.Json.JsonConvert.SerializeObject(serviceslist) };  
    }


    public DataTable GetFaciilitiesBylcmName(string lcmName, int companyid)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_FACILITIES_GH_PIE_CHART");
        sp.Command.AddParameter("@LCM_NAME", lcmName, DbType.String);
        sp.Command.AddParameter("@companyid", companyid);
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["uid"]);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public DataTable GetRequestDetailsforBarGraph( string category, string location, int companyid)
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_BOOKED_DETIAILS_FOR_BAR_CHART");
        sp.Command.AddParameter("@BOOKED_TYPE", category, DbType.String);
        sp.Command.AddParameter("@LCM_NAME", location, DbType.String);  
        sp.Command.AddParameter("@AUR_ID", Convert.ToString(HttpContext.Current.Session["uid"]), DbType.String);
        sp.Command.AddParameter("@companyid", companyid);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

}