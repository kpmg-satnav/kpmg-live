﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class GuestHouseDBAPIController : ApiController
{
    GuestHouseDBModuleService service = new GuestHouseDBModuleService();

    [HttpGet]
    public object GetFacilitiesCountbyLocation([FromUri] int companyid)
    {
        return service.GetLocationwiseFacilitiesCount(companyid);
    }

    [HttpGet]
    public object GetTotalRequestsCount([FromUri] int companyid)
    {
        return service.GetTotalReqCount(companyid);
    }
    [HttpGet]
    public object GetTotalRequestsDetails([FromUri] int companyid)
    {
        return service.GetTotalReqDetails(companyid);
    }
    [HttpGet]
    public object GetWithHoldRequestsCount([FromUri] int companyid)
    {
        return service.GetWithHoldCount(companyid);
    }
    [HttpGet]
    public object GetWithHoldRequestsDetails([FromUri] int companyid)
    {
        return service.GetWithHoldDetails(companyid);
    }
    [HttpGet]
    public object GHUtilization([FromUri] int companyid)
    {
        return service.GetGHUtilization(companyid);
    }

    [HttpGet]
    public HttpResponseMessage GetFacilitiesforPie([FromUri] string lcmname, [FromUri] int companyid)
    {
        var obj = service.GetFaciilitiesBylcmName(lcmname, companyid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetGHDetailsForBarChart([FromUri] string category, [FromUri] string location, [FromUri] int companyid)
    {
        var obj = service.GetRequestDetailsforBarGraph(category, location, companyid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
