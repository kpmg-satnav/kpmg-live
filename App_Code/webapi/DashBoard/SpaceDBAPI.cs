﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using UtiltiyVM;

public class SpaceDBAPIController : ApiController
{
    SpaceDBModuleService service = new SpaceDBModuleService();

    public class company
    {
        public int companyid { get; set; }
        public string Vertical { get; set; }
    }

    //bindWorkstation
    [HttpPost]
    public object bindWorkstation([FromBody] company c)
    {
        return service.bindWorkstation(c.companyid);
    }

    //Bind Cabins
    [HttpPost]
    public object bindCabin([FromBody] company c)
    {
        return service.bindCabin(c.companyid);
    }

    //Bind Occupied Employees
    [HttpPost]
    public object bindFTE([FromBody] company c)
    {
        return service.bindFTE(c.companyid);
    }

    //Bind Seat Utlization
    [HttpPost]
    public object bindShift([FromBody] company c)
    {
        return service.bindShift(c.companyid);
    }

    [HttpPost]
    public object bindDepartmentCount([FromBody] company c)
    {
        var obj = service.bindDepartmentCount(c.companyid, c.Vertical);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage getVerticals()
    {
        var obj = service.GetVerticals();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage getLocations()
    {
        var obj = service.getLocations();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage getCities()
    {
        var obj = service.getCities();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage getCostCenters()
    {
        var obj = service.getCostCenters();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public object Get_Space_Utilization([FromBody] ParamDetails selval)
    {
        var obj = service.Get_Space_Utilization(selval.COMPANY, selval.VALUE, selval.TYPE, selval.FLAG,selval.citylist,selval.LocationLst,selval.VerticalLst,selval.CostCenterLst);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public object Get_City_Wise_UnusedArea([FromBody] ParamDetails selval)
    {
        var obj = service.Get_City_Wise_UnusedArea(selval.COMPANY, selval.VALUE, selval.TYPE, selval.FLAG);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public object Get_Vacant_Allocate_Space_Zonewise([FromBody] ParamDetails selval)
    {
        var y = selval.COMPANY;
        var obj = service.Get_Vacant_Allocate_Space_Zonewise();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public object Get_Vertical_Wise_Allocated_But_Not_Occupied([FromBody] ParamDetails selval)
    {
        var obj = service.Get_Vertical_Wise_Allocated_But_Not_Occupied(selval.COMPANY, selval.VALUE, selval.TYPE, selval.FLAG);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public object Get_Space_Release_Details(int id)
    {
        var obj = service.Get_Space_Release_Details(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public object Get_Space_Release_Details_Onclick([FromBody] Getdetails selval)
    {
        var obj = service.Get_Space_Release_Details_Onclick(selval.companyid, selval.value);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public object Get_Space_Unused_Details_Onclick([FromBody] Getdetails code)
    {
        var obj = service.Get_Space_Unused_Details_Onclick(code.selectedval, code.TYPE, code.PageNumber, code.PageSize, code._Excel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public object Get_City_Wise_Ver_Onclick([FromBody] Getdetails code)
    {
        var obj = service.Get_City_Wise_Ver_Onclick(code.selectedval, code.TYPE, code.PageNumber, code.PageSize, code._Excel);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public async Task<HttpResponseMessage> GetDashboardExceldata(Getdetails obj)
    {
        ReportGenerator<GetExceletails> reportgen = new ReportGenerator<GetExceletails>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceDashboardReport.rdlc"),
            DataSetName = "DashboardExcelDS",
            ReportType = "Space Dashboard Report"
        };
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceDashboardReport.xlsx");
        List<GetExceletails> reportdata = service.GetExcelData(obj.selectedval, obj.TYPE, obj.PageNumber, obj.PageSize, obj._Excel, obj.flag);
        await reportgen.GenerateReport(reportdata, filePath, "xlsx");
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceDashboardReport.xlsx";
        return result;
    }


    public class Getdetails
    {
        public string AUR_ID { get; set; }
        public string selectedval { get; set; }
        public string companyid { get; set; }
        public int value { get; set; }
        public string Ver_code { get; set; }
        public string Cty_code { get; set; }
        public string TYPE { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string _Excel { get; set; }
        public int flag { get; set; }
    }

    public class ParamDetails
    {
        public string USER_ID { get; set; }
        public string COMPANY { get; set; }
        public string TYPE { get; set; }
        public int VALUE { get; set; }
        public int FLAG { get; set; }
        public string citylist { get; set; }
        public string LocationLst { get; set; }
        public string VerticalLst { get; set; }
        public string CostCenterLst { get; set; }
    }

}
