﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UtiltiyVM
/// </summary>
namespace UtiltiyVM
{
    public class SysPreference
    {
        public string SYSP_CODE { get; set; }
        public string SYSP_VAL1 { get; set; }
        public string SYSP_VAL2 { get; set; }
        public string SYSP_OPR { get; set; }
        public int SYSP_STA_ID { get; set; }
    }

    public class RoleId
    {
        public string ROL_ID { get; set; }
        public string ROL_DESCRIPTION { get; set; }
        public string URL_USR_ID { get; set; }
      
    }
    public class GetRoleAndRM
    {
        public string AUR_ROLE { get; set; }
        public string AUR_REPOTING_TO { get; set; }
        public string AUR_ID { get; set; }
        public string AUR_KNOWN_AS { get; set; }
        public string VER_CODE { get; set; }
        public string VER_NAME { get; set; }
        public string COST_CENTER_CODE { get; set; }
        public string COST_CENTER_NAME { get; set; }
    }
    public class Aurlst
    {
        public string AUR_ID { get; set; }
        public string AUR_KNOWN_AS { get; set; }
        public string AUR_DES_CODE { get; set; }
        public string AUR_DES_NAME { get; set; }        
        public string ALLOC_SPC_ID { get; set; }
        public bool ticked { get; set; }
        public string AUR_KNOWN_AS_NAME { get; set; }
    }

    public class Type_Data
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
    }

    public class Countrylst
    {
        public string CNY_CODE { get; set; }
        public string CNY_NAME { get; set; }
        public bool ticked { get; set; }
    }

    public class Citylst
    {
        public string CTY_CODE { get; set; }
        public string CTY_NAME { get; set; }
        public string CNY_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class Locationlst
    {
        public string LCM_CODE { get; set; }
        public string LCM_NAME { get; set; }
        public string CTY_CODE { get; set; }
        public string CNY_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class Towerlst
    {
        public string TWR_CODE { get; set; }
        public string TWR_NAME { get; set; }
        public string LCM_CODE { get; set; }
        public string CTY_CODE { get; set; }
        public string CNY_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class Floorlst
    {
        public string FLR_CODE { get; set; }
        public string FLR_NAME { get; set; }
        public string TWR_CODE { get; set; }
        public string LCM_CODE { get; set; }
        public string CTY_CODE { get; set; }
        public string CNY_CODE { get; set; }
        public bool ticked { get; set; }
    }
    public class Verticallst
    {
        public string VER_CODE { get; set; }
        public string VER_NAME { get; set; }
        public bool ticked { get; set; }
    }

    public class Costcenterlst
    {
        public string Cost_Center_Code { get; set; }
        public string Cost_Center_Name { get; set; }
        public string Vertical_Code { get; set; }
        public bool ticked { get; set; }
    }

    public class Shiftlst
    {
        public string SH_CODE { get; set; }
        public string SH_NAME { get; set; }
        public string SH_LOC_ID { get; set; }
        public string SH_SEAT_TYPE { get; set; }
        public string REP_COL { get; set; }
        public bool ticked { get; set; }
    }
    public class ParentEntityLst
    {
        public string PE_CODE { get; set; }
        public string PE_NAME { get; set; }
        public bool ticked { get; set; }
    }
    public class ChildEntityLst
    {
        public string CHE_CODE { get; set; }
        public string CHE_NAME { get; set; }
        public string PE_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class Department 
    {
        public string DEP_CODE { get; set; }
        public string DEP_NAME { get; set; }
        public bool ticked { get; set; }
    }

    public class Designation
    {
        public string DSG_CODE { get; set; }
        public string DSG_NAME { get; set; }
        public bool ticked { get; set; }
    }

    public class Rolelst
    {
        public string ROL_ID { get; set; }
        public string ROL_ACRONYM { get; set; }
        public string ROL_DESCRIPTION { get; set; }
        public bool isChecked { get; set; }
    }

    [BindableType(IsBindable = false)]
    [Flags]
    public enum RequestState
    {
        Canceled = 1,
        Unchanged = 2,
        Added = 4,
        Deleted = 8,
        Modified = 16,
        Approved = 32,
        Rejected = 64,
        Blocked = 128,
        closed = 256
    }

    public class Types
    {
        public string VT_CODE { get; set; }
        public string VT_TYPE { get; set; }
        public bool ticked { get; set; }
    }

    public class SubTypes
    {
        public string AST_SUBCAT_CODE { get; set; }
        public string AST_SUBCAT_NAME { get; set; }
        public string CAT_CODE { get; set; }
        public bool ticked { get; set; }
    }
    public class GetEMPID
    {
        public string AUR_ID { get; set; }
        public string AUR_NAME { get; set; }
        public string AUR_PRJ_CODE { get; set; }
        public string AUR_VERT_CODE { get; set; }
        public bool ticked { get; set; }
    }
    public class GetBrands
    {
        public string MANUFACTUER_CODE { get; set; }
        public string MANUFACTURER { get; set; }
        public string VT_TYPE { get; set; }
        public bool ticked { get; set; }
    }

    public class GetModel
    {
        public string AST_MD_CODE { get; set; }
        public string AST_MD_NAME { get; set; }
        public bool ticked { get; set; }
    }
    //property reports

    public class GetPropertyType
    {
        public string PN_TYPEID { get; set; }
        public string PN_PROPERTYTYPE { get; set; }
    }
    public class GetPropertyName
    {
        public string PM_PPT_TYPE { get; set; }
        public string PM_PPT_CODE { get; set; }
        public string PM_PPT_NAME { get; set; }
        public bool ticked { get; set; }
    }



    public class GetCompanies
    {
        public Int32 CNP_ID { get; set; }
        public string CNP_NAME { get; set; }
        public bool ticked { get; set; }
    }

    // guest house booking

    public class ReservationType
    {
        public string RT_SNO { get; set; }
        public string RT_NAME { get; set; }
        public bool ticked { get; set; }
    }

    public class EmployeeNameID
    {
        public string AUR_ID { get; set; }
        public string NAME { get; set; }
        public string Vertical { get; set; }
        public string Costcenter { get; set; }
        public bool ticked { get; set; }
    }

    public class FacilityNamelst
    {
        public string RF_NAME { get; set; }
        public string RF_SNO { get; set; }
        public string RT_SNO { get; set; }
        public bool ticked { get; set; }
        public string TVP_CNY_CODE { get; set; }
        public string TVP_CTY_CODE { get; set; }
        public string TVP_LCM_CODE { get; set; }
    }

    public class EmployeeType
    {
        public string ET_SNO { get; set; }
        public string ET_NAME { get; set; }
    }

    /////SPACE_TYPE DATA

    public class Space_Type
    {
        public string SPC_TYPE_CODE { get; set; }
        public string SPC_TYPE_NAME { get; set; }
    }

    public class GetExceletails
    {
        public string SPC_ID { get; set; }
        public string FLR_NAME { get; set; }
        public string TWR_NAME { get; set; }
        public string LCM_NAME { get; set; }
        public string CTY_NAME { get; set; }
        public string SPC_TYPE { get; set; }
        public string VER_NAME { get; set; }
        public string COST_NAME { get; set; }
    }

    public class Geographylst
    {
        public string GRY_CODE { get; set; }
        public string GRY_NAME { get; set; }
        public bool ticked { get; set; }
    }

    public class Functionlst
    {
        public string Fun_CODE { get; set; }
        public string Fun_NAME { get; set; }
        public bool ticked { get; set; }
    }
    public class vlanlist
    {
        public string VL_Code { get; set; }
        public string VL_Name { get; set; }
       
    }

    public class Distancelist
    {
        public int type{ get; set; }
        public string Floorlist { get; set; }
    }

}