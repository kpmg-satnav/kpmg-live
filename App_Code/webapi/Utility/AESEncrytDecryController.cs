﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

/// <summary>
/// Summary description for AESEncrytDecryController
/// </summary>
/// 

public class AESEncrytDecryController: ApiController
{
    AESEncrytDecryService service = new AESEncrytDecryService();
    [HttpPost]
    public HttpResponseMessage Encrypt(string cipherText)
    {
        var obj= service.DecryptStringAES(cipherText);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}