﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HDMUtilityVM
/// </summary>
namespace HDMUtilityVM
{
    public class MainCatlst
    {
        public string MNC_CODE { get; set; }
        public string MNC_NAME { get; set; }
        public bool ticked { get; set; }
    }

    public class SubCatlst
    {
        public string SUBC_CODE { get; set; }
        public string SUBC_NAME { get; set; }
        public string SUBC_MNC_CODE { get; set; }
        public bool ticked { get; set; }
    }

    public class ChildCatlst
    {
        public string CHC_TYPE_CODE { get; set; }
        public string CHC_TYPE_NAME { get; set; }
        public string CHC_TYPE_SUBC_CODE { get; set; }
        public string CHC_TYPE_MNC_CODE { get; set; }
        public bool ticked { get; set; }
    }
    public class Status
    {
        public int STA_ID { get; set; }
        public string STA_DESC { get; set; }
        public bool ticked { get; set; }
    }

    public class RequestedUsers
    {
        public string AUR_ID { get; set; }
        public string AUR_KNOWN_AS { get; set; }
        public bool ticked { get; set; }
    }
}
