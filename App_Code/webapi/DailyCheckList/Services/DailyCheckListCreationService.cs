﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using UtiltiyVM;
using System.Data.SqlClient;
using System.ComponentModel;

/// <summary>
/// Summary description for DailyCheckListCreationService
/// </summary>
public class DailyCheckListCreationService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    DailyCheckListCreationModel dclc = new DailyCheckListCreationModel();

    //get Inspectors
    public object getInspectors()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "DCLC_GET_INSPECTORS");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //get Locartions
    public object getLocation()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "DCLC_GET_LOCATIONS");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    // Get Main Categories
    public object getMainCategories()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "DCL_MAIN_CATEGORIES");
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    //Get Sub Categories
    public object getSubCategoryData(SlecetedMainCategories catdata)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "DCL_SUB_CATEGORIES");
        sp.Command.Parameters.Add("@Cat_code", catdata.BCL_MC_CODE, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    // selecteddata saving
    public object InsertDailyCheckList(selectdata objd)
    {
        int sem_id=0;
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@LOCName", SqlDbType.VarChar);
        param[0].Value = objd.LCMLST;
        param[1] = new SqlParameter("@InspectdBy", SqlDbType.VarChar);
        param[1].Value = objd.InspectdBy;
        param[2] = new SqlParameter("@date", SqlDbType.VarChar);
        param[2].Value = objd.date;
        param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["UID"];
        param[4] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[4].Value = HttpContext.Current.Session["COMPANYID"];
        param[5] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
        param[5].Value = UtilityService.ConvertToDataTable(objd.Seldata);
        param[6] = new SqlParameter("@FLAG", SqlDbType.Int);
        param[6].Value = objd.Flag;
        object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "DCL_INSERT_CHECKLIST", param);
        sem_id = (int)o;
        string msg;
        if (sem_id == 0)
            msg = "Saved as Draft";
        else
            msg = "CheckList Submitted Succesfully";
        return new { data = msg, Message = msg };

    }
}