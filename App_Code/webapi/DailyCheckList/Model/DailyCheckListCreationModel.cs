﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DailyCheckListCreationModel
/// </summary>
public class DailyCheckListCreationModel
{
    public class MainCategory
    {
        public string BCL_MC_CODE { get; set; }
        public string BCL_MC_NAME { get; set; }
    }

}
public class selectdata
{
    public string LCMLST { get; set; }
    public string InspectdBy { get; set; }
    public string date { get; set; }
    public List<SelectedRadio> Seldata { get; set; }
    public List<FileProperties> filesupload { get; set; }
    public int Flag { get; set; }
}

public class SlecetedMainCategories
{
    public string BCL_MC_CODE { get; set; }
    public string BCL_MC_NAME { get; set; }
    public string BCL_SUB_CODE { get; set; }
    public string BCL_SUB_NAME { get; set; }
}
public class SelectedRadio
{
    public string CatCode { get; set; }
    public string SubcatCode { get; set; }
    public string ScoreName { get; set; }
    public string FilePath { get; set; }
    public string row { get; set; }
    public string txtdata { get; set; }
    public string Date { get; set; }
    public string SubScore { get; set; }
}

