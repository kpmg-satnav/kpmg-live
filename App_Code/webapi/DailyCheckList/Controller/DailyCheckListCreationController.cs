﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


public class DailyCheckListCreationController : ApiController
{
    DailyCheckListCreationService dclcs = new DailyCheckListCreationService();
    

    //Get Main Categories
    [HttpGet]
    public Object getMainCategories()
    {
        return dclcs.getMainCategories();
    }

    //Get Sub Categories
    [HttpPost]
    public Object getSubCategoryData(SlecetedMainCategories  catlist)
    {
        return dclcs.getSubCategoryData(catlist);
    }


    [HttpPost]
    public HttpResponseMessage InsertDailyCheckList(selectdata seledata)
    {
        var obj = dclcs.InsertDailyCheckList(seledata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
