﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;


public class BudgetvsActualController : ApiController
{
    BudgetvsActualService Csvc = new BudgetvsActualService();
    BudgetvsActualReportView report = new BudgetvsActualReportView();

    [HttpPost]
    public HttpResponseMessage GetBudgetvsActualDetails(BudgetvsActualParams Custdata)
    {
        var obj = Csvc.GetBudgetvsActualObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetBudgetvsActualData([FromBody]BudgetvsActualParams data)
    {
        ReportGenerator<BudgetvsActualReportData> reportgen = new ReportGenerator<BudgetvsActualReportData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Energy_Mgmt/BudgetvsActual.rdlc"),
            DataSetName = "BudgetvsActual",
            //ReportType = "Budget Report"
        };

        Csvc = new BudgetvsActualService();
        List<BudgetvsActualReportData> reportdata = Csvc.GetBudgetvsActualDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/BudgetvsActual." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "BudgetvsActual." + data.Type;
        return result;
    }
}
