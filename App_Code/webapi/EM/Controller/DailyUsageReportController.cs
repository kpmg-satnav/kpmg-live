﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class DailyUsageReportController : ApiController
{
    DailyUsageReportService Csvc = new DailyUsageReportService();
    DailyUsageReportView report = new DailyUsageReportView();

    [HttpPost]
    public HttpResponseMessage GetDailyUsageDetails(DailyUsageReportParams Custdata)
    {
        var obj = Csvc.GetDailyUsageObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetDailyUsageData([FromBody]DailyUsageReportParams data)
    {
        ReportGenerator<DailyUsageReportData> reportgen = new ReportGenerator<DailyUsageReportData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Energy_Mgmt/DailyUsageReport.rdlc"),
            DataSetName = "DailyUsageReportDataSet",
            ReportType = "Usage Report"
        };

        Csvc = new DailyUsageReportService();
        List<DailyUsageReportData> reportdata = Csvc.GetDailyUsageReportDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/DailyUsageReport." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "DailyUsageReport." + data.Type;
        return result;
    }
}
