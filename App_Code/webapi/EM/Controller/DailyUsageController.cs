﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class DailyUsageController : ApiController
{
    DailyUsageService Daily = new DailyUsageService();

    [HttpPost]
    public HttpResponseMessage GetGriddata([FromBody]DailyUsageModel data)
    {
        var obj = Daily.GetGriddata(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


   
    [HttpPost]
    public HttpResponseMessage SaveData(DailyUsageModel data)
    {
        var obj = Daily.SaveData(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
