﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;

public class ThresholdController : ApiController
{
    ThresholdService ThreshService = new ThresholdService();

    [HttpPost]
    public HttpResponseMessage GetGriddata([FromBody]ThresholdModel data)
    {
        var obj = ThreshService.GetGriddata(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveData(ThresholdModel data)
    {
        var obj = ThreshService.SaveData(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
