﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ThresholdModel
/// </summary>
public class ThresholdModel
{
    public List<Countrylst> cnylst { get; set; }
    public List<Towerlst> twrlst { get; set; }
    public ThreshHold Thresh { get; set; }
    public List<ThreshHold> ThreshList { get; set; }
    public ThresholdDet ThresholdDetails { get; set; }
    public List<ThresholdDet> ThresholdDetList { get; set; }
}

public class ThreshHold
{
    public string EM_THRD_TYPE { get; set; }
    public string EM_UM_MEASUR { get; set; }
    public string EM_UM_CNY_ID { get; set; }
    public string EM_THRD_QUNTY { get; set; }
    public string LCM_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string TWR_NAME { get; set; }
}

public class ThresholdDet
{
    public string EM_THRD_TYPE { get; set; }
    public string EM_THRD_QUNTY { get; set; }
    public string EM_THRD_THR_ID { get; set; }

}
