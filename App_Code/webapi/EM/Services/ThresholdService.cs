﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ThresholdService
/// </summary>
public class ThresholdService
{
    SubSonic.StoredProcedure sp;

    public object GetGriddata(ThresholdModel Details)
    {
        List<ThreshHold> Dlist = new List<ThreshHold>();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@TWRLIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(Details.twrlst);

        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "EM_GET_THRESHOLD_TYPES", param))
        {
            while (reader.Read())
            {
                Dlist.Add(new ThreshHold()
                {
                    EM_THRD_TYPE = reader["EM_THRD_TYPE"].ToString(),
                    EM_UM_MEASUR = reader["EM_UM_MEASUR"].ToString(),
                    EM_UM_CNY_ID = reader["EM_UM_CNY_ID"].ToString(),
                    EM_THRD_QUNTY = reader["EM_THRD_QUNTY"].ToString(),
                    CTY_NAME = reader["CTY_NAME"].ToString(),
                    LCM_NAME = reader["LCM_NAME"].ToString(),
                    TWR_NAME = reader["TWR_NAME"].ToString(),
                });
            }
            reader.Close();
            return Dlist;
        }
    }

    public object SaveData(ThresholdModel Details)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@TWRLIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Details.twrlst);
            param[1] = new SqlParameter("@THRESHOLD_LIST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Details.ThresholdDetList);
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];

            int result = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "EM_INSERT_THRESHOLD_DATA", param);

            if (result != -1)
            {
                return new { Message = MessagesVM.EM_INSERTED, data = result };
            }
            else
                return new { Message = MessagesVM.EM_NO_REC_INSR, data = (object)null };

        }
        catch
        {
            throw;
        }
    }
}