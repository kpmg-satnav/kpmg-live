﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for DailyUsageService
/// </summary>
public class DailyUsageService
{
    SubSonic.StoredProcedure sp;
    public object GetGriddata(DailyUsageModel Details)
    {
        List<DailyUsage> Dlist = new List<DailyUsage>();
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@TWR_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(Details.twrlst);
        param[1] = new SqlParameter("@EM_DU_LOG_DT", SqlDbType.Date);
        param[1].Value = Details.date;

        using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "EM_GET_UM_TYPES", param))
        {
            while (reader.Read())
            {
                Dlist.Add(new DailyUsage()
                {
                    EM_DUD_TYPE = reader["EM_DUD_TYPE"].ToString(),
                    EM_UM_MEASUR = reader["EM_UM_MEASUR"].ToString(),
                    EM_UM_CNY_ID = reader["EM_UM_CNY_ID"].ToString(),
                    EM_DUD_QUNTY = reader["EM_DUD_QUNTY"].ToString(),
                    EM_DUD_SERNO = reader["EM_DUD_SERNO"].ToString(),
                    CTY_NAME = reader["CTY_NAME"].ToString(),
                    LCM_NAME = reader["LCM_NAME"].ToString(),
                    TWR_NAME = reader["TWR_NAME"].ToString(),
                });
            }
            reader.Close();
            return Dlist;
        }
    }

    public object SaveData(DailyUsageModel Details)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@TWRLIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(Details.twrlst);
            param[1] = new SqlParameter("@DAILY_LIST", SqlDbType.Structured);
            param[1].Value = UtilityService.ConvertToDataTable(Details.DailyUsgeDetList);
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];
            param[3] = new SqlParameter("@DATE", SqlDbType.DateTime);
            param[3].Value = Details.date;
            param[4] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[4].Value = Details.companyid;

            int result = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "EM_INSERT_DAILY_USAGE", param);

            if (result != -1)
            {
                return new { Message = MessagesVM.EM_INSERTED, data = result };
            }
            else
                return new { Message = MessagesVM.EM_NO_REC_INSR, data = (object)null };
        }
        catch
        {
            throw;
        }
    }
}