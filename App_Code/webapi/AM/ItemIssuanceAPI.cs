﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Text;
using Newtonsoft.Json;
using System.Reflection;

public class ItemIssuanceAPIController : ApiController
{
    ItemIssuanceService service = new ItemIssuanceService();
    public DataTable GetallEmployeeIDDS()
    {
        return service.GetallEmployeeIDDS();
    }

    public DataTable GetallLocations()
    {
        return service.GetallLocations();
    }

    public DataTable GetallCategories()
    {
        return service.GetallCategories();
    }

    [HttpPost]
    public Object Getallsubcatbycat([FromBody] ItemIssuanceVM category)
    {
        return service.Getallsubcatbycat(category);
    }

    [HttpPost]
    public DataTable GetallBrandbysubcat([FromBody] ItemIssuanceVM category)
    {
        return service.GetallBrandbysubcat(category);
    }

    [HttpPost]
    // [FromBody] is used to get the values posted from the jquery
    public Object GetallItemstotbl([FromBody] ItemIssuanceVM category, [FromUri] int? jtStartIndex, [FromUri] int? jtPageSize, [FromUri] string jtSorting)
    {
        return service.GetallItemstotbl(category,jtStartIndex,jtPageSize,jtSorting);
    }

    [HttpPost]
    public object Getallselectedmodels([FromBody] ItemIssuanceVM category)
    {
        return service.Getallselectedmodels(category);
    }

    [HttpPost]
    public string Submitdetails([FromBody] ItemIssuanceVM category)
    {
        return service.Submitdetails(category);
    }
}
