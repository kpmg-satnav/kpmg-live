﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using QuickFMS.API.Filters;
/// <summary>
/// Summary description for SpaceSeatHistoryReportController
/// </summary>
public class SpaceSeatHistoryReportController : ApiController
{
    SpaceSeatHistoryReportService Sshs = new SpaceSeatHistoryReportService();
    

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetGridData([FromBody] SpaceSeatHistoryVM data)
    {
        var obj = Sshs.GetCustomizedDetails(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}