﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;



public class SpacePlanningReportController : ApiController
{
    SpacePlanningReportService service = new SpacePlanningReportService();

    [HttpPost]
    public HttpResponseMessage BindGridData([FromBody] SpacePlanningReport searchObj)
    {
        var SpcPlnLst = service.BindGridData(searchObj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SpcPlnLst);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage BindGridData1([FromBody] SpacePlanningReport searchObj)
    {
        var SpcPlnLst = service.BindGridData1(searchObj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SpcPlnLst);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetLocChartData([FromBody] SpacePlanningReport searchObj)
    {
        var obj = service.GetSpaceChart(searchObj);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> GetSpacePlanningReportdata([FromBody]SpacePlanningReport Spcdata)
    {
        ReportGenerator<SpacePlanningReportVM> reportgen = new ReportGenerator<SpacePlanningReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpacePlanningReport.rdlc"),
            DataSetName = "SpacePlanningRptDS",
            ReportType = "Space Planning Report"
        };

        //service = new SpaceRequisitionReportService();
        List<SpacePlanningReportVM> reportdata = service.GetData(Spcdata);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpacePlanningReport." + Spcdata.Type);
        await reportgen.GenerateReport(reportdata, filePath, Spcdata.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpacePlanningReport." + Spcdata.Type;
        return result;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetSpacePlanningVerticalReportdata([FromBody]SpacePlanningReport Spcdata)
    {
        ReportGenerator<SpacePlanningReportVM> reportgen = new ReportGenerator<SpacePlanningReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpacePlanningVerticalReport.rdlc"),
            DataSetName = "SpacePlanningVerticalReport",
            ReportType = "Space Planning Vertical Report"
        };

        //service = new SpaceRequisitionReportService();
        List<SpacePlanningReportVM> reportdata = service.GetData(Spcdata);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpacePlanningVerticalReport." + Spcdata.Type);
        await reportgen.GenerateReport(reportdata, filePath, Spcdata.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpacePlanningReport." + Spcdata.Type;
        return result;
    }
}
