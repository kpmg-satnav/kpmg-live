﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

/// <summary>
/// Summary description for SpaceTypeCostController
/// </summary>
public class SpaceTypeCostController : ApiController
{

    SpaceTypeCostService service = new SpaceTypeCostService();

    [HttpGet]
    public HttpResponseMessage BindGridData([FromUri] string flrcode)
    {
        var obj = service.BindGridData(flrcode);
        //IEnumerable<SpaceTypeCost> SpaceTypeList = service.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpGet]
    public HttpResponseMessage getLocationdata([FromUri] string lcmcode)
    {
        var obj = service.getLocationdata(lcmcode);        
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetAreaType()
    {
        var obj = service.GetAreaType();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SetAreaType(SpaceTypeCostVM stcvm)
    {
        var obj = service.SetAreaType(stcvm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveCostData(List<SPCDetails> spc)
    {
        var obj = service.SaveCostData(spc);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveLocationCostData(List<locationdata> lcmcost)
    {
        var obj = service.SaveLocationCostData(lcmcost);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage GetAreaTypeByLocation(List<Locationlst> lcmlst)
    {
        var obj = service.GetAreaTypeByLocation(lcmlst);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetSpcLst([FromUri] string flrcode)
    {
        var obj = service.GetSpcLst(flrcode);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage ShowLocationGrid([FromUri] string lcmcode)
    {
        var obj = service.ShowLocationGrid(lcmcode);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //[HttpPost]
    //public HttpResponseMessage Costupdate([FromBody] SpaceDetails spcdet)
    //{
    //    var obj = service.Costupdate(spcdet);
    //    //IEnumerable<SpaceTypeCost> SpaceTypeList = service.BindGridData();
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}
}