﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

/// <summary>
/// Summary description for KotakMaploaderController
/// </summary>
public class KotakMaploaderController: ApiController
{
    KotakMaploaderService mspsrvc = new KotakMaploaderService();
    [GzipCompression]
    [HttpGet]
    public Object GetFloorLst()
    {
        return mspsrvc.GetFloorLst();
    }
    [HttpGet]
    public Object GetImageList([FromUri] string Image)
    {
        return mspsrvc.GetImageList(Image);
    }
    [GzipCompression]
    [HttpPost]
    public Object GetMapItems([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetMapItems(svm);
    }

    [GzipCompression]
    [HttpPost]
    public Object GetMarkers([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetMarkers(svm);
    }

    [GzipCompression]
    [HttpPost]
    public Object GetCornerLables([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetCornerLables(svm);
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetSpaceRecords([FromBody]Space_mapVM svm)
    {
        var obj = mspsrvc.InactiveSpacesFromFloorMaps(svm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }



    [GzipCompression]
    [HttpPost]
    public Object GetLegendsCount([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetLegendsCount(svm);
    }


    [GzipCompression]
    [HttpPost]
    public Object GetLegendsSummary([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetLegendsSummary(svm);
    }


    [GzipCompression]
    [HttpPost]
    public object GetSpaceDetailsBySPCID([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetSpaceDetailsBySPCID(svm);
    }


    [GzipCompression]
    [HttpPost]
    public object GetSpaceDetailsBySUBITEM([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetSpaceDetailsBySUBITEM(svm);
    }


    [GzipCompression]
    [HttpPost]
    public object GetSpaceDetailsByREQID([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetSpaceDetailsByREQID(svm);
    }


    [GzipCompression]
    [HttpPost]
    public object GetEmpDetails([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetEmpDetails(svm);
    }


    [GzipCompression]
    [HttpGet]
    public object GetallFilterbyItem()
    {
        return mspsrvc.GetallFilterbyItem();
    }


    [GzipCompression]
    [HttpPost]
    public object GetallFilterbySubItem([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetallFilterbySubItem(svm);
    }


    [GzipCompression]
    [HttpPost]
    public object GetAllocEmpDetails([FromBody]Space_mapVM svm)
    {
        return mspsrvc.GetAllocEmpDetails(svm);
    }

    [GzipCompression]
    [HttpPost]
    public object InactiveSpaceSeats(CLS_INACTIVE_SPACE CIS)
    {
        return mspsrvc.InactiveSeats(CIS);
    }

    [GzipCompression]
    [HttpPost]
    public object ReleaseSelectedseat(SPACE_REL_DETAILS sad)
    {
        return mspsrvc.ReleaseSelectedseat(sad);
    }

    [GzipCompression]
    [HttpPost]
    public object ActivateSpaces(CLS_INACTIVE_SPACE CIS)
    {
        return mspsrvc.ActivateSpaces(CIS);
    }

    [GzipCompression]
    [HttpPost]
    public object AllocateSeats(List<SPACE_ALLOC_DETAILS> allocDetLst)
    {
        return mspsrvc.AllocateSeats(allocDetLst);
    }

    [GzipCompression]
    [HttpPost]
    public object SpcAvailabilityByShift(SPACE_ALLOC_DETAILS data)
    {
        return mspsrvc.SpcAvailabilityByShift(data);
    }

    [GzipCompression]
    [HttpPost]
    public object GetTotalAreaDetails(Space_mapVM data)
    {
        return mspsrvc.GetTotalAreaDetails(data);
    }

    [GzipCompression]
    [HttpPost]
    public object GetSeatingCapacity(Space_mapVM data)
    {
        return mspsrvc.GetSeatingCapacity(data);
    }

    [GzipCompression]
    [HttpPost]
    public object GetEmpAllocSeat(Space_mapVM data)
    {
        return mspsrvc.GetEmpAllocSeat(data);
    }
}