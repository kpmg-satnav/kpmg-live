﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for DemandPlanController
/// </summary>
public class DemandPlanController : ApiController
{
    DemandPlanService DemandService = new DemandPlanService();

    [HttpPost]
    public HttpResponseMessage UploadTemplate()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = DemandService.UploadTemplate(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}