﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ViewSpaceProjectionController : ApiController
{
    ViewSpaceProjectionService ViewPlanReq = new ViewSpaceProjectionService();
    [HttpGet]
    public HttpResponseMessage GetPlanRequisitions()
    {
        var osplist = ViewPlanReq.GetPlanRequisitions();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, osplist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetPlansListbyReqId(string REQ_ID)
    {
        var ospdetailslist = ViewPlanReq.GetPlansListbyReqId(REQ_ID);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ospdetailslist);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage UpdateDetails(OSPSaveList dataobject)
    {
        object obj = ViewPlanReq.UpdateDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    [HttpPost]
    public HttpResponseMessage CancelDetails(OSPSaveList dataobject)
    {
        object obj = ViewPlanReq.CancelDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }
}
