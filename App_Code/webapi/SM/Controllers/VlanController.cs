﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;
using System.Web.Http;

/// <summary>
/// Summary description for VlanController
/// </summary>
public class VlanAPIController : ApiController
{
    VlanServices VS = new VlanServices();

    //[HttpPost]
    //public HttpResponseMessage InsertNUpdate(VlanModel mncCat)
    //{
    //    var obj = VS.InsertNUpdate(mncCat);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;

    //}
    [HttpPost]
    public HttpResponseMessage InsertUpdate(VlanModel mncCat)
    {
        var obj = VS.InsertUpdate(mncCat);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage VlanTypeBindGrid()
    {
        IEnumerable<VlanModel> mncCatlist = VS.GetVlanTypeBindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, mncCatlist);
        return response;
    }

}