﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Data;

public class SpaceSubTypeController : ApiController
{
    SpaceSubTypeService service = new SpaceSubTypeService();

    [HttpPost]
    public HttpResponseMessage Save(SpaceSubTypeVM SpaceSubType)
    {
        if (service.Save(SpaceSubType) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SpaceSubType);
            return response;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code Already Exists");
        }
    }

    [HttpPost]
    public HttpResponseMessage Update(SpaceSubTypeVM update)
    {

        if (service.Update(update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }

    [HttpGet]
    public HttpResponseMessage BindGrid()
    {
        IEnumerable<SpaceSubTypeVM> SpcSubTypeList = service.BindGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SpcSubTypeList);
        return response;
    }


    [HttpGet]
    public HttpResponseMessage GetSpaceTypes()
    {
        IEnumerable<SpaceTypeVM> SpaceTypeList = service.GetSpaceTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SpaceTypeList);
        return response;
    }
}
