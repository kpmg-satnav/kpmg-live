﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtiltiyVM;

public class SpaceRequisitionController : ApiController
{
    SpaceRequisitionService SRS = new SpaceRequisitionService();

    [HttpPost]
    public HttpResponseMessage GetSpaces([FromBody] SpaceReqDetails spcdet)
    {
        var obj = SRS.GetSpaces(spcdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetShifts([FromBody] List<Locationlst> Location)
    {
        var obj = SRS.GetShifts(Location);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetEmpDetails([FromBody] SpaceReqDetails spcreqdet)
    {
        var obj = SRS.GetEmpDetails(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage RaiseRequest([FromBody] SpaceReqDetails spcreqdet)
    {
        var obj = SRS.RaiseRequest(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetReqCountDetails([FromBody] SpaceReqDetails spcreqcount)
    {
        var obj = SRS.GetReqCountDetails(spcreqcount);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage RaiseCountRequest([FromBody] SpaceReqDetails Raisespcreqcnt)
    {
        var obj = SRS.RaiseCountRequest(Raisespcreqcnt);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetEmpTypes([FromBody] List<Costcenterlst> cstlst)
    {
        var obj = SRS.GetEmpTypes(cstlst);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetEmpSubTypes()
    {
        var obj = SRS.GetEmpSubTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetSpaceTypes()
    {
        var obj = SRS.GetSpaceTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetSpaceSubTypes()
    {
        var obj = SRS.GetSpaceSubTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetSubTypes([FromUri] string SpaceType)
    {
        var obj = SRS.GetSubTypes(SpaceType);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage CheckSpace([FromBody] SpaceReqDetails spcreqdet)
    {
        var obj = SRS.CheckSpace(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage CheckExists([FromBody] SpaceReqDetails spcreqdet)
    {
        var obj = SRS.CheckExists(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
