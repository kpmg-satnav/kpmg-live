﻿using System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class CostReportController : ApiController
{
    CostReportService areaService = new CostReportService();
    [HttpPost]
    public HttpResponseMessage GetCostReportBindGrid(Company cstvm)
    {
        var obj = areaService.GetCostReportObject(cstvm);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Chart Data

    [HttpPost]
    public HttpResponseMessage GetCostChartData()
    {
        var obj = areaService.GetCostChart();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetCostVerticalChartData()
    {
        var obj = areaService.GetCostVerticalChart();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> GetCostReportdata(Company data)
    {

        ReportGenerator<CostReportModel> reportgen = new ReportGenerator<CostReportModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/CostReport.rdlc"),
            DataSetName = "CostReportSPC",
            ReportType = "Cost Report",
            Vertical = data.VERTICAL,
            Costcenter = data.COSTCENTER
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/CostReport." + data.Type);
        List<CostReportModel> reportdata = areaService.GetCostReportList(data);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "CostReport." + data.Type;
        return result;

    }
}
