﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;


public class SpaceTypeController:ApiController
{
    SpaceTypeService service = new SpaceTypeService();

    [HttpPost]
    public HttpResponseMessage Create(SpaceTypeModel spcCategory)
    {
        if (service.Save(spcCategory) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, spcCategory);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already Exists");
    }

    [HttpPost]
    public HttpResponseMessage UpdateSpaceTypeData(SpaceTypeModel update)
    {
        if (service.UpdateSpaceTypeData(update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest);
    }
    [HttpGet]
    public HttpResponseMessage BindGridData()
    {
        IEnumerable<SpaceTypeModel> SpaceTypeList = service.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SpaceTypeList);
        return response;
    }

}