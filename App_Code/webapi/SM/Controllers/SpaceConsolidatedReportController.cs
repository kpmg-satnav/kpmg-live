﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using QuickFMS.API.Filters;

public class SpaceConsolidatedReportController : ApiController
{
    SpaceConsolidatedReportService SPC_CON_SER = new SpaceConsolidatedReportService();
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage BindGrid(SpaceConsolidatedParameters Params)
    {
        var obj = SPC_CON_SER.BindGrid(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetSummary(SpaceConsolidatedParameters Params)
    {
        var obj = SPC_CON_SER.GetSummary(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage SpaceConsolidatedChart()
    {
        var obj = SPC_CON_SER.SpaceConsolidatedChart();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> Export_SpaceConsolidatedRpt([FromBody]SpaceConsolidatedParameters rptCost)
    {

        ReportGenerator<SpaceConsolidatedReportVM> reportgen = new ReportGenerator<SpaceConsolidatedReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceConsolidatedReport.rdlc"),
            DataSetName = "SpaceConsolidatedReport",
            ReportType = "Space Consolidated Report",
            //Vertical=rptCost.VERTICAL,
            //Costcenter=rptCost.COSTCENTER
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceConsolidatedReport." + rptCost.DocType);

        List<SpaceConsolidatedReportVM> reportdata = SPC_CON_SER.GetReportList(rptCost);
        await reportgen.GenerateReport(reportdata, filePath, rptCost.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceConsolidatedReport." + rptCost.DocType;
        return result;

    }

    [HttpPost]
    public async Task<HttpResponseMessage> Export_SpaceConsolidatedLocationRpt([FromBody]SpaceConsolidatedParameters rptCost)
    {

        ReportGenerator<SpaceConsolidatedReportVM> reportgen = new ReportGenerator<SpaceConsolidatedReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceConsolidatedLocationReport.rdlc"),
            DataSetName = "SpaceConsolidatedLocationReport",
            ReportType = "Space Allocation report",
            //Vertical=rptCost.VERTICAL,
            //Costcenter=rptCost.COSTCENTER
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceConsolidatedLocationReport." + rptCost.DocType);

        List<SpaceConsolidatedReportVM> reportdata = SPC_CON_SER.GetReportList_location(rptCost);
        await reportgen.GenerateReport(reportdata, filePath, rptCost.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceConsolidatedLocationReport." + rptCost.DocType;
        return result;
    }

    [GzipCompression]
    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> Export_SpaceConsolidatedPrjRpt([FromBody]SpaceConsolidatedParameters rptCost)
    {

        ReportGenerator<SpaceConsolidatedReportVM> reportgen = new ReportGenerator<SpaceConsolidatedReportVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceConsolidatedProjectReport.rdlc"),
            DataSetName = "DataSet1",
            ReportType = "Project wise occupied seat count",
            //Costcenter=rptCost.COSTCENTER
            //Vertical=rptCost.VERTICAL,
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceConsolidatedProjectReport." + rptCost.DocType);

        List<SpaceConsolidatedReportVM> reportdata = SPC_CON_SER.GetExportLst(rptCost);
        await reportgen.GenerateReport(reportdata, filePath, rptCost.DocType);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceConsolidatedProjectReport." + rptCost.DocType;
        return result;

    }
}
