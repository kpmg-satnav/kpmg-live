﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ViewVerticalRequisitionController : ApiController
{
    ViewVerticalRequisitionService VerticalReqService = new ViewVerticalRequisitionService();
    //Grid View
    [HttpGet]
    public HttpResponseMessage GetMyReqList(int id)
    {
       var Verlist = VerticalReqService.GetMyReqList(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Verlist);
        return response;
    }
 
    [HttpPost]
    public HttpResponseMessage GetDetailsOnSelection(SMS_VERTICAL_REQUISITION selectedid)
    {
        var Verlist = VerticalReqService.GetDetailsOnSelection(selectedid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Verlist);
        return response;
    }
       
   
}
