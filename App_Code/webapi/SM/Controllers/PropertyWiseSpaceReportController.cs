﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for PropertyWiseSpaceReportController
/// </summary>
public class PropertyWiseSpaceReportController : ApiController
{

    PropertyWiseSpaceReportService pwsr = new PropertyWiseSpaceReportService();


    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(PropertyWiseSpaceReport Custdata)
    {
        var obj = pwsr.GetpeopertyspaceObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}