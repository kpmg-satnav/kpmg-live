﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SearchSpacesController : ApiController
{
    SearchSpacesService ssServ = new SearchSpacesService();

    [HttpPost]
    public HttpResponseMessage GetVacantSpaces([FromBody] SpaceDetails spcdet)
    {
        var obj = ssServ.GetVacantSpaces(spcdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage RaiseRequest([FromBody] SpaceDetails spcdet)
    {
        var obj = ssServ.RaiseRequest(spcdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage ApproveRequests([FromBody] SpaceDetails spcdet)
    {
        var obj = ssServ.ApproveRequests(spcdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
   
   
}
