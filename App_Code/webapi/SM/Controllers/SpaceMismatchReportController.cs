﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;
using QuickFMS.API.Filters;

public class SpaceMismatchReportController : ApiController
{
    SpaceMismatchReportService Csvc = new SpaceMismatchReportService();
    SpaceMismatchReportView report = new SpaceMismatchReportView();

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetMismatchDetails(MismatchDetails Custdata)
    {
        var obj = Csvc.GetMismatchObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public async Task<HttpResponseMessage> GetMismatchDatadwn([FromBody]MismatchDetails data)
    {
        ReportGenerator<MismatchData> reportgen = new ReportGenerator<MismatchData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/SpaceMismatchReport.rdlc"),
            DataSetName = "SpaceMismatchReport",
            //ReportType = "Space Mismatch Report",
           
        };

        Csvc = new SpaceMismatchReportService();
        DataTable reportdata = Csvc.GetMismatchDetails_dump(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceMismatchReport." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceMismatchReport." + data.Type;
        return result;
        
    }
}
