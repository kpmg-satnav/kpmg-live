﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

/// <summary>
/// Summary description for ScheduleMySeatBookingsController
/// </summary>
public class ScheduleMySeatBookingsController : ApiController
{
    ScheduleMySeatBookingsService ssbservice = new ScheduleMySeatBookingsService();

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage SearchSpaces([FromBody] ScheduleMySeatBookingsModel data)
    {
        var obj = ssbservice.SearchSpaces(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public object AllocateSeats(List<SPACE_ALLOC_DETAILS_TIME> allocDetLst)
    {
        return ssbservice.AllocateSeats(allocDetLst);
    }
    [GzipCompression]
    [HttpGet]
    public object getVLANS()
    {
        return ssbservice.getVLANS();
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage getEmployeeSpaceDetails([FromBody] ScheduleMySeatBookingsModel data)
    {
        var obj = ssbservice.getEmployeeSpaceDetails(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}