﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SpaceAllocationController : ApiController
{
    SpaceAllocationService SpaceAllocation = new SpaceAllocationService();

    [HttpGet]
    public HttpResponseMessage GetPendingSpaceAllocationDetails()
    {
        var spclist = SpaceAllocation.GetPendingSpaceAllocationDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, spclist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetDetailsOnSelection(SpaceRequistion selectedid)
    {
        var spclist = SpaceAllocation.GetDetailsOnSelection(selectedid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, spclist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetDetailsOnSelectionAlloc(SpaceRequistion selectedid)
    {
        var spclist = SpaceAllocation.GetDetailsOnSelectionAlloc(selectedid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, spclist);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveSelectedSpaces([FromBody]SpaceReqDetails spcreqdet)
    {
        var obj = SpaceAllocation.SaveSelectedSpaces(spcreqdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SendMail([FromBody]SpaceRequistion spcreq)
    {
        var obj = SpaceAllocation.SendMail(spcreq);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetVacantSpaces([FromBody] SpaceVacantDetails spcdet)
    {
        var obj = SpaceAllocation.GetVacSpaces(spcdet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage Allocatedclosed([FromBody] SpaceReq Reqid)
    {
        var obj = SpaceAllocation.Allocatedclosed(Reqid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}
