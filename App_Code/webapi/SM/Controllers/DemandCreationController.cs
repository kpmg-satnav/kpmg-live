﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Web.Http;

/// <summary>
/// Summary description for DemandCreationController
/// </summary>
public class DemandCreationController : ApiController
{
    DemandCreationService ViewPlanReq = new DemandCreationService();
    [HttpGet]
    public HttpResponseMessage GetDemandRequests()
    {
        var osplist = ViewPlanReq.GetDemandRequests();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, osplist);
        return response;
    }


    [HttpPost]
    public HttpResponseMessage Submit(DemandCreationVM DCVM)
    {
        var obj = ViewPlanReq.Submit(DCVM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}