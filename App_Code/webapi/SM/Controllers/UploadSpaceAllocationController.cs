﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class UploadSpaceAllocationController : ApiController
{
    UploadSpaceAllocationService servc = new UploadSpaceAllocationService();

    [GzipCompression]
    [HttpPost]

    public async Task<HttpResponseMessage> DownloadTemplate([FromBody] UploadSpacesVM spcdet) 
    {
        string parent = "Vertical", child = "Costcenter";
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AMT_BSM_GETALL"))
        {
            if (sdr.Read())
            {
                parent = Convert.ToString(sdr["AMT_BSM_PARENT"]);
                child = Convert.ToString(sdr["AMT_BSM_CHILD"]);
            }
        }
        ReportGenerator<UploadAllocationDataDumpVM> reportgen = new ReportGenerator<UploadAllocationDataDumpVM>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/space_mgmt/UploadSpaceAllocation.rdlc"),
            DataSetName = "DownloadAllocDT",
            Vertical = parent,
            Costcenter = child
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/SpaceUploadTemplate.xlsx");
        List<UploadAllocationDataDumpVM> reportdata = servc.DownloadTemplate(spcdet);
        await reportgen.GenerateReport(reportdata, filePath, "xlsx");
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "SpaceUploadTemplate.xlsx";
        return result;

    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage UploadTemplate()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = servc.UploadTemplate(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
