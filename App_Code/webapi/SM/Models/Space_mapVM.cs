﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Space_mapVM
/// </summary>
public class Space_mapVM
{
    public String Item { get; set; }
    public String lcm_code { get; set; }
    public String flr_code { get; set; }
    public String twr_code { get; set; }
    public String subitem { get; set; }
    public String category { get; set; }
    public String CatValue { get; set; }
    public Int32 mode { get; set; }
    public String spcid { get; set; }
    public int key_value { get; set; }
    public DateTime from_date { get; set; }
    public DateTime to_date { get; set; }
    public string sh_code { get; set; }
    public string monitor_type { get; set; }
}

public class Monitor_Details
{
    public int MO_ID { get; set; }
    public string MO_NAME { get; set; }
    public string MO_TYPE { get; set; }
    public string MO_CODE { get; set; }

}
public class SPACE_ALLOC_DETAILS_kotak
{
    public string SPC_ID { get; set; }
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SH_CODE { get; set; }
    public string VERTICAL { get; set; }
    public string Cost_Center_Code { get; set; }
    public string AUR_ID { get; set; }
    public string SPC_DESC { get; set; }
    public int STATUS { get; set; }
    public Nullable<DateTime> FROM_DATE { get; set; }
    public Nullable<DateTime> TO_DATE { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
    public string SHIFT_TYPE { get; set; }
}
public class Space_MapEmployeeVM
{
    public string SPC_ID { get; set; }
    public string x { get; set; }
    public string y { get; set; }
    public string SPC_FLR_ID { get; set; }
    public string SPC_LAYER { get; set; }
    public string SPC_DESC { get; set; }
    public string STATUS { get; set; }
    public string AD_ID { get; set; }
}

public class AD_DATA_VM
{
    public string USR_ID { get; set; }
    public string LOG_IN_TIME { get; set; }
    public string LOG_OUT_TIME { get; set; }
}

public class SPACE_ALLOC_DETAILS
{
    public string SPC_ID { get; set; }
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SH_CODE { get; set; }
    public string VERTICAL { get; set; }
    public string Cost_Center_Code { get; set; }
    public string AUR_ID { get; set; }
    public int STATUS { get; set; }
    public Nullable<DateTime> FROM_DATE { get; set; }
    public Nullable<DateTime> TO_DATE { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
    public string SHIFT_TYPE { get; set; }
    public string emp { get; set; }

}

public class SPACE_ALLOC_DETAILS_TIME
{
    public string SPC_ID { get; set; }
    public string SSA_SRNREQ_ID { get; set; }
    public string SSAD_SRN_REQ_ID { get; set; }
    public string SH_CODE { get; set; }
    public string VERTICAL { get; set; }
    public string Cost_Center_Code { get; set; }
    public string AUR_ID { get; set; }
    public int STATUS { get; set; }
    public Nullable<DateTime> FROM_DATE { get; set; }
    public Nullable<DateTime> TO_DATE { get; set; }
    public string FROM_TIME { get; set; }
    public string TO_TIME { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }
    public string SHIFT_TYPE { get; set; }
    public string VL_Code { get; set; }
    public string emp { get; set; }
}
public class TO_TIME
{
    public string TO_TIME1 { get; set; }
    public string FROM_TIME { get; set; }
}

public class LEGEND_SUMMARY
{
    public int TOTAL { get; set; }
    public int VACANT { get; set; }
    public int OCCUPIED { get; set; }
    public int ALLOCATEDVER { get; set; }
    public int ALLOCATEDCST { get; set; }
    public int OVERLOAD { get; set; }
    public string OCCUP_EMP { get; set; }
    public string ALLOCUNUSED { get; set; }
    public int LEAVE { get; set; }
    public int BLOCKED { get; set; }
}

public class SPACE_REL_DETAILS
{
    public List<SPACE_ALLOC_DETAILS> sad { get; set; }
    public int reltype { get; set; }
}

public class SPACEDETAILS
{
    public string SPACE_ID { get; set; }
    public string SPC_TYPE { get; set; }
    public string SPC_SUB_TYPE { get; set; }
    public bool ticked { get; set; }
}

public class CLS_INACTIVE_SPACE
{
    public List<SPACEDETAILS> SPC_LST { get; set; }
    public string FLR_CODE { get; set; }
}


public class AUR_DETAILS
{
    public string AUR_ID { get; set; }
}

