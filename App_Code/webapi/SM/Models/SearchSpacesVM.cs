﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SearchSpacesVM
/// </summary>

public class SpaceDetails
{
    public List<Floorlst> flrlst { get; set; }
    public VerticalRequistion verreq { get; set; }
    public List<VerticalRequistion> verreqList { get; set; }
    public List<VerticalReq_details> verreqdet { get; set; }
    public int ALLOCSTA { get; set; }
    public int MODE { get; set; }
}

public class VerticalRequistion
{
    public string SVR_REQ_ID { get; set; }
    public string SVR_REQ_BY { get; set; }
    public string SVR_VER_CODE { get; set; }
    public string SVR_COST_CODE { get; set; }
    public DateTime SVR_FROM_DATE { get; set; }
    public DateTime SVR_TO_DATE { get; set; }
    public int SVR_STA_ID { get; set; }
    public string SVR_REQ_REM { get; set; }
    public string SVR_APPR_BY { get; set; }
    public string SVR_APPR_REM { get; set; }
    public int STACHECK { get; set; }
}

public class VerticalReq_details
{
    public string SVD_REQ_ID { get; set; }
    public string SVD_SPC_ID { get; set; }
    public string SVD_SPC_NAME { get; set; }
    public string SVD_SPC_TYPE { get; set; }
    public string SVD_SPC_TYPE_NAME { get; set; }
    public string SVD_SPC_SUB_TYPE { get; set; }
    public string SVD_SPC_SUB_TYPE_NAME { get; set; }
    public string FLR_ID { get; set; }
    public string lat { get; set; }
    public string lon { get; set; }
    public int SVD_STA_ID { get; set; }
    public int STACHECK { get; set; }
    public bool ticked { get; set; }

}

