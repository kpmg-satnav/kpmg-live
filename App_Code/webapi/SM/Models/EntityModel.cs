﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EntityModel
/// </summary>
public class EntityModel
{
    public int CH_ENTY_ID { get; set; }
    public string CH_ENTY_CODE { get; set; }
    public string CH_ENTY_NAME { get; set; }
    public string CH_ENTY_STA_ID { get; set; }
    public string CH_ENTY_PRNT_CODE { get; set; }
    public string CH_ENTY_REM { get; set; }
}
public class EntityAdminModel
{
    public string PRNT_CODE { get; set; }
    public string PRNT_NAME { get; set; }
}
