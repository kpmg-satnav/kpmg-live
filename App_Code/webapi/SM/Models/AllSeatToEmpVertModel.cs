﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AllSeatToEmpVertModel
/// </summary>
public class AllSeatToEmpVertModel
{
	public AllSeatToEmpVertModel()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public class GetShifttimes
    {
        public string flr_id { get; set; }
        public string shift_id { get; set; }
    }

    public class GetSeattoAllOcc
    {
        public string location { get; set; }
        public string floor { get; set; }
        public DateTime fromdate { get; set; }
        public DateTime todate { get; set; }
        public DateTime fromtime { get; set; }
        public DateTime totime { get; set; }
        public string spacetype { get; set; }
        public string spaceid { get; set; }
        public int mode { get; set; }
    }

    public class BlockSeatsReqPOU
    {
        public string reqid { get; set; }
        public string VerticalCode { get; set; }
        public int cntWst { get; set; }
        public int cntFCB { get; set; }
        public int cntHCB { get; set; }
        public DateTime fromdate { get; set; }
        public DateTime todate { get; set; }
        public DateTime fromtime { get; set; }
        public DateTime totime { get; set; }
        public string spacetype { get; set; }
        public string costcenter { get; set; }
        public string spaceid { get; set; }
        public string AurId { get; set; }
        public string BDGID { get; set; }
        public string city { get; set; }
        public string Statusid { get; set; }
        public string shift { get; set; }
        public string DeptName { get; set; }
        public string Messege { get; set; }
        public string TmpReqseqid { get; set; }
    }
}