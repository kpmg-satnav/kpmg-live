﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EmployeeMappingVM
/// </summary>
public class EmployeeMappingVM
{
    public string SRD_SPC_ID { get; set; }
    public string SRD_SH_CODE { get; set; }
    public DateTime SRN_FROM_DATE { get; set; }
    public DateTime SRN_TO_DATE { get; set; }
}