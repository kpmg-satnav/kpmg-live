﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SpaceSubTypeVM
/// </summary>
public class SpaceSubTypeVM
{
    public int SST_ID { get; set; }
    public string SST_CODE { get; set; }
    public string SST_NAME { get; set; }
    public string SST_STA_ID { get; set; }
    public string SST_SPC_TYPE { get; set; }
    public string SST_REM { get; set; }
    public string SST_COLOR { get; set; }
}

public class SpaceTypeVM
{
    public string SPC_TYPE_CODE { get; set; }
    public string SPC_TYPE_NAME { get; set; }
}