﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;


public class SummaryGridCols
{
    public string headerName { get; set; }
    public string field { get; set; }
    public int width { get; set; }
    public string cellClass { get; set; }
    public bool suppressMenu { get; set; }
}
