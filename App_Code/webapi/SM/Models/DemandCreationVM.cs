﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DemandCreationVM
/// </summary>
public class DemandCreationVM
{
    public string Locations { get; set; }
    public string Geography { get; set; }
    public string Verticals { get; set; }
    public string Function { get; set; }
    public string ParentEntity { get; set; }

    public Months Months { get; set; }
    public CurrentyearQ1 CurrentyearQ1 { get; set; }
    public NextYearQ1 NextYearQ1 { get; set; }


}

public class Months
{
    public int Jan { get; set; }
    public int Feb { get; set; }
    public int Mar { get; set; }
    public int Apr { get; set; }
    public int May { get; set; }
    public int Jun { get; set; }
    public int Jul { get; set; }
    public int Aug { get; set; }
    public int Sep { get; set; }
    public int Oct { get; set; }
    public int Nov { get; set; }
    public int Dec { get; set; }


}
public class CurrentyearQ1
{
    public int CYQ1 { get; set; }
    public int CYQ2 { get; set; }
    public int CYQ3 { get; set; }
    public int CYQ4 { get; set; }

}
 
public class NextYearQ1
{
    public int NYQ1 { get; set; }
    public int NYQ2 { get; set; }
    public int NYQ3 { get; set; }
    public int NYQ4 { get; set; }
}