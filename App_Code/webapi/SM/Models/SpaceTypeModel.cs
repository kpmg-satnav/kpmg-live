﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class SpaceTypeModel
{
    public string SPC_Code { get; set; }
    public string SPC_Name { get; set; }
    public string SPC_Status_Id { get; set; }
    public string SPC_REM { get; set; }
    public string SPC_COLOR { get; set; }
}



public class SpaceTypeCost
{
    public string FLR_CODE { get; set; }
    public string FLR_NAME { get; set; }
    public List<seattype> lstseattype { get; set; }
    public List<seattype> variableCost { get; set; }
    public string SPCTYPE_COST { get; set; }
}

public class locationdata
{
    public string LCM_CODE { get; set; }
    public string LCM_NAME { get; set; }
    public string SEAT_COST { get; set; }

}
public class KeyVal
{
    public object Key { get; set; }
    public object Value { get; set; }
}

public class SeatType
{
    public string Workstation { get; set; }
    public string Cubicle { get; set; }
    public string Cabin { get; set; }
}


