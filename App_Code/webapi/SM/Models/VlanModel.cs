﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for VlanModel
/// </summary>
public class VlanModel
{
    public string FLAG { get; set; }
    public int VL_VNO { get; set; }
    public string VL_Name { get; set; }
    public string VL_Code { get; set; }
    public string VL_Email { get; set; }
    public string VL_STATUS { get; set; }
}