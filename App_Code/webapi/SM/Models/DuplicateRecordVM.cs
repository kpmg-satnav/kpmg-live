﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for DuplicateRecordVM
/// </summary>

public class DuplicateRecordVM
{
    public List<Floorlst> flrlst { get; set; }
    public string Type { get; set; }
    public string VERTICAL { get; set; }
    public string COSTCENTER { get; set; }
}


public class DuplicateGrid
{
    public string LCM_NAME { get; set; }
    public string SSA_SPC_ID { get; set; }
    public string VER_NAME { get; set; }
    public string COST_CENTER_NAME { get; set; }
    public string VER_CHE_CODE { get; set; }
    public string SSAD_AUR_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
}

