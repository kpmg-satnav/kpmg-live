﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class SpaceOccupancyDetials
{
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    public string Request_Type { get; set; }
    public string Type { get; set; }
    public string CNP_NAME { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
    public List<Costcenterlst> Costcenterlst { get; set; }
    public string SearchValue { get; set; }
    public string VERTICAL { get; set; }
    public string COSTCENTER { get; set; }
    public string BH1 { get; set; }
    public string BH2 { get; set; }
    public string PE { get; set; }
    public string CE { get; set; }
}

public class SpaceOccupancyData
{
    public DateTime FROM_DATE { get; set; }
    public DateTime TO_DATE { get; set; }
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public string FLR_NAME { get; set; }
    public string ENTITY { get; set; }
    public string CHILD_ENTITY { get; set; }
    public string VER_NAME { get; set; }
    public string DEP_NAME { get; set; }
    public string STATUS { get; set; }
    public string REQUEST_TYPE { get; set; }
    public string SPACE_TYPE { get; set; }
    public string SEAT_TYPE { get; set; }
    public string SHIFT { get; set; }
    public string EMP_ID { get; set; }
    public string AUR_ID { get; set; }
    public string OCCUP_COUNT { get; set; }
    public string SPC_ID { get; set; }
    public string AUR_TYPE { get; set; }
    public string AUR_REPORTING_TO { get; set; }
    public string EXTENSION { get; set; }
    public string OVERALL_COUNT { get; set; }
    public string BHO_NAME { get; set; }
    public string BHT_NAME { get; set; }

}
public class OccupancyReport
{
    public enum ReportFormat { PDF = 1, Word = 2, Excel = 3 };

    public OccupancyReport()
    {
        OccupncyData = new List<ReportDataset>();
    }

    public string Name { get; set; }

    public string ReportLanguage { get; set; }

    public string FileName { get; set; }

    public string ReportTitle { get; set; }

    public List<ReportDataset> OccupncyData { get; set; }

    public ReportFormat Format { get; set; }

    public bool ViewAsAttachment { get; set; }

    private string mimeType;

    public class ReportDataset
    {
        public string DatasetName { get; set; }
        public List<object> DataSetData { get; set; }
    }

    public string ReportExportFileName
    {
        get
        {
            return string.Format("attachment; filename={0}.{1}", this.ReportTitle, ReportExportExtention);
        }
    }

    public string ReportExportExtention
    {
        get
        {
            switch (this.Format)
            {
                case OccupancyReport.ReportFormat.Word: return ".doc";
                case OccupancyReport.ReportFormat.Excel: return ".xls";
                default:
                    return ".pdf";
            }
        }
    }
    public string LastmimeType
    {
        get
        {
            return mimeType;
        }
    }

    public byte[] RenderReport()
    {
        //geting repot data from the business object
        //creating a new report and setting its path
        LocalReport localReport = new LocalReport();
        localReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath(this.FileName);

        //adding the reort datasets with there names
        foreach (var dataset in this.OccupncyData)
        {
            ReportDataSource reportDataSource = new ReportDataSource(dataset.DatasetName, dataset.DataSetData);
            localReport.DataSources.Add(reportDataSource);
        }
        //enabeling external images
        localReport.EnableExternalImages = true;
        localReport.SetParameters(new ReportParameter("ReportTitle", this.ReportTitle));

        //preparing to render the report

        string reportType = this.Format.ToString();

        string encoding;
        string fileNameExtension;
        string deviceInfo =
        "<DeviceInfo>" +
        "  <OutputFormat>" + this.Format.ToString() + "</OutputFormat>" +
        "</DeviceInfo>";

        Warning[] warnings;
        string[] streams;
        byte[] renderedBytes;

        //Render the report
        renderedBytes = localReport.Render(
            reportType,
            deviceInfo,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);
        return renderedBytes;
    }
}
