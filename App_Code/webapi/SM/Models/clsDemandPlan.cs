﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for clsDemandPlan
/// </summary>
public class clsDemandPlan
{    
        public string DemandID { get; set; }
        public string DemandRaisedBy { get; set; }
        public string DeamndRaisedOn { get; set; }
        public string DemandApprovedBy { get; set; }
        public string DemandApprovedOn { get; set; }
        public string DemandStatus { get; set; }
        public string FulfilmentStatus { get; set; }
        public string Geography { get; set; }
        public string Location { get; set; }
        public string Floor { get; set; }
        public string Function { get; set; }
        public string SubFunction { get; set; }
        public string Teams { get; set; }
        public string Month1 { get; set; }
        public string Month2 { get; set; }
        public string Month3 { get; set; }
        public string Month4 { get; set; }
        public string Month5 { get; set; }
        public string Month6 { get; set; }
        public string Month7 { get; set; }
        public string Month8 { get; set; }
        public string Month9 { get; set; }
        public string Month10 { get; set; }
        public string Month11 { get; set; }
        public string Month12 { get; set; }
        public string CurrentYearQ1 { get; set; }
        public string CurrentYearQ2 { get; set; }
        public string CurrentYearQ3 { get; set; }
        public string CurrentYearQ4 { get; set; }
        public string NextYearQ1 { get; set; }
        public string NextYearQ2 { get; set; }
        public string NextYearQ3 { get; set; }
        public string NextYearQ4 { get; set; }
        

}