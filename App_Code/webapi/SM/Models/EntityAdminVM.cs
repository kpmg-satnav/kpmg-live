﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EntityAdminVM
/// </summary>
public class EntityAdminVM
{
    public string ADM_Code { get; set; }
    public string ADM_Name { get; set; }
    public string ADM_Status_Id { get; set; }
    public string ADM_REM { get; set; }
}