﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class SpaceAvailDetails
{
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
    //public string Request_Type { get; set; }
    public string Type { get; set; }
    public string CNP_NAME { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
    public string SearchValue { get; set; }
}
public class SpaceAvailData
{

    public DateTime FROM_DATE { get; set; }
    public DateTime TO_DATE { get; set; }
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public string FLR_NAME { get; set; }
    public string SPC_TYPE { get; set; }
    public string SPACE_SUB_TYPE { get; set; }
    public string SPACE_SUB_TYPE_CODE { get; set; }
    //public string REQUEST_TYPE { get; set; }
    //public string TOTAL_EMP { get; set; }
    //public string VACANT_EMP { get; set; }
    public string TOTAL_SEATS_COUNT { get; set; }
    public int VACANT_SEATS { get; set; }
    public string CNY_CODE { get; set; }
    public string CTY_CODE { get; set; }
    public string LCM_CODE { get; set; }
    public string FLR_CODE { get; set; }
    public string SPC_TYPE_CODE { get; set; }
    public string SST_CODE { get; set; }
    public string SPC_ID { get; set; }
    public string Type { get; set; }
    public string CNP_NAME { get; set; }
    public string CHE_CODE { get; set; }
}
public class AvailReport
{

    public enum ReportFormat { PDF = 1, Word = 2, Excel = 3 };

    public AvailReport()
    {
        AvailData = new List<ReportDataset>();
    }

    public string Name { get; set; }

    public string ReportLanguage { get; set; }

    public string FileName { get; set; }

    public string ReportTitle { get; set; }

    public List<ReportDataset> AvailData { get; set; }

    public ReportFormat Format { get; set; }

    public bool ViewAsAttachment { get; set; }

    private string mimeType;

    public class ReportDataset
    {
        public string DatasetName { get; set; }
        public List<object> DataSetData { get; set; }
    }

    public string ReportExportFileName
    {
        get
        {
            return string.Format("attachment; filename={0}.{1}", this.ReportTitle, ReportExportExtention);
        }
    }

    public string ReportExportExtention
    {
        get
        {
            switch (this.Format)
            {
                case AvailReport.ReportFormat.Word: return ".doc";
                case AvailReport.ReportFormat.Excel: return ".xls";
                default:
                    return ".pdf";
            }
        }
    }
    public string LastmimeType
    {
        get
        {
            return mimeType;
        }
    }

    public byte[] RenderReport()
    {
        //geting repot data from the business object
        //creating a new report and setting its path
        LocalReport localReport = new LocalReport();
        localReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath(this.FileName);

        //adding the reort datasets with there names
        foreach (var dataset in this.AvailData)
        {
            ReportDataSource reportDataSource = new ReportDataSource(dataset.DatasetName, dataset.DataSetData);
            localReport.DataSources.Add(reportDataSource);
        }
        //enabeling external images
        localReport.EnableExternalImages = true;
        localReport.SetParameters(new ReportParameter("ReportTitle", this.ReportTitle));

        //preparing to render the report

        string reportType = this.Format.ToString();

        string encoding;
        string fileNameExtension;
        string deviceInfo =
        "<DeviceInfo>" +
        "  <OutputFormat>" + this.Format.ToString() + "</OutputFormat>" +
        "</DeviceInfo>";

        Warning[] warnings;
        string[] streams;
        byte[] renderedBytes;

        //Render the report
        renderedBytes = localReport.Render(
            reportType,
            deviceInfo,
            out mimeType,
            out encoding,
            out fileNameExtension,
            out streams,
            out warnings);
        return renderedBytes;
    }

}