﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SUReport_hist_VM
/// </summary>
public class SUReport_hist_VM
{
    public string Country { get; set; }
    public string Filter { get; set; }
    public string VER_CODE { get; set; }
    public string DEPTID { get; set; }
    public string DEPNAME { get; set; }
    public string SPAN_LEADER_ID { get; set; }
    public string SPAN_LEADER_NAME { get; set; }
    public double ALLOCCNT { get; set; }
    public double HEADCOUNT { get; set; }
    public string SU { get; set; }
    public string SPANSU { get; set; }
}

public class SUbyFilters
{
    public List<Countrylst> Countrylst { get; set; }
    public List<Citylst> Citylst { get; set; }
    public List<Locationlst> Locationlst { get; set; }
    public List<Verticallst> Verticals { get; set; }
    public List<Costcenterlst> Costcenterlst { get; set; }
    public List<Verticallst> Verticallst { get; set; }
    public string Grouplst { get; set; }
    public string Grouptext { get; set; }
    public string Type { get; set; }
}

public class AreaWiseCostFilters
{
    //public string Country { get; set; }
    //public string City { get; set; }
    public List<Countrylst> CNY_NAME { get; set; }
    public List<Locationlst> LCM_NAME { get; set; }
    public List<Verticallst> VER_NAME { get; set; }
    public List<Costcenterlst> DEP_NAME { get; set; }
    public string SHIFT { get; set; }
    public string GRPBY { get; set; }
    public string GRPTEXT { get; set; }
    public string CRITERIA { get; set; }
    public string TRNDTYPE { get; set; }
    public string NO_OF_SEATS { get; set; }
    public string ALLOC_SEATS_COUNT { get; set; }
    public double UNITCOST { get; set; }
    public string TOTAL_BUSS_UNITCOST { get; set; }
}

public class AreaWiseCostDetails
{
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public string FLR_NAME { get; set; }
    public string SPC_TYPE_NAME { get; set; }
    public string CNY_CODE { get; set; }
    public string CTY_CODE { get; set; }
    public string LCM_CODE { get; set; }
    public string FLR_CODE { get; set; }
    public string VER_NAME { get; set; }
    public string COST_CENTER_NAME { get; set; }
    public string SH_NAME { get; set; }
    public string SPC_ID { get; set; }
    public string PE_NAME { get; set; }
    public string CHE_NAME { get; set; }
    public int UNIT_COST { get; set; }


    public string Location { get; set; }
    public string Floor { get; set; }
    public string Vertical { get; set; }
    public string Costcenter { get; set; }
    public string Shift { get; set; }
    public string ParentEntity { get; set; }
    public string ChildEntity { get; set; }
    public string Type { get; set; }
}