﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class LeaveRequestModel
{
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string SelectedEmployee { get; set; }
    public string LeaveReason { get; set; }
}

public class LeaveGridData
{
    public string ID { get; set; }
    public string NAME { get; set; }
    public string AUR_EMAIL { get; set; }
    public string AUR_CITY { get; set; }
    public string AUR_LOCATION { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string Reason { get; set; }
}