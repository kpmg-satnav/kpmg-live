﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
/// <summary>
/// Summary description for PropertyWiseSpaceReportModel
/// </summary>
public class PropertyWiseSpaceReport
{
    public List<Countrylst> cnylst { get; set; }
    public List<Citylst> ctylst { get; set; }
    public List<Locationlst> loclst { get; set; }
    public List<Towerlst> twrlst { get; set; }
    public List<Floorlst> flrlst { get; set; }
    public string Request_Type { get; set; }
    public string Columns { get; set; }
    public string Type { get; set; }
    public string CNP_NAME { get; set; }
    public Nullable<System.DateTime> FromDate { get; set; }
    public Nullable<System.DateTime> ToDate { get; set; }
}

public class PropertySpacedata
{

    public string TOTAL_AREA { get; set; }
    public string Occupaid { get; set; }
    public string TOTAL { get; set; }
    public string vacant { get; set; }
    public string OCCUPAID_COUNT { get; set; }
    public string FLR_NAME { get; set; }
    public string FLR_CODE { get; set; }
    public string PM_PPT_PM_REQ_ID { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string PM_AR_CARPET_AREA { get; set; }
    public string PM_AR_BUA_AREA { get; set; }
    public string PM_AR_COM_AREA { get; set; }
    public double PM_AR_RENT_AREA { get; set; }
    public string PM_AR_USABEL_AREA { get; set; }
    public string PM_AR_PLOT_AREA { get; set; }
    public double PM_PUR_PRICE { get; set; }
    public double PM_INS_SNO { get; set; }
    public double PM_INS_CREATED_DT { get; set; }
    public double PM_INS_END_DT { get; set; }
    public double PM_INS_AMOUNT { get; set; }
    public double PM_LES_PM_LR_REQ_ID { get; set; }
    public string PM_LES_ENTITLED_AMT { get; set; }
    public string PM_LES_BASIC_RENT { get; set; }
    public string PM_LES_SEC_DEP_MONTHS { get; set; }
    public double PM_LES_TOT_RENT { get; set; }
    public string PM_LES_RENT_PER_SQFT_CARPET { get; set; }


    public string COUNTRY { get; set; }
    public string PM_PPT_NAME { get; set; }
    public string PM_PPT_ADDRESS { get; set; }
    public string STATUS { get; set; }
    public string CITY { get; set; }
    public string LOCATION { get; set; }
    public string EMPLOYEE_ID { get; set; }
    public string EMPLOYEE_NAME { get; set; }
    public double SECURITY_DEPOSIT { get; set; }
    public string LEASE_NAME { get; set; }
    public string LANDLORD_NAME { get; set; }
    public double MONTHLY_RENT { get; set; }
    public double RECOVERY_AMOUNT { get; set; }
    public double BUILTUP_AREA { get; set; }
    public double MAINTENANCE_CHARGES { get; set; }
    public double CARPET_AREA { get; set; }
    public double RENT_AREA { get; set; }
    public string LANDLORD_ADDRESS { get; set; }
    public string branch_code { get; set; }
    public string STATE { get; set; }
    public double SERVICE_TAX { get; set; }
    public string RENT_REVISION { get; set; }
    public string PN_LEASE_TYPE { get; set; }
    public string PN_PROPERTYTYPE { get; set; }
    public string lease_payment_terms { get; set; }
    public string COMPLETE_ADDRESS { get; set; }
    public double PAID_AMOUNT { get; set; }
    public double Total_Rent { get; set; }
    public string RENEWAL_STATUS { get; set; }
    public string LEASE_ESCALATION { get; set; }
    public DateTime? LEASE_START_DATE { get; set; }
    public DateTime? EXTESNION_TODATE { get; set; }
    public DateTime? LEASE_END_DATE { get; set; }
    public DateTime? EXPIRY_AGREEMENT_DATE { get; set; }
}
