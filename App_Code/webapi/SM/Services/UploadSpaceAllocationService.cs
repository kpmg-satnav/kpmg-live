﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for UploadSpaceAllocationService
/// </summary>
public class UploadSpaceAllocationService
{
    public List<UploadAllocationDataDumpVM> DownloadTemplate(UploadSpacesVM spcdet)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(spcdet.flrlst);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@DWNTYPE", SqlDbType.VarChar);
        param[2].Value = spcdet.ALLOCSTA;
        param[3] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];

        UploadAllocationDataDumpVM dwndata;
        List<UploadAllocationDataDumpVM> dwndatalst = new List<UploadAllocationDataDumpVM>();
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_DWNLD_ALLOC_TEMPLATE", param))
        {
            while (sdr.Read())
            {
                dwndata = new UploadAllocationDataDumpVM();
                dwndata.City = sdr["City"].ToString();
                dwndata.Location = sdr["Location"].ToString();
                dwndata.Tower = sdr["Tower"].ToString();
                dwndata.Floor = sdr["Floor"].ToString();
                dwndata.SpaceID = sdr["SpaceID"].ToString();
                dwndata.SeatType = sdr["SeatType"].ToString();
                dwndata.Vertical = sdr["Vertical"].ToString();
                dwndata.Entity = sdr["Entity"].ToString();
                dwndata.Costcenter = sdr["Costcenter"].ToString();
                dwndata.EmployeeID = sdr["EmployeeID"].ToString();
                dwndata.EmployeeName = sdr["EmployeeName"].ToString();
                dwndata.FromDate = sdr["FromDate"].ToString();
                dwndata.ToDate = sdr["ToDate"].ToString();
                dwndata.FromTime = sdr["FromTime"].ToString();
                dwndata.ToTime = sdr["ToTime"].ToString();
                dwndatalst.Add(dwndata);
            }
            sdr.Close();
        }

        return dwndatalst;

    }

    public object UploadTemplate(HttpRequest httpRequest)
    {
        List<UploadAllocationDataVM> uadmlst;
        try
        {
            uadmlst = GetDataTableFrmReq(httpRequest);

            if (HttpContext.Current.Session["ErrorMsg"].ToString() == "")
            {
                String jsonstr = httpRequest.Params["CurrObj"];

                UPLTYPEVM UVM = (UPLTYPEVM)Newtonsoft.Json.JsonConvert.DeserializeObject<UPLTYPEVM>(jsonstr);

                string str = "INSERT INTO " + HttpContext.Current.Session["TENANT"] + "." + "TEMP_SPACEALLOCATION_DATA (CITY,LOCATION,TOWER,FLOOR,SPACE_ID,SEATTYPE,VERTICAL,ENTITY,COSTCENTER,EMP_ID,EMP_NAME,FROM_DATE,TO_DATE,FROM_TIME,TO_TIME,UPLOADEDBY) VALUES ";
                string str1 = "";
                int retval, cnt = 1;
                foreach (UploadAllocationDataVM uadm in uadmlst)
                {


                    if ((cnt % 1000) == 0)
                    {
                        str1 = str1.Remove(str1.Length - 1, 1);
                        retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                        if (retval == -1)
                        {
                            return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
                        }
                        else
                        {
                            str1 = "";
                            cnt = 1;
                        }

                    }

                    str1 = str1 + string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}'),",
                              uadm.City,
                              uadm.Location.Replace("'", "''"),
                              uadm.Tower.Replace("'", "''"),
                              uadm.Floor,
                              uadm.SpaceID,
                              uadm.SeatType,
                              uadm.Vertical.Replace("'", "''"),
                              uadm.Entity,
                              uadm.Costcenter.Replace("'", "''"),
                              uadm.EmployeeID,
                              uadm.EmployeeName.Replace("'", "''"),
                              uadm.FromDate,
                              uadm.ToDate,
                              uadm.FromTime,
                              uadm.ToTime,
                              HttpContext.Current.Session["Uid"]

                            );
                    cnt = cnt + 1;
                }
                str1 = str1.Remove(str1.Length - 1, 1);
                retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                if (retval != -1)
                {
                    SqlParameter[] param = new SqlParameter[4];
                    param[0] = new SqlParameter("@UPLTYPE", SqlDbType.VarChar);
                    param[0].Value = UVM.UplAllocType;
                    param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                    param[1].Value = HttpContext.Current.Session["UID"];
                    param[2] = new SqlParameter("@UPLOPTIONS", SqlDbType.VarChar);
                    param[2].Value = UVM.UplOptions;
                    param[3] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
                    param[3].Value = HttpContext.Current.Session["COMPANYID"];
                    DataSet dt = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SMS_UPLOAD_SPACE_ALLOC", param);
                    if (dt.Tables != null)
                        return new { Message = MessagesVM.UAD_UPLOK, data = dt };
                    else
                        return new { Message = MessagesVM.UAD_UPLVALDAT, data = dt };
                }
                else
                    return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
            }
            else
            {
                return new { Message = HttpContext.Current.Session["ErrorMsg"].ToString(),  data = (object)null };
            }
        }
        catch (Exception ex)
        {
                return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
           
        }
    }

    string ErrorMsg = "";
    public List<UploadAllocationDataVM> GetDataTableFrmReq(HttpRequest httpRequest)
    {
        //DataTable dt = new DataTable();
        DataSet dt = new DataSet();

        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);

        //var uplst = CreateExcelFile.ReadAsList(filePath, "all");
        var uplst = CreateExcelFile.ReadAsList(filePath);
        if (uplst.Count != 0)
        {

            uplst.RemoveAt(0);
        }
        return uplst;

    }

    public class CheckExcelColumnNames
    {
        public static string City = "City";
        public static string Location = "Location";
        public static string Tower = "Tower";
        public static string Floor = "Floor";
        public static string SpaceID = "Space ID";
        public static string SeatType = "Seat Type";
        public static string Vertical = "Business Unit";
        public static string Entity = "Entity";
        public static string Costcenter = "Costcenter";
        public static string EmployeeID = "Employee ID";
        public static string EmployeeName = "Employee Name";
        public static string FromDate = "From Date (in MM/dd/yyyy)";
        public static string ToDate = "To Date  (in MM/dd/yyyy)";
        public static string FromTime = "From Time  (in  HH:MM)";
        public static string ToTime = "To Time in (HH:MM)";
    }
}