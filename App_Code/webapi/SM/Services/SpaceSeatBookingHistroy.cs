﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SpaceSeatBookingHistroy
/// </summary>
public class SpaceSeatBookingHistroy
{
    public object SpaceSeatBookingHistry()
    {

        try
        {
            DataSet ds = new DataSet();
            List<CustomizedGridCols> gridcols = new List<CustomizedGridCols>();
            CustomizedGridCols gridcolscls;
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@USER", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["Uid"];
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SPACE_SEAT_BOOKING_HISTORY", param);
            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols };
        }
        catch (SqlException) { throw; }

    }
}