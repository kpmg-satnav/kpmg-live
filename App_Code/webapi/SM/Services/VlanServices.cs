﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using UtiltiyVM;
using System.Data.SqlClient;


/// <summary>
/// Summary description for VlanServices
/// </summary>
public class VlanServices
{
    SubSonic.StoredProcedure sp;
   
    public object InsertUpdate(VlanModel VM)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@FLAG", VM.FLAG);
            param[1] = new SqlParameter("@VL_Code", VM.VL_Code);
            param[2] = new SqlParameter("@VL_Name", VM.VL_Name);
            param[3] = new SqlParameter("@VL_Email", VM.VL_Email);
            param[4] = new SqlParameter("@VL_STATUS", VM.VL_STATUS);
            param[5] = new SqlParameter("@AUR_ID", HttpContext.Current.Session["UID"]);
            param[6] = new SqlParameter("@VL_VNO", VM.VL_VNO);

            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "INSERT_UPDATE_VLAN_MASTERS", param))
            {
                if (dr.Read())
                {
                    //if (dr["FLAG"] == "Success")
                    //{
                       return new { Message = MessagesVM.VLAN_OK, data = (object)null };
                    //}
                    //else
                    //    return new { Message = MessagesVM.VLAN_EXISTS, data = (object)null };
                }
                else
                    return new { Message = MessagesVM.VLAN_NO_REC, data = (object)null };
            }

        }
        catch
        {
            throw;
        }
    }

    public IEnumerable<VlanModel> GetVlanTypeBindGrid()
    {
        try
        {
            using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_VLAN_MASTER_GRID").GetReader())
            {
                List<VlanModel> RTList = new List<VlanModel>();
                while (reader.Read())
                {
                    RTList.Add(new VlanModel()
                    {
                        VL_VNO = Convert.ToInt32(reader["VL_VNO"]),
                        VL_Name = reader["VL_NAME"].ToString(),
                        VL_Code = reader["VL_Code"].ToString(),
                        VL_Email = reader["VL_Email"].ToString(),
                        VL_STATUS = reader["VL_STA_ID"].ToString(),


                    });
                }
                reader.Close();
                return RTList;
            }
        }
        catch
        {
            throw;
        }
    }

}

