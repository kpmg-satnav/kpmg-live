﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for DemandCreationService
/// </summary>
public class DemandCreationService
{
    SubSonic.StoredProcedure sp;
    public object GetDemandRequests()
    {
        DataSet ds = new DataSet();
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "DC_VIEW_REQ");
            //sp.Command.AddParameter("@USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            //sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            ds = sp.GetDataSet();
            return ds.Tables[0];

        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
    public object Submit(DemandCreationVM DCVM)
    {
        List<Months> months = new List<Months>();
        List<CurrentyearQ1> CurrentyearQ1 = new List<CurrentyearQ1>();
        List<NextYearQ1> NextYearQ1 = new List<NextYearQ1>();
        SqlParameter[] param = new SqlParameter[25];
        param[0] = new SqlParameter("@Geography", SqlDbType.NVarChar);
        param[0].Value = DCVM.Geography;
        param[1] = new SqlParameter("@Locations", SqlDbType.NVarChar);
        param[1].Value = DCVM.Locations;
        param[2] = new SqlParameter("@Verticals", SqlDbType.NVarChar);
        param[2].Value = DCVM.Verticals;
        param[3] = new SqlParameter("@Function", SqlDbType.NVarChar);
        param[3].Value = DCVM.Function;
        param[4] = new SqlParameter("@ParentEntity", SqlDbType.NVarChar);
        param[4].Value = DCVM.ParentEntity;
        param[5] = new SqlParameter("@Jan", SqlDbType.Int);
        param[5].Value = DCVM.Months.Jan;
        param[6] = new SqlParameter("@Feb", SqlDbType.Int);
        param[6].Value = DCVM.Months.Feb;
        param[7] = new SqlParameter("@Mar", SqlDbType.Int);
        param[7].Value = DCVM.Months.Mar;
        param[8] = new SqlParameter("@Apr", SqlDbType.Int);
        param[8].Value = DCVM.Months.Apr;
        param[9] = new SqlParameter("@May", SqlDbType.Int);
        param[9].Value = DCVM.Months.May;
        param[10] = new SqlParameter("@Jun", SqlDbType.Int);
        param[10].Value = DCVM.Months.Jun;
        param[11] = new SqlParameter("@Jul", SqlDbType.Int);
        param[11].Value = DCVM.Months.Jul;
        param[12] = new SqlParameter("@Aug", SqlDbType.Int);
        param[12].Value = DCVM.Months.Aug;
        param[13] = new SqlParameter("@Sep", SqlDbType.Int);
        param[13].Value = DCVM.Months.Sep;
        param[14] = new SqlParameter("@Oct", SqlDbType.Int);
        param[14].Value = DCVM.Months.Oct;
        param[15] = new SqlParameter("@Nov", SqlDbType.Int);
        param[15].Value = DCVM.Months.Nov;
        param[16] = new SqlParameter("@Dec", SqlDbType.Int);
        param[16].Value = DCVM.Months.Dec;
        param[17] = new SqlParameter("@CYQ1", SqlDbType.Int);
        param[17].Value = DCVM.CurrentyearQ1.CYQ1;
        param[18] = new SqlParameter("@CYQ2", SqlDbType.Int);
        param[18].Value = DCVM.CurrentyearQ1.CYQ2;
        param[19] = new SqlParameter("@CYQ3", SqlDbType.Int);
        param[19].Value = DCVM.CurrentyearQ1.CYQ3;
        param[20] = new SqlParameter("@CYQ4", SqlDbType.Int);
        param[20].Value = DCVM.CurrentyearQ1.CYQ4;
        param[21] = new SqlParameter("@NYQ1", SqlDbType.Int);
        param[21].Value = DCVM.NextYearQ1.NYQ1;
        param[22] = new SqlParameter("@NYQ2", SqlDbType.Int);
        param[22].Value = DCVM.NextYearQ1.NYQ2;
        param[23] = new SqlParameter("@NYQ3", SqlDbType.Int);
        param[23].Value = DCVM.NextYearQ1.NYQ3;
        param[24] = new SqlParameter("@NYQ4", SqlDbType.Int);
        param[24].Value = DCVM.NextYearQ1.NYQ4;
        DataSet ds = new DataSet();
        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "DC_RAISE_REQ", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                    return new { Message = MessagesVM.DemandCreation, data = dr["MSG"].ToString() };
                else
                    return new { Message = dr["MSG"].ToString(), data = (object)null };
            }
        }
        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

}