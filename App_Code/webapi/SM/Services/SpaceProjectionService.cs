﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SpaceProjectionService
/// </summary>
public class SpaceProjectionService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    SpaceExtensionVM sevm;
    string ReqID = "";

    public object UploadTemplate(HttpRequest httpRequest)
    {
        try
        {
            List<OfficeSpacePlanning> uadmlst = GetDataTableFrmReq(httpRequest);

            String jsonstr = httpRequest.Params["CurrObj"];

            //UPLTYPEVM UVM = (UPLTYPEVM)Newtonsoft.Json.JsonConvert.DeserializeObject<UPLTYPEVM>(jsonstr);


            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "OSP_EXCEL_COUNT");
            sp.Command.AddParameter("@USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ReqID = (string)sp.ExecuteScalar();

            string str = "INSERT INTO " + HttpContext.Current.Session["TENANT"] + "." + "SP_SPACE_PLANNING_EXCEL_UPL (EU_CITY,EU_LOC,EU_TOW,EU_BU,EU_LOB,EU_GRADE,EU_Q1,EU_Q2,EU_Q3,EU_Q4,EU_FY1,EU_FY2,EU_FY3,EU_CREATED_BY,EU_COMPANYID,EU_STA_ID,EU_REQ_ID) VALUES ";
            string str1 = "";
            int retval, cnt = 1;
            foreach (OfficeSpacePlanning uadm in uadmlst)
            {


                if ((cnt % 1000) == 0)
                {
                    str1 = str1.Remove(str1.Length - 1, 1);
                    retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                    if (retval == -1)
                    {
                        return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
                    }
                    else
                    {
                        str1 = "";
                        cnt = 1;
                    }

                }

                str1 = str1 + string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}'),",
                          uadm.City,
                          uadm.Location.Replace("'", "''"),
                          uadm.Facility.Replace("'", "''"),
                          uadm.BU.Replace("'", "''"),
                          uadm.LOB.Replace("'", "''"),
                          uadm.Grade.Replace("'", "''"),
                          uadm.Q1,
                          uadm.Q2,
                          uadm.Q3,
                          uadm.Q4,
                          uadm.FY1,
                          uadm.FY2,
                          uadm.FY3,
                          HttpContext.Current.Session["Uid"],
                          HttpContext.Current.Session["COMPANYID"],
                          2001,
                          ReqID
                        );
                cnt = cnt + 1;
            }
            str1 = str1.Remove(str1.Length - 1, 1);
            retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
            if (retval != -1)
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                param[0].Value = HttpContext.Current.Session["UID"];
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
                param[1].Value = HttpContext.Current.Session["COMPANYID"];
                param[2] = new SqlParameter("@REQID", SqlDbType.VarChar);
                param[2].Value = ReqID;
                DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "BIND_SPACE_PLANNING_GRID", param);
                return new { Message = MessagesVM.UAD_UPLOK, data = new { LST = dt, REQID = ReqID } };
            }
            else
                return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public List<OfficeSpacePlanning> GetDataTableFrmReq(HttpRequest httpRequest)
    {
        DataTable dt = new DataTable();

        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);


        var uplst = CreateExcelFile.ReadAsSpacePlanningList(filePath);
        if (uplst.Count != 0)
            uplst.RemoveAt(0);
        return uplst;

    }

    public object ComputeData(OSPSaveList ospobj)
    {
        try
        {
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "OSP_COMPUTE_DATA");          
            //sp.Execute();
            //return new { Message = MessagesVM.PRJ_INSERTED, data = sprjVm.ssp };
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@REQ_ID", SqlDbType.VarChar);
            param[0].Value = ospobj.REQID;
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "OSP_COMPUTE_DATA", param);
            return ds;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public object SaveDetails(OSPSaveList ospobj)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[4];
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "OSP_SAVE_DATA");
            //sp.Command.AddParameter("@CTY", sprjVm.City, DbType.String);
            //sp.Command.AddParameter("@LOC", sprjVm.Location, DbType.String);
            //sp.Command.AddParameter("@TOW", sprjVm.Facility, DbType.String);
            //sp.Command.AddParameter("@BU", sprjVm.BU, DbType.String);
            //sp.Command.AddParameter("@LOB", sprjVm.LOB, DbType.String);
            //sp.Command.AddParameter("@GRADE", sprjVm.Grade, DbType.String);
            //sp.Command.AddParameter("@REQ_WS", sprjVm.REQ_WS, DbType.String);
            //sp.Command.AddParameter("@REQ_CB", sprjVm.REQ_CB, DbType.String);
            param[0] = new SqlParameter("@SM_OSP_TVP", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(ospobj.savelst);
            param[1] = new SqlParameter("@USR_ID", HttpContext.Current.Session["UID"]);
            param[2] = new SqlParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);
            param[3] = new SqlParameter("@REQID", ospobj.REQID);
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "OSP_SAVE_DATA", param);
            return ds;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
}