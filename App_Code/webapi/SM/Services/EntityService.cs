﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for EntityService
/// </summary>
public class EntityService
{
    SubSonic.StoredProcedure sp;
    public int Save(EntityModel EntityModel)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "INSERT_CHILD_ENTITY_DATA");
        sp.Command.AddParameter("@CH_CODE", EntityModel.CH_ENTY_CODE, DbType.String);
        sp.Command.AddParameter("@CH_NAME", EntityModel.CH_ENTY_NAME, DbType.String);
        sp.Command.AddParameter("@CH_PE_CODE", EntityModel.CH_ENTY_PRNT_CODE, DbType.String);
        sp.Command.AddParameter("@CH_STATUS", EntityModel.CH_ENTY_STA_ID, DbType.String);
        sp.Command.AddParameter("@CH_REM", EntityModel.CH_ENTY_REM, DbType.String);
        sp.Command.AddParameter("@CH_CTREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
        int flag = (int)sp.ExecuteScalar();
        return flag;
    }
    public IEnumerable<EntityAdminModel> GetParentEntity()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ACTIVE_PARENT_ENTITY").GetReader();
        List<EntityAdminModel> ParentEntityList = new List<EntityAdminModel>();
        while (reader.Read())
        {
            ParentEntityList.Add(new EntityAdminModel()
            {
                PRNT_CODE = reader.GetValue(0).ToString(),
                PRNT_NAME = reader.GetValue(1).ToString()
            });
        }
        reader.Close();
        return ParentEntityList;
    }
    public Boolean UpdateChildEntity(EntityModel update)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "UPDATE_CHILD_ENTITY_DATA");
        sp.Command.AddParameter("@CH_CODE", update.CH_ENTY_CODE, DbType.String);
        sp.Command.AddParameter("@CH_NAME", update.CH_ENTY_NAME, DbType.String);
        sp.Command.AddParameter("@CH_PRNT_CODE", update.CH_ENTY_PRNT_CODE, DbType.String);
        sp.Command.AddParameter("@CH_STA_ID", update.CH_ENTY_STA_ID, DbType.String);
        sp.Command.AddParameter("@CH_REM", update.CH_ENTY_REM, DbType.String);
        sp.Command.Parameters.Add("@CH_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
        sp.Execute();
        return true;
    }

    public IEnumerable<EntityModel> BindGridEntity()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_ALL_CHILD_ENTITY_GRID]").GetReader();
        List<EntityModel> ChildEntityList = new List<EntityModel>();
        while (reader.Read())
        {
            ChildEntityList.Add(new EntityModel()
            {
                CH_ENTY_CODE = reader.GetValue(0).ToString(),
                CH_ENTY_NAME = reader.GetValue(1).ToString(),
                CH_ENTY_STA_ID = reader.GetValue(2).ToString(),
                CH_ENTY_REM = reader.GetValue(3).ToString(),
                CH_ENTY_PRNT_CODE = reader.GetValue(4).ToString()
            });
        }
        reader.Close();
        return ChildEntityList;
    }
}