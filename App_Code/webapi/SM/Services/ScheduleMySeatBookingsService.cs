﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ScheduleMySeatBookingsService
/// </summary>
public class ScheduleMySeatBookingsService
{
    SubSonic.StoredProcedure sp;


    DataSet ds;

    public object SearchSpaces(ScheduleMySeatBookingsModel Data)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SPACE_DETAILS_BY_DATE");
            sp.Command.Parameters.Add("@From_Date", Data.FROM_DATE, DbType.Date);
            sp.Command.Parameters.Add("@To_Date", Data.TO_DATE, DbType.Date);
            sp.Command.Parameters.Add("@FLR_ID", Data.flrlst, DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables;

        }
        catch (Exception e)
        {
            throw;
        }
    }

    public int AllocateSeats(List<SPACE_ALLOC_DETAILS_TIME> allocDetLst)
    {
        DataSet ds = new DataSet();

        int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };

        var allocDetLst1 = new List<SPACE_ALLOC_DETAILS_TIME>();

        //var details = allocDetLst.ToList();
        foreach (var item in allocDetLst)
        {
            item.AUR_ID = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(item.AUR_ID));
            allocDetLst1.Add(item);
        }



        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(allocDetLst1);
        // param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AURID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];
        param[3] = new SqlParameter("@Mode", SqlDbType.NVarChar);
        param[3].Value = "Schedule";

        //using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_ALLOC_MAP11", param))
        //{
        //    while (sdr.Read())
        //    {

        //    }
        //}


        Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SMS_SPACE_ALLOC_MAP11", param);
        return Convert.ToInt32(o);

    }
    public object getVLANS()
    {
        List<vlanlist> vlan = new List<vlanlist>();
        vlanlist VL;

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_VLAN");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                VL = new vlanlist();
                VL.VL_Code = sdr["VL_Code"].ToString();
                VL.VL_Name = sdr["VL_Name"].ToString();
                vlan.Add(VL);
            }
        }
        if (vlan.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = vlan };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object getEmployeeSpaceDetails(ScheduleMySeatBookingsModel Data)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "getEmployeeSpaceDetails");
            sp.Command.Parameters.Add("@AUR_ID", Data.AUR_ID, DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables;

        }
        catch (Exception e)
        {
            throw e;
        }
    }




}