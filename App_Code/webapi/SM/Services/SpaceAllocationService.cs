﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for SpaceAllocationService
/// </summary>
public class SpaceAllocationService
{
    SubSonic.StoredProcedure sp;
    List<SpaceRequistion_details> SpcVaclst;
    SpaceRequistion_details spcreqdet;

    public object GetPendingSpaceAllocationDetails()
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "View_Space_Allocation_Details");
            sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            using (IDataReader reader = sp.GetReader())
            {
                List<SpaceReq> spclist = new List<SpaceReq>();
                while (reader.Read())
                {
                    spclist.Add(new SpaceReq()
                    {
                        SRN_REQ_ID = reader["SRN_REQ_ID"].ToString(),
                        VER_NAME = reader["VER_NAME"].ToString(),
                        COST_CENTER_NAME = reader["COST_CENTER_NAME"].ToString(),
                        STA_DESC = reader["STA_DESC"].ToString(),
                        SRN_FROM_DATE = (DateTime)reader["SRN_FROM_DATE"],
                        SRN_TO_DATE = (DateTime)reader["SRN_TO_DATE"],
                        AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString(),
                        SRN_REQ_BY = reader["SRN_REQ_BY"].ToString(),
                        SRN_REQ_REM = reader["SRN_REQ_REM"].ToString(),
                        SRN_L1_REM = reader["SRN_L1_REM"].ToString(),
                        APP_REM = reader["SRN_ALLC_REM"].ToString(),
                        SRN_STA_ID = (int)reader["SRN_STA_ID"],
                        SRN_REQ_DT = (DateTime)reader["SRN_REQ_DT"],
                        SRN_SYS_PRF_CODE = reader["SRN_SYS_PRF_CODE"].ToString(),
                        SRN_VERTICAL = reader["SRN_VERTICAL"].ToString(),
                        SRN_COST_CENTER = reader["SRN_COST_CENTER"].ToString(),
                    });
                }
                reader.Close();
                if (spclist.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = spclist };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public object GetDetailsOnSelection(SpaceRequistion selectedid)
    {
        try
        {
            SpaceReqDetails SpaceReqDet = new SpaceReqDetails();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Edit_L1_Approval_Requisitions");
            sp.Command.AddParameter("@SRN_REQ_ID", selectedid.SRN_REQ_ID, DbType.String);

            DataSet ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    SpaceReqDet.cnylst = new List<Countrylst>();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SpaceReqDet.cnylst.Add(new Countrylst { CNY_CODE = Convert.ToString(dr["SRNB_CNY_CODE"]), CNY_NAME = Convert.ToString(dr["CNY_NAME"]), ticked = true });
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    SpaceReqDet.ctylst = new List<Citylst>();
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        SpaceReqDet.ctylst.Add(new Citylst { CTY_CODE = Convert.ToString(dr["SRNB_CTY_CODE"]), CTY_NAME = Convert.ToString(dr["CTY_NAME"]), CNY_CODE = Convert.ToString(dr["SRNB_CNY_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    SpaceReqDet.loclst = new List<Locationlst>();
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        SpaceReqDet.loclst.Add(new Locationlst { LCM_CODE = Convert.ToString(dr["SRNB_LOC_CODE"]), LCM_NAME = Convert.ToString(dr["LCM_NAME"]), CTY_CODE = Convert.ToString(dr["SRNB_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["SRNB_CNY_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[3].Rows.Count > 0)
                {
                    SpaceReqDet.twrlst = new List<Towerlst>();
                    foreach (DataRow dr in ds.Tables[3].Rows)
                    {
                        SpaceReqDet.twrlst.Add(new Towerlst { TWR_NAME = Convert.ToString(dr["TWR_NAME"]), TWR_CODE = Convert.ToString(dr["SRNB_TWR_CODE"]), LCM_CODE = Convert.ToString(dr["SRNB_LOC_CODE"]), CTY_CODE = Convert.ToString(dr["SRNB_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["SRNB_CNY_CODE"]), ticked = true });
                    }
                }

                if (ds.Tables[4].Rows.Count > 0)
                {
                    SpaceReqDet.flrlst = new List<Floorlst>();
                    foreach (DataRow dr in ds.Tables[4].Rows)
                    {
                        SpaceReqDet.flrlst.Add(new Floorlst { FLR_CODE = Convert.ToString(dr["SRNB_FLR_CODE"]), FLR_NAME = Convert.ToString(dr["FLR_NAME"]), TWR_CODE = Convert.ToString(dr["SRNB_TWR_CODE"]), LCM_CODE = Convert.ToString(dr["SRNB_LOC_CODE"]), CTY_CODE = Convert.ToString(dr["SRNB_CTY_CODE"]), CNY_CODE = Convert.ToString(dr["SRNB_CNY_CODE"]), ticked = true });
                    }
                }
                if (ds.Tables[5].Rows.Count > 0)
                {
                    SpaceReqDet.verlst = new List<Verticallst>();
                    foreach (DataRow dr in ds.Tables[5].Rows)
                    {
                        SpaceReqDet.verlst.Add(new Verticallst { VER_CODE = Convert.ToString(dr["SRN_VERTICAL"]), VER_NAME = Convert.ToString(dr["VER_NAME"]), ticked = true });

                    }

                }
                if (ds.Tables[6].Rows.Count > 0)
                {
                    SpaceReqDet.cstlst = new List<Costcenterlst>();
                    foreach (DataRow dr in ds.Tables[6].Rows)
                    {
                        SpaceReqDet.cstlst.Add(new Costcenterlst { Cost_Center_Code = Convert.ToString(dr["SRN_COST_CENTER"]), Cost_Center_Name = Convert.ToString(dr["Cost_Center_Name"]), Vertical_Code = Convert.ToString(dr["SRN_VERTICAL"]), ticked = true });
                    }

                }

                if (selectedid.SRN_SYS_PRF_CODE == "1039" || selectedid.SRN_SYS_PRF_CODE == "1040")
                {
                    if (ds.Tables[7].Rows.Count > 0)
                    {
                        SpaceReqDet.spcreqdet = new List<SpaceRequistion_details>();
                        foreach (DataRow dr in ds.Tables[7].Rows)
                        {
                            SpaceReqDet.spcreqdet.Add(new SpaceRequistion_details
                            {
                                SRD_SPC_ID = Convert.ToString(dr["SRD_SPC_ID"]),
                                SRD_REQ_ID = Convert.ToString(dr["SRD_REQ_ID"]),
                                SRD_SH_CODE = Convert.ToString(dr["SRD_SH_CODE"]),
                                SRD_AUR_ID = Convert.ToString(dr["SRD_AUR_ID"]),
                                SRD_SSA_SRNREQ_ID = Convert.ToString(dr["SRD_SSA_SRNREQ_ID"]),
                                SRD_STA_ID = (int)(dr["SRD_STA_ID"]),
                                SRD_SRNREQ_ID = Convert.ToString(dr["SRD_SRNREQ_ID"]),
                                STACHECK = (int)RequestState.Unchanged
                            });

                        }

                    }
                }
                else if (selectedid.SRN_SYS_PRF_CODE == "1041")
                {
                    if (ds.Tables[8].Rows.Count > 0)
                    {
                        SpaceReqDet.spcreqcountList = new List<SpaceRequistionCountList>();
                        foreach (DataRow dr in ds.Tables[8].Rows)
                        {
                            SpaceReqDet.spcreqcountList.Add(new SpaceRequistionCountList
                            {
                                SRC_CNY_CODE = Convert.ToString(dr["SRC_CNY_CODE"]),
                                SRC_CTY_CODE = Convert.ToString(dr["SRC_CTY_CODE"]),
                                SRC_LCM_CODE = Convert.ToString(dr["SRC_LCM_CODE"]),
                                SRC_TWR_CODE = Convert.ToString(dr["SRC_TWR_CODE"]),
                                SRC_FLR_CODE = Convert.ToString(dr["SRC_FLR_CODE"]),
                                SRC_CNY_NAME = Convert.ToString(dr["CNY_NAME"]),
                                SRC_CTY_NAME = Convert.ToString(dr["CTY_NAME"]),
                                SRC_LCM_NAME = Convert.ToString(dr["LCM_NAME"]),
                                SRC_TWR_NAME = Convert.ToString(dr["TWR_NAME"]),
                                SRC_FLR_NAME = Convert.ToString(dr["FLR_NAME"]),
                                SRC_REQ_CNT = (int)(dr["SRC_REQ_CNT"]),
                                SRC_REQ_SEL_TYPE = Convert.ToString(dr["SRC_REQ_SEL_TYPE"]),
                                SRC_REQ_TYPE = Convert.ToString(dr["SRC_REQ_TYPE"]),
                                SRC_STA_ID = (int)(dr["SRC_STA_ID"]),
                                SRC_SEL_SYS_PRF_CODE = Convert.ToString(dr["SRC_SEL_SYS_PRF_CODE"]),
                                SRC_SRN_REQ_ID = Convert.ToString(dr["SRC_SRN_REQ_ID"]),
                                SPC_TYPE_NAME = Convert.ToString(dr["SPC_TYPE_NAME"]),
                                SPC_SUB_TYPE_NAME = Convert.ToString(dr["SPC_SUB_TYPE_NAME"]),
                                SelectedSpacesList = new List<string>()
                            });

                        }

                    }
                }

            }
            SpaceRequisitionService spcreq = new SpaceRequisitionService();
            if (selectedid.SRN_SYS_PRF_CODE == "1039" || selectedid.SRN_SYS_PRF_CODE == "1040")
            {
                SpaceReqDet.spcreq = new SpaceRequistion() { SRN_FROM_DATE = (DateTime)selectedid.SRN_FROM_DATE, SRN_TO_DATE = (DateTime)selectedid.SRN_TO_DATE };
                List<SpaceRequistion_details> spc = spcreq.GetSpaceLst(SpaceReqDet);

                var common = from a1 in spc
                             join a2 in SpaceReqDet.spcreqdet on a1.SRD_SPC_ID equals a2.SRD_SPC_ID into temp
                             from t1 in temp.DefaultIfEmpty()
                             select new { A1 = a1, A2 = t1 };

                foreach (var c in common)
                {
                    if (c.A2 != null)
                    {
                        c.A1.SRD_REQ_ID = c.A2.SRD_REQ_ID;
                        c.A1.ticked = true;
                        c.A1.SRD_SH_CODE = c.A2.SRD_SH_CODE;
                        c.A1.SRD_AUR_ID = c.A2.SRD_AUR_ID;
                        c.A1.STACHECK = (int)RequestState.Unchanged;
                    }
                    else
                        c.A1.SRD_REQ_ID = "";
                }

                return new { Message = MessagesVM.UM_OK, data = new { SELSPACES = SpaceReqDet, DETAILS = common.Select(cmn => cmn.A1).OrderByDescending(x => x.ticked).ToList() } };
            }
            else if (selectedid.SRN_SYS_PRF_CODE == "1041")
            {
                return new { Message = MessagesVM.UM_OK, data = new { SELSPACES = SpaceReqDet } };
            }
            return new { Message = MessagesVM.UM_OK, data = new { SELSPACES = SpaceReqDet } };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object SaveSelectedSpaces(SpaceReqDetails spcreqDetails)
    {
        try
        {
            List<SpaceRequistion> spcreqLst = new List<SpaceRequistion>();
            spcreqLst.Add(spcreqDetails.spcreq);
            if (spcreqDetails.spcreqdet.Count != 0)
            {
                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@SPC_REQ", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(spcreqLst);
                param[1] = new SqlParameter("@SPC_REQ_DET", SqlDbType.Structured);
                param[1].Value = UtilityService.ConvertToDataTable(spcreqDetails.spcreqdet);
                param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[2].Value = HttpContext.Current.Session["UID"];
                param[3] = new SqlParameter("@COMPANY_ID", SqlDbType.Int);
                param[3].Value = HttpContext.Current.Session["COMPANYID"];

                using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SAVE_SPACE_ALLOCATION_DETAILS", param))
                {
                    if (dr.Read())
                    {
                        int FLAG = (int)dr["FLAG"];
                        string REQID = dr["REQID"].ToString();

                        if (FLAG == 1)
                        {
                            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_SPACE_ALLOCATION_ACTION_SM");
                            sp.Command.AddParameter("@REQID", REQID, DbType.String);
                            sp.Command.AddParameter("@ALLOCBY", HttpContext.Current.Session["UID"], DbType.String);
                            sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
                            sp.ExecuteScalar();

                            return new { Message = MessagesVM.SPC_ALL_SUC + " For : " + REQID, data = REQID };

                        }
                        else
                            return new { Message = REQID, data = (object)null };
                    }
                }
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
            else
                return new { Message = MessagesVM.L1_NOREQ, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public object SendMail(SpaceRequistion spcreq)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_SPACE_ALLOCATION_IF_NO_SPACES");
            sp.Command.AddParameter("@REQID", spcreq.SRN_REQ_ID, DbType.String);
            sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            sp.ExecuteScalar();
            return new { Message = spcreq.SRN_REQ_ID, data = (object)null };

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }

    public object GetVacSpaces(SpaceVacantDetails spcdet)
    {
        SpcVaclst = GetVacSpaceLst(spcdet);

        if (SpcVaclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SpcVaclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public List<SpaceRequistion_details> GetVacSpaceLst(SpaceVacantDetails spcdet)
    {
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@FLR", SqlDbType.NVarChar);
        param[0].Value = spcdet.Floor;
        param[1] = new SqlParameter("@TWR", SqlDbType.NVarChar);
        param[1].Value = spcdet.Tower;
        param[2] = new SqlParameter("@LOC", SqlDbType.NVarChar);
        param[2].Value = spcdet.Location;
        param[3] = new SqlParameter("@CTY", SqlDbType.NVarChar);
        param[3].Value = spcdet.City;
        param[4] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[4].Value = HttpContext.Current.Session["COMPANYID"];
        SpcVaclst = new List<SpaceRequistion_details>();
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_VACANT_SPACES_FOR_ALLOCATION", param))
        {
            while (sdr.Read())
            {

                spcreqdet = new SpaceRequistion_details();
                spcreqdet.SRD_SPC_ID = sdr["SPC_ID"].ToString();
                spcreqdet.SRD_SPC_NAME = sdr["SPC_ID"].ToString();
                spcreqdet.SRD_SPC_TYPE = sdr["SPC_LAYER"].ToString();
                spcreqdet.SRD_SPC_TYPE_NAME = sdr["SPC_TYPE_NAME"].ToString();
                spcreqdet.SRD_SPC_SUB_TYPE = sdr["SPC_SUB_TYPE"].ToString();
                spcreqdet.SRD_SPC_SUB_TYPE_NAME = sdr["SST_NAME"].ToString();
                spcreqdet.SRD_LCM_CODE = sdr["SPC_BDG_ID"].ToString();
                spcreqdet.lat = sdr["SPC_X_VALUE"].ToString();
                spcreqdet.lon = sdr["SPC_Y_VALUE"].ToString();
                spcreqdet.SSA_FLR_CODE = sdr["SPC_FLR_ID"].ToString();
                SpcVaclst.Add(spcreqdet);
            }
            sdr.Close();
        }
        return SpcVaclst;
    }
    public object GetDetailsOnSelectionAlloc(SpaceRequistion selectedid)
    {
        try
        {
            SpaceReqDetails SpaceReqDet = new SpaceReqDetails();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Edit_L1_Approval_Requisitions_alloc");
            sp.Command.AddParameter("@SRN_REQ_ID", selectedid.SRN_REQ_ID, DbType.String);

            DataSet ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
            {

                if (ds.Tables[7].Rows.Count > 0)
                {
                    SpaceReqDet.spcreqcountList = new List<SpaceRequistionCountList>();
                    foreach (DataRow dr in ds.Tables[7].Rows)
                    {
                        SpaceReqDet.spcreqcountList.Add(new SpaceRequistionCountList
                        {
                            SRC_CNY_CODE = Convert.ToString(dr["SRC_CNY_CODE"]),
                            SRC_CTY_CODE = Convert.ToString(dr["SRC_CTY_CODE"]),
                            SRC_LCM_CODE = Convert.ToString(dr["SRC_LCM_CODE"]),
                            SRC_TWR_CODE = Convert.ToString(dr["SRC_TWR_CODE"]),
                            SRC_FLR_CODE = Convert.ToString(dr["SRC_FLR_CODE"]),
                            SRC_CNY_NAME = Convert.ToString(dr["CNY_NAME"]),
                            SRC_CTY_NAME = Convert.ToString(dr["CTY_NAME"]),
                            SRC_LCM_NAME = Convert.ToString(dr["LCM_NAME"]),
                            SRC_TWR_NAME = Convert.ToString(dr["TWR_NAME"]),
                            SRC_FLR_NAME = Convert.ToString(dr["FLR_NAME"]),
                            SRC_REQ_CNT = (int)(dr["SRC_REQ_CNT"]),
                            SRC_REQ_SEL_TYPE = Convert.ToString(dr["SRC_REQ_SEL_TYPE"]),
                            SRC_REQ_TYPE = Convert.ToString(dr["SRC_REQ_TYPE"]),
                            SRC_STA_ID = (int)(dr["SRC_STA_ID"]),
                            SRC_SEL_SYS_PRF_CODE = Convert.ToString(dr["SRC_SEL_SYS_PRF_CODE"]),
                            SRC_SRN_REQ_ID = Convert.ToString(dr["SRC_SRN_REQ_ID"]),
                            SPC_TYPE_NAME = Convert.ToString(dr["SPC_TYPE_NAME"]),
                            SPC_SUB_TYPE_NAME = Convert.ToString(dr["SPC_SUB_TYPE_NAME"]),
                            SSA_SPC_ID = Convert.ToString(dr["SSA_SPC_ID"]),
                            SelectedSpacesList = new List<string>()
                        });

                    }

                }
            }

            return new { Message = MessagesVM.UM_OK, data = SpaceReqDet.spcreqcountList };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
    public object Allocatedclosed(SpaceReq Reqid)
    {
        try
        {


            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@REQID", SqlDbType.NVarChar);
            param[0].Value = Reqid.SRC_SRN_REQ_ID;
            param[3] = new SqlParameter("@SRN_ALLC_REM", SqlDbType.NVarChar);
            param[3].Value = Reqid.APP_REM;
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = HttpContext.Current.Session["UID"];
            param[2] = new SqlParameter("@COMPANY_ID", SqlDbType.Int);
            param[2].Value = HttpContext.Current.Session["COMPANYID"];

            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SAVE_SPACE_ALLOCATION_DETAILS_CLOSED", param))
            {
                if (dr.Read())
                {
                    int FLAG = (int)dr["FLAG"];
                    string REQID = dr["REQID"].ToString();

                    if (FLAG == 1)
                    {
                        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_SPACE_ALLOCATION_ACTION_SM_CLOSED");
                        sp.Command.AddParameter("@REQID", REQID, DbType.String);
                        sp.Command.AddParameter("@ALLOCBY", HttpContext.Current.Session["UID"], DbType.String);
                        sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
                        sp.ExecuteScalar();

                        return new { Message = MessagesVM.SPC_ALL_CLS + " For : " + REQID, data = REQID };

                    }
                    else
                        return new { Message = REQID, data = (object)null };
                }
            }
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }
}