﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using UtiltiyVM;
using System.Data.SqlClient;
using System.Threading.Tasks;

public class MaploaderService
{
    SubSonic.StoredProcedure sp;

    public object GetMonitors()
    {
        var monitorList = new List<Monitor_Details>();
        //Shiftlst shf;
        //SqlParameter[] param = new SqlParameter[1];
        //param[0] = new SqlParameter("@LOCLST", SqlDbType.Structured);
        //param[0].Value = UtilityService.ConvertToDataTable(Loc);

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_MONITORS"))
        {
            while (sdr.Read())
            {
                var monitor = new Monitor_Details
                {
                    MO_ID = Convert.ToInt32(sdr["MO_ID"]),
                    MO_CODE = sdr["MO_CODE"].ToString(),
                    MO_NAME = sdr["MO_NAME"].ToString(),
                    MO_TYPE = sdr["MO_TYPE"].ToString(),
                };
                monitorList.Add(monitor);
            }
            sdr.Close();
            if (monitorList.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = monitorList };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public object GetFloorLst()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GETUSERFLOORLIST");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        //sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetImageList(string Image)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SELETOWER_IMAGE_LIST");
        sp.Command.Parameters.Add("@TWI_CODE", Image, DbType.String);
        //sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GET_VERTICAL_WISE_ALLOCATIONS(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        try
        {
            sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_VERTICAL_WISE_ALLOCATIONS");
            sp.Command.AddParameter("@FLR_CODE", svm.flr_code, DbType.String);
            ds = sp.GetDataSet();
            List<object> verticals = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[0]).Distinct().ToList();
            return new { data = verticals, data1 = ds.Tables[0], data2 = ds.Tables[1] };
        }
        catch (Exception ex)
        {
            return new { data = (object)null };
        }
    }

    public object GetMapItems(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        if (svm.key_value == 1)
        {
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FLOORMAPS_View_In_Map");
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
            ds = sp.GetDataSet();
            Hashtable sendData = new Hashtable();

            foreach (DataRow drIn in ds.Tables[1].Rows)
            {
                sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
            }

            return new { mapDetails = ds.Tables[0], FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[2] };
        }
        else
        {
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FLOORMAPS");
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
            ds = sp.GetDataSet();
            Hashtable sendData = new Hashtable();

            foreach (DataRow drIn in ds.Tables[2].Rows)
            {
                sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
            }

            return new { mapDetails = ds.Tables[0], mapDetails2 = ds.Tables[1], FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[3] };
        }

    }
    public object GetMarkers(Space_mapVM svm)
    {
        svm.monitor_type = svm.monitor_type ?? "";
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_MARKER_ALL");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        sp.Command.Parameters.Add("@MODE", 1, DbType.Int32);
        sp.Command.Parameters.Add("@FROM_DATE", svm.from_date, DbType.Date);
        sp.Command.Parameters.Add("@TO_DATE", svm.to_date, DbType.Date);
        sp.Command.Parameters.Add("@SH_CODE", svm.sh_code, DbType.String);
        sp.Command.Parameters.Add("@MonitorType", svm.monitor_type, DbType.String);

        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GradeAllocStatus()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GRADE_ALLOC_STATUS");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }


    public object getSpaceDaysRestriction()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "RESTRICT_DAYS_COUNT");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }
    public object getVer_And_CC(AUR_DETAILS data)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_VERTICAL_COSTCENTER_BY_AUR_ID");
        sp.Command.Parameters.Add("@AUR_ID", data.AUR_ID, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetCornerLables(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FLOOR_CORNERS");
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetLegendsCount(Space_mapVM svm)
    {
        svm.monitor_type = svm.monitor_type ?? "";
        svm.sh_code = svm.sh_code ?? "";
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_CONSOLIDATE_REPORT_MAP");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@FLAG", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@SH_CODE", svm.sh_code, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@FROM_DATE", svm.from_date, DbType.Date);
        sp.Command.Parameters.Add("@TO_DATE", svm.to_date, DbType.Date);
        sp.Command.Parameters.Add("@SHI_CODE", svm.sh_code, DbType.String);
        sp.Command.Parameters.Add("@MONITOR_TYPE_CODE", svm.monitor_type, DbType.String);
        ds = sp.GetDataSet();
        return ds;
    }

    public object GetLegendsSummary(Space_mapVM svm)
    {
        svm.monitor_type = svm.monitor_type ?? "";
        svm.sh_code = svm.sh_code ?? "";
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_CONSOLIDATE_REPORT_MAP_LEGEND");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        sp.Command.Parameters.Add("@FROM_DATE", svm.from_date, DbType.Date);
        sp.Command.Parameters.Add("@TO_DATE", svm.to_date, DbType.Date);
        sp.Command.Parameters.Add("@SH_CODE", svm.sh_code, DbType.String);
        sp.Command.Parameters.Add("@MONITOR_TYPE_CODE", svm.monitor_type, DbType.String);

        ds = sp.GetDataSet();
        IDataReader dr = sp.GetReader();
        LEGEND_SUMMARY ls = new LEGEND_SUMMARY();
        if (dr.Read())
        {
            ls.ALLOCATEDVER = (int)dr["ALLOCATEDVER"];
            ls.ALLOCATEDCST = (int)dr["ALLOCATEDCST"];
            ls.TOTAL = (int)dr["TOTAL"];
            ls.VACANT = (int)dr["VACANT"];
            ls.OCCUPIED = (int)dr["OCCUPIED"];
            ls.OVERLOAD = (int)dr["OVERLOAD"];
            ls.BLOCKED = (int)dr["BLOCKED"];
            ls.ALLOCUNUSED = ds.Tables[1].Rows[0]["ALLOC_UNUSED"].ToString();
            ls.OCCUP_EMP = ds.Tables[2].Rows[0]["OCCUP_EMP"].ToString();
            ls.LEAVE = Convert.ToInt32(ds.Tables[3].Rows[0]["LEAVE"].ToString());
        }
        return ls;
    }

    public object GetSpaceDetailsBySPCID(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_SPACEDETAILS_BYSPCID");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@SPC_ID", svm.CatValue, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetallFilterbyItem()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FILTER_ITEM_SM");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetallFilterbySubItem(Space_mapVM param)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_FILTER_SUBITEM_SM");
        sp.Command.Parameters.Add("@LOCID", param.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@ITEMID", param.Item, DbType.String);
        sp.Command.Parameters.Add("@FLR_ID", param.flr_code, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetSpaceDetailsBySUBITEM(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SPACE_DATA_MAP_SM");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@ITEM", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@SUB_ITEM", svm.subitem, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();

        if (svm.Item == "login")
        {

            //ActiveDirectoryHelper adhelper = new ActiveDirectoryHelper();
            //List<ADUserDetail> useradlist = adhelper.GetUserslist();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_AD_DATA_LIVE");
            DataSet DS1 = sp.GetDataSet();

            List<AD_DATA_VM> useradlist = DS1.Tables[0].AsEnumerable()
                  .Select(row => new AD_DATA_VM
                  {
                      USR_ID = row.Field<string>(0),
                      LOG_IN_TIME = row.Field<string>(1),
                      LOG_OUT_TIME = row.Field<string>(2)
                  }).ToList();

            List<Space_MapEmployeeVM> empmaplist = ds.Tables[0].AsEnumerable()
              .Select(row => new Space_MapEmployeeVM
              {
                  SPC_ID = row.Field<string>(0),
                  x = row.Field<double>(1).ToString(),
                  y = row.Field<double>(2).ToString(),
                  SPC_FLR_ID = row.Field<string>(3),
                  SPC_LAYER = row.Field<string>(4),
                  SPC_DESC = row.Field<string>(5),
                  STATUS = row.Field<int>(6).ToString(),
                  AD_ID = row.Field<string>(7)
              }).ToList();

            var list3 = (from Item1 in empmaplist
                         join Item2 in useradlist
                         on Item1.AD_ID equals Item2.USR_ID // join on some property
                         into grouping
                         from Item2 in grouping.DefaultIfEmpty()
                             //where Item2 != null
                         select new { Item1, Item2 }).ToList();
            return list3;
        }

        return ds;
    }

    public object GetSpaceDetailsByREQID(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_SPACEDETAILS_BYREQID");
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@SPC_ID", svm.category, DbType.String);
        sp.Command.Parameters.Add("@REQ_ID", svm.subitem, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object ReleaseSelectedseat(SPACE_REL_DETAILS reldet)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@REL_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(reldet.sad);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@RELTYPE", SqlDbType.NVarChar);
        param[2].Value = reldet.reltype;
        param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];

        int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SMS_SPACE_REL_MAP", param);

        if (RETVAL == 1)
            return new { Message = MessagesVM.UM_OK, data = reldet.reltype };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };


    }

    public object ActivateSpaces(CLS_INACTIVE_SPACE SpcAct)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@SPACE_LIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(SpcAct.SPC_LST);
            param[1] = new SqlParameter("@FLR_ID", SqlDbType.NVarChar);
            param[1].Value = SpcAct.FLR_CODE;
            param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["COMPANYID"];
            param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"];
            int RETVAL = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "SMS_INSERT_SPACES", param);

            if (RETVAL == 1)
                return new { Message = MessagesVM.UM_OK };
            else
                return new { Message = MessagesVM.ErrorMessage, data = (object)null };

        }
        catch (SqlException)
        {
            throw;
        }
    }

    public object InactiveSeats(CLS_INACTIVE_SPACE Inac)
    {
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@SPACE_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(Inac.SPC_LST);
        param[1] = new SqlParameter("@FLR_ID", SqlDbType.NVarChar);
        param[1].Value = Inac.FLR_CODE;
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];
        param[3] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[3].Value = HttpContext.Current.Session["UID"];
        int INAC = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SMS_INACTIVE_SEATS", param);

        if (INAC == 1)
            return new { Message = MessagesVM.UM_OK };
        else
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    }

    public List<SPACEDETAILS> InactiveSpacesFromFloorMaps(Space_mapVM svm)
    {
        try
        {
            List<SPACEDETAILS> FlrMapslst = new List<SPACEDETAILS>();
            SPACEDETAILS ls;
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_INACTIVE_SEATS");
            sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            IDataReader dr = sp.GetReader();
            while (dr.Read())
            {
                ls = new SPACEDETAILS();
                ls.SPACE_ID = dr["SPACE_ID"].ToString();
                ls.SPC_TYPE = dr["SPC_TYPE_NAME"].ToString();
                ls.SPC_SUB_TYPE = dr["SST_NAME"].ToString();
                FlrMapslst.Add(ls);
            }
            dr.Close();
            return FlrMapslst;
        }
        catch
        {
            throw;
        }
    }

    public object GetEmpDetails(Space_mapVM svm)
    {
        if (svm.subitem != null && svm.subitem != "")
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_EMP_DETAILS_FLRID");
            sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
            sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
            sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
            sp.Command.Parameters.Add("@VER_CODE", svm.Item, DbType.String);
            sp.Command.Parameters.Add("@COST_CENTER_CODE", svm.subitem, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
            ds = sp.GetDataSet();
            return new { Message = MessagesVM.SelectCST, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.SelectCST, data = (object)null };
    }

    public object GetAllocEmpDetails(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_GET_ALLOC_EMP_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@ITEM", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        sp.Command.Parameters.Add("@FROM_DATE", svm.from_date, DbType.Date);
        sp.Command.Parameters.Add("@TO_DATE", svm.to_date, DbType.Date);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetSeatAllocDetails(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_MAP_SEAT_ALLOC_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@MODE", svm.mode, DbType.String);
        sp.Command.Parameters.Add("@SELETED_SPC_ID", svm.spcid, DbType.String);
        sp.Command.Parameters.Add("@FROM_DATE", svm.from_date, DbType.String);
        sp.Command.Parameters.Add("@TO_DATE", svm.to_date, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object AllocateSeats(List<SPACE_ALLOC_DETAILS_TIME> allocDetLst)
    {
        int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
        var details = allocDetLst.Where(x => arr.Contains(x.STACHECK)).ToList();

        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AURID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];
        param[3] = new SqlParameter("@Mode", SqlDbType.NVarChar);
        param[3].Value = "Map";

        List<SPACE_ALLOC_DETAILS_TIME> spcdet = new List<SPACE_ALLOC_DETAILS_TIME>();


        Object result = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SMS_SPACE_ALLOC_MAP11", param);


        if (Convert.ToInt32(result) == 1)
        {
            SPACE_ALLOC_DETAILS_TIME spc;
            DataSet ds = new DataSet();

            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[SMS_SPACE_ALLOC_MAP_DETAILS]", param))
            {
                while (sdr.Read())
                {
                    string REQ_ID = sdr["SSA_SRNREQ_ID"].ToString();

                    spc = new SPACE_ALLOC_DETAILS_TIME();
                    spc.AUR_ID = sdr["SSAD_AUR_ID"].ToString();
                    spc.VERTICAL = sdr["VERTICAL"].ToString();
                    spc.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                    spc.FROM_DATE = Convert.ToDateTime(sdr["FROM_DATE"]);
                    spc.TO_DATE = Convert.ToDateTime(sdr["TO_DATE"]);
                    spc.SH_CODE = sdr["SH_CODE"].ToString();

                    spc.SPC_ID = sdr["SPC_ID"].ToString();
                    spc.SSAD_SRN_REQ_ID = sdr["SSAD_SRN_REQ_ID"].ToString();
                    spc.SSA_SRNREQ_ID = sdr["SSA_SRNREQ_ID"].ToString();
                    spc.STACHECK = (int)RequestState.Modified;
                    spc.STATUS = Convert.ToInt16(sdr["STATUS"]);
                    spc.ticked = true;
                    spc.SHIFT_TYPE = sdr["SPACE_TYPE"].ToString();
                    spc.VL_Code = sdr["VL_Code"].ToString();
                    spc.emp = sdr["emp"].ToString();
                    spcdet.Add(spc);

                    //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_EMPLOYEE_ALLOCATION_ACTION_SM");
                    //sp.Command.AddParameter("@REQID", REQ_ID, DbType.String);
                    //sp.Command.AddParameter("@CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
                    //sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
                    //sp.ExecuteScalar();
                }
            }


            foreach (SPACE_ALLOC_DETAILS_TIME obj in allocDetLst.Where(x => x.STACHECK == (int)RequestState.Deleted).ToList())
            {
                spc = new SPACE_ALLOC_DETAILS_TIME();
                spc.SPC_ID = obj.SPC_ID;
                spc.STATUS = obj.STATUS;
                spc.ticked = true;
                spcdet.Add(spc);
            }
            if (spcdet.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = spcdet };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        else if (Convert.ToInt32(result) == 2)
        {
            return new { Message = "Seat already booked within selected date range", data = (object)null };
        }
        else
        {
            return 0;
        }



    }

    public object SpcAvailabilityByShift(SPACE_ALLOC_DETAILS sad)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_AVAILABILITY_BY_SHIFT");
        sp.Command.Parameters.Add("@SPC_ID", sad.SPC_ID);
        sp.Command.Parameters.Add("@FDATE", sad.FROM_DATE);
        sp.Command.Parameters.Add("@TDATE", sad.TO_DATE);
        sp.Command.Parameters.Add("@SH_CODE", sad.SH_CODE);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);


        using (IDataReader dr = sp.GetReader())
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    return new { Message = dr["MSG"].ToString(), data = 1 };
                }
                else if ((int)dr["FLAG"] == 2)
                {
                    return new { Message = dr["MSG"].ToString(), data = (object)null };
                }
                else if ((int)dr["FLAG"] == 0)
                {
                    return new { Message = MessagesVM.ErrorMessage, data = (object)null };
                }
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetTotalAreaDetails(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_MAP_EMP_ALLOC_SEAT_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetSeatingCapacity(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_MAP_GET_SEATING_CAPACITY_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetEmpAllocSeat(Space_mapVM svm)
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SMS_MAP_EMP_ALLOC_SEAT_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@MODE", svm.mode, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object getVLANS()
    {
        List<vlanlist> vlan = new List<vlanlist>();
        vlanlist VL;

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_VLAN");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                VL = new vlanlist();
                VL.VL_Code = sdr["VL_Code"].ToString();
                VL.VL_Name = sdr["VL_Name"].ToString();
                vlan.Add(VL);
            }
        }
        if (vlan.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = vlan };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }





    //public object Post(MaploaderVM mapVm)
    //{
    //    FLOORMAP flr = new FLOORMAP();
    //    var geom = MakeValidGeographyFromText(mapVm.Wkt);
    //    flr.geom = geom;
    //    flr.AREA_SQFT = geom.Area;
    //    flr.LATITUDE = geom.XCoordinate;
    //    flr.LONGITUDE = geom.YCoordinate;
    //    flr.FLR_ID = mapVm.flr_id;
    //    quickfmsentities.FLOORMAPS.Add(flr);
    //    quickfmsentities.SaveChanges();
    //    mapVm.ID = flr.ID;
    //    return mapVm;
    //}

    //public object Put(MaploaderVM mapVm)
    //{
    //    FLOORMAP flr = quickfmsentities.FLOORMAPS.Find(mapVm.ID);
    //    flr.geom = MakeValidGeographyFromText(mapVm.Wkt);
    //    flr.AREA_SQFT = MakeValidGeographyFromText(mapVm.Wkt).Area;
    //    quickfmsentities.Entry(flr).State = System.Data.EntityState.Modified;
    //    quickfmsentities.SaveChanges();
    //    mapVm.ID = flr.ID;
    //    return mapVm;
    //}

    //public void Delete(MaploaderVM mapVm)
    //{
    //    FLOORMAP flr = quickfmsentities.FLOORMAPS.Find(mapVm.ID);
    //    quickfmsentities.Entry(flr).State = System.Data.EntityState.Deleted;
    //    quickfmsentities.SaveChanges();
    //}

    //private DbGeometry MakeValidGeographyFromText(string inputWkt)
    //{
    //    SqlGeography sqlPolygon = SQLSpatialTools.Functions.MakeValidGeographyFromText(inputWkt, 4326);
    //    return DbGeometry.FromBinary(sqlPolygon.STAsBinary().Value);
    //}

    //public object GetMarkerDet(MaploaderVM mapVm)
    //{
    //    try
    //    {
    //        FLOORMAPS_MARKERS flrmarker = quickfmsentities.FLOORMAPS_MARKERS.Where(mrk => mrk.FM_SPC_ID == mapVm.SPACE_ID).SingleOrDefault();
    //        return new { Message = MessagesVM.UM_OK, data = flrmarker };
    //    }
    //    catch (Exception ex)
    //    {
    //        return new { Message = ex.Message, data = (object)null };
    //    }
    //}

    //public object InsertMarkerDet(MaploaderVM mapVm)
    //{
    //    return new { Message = MessagesVM.UM_OK, data = (object)null };
    //}
}
