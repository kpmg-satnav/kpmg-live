﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for AddHerenceService
/// </summary>
public class AddHerenceService
{
    SubSonic.StoredProcedure sp;

    public object GetData(AddHerenceModel Data)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." +"adherence_report");
            sp.Command.Parameters.Add("@Floors", Data.Floors, DbType.String);
            sp.Command.Parameters.Add("@Location", Data.Locations, DbType.String);
            sp.Command.Parameters.Add("@Towers", Data.Towers, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            sp.Command.Parameters.Add("@From_Date", Data.FromDate, DbType.Date);
            sp.Command.Parameters.Add("@To_Date", Data.ToDate, DbType.Date);
            ds = sp.GetDataSet();
            return ds.Tables[0];

        }
        catch (Exception e)
        {
            throw;
        }
    }

    public object NotBookButCametoOfc(AddHerenceModel Data)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "EMP_NOT_BOOK_CAME_TO_OFFICE");
            
            sp.Command.Parameters.Add("@Location", Data.Locations, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            sp.Command.Parameters.Add("@From_Date", Data.FromDateNb, DbType.String);
            sp.Command.Parameters.Add("@To_Date", Data.ToDateNb, DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables[0];

        }
        catch (Exception e)
        {
            throw;
        }
    }
}