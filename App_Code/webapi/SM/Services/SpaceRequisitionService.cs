﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

/// <summary>
/// Summary description for SpaceRequisitionService
/// </summary>
public class SpaceRequisitionService
{
    SubSonic.StoredProcedure sp;
    UtilityService utlservc;

    SpaceRequistion_details spcreqdet;
    List<SpaceRequistion_details> spcreqdetlst;

    SpaceRequistionCount spcreqcount;
    List<SpaceRequistionCount> spcreqcountlst;

    public SpaceRequisitionService()
    {
        utlservc = new UtilityService();
    }

    public object GetSpaces(SpaceReqDetails spcdet)
    {
        spcreqdetlst = GetSpaceLst(spcdet);

        if (spcreqdetlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = spcreqdetlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public List<SpaceRequistion_details> GetSpaceLst(SpaceReqDetails spcdet)
    {
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(spcdet.flrlst);
        param[1] = new SqlParameter("@VERLST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(spcdet.verlst);
        param[2] = new SqlParameter("@FDATE", SqlDbType.DateTime);
        param[2].Value = spcdet.spcreq.SRN_FROM_DATE;
        param[3] = new SqlParameter("@TDATE", SqlDbType.DateTime);
        param[3].Value = spcdet.spcreq.SRN_TO_DATE;
        param[4] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[4].Value = HttpContext.Current.Session["COMPANYID"];
        param[5] = new SqlParameter("@SHIFTID", SqlDbType.VarChar);
        if (spcdet.ShiftFilter != null)
            param[5].Value = spcdet.ShiftFilter;
        else
            param[5].Value = "";
        spcreqdetlst = new List<SpaceRequistion_details>();
        //using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_SPACES_FOR_REQUISITION", param))
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_SPACES_FOR_ALLOCATION", param))
        {
            while (sdr.Read())
            {
                spcreqdet = new SpaceRequistion_details();
                spcreqdet.SRD_SRNREQ_ID = sdr["SSAD_SRN_REQ_ID"].ToString();
                spcreqdet.SRD_SSA_SRNREQ_ID = sdr["SSA_SRNREQ_ID"].ToString();
                spcreqdet.SRD_SPC_ID = sdr["SSA_SPC_ID"].ToString();
                spcreqdet.SRD_SPC_NAME = sdr["SPC_NAME"].ToString();
                spcreqdet.SRD_SPC_TYPE = sdr["SPC_TYPE_CODE"].ToString();
                spcreqdet.SRD_SPC_TYPE_NAME = sdr["SPC_TYPE_NAME"].ToString();
                spcreqdet.SRD_SPC_SUB_TYPE = sdr["SST_CODE"].ToString();
                spcreqdet.SRD_SPC_SUB_TYPE_NAME = sdr["SST_NAME"].ToString();
                spcreqdet.SRD_LCM_CODE = sdr["SSA_LCM_CODE"].ToString();
                spcreqdet.lat = sdr["SPC_X_VALUE"].ToString();
                spcreqdet.lon = sdr["SPC_Y_VALUE"].ToString();
                spcreqdet.SSA_FLR_CODE = sdr["SSA_FLR_CODE"].ToString();
                spcreqdet.SRD_SH_CODE = sdr["SSAD_SH_CODE"].ToString();
                spcreqdetlst.Add(spcreqdet);
            }
            sdr.Close();
        }
        return spcreqdetlst;
    }

    public object GetShifts(List<Locationlst> Loc)
    {
        List<Shiftlst> Shflst = new List<Shiftlst>();
        Shiftlst shf;
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@LOCLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(Loc);

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_SHIFTS_BY_LOC", param))
        {
            while (sdr.Read())
            {
                shf = new Shiftlst();
                shf.SH_CODE = sdr["SH_CODE"].ToString();
                shf.SH_NAME = sdr["SH_NAME"].ToString();
                shf.SH_LOC_ID = sdr["SH_LOC_ID"].ToString();
                shf.SH_SEAT_TYPE = sdr["SH_SEAT_TYPE"].ToString();
                shf.REP_COL = sdr["REP_COL"].ToString();
                shf.ticked = false;
                Shflst.Add(shf);
            }
            sdr.Close();
            if (Shflst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = Shflst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public object GetEmpDetails(SpaceReqDetails SpaceReqDetails)
    {
        List<Aurlst> Aurlst = new List<Aurlst>();
        Aurlst Aur;
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CSTLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(SpaceReqDetails.cstlst);
        param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
        param[1].Value = SpaceReqDetails.spcreq.SRN_FROM_DATE;
        param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
        param[2].Value = SpaceReqDetails.spcreq.SRN_TO_DATE;
        param[3] = new SqlParameter("@COMPANY_ID", SqlDbType.Int);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_EMP_DETAILS_COST_CENTER", param))
        {
            while (sdr.Read())
            {
                Aur = new Aurlst();
                Aur.AUR_ID = sdr["AUR_ID"].ToString();
                Aur.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                Aur.AUR_DES_CODE = sdr["AUR_DESGN_ID"].ToString();
                Aur.AUR_DES_NAME = sdr["DSN_AMT_TITLE"].ToString();
                Aur.ALLOC_SPC_ID = sdr["SSAD_SPC_ID"].ToString();
             //   Aur.AUR_KNOWN_AS_NAME = sdr["EMPNAME"].ToString();
                Aur.ticked = false;
                Aurlst.Add(Aur);
            }
            sdr.Close();
            if (Aurlst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = Aurlst };
            else
                return new { Message = MessagesVM.EMP_NO_REC, data = (object)null };
        }
    }

    public object RaiseRequest(SpaceReqDetails spcreqdetails)
    {
        spcreqdetails.spcreq.SRN_COST_CENTER = spcreqdetails.cstlst[0].Cost_Center_Code;
        spcreqdetails.spcreq.SRN_VERTICAL = spcreqdetails.cstlst[0].Vertical_Code;
        List<SpaceRequistion> SpcReqlst = new List<SpaceRequistion>();
        SpcReqlst.Add(spcreqdetails.spcreq);
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@SPC_REQ", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(SpcReqlst);
        param[1] = new SqlParameter("@SPC_REQ_DET", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(spcreqdetails.spcreqdet);
        param[2] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[2].Value = UtilityService.ConvertToDataTable(spcreqdetails.flrlst);
        param[3] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
        param[3].Value = spcreqdetails.ALLOCSTA;
        param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[4].Value = HttpContext.Current.Session["UID"];
        param[5] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[5].Value = HttpContext.Current.Session["COMPANYID"];

        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_REQ_RAISE_TRAN", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_SPACE_REQUISITION_BY_SPACES_SM");
                    sp.Command.AddParameter("@REQID", dr["REQID"].ToString(), DbType.String);
                    sp.Command.AddParameter("@STATUS", 4, DbType.Int32);
                    sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
                    sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"], DbType.String);
                    sp.ExecuteScalar();
                    return new { Message = MessagesVM.SPC_REQ_INSERTED + " For Req ID " + dr["REQID"].ToString(), data = dr["REQID"].ToString() };
                }
                else
                    return new { Message = dr["REQID"].ToString(), data = (object)null };
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public object GetReqCountDetails(SpaceReqDetails spcreqcnt)
    {
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(spcreqcnt.flrlst);

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_REQ_FLOOR_COUNT_DETAILS", param))
        {
            spcreqcountlst = new List<SpaceRequistionCount>();
            while (sdr.Read())
            {
                spcreqcount = new SpaceRequistionCount();
                spcreqcount.SRC_CNY_NAME = sdr["CNY_NAME"].ToString();
                spcreqcount.SRC_CNY_CODE = sdr["CNY_CODE"].ToString();
                spcreqcount.SRC_CTY_NAME = sdr["CTY_NAME"].ToString();
                spcreqcount.SRC_CTY_CODE = sdr["CTY_CODE"].ToString();
                spcreqcount.SRC_LCM_NAME = sdr["LCM_NAME"].ToString();
                spcreqcount.SRC_LCM_CODE = sdr["LCM_CODE"].ToString();
                spcreqcount.SRC_TWR_NAME = sdr["TWR_NAME"].ToString();
                spcreqcount.SRC_TWR_CODE = sdr["TWR_CODE"].ToString();
                spcreqcount.SRC_FLR_NAME = sdr["FLR_NAME"].ToString();
                spcreqcount.SRC_FLR_CODE = sdr["FLR_CODE"].ToString();
                spcreqcount.SRC_SEL_SYS_PRF_CODE = spcreqcnt.REQ_CNT_STA_FUT.ToString();
                spcreqcountlst.Add(spcreqcount);
            }
            sdr.Close();
            if (spcreqcountlst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = spcreqcountlst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public object RaiseCountRequest(SpaceReqDetails Raisespcreqcnt)
    {
        try
        {
            Raisespcreqcnt.spcreq.SRN_COST_CENTER = Raisespcreqcnt.cstlst[0].Cost_Center_Code;
        Raisespcreqcnt.spcreq.SRN_VERTICAL = Raisespcreqcnt.cstlst[0].Vertical_Code;
        List<SpaceRequistion> SpcReqlst = new List<SpaceRequistion>();
        SpcReqlst.Add(Raisespcreqcnt.spcreq);
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@SPC_REQ", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(SpcReqlst);
        param[1] = new SqlParameter("@SPC_REQ_DET_COUNT", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(Raisespcreqcnt.spcreqcount);
        param[2] = new SqlParameter("@FLRLST", SqlDbType.Structured);
        param[2].Value = UtilityService.ConvertToDataTable(Raisespcreqcnt.flrlst);
        param[3] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
        param[3].Value = Raisespcreqcnt.ALLOCSTA;
        param[4] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[4].Value = HttpContext.Current.Session["UID"];
        param[5] = new SqlParameter("@COMPANY_ID", SqlDbType.Int);
        param[5].Value = HttpContext.Current.Session["COMPANYID"];
        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_COUNT_REQ_RAISE_TRAN", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_SPACE_REQUISITION_BY_ACTION_SM");
                    sp.Command.AddParameter("@REQID", dr["REQID"].ToString(), DbType.String);
                    sp.Command.AddParameter("@STATUS", 4, DbType.Int32);
                    sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
                    sp.ExecuteScalar();
                    return new { Message = MessagesVM.SPC_REQ_INSERTED + " For Req ID " + dr["REQID"].ToString(), data = dr["REQID"].ToString() };
                }
                else
                    return new { Message = dr["REQID"].ToString(), data = (object)null };
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public object GetEmpTypes(List<Costcenterlst> cstlst)
    {
        List<Type_Data> Typlst = new List<Type_Data>();
        Type_Data Typ;
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CSTLST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(cstlst);

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_GET_EMP_GRADE_BY_COST_CENTER", param))
        {
            while (sdr.Read())
            {
                Typ = new Type_Data();
                Typ.CODE = sdr["CODE"].ToString();
                Typ.NAME = sdr["NAME"].ToString();
                Typlst.Add(Typ);
            }
            sdr.Close();
            if (Typlst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = Typlst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public object GetEmpSubTypes()
    {
        List<Type_Data> Typlst = new List<Type_Data>();
        Type_Data Typ;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_EMP_SUB_TYPES");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Typ = new Type_Data();
                Typ.CODE = sdr["CODE"].ToString();
                Typ.NAME = sdr["NAME"].ToString();
                Typlst.Add(Typ);
            }
        }
        if (Typlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Typlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetSpaceTypes()
    {
        List<Type_Data> Typlst = new List<Type_Data>();
        Type_Data Typ;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SPACE_TYPES_ADM");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Typ = new Type_Data();
                Typ.CODE = sdr["CODE"].ToString();
                Typ.NAME = sdr["NAME"].ToString();
                Typlst.Add(Typ);
            }
        }
        if (Typlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Typlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetSpaceSubTypes()
    {
        List<Type_Data> Typlst = new List<Type_Data>();
        Type_Data Typ;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SPACE_SUB_TYPES_ADM");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Typ = new Type_Data();
                Typ.CODE = sdr["CODE"].ToString();
                Typ.NAME = sdr["NAME"].ToString();
                Typlst.Add(Typ);
            }
        }
        if (Typlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Typlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetSubTypes(string SpaceType)
    {
        List<Type_Data> Typlst = new List<Type_Data>();
        Type_Data Typ;
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@SPACETYPE", SqlDbType.NVarChar);
        param[0].Value = SpaceType;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_SPACE_SUB_TYPES_ADM_BY_SPACE_TYPE", param))
        {
            while (sdr.Read())
            {
                Typ = new Type_Data();
                Typ.CODE = sdr["CODE"].ToString();
                Typ.NAME = sdr["NAME"].ToString();
                Typlst.Add(Typ);
            }
        }
        if (Typlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Typlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object CheckSpace(SpaceReqDetails SpaceReqDetails)
    {

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_AVAILABILITY_BY_SHIFT");
        sp.Command.Parameters.Add("@SPC_ID", SpaceReqDetails.spcreqdet[0].SRD_SPC_ID);
        sp.Command.Parameters.Add("@FDATE", SpaceReqDetails.spcreq.SRN_FROM_DATE);
        sp.Command.Parameters.Add("@TDATE", SpaceReqDetails.spcreq.SRN_TO_DATE);
        sp.Command.Parameters.Add("@SH_CODE", SpaceReqDetails.spcreqdet[0].SRD_SH_CODE);
        sp.Command.Parameters.Add("@REQID", SpaceReqDetails.spcreqdet[0].SRD_SRNREQ_ID);
        sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);

        using (IDataReader dr = sp.GetReader())
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                {
                    return new { Message = dr["MSG"].ToString(), data = 1 };
                }
                else if ((int)dr["FLAG"] == 2)
                {
                    return new { Message = dr["MSG"].ToString(), data = 2 };
                }
                else if ((int)dr["FLAG"] == 0)
                {
                    return new { Message = MessagesVM.ErrorMessage, data = (object)null };
                }
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object CheckExists(SpaceReqDetails spcreqdetails)
    {
        //spcreqdetails = new List<SpaceRequistion_details>();
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@SPC_REQ_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(spcreqdetails.spcreqdet);

        if (spcreqdetlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = spcreqdetlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

}