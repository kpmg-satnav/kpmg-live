﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ViewSpaceRequisitionService
/// </summary>
public class ViewSpaceRequisitionService
{
    SubSonic.StoredProcedure sp;
    public object GetPendingSpaceRequisitions()
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "View_Space_Requisition_Details");
            sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            using (IDataReader reader = sp.GetReader())
            {
                List<SpaceReq> spclist = new List<SpaceReq>();
                while (reader.Read())
                {
                    spclist.Add(new SpaceReq()
                    {
                        SRN_REQ_ID = reader["SRN_REQ_ID"].ToString(),
                        VER_NAME = reader["VER_NAME"].ToString(),
                        COST_CENTER_NAME = reader["COST_CENTER_NAME"].ToString(),
                        STA_DESC = reader["STA_DESC"].ToString(),
                        SRN_FROM_DATE = (DateTime)reader["SRN_FROM_DATE"],
                        SRN_TO_DATE = (DateTime)reader["SRN_TO_DATE"],
                        AUR_KNOWN_AS = reader["AUR_KNOWN_AS"].ToString(),
                        SRN_REQ_BY = reader["SRN_REQ_BY"].ToString(),
                        SRN_REQ_REM = reader["SRN_REQ_REM"].ToString(),
                        SRN_STA_ID = (int)reader["SRN_STA_ID"],
                        SRN_REQ_DT = (DateTime)reader["SRN_REQ_DT"],
                        SRN_SYS_PRF_CODE = reader["SRN_SYS_PRF_CODE"].ToString(),
                        SRN_VERTICAL = reader["SRN_VERTICAL"].ToString(),
                        SRN_COST_CENTER = reader["SRN_COST_CENTER"].ToString(),
                    });
                }
                reader.Close();
                if (spclist.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = spclist };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
    public object UpdateSpaceRequisition(SpaceReqDetails spcreqDetails)
    {
        try
        {
            List<SpaceRequistion> spcreqLst = new List<SpaceRequistion>();
            spcreqLst.Add(spcreqDetails.spcreq);
            if (spcreqLst.Count != 0)
            {
                SqlParameter[] param = new SqlParameter[7];
                param[0] = new SqlParameter("@SPC_REQ", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(spcreqLst);
                param[1] = new SqlParameter("@SPC_REQ_DET", SqlDbType.Structured);
                if (spcreqDetails.spcreqdet == null)
                {
                    param[1].Value = null;
                }
                else
                {
                    param[1].Value = UtilityService.ConvertToDataTable(spcreqDetails.spcreqdet);
                }

                param[2] = new SqlParameter("@FLRLST", SqlDbType.Structured);
                param[2].Value = UtilityService.ConvertToDataTable(spcreqDetails.flrlst);
                param[3] = new SqlParameter("@SPC_REQ_DET_COUNT", SqlDbType.Structured);
                if (spcreqDetails.spcreqcount == null)
                {
                    param[3].Value = null;
                }
                else
                {
                    param[3].Value = UtilityService.ConvertToDataTable(spcreqDetails.spcreqcount);
                }
                param[4] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
                param[4].Value = spcreqDetails.ALLOCSTA;
                param[5] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[5].Value = HttpContext.Current.Session["UID"];
                param[6] = new SqlParameter("@COMPANY_ID", SqlDbType.Int);
                param[6].Value = HttpContext.Current.Session["COMPANYID"];
                using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SMS_SPACE_REQUISITION_UPDATE_TRAN", param))
                {
                    if (dr.Read())
                    {
                        int FLAG = (int)dr["FLAG"];
                        string REQ = dr["REQ"].ToString();

                        if (FLAG == 1)
                        {
                            string RetMessage = string.Empty;
                            RequestState sta = (RequestState)spcreqDetails.ALLOCSTA;
                            switch (sta)
                            {
                                case RequestState.Modified: RetMessage = MessagesVM.SPC_REQ_UPDATED;
                                    break;
                                case RequestState.Canceled: RetMessage = MessagesVM.SPC_REQ_CANCELED;
                                    break;
                            }

                            if (spcreqDetails.SRN_SYS_PRF_CODE == "1040")
                                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_SPACE_REQUISITION_BY_SPACES_SM");
                            else
                                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_SPACE_REQUISITION_BY_ACTION_SM");
                            sp.Command.AddParameter("@REQID", REQ, DbType.String);
                            sp.Command.AddParameter("@STATUS", spcreqDetails.ALLOCSTA, DbType.Int32);
                            sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
                            sp.ExecuteScalar();
                            return new { Message = RetMessage + " For : " + REQ, data = REQ };
                        }
                        else
                            return new { Message = REQ, data = (object)null };
                    }
                }
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }

            else
                return new { Message = MessagesVM.L1_NOSPC, data = (object)null };
        }

        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }
}