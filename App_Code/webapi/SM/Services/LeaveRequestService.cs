﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class LeaveRequestService
{
    SubSonic.StoredProcedure sp;
    LeaveRequestModel LeaveModel;
    LeaveGridData LeaveDetails;
    DataSet ds;

    public int SMS_Leave_Details(LeaveRequestModel Model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_EMPLOYEE_LEAVE_REQUEST");
            sp.Command.AddParameter("@SM_EMPLOYEE", Model.SelectedEmployee, DbType.String);
            sp.Command.AddParameter("@SM_FROM_DT", Model.FromDate, DbType.String);
            sp.Command.AddParameter("@SM_TO_DT", Model.ToDate, DbType.String);
            sp.Command.AddParameter("@SM_REASON ", Model.LeaveReason, DbType.String);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch { throw; }
    }

    public int DeleteData(LeaveRequestModel Model)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_SPACE_EMPLOYEE_LEAVE_REQUEST_DELETE");
            sp.Command.AddParameter("@SM_EMPLOYEE", Model.SelectedEmployee, DbType.String);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch { throw; }
    }
    public List<LeaveGridData> LeaveGridDetails()
    {
        try
        {
            List<LeaveGridData> LData = new List<LeaveGridData>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SMS_GET_EMP_LEAVE_DATA");
            using (IDataReader reader = sp.GetReader())
            {
                while (reader.Read())
                {
                    LeaveDetails = new LeaveGridData();
                    LeaveDetails.ID = reader["ID"].ToString();
                    LeaveDetails.NAME = reader["AUR_NAME"].ToString();
                    LeaveDetails.AUR_EMAIL = reader["AUR_EMAIL"].ToString();
                    LeaveDetails.FromDate = Convert.ToDateTime(reader["AUR_LONG_LEAVE_FROM_DT"].ToString());
                    LeaveDetails.ToDate = Convert.ToDateTime(reader["AUR_LONG_LEAVE_TO_DT"].ToString());
                    LeaveDetails.Reason = reader["AUR_LONG_LEAVE_REASON"].ToString();
                    LeaveDetails.AUR_CITY = reader["AUR_CITY"].ToString();
                    LeaveDetails.AUR_LOCATION = reader["AUR_LOCATION"].ToString();
                    LData.Add(LeaveDetails);
                }
                reader.Close();
            }
            return LData;
        }
        catch { throw; }
    }
}