﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ViewSpaceProjectionService
/// </summary>
public class ViewSpaceProjectionService
{
    SubSonic.StoredProcedure sp;
    public object GetPlanRequisitions()
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "OSP_VIEW_MODIFY_PLAN");
            sp.Command.AddParameter("@USR_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANY_ID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            using (IDataReader reader = sp.GetReader())
            {
                List<OfficeReq> osplist = new List<OfficeReq>();
                while (reader.Read())
                {
                    osplist.Add(new OfficeReq()
                    {
                        OSP_REQ_ID = reader["REQ_ID"].ToString(),
                        OSP_CREATED_DATE = reader["CREATED_DATE"].ToString(),
                        OSP_CREATED_BY = reader["CREATED_BY"].ToString(),
                        OSP_STA_ID = reader["STA_TITLE"].ToString(),
                    });
                }

                reader.Close();
                if (osplist.Count != 0)
                    return new { Message = MessagesVM.UM_OK, data = osplist };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object GetPlansListbyReqId(string REQ_ID)
    {
        try
        {
            DataSet ds = new DataSet();
            SqlParameter[] spm = new SqlParameter[1];
            spm[0] = new SqlParameter("@REQ_ID", REQ_ID);
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "OSP_PLAN_REQ_DETAILS", spm);
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, ErrorMsg = ex, data = (object)null };
        }
    }

    public object UpdateDetails(OSPSaveList ospobj)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@SM_OSP_TVP", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(ospobj.savelst);
            param[1] = new SqlParameter("@USR_ID", HttpContext.Current.Session["UID"]);
            param[2] = new SqlParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);
            param[3] = new SqlParameter("@REQ_ID", SqlDbType.VarChar);
            param[3].Value = ospobj.REQID;
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "OSP_MODIFY_DETAILS", param);
            return ds;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public object CancelDetails(OSPSaveList ospobj)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@REQ_ID", SqlDbType.VarChar);
            param[0].Value = ospobj.REQID;
            param[1] = new SqlParameter("@USR_ID", HttpContext.Current.Session["UID"]);
            param[2] = new SqlParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"]);
            DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "OSP_CANCEL_PLAN", param);
            return ds;
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
}