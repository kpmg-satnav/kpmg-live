﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using DocumentFormat.OpenXml.Drawing.Charts;

/// <summary>
/// Summary description for SpaceSeatHistoryReportService
/// </summary>
public class SpaceSeatHistoryReportService
{
    SubSonic.StoredProcedure sp;
    
    
    DataSet ds;

    public object GetCustomizedDetails(SpaceSeatHistoryVM Data)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "SPACE_BOOKING_HISTORY");
            sp.Command.Parameters.Add("@From_Date", Data.FromDate, DbType.Date);
            sp.Command.Parameters.Add("@To_Date", Data.ToDate, DbType.Date);
            sp.Command.Parameters.Add("@STATUS",  Data.Status, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables[0];

        }
        catch (Exception e)
        {
            throw;
        }
    }

    
}