﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SpaceHistoryReportCalService
/// </summary>
public class SpaceHistoryReportCalService
{
    SubSonic.StoredProcedure sp;

    public object GetDailyCount(SpaceHistoryReportCalVM Data)
    {
       // DataTable DT = new DataTable();
        try
        {
            SqlParameter[] param = new SqlParameter[4];
            DataSet ds = new DataSet();

           

            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "Get_Space_daily_booking_count");
            sp.Command.Parameters.Add("@From_Date", Data.FromDate, DbType.Date);
            sp.Command.Parameters.Add("@To_Date", Data.ToDate, DbType.Date);
            sp.Command.Parameters.Add("@Floors", Data.Floors, DbType.String);
            sp.Command.Parameters.Add("@Location", Data.Locations, DbType.String);
            sp.Command.Parameters.Add("@Towers", Data.Towers, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables[0];

        } 
        catch (Exception e)
        {
            throw;
        }
    }

    public object GetDayData(SpaceHistoryReportCalVM Data)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "GET_SPACE_DETAILS_BY_DATE");
            sp.Command.Parameters.Add("@DATE_VALUE", Data.FromDate, DbType.Date);
            sp.Command.Parameters.Add("@Floors", Data.Floors, DbType.String);
            sp.Command.Parameters.Add("@Location", Data.Locations, DbType.String);
            sp.Command.Parameters.Add("@Towers", Data.Towers, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables[0];

        }
        catch (Exception e)
        {
            throw;
        }
    }
}