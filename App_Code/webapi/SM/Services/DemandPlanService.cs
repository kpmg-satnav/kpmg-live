﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for DemandPlanService
/// </summary>
public class DemandPlanService
{
    SubSonic.StoredProcedure sp;
    DataSet dsDemand;
    SpaceExtensionVM sevm;

    public object UploadTemplate(HttpRequest httpRequest)
    {
        try
        {
            List<clsDemandPlan> uadmlst = GetDataTableFrmReq(httpRequest);
            int res = 0;

            sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "DEMAND_PLAN_DATA_DELETE");
            DataSet ds = sp.GetDataSet();
            //if (ds.Tables[0].Rows.Count == 0)
            //{
                foreach (clsDemandPlan uadm in uadmlst)
                {
                    SqlParameter[] param = new SqlParameter[34];
                    param[0] = new SqlParameter("@DemandRaisedBy", SqlDbType.VarChar);
                    param[0].Value = uadm.DemandRaisedBy;
                    param[1] = new SqlParameter("@DeamndRaisedOn", SqlDbType.VarChar);
                    param[1].Value = uadm.DeamndRaisedOn;
                    param[2] = new SqlParameter("@DemandApprovedBy", SqlDbType.VarChar);
                    param[2].Value = uadm.DemandApprovedBy;
                    param[3] = new SqlParameter("@DemandApprovedOn", SqlDbType.VarChar);
                    param[3].Value = uadm.DemandApprovedOn;
                    param[4] = new SqlParameter("@DemandStatus", SqlDbType.VarChar);
                    param[4].Value = uadm.DemandStatus;
                    param[5] = new SqlParameter("@FulfilmentStatus", SqlDbType.VarChar);
                    param[5].Value = uadm.FulfilmentStatus;
                    param[6] = new SqlParameter("@Geography", SqlDbType.VarChar);
                    param[6].Value = uadm.Geography;
                    param[7] = new SqlParameter("@Location", SqlDbType.VarChar);
                    param[7].Value = uadm.Location;
                    param[8] = new SqlParameter("@Floor", SqlDbType.VarChar);
                    param[8].Value = uadm.Floor;
                    param[9] = new SqlParameter("@Function", SqlDbType.VarChar);
                    param[9].Value = uadm.Function;
                    param[10] = new SqlParameter("@SubFunction", SqlDbType.VarChar);
                    param[10].Value = uadm.SubFunction;
                    param[11] = new SqlParameter("@Teams", SqlDbType.VarChar);
                    param[11].Value = uadm.Teams;
                    param[12] = new SqlParameter("@Month1", SqlDbType.VarChar);
                    param[12].Value = uadm.Month1;
                    param[13] = new SqlParameter("@Month2", SqlDbType.VarChar);
                    param[13].Value = uadm.Month2;
                    param[14] = new SqlParameter("@Month3", SqlDbType.VarChar);
                    param[14].Value = uadm.Month3;
                    param[15] = new SqlParameter("@Month4", SqlDbType.VarChar);
                    param[15].Value = uadm.Month4;
                    param[16] = new SqlParameter("@Month5", SqlDbType.VarChar);
                    param[16].Value = uadm.Month5;
                    param[17] = new SqlParameter("@Month6", SqlDbType.VarChar);
                    param[17].Value = uadm.Month6;
                    param[18] = new SqlParameter("@Month7", SqlDbType.VarChar);
                    param[18].Value = uadm.Month7;
                    param[19] = new SqlParameter("@Month8", SqlDbType.VarChar);
                    param[19].Value = uadm.Month8;
                    param[20] = new SqlParameter("@Month9", SqlDbType.VarChar);
                    param[20].Value = uadm.Month9;
                    param[21] = new SqlParameter("@Month10", SqlDbType.VarChar);
                    param[21].Value = uadm.Month10;
                    param[22] = new SqlParameter("@Month11", SqlDbType.VarChar);
                    param[22].Value = uadm.Month11;
                    param[23] = new SqlParameter("@Month12", SqlDbType.VarChar);
                    param[23].Value = uadm.Month12;
                    param[24] = new SqlParameter("@CurrentYearQ1", SqlDbType.VarChar);
                    param[24].Value = uadm.CurrentYearQ1;
                    param[25] = new SqlParameter("@CurrentYearQ2", SqlDbType.VarChar);
                    param[25].Value = uadm.CurrentYearQ2;
                    param[26] = new SqlParameter("@CurrentYearQ3", SqlDbType.VarChar);
                    param[26].Value = uadm.CurrentYearQ3;
                    param[27] = new SqlParameter("@CurrentYearQ4", SqlDbType.VarChar);
                    param[27].Value = uadm.CurrentYearQ4;
                    param[28] = new SqlParameter("@NextYearQ1", SqlDbType.VarChar);
                    param[28].Value = uadm.NextYearQ1;
                    param[29] = new SqlParameter("@NextYearQ2", SqlDbType.VarChar);
                    param[29].Value = uadm.NextYearQ2;
                    param[30] = new SqlParameter("@NextYearQ3", SqlDbType.VarChar);
                    param[30].Value = uadm.NextYearQ3;
                    param[31] = new SqlParameter("@NextYearQ4", SqlDbType.VarChar);
                    param[31].Value = uadm.NextYearQ4;
                    param[32] = new SqlParameter("@UserId", SqlDbType.Int);
                    param[32].Value = 43531;//HttpContext.Current.Session["Uid"];
                    param[33] = new SqlParameter("@CompanyId", SqlDbType.Int);
                    param[33].Value = 43532;//HttpContext.Current.Session["COMPANYID"];
                                            //param[34] = new SqlParameter("@FLAG", SqlDbType.Int);
                                            //param[34].Value = 0;
                    object Count = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "DEMAND_PLAN_DATA_INSERT", param);
                    //res = (int)Count;
                }
            //}
            //if (res > 0)
            //{
                sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "DEMAND_PLAN_GET_LIST");
                dsDemand = sp.GetDataSet();
            //}

            if (dsDemand.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = new { LST = dsDemand.Tables[0] } };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
        }
    }
    public List<clsDemandPlan> GetDataTableFrmReq(HttpRequest httpRequest)
    {
        DataTable dt = new DataTable();

        var postedFile = httpRequest.Files[0];
        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
        postedFile.SaveAs(filePath);


        var uplst1 = CreateExcelFile.ReadAsDemandPlanList(filePath);
        if (uplst1.Count != 0)
            uplst1.RemoveAt(0);
        return uplst1;

    }


    

}