﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;


/// <summary>
/// Summary description for TradeMomentOfEmployeeRptService
/// </summary>
public class TrailMomentOfEmployeeRptService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;


    public object GetTrailMomentOfEmployeeRpt()
    {
        try
        {

            List<GridCols> gridcols = new List<GridCols>();
            GridCols gridcolscls;
            ExportClass exportcls;
            List<ExportClass> lstexportcls = new List<ExportClass>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "TRAIL_MOMENT_OF_EMPLOYEE_RPT_DYNAMIC");

            DataSet ds = sp.GetDataSet();

            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();

            foreach (string col in Colstr)
            {
                gridcolscls = new GridCols();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                //gridcolscls.suppressMenu = true;
                gridcols.Add(gridcolscls);

                exportcls = new ExportClass();
                exportcls.title = col;
                exportcls.key = col;
                lstexportcls.Add(exportcls);
            }
            return new { griddata = ds.Tables[0], Coldef = gridcols, exportCols = lstexportcls };
        }
        catch
        {
            throw;
        }
    }

}