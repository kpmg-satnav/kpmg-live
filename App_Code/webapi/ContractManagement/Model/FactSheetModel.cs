﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FactSheetModel
/// </summary>

public class ContractMst
{
    public string DCT_CODE { set; get; }
    public string DCT_NAME { set; get; }
    public string DCT_STA_ID { set; get; }
    public string DCT_REM { set; get; }
}
public class ContractReportDetails
{
    public string DFM_DCT_CODE { set; get; }
    public string DFM_DST_CODE { set; get; }
    public string DFM_DBT_CODE { set; get; }
    public string DFM_DSD_CODE { set; get; }  
    public DateTime EFFECTIVE_DATE { set; get; }
    public DateTime FROM_DATE { set; get; }
    public DateTime TO_DATE { set; get; }

}
public class AgreementReportDetails
{
    public string DMC_CODE { set; get; }
    public string DBT_CODE { set; get; }
    public string DRS_CODE { set; get; }
    public DateTime FROM_DATE { set; get; }
    public DateTime TO_DATE { set; get; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
    public string SearchValue { get; set; }

}
public class CategoryMst
{
    public string DST_CODE { set; get; }
    public string DST_NAME { set; get; }
    public string DST_STA_ID { set; get; }
    public string DST_REM { set; get; }
}

public class BusinessMst
{
    public string DBT_CODE { set; get; }
    public string DBT_NAME { set; get; }
    public string DBT_STA_ID { set; get; }
    public string DBT_REM { set; get; }
}

public class DocumentMst
{
    public string DDT_CODE { set; get; }
    public string DDT_NAME { set; get; }
    public string DDT_STA_ID { set; get; }
    public string DDT_REM { set; get; }
}

public class SupplierMst
{
    public string DSD_CODE { set; get; }
    public string DSD_NAME { set; get; }
    public string DSD_STA_ID { set; get; }
    public string DSD_REM { set; get; }
    public string DSD_EMAIL { set; get; }
    public string DSD_PHONE { set; get; }
    public string DSD_ADDR { set; get; }
}

public class RemainderDetails
{
    public string DRD_SNO { set; get; }
    public string DRD_NAME { set; get; }
    public string DRD_STA_ID { set; get; }
}

public class NotifcationDetails
{
    public string DRN_SNO { set; get; }
    public string DRN_NAME { set; get; }
    public string DRN_STA_ID { set; get; }
}

public class NoticesDetails
{
    public string DNP_CODE { set; get; }
    public string DNP_NAME { set; get; }
    public string DNP_STA_ID { set; get; }
}


public class FactSheetVm
{
    public MainSheet MainSheet { get; set; }
    public AgreeMentSheet AgreeMentSheet { get; set; }
    public POSheet POSheet { get; set; }
    public List<RemaindersSheet> RemainderSheet { get; set; }
    public ValuesSheet ValuesSheet { get; set; }
    public StakeholderSheet StakeholderSheet { get; set; }
    public List<BusinessStakeHolder> BusinessStakehol { get; set; }
    public List<GetDocumentDetailsVM> DocumentDetails { get; set; }
    public int DRN_SNO { get; set; }
    public int DRD_SNO { get; set; }
   
}

public class MainSheet
{
    public string DFM_DCT_CODE { get; set; }
    public string DFM_DST_CODE { get; set; }
    public string DFM_DBT_CODE { get; set; }
    public string DFM_DSD_CODE { get; set; }
    public string DFM_REF_NO { get; set; }
    public string DFM_CREATED_BY { get; set; }
    public string DFM_UPDATED_BY { get; set; }
    public string DFM_REQ_ID { get; set; }
}

public class AgreeMentSheet
{
    public Nullable<DateTime> DFDT_SIGN_DATE { get; set; }
    public Nullable<DateTime> DFDT_EFRM_DATE { get; set; }
    public Nullable<DateTime> DFDT_TRMN_DATE { get; set; }
    public Nullable<DateTime> DFDT_REN_DATE { get; set; }
    public string DFDT_NOTF_PRD_WITH_CLAUSE { get; set; }
    public string DFDT_NOTF_PRD_WITHOUT_CLAUSE { get; set; }
}
public class POSheet
{
    public DateTime? DFDT_PO_EFRM_DATE { get; set; }
    public Nullable<DateTime> DFDT_PO_TRMN_DATE { get; set; }
    public Nullable<DateTime> DFDT_PO_REN_DATE { get; set; }
    public string DFDT_PO_NOTF_PRD_WITH_CLAUSE { get; set; }
    public string DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE { get; set; }
}
public class RemaindersSheet
{
    public int? DRD_SNO { get; set; }
    public string DRD_NAME { get; set; }
}

public class ValuesSheet
{
    public int DFV_CNRT_VAL { get; set; }
    public string DFV_CNRT_SCP { get; set; }
    public int DFV_ANNL_VAL { get; set; }
}

public class StakeholderSheet
{
    public string DFD_CT { get; set; }
    public string DFD_AC { get; set; }
    public string DFD_PT { get; set; }
    public string DFD_TC { get; set; }
    public string DFD_DW { get; set; }
    public string DFD_IPR { get; set; }
    public string DFD_CONFD { get; set; }
    public string DFD_CD { get; set; }
}

public class BusinessStakeHolder
{
    public string DFKD_NAME { get; set; }
}

public class GetDocumentDetailsVM
{
    public int DFDC_ID { get; set; }
    public string DFDC_DDT_CODE { get; set; }
    public string DFDC_NAME { get; set; }
    public string DFDC_ACT_NAME { get; set; }
}

public class GetFactDetails
{
    public string DFM_REQ_ID { get; set; }
    public string DCT_NAME { get; set; }
    public string DSD_NAME { get; set; }
    public string DST_NAME { get; set; }
    public string DBT_NAME { get; set; }
    public string DFM_REF_NO { get; set; }
    public string DFM_CREATED_BY { get; set; }
}

public class DRMExcel
{
    public string DFDC_ID { get; set; }
}