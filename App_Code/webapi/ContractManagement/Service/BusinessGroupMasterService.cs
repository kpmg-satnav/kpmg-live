﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BusinessGroupMasterService
/// </summary>
public class BusinessGroupMasterService
{
    SubSonic.StoredProcedure sp;
    public IEnumerable<BusinessMst> BindBusinessData()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_BUSINESS_GROUP_DETAILS]").GetReader();
        List<BusinessMst> BussMst = new List<BusinessMst>();
        while (reader.Read())
        {
            BussMst.Add(new BusinessMst()
            {
                DBT_CODE = reader.GetValue(0).ToString(),
                DBT_NAME = reader.GetValue(1).ToString(),
                DBT_STA_ID = reader.GetValue(2).ToString(),
                DBT_REM = reader.GetValue(3).ToString(),
            });
        }
        reader.Close();
        return BussMst;
    }

    public int Insert(BusinessMst Dataobj)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "INSERT_BUSINESS_GROUP_DETAILS");
        sp.Command.AddParameter("@DBT_CODE", Dataobj.DBT_CODE, DbType.String);
        sp.Command.AddParameter("@DBT_NAME", Dataobj.DBT_NAME, DbType.String);
        sp.Command.AddParameter("@DBT_STA_ID", Dataobj.DBT_STA_ID, DbType.String);
        sp.Command.AddParameter("@DBT_REM", Dataobj.DBT_REM, DbType.String);
        sp.Command.AddParameter("@DBT_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        int flag = (int)sp.ExecuteScalar();
        return flag;
    }

    //Update
    public Boolean Update(BusinessMst Update)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "UPDATE_BUSINESS_GROUP_DETAILS");
        sp.Command.AddParameter("@DBT_CODE", Update.DBT_CODE, DbType.String);
        sp.Command.AddParameter("@DBT_NAME", Update.DBT_NAME, DbType.String);
        sp.Command.AddParameter("@DBT_STA_ID", Update.DBT_STA_ID, DbType.Int32);
        sp.Command.AddParameter("@DBT_REM", Update.DBT_REM, DbType.String);
        sp.Command.Parameters.Add("@DBT_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        sp.Execute();
        return true;
    }
}