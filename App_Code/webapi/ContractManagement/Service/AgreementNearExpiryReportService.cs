﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class AgreementNearExpiryReportService
{
    SubSonic.StoredProcedure sp;

    public object GetAgreementNearExpiryReport(AgreementReportDetails angrex)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[4];          
            param[0] = new SqlParameter("@FROM_DATE", SqlDbType.DateTime);
            param[0].Value = angrex.FROM_DATE;
            param[1] = new SqlParameter("@TO_DATE", SqlDbType.DateTime);
            param[1].Value = angrex.TO_DATE;
            param[2] = new SqlParameter("@PageNumber", SqlDbType.Int);
            param[2].Value = angrex.PageNumber;
            param[3] = new SqlParameter("@PageSize", SqlDbType.Int);
            param[3].Value = angrex.PageSize;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_AGREEMENT_NEAR_EXPIRY_DRM", param);
            return new { data = ds.Tables[0] };
        }
        catch (Exception Ex)
        {

            return new { Message = "Invalid Input! Please try again", data = (object)null, ErrorMessage = Ex.InnerException };
        }
    }

    public object SearchAgreementNearExpiryReport(AgreementReportDetails Sangrex)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@FROM_DATE", SqlDbType.DateTime);
            param[0].Value = Sangrex.FROM_DATE;
            param[1] = new SqlParameter("@TO_DATE", SqlDbType.DateTime);
            param[1].Value = Sangrex.TO_DATE;
            param[2] = new SqlParameter("@PageNumber", SqlDbType.Int);
            param[2].Value = Sangrex.PageNumber;
            param[3] = new SqlParameter("@PageSize", SqlDbType.Int);
            param[3].Value = Sangrex.PageSize;
            param[4] = new SqlParameter("@SearchValue", SqlDbType.VarChar);
            param[4].Value = Sangrex.SearchValue;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_AGREEMENT_NEAR_EXPIRY_DRM_SEARCH", param);
            return new { data = ds.Tables[0] };
        }
        catch (Exception Ex)
        {

            return new { Message = "Invalid Input! Please try again", data = (object)null, ErrorMessage = Ex.InnerException };
        }
    }
}