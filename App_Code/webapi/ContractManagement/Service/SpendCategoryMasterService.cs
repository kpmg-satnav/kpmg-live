﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SpendCategoryMasterService
/// </summary>
public class SpendCategoryMasterService
{
    SubSonic.StoredProcedure sp;
    public IEnumerable<CategoryMst> BindCategoryData()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_SPEND_CATEGORY_DETAILS]").GetReader();
        List<CategoryMst> CatMst = new List<CategoryMst>();
        while (reader.Read())
        {
            CatMst.Add(new CategoryMst()
            {
                DST_CODE = reader.GetValue(0).ToString(),
                DST_NAME = reader.GetValue(1).ToString(),
                DST_STA_ID = reader.GetValue(2).ToString(),
                DST_REM = reader.GetValue(3).ToString(),
            });
        }
        reader.Close();
        return CatMst;
    }

    public int Insert(CategoryMst Dataobj)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "INSERT_CATEGORY_DETAILS");
        sp.Command.AddParameter("@DST_CODE", Dataobj.DST_CODE, DbType.String);
        sp.Command.AddParameter("@DST_NAME", Dataobj.DST_NAME, DbType.String);
        sp.Command.AddParameter("@DST_STA_ID", Dataobj.DST_STA_ID, DbType.String);
        sp.Command.AddParameter("@DST_REM", Dataobj.DST_REM, DbType.String);
        sp.Command.AddParameter("@DST_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        int flag = (int)sp.ExecuteScalar();
        return flag;
    }

    //Update
    public Boolean Update(CategoryMst Update)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "UPDATE_SPEND_CATEGORY_MASTER_DETAILS");
        sp.Command.AddParameter("@DST_CODE", Update.DST_CODE, DbType.String);
        sp.Command.AddParameter("@DST_NAME", Update.DST_NAME, DbType.String);
        sp.Command.AddParameter("@DST_STA_ID", Update.DST_STA_ID, DbType.Int32);
        sp.Command.AddParameter("@DST_REM", Update.DST_REM, DbType.String);
        sp.Command.Parameters.Add("@DST_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        sp.Execute();
        return true;
    }
}