﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class SupplierWiseAgreementReportService
{
    SubSonic.StoredProcedure sp;

    public object GetSupplierWiseData(SupplierData suppdata )
    {
        try
        {

            //SqlParameter[] param = new SqlParameter[2];
            //param[0] = new SqlParameter("@PageNumber", SqlDbType.Int);
            //param[0].Value = suppdata.PageNumber;
            //param[1] = new SqlParameter("@PageSize", SqlDbType.Int);
            //param[1].Value = suppdata.PageSize;           
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_VENDOR_WISE_AGREEMENT_DATA");
            UtilityService userv = new UtilityService();
            DataTable dt= userv.GetInversedDataTable(ds.Tables[0], "DSD_NAME");
            var columns = dt.Columns.Cast<DataColumn>()
                                    .Select(x => x.ColumnName)
                                    .ToArray();
            return new { data = new { gridData = ds.Tables[0], graphData = dt.Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray(), columnNames = columns } };
        }
        catch (Exception Ex)
        {

            return new { Message = "Invalid Input! Please try again", data = (object)null, ErrorMessage = Ex.InnerException };
        }
    }
     public object SearchSupplierWiseData(SupplierData suppdata )
    {
        try
        {

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@PageNumber", SqlDbType.Int);
            param[0].Value = suppdata.PageNumber;
            param[1] = new SqlParameter("@PageSize", SqlDbType.Int);
            param[1].Value = suppdata.PageSize;
            param[2] = new SqlParameter("@SearchValue", SqlDbType.VarChar);
            param[2].Value = suppdata.SearchValue;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_VENDOR_WISE_AGREEMENT_DATA_SEARCH", param);
            UtilityService userv = new UtilityService();
            DataTable dt= userv.GetInversedDataTable(ds.Tables[0], "DSD_NAME");
            var columns = dt.Columns.Cast<DataColumn>()
                                    .Select(x => x.ColumnName)
                                    .ToArray();
            return new { data = new { gridData = ds.Tables[0], graphData = dt.Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray(), columnNames = columns } };
        }
        catch (Exception Ex)
        {

            return new { Message = "Invalid Input! Please try again", data = (object)null, ErrorMessage = Ex.InnerException };
        }
    }
}