﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SupplierMasterService
/// </summary>
public class SupplierMasterService
{
    SubSonic.StoredProcedure sp;
    public IEnumerable<SupplierMst> BindSupplierData()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_SUPPLIER_DETAILS]").GetReader();
        List<SupplierMst> SuppMst = new List<SupplierMst>();
        while (reader.Read())
        {
            SuppMst.Add(new SupplierMst()
            {
                DSD_CODE = reader.GetValue(0).ToString(),
                DSD_NAME = reader.GetValue(1).ToString(),
                DSD_STA_ID = reader.GetValue(2).ToString(),
                DSD_REM = reader.GetValue(3).ToString(),
                DSD_PHONE = reader.GetValue(4).ToString(),
                DSD_EMAIL = reader.GetValue(5).ToString(),
                DSD_ADDR = reader.GetValue(6).ToString(),
            });
        }
        reader.Close();
        return SuppMst;
    }

    public int Insert(SupplierMst Dataobj)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "INSERT_SUPPLIER_DETAILS");
        sp.Command.AddParameter("@DSD_CODE", Dataobj.DSD_CODE, DbType.String);
        sp.Command.AddParameter("@DSD_NAME", Dataobj.DSD_NAME, DbType.String);
        sp.Command.AddParameter("@DSD_STA_ID", Dataobj.DSD_STA_ID, DbType.String);
        sp.Command.AddParameter("@DSD_REM", Dataobj.DSD_REM, DbType.String);
        sp.Command.AddParameter("@DSD_PHONE", Dataobj.DSD_PHONE, DbType.String);
        sp.Command.AddParameter("@DSD_EMAIL", Dataobj.DSD_EMAIL, DbType.String);
        sp.Command.AddParameter("@DSD_ADDR", Dataobj.DSD_ADDR, DbType.String);
        sp.Command.AddParameter("@DSD_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        int flag = (int)sp.ExecuteScalar();
        return flag;
    }

    //Update
    public Boolean Update(SupplierMst Update)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "UPDATE_SUPPLIER_DETAILS");
        sp.Command.AddParameter("@DSD_CODE", Update.DSD_CODE, DbType.String);
        sp.Command.AddParameter("@DSD_NAME", Update.DSD_NAME, DbType.String);
        sp.Command.AddParameter("@DSD_STA_ID", Update.DSD_STA_ID, DbType.Int32);
        sp.Command.AddParameter("@DSD_REM", Update.DSD_REM, DbType.String);
        sp.Command.AddParameter("@DSD_PHONE", Update.DSD_PHONE, DbType.String);
        sp.Command.AddParameter("@DSD_EMAIL", Update.DSD_EMAIL, DbType.String);
        sp.Command.AddParameter("@DSD_ADDR", Update.DSD_ADDR, DbType.String);
        sp.Command.Parameters.Add("@DSD_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        sp.Execute();
        return true;
    }
}