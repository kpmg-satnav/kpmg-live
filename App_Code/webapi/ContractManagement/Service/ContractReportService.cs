﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;



public class ContractReportService
{
    SubSonic.StoredProcedure sp;

    public object GetData(ContractReportDetails ContractData)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@CONTRACT_TYPE", SqlDbType.VarChar);
            param[0].Value = ContractData.DFM_DCT_CODE;
            param[1] = new SqlParameter("@SPEND_CATEGORY", SqlDbType.VarChar);
            param[1].Value = ContractData.DFM_DST_CODE;
            param[2] = new SqlParameter("@BUSINESS_GROUP", SqlDbType.VarChar);
            param[2].Value = ContractData.DFM_DBT_CODE;
            param[3] = new SqlParameter("@SUPPLIER_NAME", SqlDbType.VarChar);
            param[3].Value = ContractData.DFM_DSD_CODE;
            param[4] = new SqlParameter("@FROM_DATE", SqlDbType.DateTime);
            param[4].Value = ContractData.FROM_DATE;
            param[5] = new SqlParameter("@TO_DATE", SqlDbType.DateTime);
            param[5].Value = ContractData.TO_DATE;
            param[6] = new SqlParameter("@EFFECTIVE_DATE", SqlDbType.VarChar);
            param[6].Value = ContractData.EFFECTIVE_DATE;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_FACTSHEET_DETAILS_RPT", param);
            return new {data = ds.Tables[0]};
        }
        catch (Exception Ex)
        {

            return new { Message = "Invalid Input! Please try again", data = (object)null, ErrorMessage = Ex.InnerException };
        }
    }
 
}