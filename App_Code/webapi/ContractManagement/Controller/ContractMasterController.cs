﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;

public class ContractMasterController : ApiController
{
    ContractMasterService CMS = new ContractMasterService();

    [HttpGet]
    public HttpResponseMessage BindContractData()
    {
        IEnumerable<ContractMst> ChildEntityList = CMS.BindContractData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ChildEntityList);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage Insert(ContractMst Dataobj)
    {
        if (CMS.Insert(Dataobj) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Dataobj);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code Already Exists");
    }

    [HttpPost]
    public HttpResponseMessage Update(ContractMst Update)
    {
        if (CMS.Update(Update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }

}
