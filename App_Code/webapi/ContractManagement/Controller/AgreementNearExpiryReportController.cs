﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;


public class AgreementNearExpiryReportController:ApiController
{
    AgreementNearExpiryReportService Agnrex = new AgreementNearExpiryReportService();

    [HttpPost]
    public Object GetAgreementNearExpiryReport(AgreementReportDetails AgreementNearExpiryData)
    {
        var obj = Agnrex.GetAgreementNearExpiryReport(AgreementNearExpiryData);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public Object SearchAgreementNearExpiryReport(AgreementReportDetails SearchData)
    {
        var obj = Agnrex.SearchAgreementNearExpiryReport(SearchData);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}