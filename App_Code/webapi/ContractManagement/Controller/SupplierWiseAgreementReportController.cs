﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;


public class SupplierWiseAgreementReportController:ApiController
{
    SupplierWiseAgreementReportService swars = new SupplierWiseAgreementReportService();

    [HttpPost]
    public Object GetSupplierWiseData(SupplierData supp)
    {
        var obj = swars.GetSupplierWiseData(supp);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public Object SearchSupplierWiseData(SupplierData searchsupp)
    {
        var obj = swars.SearchSupplierWiseData(searchsupp);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}