﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class ConferenceUtilizationReportController : ApiController
{

    ConferenceUtilizationReportService cnfUtilService = new ConferenceUtilizationReportService();
    [HttpPost]
    public HttpResponseMessage GetConferenceReportBindGrid(ConfUtilParams CNPDet)
    {
        var obj = cnfUtilService.GetConferenceUtilizationObject(CNPDet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Chart Data

    [HttpPost]
    public HttpResponseMessage GetConferenceReportChartData(ConfUtilParams CNPDet)
    {
        var obj = cnfUtilService.GetConferenceUtilizationChart(CNPDet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> ExportConferenceUtilizationReportData(ConfUtilParams CNPDet)
    {

        ReportGenerator<ConferenceUtilizationReport> reportgen = new ReportGenerator<ConferenceUtilizationReport>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Conf_Mgmt/ConfUtilizeReport.rdlc"),
            DataSetName = "ConfUtilizeRptDS",
            ReportType = "Reservation Utilization Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/ConferenceUtilizationReport." + CNPDet.Type);
        List<ConferenceUtilizationReport> reportdata = cnfUtilService.GetConferenceUtilizationList(CNPDet);
        await reportgen.GenerateReport(reportdata, filePath, CNPDet.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "ConferenceUtilizationReport." + CNPDet.Type;
        return result;

    }

}
