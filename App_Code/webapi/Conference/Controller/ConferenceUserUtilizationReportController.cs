﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class ConferenceUserUtilizationReportController : ApiController
{


    ConferenceUserUtilizationReportService ConfUserUtilService = new ConferenceUserUtilizationReportService();
    [HttpPost]
    public HttpResponseMessage GetConferenceUserReportBindGrid(ConfUserUtilParams CNPDet)
    {
        var obj = ConfUserUtilService.GetConferenceUserUtilizationObject(CNPDet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //Chart Data

    [HttpPost]
    public HttpResponseMessage GetConferenceUserReportChartData(ConfUserUtilParams CNPDet)
    {
        var obj = ConfUserUtilService.GetConferenceUtilizationChart(CNPDet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> ExportConferenceUserUtilizationReportData(ConfUserUtilParams CNPDet)
    {

        ReportGenerator<ConferenceUserUtilizationReport> reportgen = new ReportGenerator<ConferenceUserUtilizationReport>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Conf_Mgmt/ConferenceUserUtilizationReport.rdlc"),
            DataSetName = "ConferenceUserUtilizationDs",
            ReportType = "Reservation User Utilization Report"
        };

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/ConferenceUserUtilizationReport." + CNPDet.Type);
        List<ConferenceUserUtilizationReport> reportdata = ConfUserUtilService.GetConferenceUserUtilizationList(CNPDet);
        await reportgen.GenerateReport(reportdata, filePath, CNPDet.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "Conference User Utilization Report." + CNPDet.Type;
        return result;

    }

}
