﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class ConferenceCustomizableReportController : ApiController
{

    ConferenceCustomizableReportService ConfUtilService = new ConferenceCustomizableReportService();
    [HttpPost]
    public HttpResponseMessage GetConferenceReportBindGrid(ConfCustomizedParams CNPDet)
    {
        var obj = ConfUtilService.GetConferenceUtilizationObject(CNPDet);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //Export Report Format
    [HttpPost]
    public async Task<HttpResponseMessage> GetCustomizedData([FromBody]ConfCustomizedParams data)
    {
        ReportGenerator<ConferenceCustomizableReport> reportgen = new ReportGenerator<ConferenceCustomizableReport>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Conference_Mgmt/CustomizedReport.rdlc"),
            DataSetName = "CustomizedReportDS",
            ReportType = "Reservation Customized Report"
        };

        ConfUtilService = new ConferenceCustomizableReportService();
        List<ConferenceCustomizableReport> reportdata = ConfUtilService.GetConferenceUtilizationList(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/Reservation_CustomizedReport." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "Reservation_CustomizedReport." + data.Type;
        return result;
    }
}
