﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;


public class ConferenceUtilizationReportService
{
    SubSonic.StoredProcedure sp;
    List<ConferenceUtilizationReport> ConfUtlList;

    DataSet ds;
    public object GetConferenceUtilizationObject(ConfUtilParams CNPDet)
    {
        try
        {
            ConfUtlList = GetConferenceUtilizationList(CNPDet);

            if (ConfUtlList.Count != 0)

                return new { Message = MessagesVM.UM_NO_REC, data = ConfUtlList };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public List<ConferenceUtilizationReport> GetConferenceUtilizationList(ConfUtilParams confUtlParams)
    {
        try
        {
            List<ConferenceUtilizationReport> ConfUtlList = new List<ConferenceUtilizationReport>();
            ConferenceUtilizationReport confUtlRpt;
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPC_AREA_REPORT");
            //sp.Command.AddParameter("@AUR_USER", HttpContext.Current.Session["UID"], DbType.String);

            //if (confUtlParams.CNP_NAME == null)
            //    sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            //else
            //    sp.Command.AddParameter("@COMPANYID", confUtlParams.CNP_NAME, DbType.Int32);


            /////////
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            if (confUtlParams.flrlst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(confUtlParams.flrlst);
            }
            param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
            param[1].Value = confUtlParams.CNF_FROM_DATE;
            param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
            param[2].Value = confUtlParams.CNF_TO_DATE;
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            if (confUtlParams.CNP_NAME == null)
                param[3].Value = HttpContext.Current.Session["COMPANYID"];
            else
                param[3].Value = confUtlParams.CNP_NAME;
            param[4] = new SqlParameter("@AUR_USER", SqlDbType.VarChar);
            param[4].Value = HttpContext.Current.Session["UID"].ToString();
            param[5] = new SqlParameter("@SEARCHVAL", SqlDbType.VarChar);
            param[5].Value = confUtlParams.SearchValue;
            param[6] = new SqlParameter("@PAGENUM", SqlDbType.VarChar);
            param[6].Value = confUtlParams.PageNumber;
            param[7] = new SqlParameter("@PAGESIZE", SqlDbType.VarChar);
            param[7].Value = confUtlParams.PageSize;
            ////////
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CONFERENCE_UTILIZATION_REPORT", param))


            //using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    confUtlRpt = new ConferenceUtilizationReport();
                    confUtlRpt.CNY_NAME = sdr["CNY_NAME"].ToString();
                    confUtlRpt.CTY_NAME = sdr["CTY_NAME"].ToString();
                    confUtlRpt.LCM_NAME = sdr["LCM_NAME"].ToString();
                    confUtlRpt.TWR_NAME = sdr["TWR_NAME"].ToString();
                    confUtlRpt.FLR_NAME = sdr["FLR_NAME"].ToString();
                    //confUtlRpt.FLR_AREA = (double)sdr["CONFERENCE_ROOM_NAME"];
                    confUtlRpt.CONFERENCE_ROOM_NAME = sdr["CONFERENCE_ROOM_NAME"].ToString();
                    confUtlRpt.TOTAL_HOURS = sdr["TOTAL_HOURS"].ToString();
                    confUtlRpt.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                    ConfUtlList.Add(confUtlRpt);
                }
                sdr.Close();
            }
            if (ConfUtlList.Count != 0)
                //return new { Message = MessagesVM.HDM_UM_OK, data = ConfUtlList };
                return ConfUtlList;
            else
                return new List<ConferenceUtilizationReport>();
        }
        catch
        {
            throw;
        }

    }


    public object GetConferenceUtilizationChart(ConfUtilParams confUtlParams)
    {
        try
        {
            //List<SpaceData> area = new List<SpaceData>();
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CONF_UTILIZATION_GRAPH");
            ////sp.Command.AddParameter("@FROM_DATE", Spcdetails.FromDate, DbType.DateTime);
            ////sp.Command.AddParameter("@TO_DATE", Spcdetails.ToDate, DbType.DateTime);
            ////sp.Command.AddParameter("@REQTYP", Spcdetails.Request_Type, DbType.String);
            //sp.Command.AddParameter("@AUR_USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            //if (ID == null)
            //    sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            //else
            //    sp.Command.AddParameter("@COMPANYID", ID, DbType.Int32);



            List<ConferenceUtilizationReport> ConfUtlList = new List<ConferenceUtilizationReport>();
            //ConferenceUtilizationReport confUtlRpt;

            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            if (confUtlParams.flrlst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(confUtlParams.flrlst);
            }
            param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
            param[1].Value = confUtlParams.CNF_FROM_DATE;
            param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
            param[2].Value = confUtlParams.CNF_TO_DATE;
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            if (confUtlParams.CNP_NAME == null)
                param[3].Value = HttpContext.Current.Session["COMPANYID"];
            else
                param[3].Value = confUtlParams.CNP_NAME;
            param[4] = new SqlParameter("@AUR_USER", SqlDbType.VarChar);
            param[4].Value = HttpContext.Current.Session["UID"].ToString();
            ////////
            //using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CONFERENCE_UTILIZATION_REPORT", param))



            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_CONF_UTILIZATION_GRAPH", param);
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }

        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
}