﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;
public class ConferenceTypeMasterService
{
    SubSonic.StoredProcedure sp;
    public int SaveConferenceData(ConferenceTypeMasterModel ConfModel)
    {
        try {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "CONF_SAVE_CONFERENCE_DETAILS");
            sp.Command.AddParameter("@CONF_CODE", ConfModel.CONF_CODE, DbType.String);
            sp.Command.AddParameter("@CONF_NAME", ConfModel.CONF_NAME, DbType.String);
            sp.Command.AddParameter("@CONF_STA_ID", ConfModel.CONF_STA_ID, DbType.String);
            sp.Command.AddParameter("@CONF_REMARKS", ConfModel.CONF_REMARKS, DbType.String);
            sp.Command.AddParameter("@CONF_CRTBY", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            int flag = (int)sp.ExecuteScalar();
            return flag;
            //SqlParameter[] param = new SqlParameter[6];
            //param[0] = new SqlParameter("@CONF_CODE", SqlDbType.NVarChar);
            //param[0].Value = ConfModel.CONF_CODE;
            //param[1] = new SqlParameter("@CONF_NAME", SqlDbType.NVarChar);
            //param[1].Value = ConfModel.CONF_NAME;
            //param[2] = new SqlParameter("@CONF_STA_ID", SqlDbType.NVarChar);
            //param[2].Value = ConfModel.CONF_STA_ID;
            //param[3] = new SqlParameter("@CONF_REMARKS", SqlDbType.NVarChar);
            //param[3].Value = ConfModel.CONF_REMARKS;
            //param[4] = new SqlParameter("@CONF_CRTBY", SqlDbType.NVarChar);
            //param[4].Value = HttpContext.Current.Session["UID"].ToString();
            //param[5] = new SqlParameter("@COMPANY", SqlDbType.Int);
            //param[5].Value = HttpContext.Current.Session["COMPANYID"]; 
            //using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "CONF_SAVE_CONFERENCE_DETAILS", param))
            //{
            //    if (reader.Read())
            //    {
            //        if ((int)reader["FLAG"] == 0)
            //            return new { Message = MessagesVM.Facility_Success };
            //        else
            //            return new { Message = MessagesVM.Facility_InsertError };
            //    }
            //    return new { Message = MessagesVM.Facility_InsertError };
            //}
        }
        catch
        {
            throw;
        }
    }

    public object ModifyConferenceData(ConferenceTypeMasterModel ConfUpdt)
    {
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@CONF_CODE",SqlDbType.NVarChar);
        param[0].Value = ConfUpdt.CONF_CODE;
        param[1] = new SqlParameter("@CONF_NAME",SqlDbType.NVarChar);
        param[1].Value = ConfUpdt.CONF_NAME;
        param[2] = new SqlParameter("@CONF_STA_ID", SqlDbType.NVarChar);
        param[2].Value = ConfUpdt.CONF_STA_ID;
        param[3] = new SqlParameter("@CONF_REMARKS", SqlDbType.NVarChar);
        param[3].Value = ConfUpdt.CONF_REMARKS;
        param[4] = new SqlParameter("@CONF_UPDATED_BY", SqlDbType.NVarChar);
        param[4].Value = HttpContext.Current.Session["UID"].ToString();
        param[5] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[5].Value = HttpContext.Current.Session["COMPANYID"]; 
        SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "CONF_MODIFY_CONFERENCE_DETAILS", param);
        return new { Message = MessagesVM.Facility_Modify, data = (object)null };
    }

    public IEnumerable<ConferenceTypeMasterModel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "CONF_BIND_GRID_DATA").GetReader())
        {
            try
            {
                List<ConferenceTypeMasterModel> Conflist = new List<ConferenceTypeMasterModel>();
                while (reader.Read())
                {
                    Conflist.Add(new ConferenceTypeMasterModel()
                    {
                        CONF_CODE = reader.GetValue(0).ToString(),
                        CONF_NAME = reader.GetValue(1).ToString(),
                        CONF_STA_ID = reader.GetValue(2).ToString(),
                        CONF_REMARKS = reader.GetValue(3).ToString()
                    });
                }
                reader.Close();
                return Conflist;
            }
            catch
            {
                throw;
            }
        }

    }
}