﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using UtiltiyVM;

public class ConferenceAssetMasterService
{
    public object SaveAssetDetails(ConferenceAssetMasterModel Ast)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@AST_CODE", SqlDbType.NVarChar);
            param[0].Value = Ast.AST_CODE;
            param[1] = new SqlParameter("@AST_NAME", SqlDbType.NVarChar);
            param[1].Value = Ast.AST_NAME;
            param[2] = new SqlParameter("@AST_STA_ID", SqlDbType.Int);
            param[2].Value = Ast.AST_STA_ID;
            param[3] = new SqlParameter("@AST_REMARKS", SqlDbType.NVarChar);
            param[3].Value = Ast.AST_REMARKS;
            param[4] = new SqlParameter("@AST_CRTBY", SqlDbType.NVarChar);
            param[4].Value = HttpContext.Current.Session["UID"].ToString();
            param[5] = new SqlParameter("@COMPANY", SqlDbType.Int);
            param[5].Value = HttpContext.Current.Session["COMPANYID"];
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AST_SAVE_ASSET_DETAILS", param))
            {
                if (reader.Read())
                {
                    if ((int)reader["FLAG"] == 0)
                        return new { Message = MessagesVM.Facility_Success,data=Ast.AST_CODE };
                    else
                        return new { Message = MessagesVM.Facility_InsertError, data = (object)null };
                }
                return new { Message = MessagesVM.Facility_InsertError, data = (object)null };
            }
        }
        catch
        {
            throw;
        }
    }

    public object ModifyAssetDetails(ConferenceAssetMasterModel Ast)
    {
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@AST_CODE", SqlDbType.NVarChar);
        param[0].Value = Ast.AST_CODE;
        param[1] = new SqlParameter("@AST_NAME", SqlDbType.NVarChar);
        param[1].Value = Ast.AST_NAME;
        param[2] = new SqlParameter("@AST_STA_ID", SqlDbType.NVarChar);
        param[2].Value = Ast.AST_STA_ID;
        param[3] = new SqlParameter("@AST_REMARKS", SqlDbType.NVarChar);
        param[3].Value = Ast.AST_REMARKS;
        param[4] = new SqlParameter("@AST_UPDATED_BY", SqlDbType.NVarChar);
        param[4].Value = HttpContext.Current.Session["UID"].ToString();
        param[5] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[5].Value = HttpContext.Current.Session["COMPANYID"];
        SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AST_MODIFY_ASSET_DETAILS", param);
        return new { Message = MessagesVM.Facility_Modify, data = (object)null };
    }

    public IEnumerable<ConferenceAssetMasterModel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AST_BIND_GRID_DATA").GetReader())
        {
            try
            {
                List<ConferenceAssetMasterModel> astlist = new List<ConferenceAssetMasterModel>();
                while (reader.Read())
                {
                    astlist.Add(new ConferenceAssetMasterModel()
                    {
                        AST_CODE = reader.GetValue(0).ToString(),
                        AST_NAME = reader.GetValue(1).ToString(),
                        AST_STA_ID = reader.GetValue(2).ToString(),
                        AST_REMARKS = reader.GetValue(3).ToString()
                    });
                }
                reader.Close();
                return astlist;
            }
            catch
            {
                throw;
            }
        }

    }
}