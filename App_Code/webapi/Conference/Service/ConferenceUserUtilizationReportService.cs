﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class ConferenceUserUtilizationReportService
{
    SubSonic.StoredProcedure sp;
    List<ConferenceUserUtilizationReport> ConfUserUtlList;

    DataSet ds;
    public object GetConferenceUserUtilizationObject(ConfUserUtilParams CNPDet)
    {
        try
        {
            ConfUserUtlList = GetConferenceUserUtilizationList(CNPDet);

            if (ConfUserUtlList.Count != 0)

                return new { Message = MessagesVM.UM_NO_REC, data = ConfUserUtlList };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public List<ConferenceUserUtilizationReport> GetConferenceUserUtilizationList(ConfUserUtilParams confUtlParams)
    {
        try
        {
            List<ConferenceUserUtilizationReport> ConfUserUtlList = new List<ConferenceUserUtilizationReport>();
            ConferenceUserUtilizationReport confUserUtlRpt;
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SPC_AREA_REPORT");
            //sp.Command.AddParameter("@AUR_USER", HttpContext.Current.Session["UID"], DbType.String);

            //if (confUtlParams.CNP_NAME == null)
            //    sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            //else
            //    sp.Command.AddParameter("@COMPANYID", confUtlParams.CNP_NAME, DbType.Int32);


            /////////
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            if (confUtlParams.flrlst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(confUtlParams.flrlst);
            }
            param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
            param[1].Value = confUtlParams.CNF_FROM_DATE;
            param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
            param[2].Value = confUtlParams.CNF_TO_DATE;
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            if (confUtlParams.CNP_NAME == null)
                param[3].Value = HttpContext.Current.Session["COMPANYID"];
            else
                param[3].Value = confUtlParams.CNP_NAME;
            param[4] = new SqlParameter("@AUR_USER", SqlDbType.VarChar);
            param[4].Value = HttpContext.Current.Session["UID"].ToString();
            param[5] = new SqlParameter("@SEARCHVAL", SqlDbType.VarChar);
            param[5].Value = confUtlParams.SearchValue;
            param[6] = new SqlParameter("@PAGENUM", SqlDbType.VarChar);
            param[6].Value = confUtlParams.PageNumber;
            param[7] = new SqlParameter("@PAGESIZE", SqlDbType.VarChar);
            param[7].Value = confUtlParams.PageSize;
            ////////
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CONFERENCE_USER_UTILIZATION_REPORT", param))


            //using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    confUserUtlRpt = new ConferenceUserUtilizationReport();
                    confUserUtlRpt.CNY_NAME = sdr["CNY_NAME"].ToString();
                    confUserUtlRpt.CTY_NAME = sdr["CTY_NAME"].ToString();
                    confUserUtlRpt.LCM_NAME = sdr["LCM_NAME"].ToString();
                    confUserUtlRpt.TWR_NAME = sdr["TWR_NAME"].ToString();
                    confUserUtlRpt.FLR_NAME = sdr["FLR_NAME"].ToString();
                    //confUserUtlRpt.FLR_AREA = (double)sdr["CONFERENCE_ROOM_NAME"];
                    confUserUtlRpt.CONFERENCE_ROOM_NAME = sdr["CONFERENCE_ROOM_NAME"].ToString();
                    confUserUtlRpt.TOTAL_HOURS = sdr["TOTAL_HOURS"].ToString();
                    confUserUtlRpt.REQUESTED_BY = sdr["REQUESTED_BY"].ToString();
                    confUserUtlRpt.OVERALL_COUNT = sdr["OVERALL_COUNT"].ToString();
                    ConfUserUtlList.Add(confUserUtlRpt);
                }
                sdr.Close();
            }
            if (ConfUserUtlList.Count != 0)
                //return new { Message = MessagesVM.HDM_UM_OK, data = ConfUserUtlList };
                return ConfUserUtlList;
            else
                return new List<ConferenceUserUtilizationReport>();
        }
        catch
        {
            throw;
        }

    }


    public object GetConferenceUtilizationChart(ConfUserUtilParams confUtlParams)
    {
        try
        {
            //List<SpaceData> area = new List<SpaceData>();
            //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CONF_UTILIZATION_GRAPH");
            ////sp.Command.AddParameter("@FROM_DATE", Spcdetails.FromDate, DbType.DateTime);
            ////sp.Command.AddParameter("@TO_DATE", Spcdetails.ToDate, DbType.DateTime);
            ////sp.Command.AddParameter("@REQTYP", Spcdetails.Request_Type, DbType.String);
            //sp.Command.AddParameter("@AUR_USER", HttpContext.Current.Session["Uid"].ToString(), DbType.String);
            //if (ID == null)
            //    sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            //else
            //    sp.Command.AddParameter("@COMPANYID", ID, DbType.Int32);



            List<ConferenceUserUtilizationReport> ConfUserUtlList = new List<ConferenceUserUtilizationReport>();
            //ConferenceUserUtilizationReport confUserUtlRpt;

            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            if (confUtlParams.flrlst == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(confUtlParams.flrlst);
            }
            param[1] = new SqlParameter("@FDATE", SqlDbType.DateTime);
            param[1].Value = confUtlParams.CNF_FROM_DATE;
            param[2] = new SqlParameter("@TDATE", SqlDbType.DateTime);
            param[2].Value = confUtlParams.CNF_TO_DATE;
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            if (confUtlParams.CNP_NAME == null)
                param[3].Value = HttpContext.Current.Session["COMPANYID"];
            else
                param[3].Value = confUtlParams.CNP_NAME;
            param[4] = new SqlParameter("@AUR_USER", SqlDbType.VarChar);
            param[4].Value = HttpContext.Current.Session["UID"].ToString();
            ////////
            //using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CONFERENCE_UTILIZATION_REPORT", param))



            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_CONF_UTILIZATION_GRAPH", param);
            object[] arr = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray.Reverse()).ToArray();
            return arr;
        }

        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }
}