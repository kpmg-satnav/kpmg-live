﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class ConferenceCustomizableReportService
{
    SubSonic.StoredProcedure sp;
    List<ConferenceCustomizableReport> ConfCustomList;
    ConferenceCustomizableReport Custm;
    DataSet ds;
    public object GetConferenceUtilizationObject(ConfCustomizedParams CNPDet)
    {
        try
        {
            ConfCustomList = GetConferenceUtilizationList(CNPDet);

            if (ConfCustomList.Count != 0)

                return new { Message = MessagesVM.UM_NO_REC, data = ConfCustomList };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, Info = ex.Message, data = (object)null };
        }
    }

    public List<ConferenceCustomizableReport> GetConferenceUtilizationList(ConfCustomizedParams Details)
    {
        try
        {
            List<ConferenceCustomizableReport> CData = new List<ConferenceCustomizableReport>();
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@REQTYPE", SqlDbType.NVarChar);
            param[0].Value = Details.Request_Type;
            param[1] = new SqlParameter("@FLRLST", SqlDbType.Structured);


            if (Details.flrlst == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(Details.flrlst);
            }


            param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];

            param[3] = new SqlParameter("@STAT", SqlDbType.Int);
            param[3].Value = Details.STAT;

            param[4] = new SqlParameter("@CONF_FROMDATE", SqlDbType.DateTime);
            param[4].Value = Details.FromDate;

            param[5] = new SqlParameter("@CONF_TODATE", SqlDbType.DateTime);
            param[5].Value = Details.ToDate;

            param[6] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[6].Value = Details.CNP_NAME;
            param[7] = new SqlParameter("@SEARCHVAL", SqlDbType.NVarChar);
            param[7].Value = Details.SearchValue;
            param[8] = new SqlParameter("@PAGENUM", SqlDbType.NVarChar);
            param[8].Value = Details.PageNumber;
            param[9] = new SqlParameter("@PAGESIZE", SqlDbType.NVarChar);
            param[9].Value = Details.PageSize;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "RESERVATION_CUSTOMIZED_REPORT", param))
            {
                while (reader.Read())
                {
                    Custm = new ConferenceCustomizableReport();
                    Custm.Request_Id = Convert.ToString(reader["SSA_SRNREQ_ID"]);
                    Custm.COUNTRY = Convert.ToString(reader["COUNTRY"]);
                    Custm.CITY = Convert.ToString(reader["CITY"]);
                    Custm.LOCATION = Convert.ToString(reader["LOCATION"]);
                    Custm.TOWER = Convert.ToString(reader["TOWER"]);
                    Custm.FLOOR = Convert.ToString(reader["FLOOR"]);
                    Custm.RoomName = Convert.ToString(reader["ROOM_NAME"]);
                    Custm.DEP_NAME = Convert.ToString(reader["DEP_NAME"]);
                    Custm.EMP_NAME = Convert.ToString(reader["EMP_NAME"]);
                    Custm.EMP_ID = Convert.ToString(reader["EMP_ID"]);
                    Custm.From_Date = reader["FROM_DATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["FROM_DATE"]); 
                    Custm.To_Date = reader["TODATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["TODATE"]);
                    Custm.From_Time = Convert.ToString(reader["SSA_FROM_TIME"]);
                    Custm.To_Time = Convert.ToString(reader["SSA_TO_TIME"]);
                    Custm.TotalHours = Convert.ToInt32(reader["TOT_HRS"]);
                    Custm.Status = Convert.ToString(reader["Status"]);
                    Custm.Description = Convert.ToString(reader["SSA_DESC"]);
                    Custm.OVERALL_COUNT = Convert.ToString(reader["OVERALL_COUNT"]);
                    CData.Add(Custm);
                }
                reader.Close();
            }
            return CData;
        }
        catch
        {
            throw;
        }
    }
}