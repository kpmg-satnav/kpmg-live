﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class MaintenanceCustomizedService
{
    List<MainCustReport> Cust;
    SubSonic.StoredProcedure sp;
    MainCustReport Mnt;
    DataSet ds;

    public object GetCustomizedObject(MaintenanceCustomizedModel Det)
    {
        try
        {
            Cust = GetMntReportDetails(Det);
            if (Cust.Count != 0) { return new { Message = MessagesVM.SER_OK, data = Cust }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }


    public List<MainCustReport> GetMntReportDetails(MaintenanceCustomizedModel Details)
    {
        try
        {
            DataSet ds;
            List<MainCustReport> Mntdata = new List<MainCustReport>();
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "MAINTENANCE_CUSTOMIZED_REPORT");
            sp.Command.AddParameter("@FRMDT", Details.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@TODT", Details.ToDate, DbType.DateTime);
            sp.Command.AddParameter("@STATUS", Details.Request_Type, DbType.String);
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.Command.AddParameter("@SEARCHVAL", Details.SearchValue, DbType.String);
            sp.Command.AddParameter("@PAGENUM", Details.PageNumber, DbType.String);
            sp.Command.AddParameter("@PAGESIZE", Details.PageSize, DbType.String);
            if (Details.CNP_NAME== null)
            {
                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
            }
            else 
            {
                sp.Command.AddParameter("@COMPANYID", Details.CNP_NAME, DbType.String);
            }
            using (IDataReader reader = sp.GetReader())
            {
               // ds = sp.GetDataSet();
                while (reader.Read())
                {
                    DateTime? dt = null;
                    Mnt = new MainCustReport();
                    Mnt.AAG_MFTYPE = reader["AAG_NAME"].ToString();
                    Mnt.AGT_NAME = reader["AGT_NAME"].ToString();
                    Mnt.AAB_NAME = reader["AAB_NAME"].ToString();
                    Mnt.LCM_NAME = reader["LCM_NAME"].ToString();
                    Mnt.CTY_NAME = reader["CTY_NAME"].ToString();
                    Mnt.CNY_NAME = reader["CNY_NAME"].ToString();
                    Mnt.ASSET_CODE = reader["ASSET_CODE"].ToString();
                    Mnt.AAP_WRNT_DATE = reader["AAP_WRNT_DATE"].ToString() == "" ? dt : Convert.ToDateTime(reader["AAP_WRNT_DATE"]);
                    Mnt.AMN_TO_DATE = reader["AMN_TO_DATE"].ToString() == ""  ? dt : Convert.ToDateTime(reader["AMN_TO_DATE"]);
                    Mnt.AMN_FROM_DATE = reader["AMN_FROM_DATE"].ToString() == "" ? dt : Convert.ToDateTime(reader["AMN_FROM_DATE"]);
                    Mnt.AVR_NAME = reader["AVR_NAME"].ToString();
                    Mnt.AMC_STATUS = reader["AMC_STATUS"].ToString();
                    Mnt.PVM_STATUS = reader["PVM_STATUS"].ToString();
                    Mnt.BRK_STATUS = reader["BRK_STATUS"].ToString();
                    Mnt.PLAN_FREQUENCY = reader["PLAN_FREQUENCY"].ToString();                   
                    Mnt.PVD_PLANLABOUR_COST = Convert.ToDouble(reader["PVD_PLANLABOUR_COST"].ToString());
                    Mnt.PVD_PLANSPARES_COST = Convert.ToDouble(reader["PVD_PLANSPARES_COST"].ToString());
                    Mnt.CNP_NAME = reader["CNP_NAME"].ToString();
                    Mnt.PVD_PLANSCHD_DT = reader["PVD_PLANSCHD_DT"].ToString() == "" ? dt : Convert.ToDateTime(reader["PVD_PLANSCHD_DT"]);
                    Mnt.PVD_PLAN_REMARKS = reader["PVD_PLAN_REMARKS"].ToString();
                    Mnt.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    Mntdata.Add(Mnt);
                }
                reader.Close();
            }
            return Mntdata;
        }
        catch
        {
            throw;
        }

    }
}