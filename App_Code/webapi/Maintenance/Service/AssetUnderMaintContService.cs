﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class AssetUnderMaintContService
{
    SubSonic.StoredProcedure sp;
    List<AssetUnderMaint> MaintList;
    AssetUnderMaint Maint;
    DataSet ds;
    public object GetMaintDetails(MaintenanceData AMaint)
    {
        try
        {
            MaintList = MaintenanceDetails(AMaint);
            if (MaintList.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = MaintList };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }
    public List<AssetUnderMaint> MaintenanceDetails(MaintenanceData AMaint)
    {
        try
        {
            List<AssetUnderMaint> MaintData = new List<AssetUnderMaint>();

            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["UID"];

            if (AMaint.CNP_NAME == null)
            {
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[1].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else {
                param[1] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[1].Value = AMaint.CNP_NAME;
            }
            param[2] = new SqlParameter("@SEARCHVAL", SqlDbType.NVarChar);
            param[2].Value = AMaint.SearchValue;
            param[3] = new SqlParameter("@PAGENUM", SqlDbType.NVarChar);
            param[3].Value = AMaint.PageNumber;
            param[4] = new SqlParameter("@PAGESIZE", SqlDbType.NVarChar);
            param[4].Value = AMaint.PageSize;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "MN_AMC_RPT_VIEW_ASSETS", param))
            {
                while (reader.Read())
                {
                    DateTime? dt = null;
                    Maint = new AssetUnderMaint();
                    Maint.PLANID = reader["PLANID"].ToString();
                    Maint.AST_SUBCAT_NAME = reader["AST_SUBCAT_NAME"].ToString();
                    Maint.AST_MD_NAME = reader["AST_MD_NAME"].ToString();
                    Maint.LCM_NAME = reader["LCM_NAME"].ToString();
                    Maint.AVR_NAME = reader["AVR_NAME"].ToString();
                    Maint.AMN_FROM_DATE = reader["AMN_FROM_DATE"] is DBNull ? dt : Convert.ToDateTime(reader["AMN_FROM_DATE"]);
                    Maint.AMN_TO_DATE = reader["AMN_TO_DATE"] is DBNull ? dt : Convert.ToDateTime(reader["AMN_TO_DATE"]);
                    Maint.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    MaintData.Add(Maint);
                }
                reader.Close();
            }
            return MaintData;
        }
        catch
        {
           throw;
        }
    }


    public List<AssetUnderMaint> GetDetailsOnSelection(MaintenanceData AMaint)
    {
        try
        {
            List<AssetUnderMaint> MaintData = new List<AssetUnderMaint>();

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@PLAN_ID", SqlDbType.NVarChar);
            param[0].Value = AMaint.PLAN_ID;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_ASSETLIST", param))
            {
                while (reader.Read())
                {
                    Maint = new AssetUnderMaint();
                    Maint.AMN_PLAN_ID = reader["AMN_PLAN_ID"].ToString();
                    Maint.AAT_NAME = reader["AAT_NAME"].ToString();
                    MaintData.Add(Maint);
                }
                reader.Close();
            }
            return MaintData;
        }
        catch
        { throw; }
    }
}