﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;



public class PMStatusDetailsService
{
    SubSonic.StoredProcedure sp;
    List<PMStatusList> pmslst;
    PMStatusList pmsobj;
    DataSet ds;
    public object GetMaintDetails(PMStatus obj)
    {
        try
        {
            pmslst = BindDataToGridList(obj);
            if (pmslst.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = pmslst };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }
    public List<PMStatusList> BindDataToGridList(PMStatus obj)
    {
        try
        {
            List<PMStatusList> pmslistdt = new List<PMStatusList>();

            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@USR_ID", SqlDbType.NVarChar);
            param[0].Value = HttpContext.Current.Session["UID"];
            
            param[1] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[1].Value = obj.CNP_NAME;
          
            param[2] = new SqlParameter("@FRMDT", SqlDbType.NVarChar );
            param[2].Value = obj.FromDate;

            param[3] = new SqlParameter("@TODT", SqlDbType.NVarChar);
            param[3].Value = obj.ToDate;


            param[4] = new SqlParameter("@LCM_LST", SqlDbType.Structured);
            if (obj.selectedLoc == null)
            {
                param[4].Value = null;
            }
            else
            {
                param[4].Value = UtilityService.ConvertToDataTable(obj.selectedLoc);
            }
            param[5] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[5].Value = HttpContext.Current.Session["Uid"].ToString();

            param[6] = new SqlParameter("@VT_TYPE", SqlDbType.Structured);
            if (obj.selectedgroup == null)
            {
                param[6].Value = null;
            }
            else
            {
                param[6].Value = UtilityService.ConvertToDataTable(obj.selectedgroup);
            }
            param[7] = new SqlParameter("@SEARCHVAL", SqlDbType.NVarChar);
            param[7].Value = obj.SearchValue;
            param[8] = new SqlParameter("@PAGENUM", SqlDbType.NVarChar);
            param[8].Value = obj.PageNumber;
            param[9] = new SqlParameter("@PAGESIZE", SqlDbType.NVarChar);
            param[9].Value = obj.PageSize;





            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "BIND_GROLE_PVM_STATUS_DETAILS_ALL", param))
            {
                while (reader.Read())
                {
                    pmsobj = new PMStatusList();
                    pmsobj.AssetGroup = reader["AssetGroup"].ToString();
                    pmsobj.AssetGroupType = reader["Category"].ToString();
                    pmsobj.AssetBrand = reader["Brand"].ToString();
                    pmsobj.AssetName = reader["PVM_ASSET_NAME"].ToString();
                    pmsobj.Vendor = reader["Vendor"].ToString();
                    pmsobj.Location = reader["Location"].ToString();
                    pmsobj.ScheduledDate = reader["PVD_PLANSCHD_DT"].ToString();
                    pmsobj.PlanType = reader["PVM_PLAN_FREQ"].ToString();
                    pmsobj.Status = reader["STA_TITLE"].ToString();
                    pmsobj.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    pmslistdt.Add(pmsobj);
                }
                reader.Close();
            }
            return pmslistdt;
        }
        catch
        {
            throw;
        }
    }
}