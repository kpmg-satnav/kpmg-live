﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class PMCostService
{
    List<PMCostModel> Costlist;
    SubSonic.StoredProcedure sp;
    PMCostModel Cost;
    DataSet ds;

    public object GetCostObject(PMCostVM Det)
    {
        try
        {
            Costlist = GetCostDetails(Det);
            if (Costlist.Count != 0) { return new { Message = MessagesVM.SER_OK, data = Costlist }; }
            else { return new { Message = MessagesVM.SER_OK, data = (object)null }; }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<PMCostModel> GetCostDetails(PMCostVM Details)
    {
        try
        {
            DataSet ds;
            List<PMCostModel> Mntdata = new List<PMCostModel>();
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@BDGID", SqlDbType.Structured);
            if (Details.selectedLoc == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(Details.selectedLoc);
            }
            param[1] = new SqlParameter("@FRMDT",SqlDbType.DateTime);
            param[1].Value = Details.FromDate;
            param[2] = new SqlParameter("@TODT", SqlDbType.DateTime);
            param[2].Value = Details.ToDate;
            param[3] = new SqlParameter("@USR_ID", SqlDbType.NVarChar);
            param[3].Value = HttpContext.Current.Session["UID"].ToString();
            if (Details.CNP_NAME == null)
            {
                param[4] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[4].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[4] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[4].Value = Details.CNP_NAME;
            }
            param[5] = new SqlParameter("@SEARCHVAL", SqlDbType.NVarChar);
            param[5].Value = Details.SearchValue;
            param[6] = new SqlParameter("@PAGENUM", SqlDbType.NVarChar);
            param[6].Value = Details.PageNumber;
            param[7] = new SqlParameter("@PAGESIZE", SqlDbType.NVarChar);
            param[7].Value = Details.PageSize;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PVM_COSTDTLS_BIND_CTYPE", param))
            {
                while (reader.Read())
                {
                    DateTime? dt = null;
                    Cost = new PMCostModel();
                    Cost.PVM_PLAN_TYPE = reader["PVM_PLAN_TYPE"].ToString();
                    Cost.PVM_ASSET_NAME = reader["PVM_ASSET_NAME"].ToString();
                    Cost.PVD_PLANEXEC_DT = reader["PVD_PLANEXEC_DT"].ToString() == "" ? dt : Convert.ToDateTime(reader["PVD_PLANEXEC_DT"]);
                    Cost.PVD_PLANSPARES_COST = reader["PVD_PLANSPARES_COST"].ToString();
                    Cost.PVD_PLANLABOUR_COST = reader["PVD_PLANLABOUR_COST"].ToString();
                    Cost.STA_TITLE = reader["STA_TITLE"].ToString();
                    Cost.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    Mntdata.Add(Cost);
                }
                reader.Close();
            }
            return Mntdata;
        }
        catch
        {
            throw;
        }
    }
}