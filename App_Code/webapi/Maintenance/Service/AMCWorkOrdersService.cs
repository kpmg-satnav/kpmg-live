﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class AMCWorkOrdersService
{
    SubSonic.StoredProcedure sp;
    List<AMCWorkOrderVMGridList> gridList;
    AMCWorkOrderVMGridList gridlstObj;
    DataSet ds;
    public object BindDataToGrid(AMCWorkOrderVM VM)
    {
        try
        {
            gridList = BindDataToGridList(VM);
            if (gridList.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = gridList };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }
        }
        catch (Exception ex) { return new { Message = ex.Message, Info = ex.Message, data = (object)null }; }
    }
    public List<AMCWorkOrderVMGridList> BindDataToGridList(AMCWorkOrderVM VM) 
    {
        try {
            List<AMCWorkOrderVMGridList> WData = new List<AMCWorkOrderVMGridList>();
            SqlParameter[] param = new SqlParameter[5];

            //param[0] = new SqlParameter("@LCM_LST", SqlDbType.Structured);
            //if (VM.selectedLoc == null)
            //{
            //    param[0].Value = null;
            //}
            //else
            //{
            //    param[0].Value = UtilityService.ConvertToDataTable(VM.selectedLoc);
            //}
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[0].Value = HttpContext.Current.Session["Uid"].ToString();

            param[1] = new SqlParameter("@COMPANY_ID", SqlDbType.VarChar);
            param[1].Value = VM.CNP_NAME;
            param[2] = new SqlParameter("@SEARCHVAL", SqlDbType.VarChar);
            param[2].Value = VM.SearchValue;
            param[3] = new SqlParameter("@PAGENUM", SqlDbType.VarChar);
            param[3].Value = VM.PageNumber;
            param[4] = new SqlParameter("@PAGESIZE", SqlDbType.VarChar);
            param[4].Value = VM.PageSize;

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "MN_AMC_RPT_WO_MAINADMIN", param))
            {
                while (reader.Read())
                {
                    gridlstObj = new AMCWorkOrderVMGridList();
                    gridlstObj.PLANID = reader["AMN_PLAN_ID"].ToString();
                    gridlstObj.PLANFOR = reader["AMN_PLAN_FOR"].ToString();
                    gridlstObj.LOCATION = reader["LCM_NAME"].ToString();
                    gridlstObj.VENDOR = reader["AVR_NAME"].ToString();
                    gridlstObj.COST = reader["AWO_AMC_COST"].ToString();
                    gridlstObj.FROM_DATE = reader["AMN_FROM_DATE"].ToString();
                    gridlstObj.TO_DATE = reader["AMN_TO_DATE"].ToString();
                    gridlstObj.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    WData.Add(gridlstObj);
                }
                reader.Close();
            }
            return WData;
        }
        catch { throw; }
    }
}