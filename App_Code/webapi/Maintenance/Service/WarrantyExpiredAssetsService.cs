﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;

public class WarrantyExpiredAssetsService
{
    SubSonic.StoredProcedure sp;
    List<WarrantyModels> Wrntylst;
    WarrantyModels Wrnty;
    DataSet ds;
    public object GetWarrantyObject(WarrantyExpiredModel VM)
    {
        try
        {
            Wrntylst = GetWarrantyListDetails(VM);
            if (Wrntylst.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = Wrntylst };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }
        }
        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }
    public List<WarrantyModels> GetWarrantyListDetails(WarrantyExpiredModel VM) 
    {
        try {
            List<WarrantyModels> WData = new List<WarrantyModels>();
            SqlParameter[] param = new SqlParameter[10];

            param[0] = new SqlParameter("@LCM_CODE", SqlDbType.Structured);
            if (VM.selectedLoc == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = UtilityService.ConvertToDataTable(VM.selectedLoc);
            }
            param[1] = new SqlParameter("@VT_CODE", SqlDbType.Structured);
            if (VM.selectedGrp == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = UtilityService.ConvertToDataTable(VM.selectedGrp);
            }
            param[2] = new SqlParameter("@AST_SUBCAT_CODE", SqlDbType.Structured);
            if (VM.selectedGrpType == null)
            {
                param[2].Value = null;
            }
            else
            {
                param[2].Value = UtilityService.ConvertToDataTable(VM.selectedGrpType);
            }
            param[3] = new SqlParameter("@MANUFACTUER_CODE", SqlDbType.Structured);
            if (VM.selectedBrand == null)
            {
                param[3].Value = null;
            }
            else
            {
                param[3].Value = UtilityService.ConvertToDataTable(VM.selectedBrand);
            }
            param[4] = new SqlParameter("@AST_MD_CODE", SqlDbType.Structured);
            if (VM.selectedModel == null)
            {
                param[4].Value = null;
            }
            else
            {
                param[4].Value = UtilityService.ConvertToDataTable(VM.selectedModel);
            }
            param[5] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[5].Value = HttpContext.Current.Session["Uid"].ToString();

            param[6] = new SqlParameter("@COMPANY_ID", SqlDbType.VarChar);
            param[6].Value = VM.CNP_NAME;
            param[7] = new SqlParameter("@SEARCHVAL", SqlDbType.VarChar);
            param[7].Value = VM.SearchValue;
            param[8] = new SqlParameter("@PAGENUM", SqlDbType.VarChar);
            param[8].Value = VM.PageNumber;
            param[9] = new SqlParameter("@PAGESIZE", SqlDbType.VarChar);
            param[9].Value = VM.PageSize;
            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "MN_GET_ASSETNOT_MAINT_REPORT", param))
            {
                while (reader.Read())
                {
                    Wrnty = new WarrantyModels();
                    Wrnty.AAT_NAME = reader["AAT_NAME"].ToString();
                    Wrnty.AVR_NAME = reader["AVR_NAME"].ToString();
                    Wrnty.LCM_NAME = reader["LCM_NAME"].ToString();
                    Wrnty.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    WData.Add(Wrnty);
                }
                reader.Close();
            }
            return WData;
        }
        catch { throw; }
    }
}