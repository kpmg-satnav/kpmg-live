﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

public class WarrantyExpiredModel
{
    public List<Locationlst> selectedLoc { get; set; }
    public List<Types> selectedGrp {get; set;}
    public List<SubTypes> selectedGrpType {get; set;}
    public List<GetBrands> selectedBrand {get; set;}
    public List<GetModel> selectedModel { get; set; }
    public string CNP_NAME {get;set;}
    public string Type { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}

public class WarrantyModels
{
    public string AAT_NAME { get; set; }
    public string AVR_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string OVERALL_COUNT { get; set; }
}