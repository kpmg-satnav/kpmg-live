﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;



public class PMStatus
{
    public List<Locationlst> selectedLoc { get; set; }
    public List<Types> selectedgroup { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }
    public string CNP_NAME { get; set; }
    public string Type { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}

public class PMStatusList
{
    public string AssetGroup { get; set; }
    public string AssetGroupType { get; set; }
    public string AssetBrand { get; set; }
    public string AssetName { get; set; }
    public string Vendor { get; set; }
    public string PlanType { get; set; }
    public string ScheduledDate { get; set; }    
    public string Status { get; set; }
    public string Location { get; set; }
    public string OVERALL_COUNT { get; set; }
}