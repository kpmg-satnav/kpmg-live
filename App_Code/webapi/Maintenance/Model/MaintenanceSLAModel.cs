﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MaintenanceSLAModel
/// </summary>
public class MaintenanceSLAModel
{
    public MaintSLA SLA { get; set; }
    public JArray SLADET { get; set; }
    public List<Locationlst> loclst { get; set; }
  }



public class MaintSLA
{
    public int SLA_ID { get; set; }
    public string SLA_CNY_CODE { get; set; }
    public string SLA_CTY_CODE { get; set; }
    public string SLA_LOC_CODE { get; set; }   
    public string SLA_STA_ID { get; set; }
    public string CNY_NAME { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
   
}

public class MSLADetailsModel
{
    public int SLAD_SLA_ID { get; set; }
    public int SLAD_HDM_STATUS { get; set; }
    public int SLAD_HDM_ROL_ID { get; set; }
    public int SLAD_ESC_TIME { get; set; }
    public int SLAD_ESC_TIME_TYPE { get; set; }
    public Boolean SLAD_EMAIL_ESC { get; set; }
}