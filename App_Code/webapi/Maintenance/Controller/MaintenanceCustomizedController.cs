﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class MaintenanceCustomizedController : ApiController
{
    MaintenanceCustomizedService Msvc = new MaintenanceCustomizedService();
    MaintReportView view = new MaintReportView();


    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(MaintenanceCustomizedModel Custdata)
    {
        var obj = Msvc.GetCustomizedObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid(MaintenanceCustomizedModel data)
    {
        ReportGenerator<MainCustReport> reportgen = new ReportGenerator<MainCustReport>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/MaintenanceCustomizableReport.rdlc"),
            DataSetName = "CustomizedReport",
            ReportType = "Maintenance Customizable Report"
        };

        Msvc = new MaintenanceCustomizedService();
        List<MainCustReport> reportdata = Msvc.GetMntReportDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/MaintenanceCustomizableReport." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "MaintenanceCustomizableReport." + data.Type;
        return result;
    }
}

