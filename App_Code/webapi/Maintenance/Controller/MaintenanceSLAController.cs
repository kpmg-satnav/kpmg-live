﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class MaintenanceSLAController : ApiController
{
    MaintenanceSLAService MaintenanceSLAService = new MaintenanceSLAService();


       //For Status/Role view
    [HttpGet]
    public object GetSLATime()
    {
        return MaintenanceSLAService.GetSLATime();
    }
    //Grid View
    [HttpGet]
    public HttpResponseMessage GetSLAGrid()
    {
        IEnumerable<MaintSLA> SLAlist = MaintenanceSLAService.GetSLAList();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, SLAlist);
        return response;
    }

    //To save records

    [HttpPost]
    public HttpResponseMessage SaveDetails(MaintenanceSLAModel dataobject)
    {

        if (MaintenanceSLAService.SaveDetails(dataobject).SLA_ID == 0)
        {
            return Request.CreateResponse(HttpStatusCode.OK, new  {msg = "SLA Already Exists With Same Details"});
       
        }
        else
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, new { msg = "Data Inserted Successfully" });
            return response;
        }
    }


    //edit
    [HttpGet]
    public object EditSLA(int id)
    {
        return MaintenanceSLAService.EditSLADetails(id);

    }

    //To Update records
    [HttpPost]
    public HttpResponseMessage UpdateDetails(MaintenanceSLAModel dataobject)
    {

        var obj = MaintenanceSLAService.UpdateDetails(dataobject);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
        //if (MaintenanceSLAService.UpdateDetails(dataobject).SLA_ID == 0)
        //{
        //    return Request.CreateResponse(HttpStatusCode.OK, new { msg = "SLA Already Exists With Same Details" });
        //}
        //else
        //{
        //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, new { msg = "Data Updated Successfully" });
        //    return response;
        //}
    }


}
