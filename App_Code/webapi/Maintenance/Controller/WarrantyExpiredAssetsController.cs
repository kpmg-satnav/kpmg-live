﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class WarrantyExpiredAssetsController : ApiController
{
    WarrantyExpiredAssetsService Wservice = new WarrantyExpiredAssetsService();
    
    [HttpPost]
    public HttpResponseMessage GetWarrantyDetails(WarrantyExpiredModel VM)
    {
        var obj = Wservice.GetWarrantyObject(VM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetGrid(WarrantyExpiredModel data)
    {
        ReportGenerator<WarrantyModels> reportgen = new ReportGenerator<WarrantyModels>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/WarrantyExpiredAssets.rdlc"),
            DataSetName = "WarrantyExpiredAssets",
            ReportType = "Warranty Expired Assets"
        };

        Wservice = new WarrantyExpiredAssetsService();
        List<WarrantyModels> reportdata = Wservice.GetWarrantyListDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/WarrantyExpiredAssets.rdlc." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "WarrantyExpiredAssets." + data.Type;
        return result;
    }
}
