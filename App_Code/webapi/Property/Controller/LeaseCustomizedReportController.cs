﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

public class LeaseCustomizedReportController : ApiController
{
    LeaseCustomizedReportService Csvc = new LeaseCustomizedReportService();
    LeaseCustomizedReportView report = new LeaseCustomizedReportView();

    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(LeaseCustomizedDetails Custdata)
    {
        var obj = Csvc.GetCustomizedObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage getProjects()
    {
        //return Csvc.getProjects();
        var obj = Csvc.getProjects();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetCustomizedData([FromBody]LeaseCustomizedDetails data)
    {
        try
        {
            ReportGenerator<LeaseCustomizedData> reportgen = new ReportGenerator<LeaseCustomizedData>()
            {
                ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/LeaseCustomizableReport.rdlc"),
                DataSetName = "LeaseCustomizableReport",
                ReportType = "Lease Customizable Report"
            };

            Csvc = new LeaseCustomizedReportService();
            List<LeaseCustomizedData> reportdata = Csvc.GetCustomizedDetails(data);
            string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/LeaseCustomizableReport." + data.Type);
            await reportgen.GenerateReport(reportdata, filePath, data.Type);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = "LeaseCustomizableReport." + data.Type;
            return result;
        } 
         catch (Exception)
        {            
            throw;
        }
    }

}
