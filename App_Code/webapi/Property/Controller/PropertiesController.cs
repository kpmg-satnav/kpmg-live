﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;


public class PropertiesController : ApiController
{
    PropertiesService PropService = new PropertiesService();

    [HttpPost]
    public HttpResponseMessage GetProperties(ReportType Type)
    {
        var obj = PropService.ViewProperties (Type);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetMarkers(ReportType Type)
    {
        var obj = PropService.GetMarkers(Type);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetAllProperties([FromBody] ReportType obj)
    {
        ReportGenerator<PropertiesModel> reportgen = new ReportGenerator<PropertiesModel>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/ViewProperties.rdlc"),
            DataSetName = "Properties",
            ReportType = "View Properties"
        };

        PropService = new PropertiesService();
        List<PropertiesModel> reportdata = PropService.GetProperties(obj);

        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/Properties." + obj.Type);
        await reportgen.GenerateReport(reportdata, filePath, obj.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "Properties." + obj.Type;
        return result;
    }
}
