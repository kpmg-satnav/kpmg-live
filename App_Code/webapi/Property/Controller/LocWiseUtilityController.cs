﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Collections.Generic;
using LocWiseUtilityVModel;
using System.Threading.Tasks;
using System.IO;
using System;
/// <summary>
/// Summary description for LocUtilityController
/// </summary>
public class LocUtilityController : ApiController
{
    LocWiseUtilityService LWUtility = new LocWiseUtilityService();

    [HttpGet]
    public HttpResponseMessage BindLocation()
    {
        IEnumerable<ExpLocationVM> LocUtilityList = LWUtility.BindLocation();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, LocUtilityList);
        return response;
    }
    [HttpPost]

    public async Task<HttpResponseMessage> ExcelDownload([FromBody] ExcelDownload spcdet)
    {
        HttpResponseMessage val = null;
        try
        {
            ReportGenerator<ExcelDownload> reportgen = new ReportGenerator<ExcelDownload>()
            {
                ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/UtilityExpenses.rdlc"),
                DataSetName = "UtilityDataset"

            };
            string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/UtilityExpenses.xlsx");
            List<ExcelDownload> reportdata = LWUtility.ExcelDownload(spcdet);
            await reportgen.GenerateReport(reportdata, filePath, "xlsx");
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = "UtilityExpenses.xlsx";
            return result;
        }
        catch (Exception Ex)
        {

            return val;
        }

    }

    [HttpGet]
    public HttpResponseMessage BindExpenseHead()
    {
        IEnumerable<ExpHeadUtilityModel> ExpenseUtility = LWUtility.BindExpenseHead();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ExpenseUtility);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage BindDepartment()
    {
        IEnumerable<DepVM> DepList = LWUtility.BindDepartment();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, DepList);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveData(LocWiseUtilityModel Save)
    {
        var obj = LWUtility.SaveData(Save);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage BindUtilityExpenseGrid()
    {
        var obj = LWUtility.BindUtilityExpenseGrid();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage GetUtilityDetails(LocWiseUtilityModel update)
    {
        var UDVM = LWUtility.GetUtilityDetails(update);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, UDVM);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage UpdateExpUtilityDetails(LocWiseUtilityModel data)
    {
        var obj = LWUtility.UpdateExpUtilityDetails(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage UploadExcel()
    {
        var httpRequest = HttpContext.Current.Request;
        var obj = LWUtility.UploadExcel(httpRequest);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
}