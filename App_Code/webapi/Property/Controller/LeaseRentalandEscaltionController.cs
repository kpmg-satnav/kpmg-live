﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using UtiltiyVM;
/// <summary>
/// Summary description for LeaseRentalandEscaltionController
/// </summary>
public class LeaseRentalandEscaltionController : ApiController
{
    LeaseRentalandEscaltionService LRE = new LeaseRentalandEscaltionService();
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetPoprtyData()
    {
        var obj = LRE.GetPoprtyData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    //[HttpGet]
    //public HttpResponseMessage getProjects()
    //{
    //    //return Csvc.getProjects();
    //    var obj = LRE.getProjects();
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetGriddata(LeaseProperty data)
    {
        var obj = LRE.GetGriddata(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}