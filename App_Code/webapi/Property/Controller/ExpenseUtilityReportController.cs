﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


public class ExpenseUtilityReportController : ApiController
{

    ExpenseUtilityReportService ExpSer = new ExpenseUtilityReportService();

    [HttpPost]
    public HttpResponseMessage GetCustomizedDetails(ExpenseUtilityReportVM Custdata)
    {
        var obj = ExpSer.GetCustomizedObject(Custdata);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public async Task<HttpResponseMessage> GetCustomizedData([FromBody]ExpenseUtilityReportVM data)
    {
        ReportGenerator<ExpenseCustomizedData> reportgen = new ReportGenerator<ExpenseCustomizedData>()
        {
            ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Property_Mgmt/ExpenseUtilityReport.rdlc"),
            DataSetName = "ExpenseUtilityDS",
            ReportType = "Expense Utility Report"
        };

        ExpSer = new ExpenseUtilityReportService();
        List<ExpenseCustomizedData> reportdata = ExpSer.GetCustomizedDetails(data);
        string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/ExpenseUtilityReport." + data.Type);
        await reportgen.GenerateReport(reportdata, filePath, data.Type);
        HttpResponseMessage result = null;
        result = Request.CreateResponse(HttpStatusCode.OK);
        result.Content = new StreamContent(new FileStream(filePath, FileMode.Open));
        result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
        result.Content.Headers.ContentDisposition.FileName = "ExpenseUtilityReport." + data.Type;
        return result;
    }
}