﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
/// <summary>
/// Summary description for LeaseReportService
/// </summary>
public class LeaseReportService
{
    SubSonic.StoredProcedure sp;
    List<LeaseReportModel> Leaselist;
    LeaseReportModel Lease;
    DataSet ds;
    public object GetLeaseData(Company Propdetails)
    {
        try
        {
            Leaselist = LeaseReportDetails(Propdetails);
            if (Leaselist.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = Leaselist };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<LeaseReportModel> LeaseReportDetails(Company Propdetails)
    {
        try
        {
            List<LeaseReportModel> LeaseData = new List<LeaseReportModel>();

            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@FRMDT", SqlDbType.DateTime);
            param[0].Value = Propdetails.FromDate;
            param[1] = new SqlParameter("@TODT", SqlDbType.DateTime);
            param[1].Value = Propdetails.ToDate;
            param[2] = new SqlParameter("@AURID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];

            if (Propdetails.CNP_NAME == null)
            {
                param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[3].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[3].Value = Propdetails.CNP_NAME;
            }
            param[4] = new SqlParameter("@LOCLST", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(Propdetails.loclst);
            param[5] = new SqlParameter("@SEARCHVAL", SqlDbType.VarChar);
            param[5].Value = Propdetails.SearchValue;
            param[6] = new SqlParameter("@PAGENUM", SqlDbType.Int);
            param[6].Value = Propdetails.PageNumber;
            param[7] = new SqlParameter("@PAGESIZE", SqlDbType.Int);
            param[7].Value = Propdetails.PageSize;

        


            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_LEASE_REPORT", param))
            {
               
                while (reader.Read())
                {
                    Lease = new LeaseReportModel();
                    Lease.PROP_TYPE = reader["PROP_TYPE"].ToString();
                    Lease.PRP_NAME = reader["PRP_NAME"].ToString();
                    Lease.LEASE_SDATE = reader["LEASE_SDATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["LEASE_SDATE"]);
                    Lease.LEASE_EDATE = reader["LEASE_EDATE"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["LEASE_EDATE"]);
                    Lease.MON_RENT = reader["MONTHLY_RENT"].ToString();
                    Lease.SEC_DEPOSIT = reader["SECURITY_DEPOSIT"].ToString();
                    Lease.TOTAL_RENT = reader["TOTAL_RENT"].ToString();
                    Lease.branch_code = reader["branch_code"].ToString();
                    Lease.STATE = reader["STATE"].ToString();
                    Lease.MAINT_CHRG = reader["PM_LC_MAINT_CHARGES"].ToString();
                    Lease.LL_NUM = reader["PM_LL_NUMBER"].ToString();
                    Lease.BRO_NAME = reader["BROKER_NAME"].ToString();
                    Lease.BRO_FEE = reader["BROKER_FEE"].ToString();
                    Lease.LAND_NAME = reader["LANDLORD_NAME"].ToString();
                    Lease.LAND_ADDR = reader["LANDLORD_ADDRESS"].ToString();
                    Lease.LAND_RENT = reader["LANDLORD_RENT"].ToString();
                    Lease.LAND_SEC_DEP = reader["LANDLORD_DEPOSIT"].ToString();
                    Lease.LAND_GST = reader["LAND_GST"].ToString();
                    Lease.landlord_phone = reader["LANDLORD_NUMBER"].ToString();
                    Lease.lease_payment_terms = reader["LEASE_PAYMENT_TERMS"].ToString();
                    Lease.CHE_NAME = reader["CHE_NAME"].ToString();
                    Lease.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();

                    LeaseData.Add(Lease);
                }
                reader.Close();
            }

            return LeaseData;
        }
        catch(Exception ex)
        {
            throw;
        }
    }

    public string GetTotlaSum()
    {
        //object Tol_Amount = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_LEASE_LAST_3MONTHS_RENT");
        
       DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PM_LEASE_LAST_3MONTHS_RENT");
       string val = ds.Tables[0].Rows[0]["LAST_3MONTHS_TOTAL_RENT"].ToString(); 
        return val;
    }


}