﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LeaseEscalationService
/// </summary>
public class LeaseEscalationService
{
    SubSonic.StoredProcedure sp;
    public object GetSLATime()
    {

        List<DataRow> statusRows = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PROP_GET_LEASE_ESC_STATUS").GetDataSet().Tables[0].Select().AsEnumerable().ToList();
        List<DataRow> rolesRows = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PROP_GET_LEASE_ESC_ROLES").GetDataSet().Tables[0].Select().AsEnumerable().ToList();

        List<STATUS> statuslist = new List<STATUS>();
        List<ROLE> rolelist = new List<ROLE>();
        STATUS stat;
        foreach (DataRow dr in statusRows)
        {
            stat = new STATUS();
            stat.STA_ID = (int)dr["STA_ID"];
            stat.STA_DESC = dr["STA_TITLE"].ToString();
            statuslist.Add(stat);
        }

        ROLE role;
        foreach (DataRow dr in rolesRows)
        {
            role = new ROLE();
            role.ROL_ID = (int)dr["ROL_ID"];
            role.ROL_DESCRIPTION = dr["ROL_DESCRIPTION"].ToString();
            rolelist.Add(role);
        }

        Dictionary<object, object> dictStatus = new Dictionary<object, object>();
        Dictionary<object, object> dictRole;


        foreach (STATUS status in statuslist)
        {
            var selected = false;
            dictRole = new Dictionary<object, object>();
            foreach (ROLE rol in rolelist)
            {
                dictRole.Add(new { SLAD_HDM_ROL_ID = rol.ROL_ID }, new { SLAD_ESC_TIME = 0, SLAD_ESC_TIME_TYPE = "1" });
            }
            dictStatus.Add(new { SLAD_HDM_STATUS = status.STA_ID, SLAD_DESC = status.STA_DESC, selected = selected }, dictRole.ToList());
        }
        return new { SLADET = dictStatus.ToList(), ROLELST = rolelist, STATUSLST = statuslist };
    }



    //To insert  records into SLA

    public LeaseEsc SaveDetails(LeaseEscalationModel dataobject)
    {
        List<LeaseEscalationModel> SLAlist = new List<LeaseEscalationModel>();
        JArray jarr = dataobject.SLADET;
        var SLA = new LeaseEsc();

        List<JarrMain> Slamain = jarr.ToObject<List<JarrMain>>();

        SqlParameter[] param = new SqlParameter[4];

        param[0] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(dataobject.loclst);
        param[1] = new SqlParameter("@SLA_USR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[2].Value = HttpContext.Current.Session["COMPANYID"];
        param[3] = new SqlParameter("@ESC_TYPE", SqlDbType.Structured);
        param[3].Value = UtilityService.ConvertToDataTable(dataobject.OPStaDet);

        Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PROP_INSERT_LEASE_ESC", param);

        int slad_sla_id = (int)o;

        if (slad_sla_id == 0)
        {
            SLA.SLA_ID = slad_sla_id;
        }
        else
        {
            List<LeaseSLADetailsModel> slalst = Insert_SLA_Details(Slamain, slad_sla_id);
            SLA.SLA_ID = 1;

        }
        dataobject.SLA = SLA;
        return SLA;
    }

    //To insert records into SLADetails
    public List<LeaseSLADetailsModel> Insert_SLA_Details(List<JarrMain> slamain, Int32 slad_sla_id)
    {
        LeaseSLADetailsModel staref;
        List<LeaseSLADetailsModel> slalst = new List<LeaseSLADetailsModel>();
        foreach (JarrMain slaouter in slamain)
        {
            dynamic status = slaouter.Key;
            int sta = Convert.ToInt32(status.SLAD_HDM_STATUS.Value);
            bool email = status.selected.Value;
            foreach (JarrSub slainner in slaouter.Value)
            {
                dynamic role = slainner;
                if (Convert.ToInt32(role.Value.SLAD_ESC_TIME.Value) == 0)
                    continue;
                staref = new LeaseSLADetailsModel();
                staref.SLAD_HDM_STATUS = sta;
                staref.SLAD_EMAIL_ESC = email;
                staref.SLAD_HDM_ROL_ID = Convert.ToInt32(role.Key.SLAD_HDM_ROL_ID.Value);
                staref.SLAD_ESC_TIME = Convert.ToInt32(role.Value.SLAD_ESC_TIME.Value);
                staref.SLAD_ESC_TIME_TYPE = Convert.ToInt32(role.Value.SLAD_ESC_TIME_TYPE.Value);
                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PROP_INSERT_LEASE_ESC_DETAILS");
                sp.Command.AddParameter("@SLAD_SLA_ID", slad_sla_id, DbType.Int32);
                sp.Command.AddParameter("@SLAD_HDM_STATUS", staref.SLAD_HDM_STATUS, DbType.Int32);
                sp.Command.AddParameter("@SLAD_HDM_ROL_ID", staref.SLAD_HDM_ROL_ID, DbType.Int32);
                sp.Command.AddParameter("@SLAD_ESC_TIME", staref.SLAD_ESC_TIME, DbType.Int32);
                sp.Command.AddParameter("@SLAD_ESC_TIME_TYPE", staref.SLAD_ESC_TIME_TYPE, DbType.Int32);
                sp.Command.AddParameter("@SLAD_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.AddParameter("@SLAD_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.AddParameter("@SLAD_EMAIL_ESC", staref.SLAD_EMAIL_ESC, DbType.Boolean);
                sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
                sp.Execute();
            }
        }
        return slalst;
    }
    //update SLA
    public LeaseEsc UpdateDetails(LeaseEscalationModel dataobject)
    {
        var SLA = new LeaseEsc();
        List<LeaseEscalationModel> SLAlist = new List<LeaseEscalationModel>();
        JArray jarr = dataobject.SLADET;
        List<JarrMain> Slamain = jarr.ToObject<List<JarrMain>>();

        SqlParameter[] param = new SqlParameter[5];

        param[0] = new SqlParameter("@SLA_ID", SqlDbType.NVarChar);
        param[0].Value = dataobject.SLA.SLA_ID;
        param[1] = new SqlParameter("@LOC_LIST", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(dataobject.loclst);
        param[2] = new SqlParameter("@SLA_USR_ID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["UID"];
        param[3] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[3].Value = HttpContext.Current.Session["COMPANYID"];
        param[4] = new SqlParameter("@ESC_TYPE", SqlDbType.Structured);
        param[4].Value = UtilityService.ConvertToDataTable(dataobject.OPStaDet);
        Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PROP_UPDATE_LEASE_ESC", param);

        int slad_sla_id = (int)o;

        if (slad_sla_id == 0)
        {
            SLA.SLA_ID = slad_sla_id;
        }
        else
        {
            List<LeaseSLADetailsModel> slalst = Update_SLA_Details(Slamain, dataobject.SLA.SLA_ID);
            SLA.SLA_ID = dataobject.SLA.SLA_ID;
        }
        return SLA;
    }
    // Update SLA Details
    public List<LeaseSLADetailsModel> Update_SLA_Details(List<JarrMain> slamain, int i)
    {
        LeaseSLADetailsModel staref;
        List<LeaseSLADetailsModel> slalst = new List<LeaseSLADetailsModel>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PROP_DELETE_LEASE_ESC_DETAILS");
        sp.Command.AddParameter("@SLAD_SLA_ID", i, DbType.Int32);
        sp.Execute();


        foreach (JarrMain slaouter in slamain)
        {
            dynamic status = slaouter.Key;

            int sta = Convert.ToInt32(status.SLAD_HDM_STATUS.Value);
            bool email = status.selected.Value;
            foreach (JarrSub slainner in slaouter.Value)
            {
                dynamic role = slainner;
                if (Convert.ToInt32(role.Value.SLAD_ESC_TIME.Value) == 0)
                    continue;
                staref = new LeaseSLADetailsModel();
                staref.SLAD_HDM_STATUS = sta;
                staref.SLAD_EMAIL_ESC = email;
                staref.SLAD_HDM_ROL_ID = Convert.ToInt32(role.Key.SLAD_HDM_ROL_ID.Value);
                staref.SLAD_ESC_TIME = Convert.ToInt32(role.Value.SLAD_ESC_TIME.Value);
                staref.SLAD_ESC_TIME_TYPE = Convert.ToInt32(role.Value.SLAD_ESC_TIME_TYPE.Value);
                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PROP_INSERT_LEASE_ESC_DETAILS");
                sp.Command.AddParameter("@SLAD_SLA_ID", i, DbType.Int32);
                sp.Command.AddParameter("@SLAD_HDM_STATUS", staref.SLAD_HDM_STATUS, DbType.Int32);
                sp.Command.AddParameter("@SLAD_HDM_ROL_ID", staref.SLAD_HDM_ROL_ID, DbType.Int32);
                sp.Command.AddParameter("@SLAD_ESC_TIME", staref.SLAD_ESC_TIME, DbType.Int32);
                sp.Command.AddParameter("@SLAD_ESC_TIME_TYPE", staref.SLAD_ESC_TIME_TYPE, DbType.Int32);
                sp.Command.AddParameter("@SLAD_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.AddParameter("@SLAD_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.AddParameter("@SLAD_EMAIL_ESC", staref.SLAD_EMAIL_ESC, DbType.Boolean);
                sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
                sp.Execute();
            }
        }
        return slalst;
    }

    //Grid View
    public IEnumerable<LeaseEsc> GetSLAList()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PROP_GET_LEASE_ESC_DETAILS").GetReader())
        {

            List<LeaseEsc> SLAlist = new List<LeaseEsc>();
            while (reader.Read())
            {
                SLAlist.Add(new LeaseEsc()
                {
                    SLA_CNY_CODE = reader["L_ESC_CNY_CODE"].ToString(),
                    SLA_CTY_CODE = reader["L_ESC_CTY_CODE"].ToString(),
                    SLA_LOC_CODE = reader["L_ESC_LOC_CODE"].ToString(),
                    CNY_NAME = reader["CNY_NAME"].ToString(),
                    CTY_NAME = reader["CTY_NAME"].ToString(),
                    LCM_NAME = reader["LCM_NAME"].ToString(),
                    SLA_ID = (int)reader["L_ESC_ID"],
                    L_ESC_TYPE = (int)reader["L_ESC_TYPE"],
                });
            }
            reader.Close();
            return SLAlist;
        }
    }



    // To edit the record
    public object EditSLADetails(int id)
    {

        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PROP_EDIT_LEASE_ESC_DETAILS");
        sp.Command.Parameters.Add("@SLA_ID", id);
        using (IDataReader reader = sp.GetReader())
        {
            List<LeaseSLADetailsModel> sladetlist = new List<LeaseSLADetailsModel>();

            while (reader.Read())
            {
                sladetlist.Add(new LeaseSLADetailsModel()
                {
                    SLAD_HDM_STATUS = (int)reader["L_ESCD_STATUS"],
                    SLAD_HDM_ROL_ID = (int)reader["L_ESCD_ROL_ID"],
                    SLAD_ESC_TIME = (int)reader["L_ESCD_ESC_TIME"],
                    SLAD_ESC_TIME_TYPE = (int)reader["L_ESCD_ESC_TIME_TYPE"],
                    SLAD_SLA_ID = (int)reader["L_ESCD_ESC_ID"],
                    SLAD_EMAIL_ESC = (bool)reader["L_ESCD_EMAIL_ESC"]
                });
            }
            reader.Close();


            List<DataRow> statusRows = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PROP_GET_LEASE_ESC_STATUS").GetDataSet().Tables[0].Select().AsEnumerable().ToList();
            List<DataRow> rolesRows = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PROP_GET_LEASE_ESC_ROLES").GetDataSet().Tables[0].Select().AsEnumerable().ToList();
            List<STATUS> statuslist = new List<STATUS>();
            List<ROLE> rolelist = new List<ROLE>();
            STATUS stat;
            foreach (DataRow dr in statusRows)
            {
                stat = new STATUS();
                stat.STA_ID = (int)dr["STA_ID"];
                stat.STA_DESC = dr["STA_TITLE"].ToString();
                statuslist.Add(stat);
            }

            ROLE role;
            foreach (DataRow dr in rolesRows)
            {
                role = new ROLE();
                role.ROL_ID = (int)dr["ROL_ID"];
                role.ROL_DESCRIPTION = dr["ROL_DESCRIPTION"].ToString();
                rolelist.Add(role);
            }

            Dictionary<object, object> dictStatus = new Dictionary<object, object>();

            Dictionary<object, object> dictRole;
            foreach (STATUS status in statuslist)
            {
                dictRole = new Dictionary<object, object>();
                var selected = false;
                foreach (ROLE rol in rolelist)
                {
                    var sladet = sladetlist.Where(det => det.SLAD_HDM_STATUS == status.STA_ID && det.SLAD_HDM_ROL_ID == rol.ROL_ID)
                                .Select(slad => new
                                {
                                    SLAD_ESC_TIME = slad.SLAD_ESC_TIME,
                                    SLAD_ESC_TIME_TYPE = slad.SLAD_ESC_TIME_TYPE.ToString(),
                                    SLAD_EMAIL_ESC = slad.SLAD_EMAIL_ESC

                                }).FirstOrDefault();
                    if (sladet != null)
                    {
                        dictRole.Add(new { SLAD_HDM_ROL_ID = rol.ROL_ID }, sladet);
                        selected = selected ? selected : sladet.SLAD_EMAIL_ESC;
                    }

                    else
                        dictRole.Add(new { SLAD_HDM_ROL_ID = rol.ROL_ID }, new
                        {
                            SLAD_ESC_TIME = 0,
                            SLAD_ESC_TIME_TYPE = "1"
                        });
                }
                dictStatus.Add(new { SLAD_HDM_STATUS = status.STA_ID, SLAD_DESC = status.STA_DESC, selected = selected }, dictRole.ToList());

            }
            return new { SLADET = dictStatus.ToList(), ROLELST = rolelist, STATUSLST = statuslist };

        }
    }
}