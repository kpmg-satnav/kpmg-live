﻿using Newtonsoft.Json.Linq;
using System;
using System.Activities.Statements;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using UtiltiyVM;
using Microsoft.Reporting.WebForms;
/// <summary>
/// Summary description for TenantReportService
/// </summary>
public class TenantPaymentReportService
{
    SubSonic.StoredProcedure sp;
    List<TenantPaymentReportModel> Tenlist;
    TenantPaymentReportModel Tenant;
    DataSet ds;
    public object GetLeaseData(Company Propdetails)
    {
        try
        {
            Tenlist = TenantReportDetails(Propdetails);
            if (Tenlist.Count != 0)
            {
                return new { Message = MessagesVM.SER_OK, data = Tenlist };
            }
            else
            {
                return new { Message = MessagesVM.SER_OK, data = (object)null };
            }

        }

        catch (Exception ex) { return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null }; }
    }

    public List<TenantPaymentReportModel> TenantReportDetails(Company Propdetails)
    {
        try
        {
            List<TenantPaymentReportModel> TenData = new List<TenantPaymentReportModel>();

            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@FRMDT", SqlDbType.DateTime);
            param[0].Value = Propdetails.FromDate;
            param[1] = new SqlParameter("@TODT", SqlDbType.DateTime);
            param[1].Value = Propdetails.ToDate;
            param[2] = new SqlParameter("@AURID", SqlDbType.NVarChar);
            param[2].Value = HttpContext.Current.Session["UID"];

            if (Propdetails.CNP_NAME == null)
            {
                param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[3].Value = HttpContext.Current.Session["COMPANYID"];
            }
            else
            {
                param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[3].Value = Propdetails.CNP_NAME;
            }
            param[4] = new SqlParameter("@SEARCHVAL", SqlDbType.VarChar);
            param[4].Value = Propdetails.SearchValue;
            param[5] = new SqlParameter("@PAGENUM", SqlDbType.VarChar);
            param[5].Value = Propdetails.PageNumber;
            param[6] = new SqlParameter("@PAGESIZE", SqlDbType.VarChar);
            param[6].Value = Propdetails.PageSize;

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_GET_TEN_PAYMENT_RPT", param))
            {
                while (reader.Read())
                {
                    Tenant = new TenantPaymentReportModel();
                    Tenant.TENANT = reader["TENANT"].ToString();
                    Tenant.PRP_NAME = reader["PM_PPT_NAME"].ToString();
                    //Tenant.APT_NO = reader["PM_PPT_APT_NO"].ToString();
                    Tenant.RENT = Convert.ToDecimal(reader["PM_RP_TEN_RENT"]);
                    Tenant.MAIN_FEE = Convert.ToDecimal(reader["PM_RP_TEN_MAINT_FEES"]);
                    Tenant.TOT_RENT = Convert.ToDecimal(reader["PM_RP_OUST_TDS_AMOUNT"]);
                    Tenant.PAIDDATE = reader["PAID_DATE"].ToString();
                    Tenant.FROMDATE = reader["PM_RP_FROMDATE"].ToString();
                    Tenant.TODATE = reader["PM_RP_TODATE"].ToString();
                    Tenant.TDS = reader["PM_RP_TDS_VALUE"].ToString();
                    Tenant.PAY_MODE = reader["PM_RP_PAY_MODE"].ToString();
                    Tenant.CHEQUENO = reader["PM_RP_CHEQUENO"].ToString();
                    Tenant.CHEQUEDATE = reader["PM_RP_CHEQUEDATE"].ToString();
                    Tenant.ISS_BANK = reader["PM_RP_ISSUINGBANK"].ToString();
                    Tenant.IFSC = reader["PM_RP_IFCB"].ToString();
                    Tenant.ACCNUM = reader["PM_RP_ACCOUNTNUM"].ToString();
                    Tenant.INVOICE_NO = reader["PM_RP_INV_NO"].ToString();
                    Tenant.TRN_NO = reader["PM_RP_TRN_NO"].ToString();
                    Tenant.OVERALL_COUNT = reader["OVERALL_COUNT"].ToString();
                    TenData.Add(Tenant);
                }
                reader.Close();
            }
            return TenData;
        }
        catch
        {
            throw;
        }
    }
	
}