﻿using Microsoft.Reporting.WebForms;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using UtiltiyVM;
using LocWiseUtilityVModel;

    public class LocWiseUtilityService
    {
        SubSonic.StoredProcedure sp;
        //Location
        public IEnumerable<ExpLocationVM> BindLocation()
        {
            IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_UTILITY_LOCATION").GetReader();
            List<ExpLocationVM> LocUtilityList = new List<ExpLocationVM>();
            while (reader.Read())
            {
                LocUtilityList.Add(new ExpLocationVM()
                {
                    LCM_CODE = reader.GetValue(0).ToString(),
                    LCM_NAME = reader.GetValue(1).ToString(),
                    ticked = false
                });
            }
            reader.Close();
            return LocUtilityList;
        }

        //ExpenseHead
        public IEnumerable<ExpHeadUtilityModel> BindExpenseHead()
        {
            IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_BIND_EXPENSE_HEAD").GetReader();
            List<ExpHeadUtilityModel> ExpenseUtility = new List<ExpHeadUtilityModel>();
            while (reader.Read())
            {
                ExpenseUtility.Add(new ExpHeadUtilityModel()
                {
                    EXP_CODE = reader.GetValue(0).ToString(),
                    EXP_NAME = reader.GetValue(1).ToString(),
                   ticked = false

                });
            }
            reader.Close();
            return ExpenseUtility;
        }

        //BindUser
        //public IEnumerable<UserVM> BindUser()
        //{
        //    IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_FINANCE_HEAD").GetReader();
        //    List<UserVM> UserList = new List<UserVM>();
        //    while (reader.Read())
        //    {
        //        UserList.Add(new UserVM()
        //        {
        //            AUR_ID = reader.GetValue(0).ToString(),
        //            AUR_NAME = reader.GetValue(1).ToString()

        //        });
        //    }
        //    reader.Close();
        //    return UserList;
        //}

        //BindDepartment
        public IEnumerable<DepVM> BindDepartment()
        {
            IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_BIND_DEPARTMENT").GetReader();
            List<DepVM> DepList = new List<DepVM>();
            while (reader.Read())
            {
                DepList.Add(new DepVM()
                {
                    DEP_CODE = reader.GetValue(0).ToString(),
                    DEP_NAME = reader.GetValue(1).ToString()

                });
            }
            reader.Close();
            return DepList;
        }
        public List<ExcelDownload> ExcelDownload(ExcelDownload spcdet)
        {
            List<ExcelDownload> dwndatalst = new List<ExcelDownload>();
            try
            {

                SqlParameter[] param = new SqlParameter[1];

                ExcelDownload dwndata;

                using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_EXPENSE_DOWN_DAT"))
                {
                    while (sdr.Read())
                    {
                        dwndata = new ExcelDownload();
                        dwndata.LCM_NAME = sdr["LCM_NAME"].ToString();
                        dwndata.EXP_NAME = sdr["EXP_NAME"].ToString();
                        dwndata.BILL_DATE = sdr["BILL_DATE"].ToString();
                        dwndata.BILL_NO = sdr["BILL_NO"].ToString();
                        dwndata.BILL_INVOICE = sdr["BILL_INVOICE"].ToString();
                        dwndata.AMOUNT = sdr["AMOUNT"].ToString();
                        dwndata.VENDOR = sdr["VENDOR"].ToString();
                        dwndata.VENDOR_EMAIL = sdr["VENDOR_EMAIL"].ToString();
                        dwndata.VENDOR_PHNO = sdr["VENDOR_PHNO"].ToString();
                        dwndata.DEP_NAME = sdr["DEP_NAME"].ToString();
                        dwndatalst.Add(dwndata);
                    }
                    sdr.Close();
                }

                return dwndatalst;

            }
            catch (Exception Ex)
            {
                return dwndatalst;
            }

        }
        //SaveData
        public object SaveData(LocWiseUtilityModel Save)
        {
            try
            {
                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_INSERT_UTILITY_EXPENSES");
                sp.Command.AddParameter("@LOC_CODE", Save.LCM_CODE, DbType.String);
                sp.Command.AddParameter("@EXP_HEAD", Save.EXP_CODE, DbType.String);
                sp.Command.AddParameter("@BILL_NO", Save.BILL_NO, DbType.String);
                sp.Command.AddParameter("@INVOICE", Save.BILL_INVOICE, DbType.String);
                sp.Command.AddParameter("@AMT", Save.AMT, DbType.String);
                sp.Command.AddParameter("@BILL_DATE", Save.FromDate, DbType.String);
                sp.Command.AddParameter("@VEN_NAME", (Save.VENDOR == null) ? "--" : Save.VENDOR, DbType.String);
                sp.Command.AddParameter("@VEN_EMAIL", (Save.VEN_MAIL == null) ? "--" : Save.VEN_MAIL, DbType.String);
                sp.Command.AddParameter("@VEN_PH", (Save.VEN_PHNO == null) ? "--" : Save.VEN_PHNO, DbType.String);
                sp.Command.AddParameter("@DEPID", Save.DEP_CODE, DbType.String);
                sp.Command.AddParameter("@REM", (Save.REM == null) ? "--" : Save.REM, DbType.String);
                sp.Command.Parameters.Add("@AURID", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
                sp.Execute();
                return new { Message = "Data Inserted Successfully", data = Save };
            }
            catch (Exception ex)
            {
                return new { Message = ex.Message, data = (object)null };
            }

        }

        //update
        public object UpdateExpUtilityDetails(LocWiseUtilityModel update)
        {
            try
            {

                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_UPDATE_UTILITY_EXPENSE_DETAILS");
                sp.Command.AddParameter("@LOC_CODE", update.LCM_CODE, DbType.String);
                sp.Command.AddParameter("@EXP_HEAD", update.EXP_CODE, DbType.String);
                sp.Command.AddParameter("@BILL_NO", update.BILL_NO, DbType.String);
                sp.Command.AddParameter("@INVOICE", update.BILL_INVOICE, DbType.String);
                sp.Command.AddParameter("@AMT", update.AMT, DbType.String);
                sp.Command.AddParameter("@BILL_DATE", update.FromDate, DbType.String);
                sp.Command.AddParameter("@VEN_NAME", (update.VENDOR == null) ? "--" : update.VENDOR, DbType.String);
                sp.Command.AddParameter("@VEN_EMAIL", (update.VEN_MAIL == null) ? "--" : update.VEN_MAIL, DbType.String);
                sp.Command.AddParameter("@VEN_PH", (update.VEN_PHNO == null) ? "--" : update.VEN_PHNO, DbType.String);
                sp.Command.AddParameter("@DEPID", update.DEP_CODE, DbType.String);
                sp.Command.AddParameter("@REM", (update.REM == null) ? "--" : update.REM, DbType.String);
                sp.Command.Parameters.Add("@AURID", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.Parameters.Add("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
                sp.Execute();
                return new { Message = "Data Updated Successfully", data = update };
            }
            catch (Exception ex)
            {
                return new { Message = ex.Message, data = (object)null };
            }
        }

        //BindGrid
        public IEnumerable<LocWiseUtilityModel> BindUtilityExpenseGrid()
        {
            IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_BIND_UTILITY_EXPENSE_GRID").GetReader();
            List<LocWiseUtilityModel> UtilityExpGridList = new List<LocWiseUtilityModel>();
            while (reader.Read())
            {
                UtilityExpGridList.Add(new LocWiseUtilityModel()
                {
                    EXP_CODE = reader["EXP_CODE"].ToString(),
                    LCM_CODE = reader["LCM_CODE"].ToString(),
                    FromDate = reader["BILL_DATE"].ToString(),
                    BILL_NO = reader["BILL_NO"].ToString(),
                    BILL_INVOICE = reader["BILL_INVOICE"].ToString(),
                    AMT = Convert.ToDecimal(reader["AMOUNT"]),
                    DEP_CODE = reader["BILL_GENERATED_BY"].ToString(),
                    VENDOR = reader["VENDOR"].ToString(),
                    VEN_MAIL = reader["VENDOR_EMAIL"].ToString(),
                    LCM_NAME = reader["LCM_NAME"].ToString(),
                    EXP_NAME = reader["EXP_NAME"].ToString(),
                    DEP_NAME = reader["DEP_NAME"].ToString()

                });
            }
            reader.Close();
            return UtilityExpGridList;
        }

        public object GetUtilityDetails(LocWiseUtilityModel BindDetails)
        {
            try
            {
                LocWiseUtilityVM UDVMList = new LocWiseUtilityVM();

                sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_UTILDETAILS_ON_SELECTION");
                sp.Command.AddParameter("@LOC_CODE", BindDetails.LCM_CODE, DbType.String);
                sp.Command.AddParameter("@EXP_CODE", BindDetails.EXP_CODE, DbType.String);
                sp.Command.AddParameter("@DATE", BindDetails.FromDate, DbType.String);
                using (IDataReader reader = sp.GetReader())
                {
                    LocWiseUtilityModel UDList = new LocWiseUtilityModel();
                    if (reader.Read())
                    {

                        UDList.LCM_CODE = reader["LCM_CODE"].ToString();
                        UDList.EXP_CODE = reader["EXP_CODE"].ToString();
                        UDList.FromDate = reader["BILL_DATE"].ToString();
                        UDList.BILL_NO = reader["BILL_NO"].ToString();
                        UDList.BILL_INVOICE = reader["BILL_INVOICE"].ToString();
                        UDList.AMT = Convert.ToDecimal(reader["AMOUNT"]);
                        UDList.VENDOR = reader["VENDOR"].ToString();
                        UDList.VEN_MAIL = reader["VENDOR_EMAIL"].ToString();
                        UDList.VEN_PHNO = reader["VENDOR_PHNO"].ToString();
                        UDList.DEP_CODE = reader["BILL_GENERATED_BY"].ToString();
                        UDList.REM = reader["REMARKS"].ToString();
                        UDList.STATUS = "Modify";

                    }
                    else
                    {
                        UDList.FromDate = BindDetails.FromDate;
                        UDList.EXP_CODE = BindDetails.EXP_CODE;
                        UDList.LCM_CODE = BindDetails.LCM_CODE;
                        UDList.STATUS = "New";
                    }
                    reader.Close();
                    UDVMList.LocWiseUtilityModel = UDList;
                    return UDVMList;
                }

            }
            catch (Exception ex)
            {
                return new { Message = ex.ToString(), data = (object)null };
            }
        }
        public object UploadExcel(HttpRequest httpRequest)
        {
            try
            {
                List<UtilityExpenses> uadmlst = GetDataTableFrmReq(httpRequest);

                String jsonstr = httpRequest.Params["CurrObj"];

                UPLTYPEVM UVM = (UPLTYPEVM)Newtonsoft.Json.JsonConvert.DeserializeObject<UPLTYPEVM>(jsonstr);

                string str = "INSERT INTO " + HttpContext.Current.Session["TENANT"] + "." + "UTILITY_BY_LOCATION_TEMP (LCM_CODE,EXP_HEAD,BILL_DATE,BILL_NO,BILL_INVOICE,AMOUNT,VENDOR,VENDOR_EMAIL,VENDOR_PHNO,Bill_GENERATED_BY,UPDATED_BY) VALUES ";
                string str1 = "";
                int retval, cnt = 1;
                foreach (UtilityExpenses uadm in uadmlst)
                {


                    if ((cnt % 1000) == 0)
                    {
                        str1 = str1.Remove(str1.Length - 1, 1);
                        retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                        if (retval == -1)
                        {
                            return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
                        }
                        else
                        {
                            str1 = "";
                            cnt = 1;
                        }

                    }

                    str1 = str1 + string.Format("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}'),",
                              uadm.LCM_NAME,
                              uadm.EXP_NAME,
                              uadm.BILL_DATE,
                              uadm.BILL_NO,
                              uadm.BILL_INVOICE,
                              uadm.AMOUNT,
                              uadm.VENDOR,
                              uadm.VENDOR_EMAIL,
                              uadm.VENDOR_PHNO,
                              uadm.DEP_NAME,
                              HttpContext.Current.Session["Uid"]

                            );
                    cnt = cnt + 1;
                }
                str1 = str1.Remove(str1.Length - 1, 1);
                retval = SqlHelper.ExecuteNonQuery(CommandType.Text, str + str1);
                if (retval != -1)
                {
                    SqlParameter[] param = new SqlParameter[2];
                    param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                    param[0].Value = HttpContext.Current.Session["UID"];
                    param[1] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
                    param[1].Value = HttpContext.Current.Session["COMPANYID"];
                    DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "PM_UTILITY_EXPENSES_UPL_DATA", param);
                    return new { Message = MessagesVM.UAD_UPLOK, data = dt };
                }
                else
                    return new { Message = MessagesVM.UAD_UPLFAIL, data = (object)null };
            }
            catch (Exception ex)
            {
                return new { Message = MessagesVM.ErrorMessage, Info = ex.InnerException, data = (object)null };
            }
        }
        public List<UtilityExpenses> GetDataTableFrmReq(HttpRequest httpRequest)
        {
            DataTable dt = new DataTable();

            var postedFile = httpRequest.Files[0];
            var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + Path.GetFileName(postedFile.FileName));
            postedFile.SaveAs(filePath);

            //var uplst = CreateExcelFile.ReadAsList(filePath, "all");
            var uplst = CreateExcelFile.ReadAsListExp(filePath);
            if (uplst.Count != 0)
                uplst.RemoveAt(0);
            return uplst;
        }

    }

