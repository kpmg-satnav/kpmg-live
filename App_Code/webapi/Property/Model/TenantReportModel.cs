﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for TenantReportModel
/// </summary>
public class TenantReportModel
{
    public List<Locationlst> selectedLoc { get; set; }
    public List<GetPropertyType> selectedPrpType { get; set; }
    public List<GetPropertyName> selectedPrpName { get; set; }
    public string CNP_NAME { get; set; }
    public string Type { get; set; }
    public string SearchValue { get; set; }
    public string PageNumber { get; set; }
    public string PageSize { get; set; }
}



public class TenantModels
{
    public string TEN_PRPTYPE { get; set; }
    public string TEN_CODE { get; set; }
    public string TEN_NAME { get; set; }
    public string TEN_EMAIL { get; set; }
    public string OCC_AREA { get; set; }
    public string TEN_RENT { get; set; }
    public string MAINT_FEE { get; set; }
    public string TOT_RENT_AMT { get; set; }
    public string OVERALL_COUNT { get; set; }
}