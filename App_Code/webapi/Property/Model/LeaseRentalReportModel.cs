﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class LeaseRentalReportModel
{
    public string LCM_NAME { get; set; }
    public string PM_PPT_NAME { get; set; }
    public string PM_PPT_ADDRESS { get; set; }
    public string PM_LPD_REQ_ID { get; set; }
    public string PM_LPD_LL_NAME { get; set; }
    public string PM_LPD_LL_BASIC_RENT { get; set; }
    public string PM_LPD_LL_PAY_TERM { get; set; }
    public string PM_PPT_LOC_CODE { get; set; }
    public string PM_LPD_LL_PAY_MONTH { get; set; }
    public string PM_LPD_LL_PAY_YEAR { get; set; }
    public string company { get; set; }
    public string RentPaid { get; set; }
    public string dueamnt { get; set; }
    public string withhold { get; set; }
    public string INVOICE_NO { get; set; }
    public string PAY_MODE { get; set; }
    public string PM_LP_ACC_NO { get; set; }
    public string PM_LP_NEFT_BRNCH { get; set; }
    public string PM_LP_BANK_NAME { get; set; }
    public string PM_LP_IFSC { get; set; }
    public string PM_LPD_CREATED_DT { get; set; }
    public DateTime? PM_LAD_EFFE_DT_AGREEMENT { get; set; }
    public DateTime? PM_LAD_EXP_DT_AGREEMENT { get; set; }

    public DateTime? INVOICE_DT { get; set; }
    public string PM_LPD_LL_STATUS { get; set; }
    public string OVERALL_COUNT { get; set; }
    public string CHE_CODE { get; set; }

}