﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LeaseReportModel
/// </summary>
//public class Company
//{
//    public string CNP_NAME { get; set; }
//    public string PLAN_ID { get; set; }
//    public string Type { get; set; }
//}
public class LeaseReportModel
{
    public string PROP_TYPE { get; set; }
    public string PRP_NAME { get; set; }
    //public string LEASE_SDATE { get; set; }
    //public string LEASE_EDATE { get; set; }
    public DateTime? LEASE_SDATE { get; set; }
    public DateTime? LEASE_EDATE { get; set; }
    public string MON_RENT { get; set; }
    public string SEC_DEPOSIT { get; set; }
    public string BRO_NAME { get; set; }
    public string BRO_FEE { get; set; }
    public string LAND_NAME { get; set; }
    public string LAND_ADDR { get; set; }
    public string LAND_RENT { get; set; }
    public string LAND_SEC_DEP { get; set; }
    public string branch_code { get; set; }
    public string STATE { get; set; }
    public string MAINT_CHRG { get; set; }
    public string LL_NUM { get; set; }
    public string LAND_GST { get; set; }
    public string TOTAL_RENT { get; set; }
    //public string STATE { get; set; }
    public string landlord_phone { get; set; }
    public string lease_payment_terms { get; set; }
    public string CHE_NAME { get; set; }
    public string OVERALL_COUNT { get; set; }
}