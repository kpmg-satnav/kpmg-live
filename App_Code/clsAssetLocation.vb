Imports Microsoft.VisualBasic

Public Class clsAssetLocation
    Private mCategoryName As String

    Public Property CategoryName() As String
        Get
            Return mCategoryName
        End Get
        Set(ByVal value As String)
            mCategoryName = value
        End Set
    End Property
    Private mCategoryId As String

    Public Property CategoryId() As String
        Get
            Return mCategoryId
        End Get
        Set(ByVal value As String)
            mCategoryId = value
        End Set
    End Property
End Class
