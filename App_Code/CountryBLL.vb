Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Collections.Generic
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Amantra.CountryDALN
Imports Amantra.CountryDTON

Namespace Amantra.CountryBLLN

    Public Class CountryBLL
#Region "[ BLL and DTO INSTANCE CREATION ]"
        '' ----------------------------------------------------------------------------------------------- 

        ''' <summary> 
        ''' <Author>Srinivasulu</Author> 
        ''' </summary> 

        Dim countrydal As New CountryDAL()
        Dim countryDTO As IList(Of CountryDTO) = New List(Of CountryDTO)()

        '' ----------------------------------------------------------------------------------------------- 
#End Region

#Region "[ PRIVATE VARIABLE DECLARATION ]"
        '' ----------------------------------------------------------------------------------------------- 
        ''' <summary> 
        ''' <Author>Srinivasulu M</Author> 
        ''' </summary> 

        Dim updatetravel As Integer

        '' ----------------------------------------------------------------------------------------------- 
#End Region
#Region "[ InsertCountry ]"
        '' ----------------------------------------------------------------------------------------------- 

        Public Function insertCountry(ByVal cdto As CountryDTO) As Integer
            Dim iCountryID As Integer = 0
            cdto.Countryid = Convert.ToString(iCountryID)
            iCountryID = countrydal.insertCountry(cdto)
            Return iCountryID
        End Function

        '' ----------------------------------------------------------------------------------------------- 
#End Region
    End Class
End Namespace
