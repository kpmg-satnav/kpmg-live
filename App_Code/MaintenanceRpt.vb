Imports Microsoft.VisualBasic
Imports System

'Public Structure JQGridResults
'    Public page As Integer
'    Public total As Integer
'    Public records As Integer
'    Public rows As JQGridRow()

'End Structure
'Public Structure JQGridRow
'    Public id As Integer
'    Public cell As String()
'End Structure

<Serializable()> _
Public Class MaintenanceRpt

    Public Property strassignedtime() As String
        Get
            Return m_Assign_time
        End Get
        Set(ByVal value As String)
            m_Assign_time = value
        End Set
    End Property
    Private m_Assign_time As String

    Public Property strspcid() As String
        Get
            Return m_spc_id
        End Get
        Set(ByVal value As String)
            m_spc_id = value
        End Set
    End Property
    Private m_spc_id As String
    Public Property strreq() As String
        Get
            Return m_str_req
        End Get
        Set(ByVal value As String)
            m_str_req = value
        End Set
    End Property
    Private m_str_req As String

    Public Property STRDESC() As String
        Get
            Return m_strdesc
        End Get
        Set(ByVal value As String)
            m_strdesc = value
        End Set
    End Property
    Private m_strdesc As String

    Public Property strrequisitionid() As String
        Get
            Return m_req
        End Get
        Set(ByVal value As String)
            m_req = value
        End Set
    End Property
    Private m_req As String
    Public Property straur_type() As String
        Get
            Return m_aur_type
        End Get
        Set(ByVal value As String)
            m_aur_type = value
        End Set
    End Property
    Private m_aur_type As String
    Public Property strservicetype() As String
        Get
            Return m_Ser_type
        End Get
        Set(ByVal value As String)
            m_Ser_type = value
        End Set
    End Property
    Private m_Ser_type As String
    Public Property strcomments() As String
        Get
            Return m_comments
        End Get
        Set(ByVal value As String)
            m_comments = value
        End Set
    End Property
    Private m_comments As String
    Public Property strupdatedby() As String
        Get
            Return m_updated_by
        End Get
        Set(ByVal value As String)
            m_updated_by = value
        End Set
    End Property
    Private m_updated_by As String
    Public Property strdoc() As String
        Get
            Return m_doc
        End Get
        Set(ByVal value As String)
            m_doc = value
        End Set
    End Property
    Private m_doc As String

    Public Property strsno() As String
        Get
            Return m_Sno
        End Get
        Set(ByVal value As String)
            m_Sno = value
        End Set
    End Property
    Private m_Sno As String
    Public Property strreqdate() As String
        Get
            Return m_reqdate
        End Get
        Set(ByVal value As String)
            m_reqdate = value
        End Set
    End Property
    Private m_reqdate As String
    Public Property strreqby() As String
        Get
            Return m_reqby
        End Get
        Set(ByVal value As String)
            m_reqby = value
        End Set
    End Property
    Private m_reqby As String
    Public Property strreqtype() As String
        Get
            Return m_reqtype
        End Get
        Set(ByVal value As String)
            m_reqtype = value
        End Set
    End Property
    Private m_reqtype As String
    Public Property strassignedto() As String
        Get
            Return m_Assignedto
        End Get
        Set(ByVal value As String)
            m_Assignedto = value
        End Set
    End Property
    Private m_Assignedto As String
    Public Property strclosed() As String
        Get
            Return m_closed
        End Get
        Set(ByVal value As String)
            m_closed = value
        End Set
    End Property
    Private m_closed As String
    Public Property strdelayedby() As String
        Get
            Return m_delayed
        End Get
        Set(ByVal value As String)
            m_delayed = value
        End Set
    End Property
    Private m_delayed As String

    Public Property strtotal() As String
        Get
            Return m_total
        End Get
        Set(ByVal value As String)
            m_total = value
        End Set
    End Property
    Private m_total As String

    Public Property strstatus() As String
        Get
            Return m_status
        End Get
        Set(ByVal value As String)
            m_status = value
        End Set
    End Property
    Private m_status As String

    Public Property stralert() As String
        Get
            Return m_alert
        End Get
        Set(ByVal value As String)
            m_alert = value
        End Set
    End Property
    Private m_alert As String

    Public Property strfeedback() As String
        Get
            Return m_feedback
        End Get
        Set(ByVal value As String)
            m_feedback = value
        End Set
    End Property
    Private m_feedback As String

End Class
