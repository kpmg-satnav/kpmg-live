#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Commerce.Common

Namespace Commerce.Providers

	Public Class DeliveryOptionCollection
		Inherits List(Of DeliveryOption)
		Public Sub Load(ByVal rdr As IDataReader)
			Dim [option] As DeliveryOption
			Do While rdr.Read()
				[option] = New DeliveryOption()
				[option].Load(rdr)
				Me.Add([option])
			Loop
		End Sub
		Public Sub Combine(ByVal options As DeliveryOptionCollection)
			For Each [option] As DeliveryOption In options
				Me.Add([option])
			Next [option]
		End Sub
	End Class

	Public Class DeliveryOption

		Private rate_Renamed As Decimal

		Public Property Rate() As Decimal
			Get
				Return rate_Renamed
			End Get
			Set
				rate_Renamed = Value
			End Set
		End Property

		Private amountPerUnit_Renamed As Decimal

		Public Property AmountPerUnit() As Decimal
			Get
				Return amountPerUnit_Renamed
			End Get
			Set
				amountPerUnit_Renamed = Value
			End Set
		End Property
		Private service_Renamed As String

		Public Property Service() As String
			Get
				Return service_Renamed
			End Get
			Set
				service_Renamed = Value
			End Set
		End Property

		Private isAirOnly_Renamed As Boolean

		Public Property IsAirOnly() As Boolean
			Get
				Return isAirOnly_Renamed
			End Get
			Set
				isAirOnly_Renamed = Value
			End Set
		End Property
		Private isGroundOnly_Renamed As Boolean

		Public Property IsGroundOnly() As Boolean
			Get
				Return isGroundOnly_Renamed
			End Get
			Set
				isGroundOnly_Renamed = Value
			End Set
		End Property
		Private isDownloadOnly_Renamed As Boolean

		Public Property IsDownloadOnly() As Boolean
			Get
				Return isDownloadOnly_Renamed
			End Get
			Set
				isDownloadOnly_Renamed = Value
			End Set
		End Property

        Public Sub Load(ByVal rdr As IDataReader)
            'LJD 10/17/07 Cleaned up
            Try
                Me.service_Renamed = rdr("service").ToString()
            Catch
            End Try

            Try
                Me.rate_Renamed = CDec(rdr("rate"))
            Catch
            End Try

            Try
                Me.amountPerUnit_Renamed = CDec(rdr("amountPerUnit"))
            Catch
            End Try

            Try
                Me.isAirOnly_Renamed = CBool(rdr("isAirOnly"))
            Catch
            End Try

            Try
                Me.isDownloadOnly_Renamed = CBool(rdr("isDownloadOnly"))
            Catch

            End Try

        End Sub
    End Class
    'LJD 10/17/07 Added End Namespace
End Namespace
