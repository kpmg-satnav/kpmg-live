#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports Commerce.Providers
Imports System.Configuration
Imports System.Configuration.Provider
Imports System.Web.Configuration
Imports System.Web
Imports System.Web.Caching
Imports System.Data

Namespace Commerce.Providers
	''' <summary>
	''' The tax calculator uses the TaxProvider to calculate the tax amount on 
	''' a set amount. You should consult your local tax laws before using/implementing.
	''' </summary>

	Public Class TaxService
		#Region "Provider-specific bits"
		Private Shared _provider As TaxProvider = Nothing
		Private Shared _lock As Object = New Object()

		Public ReadOnly Property Provider() As TaxProvider
			Get
				Return _provider
			End Get
		End Property

		Public Shared ReadOnly Property Instance() As TaxProvider
			Get
				LoadProviders()
				Return _provider
			End Get
		End Property
		Private Shared Sub LoadProviders()
			' Avoid claiming lock if providers are already loaded
			If _provider Is Nothing Then
				SyncLock _lock
					' Do this again to make sure _provider is still null
					If _provider Is Nothing Then
						' Get a reference to the <TaxServiceSection> section
						Dim section As TaxServiceSection = CType(WebConfigurationManager.GetSection ("TaxService"), TaxServiceSection)

						' Only want one provider here
						 _provider = CType(ProvidersHelper.InstantiateProvider (section.Providers(0), GetType(TaxProvider)), TaxProvider)


						If _provider Is Nothing Then
							Throw New ProviderException ("Unable to load default TaxProvider")
						End If
					End If
				End SyncLock
			End If
		End Sub
		#End Region


		Public Shared Function CalculateAmountByZIP(ByVal zipCode As String, ByVal subTotal As Decimal) As Decimal
			Dim dOut As Decimal = 0
			Try
				Dim dRate As Decimal = Instance.GetTaxRate(zipCode)
				dOut = subTotal * dRate
			Catch x As Exception
				Throw New ApplicationException("Tax calculation failed: " & x.Message, x)
			End Try
			Return dOut
		End Function
		Public Shared Function GetUSTaxRate(ByVal zipCode As String) As Decimal
			Return Instance.GetTaxRate(zipCode)

		End Function
		Public Shared Function GetUSTaxRate(ByVal state As Commerce.Common.USState) As Decimal
			Return Instance.GetTaxRate(state)
		End Function
	End Class
End Namespace
