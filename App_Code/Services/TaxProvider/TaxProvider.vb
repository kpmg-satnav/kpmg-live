#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections
Imports System.Configuration
Imports System.Configuration.Provider
Imports System.Data
Imports System.Web.Caching
Imports System.Web.Configuration

Namespace Commerce.Providers
	Public Class TaxProviderCollection
		Inherits System.Configuration.Provider.ProviderCollection
		Public Shadows ReadOnly Default Property Item(ByVal name As String) As TaxProvider
			Get
				Return CType(MyBase.Item(name), TaxProvider)
			End Get
		End Property

		Public Overrides Sub Add(ByVal provider As ProviderBase)
			If provider Is Nothing Then
				Throw New ArgumentNullException("provider")
			End If

			If Not(TypeOf provider Is TaxProvider) Then
				Throw New ArgumentException ("Invalid provider type", "provider")
			End If

			MyBase.Add(provider)
		End Sub
	End Class
	Public MustInherit Class TaxProvider
		Inherits ProviderBase

		Private serviceLogin_Renamed As String
		''' <summary>
		''' Used for remote service calls
		''' </summary>
		Public Property ServiceLogin() As String
			Get
				Return serviceLogin_Renamed
			End Get
			Set
				serviceLogin_Renamed = Value
			End Set
		End Property
		Private servicePassword_Renamed As String

		''' <summary>
		''' Used for remote service calls
		''' </summary>
		Public Property ServicePassword() As String
			Get
				Return servicePassword_Renamed
			End Get
			Set
				servicePassword_Renamed = Value
			End Set
		End Property

		Private serviceKey_Renamed As String

		Public Property ServiceKey() As String
			Get
				Return serviceKey_Renamed
			End Get
			Set
				serviceKey_Renamed = Value
			End Set
		End Property

		Private Const _providerType As String = "CommerceTaxProvider"

'TODO: INSTANT VB TODO TASK: There is no VB.NET equivalent to 'volatile':
'ORIGINAL LINE: private static volatile TaxProvider _provider = Nothing;
		Private Shared _provider As TaxProvider = Nothing
		Private Shared padLock As Object = New Object()

		#Region "Tax Calls"
		Public MustOverride Function GetTaxRate(ByVal zip As String) As Decimal
		Public MustOverride Function GetTaxRate(ByVal state As Commerce.Common.USState) As Decimal

		''' <summary>
		''' Returns a DataSet containing State, Zip, and Tax Rate info for a 
		''' given state
		''' </summary>
		''' <param name="state"></param>
		''' <returns></returns>
		Public MustOverride Function GetTaxTable(ByVal state As Commerce.Common.USState) As DataSet
		#End Region

		Public Overrides Sub Initialize(ByVal name As String, ByVal config As System.Collections.Specialized.NameValueCollection)
			MyBase.Initialize(name, config)
		End Sub

	End Class
End Namespace
