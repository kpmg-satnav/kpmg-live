#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Specialized
Imports System.Configuration.Provider
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.Data.Common

Namespace Commerce.Providers
	Public Class ZeroTaxRateProvider
		Inherits TaxProvider
		#Region "Provider specific behaviors"
		Public Overrides Sub Initialize(ByVal name As String, ByVal config As NameValueCollection)
			If config Is Nothing Then
				Throw New ArgumentNullException("config")
			End If

			MyBase.Initialize(name, config)


		End Sub
		#End Region

		Public Overrides Function GetTaxRate(ByVal zip As String) As Decimal

			Return 0

		End Function

		Public Overrides Function GetTaxRate(ByVal state As Commerce.Common.USState) As Decimal
			Throw New Exception("The method or operation is not implemented.")
		End Function
		Public Overrides Function GetTaxTable(ByVal state As Commerce.Common.USState) As DataSet
			Throw New Exception("The method or operation is not implemented.")
		End Function
	End Class
End Namespace
