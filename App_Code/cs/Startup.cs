﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.WsFederation;
using Owin;
using System.Web.Routing;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

[assembly: OwinStartup(typeof(adfstest.Startup))]

namespace adfstest
{
    public class ADFSInfo
    {
        public WsFederationAuthenticationOptions FederationAuthenticationOptions { get; set; }
        public String Client { get; set; }
        public String Provider { get; set; }
        public String RedirectUri { get; set; } //when authentication success.
    }

    public class Startup
    {
        List<ADFSInfo> adfsinfolist;

        public Startup()
        {
            try
            {

                adfsinfolist = new List<ADFSInfo>();
                adfsinfolist.Add(new ADFSInfo()
                {
                    FederationAuthenticationOptions = new WsFederationAuthenticationOptions
                    {
                        MetadataAddress = "https://login.adityabirlacapital.com/federationmetadata/2007-06/federationmetadata.xml",
                        AuthenticationType = "Aditya",
                        Caption = "Aditya",
                        BackchannelCertificateValidator = null,
                        //localhost
                        Wreply = "https://live.quickfms.com/Aditya",
                        Wtrealm = "https://live.quickfms.com/Aditya"
                        //Wreply = "https://localhost:44301/Aditya",
                        //Wtrealm = "https://localhost:44301/Aditya"
                    },
                    Client = "Aditya",
                    Provider = "Aditya",
                    RedirectUri = "/login.aspx?tenant=Aditya"
                });
                adfsinfolist.Add(new ADFSInfo()
                {
                    FederationAuthenticationOptions = new WsFederationAuthenticationOptions
                    {
                        MetadataAddress = "https://login.microsoftonline.com/c6c1e9da-5d0c-4f8f-9a02-3c67206efbd6/federationmetadata/2007-06/federationmetadata.xml?appid=98e047af-606e-4409-aee7-27e53797bab1",
                        AuthenticationType = "Tavant",
                        Caption = "Tavant",
                        BackchannelCertificateValidator = null,

                        Wreply = "https://live.quickfms.com/Tavant",
                        Wtrealm = "https://live.quickfms.com/Tavant"

                    },
                    Client = "Tavant",
                    Provider = "Tavant",
                    RedirectUri = "/login.aspx?tenant=Tavant"
                });
                adfsinfolist.Add(new ADFSInfo()
                {
                    FederationAuthenticationOptions = new WsFederationAuthenticationOptions
                    {
                        MetadataAddress = "https://app-eu.onelogin.com/saml/metadata/5a7e6ab5-e815-447c-969f-9240473a055f",
                        AuthenticationType = "Flipkart",
                        Caption = "Flipkart",
                        BackchannelCertificateValidator = null,

                        Wreply = "http://localhost:52205/Flipkart",
                        Wtrealm = "http://localhost:52205/Flipkart"

                    },
                    Client = "Flipkart",
                    Provider = "Flipkart",
                    RedirectUri = "/login.aspx?tenant=Flipkart"
                });
                adfsinfolist.Add(new ADFSInfo()
                {
                    FederationAuthenticationOptions = new WsFederationAuthenticationOptions
                    {
                        MetadataAddress = "https://login.cloudfms.net/FederationMetadata/2007-06/federationMetadata.xml",
                        AuthenticationType = "fms",
                        Caption = "fms",
                        BackchannelCertificateValidator = null,
                        //localhost
                        //Wreply = "https://live.quickfms.com/FMS",
                        //Wtrealm = "https://live.quickfms.com/FMS"
                        Wreply = "https://localhost:44301/FMS",
                        Wtrealm = "https://localhost:44301/FMS"
                    },
                    Client = "fms",
                    Provider = "fms",
                    RedirectUri = "/login.aspx?tenant=fms"
                });
            }

            catch (Exception e)
            {
                ErrorHandler err = new ErrorHandler();
                err._WriteErrorLog("Exception:" + e.Message);
            }

        }

        //public class FakeCertificateValidator : ICertificateValidator
        //{
        //    public bool Validate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        //    {
        //        return true;
        //    }
        //}

        public void Configuration(IAppBuilder app)
        {
            ErrorHandler err = new ErrorHandler();
            try
            {
                //// Enable the application to use a cookie to store information for the signed in user
                //app.UseCookieAuthentication(new CookieAuthenticationOptions
                //{
                //    AuthenticationType = "ExternalCookie",
                //    AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Active,
                //    LoginPath = new PathString("/Login.aspx")
                //});
                //// Use a cookie to temporarily store information about a user logging in with a third party login provider
                ////app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

                //// these two lines of code are needed if you are using any of the external authentication middleware
                //app.Properties["Microsoft.Owin.Security.Constants.DefaultSignInAsAuthenticationType"] = "ExternalCookie";
                //app.UseCookieAuthentication(new CookieAuthenticationOptions
                //{
                //    AuthenticationType = "ExternalCookie",
                //    AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Passive
                //});

                //foreach (ADFSInfo info in adfsinfolist)
                //{
                //    app.UseWsFederationAuthentication(info.FederationAuthenticationOptions);
                //    app.Map("/" + info.Client, map =>
                //        {
                //            map.Run(async ctx =>
                //            {
                //                ctx.Authentication.Challenge(
                //                new AuthenticationProperties
                //                {
                //                    RedirectUri = String.Format("Login{0}.aspx?tenant={0}", info.Client)
                //                },
                //                info.Provider);
                //            });
                //        });
                //    RouteTable.Routes.MapPageRoute("", String.Format("Login{0}.aspx", info.Client), "~/Login.aspx");
                //}

                //app.Map("/logout", map =>
                //{
                //    map.Run(async ctx =>
                //    {
                //        var appTypes = ctx.Authentication.GetAuthenticationTypes().Select(at => at.AuthenticationType).ToArray();
                //        ctx.Authentication.SignOut(appTypes);
                //        ctx.Authentication.SignOut(WsFederationAuthenticationDefaults.AuthenticationType, CookieAuthenticationDefaults.AuthenticationType);
                //        ctx.Response.Redirect("/");
                //    });
                //});
            }
            catch (Exception e)
            {

                err._WriteErrorLog("Exception:" + e.Message);
            }


        }
    }
}

