﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Claims;
using System.Threading;
using System.Security.Principal;
using System.Text;


public static class TokenDataManager
{
    static public TokenData GetTokenObject()
    {
        TokenData td = new TokenData();
        StringBuilder sb = new StringBuilder();
        try
        {
            ClaimsPrincipal claimsprincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
            
            td.IsAuthenticated = claimsprincipal.Identity.IsAuthenticated;
            if (td.IsAuthenticated)
                sb.Append("Aunthenitacted");
            else
                sb.Append("NotAunthenitacted");
           //get claims object

            var Upnid = claimsprincipal.Claims.Where(c => c.Type == ClaimTypes.Upn).Select(c => c.Value).SingleOrDefault();
            sb.Append(Upnid.ToString());

            //td.Upn = claimsprincipal.FindFirst(ClaimTypes.Upn).Value;
            td.NameIdentifier = claimsprincipal.FindFirst(ClaimTypes.NameIdentifier).Value;

            td.Upn = Upnid.Replace("@rcap.co.in", "");

            //sb.Append("Object assigned");
            //sb.Append("Actor - " + claimsprincipal.FindFirst(ClaimTypes.Actor).Value + System.Environment.NewLine);
            //sb.Append("Anonymous - " + claimsprincipal.FindFirst(ClaimTypes.Anonymous).Value + System.Environment.NewLine);
            //sb.Append("Authentication - " + claimsprincipal.FindFirst(ClaimTypes.Authentication).Value + System.Environment.NewLine);
            //sb.Append("AuthenticationInstant - " + claimsprincipal.FindFirst(ClaimTypes.AuthenticationInstant).Value + System.Environment.NewLine);
            //sb.Append("AuthenticationMethod - " + claimsprincipal.FindFirst(ClaimTypes.AuthenticationMethod).Value + System.Environment.NewLine);
            //sb.Append("AuthorizationDecision - " + claimsprincipal.FindFirst(ClaimTypes.AuthorizationDecision).Value + System.Environment.NewLine);
            //sb.Append("CookiePath - " + claimsprincipal.FindFirst(ClaimTypes.CookiePath).Value + System.Environment.NewLine);
            //sb.Append("Country - " + claimsprincipal.FindFirst(ClaimTypes.Country).Value + System.Environment.NewLine);
            //sb.Append("DateOfBirth - " + claimsprincipal.FindFirst(ClaimTypes.DateOfBirth).Value + System.Environment.NewLine);
            //sb.Append("DenyOnlyPrimaryGroupSid - " + claimsprincipal.FindFirst(ClaimTypes.DenyOnlyPrimaryGroupSid).Value + System.Environment.NewLine);
            //sb.Append("DenyOnlyPrimarySid - " + claimsprincipal.FindFirst(ClaimTypes.DenyOnlyPrimarySid).Value + System.Environment.NewLine);
            //sb.Append("DenyOnlySid - " + claimsprincipal.FindFirst(ClaimTypes.DenyOnlySid).Value + System.Environment.NewLine);
            //sb.Append("DenyOnlyWindowsDeviceGroup - " + claimsprincipal.FindFirst(ClaimTypes.DenyOnlyWindowsDeviceGroup).Value + System.Environment.NewLine);
            //sb.Append("Dns - " + claimsprincipal.FindFirst(ClaimTypes.Dns).Value + System.Environment.NewLine);
            //sb.Append("Dsa - " + claimsprincipal.FindFirst(ClaimTypes.Dsa).Value + System.Environment.NewLine);
            //sb.Append("Email - " + claimsprincipal.FindFirst(ClaimTypes.Email).Value + System.Environment.NewLine);
            //sb.Append("Expiration - " + claimsprincipal.FindFirst(ClaimTypes.Expiration).Value + System.Environment.NewLine);
            //sb.Append("Expired - " + claimsprincipal.FindFirst(ClaimTypes.Expired).Value + System.Environment.NewLine);
            //sb.Append("Gender - " + claimsprincipal.FindFirst(ClaimTypes.Gender).Value + System.Environment.NewLine);
            //sb.Append("GivenName  - " + claimsprincipal.FindFirst(ClaimTypes.GivenName).Value + System.Environment.NewLine);
            //sb.Append("GroupSid - " + claimsprincipal.FindFirst(ClaimTypes.GroupSid).Value + System.Environment.NewLine);
            //sb.Append("Hash - " + claimsprincipal.FindFirst(ClaimTypes.Hash).Value + System.Environment.NewLine);
            //sb.Append("HomePhone - " + claimsprincipal.FindFirst(ClaimTypes.HomePhone).Value + System.Environment.NewLine);
            //sb.Append("IsPersistent - " + claimsprincipal.FindFirst(ClaimTypes.IsPersistent).Value + System.Environment.NewLine);
            //sb.Append("Locality - " + claimsprincipal.FindFirst(ClaimTypes.Locality).Value + System.Environment.NewLine);
            //sb.Append("MobilePhone - " + claimsprincipal.FindFirst(ClaimTypes.MobilePhone).Value + System.Environment.NewLine);
            //sb.Append("Name - " + claimsprincipal.FindFirst(ClaimTypes.Name).Value + System.Environment.NewLine);
            //sb.Append("NameIdentifier - " + claimsprincipal.FindFirst(ClaimTypes.NameIdentifier).Value + System.Environment.NewLine);
            //sb.Append("OtherPhone - " + claimsprincipal.FindFirst(ClaimTypes.OtherPhone).Value + System.Environment.NewLine);
            //sb.Append("WindowsAccountName - " + claimsprincipal.FindFirst(ClaimTypes.WindowsAccountName).Value + System.Environment.NewLine);
            //sb.Append("X500DistinguishedName - " + claimsprincipal.FindFirst(ClaimTypes.X500DistinguishedName).Value + System.Environment.NewLine);
            //sb.Append("WindowsUserClaim - " + claimsprincipal.FindFirst(ClaimTypes.WindowsUserClaim).Value + System.Environment.NewLine);
            //sb.Append("Webpage - " + claimsprincipal.FindFirst(ClaimTypes.Webpage).Value + System.Environment.NewLine);
            //sb.Append("UserData - " + claimsprincipal.FindFirst(ClaimTypes.UserData).Value + System.Environment.NewLine);
            //sb.Append("Uri - " + claimsprincipal.FindFirst(ClaimTypes.Uri).Value + System.Environment.NewLine);

            ErrorHandler errh = new ErrorHandler();
            errh._WriteErrorLog(td.NameIdentifier.ToString());
            errh._WriteErrorLog(sb.ToString());
            return td;
        }
        catch (Exception ex)
        {
            ErrorHandler errh = new ErrorHandler();
            errh._WriteErrorLog(sb.ToString());
            errh._WriteErrorLog(ex.ToString());
            return td;
        }
    }

    static public List<Claim> GetClaimsList()
    {
        ClaimsPrincipal claimsprincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
        return claimsprincipal.Claims.ToList();
    }
}
