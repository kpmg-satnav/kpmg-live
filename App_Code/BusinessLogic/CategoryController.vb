#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common
Imports System.Collections.Generic
Imports System.Data.Common
Imports SubSonic

''' <summary>
'''  Business logic for categories
''' </summary>
Public Class CategoryController

  Private Shared catList As CategoryCollection

  ''' <summary>
  ''' This is the static list for the site's categories. This is held in application
  ''' memory and used as needed so the site doesn't need to hit the DB everytime. Will
  ''' need to opt out of this if you use a server farm or cluster.
  ''' </summary>
  Private Sub New()
  End Sub
  Public Shared Property CategoryList() As CategoryCollection
	Get
	  If catList Is Nothing OrElse catList.Count = 0 Then
		CategoryController.Load()
	  End If
	  Return catList
	End Get
	Set
	  catList = Value
	End Set
  End Property

  ''' <summary>
  ''' Loads the category info into the static CategoryList
  ''' </summary>
  Public Shared Sub Load()
	catList = New CategoryCollection()
	Dim rdr As IDataReader = Category.FetchAll(OrderBy.Asc("listOrder"))
	catList.Load(rdr)
	rdr.Close()
  End Sub

  ''' <summary>
  ''' Find a category in the CategoryList
  ''' </summary>
  ''' <param name="categoryID"></param>
  ''' <returns></returns>
  Public Shared Function Find(ByVal categoryID As Integer) As Category
	Dim cOut As Category = Nothing
	For Each cat As Category In catList
	  If cat.CategoryID = categoryID Then
		cOut = cat
		Exit For
	  End If
	Next cat
	Return cOut
  End Function

  ''' <summary>
  ''' Finds a category by name
  ''' </summary>
  ''' <param name="categoryName"></param>
  ''' <returns></returns>
  Public Shared Function FindByName(ByVal categoryName As String) As Category
	Dim cOut As Category = Nothing
	For Each cat As Category In catList
	  If cat.CategoryName.ToLower().Equals(categoryName) Then
		cOut = cat
		Exit For
	  End If
	Next cat
	Return cOut
  End Function

  ''' <summary>
  ''' Returns all categories in a dataset; used by the
  ''' menuing controls
  ''' </summary>
  ''' <returns></returns>
  Public Shared Function GetDataSetList() As DataSet
	Dim q As Query = New Query(Category.GetTableSchema())
	Return q.ExecuteDataSet()
  End Function

  ''' <summary>
  ''' Returns all categories for a given product
  ''' </summary>
  ''' <param name="productID"></param>
  ''' <returns>CategoryCollection</returns>
  Public Shared Function GetByProductID(ByVal productID As Integer) As CategoryCollection
	Dim rdr As IDataReader = SPs.StoreCategoryGetByProductID(productID).GetReader()
	Dim list As CategoryCollection = New CategoryCollection()
	list.Load(rdr)
	rdr.Close()
	Return list
  End Function

  ''' <summary>
  ''' Returns a multiple-result set for populating the Category.aspx page. Includes
  ''' category info, products, etc.
  ''' </summary>
  ''' <param name="categoryGUID"></param>
  ''' <returns></returns>
  Public Shared Function GetPageByName(ByVal categoryName As String) As DataSet
	Return SPs.StoreCategoryGetPageByNameMulti(categoryName).GetDataSet()

  End Function
  ''' <summary>
  ''' Returns a multiple-result set for populating the Category.aspx page. Includes
  ''' category info, products, etc.
  ''' </summary>
  ''' <param name="categoryGUID"></param>
  ''' <returns></returns>
  Public Shared Function GetPageByID(ByVal categoryID As Integer) As DataSet

	Return SPs.StoreCategoryGetPageMulti(categoryID).GetDataSet()

  End Function
  ''' <summary>
  ''' Returns a multiple-result set for populating the Category.aspx page. Includes
  ''' category info, products, etc.
  ''' </summary>
  ''' <param name="categoryGUID"></param>
  ''' <returns></returns>
    Public Shared Function GetPageByGUID(ByVal categoryGUID As String) As DataSet

        Return SPs.StoreCategoryGetPageByGUIDMulti(categoryGUID).GetDataSet()
    End Function

    Public Shared Function GetAllParentCategories() As DataSet

        '     Return SPs.StoreCategoryGetAllParent
        Dim q As Query = New Query(Category.GetTableSchema())
        q.AddWhere("parentID", 0)
        Return q.ExecuteDataSet()
    End Function

    Public Shared Function GetSubCategoriesById(ByVal IdCategory As Integer) As CategoryCollection
        Dim rdr As IDataReader = SPs.GetSubCategoriesByCategoryId(IdCategory).GetReader()
        Dim list As CategoryCollection = New CategoryCollection()
        list.Load(rdr)
        rdr.Close()
        Return list
    End Function

    ''''Get Category By CategoryId
    'Public Shared Function GetCategoryById(ByVal IdCategory As Integer) As CategoryCollection
    '    Dim rdr As IDataReader = SPs.GetCategoryByCategoryId(IdCategory).GetReader()
    '    Dim list As CategoryCollection = New CategoryCollection()
    '    list.Load(rdr)
    '    rdr.Close()
    '    Return list
    'End Function

    Public Shared Function GetshortDescription(ByVal shortDescription As String) As DataSet
        shortDescription = shortDescription.Replace("_", " ")
        Return SPs.spGetshortDescription(shortDescription).GetDataSet
    End Function
    Public Shared Function GetGuidCategoryName(ByVal CatName As String) As DataSet
        CatName = CatName.Replace("_", " ")
        Return SPs.spGetGuidCategoryName(CatName).GetDataSet
    End Function
    Public Shared Function GetCategoryById(ByVal IdCategory As String) As CategoryCollection
        Dim rdr As IDataReader = SPs.GetCategoryByCategoryId(IdCategory).GetReader()
        Dim list As CategoryCollection = New CategoryCollection()
        list.Load(rdr)
        rdr.Close()
        Return list
    End Function
    Public Shared Function Categoroies_GetCustomisedByCatId(ByVal CatId As Integer) As Data.SqlClient.SqlDataReader
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "Categoroies_GetCustomisedByCatId")
        sp.Command.AddParameter("@CatId", CatId, DbType.Int32)

        Return sp.GetReader()
    End Function
End Class
