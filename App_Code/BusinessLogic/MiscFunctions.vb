Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.ApplicationException
Imports System.Net.Mail
Imports System.IO.Directory
Imports System.Data
Imports System.IO



Imports System.Net
Imports System.Web.Configuration
Imports System.Web.Mail
Imports System.Web
Imports System

Public Class MiscFunctions
    'Public Sub SendMail(ByVal from As String, ByVal ToMail As String, ByVal CC As String, ByVal BCC As String, ByVal Body As String, ByVal Subject As String, ByVal Host As String)
    '    Try
    '        Dim objmessage As New System.Net.Mail.MailMessage(from, ToMail, Subject, Body)
    '        Dim objsmtp As New System.Net.Mail.SmtpClient
    '        objmessage.Bcc.Add(BCC)
    '        objmessage.IsBodyHtml = True
    '        objmessage.Priority = MailPriority.Normal
    '        objsmtp.Host = Host
    '        objsmtp.Send(objmessage)
    '    Catch ex As Exception
    '        'HttpContext.Current.Response.Write(ex.Message)
    '    Finally
    '    End Try
    'End Sub

    Public Function SendMail(ByVal FromAddress As String, ByVal ToAddress As String, ByVal CCAddress As String, ByVal BCC As String, ByVal Body As String, ByVal Subject As String, ByVal Host As String) As String
        SendMail = String.Empty
        Dim config As System.Configuration.Configuration = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath)
        Dim settings As System.Net.Configuration.MailSettingsSectionGroup = _
            CType(config.GetSectionGroup("system.net/mailSettings"),  _
            System.Net.Configuration.MailSettingsSectionGroup)
        'Obtain the Network Credentials from the mailSettings section
        Dim credential As New System.Net.NetworkCredential( _
            settings.Smtp.Network.UserName, settings.Smtp.Network.Password)
        'Create the SMTP Client
        Dim client As New SmtpClient()
        client.Host = settings.Smtp.Network.Host
        client.Credentials = credential
        'client.EnableSsl = True
        'Build Email Message
        Dim email As New Net.Mail.MailMessage
        email.From = New MailAddress(FromAddress)
        email.To.Add(ToAddress)
        email.CC.Clear()
        If Not String.IsNullOrEmpty(CCAddress) Then
            email.CC.Add(CCAddress)
        End If
        email.Subject = Subject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = Body
        Try
            'Send Email
            client.Send(email)
            SendMail = "Your message has been successfully Sent, Thank you for contacting us!!"
        Catch ex As Exception
            SendMail = "Failed to send Mail" & "<br>" & ex.Message & "<br />" & ex.StackTrace
        Finally
            email = Nothing
            client = Nothing
        End Try
        Return SendMail
    End Function
End Class
