Imports Microsoft.VisualBasic
Imports Amantra.VerticalDTON
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Data
Imports Amantra.SmartReader
Imports System

Namespace Amantra.VerticalDALN

    Public Class VerticalDAL
#Region "BLL and DTO Instance declarations"

        Private m_Classname As String = "VerticalDAL"
        Private vDTO As New VerticalDTO()
        Private insertData As Integer = 0
        Private verId As Integer = 0

#End Region

#Region "Private Variable Declaration"
        Private updatedata As Integer = 0
        Private verticalId As Integer = 0

#End Region

#Region "Get Requistion Ids"
        Public Function GetRequisitionIDs(ByVal Sta_id As Integer) As IList(Of VerticalDTO)

            Dim m_Methodname As String = "In GetRequisitionData Method"
            Dim verticalDetails As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@sta_id", SqlDbType.NVarChar, 50)}

            parms(0).Value = Sta_id
            Try
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_AmantraAxis_getVerReqIDs", parms)
                Dim smartReader As New SmartDataReader(dr)
                Dim verticalData As VerticalDTO = Nothing
                While smartReader.Read()
                    verticalData = New VerticalDTO(smartReader.GetString("svr_req_id"))
                    verticalDetails.Add(verticalData)
                End While
                dr.Close()
            Catch ex As System.Exception
                'Throw New Amantra.Exception.DataException("Error in clsTMS DAL", m_Classname, m_Methodname, ex)
            End Try
            Return verticalDetails
        End Function
#End Region
#Region "Get Requistion Ids"
        Public Function GetRequisitionIDsForVertical() As Data.DataTable

            Dim m_Methodname As String = "In GetRequisitionData Method"
            Dim dt As New DataTable
            Try
                dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_AmantraAxis_getVerReqIDsForVertical")
                Return dt
            Catch ex As System.Exception
                'Throw New Amantra.Exception.DataException("ERROR", m_Classname, m_Methodname, ex)
            End Try
            Return dt
        End Function
#End Region
#Region "Get dates for Requistion Ids"
        Public Function GetdatesforReqIDs(ByVal req_id As String) As IList(Of VerticalDTO)

            Dim m_Methodname As String = "In GetDatesRequisitionData Method"
            Dim verticalDetails As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@req_id", SqlDbType.NVarChar, 50)}

            parms(0).Value = req_id
            Try
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_getDates", parms)
                Dim smartReader As New SmartDataReader(dr)
                Dim verticalData As VerticalDTO = Nothing
                While smartReader.Read()
                    verticalData = New VerticalDTO(smartReader.GetString("mon_name"), smartReader.GetString("svd_mon"), smartReader.GetString("svd_year"))
                    verticalDetails.Add(verticalData)
                End While
                dr.Close()
            Catch ex As System.Exception
                'Throw New Amantra.Exception.DataException("Error in clsTMS DAL", m_Classname, m_Methodname, ex)
            End Try
            Return verticalDetails
        End Function
#End Region




#Region "Get Requisition Id's for Selected Verticals"
        Public Function GetSelectedVerticalReqIDs(ByVal vert_id As String) As IList(Of VerticalDTO)

            Dim m_Methodname As String = "In GetSelectedVerticalReqIDs Method"
            Dim verticalDetails As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@vc_vertical", SqlDbType.VarChar, 4000)}

            parms(0).Value = vert_id
            Try
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_AmantraAxis_get_VerReqIDs_selected", parms)
                Dim smartReader As New SmartDataReader(dr)
                Dim verticalData As VerticalDTO = Nothing
                While smartReader.Read()
                    verticalData = New VerticalDTO(smartReader.GetString("RequestID"))
                    verticalDetails.Add(verticalData)
                End While
                dr.Close()
            Catch ex As System.Exception
                'Throw New Amantra.Exception.DataException("Error in clsTMS DAL", m_Classname, m_Methodname, ex)
            End Try
            Return verticalDetails
        End Function
#End Region



#Region "Get dates for Requistion Ids"
        Public Function GetYears(ByVal req_id As String, ByVal mon As String) As DataTable
            Dim parms As SqlParameter() = {New SqlParameter("@req_id", SqlDbType.NVarChar, 50), New SqlParameter("@mon", SqlDbType.NVarChar, 50)}
            parms(0).Value = req_id
            parms(1).Value = mon.Trim()
            Dim dt As DataTable = New DataTable
            Try
                dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "usp_getYear", parms)
                Return dt
            Catch ex As System.Exception
                'Throw New Amantra.Exception.DataException("Error in clsTMS DAL", m_Classname, m_Methodname, ex)
            End Try
            Return dt
        End Function
#End Region

#Region "Get data for Requistion Ids"
        Public Function GetDataforReqIDs(ByVal req_id As String, ByVal month As DateTime, ByVal year As DateTime) As IList(Of VerticalDTO)

            Dim m_Methodname As String = "In GetDateRequisitionData Method"
            Dim verticalDetails As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@req_id", SqlDbType.NVarChar, 50), _
                                           New SqlParameter("@month", SqlDbType.DateTime), _
                                           New SqlParameter("@year", SqlDbType.DateTime)}

            parms(0).Value = req_id
            parms(1).Value = month
            parms(2).Value = year
            Try
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_get_vertical_details", parms)
                Dim smartReader As New SmartDataReader(dr)
                Dim verticalData As VerticalDTO = Nothing
                While smartReader.Read()
                    verticalData = New VerticalDTO(smartReader.GetString("svr_rem"), smartReader.GetString("req_name"), smartReader.GetInt32("svd_wstno"), smartReader.GetInt32("svd_hcno"), smartReader.GetInt32("svd_fcno"), smartReader.GetString("svr_loc_id"), smartReader.GetString("lcm_name"), smartReader.GetString("svr_vertical"), smartReader.GetString("svr_req_date"), smartReader.GetString("SVR_LABSPACE"), smartReader.GetString("cty_name"), _
                             smartReader.GetString("FromDate"), smartReader.GetString("ToDate"))
                    verticalDetails.Add(verticalData)
                End While
                dr.Close()
            Catch ex As System.Exception
                'Throw New Amantra.Exception.DataException("Error in clsTMS DAL", m_Classname, m_Methodname, ex)
            End Try
            Return verticalDetails
        End Function
#End Region

#Region "Get data for Towers"
        Public Function GetDataforTowers(ByVal loc_id As String) As IList(Of VerticalDTO)

            Dim m_Methodname As String = "In GetDataforTowers Method"
            Dim verticalDetails As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@loc_id", SqlDbType.NVarChar, 50)}

            parms(0).Value = loc_id

            Try
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "USP_getTowers", parms)
                Dim smartReader As New SmartDataReader(dr)
                Dim verticalData As VerticalDTO = Nothing
                While smartReader.Read()
                    verticalData = New VerticalDTO(smartReader.GetString("twr_name"), smartReader.GetString("twr_code"), 1)
                    verticalDetails.Add(verticalData)
                End While
                dr.Close()
            Catch ex As System.Exception
                'Throw New Amantra.Exception.DataException("ERROR", m_Classname, m_Methodname, ex)
            End Try
            Return verticalDetails
        End Function
#End Region

#Region "Get data for Floors"
        Public Function GetDataforFloors(ByVal twr_id As String, ByVal loc_id As String) As IList(Of VerticalDTO)

            Dim m_Methodname As String = "In GetDataforFloors Method"
            Dim verticalDetails As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@twr_id", SqlDbType.NVarChar, 50), New SqlParameter("@flr_loc_id", SqlDbType.NVarChar, 50)}

            parms(0).Value = twr_id
            parms(1).Value = loc_id

            Try
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "USP_getFloors", parms)
                Dim dt As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_getFloors", parms)
                Dim smartReader As New SmartDataReader(dr)
                Dim verticalData As VerticalDTO = Nothing
                While smartReader.Read()
                    verticalData = New VerticalDTO(smartReader.GetString("flr_name"), smartReader.GetString("flr_code"), 2)
                    verticalDetails.Add(verticalData)
                End While
                dr.Close()
            Catch ex As System.Exception
                'Throw New Amantra.Exception.DataException("Error", m_Classname, m_Methodname, ex)
            End Try
            Return verticalDetails
        End Function
#End Region

#Region "Get data for Wings"
        Public Function GetDataforWings(ByVal flr_id As String, ByVal twr_id As String, ByVal loc_id As String) As IList(Of VerticalDTO)

            Dim m_Methodname As String = "In GetDataforWings Method"
            Dim verticalDetails As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@flr_id", SqlDbType.NVarChar, 50), New SqlParameter("@WNG_TWR_ID", SqlDbType.NVarChar, 50), New SqlParameter("@WNG_LCN_ID", SqlDbType.NVarChar, 50)}

            parms(0).Value = flr_id
            parms(1).Value = twr_id
            parms(2).Value = loc_id

            Try
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "USP_getWings", parms)
                Dim smartReader As New SmartDataReader(dr)
                Dim verticalData As VerticalDTO = Nothing
                While smartReader.Read()
                    verticalData = New VerticalDTO(smartReader.GetString("wng_name"), smartReader.GetString("wng_code"), 3)
                    verticalDetails.Add(verticalData)
                End While
                dr.Close()
            Catch ex As System.Exception
                'Throw New Amantra.Exception.DataException("Error in clsTMS DAL", m_Classname, m_Methodname, ex)
            End Try
            Return verticalDetails
        End Function
#End Region

#Region "Get data for SpaceIDs"
        Public Function GetDataforSpaceIDs(ByVal fromdate As DateTime, ByVal todate As DateTime, ByVal spc_type As String, ByVal twr_id As String, ByVal flr_id As String, ByVal wng_id As String, ByVal loc_id As String) As IList(Of VerticalDTO)

            Dim m_Methodname As String = "In GetDataforSpaceIDS Method"
            Dim verticalDetails As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@frmdate", SqlDbType.DateTime), _
                                           New SqlParameter("@todate", SqlDbType.DateTime), _
                                           New SqlParameter("@spc_type", SqlDbType.NVarChar, 50), _
                                           New SqlParameter("@twr_code", SqlDbType.NVarChar, 50), _
                                           New SqlParameter("@flr_code", SqlDbType.NVarChar, 50), _
                                           New SqlParameter("@wng_code", SqlDbType.NVarChar, 50), _
                                           New SqlParameter("@loc_code", SqlDbType.NVarChar, 50)}

            parms(0).Value = fromdate
            parms(1).Value = todate
            parms(2).Value = spc_type
            parms(3).Value = twr_id
            parms(4).Value = flr_id
            parms(5).Value = wng_id
            parms(6).Value = loc_id


            Try
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "USP_vacant_space", parms)
                Dim smartReader As New SmartDataReader(dr)
                Dim verticalData As VerticalDTO = Nothing
                While smartReader.Read()
                    verticalData = New VerticalDTO(smartReader.GetString("spc_id"), 1)
                    verticalDetails.Add(verticalData)
                End While
                dr.Close()
            Catch ex As System.Exception
                'Throw New Amantra.Exception.DataException("Error in clsTMS DAL", m_Classname, m_Methodname, ex)
            End Try
            Return verticalDetails
        End Function
#End Region

#Region " insert Space details"
        Public Function insertSpaceID(ByVal vdto As VerticalDTO, ByVal strVertical As String) As Integer

            Dim m_Methodname As String = "insertSpace"
            Dim verticalList As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@status", SqlDbType.Int), New SqlParameter("@req_id", SqlDbType.NVarChar, 50), New SqlParameter("@bdg_id", SqlDbType.NVarChar, 50), New SqlParameter("@twr_id", SqlDbType.NVarChar, 50), New SqlParameter("@flr_id", SqlDbType.NVarChar, 50), New SqlParameter("@wng_id", SqlDbType.NVarChar, 50), New SqlParameter("@spc_id", SqlDbType.NVarChar, 50), New SqlParameter("@user_id", SqlDbType.NVarChar, 15), New SqlParameter("@sta_id", SqlDbType.Int), New SqlParameter("@frmdate", SqlDbType.DateTime), New SqlParameter("@todate", SqlDbType.DateTime), New SqlParameter("@spc_type", SqlDbType.NVarChar, 50), New SqlParameter("@SSA_VERTICAL", SqlDbType.NVarChar, 50)}

            parms(0).Direction = ParameterDirection.Output
            parms(1).Value = vdto.RequestID
            parms(2).Value = vdto.LocationCode
            parms(3).Value = vdto.Towercode
            parms(4).Value = vdto.FloorCode
            parms(5).Value = vdto.WingCode
            parms(6).Value = vdto.SpaceID
            parms(7).Value = vdto.UserID
            parms(8).Value = vdto.Status
            parms(9).Value = vdto.FromDate
            parms(10).Value = vdto.ToDate
            parms(11).Value = vdto.SpaceType
            parms(12).Value = strVertical
            Try
                insertData = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_Space_insert", parms)
                'insertdata = SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionString, CommandType.StoredProcedure, " usp_AmantraAxis_Country_InsertCountry", parms)
                If insertData <> -1 Then
                    If Convert.ToString(parms(0).Value) = String.Empty Then
                        'If TravelId is Null then TravelId returns 0 
                        verId = 0
                    Else
                        ' If TravelId is Not Null then TravelId returns Value 
                        verId = Convert.ToInt32(parms(0).Value)

                    End If
                End If
            Catch ex As System.Exception
                ' Throw New Amantra.Exception.DataException("Error in DAL", m_Classname, m_Methodname, ex)
            End Try
            Return insertData
        End Function
        '' ----------------------------------------------------------------------------------------------- 
#End Region

#Region " insert Child Vertical Alloc details"
        Public Function insertVerticalAllocDtls(ByVal vdto As VerticalDTO) As Integer

            Dim m_Methodname As String = "insertvertical"
            Dim verticalList As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@req_id", SqlDbType.NVarChar, 50), _
              New SqlParameter("@wst_num", SqlDbType.NVarChar, 25), _
              New SqlParameter("@full_cabin", SqlDbType.NVarChar, 25), _
              New SqlParameter("@half_cabin", SqlDbType.NVarChar, 25), _
              New SqlParameter("@wst_alloc", SqlDbType.NVarChar, 10), _
              New SqlParameter("@full_cabin_alloc", SqlDbType.NVarChar, 10), _
              New SqlParameter("@half_cabin_alloc", SqlDbType.NVarChar, 10), _
              New SqlParameter("@USER_ID", SqlDbType.NVarChar, 10), _
              New SqlParameter("@remarks", SqlDbType.NVarChar, 500), _
              New SqlParameter("@month", SqlDbType.DateTime), _
              New SqlParameter("@year", SqlDbType.DateTime)}

            'New SqlParameter("@month", SqlDbType.NVarChar, 10), _
            'New SqlParameter("@year", SqlDbType.NVarChar, 10)}


            parms(0).Value = vdto.RequestID
            parms(1).Value = vdto.WorkStations
            parms(2).Value = vdto.FullCabins
            parms(3).Value = vdto.HalfCabins
            parms(4).Value = vdto.WorkStationsAllocated
            parms(5).Value = vdto.FullCabinsAllocated
            parms(6).Value = vdto.HalfCabinsAllocated
            parms(7).Value = vdto.UserID
            parms(8).Value = vdto.Remarks
            parms(9).Value = vdto.SvrFromDate
            parms(10).Value = vdto.SvrToDate


            Try
                insertData = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_INSERT_VERALLOC_DTLS", parms)
                'insertdata = SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionString, CommandType.StoredProcedure, " usp_AmantraAxis_Country_InsertCountry", parms)
                If insertData <> -1 Then
                    'If Convert.ToString(parms(0).Value) = String.Empty Then
                    '    'If TravelId is Null then TravelId returns 0 
                    '    verId = 0
                    'Else
                    '    ' If TravelId is Not Null then TravelId returns Value 
                    '    verId = Convert.ToInt32(parms(0).Value)

                    'End If
                End If
            Catch ex As System.Exception
                ' Throw New Amantra.Exception.DataException("Error in DAL", m_Classname, m_Methodname, ex)
            End Try
            Return insertData
        End Function
        '' ----------------------------------------------------------------------------------------------- 
#End Region
#Region " insert Vertical Alloc details"
        Public Function insertVerticalAlloc(ByVal vdto As VerticalDTO) As Integer

            Dim m_Methodname As String = "insertvertical"
            Dim verticalList As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@status", SqlDbType.Int), _
              New SqlParameter("@req_id", SqlDbType.NVarChar, 50), _
              New SqlParameter("@bdg_id", SqlDbType.NVarChar, 25), _
              New SqlParameter("@work_station", SqlDbType.NVarChar, 25), _
              New SqlParameter("@full_cabin", SqlDbType.NVarChar, 25), _
              New SqlParameter("@half_cabin", SqlDbType.NVarChar, 25), _
              New SqlParameter("@flr_id", SqlDbType.NVarChar, 25), _
              New SqlParameter("@twr_id", SqlDbType.NVarChar, 25), _
              New SqlParameter("@wng_id", SqlDbType.NVarChar), _
              New SqlParameter("@remarks", SqlDbType.NVarChar, 500), _
              New SqlParameter("@wst_alloc", SqlDbType.NVarChar, 10), _
              New SqlParameter("@full_cabin_alloc", SqlDbType.NVarChar, 10), _
              New SqlParameter("@half_cabin_alloc", SqlDbType.NVarChar, 10), _
              New SqlParameter("@month", SqlDbType.DateTime), _
              New SqlParameter("@year", SqlDbType.DateTime)}
            'New SqlParameter("@month", SqlDbType.NVarChar, 10), _
            'New SqlParameter("@year", SqlDbType.NVarChar, 10)}

            parms(0).Direction = ParameterDirection.Output
            parms(1).Value = vdto.RequestID
            parms(2).Value = String.Empty
            parms(3).Value = vdto.WorkStations
            parms(4).Value = vdto.FullCabins
            parms(5).Value = vdto.HalfCabins
            parms(6).Value = vdto.FloorCode
            parms(7).Value = vdto.Towercode
            parms(8).Value = vdto.WingCode
            parms(9).Value = vdto.Remarks
            parms(10).Value = vdto.WorkStationsAllocated
            parms(11).Value = vdto.FullCabinsAllocated
            parms(12).Value = vdto.HalfCabinsAllocated
            parms(13).Value = vdto.SvrFromDate
            parms(14).Value = vdto.SvrToDate


            Try
                insertData = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_VerAlloc_Insert", parms)
                'insertdata = SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionString, CommandType.StoredProcedure, " usp_AmantraAxis_Country_InsertCountry", parms)
                If insertData <> -1 Then
                    If Convert.ToString(parms(0).Value) = String.Empty Then
                        'If TravelId is Null then TravelId returns 0 
                        verId = 0
                    Else
                        ' If TravelId is Not Null then TravelId returns Value 
                        verId = Convert.ToInt32(parms(0).Value)

                    End If
                End If
            Catch ex As System.Exception
                ' Throw New Amantra.Exception.DataException("Error in DAL", m_Classname, m_Methodname, ex)
            End Try
            Return insertData
        End Function
        '' ----------------------------------------------------------------------------------------------- 
#End Region

#Region "Get partial allocated verticals for Requistion Ids"
        Public Function GetPartialAllocverticalsbyReqIDs(ByVal req_id As String, ByVal month As DateTime, ByVal year As DateTime) As IList(Of VerticalDTO)

            Dim m_Methodname As String = "In GetRequisitionData Method"
            Dim verticalDetails As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@req_id", SqlDbType.NVarChar, 50), _
                                        New SqlParameter("@month", SqlDbType.DateTime), _
                                        New SqlParameter("@year", SqlDbType.DateTime)}

            parms(0).Value = req_id
            parms(1).Value = month
            parms(2).Value = year

            Try
                Dim dr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_VerAlloc_partial", parms)
                Dim smartReader As New SmartDataReader(dr)
                Dim verticalData As VerticalDTO = Nothing
                While smartReader.Read()
                    verticalData = New VerticalDTO(smartReader.GetString("wst_alloc"), smartReader.GetString("full_alloc"), smartReader.GetString("half_alloc"), 1)
                    verticalDetails.Add(verticalData)
                End While
                dr.Close()
            Catch ex As System.Exception
                'Throw New Amantra.Exception.DataException("Error in clsTMS DAL", m_Classname, m_Methodname, ex)
            End Try
            Return verticalDetails
        End Function
#End Region

#Region " Update Vertical Status"
        Public Function UpdateVertStatus(ByVal req_id As String, ByVal sta_id As Integer) As Integer
            Dim m_Methodname As String = "insertvertical"
            Dim verticalList As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            Dim parms As SqlParameter() = {New SqlParameter("@sta_id", SqlDbType.Int), _
              New SqlParameter("@req_id", SqlDbType.NVarChar, 50)}

            parms(0).Value = sta_id
            parms(1).Value = req_id

            Try
                updatedata = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_update_VerReq", parms)
                'insertdata = SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionString, CommandType.StoredProcedure, " usp_AmantraAxis_Country_InsertCountry", parms)
                If updatedata <> -1 Then
                    verId = 1
                Else
                    verId = -1
                End If
            Catch ex As System.Exception
                ' Throw New Amantra.Exception.DataException("Error in DAL", m_Classname, m_Methodname, ex)
            End Try
            Return updatedata
        End Function
        '' ----------------------------------------------------------------------------------------------- 
#End Region
    End Class

End Namespace
