#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports SubSonic

Namespace Commerce.Common
	''' <summary>
	''' This is an empty partial construct for you to extend the base class as needed, without
	''' needing to inheret from it. 
	''' </summary>
	Public Partial Class ProductCollection
		Inherits ActiveList(Of Product)

	End Class

	''' <summary>
	''' A Persistable class that uses Generics to store it's state
	''' in the database. This class maps to the CSK_Store_Product table.
	''' </summary>
	Public Partial Class Product
		Inherits ActiveRecord(Of Product)

		#Region "Custom Constructors"
		Public Sub New(ByVal sku As String)

			SetSQLProps()
			LoadByParam("sku", sku)

		End Sub

		#End Region

		#Region "Custom"

		'enum
		Public Property Status() As ProductStatus
			Get
				Return CType(Me.GetColumnValue("statusID"), ProductStatus)
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("statusID", Value)
			End Set
		End Property

		'enum
		Public Property ProductType() As ProductType
			Get
				Return CType(Me.GetColumnValue("productTypeID"), ProductType)
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("productTypeID", Value)
			End Set
		End Property

		'enum
		Public Property ShippingType() As ShippingType
			Get
				Return CType(Me.GetColumnValue("shippingTypeID"), ShippingType)
			End Get
			Set
				Me.MarkDirty()
				Me.SetColumnValue("shippingTypeID", Value)
			End Set
		End Property
		'holds the XML and is responsible for putting/pulling from DB
		'XML attribute listing
		Private attributes_Renamed As Attributes
		Public Property Attributes() As Attributes
			Get
				attributes_Renamed = CType(Utility.XmlToObject(GetType(Attributes), Me.AttributeXML), Attributes)
				Return attributes_Renamed
			End Get
			Set
				attributes_Renamed = Value
				Me.AttributeXML = Utility.ObjectToXML(GetType(Attributes), attributes_Renamed)
			End Set
		End Property
		Private selectedAttributes_Renamed As Attributes
		Public Property SelectedAttributes() As Attributes
			Get
				Return selectedAttributes_Renamed
			End Get
			Set
				selectedAttributes_Renamed = Value
			End Set
		End Property

		'images
		Private images_Renamed As ImageCollection

		Public Property Images() As ImageCollection
			Get
				Return images_Renamed
			End Get
			Set
				images_Renamed = Value
			End Set
		End Property

		'reviews
		Private reviews_Renamed As ProductReviewCollection

		Public Property Reviews() As ProductReviewCollection
			Get
				Return reviews_Renamed
			End Get
			Set
				reviews_Renamed = Value
			End Set
		End Property

		'descriptors
		Private descriptors_Renamed As ProductDescriptorCollection

		Public Property Descriptors() As ProductDescriptorCollection
			Get
				Return descriptors_Renamed
			End Get
			Set
				descriptors_Renamed = Value
			End Set
		End Property



		Private rating_Renamed As Decimal

		Public ReadOnly Property Rating() As Decimal
			Get

				If Me.TotalRatingVotes > 0 Then
					rating_Renamed = Me.RatingSum / TotalRatingVotes
				Else
					rating_Renamed = 4
				End If
				Return rating_Renamed

			End Get

		End Property

		Private shippingEstimate_Renamed As String

		Public Property ShippingEstimate() As String
			Get
				Return shippingEstimate_Renamed
			End Get
			Set
				shippingEstimate_Renamed = Value
			End Set
		End Property

		Private imageFile_Renamed As String

		Public Property ImageFile() As String
			Get
				Return imageFile_Renamed
			End Get
			Set
				imageFile_Renamed = Value
			End Set

		End Property

		Private quantity_Renamed As Integer

		Public Property Quantity() As Integer
			Get
				Return quantity_Renamed
			End Get
			Set
				quantity_Renamed = Value
			End Set
		End Property

		Private discountAmount_Renamed As Decimal

		Public Property DiscountAmount() As Decimal
			Get
				Return discountAmount_Renamed
			End Get
			Set
				discountAmount_Renamed = Value
			End Set
		End Property

		Private promoCode_Renamed As String

		Public Property PromoCode() As String
			Get
				Return promoCode_Renamed
			End Get
			Set
				promoCode_Renamed = Value
			End Set
		End Property
		Private _youSavePercent As Decimal

		Public Property YouSavePercent() As Decimal
			Get
				Return _youSavePercent
			End Get
			Set
				_youSavePercent = Value
			End Set
		End Property

		Private _youSavePrice As Decimal

		Public Property YouSavePrice() As Decimal
			Get
				Return _youSavePrice
			End Get
			Set
				_youSavePrice = Value
			End Set
		End Property

		#End Region

	End Class
End Namespace
