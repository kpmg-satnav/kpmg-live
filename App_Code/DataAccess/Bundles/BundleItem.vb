#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data.Common

Namespace Commerce.Promotions
    Public Class BundleItemCollection
        Inherits List(Of BundleItem)

    End Class
    Public Class BundleItem


#Region "private vars"

        Private _ProductID As Integer
        Private _Sku As String
        Private _ProductName As String
        Private _ShortDescription As String
        Private _OurPrice As Decimal
        Private _RetailPrice As Decimal
        Private _ImageFile As String
        Private _BundleName As String
        Private _DiscountPercent As Integer
        Private _Description As String
        Private _BundleID As Integer
#End Region

#Region "Public Props"
        Public Property ProductID() As Integer
            Get
                Return _ProductID
            End Get
            Set(ByVal value As Integer)
                _ProductID = Value
            End Set
        End Property
        Public Property Sku() As String
            Get
                Return _Sku
            End Get
            Set(ByVal value As String)
                _Sku = Value
            End Set
        End Property
        Public Property ProductName() As String
            Get
                Return _ProductName
            End Get
            Set(ByVal value As String)
                _ProductName = Value
            End Set
        End Property
        Public Property ShortDescription() As String
            Get
                Return _ShortDescription
            End Get
            Set(ByVal value As String)
                _ShortDescription = Value
            End Set
        End Property
        Public Property OurPrice() As Decimal
            Get
                Return _OurPrice
            End Get
            Set(ByVal value As Decimal)
                _OurPrice = Value
            End Set
        End Property
        Public Property RetailPrice() As Decimal
            Get
                Return _RetailPrice
            End Get
            Set(ByVal value As Decimal)
                _RetailPrice = Value
            End Set
        End Property
        Public Property ImageFile() As String
            Get
                Return _ImageFile
            End Get
            Set(ByVal value As String)
                _ImageFile = Value
            End Set
        End Property
        Public Property BundleName() As String
            Get
                Return _BundleName
            End Get
            Set(ByVal value As String)
                _BundleName = Value
            End Set
        End Property
        Public Property DiscountPercent() As Integer
            Get
                Return _DiscountPercent
            End Get
            Set(ByVal value As Integer)
                _DiscountPercent = Value
            End Set
        End Property
        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal value As String)
                _Description = Value
            End Set
        End Property
        Public Property BundleID() As Integer
            Get
                Return _BundleID
            End Get
            Set(ByVal value As Integer)
                _BundleID = Value
            End Set
        End Property
#End Region

        Public Sub Load(ByVal rdr As IDataReader)
            'LJD 10/17/2007 Converted code via http://labs.developerfusion.co.uk/convert/csharp-to-vb.aspx
            'Replaced [CInt] with CInt, [CDec] with CDec
            Try
                _ProductID = CInt(Fix(rdr("ProductID")))
            Catch
            End Try
            Try
                _Sku = rdr("Sku").ToString()
            Catch
            End Try
            Try
                _ProductName = rdr("ProductName").ToString()
            Catch
            End Try
            Try
                _ShortDescription = rdr("ShortDescription").ToString()
            Catch
            End Try
            Try
                _OurPrice = CDec(rdr("OurPrice"))
            Catch
            End Try
            Try
                _RetailPrice = CDec(rdr("RetailPrice"))
            Catch
            End Try
            Try
                _ImageFile = rdr("ImageFile").ToString()
            Catch
            End Try
            Try
                _BundleName = rdr("BundleName").ToString()
            Catch
            End Try
            Try
                _DiscountPercent = CInt(Fix(rdr("DiscountPercent")))
            Catch
            End Try
            Try
                _Description = rdr("Description").ToString()
            Catch
            End Try
            Try
                _BundleID = CInt(Fix(rdr("BundleID")))
            Catch
            End Try
        End Sub

#Region "Extended Data Access"
        ''' <summary>
        ''' Executes "CSK_Promo_Bundle_GetList" and returns the results in an BundleCollection 
        ''' </summary>
        ''' <returns>Commerce.Common.BundleCollection </returns>	
        Public Shared Function GetByProductID(ByVal productID As Integer) As BundleItemCollection
            Dim db As Database = DatabaseFactory.CreateDatabase()
            Dim coll As BundleItemCollection = Nothing
            Dim dbCommand As DbCommand = Nothing
            Try
                'specify the SP
                Dim cmd As String = "CSK_Promo_Bundle_GetByProductID"
                dbCommand = db.GetStoredProcCommand(cmd)
                Using dbCommand
                    db.AddInParameter(dbCommand, "@productID", DbType.Int32, productID)
                    coll = New BundleItemCollection()

                    'Load the reader
                    Dim rdr As IDataReader = db.ExecuteReader(dbCommand)
                    Dim item As BundleItem
                    Do While rdr.Read()
                        item = New BundleItem()
                        item.Load(rdr)
                        coll.Add(item)
                    Loop
                    rdr.Close()
                End Using

            Finally
                dbCommand.Dispose()
            End Try
            Return coll
        End Function

#End Region


    End Class


End Namespace
