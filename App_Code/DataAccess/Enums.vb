#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.ComponentModel


Namespace Commerce.Common

	Public Enum AttributeType
		SingleSelection
		MultipleSelection
		UserInput
	End Enum

	Public Enum AdBoxPlacement
		Right
        CenterTop
        CenterBottom
		Left
	End Enum

	Public Enum ShippingEstimate As Integer
		OneToTwoDays=1
		ThreeToFiveDays=2
		OneToTwoWeeks=3
	End Enum

	Public Enum ShippingType As Integer
		NormalShipping=1
		AirOnly=2
		DownloadOnly=3
	End Enum
	Public Enum ProductType As Integer
		DownloadableSoftware = 1
		Books = 2
		ElectronicGoods = 3
		SoftwareShipOnly = 4
	End Enum


	Public Enum ProductStatus As Integer
		Active=1
		[New] = 2
		OnBackorder=3
		TopSeller=4
		Featured=5
		Discontinued=6
		NoLongerAvailable=7
		FutureRelease=8
		Inactive=99
	End Enum
	''' <summary>
	''' The type of payment for an order
	''' </summary>
	Public Enum TransactionType As Integer
		CreditCardPayment = 1
        PayPalPayment = 2
		Refund = 3
		PurchaseOrder = 4
		OfflineCreditCardPayment = 5
        CashOnDelivery = 6
	End Enum
	''' <summary>
	''' Credit Cards the site accepts. Alter as needed
	''' </summary>
	<Flags, Serializable> _
	Public Enum CreditCardType
		MasterCard
		VISA
		Amex
		Discover
		PayPal
	End Enum

	''' <summary>
	''' The Order Status
	''' </summary>
	Public Enum OrderStatus As Integer
		NotProcessed = 9999
		ReceivedAwaitingPayment = 1
		ReceivedPaymentProcessingOrder = 10
		GatheringItemsFromInventory = 20
		AwatingShipmentToCustomer = 30
		DelayedItemsNotAvailable = 50
		ShippedToCustomer = 100
		DelayedReroutingShipping = 60
		DelayedCustomerRequest = 70
		DelayedOrderUnderReview = 80
		OrderCancelledPriorToShipping = 99
		OrderRefunded = 200
	End Enum
	Public Enum USState
		''' AK
			''' AL
		<Description("AK")> _
		AK = 1
			''' AR
		<Description("AL")> _
		AL = 2
			''' AZ
		<Description("AR")> _
		AR = 3
			''' CA
		<Description("AZ")> _
		AZ = 4
			''' CO
		<Description("CA")> _
		CA = 5
			''' CT
		<Description("CO")> _
		CO = 6
			''' DC
		<Description("CT")> _
		CT = 7
			''' DE
		<Description("DC")> _
		DC = 8
			''' FL
		<Description("DE")> _
		DE = 9
			''' GA
		<Description("FL")> _
		FL = 10
			''' HI
		<Description("GA")> _
		GA = 11
			''' IA
		<Description("HI")> _
		HI = 12
			''' ID
		<Description("IA")> _
		IA = 13
			''' IL
		<Description("ID")> _
		ID = 14
			''' IN
		<Description("IL")> _
		IL = 15
			''' KS
		<Description("IN")> _
		[IN] = 16
			''' KY
		<Description("KS")> _
		KS = 17
			''' LA
		<Description("KY")> _
		KY = 18
			''' MA
		<Description("LA")> _
		LA = 19
			''' MD
		<Description("MA")> _
		MA = 20
			''' ME
		<Description("MD")> _
		MD = 21
			''' MI
		<Description("ME")> _
		[ME] = 22
			''' MN
		<Description("MI")> _
		MI = 23
			''' MO
		<Description("MN")> _
		MN = 24
			''' MS
		<Description("MO")> _
		MO = 25
			''' MT
		<Description("MS")> _
		MS = 26
			''' NC
		<Description("MT")> _
		MT = 27
			''' ND
		<Description("NC")> _
		NC = 28
			''' NE
		<Description("ND")> _
		ND = 29
			''' NH
		<Description("NE")> _
		NE = 30
			''' NJ
		<Description("NH")> _
		NH = 31
			''' NM
		<Description("NJ")> _
		NJ = 32
			''' NV
		<Description("NM")> _
		NM = 33
			''' NY
		<Description("NV")> _
		NV = 34
			''' OH
		<Description("NY")> _
		NY = 35
			''' OK
		<Description("OH")> _
		OH = 36
			''' OR
		<Description("OK")> _
		OK = 37
			''' PA
		<Description("OR")> _
		[OR] = 38
			''' RI
		<Description("PA")> _
		PA = 39
			''' SC
		<Description("RI")> _
		RI = 40
			''' SD
		<Description("SC")> _
		SC = 41
			''' TN
		<Description("SD")> _
		SD = 42
			''' TX
		<Description("TN")> _
		TN = 43
			''' UT
		<Description("TX")> _
		TX = 44
			''' VA
		<Description("UT")> _
		UT = 45
			''' VT
		<Description("VA")> _
		VA = 46
			''' WA
		<Description("VT")> _
		VT = 47
			''' WI
		<Description("WA")> _
		WA = 48
			''' WV
		<Description("WI")> _
		WI = 49
			''' WY
		<Description("WV")> _
		WV = 50
		<Description("WY")> _
		WY = 51
	End Enum

	Public Enum CurrencyCode
		<Description("Afghanistan afghani")> _
		AFA = 1
		<Description("Albanian lek")> _
		ALL = 2
		<Description("Algerian dinar")> _
		DZD = 3
		<Description("Angolan kwanza reajustado")> _
		AOR = 4
		<Description("Argentine peso")> _
		ARS = 5
		<Description("Armenian dram")> _
		AMD = 6
		<Description("Aruban guilder")> _
		AWG = 7
		<Description("Australian dollar")> _
		AUD = 8
		<Description("Azerbaijanian new manat")> _
		AZN = 9
		<Description("Bahamian dollar")> _
		BSD = 10
		<Description("Bahraini dinar")> _
		BHD = 11
		<Description("Bangladeshi taka")> _
		BDT = 12
		<Description("Barbados dollar")> _
		BBD = 13
		<Description("Belarusian ruble")> _
		BYR = 14
		<Description("Belize dollar")> _
		BZD = 15
		<Description("Bermudian dollar")> _
		BMD = 16
		<Description("Bhutan ngultrum")> _
		BTN = 17
		<Description("Bolivian boliviano")> _
		BOB = 18
		<Description("Botswana pula")> _
		BWP = 19
		<Description("Brazilian real")> _
		BRL = 20
		<Description("British pound")> _
		GBP = 21
		<Description("Brunei dollar")> _
		BND = 22
		<Description("Bulgarian lev")> _
		BGN = 23
		<Description("Burundi franc")> _
		BIF = 24
		<Description("Cambodian riel")> _
		KHR = 25
		<Description("Canadian dollar")> _
		CAD = 26
		<Description("Cape Verde escudo")> _
		CVE = 27
		<Description("Cayman Islands dollar")> _
		KYD = 28
		<Description("CFA franc BCEAO")> _
		XOF = 29
		<Description("CFA franc BEAC")> _
		XAF = 30
		<Description("CFP franc")> _
		XPF = 31
		<Description("Chilean peso")> _
		CLP = 32
		<Description("Chinese yuan renminbi")> _
		CNY = 33
		<Description("Colombian peso")> _
		COP = 34
		<Description("Comoros franc")> _
		KMF = 35
		<Description("Congolese franc")> _
		CDF = 36
		<Description("Costa Rican colon")> _
		CRC = 37
		<Description("Croatian kuna")> _
		HRK = 38
		<Description("Cuban peso")> _
		CUP = 39
		<Description("Cypriot pound")> _
		CYP = 40
		<Description("Czech koruna")> _
		CZK = 41
		<Description("Danish krone")> _
		DKK = 42
		<Description("Djibouti franc")> _
		DJF = 43
		<Description("Dominican peso")> _
		DOP = 44
		<Description("East Caribbean dollar")> _
		XCD = 45
		<Description("Egyptian pound")> _
		EGP = 46
		<Description("El Salvador colon")> _
		SVC = 47
		<Description("Eritrean nakfa")> _
		ERN = 48
		<Description("Estonian kroon")> _
		EEK = 49
		<Description("Ethiopian birr")> _
		ETB = 50
		<Description("EU euro")> _
		EUR = 51
		<Description("Falkland Islands pound")> _
		FKP = 52
		<Description("Fiji dollar")> _
		FJD = 53
		<Description("Gambian dalasi")> _
		GMD = 54
		<Description("Georgian lari")> _
		GEL = 55
		<Description("Ghanaian cedi")> _
		GHC = 56
		<Description("Gibraltar pound")> _
		GIP = 57
		<Description("Gold (ounce)")> _
		XAU = 58
		<Description("Gold franc")> _
		XFO = 59
		<Description("Guatemalan quetzal")> _
		GTQ = 60
		<Description("Guinean franc")> _
		GNF = 61
		<Description("Guyana dollar")> _
		GYD = 62
		<Description("Haitian gourde")> _
		HTG = 63
		<Description("Honduran lempira")> _
		HNL = 64
		<Description("Hong Kong SAR dollar")> _
		HKD = 65
		<Description("Hungarian forint")> _
		HUF = 66
		<Description("Icelandic krona")> _
		ISK = 67
		<Description("IMF special drawing right")> _
		XDR = 68
		<Description("Indian rupee")> _
		INR = 69
		<Description("Indonesian rupiah")> _
		IDR = 70
		<Description("Iranian rial")> _
		IRR = 71
		<Description("Iraqi dinar")> _
		IQD = 72
		<Description("Israeli new shekel")> _
		ILS = 73
		<Description("Jamaican dollar")> _
		JMD = 74
		<Description("Japanese yen")> _
		JPY = 75
		<Description("Jordanian dinar")> _
		JOD = 76
		<Description("Kazakh tenge")> _
		KZT = 77
		<Description("Kenyan shilling")> _
		KES = 78
		<Description("Kuwaiti dinar")> _
		KWD = 79
		<Description("Kyrgyz som")> _
		KGS = 80
		<Description("Lao kip")> _
		LAK = 81
		<Description("Latvian lats")> _
		LVL = 82
		<Description("Lebanese pound")> _
		LBP = 83
		<Description("Lesotho loti")> _
		LSL = 84
		<Description("Liberian dollar")> _
		LRD = 85
		<Description("Libyan dinar")> _
		LYD = 86
		<Description("Lithuanian litas")> _
		LTL = 87
		<Description("Macao SAR pataca")> _
		MOP = 88
		<Description("Macedonian denar")> _
		MKD = 89
		<Description("Malagasy ariary")> _
		MGA = 90
		<Description("Malawi kwacha")> _
		MWK = 91
		<Description("Malaysian ringgit")> _
		MYR = 92
		<Description("Maldivian rufiyaa")> _
		MVR = 93
		<Description("Maltese lira")> _
		MTL = 94
		<Description("Mauritanian ouguiya")> _
		MRO = 95
		<Description("Mauritius rupee")> _
		MUR = 96
		<Description("Mexican peso")> _
		MXN = 97
		<Description("Moldovan leu")> _
		MDL = 98
		<Description("Mongolian tugrik")> _
		MNT = 99
		<Description("Moroccan dirham")> _
		MAD = 100
		<Description("Mozambique new metical")> _
		MZN = 101
		<Description("Myanmar kyat")> _
		MMK = 102
		<Description("Namibian dollar")> _
		NAD = 103
		<Description("Nepalese rupee")> _
		NPR = 104
		<Description("Netherlands Antillian guilder")> _
		ANG = 105
		<Description("New Zealand dollar")> _
		NZD = 106
		<Description("Nicaraguan cordoba oro")> _
		NIO = 107
		<Description("Nigerian naira")> _
		NGN = 108
		<Description("North Korean won")> _
		KPW = 109
		<Description("Norwegian krone")> _
		NOK = 110
		<Description("Omani rial")> _
		OMR = 111
		<Description("Pakistani rupee")> _
		PKR = 112
		<Description("Palladium (ounce)")> _
		XPD = 113
		<Description("Panamanian balboa")> _
		PAB = 114
		<Description("Papua New Guinea kina")> _
		PGK = 115
		<Description("Paraguayan guarani")> _
		PYG = 116
		<Description("Peruvian nuevo sol")> _
		PEN = 117
		<Description("Philippine peso")> _
		PHP = 118
		<Description("Platinum (ounce)")> _
		XPT = 119
		<Description("Polish zloty")> _
		PLN = 120
		<Description("Qatari rial")> _
		QAR = 121
		<Description("Romanian new leu")> _
		RON = 122
		<Description("Russian ruble")> _
		RUB = 123
		<Description("Rwandan franc")> _
		RWF = 124
		<Description("Saint Helena pound")> _
		SHP = 125
		<Description("Samoan tala")> _
		WST = 126
		<Description("Sao Tome and Principe dobra")> _
		STD = 127
		<Description("Saudi riyal")> _
		SAR = 128
		<Description("Serbian dinar")> _
		CSD = 129
		<Description("Seychelles rupee")> _
		SCR = 130
		<Description("Sierra Leone leone")> _
		SLL = 131
		<Description("Silver (ounce)")> _
		XAG = 132
		<Description("Singapore dollar")> _
		SGD = 133
		<Description("Slovak koruna")> _
		SKK = 134
		<Description("Slovenian tolar")> _
		SIT = 135
		<Description("Solomon Islands dollar")> _
		SBD = 136
		<Description("Somali shilling")> _
		SOS = 137
		<Description("South African rand")> _
		ZAR = 138
		<Description("South Korean won")> _
		KRW = 139
		<Description("Sri Lanka rupee")> _
		LKR = 140
		<Description("Sudanese dinar")> _
		SDD = 141
		<Description("Suriname dollar")> _
		SRD = 142
		<Description("Swaziland lilangeni")> _
		SZL = 143
		<Description("Swedish krona")> _
		SEK = 144
		<Description("Swiss franc")> _
		CHF = 145
		<Description("Syrian pound")> _
		SYP = 146
		<Description("Taiwan New dollar")> _
		TWD = 147
		<Description("Tajik somoni")> _
		TJS = 148
		<Description("Tanzanian shilling")> _
		TZS = 149
		<Description("Thai baht")> _
		THB = 150
		<Description("Tongan pa'anga")> _
		TOP = 151
		<Description("Trinidad and Tobago dollar")> _
		TTD = 152
		<Description("Tunisian dinar")> _
		TND = 153
		<Description("Turkish lira")> _
		[TRY] = 154
		<Description("Turkmen manat")> _
		TMM = 155
		<Description("UAE dirham")> _
		AED = 156
		<Description("Uganda new shilling")> _
		UGX = 157
		<Description("UIC franc")> _
		XFU = 158
		<Description("Ukrainian hryvnia")> _
		UAH = 159
		<Description("Uruguayan peso uruguayo")> _
		UYU = 160
		<Description("US dollar")> _
		USD = 161
		<Description("Uzbekistani sum")> _
		UZS = 162
		<Description("Vanuatu vatu")> _
		VUV = 163
		<Description("Venezuelan bolivar")> _
		VEB = 164
		<Description("Vietnamese dong")> _
		VND = 165
		<Description("Yemeni rial")> _
		YER = 166
		<Description("Zambian kwacha")> _
		ZMK = 167
		<Description("Zimbabwe dollar")> _
		ZWD = 168
	End Enum


    Public Enum Country
        <Description("Afghanistan")>
        AF = 1
        <Description("Albania")>
        AL = 2
        <Description("Algeria")>
        DZ = 3
        <Description("American Samoa")>
        [AS] = 4
        <Description("Andorra")>
        AD = 5
        <Description("Angola")>
        AO = 6
        <Description("Anguilla")>
        AI = 7
        <Description("Antarctica")>
        AQ = 8
        <Description("Antigua and Barbuda")>
        AG = 9
        <Description("Argentina")>
        AR = 10
        <Description("Armenia")>
        AM = 11
        <Description("Aruba")>
        AW = 12
        <Description("Australia")>
        AU = 13
        <Description("Austria")>
        AT = 14
        <Description("Azerbaijan")>
        AZ = 15
        <Description("Bahamas")>
        BS = 16
        <Description("Bahrain")>
        BH = 17
        <Description("Bangladesh")>
        BD = 18
        <Description("Barbados")>
        BB = 19
        <Description("Belarus")>
        BY = 20
        <Description("Belgium")>
        BE = 21
        <Description("Belize")>
        BZ = 22
        <Description("Benin")>
        BJ = 23
        <Description("Bermuda")>
        BM = 24
        <Description("Bhutan")>
        BT = 25
        <Description("Bolivia")>
        BO = 26
        <Description("Bosnia and Herzegovina")>
        BA = 27
        <Description("Botswana")>
        BW = 28
        <Description("Bouvet Island")>
        BV = 29
        <Description("Brazil")>
        BR = 30
        <Description("British Indian Ocean Territory")>
        IO = 31
        <Description("British Virgin Islands")>
        VG = 32
        <Description("Brunei Darussalam")>
        BN = 33
        <Description("Bulgaria")>
        BG = 34
        <Description("Burkina Faso")>
        BF = 35
        <Description("Burundi")>
        BI = 36
        <Description("Cambodia")>
        KH = 37
        <Description("Cameroon")>
        CM = 38
        <Description("Canada")>
        CA = 39
        <Description("Cape Verde")>
        CV = 40
        <Description("Cayman Islands")>
        KY = 41
        <Description("Central African Republic")>
        CF = 42
        <Description("Chad")>
        TD = 43
        <Description("Chile")>
        CL = 44
        <Description("China")>
        CN = 45
        <Description("Christmas Island")>
        CX = 46
        <Description("Cocos")>
        CC = 47
        <Description("Colombia")>
        CO = 48
        <Description("Comoros")>
        KM = 49
        <Description("Congo")>
        CG = 50
        <Description("Cook Islands")>
        CK = 51
        <Description("Costa Rica")>
        CR = 52
        <Description("Croatia")>
        HR = 53
        <Description("Cuba")>
        CU = 54
        <Description("Cyprus")>
        CY = 55
        <Description("Czech Republic")>
        CZ = 56
        <Description("Denmark")>
        DK = 57
        <Description("Djibouti")>
        DJ = 58
        <Description("Dominica")>
        DM = 59
        <Description("Dominican Republic")>
        [DO] = 60
        <Description("East Timor")>
        TP = 61
        <Description("Ecuador")>
        EC = 62
        <Description("Egypt")>
        EG = 63
        <Description("El Salvador")>
        SV = 64
        <Description("Equatorial Guinea")>
        GQ = 65
        <Description("Eritrea")>
        ER = 66
        <Description("Estonia")>
        EE = 67
        <Description("Ethiopia")>
        ET = 68
        <Description("Falkland Islands")>
        FK = 69
        <Description("Faroe Islands")>
        FO = 70
        <Description("Fiji")>
        FJ = 71
        <Description("Finland")>
        FI = 72
        <Description("France")>
        FR = 73
        <Description("French Guiana")>
        GF = 74
        <Description("French Polynesia")>
        PF = 75
        <Description("French Southern Territories")>
        TF = 76
        <Description("Gabon")>
        GA = 77
        <Description("Gambia")>
        GM = 78
        <Description("Georgia")>
        GE = 79
        <Description("Germany")>
        DE = 80
        <Description("Ghana")>
        GH = 81
        <Description("Gibraltar")>
        GI = 82
        <Description("Greece")>
        GR = 83
        <Description("Greenland")>
        GL = 84
        <Description("Grenada")>
        GD = 85
        <Description("Guadeloupe")>
        GP = 86
        <Description("Guam")>
        GU = 87
        <Description("Guatemala")>
        GT = 88
        <Description("Guinea")>
        GN = 89
        <Description("Guinea-Bissau")>
        GW = 90
        <Description("Guyana")>
        GY = 91
        <Description("Haiti")>
        HT = 92
        <Description("Heard and McDonald Islands")>
        HM = 93
        <Description("Honduras")>
        HN = 94
        <Description("Hong Kong")>
        HK = 95
        <Description("Hungary")>
        HU = 96
        <Description("Iceland")>
        [IS] = 97
        <Description("India")>
        [IN] = 98
        <Description("Indonesia")>
        ID = 99
        <Description("Iran")>
        IR = 100
        <Description("Iraq")>
        IQ = 101
        <Description("Ireland")>
        IE = 102
        <Description("Israel")>
        IL = 103
        <Description("Italy")>
        IT = 104
        <Description("Ivory Coast")>
        CI = 105
        <Description("Jamaica")>
        JM = 106
        <Description("Japan")>
        JP = 107
        <Description("Jordan")>
        JO = 108
        <Description("Kazakhstan")>
        KZ = 109
        <Description("Kenya")>
        KE = 110
        <Description("Kiribati")>
        KI = 111
        <Description("Kuwait")>
        KW = 112
        <Description("Kyrgyzstan")>
        KG = 113
        <Description("Laos")>
        LA = 114
        <Description("Latvia")>
        LV = 115
        <Description("Lebanon")>
        LB = 116
        <Description("Lesotho")>
        LS = 117
        <Description("Liberia")>
        LR = 118
        <Description("Libya")>
        LY = 119
        <Description("Liechtenstein")>
        LI = 120
        <Description("Lithuania")>
        LT = 121
        <Description("Luxembourg")>
        LU = 122
        <Description("Macau")>
        MO = 123
        <Description("Macedonia")>
        MK = 124
        <Description("Madagascar")>
        MG = 125
        <Description("Malawi")>
        MW = 126
        <Description("Malaysia")>
        MY = 127
        <Description("Maldives")>
        MV = 128
        <Description("Mali")>
        ML = 129
        <Description("Malta")>
        MT = 130
        <Description("Marshall Islands")>
        MH = 131
        <Description("Martinique")>
        MQ = 132
        <Description("Mauritania")>
        MR = 133
        <Description("Mauritius")>
        MU = 134
        <Description("Mayotte")>
        YT = 135
        <Description("Mexico")>
        MX = 136
        <Description("Micronesia")>
        FM = 137
        <Description("Moldova")>
        MD = 138
        <Description("Monaco")>
        MC = 139
        <Description("Mongolia")>
        MN = 140
        <Description("Montserrat")>
        MS = 141
        <Description("Morocco")>
        MA = 142
        <Description("Mozambique")>
        MZ = 143
        <Description("Myanmar")>
        MM = 144
        <Description("Namibia")>
        NA = 145
        <Description("Nauru")>
        NR = 146
        <Description("Nepal")>
        NP = 147
        <Description("Netherlands")>
        NL = 148
        <Description("Netherlands Antilles")>
        AN = 149
        <Description("New Caledonia")>
        NC = 150
        <Description("New Zealand")>
        NZ = 151
        <Description("Nicaragua")>
        NI = 152
        <Description("Niger")>
        NE = 153
        <Description("Nigeria")>
        NG = 154
        <Description("Niue")>
        NU = 155
        <Description("Norfolk Island")>
        NF = 156
        <Description("North Korea")>
        KP = 157
        <Description("Northern Mariana Islands")>
        MP = 158
        <Description("Norway")>
        NO = 159
        <Description("Oman")>
        OM = 160
        <Description("Pakistan")>
        PK = 161
        <Description("Palau")>
        PW = 162
        <Description("Panama")>
        PA = 163
        <Description("Papua New Guinea")>
        PG = 164
        <Description("Paraguay")>
        PY = 165
        <Description("Peru")>
        PE = 166
        <Description("Philippines")>
        PH = 167
        <Description("Pitcairn")>
        PN = 168
        <Description("Poland")>
        PL = 169
        <Description("Portugal")>
        PT = 170
        <Description("Puerto Rico")>
        PR = 171
        <Description("Qatar")>
        QA = 172
        <Description("Reunion")>
        RE = 173
        <Description("Romania")>
        RO = 174
        <Description("Russian Federation")>
        RU = 175
        <Description("Rwanda")>
        RW = 176
        <Description("S. Georgia and S. Sandwich Islands")>
        GS = 177
        <Description("Saint Kitts and Nevis")>
        KN = 178
        <Description("Saint Lucia")>
        LC = 179
        <Description("Saint Vincent and The Grenadines")>
        VC = 180
        <Description("Samoa")>
        WS = 181
        <Description("San Marino")>
        SM = 182
        <Description("Sao Tome and Principe")>
        ST = 183
        <Description("Saudi Arabia")>
        SA = 184
        <Description("Senegal")>
        SN = 185
        <Description("Seychelles")>
        SC = 186
        <Description("Sierra Leone")>
        SL = 187
        <Description("Singapore")>
        SG = 188
        <Description("Slovakia")>
        SK = 189
        <Description("Slovenia")>
        SI = 190
        <Description("Solomon Islands")>
        SB = 191
        <Description("Somalia")>
        SO = 192
        <Description("South Africa")>
        ZA = 193
        <Description("South Korea")>
        KR = 194
        <Description("Soviet Union")>
        SU = 195
        <Description("Spain")>
        ES = 196
        <Description("Sri Lanka")>
        LK = 197
        <Description("St. Helena")>
        SH = 198
        <Description("St. Pierre and Miquelon")>
        PM = 199
        <Description("Sudan")>
        SD = 200
        <Description("Suriname")>
        SR = 201
        <Description("Svalbard and Jan Mayen Islands")>
        SJ = 202
        <Description("Swaziland")>
        SZ = 203
        <Description("Sweden")>
        SE = 204
        <Description("Switzerland")>
        CH = 205
        <Description("Syria")>
        SY = 206
        <Description("Taiwan")>
        TW = 207
        <Description("Tajikistan")>
        TJ = 208
        <Description("Tanzania")>
        TZ = 209
        <Description("Thailand")>
        TH = 210
        <Description("Togo")>
        TG = 211
        <Description("Tokelau")>
        TK = 212
        <Description("Tonga")>
        [TO] = 213
        <Description("Trinidad and Tobago")>
        TT = 214
        <Description("Tunisia")>
        TN = 215
        <Description("Turkey")>
        TR = 216
        <Description("Turkmenistan")>
        TM = 217
        <Description("Turks and Caicos Islands")>
        TC = 218
        <Description("Tuvalu")>
        TV = 219
        <Description("Uganda")>
        UG = 220
        <Description("Ukraine")>
        UA = 221
        <Description("United Arab Emirates")>
        AE = 222
        <Description("United Kingdom")>
        GB = 223
        <Description("United States")>
        US = 224
        <Description("Uruguay")>
        UY = 225
        <Description("US Minor Outlying Islands")>
        UM = 226
        <Description("US Virgin Islands")>
        VI = 227
        <Description("Uzbekistan")>
        UZ = 228
        <Description("Vanuatu")>
        VU = 229
        <Description("Venezuela")>
        VE = 230
        <Description("Viet Nam")>
        VN = 231
        <Description("Wallis and Futuna Islands")>
        WF = 232
        <Description("Western Sahara")>
        EH = 233
        <Description("Yemen")>
        YE = 234
        <Description("Yugoslavia")>
        YU = 235
        <Description("Zaire")>
        ZR = 236
        <Description("Zambia")>
        ZM = 237
        <Description("Zimbabwe")>
        ZW = 238
    End Enum

    Public Enum ADValidate
        Success = 1
        Failed = 2
        NotValidUser = 3
        [Error] = 4
    End Enum

    Public Enum LoginStatus
        Success = 1
        Expired = 2
        AlreadyLogged = 3
        TokenInValid = 4
        Valid = 5
        [error] = 6
    End Enum


End Namespace
