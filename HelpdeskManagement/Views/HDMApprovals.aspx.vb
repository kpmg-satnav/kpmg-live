﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions
Imports System.IO

Partial Class HelpdeskManagement_Views_HDMApprovals
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim filePath As String
    Dim feedbackVal = String.Empty
    Dim STA_ID As Integer

    Protected Sub ddlStatus_SelectedIndexChanged() Handles ddlStatus.SelectedIndexChanged
        btnApprove.Enabled = False
        btnReject.Enabled = False

    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        filePath = HttpContext.Current.Request.Url.Authority & "/UploadFiles/" & Session("TENANT")
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If STA_ID = 8 Then
                btnOnHold.Enabled = False
            End If
            If Not IsPostBack Then
                Dim UID As String = Session("uid")
                Session("reqid") = Request.QueryString("rid").ToString()
                'ApprovedComments()
                Dim sp6 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
                sp6.Command.AddParameter("@TYPE ", 6, DbType.Int32)
                ViewState("DSN_MAPPING_STA") = sp6.ExecuteScalar()
                BindRequisition()

                ' To check admin roles mapping or not
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_HAVING_ADMIN_ROLES")
                sp1.Command.AddParameter("@USER_ID ", Session("UID").ToString(), DbType.String)
                ViewState("CHK_ROLE") = sp1.ExecuteScalar()
                If ViewState("CHK_ROLE") > 0 Then
                    btnSkip.Visible = True
                Else
                    btnSkip.Visible = False
                End If

            End If
        End If

    End Sub

    Private Sub BindRequisition()
        Try
            Dim ds As New DataSet
            Dim param As SqlParameter() = New SqlParameter(0) {}
            param(0) = New SqlParameter("@REQID", Session("reqid").ToString())
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "HDM_VIEW_REQUISITION_BY_REQ_ID", param)
            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblReqId.Text = ds.Tables(0).Rows(0)("REQUEST_ID").ToString()
                    txtEmp.Text = ds.Tables(0).Rows(0).Item("REQUESTED_BY")
                    txtDepartment.Text = ds.Tables(0).Rows(0)("DEPARTMENT").ToString()
                    txtSpaceID.Text = ds.Tables(0).Rows(0)("SPACE_ID").ToString()
                    lblStatus.Text = ds.Tables(0).Rows(0)("STATUS ID").ToString()
                    ViewState("DSN_CODE") = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
                    feedbackVal = ds.Tables(0).Rows(0)("FEEDBACK_CODE").ToString()
                    txtclaimamount.Text = ds.Tables(0).Rows(0)("SER_CLAIM_AMT").ToString()
                    txtAppRemarks.Text = ds.Tables(0).Rows(0)("SERH_COMMENTS").ToString()
                    txtamt.Text = ds.Tables(0).Rows(0)("SER_APPR_AMOUNT").ToString()
                    BindStatus()
                    ddlStatus.ClearSelection()
                    If ds.Tables(0).Rows(0)("STATUS ID").ToString <> "" AndAlso ds.Tables(0).Rows(0)("STATUS ID").ToString() IsNot Nothing Then
                        ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0)("STATUS ID").ToString()).Selected = True
                    End If

                    BindLocations()
                    ddlLocation.ClearSelection()
                    If ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString() IsNot Nothing Then
                        ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString()).Selected = True
                    End If
                    BindMainCategory()

                    ddlMainCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString() IsNot Nothing Then
                        ddlMainCategory.Items.FindByValue(ds.Tables(0).Rows(0)("MAIN_CATEGORY").ToString()).Selected = True
                    End If
                    BindSubCategory()
                    ddlSubCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString() IsNot Nothing Then
                        ddlSubCategory.Items.FindByValue(ds.Tables(0).Rows(0)("SUB_CATEGORY").ToString()).Selected = True
                    End If
                    BindChildCategory()
                    ddlChildCategory.ClearSelection()
                    If ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString() IsNot Nothing Then
                        ddlChildCategory.Items.FindByValue(ds.Tables(0).Rows(0)("CHILD_CATEGORY").ToString()).Selected = True
                    End If
                    BindAssetLocations()
                    ddlAssetLoaction.ClearSelection()
                    If ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString() IsNot Nothing Then
                        ddlAssetLoaction.Items.FindByValue(ds.Tables(0).Rows(0)("ASSET_LOCATION").ToString()).Selected = True
                    End If
                    txtMobile.Text = ds.Tables(0).Rows(0)("MOBILE").ToString()
                    BindRepeatCalls()
                    ddlRepeatCalls.ClearSelection()
                    If ds.Tables(0).Rows(0)("REPEAT_CALL").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("REPEAT_CALL").ToString() IsNot Nothing Then
                        ddlRepeatCalls.Items.FindByValue(ds.Tables(0).Rows(0)("REPEAT_CALL").ToString()).Selected = True
                    End If
                    BindImpact()
                    ddlImpact.ClearSelection()
                    If ds.Tables(0).Rows(0)("IMPACT").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("IMPACT").ToString() IsNot Nothing Then
                        ddlImpact.Items.FindByValue(ds.Tables(0).Rows(0)("IMPACT").ToString()).Selected = True
                    End If
                    BindUrgency()
                    ddlUrgency.ClearSelection()
                    If ds.Tables(0).Rows(0)("URGENCY").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("URGENCY").ToString() IsNot Nothing Then
                        ddlUrgency.Items.FindByValue(ds.Tables(0).Rows(0)("URGENCY").ToString()).Selected = True
                    End If

                    txtProbDesc.Text = ds.Tables(0).Rows(0)("REMARKS").ToString()

                    BindFacilities(ds.Tables(0).Rows(0)("SER_LOC_CODE").ToString())
                    ddlfacility.ClearSelection()
                    If ds.Tables(0).Rows(0)("FD_CODE").ToString() <> "" AndAlso ds.Tables(0).Rows(0)("FD_CODE").ToString() IsNot Nothing Then
                        ddlfacility.Items.FindByValue(ds.Tables(0).Rows(0)("FD_CODE").ToString()).Selected = True
                    End If

                    Dim list As List(Of fileInfo)
                    list = GetFiles()
                    Session.Add("ExistingFileList", list)
                    Dim jsonstring = Newtonsoft.Json.JsonConvert.SerializeObject(list)
                    jsonstringhdn.Value = jsonstring
                End If
            End If

            'HISTORY IN GRID
            Dim dsHistory As New DataSet
            Dim spHist As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_REQUEST_HISTORY_DETAILS")
            spHist.Command.AddParameter("@REQID", Session("reqid"), Data.DbType.String)
            dsHistory = spHist.GetDataSet()
            gvReqHistory.DataSource = dsHistory
            gvReqHistory.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Class fileInfo
        Public Property Name As String
        Public Property Path As String
        Public Property UplTimeName As String
    End Class

    Public Function GetFiles() As List(Of fileInfo)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_FILES")
        sp.Command.AddParameter("@REQ_ID", lblReqId.Text, Data.DbType.String)
        Dim fileslist As List(Of fileInfo) = New List(Of fileInfo)()

        Dim dr As SqlDataReader
        dr = sp.GetReader()
        While (dr.Read())
            fileslist.Add(New fileInfo() With {.Name = dr.GetString(0), .Path = "/UploadFiles/" & Session("TENANT") & "/" & dr.GetString(1), .UplTimeName = dr.GetString(1)})
        End While
        dr.Close()
        Return fileslist
    End Function

    Public Sub BindStatus()
        ddlStatus.Items.Clear()
        ObjSubsonic.Binddropdown(ddlStatus, "HDM_STATUS_BIND_APPROVAL", "STA_TITLE", "STA_ID")
    End Sub

    Public Sub BindFacilities(ByVal loc As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 50)
        param(0).Value = loc
        param(1) = New SqlParameter("@CMP_ID", SqlDbType.Int)
        param(1).Value = Session("COMPANYID").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlfacility, "HDM_GET_FACILITY_DTLS", "FD_NAME", "FD_CODE", param)
    End Sub

    Private Sub BindLocations()
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindMainCategory()
        ddlMainCategory.Items.Clear()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(0).Value = ViewState("DSN_MAPPING_STA")
        param(1) = New SqlParameter("@DSN_CODE", DbType.String)
        param(1).Value = ViewState("DSN_CODE")
        ObjSubsonic.Binddropdown(ddlMainCategory, "HDM_GET_ALL_ACTIVE_MAINCATEGORY", "MNC_NAME", "MNC_CODE", param)
    End Sub

    Public Sub BindSubCategory()
        ddlSubCategory.Items.Clear()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = ddlMainCategory.SelectedValue
        param(1) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(1).Value = ViewState("DSN_MAPPING_STA")
        param(2) = New SqlParameter("@DSN_CODE", DbType.String)
        param(2).Value = ViewState("DSN_CODE")
        ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_SUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param)
    End Sub

    Public Sub BindChildCategory()
        ddlChildCategory.Items.Clear()
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = ddlMainCategory.SelectedValue
        param(1) = New SqlParameter("@SUB_CODE", SqlDbType.NVarChar, 50)
        param(1).Value = ddlSubCategory.SelectedValue
        param(2) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(2).Value = ViewState("DSN_MAPPING_STA")
        param(3) = New SqlParameter("@DSN_CODE", DbType.String)
        param(3).Value = ViewState("DSN_CODE")
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_CHILD_CATEGORYS_BY_SUBCATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param)
    End Sub

    Public Sub BindAssetLocations()
        ddlAssetLoaction.Items.Clear()
        ObjSubsonic.Binddropdown(ddlAssetLoaction, "HDM_GET_ALL_ACTIVE_ASSETlOCATIONS", "HAL_LOC_NAME", "HAL_LOC_CODE")
    End Sub

    Public Sub BindRepeatCalls()
        ddlRepeatCalls.Items.Clear()
        ObjSubsonic.Binddropdown(ddlRepeatCalls, "HDM_GET_ALL_ACTIVE_REPEATCALLS", "RPT_NAME", "RPT_CODE")
    End Sub

    Public Sub BindImpact()
        ddlImpact.Items.Clear()
        ObjSubsonic.Binddropdown(ddlImpact, "HDM_GET_ALL_ACTIVE_IMPACT", "IMP_NAME", "IMP_CODE")
    End Sub

    Public Sub BindUrgency()
        ddlUrgency.Items.Clear()
        ObjSubsonic.Binddropdown(ddlUrgency, "HDM_GET_ALL_ACTIVE_URGENCY", "UGC_NAME", "UGC_CODE")
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmHDMViewRequisitions.aspx")
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        ddlMainCategory.SelectedIndex = 0
        lblMessage.Visible = False
    End Sub

    Public Sub BindSubCategory_By_MainCategory(Main_Category As String, DSN_STA As Integer, DSN_CODE As String)
        ddlSubCategory.Items.Clear()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = Main_Category
        param(1) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(1).Value = DSN_STA
        param(2) = New SqlParameter("@DSN_CODE", DbType.String)
        param(2).Value = DSN_CODE
        ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_SUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param)
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Try
            ApproveRequest()
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub ApproveRequest()
        Dim Req_status As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_APPROVE_REQUISITION_BY_REQ_ID")
        sp.Command.AddParameter("@REQID", lblReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
        sp.Command.AddParameter("@STA_ID ", ddlStatus.SelectedValue, DbType.Int32)
        sp.Command.AddParameter("@SER_LOC_CODE ", ddlLocation.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_MNC_CODE", ddlMainCategory.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_SUB_CAT_CODE ", ddlSubCategory.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_CHILD_CAT_CODE", ddlChildCategory.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_APPR_COMMENTS", txtAppRemarks.Text, DbType.String)
        'sp.Command.AddParameter("@SER_APPR_AMOUNT", txtamt.Text, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        sp.Command.AddParameter("@SER_CLAIM_AMT", txtclaimamount.Text, DbType.String)
        sp.Command.AddParameter("@SER_APPR_AMOUNT", txtamt.Text, DbType.String)
        sp.Execute()
        lblMessage.Visible = True
        lblMessage.Text = "Service Requisition (" + lblReqId.Text + ") Approved successfully."
        btnApprove.Enabled = False
        btnSkip.Enabled = False
        btnReject.Enabled = False
    End Sub

    Protected Sub btnSkip_Click(sender As Object, e As EventArgs) Handles btnSkip.Click
        Try
            SkipRequest()
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub SkipRequest()
        Dim Req_status As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_SKIP_REQUISITION_BY_REQ_ID")
        sp.Command.AddParameter("@REQID", lblReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
        sp.Command.AddParameter("@STA_ID ", ddlStatus.SelectedValue, DbType.Int32)
        sp.Command.AddParameter("@SER_LOC_CODE ", ddlLocation.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_MNC_CODE", ddlMainCategory.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_SUB_CAT_CODE ", ddlSubCategory.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_CHILD_CAT_CODE", ddlChildCategory.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_APPR_COMMENTS", txtAppRemarks.Text, DbType.String)
        sp.Command.AddParameter("@SER_APPR_AMOUNT", txtamt.Text, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        sp.Execute()
        lblMessage.Visible = True
        lblMessage.Text = "Service Requisition (" + lblReqId.Text + ") Skipped successfully. "
        btnApprove.Enabled = False
        btnSkip.Enabled = False
        btnReject.Enabled = False
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Try
            RejectRequest()
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub RejectRequest()
        Dim Req_status As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_REJECT_REQUISITION_BY_REQ_ID")
        sp.Command.AddParameter("@REQID", lblReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
        sp.Command.AddParameter("@STA_ID ", ddlStatus.SelectedValue, DbType.Int32)
        sp.Command.AddParameter("@SER_LOC_CODE ", ddlLocation.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_MNC_CODE", ddlMainCategory.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_SUB_CAT_CODE ", ddlSubCategory.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_CHILD_CAT_CODE", ddlChildCategory.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_APPR_COMMENTS", txtAppRemarks.Text, DbType.String)
        sp.Command.AddParameter("@SER_APPR_AMOUNT", txtamt.Text, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        sp.Execute()
        lblMessage.Visible = True
        lblMessage.Text = "Service Requisition (" + lblReqId.Text + ") Rejected successfully. "
        btnApprove.Enabled = False
        btnSkip.Enabled = False
        btnReject.Enabled = False
    End Sub

    Protected Sub btnOnHold_Click(sender As Object, e As EventArgs) Handles btnOnHold.Click
        'Dim Req_status As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_APPROVE_REQUEST_MODIFY_BY_REQ_ID")
        sp.Command.AddParameter("@REQID", lblReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
        sp.Command.AddParameter("@SER_LOC_CODE ", ddlLocation.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_MNC_CODE", ddlMainCategory.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_SUB_CAT_CODE ", ddlSubCategory.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_CHILD_CAT_CODE", ddlChildCategory.SelectedValue, DbType.String)
        sp.Command.AddParameter("@STA_ID ", ddlStatus.SelectedValue, DbType.Int32)
        sp.Command.AddParameter("@SER_AST_LOC_CODE ", ddlAssetLoaction.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_MOBILE ", txtMobile.Text, DbType.String)
        sp.Command.AddParameter("@SER_CALL_TYPE ", ddlRepeatCalls.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_IMPACT ", ddlImpact.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_URGENCY ", ddlUrgency.SelectedValue, DbType.String)
        sp.Command.AddParameter("@SER_PROB_DESC ", txtProbDesc.Text, DbType.String)
        sp.Command.AddParameter("@SER_CLAIM_AMT ", txtclaimamount.Text, DbType.String)
        'sp.Command.AddParameter("@SER_AMOUNT", txtamt.Text, DbType.String)
        sp.Command.AddParameter("@SER_APPR_COMMENTS", txtAppRemarks.Text, DbType.String)
        'sp.Command.AddParameter("@SER_DOC_LINK", FLstr, DbType.String)
        sp.ExecuteScalar()
        lblMessage.Visible = True
        lblMessage.Text = "Service Requisition (" + lblReqId.Text + ") Modified successfully..."

    End Sub
End Class
