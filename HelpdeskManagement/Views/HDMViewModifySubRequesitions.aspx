﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script src="../../Scripts/aggrid/ag-grid-2.js" defer></script>
    <%--<script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

        var columnDefs = [
            { headerName: "Selected File", field: "Name", cellClass: "grid-align", width: 250 },
            { headerName: "Image", field: "Name", cellClass: "grid-align", width: 250 },
            { headerName: "Remove", template: '<a href="#" onclick="Remove(this.parentNode.parentNode)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 80 }
        ];

        var rowData = [];

        var gridOptions = {
            columnDefs: columnDefs,
            rowData: rowData,
            onGridReady: function () {
                gridOptions.api.sizeColumnsToFit();
            }
        };
        var selectedfiles;
        function resetselectedfiles() {
            console.log('btnsbt');
            console.log(rowData);
            for (i = 0; i < rowData.length; i++) {
                selectedfiles = document.createElement("input");
                selectedfiles.setAttribute("type", "hidden");
                selectedfiles.setAttribute("name", "selectedfiles");
                selectedfiles.setAttribute("value", rowData[i].Name);
                document.getElementById("form1").appendChild(selectedfiles);
            }
        }
        var length;
        var PostedFiles;
        var fd = [];
        function showselectedfiles(fu) {
            debugger;
            //alert(fu.files[0].name);
            //$('#HDMRaiseSubRequest.UPLFILE') = fu.files[0].name;
            // s = $('#HDMRaiseSubRequest.UPLFILE');
            // alert(s);
            //clearing prev uploaded files from grid

            var gridLength = gridOptions.rowData.length;
            if (gridLength > 0) {
                for (j = 1; j <= gridLength; j++) {
                    rowData.splice(0, 1);
                    gridOptions.api.setRowData(rowData);
                }
            }

            //for (i = 0; i < $('#UPLFILE')[0].files.length; i++) {
            //        var file = $('#UPLFILE')[0].files[i];
            //    fd.append(file.name, file);
            //    //fd = file;
            //    //length = $('#UPLFILE')[0].files.length;
            //    fileName = file.name;
            //   // $scope.HDMRaiseSubRequest.UPLFILE = fd;
            //    }

            //Will hide
            //rowData = [];
            //console.log(fu.files);
            for (i = 0; i < fu.files.length; i++) {
                rowData.push({ Name: fu.files[i].name, file: fu.files[i] });
                //file = $('#UPLFILE')[i].files[i];
                //fileName = file.name;
                fd.push({ Name: fu.files[i].name, size: fu.files[i].size, type: fu.files[i].type });
                length = fu.files.length;
                console.log(length);
                //alert(PostedFiles);
                //alert(fileName);
                //fd.append(file.name, file);
            }
            gridOptions.api.setRowData(rowData);

            //alert($('#UPLFILE').val());
        }

        function Remove(node) {
            $("#fu1").val("");
            var ndx = parseInt(node.getAttribute("row"));
            rowData.splice(ndx, 1);
            gridOptions.api.setRowData(rowData);
        }

        document.addEventListener("DOMContentLoaded", function () {
            var eGridDiv = document.getElementById('myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);
        });

        function GetValueOnKeyPress() {
            var edValue = document.getElementById("txtEmpId");
            var s = edValue.value;
            var lblValue = document.getElementById("lblValue");
            lblValue.innerText = "The text box contains: " + s;
        }

    </script>--%>
    <script defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
        var gridLengthInitial;
        var fd = [];
        var columnDefs = [
            { headerName: "Uploaded Files", field: "Name", width: 350 },
            {
                headerName: "Image/File Name", field: "", cellRenderer: ghimages, suppressMenu: true, width: 250
            },
            {
                headerName: "Download", field: "Path", width: 250, cellRenderer: function (params) {
                    return '<a download="' + params.data.Name + '" href="' + params.data.Path + '"><span class="glyphicon glyphicon-download"></span></a>';
                }
            },
            { headerName: "Remove", template: '<a href="#" onclick="Remove(this.parentNode.parentNode)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 150 }
        ];

        function createImageSpan(image) {
            var resultElement = document.createElement("span");
            for (var i = 0; i < 1; i++) {
                var imageElement = document.createElement("img");
                imageElement.src = "../.." + image;
                imageElement.height = 50;
                imageElement.width = 50;
                resultElement.appendChild(imageElement);
            }
            return resultElement;
        }

        function ghimages(params) {
            //var Extn = params.data.Path.substr((params.data.Path.lastIndexOf('.') + 1)).toLowerCase();
            //var ExtArray = ['jpg', 'png', 'jpeg', 'gif', 'bmp'];
            //if ($.inArray(Extn, ExtArray) > -1) {
            //    return createImageSpan(params.data.Path);
            //}
            //else {
            //    return params.data.Name;
            //}
        }

        var rowData = [];

        var gridOptions = {
            columnDefs: columnDefs,
            rowData: rowData,
            rowHeight: 70,          //image size in ag grid
            enableColResize: true,
            suppressHorizontalScroll: true,
            onGridReady: function sizeToFit() {
                gridOptions.api.sizeColumnsToFit();
            }
        };

        function resetselectedfiles() {
            for (i = 0; i < rowData.length; i++) {
                var selectedfiles = document.createElement("input");
                selectedfiles.setAttribute("type", "hidden");
                selectedfiles.setAttribute("name", "selectedfiles");
                selectedfiles.setAttribute("value", rowData[i].Name);
                document.getElementById("form1").appendChild(selectedfiles);
            }
        }

        function showselectedfiles(fu) {
            if (gridLengthInitial < gridOptions.rowData.length) {
                for (j = gridLengthInitial; j <= gridOptions.rowData.length; j++) {
                    rowData.splice(gridLengthInitial, 1);
                    gridOptions.api.setRowData(rowData);
                }
            }
            for (i = 0; i < fu.files.length; i++) {
                rowData.push({ Name: fu.files[i].name, file: fu.files[i] });
                fd.push({ Name: fu.files[i].name, size: fu.files[i].size, type: fu.files[i].type });
            }
            gridOptions.api.setRowData(rowData);
        }

        function Remove(node) {
            var ndx = parseInt(node.getAttribute("row"));
            rowData.splice(ndx, 1);
            gridOptions.api.setRowData(rowData);
        }

        //$('#BtnModify').click(function (e) {
        // function Modify() {
        //    debugger;
        //    //$scope.UploadFile = function () {
        //    var Ext = $('#UPLFILE').val().split('.').pop().toLowerCase();
        //    console.log(Ext);
        //    var fname = $('#UPLFILE').val().split('\\');
        //    var str = fname[fname.length - 1];
        //    var ExtCount = str.split('.');
        //    var formData = new FormData();
        //    var UplFile = $('#UPLFILE')[0];
        //    formData.append("UplFile", UplFile.files[0]);
        ////     var urls = '../api/PropertyDBAPI/GetExpLeasesDetails';
        ////     var request = {
        ////         method: 'POST',
        ////         url:'../api/PropertyDBAPI/GetExpLeasesDetails',
        ////    //url: '../../App_Code/webapi/HelpDeskMaster/Controllers/HDMViewSubRequistionsController/UpdateRequest',
        ////    data: formData,
        ////    headers: {
        ////        'Content-Type': undefined
        ////    }
        ////};
        //     var param = { formData:formData };
        //    $.ajax({
        //        url: "../api/HDMViewSubRequistionsController/UpdateRequest",
        //        //url: "../api/PropertyDBAPI/BindProperties",
        //        data: param,
        //        //contentType: "application/json; charset=utf-8",
        //        type: "POST",
        //        dataType: 'json',
        //         cache : false,
        //        processData: false,
        //        success: function (data) {
        //            console.log(data);
        //            var respdata = JSON.parse(data);
        //            if (respdata.data != null) {
        //                console.log(respdata.data);
        //            }
        //            else {
        //                alert('hai');
        //                //showNotification('error', 8, 'bottom-right', respdata.Message);
        //                //cfpLoadingBar.complete();
        //            }
        //        }
        //    });
        //};


        //var jsonstringhdn = $scope.HDMViewModifySubRequesition.jsonstringhdn;
        //var jsonstringhdn = document.getElementById("jsonstringhdn");
        // document.addEventListener("DOMContentLoaded", function () {
        //     alert('hai');
        //     debugger;
        //     var eGridDiv = document.getElementById('myGrid');
        //     new agGrid.Grid(eGridDiv, gridOptions);
        //     //var jsonstringhdn = document.getElementById("jsonstringhdn");
        //     var jsonstring = jsonstringhdn.value;
        //     var jsonobj = JSON.parse(jsonstring);
        //     for (i = 0; i < jsonobj.length; i++) {
        //         rowData.push(jsonobj[i]);
        //     }
        //     gridOptions.api.setRowData(rowData);
        //     gridLengthInitial = gridOptions.rowData.length;
        // });

        //$(function () {
        //    var maybe = true;
        //    var text = $('#txtSpaceID').val();
        //    if (maybe) {
        //        $('#txtSpaceID').attr('title', text);
        //    }
        //});
    </script>

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMViewModifySubRequesitionsController" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Customized Report" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">View & Modify Sub Request</h3>
                    </div>

                    <div class="panel-body" style="padding-right: 10px;">
                        <form id="form1" name="HDMViewModifySubReq" novalidate>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMViewModifySubReq.$submitted && HDMViewModifySubReq.LCM_NAME.$invalid}">
                                        <label class="control-label">Location <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Locations" data-output-model="HDMViewModifySubRequesition.Locations" data-button-label="icon LCM_NAME"
                                            data-item-label="icon LCM_NAME maker" id="DDL"
                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single" data-is-disabled="EnableStatus != 0">
                                        </div>
                                        <input type="text" data-ng-model="HDMViewModifySubRequesition.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                        <span id="Error" class="error" data-ng-show="HDMViewModifySubReq.$submitted && HDMViewModifySubReq.LCM_NAME.$invalid" style="color: red;">Please Select Location</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMViewModifySubReq.$submitted && HDMViewModifySubReq.MNC_NAME.$invalid}">
                                        <label class="control-label">Main Category <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Main_Category" data-output-model="HDMViewModifySubRequesition.Main_Category" data-button-label="icon MNC_NAME"
                                            data-item-label="icon MNC_NAME maker" data-on-item-click="getsubbymain()" data-tick-property="ticked" data-max-labels="1" selection-mode="single" data-is-disabled="EnableStatus != 0">
                                        </div>
                                        <input type="text" data-ng-model="HDMViewModifySubRequesition.Main_Category[0]" name="MNC_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="HDMViewModifySubReq.$submitted && HDMViewModifySubReq.MNC_NAME.$invalid" style="color: red;">Please Select Main Category</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMViewModifySubReq.$submitted && HDMViewModifySubReq.SUBC_NAME.$invalid}">
                                        <label class="control-label">Sub Category <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Sub_Category" data-output-model="HDMViewModifySubRequesition.Sub_Category" data-button-label="icon SUBC_NAME"
                                            data-item-label="icon SUBC_NAME maker" data-on-item-click="getchildbysub()" data-tick-property="ticked" data-max-labels="1" selection-mode="single" data-is-disabled="EnableStatus != 0">
                                        </div>
                                        <input type="text" data-ng-model="HDMViewModifySubRequesition.Sub_Category[0]" name="SUBC_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="HDMViewModifySubReq.$submitted && HDMViewModifySubReq.SUBC_NAME.$invalid" style="color: red;">Please Select Sub Category</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMViewModifySubReq.$submitted && HDMViewModifySubReq.CHC_TYPE_NAME.$invalid}">
                                        <label class="control-label">Child Category <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="child_Category" data-output-model="HDMViewModifySubRequesition.child_Category" data-button-label="icon CHC_TYPE_NAME"
                                            data-item-label="icon CHC_TYPE_NAME maker" data-on-item-click="childchange()" data-tick-property="ticked" data-max-labels="1" selection-mode="single" data-is-disabled="EnableStatus != 0">
                                        </div>
                                        <input type="text" data-ng-model="HDMViewModifySubRequesition.child_Category[0]" name="CHC_TYPE_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="HDMViewModifySubReq.$submitted && HDMViewModifySubReq.CHC_TYPE_NAME.$invalid" style="color: red;">Please Select Child Category</span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Mobile/Extension Number</label>
                                        <div class="control-label" id='Mobile'>
                                            <input type="text" class="form-control" data-ng-model="HDMViewModifySubRequesition.Mobile" id="txtMobile" name="Mobile" maxlength="12" data-ng-disabled="EnableStatus !=0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMViewModifySubReq.$submitted && HDMViewModifySubReq.CustID.$invalid}">
                                        <label for="txtcode">Customer ID <span style="color: red;">*</span></label>
                                        <div class="control-label" id='CustID'>
                                            <input type="text" class="form-control" data-ng-model="HDMViewModifySubRequesition.CustID" id="txtCustID" name="CustID" required="" data-ng-disabled="EnableStatus !=0" />
                                            <span class="error" data-ng-show="HDMViewModifySubReq.$submitted && HDMViewModifySubReq.CustID.$invalid" style="color: red;">Please Enter Customer ID</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMViewModifySubReq.$submitted && HDMViewModifySubReq.CustName.$invalid}">
                                        <label for="txtcode">Customer Name <span style="color: red;">*</span></label>
                                        <div class="control-label" id='CustName'>
                                            <input type="text" class="form-control" data-ng-model="HDMViewModifySubRequesition.CustName" id="txtCustName" name="CustName" required="" data-ng-disabled="EnableStatus !=0" />
                                            <span class="error" data-ng-show="HDMViewModifySubReq.$submitted && HDMViewModifySubReq.CustName.$invalid" style="color: red;">Please Enter Customer Name</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMViewModifySubReq.$submitted && HDMViewModifySubReq.BilledBy.$invalid}">
                                        <label for="txtcode">BilledBy <span style="color: red;">*</span></label>
                                        <div class="control-label" id='BilledBy'>
                                            <input type="text" class="form-control" data-ng-model="HDMViewModifySubRequesition.BilledBy" id="txtBilledBy" name="BilledBy" required="" data-ng-disabled="EnableStatus !=0" />
                                            <span class="error" data-ng-show="HDMViewModifySubReq.$submitted && HDMViewModifySubReq.BilledBy.$invalid" style="color: red;">Please Enter Billed By</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMViewModifySubReq.$submitted && HDMViewModifySubReq.InvoiceNo.$invalid}">
                                        <label for="txtcode">InvoiceNo <span style="color: red;">*</span></label>
                                        <div class="control-label" id='InvoiceNo'>
                                            <input type="text" class="form-control" data-ng-model="HDMViewModifySubRequesition.InvoiceNo" id="txtInvoiceNo" name="InvoiceNo" required="" data-ng-disabled="EnableStatus !=0" />
                                            <span class="error" data-ng-show="HDMRaiseHDMViewModifySubReqSubReq.$submitted && HDMViewModifySubReq.InvoiceNo.$invalid" style="color: red;">Please Enter InvoiceNo</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMViewModifySubReq.$submitted && HDMViewModifySubReq.InvoiceDate.$invalid}">
                                        <label for="txtcode">Invoice Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" id='Invoicedat'>
                                            <input type="text" class="form-control" data-ng-model="HDMViewModifySubRequesition.InvoiceDate" id="InvoiceDate" name="InvoiceDate" required="" placeholder="mm/dd/yyyy" data-ng-disabled="EnableStatus !=0" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('Invoicedat')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="HDMViewModifySubReq.$submitted && HDMViewModifySubReq.InvoiceDate.$invalid" style="color: red;">Please Select Invoice Date</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <label for="txtcode" class="custom-file">Upload Images/Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                    <input multiple type="file" name="UPLFILE" class="form-control" data-ng-model="HDMViewModifySubRequesition.UPLFILE[0]" id="UPLFILE" accept=".xls,.xlsx" class="custom-file-input" onchange="showselectedfiles(this)" data-ng-disabled="EnableStatus !=0">
                                    <span class="custom-file-control"></span>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMViewModifySubReq.$submitted && HDMViewModifySubReq.txtProbDesc.$invalid}">
                                        <label for="txtcode">Description/ Service and location <span style="color: red;">*</span></label>
                                        <div class="control-label" id='Descriptions'>
                                            <textarea name="txtProbDesc" class="form-control" data-ng-model="HDMViewModifySubRequesition.ProbDesc" cols="40" rows="5" required="" data-ng-disabled="EnableStatus !=0"></textarea>
                                            <span class="error" data-ng-show="HDMViewModifySubReq.$submitted && HDMViewModifySubReq.txtProbDesc.$invalid" style="color: red;">Please Enter Description</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix" data-ng-hide="CommentsHide">
                                <div class="col-md-3 col-sm-6 col-xs-12" data-ng-hide="StatusHide">
                                    <div class="form-group" data-ng-class="{'has-error': HDMViewModifySubReq.$submitted && HDMViewModifySubReq.SER_APP_STA_REMARKS_NAME.$invalid}">
                                        <label class="control-label">Status <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="ApprovalStatus" data-output-model="HDMViewModifySubRequesition.ApprovalStatus" data-button-label="icon SER_APP_STA_REMARKS_NAME"
                                            data-item-label="icon SER_APP_STA_REMARKS_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMViewModifySubRequesition.ApprovalStatus[0]" name="SER_APP_STA_REMARKS_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="HDMViewModifySubReq.$submitted && HDMViewModifySubReq.SER_APP_STA_REMARKS_NAME.$invalid" style="color: red;">Please Select Approval Status</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <%--data-ng-class="{'has-error': HDMViewModifySubReq.$submitted && HDMViewModifySubReq.txtComments.$invalid}"--%>
                                        <label for="txtcode">Comments <span style="color: red;"></span></label>
                                        <div class="control-label" id='Description'>
                                            <textarea name="txtComments" class="form-control" data-ng-model="HDMViewModifySubRequesition.Comments" cols="40" rows="5" required=""></textarea>
                                            <%--<span class="error" data-ng-show="HDMViewModifySubReq.$submitted && HDMViewModifySubReq.Comments.$invalid" style="color: red;">Please Enter Approval Comments</span>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                            </div>
                            <div class="clearfix">
                                <div class="col-md-6 col-sm-12 col-xs-12 text-right" style="margin-left: 500px; margin-top: 20px;">
                                    <div class="form-group">
                                        <input type="submit" value="Approve" data-ng-click="ApproveEmpDetails()" class="btn btn-primary custom-button-color" ng-hide="ApproveEmp" />
                                        <input type="submit" id="BtnModify" value="Modify" data-ng-click="ModifyEmpDetails()" class="btn btn-primary custom-button-color" ng-hide="ModifyEmp" />
                                        <input type="submit" value="Cancel" data-ng-click="CancelRequest()" class="btn btn-primary custom-button-color" ng-hide="CancelEmp" />
                                        <input type="submit" value="Back" data-ng-click="Redirect()" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                            </div>
                            <%--<div class="col-md-1 col-sm-6 col-xs-12" style="margin-left: 1100px"   data-ng-click="ModifyEmpDetails()">
                                    <br />
                                    <div class="col-md-6 col-sm-12 col-xs-12 text-right">
                                        <div class="form-group">
                                            <input type="submit" value="Modify" data-ng-click="ModifyEmpDetails()" class="btn btn-primary custom-button-color" />
                                            <input type="submit" value="Cancel" data-ng-click="ModifyEmpDetails()" class="btn btn-primary custom-button-color" />
                                            <input type="submit" value="Back" data-ng-click="ModifyEmpDetails()" class="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                    <%-- <div class="box-footer">
                                        <input type="submit" value="Modify" data-ng-click="ModifyEmpDetails()" class="btn btn-primary custom-button-color" />
                                        <input type="submit" value="Cancel" data-ng-click="ModifyEmpDetails()" class="btn btn-primary custom-button-color" />
                                        <input type="submit" value="Back" data-ng-click="ModifyEmpDetails()" class="btn btn-primary custom-button-color" />
                                    </div>--%>

                            <div class="clearfix">
                                <div class="col-md-6">
                                    <a id="lnkShowEscaltion" href="#" onclick="showPopWin()" runat="server" hidden="">Click here to view SLA & Escalation</a>
                                </div>
                                <div class="col-md-12">
                                    <div id="myGrid" data-ng-model="HDMViewModifySubRequesition.myGrid" style="height: 100px; width: 400px" class="ag-blue"></div>
                                </div>
                            </div>
                            <%--  <div class="clearfix">
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 100px; width: auto"></div>
                                        </div>
                                    </div>
                                </div>--%>
                        </form>

                        <form id="form2">
                            <div>

                                <div class="clearfix">
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="hidden" id="jsonstringhdn" runat="server" data-ng-model="HDMViewModifySubRequesition.jsonstringhdn" />
                                            <%--<label id="jsonstringhdn" data-ng-model="HDMViewModifySubRequesition.jsonstringhdn"></label>--%>
                                            <%--<div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>--%>

                                            <div id="myGridData" data-ng-model="HDMViewModifySubRequesition.myGridData" style="height: 250px; width: 100%" class="ag-blue"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <%--  <form id="form2">--%>
                        <%--<div data-ng-show="GridVisiblity">--%>
                        <%-- <div class="row" style="padding-left: 30px">--%>
                        <%--<div class="clearfix">
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 100px; width: auto"></div>
                                            </div>
                                        </div>
                                    </div>--%>
                        <%--</div>--%>
                        <%-- </form>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--</div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>

    <script src="../../../../SMViews/Utility.js" defer></script>
    <script src="../../../../Scripts/moment.min.js" defer></script>
    <script src="../JS/HDMViewModifySubRequesitions.js" defer></script>
    <script defer>
        var CompanySession = '<%= Session["COMPANYID"]%>';
        //var RequestId;
        //$(document).ready(function () {
        //     alert('hi');
        //    debugger;
        //    RequestId = getUrlParameter('RequestId');
        //    function getUrlParameter(param, dummyPath) {
        //        var sPageURL = dummyPath || window.location.search.substring(1),
        //            sURLVariables = sPageURL.split(/[&||?]/),
        //            res;

        //        for (var i = 0; i < sURLVariables.length; i += 1) {
        //            var paramName = sURLVariables[i],
        //                sParameterName = (paramName || '').split('=');

        //            if (sParameterName[0] === param) {
        //                res = sParameterName[1];
        //            }
        //        }

        //        return res;
        //    }
        //    alert(RequestId);
        //});

        //function setDateVals() {
        //$('#Billdate').datepicker({
        //    format: 'dd-M-yyyy',
        //    autoclose: true,
        //    todayHighlight: true
        //});
        //$('#InvoiceDate').datepicker({
        //    format: 'dd-M-yyyy',
        //    autoclose: true,
        //    todayHighlight: true
        //});
        //$('#Billdate').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY')));
        //$('#InvoiceDate').datepicker('setDate', new Date(moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY')));
        //}
    </script>
</body>
</html>
