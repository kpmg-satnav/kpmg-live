﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports clsSubSonicCommonFunctions
Partial Class HDM_HDM_Webfiles_frmRMViewRequisitions
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Public Shared CostStatus As Boolean
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then

            ' Check Labour/Spare Costs visible status
            Dim spCS As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
            spCS.Command.AddParameter("@TYPE ", 4, DbType.Int32)
            Dim ChkCostSts = spCS.ExecuteScalar()
            CostStatus = IIf(ChkCostSts = 1, True, False)

            BindGrid()
            BindUnassignedGrid()
            BindLocations()
            BindChildCategory()
            BindSubCategory()
            BindMainCategory()
            FillStatus()
            If gvViewRequisitions.Rows.Count = 0 Then
                showDiv.Visible = False
                btnUpdate1.Visible = False
            Else
                showDiv.Visible = True
                btnUpdate1.Visible = True
            End If
            If gvUnassigned.Rows.Count = 0 Then
                showDiv2.Visible = False
                btnUpdate2.Visible = False
            Else
                showDiv2.Visible = True
                btnUpdate2.Visible = True
            End If
            If Request.QueryString("Updated") = 1 Then
                lblMessage.Text = "Request Updated Successfully"
            Else
                lblMessage.Text = ""
            End If
        End If
    End Sub

    Private Sub FillStatus()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_HDMSTATUS_FILTERS")
        sp.Command.AddParameter("@TYPE", 2, Data.DbType.String)
        ddlStatus.DataSource = sp.GetDataSet
        ddlStatus.DataTextField = "STA_TITLE"
        ddlStatus.DataValueField = "STA_ID"
        ddlStatus.DataBind()
        ddlStatus.Items.Insert(0, "--Select--")
    End Sub

    Private Sub BindGrid()
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_PENDING_REQUESTS_INCHARGE")
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        sp.Command.AddParameter("@CompanyId", Session("COMPANYID"), Data.DbType.String)
        ds = sp.GetDataSet
        gvViewRequisitions.DataSource = ds
        gvViewRequisitions.DataBind()
        Dim ds1 As New DataSet
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)
        ds1 = spSts.GetDataSet()

        For i As Integer = 0 To gvViewRequisitions.Rows.Count - 1
            Dim ddlstat As DropDownList = CType(gvViewRequisitions.Rows(i).FindControl("ddlModifyStatus"), DropDownList)
            ddlstat.DataSource = ds1
            ddlstat.DataTextField = "STA_TITLE"
            ddlstat.DataValueField = "STA_ID"
            ddlstat.DataBind()
            ddlstat.ClearSelection()
            gvViewRequisitions.HeaderRow.Cells(18).Visible = CostStatus
            gvViewRequisitions.HeaderRow.Cells(19).Visible = CostStatus
            gvViewRequisitions.Rows(i).Cells(18).Visible = CostStatus
            gvViewRequisitions.Rows(i).Cells(19).Visible = CostStatus
        Next
    End Sub

    Private Sub BindUnassignedGrid()
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_PENDING_UNASSIGNED_REQUESTS_INCHARGE")
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        sp.Command.AddParameter("@CompanyId", Session("COMPANYID"), Data.DbType.String)
        ds = sp.GetDataSet
        gvUnassigned.DataSource = ds
        gvUnassigned.DataBind()
        Dim ds1 As New DataSet
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)
        ds1 = spSts.GetDataSet()
        For i As Integer = 0 To gvUnassigned.Rows.Count - 1
            Dim ddlAssignStatus As DropDownList = CType(gvUnassigned.Rows(i).FindControl("ddlAssignStatus"), DropDownList)
            ddlAssignStatus.DataSource = ds1
            ddlAssignStatus.DataTextField = "STA_TITLE"
            ddlAssignStatus.DataValueField = "STA_ID"
            ddlAssignStatus.DataBind()
            ddlAssignStatus.ClearSelection()

            gvUnassigned.HeaderRow.Cells(17).Visible = CostStatus
            gvUnassigned.HeaderRow.Cells(18).Visible = CostStatus
            gvUnassigned.Rows(i).Cells(17).Visible = CostStatus
            gvUnassigned.Rows(i).Cells(18).Visible = CostStatus
        Next
    End Sub

    Protected Sub gvViewRequisitions_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvViewRequisitions.PageIndexChanging
        gvViewRequisitions.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub btnUpdate1_Click(sender As Object, e As EventArgs) Handles btnUpdate1.Click
        Try
            Dim ds As New DataSet
            For Each row As GridViewRow In gvViewRequisitions.Rows
                Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                Dim lblreqid As Label = DirectCast(row.FindControl("lblreqid"), Label)
                Dim txtUpdateRemarks As TextBox = DirectCast(row.FindControl("txtUpdateRemarks"), TextBox)
                Dim ddlStatus As DropDownList = DirectCast(row.FindControl("ddlModifyStatus"), DropDownList)
                Dim txtAC As TextBox = DirectCast(row.FindControl("txtADC"), TextBox)
                Dim txtLBR As TextBox = DirectCast(row.FindControl("txtLBC"), TextBox)
                Dim txtSPR As TextBox = DirectCast(row.FindControl("txtSPC"), TextBox)
                Dim txtClaim = row.Cells(9).Text.Trim()

                Dim claim As String
                If txtClaim Is Nothing Then
                    claim = ""
                Else
                    claim = txtClaim
                End If
                If chkselect.Checked = True Then
                    Dim param As SqlParameter() = New SqlParameter(7) {}
                    param(0) = New SqlParameter("@REQ_ID", lblreqid.Text)
                    param(1) = New SqlParameter("@STA_ID", ddlStatus.SelectedValue)
                    param(2) = New SqlParameter("@UPDATE_REMARKS", txtUpdateRemarks.Text)
                    param(3) = New SqlParameter("@USR_ID", Session("UID").ToString())
                    param(4) = New SqlParameter("@ADDN_COST", txtAC.Text)
                    param(5) = New SqlParameter("@LBR_COST", txtLBR.Text)
                    param(6) = New SqlParameter("@SPR_COST", txtSPR.Text)
                    param(7) = New SqlParameter("@CLA_AMT", claim)
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_VIEW_UPDATE_REQ_UPDATE", param)
                End If
            Next
            lblMessage.Text = "Request Updated Successfully"
            BindGrid()
        Catch ex As Exception

        End Try
        lblMessage.Visible = True

    End Sub

    Protected Sub btnUpdate2_Click(sender As Object, e As EventArgs) Handles btnUpdate2.Click
        Try
            Dim ds As New DataSet
            For Each row As GridViewRow In gvUnassigned.Rows
                Dim chkSelect1 As CheckBox = DirectCast(row.FindControl("chkSelect1"), CheckBox)
                Dim lblreqid As Label = DirectCast(row.FindControl("lblreqid"), Label)
                Dim txtUpdate2Remarks As TextBox = DirectCast(row.FindControl("txtUpdate2Remarks"), TextBox)
                Dim ddlStatus2 As DropDownList = DirectCast(row.FindControl("ddlAssignStatus"), DropDownList)

                Dim txtUpdateRemarks As TextBox = DirectCast(row.FindControl("txtUpdateRemarks"), TextBox)
                Dim txtAC As TextBox = DirectCast(row.FindControl("txtADC"), TextBox)
                Dim txtLBC As TextBox = DirectCast(row.FindControl("txtLBC"), TextBox)
                Dim txtSC As TextBox = DirectCast(row.FindControl("txtSPC"), TextBox)

                If chkSelect1.Checked = True Then
                    Dim param As SqlParameter() = New SqlParameter(4) {}
                    param(0) = New SqlParameter("@REQ_ID", lblreqid.Text)
                    param(1) = New SqlParameter("@STA_ID", ddlStatus2.SelectedValue)
                    param(2) = New SqlParameter("@UPDATE_REMARKS", txtUpdate2Remarks.Text)
                    param(3) = New SqlParameter("@USR_ID", Session("UID").ToString())
                    param(4) = New SqlParameter("@ADDN_COST", txtAC.Text)
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_UPDATE_UNASSIGNED_REQUESTS", param)
                End If
            Next
            lblMessage.Visible = True
            lblMessage.Text = "Request Updated Successfully"
        Catch ex As Exception

        End Try
        BindUnassignedGrid()
        BindGrid()
    End Sub

    Public Sub BindLocations()
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
        ObjSubsonic.Binddropdown(ddlLocaton1, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindChildCategory()
        'ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
        ObjSubsonic.Binddropdown(ddlChildCategroy1, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
    End Sub

    Public Sub BindSubCategory()
        'ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlSubCatMR, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE")
        ObjSubsonic.Binddropdown(ddlSubCatUAReq, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE")
    End Sub
    Public Sub BindMainCategory()
        'ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlManinCatMR, "HDM_GET_ACTIVE_MAIN_CATEGORY", "MNC_NAME", "MNC_CODE")
        ObjSubsonic.Binddropdown(ddlManinCatMR1, "HDM_GET_ACTIVE_MAIN_CATEGORY", "MNC_NAME", "MNC_CODE")
    End Sub

    Protected Sub txtReqIdFilter_Click(sender As Object, e As EventArgs) Handles txtReqIdFilter.Click
        Dim DS As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_SEARCH_PENDING_REQUESTS_INCHARGE")
        sp.Command.AddParameter("@REQID", txtReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@LOCATION", ddlLocaton1.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@CATEGORY", ddlChildCategroy1.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@SUBCATEGORY", ddlSubCatMR.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@MAINCATEGORY", ddlManinCatMR.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@FROMDATE", txtfromDt.Text, DbType.String)
        sp.Command.AddParameter("@TODATE", txtToDt.Text, DbType.String)
        sp.Command.AddParameter("@STATUS", ddlStatus.SelectedValue, DbType.String)
        sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), Data.DbType.String)
        DS = sp.GetDataSet
        gvViewRequisitions.DataSource = DS
        gvViewRequisitions.DataBind()


        Dim ds1 As New DataSet
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)
        ds1 = spSts.GetDataSet()
        For i As Integer = 0 To gvViewRequisitions.Rows.Count - 1
            Dim ddlstat As DropDownList = CType(gvViewRequisitions.Rows(i).FindControl("ddlModifyStatus"), DropDownList)
            ddlstat.DataSource = ds1
            ddlstat.DataTextField = "STA_TITLE"
            ddlstat.DataValueField = "STA_ID"
            ddlstat.DataBind()
            ddlstat.ClearSelection()
        Next
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim DS As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_SEARCH_PENDING_UNASSIGNED_REQUESTS_INCHARGE")
        sp.Command.AddParameter("@REQID", textReqID2.Text, Data.DbType.String)
        sp.Command.AddParameter("@LOCATION", ddlLocation.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@CATEGORY", ddlChildCategory.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@SUBCATEGORY", ddlSubCatUAReq.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@MAINCATEGORY", ddlManinCatMR1.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@FROMDATE", txtUAfromDt.Text, DbType.String)
        sp.Command.AddParameter("@TODATE", txtUAtoDt.Text, DbType.String)
        sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), Data.DbType.String)
        DS = sp.GetDataSet
        gvUnassigned.DataSource = DS
        gvUnassigned.DataBind()

        Dim ds1 As New DataSet
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_STATUS_BYROLE")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)
        ds1 = spSts.GetDataSet()
        For i As Integer = 0 To gvUnassigned.Rows.Count - 1
            Dim ddlAssignStatus As DropDownList = CType(gvUnassigned.Rows(i).FindControl("ddlAssignStatus"), DropDownList)
            ddlAssignStatus.DataSource = ds1
            ddlAssignStatus.DataTextField = "STA_TITLE"
            ddlAssignStatus.DataValueField = "STA_ID"
            ddlAssignStatus.DataBind()
            ddlAssignStatus.ClearSelection()
        Next
    End Sub

    Protected Sub gvUnassigned_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvUnassigned.PageIndexChanging
        gvUnassigned.PageIndex = e.NewPageIndex()
        BindUnassignedGrid()
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            Dim ddl_status As DropDownList = DirectCast(sender, DropDownList)
            Dim row As GridViewRow = DirectCast(ddl_status.Parent.Parent, GridViewRow)
            Dim txtadncost As TextBox = DirectCast(row.FindControl("txtADC"), TextBox)
            Dim lbladncost As Label = DirectCast(row.FindControl("lblADC"), Label)

            Dim txtLbrcost As TextBox
            Dim lblLbrcost As Label

            Dim txtSprcost As TextBox
            Dim lblSprcost As Label

            '  If CostStatus = True Then
            txtLbrcost = DirectCast(row.FindControl("txtLBC"), TextBox)
            lblLbrcost = DirectCast(row.FindControl("lblLBC"), Label)

            txtSprcost = DirectCast(row.FindControl("txtSPC"), TextBox)
            lblSprcost = DirectCast(row.FindControl("lblSPC"), Label)

            If ddl_status.SelectedValue = "9" Then
                txtadncost.Visible = True
                lbladncost.Visible = False

                If CostStatus = True Then
                    txtLbrcost.Visible = True
                    lblLbrcost.Visible = False

                    txtSprcost.Visible = True
                    lblSprcost.Visible = False
                Else
                    txtLbrcost.Visible = False
                    lblLbrcost.Visible = False 'True

                    txtSprcost.Visible = False
                    lblSprcost.Visible = False 'True

                End If

            Else
                txtadncost.Visible = False
                lbladncost.Visible = True

                If CostStatus = True Then
                    lblLbrcost.Visible = True
                    txtLbrcost.Visible = False

                    lblSprcost.Visible = True
                    txtSprcost.Visible = False
                    'Else
                    '    lblLbrcost.Visible = False
                    '    txtLbrcost.Visible = False 'True

                    '    lblSprcost.Visible = False
                    '    txtSprcost.Visible = False 'True
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlSubCatMR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSubCatMR.SelectedIndexChanged
        ddlChildCategroy1.Items.Clear()
        If ddlSubCatMR.SelectedIndex > 0 Then
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@SUBC_CODE", SqlDbType.VarChar, 50)
            param(0).Value = ddlSubCatMR.SelectedValue
            ObjSubsonic.Binddropdown(ddlChildCategroy1, "HDM_GET_CHILDBY_SUBCATG", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param)
        Else
            ObjSubsonic.Binddropdown(ddlChildCategroy1, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
        End If
    End Sub
    Protected Sub ddlManinCatMR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlManinCatMR.SelectedIndexChanged
        ddlSubCatMR.Items.Clear()
        If ddlManinCatMR.SelectedIndex > 0 Then
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@MNC_CODE", SqlDbType.VarChar, 50)
            param(0).Value = ddlManinCatMR.SelectedValue
            ObjSubsonic.Binddropdown(ddlSubCatMR, "HDM_GET_ALLSUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param)
        Else
            ObjSubsonic.Binddropdown(ddlSubCatMR, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE")
        End If
    End Sub
    Protected Sub ddlManinCatMR1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlManinCatMR1.SelectedIndexChanged
        ddlSubCatUAReq.Items.Clear()
        If ddlManinCatMR1.SelectedIndex > 0 Then
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@MNC_CODE", SqlDbType.VarChar, 50)
            param(0).Value = ddlManinCatMR1.SelectedValue
            ObjSubsonic.Binddropdown(ddlSubCatUAReq, "HDM_GET_ALLSUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param)
        Else
            ObjSubsonic.Binddropdown(ddlSubCatUAReq, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE")
        End If
    End Sub

    Protected Sub ddlSubCatUAReq_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSubCatUAReq.SelectedIndexChanged
        ddlChildCategory.Items.Clear()
        If ddlSubCatUAReq.SelectedIndex > 0 Then
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@SUBC_CODE", SqlDbType.VarChar, 50)
            param(0).Value = ddlSubCatUAReq.SelectedValue
            ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_CHILDBY_SUBCATG", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param)
        Else
            ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
        End If
    End Sub
End Class
