﻿<%@ Page Title="Admin View Requisitions" Language="VB" AutoEventWireup="false" CodeFile="frmHDMAdminViewRequisitions.aspx.vb"
    Inherits="HDM_HDM_Webfiles_frmHDMAdminViewRequisitions" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        function refreshSelectpicker() {
            $("#<%=ddlLocation.ClientID%>").selectpicker();
             $("#<%=ddlManinCatMR.ClientID%>").selectpicker();
             $("#<%=ddlSubCatMR.ClientID%>").selectpicker();
             $("#<%=ddlChildCategory.ClientID%>").selectpicker();
             $("#<%=ddlStatus.ClientID%>").selectpicker();
         }
         refreshSelectpicker();
    </script>
    <style type="text/css">
        .custom-button-color {
            height: 26px;
        }
    </style>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="View and Assign Requests" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">View & Assign</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="CityPanel1" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMessage" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="showDiv" runat="server">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtid">Enter Request Id </label>
                                                <asp:TextBox ID="txtReqId" runat="server" Style="padding-right: 20px" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtloc">Location</label>
                                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcat">Main Category</label>
                                                <asp:DropDownList ID="ddlManinCatMR" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcat">Sub Category</label>
                                                <asp:DropDownList ID="ddlSubCatMR" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcat">Child Category</label>
                                                <asp:DropDownList ID="ddlChildCategory" runat="server" CssClass="selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>




                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcat">Select Status</label>
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtid">Enter From Date</label>
                                                <div class='input-group date' id='fromdate'>
                                                    <asp:TextBox ID="txtfromDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtloc">Enter To Date</label>
                                                <div class='input-group date' id='toDt'>
                                                    <asp:TextBox ID="txtToDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('toDt')"></span>
                                                    </span>
                                                </div>
                                                <asp:CompareValidator ID="CompareValidator1" ErrorMessage="To date should be greater than From date" ControlToValidate="txtToDt" ControlToCompare="txtfromDt" Type="Date" Operator="GreaterThan" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="col-md-12 text-right">
                                                <br />
                                                <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                                                    CausesValidation="true" TabIndex="2" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="horizontal-scroll">
                                    <div class="form-group col-md-12">
                                        <asp:GridView ID="gvViewRequisitions" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                            EmptyDataText="No Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped" OnRowDataBound="OnRowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Request Id">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("REQUEST_ID", "~/HelpdeskManagement/Views/frmHDMViewAssignRequisition.aspx?RID={0}")%>'>
                                                            <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQUEST_ID") %>'></asp:Label>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="REQUESTED_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="REQUESTED_BY" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="LOCATION" HeaderText="Location" ItemStyle-HorizontalAlign="left" />

                                                <asp:BoundField DataField="MAIN_CATEGORY" HeaderText="Main Category" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="SUB_CATEGORY" HeaderText="Sub Category" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="CHILD_CATEGORY" HeaderText="Child Category" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="STATUS" HeaderText="Current Status" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="ASSIGNED_TO" HeaderText="Assigned To" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="CONTACT_NO" HeaderText="Contact No" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="SER_CLAIM_AMT" HeaderText="Claim Amount" ItemStyle-HorizontalAlign="left" />
                                                <asp:TemplateField HeaderText="Re-Assign">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlAssignTo" runat="server" CssClass="selectpicker" Width="70px">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remarks">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtUpdateRemarks" runat="server" TextMode="MultiLine" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ESC_COUNT" HeaderText="No. Of Escalations" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="TOTAL_TIME" HeaderText="Time Taken" ItemStyle-HorizontalAlign="left" />
                                                <asp:TemplateField HeaderText="Location" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("LCM_CODE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Main" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMain" runat="server" Text='<%# Eval("MNC_CODE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sub" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSub" runat="server" Text='<%# Eval("SUBC_CODE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Child" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChild" runat="server" Text='<%# Eval("CHC_TYPE_CODE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>

                                <%-- <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" Style="display: none">
                                    <div style="height: 60px;">

                                        <div class="row">

                                            <div class="col-md-4">
                                                <label>Total Records </label>
                                                <asp:Label ID="totalnumberofrecords" runat="server"></asp:Label>
                                            </div>
                                            <div class="col-md-8">
                                                Search by Any     
                                                <asp:TextBox ID="txtSearchVal" runat="server"></asp:TextBox>--%>
                                <%--<asp:Button ID="btnSearchItems" runat="server" Text="Submit" OnClick="btnSearchItems_Click" />--%>
                                <%-- </div>
                                          </div>
                                        <div class="clearfix">
                                            <div class="col-md-7">--%>
                                <%-- <asp:Button ID="Btn_Previous" CommandName="Previous"
                                                    runat="server" OnCommand="ChangePage"
                                                    Text="Previous" />
                                                Page --%>
                                <%-- <asp:Label runat="server" ID="lblCurrentPage"></asp:Label>
                                                of 
                                                <asp:Label runat="server" ID="lblTotalPages"></asp:Label>--%>

                                <%--<asp:Button ID="Btn_Next" runat="server" CommandName="Next"
                                                    OnCommand="ChangePage" Text="Next" />--%>
                                <%--</div>

                                        </div>
                                    </div>
                                </asp:Panel>--%>
                                <div class="row" style="padding-top: 18px;">
                                    <%-- <div class="col-md-6 col-sm-12 col-xs-12"></div>
                                    <div class="col-md-3 col-sm-12 col-xs-12 text-left">
                                        <div class="form-group">
                                            <label for="txtcode">Remarks</label>
                                            <asp:TextBox ID="txtUpdateRemarks" Visible="false" runat="server" CssClass="form-control" Rows="3" TabIndex="15" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div><br /><br /><br />--%>
                                    <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="Button1" CssClass="btn btn-primary custom-button-color" runat="server" Text="Bulk Assign" CausesValidation="true" TabIndex="17" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


