﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSLADetails.aspx.vb" Inherits="HDM_HDM_Webfiles_frmSLADetails" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="SLA Details" ba-panel-class="with-scroll" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h4>SLA Details</h4>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <div class="row form-inline">
                            <div class="form-group col-md-12">
                                <asp:GridView ID="gvSLA" runat="server" AutoGenerateColumns="true"
                                    EmptyDataText="SLA Not Defined. Please Define SLA." CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                        <br />
                        <h4>Service Escalation Mapping Details</h4>
                        <div class="row form-inline">
                            <div class="form-group col-md-12">
                                <asp:GridView ID="gvSLAMapped" runat="server" AutoGenerateColumns="false"
                                    EmptyDataText="Service Escalation Not Defined. Please Define Service Escalation." CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:BoundField DataField="ROL_DESC" HeaderText="Role" ItemStyle-HorizontalAlign="left" />
                                        <asp:BoundField DataField="USERS" HeaderText="Employee ID" ItemStyle-HorizontalAlign="left" />
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
