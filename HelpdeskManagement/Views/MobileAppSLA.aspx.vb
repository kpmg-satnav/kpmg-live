﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class HelpdeskManagement_Views_MobileAppSLA
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
       
        If Not Page.IsPostBack Then

            Dim ServiceType As String
            Dim APIKey As String
            Dim UserLoc As String
            Dim Valcnt As Integer
            Dim Categories As DataSet

            ServiceType = Request.QueryString("ServiceType")
            APIKey = Request.QueryString("APIKey")
            Categories = GetCategories(ServiceType, APIKey)
            UserLoc = GetuserLocation(APIKey)
            Valcnt = ValidateAPI(APIKey)
            If Valcnt = 1 Then
                Dim VALDB As ValidateKey = _ValidateAPIKey(APIKey)
                'param = New SqlParameter(4) {}
                'param(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
                'param(0).Value = UserLoc
                'param(1) = New SqlParameter("@MAIN_CAT_CODE", SqlDbType.NVarChar, 200)
                'param(1).Value = Categories.Tables(0).Rows(0)("MAINCAT").ToString()
                'param(2) = New SqlParameter("@SUB_CAT_CODE", SqlDbType.NVarChar, 200)
                'param(2).Value = Categories.Tables(0).Rows(0)("SUBCAT").ToString()
                'param(3) = New SqlParameter("@CHILD_CAT_CODE", SqlDbType.NVarChar, 200)
                'param(3).Value = Categories.Tables(0).Rows(0)("CHILDCAT").ToString()
                'param(4) = New SqlParameter("@COMPANY_ID", SqlDbType.Int)
                'param(4).Value = Session("COMPANYID")
                'Dim ds As New DataSet
                'ds = ObjSubSonic.GetSubSonicDataSet("HDM_GET_SLA_DETAILS_BY_LOCATION", param)
                Dim sp As New SubSonic.StoredProcedure(VALDB.TENANT_ID & "." & "HDM_GET_SLA_DETAILS_BY_LOCATION")
                sp.Command.AddParameter("@LOC_CODE", VALDB.AUR_LOCATION, DbType.String)
                sp.Command.AddParameter("@MAIN_CAT_CODE", Categories.Tables(0).Rows(0)("MAINCAT").ToString(), DbType.String)
                sp.Command.AddParameter("@SUB_CAT_CODE", Categories.Tables(0).Rows(0)("SUBCAT").ToString(), DbType.String)
                sp.Command.AddParameter("@CHILD_CAT_CODE", Categories.Tables(0).Rows(0)("CHILDCAT").ToString(), DbType.String)
                sp.Command.AddParameter("@COMPANY_ID", VALDB.COMPANYID, DbType.String)
                Dim ds As DataSet
                ds = sp.GetDataSet()
                If ds.Tables(0).Columns.Count > 1 And ds.Tables(0).Rows.Count > 0 Then
                    gvSLA.DataSource = ds.Tables(0)
                    gvSLA.DataBind()
                Else
                    gvSLA.DataSource = Nothing
                    gvSLA.DataBind()
                End If
                gvSLAMapped.DataSource = ds.Tables(1)
                gvSLAMapped.DataBind()
            End If
        Else
            gvSLA.EmptyDataText = "No Records Found"
            gvSLAMapped.EmptyDataText = "No Records Found"
        End If
    End Sub

    Private Function GetCategories(ByVal ChildCategory As String, ByVal Key As String) As DataSet
        Dim ds As DataSet
        Dim VALDB As ValidateKey = _ValidateAPIKey(Key)
        Dim sp As New SubSonic.StoredProcedure(VALDB.TENANT_ID & "." & "API_HD_GET_CATEGORIES")
        sp.Command.AddParameter("@CCAT", ChildCategory, DbType.String)
        sp.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String)
        ds = sp.GetDataSet()
        Return ds
    End Function

    Private Function GetuserLocation(ByVal Key As String) As String
        Dim Loc As String
        Dim VALDB As ValidateKey = _ValidateAPIKey(Key)
        Dim sp As New SubSonic.StoredProcedure(VALDB.TENANT_ID & "." & "GET_USER_LOCATION")
        sp.Command.AddParameter("@AURID", VALDB.AURID, DbType.String)
        sp.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String)
        Loc = sp.ExecuteScalar()
        Return Loc
    End Function

    Private Function ValidateAPI(ByVal Key As String) As Integer
        Dim cnt As Integer
        Dim sp As New SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings("FRMDB") + "." + "VAL_API")
        sp.Command.AddParameter("@kEY", Key, DbType.[String])
        cnt = sp.ExecuteScalar()
        Return cnt
    End Function

    Public Function _ValidateAPIKey(API As String) As ValidateKey
        Dim ver As New ValidateKey()
        If Not (API Is Nothing) Then
            Try
                Dim sp As New SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings("FRMDB") + "." + "GN_VALIDATE_API")
                sp.Command.AddParameter("@API", API, DbType.[String])
                Using sdr As IDataReader = sp.GetReader()
                    While sdr.Read()
                        ver.TENANT_ID = sdr("TENANT_ID").ToString()
                        ver.COMPANYID = Convert.ToInt32(sdr("COMPANYID"))
                        HttpContext.Current.Session("useroffset") = sdr("TENANT_DT_OFFSET").ToString()
                        ver.AURID = sdr("USER_ID").ToString()
                        ver.AUR_LOCATION = sdr("AUR_LOCATION").ToString()
                    End While
                End Using
                Return ver
            Catch ex As SqlException
                'Dim erhndlr As New ErrorHandler()
                'erhndlr._WriteErrorLog(ex)
                Return ver
            End Try
        Else
            Return ver
        End If
    End Function
End Class
