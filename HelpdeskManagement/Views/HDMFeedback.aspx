﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HDMFeedback.aspx.cs" Inherits="HelpdeskManagement_Views_HDMFeedback" %>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
        var length;
        var PostedFiles;
        var rowData = [];
        var data;
        var fd = [];
        function showselectedfiles(fu) {
            data = new FormData($('form')[0]);
            for (i = 0; i < fu.files.length; i++) {
                rowData.push({ Name: fu.files[i].name, file: fu.files[i] });
                fd.push({ Name: fu.files[i].name, size: fu.files[i].size, type: fu.files[i].type });
                length = fu.files.length;
            }

        }

    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMFeedBackController" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Customized Report" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">FeedBack</h3>
                    </div>

                    <div class="panel-body" style="padding-right: 10px;">
                        <form id="form1" name="HDMFeedBack" novalidate>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMFeedBack.$submitted && HDMFeedBack.ComplaintDate.$invalid}">
                                        <label for="txtcode">Complaint Received Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" id='ComplaintReceiveddat'>
                                            <input type="text" class="form-control" data-ng-model="HDMFeedBackRequest.ComplaintDate" id="ComplaintDate" name="ComplaintDate" required="" placeholder="mm/dd/yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('ComplaintReceiveddat')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.ComplaintDate.$invalid" style="color: red;">Please Select Complaint Date</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMFeedBack.$submitted && HDMFeedBack.LCM_NAME.$invalid}">
                                        <label class="control-label">Clinic <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Clinic" data-output-model="HDMFeedBackRequest.Clinic" data-button-label="icon LCM_NAME"
                                            data-item-label="icon LCM_NAME maker" id="DDL" data-on-item-click="getCMByLoc()"
                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMFeedBackRequest.Clinic[0]" name="LCM_NAME" style="display: none" required="" />
                                        <span id="Error" class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.LCM_NAME.$invalid" style="color: red;">Please Select Clinic</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Clinic Manager </label>
                                        <div isteven-multi-select data-input-model="Clinic_Manager" data-output-model="HDMFeedBackRequest.Clinic_Manager" data-button-label="icon CLINIC_MGR_NAME"
                                            data-item-label="icon CLINIC_MGR_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <%--<input type="text" data-ng-model="HDMFeedBackRequest.Clinic_Manager[0]" name="CLINIC_MGR_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.CLINIC_MGR_NAME.$invalid" style="color: red;">Please Select Clinic Manager</span>--%>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMFeedBack.$submitted && HDMFeedBack.CustID.$invalid}">
                                        <label for="txtcode">Client Name <span style="color: red;">*</span></label>
                                        <div class="control-label" id='Client_Name'>
                                            <input type="text" class="form-control" data-ng-model="HDMFeedBackRequest.Client_Name" id="txtClientname" name="CustID" required="" />
                                            <span class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.CustID.$invalid" style="color: red;">Please Enter Client Name</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMFeedBack.$submitted && HDMFeedBack.Contact.$invalid}">
                                        <label for="txtcode">Contact <span style="color: red;">*</span></label>
                                        <div class="control-label" id='Contact'>
                                            <input type="text" class="form-control" data-ng-model="HDMFeedBackRequest.Contact" id="txtContact" name="Contact" required="" />
                                            <span class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.Contact.$invalid" style="color: red;">Please Enter Contact</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMFeedBack.$submitted && HDMFeedBack.MNC_NAME.$invalid}">
                                        <label class="control-label">Main Category <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Main_Category" data-output-model="HDMFeedBackRequest.Main_Category" data-button-label="icon MNC_NAME"
                                            data-item-label="icon MNC_NAME maker" data-on-item-click="getsubbymain()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMFeedBackRequest.Main_Category[0]" name="MNC_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.MNC_NAME.$invalid" style="color: red;">Please Select Main Category</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMFeedBack.$submitted && HDMFeedBack.SUBC_NAME.$invalid}">
                                        <label class="control-label">Sub Category <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Sub_Category" data-output-model="HDMFeedBackRequest.Sub_Category" data-button-label="icon SUBC_NAME"
                                            data-item-label="icon SUBC_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMFeedBackRequest.Sub_Category[0]" name="SUBC_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.SUBC_NAME.$invalid" style="color: red;">Please Select Sub Category</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">AOM </label>
                                        <div isteven-multi-select data-input-model="AOM" data-output-model="HDMFeedBackRequest.AOM" data-button-label="icon AOM_NAME"
                                            data-item-label="icon AOM_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <%--<input type="text" data-ng-model="HDMFeedBackRequest.AOM[0]" name="AOM_NAME" style="display: none" required="" />--%>
                                        <%--<span class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.AOM_NAME.$invalid" style="color: red;">Please Select AOM</span>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Others </label>
                                        <div isteven-multi-select data-input-model="OTHERS" data-output-model="HDMFeedBackRequest.OTHERS" data-button-label="icon OTH_NAME"
                                            data-item-label="icon OTH_NAME maker" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="HDMFeedBackRequest.OTHERS[0]" name="OTH_NAME" style="display: none" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMFeedBack.$submitted && HDMFeedBack.SER_APP_STA_REMARKS_NAME.$invalid}">
                                        <label class="control-label">Status <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Status" data-output-model="HDMFeedBackRequest.Status" data-button-label="icon SER_APP_STA_REMARKS_NAME"
                                            data-item-label="icon SER_APP_STA_REMARKS_NAME maker" data-on-item-click="getchildbysub()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMFeedBackRequest.Status[0]" name="SER_APP_STA_REMARKS_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.SER_APP_STA_REMARKS_NAME.$invalid" style="color: red;">Please Select Status</span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix" style="margin-left: 200px; margin-top: 30px;">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMFeedBack.$submitted && HDMFeedBack.txtModeComments.$invalid}">
                                        <label for="txtcode">Comments <span style="color: red;">*</span></label>
                                        <div class="control-label" id='ModeComments'>
                                            <textarea name="txtModeComments" class="form-control" data-ng-model="HDMFeedBackRequest.ModeComments" cols="40" rows="5" required=""></textarea>
                                            <span class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.txtModeComments.$invalid" style="color: red;">Please Mode Comments</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMFeedBack.$submitted && HDMFeedBack.MODE_NAME.$invalid}">
                                        <label class="control-label">MODE <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="MODE" data-output-model="HDMFeedBackRequest.MODE" data-button-label="icon MODE_NAME"
                                            data-item-label="icon MODE_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMFeedBackRequest.MODE[0]" name="MODE_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.MODE_NAME.$invalid" style="color: red;">Please Select Mode</span>
                                    </div>
                                    <label for="txtcode" class="custom-file">Upload Images/Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                    <input multiple type="file" name="UPLFILE" class="form-control" data-ng-model="HDMFeedBackRequest.UPLFILE[0]" id="UPLFILE" accept=".mp3" class="custom-file-input" onchange="showselectedfiles(this)">
                                    <span class="custom-file-control"></span>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="clearfix" style="margin-left: 200PX; margin-top: 30px;">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <%--data-ng-class="{'has-error': HDMFeedBack.$submitted && HDMFeedBack.txtStatusComments.$invalid}"--%>
                                        <label for="txtcode">Status Comments </label>
                                        <%--<span style="color: red;">*</span>--%>
                                        <div class="control-label" id='Description'>
                                            <textarea name="txtStatusComments" class="form-control" data-ng-model="HDMFeedBackRequest.StatusComments" cols="40" rows="5" required=""></textarea>
                                            <%--<span class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.txtStatusComments.$invalid" style="color: red;">Please Enter Status Comments</span>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Status </label>
                                        <div isteven-multi-select data-input-model="STATUSCOM" data-output-model="HDMFeedBackRequest.STATUSCOM" data-button-label="icon SER_APP_STA_REMARKS_NAME"
                                            data-item-label="icon SER_APP_STA_REMARKS_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMFeedBackRequest.STATUSCOM[0]" name="SER_APP_STA_REMARKS_NAME" style="display: none" />
                                        <%--<input type="text" data-ng-model="HDMFeedBackRequest.STATUSCOM[0]" name="SER_APP_STA_REMARKS_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="HDMFeedBack.$submitted && HDMFeedBack.SER_APP_STA_REMARKS_NAME.$invalid" style="color: red;">Please Select Status</span>--%>
                                    </div>
                                    <label for="txtcode" class="custom-file">Upload Images/Document <a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                    <input multiple type="file" name="UPLFILE1" class="form-control" data-ng-model="HDMFeedBackRequest.UPLFILE1[0]" id="UPLFILE1" accept=".mp3" class="custom-file-input" onchange="showselectedfiles(this)">
                                    <span class="custom-file-control"></span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-6 col-xs-12" style="margin-left: 875px">
                                <br />
                                <div class="box-footer">
                                    <input type="submit" value="Submit Request" data-ng-click="SaveFDBckDetails()" class="btn btn-primary custom-button-color" />
                                    <%--onclick="GetUpload()" SaveEmpDetails--%>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-12">
                                    <div id="myGrid" data-ng-model="HDMFeedBackRequest.myGrid" style="height: 250px; width: 400px" class="ag-blue"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../JS/HDMFeedBack.js" defer></script>
    <script src="../../../../SMViews/Utility.js" defer></script>
    <script src="../../../../Scripts/moment.min.js" defer></script>

    <script defer>
        var CompanySession = '<%= Session["COMPANYID"]%>';
        //$(document).ready(function () {
        //    setDateVals();
        //});

        //function setDateVals() {
        //$('#Billdate').datepicker({
        //    format: 'dd-M-yyyy',
        //    autoclose: true,
        //    todayHighlight: true
        //});
        //$('#InvoiceDate').datepicker({
        //    format: 'dd-M-yyyy',
        //    autoclose: true,
        //    todayHighlight: true
        //});
        //$('#Billdate').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY')));
        //$('#InvoiceDate').datepicker('setDate', new Date(moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY')));
        //}
    </script>
</body>
</html>


