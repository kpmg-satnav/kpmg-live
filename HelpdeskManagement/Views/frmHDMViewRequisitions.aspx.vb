﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class HDM_HDM_Webfiles_frmHDMViewRequisitions
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        If Not IsPostBack Then
            BindGrid()
            BindGridApprovals()
            BindLocations()
            BindSubCategory()
            BindChildCategory()
            BindMainCategory()
            FillStatus()
            If Request.QueryString("Reopened") = 1 Then
                lblMsg.Text = " Request " + Session("reqid") + " Reopened Successfully"
            End If

            ' To check designation mapping enabled or not
            Dim sp6 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
            sp6.Command.AddParameter("@TYPE ", 6, DbType.Int32)
            ViewState("DSN_MAPPING_STA") = sp6.ExecuteScalar()
            If ViewState("DSN_MAPPING_STA") = 1 Then
                frmApproval.Visible = True
            Else
                frmApproval.Visible = False
            End If

            ' To check admin roles mapping or not
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_HAVING_ADMIN_ROLES")
            sp1.Command.AddParameter("@USER_ID ", Session("UID").ToString(), DbType.String)
            ViewState("CHK_ROLE") = sp1.ExecuteScalar()
            If ViewState("CHK_ROLE") > 0 Then
                btnSkip.Visible = True
            Else
                btnSkip.Visible = False
            End If

        End If
    End Sub

    Private Sub FillStatus()
        Dim dsStatus As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_HDMSTATUS_FILTERS")
        sp.Command.AddParameter("@TYPE", 1, Data.DbType.String)
        dsStatus = sp.GetDataSet()
        ddlStatus.DataSource = dsStatus
        ddlStatus.DataSource = dsStatus
        ddlStatus.DataTextField = "STA_TITLE"
        ddlStatus.DataValueField = "STA_ID"
        ddlStatus.DataBind()
        ddlStatus.Items.Insert(0, "--Select--")
    End Sub

    Private Sub BindGrid()
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_VIEW_REQUISITIONS")
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        ViewState("Search") = sp.GetDataSet
        gvViewRequisitions.DataSource = ViewState("Search")
        gvViewRequisitions.DataBind()
    End Sub

    Private Sub BindGridSearch()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_SEARCH_VIEW_REQUISITIONS")
        sp.Command.AddParameter("@REQID", txtReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@LOCATION", ddlLocation.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@CATEGORY", ddlChildCategory.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@SUBCATEGORY", ddlSubCatMR.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@MAINCATEGORY", ddlManinCatMR.SelectedValue, Data.DbType.String)
        sp.Command.AddParameter("@FROMDATE", txtfromDt.Text, DbType.String)
        sp.Command.AddParameter("@TODATE", txtToDt.Text, DbType.String)
        sp.Command.AddParameter("@STATUS", ddlStatus.SelectedValue, DbType.String)
        sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), Data.DbType.String)
        ViewState("Search") = sp.GetDataSet
        gvViewRequisitions.DataSource = ViewState("Search")
        gvViewRequisitions.DataBind()
    End Sub

    Private Sub BindGridApprovals()
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_VIEW_REQUISITIONS_FOR_APPROVALS")
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        ds = sp.GetDataSet
        gvApprovals.DataSource = ds
        gvApprovals.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            ApprovalBtnTab.Visible = True
            cmts.Visible = True
        Else
            ApprovalBtnTab.Visible = False
            cmts.Visible = False
        End If
    End Sub

    Public Sub BindLocations()
        ddlLocation.Items.Clear()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("Uid").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
        'ObjSubsonic.Binddropdown(ddlLocation1, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)
    End Sub
    Public Sub BindSubCategory()
        'ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlSubCatMR, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE")
        'ObjSubsonic.Binddropdown(ddlSubCatUAReq, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE")
    End Sub

    Public Sub BindChildCategory()
        ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
        'ObjSubsonic.Binddropdown(ddlChildCategory1, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
    End Sub
    Public Sub BindMainCategory()
        'ddlChildCategory.Items.Clear()
        ObjSubsonic.Binddropdown(ddlManinCatMR, "HDM_GET_ACTIVE_MAIN_CATEGORY", "MNC_NAME", "MNC_CODE")
        'ObjSubsonic.Binddropdown(ddlManinCatMR1, "HDM_GET_ACTIVE_MAIN_CATEGORY", "MNC_NAME", "MNC_CODE")
    End Sub

    Protected Sub gvViewRequisitions_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvViewRequisitions.PageIndexChanging
        gvViewRequisitions.PageIndex = e.NewPageIndex()
        gvViewRequisitions.DataSource = ViewState("Search")
        gvViewRequisitions.DataBind()
    End Sub

    Protected Sub gvApprovals_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvApprovals.PageIndexChanging
        gvApprovals.PageIndex = e.NewPageIndex()
        BindGridApprovals()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            BindGridSearch()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'Protected Sub btnSearchApproval_Click(sender As Object, e As EventArgs) Handles btnSearchApproval.Click
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_SEARCH_REQUISITIONS_PENDING_FOR_APPROVAL")
    '        sp.Command.AddParameter("@REQID", txtReqId.Text, Data.DbType.String)
    '        sp.Command.AddParameter("@LOCATION", ddlLocation.SelectedValue, Data.DbType.String)
    '        sp.Command.AddParameter("@CATEGORY", ddlChildCategory.SelectedValue, Data.DbType.String)
    '        sp.Command.AddParameter("@FROMDATE", txtfromDt.Text, DbType.String)
    '        sp.Command.AddParameter("@TODATE", txtToDt.Text, DbType.String)
    '        sp.Command.AddParameter("@AURID", Session("UID"), Data.DbType.String)
    '        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), Data.DbType.String)
    '        gvApprovals.DataSource = sp.GetDataSet
    '        gvApprovals.DataBind()
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Try
            Dim ds As New DataSet
            For Each row As GridViewRow In gvApprovals.Rows
                Dim chkselect As CheckBox = DirectCast(row.FindControl("chkSelect1"), CheckBox)
                Dim lblreqid As Label = DirectCast(row.FindControl("lblreqid"), Label)
                Dim lblLocation As Label = DirectCast(row.FindControl("lblLoc_Code"), Label)
                Dim lblMain As Label = DirectCast(row.FindControl("lblMNC_Code"), Label)
                Dim lblSub As Label = DirectCast(row.FindControl("lblSUB_Code"), Label)
                Dim lblChild As Label = DirectCast(row.FindControl("lblCHC_Code"), Label)
                Dim lblStatus As Label = DirectCast(row.FindControl("lblReqStatus"), Label)
                Dim lblclaimamt As Label = DirectCast(row.FindControl("lblclaimamt"), Label)

                If chkselect.Checked = True Then
                    Dim param As SqlParameter() = New SqlParameter(10) {}
                    param(0) = New SqlParameter("@REQID", lblreqid.Text)
                    param(1) = New SqlParameter("@AURID", Session("UID").ToString())
                    param(2) = New SqlParameter("@STA_ID", lblStatus.Text)
                    param(3) = New SqlParameter("@SER_LOC_CODE", lblLocation.Text)
                    param(4) = New SqlParameter("@SER_MNC_CODE", lblMain.Text)
                    param(5) = New SqlParameter("@SER_SUB_CAT_CODE", lblSub.Text)
                    param(6) = New SqlParameter("@SER_CHILD_CAT_CODE", lblChild.Text)
                    param(7) = New SqlParameter("@SER_APPR_COMMENTS", txtremarks.Text)
                    param(8) = New SqlParameter("@SER_APPR_AMOUNT", txtamount.Text)
                    param(9) = New SqlParameter("@SER_CLAIM_AMT", lblclaimamt.Text)
                    param(10) = New SqlParameter("@COMPANYID", Session("COMPANYID"))
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_APPROVE_REQUISITION_BY_REQ_ID", param)
                End If
            Next
            lblMsg.Text = "Service Requisition(s) Approved successfully..."
            txtamount.Text = 0
            txtremarks.Text = ""
            BindGridApprovals()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSkip_Click(sender As Object, e As EventArgs) Handles btnSkip.Click
        Try
            Dim ds As New DataSet
            For Each row As GridViewRow In gvApprovals.Rows
                Dim chkselect As CheckBox = DirectCast(row.FindControl("chkSelect1"), CheckBox)
                Dim lblreqid As Label = DirectCast(row.FindControl("lblreqid"), Label)
                Dim lblStatus As Label = DirectCast(row.FindControl("lblReqStatus"), Label)
                Dim lblLocation As Label = DirectCast(row.FindControl("lblLoc_Code"), Label)
                Dim lblMain As Label = DirectCast(row.FindControl("lblMNC_Code"), Label)
                Dim lblSub As Label = DirectCast(row.FindControl("lblSUB_Code"), Label)
                Dim lblChild As Label = DirectCast(row.FindControl("lblCHC_Code"), Label)

                If chkselect.Checked = True Then
                    Dim param As SqlParameter() = New SqlParameter(9) {}
                    param(0) = New SqlParameter("@REQID", lblreqid.Text)
                    param(1) = New SqlParameter("@AURID", Session("UID").ToString())
                    param(2) = New SqlParameter("@STA_ID", lblStatus)
                    param(3) = New SqlParameter("@SER_LOC_CODE", lblLocation.Text)
                    param(4) = New SqlParameter("@SER_MNC_CODE", lblMain.Text)
                    param(5) = New SqlParameter("@SER_SUB_CAT_CODE", lblSub.Text)
                    param(6) = New SqlParameter("@SER_CHILD_CAT_CODE", lblChild.Text)
                    param(7) = New SqlParameter("@SER_APPR_COMMENTS", txtremarks.Text)
                    param(8) = New SqlParameter("@SER_AMOUNT", txtamount.Text)
                    param(9) = New SqlParameter("@COMPANYID", Session("COMPANYID"))
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_SKIP_REQUISITION_BY_REQ_ID", param)
                End If
            Next
            lblMsg.Text = "Service Requisition(s) Skipped successfully. "
            BindGridApprovals()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Try
            Dim ds As New DataSet
            For Each row As GridViewRow In gvApprovals.Rows
                Dim chkselect As CheckBox = DirectCast(row.FindControl("chkSelect1"), CheckBox)
                Dim lblreqid As Label = DirectCast(row.FindControl("lblreqid"), Label)
                Dim lblStatus As Label = DirectCast(row.FindControl("lblReqStatus"), Label)
                Dim lblLocation As Label = DirectCast(row.FindControl("lblLoc_Code"), Label)
                Dim lblMain As Label = DirectCast(row.FindControl("lblMNC_Code"), Label)
                Dim lblSub As Label = DirectCast(row.FindControl("lblSUB_Code"), Label)
                Dim lblChild As Label = DirectCast(row.FindControl("lblCHC_Code"), Label)

                If chkselect.Checked = True Then
                    Dim param As SqlParameter() = New SqlParameter(9) {}
                    param(0) = New SqlParameter("@REQID", lblreqid.Text)
                    param(1) = New SqlParameter("@AURID", Session("UID").ToString())
                    param(2) = New SqlParameter("@STA_ID", lblStatus)
                    param(3) = New SqlParameter("@SER_LOC_CODE", lblLocation.Text)
                    param(4) = New SqlParameter("@SER_MNC_CODE", lblMain.Text)
                    param(5) = New SqlParameter("@SER_SUB_CAT_CODE", lblSub.Text)
                    param(6) = New SqlParameter("@SER_CHILD_CAT_CODE", lblChild.Text)
                    param(7) = New SqlParameter("@SER_APPR_COMMENTS", txtremarks.Text)
                    param(8) = New SqlParameter("@SER_APPR_AMOUNT", txtamount.Text)
                    param(9) = New SqlParameter("@COMPANYID", Session("COMPANYID"))
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "HDM_REJECT_REQUISITION_BY_REQ_ID", param)
                End If
            Next
            lblMsg.Text = "Service Requisition(s) Rejected successfully. "
            BindGridApprovals()
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlSubCatMR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSubCatMR.SelectedIndexChanged
        ddlChildCategory.Items.Clear()
        If ddlSubCatMR.SelectedIndex > 0 Then
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@SUBC_CODE", SqlDbType.VarChar, 50)
            param(0).Value = ddlSubCatMR.SelectedValue
            ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_CHILDBY_SUBCATG", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param)
        Else
            ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE")
        End If
    End Sub
    Protected Sub ddlManinCatMR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlManinCatMR.SelectedIndexChanged
        ddlSubCatMR.Items.Clear()
        If ddlManinCatMR.SelectedIndex > 0 Then
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@MNC_CODE", SqlDbType.VarChar, 50)
            param(0).Value = ddlManinCatMR.SelectedValue
            ObjSubsonic.Binddropdown(ddlSubCatMR, "HDM_GET_ALLSUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param)
        Else
            ObjSubsonic.Binddropdown(ddlSubCatMR, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE")
        End If
    End Sub

End Class
