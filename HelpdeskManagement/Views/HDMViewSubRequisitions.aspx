﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .disabled {
            cursor: not-allowed;
            pointer-events: none;
            opacity: .5;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMViewSubRequisitionsController" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Customized Report" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">View Sub Requests</h3>
                    </div>

                    <div class="panel-body" style="padding-right: 10px;">
                        <form id="form1" name="HDMViewSubRequisitionsForm">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': HDMViewSubRequisitions.$submitted && HDMViewSubRequisitions.CustID.$invalid}">
                                        <label for="txtcode">Enter Request ID <span style="color: red;"></span></label>
                                        <div class="control-label" id='RequestID'>
                                            <input type="text" class="form-control" data-ng-model="HDMViewSubRequisitions.RequestID" id="txtRequestID" name="RequesID" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Location <span style="color: red;"></span></label>
                                        <div isteven-multi-select data-input-model="Locations" data-output-model="HDMViewSubRequisitions.Locations" data-button-label="icon LCM_NAME"
                                            data-item-label="icon LCM_NAME maker" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMViewSubRequisitions.Locations[0]" name="LCM_NAME" style="display: none" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Main Category <span style="color: red;"></span></label>
                                        <div isteven-multi-select data-input-model="Main_Category" data-output-model="HDMViewSubRequisitions.Main_Category" data-button-label="icon MNC_NAME"
                                            data-item-label="icon MNC_NAME maker" data-on-item-click="getsubbymain()" data-on-select-all="mainSelectAll()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMViewSubRequisitions.Main_Category[0]" name="MNC_NAME" style="display: none" />

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Sub Category <span style="color: red;"></span></label>
                                        <div isteven-multi-select data-input-model="Sub_Category" data-output-model="HDMViewSubRequisitions.Sub_Category" data-button-label="icon SUBC_NAME"
                                            data-item-label="icon SUBC_NAME maker" data-on-item-click="getchildbysub()" data-on-select-all="subSelectAll()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMViewSubRequisitions.Sub_Category[0]" name="SUBC_NAME" style="display: none" />

                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Child Category <span style="color: red;"></span></label>
                                        <div isteven-multi-select data-input-model="child_Category" data-output-model="HDMViewSubRequisitions.child_Category" data-button-label="icon CHC_TYPE_NAME"
                                            data-item-label="icon CHC_TYPE_NAME maker" data-on-item-click="childchange()" data-on-select-all="childSelectAll()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMViewSubRequisitions.child_Category[0]" name="CHC_TYPE_NAME" style="display: none" />

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Status <span style="color: red;"></span></label>
                                        <div isteven-multi-select data-input-model="status" data-output-model="HDMViewSubRequisitions.status" data-button-label="icon STA_TITLE"
                                            data-item-label="icon STA_TITLE maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMViewSubRequisitions.status[0]" name="STA_TITLE" style="display: none" />
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">History Status<span style="color: red;"></span></label>
                                        <br />
                                        <select id="ddlstatus" class="selectpicker" ng-model="HDMViewSubRequisitions.selstatus" data-ng-change="STATUSCHANGE()">
                                            <option value="1">ACTIVE</option>
                                            <option value="2">IN-ACTIVE</option>
                                            <option value="3">ALL</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Select Range<span style="color: red;">*</span></label>
                                        <br />
                                        <select id="ddlRange" class="selectpicker" ng-model="HDMViewSubRequisitions.selVal" data-ng-change="rptDateRanges()">
                                            <option value="">Select Range</option>
                                            <option value="TODAY">Today</option>
                                            <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="LASTMONTH">Last Month</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">From Date</label>
                                        <div class="input-group date" id='fromdate'>
                                            <input type="text" class="form-control" data-ng-model="HDMViewSubRequisitions.FromDate" id="Text1" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="form1.$submitted && form1.FromDate.$invalid" style="color: red;">Please From Date</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">To Date</label>
                                        <div class="input-group date" id='todate'>
                                            <input type="text" class="form-control" data-ng-model="HDMViewSubRequisitions.ToDate" id="Text2" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="form1.$submitted && form1.ToDate.$invalid" style="color: red;">Please To Date</span>
                                    </div>
                                </div>
                                <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': form1.$submitted && form1.Columns.$invalid}">
                                            <div isteven-multi-select data-input-model="Cols" data-output-model="COL" button-label="icon COL" item-label="icon COL"
                                                tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" style="display:none">
                                            </div>
                                            <input type="text" data-ng-model="COL" name="Columns" style="display: none" required="" />
                                            <span class="error" data-ng-show="form1.$submitted && form1.Columns.$invalid" style="color: red">Please Select Columns </span>
                                        </div>
                                    </div>
                                      <div class="col-md-3 col-sm-6 col-xs-6" data-ng-show="CompanyVisible==0">
                                        <div class="form-group">
                                            <label class="control-label">Company</label>
                                            <div isteven-multi-select data-input-model="Company" data-output-model="HDMcustomized.Company" button-label="icon CNP_NAME"
                                                item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="HDMcustomized.Company[0]" name="CNP_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="form1.$submitted && form1.Company.$invalid" style="color: red">Please Select Company </span>
                                        </div>
                                    </div>--%>
                                <div class="col-md-1 col-sm-6 col-xs-12">
                                    <br />
                                    <div class="box-footer">
                                        <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="LoadData()" />
                                    </div>
                                </div>
                            </div>




                        </form>
                    </div>

                    <form id="form2">
                        <div data-ng-show="GridVisiblity">
                            <div class="clearfix">
                                <%--<div class="col-md-12 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <a data-ng-click="GenReport(Customized,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                                <a data-ng-click="GenReport(Customized,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                                <a data-ng-click="GenReport(Customized,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                            </div>
                                        </div>--%>
                            </div>
                            <%-- <div class="row" style="padding-left: 30px">--%>
                            <div class="clearfix">
                                <div class="col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../JS/HDMViewSubRequistions.js" defer></script>
    <%--<script src="../Js/HDMcustomizedReport.js"></script>--%>
    <script src="../../../../SMViews/Utility.js" defer></script>
    <script src="../../../../Scripts/moment.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../../Scripts/JSONToCSVConvertor.js" defer></script>
    <script>
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
</body>
</html>

