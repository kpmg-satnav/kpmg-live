﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmHDMRaiseRequest.aspx.vb" Inherits="HDM_HDM_Webfiles_frmHDMRaiseRequest" EnableEventValidation="false" EnableViewStateMac="false" ViewStateEncryptionMode="Never" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <style>
        #user, #dept, #id {
            color: deepskyblue;
        }
    </style>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        #txtEmpId {
            overflow: scroll;
        }
    </style>
    <script src="../../Scripts/aggrid/ag-grid-2.js" defer></script>
    <script defer>

        var columnDefs = [
            { headerName: "Selected File", field: "Name", cellClass: "grid-align", width: 250 },
            { headerName: "Image", field: "Name", cellClass: "grid-align", width: 250 },
            { headerName: "Remove", template: '<a href="#" onclick="Remove(this.parentNode.parentNode)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 80 }
        ];

        var rowData = [];

        var gridOptions = {
            columnDefs: columnDefs,
            rowData: rowData,
            onGridReady: function () {
                gridOptions.api.sizeColumnsToFit();
            }
        };

        function resetselectedfiles() {
            console.log('btnsbt');
            console.log(rowData);
            for (i = 0; i < rowData.length; i++) {
                var selectedfiles = document.createElement("input");
                selectedfiles.setAttribute("type", "hidden");
                selectedfiles.setAttribute("name", "selectedfiles");
                selectedfiles.setAttribute("value", rowData[i].Name);
                document.getElementById("form1").appendChild(selectedfiles);
            }
        }

        function showselectedfiles(fu) {
            //clearing prev uploaded files from grid
            var gridLength = gridOptions.rowData.length;
            if (gridLength > 0) {
                for (j = 1; j <= gridLength; j++) {
                    rowData.splice(0, 1);
                    gridOptions.api.setRowData(rowData);
                }
            }

            //Will hide
            //rowData = [];
            console.log(fu.files);
            for (i = 0; i < fu.files.length; i++) {
                rowData.push({ Name: fu.files[i].name, file: fu.files[i] });
            }
            gridOptions.api.setRowData(rowData);
        }

        function Remove(node) {
            $("#fu1").val("");
            var ndx = parseInt(node.getAttribute("row"));
            rowData.splice(ndx, 1);
            gridOptions.api.setRowData(rowData);
        }

        document.addEventListener("DOMContentLoaded", function () {
            var eGridDiv = document.getElementById('myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);
        });

        function GetValueOnKeyPress() {
            var edValue = document.getElementById("txtEmpId");
            var s = edValue.value;
            var lblValue = document.getElementById("lblValue");
            lblValue.innerText = "The text box contains: " + s;
        }

        $(function () {
            var maybe = true;
            var text = $('#txtSpaceID').val();
            if (maybe) {
                $('#txtSpaceID').attr('title', text);
            }
        });


    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Raise Request" ba-panel-class="with-scroll" style="padding-right: 18px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Raise Request </h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <%--  <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>--%>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:UpdatePanel runat="server" ID="UpdatePanel5" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Label ID="lblMsg" runat="server" CssClass="control-label" ForeColor="Red"></asp:Label>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlChildCategory" EventName="SelectedIndexChanged" />
                                                <asp:PostBackTrigger ControlID="btnSubmit" />
                                                <asp:PostBackTrigger ControlID="btnDraft" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="divOnbehalfSts" class="row" runat="server" style="padding-bottom: 20px">
                            <div class="col-md-12 text-center">
                                <label class="btn btn-default">
                                    <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true" />
                                    Self
                                </label>
                                <label class="btn btn-default">
                                    <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true" />
                                    On behalf of
                                </label>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtEmpId"
                                        Display="none" ErrorMessage="Please Enter Employee Id" ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i id="id" class="fa fa-tag"></i></div>
                                        <asp:TextBox ID="txtEmpId" runat="server" CssClass="form-control" placeholder="Enter Employee Id" onKeyPress="GetValueOnKeyPress()" AutoComplete="on" AutoPostBack="True"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i id="user" class="fa fa-user"></i></div>
                                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control" AutoPostBack="True" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i id="dept" class="fa fa-bookmark"></i>
                                        </div>
                                        <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <img src="../../images/Chair_Blue.gif" />
                                        </div>
                                        <asp:TextBox ID="txtSpaceID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Location<span style="color: red;">*</span></label>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ControlToValidate="ddlLocation"
                                        ErrorMessage="Please Select Location" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                    </asp:CompareValidator>

                                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true" ClientIDMode="Static"></asp:DropDownList>

                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Main Category<span style="color: red;">*</span></label>
                                    <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlMainCategory"
                                        ErrorMessage="Please Select Main Category" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                    </asp:CompareValidator>
                                    <asp:UpdatePanel runat="server" ID="updTerms" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlMainCategory" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true" ClientIDMode="Static"></asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlLocation" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="ddlChildCategory" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Sub Category<span style="color: red;">*</span></label>
                                    <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None" ControlToValidate="ddlSubCategory"
                                        ErrorMessage="Please Select Sub Category" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                    </asp:CompareValidator>
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlSubCategory" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlLocation" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="ddlMainCategory" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="ddlChildCategory" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Child Category<span style="color: red;">*</span></label>
                                    <asp:CompareValidator ID="CompareValidator5" runat="server" Display="None" ControlToValidate="ddlChildCategory"
                                        ErrorMessage="Please Select Child Category" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1">
                                    </asp:CompareValidator>
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlChildCategory" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlLocation" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="ddlSubCategory" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="ddlMainCategory" EventName="SelectedIndexChanged" />

                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Asset Location</label>
                                    <asp:DropDownList ID="ddlAssetLoaction" runat="server" CssClass="form-control selectpicker" data-live-search="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Mobile/Extension Number <span style="color: red;"></span></label>
                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtMobile"
                                                ErrorMessage="Please Enter Valid Mobile Number" Display="None" ValidationExpression="^[0-9]{10,12}" ValidationGroup="Val1">
                                            </asp:RegularExpressionValidator>--%>
                                    <cc1:FilteredTextBoxExtender ID="ccftmbl" runat="server" TargetControlID="txtMobile" FilterType="Numbers" ValidChars="0123456789." />
                                    <asp:TextBox ID="txtMobile" MaxLength="12" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Repeat Call</label>
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel3" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlRepeatCalls" runat="server" AutoPostBack="true" CssClass="form-control selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <br />
                                    <asp:HiddenField ID="hdnReqid" runat="server" />
                                    <a id="ancReqid" visible="false" href="#" onclick="GetHistory()" runat="server"><%=hdnReqid.Value %></a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Impact</label>
                                    <asp:DropDownList ID="ddlImpact" runat="server" CssClass="form-control selectpicker" data-live-search="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Urgency</label>
                                    <asp:DropDownList ID="ddlUrgency" runat="server" CssClass="form-control selectpicker" data-live-search="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel8" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <label for="txtcode">Upload Images/Document  <a href="#" data-toggle="tooltip" title="Upload File Type:All and size should not be more than 20MB">?</a></label>
                                            <div class="btn-default">
                                                <i class="fa fa-folder-open-o fa-lg"></i>
                                                <asp:FileUpload ID="fu1" runat="Server" Width="90%" AllowMultiple="True" onchange="showselectedfiles(this)" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSubmit" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <asp:RegularExpressionValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtProbDesc"
                                    Display="None" ErrorMessage="Please Enter Valid Description"
                                    ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                <label for="txtcode">Description/ Service and location<span style="color: red;">*</span></label>
                                <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ErrorMessage="Please Enter Description"
                                    ControlToValidate="txtProbDesc" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtProbDesc" Rows="4" runat="server" CssClass="form-control" Height="30%" TabIndex="15" TextMode="MultiLine">                               
                                </asp:TextBox>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Claim Amount<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Claim Amount"
                                        ControlToValidate="txtclaimamount" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtclaimamount" runat="server" MaxLength="10" CssClass="form-control" Height="30%" TabIndex="15">0</asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <%--<label for="txtcode">Facility</label>--%>
                                    <asp:DropDownList ID="ddlfacility" runat="server" Visible="false" CssClass="form-control selectpicker" data-live-search="true">
                                        <asp:ListItem Value="Select">Select</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6" runat="server">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel4" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <a id="lnkShowEscaltion" href="#" onclick="showPopWin()" runat="server">Click here to view SLA & Escalation</a>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlChildCategory" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlLocation" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlSubCategory" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlMainCategory" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel6" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Button ID="btnDraft" CssClass="btn btn-primary custom-button-color" runat="server" Text="Save as Draft" ValidationGroup="Val1"
                                                CausesValidation="true" TabIndex="17" OnClientClick="resetselectedfiles()" />
                                            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit Request" ValidationGroup="Val1"
                                                CausesValidation="true" TabIndex="17" OnClientClick="resetselectedfiles()" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlChildCategory" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="grid">
                            <div class="col-md-12">
                                <div id="myGrid" style="height: 250px; width: 400px" class="ag-blue"></div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block --%>

    <div class="modal fade" id="myModal" tabindex='-1' data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span aria-hidden="true">View SLA & Escalation Details </span>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- <div class="watermark" show="!gridOptions.data.length">No Files Selected</div>--%>
                    <iframe id="modalcontentframe" width="100%" height="450px" style='border: none'></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="HistoryModal" tabindex='-1' data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Request History Details</h4>
                </div>
                <div class="modal-body" id="Div2">
                    <iframe id="ReqHistoryFrame" width="100%" height="450px" style='border: none'></iframe>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <%-- Modal popup block --%>    <%-- <div class="watermark" show="!gridOptions.data.length">No Files Selected</div>--%>

    <style>
        .ui-autocomplete-loading {
            background: white url("images/ui-anim_basic_16x16.gif") right center no-repeat;
        }
    </style>

    <link href="../../BootStrapCSS/jquery-ui.min.css" rel="stylesheet" />
    <script src="../../BootStrapCSS/Scripts/jquery-ui.min.js" defer></script>

    <script defer>
        function showPopWin() {
            $("#modalcontentframe").attr("src", "frmSLADetails.aspx?LOC_CODE=" + document.getElementById('ddlLocation').value + "&MAIN_CAT_CODE=" + encodeURIComponent(document.getElementById('ddlMainCategory').value)
                + "&SUB_CAT_CODE=" + encodeURIComponent(document.getElementById('ddlSubCategory').value) + "&CHILD_CAT_CODE=" + encodeURIComponent(document.getElementById('ddlChildCategory').value));
            $("#myModal").modal().fadeIn();
            //return false;
        }

        function GetHistory() {
            $("#ReqHistoryFrame").attr("src", "frmReqHistoryDetails.aspx?Reqid=" + document.getElementById('hdnReqid').value);
            $("#HistoryModal").modal().fadeIn();
            return false;
        }

        $("#txtEmpId").autocomplete({
            minLength: 1,
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "frmHDMRaiseRequest.aspx/SearchCustomers",
                    data: "{'prefixText':'" + $('#txtEmpId').val() + "'}",
                    dataType: "json",
                    success: function (data) {
                        response(data.d);
                    },
                    error: function (result) {
                        alert(result.data);
                    }
                });
            },
            select: function (event, ui) {
                var selectedempid = ui.item.value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "frmHDMRaiseRequest.aspx/GetUserDataClient",
                    data: "{'aur_id':'" + selectedempid + "'}",
                    dataType: "json",
                    success: function (data) {
                        $("#txtName").val(data.d.Name);
                        $("#txtDepartment").val(data.d.DEPARTMENT);
                        $("#txtSpaceID").val(data.d.SPACEID);
                    },
                    error: function (result) {
                        alert(result.data);
                    }
                });
            }
        });
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };

        function refreshSelectpicker() {
            function refreshSelectpicker() {
                $("#<%=ddlLocation.ClientID%>").selectpicker();
                $("#<%=ddlSubCategory.ClientID%>").selectpicker();
                $("#<%=ddlChildCategory.ClientID%>").selectpicker();
                $("#<%=ddlMainCategory.ClientID%>").selectpicker();

            }
            refreshSelectpicker();
        }
    </script>

</body>
</html>

