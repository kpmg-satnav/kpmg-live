﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.css" rel="stylesheet" />
    <link href="../../../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <%-- <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
    <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>--%>
</head>
<body data-ng-controller="WorkorderController" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Work Order Creation" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">Work Order Creation</h3>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <form id="Form1" name="frmCreateWorkorder" novalidate>

                            <div id="divWorkOrder" class="row" runat="server" style="padding-bottom: 20px">
                                <div class="col-md-6">
                                    <label class="btn btn-default pull-right">
                                        <input type="radio" id="rbt_hdm" name="rbt_WO" ng-model="Workorder.rbt_hdm" checked='true' />
                                        HelpDesk
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="btn btn-default" style="margin-left: 25px">
                                        <input type="radio" id="rdt_main" name="rbt_WO" ng-model="Workorder.rdt_main" ng-value="false" />
                                        Maintenance
                                    </label>
                                </div>
                            </div>

                            <div class="clearfix" id="HDMGrid">
                                <div class="col-md-12">
                                    <input id="filtertxt" placeholder="Filter..." type="text" style="width: 25%" />
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </div>
                            <div class="clearfix" id="MainGrid">
                                <div class="col-md-12">
                                    <input id="mainfiltertxt" placeholder="Filter..." type="text" style="width: 25%" />
                                    <div data-ag-grid="gridOptionsMain" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </div>
                        </form>
                        <br />
                        <br />
                        <form name="createworkorder" id="HDMfrm" style="display: none">

                            <div class="col-md-12">
                                <div data-ag-grid="gridRaiseWorkorder" class="ag-blue" style="height: 180px; width: auto"></div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 row box-footer text-right">
                                    <div class="form-group">
                                        <input type="submit" value="Raise Workorder" class="btn btn-primary custom-button-color" data-ng-click="SavingData(0)" />
                                        <button type="submit" id="btnCancel" class="btn btn-primary custom-button-color">Cancel</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">WorkOrder ID</label>
                                            <input type="text" class="form-control" id="txtwoid" name="txtwoid" ng-model="frmCreateWorkorder.txtwoid" />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Raised By</label>
                                            <input type="text" class="form-control" id="Raised_by" name="Raised_by" ng-model="frmCreateWorkorder.Raised_by" />
                                        </div>
                                    </div>--%>
                                <div class="col-md-3 col-sm-6 col-xs-12 pull-left">
                                    <div class="form-group" data-ng-class="{'has-error': createworkorder.$submitted && createworkorder.SRN_REQ_REM.$invalid}">
                                        <label class="control-label">Remarks</label>
                                        <textarea rows="3" cols="50" name="remarks" data-ng-model="createworkorder.SRN_REQ_REM" required="" class="form-control" placeholder="Requestor remarks"></textarea>
                                        <span class="error" data-ng-show="createworkorder.$submitted && createworkorder.$invalid" style="color: red;">Please Enter Remarks</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 pull-left">
                                    <div class="form-group" data-ng-class="{'has-error': frmCreateWorkorder.$submitted && frmCreateWorkorder.Terms_Conditions.$invalid}">
                                        <label class="control-label">Terms & Conditions </label>
                                        <textarea rows="3" cols="50" name="Terms_Conditions" data-ng-model="frmCreateWorkorder.Terms_Conditions" required="" class="form-control"></textarea>
                                        <span class="error" data-ng-show="frmCreateWorkorder.$submitted && frmCreateWorkorder.$invalid" style="color: red;">Please Enter Terms & Conditions</span>
                                    </div>
                                </div>
                            </div>

                        </form>
                        <br />
                        <form id="MAINfrm" name="MAINworkorder" data-valid-submit="SavingMaintData(0)" novalidate>
                            <%-- data-ng-show="Markers.length !=0"--%>
                            <div class="col-md-12">
                                <div data-ag-grid="gridMaintRaise" style="height: 180px; width: auto" class="ag-blue"></div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 row box-footer text-right">
                                    <div class="form-group">
                                        <input type="submit" value="Raise Workorder" class="btn btn-primary custom-button-color" />
                                        <%--<button type="submit" id="btnCancel" class="btn btn-primary custom-button-color">Cancel</button>--%>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">WorkOrder ID</label>
                                                <input type="text" class="form-control" id="txtwoidid" name="txtwoidid" ng-model="frmCreateWorkorder.txtwoidid" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Raised By</label>
                                                <input type="text" class="form-control" id="Raisedby" name="Raisedby" ng-model="CheckList.Raisedby" />
                                            </div>
                                        </div>--%>

                                <div class="col-md-3 col-sm-6 col-xs-12 pull-left">
                                    <div class="form-group" data-ng-class="{'has-error': MAINworkorder.$submitted && MAINworkorder.Remarks.$invalid}">
                                        <label class="control-label">Remarks</label>
                                        <textarea rows="3" cols="50" name="remarks" data-ng-model="MAINworkorder.Remarks" required class="form-control" placeholder="Requestor remarks"></textarea>
                                        <span class="error" data-ng-show="MAINworkorder.$submitted && MAINworkorder.$invalid" style="color: red;">Please Enter Remarks</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 pull-left">
                                    <div class="form-group" data-ng-class="{'has-error': MAINworkorder.$submitted && MAINworkorder.TermsConditions.$invalid}">
                                        <label class="control-label">Terms & Conditions </label>
                                        <textarea rows="3" cols="50" name="Terms_Conditions" required data-ng-model="MAINworkorder.TermsConditions" class="form-control" placeholder="Terms & Conditions"></textarea>
                                        <span class="error" data-ng-show="MAINworkorder.$submitted && MAINworkorder.$invalid" style="color: red;">Terms & Conditions</span>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>


    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../JS/WorkOrderCreation.js" defer></script>




</body>
</html>
