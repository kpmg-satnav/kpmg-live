﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MobileAppSLA.aspx.vb" Inherits="HelpdeskManagement_Views_MobileAppSLA" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    
</head>
<body>
    <div ba-panel ba-panel-title="SLA Details" ba-panel-class="with-scroll" style="padding-right: 45px;">
        <form id="form1" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <h4>SLA Details</h4>
            <div class="row form-inline">
                <div class="form-group col-md-6">
                    <div class="table-responsive">
                        <asp:GridView ID="gvSLA" runat="server" AutoGenerateColumns="true"
                            EmptyDataText="SLA Not Defined. Please Define SLA." CssClass="table table-condensed table-bordered table-hover table-striped">
                        </asp:GridView>
                    </div>
                </div>
            </div>
            <br />
            <h4>Service Escalation Mapping Details</h4>
            <div class="row form-inline">
                <div class="form-group col-md-6">
                    <div class="table-responsive">
                        <asp:GridView ID="gvSLAMapped" runat="server" AutoGenerateColumns="false"
                            EmptyDataText="Service Escalation Not Defined. Please Define Service Escalation." CssClass="table table-condensed table-bordered table-hover table-striped">
                            <Columns>
                                <asp:BoundField DataField="ROL_DESC" HeaderText="Role" ItemStyle-HorizontalAlign="left" />
                                <asp:BoundField DataField="USERS" HeaderText="Employee ID" ItemStyle-HorizontalAlign="left" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>
