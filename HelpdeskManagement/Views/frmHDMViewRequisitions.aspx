﻿<%@ Page Title="View Requisitions" Language="VB" AutoEventWireup="false" CodeFile="frmHDMViewRequisitions.aspx.vb" Inherits="HDM_HDM_Webfiles_frmHDMViewRequisitions" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script lang="javascript" type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvApprovals.ClientID %>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }



    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Raise Request" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">View Requests</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <asp:UpdatePanel ID="CityPanel1" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage" ForeColor="Red"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtid">Enter Request Id </label>
                                            <asp:TextBox ID="txtReqId" runat="server" TabIndex="1" Style="padding-right: 20px" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtloc">Location</label>
                                            <asp:DropDownList ID="ddlLocation" runat="server" TabIndex="2" CssClass="form-control selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcat">Main Category</label>
                                            <asp:DropDownList ID="ddlManinCatMR" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcat">Sub Category</label>
                                            <asp:DropDownList ID="ddlSubCatMR" runat="server" CssClass="form-control selectpicker" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">


                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcat">Child Category</label>
                                            <asp:DropDownList ID="ddlChildCategory" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcat">Select Status</label>
                                            <asp:DropDownList ID="ddlStatus" runat="server" TabIndex="6" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtid">Enter From Date</label>
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtfromDt" runat="server" TabIndex="4" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtloc">Enter To Date</label>
                                            <div class='input-group date' id='toDt'>
                                                <asp:TextBox ID="txtToDt" runat="server" TabIndex="5" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('toDt')"></span>
                                                </span>
                                            </div>
                                            <asp:CompareValidator ID="CompareValidator1" ErrorMessage="To date should be greater than From date" ControlToValidate="txtToDt" ControlToCompare="txtfromDt" Type="Date" Operator="GreaterThan" runat="server" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">


                                    <div class="col-md-12 ">
                                        <div class="col-md-12 text-right">

                                            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" TabIndex="7" runat="server" Text="Search" ValidationGroup="Val1"
                                                CausesValidation="true" />
                                        </div>
                                    </div>
                                </div>

                                <div class="horizontal-scroll">
                                    <div class="form-group col-md-12">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="gvViewRequisitions" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                    EmptyDataText="No Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Request Id">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("REQUEST_ID", "~/HelpdeskManagement/Views/HDMViewModifyRequesitions.aspx?RID={0}")%>'>
                                                                    <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQUEST_ID") %>'></asp:Label>
                                                                </asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="REQUESTED_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="REQUESTED_BY" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="LOCATION" HeaderText="Location" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="MAIN_CATEGORY" HeaderText="Main Category" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="SUB_CATEGORY" HeaderText="Sub Category" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="CHILD_CATEGORY" HeaderText="Child Category" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="ASSIGNED_TO" HeaderText="Assigned To" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="CONTACT_NO" HeaderText="Contact No" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="REQ_STATUS" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="SER_APPR_AMOUNT" HeaderText="Approved Amount" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="SER_CLAIM_AMT" HeaderText="Claim Amount" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="ESC_COUNT" HeaderText="No.Of Escalations" ItemStyle-HorizontalAlign="left" />
                                                        <asp:BoundField DataField="TOTAL_TIME" HeaderText="Time Taken" ItemStyle-HorizontalAlign="left" />
                                                    </Columns>
                                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                    <PagerStyle CssClass="pagination-ys" />
                                                </asp:GridView>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <hr style="border-color: black" />
                                <div class="row" id="frmApproval" runat="server" visible="false">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div id="accordion1">
                                            <div runat="server" id="divhd" visible="true">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title panel-heading-qfms-title">Requests Pending for Approvals</h3>
                                                </div>
                                                <br />
                                                <br />
                                                <div class="horizontal-scroll">
                                                    <div class="form-group col-md-12">
                                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="gvApprovals" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                                    EmptyDataText="No Requisition Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Request Id">
                                                                            <ItemTemplate>
                                                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#Eval("REQUEST_ID", "~/HelpdeskManagement/Views/HDMApprovals.aspx?RID={0}")%>'>
                                                                                    <asp:Label ID="lblreqid" runat="server" Text='<%# Eval("REQUEST_ID") %>'></asp:Label>
                                                                                </asp:HyperLink>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="REQUESTED_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                                                                        <asp:BoundField DataField="REQUESTED_BY" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                                                                        <asp:TemplateField HeaderText="Location">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("LOCATION") %>'></asp:Label>
                                                                                <asp:Label ID="lblLoc_Code" runat="server" Text='<%# Eval("SER_LOC_CODE") %>' Visible="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Main Category">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblMainCateogry" runat="server" Text='<%# Eval("MAIN_CATEGORY")%>'></asp:Label>
                                                                                <asp:Label ID="lblMNC_Code" runat="server" Text='<%# Eval("SER_MNC_CODE")%>' Visible="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Sub Category">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSubCategory" runat="server" Text='<%# Eval("SUB_CATEGORY")%>'></asp:Label>
                                                                                <asp:Label ID="lblSUB_Code" runat="server" Text='<%# Eval("SER_SUB_CAT_CODE")%>' Visible="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Child Category">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblChildCateory" runat="server" Text='<%# Eval("CHILD_CATEGORY") %>'></asp:Label>
                                                                                <asp:Label ID="lblCHC_Code" runat="server" Text='<%# Eval("SER_CHILD_CAT_CODE") %>' Visible="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Request Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblReqStatusDisplay" runat="server" Text='<%# Eval("REQ_STATUS") %>'></asp:Label>
                                                                                <asp:Label ID="lblReqStatus" runat="server" Text='<%# Eval("REQ_STATUS_ID") %>' Visible="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <%--   <asp:BoundField DataField="REQ_STATUS" HeaderText="Request Status" ItemStyle-HorizontalAlign="left" />--%>
                                                                        <asp:BoundField DataField="TOTAL_TIME" HeaderText="Total Time Taken" ItemStyle-HorizontalAlign="left" />
                                                                        <asp:BoundField DataField="SER_APPR_AMOUNT" HeaderText="Approved Amount" ItemStyle-HorizontalAlign="left" />
                                                                        <%-- <asp:BoundField DataField="SER_CLAIM_AMT" HeaderText="Claim Amount" ItemStyle-HorizontalAlign="left" />--%>
                                                                        <asp:TemplateField HeaderText="Claim Amount">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblclaimamt" runat="server" Text='<%# Eval("SER_CLAIM_AMT") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                Select All
                                                                        <input id="chkSelect2" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkSelect1', this.checked)">
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkSelect1" runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                    <PagerStyle CssClass="pagination-ys" />
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="ddlStatus" EventName="SelectedIndexChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row" id="cmts" runat="server">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="txtid">Amount</label>
                                                        <asp:TextBox ID="txtamount" TextMode="Number" runat="server" TabIndex="1" Style="padding-right: 20px" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label for="txtid">Approver Remarks</label>
                                                        <asp:RegularExpressionValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtRemarks"
                                                            Display="None" ErrorMessage="Please Enter Valid Remarks"
                                                            ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                        <asp:TextBox ID="txtremarks" runat="server" TabIndex="1" Style="padding-right: 20px" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" id="ApprovalBtnTab" runat="server">
                                                <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                                    <div class="form-group">
                                                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq();" />
                                                        <asp:Button ID="btnSkip" runat="server" Text="Skip" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq();" />
                                                        <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq();" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>

        function refreshSelectpicker() {
            $("#<%=ddlLocation.ClientID%>").selectpicker();
            $("#<%=ddlManinCatMR.ClientID%>").selectpicker();
            $("#<%=ddlSubCatMR.ClientID%>").selectpicker();
            $("#<%=ddlChildCategory.ClientID%>").selectpicker();
            $("#<%=ddlStatus.ClientID%>").selectpicker();
        }
        refreshSelectpicker();
    </script>
</body>
</html>


