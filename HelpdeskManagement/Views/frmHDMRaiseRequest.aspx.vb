﻿Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.IO
Imports System.Web
Imports System.Web.Services
Imports System.Web.Script.Services


Partial Class HDM_HDM_Webfiles_frmHDMRaiseRequest
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim MaintenanceReq As String = ""
    Dim MaintenanceReq_Upl As String = ""
    'Dim Enable_Save_As_Draft As Integer

    Private Sub ClearAll()
        'txtDepartment.Text = ""
        'txtSpaceID.Text = ""
        'txtEmpId.Text = ""
        ddlLocation.SelectedIndex = 0
        ddlMainCategory.SelectedIndex = 0
        ddlSubCategory.SelectedIndex = 0
        ddlChildCategory.SelectedIndex = 0
        txtMobile.Text = ""
        txtProbDesc.Text = ""
        ddlImpact.SelectedIndex = 1
        ddlUrgency.SelectedIndex = 1
        ddlAssetLoaction.SelectedIndex = 1
        ddlRepeatCalls.SelectedIndex = 1
        txtProbDesc.Text = ""
        ancReqid.Visible = False
        If ViewState("Enable_Save_As_Draft") = 1 Then
            btnDraft.Visible = True
        Else
            btnDraft.Visible = False
        End If
        btnSubmit.Text = "Submit Request"
        ddlfacility.SelectedIndex = 0
    End Sub

    Private Sub ClearFileds_Rbt()
        txtEmpId.Text = ""
        txtEmpId.Enabled = True
        txtName.Text = ""
        txtDepartment.Text = ""
        txtSpaceID.Text = ""
        ddlLocation.ClearSelection()
        'ddlTower.ClearSelection()
        ddlMainCategory.ClearSelection()
        ddlSubCategory.ClearSelection()
        ddlChildCategory.ClearSelection()
        ddlAssetLoaction.ClearSelection()
        txtMobile.Text = ""
        ddlRepeatCalls.ClearSelection()
        ddlImpact.ClearSelection()
        ddlUrgency.ClearSelection()
        txtProbDesc.Text = ""
    End Sub

    'Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    '    Dim sb As StringBuilder = New StringBuilder
    '    Dim sw As StringWriter = New StringWriter(sb)
    '    Dim hWriter As HtmlTextWriter = New HtmlTextWriter(sw)
    '    MyBase.Render(hWriter)
    '    Dim html As String = sb.ToString
    '    html = Regex.Replace(html, "<input[^>]*id=\""(__VIEWSTATE)\""[^>]*>", String.Empty, RegexOptions.IgnoreCase)
    '    writer.Write(html)
    'End Sub


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'If Session("UID") = "" Then
        '    Response.Redirect(Application("FMGLogout"))
        'End If
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        If Not IsPostBack() Then
            lnkShowEscaltion.Visible = False
            'BindTowers()
            BindAssetLocations()
            BindRepeatCalls()
            BindImpact()
            BindUrgency()
            BindFacilities()
            HelpDeskFeedBackPreferences()
            ddlImpact.SelectedIndex = 1
            ddlUrgency.SelectedIndex = 1
            'ddlAssetLoaction.SelectedIndex = 1
            ddlRepeatCalls.SelectedIndex = 1


            ' Disabling the location based on system preferences

            Dim sp7 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES_FOR_LOC")
            Dim Flag As Integer = sp7.ExecuteScalar()
            If Flag = 1 Then
                ddlLocation.Enabled = False
            Else
                ddlLocation.Enabled = True
            End If


            ' To check designation mapping enabled or not
            Dim sp6 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
            sp6.Command.AddParameter("@TYPE ", 6, DbType.Int32)
            ViewState("DSN_MAPPING_STA") = sp6.ExecuteScalar()

            ' CHECK ONBEHALF OF VISIBILITY STATUS
            'Dim ds As New DataSet
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
            sp.Command.AddParameter("@TYPE ", 1, DbType.Int32)
            Dim ChkOnbehalfSts = sp.ExecuteScalar()

            If ChkOnbehalfSts = 1 Then
                divOnbehalfSts.Visible = True
            Else
                divOnbehalfSts.Visible = False
            End If

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
            sp1.Command.AddParameter("@TYPE ", 5, DbType.Int32)
            Dim Enable_Save_As_Draft = sp1.ExecuteScalar()
            ViewState("Enable_Save_As_Draft") = Enable_Save_As_Draft

            If ViewState("Enable_Save_As_Draft") = 1 Then
                btnDraft.Visible = True
            Else
                btnDraft.Visible = False
            End If

            If rbActions.Checked = True Then
                txtEmpId.Text = Session("Uid")
                txtEmpId.Enabled = False
                GetUserData(txtEmpId.Text)
                ddlImpact.SelectedIndex = 1
                ddlUrgency.SelectedIndex = 1
                ddlRepeatCalls.SelectedIndex = 1
            Else
                txtEmpId.Text = ""
                txtEmpId.Enabled = True
                ddlLocation.ClearSelection()

            End If
            BindLocations()
        End If
    End Sub

    Public Function validateempid(empid As String) As String
        Dim validatecode As String
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_AUR_ID")
        sp.Command.AddParameter("@AUR_ID", empid, DbType.String)
        validatecode = sp.ExecuteScalar
        Return validatecode
    End Function
    Public Sub HelpDeskFeedBackPreferences()
        Dim ReqFbCount, SysVal As Integer

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Check_Feedback_SysVal")
        sp.Command.AddParameter("@USER_ID", Session("Uid").ToString().Trim(), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        ReqFbCount = ds.Tables(0).Rows(0).Item("ReqFbCount")
        SysVal = ds.Tables(0).Rows(0).Item("SysVal")

        If SysVal = 1 Then
            If ReqFbCount <> 0 Then
                btnSubmit.Enabled = False

                Dim message As String = "Provide Feedback for all closed Requests Raised by You."
                Dim sb As New System.Text.StringBuilder()
                sb.Append("<script type = 'text/javascript'>")
                sb.Append("window.onload=function() { alert('" + message + "') }; </script>")

                ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
            Else
                btnSubmit.Enabled = True
            End If

        End If


    End Sub

    Public Sub GetUserData(aur_id As String)
        Dim validatecode As String = validateempid(txtEmpId.Text)
        If validatecode = "0" Then
            lblMsg.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HRDGET_USER_DETAILS")
            sp.Command.AddParameter("@AUR_ID", aur_id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtName.Text = ds.Tables(0).Rows(0).Item("NAME")
                txtDepartment.Text = ds.Tables(0).Rows(0).Item("DEPARTMENT")
                txtSpaceID.Text = ds.Tables(0).Rows(0).Item("SPACEID")
                txtMobile.Text = ds.Tables(0).Rows(0).Item("PHONE_NUBER")
                ViewState("DSN_CODE") = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
                'If ds.Tables(1).Rows.Count > 0 Then
                '    txtSpaceID.Text = ds.Tables(1).Rows(0).Item("SSA_SPC_ID")
                'Else
                '    txtSpaceID.Text = "--"
                'End If
            Else
                lblMsg.Text = "Department Code not exist..."
                txtName.Text = ""
                ' ClearAll()
            End If
        Else
            lblMsg.Text = "Employee Id not existed...Please enter valid empoyee Id"
            txtName.Text = ""
            'ClearAll()
        End If
    End Sub

    Public Sub BindFacilities()
        ddlfacility.Items.Clear()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 50)
        param(0).Value = ddlLocation.SelectedValue
        param(1) = New SqlParameter("@CMP_ID", SqlDbType.Int)
        param(1).Value = Session("COMPANYID").ToString().Trim()
        ObjSubsonic.Binddropdown(ddlfacility, "HDM_GET_FACILITY_DTLS", "FD_NAME", "FD_CODE", param)
    End Sub

    Public Sub BindLocations()
        'ddlLocation.Items.Clear()
        'Dim param(0) As SqlParameter
        'param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 50)
        'param(0).Value = Session("Uid").ToString().Trim()
        'ObjSubsonic.Binddropdown(ddlLocation, "USP_GETACTIVELOCATION", "LCM_NAME", "LCM_CODE", param)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GETACTIVELOCATION")
        sp.Command.AddParameter("@USER_ID", Session("Uid").ToString().Trim(), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count = 1 Then
            ddlLocation.DataSource = ds
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, "--Select--")
            ddlLocation.ClearSelection()
            ddlLocation.SelectedIndex = 1
            BindMainCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
            BindSubCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
            BindChildCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))

            BindFacilities()
        Else
            ddlLocation.DataSource = ds
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, "--Select--")
            'ddlMainCategory.Items.Insert(0, "--Select--")
            ddlSubCategory.Items.Insert(0, "--Select--")

            BindMainCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
            'BindSubCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
            BindChildCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
            ddlLocation.ClearSelection()
            Dim li3 As ListItem = Nothing
            li3 = ddlLocation.Items.FindByValue(Session("location"))
            If Not li3 Is Nothing Then
                li3.Selected = True
            End If
        End If

    End Sub

    Public Sub BindMainCategory(DSN_STA As Integer, DSN_CODE As String)
        ddlMainCategory.Items.Clear()
        'ObjSubsonic.Binddropdown(ddlMainCategory, "HDM_GET_ALL_ACTIVE_MAINCATEGORY", "MNC_NAME", "MNC_CODE")
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(0).Value = DSN_STA
        param(1) = New SqlParameter("@DSN_CODE", DbType.String)
        param(1).Value = DSN_CODE
        ObjSubsonic.Binddropdown(ddlMainCategory, "HDM_GET_ALL_ACTIVE_MAINCATEGORY", "MNC_NAME", "MNC_CODE", param)
        
    End Sub

    Public Sub BindSubCategory(DSN_STA As Integer, DSN_CODE As String)
        ddlSubCategory.Items.Clear()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(0).Value = DSN_STA
        param(1) = New SqlParameter("@DSN_CODE", DbType.String)
        param(1).Value = DSN_CODE
        ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_ALL_ACTIVE_SUB_CATEGORY", "SUBC_NAME", "SUBC_CODE", param)
    End Sub

    Public Sub BindChildCategory(DSN_STA As Integer, DSN_CODE As String)
        ddlChildCategory.Items.Clear()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(0).Value = DSN_STA
        param(1) = New SqlParameter("@DSN_CODE", DbType.String)
        param(1).Value = DSN_CODE
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_ALL_ACTIVE_CHILD_CATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param)
    End Sub

    Public Sub BindAssetLocations()
        ddlAssetLoaction.Items.Clear()
        ObjSubsonic.Binddropdown(ddlAssetLoaction, "HDM_GET_ALL_ACTIVE_ASSETlOCATIONS", "HAL_LOC_NAME", "HAL_LOC_CODE")
    End Sub

    Public Sub BindRepeatCalls()
        ddlRepeatCalls.Items.Clear()
        ObjSubsonic.Binddropdown(ddlRepeatCalls, "HDM_GET_ALL_ACTIVE_REPEATCALLS", "RPT_NAME", "RPT_CODE")
    End Sub

    Public Sub BindImpact()
        ddlImpact.Items.Clear()
        ObjSubsonic.Binddropdown(ddlImpact, "HDM_BIND_IMPACT", "IMP_NAME", "IMP_CODE")
    End Sub

    Public Sub BindUrgency()
        ddlUrgency.Items.Clear()
        ObjSubsonic.Binddropdown(ddlUrgency, "HDM_BIND_URGENCY", "UGC_NAME", "UGC_CODE")
    End Sub

    Public Sub BindSubCategory_By_MainCategory(Main_Category As String, DSN_STA As Integer, DSN_CODE As String)
        'ddlSubCategory.Items.Clear()
        'Dim param(0) As SqlParameter
        'param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        'param(0).Value = Main_Category
        'ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_SUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param)

        ddlSubCategory.Items.Clear()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = Main_Category
        param(1) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(1).Value = DSN_STA
        param(2) = New SqlParameter("@DSN_CODE", DbType.String)
        param(2).Value = DSN_CODE
        ObjSubsonic.Binddropdown(ddlSubCategory, "HDM_GET_SUB_CATEGORYS_BY_MAIN", "SUBC_NAME", "SUBC_CODE", param)
    End Sub

    Public Sub BindChildCategory_By_SubCategory(Main_Category As String, Sub_Category As String, DSN_STA As Integer, DSN_CODE As String)
        'ddlChildCategory.Items.Clear()
        'Dim param(1) As SqlParameter
        'param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        'param(0).Value = Main_Category
        'param(1) = New SqlParameter("@SUB_CODE", SqlDbType.NVarChar, 50)
        'param(1).Value = Sub_Category
        'ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_CHILD_CATEGORYS_BY_SUBCATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param)

        ddlChildCategory.Items.Clear()
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@MNC_CODE", SqlDbType.NVarChar, 50)
        param(0).Value = Main_Category
        param(1) = New SqlParameter("@SUB_CODE", SqlDbType.NVarChar, 50)
        param(1).Value = Sub_Category
        param(2) = New SqlParameter("@DSN_STA", SqlDbType.Int)
        param(2).Value = DSN_STA
        param(3) = New SqlParameter("@DSN_CODE", DbType.String)
        param(3).Value = DSN_CODE
        ObjSubsonic.Binddropdown(ddlChildCategory, "HDM_GET_CHILD_CATEGORYS_BY_SUBCATEGORY", "CHC_TYPE_NAME", "CHC_TYPE_CODE", param)

    End Sub

    Private Sub RaiseRequest(ByVal MaintenanceReq As String, ByVal STATUS As Integer)
        MaintenanceReq_Upl = MaintenanceReq.Replace("/", "_")
        Dim projectPath As String = Server.MapPath("~/")
        Dim folderName As String = Path.Combine(projectPath, "UploadFiles\" & Session("TENANT"))
        If Not Directory.Exists(folderName) Then
            System.IO.Directory.CreateDirectory(folderName)
        End If

        Dim RepeatCall As String
        Dim Impact As String
        Dim Urgency As String
        Dim REQ_TYPE As Integer
        Dim CALL_LOG_BY As String
        Dim Facility As String

        If ddlRepeatCalls.SelectedIndex > 0 Then
            RepeatCall = ddlRepeatCalls.SelectedItem.Value
        Else
            RepeatCall = ""
        End If
        If ddlImpact.SelectedIndex > 0 Then
            Impact = ddlImpact.SelectedItem.Value
        Else
            Impact = ""
        End If
        If ddlUrgency.SelectedIndex > 0 Then
            Urgency = ddlUrgency.SelectedItem.Value
        Else
            Urgency = ""
        End If
        Facility = IIf(ddlfacility.SelectedIndex > 0, ddlfacility.SelectedItem.Value, "")

        If fu1.PostedFiles IsNot Nothing Then
            Dim selectedfiles = Request.Form("selectedfiles")
            If selectedfiles <> Nothing Then
                Dim selectedfilesarray = selectedfiles.Split(",")
                For fucount As Integer = 0 To selectedfilesarray.Length - 1
                    'Dim intsixe As Long = fu1.PostedFiles(fucount).ContentLength
                    For Each File In fu1.PostedFiles
                        If selectedfilesarray(fucount).Equals(File.FileName) Then
                            Dim intsize As Long = CInt(File.ContentLength)
                            Dim strFileName As String
                            Dim strFileExt As String
                            If intsize <= 20971520 Then
                                Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("hhmmss")
                                strFileName = System.IO.Path.GetFileName(File.FileName)
                                strFileExt = System.IO.Path.GetExtension(File.FileName)
                                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Session("TENANT") & "\" & MaintenanceReq_Upl & "_" & Upload_Time & "_" & strFileName '& "." & strFileExt
                                File.SaveAs(filePath)

                                Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_INSERT_UPLOADED_FILES")
                                sp2.Command.AddParameter("@REQ_ID", MaintenanceReq, DbType.String)
                                sp2.Command.AddParameter("@STATUS_ID", STATUS, DbType.Int32)
                                sp2.Command.AddParameter("@FILENAME", strFileName, DbType.String)
                                sp2.Command.AddParameter("@UPLOAD_PATH", MaintenanceReq_Upl + "_" + Upload_Time + "_" + strFileName, DbType.String)
                                sp2.Command.AddParameter("@REMARKS", txtProbDesc.Text, DbType.String)
                                sp2.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
                                sp2.ExecuteScalar()
                            End If
                        End If
                    Next
                Next
            End If
        Else
            lblMsg.Text = "Select Uploadable file by clicking the Browse button"
        End If


        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_RAISE_HELP_DESK_REQUEST")
        sp.Command.AddParameter("@REQ_ID", MaintenanceReq, DbType.String)
        sp.Command.AddParameter("@STATUS", STATUS, DbType.Int32)
        sp.Command.AddParameter("@LOC_CODE", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SPC_ID", txtSpaceID.Text, DbType.String)
        sp.Command.AddParameter("@MNC_CODE", ddlMainCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SUB_CAT", ddlSubCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CHILD_CAT", ddlChildCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_LOC", ddlAssetLoaction.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REPEATCALL", RepeatCall, DbType.String)
        sp.Command.AddParameter("@MOBILE", txtMobile.Text, DbType.String)
        sp.Command.AddParameter("@PROB_DESC", txtProbDesc.Text, DbType.String)
        sp.Command.AddParameter("@IMPACT", Impact, DbType.String)
        sp.Command.AddParameter("@URGENCY", Urgency, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@SER_CLAIM_AMT", txtclaimamount.Text, DbType.String)
        If rbActions.Checked = True Then
            REQ_TYPE = 0
            CALL_LOG_BY = Session("UID")
        Else
            REQ_TYPE = 1
            CALL_LOG_BY = txtEmpId.Text
        End If
        sp.Command.AddParameter("@SER_REQ_TYPE", REQ_TYPE, DbType.Int32)
        sp.Command.AddParameter("@SER_CALL_LOG_BY", CALL_LOG_BY, DbType.String)
        sp.Command.AddParameter("@RAISED_BY", Session("UID"), DbType.String)
        sp.Command.AddParameter("@EMP_NAME", txtName.Text, DbType.String)
        sp.Command.AddParameter("@FACILITY", ddlfacility.SelectedValue, DbType.String)
        sp.ExecuteScalar()
        If STATUS = 13 Then
            lblMsg.Text = "Request Saved Successfully"
        Else
            lblMsg.Text = "Service Requisition (" + MaintenanceReq + ") raised successfully."
        End If
        'lblMsg.Text = "Service Requisition (" + MaintenanceReq + ") raised successfully."
    End Sub

    Function GetTodaysReqCount() As String
        Dim cnt As String
        cnt = 0
        Dim ds As New DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TODAYS_REQUEST_ID")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ds = sp.GetDataSet()
        cnt = CInt(ds.Tables(0).Rows(0).Item("cnt"))
        Return cnt
    End Function

    Protected Sub rbActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        If rbActions.Checked = True Then
            txtEmpId.Text = Session("Uid")

            GetUserData(txtEmpId.Text)
            txtEmpId.Enabled = False
            lblMsg.Text = ""
            lnkShowEscaltion.Visible = False
        Else
            'txtEmpId.Text = Session("Uid")

            lnkShowEscaltion.Visible = False
            ClearFileds_Rbt()
        End If
        ' BindLocations()
        'BindMainCategory()
        'BindSubCategory()
        'BindChildCategory()
        'BindAssetLocations()
        'BindRepeatCalls()
        'BindImpact()
        'BindUrgency()
        ddlImpact.SelectedIndex = 1
        ddlUrgency.SelectedIndex = 1
        ddlRepeatCalls.Enabled = True
        ddlRepeatCalls.SelectedIndex = 1
    End Sub

    Protected Sub txtEmpId_TextChanged(sender As Object, e As EventArgs) Handles txtEmpId.TextChanged
        GetUserData(txtEmpId.Text)
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            lblMsg.Text = ""
            If ddlLocation.SelectedIndex <> 0 Then
                BindMainCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
                ddlMainCategory.ClearSelection()
                ddlSubCategory.Items.Clear()
                ddlSubCategory.Items.Insert(0, "--Select--")
                ddlChildCategory.Items.Clear()
                ddlChildCategory.Items.Insert(0, "--Select--")
                BindChildCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
                BindFacilities()
                If ddlMainCategory.SelectedIndex <> 0 And ddlSubCategory.SelectedIndex <> 0 And ddlChildCategory.SelectedIndex <> 0 Then
                    lnkShowEscaltion.Visible = True
                Else
                    lnkShowEscaltion.Visible = False
                End If
            Else
                lnkShowEscaltion.Visible = False
                ddlMainCategory.ClearSelection()
                ddlSubCategory.Items.Clear()
                ddlChildCategory.ClearSelection()
                ddlSubCategory.Items.Insert(0, "--Select--")
                BindMainCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
                BindChildCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))

            End If
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlRepeatCalls_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRepeatCalls.SelectedIndexChanged
        Try
            ' CHECK AUTO REPEAT CALL YES/NO                
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
            sp.Command.AddParameter("@TYPE ", 2, DbType.Int32)
            Dim repeatCallSts = sp.ExecuteScalar()

            If repeatCallSts = 1 Then
                If ddlRepeatCalls.SelectedValue = "Y" Then
                    Dim ds As New DataSet
                    Dim spLR As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LASTSERVED_REQID")
                    spLR.Command.AddParameter("@LOC", ddlLocation.SelectedValue, DbType.String)
                    spLR.Command.AddParameter("@MNCCAT", ddlMainCategory.SelectedValue, DbType.String)
                    spLR.Command.AddParameter("@SUBCAT", ddlSubCategory.SelectedValue, DbType.String)
                    spLR.Command.AddParameter("@CHILDCAT", ddlChildCategory.SelectedValue, DbType.String)
                    spLR.Command.AddParameter("@AURID", txtEmpId.Text, DbType.String)
                    spLR.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
                    ds = spLR.GetDataSet()

                    If (ds.Tables(0).Rows.Count > 0) Then
                        hdnReqid.Value = Convert.ToString(ds.Tables(0)(0)(0))
                        ancReqid.Visible = True
                        btnDraft.Visible = False
                        btnSubmit.Text = "Reopen and Submit"
                    Else
                        ancReqid.Visible = False
                        If ViewState("Enable_Save_As_Draft") = 1 Then
                            btnDraft.Visible = True
                        Else
                            btnDraft.Visible = False
                        End If
                        btnSubmit.Text = "Submit Request"
                    End If

                Else
                    ancReqid.Visible = False
                    If ViewState("Enable_Save_As_Draft") = 1 Then
                        btnDraft.Visible = True
                    Else
                        btnDraft.Visible = False
                    End If
                    btnSubmit.Text = "Submit Request"
                End If
            Else
                'RCAP IF REPEAT CALL IS NO
                hdnReqid.Value = ""
                btnSubmit.Text = "Submit Request"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlMainCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMainCategory.SelectedIndexChanged
        Try
            'localhost:54351/FAM/FAM_WebFiles/frmItemReq.aspx       SP HDM_CHK_MAINCAT_CONS_TYPE
            If ddlMainCategory.SelectedIndex > 0 Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHK_MAINCAT_CONS_TYPE")
                sp.Command.AddParameter("@MNC_CODE ", ddlMainCategory.SelectedValue, DbType.String)
                Dim ChkConStatus = sp.ExecuteScalar()

                If ChkConStatus = 1 Then
                    'divOnbehalfSts.Visible = True
                    Response.Redirect("~/FAM/FAM_Webfiles/frmItemReq.aspx?HDM=1")
                Else
                    'divOnbehalfSts.Visible = False

                    ddlRepeatCalls.ClearSelection()
                    ddlRepeatCalls.SelectedIndex = 1
                    hdnReqid.Value = ""
                    lblMsg.Text = ""
                    lnkShowEscaltion.Visible = False
                    If ddlMainCategory.SelectedIndex <> 0 Then
                        ddlSubCategory.Items.Clear()
                        BindSubCategory_By_MainCategory(ddlMainCategory.SelectedItem.Value, ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
                        ddlChildCategory.Items.Clear()
                        ddlChildCategory.Items.Insert(0, "--Select--")
                    Else
                        ddlSubCategory.Items.Clear()
                        ddlSubCategory.Items.Insert(0, "--Select--")
                        ddlChildCategory.Items.Clear()
                        ddlChildCategory.Items.Insert(0, "--Select--")
                    End If
                End If
            End If
            If ddlMainCategory.SelectedIndex = 0 Then
                ddlSubCategory.Items.Clear()
                ddlSubCategory.Items.Insert(0, "--Select--")
                ddlChildCategory.Items.Clear()
                ddlChildCategory.Items.Insert(0, "--Select--")
                BindChildCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
            End If
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub ddlSubCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSubCategory.SelectedIndexChanged
        Try
            ddlRepeatCalls.ClearSelection()
            ddlRepeatCalls.SelectedIndex = 1
            hdnReqid.Value = ""
            lblMsg.Text = ""
            lnkShowEscaltion.Visible = False
            If ddlMainCategory.SelectedIndex <> 0 And ddlSubCategory.SelectedIndex <> 0 Then
                ddlChildCategory.Items.Clear()
                BindChildCategory_By_SubCategory(ddlMainCategory.SelectedItem.Value, ddlSubCategory.SelectedItem.Value, ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
            Else
                ddlChildCategory.Items.Clear()
                ddlChildCategory.Items.Insert(0, "--Select--")
            End If
        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub
    Protected Shared Function ValidateMappingsFromSysPreferences(ByVal MNC As String, ByVal SUBCAT As String, ByVal CC As String, ByVal emp As String) As DataSet
        Dim ds As DataSet
        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "VALIDATE_MAPPINGS")
        sp.Command.AddParameter("@MNC", MNC, DbType.String)
        sp.Command.AddParameter("@SUB", SUBCAT, DbType.String)
        sp.Command.AddParameter("@CC", CC, DbType.String)
        sp.Command.AddParameter("@AURID", emp, DbType.String)
        ds = sp.GetDataSet
        Return ds
    End Function
    Protected Sub ddlChildCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlChildCategory.SelectedIndexChanged


        If ddlChildCategory.SelectedIndex > 0 Then
            'ddlSubCategory.Items.Insert(0, "--Select--")
            'ddlSubCategory.SelectedIndex = 0
            ddlMainCategory.SelectedIndex = 0
            ddlRepeatCalls.ClearSelection()
            ddlRepeatCalls.SelectedIndex = 1
            hdnReqid.Value = ""
            lblMsg.Text = ""
            If ddlSubCategory.SelectedIndex = 0 Then
                ddlSubCategory.Items.Insert(0, "--Select--")
                ddlSubCategory.SelectedIndex = 0
                BindSubCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
            End If





            'BindSubCategory(ViewState("DSN_MAPPING_STA"), ViewState("DSN_CODE"))
            Dim dr As SqlDataReader = Child_getmain_dtls(ddlChildCategory.SelectedValue)
                If (dr.Read) Then
                    ddlSubCategory.SelectedValue = dr("CHC_TYPE_SUBC_CODE")
                    ddlMainCategory.SelectedValue = dr("CHC_TYPE_MNC_CODE")
                End If



                Dim Validate As DataSet = ValidateMappingsFromSysPreferences(ddlMainCategory.SelectedItem.Value, ddlSubCategory.SelectedItem.Value, ddlChildCategory.SelectedItem.Value, txtEmpId.Text)
                If Validate.Tables(0).Rows(0)("VLD") = 2 Then
                    btnSubmit.Enabled = False
                    btnDraft.Enabled = False
                    lblMsg.Text = "Please define approvals for the selected category"
                Else
                    lblMsg.Text = ""
                    btnSubmit.Enabled = True
                    btnDraft.Enabled = True
                    If ddlLocation.SelectedIndex > 0 And ddlMainCategory.SelectedIndex > 0 And ddlSubCategory.SelectedIndex > 0 And ddlChildCategory.SelectedIndex > 0 Then
                        lnkShowEscaltion.Visible = True

                    Else
                        lnkShowEscaltion.Visible = False
                    End If

                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CHECK_SERVICE_INCHARGE")
                    sp1.Command.AddParameter("@LOC", ddlLocation.SelectedItem.Value, DbType.String)
                    sp1.Command.AddParameter("@MAIN_CATEGORY", ddlMainCategory.SelectedItem.Value, DbType.String)
                    sp1.Command.AddParameter("@SUB_CATEGORY", ddlSubCategory.SelectedItem.Value, DbType.String)
                    sp1.Command.AddParameter("@CHILD_CATEGORY", ddlChildCategory.SelectedItem.Value, DbType.String)
                    sp1.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
                    Dim flag1 As Integer = sp1.ExecuteScalar()
                    Dim Ser_Inch_errmsg As String = String.Empty
                    If flag1 = 0 Then
                        lblMsg.Text = "Service Incharges are Not mapped to the selected service."
                        btnDraft.Visible = False
                        btnSubmit.Visible = False
                    Else
                        If ViewState("Enable_Save_As_Draft") = 1 Then
                            btnDraft.Visible = True
                        Else
                            btnDraft.Visible = False
                        End If

                        btnSubmit.Visible = True

                        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CHECK_ESCALTIONS")
                        sp2.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
                        sp2.Command.AddParameter("@MAIN_CATEGORY", ddlMainCategory.SelectedItem.Value, DbType.String)
                        sp2.Command.AddParameter("@SUB_CATEGORY", ddlSubCategory.SelectedItem.Value, DbType.String)
                        sp2.Command.AddParameter("@CHILD_CATEGORY", ddlChildCategory.SelectedItem.Value, DbType.String)
                        sp2.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
                        Dim flag2 As Integer = sp2.ExecuteScalar()
                        If flag2 = 0 Then
                            lblMsg.Text = Ser_Inch_errmsg + "<br/> SLA was not Defined to the selected service."
                            btnDraft.Visible = False
                            btnSubmit.Visible = False
                            Exit Sub
                        Else
                            If ViewState("Enable_Save_As_Draft") = 1 Then
                                btnDraft.Visible = True
                            Else
                                btnDraft.Visible = False
                            End If
                            btnSubmit.Visible = True

                            ' CHECK AUTO REPEAT CALL YES/NO                
                            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
                            sp.Command.AddParameter("@TYPE ", 2, DbType.Int32)
                            Dim repeatCallSts = sp.ExecuteScalar()

                            If repeatCallSts = 1 Then
                                'CHECKING WHETHER THE TICKET HAS EXCEEDED FOR 3 TIMES OR NOT
                                Dim ds As New DataSet
                                Dim SPExdChk As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHK_TKT_3EXCEEDED")
                                SPExdChk.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
                                SPExdChk.Command.AddParameter("@MAIN_CATEGORY", ddlMainCategory.SelectedItem.Value, DbType.String)
                                SPExdChk.Command.AddParameter("@SUB_CATEGORY", ddlSubCategory.SelectedItem.Value, DbType.String)
                                SPExdChk.Command.AddParameter("@CHILD_CATEGORY", ddlChildCategory.SelectedItem.Value, DbType.String)
                                SPExdChk.Command.AddParameter("@AURID", txtEmpId.Text, DbType.String)
                                SPExdChk.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
                                ds = SPExdChk.GetDataSet()

                                Dim SPExdChkflag As Integer = ds.Tables(0)(0)(0)
                                If SPExdChkflag >= 3 Then
                                    ddlRepeatCalls.ClearSelection()
                                    ddlRepeatCalls.SelectedIndex = 2
                                    ddlRepeatCalls.Enabled = False

                                    Dim spLR As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LASTSERVED_REQID")
                                    spLR.Command.AddParameter("@LOC", ddlLocation.SelectedValue, DbType.String)
                                    spLR.Command.AddParameter("@MNCCAT", ddlMainCategory.SelectedValue, DbType.String)
                                    spLR.Command.AddParameter("@SUBCAT", ddlSubCategory.SelectedValue, DbType.String)
                                    spLR.Command.AddParameter("@CHILDCAT", ddlChildCategory.SelectedValue, DbType.String)
                                    spLR.Command.AddParameter("@AURID", txtEmpId.Text, DbType.String)
                                    spLR.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.Int32)
                                    ds = spLR.GetDataSet()

                                    If (ds.Tables(0).Rows.Count > 0) Then
                                        hdnReqid.Value = Convert.ToString(ds.Tables(0)(0)(0))
                                        ancReqid.Visible = True
                                        btnDraft.Visible = False
                                        btnSubmit.Text = "Reopen and Submit"
                                    Else
                                        ancReqid.Visible = False
                                        If ViewState("Enable_Save_As_Draft") = 1 Then
                                            btnDraft.Visible = True
                                        Else
                                            btnDraft.Visible = False
                                        End If
                                        btnSubmit.Text = "Submit Request"
                                    End If
                                Else
                                    ddlRepeatCalls.Enabled = True
                                    ancReqid.Visible = False
                                    If ViewState("Enable_Save_As_Draft") = 1 Then
                                        btnDraft.Visible = True
                                    Else
                                        btnDraft.Visible = False
                                    End If
                                    btnSubmit.Text = "Submit Request"
                                End If
                            Else

                            End If
                        End If
                    End If
                End If
            End If
    End Sub

    Private Sub RaiseReopenRequest(Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_MODIFY_REQ_REOPEN_TICKET")
        sp.Command.AddParameter("@REQID", hdnReqid.Value, DbType.String)
        sp.Command.AddParameter("@AURID", Session("uid"), DbType.String)
        sp.Command.AddParameter("@SER_CALL_TYPE", ddlRepeatCalls.SelectedValue, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            Dim Main As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyy")
            MaintenanceReq = Main + "/" + GetTodaysReqCount().ToString()
            Dim Req_status As Integer

            If ViewState("DSN_MAPPING_STA") = 1 Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_APPROVAL_LEVELS_BY_CATEGORIES")
                sp.Command.AddParameter("@MNC_CODE", ddlMainCategory.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@SUB_CAT", ddlSubCategory.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@CHILD_CAT", ddlChildCategory.SelectedItem.Value, DbType.String)
                ViewState("APPR_LEVELS") = sp.ExecuteScalar()

                If ViewState("APPR_LEVELS") = 0 Then
                    Req_status = 1
                Else
                    Req_status = 8
                End If
            Else
                Req_status = 1
            End If
            If hdnReqid.Value = "" Then
                RaiseRequest(MaintenanceReq, Req_status)
            Else
                RaiseReopenRequest(hdnReqid.Value) 'reopen
                lblMsg.Text = "Service Requisition (" + hdnReqid.Value + ") reopened successfully."
            End If
            ClearAll()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnDraft_Click(sender As Object, e As EventArgs) Handles btnDraft.Click
        Try
            Dim Main As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyy")
            MaintenanceReq = Main + "/" + GetTodaysReqCount().ToString()
            RaiseRequest(MaintenanceReq, 13)
            ClearAll()
            lblMsg.Text = "Request Saved Successfully"
        Catch ex As Exception

        End Try
    End Sub

    <System.Web.Script.Services.ScriptMethod(), _
    System.Web.Services.WebMethod()> _
    Public Shared Function SearchCustomers(ByVal prefixText As String) As List(Of String)
        Dim conn As SqlConnection = New SqlConnection
        conn.ConnectionString = ConfigurationManager _
         .ConnectionStrings("CSAmantraFAM").ConnectionString
        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandType = Data.CommandType.StoredProcedure
        cmd.CommandText = HttpContext.Current.Session("TENANT") & "." & "AUTOCOMPLETE_SEARCH"
        'cmd.CommandText = "getsearchEmpName"
        cmd.Parameters.AddWithValue("@TERM", prefixText)
        cmd.Connection = conn
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If

        Dim customers As List(Of String) = New List(Of String)
        Dim sdr As SqlDataReader = cmd.ExecuteReader
        While sdr.Read
            customers.Add(sdr("AUR_ID").ToString)
        End While

        conn.Close()
        Return customers
    End Function

    <System.Web.Script.Services.ScriptMethod(), _
   System.Web.Services.WebMethod()> _
    Public Shared Function GetUserDataClient(ByVal aur_id As String) As Object

        Dim sp As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "HRDGET_USER_DETAILS")
        sp.Command.AddParameter("@AUR_ID", aur_id, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            Return New With {.Name = ds.Tables(0).Rows(0).Item("NAME"), .DEPARTMENT = ds.Tables(0).Rows(0).Item("DEPARTMENT"), .SPACEID = ds.Tables(0).Rows(0).Item("SPACEID"), .PHONE_NUBER = ds.Tables(0).Rows(0).Item("PHONE_NUBER")}
        End If
        Return Nothing
    End Function

    'Protected Sub ddlChildCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChildCategory.SelectedIndexChanged
    '    Try
    '        If (ddlChildCategory.SelectedIndex <> 0) Then
    '            Dim dr As SqlDataReader = Child_getmain_dtls(ddlChildCategory.SelectedValue)
    '            If (dr.Read) Then

    '                ddlSubCategory.SelectedValue = dr("CHC_TYPE_SUBC_CODE")
    '                ddlMainCategory.SelectedValue = dr("CHC_TYPE_MNC_CODE")
    '            End If
    '        Else

    '            ddlSubCategory.SelectedIndex = 0
    '            ddlMainCategory.SelectedIndex = 0
    '        End If
    '    Catch exp As System.Exception
    '        Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmHDMRaiseRequest", "ddlChildCategory_SelectedIndexChanged", exp)
    '    End Try
    'End Sub

    Public Function Child_getmain_dtls(ByVal CHC_CODE As String) As SqlDataReader
        'strSQL = "select twr_cty_id,twr_loc_id,twr_cny_id from " & HttpContext.Current.Session("TENANT") & "."  & "tower where twr_code='" & tow_id & "' "
        'Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
        Dim sp1 As SqlParameter = New SqlParameter("@CHC_CODE", SqlDbType.NVarChar, 50)
        sp1.Value = CHC_CODE
        Dim ds As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_MAIN_SUB_BY_CHILD_CATEGORY", sp1)
        Return ds
    End Function


End Class


