﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class HDM_HDM_Webfiles_frmSLADetails
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            Dim LOC_CODE As String
            Dim MAIN_CAT_CODE As String
            Dim SUB_CAT_CODE As String
            Dim CHILD_CAT_CODE As String
            LOC_CODE = Request.QueryString("LOC_CODE")
            MAIN_CAT_CODE = Request.QueryString("MAIN_CAT_CODE")
            SUB_CAT_CODE = Request.QueryString("SUB_CAT_CODE")
            CHILD_CAT_CODE = Request.QueryString("CHILD_CAT_CODE")
            param = New SqlParameter(4) {}
            param(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
            param(0).Value = LOC_CODE
            param(1) = New SqlParameter("@MAIN_CAT_CODE", SqlDbType.NVarChar, 200)
            param(1).Value = MAIN_CAT_CODE
            param(2) = New SqlParameter("@SUB_CAT_CODE", SqlDbType.NVarChar, 200)
            param(2).Value = SUB_CAT_CODE
            param(3) = New SqlParameter("@CHILD_CAT_CODE", SqlDbType.NVarChar, 200)
            param(3).Value = CHILD_CAT_CODE
            param(4) = New SqlParameter("@COMPANY_ID", SqlDbType.Int)
            param(4).Value = Session("COMPANYID")
            Dim ds As New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("HDM_GET_SLA_DETAILS_BY_LOCATION", param)
            If ds.Tables(0).Columns.Count > 1 And ds.Tables(0).Rows.Count > 0 Then
                gvSLA.DataSource = ds.Tables(0) 'TransposeData.GenerateTransposedTable(ds.Tables(0))
                gvSLA.DataBind()
            Else
                gvSLA.DataSource = Nothing 'TransposeData.GenerateTransposedTable(ds.Tables(0))
                gvSLA.DataBind()
            End If

            gvSLAMapped.DataSource = ds.Tables(1)
            gvSLAMapped.DataBind()
        End If
    End Sub

End Class
