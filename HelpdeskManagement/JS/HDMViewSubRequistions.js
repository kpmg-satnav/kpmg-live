﻿app.service("HDMViewSubRequisitionsService", function ($http, $q, UtilityService) {
    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMViewSubRequistions/GetGriddata')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetMainCat = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMViewSubRequistions/GetMainCat')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.Getsubcat = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMViewSubRequistions/Getsubcat')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.Getchildcat = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMViewSubRequistions/Getchildcat')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.GetStatusList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMViewSubRequistions/GetStatusList')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.GetGriddataBySearch = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMViewSubRequistions/GetGriddataBySearch', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});


app.controller('HDMViewSubRequisitionsController', function ($scope, $q, $http, HDMViewSubRequisitionsService, UtilityService, $timeout, $filter, $window) {
    $scope.HDMViewSubRequisitions = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.Cols = [];
    $scope.CompanyVisible = 0;
    $scope.columnDefs = [];
    var RequestId;
    $scope.ReqId = true;
    // fill company ddls

    var ROLID; var b;
    $scope.PageLoad = function () {

        UtilityService.GetLocationsall(1).then(function (response) {
            if (response.data != null) {
                $scope.Locations = response.data;
                //angular.forEach($scope.Locations, function (value, key) {
                //    value.ticked = true;
                //});
            }
        });

        HDMViewSubRequisitionsService.GetMainCat().then(function (response) {
            if (response.data != null) {
                $scope.Main_Category = response.data;
                //angular.forEach($scope.Main_Category, function (value, key) {
                //    value.ticked = true;
                //});
            }
        });
        HDMViewSubRequisitionsService.Getsubcat().then(function (response) {
            if (response.data != null) {
                $scope.Sub_Category = response.data;
                //angular.forEach($scope.Sub_Category, function (value, key) {
                //    value.ticked = true;
                //});
            }
        });
        HDMViewSubRequisitionsService.Getchildcat().then(function (response) {
            if (response.data != null) {
                $scope.child_Category = response.data;
                //angular.forEach($scope.child_Category, function (value, key) {
                //    value.ticked = true;
                //});
            }
        });
        HDMViewSubRequisitionsService.GetStatusList().then(function (response) {
            if (response.data != null) {
                $scope.status = response.data;
                //angular.forEach($scope.status, function (value, key) {
                //    value.ticked = true;
                //});
            }
        });

        HDMViewSubRequisitionsService.GetGriddata().then(function (response) {
            console.log(response.data[0]);
            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                //if (response.data[0] == undefined) {
                //    ROLID = 0;
                //}
                //else {
                //    ROLID = response.data[0].ROLID;
                //    if (ROLID == "46") {
                //        $scope.gridOption.api.setRowData(response.data);
                //    }
                //    else {
                //        $scope.gridOptions.api.setRowData(response.data);
                //    }
                //}
                $scope.gridOptions.api.setRowData(response.data);
                progress(0, 'Loading...', false);
                //$scope.ReqId.disabled = true;
            }
        });
        
        //setTimeout(function () {
        //    UtilityService.GetCompanies().then(function (response) {
        //        if (response.data != null) {
        //            $scope.Company = response.data;

        //            var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
        //            if (a)
        //                a.ticked = true;

        //            $scope.LoadData();
        //            if (CompanySession == "1") { $scope.CompanyVisible = 0; }
        //            else { $scope.CompanyVisible = 1; }
        //        }
        //    });
        //}, 2000);
    }

    $scope.PageLoad();

    $scope.Redirect = function (data) {
        RequestId = data.REQUEST_ID;
        $window.location.href = 'HDMViewModifySubRequesitions.aspx?RequestId=' + RequestId + '';
    }

    // }

    var columnDefs = [
        { headerName: "Requisition Id", field: "REQUEST_ID", width: 130, cellClass: 'grid-align', template: '<a ng-click="Redirect(data)">{{data.REQUEST_ID}}</a>' },
        //{ headerName: "SER_ID", field: "SER_ID", width: 130, cellClass: 'grid-align', },    ng-class="{disabled:true}"
        { headerName: "Requested Date", field: "REQUESTED_DATE", width: 140, cellClass: 'grid-align', },
        { headerName: "Requested By", field: "REQUESTED_BY", width: 150, cellClass: 'grid-align', },
        { headerName: "Location", field: "LOCATION", width: 120, cellClass: 'grid-align', },
        { headerName: "Main Category", field: "MAIN_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Sub Category", field: "SUB_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Child Category", field: "CHILD_CATEGORY", width: 150, cellClass: 'grid-align', },
        //{ headerName: "Assigned To", field: "ASSIGNED_TO", width: 150, cellClass: 'grid-align', },
        { headerName: "Client Name", field: "CONTACT_NO", width: 130, cellClass: 'grid-align', },
        { headerName: "Status", field: "REQ_STATUS", width: 100, cellClass: 'grid-align', },
        //{ headerName: "No.Of Escalations", field: "ESC_COUNT", width: 100, cellClass: 'grid-align', },
        { headerName: "Time Taken", field: "TOTAL_TIME", width: 130, cellClass: 'grid-align', },
        { headerName: "Approval Comments", field: "UPDATED_COM", width: 130, cellClass: 'grid-align', },
        //{ headerName: "Assigned To ID", field: "ASSIGNED_TO", width: 150, cellClass: 'grid-align', },
        //{ headerName: "Assigned To Name", field: "ASSIGN_TO", width: 150, cellClass: 'grid-align', },
        //{ headerName: "Contact No", field: "CONTACT_NO", width: 150, cellClass: 'grid-align', },

        //{ headerName: "FeedBack", field: "FEEBACK", width: 100, cellClass: 'grid-align', },
        //{ headerName: "Remarks", field: "COMMENTS", width: 100, cellClass: 'grid-align', },
        //{ headerName: "Labour Cost", field: "LABOUR_COST", width: 100, cellClass: 'grid-align', },
        //{ headerName: "Spare Cost", field: "SPARE_COST", width: 100, cellClass: 'grid-align', },
        //{ headerName: "Additional Cost", field: "ADDITIONAL_COST", width: 120, cellClass: 'grid-align', },
        //{ headerName: "Closed Time", field: "CLOSED_TIME", width: 130, cellClass: 'grid-align', },
        //{ headerName: "Defined TAT", field: "DEFINED_TAT", width: 100, cellClass: 'grid-align', },
        //{ headerName: "Response TAT", field: "RESPONSE_TAT", width: 140, cellClass: 'grid-align', },
        //{ headerName: "Delayed TAT", field: "DELAYED_TAT", width: 140, cellClass: 'grid-align', },
    ];

    
    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: false,
        //groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Main Category", field: "Main_Category",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.HDMViewSubRequisitions.selVal = "LASTMONTH";
    $scope.HDMViewSubRequisitions.FromDate = moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY');
    $scope.HDMViewSubRequisitions.ToDate = moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY');
    $scope.HDMViewSubRequisitions.selstatus = "1";
    $scope.STATUSCHANGE = function () {
        switch ($scope.HDMViewSubRequisitions.selstatus) {
            case '1':
                $scope.HDMViewSubRequisitions.selstatus = "1";
                break;
            case '2':
                $scope.HDMViewSubRequisitions.selstatus = "2";
                break;
            case '3':
                $scope.HDMViewSubRequisitions.selstatus = "3";
                break
        }
    }
    $scope.rptDateRanges = function () {
        switch ($scope.HDMViewSubRequisitions.selVal) {
            case 'TODAY':
                $scope.HDMViewSubRequisitions.FromDate = moment().format('DD-MMM-YYYY');
                $scope.HDMViewSubRequisitions.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'YESTERDAY':
                $scope.HDMViewSubRequisitions.FromDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                $scope.HDMViewSubRequisitions.ToDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                break
            case '7':
                $scope.HDMViewSubRequisitions.FromDate = moment().subtract(6, 'days').format('DD-MMM-YYYY');
                $scope.HDMViewSubRequisitions.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case '30':
                $scope.HDMViewSubRequisitions.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
                $scope.HDMViewSubRequisitions.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'THISMONTH':
                $scope.HDMViewSubRequisitions.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
                $scope.HDMViewSubRequisitions.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
                break;
            case 'LASTMONTH':
                $scope.HDMViewSubRequisitions.FromDate = moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY');
                $scope.HDMViewSubRequisitions.ToDate = moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY');
                break;
        }
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    var clk = true;
    var ExportColumns;
    $scope.LoadData = function () {
        var unticked = _.filter($scope.Cols, function (item) {
            return item.ticked == false;
        });
        var ticked = _.filter($scope.Cols, function (item) {
            return item.ticked == true;
        });

        if ($scope.HDMViewSubRequisitions.FromDate == undefined && $scope.HDMViewSubRequisitions.ToDate == undefined) {
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var firstDayWithSlashes = ((firstDay.getMonth() + 1)) + '/' + firstDay.getDate() + '/' + firstDay.getFullYear();
            var lastDayWithSlashes = ((lastDay.getMonth() + 1)) + '/' + lastDay.getDate() + '/' + lastDay.getFullYear();

            $scope.HDMViewSubRequisitions.FromDate = firstDayWithSlashes;
            $scope.HDMViewSubRequisitions.ToDate = lastDayWithSlashes;
        }
        //var cp = _.find($scope.Company, { ticked: true })
        var params = {
            RequestID: $scope.HDMViewSubRequisitions.RequestID,
            LCMlst: $scope.HDMViewSubRequisitions.Locations,
            CHILDlst: $scope.HDMViewSubRequisitions.child_Category,
            STATUSlst: $scope.HDMViewSubRequisitions.status,
            HSTSTATUS: $scope.HDMViewSubRequisitions.selstatus,
            //Request_Type: $scope.HDMViewSubRequisitionsForm.Request_Type,
            FromDate: $scope.HDMViewSubRequisitions.FromDate,
            ToDate: $scope.HDMViewSubRequisitions.ToDate,
            //CompanyId: cp.CNP_ID
        };

        //HDMcustomizedReportService.GetGriddata().then(function (response) {
        //    console.log(response);
        //    if (clk) {
        //        $scope.Cols = response.data.coldata;
        //    }
        //    clk = false;
        //    ExportColumns = response.data.lst.exportCols;
        //    $scope.gridOptions.api.setColumnDefs(response.data.lst.Coldef);
        //    $scope.gridata = response.data.lst.griddata;
        //    console.log($scope.gridata);
        //    if ($scope.gridata == null) {
        //        $scope.GridVisiblity = false;
        //        $scope.gridOptions.api.setRowData([]);
        //    }
        //    else {
        //        // progress(0, 'Loading...', true);
        //        $scope.GridVisiblity = true;
        //        $scope.gridOptions.api.setRowData($scope.gridata);
        //        var cols = [];

        //        for (i = 0; i < unticked.length; i++) {
        //            cols[i] = unticked[i].value;
        //        }
        //        $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
        //        cols = [];
        //        for (i = 0; i < ticked.length; i++) {
        //            cols[i] = ticked[i].value;
        //        }
        //        //$scope.gridOptions.columnApi.setColumnsVisible(cols, true);
        //    }
        //    // progress(0, '', false);
        //})

        HDMViewSubRequisitionsService.GetGriddataBySearch(params).then(function (response) {
            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData(response.data);
                progress(0, 'Loading...', false);
            }
        });

    }, function (error) {
        console.log(error);
    }

    $scope.CustmPageLoad = function () {
    }, function (error) {
        console.log(error);
    }

    //$scope.GenerateFilterPdf = function () {
    //    progress(0, 'Loading...', true);
    //    var columns = ExportColumns; //[{ title: "Requisition Id", key: "REQ_ID" }, { title: "Requisition Date", key: "REQ_DATE" }, { title: "From Date", key: "FROM_DATE" }, { title: "To Date", key: "TO_DATE" }, { title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "Vertical", key: "VER_NAME" }, { title: "Status", key: "STATUS" }, { title: "Request Type", key: "REQUEST_TYPE" }];
    //    var model = $scope.gridOptions.api.getModel();
    //    var data = [];
    //    model.forEachNodeAfterFilter(function (node) {
    //        data.push(node.data);
    //    });
    //    var jsondata = JSON.parse(JSON.stringify(data));
    //    var doc = new jsPDF("landscape", "pt", "a4");
    //    doc.autoTable(columns, jsondata);
    //    doc.save("HDMcustomizationReport.pdf");
    //    setTimeout(function () {
    //        progress(0, 'Loading...', false);
    //    }, 1000);
    //}

    //$scope.GenerateFilterExcel = function () {
    //    progress(0, 'Loading...', true);
    //    var Filterparams = {
    //        skipHeader: false,
    //        skipFooters: false,
    //        skipGroups: false,
    //        allColumns: false,
    //        onlySelected: false,
    //        columnSeparator: ',',
    //        fileName: "HDMcustomizationReport.csv"
    //    };
    //    $scope.gridOptions.api.exportDataAsCsv(Filterparams);
    //    setTimeout(function () {
    //        progress(0, 'Loading...', false);
    //    }, 1000);
    //}

    //$scope.GenReport = function (HDMcustomized, Type) {
    //    progress(0, 'Loading...', true);
    //    $scope.HDMcustomized.Type = Type;
    //    if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
    //        if (Type == "pdf") {
    //            $scope.GenerateFilterPdf();
    //        }
    //        else {
    //            if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
    //                $scope.GenerateFilterExcel();
    //            }
    //        }
    //    }
    //    else {
    //        if (Type == 'xls') {
    //            //if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
    //            //    JSONToCSVConvertor($scope.gridata, "Customized Report", true, "CustomizedReport");
    //            //}
    //            //else {
    //            //    JSONToCSVConvertor($scope.gridata, "Customized Report", true, "CustomizedReport");
    //            //}
    //            $scope.GenerateFilterExcel();
    //        }
    //        else if (Type == 'pdf') {
    //            if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
    //                $scope.GenerateFilterPdf();
    //            }
    //            else {
    //                $scope.GenerateFilterPdf();
    //            }
    //        }
    //    };
    //    progress(0, '', false);
    //}
    $scope.locSelectAll = function () {
        $scope.HDMViewSubRequisitions.Locations = $scope.Locations;

    }
    $scope.getsubbymain = function () {
        UtilityService.getsubbymain($scope.HDMViewSubRequisitions.Main_Category).then(function (response) {
            $scope.Sub_Category = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getchildbysub = function () {
        UtilityService.getchildbysub($scope.HDMViewSubRequisitions.Sub_Category).then(function (response) {
            $scope.child_Category = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Main_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (value, key) {
            var main = _.find($scope.Main_Category, { MNC_CODE: value.MNC_CODE });
            if (main != undefined && value.ticked == true) {
                main.ticked = true;
                $scope.HDMViewSubRequisitions.Main_Category[0] = main;
            }
        });
    }
    $scope.childchange = function () {

        angular.forEach($scope.Main_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.child_Category, function (value, key) {
            var main = _.find($scope.Main_Category, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
            if (main != undefined && value.ticked == true) {
                main.ticked = true;
                $scope.HDMViewSubRequisitions.Main_Category[0] = main;
            }
        });

        angular.forEach($scope.child_Category, function (value, key) {
            var sub = _.find($scope.Sub_Category, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sub != undefined && value.ticked == true) {
                sub.ticked = true;
                $scope.HDMViewSubRequisitions.Sub_Category[0] = sub;
            }
        });

    }
    $scope.mainSelectAll = function () {
        $scope.HDMViewSubRequisitions.Main_Category = $scope.Main_Category;
        $scope.getsubbymain();
    }
    $scope.subSelectAll = function () {
        $scope.HDMViewSubRequisitions.Sub_Category = $scope.Sub_Category;
        $scope.getchildbysub();
    }
    $scope.subSelectAll = function () {
        $scope.HDMViewSubRequisitions.child_Category = $scope.child_Category;
        $scope.childchange();
    }

    

    


});
