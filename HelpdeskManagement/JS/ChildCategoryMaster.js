﻿app.service("ChildCategoryService", ['$http', '$q', function ($http, $q) {
    var deferred = $q.defer();
    this.getChildCategory = function () {
        return $http.get('../../../api/ChildCategory')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.CreateSubCategory = function (category) {
        deferred = $q.defer();
        return $http.post('../../../api/ChildCategory/Create', category)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.UpdateSubCategory = function (category) {
        deferred = $q.defer();
        return $http.post('../../../api/ChildCategory/UpdateChildcatData', category)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    //sub
    this.GetSubCategory = function () {
        deferred = $q.defer();
        return $http.get('../../../api/ChildCategory/GetSubCategory')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });

    }
    //main
    this.GetMainCategory = function () {
        deferred = $q.defer();
        return $http.get('../../../api/ChildCategory/GetMaincategory')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });

    }
    //main by sub
    this.GetChildCategoryBySub = function (SubCode) {
        deferred = $q.defer();
        return $http.get('../../../api/ChildCategory/GetMaincategoryBySub/' + SubCode)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    }

    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.get('../../../api/ChildCategory/GetGridData')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetCompanyModules = function () {
        deferred = $q.defer();
        return $http.get('../../../api/ChildCategory/GetCompanyModules')
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSubCategoryByModule = function (dataObject) {
        deferred = $q.defer();
        return $http.post('../../../api/ChildCategory/GetSubCategoryByModule', dataObject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('ChildCategoryController', ['$scope', '$q', 'ChildCategoryService', 'MainCategoryService', function ($scope, $q, ChildCategoryService, MainCategoryService) {
    $scope.StaDet = [{ Id: 'Active', Name: 'Active' }, { Id: 'Inactive', Name: 'Inactive' }];
    $scope.ChildCategory = {};
    $scope.categorydata = [];
    $scope.maincategorylist = [];
    $scope.subcategorylist = [];
    $scope.changedlist = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;

    ChildCategoryService.GetSubCategory().then(function (data) {
        $scope.subcategorylist = data;
        $scope.LoadData();
    }, function (error) {
        console.log(error);
    });

    $scope.getSubCategoriesByModule = function () {
        var params = {
            CompanyId: $scope.ChildCategory.Company[0].HDM_MAIN_MOD_ID
        }
        ChildCategoryService.GetSubCategoryByModule(params).then(function (response) {
            $scope.subcategorylist = response;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.SubCategoryChanged = function () {
        ChildCategoryService.GetChildCategoryBySub($scope.ChildCategory.SubCode).then(function (data) {
            $scope.maincategorylist = data;
            $scope.ChildCategory.MainCode = data[0].MNC_CODE;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.PageLoad = function () {
        MainCategoryService.HelpDeskModuleHide().then(function (response) {
            var result = response.data;
            if (result == "1") {
                ChildCategoryService.GetCompanyModules().then(function (response) {

                    $scope.Company = response.data.data;
                    $scope.ChildCategory.Company = $scope.Company;
                });
            } else {
                $scope.HelpDeskModule = true;
                $scope.ChildCategory.Company = "1";
            }

        });
    }
    $scope.PageLoad();

    $scope.Save = function () {
        if ($scope.IsInEdit) {
            $scope.ChildCategory.Company = $scope.ChildCategory.Company[0].HDM_MAIN_MOD_ID;
            ChildCategoryService.UpdateSubCategory($scope.ChildCategory).then(function (category) {
                $scope.ShowMessage = true;
                $scope.Success = "Data Updated Successfully";
                $scope.LoadData();
                var savedobj = {};
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.IsInEdit = false;
                $scope.EraseData();
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
                $scope.LoadData();
                $scope.ChildCategory = {};
                $scope.ActionStatus = 0;
            }, function (error) {
                console.log(error);
            })
        }
        else {
            $scope.ChildCategory.Status = "1";
            $scope.ChildCategory.Company = $scope.ChildCategory.Company[0].HDM_MAIN_MOD_ID;
            ChildCategoryService.CreateSubCategory($scope.ChildCategory).then(function (response) {
                $scope.ShowMessage = true;
                $scope.Success = "Data Inserted Successfully";
                //var savedobj = {};
                //angular.copy($scope.ChildCategory, savedobj)
                //$scope.gridata.unshift(savedobj);
                //$scope.gridOptions.api.setRowData($scope.gridata);
                $scope.EraseData();
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
                $scope.LoadData();
                $scope.ChildCategory = {};
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    }
    var columnDefs = [
        { headerName: "Child Category Code", field: "Code", width: 190, cellClass: 'grid-align' },
        { headerName: "Child Category Name", field: "Name", width: 180, cellClass: 'grid-align' },
        //{ headerName: "Sub Category Code", field: "SubCode", width: 190, cellClass: 'grid-align' },
        { headerName: "Sub Category Name", field: "SubName", width: 180, cellClass: 'grid-align' },
        { headerName: "Main Category", field: "MainName", width: 180, cellClass: 'grid-align' },
        { headerName: "Module", field: "Company", width: 180, cellClass: 'grid-align' },
        //{ headerName: "Status", suppressMenu: true, template: "{{ShowStatus(data.Status)}}", width: 270, cellClass: 'grid-align' },
        { headerName: "Status", field: "Status", suppressMenu: true, width: 270, cellClass: 'grid-align' },
        { headerName: "Edit", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', suppressMenu: true, width: 110 }];

    $scope.LoadData = function () {
        ChildCategoryService.GetGriddata().then(function (data) {
            $scope.gridata = data;
            //$scope.createNewDatasource();
            $scope.gridOptions.api.setRowData(data);
        }, function (error) {
            console.log(error);
        });
    }
    //$scope.pageSize = '10';

    $scope.createNewDatasource = function () {
        var dataSource = {
            pageSize: parseInt($scope.pageSize),
            getRows: function (params) {
                setTimeout(function () {
                    var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                    var lastRow = -1;
                    if ($scope.gridata.length <= params.endRow) {
                        lastRow = $scope.gridata.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
                }, 500);
            }
        };
        $scope.gridOptions.api.setDatasource(dataSource);
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        showToolPanel: true,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };
    $scope.LoadData();

    $scope.EditData = function (data) {
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;

        angular.forEach($scope.Company, function (val) {
            if (val.HDM_MAIN_MOD_NAME == data.Company) {
                val.ticked = true;
            }
            else { val.ticked = false; }

        });

        ChildCategoryService.GetSubCategoryByModule(data).then(function (response) {
            $scope.subcategorylist = response;
            ChildCategoryService.GetChildCategoryBySub(data.SubCode).then(function (responses) {
                $scope.maincategorylist = responses;
                $scope.ChildCategory.MainCode = responses[0].MNC_CODE;
            })
        })
        //$scope.ChildCategory.MainCode = data.MainCode;


        //$scope.ChildCategory.SubCode = data.SubCode
        //$scope.SubCategoryChanged();
        //$scope.SubCategoryChanged(data.SubCode);

        angular.copy(data, $scope.ChildCategory);
    }

    //$scope.GenReport = function (ChildMasterData, Type) {
    //    progress(0, 'Loading...', true);
    //    soobj = {};
    //    angular.copy(ChildMasterData, soobj);
    //    //soobj.CNP_NAME = Occupdata.CNP_NAME[0].CNP_ID;
    //    soobj.Type = Type;
    //    //soobj.FromDate = Occupdata.FromDate;
    //    //soobj.ToDate = Occupdata.ToDate;
    //    //soobj.Costcenterlst = Occupdata.Cost;
    //    //Occupdata.Type = Type;
    //    if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
    //        if (Occupdata.Type == "pdf") {
    //            $scope.GenerateFilterPdf();
    //        }
    //        else {
    //            $scope.GenerateFilterExcel();
    //        }
    //    }
    //    else {
    //        $http({
    //            url: UtilityService.path + '/api/ChildCategory/GetGridData',
    //            method: 'POST',
    //            data: soobj,
    //            responseType: 'arraybuffer'

    //        }).success(function (data, status, headers, config) {
    //            var file = new Blob([data], {
    //                type: 'application/' + Type
    //            });
    //            var fileURL = URL.createObjectURL(file);
    //            $("#reportcontainer").attr("src", fileURL);
    //            var a = document.createElement('a');
    //            a.href = fileURL;
    //            a.target = '_blank';
    //            a.download = 'HD Child Category Report.' + Type;
    //            document.body.appendChild(a);
    //            a.click();
    //            progress(0, '', false);
    //        }).error(function (data, status, headers, config) {
    //        });
    //    };
    //}

    $scope.EraseData = function () {
        $scope.ChildCategory = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frm.$submitted = false;
        angular.forEach($scope.Company, function (Company) {
            Company.ticked = false;
        });
    }

    $scope.onBtExport = function () {

        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: true,
            columnSeparator: ",",
            columnKeys: ['Child Category Code'],
            fileName: "HD Child Category Master.csv"

        };


        $scope.gridOptions.api.exportDataAsCsv(Filterparams);


    }


    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
}]);