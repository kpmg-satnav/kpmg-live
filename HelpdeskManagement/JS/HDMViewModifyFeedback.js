﻿app.service("HDMViewModifyFeedbackService", function ($http, $q, UtilityService) {

    this.GetDetailsByRequestId = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMViewModifyFeedback/GetDetailsByRequestId', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.ModifyFDBckDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMViewModifyFeedback/ModifyFDBckDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };
});

app.controller('HDMViewModifyFeedbackController', function ($scope, $q, $http, HDMViewModifyFeedbackService, HDMFeedBackService, UtilityService, $timeout, $filter, $window) {
    $scope.HDMViewModifyFeedback = {};

    $scope.getsubbymain = function () {
        UtilityService.getsubbymain($scope.HDMViewModifyFeedback.Main_Category).then(function (response) {
            $scope.Sub_Category = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.getCMByLoc = function () {
        var param =
        {
            LCM_CODE: $scope.HDMViewModifyFeedback.Clinic[0].LCM_CODE,
        };

        HDMFeedBackService.GetClinicMangerByLoc(param).then(function (response) {
            $scope.Clinic_Manager = response.data;
        }, function (error) {
            console.log(error);
        });
        HDMFeedBackService.GetAOMByLoc(param).then(function (response) {
            $scope.AOM = response.data;
        }, function (error) {
            console.log(error);
            });
        HDMFeedBackService.GetOTHERSByLoc(param).then(function (response) {
            $scope.OTHERS = response.data;
        }, function (error) {
            console.log(error);
        });

    }


    $scope.ModifyFdbkDetails = function () {
        var statuscom = "";
        var Others = "";
        var ClinicManager = "";
        var Aom = "";
        if ($scope.HDMViewModifyFeedback.STATUSCOM.length == "0") {
            statuscom = "";
        }
        else {
            statuscom = $scope.HDMViewModifyFeedback.STATUSCOM[0].SER_APP_STA_REMARKS_ID;
        }
        if ($scope.HDMViewModifyFeedback.OTHERS.length == "0") {
            Others = "";
        }
        else {
            var selectedOthers = [];
            angular.forEach($scope.HDMViewModifyFeedback.OTHERS, function (value, i) {
                selectedOthers[i] = value.OTH_ID;
            });
            Others = selectedOthers.join(",");
        }
        if ($scope.HDMViewModifyFeedback.Clinic_Manager.length == "0") {
            ClinicManager = "";
        }
        else {
            ClinicManager = $scope.HDMViewModifyFeedback.Clinic_Manager[0].CLINIC_MGR_ID;
        }
        if ($scope.HDMViewModifyFeedback.AOM.length == "0") {
            Aom = "";
        }
        else {
            Aom = $scope.HDMViewModifyFeedback.AOM[0].AOM_ID;
        }
        var params = {
            COMPLAINT_DATE: $scope.HDMViewModifyFeedback.ComplaintDate,
            LCM_CODE: $scope.HDMViewModifyFeedback.Clinic[0].LCM_CODE,
            CLINIC_MGR_ID: ClinicManager,// $scope.HDMViewModifyFeedback.Clinic_Manager[0].CLINIC_MGR_ID,
            CLIENTNAME: $scope.HDMViewModifyFeedback.Client_Name,
            CONTACT: $scope.HDMViewModifyFeedback.Contact,
            MAIN_CAT: $scope.HDMViewModifyFeedback.Sub_Category[0].MNC_CODE,
            SUB_CAT: $scope.HDMViewModifyFeedback.Sub_Category[0].SUBC_CODE,
            AOM_ID: Aom,//$scope.HDMViewModifyFeedback.AOM[0].AOM_ID,
            OTH_ID: Others,//$scope.HDMViewModifyFeedback.OTHERS[0].OTH_ID,
            STATUS: $scope.HDMViewModifyFeedback.Status[0].SER_APP_STA_REMARKS_ID,
            MODECOMMENTS: $scope.HDMViewModifyFeedback.ModeComments,
            MODE: $scope.HDMViewModifyFeedback.MODE[0].MODE_ID,
            STATUSCOMMENTS: $scope.HDMViewModifyFeedback.StatusComments,
            STATUSCOM: statuscom,
            PostedFiles: rowData,
            Data: data,
            RequestId: getUrlParameter('RequestId'),
            //Approvals: $scope.HDMRaiseSubRequest.Approvals[0].SER_APP_STA_ID,
            //CompanyId: cp.CNP_ID
        };

        if ($scope.HDMViewModifyFeedback.ComplaintDate != undefined && $scope.HDMViewModifyFeedback.Clinic[0].LCM_CODE != undefined && //$scope.HDMViewModifyFeedback.Clinic_Manager[0].CLINIC_MGR_ID != undefined &&
            $scope.HDMViewModifyFeedback.Client_Name != undefined && $scope.HDMViewModifyFeedback.Contact != undefined && $scope.HDMViewModifyFeedback.Sub_Category[0].MNC_CODE != undefined &&
            $scope.HDMViewModifyFeedback.Sub_Category[0].SUBC_CODE != undefined && $scope.HDMViewModifyFeedback.Status[0].SER_APP_STA_REMARKS_ID != undefined &&  //$scope.HDMViewModifyFeedback.AOM[0].AOM_ID != undefined 
            $scope.HDMViewModifyFeedback.ModeComments != undefined) {    //&& $scope.HDMViewModifyFeedback.StatusComments != undefined && $scope.HDMViewModifyFeedback.STATUSCOM[0].SER_APP_STA_REMARKS_ID != undefined
            HDMViewModifyFeedbackService.ModifyFDBckDetails(params).then(function (response) {
                $.ajax({
                    type: "POST",
                    url: 'https://live.quickfms.com/api/HDMFeedBack/UploadFiles',    // CALL WEB API TO SAVE THE FILES.
                    //enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                    cache: false,
                    data: params.Data, 		        // DATA OR FILES IN THIS CONTEXT.
                    success: function (data, textStatus, xhr) {
                        showNotification('success', 8, 'bottom-right', response.Message);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus + ': ' + errorThrown);
                    }
                });
                $scope.Clear();
                $scope.Redirect();
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.Redirect = function () {
        $window.location.href = 'HDMViewFeedBack.aspx';
    }

    $scope.Clear = function () {

        angular.forEach($scope.Clinic, function (Clinic) {
            Clinic.ticked = false;
        });
        angular.forEach($scope.Main_Category, function (Main_Category) {
            Main_Category.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (Sub_Category) {
            Sub_Category.ticked = false;
        });
        angular.forEach($scope.Status, function (Status) {
            Status.ticked = false;
        });
        angular.forEach($scope.STATUSCOM, function (STATUSCOM) {
            STATUSCOM.ticked = false;
        });
        angular.forEach($scope.MODE, function (MODE) {
            MODE.ticked = false;
        });
        angular.forEach($scope.Clinic_Manager, function (Clinic_Manager) {
            Clinic_Manager.ticked = false;
        });
        angular.forEach($scope.AOM, function (AOM) {
            AOM.ticked = false;
        });
        angular.element("input[type='file']").val(null);
        $scope.HDMViewModifyFeedback = {};
        $scope.HDMViewModifyFeedbackReq.$submitted = false;
        rowData = [];
        gridOptions.api.setRowData(rowData);
    }

    function getUrlParameter(param, dummyPath) {
        var sPageURL = dummyPath || window.location.search.substring(1),
            sURLVariables = sPageURL.split(/[&||?]/),
            res;

        for (var i = 0; i < sURLVariables.length; i += 1) {
            var paramName = sURLVariables[i],
                sParameterName = (paramName || '').split('=');

            if (sParameterName[0] === param) {
                res = sParameterName[1];
            }
        }

        return res;
    }

    $scope.PageLoad = function () {

        RequestId = getUrlParameter('RequestId');
        var params = {
            RequestId: RequestId,
        };

        HDMViewModifyFeedbackService.GetDetailsByRequestId(params).then(function (response) {
            console.log(response);
            $scope.HDMViewModifyFeedback = response;
            $scope.HDMViewModifyFeedback.Client_Name = response.data.CLIENTNAME;
            $scope.HDMViewModifyFeedback.Contact = response.data.CONTACT;
            var inv = response.data.COMPLAINT_DATE;
            $scope.HDMViewModifyFeedback.ComplaintDate = $filter('date')(inv, 'MM/dd/yyyy');
            $scope.HDMViewModifyFeedback.ModeComments = response.data.MODECOMMENTS;
            $scope.HDMViewModifyFeedback.StatusComments = response.data.STATUSCOMMENTS;
            var CLINIC_MGR_ID = response.data.CLINIC_MGR_ID;
            var AOM_ID = response.data.AOM_ID;
            var OTH_ID = response.data.OTH_ID;

            jsonstring = response.data.jsonstring;
            $scope.HDMViewModifyFeedback.jsonstringhdn = jsonstring;
            //$('#jsonstringhdn').val(jsonstring);

            var eGridDiv = document.getElementById('myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);
            var jsonobj = JSON.parse(jsonstring);

            if (jsonobj.length < 1) {
                console.log(1);
            } else {
                for (i = 0; i < jsonobj.length; i++) {
                    rowData.push(jsonobj[i]);
                }
                gridOptions.api.setRowData(rowData);
                gridLengthInitial = gridOptions.rowData.length;
            }
            //if (jsonobj = null) {
            //    console.log(2);
            //}
           
            //console.log(jsonobj.length);


            var lcmlst = response.data.lcmlst;
            var SubLst = response.data.SubLst;
            var StatusList = response.data.StatusList;
            var SubStatusList = response.data.SubStatusList;
            var Modes = response.data.Modes;
            var data = response.data;


            UtilityService.GetLocationsall(1).then(function (response) {
                if (response.data != null) {
                    $scope.Clinic = response.data;
                    angular.forEach(lcmlst, function (value, key) {
                        var sb = _.find($scope.Clinic, { LCM_CODE: value.LCM_CODE });
                        if (sb != undefined && sb.ticked == false) {
                            sb.ticked = true;
                        }
                    });
                }
            });

            HDMFeedBackService.GetMainCat().then(function (response) {
                if (response.data != null) {
                    $scope.Main_Category = response.data;
                    angular.forEach(SubLst, function (value, key) {
                        var main = _.find($scope.Main_Category, { MNC_CODE: value.MNC_CODE });
                        if (main != undefined && main.ticked == false) {
                            main.ticked = true;
                        }
                    });
                }
            });

            //console.log(SubLst[0].MNC_CODE);
            //console.log(SubLst);
            HDMFeedBackService.getmainbysub(SubLst[0].MNC_CODE).then(function (response) {
                if (response.data != null) {
                    $scope.Sub_Category = response.data;
                    angular.forEach(SubLst, function (value, key) {
                        var sub = _.find($scope.Sub_Category, { SUBC_CODE: value.SUBC_CODE });
                        if (sub != undefined && sub.ticked == false) {
                            sub.ticked = true;
                        }
                    });
                }
            }, function (error) {
                console.log(error);
            });

            //HDMFeedBackService.Getsubcat().then(function (response) {
            //    if (response.data != null) {
            //        $scope.Sub_Category = response.data;
            //        angular.forEach(SubLst, function (value, key) {
            //            var sub = _.find($scope.Sub_Category, { SUBC_CODE: value.SUBC_CODE });
            //            if (sub != undefined && sub.ticked == false) {
            //                sub.ticked = true;
            //            }
            //        });
            //    }
            //});

            //console.log(StatusList);
            HDMFeedBackService.GetStatusList().then(function (response) {
                if (response.data != null) {
                    $scope.Status = response.data;
                    angular.forEach(StatusList, function (value, key) {
                        var MainStatus = _.find($scope.Status, { SER_APP_STA_REMARKS_ID: value.SER_APP_STA_REMARKS_ID });
                        if (MainStatus != undefined && MainStatus.ticked == false) {
                            MainStatus.ticked = true;
                        }
                    });
                }
            });

            HDMFeedBackService.GetSubStatusList().then(function (response) {
                if (response.data != null) {
                    $scope.STATUSCOM = response.data;
                    angular.forEach(SubStatusList, function (value, key) {
                        var SubStatus = _.find($scope.STATUSCOM, { SER_APP_STA_REMARKS_ID: value.SER_APP_STA_REMARKS_ID });
                        if (SubStatus != undefined && SubStatus.ticked == false) {
                            SubStatus.ticked = true;
                        }
                    });
                }
            });

            HDMFeedBackService.GetFdBackMode().then(function (response) {
                if (response.data != null) {
                    $scope.MODE = response.data;
                    angular.forEach(Modes, function (value, key) {
                        var Mode = _.find($scope.MODE, { MODE_ID: value.MODE_ID });
                        if (Mode != undefined && Mode.ticked == false) {
                            Mode.ticked = true;
                        }
                    });
                }
            });

            var param =
            {
                LCM_CODE: response.data.LCM_CODE,
            };

            HDMFeedBackService.GetClinicMangerByLoc(param).then(function (response) {
                $scope.Clinic_Manager = response.data;
                angular.forEach($scope.Clinic_Manager, function (value, key) {
                    var Clinc_Mgr = _.find($scope.Clinic_Manager, { CLINIC_MGR_ID: CLINIC_MGR_ID });  //value.CLINIC_MGR_ID
                    if (Clinc_Mgr != undefined && Clinc_Mgr.ticked == false) {
                        Clinc_Mgr.ticked = true;
                    }
                });
            }, function (error) {
                console.log(error);
            });
            HDMFeedBackService.GetAOMByLoc(param).then(function (response) {
                $scope.AOM = response.data;
                angular.forEach($scope.AOM, function (value, key) {
                    var AOM = _.find($scope.AOM, { AOM_ID: AOM_ID });  //value.AOM_ID
                    if (AOM != undefined && AOM.ticked == false) {
                        AOM.ticked = true;
                    }
                });
            }, function (error) {
                console.log(error);
                });

            var OthersArr = OTH_ID.split(',');
            console.log(OthersArr);
            HDMFeedBackService.GetOTHERSByLoc(param).then(function (response) {
                $scope.OTHERS = response.data;
                angular.forEach(OthersArr, function (value, key) {
                    var OTHERS = _.find($scope.OTHERS, { OTH_ID: OthersArr[key] });
                    if (OTHERS != undefined && OTHERS.ticked == false) {
                        OTHERS.ticked = true;
                    }
                });
            }, function (error) {
                console.log(error);
            });

        }, function (error) {
            console.log(error);
            });

       
    
    }
    $scope.PageLoad();

    $scope.getmainbysub = function () {

        HDMFeedBackService.getmainbysub($scope.HDMViewModifyFeedback.Sub_Category[0].SUBC_CODE).then(function (response) {
            var MainLst = response.data;
            angular.forEach(MainLst, function (value, key) {
                var main = _.find($scope.Main_Category, { MNC_CODE: value.MNC_CODE });
                if (main != undefined && main.ticked == false) {
                    main.ticked = true;
                }
            });
        }, function (error) {
            console.log(error);
        });
    }

    $scope.getmainbysub = function () {
        HDMFeedBackService.getmainbysub($scope.HDMViewModifyFeedback.Sub_Category[0].SUBC_CODE).then(function (response) {
            angular.forEach($scope.Main_Category, function (Main_Category) {
                Main_Category.ticked = false;
            });
            var MainLst = response.data;
            angular.forEach(MainLst, function (value, key) {
                var main = _.find($scope.Main_Category, { MNC_CODE: value.MNC_CODE });
                if (main != undefined && main.ticked == false) {
                    main.ticked = true;
                }
            });
        }, function (error) {
            console.log(error);
        });
    }

});