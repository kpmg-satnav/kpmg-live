﻿app.service("SubCategoryService", ['$http', '$q', function ($http, $q) {
    var deferred = $q.defer();
    this.getSubCategory = function () {
        return $http.get('../../../api/SubCategory')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.CreateSubCategory = function (category) {
        deferred = $q.defer();
        return $http.post('../../../api/SubCategory/Create', category)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.UpdateSubCategory = function (category) {
        deferred = $q.defer();
        return $http.post('../../../api/SubCategory/UpdateSubcatData', category)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetMainCategory = function () {
        deferred = $q.defer();
        return $http.get('../../../api/SubCategory/GetMaincategory')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    }

    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.get('../../../api/SubCategory/GetGridData')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetMainCategoryByModule = function (dataObject) {
        deferred = $q.defer();
        return $http.post('../../../api/SubCategory/GetMainCategoryByModule', dataObject)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //this.HelpDeskModuleHide = function () {
    //    deferred = $q.defer();
    //    return $http.get('../../../api/SubCategory/HelpDeskModuleHide')
    //        .then(function (response) {
    //            deferred.resolve(response);
    //            return deferred.promise;
    //        }, function (response) {
    //            deferred.reject(response);
    //            return deferred.promise;
    //        });
    //};

}]);



app.controller('SubCategoryController', ['$scope', '$q', 'SubCategoryService', 'MainCategoryService', function ($scope, $q, SubCategoryService, MainCategoryService) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.SubCategory = {};
    $scope.categorydata = [];
    $scope.maincategorylist = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;

    SubCategoryService.GetMainCategory().then(function (data) {
        $scope.maincategorylist = data;
        $scope.LoadData();
    }, function (error) {
        console.log(error);
    });

    $scope.Save = function () {
        if ($scope.IsInEdit) {
            $scope.SubCategory.Company = $scope.SubCategory.Company[0].HDM_MAIN_MOD_ID;
            SubCategoryService.UpdateSubCategory($scope.SubCategory).then(function (category) {
                $scope.ShowMessage = true;
                $scope.Success = "Data Updated Successfully";
                var savedobj = {};
                $scope.gridOptions.api.setRowData($scope.gridata);
                $scope.EraseData();
                $scope.IsInEdit = false;
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
                $scope.LoadData();
                $scope.SubCategory = {};
                $scope.ActionStatus = 0;
            }, function (error) {
                console.log(error);
            })
        }
        else {
            $scope.SubCategory.SUBC_STA_ID = "1";
            $scope.SubCategory.Company = $scope.SubCategory.Company[0].HDM_MAIN_MOD_ID;
            SubCategoryService.CreateSubCategory($scope.SubCategory).then(function (response) {
                $scope.ShowMessage = true;
                $scope.Success = "Data Inserted Successfully";
                var savedobj = {};
                $scope.SubCategory.MNC_NAME = $scope.GetMainName($scope.SubCategory.MNC_CODE);
                $scope.LoadData();
                //angular.copy($scope.SubCategory, savedobj)
                //$scope.gridata.unshift(savedobj);
                //$scope.gridOptions.api.setRowData($scope.gridata);
                $scope.EraseData();
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
                $scope.SubCategory = {};
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    }
    var columnDefs = [
        { headerName: "Sub Category Code", field: "SUBC_CODE", width: 190, cellClass: 'grid-align' },
        { headerName: "Sub Category Name", field: "SUBC_NAME", width: 180, cellClass: 'grid-align' },
        { headerName: "Main Category", field: "MNC_NAME", width: 180, cellClass: 'grid-align' },
        { headerName: "Module", field: "Company", width: 180, cellClass: 'grid-align' },
        { headerName: "Status", suppressMenu: true, template: "{{ShowStatus(data.SUBC_STA_ID)}}", width: 270, cellClass: 'grid-align' },
        { headerName: "Edit", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110, suppressMenu: true }];

    $scope.PageLoad = function () {
        MainCategoryService.HelpDeskModuleHide().then(function (response) {
            var result = response.data;
            if (result == "1") {
                MainCategoryService.GetCompanyModules().then(function (response) {

                    $scope.Company = response.data.data;
                    $scope.SubCategory.Company = $scope.Company;
                });
            } else {
                $scope.HelpDeskModule = true;
                $scope.SubCategory.Company = "1";
            }

        });
    }
    $scope.PageLoad();

    $scope.getMainCategories = function () {
        var params = {
            CompanyId: $scope.SubCategory.Company[0].HDM_MAIN_MOD_ID
        }
        SubCategoryService.GetMainCategoryByModule(params).then(function (response) {
            $scope.maincategorylist = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.LoadData = function () {
        SubCategoryService.GetGriddata().then(function (data) {
            $scope.gridata = data;
            //$scope.createNewDatasource();
            $scope.gridOptions.api.setRowData(data);
        }, function (error) {
            console.log(error);
        });
    }

    //$scope.pageSize = '10';

    $scope.createNewDatasource = function () {
        var dataSource = {
            pageSize: parseInt($scope.pageSize),
            getRows: function (params) {
                setTimeout(function () {
                    var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                    var lastRow = -1;
                    if ($scope.gridata.length <= params.endRow) {
                        lastRow = $scope.gridata.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
                }, 500);
            }
        };
        $scope.gridOptions.api.setDatasource(dataSource);
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };
    //$scope.LoadData();

    $scope.EditData = function (data) {
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        angular.forEach($scope.Company, function (val) {
            if (val.HDM_MAIN_MOD_NAME == data.Company) {
                val.ticked = true;
            }
            else { val.ticked = false; }

        });
        SubCategoryService.GetMainCategoryByModule(data).then(function (response) {
            $scope.maincategorylist = response.data;
        })
        //SubCategoryService.GetMainCategory().then(function (data) {
        //    $scope.maincategorylist = data;
        //}, function (error) {
        //    console.log(error);
        //});
        angular.copy(data, $scope.SubCategory);
    }

    $scope.EraseData = function () {
        $scope.SubCategory = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frm.$submitted = false;
        angular.forEach($scope.Company, function (Company) {
            Company.ticked = false;
        });
    }

    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

    //get main cat name to display in grid after submit
    $scope.GetMainName = function (mncCode) {
        //var fruitId = $scope.maincategorylist;
        for (var i = 0; $scope.maincategorylist != null && i < $scope.maincategorylist.length; i += 1) {
            var result = $scope.maincategorylist[i];
            if (result.MNC_CODE === mncCode) {
                return result.MNC_NAME;
            }
        }
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
}]);