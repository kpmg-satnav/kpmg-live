﻿app.service("HDMViewModifySubRequesitionsService", function ($http, $q, UtilityService) {
    this.GetDetailsByRequestId = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMViewModifySubRequesitions/GetDetailsByRequestId', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.GetMainCat = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMViewSubRequistions/GetMainCat')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.Getsubcat = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMViewSubRequistions/Getsubcat')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.Getchildcat = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMViewSubRequistions/Getchildcat')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.ModifyEmployeeDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMViewModifySubRequesitions/ModifyEmpDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.GetApprovalStatus = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMViewModifySubRequesitions/GetApprovalStatus')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.ApproveEmployeeDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMViewModifySubRequesitions/ApproveEmployeeDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.CancelRequest = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMViewModifySubRequesitions/CancelRequest', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

});


app.controller('HDMViewModifySubRequesitionsController', function ($scope, $q, $http, HDMViewModifySubRequesitionsService, UtilityService, $timeout, $filter, $location, $window) {
    $scope.HDMViewModifySubRequesition = {};
    //$scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.Cols = [];
    $scope.CompanyVisible = 0;
    $scope.columnDef = [];
    $scope.EnableStatus = 1;

    var CHC_TYPE_NAME;
    var jsonstring;
    var RequestId;
    var columnDef = [
        { headerName: "Requested Date", field: "REQUESTED_DATE", width: 100, cellClass: 'grid-align', },
        { headerName: "Requested By", field: "REQUESTED_BY", width: 100, cellClass: 'grid-align' },
        //{ headerName: "Assigned To", field: "ASSIGNED_TO", width: 150, cellClass: 'grid-align' },
        //{ headerName: "Contact No", field: "CONTACT_NO", width: 130, cellClass: 'grid-align' },
        { headerName: "Updated Date", field: "SER_UPDATED_DT_SUB1", width: 100, cellClass: 'grid-align' },
        { headerName: "Updated By", field: "SER_UPDATED_BY_SUB1", width: 100, cellClass: 'grid-align' },
        { headerName: "Remarks", field: "ProbDesc", width: 150, cellClass: 'grid-align', cellRenderer: (invNum) => `<label for="male" title="${invNum.value}" style="color:black">${invNum.value}</label> ` },  //(invNum) => `<input type="submit" value="${invNum.value}"onclick = "Redirect()" ></input >  
        { headerName: "Status", field: "STATUS", width: 150, cellClass: 'grid-align' }
    ];


    var gridOption = {
        columnDefs: columnDef,
        //enableCellSelection: false,
        //rowData: null,
        //enableFilter: true,
        //enableSorting: true,
        enableColResize: true,
        //showToolPanel: true,
        ////groupAggFunction: groupAggFunction,
        //groupHideGroupColumns: true,
        onGridReady: function () {
            gridOption.api.sizeColumnsToFit();
        }
    };

    var STA_IS_BTN_ENB;
    $scope.PageLoad = function () {
        //$scope.HDMViewModifySubRequesition = HDMViewSubRequisService.get();
        //$scope.HDMViewModifySubRequesition = $rootScope.value;
        //console.log($scope.HDMViewModifySubRequesition);
        RequestId = getUrlParameter('RequestId');
        var params = {
            RequestId: RequestId,
        };

        HDMViewModifySubRequesitionsService.GetDetailsByRequestId(params).then(function (response) {
            //if (response.data[0].STATUS == "Request Cancelled") {
            //    $scope.ModifyEmp = true;
            //    $scope.CancelEmp = true;
            //}
            //if (response.data[0].STATUS == "Request Submitted" || ) {
            //    $scope.ModifyEmp = true;
            //    $scope.CancelEmp = true;
            //}
           console.log(response);


           $scope.gridata = response.data.GridDetails;
           console.log(response.data.GridDetails);

            var eGridDiv = document.getElementById('myGridData');
            new agGrid.Grid(eGridDiv, gridOption);
            gridOption.api.setRowData(response.data.GridDetails);
            //$scope.gridOptions.api.setRowData(response.data);
            //gridLengthInitial = gridOptions.rowData.length;

            //if (response.data == null) {
            //    $scope.gridOptions.api.setRowData([]);
            //    progress(0, 'Loading...', false);
            //}
            //else {
            //    progress(0, 'Loading...', true);
            //    console.log(response.data);
            //    //$scope.GridVisiblity = true;
            //    //console.log($scope.gridOptions1.api);
            //    $scope.gridOptions.api.setRowData(response.data);
            //    progress(0, 'Loading...', false);
            //}
            STA_IS_BTN_ENB = response.data.STA_IS_BTN_ENB;
            $scope.HDMViewModifySubRequesition = response;
            $scope.HDMViewModifySubRequesition.CustID = response.data.CustID;
            $scope.HDMViewModifySubRequesition.Main_Category = response.data.MAIN_CATEGORY;
            $scope.HDMViewModifySubRequesition.SUB_CATEGORY = response.data.SUB_CATEGORY;
            $scope.HDMViewModifySubRequesition.CHILD_CATEGORY = response.data.CHILD_CATEGORY;
            $scope.HDMViewModifySubRequesition.Locations = response.data.SER_LOC_CODE;

            $scope.HDMViewModifySubRequesition.CustName = response.data.CustName;
            var inv = response.data.InvoiceDate;
            $scope.HDMViewModifySubRequesition.InvoiceDate = $filter('date')(inv, 'MM/dd/yyyy');
            $scope.HDMViewModifySubRequesition.InvoiceNo = response.data.InvoiceNo;
            $scope.HDMViewModifySubRequesition.Mobile = response.data.Mobile;
            $scope.HDMViewModifySubRequesition.BilledBy = response.data.BilledBy;
            //$scope.HDMViewModifySubRequesition.Status = response.data.Table[0].STA_TITLE
            $scope.HDMViewModifySubRequesition.ProbDesc = response.data.ProbDesc;


            jsonstring = response.data.jsonstring;
            $scope.HDMViewModifySubRequesition.jsonstringhdn = jsonstring;
            $('#jsonstringhdn').val(jsonstring);


            var eGridDiv = document.getElementById('myGrid');
            new agGrid.Grid(eGridDiv, gridOptions);
            var jsonobj = JSON.parse(jsonstring);
            for (i = 0; i < jsonobj.length; i++) {
                rowData.push(jsonobj[i]);
            }
            gridOptions.api.setRowData(rowData);
            //$scope.gridOptions.api.setRowData(response.data);
            gridLengthInitial = gridOptions.rowData.length;
            CHC_TYPE_NAME = response.data.CHC_TYPE_NAME;

            var lcmlst = response.data.lcmlst;
            var ChildCatlst = response.data.ChildCatlst;
            UtilityService.GetLocationsall(3).then(function (response) {
                if (response.data != null) {
                    $scope.Locations = response.data;
                    angular.forEach(lcmlst, function (value, key) {
                        var sb = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
                        if (sb != undefined && sb.ticked == false) {
                            sb.ticked = true;
                        }
                    });
                }
            });
            HDMViewModifySubRequesitionsService.Getchildcat().then(function (response) {
                if (response.data != null) {
                    $scope.child_Category = response.data;
                    angular.forEach(ChildCatlst, function (value, key) {
                        var ch = _.find($scope.child_Category, { CHC_TYPE_CODE: value.CHC_TYPE_CODE });
                        if (ch != undefined && ch.ticked == false) {
                            ch.ticked = true;
                            //$scope.HDMViewModifySubRequesition.CHILD_CATEGORY.push(ch);
                        }
                    });
                }
            });

            HDMViewModifySubRequesitionsService.Getsubcat().then(function (response) {
                if (response.data != null) {
                    $scope.Sub_Category = response.data;
                    angular.forEach(ChildCatlst, function (value, key) {
                        var sub = _.find($scope.Sub_Category, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
                        if (sub != undefined && sub.ticked == false) {
                            sub.ticked = true;
                            //$scope.HDMViewModifySubRequesition.CHILD_CATEGORY.push(ch);
                        }
                    });
                }
            });

            switch (response.data.STA_IS_BTN_ENB) {
                case '1':
                    if (response.data.ROLE_ID == "43") {
                        $scope.ApproveEmp = true;
                    }
                    $scope.ModifyEmp = true;
                    $scope.CancelEmp = true;
                    break;
                case '3':
                    if (response.data.ROLE_ID == "43") {
                        $scope.EnableStatus = 0;
                        $scope.ApproveEmp = true;
                        $scope.StatusHide = true;
                        $scope.CommentsHide = true;
                    }
                    else {
                        $scope.ModifyEmp = true;
                        $scope.CancelEmp = true;
                    }
                    break;
                case '0':
                    $scope.StatusHide = true;
                    //$scope.CommentsHide = true;
                    $scope.ApproveEmp = true;
                    $scope.EnableStatus = 0;
                    break;
                case '4':
                    $scope.ApproveEmp = true;
                    break;
                default:
                    $scope.ModifyEmp = true;
                    $scope.CancelEmp = true;
                    $scope.ApproveEmp = true;
            }

            //if (response.data.STA_IS_BTN_ENB == "1") {
            //    if (response.data.ROLE_ID == "43") {
            //        $scope.ApproveEmp = true;
            //    }
            //    $scope.ModifyEmp = true;
            //    $scope.CancelEmp = true;
            //}
            //else if (response.data.STA_IS_BTN_ENB == "3") {
            //    if (response.data.ROLE_ID == "43") {
            //        $scope.EnableStatus = 0;
            //        $scope.ApproveEmp = true;
            //        $scope.StatusHide = true;
            //        $scope.CommentsHide = true;
            //    }
            //    else {
            //        $scope.ModifyEmp = true;
            //        $scope.CancelEmp = true;
            //    }
            //}
            //else if (response.data.STA_IS_BTN_ENB == "0") {
            //    $scope.StatusHide = true;
            //    $scope.CommentsHide = true;
            //    $scope.ApproveEmp = true;
            //    $scope.EnableStatus = 0;
            //}
            //else if (response.data.STA_IS_BTN_ENB == "4") {
            //    $scope.ApproveEmp = true;
            //}
            //else {
            //    $scope.ModifyEmp = true;
            //    $scope.CancelEmp = true;
            //    $scope.ApproveEmp = true;
            //}

            //if (response.data.STATUS == "2" || response.data.STATUS == "51" || response.data.STATUS == "52" || response.data.STATUS == "53"
            //    || response.data.STATUS == "54" || response.data.STATUS == "55") {
            //    $scope.ApproveEmp = true;
            //    if (response.data.STATUS == "54") {
            //        $scope.ModifyEmp = true;
            //        $scope.CancelEmp = true;
            //    }
            //    $scope.EnableStatus = 0;
            //    //$scope.CancelEmp = true;
            //} else {
            //    $scope.EnableStatus = 1;
            //    if (response.data.STATUS == "49") {

            //    }

            //}

        }, function (error) {
            console.log(error);
        });

        //$scope.HDMViewModifySubRequesition.Main_Category = '--select--';



        //UtilityService.GetLocationsall(1).then(function (response) {
        //    if (response.data != null) {
        //        $scope.Locations = response.data;
        //        angular.forEach($scope.Locations, function (value, key) {
        //            value.LCM_CODE = $scope.HDMViewModifySubRequesition.Locations;
        //            var Loc = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
        //            if (Loc != undefined) {
        //                Loc.ticked = true;
        //                $scope.HDMViewModifySubRequesition.Locations[0] = Loc;
        //            }
        //        });
        //    }
        //});

        HDMViewModifySubRequesitionsService.GetMainCat().then(function (response) {
            if (response.data != null) {
                $scope.Main_Category = response.data;
                //$scope.HDMViewModifySubRequesition.Main_Category = $scope.HDMViewModifySubRequesition.Main_Category;
                angular.forEach($scope.Main_Category, function (value, key) {
                    value.CHC_TYPE_MNC_CODE = $scope.HDMViewModifySubRequesition.Main_Category;
                    var main = _.find($scope.Main_Category, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
                    if (main != undefined) {
                        main.ticked = true;
                        $scope.HDMViewModifySubRequesition.Main_Category[0] = main;
                    }
                });
            }
        });
        //HDMViewModifySubRequesitionsService.Getsubcat().then(function (response) {
        //    if (response.data != null) {
        //        $scope.Sub_Category = response.data;
        //        //$scope.HDMViewModifySubRequesition.Sub_Category = $scope.Sub_Category;
        //        angular.forEach($scope.Sub_Category, function (value, key) {
        //            value.CHC_TYPE_SUBC_CODE = $scope.HDMViewModifySubRequesition.SUB_CATEGORY;
        //            var Sub = _.find($scope.Sub_Category, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
        //            if (Sub != undefined) {
        //                Sub.ticked = true;
        //                $scope.HDMViewModifySubRequesition.SUB_CATEGORY[0] = Sub;
        //            }
        //        });
        //        //console.log($scope.HDMRaiseSubRequest[0].Sub_Category);
        //    }
        //});
        //HDMViewModifySubRequesitionsService.Getchildcat().then(function (response) {
        //    if (response.data != null) {
        //        $scope.child_Category = response.data;

        //        angular.forEach($scope.child_Category, function (value, key) {
        //            value.CHC_TYPE_CODE = $scope.HDMViewModifySubRequesition.CHILD_CATEGORY;
        //            if (value.CHC_TYPE_CODE != undefined) {
        //                $scope.child_Category.CHC_TYPE_NAME = CHC_TYPE_NAME;
        //            }
        //            //value.CHC_TYPE_CODE = $scope.HDMViewModifySubRequesition.CHILD_CATEGORY;
        //            var Child = _.find($scope.child_Category, { CHC_TYPE_CODE: value.CHC_TYPE_CODE });
        //            if (Child != undefined) {
        //                Child.ticked = true;
        //                $scope.HDMViewModifySubRequesition.CHILD_CATEGORY[0] = Child;
        //            }
        //        });
        //    }
        //});



        HDMViewModifySubRequesitionsService.GetApprovalStatus().then(function (response) {
            $scope.ApprovalStatus = response.data;
            $scope.HDMViewModifySubRequesition.ApprovalStatus = $scope.ApprovalStatus;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.Clear = function () {

        angular.forEach($scope.Locations, function (Locations) {
            Locations.ticked = false;
        });
        angular.forEach($scope.Main_Category, function (Main_Category) {
            Main_Category.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (Sub_Category) {
            Sub_Category.ticked = false;
        });
        angular.forEach($scope.child_Category, function (child_Category) {
            child_Category.ticked = false;
        });
        angular.forEach($scope.AssetLocation, function (AssetLocation) {
            AssetLocation.ticked = false;
        });
        angular.forEach($scope.Floors, function (floor) {
            floor.ticked = false;
        });
        angular.forEach($scope.Verticals, function (vertical) {
            vertical.ticked = false;
        });
        angular.forEach($scope.CostCenters, function (costCenter) {
            costCenter.ticked = false;
        });
        angular.forEach($scope.ShiftFilter, function (shift) {
            shift.ticked = false;
        });
        angular.element("input[type='file']").val(null);
        $scope.HDMViewModifySubRequesition = {};
        $scope.HDMRaiseSubReq.$submitted = false;

    }

    $scope.Redirect = function () {
        //RequestId = data.REQUEST_ID;
        //$rootScope.value = data;
        //HDMViewSubRequisService.set(data);
        //console.log(RequestId);
        $window.location.href = 'HDMViewSubRequisitions.aspx';
    }

    $scope.ApproveEmpDetails = function () {
        var params = {
            RequestId: getUrlParameter('RequestId'),
            ApprovalStatus: $scope.HDMViewModifySubRequesition.ApprovalStatus[0].SER_APP_STA_REMARKS_ID,
            Comments: $scope.HDMViewModifySubRequesition.Comments,
        };
        if ($scope.HDMViewModifySubRequesition.ApprovalStatus[0].SER_APP_STA_REMARKS_ID != undefined) {
            HDMViewModifySubRequesitionsService.ApproveEmployeeDetails(params).then(function (response) {
                showNotification('success', 20, 'bottom-right', response.Message);
                //$scope.Clear();
                $scope.Redirect();
            }, function (error) {
                console.log(error);
            });
        }
    }

    $scope.ModifyEmpDetails = function () {
        
        var params = {
            RequestId: getUrlParameter('RequestId'),
            LCMlst: $scope.HDMViewModifySubRequesition.Locations,
            CHILDlst: $scope.HDMViewModifySubRequesition.child_Category,
            Mobile: $scope.HDMViewModifySubRequesition.Mobile,
            InvoiceNo: $scope.HDMViewModifySubRequesition.InvoiceNo,
            CustID: $scope.HDMViewModifySubRequesition.CustID,
            CustName: $scope.HDMViewModifySubRequesition.CustName,
            ProbDesc: $scope.HDMViewModifySubRequesition.ProbDesc,
            BilledBy: $scope.HDMViewModifySubRequesition.BilledBy,
            //BillDate: $scope.HDMViewModifySubRequesition.Billdate,
            //BillNo: $scope.HDMViewModifySubRequesition.BillNo,
            InvoiceDate: $scope.HDMViewModifySubRequesition.InvoiceDate,
            PostedFiles: fd,
            Comments: $scope.HDMViewModifySubRequesition.Comments
            //CompanyId: cp.CNP_ID
        };
        
        if ($scope.HDMViewModifySubRequesition.Locations.length != 0 && $scope.HDMViewModifySubRequesition.child_Category.length != 0 && $scope.HDMViewModifySubRequesition.InvoiceNo != undefined && $scope.HDMViewModifySubRequesition.InvoiceDate != undefined || $scope.HDMViewModifySubRequesition.ApprovalStatus[0].SER_APP_STA_REMARKS_ID != undefined) {
            HDMViewModifySubRequesitionsService.ModifyEmployeeDetails(params).then(function (response) {
                showNotification('success', 20, 'bottom-right', response.Message);
                //$scope.Clear();
                $scope.Redirect();
            }, function (error) {
                console.log(error);
            });

        }
    }

    $scope.CancelRequest = function () {
        var params = {
            RequestId: getUrlParameter('RequestId'),
            ProbDesc: $scope.HDMViewModifySubRequesition.ProbDesc
        };
        HDMViewModifySubRequesitionsService.CancelRequest(params).then(function (response) {
            showNotification('success', 20, 'bottom-right', response.Message);
            //$scope.Clear();
            $scope.Redirect();
        }, function (error) {
            console.log(error);
        });


    }

    $scope.getsubbymain = function () {
        UtilityService.getsubbymain($scope.HDMViewModifySubRequesition.Main_Category).then(function (response) {
            $scope.Sub_Category = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getchildbysub = function () {
        UtilityService.getchildbysub($scope.HDMViewModifySubRequesition.Sub_Category).then(function (response) {
            $scope.child_Category = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Main_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (value, key) {
            var main = _.find($scope.Main_Category, { MNC_CODE: value.MNC_CODE });
            if (main != undefined && value.ticked == true) {
                main.ticked = true;
                $scope.HDMViewModifySubRequesition.Main_Category[0] = main;
            }
        });
    }
    $scope.childchange = function () {

        angular.forEach($scope.Main_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.child_Category, function (value, key) {
            var main = _.find($scope.Main_Category, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
            if (main != undefined && value.ticked == true) {
                main.ticked = true;
                $scope.HDMViewModifySubRequesition.Main_Category[0] = main;
            }
        });

        angular.forEach($scope.child_Category, function (value, key) {
            var sub = _.find($scope.Sub_Category, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sub != undefined && value.ticked == true) {
                sub.ticked = true;
                $scope.HDMViewModifySubRequesition.Sub_Category[0] = sub;
            }
        });

    }


    //$scope.getchildbysub = function (MNC_SUB_CODE) {
    //    console.log(MNC_SUB_CODE);
    //    HDMRaiseSubRequestService.GetChildCategoriessBySub(MNC_SUB_CODE).then(function (response) {
    //        $scope.child_Category = response.data;
    //    }, function (error) {
    //        console.log(error);
    //    });
    //}
    //$scope.getsubbymain = function () {
    //    UtilityService.getsubbymain($scope.HDMcustomized.Main_Category).then(function (response) {
    //        $scope.Sub_Category = response.data;
    //    }, function (error) {
    //        console.log(error);
    //    });
    //}
    //$scope.getchildbysub = function () {
    //    UtilityService.getchildbysub($scope.HDMcustomized.Sub_Category).then(function (response) {
    //        $scope.child_Category = response.data;
    //    }, function (error) {
    //        console.log(error);
    //    });
    //    angular.forEach($scope.Main_Category, function (value, key) {
    //        value.ticked = false;
    //    });
    //    angular.forEach($scope.Sub_Category, function (value, key) {
    //        var main = _.find($scope.Main_Category, { MNC_CODE: value.MNC_CODE });
    //        if (main != undefined && value.ticked == true) {
    //            main.ticked = true;
    //            $scope.HDMcustomized.Main_Category[0] = main;
    //        }
    //    });
    //}
    $scope.childchange = function () {

        angular.forEach($scope.Main_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.child_Category, function (value, key) {
            var main = _.find($scope.Main_Category, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
            if (main != undefined && value.ticked == true) {
                main.ticked = true;
                $scope.HDMViewModifySubRequesition.Main_Category[0] = main;
            }
        });

        angular.forEach($scope.child_Category, function (value, key) {
            var sub = _.find($scope.Sub_Category, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sub != undefined && value.ticked == true) {
                sub.ticked = true;
                $scope.HDMViewModifySubRequesition.Sub_Category[0] = sub;
            }
        });

    }

    function getUrlParameter(param, dummyPath) {
        var sPageURL = dummyPath || window.location.search.substring(1),
            sURLVariables = sPageURL.split(/[&||?]/),
            res;

        for (var i = 0; i < sURLVariables.length; i += 1) {
            var paramName = sURLVariables[i],
                sParameterName = (paramName || '').split('=');

            if (sParameterName[0] === param) {
                res = sParameterName[1];
            }
        }

        return res;
    }

    //document.addEventListener("DOMContentLoaded", function () {
    //    alert('hai');
    //    debugger;
    //    var eGridDiv = document.getElementById('myGrid');
    //    new agGrid.Grid(eGridDiv, gridOptions);
    //    //var jsonstringhdn = document.getElementById("jsonstringhdn");
    //    //var jsonstring = jsonstringhdn.value;
    //    console.log(jsonstring);
    //    var jsonobj = JSON.parse(jsonstring);
    //    for (i = 0; i < jsonobj.length; i++) {
    //        rowData.push(jsonobj[i]);
    //    }
    //    gridOptions.api.setRowData(rowData);
    //    gridLengthInitial = gridOptions.rowData.length;
    //});
    $scope.PageLoad();

});
