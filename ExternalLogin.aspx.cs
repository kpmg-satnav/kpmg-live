﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ExternalLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var provider = Request.QueryString["tenant"];
        var ctx = Request.GetOwinContext();
        var result = ctx.Authentication.AuthenticateAsync("ExternalCookie").Result;
        ctx.Authentication.SignOut("ExternalCookie");

        if (result != null)
        {
            var claims = result.Identity.Claims.ToList();
            claims.Add(new Claim(ClaimTypes.AuthenticationMethod, provider));
            var ci = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
            ctx.Authentication.SignIn(ci);
        }
        Response.Redirect("~/login.aspx?tenant="+provider);
    }
}