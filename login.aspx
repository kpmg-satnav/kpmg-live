<%@ Page Language="VB" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KGS My Seat</title>
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--%>
    <link href="BootStrapCSS/Scripts/bootstrap4.0.css" rel="stylesheet" />
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--%>
    <link href="BootStrapCSS/font-awesome/css/font-awesome4.7.2.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/css/GlobalStyle.css">
    <link rel="stylesheet" type="text/css" href="assets/css/skincolors.css">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;400;700;900&display=swap" rel="stylesheet">
    <style type="text/css">
        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=90);
            opacity: 0.8;
            color: black;
        }

        .modalPopup {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding-top: 10px;
            padding-left: 10px;
            width: 300px;
            height: 140px;
        }
    </style>
</head>

<body>
    <div class="Login-body-wrapper">
        <div class="row m-0">
            <div class="col-md-6 col-lg-7 no-padding d-none d-sm-block">
                <div id="login-left-content">
                    <div class="row">
                        <%--<div class="col-8 offset-2 login-content">
                            <h4>Facilities Management
                                <br>
                                Software</h4>
                            <hr class="divider-30">
                            <p>
                                QuickFMS is an emerging player disrupting Global Computer Aided Facility Management
                                (CAFM) & Integrated WorkForce Management (IWFM) markets.
                            </p>
                            <div class="Login-left-buttons">
                                <button type="button" class="btn btn-Qfms">Learn More</button>
                            </div>
                        </div>--%>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 no-padding  d-block d-sm-none">
                <div id="login-bg-mobile">
                    <div class="row">
                        <div class="col-10 offset-1 login-content login-content-mobile text-center">
                            <div class="Qfm-logo logo-mobile text-center">
                                <img src="./assets/images/logo-light.png">
                            </div>
                            <!-- <h5>Facilities Management <br>
                                Software</h5> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-5">
                <div class="row h-100">

                    <div class="col-lg-8 offset-lg-2 my-auto">
                        <div class="Form-wrap">
                            <div id="login-form" class="login-forn-content tab-pane active">
                                <div class="Qfm-logo text-center d-none d-sm-block">
                                    <img src="../BootStrapCSS/images/MySeatlogo.png" alt="Company logo" width="100%" height="100%">
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lbl1" runat="server" CssClass="col-md-12" ForeColor="Red" Style="padding-left: 45px;"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlMain" runat="server" DefaultButton="btnSignIn">
                                    <form runat="server">
                                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>


                                        <asp:HiddenField ID="hidForModel" runat="server" />

                                        <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="hidForModel"
                                            CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                                        </cc1:ModalPopupExtender>

                                        <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" Style="display: none; color: green">
                                            You have already Logged-In. Do you want to Clear that Session?<br />
                                            <br />
                                            <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btn btn-primary " OnClick="btnYes_Click" />
                                            <asp:Button ID="btnClose" runat="server" Text="No" CssClass="btn" OnClick="btnClose_Click" />
                                        </asp:Panel>

                                        <asp:ValidationSummary ID="ValidationSummary2" CssClass="col-md-12" runat="server" ForeColor="Red" ValidationGroup="Val1" />

                                        <div class="form-group has-search">
                                            <span class="fa fa-id-card form-control-feedback"></span>
                                            <asp:RequiredFieldValidator ID="rfvtnt" runat="server" ControlToValidate="tenantID"
                                                Display="None" ErrorMessage="Please Enter Company Id" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                                            <asp:TextBox ID="tenantID" runat="server" type="text" autocomplete="off" class="form-control qfms-input" placeholder="Company Id" Visible="false" Text="KPMG"></asp:TextBox>

                                        </div>
                                        <div class="form-group has-search">
                                            <span class="fa fa-user form-control-feedback"></span>
                                            <asp:RequiredFieldValidator ID="rfvusr" runat="server" ControlToValidate="txtUsrId"
                                                Display="None" ErrorMessage="Please Enter Username" ValidationGroup="Val1"> </asp:RequiredFieldValidator>

                                            <asp:TextBox ID="txtUsrId" runat="server" type="text" autocomplete="off" class="form-control qfms-input" placeholder="User ID"></asp:TextBox>

                                        </div>
                                        <div class="form-group has-search">
                                            <span class="fa fa-key form-control-feedback"></span>

                                            <asp:RequiredFieldValidator ID="rfvpwd" runat="server" ControlToValidate="txtUsrPwd"
                                                Display="None" ErrorMessage="Please Enter Password" ValidationGroup="Val1"> </asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtUsrPwd" runat="server" TextMode="Password" autocomplete="off" autofill="false" MaxLength="20" class="form-control qfms-input" placeholder="Password"></asp:TextBox>

                                        </div>
                                        <asp:Button runat="server" ID="btnSignIn" class="btn btn-brand-primary submit-button"
                                            role="button" Text="Sign In" ValidationGroup="Val1" />

                                    </form>
                                    <div class="Login-form-footer text-center curser-pointer">

                                        <%--<div class="or-divider-social">
                                            <img src="./assets/images/login-divider.svg">
                                        </div>--%>
                                        <div class="form-group">
                                            <div class="checkbox col-sm-6">

                                                <div class="modal-body" id="modelcontainer">
                                                </div>
                                            </div>

                                        </div>
                                        </div>
                                </asp:Panel>
                                <asp:Panel ID="pnllic" runat="server">
                                    <form runat="server">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                            ForeColor="Red" ValidationGroup="Val3" />
                                        <div class="form-group has-search">
                                            <b>
                                                <asp:Label ID="lblKeyDesc" runat="server" CssClass="control-label" Text="License Code: "></asp:Label></b>
                                            <asp:Label ID="lblKey" runat="server" CssClass="control-label" Text="Key"></asp:Label>
                                        </div>
                                        <div class="form-group has-search">
                                            <%--<span class="input-group-addon"><i class="fa fa-user"></i></span>--%>
                                            <span class="fa fa-user form-control-feedback"></span>
                                            <asp:TextBox ID="txtSerialKey" runat="server" type="text" autofill="false" class="form-control qfms-input" placeholder="Enter Activation Code"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSerialKey"
                                                Display="None" ErrorMessage="Please Enter Valid Activation Code" ValidationGroup="Val3"> </asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6 controls">
                                            </div>
                                            <div class="col-sm-6 controls">
                                                <asp:Button ID="btnNext" runat="server" role="button" class="btn login-btn pull-right" Text="Next" ValidationGroup="Val3" />
                                            </div>
                                        </div>
                                    </form>
                                </asp:Panel>
                            </div>
                           
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="login-page-footer-text">
        <%--<p class="float-left">
            QuickFMS INC � 2020.
        </p>
        <p class="float-right">Powered by QuickFMS INC Team</p>--%>
    </div>

    <%--   <script type="text/javascript">
         function showPopWin() {

             $("#modalcontentframe").attr("src", "frmForgetPwd.aspx");
             $("#myModal").modal().fadeIn();
             return false;
             //$("#modelcontainer").load("frmForgetPwd.aspx", function (responseTxt, statusTxt, xhr) {
             //    $("#myModal").modal().fadeIn();
             //});
         }



         gapi.load('auth2', function () {
             gapi.auth2.init();
         });

         function onSignIn(googleUser) {
             console.log(googleUser);
             var profile = googleUser.getBasicProfile();
             var id_token = googleUser.getAuthResponse().id_token;
             console.log(profile);

             console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
             console.log('Name: ' + profile.getName());
             console.log('Image URL: ' + profile.getImageUrl());
             console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

             //window.location.href = "GoogleAuth.aspx?email=" + profile.getEmail();
             window.location.href = "GoogleAuth2.aspx?email=" + profile.getEmail() + "&token=" + id_token;

         }
     </script>--%>
</body>


<script src="assets/js/scripts.js"></script>
<script src="assets/jquery/jquery.min.js"></script>
<script src="assets/js/bootstrap.bundle.min.js"></script>

</html>
