﻿app.service("ASTDepreciationReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ASTDepreciationReport/GetDepreciationDetails', Depreciation)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

});
app.controller('ASTDepreciationReportController', function ($scope, $q, $http, ASTDepreciationReportService, UtilityService, $timeout, $filter) {
    $scope.Depreciation = {};
    $scope.Request_Type = [];
    $scope.categorylist = [];
    $scope.SubCatlist = [];
    $scope.Brandlist = [];
    $scope.Modellist = [];
    $scope.Loclist = [];
    // $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];

    $scope.Cols = [

                { COL: "Asset Id", value: "AAT_NAME", ticked: false },
                { COL: "Asset Type", value: "VT_TYPE", ticked: false },
                { COL: "Sub Category", value: "AST_SUBCAT_NAME", ticked: false },
                { COL: "Brand", value: "MANUFACTURER", ticked: false },
                { COL: "Model", value: "AST_MD_NAME", ticked: false },
                 { COL: "Asset Count", value: "Asset_count" },//, aggFunc: 'sum', suppressMenu: true, ticked: false
                { COL: "Location", value: "LCM_NAME", ticked: false },
                { COL: "Vendor Name", value: "AVR_NAME", ticked: false },
                { COL: "Asset Serial Number", value: "AAT_AST_SERIALNO", ticked: false },
                { COL: "Purchased Date", value: "PURCHASEDDATE", ticked: false },
                { COL: "Warranty Date", value: "AAT_WRNTY_DATE", ticked: false },
                { COL: "Asset Age", value: "Asset_Age", ticked: false },
                { COL: "Asset Life Span(in Months)", value: "TOTALMONTHS", ticked: false },
                { COL: "Status", value: "STATUS", ticked: false },
                { COL: "Asset Mapped To", value: "MAPPEDTO", ticked: false },
                { COL: "Asset Movement From", value: "MOVEDFROMLOCATION", ticked: false },
                { COL: "Asset Movement To", value: "MOVEDTOLOCATION", ticked: false },
                { COL: "Movement Raised By", value: "RAISEDBY", ticked: false },
                { COL: "Received By", value: "INTERRECEIVEDBY", ticked: false },
                { COL: "Disposal Status", value: "DISPOSAL_STATUS", ticked: false },
                { COL: "Manufacture Date", value: "AAT_MFG_DATE", ticked: false },

    ];

    $scope.columnDefs = [
    { headerName: "Asset Id", field: "AAT_NAME", width: 150, cellClass: 'grid-align', width: 145 },

    { headerName: "Asset Category", field: "VT_TYPE", cellClass: 'grid-align', width: 150 },
    { headerName: "Sub Category", field: "AST_SUBCAT_NAME", cellClass: 'grid-align', width: 150 },
    { headerName: "Brand", field: "MANUFACTURER", cellClass: 'grid-align', width: 200, },
    { headerName: "Model", field: "AST_MD_NAME", cellClass: 'grid-align', width: 200 },

    { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 200, },
    { headerName: "Vendor Name", field: "AVR_NAME", cellClass: 'grid-align', width: 200 },
    { headerName: "Asset Count", field: "Asset_count", cellClass: 'grid-align', width: 150, aggFunc: 'sum', suppressMenu: true, },

    { headerName: "Status", field: "STATUS", cellClass: 'grid-align', width: 200 },
    { headerName: "Asset Mapped To", field: "MAPPEDTO", cellClass: 'grid-align', width: 200, suppressMenu: true },
    { headerName: "Asset Movement From", field: "MOVEDFROMLOCATION", cellClass: 'grid-align', width: 200, suppressMenu: true },
    { headerName: "Asset Movement To", field: "MOVEDTOLOCATION", cellClass: 'grid-align', width: 200, suppressMenu: true },
    { headerName: "Movement Raised By", field: "RAISEDBY", cellClass: 'grid-align', width: 200, suppressMenu: true },
    { headerName: "Asset Received By", field: "INTERRECEIVEDBY", cellClass: 'grid-align', width: 200, suppressMenu: true },
    { headerName: "Asset Life Span(in Months)", field: "TOTALMONTHS", cellClass: 'grid-align', width: 200, suppressMenu: true },
    { headerName: "Asset Serial Number", field: "AAT_AST_SERIALNO", cellClass: 'grid-align', width: 150 },
    { headerName: "Purchased Date", field: "PURCHASEDDATE", template: '<span>{{data.PURCHASEDDATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, suppressMenu: true },
    ////{ headerName: "AMC Date", field: "AAT_AMC_DATE", template: '<span>{{data.AAT_AMC_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 200, suppressMenu: true },

    { headerName: "Warranty Date", field: "AAT_WRNTY_DATE", template: '<span>{{data.AAT_WRNTY_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, suppressMenu: true, },
    { headerName: "Asset Age", field: "Asset_Age", cellClass: 'grid-align', width: 100, suppressMenu: true },

    { headerName: "Disposal Status", field: "DISPOSAL_STATUS", cellClass: 'grid-align', width: 100, suppressMenu: true },
    { headerName: "Manufacture Date", field: "AAT_MFG_DATE", template: '<span>{{data.AAT_WRNTY_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 120, suppressMenu: true }



    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Asset Name", field: "AAT_NAME",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };


    function groupAggFunction(rows) {
        var sums = {
            Asset_count: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.Asset_count += (data.Asset_count);
        });
        return sums;
    }
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        UtilityService.getLocations(1).then(function (response) {
            if (response.data != null) {
                $scope.Loclist = response.data;
                angular.forEach($scope.Loclist, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.GetCategories(1).then(function (response) {
            if (response.data != null) {
                $scope.categorylist = response.data;
                angular.forEach($scope.categorylist, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.GetSubCategories(1).then(function (response) {
            if (response.data != null) {
                $scope.SubCatlist = response.data;
                angular.forEach($scope.SubCatlist, function (value, key) {
                    value.ticked = true;
                });
            }
        });

        UtilityService.GetBrands(1).then(function (response) {
            if (response.data != null) {
                $scope.Brandlist = response.data;
                angular.forEach($scope.Brandlist, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.GetModels(1).then(function (response) {
            if (response.data != null) {
                $scope.Modellist = response.data;
                angular.forEach($scope.Modellist, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.Depreciation.CNP_NAME = parseInt(CompanySessionId);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySessionId) });
                    a.ticked = true;
                });
                if (CompanySessionId == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }
            progress(0, 'Loading...', false);
        });

        //$scope.SubmitData(1);
        setTimeout(function () {
            $scope.SubmitData(1, 'All');
        }, 1000);
        progress(0, '', false);

    }, function (error) {
        console.log(error);
    }


    $scope.CatChanged = function () {
        UtilityService.GetSubCategoryByCategory($scope.Depreciation.selectedcat, 1).then(function (response) {
            if (response.data != null) {
                $scope.SubCatlist = response.data;


            }
        });
    }
    $scope.SubCatChanged = function () {

        UtilityService.GetBrandBySubCategory($scope.Depreciation.selectedsubcat, 1).then(function (response) {
            if (response.data != null) {
                $scope.Brandlist = response.data;

            }
        });
    }
    $scope.BrandChanged = function () {
        UtilityService.GetModelByBrand($scope.Depreciation.selectedBrands, 1).then(function (response) {
            if (response.data != null) {
                $scope.Modellist = response.data;

            }
        });
    }

    //$scope.CatChanged = function () {
    //    UtilityService.GetBrandByCategory($scope.Depreciation.selectedcat, 1).then(function (response) {
    //        if (response.data != null) {
    //            $scope.Brandlist = response.data;

    //        }
    //    });
    //}
    $scope.catSelectAll = function () {
        $scope.Depreciation.selectedcat = $scope.categorylist;
        $scope.CatChanged();
    }
    $scope.subcatSelectAll = function () {
        $scope.Depreciation.selectedsubcat = $scope.SubCatlist;
        $scope.SubCatChanged();
    }
    $scope.BrandSelectAll = function () {
        $scope.Depreciation.selectedBrands = $scope.Brandlist;
        $scope.BrandChanged();
    }
    $scope.CustmPageLoad = function () {


    }, function (error) {
        console.log(error);
    }
    $scope.SubmitData = function (sta) {
        if (sta == 1) {
            ReqType = $scope.Depreciation.Request_Type;
            var params = {
                Categorylst: $scope.Depreciation.selectedcat,
                Subcatlst: $scope.Depreciation.selectedsubcat,
                Modellst: $scope.Depreciation.selectedModels,
                Brandlst: $scope.Depreciation.selectedBrands,
                loclst: $scope.Depreciation.selectedLoc,
                CNP_NAME: $scope.Depreciation.CNP_NAME[0].CNP_ID,
                Request_Type: $scope.Depreciation.Request_Type
            };
        } else {
            var params = {
                Categorylst: $scope.Depreciation.selectedcat,
                Subcatlst: $scope.Depreciation.selectedsubcat,
                Modellst: $scope.Depreciation.selectedModels,
                Brandlst: $scope.Depreciation.selectedBrands,
                loclst: $scope.Depreciation.selectedLoc,
                CNP_NAME: $scope.Depreciation.CNP_NAME[0].CNP_ID,
                Request_Type: $scope.Depreciation.Request_Type
            };
        }
        CustomizedReportService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            progress(0, 'Loading...', true);
            $scope.GridVisiblity = true;
            $scope.GridVisiblity2 = true;
            if ($scope.gridata == null) {
                // $scope.GridVisiblity = false;
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            else {
                showNotification('', 8, 'bottom-right', '');
                $scope.GridVisiblity = true;
                $scope.GridVisiblity2 = true;
                $scope.gridOptions.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);

                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });

                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                console.log(ticked);
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, false);                cols = [];                for (i = 0; i < ticked.length; i++) {
                    // console.log(ticked[i]);
                    cols[i] = ticked[i].value;
                }                $scope.gridOptions.columnApi.setColumnsVisible(cols, true);


            }


        })
    }, function (error) {
        console.log(error);
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Asset Id", key: "AAT_NAME" }, { title: "Asset Category", key: "VT_TYPE" }, { title: "Brand", key: "MANUFACTURER" }, { title: "Location", key: "LCM_NAME" }, { title: "Vendor Name", key: "AVR_NAME" },


        { title: "Asset_count", key: "Asset_count" },
        { title: "Status", key: "STATUS" },
        { title: "Asset Mapped To", key: "MAPPEDTO" },
        { title: "Asset Movement From", key: "MOVEDFROMLOCATION" },
        { title: "Asset Movement To", key: "MOVEDTOLOCATION" },
        { title: "Movement Raised By", key: "RAISEDBY" },
             { title: "Asset Received By", key: "INTERRECEIVEDBY" },
                  { title: "Asset Life Span(in Months)", key: "TOTALMONTHS" },




        { title: "Asset Serial Number", key: "AAT_AST_SERIALNO" }, { title: "Purchased Date", key: "PURCHASEDDATE" }, { title: "Warranty Date", key: "AATD_WRNTY_DT" },
        { title: "Warranty Date", key: "AAT_MFG_DATE" }



        ];


        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("AssetDepreciationReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);

        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "AssetDepreciationReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Customized, Type) {
        progress(0, 'Loading...', true);
        Depreciation.categorylist = $scope.Depreciation.selectedcat;
        Depreciation.SubCatlist = $scope.Depreciation.SubCatlist;
        Depreciation.Brandlst = $scope.Depreciation.selectedBrands;
        Depreciation.Modellst = $scope.Depreciation.selectedModels;
        Depreciation.loclst = $scope.Depreciation.selectedLoc;
        Depreciation.ReqType = $scope.Depreciation.Request_Type;
        Depreciation.CNP_NAME = $scope.Depreciation.CNP_NAME[0].CNP_ID;
        Depreciation.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/ASTDepreciationReport/GetDepreciationData',
                method: 'POST',
                data: Customized,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'AssetDepreciationReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });
    $timeout($scope.LoadData, 100);
    $scope.Depreciation.Request_Type = "All";
});
