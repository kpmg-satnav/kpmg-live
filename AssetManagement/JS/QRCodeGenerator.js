﻿app.service("QRCodeGeneratorService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/QRCodeGenerator/GetGriddata',data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


});
app.controller('QRCodeGeneratorController', function ($scope, $q, $http, QRCodeGeneratorService, UtilityService, $timeout, $filter, $window, $document) {
    $scope.QRCodeGenerator = {};
    $scope.LoadInfo = [];

    UtilityService.GetCompanies().then(function (response) {
        if (response.data != null) {
            $scope.Company = response.data;
            $scope.QRCodeGenerator.CNP_NAME = parseInt(CompanySessionId);
            angular.forEach($scope.Company, function (value, key) {
                var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySessionId) });
                a.ticked = true;
            });
            if (CompanySessionId == "1") { $scope.EnableStatus = 1; }
            else { $scope.EnableStatus = 0; }
        }

    });
    UtilityService.GetLocationsall(1).then(function (response) {
        if (response.data != null) {
            $scope.Location = response.data;
            angular.forEach($scope.Location, function (item, index) {
                if (index == 0) {
                    item.ticked = true
                }

            })

        }
    });


    var columnDefs = [
        { headerName: 'Country', field: 'CNY_NAME', width: 150, height: 150 },
        { headerName: 'City', field: 'CTY_NAME', width: 150, height: 150 },
        { headerName: 'Location', field: 'LCM_NAME', width: 150, height: 150 },
        { headerName: 'Asset Code', field: 'AAT_CODE', width: 150, height: 150 },
        { headerName: 'Asset Name', field: 'AAT_NAME', width: 150, height: 150 },
        { headerName: 'Brand', field: 'AAT_AAB_CODE', width: 150, height: 150 },
        { headerName: 'Model', field: 'AST_MD_NAME', width: 150, height: 150 },
        { headerName: 'QR Code', field: 'QrCode', width: 150, height: 150, cellRenderer: deltaIndicator }
    ];
    $scope.gridOptions = {
        columnDefs: columnDefs,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
        rowHeight: 80
    };

    function deltaIndicator(params) {
        var element = document.createElement("span");
        var DivElement = document.createElement("div");      
        $(DivElement).qrcode(
            {
                width: 70,
                height: 70,
                text: params.data.AAT_CODE
            });      
        element.appendChild(DivElement);
        element.appendChild(document.createTextNode(params.value));
        return element;
    }



    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var params = {
            LCM_CODE: _.find($scope.Location, { ticked: true }).LCM_CODE,
            CompanyId: _.find($scope.Company, { ticked: true }).CNP_ID
        };

        QRCodeGeneratorService.GetGriddata(params).then(function (response) {

            if (response.griddata == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                $scope.LoadInfo = response.griddata;
                $scope.gridOptions.api.setRowData(response.griddata);

                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 600);

            }

        }, function (error) {
            console.log(error);
        });
    }
    setTimeout(function () { $scope.LoadData(); }, 1000);
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);

    }
    $scope.GenReport = function (data) {
        if (data == 'pdf') {
            var codes = {};
            const songs = [];
            angular.forEach($scope.LoadInfo, function (val) {
                songs.push(val.AAT_CODE);
            });

          
            console.log(songs);
            qr_generate(songs);
        }
    }
    
 
});
