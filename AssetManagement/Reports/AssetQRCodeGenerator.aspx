﻿<%@ Page Language="C#" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="../../BlurScripts/BlurJs/jquery.qrcode.min.js"></script>
<script>
jQuery(function(){
	jQuery('#output').qrcode("http://jetienne.com");
})
    $(document).ready(function () {
        $.ajax({
            type: 'GET',
            url: 'api/Employees',
            dataType: 'json',
            success: function (data) {

                ulEmployees.empty();
                $.each(data, function (index, val) {
                    var fullName = val.FirstName + ' ' + val.LastName;
                    ulEmployees.append('<li>' + fullName + '</li>')

                });
            }

        });
    });
  

  
    
</script>
    <style>
        .table-fixed thead {
  width: 97%;
}
.table-fixed tbody {
  height: 230px;
  overflow-y: auto;
  width: 100%;
}
.table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
  display: block;
}
.table-fixed tbody td, .table-fixed thead > tr> th {
  float: left;
  border-bottom-width: 0;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
 
<div class="container">
  <div class="row">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4>
           QR Code Report
          </h4>
        </div>
        <table class="table table-fixed">
          <thead>
            <tr>
              <th class="col-xs-8">Asset ID</th><th class="col-xs-2">QR Code</th>
            </tr>
          </thead>
          <tbody>
            <tr>
             <td class="col-xs-8">Mike Adams</td><td class="col-xs-2">23</td>
            </tr>
            <tr>
              <td class="col-xs-8">Holly Galivan</td><td class="col-xs-2">44</td>
            </tr>
            <tr>
              <td class="col-xs-8">Mary Shea</td><td class="col-xs-2">86</td>
            </tr>
            <tr>
              <td class="col-xs-8">Jim Adams</td><td>23</td>
            </tr>
            <tr>
            <td class="col-xs-8">Henry Galivan</td><td class="col-xs-2">44</td>
            </tr>
            <tr>
             <td class="col-xs-8">Bob Shea</td><td class="col-xs-2">26</td>
            </tr>
            <tr>
              <td class="col-xs-8">Andy Parks</td><td class="col-xs-2">56</td>
            </tr>
            <tr>
             <td class="col-xs-8">Bob Skelly</td><td class="col-xs-2">96</td>
            </tr>
            <tr>
            <td class="col-xs-8">William Defoe</td><td class="col-xs-2">13</td>
            </tr>
            <tr>
            <td class="col-xs-8">Will Tripp</td><td class="col-xs-2">16</td>
            </tr>
            <tr>
             <td class="col-xs-8">Bill Champion</td><td class="col-xs-2">44</td>
            </tr>
            <tr>
              <td class="col-xs-8">Lastly Jane</td><td class="col-xs-2">6</td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>
  

</div>

    </form>
</body>
</html>
