﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Partial Class FAM_FAM_Webfiles_frmGeneratePODetails
    Inherits System.Web.UI.Page
    Public param() As SqlParameter
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            Dim poid As String
            poid = Request.QueryString("id")
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@PO", SqlDbType.NVarChar, 200)
            param(0).Value = poid
            Dim ds As New DataSet
            ds = objsubsonic.GetSubSonicDataSet("USP_AMG_ITEM_PO_GetBYPOPRINT", param)
            Dim ds1 As New DataSet
            ds1 = objsubsonic.GetSubSonicDataSet("USP_AMG_ITEMPO_DETAILS_GetByPO", param)
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/GeneratePODetails.rdlc")

            ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("ViewPODetailsDS", ds.Tables(0)))
            ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("GenerateReqPODT", ds1.Tables(0)))


            Dim ci As New CultureInfo(Session("userculture").ToString())
            Dim nfi As NumberFormatInfo = ci.NumberFormat
            Dim p1 As New ReportParameter("Currencyparam", nfi.CurrencySymbol())
            ReportViewer1.LocalReport.SetParameters(p1)

            ReportViewer1.LocalReport.EnableExternalImages = True
            Dim imagePath As String = BindLogo()
            Dim parameter As New ReportParameter("ImagePath", imagePath)
            ReportViewer1.LocalReport.SetParameters(parameter)

            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.Visible = True

        End If
    End Sub

    Public Function BindLogo() As String
        Dim imagePath As String
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Update_Get_LogoImage")
        sp3.Command.AddParameter("@type", "2", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp3.Command.AddParameter("@Tenant", Session("TENANT"), DbType.String)
        Dim ds3 As DataSet = sp3.GetDataSet()
        If ds3.Tables(0).Rows.Count > 0 Then
            imagePath = ds3.Tables(0).Rows(0).Item("ImagePath")
        Else
            imagePath = "~/BootStrapCSS/images/yourlogo.png"
        End If
        Return New Uri(Server.MapPath(imagePath)).AbsoluteUri
    End Function

End Class
