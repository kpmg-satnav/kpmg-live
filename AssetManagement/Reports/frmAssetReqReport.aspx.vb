Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports Microsoft.Reporting.WebForms

Partial Class frmAssetREQreport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        Try
            If Not IsPostBack Then
                BindGrid()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_ASSETREQREPORT_DETAILS")
            sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)

            Dim ds As New DataSet
            ds = sp.GetDataSet()

            Dim rds As New ReportDataSource()
            rds.Name = "AssetReqRptDS"
            'This refers to the dataset name in the RDLC file
            rds.Value = ds.Tables(0)
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.DataSources.Clear()
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetReqReport.rdlc")
            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.Visible = True
            ReportViewer1.LocalReport.EnableHyperlinks = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Protected Sub ReportViewer1_Drillthrough(sender As Object, e As DrillthroughEventArgs) Handles ReportViewer1.Drillthrough
        'Get OrderID that was clicked by 
        'user via e.Report.GetParameters()
        Dim DrillThroughValues As ReportParameterInfoCollection = e.Report.GetParameters()

        Dim thisConnectionString As String = ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString
        'Dim thatConnectionString As String = ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString

        'This is just to show you how to iterate 
        'through the collection if you have
        'multiple parameters values instead of a single parameter value.
        'To process multiple parameters values, 
        'concatenate d.Values[0] into a string with a delimiter.
        'Use the Split() method to  separate values 
        'into an array. Assign indivdual array element to
        'corresponding parameter array element.
        For Each d As ReportParameterInfo In DrillThroughValues
            Session("RPTParam") = d.Values(0).ToString().Trim()
        Next
        Dim localreport As LocalReport = DirectCast(e.Report, LocalReport)
        Dim Level1SearchValue As SqlParameter() = New SqlParameter(0) {}
        'Fill dataset for Level1.rdlc
        Dim thisConnection As New SqlConnection(thisConnectionString)
        Dim Level1DataSet As New System.Data.DataSet()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_STATDTLS")
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        sp.Command.AddParameter("@status", Session("RPTParam").Trim())
        Level1DataSet = sp.GetDataSet()
        'Level1SearchValue(0) = New SqlParameter("@status", Session("RPTParam").Trim())
        'Level1DataSet = SqlHelper.ExecuteDataset(thisConnection, "GET_STATDTLS", Level1SearchValue)

        If Level1DataSet.Tables(0).Rows.Count > 0 Then
            Dim level1datasource As New ReportDataSource("AssetReqSubReportDS", Level1DataSet.Tables(0))
            localreport.DataSources.Clear()
            localreport.DataSources.Add(level1datasource)
            localreport.Refresh()
        End If
    End Sub

End Class
