﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmGeneratePODetails.aspx.vb" Inherits="FAM_FAM_Webfiles_frmGeneratePODetails" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="row table table table-condensed table-responsive">
            <div class="form-group">
                <div class="col-md-12">

                  <div align="right">
                        <button type="button" class="btn btn-default btn-sm" onclick="Print('MMAssets')" style="color: rgba(58, 156, 193, 0.87)">
                            <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                        </button>

                    </div>

                    <div id="MMAssets">
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" ShowToolBar="false"></rsweb:ReportViewer>
                    </div>
                </div>

            </div>

        </div>
    </form>     
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>


    <script src="../../Scripts/jQuery.print.js" defer></script>

    <script src="../../BlurScripts/BlurJs/DashboardPrint.js" defer></script>
</body>
</html>

