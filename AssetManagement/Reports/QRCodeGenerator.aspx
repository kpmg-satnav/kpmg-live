﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/plain; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.4/angular.min.js"></script>


    <style>
        canvas {
            width: 70px !important;
            height: 70px !important;
        }
    </style>
</head>
<body data-ng-controller="QRCodeGeneratorController" class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="QR Code Generator" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">QR Code Generator</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="frmQRCodeGenerator" name="QRCodeGenerator">
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Location</label>
                                    <div isteven-multi-select data-input-model="Location" data-output-model="QRCodeGenerator.LCM_NAME" button-label="icon LCM_NAME"
                                        item-label="icon LCM_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="QRCodeGenerator.LCM_NAME" name="LCM_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmQRCodeGenerator.$submitted && frmQRCodeGenerator.Location.$invalid" style="color: red">Please Select Location </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Company</label>
                                    <div isteven-multi-select data-input-model="Company" data-output-model="QRCodeGenerator.CNP_NAME" button-label="icon CNP_NAME"
                                        item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="QRCodeGenerator.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmQRCodeGenerator.$submitted && frmQRCodeGenerator.Company.$invalid" style="color: red">Please Select Company </span>
                                </div>
                            </div>
                            <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">From Date</label>
                                            <div class="input-group date" id='fromdate'>
                                                <input type="text" class="form-control" data-ng-model="QRCodeGenerator.FromDate" id="FromDate" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmQRCodeGenerator.$submitted && frmQRCodeGenerator.FromDate.$invalid" style="color: red;">Please From Date</span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">To Date</label>
                                            <div class="input-group date" id='todate'>
                                                <input type="text" class="form-control" data-ng-model="QRCodeGenerator.ToDate" id="ToDate" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmQRCodeGenerator.$submitted && frmQRCodeGenerator.ToDate.$invalid" style="color: red;">Please To Date</span>
                                        </div>
                                    </div>--%>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="box-footer text-right">
                                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="LoadData()" />
                                        <%--<a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-left: 18px">
                            <div class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <a data-ng-click="GenReport('doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                <%-- <a data-ng-click="exportToExcel('#exportthis')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>--%>
                                <a data-ng-click="GenReport('pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                            </div>
                        </div>

                        <div>
                            <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto" id="exportthis"></div>
                            </div>
                            <div id="dvTable" style="">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySessionId = '<%= Session["COMPANYID"]%>';
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };

        $('#FromDate').datepicker('setDate', new Date(moment().startOf('month').format('MM/DD/YYYY')));
        $('#ToDate').datepicker('setDate', new Date(moment().endOf('month').format('MM/DD/YYYY')));
    </script>

    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/QRCodeGenerator.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/FileSaver.min.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/jspdf.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/qr_generator.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/qrcode.js"></script>
    <script src="../../BlurScripts/BlurJs/jquery.qrcode.min.js"></script>
</body>
</html>

