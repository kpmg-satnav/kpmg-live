﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Security
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Reporting.WebForms

Partial Class FAM_FAM_Webfiles_frmAssetUtilizationReport
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            Binddrop()
            FillCompanies()
            bindgridview()
        End If
    End Sub

    Private Sub FillCompanies()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_COMPANIES")
        ddlCompany.DataSource = sp.GetReader
        ddlCompany.DataTextField = "CNP_NAME"
        ddlCompany.DataValueField = "CNP_ID"
        ddlCompany.DataBind()
    End Sub

    Public Sub Binddrop()
        Dim ds As New DataSet
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.Int)
        param(0).Value = 1
        ObjSubsonic.Binddropdown(ddlassetid, "USP_GET_ASSETCATEGORIESSALL", "VT_TYPE", "VT_CODE", param)
        ddlassetid.Items.Insert(0, "--All--")
        ddlassetid.Items.RemoveAt(1)
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        bindgridview()
    End Sub
   
    Public Sub bindgridview()
        Dim assetid As String = ""

        If ddlassetid.SelectedItem.Text = "--All--" Then
            assetid = ""
        Else
            assetid = ddlassetid.SelectedItem.Text
        End If

        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@assetcat", SqlDbType.VarChar, 200)
        param(0).Value = assetid
        param(1) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(1).Value = 1
        param(2) = New SqlParameter("@SortColumnName", SqlDbType.Char)
        param(2).Value = "PRODUCTNAME"
        param(3) = New SqlParameter("@SortOrderBy", SqlDbType.Char)
        param(3).Value = "ASC"
        param(4) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(4).Value = 100
        param(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(5).Value = 0
        param(6) = New SqlParameter("@COMPANY", SqlDbType.VarChar)
        param(6).Value = ddlCompany.SelectedValue

        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("AM_GET_ASSET_STATUSREPORT_V", param)

        Dim rds As New ReportDataSource()
        rds.Name = "AssetUtilRptDS"
        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetUtilReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
    End Sub

End Class

