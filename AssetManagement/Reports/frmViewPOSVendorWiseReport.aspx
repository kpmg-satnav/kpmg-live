﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewPOSVendorWiseReport.aspx.vb" Inherits="FAM_FAM_Webfiles_frmViewPOSVendorWiseReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Ast Mvmt Report" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Vendor Wise PO's</h3>
                </div>

                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                                <div class="row">
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Select Vendor</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Select Asset" ControlToValidate="ddlVendor"
                                                InitialValue="--Select--" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Company</label>
                                            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" ToolTip="Select company">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <br />
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row table table table-condensed table-responsive">
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" AsyncRendering="true"></rsweb:ReportViewer>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View PO Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>
        function showPopWin1(id) {
            //$("#modalcontentframe").attr("src", "frmGeneratePODetails.aspx?id=" + id);
            $("#modalcontentframe").attr("src", "/FAM/FAM_Webfiles/ExportPO.aspx?rid=" + id);
            $("#myModal").modal().fadeIn();
            return false;
        }
    </script>

    <script defer>
        function refreshSelectpicker() {
            $("#<%=ddlVendor.ClientID%>").selectpicker();
        $("#<%=ddlCompany.ClientID%>").selectpicker();
    }
    refreshSelectpicker();
    </script>

</body>
</html>
