﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="ASTDepreciationReportController" class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Depreciation Report" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Depreciation Report</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" name="DepreciationReport" data-valid-submit="SubmitData(0)" novalidate>
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Asset Category<span style="color: red;"></span></label>
                                    <div isteven-multi-select data-input-model="categorylist" data-output-model="Depreciation.selectedcat" data-button-label="icon CAT_NAME" data-item-label="icon CAT_NAME maker"
                                        data-on-item-click="CatChanged()" data-on-select-all="catSelectAll()" data-on-select-none="catSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Depreciation.selectedcat" name="CAT_NAME" style="display: none" required="" />
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Asset Sub Category<span style="color: red;"></span></label>
                                    <div isteven-multi-select data-input-model="SubCatlist" data-output-model="Depreciation.selectedsubcat" data-button-label="icon AST_SUBCAT_NAME" data-item-label="icon AST_SUBCAT_NAME maker"
                                        data-on-item-click="SubCatChanged()" data-on-select-all="subcatSelectAll()" data-on-select-none="" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Depreciation.selectedsubcat" name="AST_SUBCAT_NAME" style="display: none" required="" />
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Manufacturer <span style="color: red;"></span></label>
                                    <div isteven-multi-select data-input-model="Brandlist" data-output-model="Depreciation.selectedBrands" data-button-label="icon BRND_NAME" data-item-label="icon BRND_NAME maker"
                                        data-on-item-click="BrandChanged()" data-on-select-all="BrandSelectAll()" data-on-select-none="BrandSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Depreciation.selectedBrands" name="BRND_NAME" style="display: none" required="" />
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Model <span style="color: red;"></span></label>
                                    <div isteven-multi-select data-input-model="Modellist" data-output-model="Depreciation.selectedModels" data-button-label="icon MD_NAME" data-item-label="icon MD_NAME maker"
                                        data-on-item-click="MdlChanged()" data-on-select-all="MdlChangeAll()" data-on-select-none="MdlSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Depreciation.selectedModels" name="MD_NAME" style="display: none" required="" />
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="form-group" data-ng-class="{'has-error': DepreciationReport.$submitted && DepreciationReport.LCM_NAME.$invalid}">
                                        <label for="txtcode">Location <span style="color: red;"></span></label>
                                        <div isteven-multi-select data-input-model="Loclist" data-output-model="Depreciation.selectedLoc" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                            data-on-item-click="LocChanged()" data-on-select-all="LocChangeAll()" data-on-select-none="LocSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Depreciation.selectedLoc" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="DepreciationReport.$submitted && DepreciationReport.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': DepreciationReport.$submitted && DepreciationReport.Request_Type.$invalid}">
                                    <%--<label class="control-label">Status Type</label>
                                            <select id="Request_Type" name="Request_Type" required="" data-ng-model="Depreciation.Request_Type" class="selectpicker">
                                                <option value="All" selected>All</option>
                                                <option value="MAPPED">Mapped</option>
                                                <option value="UNMAPPED">UnMapped</option>
                                            </select>
                                            <span class="error" data-ng-show="DepreciationReport.$submitted && DepreciationReport.Request_Type.$invalid" style="color: red">Please Select Status</span>--%>

                                    <label for="txtcode">From Date</label>
                                    <div class="input-group date" id='fromdate'>
                                        <input type="text" class="form-control" data-ng-model="Depreciation.FromDate" id="Text1" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="DepreciationReport.$submitted && DepreciationReport.FromDate.$invalid" style="color: red;">Please From Date</span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': DepreciationReport.$submitted && DepreciationReport.Columns.$invalid}">
                                    <label class="control-label">Select Columns<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Cols" data-output-model="COL" button-label="icon COL" item-label="icon COL" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="2">
                                    </div>
                                    <input type="text" data-ng-model="COL" name="Columns" style="display: none" required="" />
                                    <span class="error" data-ng-show="DepreciationReport.$submitted && DepreciationReport.Columns.$invalid" style="color: red">Please Select Columns </span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                <div class="form-group">
                                    <label class="control-label">Company</label>
                                    <div isteven-multi-select data-input-model="Company" data-output-model="Depreciation.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                        item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="Depreciation.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="DepreciationReport.$submitted && DepreciationReport.Company.$invalid" style="color: red">Please Select Company </span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-9 col-sm-6 col-xs-12"></div>
                            <div class="col-md-3 col-sm-6 col-xs-12 text-left">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>


                        <div class="row" style="padding-left: 18px">
                            <div class="box-footer text-right" data-ng-show="GridVisiblity2" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <a data-ng-click="GenReport(Customized,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(Customized,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(Customized,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                            </div>
                        </div>

                        <div data-ng-show="GridVisiblity">
                            <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="../../Scripts/jspdf.min.js" defer></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySessionId = '<%= Session["COMPANYID"]%>';
    </script>

    <script src="../../SMViews/Utility.js" defer></script>

    <script src="../JS/ASTDepreciationReport.js" defer></script>
</body>
</html>
