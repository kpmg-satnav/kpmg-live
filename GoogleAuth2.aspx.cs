﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using System.Configuration;
using System.Net.Mail;
using System.Collections.ObjectModel;


partial class GoogleAuth2 : System.Web.UI.Page
{
    private string email;
    private string token;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Dim Remark As String = ""
        // If Remark = "ManualLogout" Then
        ProviderUserDetails ProviderUserDetailsobj;
        ProviderUserDetailsobj = GetUserDetails(Request.QueryString["token"]);
        if (ProviderUserDetailsobj != null && ProviderUserDetailsobj.Email== Request.QueryString["email"])
        {
            Session["useroffset"] = "+05:30";
            email = Request.QueryString["email"];
            token = Request.QueryString["token"];
            if (email != null)
            {
                DataSet ds = new DataSet();
                MailAddress address = new MailAddress(email);
                string ParentDB;
                ParentDB = ConfigurationManager.AppSettings["FRMDB"].ToString();
                string host = address.Host;
                string Tenant_Name = "";
                string Tenant_Id = "";
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(ParentDB + "." + "GET_DOMAIN");
                sp.Command.AddParameter("@DOMAIN", host, DbType.String);
                ds = sp.GetDataSet();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Session["TENANT"] = ds.Tables[0].Rows[0]["TENANT_ID"];
                    Tenant_Name = ds.Tables[0].Rows[0]["TENANT_NAME"].ToString();
                    GetDetailsByEmailId(email);
                }
                else
                    Response.Redirect("GoogleAuthError.aspx?redirect=domain");
            }
            else
                Response.Redirect("GoogleAuthError.aspx?redirect=error"); 
        }
    }
    public void GetDetailsByEmailId(string email)
    {
        try
        {
            Session["useroffset"] = "+05:30";
            DataSet ds = new DataSet();
            string Aur_Id = "";
            string User_Role = "";
            string Ad_ID = "";

            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GOOGLE_AUTHENTICATION");
            sp.Command.AddParameter("@email", email, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
            {
                Aur_Id = ds.Tables[0].Rows[0]["AUR_ID"].ToString();
                Session["UID"] = Aur_Id;
                User_Role = ds.Tables[0].Rows[0]["URL_ROL_ID"].ToString();
                Ad_ID = ds.Tables[0].Rows[0]["AD_ID"].ToString();


                FormsAuthentication.Initialize();
                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(Aur_Id, true, 60);
                string encryptedticket = FormsAuthentication.Encrypt(authTicket);
                HttpCookie authcookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedticket);

                Session["COMPANYID"] = ds.Tables[0].Rows[0]["COMPANYID"];

                SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AM_CHECK_CATEGORY_DEPRECIATION");
                DataSet ds1 = new DataSet();
                ds1 = sp1.GetDataSet();
                Session["DepMethod"] = ds1.Tables[0].Rows[0]["returnstatus"];

                Response.Cookies.Add(authcookie);
                Session["LoginTime"] = getoffsetdatetime(DateTime.Now);
                Session["uname"] = Ad_ID;
                displaybsmdata();
                GET_ASSET_MODULE_CHECK_FOR_USER();
                GetuserRoleMappingEdit();
                DateTime lastlogin = savelastlogin(Session["UID"].ToString());
                Session["Lastlogintime"] = lastlogin.ToString("dd MMM yyyy HH:mm:ss");
                Response.Redirect("frmAMTDefault.aspx");
            }
            else
                Response.Redirect("GoogleAuthError.aspx?redirect=invalid");
        }
        catch (Exception ex)
        {
        }
    }
    public void displaybsmdata()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "AMT_BSM_GETALL");
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                Session["Parent"] = ds.Tables[0].Rows[0]["AMT_BSM_PARENT"];
                Session["Child"] = ds.Tables[0].Rows[0]["AMT_BSM_CHILD"];
                // Session("GHT") = ds.Tables(1).Rows(0).Item("ASM_PARENT")
                if (ds.Tables[1].Rows.Count > 0)
                    Session["GHT"] = ds.Tables[1].Rows[0]["ASM_PARENT"];
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void GET_ASSET_MODULE_CHECK_FOR_USER()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(ConfigurationManager.AppSettings["FRMDB"] + "." + "GET_ASSET_MODULE_CHECK_FOR_USER");
            sp.Command.AddParameter("@TENANT_ID", Session["TENANT"], DbType.String);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                var asset = ds.Tables[0].Rows[0]["T_STA_ID"].ToString();                
                if (Convert.ToInt32(asset) == 1)
                    Get_asset_sysp_value();
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void Get_asset_sysp_value()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_AST_SYSP_VALUE");
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
                Session["Procurement"] = ds.Tables[0].Rows[0]["AST_SYSP_VAL1"];
        }
        catch (Exception ex)
        {
        }
    }
    public void GetuserRoleMappingEdit()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_USERROLEMAPPINGEDIT");

            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
                Session["USER_EDIT"] = ds.Tables[0].Rows[0]["SYSP_VAL1"];
        }
        catch (Exception ex)
        {
        }
    }
    public DateTime savelastlogin(string UserName)
    {
        DateTime ValidateStatus = getoffsetdatetime(DateTime.Now);
        string Mode = "1";
        DataSet ds = new DataSet();

        try
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@USR_ID", SqlDbType.NVarChar);
            param[0].Value = UserName;
            param[1] = new SqlParameter("@Mode", SqlDbType.NVarChar);
            param[1].Value = Mode;
            param[2] = new SqlParameter("@TIMEOUT", SqlDbType.NVarChar);
            param[2].Value = ConfigurationManager.AppSettings["TIMEOUT"].ToString();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SAVE_USER_LASTLOGIN", param);
            Session["LoginUniqueID"] = ds.Tables[0].Rows[0]["SNO"];
            ValidateStatus = Convert.ToDateTime(ds.Tables[0].Rows[0]["LASTLOGINTIME"]);
        }
        catch (Exception ex)
        {
        }
        finally
        {
        }
        return ValidateStatus;
    }

    public DateTime getoffsetdatetime(DateTime date)
    {
        Object O = HttpContext.Current.Session["useroffset"];
        if (O != null)
        {
            string Temp = O.ToString().Trim();
            if (!Temp.Contains("+") && !Temp.Contains("-"))
                Temp = Temp.Insert(0, "+");
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            DateTime startTime = DateTime.Parse(DateTime.UtcNow.ToString());
            DateTime _now = DateTime.Parse(DateTime.UtcNow.ToString());
            foreach (TimeZoneInfo timeZoneInfo__1 in timeZones)
            {
                if (timeZoneInfo__1.ToString().Contains(Temp))
                {
                    // Response.Write(timeZoneInfo.ToString());
                    // string _timeZoneId = "Pacific Standard Time";
                    TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id);
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
                    break;
                }
            }
            return _now;
        }
        else
            return DateTime.Now;
    }
    
    private const string GoogleApiTokenInfoUrl = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token={0}";
    public ProviderUserDetails GetUserDetails(string providerToken)
    {
        List<string> SupportedClientsIds = new List<string>();
        //SupportedClientsIds.Add("469715905219-73hob6mud1118mn4hoel5f5eg6fks432.apps.googleusercontent.com");
        SupportedClientsIds.Add("817305281174-8k1t7lombpfpo0gij169s90fcqo6imsq.apps.googleusercontent.com");
        var httpClient = new HttpClient();
        var requestUri = new Uri(string.Format(GoogleApiTokenInfoUrl, providerToken));

        HttpResponseMessage httpResponseMessage;
        try
        {
            httpResponseMessage = httpClient.GetAsync(requestUri).Result;
        }
        catch (Exception ex)
        {
            return null;
        }

        if (httpResponseMessage.StatusCode != HttpStatusCode.OK)
        {
            return null;
        }

        var response = httpResponseMessage.Content.ReadAsStringAsync().Result;
        var googleApiTokenInfo = JsonConvert.DeserializeObject<GoogleApiTokenInfo>(response);

        if (!SupportedClientsIds.Contains(googleApiTokenInfo.aud))
        {
            //Log.WarnFormat("Google API Token Info aud field ({0}) not containing the required client id", googleApiTokenInfo.aud);
            return null;
        }

        return new ProviderUserDetails
        {
            Email = googleApiTokenInfo.email
            //FirstName = googleApiTokenInfo.given_name,
            //LastName = googleApiTokenInfo.family_name,
            //Locale = googleApiTokenInfo.locale,
            //Name = googleApiTokenInfo.name,
            //ProviderUserId = googleApiTokenInfo.sub
        };
    }


}

public class GoogleApiTokenInfo
{
   
    public string iss { get; set; }

    public string at_hash { get; set; }

    public string aud { get; set; }

    public string sub { get; set; }

    public string email_verified { get; set; }

    public string azp { get; set; }

    public string email { get; set; }

    public string iat { get; set; }

    public string exp { get; set; }

    public string name { get; set; }

    public string picture { get; set; }

    public string given_name { get; set; }

    public string family_name { get; set; }

    public string locale { get; set; }

    public string alg { get; set; }

    public string kid { get; set; }
}

public class ProviderUserDetails
{
    public string Email { get; internal set; }
}
