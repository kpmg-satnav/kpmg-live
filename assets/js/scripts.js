jQuery(function ($) {

  $(".sidebar-dropdown > a").click(function () {
    $(".sidebar-submenu").slideUp(200);
    if (
      $(this)
        .parent()
        .hasClass("active")
    ) {
      $(".sidebar-dropdown").removeClass("active");
      $(this)
        .parent()
        .removeClass("active");
    } else {
      $(".sidebar-dropdown").removeClass("active");
      $(this)
        .next(".sidebar-submenu")
        .slideDown(200);
      $(this)
        .parent()
        .addClass("active");
    }
  });

 function isMobile(){
        if (window.innerWidth <= 780) {
            $(".page-wrapper").removeClass("toggled");
        }
        else {
            $(".page-wrapper").addClass("toggled");
        }
    }

    isMobile();

  $(".HideMenuForMobile").click(function () {
        if (window.innerWidth <= 780) {
            $(".page-wrapper").removeClass("toggled");
        }
    });

function isMobile() {
        //var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        //if (isMobile) {
        if (window.innerWidth <= 780) {
            $(".page-wrapper").removeClass("toggled");
        }
        else {
            $(".page-wrapper").addClass("toggled");
        }
    };
    isMobile();

    $(".HideMenuForMobile").click(function () {
        if (window.innerWidth <= 780) {
            $(".page-wrapper").removeClass("toggled");
        }
    });

  $("#close-sidebar").click(function () {
    $(".page-wrapper").removeClass("toggled");
  });
  $("#show-sidebar").click(function () {
    $(".page-wrapper").addClass("toggled");
  });

  $(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
  });

  $(document).ready(function () {
    $('.table-tab-icons li').click(function () {
      $('li').removeClass("active-tabs");
      $(this).addClass("active-tabs");
    });
  });

  $(document).ready(function () {
    $('#tslshow .date-inline').click(function () {
      $('.date-inline').removeClass("date-selected");
      $(this).addClass("date-selected");
    });
  });

  $(".close-alert").click(function (e) {
    $(this).parent().remove();
    e.preventDefault();
  });

  $(document).ready(function () {
    $("#btnFetch").click(function () {
      // disable button
      $(this).prop("disabled", true);
      // add spinner to button
      $(this).html(
        '<i class="fa fa-circle-o-notch fa-spin"></i> loading...'
      );
    });
  });


  $(document).ready(function () {

    var navListItems = $('ul.setup-panel li a'),
      allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
        $item = $(this).closest('li');

      if (!$item.hasClass('disabled')) {
        navListItems.closest('li').removeClass('active');
        $item.addClass('active');
        allWells.hide();
        $target.show();
      }
    });

    $('ul.setup-panel li.active a').trigger('click');

    // DEMO ONLY //
    $('#activate-step-2').on('click', function (e) {
      $('ul.setup-panel li:eq(1)').removeClass('disabled');
      $('ul.setup-panel li a[href="#step-2"]').trigger('click');
      $(this).remove();
    })

    $('#activate-step-3').on('click', function (e) {
      $('ul.setup-panel li:eq(2)').removeClass('disabled');
      $('ul.setup-panel li a[href="#step-3"]').trigger('click');
      $(this).remove();
    })

    $('#activate-step-4').on('click', function (e) {
      $('ul.setup-panel li:eq(3)').removeClass('disabled');
      $('ul.setup-panel li a[href="#step-4"]').trigger('click');
      $(this).remove();
    })

    $('#activate-step-3').on('click', function (e) {
      $('ul.setup-panel li:eq(2)').removeClass('disabled');
      $('ul.setup-panel li a[href="#step-3"]').trigger('click');
      $(this).remove();
    })
  });

});




