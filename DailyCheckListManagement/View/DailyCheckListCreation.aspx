﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js" defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <script type="text/javascript" defer>

        $(document).ready(function () {
            $("input[type=file]").click(function () {
                $(this).val("");
            });

            $("input[type=file]").change(function () {
                alert($(this).val());
            });
        });

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: 'today',
                maxDate: 'today'
            });
        };

        function OnlyNumeric(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }


    </script>
    <style>
        .col-xs-3 selected {
            border-color: blue;
        }



        .has-error2 {
            border-style: solid;
            border-color: #ff0000;
        }

        .has-error3 {
        }

        .mystyle {
            border-color: red !important;
            border-width: 2px !important;
        }

        .thead {
            border-style: solid;
            border-color: #ff0000;
        }



        .highlight {
            background-color: red;
        }

        input[type='radio'], label {
            margin: 10px;
        }

        .clearBoth {
            clear: both;
        }

        .Fonts {
            font-size: 14px;
        }

        input {
            height: 35px;
            font-size: 15px;
        }



        /*th {
            color: black;
            background: #99CCFF;
            font-size: 15px;
        }*/

        /*table, th, td {
            border: 1px solid black;
            float: left;
        }*/

        .panel {
            box-shadow: 0 5px 5px 0 rgba(0,0,0,.25);
        }

        .panel-heading {
            border-bottom: 1px solid rgba(0,0,0,.12);
        }

        .list-inline {
            display: block;
        }

            .list-inline li {
                display: inline-block;
            }

                .list-inline li:after {
                    content: '|';
                    margin: 0 10px;
                }
    </style>

</head>

<body data-ng-controller="DailyCheckListCreationController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">

                        <div class="panel-heading" style="height: 42px;">
                            <h6 class="panel-title"><b>Security Guard Daily Checklist</b><a href="#ex1" rel="modal:open" style="margin-left: 600px; color: green;">Saved Drafts</a></h6>
                        </div>

                        <div class="panel-body" style="padding-right: 10px;">

                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <span style="color: red;">*</span>  Required &nbsp; &nbsp;   <%--<span style="color: red;">**</span>  Select to auto fill the data--%>
                                </div>
                            </div>

                            <br />

                            <form id="Form1" name="frmDailyCheckList" data-valid-submit="Creation()" novalidate>

                                <div class="row">



                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmDailyCheckList.$submitted && frmDailyCheckList.LCM_NAME.$invalid}">
                                            <label class="control-label">Location <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Location" data-output-model="CheckList.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="CheckList.Location" name="LCM_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmDailyCheckList.$submitted && frmDailyCheckList.LCM_NAME.$invalid" style="color: red">Please select Location </span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmDailyCheckList.$submitted && frmDailyCheckList.INSPECTOR.$invalid}">
                                            <label class="control-label">Inspection By <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Inspection" data-output-model="CheckList.Inspection" data-button-label="icon INSPECTOR" data-item-label="icon INSPECTOR"
                                                data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="CheckList.Inspection" name="INSPECTOR" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmDailyCheckList.$submitted && frmDailyCheckList.INSPECTOR.$invalid" style="color: red">Please select Inspection By  </span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmDailyCheckList.$submitted && frmDailyCheckList.SVR_FROM_DATE.$invalid}">
                                            <label class="control-label">Inspection Date <span style="color: red;">*</span></label>
                                            <div class="input-group date" style="width: 150px" id='fromdate'>
                                                <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="InspectionDate" name="InspectionDate" ng-model="CheckList.InspectionDate" required />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmDailyCheckList.$submitted && frmDailyCheckList.SVR_FROM_DATE.$invalid" style="color: red">Please select from date</span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmDailyCheckList.$submitted && frmDailyCheckList.BCL_MC_NAME.$invalid}">
                                            <label class="control-label">Category <span style="color: red;">*</span></label>
                                            <div isteven-multi-select data-input-model="Category" data-output-model="CheckList.Category" data-button-label="icon BCL_MC_NAME" data-item-label="icon BCL_MC_NAME"
                                                data-on-item-click="CategoryChanged()" data-on-select-all="CategoryChangeAll()" data-on-select-none="CategorySelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="All">
                                            </div>
                                            <input type="text" data-ng-model="CheckList.Category" name="BCL_MC_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmDailyCheckList.$submitted && frmDailyCheckList.BCL_MC_NAME.$invalid" style="color: red">Please select Category </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="box-footer text-middle" style="padding-left: 680px; padding-right: 30px; padding-top: 26px">
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <div class="box-footer text-middle">
                                            <button type="submit" id="btnSub" class="btn btn-primary custom-button-color">Search</button>
                                            <input type="button" id="btnNew" ng-click="Clear()" value="Clear" class="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading" style="height: 42px;">
                            <h6 class="panel-title"><b>Checklist
                                <table>
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </b></h6>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="Form2" name="" data-valid-submit="Submit()" novalidate>
                                <div class="row">

                                    <div class="col-md-40 col-sm-10 col-xs-18" style="width: 100%">
                                        <div class="form-group">
                                            <div>
                                                <table style="padding-left: 20px" id="tabledata" class="table ng-table-responsive table-condensed table-bordered table-hover table-striped">
                                                    <thead style="background-color: #366599; color: white">
                                                        <tr>
                                                            <th><b>Main Category</b></th>
                                                            <th><b>Sub Category</b></th>
                                                            <th><b>Working Condition</b></th>
                                                            <th><b>Upload Proofs</b></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr data-ng-repeat="(key, value)  in SubCategories">

                                                            <td><b>{{value.BCL_MC_NAME}}</b></td>
                                                            <td><b>{{value.BCL_SUB_NAME}}</b></td>
                                                            <td>
                                                                <input type="radio" class="custom-control-input"
                                                                    name="{{index}}" value="Ok"
                                                                    data-ng-click="checkchange('Ok',value.BCL_MC_CODE,value.BCL_SUB_CODE)"
                                                                    data-ng-model="ProjectCheklist.CheckedValue[Ok]">
                                                                <label <%--class="radio-inline"--%>>Ok</label>

                                                                <input type="radio" class="custom-control-input"
                                                                    name="{{index}}" value="Not Ok"
                                                                    data-ng-click="checkchange('Not Ok',value.BCL_MC_CODE,value.BCL_SUB_CODE)"
                                                                    data-ng-model="ProjectCheklist.CheckedValue[Ok]">
                                                                <label <%--class="radio-inline"--%>>Not Ok</label>

                                                            </td>
                                                            <td>
                                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                                    <label for="txtcode" class="custom-file">Proofs<a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB"></a></label>
                                                                    <input multiple type="file" name="{{$index}}" data-ng-model="CheckList.UPLFILE[0]" id="UPLFILE" accept=".png,.jpg,.jpeg" class="custom-file-input" onchange="angular.element(this).scope().fileNameChanged(this)">
                                                                    <span class="custom-file-control"></span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-top: 26px">
                                    <input type="button" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="Submit()" />
                                    <input type="submit" value="Save as drapt" class="btn btn-primary custom-button-color" data-ng-click="Save()" data-ng-hide="true" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>

    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../JS/DailyCheckListCreation.js"></script>
    <script src="../../BranchCheckListManagement/JS/CheckListCreation.js"></script>
</body>
</html>
