﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js" defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <script type="text/javascript" defer>

        $(document).ready(function () {
            $("input[type=file]").click(function () {
                $(this).val("");
            });

            $("input[type=file]").change(function () {
                alert($(this).val());
            });
        });

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: 'today',
                maxDate: 'today'
            });
        };

        function OnlyNumeric(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }


    </script>
    <style>
        .col-xs-3 selected {
            border-color: blue;
        }

        .container {
            width: 432% !important;
        }

        .has-error2 {
            border-style: solid;
            border-color: #ff0000;
        }

        .has-error3 {
        }

        .mystyle {
            border-color: red !important;
            border-width: 2px !important;
        }



        .highlight {
            background-color: red;
        }

        input[type='radio'], label {
            margin: 10px;
        }

        .clearBoth {
            clear: both;
        }

        .Fonts {
            font-size: 14px;
        }

        input {
            height: 35px;
            font-size: 15px;
        }




        .table-bordered th {
            color: black;
            background: #99CCFF;
            font-size: 15px;
        }

        .table-bordered td {
            border: 1px solid black;
            float: left;
        }

        /*th {
            color: black;
            background: #99CCFF;
            font-size: 15px;
        }*/

        /*table, th, td {
            border: 1px solid black;
            float: left;
        }*/

        .panel {
            box-shadow: 0 5px 5px 0 rgba(0,0,0,.25);
        }

        .panel-heading {
            border-bottom: 1px solid rgba(0,0,0,.12);
        }

        .list-inline {
            display: block;
        }

            .list-inline li {
                display: inline-block;
            }

                .list-inline li:after {
                    content: '|';
                    margin: 0 10px;
                }
    </style>
</head>

<body data-ng-controller="DailyCheckListCreationController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">

                        <div class="panel-heading" style="height: 42px;">
                            <h6 class="panel-title"><b>Central Team</b></h6>
                        </div>

                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="Form1" name="frmZonalFMCheckList" data-valid-submit="Creation()" novalidate>

                               
                                <div class="row" >

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                         <div class="form-group">
                                                <label for="textfield" class="control-label">Location Name<span style="color: red;">*</span></label>
                                                <input type="text" name="novalidation" id="Location" 
                                                class="form-control" placeholder="location name" 
                                                data-ng-model="frmZonalFMCheckList"data-ng-change="checkchange()">
                                         </div>
                                      </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                         <div class="form-group">
                                                    <label for="textfield" class="control-label">Inspected By<span style="color: red;">*</span></label>
                                                     <input type="text" name="novalidation" id="InsBy" 
                                                     class="form-control" placeholder="inspected by"
                                                     data-ng-model="frmZonalFMCheckList"data-ng-change="checkchange()">
                                         </div>
                                    </div>
                                                              
                                   

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmDailyCheckList.$submitted && frmDailyCheckList.SVR_FROM_DATE.$invalid}">
                                            <label class="control-label">Inspection Date <span style="color: red;">*</span></label>
                                            <div class="input-group date" style="width: 150px" id='fromdate'>
                                                <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="InspectionDate" name="InspectionDate" ng-model="CheckList.InspectionDate" required />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                         <span class="error" data-ng-show="frmDailyCheckList.$submitted && frmDailyCheckList.SVR_FROM_DATE.$invalid" style="color: red">Please select from date</span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                         <div class="form-group">
                                                    <label for="textfield" class="control-label">Reviewing Authority<span style="color: red;">*</span></label>
                                                     <input type="text" name="novalidation" id="ReviewAuthority" 
                                                     class="form-control" placeholder="review authority"
                                                      data-ng-model="frmZonalFMCheckList"data-ng-change="checkchange()">
                                         </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                         <div class="form-group">
                                                   <label for="textfield" class="control-label">Reviewing Date<span style="color: red;">*</span></label>
                                                   <input type="text" name="novalidation" id="ReviewDate" 
                                                   class="form-control" placeholder="review date"
                                                  data-ng-model="frmZonalFMCheckList"data-ng-change="checkchange()">
                                         </div>
                                    </div>
                                     <div class="col-md-3 col-sm-6 col-xs-12">
                                         <div class="form-group">
                                                    <label for="textfield" class="control-label">Central Team<span style="color: red;">*</span></label>
                                                    <input type="text" name="novalidation" id="Team" 
                                                    class="form-control" placeholder="central team"
                                                    data-ng-model="frmZonalFMCheckList"data-ng-change="checkchange()">
                                         </div>
                                      </div>
                                    
                             </div>
                             <div class="row">
                      
                                <div class="box-footer text-left" style="padding-left: 20px; padding-right: 100px; padding-top: 26px">
                                    <input type="submit" value="Home" class="btn btn-primary custom-button-color" data-ng-click="Home()" />
                                    <input type="button" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="Submit()" />
                                </div>

                             </div>
                           </form>
                        </div>
                   </div>

                      <div class="panel">

                        <div class="panel-heading" style="height: 42px;">
                            <h6 class="panel-title" ><b>Checklist Table</b></h6>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                         <form id="Form2" name="" data-valid-submit="Submit()" novalidate>

                                <div class="row">

                                    <div class="col-md-40 col-sm-10 col-xs-18" style="width:100%">
                                        <div class="form-group">
                                            <div>
                                                 <table  style="padding-left: 20px"   id="tabledata" class="table ng-table-responsive table-condensed table-bordered table-hover table-striped">
                                                     <tr style="background-color: #366599; color: white">
                                                        <thead>
                                                        <tr style="padding-left: 20px" role="row">
                                                            <th style="padding-left: 20px"><b>Sub Category</b></th>
                                                            <th style="padding-left: 20px"><b>Working Category</b></th>
                                                            <th style="padding-left: 20px"><b>Inspection Comments</b></th>
                                                            <th style="padding-left: 20px"><b>View More Details</b></th>
                                                            <th style="padding-left: 20px"><b>Zonal FM Comments</b></th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                                       <tr style="padding-left: 40px" role="row" data-ng-repeat="(key, value)  in subcategories">

                                                        <td style="padding-left: 20px">{{value.bcl_mc_name}}</td>
                                                        <td style="padding-left: 20px">{{value.bcl_sub_name}}</td>
                                                        <td>

                                                            <input type="radio" name="{{$index}}" value="{{feedback}}" /><label>ok</label>
                                                            <input type="radio" name="{{$index}}" value="{{feedback}}" /><label>not ok</label>
                                                            <input multiple type="file" name="{{$index}}" data-ng-model="checklist.uplfile[0]" id="uplfile" accept=".png,.jpg,.jpeg" class="custom-file-input" onchange="angular.element(this).scope().filenamechanged(this)">

                                                        </td>
                                                    </tr>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                       

                                
                       </form>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <s src="../../Scripts/moment.min.js" defer></s cript>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>

    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../JS/DailyCheckListCreation.js"></script>
    <script src="../../BranchCheckListManagement/JS/CheckListCreation.js"></script>
</body>
</html>

