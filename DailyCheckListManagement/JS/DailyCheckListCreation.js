﻿
app.service("DailyCheckListCreationService", function ($http, $q, UtilityService, CheckListCreationService) {

 
    this.getMainCategories = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/DailyCheckListCreation/getMainCategories')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getSubCategoryData = function (data) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/DailyCheckListCreation/getSubCategoryData',data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.InsertDailyCheckList = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/DailyCheckListCreation/InsertDailyCheckList', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


});
app.controller('DailyCheckListCreationController', function ($scope, $q, $location, DailyCheckListCreationService, CheckListCreationService, UtilityService, $filter) {
    $scope.CheckList = {};
    $scope.Location = [];
    $scope.Inspection = [];
    $scope.CheckList.Location = [];
    $scope.CheckList.Inspection = [];
    $scope.CheckList.InspectionDate = [];
    $scope.CheckList.Category = [];
    $scope.CheckList.OVERALL_CMTS = [];
    $scope.InspectionDate = [];
    $scope.DraftData = [];
    $scope.Category = [];
    $scope.SubCategories = [];
    $scope.TotalData = [];
    rowData = [];
    MultiUploadData = [];

    //Bind Countires name By using UtilityService
    UtilityService.getCountires(2).then(function (response) {
        if (response.data != null) {
            $scope.Country = response.data;

            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.City = response.data;

                }
            });
        }
    });

    //Bind Location name By using CheckListCreationService
    CheckListCreationService.getLocations().then(function (response) {
        if (response.data != null) {
            $scope.Location = response.data;
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });

    //Bind inspected By By using CheckListCreationService
    CheckListCreationService.getInspectors().then(function (response) {
        if (response.data != null) {
            $scope.Inspection = response.data;
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });

    //Bind MainCategories in category darop down By using CheckListCreationService
    DailyCheckListCreationService.getMainCategories().then(function (response) {
        if (response.data != null) {
            $scope.Category = response.data;
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });


    //Clear  the selected Values
    $scope.Clear = function () {


        angular.forEach($scope.Country, function (Country) {
            Country.ticked = false;
        });
        angular.forEach($scope.City, function (City) {
            City.ticked = false;
        });
        angular.forEach($scope.Location, function (Location) {
            Location.ticked = false;
        });
        angular.forEach($scope.Inspection, function (Inspection) {
            Inspection.ticked = false;
        });

        angular.forEach($scope.Category, function (Category) {
            Category.ticked = false;
        });

        angular.forEach($scope.SelectedData, function (SelectedData) {
            SelectedData.ticked = false;
        });
        $scope.SelectedData = [];
        MultiUploadData = [];
        $('#InspectionDate').val('').datepicker('update');

    };
    //selected data get in single param
    $scope.Creation = function () {
                
        var category = _.filter($scope.CheckList.Category, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_MC_CODE; }).join(',');
        var params = { BCL_MC_CODE: category };

        DailyCheckListCreationService.getSubCategoryData(params).then(function (response) {
           
            if (response.data != null) {
                console.log(response.data);
                $scope.SubCategories = response.data;
               
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);

            progress(0, '', false);
        }, function (error) {
            console.log(error);
        });
    };
    //Selected form data
    $scope.FinalData = [];
    $scope.checkchange = function (cat, subcat, selectedVal, FilePath) {
        $scope.selectedData;
        $scope.selectedData = { "CatCode": subcat, "SubcatCode": selectedVal, "ScoreName": cat, "FilePath": FilePath};
        console.log($scope.selectedData);
        if ($scope.FinalData.length == 0) {
            $scope.FinalData.push($scope.selectedData);
        }
        else {

            var i = _.find($scope.FinalData, function (o) { return o.Category == cat && o.Sub_Category == subcat; });            
            if (i != undefined) {                
                _.remove($scope.FinalData, function (o) { return o.Category == cat && o.Sub_Category == subcat });
                $scope.FinalData.push($scope.selectedData);
                
            } else {
                $scope.FinalData.push($scope.selectedData);
               
            }
            showNotification('error', 8, 'bottom-right', "Please Select  All The Fields ");     
            $scope.clear();
        }
             
    };
    //data saved in to database table
    $scope.Submit = function ()
    {
        console.log($scope.FinalData);
        $scope.data =
            {
            LCMLST: $scope.CheckList.Location[0].LCM_CODE, InspectdBy: $scope.CheckList.Inspection[0].INSPECTOR,
            date: $scope.CheckList.InspectionDate, /*Category: $scope.CheckList.Category[0].BCL_MC_CODE,*/ Seldata: $scope.FinalData
            };

        console.log($scope.data);
   
        DailyCheckListCreationService.InsertDailyCheckList($scope.data).then(function (response)
        {
        console.log(response);
        if (response != null) {
            $.ajax({
                type: "POST",
                url: 'https://live.quickfms.com/api/DailyCheckListCreation/UploadFiles',    // CALL WEB API TO SAVE THE FILES.                        
                contentType: false,
                processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                cache: false,
                data: data, 		        // DATA OR FILES IN THIS CONTEXT.
                success: function (data, textStatus, xhr) {
                    showNotification('success', 8, 'bottom-right', response.Message);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus + ': ' + errorThrown);
                }
            });
        }
      
    }, function (error) {
        console.log(error);
            });

        showNotification('success', 8, 'bottom-right', "Success");     
        $scope.clear();
    };
});

   