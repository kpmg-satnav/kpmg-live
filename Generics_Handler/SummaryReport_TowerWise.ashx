<%@ WebHandler Language="VB" Class="SummaryReport_TowerWise" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports clsJson
Imports clsSubSonicCommonFunctions

Public Class SummaryReport_TowerWise : Implements IHttpHandler, IRequiresSessionState 
    
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response

        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim strCity As String = request("Mcity")
        Dim strLCM_CODE As String = request("MLCM_CODE")
        Dim strTWR_CODE As String = request("MTWR_CODE")
        Dim totalRecords As Integer
        
        Dim clsJson As Collection(Of clsJson) = GetCitySummary(strCity, strLCM_CODE, strTWR_CODE, numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords)
        Dim output As String = BuildJQGridResults(clsJson, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        response.Write(output)
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsJson As Collection(Of clsJson), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String

        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each City1 As clsJson In clsJson
            Dim row As New JQGridRow()
            'row.id = clsJson1.strProject
            row.cell = New String(5) {}
            row.cell(0) = City1.strCTY_NAME.ToString()
            row.cell(1) = City1.strlcm_name.ToString()
            row.cell(2) = City1.strTwr_Name.ToString()
            row.cell(3) = City1.strTotalseats.ToString()
            row.cell(4) = City1.strAlloted.ToString()
            row.cell(5) = City1.strAvailable.ToString()
            
            rows.Add(row)
        Next
        
        
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords / numberOfRows
        result.records = totalRecords
               
        Return New JavaScriptSerializer().Serialize(result)
    End Function
    
    
    Private Function GetCitySummary(ByVal strCity As String, ByVal strLCM_CODE As String, ByVal strTWR_CODE As String, ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer) As Collection(Of clsJson)
        Dim users As New Collection(Of clsJson)()
        If strCity = "--All--" Then
            strCity = ""
        End If
        If strLCM_CODE = "--All--" Then
            strLCM_CODE = ""
        End If
        If strTWR_CODE = "--All--" Then
            strTWR_CODE = ""
        End If
        
        
        
        Dim param(7) As SqlParameter
        param(0) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
        param(0).Value = strCity
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = strLCM_CODE
        param(2) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = strTWR_CODE
        param(3) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(3).Value = pageIndex
        param(4) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(4).Value = sortColumnName
        param(5) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(5).Value = sortOrderBy
        param(6) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(6).Value = Convert.ToInt32(numberOfRows)
        param(7) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(7).Value = 0
        param(7).Direction = ParameterDirection.Output
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("GET_CITYWISE_RPT", param)
        
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim user As clsJson
            user = New clsJson()
            user.strCTY_NAME = Convert.ToString(ds.Tables(0).Rows(i).Item("CTY_NAME"))
            user.strlcm_name = Convert.ToString(ds.Tables(0).Rows(i).Item("LCM_NAME"))
            user.strTwr_Name = Convert.ToString(ds.Tables(0).Rows(i).Item("TWR_NAME"))
            user.strTotalseats = Convert.ToString(ds.Tables(0).Rows(i).Item("TOTAL_SEATS"))
            user.strAlloted = Convert.ToString(ds.Tables(0).Rows(i).Item("ALLOTED_SEATS"))
            user.strAvailable = Convert.ToString(ds.Tables(0).Rows(i).Item("AVLBL_FOR_ALLOCATION"))
            users.Add(user)
            totalRecords = CInt(ds.Tables(1).Rows(0).Item("TotalRecords"))
        Next
         
        Return users
    End Function

End Class