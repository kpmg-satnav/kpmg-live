<%@ WebHandler Language="VB" Class="LocationSummary" %>


Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports clsJson
Imports clsSubSonicCommonFunctions

Public Class LocationSummary : Implements IHttpHandler, IRequiresSessionState 
    
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response

        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim strvertical As String = request("MVertical")
       
        Dim totalRecords As Integer
        
        Dim clsJson As Collection(Of clsJson) = GetVerticalSummary(strvertical, numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords)
        Dim output As String = BuildJQGridResults(clsJson, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        response.Write(output)
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    Private Function BuildJQGridResults(ByVal clsJson As Collection(Of clsJson), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String

        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Vertical1 As clsJson In clsJson
            Dim row As New JQGridRow()
            'row.id = clsJson1.strProject
            row.cell = New String(5) {}
            row.cell(0) = Vertical1.strVert.ToString()
            row.cell(1) = Vertical1.strVertAlloc.ToString()
            
            rows.Add(row)
        Next
        
        
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords / numberOfRows
        result.records = totalRecords
               
        Return New JavaScriptSerializer().Serialize(result)
    End Function
    
    Private Function GetVerticalSummary(ByVal strvertical As String, ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer) As Collection(Of clsJson)
        Dim users As New Collection(Of clsJson)()
       
        
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@CC_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = strvertical
     
        param(1) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(1).Value = pageIndex
        param(2) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(2).Value = sortColumnName
        param(3) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(3).Value = sortOrderBy
        param(4) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(4).Value = Convert.ToInt32(numberOfRows)
        param(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(5).Value = 10
        param(5).Direction = ParameterDirection.Output
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("GET_COSTCENTER_RPT", param)
        
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim user As clsJson
            user = New clsJson()
            user.strVert = Convert.ToString(ds.Tables(0).Rows(i).Item("COST_CENTER_NAME"))
            user.strVertAlloc = Convert.ToString(ds.Tables(0).Rows(i).Item("ALLOCATED_SEATS"))
            
            users.Add(user)
            totalRecords = CInt(ds.Tables(1).Rows(0).Item("TotalRecords"))
        Next
         
        Return users
    End Function
End Class