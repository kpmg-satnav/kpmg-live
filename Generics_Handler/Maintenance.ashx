<%@ WebHandler Language="VB" Class="Maintenance" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports MaintenanceRpt
Imports clsSubSonicCommonFunctions
iMPORTS System.IO 

Public Class Maintenance : Implements IHttpHandler, IRequiresSessionState 
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        
        Dim strFromDate As String  
        Dim strToDate as String
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response

        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim strDep As String = request("Mdep")
        if strDep="Nothing" or strDep="undefined"
        strdep=""
        End if
        Dim strUser As String = request("Muser")
        If strUser = "undefined" or strUser = "Nothing" Then
            strUser = ""
        End If
        
        If request("Mfromdate") = " " Or request("Mfromdate") Is Nothing Or request("Mfromdate") = "Nothing" Then
            strFromDate = ""
        Else
            strFromDate = request("Mfromdate")
        End If
        Dim strtype As String = request("Mtype")
        If strtype = "--Select--" Then
            strtype = ""
        Else
            strtype = request("Mtype")
        End If
        If request("Mtodate") = " " Or request("Mtodate") Is Nothing Or request("Mtodate") = "Nothing" Then
            strToDate = ""
        Else
            strToDate = request("Mtodate")
        End If
        
        
        
        
        Dim strstatus As String = request("Mstatus")
        if strstatus = "Nothing"
        strstatus=""
        End if
        Dim totalRecords As Integer
        
       
         
        Dim MaintenanceRpt As Collection(Of MaintenanceRpt) = Maintenance(strDep, strUser, strFromDate, strToDate, strstatus, strtype, numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords)
        Dim output As String = BuildJQGridResults(MaintenanceRpt, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        response.Write(output)
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    Private Function BuildJQGridResults(ByVal MaintenanceRpt As Collection(Of MaintenanceRpt), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String

        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Maintenance1 As MaintenanceRpt In MaintenanceRpt
            Dim row As New JQGridRow()
            'row.id = ConsolidateRpt1.strProject
            row.cell = New String(11) {}
            row.cell(0) = Maintenance1.strrequisitionid.ToString()
            row.cell(1) = Maintenance1.strreqdate.ToString()
            row.cell(2) = Maintenance1.strspcid.ToString()
            row.cell(3) = Maintenance1.strreqby.ToString()
            row.cell(4) = Maintenance1.strreqtype.ToString()
            row.cell(5) = Maintenance1.strreq.ToString()
            row.cell(6) = Maintenance1.STRDESC.ToString()
            row.cell(7) = Maintenance1.strassignedto.ToString()
            row.cell(8) = Maintenance1.strclosed.ToString()
            row.cell(9) = Maintenance1.strdelayedby.ToString()
            row.cell(10) = Maintenance1.strtotal.ToString()
            row.cell(11) = Maintenance1.strstatus.ToString()
            
            
            rows.Add(row)
        Next
        
        
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords / numberOfRows
        result.records = totalRecords
               
        Return New JavaScriptSerializer().Serialize(result)
    End Function
    
    Private Function Maintenance(ByVal strDep As String, ByVal strUser As String, ByVal strFromDate As String, ByVal strToDate As String, ByVal strstatus As String, ByVal strtype As String, ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer) As Collection(Of MaintenanceRpt)
        Dim users As New Collection(Of MaintenanceRpt)()
        Dim param(10) As SqlParameter
        param(0) = New SqlParameter("@DEP", SqlDbType.NVarChar, 200)
        param(0).Value = strDep
        param(1) = New SqlParameter("@ASSIGNED", SqlDbType.NVarChar, 200)
        param(1).Value = strUser
        param(2) = New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
        param(2).Value = strFromDate
        param(3) = New SqlParameter("@TO_DATE", SqlDbType.DateTime)
        param(3).Value = strToDate
        param(4) = New SqlParameter("@SER_STATUS", SqlDbType.NVarChar, 200)
        param(4).Value = strstatus
        param(5) = New SqlParameter("@SER_TYPE", SqlDbType.NVarChar, 200)
        param(5).Value = strtype
        param(6) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(6).Value = pageIndex
        param(7) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(7).Value = sortColumnName
        param(8) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(8).Value = sortOrderBy
        param(9) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(9).Value = Convert.ToInt32(numberOfRows)
        param(10) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(10).Value = 0
        param(10).Direction = ParameterDirection.Output
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("GET_MAINTENANCE_REPORT1", param)
        
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim user As MaintenanceRpt
            user = New MaintenanceRpt()
            user.strrequisitionid = "<a href=../../EFM/EFM_Webfiles/ViewuserDetails.aspx?id=" + Convert.ToString(ds.Tables(0).Rows(i).Item("REQUESITION_ID")) + ">" + Convert.ToString(ds.Tables(0).Rows(i).Item("REQUESITION_ID")) + "</a>"
            user.strreqdate = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("REQUESTED_DATE"))
            user.strreqby = Convert.ToString(ds.Tables(0).Rows(i).Item("REQUESTED_BY"))
            user.strspcid = Convert.ToString(ds.Tables(0).Rows(i).Item("SPACE_ID"))
            user.strreqtype = Convert.ToString(ds.Tables(0).Rows(i).Item("REQUEST_TYPE"))
            user.strreq = Convert.ToString(ds.Tables(0).Rows(i).Item("REQUEST"))
            user.STRDESC = Convert.ToString(ds.Tables(0).Rows(i).Item("DESCRIPTION"))
            user.strassignedto = Convert.ToString(ds.Tables(0).Rows(i).Item("ASSIGNED_TO"))
            user.strclosed = Convert.ToString(ds.Tables(0).Rows(i).Item("CLOSED_TIME"))
            user.strdelayedby = Convert.ToString(ds.Tables(0).Rows(i).Item("TOTAL_TIME"))
            user.strtotal = Convert.ToString(ds.Tables(0).Rows(i).Item("DELAYED_BY"))
            user.strstatus = Convert.ToString(ds.Tables(0).Rows(i).Item("SER_STATUS"))
            users.Add(user)
            totalRecords = CInt(ds.Tables(1).Rows(0).Item("TotalRecords"))
        Next
         
        Return users
    End Function

End Class