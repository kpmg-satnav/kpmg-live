<%@ WebHandler Language="VB" Class="Solution" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports MaintenanceRpt
Imports clsSubSonicCommonFunctions
Imports System.IO

Public Class Solution : Implements IHttpHandler, IRequiresSessionState 
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response

        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim strid As String = request("MUser")
       
        Dim totalRecords As Integer
        Dim MaintenanceRpt As Collection(Of MaintenanceRpt) = Solution(strid, numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords)
        Dim output As String = BuildJQGridResults(MaintenanceRpt, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        response.Write(output)
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    Private Function BuildJQGridResults(ByVal MaintenanceRpt As Collection(Of MaintenanceRpt), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String

        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Activities As MaintenanceRpt In MaintenanceRpt
            Dim row As New JQGridRow()
            row.cell = New String(4) {}
            row.cell(0) = Activities.strcomments.ToString()
            row.cell(1) = Activities.strupdatedby.ToString()
            row.cell(2) = Activities.strstatus.ToString()
            row.cell(3) = Activities.stralert.ToString()
            rows.Add(row)
        Next
        
        
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords / numberOfRows
        result.records = totalRecords
               
        Return New JavaScriptSerializer().Serialize(result)
    End Function
    
    
    Private Function Solution(ByVal strid As String, ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer) As Collection(Of MaintenanceRpt)
        Dim users As New Collection(Of MaintenanceRpt)()
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strid
        
        param(1) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(1).Value = pageIndex
        param(2) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(2).Value = sortColumnName
        param(3) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(3).Value = sortOrderBy
        param(4) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(4).Value = Convert.ToInt32(numberOfRows)
        param(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(5).Value = 0
        param(5).Direction = ParameterDirection.Output
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("GET_MAINTENANCE_SOLUTION", param)
        
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim user As MaintenanceRpt
            user = New MaintenanceRpt()
            user.strcomments = Convert.ToString(ds.Tables(0).Rows(i).Item("COMMENTS"))
            user.strupdatedby = Convert.ToString(ds.Tables(0).Rows(i).Item("UPDATED_BY"))
            user.strstatus = Convert.ToString(ds.Tables(0).Rows(i).Item("REQUEST_STATUS"))
            If user.strstatus = "Pending" Then
                user.stralert = "<Img class='grdImg' 'Src=../../images/icon_alert.gif' title='Pending' />"
            ElseIf user.strstatus = "InProgress" Then
                user.stralert = "<Img class='grdImg' Src='../../images/alert_icon.gif' title='InProgress' />"
            ElseIf user.strstatus = "Closed" Then
                user.stralert = "<Img class='grdImg' Src='../../images/error_small.png' title='Closed' />"
            End If
            
            users.Add(user)
            totalRecords = CInt(ds.Tables(1).Rows(0).Item("TotalRecords"))
        Next
         
        Return users
    End Function

End Class