﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BusinessCardViewModify.aspx.cs" Inherits="BusinessCard_BusinessCardViewModify" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">View or Modify RequesView or Modify Request</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <div class="cleardfix">
                            <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical">
                                <asp:GridView ID="gvcards" runat="server" AllowPaging="True" AllowSorting="False"
                                    RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"
                                    PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Business Card Request Found" CssClass="table table-condensed table-bordered table-hover table-striped" OnPageIndexChanging="gvcards_PageIndexChanging">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <a href="#" onclick="showPopWin('<%# Eval("BC_REQ_ID") %>')">
                                                    <asp:Label ID="lblReq" runat="server" Text='<%# Eval("BC_REQ_ID") %>'></asp:Label>
                                                </a>
                                                <%--<asp:LinkButton ID="hLinkDetails" runat="server" CommandName="DataEditing" Text='<%# Eval("BC_REQ_ID")%> ' CommandArgument='<% #Bind("BC_REQ_ID")%>'></asp:LinkButton>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Requested Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDate" runat="server" CssClass="lblASTCode" Text='<%#Eval("BC_CREATED_DT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Requested By">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReq" runat="server" CssClass="lblStatus" Text='<%#Bind("AUR_KNOWN_AS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLoc" runat="server" CssClass="lblStatus" Text='<%#Bind("LCM_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Department Name/LOB">
                                            <ItemTemplate>
                                                <asp:Label ID="lolLOB" runat="server" CssClass="lblStatus" Text='<%#Bind("DEP_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Designation">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDes" runat="server" CssClass="lblStatus" Text='<%#Bind("DSN_AMT_TITLE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddr" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_ADDR")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email Id">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmail" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_EMAIL")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mobile">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMob" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_PHONE_NO")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No.of Cards">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcards" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_NO_OF_CARDS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSts" runat="server" CssClass="lblStatus" Text='<%#Bind("BC_STA_DESC")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex='-1' data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;    
                     <h4 class="modal-title">Modify Business Card Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <iframe id="modalcontentframe" src="#" width="100%" height="450px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        function showPopWin(id) {
            $("#modalcontentframe").attr("src", "BusinessCardRequest.aspx?BID=" + id);
            $("#myModal").modal().fadeIn();
        }
    </script>
</body>
</html>
