﻿app.service("BusinessCardVendorWise", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    this.GetGriddata = function (param) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BusinessCardVendorWise/GetVendorWiseDetails', param)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('BusinessCardVendorWiseController', ['$scope', '$q', '$http', 'UtilityService', '$timeout','BusinessCardVendorWise', function ($scope, $q, $http, UtilityService, $timeout, BusinessCardVendorWise) {
    $scope.VendorReq = {};
    $scope.Type = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    $scope.GridVisiblity = true;
    $scope.EnableStatus = 0;
    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.VendorReq.CNP_NAME.push(a);
                });

                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });
    }, 500);

    $scope.columnDefs = [
        { headerName: "Vendor", field: "VENDOR", cellClass: 'grid-align', width: 220 },
        { headerName: "No.of Requests", field: "REQ", cellClass: 'grid-align', width: 170 },
        { headerName: "Department/LOB", field: "DEPARTMENT", suppressMenu: true, cellClass: 'grid-align', width: 190 },
        { headerName: "Location Code", field: "LCM_CODE", cellClass: 'grid-align', width: 150 },
        { headerName: "Location Name", field: "LOCATION", cellClass: 'grid-align', width: 150 },
        { headerName: "Company", field: "CNP_NAME", cellClass: 'grid-align', width: 150 },
        { headerName: "Address", field: "ADDRESS", cellClass: 'grid-align', width: 260 }],

        $scope.LoadData = function () {
            var searchval = $("#filtertxt").val();
            $("#btLast").hide();
            $("#btFirst").hide();

            var dataSource = {
                rowCount: null,
                getRows: function (params) {
                    var params = {
                        SearchValue: searchval,
                        PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                        PageSize: 10,
                        FromDate: $scope.VendorReq.FromDate,
                        ToDate: $scope.VendorReq.ToDate,
                        CompanyId: $scope.VendorReq.CNP_NAME[0].CNP_ID
                    };
                    var fromdate = moment($scope.VendorReq.FromDate);
                    var todate = moment($scope.VendorReq.ToDate);
                    if (fromdate > todate) {
                        $scope.GridVisiblity = false;
                        showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
                    }
                    else {
                        progress(0, 'Loading...', true);
                        $scope.GridVisiblity = true;
                        $scope.gridata = [];
                        BusinessCardVendorWise.GetGriddata(params).then(function (response) {
                            if (response.data != null) {
                                $scope.gridata = response.data.VMlist;
                                if ($scope.gridata.length != 0) {
                                    $scope.gridOptions.api.setRowData([]);
                                    $scope.gridOptions.api.setRowData($scope.gridata);
                                }
                            }
                            else {
                                $scope.gridOptions.api.setRowData([]);
                                $("#btNext").attr("disabled", true);
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', 'No Records Found');
                            }
                            progress(0, '', false);
                        }, function (error) {
                            console.log(error);
                        });
                    }
                }
            }
            $scope.gridOptions.api.setDatasource(dataSource);
        };

    $scope.ColumnNames = [];
    $scope.VendorWiseReqLoad = function () {
        progress(0, 'Loading...', true);
        $scope.Pageload = {
            FromDate: $scope.VendorReq.FromDate,
            ToDate: $scope.VendorReq.ToDate,
            CompanyId: $scope.VendorReq.CNP_NAME[0].CNP_ID
        };
        BusinessCardVendorWise.GetGriddata($scope.Pageload).then(function (data) {

            if (data.data == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
            }
            else {
                $scope.gridata = data.data.VMlist;
                progress(0, 'Loading...', true);
                $scope.gridOptions.api.setRowData([]);
                $scope.gridOptions.api.setRowData($scope.gridata);
                progress(0, '', false);
            }
        }, function (error) {
            console.log(error);
        });
    }
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
    };


    $scope.GenerateFilterPdf = function () {
        var columns = [{ title: "", key: "COL3" }, { title: "", key: "REQCOUNT" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("VendorWiseRequest.pdf");
    }

    $scope.GenerateFilterExcel = function () {
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "VendorWiseRequest.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
    }
    $scope.GenReport = function (VendorReq, Type) {
        var searchval = $("#filtertxt").val();
        VendorReq.Type = Type;
        woobj = {};
        woobj.CompanyId = VendorReq.CNP_NAME[0].CNP_ID;
        woobj.Type = Type;
        woobj.SearchValue = searchval;
        woobj.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        woobj.PageSize = $scope.gridata[0].OVERALL_COUNT;
        woobj.FromDate = VendorReq.FromDate;
        woobj.ToDate = VendorReq.ToDate;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (VendorReq.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/BusinessCardVendorWise/GetVendorWiseReport',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'VendorWiseRequest.' + Type;
                document.body.appendChild(a);
                a.click();
            }).error(function (data, status, headers, config) {
            });
        };
    }
    $scope.selVal = "30";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.VendorReq.FromDate = moment().format('MM/DD/YYYY');
                $scope.VendorReq.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.VendorReq.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.VendorReq.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.VendorReq.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.VendorReq.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.VendorReq.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.VendorReq.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.VendorReq.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.VendorReq.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.VendorReq.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.VendorReq.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }
    setTimeout(function () {
        $scope.LoadData();
    }, 1500);

}]);
