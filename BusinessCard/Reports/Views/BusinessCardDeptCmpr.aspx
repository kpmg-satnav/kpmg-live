﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="BusinessCardDeptCmprController" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="User Wise Request" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">Department Wise Request Comparision Report</h3>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <form id="form1" name="frmDept" data-valid-submit="LoadData()">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Year</label>
                                        <select id="Years" class="form-control selectpicker select1">
                                            <option value="">Select Years</option>
                                        </select>
                                        <span class="error" data-ng-show="frmDept.$submitted && frmDept.Year.$invalid" style="color: red">Please Select Year </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                    <div class="form-group">
                                        <label class="control-label">Company</label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="DeptWise.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                            item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="DeptWise.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmDept.$submitted && frmDept.Company.$invalid" style="color: red">Please Select Company </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="clearfix">
                                        <div class="box-footer text-right" style="padding-top: 18px">
                                            <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="LoadData()" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" id="table2">
                                    <br />
                                    <a data-ng-click="GenReport(DeptWise,'doc')"><i id="word" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(DeptWise,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(DeptWise,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                            </div>
                            <%--</form>
                            <form id="form2" data-ng-show="GridVisiblity">--%>
                            <div class="row" style="padding-left: 17px;">
                                <div class="col-md-12" style="height: 320px">
                                    <%--<input id="filtertxt" placeholder="Filter..." type="text" class="form-control" style="width: 25%;" />--%>
                                    <div class="input-group" style="width: 20%">
                                        <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary custom-button-color" type="submit">
                                                <i class="glyphicon glyphicon-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 290px; width: auto"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../Js/BusinessCardDeptCmpr.js" defer></script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script type="text/javascript" defer>
        var CompanySession = '<%= Session["COMPANYID"]%>';
        var selectBox = document.getElementById('Years');
        for (var i = 2020; i >= 2017; i--) {
            var option = document.createElement('option');
            option.value = i;
            option.innerHTML = i;
            selectBox.appendChild(option);
        }
        var d = new Date(),
        year = d.getFullYear();
        $('#Years option[value=' + year + ']').attr('selected', true);
    </script>
</body>
</html>
