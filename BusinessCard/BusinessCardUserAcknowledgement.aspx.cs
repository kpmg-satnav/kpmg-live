﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BusinessCard_BusinessCardUserAcknowledgement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GetBusinessCardsDetails();
        Details.Visible = false;
        ddlDepartment.Attributes.Add("disabled", "disabled");
        ddlEmployee.Attributes.Add("disabled", "disabled");
        ddlLocation.Attributes.Add("disabled", "disabled");
        ddlDesignation.Attributes.Add("disabled", "disabled");
        ddlVendor.Attributes.Add("disabled", "disabled");
        ddlStatus.Attributes.Add("disabled", "disabled");
        txtEmailId.Attributes.Add("disabled", "disabled");
        txtFax.Attributes.Add("disabled", "disabled");
        txtPhone.Attributes.Add("disabled", "disabled");
        txtMobile.Attributes.Add("disabled", "disabled");
        txtAddress.Attributes.Add("disabled", "disabled");
        txtRemarks.Attributes.Add("disabled", "disabled");
        txtAppr.Attributes.Add("disabled", "disabled");
        rbl.Enabled = false;
    }

    public void GetVendors()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "USP_GETALLVENDORS");
        ddlVendor.DataSource = sp.GetDataSet();
        ddlVendor.DataTextField = "AVR_NAME";
        ddlVendor.DataValueField = "AVR_CODE";
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, "--Select Vendors--");
    }
    public void GetBusinessStatus()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BUSINESS_STATUS");
        ddlStatus.DataSource = sp.GetDataSet();
        ddlStatus.DataTextField = "BC_STA_DESC";
        ddlStatus.DataValueField = "BC_STA_ID";
        ddlStatus.DataBind();
        ddlStatus.Items.Insert(0, "--Select Status--");
    }
    public void ClearFields()
    {
        ddlEmployee.Text = "";
        ddlDepartment.Text = "";
        ddlLocation.Text = "";
        ddlDesignation.Text = "";
        txtEmailId.Text = "";
        txtFax.Text = "";
        txtPhone.Text = "";
        txtMobile.Text = "";
        txtAddress.Text = "";
    }
    public void GetBusinessCardsDetails()
    {
        DataSet ds;
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BC_USER_ACKNOWLEDGEMENT");
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();
        gvcards.DataSource = sp.GetDataSet();
        gvcards.DataBind();
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        //LoadEmployeesByDepartment(ddlDepartment.SelectedItem.Value);
    }
    protected void gvcards_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "UserAck")
        {
            Details.Visible = true;
            int currentpageindex = 0;
            if (Session["CurrentPageIndex"] != null)
            {
                currentpageindex = (Convert.ToInt32(Session["CurrentPageIndex"]));
            }
            int rowIndex = Convert.ToInt32(e.CommandArgument.ToString()) - (currentpageindex * 5);
            LinkButton lblid = (LinkButton)gvcards.Rows[rowIndex].FindControl("lnksurrender");
            string Reqid = lblid.Text;
            Session["Acknowledgement"] = Reqid;
            DataSet ds;
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BC_USER_ACKNOWLWDGE_DETAILS");
            sp.Command.AddParameter("@BID", Reqid, DbType.String);
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
            ds = sp.GetDataSet();
            IDataReader dr = sp.GetReader();
            if (dr.Read())
            {

                if (!string.IsNullOrEmpty((string)dr["AUR_ID"]))
                {
                    string Employees = (string)dr["AUR_ID"];
                    ddlEmployee.Text = Employees;
                }

                if (!string.IsNullOrEmpty((string)dr["DEP_CODE"]))
                {
                    string Department = (string)dr["DEP_CODE"];
                    ddlDepartment.Text = Department;
                }
                if (!string.IsNullOrEmpty((string)dr["LCM_CODE"]))
                {
                    string Location = (string)dr["LCM_CODE"];
                    ddlLocation.Text = Location;
                }
                if (!string.IsNullOrEmpty((string)dr["DSN_CODE"]))
                {
                    string Designation = (string)dr["DSN_CODE"];
                    ddlDesignation.Text = Designation;
                } 
                GetVendors();
                string Vendors = (string)dr["AVR_CODE"];
                if (!string.IsNullOrEmpty((string)dr["AVR_CODE"]))
                {
                    ddlVendor.ClearSelection();
                    ddlVendor.Items.FindByValue((string)dr["AVR_CODE"]).Selected = true;
                    ddlVendor.Enabled = true;
                }
                GetBusinessStatus();
                string Status = (string)dr["BC_APPR_STATUS"].ToString(); ;
                if ((Status) != "0")
                {
                    ddlStatus.ClearSelection();
                    ddlStatus.Items.FindByValue(Status).Selected = true;
                    ddlStatus.Enabled = true;
                }
                if (!string.IsNullOrEmpty((string)dr["BC_EMAIL"]))
                {
                    string Email = (string)dr["BC_EMAIL"];
                    txtEmailId.Text = Email;
                }
                if (!string.IsNullOrEmpty((string)dr["BC_FAX"]))
                {
                    string Fax = (string)dr["BC_FAX"];
                    txtFax.Text = Fax;
                }
                if (!string.IsNullOrEmpty((string)dr["BC_LAND_NO"]))
                {
                    string Phone = (string)dr["BC_LAND_NO"];
                    txtPhone.Text = Phone;
                }
                if (!string.IsNullOrEmpty((string)dr["BC_PHONE_NO"]))
                {
                    string Mobile = (string)dr["BC_PHONE_NO"];
                    txtMobile.Text = Mobile;
                }
                if (!string.IsNullOrEmpty((string)dr["BC_ADDR"]))
                {
                    string Address = (string)dr["BC_ADDR"];
                    txtAddress.Text = Address;
                }
                if (!string.IsNullOrEmpty((string)dr["BC_REM"]))
                {
                    string Remarks = (string)dr["BC_REM"];
                    txtRemarks.Text = Remarks;
                }
                if (!string.IsNullOrEmpty((string)dr["BC_BCD_APP_REMARKS"]))
                {
                    string ApprRemarks = (string)dr["BC_BCD_APP_REMARKS"];
                    txtAppr.Text = ApprRemarks;
                }
                string Cards = dr["BC_NO_OF_CARDS"].ToString();
                rbl.Items.FindByValue(Cards).Selected = true;
            }

        }
    }
    protected void gvcards_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvcards.PageIndex = e.NewPageIndex;
        Session["CurrentPageIndex"] = e.NewPageIndex;
        GetBusinessCardsDetails();
    }
    protected void btnAcknow_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BC_USERACKNOWLEDGE");
            sp.Command.AddParameter("@BREQ", Session["Acknowledgement"], DbType.String);
            sp.Command.AddParameter("@COMPANYID", Session["COMPANYID"], DbType.Int32);
            sp.Command.AddParameter("@EMP_ACK", Session["UID"], DbType.String);
            sp.ExecuteScalar();
            lblMsg.Text = "User Acknowledgement For Business Card Is Successfully Generated..";
        }
        else
        {
            return;
        }
    }
}