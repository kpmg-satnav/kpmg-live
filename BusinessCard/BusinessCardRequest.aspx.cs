﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.UI;
using SubSonic;
using System.Web.UI.WebControls;
using System.IO;
using System.Threading;
using System.Data.OleDb;
using System.Windows.Forms;
public partial class Business_Card_BusinessCardRequest : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e) 
    {
        string RequestId =null;
        string BusinessApproval=null;
        if (!IsPostBack)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "anything", "refreshSelectpicker();", true);
            RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks().VAL_EXPR;
            RegularExpAddress.ValidationExpression = User_Validation.GetValidationExpressionForAddress().VAL_EXPR;
            RegularExpAppRem.ValidationExpression = User_Validation.GetValidationExpressionForRemarks().VAL_EXPR;
            {
                string path = HttpContext.Current.Request.Url.AbsolutePath;
                string host = HttpContext.Current.Request.Url.Host;
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 50);
                param[0].Value = Session["UID"];
                param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
                param[1].Value = path;
                using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
                {
                    if (HttpContext.Current.Session["UID"]==null)
                    {
                        Response.Redirect(Application["FMGLogout"].ToString());
                    }
                    
                    else if (sdr.HasRows)
                    {
                    }
                    else
                        Response.Redirect(Application["FMGLogout"].ToString());
                }
            }

            //LoadEmployees();
            //LoadLocations();
            //LoadDepartment();
            //LoadDesignations();
            btnModify.Visible = false;
            btnApproval.Visible = false;
            btnRejection.Visible = false;
            Vendor.Visible = false;
            btnAcknow.Visible = false;
            StRem.Visible = false;
            RequestId = Request.QueryString["BID"];
            Session["BusinessCard"] = RequestId;
            if (RequestId != null)
            {
                ValidateRequest(RequestId);
                header.Visible = false;
                ddlEmployee.Enabled = false;
                ddlDepartment.Enabled = false;
                btnSubmit.Visible = false;
                btnModify.Visible = true;
                GetBusinessDetails(RequestId);
            }
            BusinessApproval = Request.QueryString["ApprovalBID"];
            Session["BApproval"] = BusinessApproval;
            if (BusinessApproval != null)
            {
                GetVendors();
                GetBusinessStatus();
                header.Visible = false; btnSubmit.Visible = false; btnModify.Visible = false; btnApproval.Visible = true; btnRejection.Visible = true; Vendor.Visible = true; StRem.Visible = true;
                ddlDepartment.Attributes.Add("disabled", "disabled");
                ddlEmployee.Attributes.Add("disabled", "disabled");
                ddlLocation.Attributes.Add("disabled", "disabled");
                ddlDesignation.Attributes.Add("disabled", "disabled");
                txtEmailId.Attributes.Add("disabled", "disabled");
                txtFax.Attributes.Add("disabled", "disabled");
                txtPhone.Attributes.Add("disabled", "disabled");
                txtMobile.Attributes.Add("disabled", "disabled");
                txtAddress.Attributes.Add("disabled", "disabled");
                txtRemarks.Attributes.Add("disabled", "disabled");
                rbl.Enabled = false;
                GetBusinessDetails(BusinessApproval);
            }
            string Acknowledge = Request.QueryString["AckBID"];
            Session["Acknowledgement"] = Acknowledge;
            if (Acknowledge != null)
            {
                GetVendors();
                GetBusinessStatus();
                header.Visible = false; btnSubmit.Visible = false; btnModify.Visible = false; btnApproval.Visible = false; btnRejection.Visible = false; Vendor.Visible = true; StRem.Visible = true; btnAcknow.Visible = true;
                ddlDepartment.Attributes.Add("disabled", "disabled");
                ddlEmployee.Attributes.Add("disabled", "disabled");
                ddlLocation.Attributes.Add("disabled", "disabled");
                ddlDesignation.Attributes.Add("disabled", "disabled");
                ddlVendor.Attributes.Add("disabled", "disabled");
                ddlStatus.Attributes.Add("disabled", "disabled");
                txtEmailId.Attributes.Add("disabled", "disabled");
                txtFax.Attributes.Add("disabled", "disabled");
                txtPhone.Attributes.Add("disabled", "disabled");
                txtMobile.Attributes.Add("disabled", "disabled");
                txtAddress.Attributes.Add("disabled", "disabled");
                txtRemarks.Attributes.Add("disabled", "disabled");
                txtAppr.Attributes.Add("disabled", "disabled");
                rbl.Enabled = false;
                GetBusinessDetails(Acknowledge);
            }
            if (!String.IsNullOrEmpty(HttpContext.Current.Session["UID"].ToString()) && RequestId == null && BusinessApproval == null)
            {
                ddlEmployee.Text = HttpContext.Current.Session["UID"].ToString();
                LoadEmployeesData(ddlEmployee.Text);
            }
        }
        
    }
    public void ValidateRequest(string Rq)
    {
        DataSet ds;
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "VALIDATE_REQUESTS");
        sp.Command.AddParameter("@REQ", Rq, DbType.String);
        ds = sp.GetDataSet();
        //int flag = (int)sp.ExecuteScalar();
        if (ds.Tables[0].Rows.Count == 0)
        {
            btnModify.Visible = true;
        }
        else
        {
            btnModify.Attributes.Add("disabled", "disabled");
        }
    }
   
    public void GetVendors()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "USP_GETALLVENDORS");
        ddlVendor.DataSource = sp.GetDataSet();
        ddlVendor.DataTextField = "AVR_NAME";
        ddlVendor.DataValueField = "AVR_CODE";
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, "--Select Vendors--");
    }
    public void GetBusinessStatus()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BUSINESS_STATUS");
        ddlStatus.DataSource = sp.GetDataSet();
        ddlStatus.DataTextField = "BC_STA_DESC";
        ddlStatus.DataValueField = "BC_STA_ID";
        ddlStatus.DataBind();
        ddlStatus.Items.Insert(0, "--Select Status--");
    }
    public void ClearFields()
    {
        ddlEmployee.Text = "";
        ddlDepartment.Text = "";
        ddlLocation.Text = "";
        ddlDesignation.Text = "";
        txtEmailId.Text = "";
        txtFax.Text = "";
        txtPhone.Text = "";
        txtMobile.Text = "";
        txtAddress.Text = "";
        txtRemarks.Text = "";
    }
    public void GetBusinessDetails(string BusinessRequest)
    {
        ClearFields();
        DataSet ds;
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BC_BUSINESS_DETAILS");
        sp.Command.AddParameter("@BID", BusinessRequest, DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
        ds = sp.GetDataSet();
        IDataReader dr = sp.GetReader();
        if (dr.Read())
        {

          
            if (!string.IsNullOrEmpty((string)dr["AUR_ID"]))
            {
                string Employees = (string)dr["AUR_ID"];
                ddlEmployee.Text = Employees;
            }
            if (!string.IsNullOrEmpty((string)dr["DEP_CODE"]))
            {
                string Department = (string)dr["DEP_CODE"];
                ddlDepartment.Text = Department;
            }


            if (!string.IsNullOrEmpty((string)dr["LCM_CODE"]))
            {
                string Location = (string)dr["LCM_CODE"];
                ddlLocation.Text = Location;
            }

            if (!string.IsNullOrEmpty((string)dr["DSN_CODE"]))
            {
                string Designation = (string)dr["DSN_CODE"];
                ddlDesignation.Text = Designation;
            }

            GetVendors();
            string Vendors = (string)dr["AVR_CODE"];
            if (!string.IsNullOrEmpty((string)dr["AVR_CODE"]))
            {
                ddlVendor.ClearSelection();
                ddlVendor.Items.FindByValue((string)dr["AVR_CODE"]).Selected = true;
                ddlVendor.Enabled = true;
            }
            GetBusinessStatus();
            string Status = (string)dr["BC_APPR_STATUS"].ToString(); ;
            if ((Status) != "0")
            {
                ddlStatus.ClearSelection();
                ddlStatus.Items.FindByValue(Status).Selected = true;
                ddlStatus.Enabled = true;
            }
            if (!string.IsNullOrEmpty((string)dr["BC_EMAIL"]))
            {
                string Email = (string)dr["BC_EMAIL"];
                txtEmailId.Text = Email;
            }
            if (!string.IsNullOrEmpty((string)dr["BC_FAX"]))
            {
                string Fax = (string)dr["BC_FAX"];
                txtFax.Text = Fax;
            }
            if (!string.IsNullOrEmpty((string)dr["BC_LAND_NO"]))
            {
                string Phone = (string)dr["BC_LAND_NO"];
                txtPhone.Text = Phone;
            }
            if (!string.IsNullOrEmpty((string)dr["BC_PHONE_NO"]))
            {
                string Mobile = (string)dr["BC_PHONE_NO"];
                txtMobile.Text = Mobile;
            }
            if (!string.IsNullOrEmpty((string)dr["BC_ADDR"]))
            {
                string Address = (string)dr["BC_ADDR"];
                txtAddress.Text = Address;
            }
            if (!string.IsNullOrEmpty((string)dr["BC_REM"]))
            {
                string Remarks = (string)dr["BC_REM"];
                txtRemarks.Text = Remarks;
            }
            string Cards = dr["BC_NO_OF_CARDS"].ToString();
            rbl.Items.FindByValue(Cards).Selected = true;
        }
    }
    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //if (ddlDepartment.Text.Trim().Length == 0)
        //{
        //    MessageBox.Show("Your Message");
        //    retur
        //}
        if (ddlLocation.Text != "")
        {
            int backSlashIndex = ddlEmployee.Text.IndexOf("/");
            int backSlashIndex2 = ddlLocation.Text.IndexOf("(");
            int backSlashIndex3 = ddlLocation.Text.IndexOf(")");
            backSlashIndex3 = backSlashIndex3 - (backSlashIndex2 + 3);
            backSlashIndex2 = backSlashIndex2 + 2;
            String locid = ddlLocation.Text.Substring(backSlashIndex2, backSlashIndex3);
            String empid = (backSlashIndex >= 0) ? ddlEmployee.Text.Substring(0, backSlashIndex) : ddlEmployee.Text;
            SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "VALIDATE_EMPLOYEE");
            SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "VALIDATE_LOCATION");
            sp1.Command.AddParameter("@EMPID", empid, DbType.String);
            sp2.Command.AddParameter("@LOCID", locid, DbType.String);
            int returnvalue = (int)sp1.ExecuteScalar();
            int returnvalue_Location = (int)sp2.ExecuteScalar();
            if (returnvalue == 1 && returnvalue_Location == 1)
            {
                cusCustom.Visible = false;
                cusCustom1.Visible = false;
                
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BC_INSERT_DETAILS");
                sp.Command.AddParameter("@EMPLOYEE", (backSlashIndex >= 0) ? ddlEmployee.Text.Substring(0, backSlashIndex) : ddlEmployee.Text, DbType.String);
                sp.Command.AddParameter("@DEPARTMENT", ddlDepartment.Text, DbType.String);
                sp.Command.AddParameter("@LOCATION", locid, DbType.String);
                sp.Command.AddParameter("@DESIGNATION", ddlDesignation.Text, DbType.String);
                sp.Command.AddParameter("@EMAIL", txtEmailId.Text, DbType.String);
                sp.Command.AddParameter("@FAX", txtFax.Text, DbType.String);
                sp.Command.AddParameter("@TELEPHONE", txtPhone.Text, DbType.String);
                sp.Command.AddParameter("@MOBILE", Request.Form[txtMobile.UniqueID].ToString(), DbType.String);
                sp.Command.AddParameter("@ADDRESS", txtAddress.Text, DbType.String);
                sp.Command.AddParameter("@CARDS", rbl.SelectedItem.Value, DbType.String);
                sp.Command.AddParameter("@CREATEDBY", HttpContext.Current.Session["UID"], DbType.String);
                sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String);
                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
                sp.ExecuteScalar();
                lblMsg.Text = "Business Card Request Successful";
                ClearFields();
            }
        }
    }
    protected void btnModify_Click(object sender, EventArgs e)
    {
        int backSlashIndex = ddlEmployee.Text.IndexOf("/");
        int backSlashIndex2 = ddlLocation.Text.IndexOf("(");
        int backSlashIndex3 = ddlLocation.Text.IndexOf(")");
        backSlashIndex3 = backSlashIndex3 - (backSlashIndex2 + 3);
        backSlashIndex2 = backSlashIndex2 + 2;
        String locid = ddlLocation.Text.Substring(backSlashIndex2, backSlashIndex3);
        String empid = (backSlashIndex >= 0) ? ddlEmployee.Text.Substring(0, backSlashIndex) : ddlEmployee.Text;

        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BC_MODIFY_BUSINESS_CARD");
        sp.Command.AddParameter("@BID", Session["BusinessCard"], DbType.String);
        sp.Command.AddParameter("@EMPLOYEE", (backSlashIndex >= 0) ? ddlEmployee.Text.Substring(0, backSlashIndex) : ddlEmployee.Text, DbType.String);     
        sp.Command.AddParameter("@DEPARTMENT", ddlDepartment.Text, DbType.String);
        sp.Command.AddParameter("@LOCATION", locid, DbType.String);     
        sp.Command.AddParameter("@DESIGNATION", ddlDesignation.Text, DbType.String);
        sp.Command.AddParameter("@EMAIL", txtEmailId.Text, DbType.String);
        sp.Command.AddParameter("@FAX", txtFax.Text, DbType.String);
        sp.Command.AddParameter("@TELEPHONE", txtPhone.Text, DbType.String);
        sp.Command.AddParameter("@MOBILE", txtMobile.Text, DbType.String);
        sp.Command.AddParameter("@ADDRESS", txtAddress.Text, DbType.String);
        sp.Command.AddParameter("@CARDS", rbl.SelectedItem.Value, DbType.String);
        sp.Command.AddParameter("@UPDATEDBY", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.Int32);
        sp.ExecuteScalar();
        lblMsg.Text = "Business Cards Details Updated Successfully..";
    }
    protected void btnApproval_Click(object sender, EventArgs e)
    {
        btnApproval.Visible = false;
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BC_REQUISITION_APPROVAL");
            sp.Command.AddParameter("@BREQ", Session["BApproval"], DbType.String);
            sp.Command.AddParameter("@BVEN", ddlVendor.SelectedValue, DbType.String);
            sp.Command.AddParameter("@BSTA", ddlStatus.SelectedValue, DbType.String);
            sp.Command.AddParameter("@BREM", txtAppr.Text, DbType.String);
            sp.Command.AddParameter("@BAPP", Session["UID"], DbType.String);
            sp.Command.AddParameter("@COMPANYID", Session["COMPANYID"], DbType.Int32);
            sp.ExecuteScalar();
            btnRejection.Visible = false;
            lblMsg.Text = "Businesss Card Request Has Been Approved Successfully..";
           
       
    }
    protected void btnRejection_Click(object sender, EventArgs e)
    {
        btnRejection.Visible = false;
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BC_REQUISITION_REJECTION");
        sp.Command.AddParameter("@BREQ", Session["BApproval"], DbType.String);
        sp.Command.AddParameter("@BVEN", ddlVendor.SelectedValue, DbType.String);
        sp.Command.AddParameter("@BSTA", ddlStatus.SelectedValue, DbType.String);
        sp.Command.AddParameter("@BREM", txtAppr.Text, DbType.String);
        sp.Command.AddParameter("@BAPP", Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", Session["COMPANYID"], DbType.Int32);
        sp.ExecuteScalar();
        btnApproval.Visible = false;
        lblMsg.Text = "Businesss Card Request Has Been Rejected Successfully..";
    }
    protected void btnAcknow_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            btnAcknow.Visible = false;
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "BC_USERACKNOWLEDGE");
            sp.Command.AddParameter("@BREQ", Session["Acknowledgement"], DbType.String);
            sp.Command.AddParameter("@COMPANYID", Session["COMPANYID"], DbType.Int32);
            sp.Command.AddParameter("@EMP_ACK", Session["UID"], DbType.String);
            sp.ExecuteScalar();
            lblMsg.Text = "User Acknowledgement For Business Card Is Successfully Generated..";
        }
        else
        {
            return;
        }
    }
    public void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        int backSlashIndex = ddlEmployee.Text.IndexOf("/");
        String empid = (backSlashIndex >= 0) ? ddlEmployee.Text.Substring(0, backSlashIndex) : ddlEmployee.Text;       
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "VALIDATE_EMPLOYEE");
        sp.Command.AddParameter("@EMPID", empid, DbType.String);
        int returnvalue = (int)sp.ExecuteScalar();
        if (returnvalue == 1)
        {
            cusCustom.Visible = false;
            cusCustom1.Visible = false;
            LoadEmployeesData(empid);
        }
        else
        {
            cusCustom.Visible = true;          
           
        }
        }    
    public void LoadEmployeesData(String Employee)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_EMPLOYEES_DATA_BY_EMPID");
        sp.Command.AddParameter("@EMPID", Employee, DbType.String);
        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlDepartment.Text = ds.Tables[0].Rows[0]["DEPT"].ToString();
            ddlLocation.Text = ds.Tables[0].Rows[0]["LOC"].ToString();
            ddlDesignation.Text = ds.Tables[0].Rows[0]["DESG"].ToString();
            txtEmailId.Text = ds.Tables[0].Rows[0]["EMAIL"].ToString();
            txtMobile.Text = ds.Tables[0].Rows[0]["PH_NO"].ToString();
            txtEmailId.Text = ds.Tables[0].Rows[0]["EMAIL"].ToString();
            txtMobile.Text = ds.Tables[0].Rows[0]["PH_NO"].ToString();
            txtAddress.Text = ds.Tables[0].Rows[0]["LCM_ADDR"].ToString();
        }
        else
        {
            //LoadDepartment();
            //LoadLocations();
            //LoadDesignations();
        }      

    }
    public void LoadLocationData(String Location)
	    {
	        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_LOCATIONS_ADDRESS");
	        sp.Command.AddParameter("@lcm_code", Location, DbType.String);
	        DataSet ds = new DataSet();
	        ds = sp.GetDataSet();
	        if (ds.Tables[0].Rows.Count > 0)
	        {	            
	            txtAddress.Text = ds.Tables[0].Rows[0]["LCM_ADDR"].ToString();
	        }
	    }	
    protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLocation.Text != "")
        {
            int backSlashIndex2 = ddlLocation.Text.IndexOf("(");
            int backSlashIndex3 = ddlLocation.Text.IndexOf(")");
            backSlashIndex3 = backSlashIndex3 - (backSlashIndex2 + 3);
            backSlashIndex2 = backSlashIndex2 + 2;
            String locid;
            // String locid = ddlLocation.Text.Substring(backSlashIndex2, backSlashIndex3);          
            //String locid = (backSlashIndex2 >= 0) ? ddlLocation.Text.Substring(0, backSlashIndex2) : ddlLocation.Text;            
            if (backSlashIndex2 >= 0 && backSlashIndex3 > 0)
            {
                locid = ddlLocation.Text.Substring(backSlashIndex2, backSlashIndex3);
                LoadLocationData(ddlLocation.Text.Substring(backSlashIndex2, backSlashIndex3));
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "VALIDATE_LOCATION");
                sp.Command.AddParameter("@LOCID", locid, DbType.String);
                int returnvalue_Location = (int)sp.ExecuteScalar();
                if (returnvalue_Location == 1)
                {
                    cusCustom1.Visible = false;
                }
                else
                {
                    cusCustom1.Visible = true;
                }
            }
            else {
                locid = ddlLocation.Text;
                cusCustom1.Visible = true;
            }  
        }
    }
}