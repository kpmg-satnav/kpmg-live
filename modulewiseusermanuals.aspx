﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    
</head>
<body>
    <script defer src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="User Manuals" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">User Manuals</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" runat="server">

                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <a href="#" onclick="SpacePopWin()">Space Management</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <a href="#" onclick="PropertyPopWin()">Property Management</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <a href="#" onclick="AssetPopWin()">Asset Management</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <a href="#" onclick="MaintenancePopWin()">Maintenance Management</a>
                                        </div>
                                    </div>
                                </div>
                    
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <a href="#" onclick="HelpdiskPopWin()">Help Desk Management</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <a href="#" onclick="ConferencePopWin()">Conference Management</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block --%>

    <div class="modal fade" id="myModal" tabindex='-1' data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">User Manual </h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>

        function SpacePopWin() {
            $("#modalcontentframe").attr("src", "User_Manuals/Space_Management_System.pdf");
            $("#myModal").modal().fadeIn();
            return false;
        }

        function PropertyPopWin() {
            $("#modalcontentframe").attr("src", "User_Manuals/Property_Management_System.pdf");
            $("#myModal").modal().fadeIn();
            return false;
        }

        function AssetPopWin() {
            $("#modalcontentframe").attr("src", "User_Manuals/Asset_Management_System.pdf");
            $("#myModal").modal().fadeIn();
            return false;
        }

        function MaintenancePopWin() {
            $("#modalcontentframe").attr("src", "User_Manuals/Maintenance_Management_System.pdf");
            $("#myModal").modal().fadeIn();
            return false;
        }

        function HelpdiskPopWin() {
            $("#modalcontentframe").attr("src", "User_Manuals/Helpdesk_Management_System.pdf");
            $("#myModal").modal().fadeIn();
            return false;
        }

        function ConferencePopWin() {
            $("#modalcontentframe").attr("src", "User_Manuals/Conference_Management_System.pdf");
            $("#myModal").modal().fadeIn();
            return false;
        }
    </script>

</body>
</html>
