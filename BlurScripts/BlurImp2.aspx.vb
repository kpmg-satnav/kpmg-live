﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.IO

Partial Class BlurScripts_BlurImp2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If

        If Not IsPostBack Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_MENU_SCRIPT1_BOOTSTRAP")
            sp.Command.AddParameter("@AURID", Session("uid").ToString(), Data.DbType.String)
            litMenu.Text = sp.ExecuteScalar

            BindEmpDetails()
        End If
    End Sub

    Private Sub BindEmpDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_EMP_DETAILS_NEW")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As DataSet = sp.GetDataSet()
            If ds.Tables(0).Rows(0).Item("EMP_IMG") = "" Then
                img.ImageUrl = "../Userprofiles/default-user-icon-profile.jpg"
            Else
                img.ImageUrl = "~/userprofiles/" + ds.Tables(0).Rows(0).Item("EMP_IMG")
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class
