﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BlurImp2.aspx.vb" Inherits="BlurScripts_BlurImp2" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Facility Management Services::a-mantra</title>
    
    <link href="../BootStrapCSS/menu/css/metismenu.min.css" rel="stylesheet" />
    <style type="text/css">
        .sidebar {
            display: block;
            float: left;
            width: 250px;
            background: linear-gradient(to bottom,#466788 0,#1D4A81 100%);
            max-height: 800px;
            overflow: hidden;
            overflow-y: scroll;
            -webkit-overflow-scrolling: touch;
            position: initial;
        }

        .content {
            display: block;
            overflow: hidden;
            width: auto;
        }

        .sidebar-nav {
            background-repeat: repeat-x;
        }

            .sidebar-nav ul {
                padding: 0;
                margin: 0;
                list-style: none;
            }

            .sidebar-nav a,
            .sidebar-nav a:hover,
            .sidebar-nav a:focus,
            .sidebar-nav a:active {
                outline: none;
            }

            .sidebar-nav ul li,
            .sidebar-nav ul a {
                display: block;
            }

            .sidebar-nav ul a {
                padding: 10px 20px;
                border-top: 1px solid rgba(0, 0, 0, 0.3);
                box-shadow: 0px 1px 0px rgba(255, 255, 255, 0.05) inset;
                text-shadow: 0px 1px 0px rgba(0, 0, 0, 0.5);
                -ms-filter: "progid:DXImageTransform.Microsoft.Gradient(GradientType=1,StartColorStr=#466788,EndColorStr=#1D4A81)";
            }

                .sidebar-nav ul a:hover,
                .sidebar-nav ul a:focus,
                .sidebar-nav ul a:active {
                    color: #fff;
                    text-decoration: none;
                }

            .sidebar-nav ul ul a {
                padding: 10px 30px;
            }

        .sidebar-nav-item {
            padding-left: 5px;
        }

        .sidebar-nav-item-icon {
            padding-right: 5px;
        }
    </style>
    <%--<link href="BlurCss/ionicons.css" rel="stylesheet" />
    <link href="BlurCss/angular-toastr.css" rel="stylesheet" />
    <link href="BlurCss/animate.css" rel="stylesheet" />
    <link href="BlurCss/bootstrap.css" rel="stylesheet" />
    <link href="BlurCss/bootstrap-select.css" rel="stylesheet" />
    <link href="BlurCss/bootstrap-switch.css" rel="stylesheet" />
    <link href="BlurCss/font-awesome.css" rel="stylesheet" />
    <link href="BlurCss/leaflet.css" rel="stylesheet" />
    <link href="BlurCss/angular-progress-button-styles.min.css" rel="stylesheet" />
    <link href="BlurCss/angular-chart.css" rel="stylesheet" />
    <link href="BlurCss/morris.css" rel="stylesheet" />
    <link href="BlurCss/ion.rangeSlider.css" rel="stylesheet" />
    <link href="BlurCss/ion.rangeSlider.skinFlat.css" rel="stylesheet" />
    <link href="BlurCss/textAngular.css" rel="stylesheet" />
    <link href="BlurCss/xeditable.css" rel="stylesheet" />
    <link href="BlurCss/style.css" rel="stylesheet" />--%>
    <%--<link href="BlurCss/main.css" rel="stylesheet" />--%>
    <link href="../BootStrapCSS/FMS_UI/css/flaticon.css" rel="stylesheet" />
    <link href="BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="BlurCss/app-5b02d1ea3b.css" rel="stylesheet" />
    <style>
        body {
            background-image: url('/BlurScripts/BlurImages/blur-bg-blurred.jpg');
        }

        .panel {
            font-weight: 400;
            opacity: 30;
        }
    </style>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body>
    <div class="body-bg"></div>
    <main>

    <ba-sidebar>
        <aside class="al-sidebar" style="width:210px">
             <nav class="sidebar-nav">
                    <ul id="menu">
                        <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                    </ul>
                </nav>
        </aside>
    </ba-sidebar>

<page-top >
            <div class="page-top clearfix" scroll-position="scrolled" max-height="50" ng-class="{'scrolled': scrolled}">
<a href="BlurImp2.aspx" class="al-logo clearfix"><span>QuickFMS</span></a>
   <a href class="collapse-menu-link ion-navicon" ba-sidebar-toggle-menu></a> 
    <div class="user-profile clearfix">
        <div class="al-user-profile" uib-dropdown>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <asp:Image ID="img" runat="server" ImageUrl="../Userprofiles/default-user-icon-profile.jpg" AlternateText="No Image Available" />
        </a>
        </div>
    </div>
</div>
      
</page-top>
    <div class="al-main" style="padding-left: 19px;">
        <div class="al-content">
            <div class="widgets">
                
                <h4><b>VERTICAL ALLOCATION</b></h4>
                <br />
                <div ba-panel ba-panel-title="Vertical Release" ba-panel-class="with-scroll">
                    <div class="col-md-12 panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Vertical Release</h3>
                        </div>
                        <div class="col-md-12">
                            <div class="panel-body">
                                <div class="row">
                                    <form class="row form-inline">
                                        <div class="form-group">
                                            <div class="form-group col-md-4 col-xs-6">
                                                <label class="control-label">Country</label>
                                                <br />
                                                <select class="form-control selectpicker" selectpicker title="Select Country">
                                                    <option>India</option>
                                                    <option>United States</option>
                                                    <option>Canada</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4 col-xs-6">
                                                <label class="control-label">City</label>
                                                <select class="form-control selectpicker" selectpicker title="Select City">
                                                    <option>Hyderabad</option>
                                                    <option>Washington</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4 col-xs-6">
                                                <label class="control-label">Location</label>
                                                <select class="form-control selectpicker" selectpicker title="Select Location">
                                                    <option>Somajiguda</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-md-4 col-xs-6">
                                                <label class="control-label">Tower</label>
                                                <select class="form-control selectpicker" selectpicker title="Select Tower">
                                                    <option>Tower A</option>
                                                    <option>Tower B</option>
                                                    <option>Tower C</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4 col-xs-6">
                                                <label class="control-label">Floor</label>
                                                <select class="form-control selectpicker" selectpicker title="Select Tower">
                                                    <option>1 Floor</option>
                                                    <option>2 Floor</option>
                                                    <option>3 Floor</option>
                                                </select>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary pull-right">Clear</button>
                                        <button type="button" class="btn btn-primary pull-right">Search Spaces</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="al-footer clearfix ">
    <div class="al-footer-right" >QuickFMS 2016</div>
        <div class="al-footer-left" style="margin-left: 40px;">Other Links : www.quickfms.com</div>
    </footer>
   </main>
    <script src="BlurJs/angular.js" defer></script>
    <script src="BlurJs/jquery.js" defer></script>
    <script src="BlurJs/app-e2d130879d.js" defer></script>
    <script src="BlurJs/vendor-5da9147322.js" defer></script>
    <script src="BlurJs/moment.js" defer></script>
    <script src="BlurJs/angular-route.js" defer></script>
    <script src="BlurJs/jquery.slimscroll.js" defer></script>
    <script src="BlurJs/pageTop.directive.js" defer></script>
    <%--<script src="BlurJs/smart-table.js"></script>
    <script src="BlurJs/angular-toastr.tpls.js"></script>
    <script src="BlurJs/angular-touch.js"></script>
    <script src="BlurJs/jquery-ui.js"></script>
    <script src="BlurJs/sortable.js"></script>
    <script src="BlurJs/dropdown.js"></script>
    <script src="BlurJs/bootstrap-select.js"></script>
    <script src="BlurJs/bootstrap-switch.js"></script>
    <script src="BlurJs/bootstrap-tagsinput.js"></script>
    <script src="BlurJs/jquery.easing.js"></script>
    <script src="BlurJs/leaflet-src.js"></script>
    <script src="BlurJs/angular-progress-button-styles.min.js"></script>
    <script src="BlurJs/ui-bootstrap-tpls.js"></script>
    <script src="BlurJs/angular-animate.js"></script>
    <script src="BlurJs/rangy-core.js"></script>
    <script src="BlurJs/rangy-classapplier.js"></script>
    <script src="BlurJs/rangy-highlighter.js"></script>
    <script src="BlurJs/rangy-selectionsaverestore.js"></script>
    <script src="BlurJs/rangy-serializer.js"></script>
    <script src="BlurJs/rangy-textrange.js"></script>
    <script src="BlurJs/textAngular.js"></script>
    <script src="BlurJs/textAngularSetup.js"></script>
    <script src="BlurJs/xeditable.js"></script>
    <script src="BlurJs/selectpicker.directive.js"></script>
    <script src="BlurJs/SelectpickerPanelCtrl.js"></script>--%>
    <script src="../BootStrapCSS/FMS_UI/js/bootstrap.min.js" defer></script>
    <script src="../BootStrapCSS/menu/scripts/metismenu.min.js" defer></script>
    <script defer>
        $(function () {
            $('#menu').metisMenu();
        });
    </script>
</body>
</html>
