Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_PMC_Controls_PVMViewPlan
    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet

    Private Sub BindData()
        ds = New DataSet
        'ds = ObjSubSonic.GetSubSonicDataSet("GET_ALL_PVM_REQ")
        'ds = ObjSubSonic.GetSubSonicDataSet("MN_GET_ALL_PVM_REQ")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_PVM_REQ")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@STATUS", 3008, DbType.Int32)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            PM_REQ_DATA.DataSource = ds
            PM_REQ_DATA.DataBind()
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim flag
            flag = 1
            Dim uid
            uid = Session("uid")
            BindData()
        End If
    End Sub

    'Protected Sub PM_REQ_DATA_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
    '    'Try
    '    '    Dim btn As LinkButton = CType(e.CommandSource, LinkButton)
    '    '    If btn.Text = "View" Then
    '    '        ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "key", "window.open('frmPVMViewDetails.aspx?rid=" & e.Item.Cells(0).Text & "','_blank');", True)

    '    '        ' Response.Write("<script language=javascript>javascript:window.open('frmPVMViewDetails.aspx?rid=" & e.Item.Cells(0).Text & "','Window','toolbar=no,resizable=yes,statusbar=yes,top=0,left=0,width=790,height=550')</script>")
    '    '        'response.write("<script language=javascript>javascript:window.open('frmPVMViewDetails.aspx?rid=" & e.Item.Cells(0).Text & �&type=� & e.Item.Cells(3).Text "','Window','toolbar=no,resizable=yes,statusbar=yes,top=0,left=0,width=790,height=550')</script>")
    '    '    End If
    '    'Catch ex As Exception
    '    '    'Dim pageURL, trace, MSG As String
    '    '    Session("pageURL") = ex.GetBaseException.ToString
    '    '    Session("trace") = (ex.StackTrace.ToString)
    '    '    Session("MSG") = Session("pageURL") + Session("Trace")
    '    '    Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
    '    'End Try
    'End Sub

    Protected Sub PM_REQ_DATA_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles PM_REQ_DATA.PageIndexChanged
        PM_REQ_DATA.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        PM_REQ_DATA.CurrentPageIndex = 0

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEARCH_MN_GET_ALL_PVM_REQ")
        sp.Command.AddParameter("@PLAN_ID", txtAssetName.Text, Data.DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)


        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEARCH_MN_GET_ALL_PVM_REQ")
        'sp.Command.AddParameter("@PLAN_ID", txtAssetName.Text, Data.DbType.String)
        PM_REQ_DATA.DataSource = sp.GetDataSet
        PM_REQ_DATA.DataBind()
    End Sub
End Class
