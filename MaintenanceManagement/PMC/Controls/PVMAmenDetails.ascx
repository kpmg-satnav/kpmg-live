<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PVMAmenDetails.ascx.vb"
    Inherits="MaintenanceManagement_PMC_Controls_PVMAmenDetails" %>

<asp:TextBox ID="txtHiddenDate" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
<div class="row">
    <div class="col-md-12 col-sm-6 col-xs-12" style="padding-bottom: 17px">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-6 control-label"></asp:Label>
                <div class="col-md-6 pull right">
                    <button type="button" class="btn btn-default btn-sm " onclick="Print('helpdeskcontainer')" style="color: rgba(58, 156, 193, 0.87)">
                        <i class="fa fa-print fa-2x" aria-hidden="true"></i><span style="color: rgba(58, 156, 193, 0.87)"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="pnlContainer" runat="server">
    <div id="PanPremise" runat="server">
        <div class="row">

            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label style="font-weight: bold">Location:  </label>
                    <%--<asp:CompareValidator ID="CompareValidator9" runat="server" ControlToValidate="cboBuilding" Display="None" ErrorMessage="Please Select Location" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>--%>
                    <%--<asp:DropDownList ID="cboBuilding" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>--%>
                    <asp:Label ID="cboBuilding" runat="server" Text="Label" ForeColor="#209e91"></asp:Label>
                </div>
            </div>

            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label style="font-weight: bold">Asset Group</label>
                    :
                     <asp:Label ID="ddlGroup" runat="server" Text="Label" ForeColor="#209e91"></asp:Label>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label style="font-weight: bold">Asset Group Type :</label>
                    <%--<asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlgrouptype" Display="None" ErrorMessage="Please Select Asset Group Type" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>--%>
                    <%--<asp:DropDownList ID="ddlgrouptype" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>--%>
                    <asp:Label ID="ddlgrouptype" runat="server" Text="Label" ForeColor="#209e91"></asp:Label>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label style="font-weight: bold">Asset Brand :</label>
                    <%--<asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlbrand" Display="None" ErrorMessage="Please Select Asset Brand" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>--%>
                    <%--<asp:DropDownList ID="ddlbrand" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>--%>
                    <asp:Label ID="ddlbrand" runat="server" Text="Label" ForeColor="#209e91"></asp:Label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label style="font-weight: bold">Vendor :</label>
                    <%--<asp:CompareValidator ID="Comparevalidator10" runat="server" ControlToValidate="cboVendor11" Display="None" ErrorMessage="Please Select Vendor" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>--%>
                    <%--  <asp:DropDownList ID="cboVendor11" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                    </asp:DropDownList>--%>
                    <asp:Label ID="cboVendor11" runat="server" Text="Label" ForeColor="#209e91"></asp:Label>

                </div>
            </div>

            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">

                    <label style="font-weight: bold">Contract : </label>
                    <asp:Label ID="cboContract" runat="server" Text="Label" ForeColor="#209e91"></asp:Label>


                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">

                    <label style="font-weight: bold">Contract Start Date : </label>
                    <asp:Label ID="ContStdate" runat="server" Text="Label" ForeColor="#209e91"></asp:Label>


                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">

                    <label style="font-weight: bold">Contract End Date : </label>
                    <asp:Label ID="ContEndDate" runat="server" Text="Label" ForeColor="#209e91"></asp:Label>


                </div>
            </div>
            <div id="pnlCon" runat="server" visible="false">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">



                        <%--<div class="bootstrap-tagsinput">--%>
                        <%-- <label>No of Years<span style="color: red;">*</span></label>--%>
                        <div class="row ">
                            <div class="col-md-6">
                                <span class=" tag label label-info" id="venName" runat="server" style="font-size: small"></span>
                                <br />
                                <br />
                                <span class=" tag label label-info" id="NoOfYears" runat="server" style="font-size: small"></span>
                            </div>
                            <div class="col-md-6">
                                <span class=" tag label label-info" id="ConStartDate" runat="server" style="font-size: small"></span>
                                <br />
                                <br />

                                <span class=" tag label label-info" id="ConEndDate" runat="server" style="font-size: small"></span>
                            </div>
                        </div>
                        <%--</div>--%>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <br />
    <asp:Label ID="lblAmcFromDT" runat="server" Text="Label" Visible="false"></asp:Label>
    <asp:Label ID="lblAmcToDT" runat="server" Text="Label" Visible="false"></asp:Label>
    <div>
        <span class=" tag label label-info" id="Span1" runat="server" style="font-size: small"></span>
        <br />
    </div>
    <div class="row" id="PanSelAssets" runat="server">

        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <%--<div class="col-md-5">--%>
                    <label class="col-md-6 control-label">
                        <asp:ListBox ID="lstDisAssets" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                    </label>
                    <%-- </div>--%>
                    <div class="col-md-1">
                        <asp:Button ID="btnRight" runat="server" Text=">>" CssClass="btn btn-primary custom-button-color" CausesValidation="False"></asp:Button>
                        <asp:Button ID="btnLeft" runat="server" Text="<<" CssClass="btn btn-primary custom-button-color" CausesValidation="False"></asp:Button>
                    </div>
                    <div class="col-md-5">
                        <asp:ListBox ID="lstSelAssets" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <%--  <br />--%>
    </div>

    <div id="panAssetService" runat="server">

        <div id="df" runat="server" visible="false">

            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">

                        <label>Vendor Name<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Vendor Name"
                            ControlToValidate="txtConName" Display="None"></asp:RequiredFieldValidator>

                        <asp:TextBox ID="txtConName" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>


                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">

                        <label>No of Years<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Enter Vendor Name"
                            ControlToValidate="txtNoofYears" Display="None"></asp:RequiredFieldValidator>

                        <asp:TextBox ID="txtNoofYears" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>

                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">

                        <label>Contract Start Date<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Contractor Start Date"
                            ControlToValidate="txtConStart" Display="None"></asp:RequiredFieldValidator>

                        <asp:TextBox ID="txtConStart" CssClass="form-control" ReadOnly="True" runat="server" MaxLength="50"></asp:TextBox>

                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">

                        <label>Contract End Date<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please Select Contractor End Date"
                            ControlToValidate="txtConTo" Display="None"></asp:RequiredFieldValidator>

                        <asp:TextBox ID="txtConTo" CssClass="form-control" ReadOnly="True" runat="server" MaxLength="50"></asp:TextBox>

                    </div>
                </div>
            </div>




        </div>
        <%--   <div><br /></div>--%>

        <div id="panPeriod" runat="server">

            <div class="row">
                <div class="panel-heading" style="height: 41px;">
                    <h3 class="panel-title">Reschedule Plan</h3>

                    <%-- <label  >Schedule Plan</label></div>--%>
                </div>
                <%--</div>--%>
            </div>
            <br />

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-1 control-label">
                                <asp:RadioButton ID="radDaily" runat="server" CssClass="clsRadioButton" Text="Daily"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                            <label class="col-md-1 control-label">
                                <asp:RadioButton ID="radWeekly" runat="server" CssClass="clsRadioButton" Text="Weekly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                            <label class="col-md-2 control-label">
                                <asp:RadioButton ID="radMonthly" runat="server" CssClass="clsRadioButton" Text="Monthly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                            <label class="col-md-2 control-label">
                                <asp:RadioButton ID="radBiMonthly" runat="server" CssClass="clsRadioButton" Text="Bimonthly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                            <label class="col-md-2 control-label">
                                <asp:RadioButton ID="radQuarterly" runat="server" CssClass="clsRadioButton" Text="Quarterly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                            <label class="col-md-2 control-label">
                                <asp:RadioButton ID="radhalfyearly" runat="server" CssClass="clsRadioButton" Text="Half-Yearly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                            <label class="col-md-2 control-label">
                                <asp:RadioButton ID="radYearly" runat="server" CssClass="clsRadioButton" Text="Yearly"
                                    AutoPostBack="True"></asp:RadioButton>
                            </label>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div>


            <br />
        </div>


        <div id="panAllPeriods" runat="server">


            <div id="panWeekly" runat="server">

                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group">

                            <label>Day of the Week<span style="color: red;"></span></label>

                            <asp:DropDownList ID="W_DrpDwnWeek" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                <asp:ListItem Value="2">Monday</asp:ListItem>
                                <asp:ListItem Value="3">Tuesday</asp:ListItem>
                                <asp:ListItem Value="4">Wednesday</asp:ListItem>
                                <asp:ListItem Value="5">Thursday</asp:ListItem>
                                <asp:ListItem Value="6">Friday</asp:ListItem>
                                <asp:ListItem Value="7">Saturday</asp:ListItem>
                                <asp:ListItem Value="1">Sunday</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                </div>

            </div>

            <div id="panMonthly" runat="server">

                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group">

                            <label>Day of the Month<span style="color: red;"></span></label>

                            <asp:DropDownList ID="M_DrpDwnDate" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                            </asp:DropDownList>

                        </div>
                    </div>
                </div>

            </div>

            <div id="panBiMonthly" runat="server">

                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Month<span style="color: red;"></span></label>
                            <asp:DropDownList ID="B_DrpDwnMonth" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                <asp:ListItem Value="1">January</asp:ListItem>
                                <asp:ListItem Value="2">Febuary</asp:ListItem>
                                <asp:ListItem Value="3">March</asp:ListItem>
                                <asp:ListItem Value="4">April</asp:ListItem>
                                <asp:ListItem Value="5">May</asp:ListItem>
                                <asp:ListItem Value="6">June</asp:ListItem>
                                <asp:ListItem Value="7">July</asp:ListItem>
                                <asp:ListItem Value="8">August</asp:ListItem>
                                <asp:ListItem Value="9">September</asp:ListItem>
                                <asp:ListItem Value="10">October</asp:ListItem>
                                <asp:ListItem Value="11">November</asp:ListItem>
                                <asp:ListItem Value="12">December</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group">

                            <label>Day<span style="color: red;"></span></label>

                            <asp:DropDownList ID="B_DrpDwnDate" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>

                        </div>
                    </div>
                </div>

            </div>

            <div id="panQuarterly" runat="server">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-3 control-label">First Quarter </label>
                                <label class="col-md-3 control-label">Second Quarter</label>
                                <label class="col-md-3 control-label">Third Quarter</label>
                                <label class="col-md-3 control-label">Fourth Quarter</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="Q1_DrpDwnMonth" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                        <asp:ListItem Value="4">April</asp:ListItem>
                                        <asp:ListItem Value="5">May</asp:ListItem>
                                        <asp:ListItem Value="6">June</asp:ListItem>
                                    </asp:DropDownList>
                                </label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="Q2_DrpDwnMonth" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                        <asp:ListItem Value="7">July</asp:ListItem>
                                        <asp:ListItem Value="8">August</asp:ListItem>
                                        <asp:ListItem Value="9">September</asp:ListItem>
                                    </asp:DropDownList></label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="Q3_DrpDwnMonth" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                        <asp:ListItem Value="10">October</asp:ListItem>
                                        <asp:ListItem Value="11">November</asp:ListItem>
                                        <asp:ListItem Value="12">December</asp:ListItem>
                                    </asp:DropDownList></label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="Q4_DrpDwnMonth" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                        <asp:ListItem Value="1">January</asp:ListItem>
                                        <asp:ListItem Value="2">Febuary</asp:ListItem>
                                        <asp:ListItem Value="3">March</asp:ListItem>
                                    </asp:DropDownList></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="Q1_DrpDwnDate" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList></label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="Q2_DrpDwnDate" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList></label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="Q3_DrpDwnDate" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList></label>
                                <label class="col-md-3 control-label">
                                    <asp:DropDownList ID="Q4_DrpDwnDate" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                    </asp:DropDownList></label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div id="panYearly" runat="server">

                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group">

                            <label>Month<span style="color: red;"></span></label>

                            <asp:DropDownList ID="Y_DrpDwnMonth" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                <asp:ListItem Value="1">January</asp:ListItem>
                                <asp:ListItem Value="2">Febuary</asp:ListItem>
                                <asp:ListItem Value="3">March</asp:ListItem>
                                <asp:ListItem Value="4">April</asp:ListItem>
                                <asp:ListItem Value="5">May</asp:ListItem>
                                <asp:ListItem Value="6">June</asp:ListItem>
                                <asp:ListItem Value="7">July</asp:ListItem>
                                <asp:ListItem Value="8">August</asp:ListItem>
                                <asp:ListItem Value="9">September</asp:ListItem>
                                <asp:ListItem Value="10">October</asp:ListItem>
                                <asp:ListItem Value="11">November</asp:ListItem>
                                <asp:ListItem Value="12">December</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group">

                            <label>Day<span style="color: red;"></span></label>

                            <asp:DropDownList ID="Y_DrpDwnDate" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>

                        </div>
                    </div>
                </div>

            </div>

            <div id="panDaily" runat="server">

                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group">

                            <label>Time<span style="color: red;"></span></label>

                            <asp:DropDownList ID="D_DrpDwnHour" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                            </asp:DropDownList>

                        </div>
                    </div>
                </div>

            </div>
            <div></div>
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">

                        <label>PPM From Date<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvFromDt" runat="server" ErrorMessage="Please Select The From Date"
                            ControlToValidate="txtFromDate" Display="None"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvDates" runat="server" ErrorMessage="Please Select To Date Greater Than From Date"
                            ControlToValidate="txtFromDate" Display="None" Operator="LessThan" ControlToCompare="txtToDate"
                            Type="Date"></asp:CompareValidator>


                        <div class='input-group date' id='fromdate'>
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                            </span>
                        </div>


                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">

                        <label>PPM To Date<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="rfvToDt" runat="server" ErrorMessage="Please Select The To Date"
                            ControlToValidate="txtToDate" Display="None"></asp:RequiredFieldValidator>


                        <div class='input-group date' id='todate'>
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('todate')"></span>
                            </span>
                        </div>

                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6 col-sm-12 col-xs-12 ">
                    <label>Reminder Before</label>
                    <%--  <asp:TextBox ID="txtPriorDays"  runat="server"></asp:TextBox>--%>
                    <div class="bootstrap-tagsinput">
                        <%--         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please Select atleast one checkbox"
                            ControlToValidate="CheckBoxList1" Display="None"></asp:RequiredFieldValidator>--%>

                        <asp:CheckBoxList ID="CheckBoxList1" runat="server" CellPadding="25" CellSpacing="100" RepeatColumns="5" RepeatLayout="Table" RepeatDirection="Vertical" CausesValidation="True">
                            <asp:ListItem Value="90">90 Days</asp:ListItem>
                            <asp:ListItem Value="10">10 Days</asp:ListItem>

                            <asp:ListItem Value="60">60 Days</asp:ListItem>
                            <asp:ListItem Value="7">7 Days</asp:ListItem>

                            <asp:ListItem Value="45">45 Days</asp:ListItem>
                            <asp:ListItem Value="3">3 Days</asp:ListItem>

                            <asp:ListItem Value="30">30 Days</asp:ListItem>
                            <asp:ListItem Value="2">2 Days</asp:ListItem>

                            <asp:ListItem Value="20">20 Days</asp:ListItem>
                            <asp:ListItem Value="1">1 Day</asp:ListItem>
                            <%-- <asp:ListItem Text="Purple">Purple</asp:ListItem>--%>
                        </asp:CheckBoxList>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="col-md-12" for="txtcode">Remarks</label>
                        <div class="col-md-12">
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Height="30%" MaxLength="500"
                                    TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                    </div>
                </div>

            </div>
            <br />
            <div class="row">
                <div class="col-md-12 text-right">
                    <div class="form-group">
                        <asp:CheckBox ID="chkmail" runat="server" Checked="True" Text="Do You Want To Send A Mail To Vendor?" TextAlign="Right" />
                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit"></asp:Button>
                    </div>
                </div>
            </div>


        </div>

    </div>

</div>




