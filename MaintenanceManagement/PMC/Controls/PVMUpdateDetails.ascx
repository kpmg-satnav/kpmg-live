<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PVMUpdateDetails.ascx.vb" 
    Inherits="MaintenanceManagement_PMC_Controls_PVMUpdateDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<div class="row">
    <div class="col-md-12 col-sm-6 col-xs-12" style="padding-bottom: 17px">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblWarn" runat="server" ForeColor="RED" ></asp:Label>

                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" Visible="False"></asp:TextBox>
                <asp:Label ID="lblError" runat="server" ForeColor="RED"  Visible="False"></asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <%-- style="margin-top: 10px"--%>
    <div class="col-md-12">
        <div class="  table-responsive">
            <asp:DataGrid ID="PM_REQ_DATA" runat="server" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" PageSize="10"
                CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>
                    <asp:ButtonColumn Text="Update" HeaderText="Action" CommandName="Update">
                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                    </asp:ButtonColumn>

                    <asp:BoundColumn DataField="PVD_ID" HeaderText="S.No." Visible="false">
                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="ast_id" HeaderText="Asset S.No">
                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="ass_code" HeaderText="Asset ID" ItemStyle-Wrap="False">
                        <HeaderStyle Wrap="False" CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="PVD_PLANSCHD_DT" HeaderText="ScheduleDate" DataFormatString="{0:d}">
                        <HeaderStyle CssClass="clsTblHead" ></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="STA_TITLE" HeaderText="Status" ItemStyle-Wrap="False">
                        <HeaderStyle Wrap="False" CssClass="clsTblHead" Width="50px"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="stime" HeaderText="ScheduleTime" Visible="false">
                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="EXEC_DT" HeaderText="Executed Date">
                        <HeaderStyle  Wrap="False" CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="ETIME" HeaderText="Executed Time" >
                        <HeaderStyle Wrap="False" CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="SCOST" HeaderText="Spare Cost" ItemStyle-Wrap="False" DataFormatString="{0:c}" >
                        <HeaderStyle Wrap="False" CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="LCOST" HeaderText="Labour Cost" ItemStyle-Wrap="False" DataFormatString="{0:c}">
                        <HeaderStyle Wrap="False" CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="REMARKS" HeaderText="Remarks">
                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="brkStart" HeaderText="Breakdown Startup Time">
                        <HeaderStyle Wrap="False" CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="brkEnd" HeaderText="Breakdown EndUp Time">
                        <HeaderStyle Wrap="False" CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="brkExp" HeaderText="Breakdown Expenses" ItemStyle-Wrap="False" DataFormatString="{0:c}">
                        <HeaderStyle Wrap="False" CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="brkRem" HeaderText="Breakdown Remarks">
                        <HeaderStyle Wrap="False" CssClass="clsTblHead"></HeaderStyle>
                    </asp:BoundColumn>
                         <asp:TemplateColumn HeaderText="Download Updated Checklist" >
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDownload" Text='<%# If(Eval("PVD_UPLOADED_DOC").ToString() = "NA", "NA", "Download")%>' CommandArgument='<%# Eval("PVD_UPLOADED_DOC")%>' runat="server" OnClick="DownloadFile"></asp:LinkButton>
                        </ItemTemplate>
                              <HeaderStyle Wrap="False" CssClass="clsTblHead"></HeaderStyle>
                    </asp:TemplateColumn>
                </Columns>
                        <HeaderStyle ForeColor="white" BackColor="Black" />
     
                <PagerStyle NextPageText=" Next" PrevPageText="Previous " HorizontalAlign="Left" Position="Top"></PagerStyle>
            </asp:DataGrid>
        </div>
    </div>
</div>
 <br />

<div id="pnlButton" runat="server" visible="False">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-6 control-label">
                        (TIME FORMAT: 00:00 to 23:59)</label>
                </div>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
           
                    <label >Executed Date<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtExeDate"
                        Display="none" ErrorMessage="Please Select Executed Date"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvfDate" runat="server" ControlToValidate="txtExeDate"
                        Display="None" ErrorMessage="Please Select Executed Date Less Than Current Date" Operator="LessThanEqual"
                        ControlToCompare="txtDate"></asp:CompareValidator>

                
                        <div class='input-group date' id='fromdate'>
                            <asp:TextBox ID="txtExeDate" runat="server" CssClass="form-control"></asp:TextBox>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                            </span>
                        </div>
                  
            
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
             
                    <label >Executed Time<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtExeTime"
                        Display="none" ErrorMessage="Please Enter Executed Time"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revTime" runat="server" ErrorMessage="Incorrect Time Format (Please Follow 24Hr Format)"
                        Display="None" ControlToValidate="txtExeTime" ValidationExpression="^([0-1][0-9]|[2][0-3]):([0-5][0-9])$"></asp:RegularExpressionValidator>
                 
                        <asp:TextBox ID="txtExeTime" runat="server" CssClass="form-control"></asp:TextBox>
                 
                </div>
           
        </div>
               <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
           
                    <label >Spare Cost<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtSpr"
                        Display="none" ErrorMessage="Please Enter Spare Cost"></asp:RequiredFieldValidator>

                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtSpr"
                        FilterMode="ValidChars" ValidChars="0123456789">
                    </cc1:FilteredTextBoxExtender>
               
                        <asp:TextBox ID="txtSpr" CssClass="form-control" runat="server"></asp:TextBox>
               
          
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
        
                    <label >Labour Cost<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLbr"
                        Display="none" ErrorMessage="Please Enter Labour Cost"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtLbr"
                        FilterMode="ValidChars" ValidChars="0123456789">
                    </cc1:FilteredTextBoxExtender>
                
                        <asp:TextBox ID="txtLbr" CssClass="form-control" runat="server"></asp:TextBox>
                 
            
            </div>
        </div>
    </div>

 

    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
          
                    <label >Remarks<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtRmks"
                        Display="none" ErrorMessage="Please Enter Remarks"></asp:RequiredFieldValidator>
               
                        <asp:TextBox ID="txtRmks" CssClass="form-control" runat="server"></asp:TextBox>
                
             
            </div>
        </div>
           <div class="col-md-3 col-sm-12 col-xs-12" id="rbnInsUpload" runat="server">
            <div class="form-group">
              
                    <label >Upload Checklist Document</label>

                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" ControlToValidate="fpBrowseIncDoc"
                        ValidationGroup="Val1" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                        ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([pP][nN][gG])|([tT][iI][fF]))$"> 
                            
                    </asp:RegularExpressionValidator>

                   
                        <div class=" btn-default">
                            <i class="fa fa-folder-open-o fa-lg"></i>
                            <asp:FileUpload ID="fpBrowseIncDoc" runat="Server" onchange="show(this)" Width="90%" />

                        </div>
                        <div id="image-holder"></div>

                        <asp:LinkButton ID="hplINC" runat="server" Visible="false"></asp:LinkButton>
                        <asp:Label ID="lblINC" runat="server" Text="" Visible="false"></asp:Label>
                        <asp:Literal ID="litINC" Text="" runat="server"></asp:Literal>
                
             
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
            
                      <asp:CheckBox ID="chkBrk" runat="server" AutoPostBack="True"></asp:CheckBox>
                    <label >Want to Enter Breakdown Info?</label>
                  
                      
                    
            
            </div>
        </div>
    </div>

    <div id="pnlBrkDwn" runat="server" style="padding-top:30px">
        Breakdown Details:
   <hr />

        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                   
                        <label >Startup Time & Date:<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtBRKStartDt"
                            Display="none" ErrorMessage="Please Select Startup Time And Date"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtBRKStartDt"
                            Display="None" ErrorMessage="Please Select Startup Time & Date Less Than Current Date" Operator="LessThanEqual"
                            ControlToCompare="txtDate"></asp:CompareValidator>
                   
                            <asp:DropDownList ID="cboBRKStHr" runat="server" CssClass="selectpicker" data-live-search="true">
                                <asp:ListItem Value="00">00</asp:ListItem>
                                <asp:ListItem Value="01">01</asp:ListItem>
                                <asp:ListItem Value="02">02</asp:ListItem>
                                <asp:ListItem Value="03">03</asp:ListItem>
                                <asp:ListItem Value="04">04</asp:ListItem>
                                <asp:ListItem Value="05">05</asp:ListItem>
                                <asp:ListItem Value="06">06</asp:ListItem>
                                <asp:ListItem Value="07">07</asp:ListItem>
                                <asp:ListItem Value="08">08</asp:ListItem>
                                <asp:ListItem Value="09">09</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="13">13</asp:ListItem>
                                <asp:ListItem Value="14">14</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="17">17</asp:ListItem>
                                <asp:ListItem Value="18">18</asp:ListItem>
                                <asp:ListItem Value="19">19</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="21">21</asp:ListItem>
                                <asp:ListItem Value="22">22</asp:ListItem>
                                <asp:ListItem Value="23">23</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="cboBRKStMin" runat="server" CssClass="selectpicker" data-live-search="true">
                                <asp:ListItem Value="00">00</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="30">30</asp:ListItem>
                                <asp:ListItem Value="45">45</asp:ListItem>
                            </asp:DropDownList>

                            <div class='input-group date' id='fromdate1'>
                                <asp:TextBox ID="txtBRKStartDt" runat="server" CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('fromdate1')"></span>
                                </span>
                            </div>
                        
                   
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
             
                        <label >End Up Time & Date:<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtBRKEndDt"
                            Display="none" ErrorMessage="Please Select End Up Time And Date"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtBRKEndDt"
                            Display="None" ErrorMessage="Please Select End Up Time & Date Less Than Current Date" Operator="LessThanEqual"
                            ControlToCompare="txtDate"></asp:CompareValidator>
                   
                            <asp:DropDownList ID="cboBRKEndHr" runat="server" CssClass="selectpicker" data-live-search="true">
                                <asp:ListItem Value="00">00</asp:ListItem>
                                <asp:ListItem Value="01">01</asp:ListItem>
                                <asp:ListItem Value="02">02</asp:ListItem>
                                <asp:ListItem Value="03">03</asp:ListItem>
                                <asp:ListItem Value="04">04</asp:ListItem>
                                <asp:ListItem Value="05">05</asp:ListItem>
                                <asp:ListItem Value="06">06</asp:ListItem>
                                <asp:ListItem Value="07">07</asp:ListItem>
                                <asp:ListItem Value="08">08</asp:ListItem>
                                <asp:ListItem Value="09">09</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="13">13</asp:ListItem>
                                <asp:ListItem Value="14">14</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="17">17</asp:ListItem>
                                <asp:ListItem Value="18">18</asp:ListItem>
                                <asp:ListItem Value="19">19</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="21">21</asp:ListItem>
                                <asp:ListItem Value="22">22</asp:ListItem>
                                <asp:ListItem Value="23">23</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="cboBRKEndMin" runat="server" CssClass="selectpicker" data-live-search="true">
                                <asp:ListItem Value="00">00</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="30">30</asp:ListItem>
                                <asp:ListItem Value="45">45</asp:ListItem>
                            </asp:DropDownList>

                            <div class='input-group date' id='fromdate2'>
                                <asp:TextBox ID="txtBRKEndDt" runat="server" CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar" onclick="setup('fromdate2')"></span>
                                </span>
                            </div>
                   
               
                </div>
            </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                   
                        <label >Expenditure for the Services(In Cost):<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtBrkExp"
                            Display="none" ErrorMessage="Please Enter Expenditure Incurred for the Services(in Cost)"></asp:RequiredFieldValidator>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtBrkExp"
                            FilterMode="ValidChars" ValidChars="0123456789">
                        </cc1:FilteredTextBoxExtender>
                      
                            <asp:TextBox ID="txtBrkExp" CssClass="form-control" runat="server" MaxLength="10"></asp:TextBox>
                        
                 
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
          
                        <label >Breakdown Remarks<span style="color: red;">*</span></label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtBRKRem"
                            Display="none" ErrorMessage="Please Enter Breakdown Remarks"></asp:RequiredFieldValidator>
                    
                            <asp:TextBox ID="txtBRKRem" CssClass="form-control" runat="server"></asp:TextBox>
                   
                 
                </div>
            </div>
        </div>

        <div class="row">

        </div>
            <div class="row">
     
    </div>

    </div>


    <div class="row">
        <div class="col-md-12 text-right">
            <div class="form-group">
                <asp:Button ID="cmdSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit"></asp:Button>

            </div>
        </div>
    </div>

</div>
