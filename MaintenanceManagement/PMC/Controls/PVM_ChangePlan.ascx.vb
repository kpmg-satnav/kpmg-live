Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_PMC_Controls_PVM_ChangePlan
    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet

    Private Sub BindData()
        ds = New DataSet
        'ds = ObjSubSonic.GetSubSonicDataSet("GET_ALL_PVM_REQ")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_PVM_REQ_NEW")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@STATUS", 3008, DbType.Int32)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            dgPvmReq.DataSource = ds
            dgPvmReq.DataBind()
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim flag
            flag = 1
            Dim uid
            uid = Session("uid")
            BindData()
        End If
    End Sub

    Protected Sub dgPvmReq_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgPvmReq.PageIndexChanged
        dgPvmReq.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        dgPvmReq.CurrentPageIndex = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEARCH_MN_GET_ALL_PVM_REQ_NEW")
        sp.Command.AddParameter("@PLAN_ID", txtAssetName1.Text, Data.DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        dgPvmReq.DataSource = sp.GetDataSet
        dgPvmReq.DataBind()
    End Sub
End Class
