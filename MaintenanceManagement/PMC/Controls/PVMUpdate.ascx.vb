Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_PMC_Controls_PVMUpdate
    Inherits System.Web.UI.UserControl

    Dim ds As New DataSet
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Private Sub BindData()
        ds = New DataSet
        'ds = objsubsonic.GetSubSonicDataSet("GET_ALL_PVM_REQ")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_PVM_REQ")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@STATUS", 3008, DbType.Int32)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            PM_REQ_DATA.DataSource = ds
            PM_REQ_DATA.DataBind()
        End If

       

        Dim cnt As Integer
        For cnt = 0 To PM_REQ_DATA.Items.Count() - 1
            Dim val As String
            Dim lbClose As LinkButton = DirectCast(PM_REQ_DATA.Items(cnt).Cells(4).FindControl("lnkDownload"), LinkButton)

            val = lbClose.Text
            If val = "NA" Then

                Dim lnkbtn As LinkButton = TryCast(PM_REQ_DATA.Items(0).FindControl("lnkDownload"), LinkButton)
                lnkbtn.Enabled = False

            End If
        Next
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            Dim flag
            flag = 1
            Dim uid
            uid = Session("uid")
            BindData()
        End If
    End Sub

    Sub PM_REQ_DATA_Paged(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
        PM_REQ_DATA.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub

    Protected Sub PM_REQ_DATA_PageIndexChanged(source As Object, e As DataGridPageChangedEventArgs) Handles PM_REQ_DATA.PageIndexChanged
        PM_REQ_DATA.CurrentPageIndex = e.NewPageIndex
        BindData()
    End Sub
    'Protected Sub PM_REQ_DATA_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles PM_REQ_DATA.PageIndexChanging
    '    PM_REQ_DATA.PageIndex = e.NewPageIndex
    '    BindData()
    'End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        'PM_REQ_DATA.CurrentPageIndex = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEARCH_MN_GET_ALL_PVM_REQ")
        sp.Command.AddParameter("@PLAN_ID", txtAssetName.Text, Data.DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEARCH_MN_GET_ALL_PVM_REQ")
        'sp.Command.AddParameter("@PLAN_ID", txtAssetName.Text, Data.DbType.String)
        PM_REQ_DATA.DataSource = sp.GetDataSet
        PM_REQ_DATA.DataBind()
    End Sub
    Protected Sub DownloadFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim filePath As String = CType(sender, LinkButton).CommandArgument
        If Not filePath = "NA" Then
            filePath = "~\UploadFiles\" & filePath
            Response.ContentType = "application/CSV"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "~\UploadFiles", "") & """")
            Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If

    End Sub
End Class
