Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Reporting.WebForms

Partial Class MaintenanceManagement_PMC_Reports_frmAssetNotUnderMaintenance
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If

        If Not Page.IsPostBack Then

            Dim flag
            Dim uid
            uid = Session("uid")
            flag = 1
            BindBuilding()
            cboGrp.Items.Insert("0", "--All--")
            cboGrpType.Items.Insert("0", "--All--")
            cboBrd.Items.Insert("0", "--All--")
            cboMdl.Items.Insert("0", "--All--")
            BindGrid()
        End If
    End Sub

    Private Sub BindBuilding()
        'ObjSubSonic.Binddropdown(cboBuilding, "AMC_GETBUILDING", "BDG_NAME", "BDG_ADM_CODE")
        ObjSubSonic.Binddropdown(cboBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE")
        cboBuilding.Items.Insert("0", "--All--")
        cboBuilding.Items.RemoveAt(1)
    End Sub

    Protected Sub cboBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBuilding.SelectedIndexChanged

        ReportViewer1.Visible = False
        lblMsg.Visible = False
        cboGrp.Enabled = True
        cboGrp.Items.Clear()
        cboGrp.Items.Insert("0", "--All--")

        'gdAsset.Visible = False
        If cboBuilding.SelectedItem.Value <> "--All--" Then
            'If cboBuilding.SelectedItem.Text = "--All--" Then
            '    ObjSubSonic.Binddropdown(cboGrp, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)
            '    cboGrp.Items.Insert("1", "--All--")
            '    'BindGrid()
            '    'gdAsset.Visible = False
            '    'cboGrp.Enabled = False
            '    'cboGrpType.Enabled = False
            '    'cboBrd.Enabled = False
            '    'cboMdl.Enabled = False
            '    'cboGrp.Enabled = False
            'Else
            Dim param() As SqlParameter = New SqlParameter(0) {}
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            'ObjSubSonic.Binddropdown(cboGrp, "PVM_ANUM_BDG", "FLR_NAME", "FLR_CODE", param)
            'ObjSubSonic.Binddropdown(cboGrp, "PVM_ANUM_FLOOR", "GP", "GP_ID", param)
            ObjSubSonic.Binddropdown(cboGrp, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION_REPORT", "GROUP_NAME", "GROUP_ID", param)
            cboGrp.Items.Insert("0", "--All--")
            cboGrp.Items.RemoveAt(1)
            'End If
        End If

        'cboGrp.Items.Clear()
        'cboGrp.Items.Insert("0", "--Select--")

        cboGrpType.Items.Clear()
        cboGrpType.Items.Insert("0", "--All--")


        cboBrd.Items.Clear()
        cboBrd.Items.Insert("0", "--All--")


        cboMdl.Items.Clear()
        cboMdl.Items.Insert("0", "--All--")


        lblNo.Visible = False
    End Sub

    Private Sub BindGrid()

        Dim strBuilding As String = ""
        Dim strFloor As String = ""
        Dim strAstGrp As String = ""
        Dim strAstGrpType As String = ""
        Dim strAstBrand As String = ""
        Dim strAstMdl As String = ""

        Try
            If cboBuilding.SelectedItem.Value = "--All--" Then
                strBuilding = ""
            Else
                strBuilding = cboBuilding.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try

        Try

            If cboGrp.SelectedItem.Value = "--All--" Then
                strFloor = ""
            Else
                strFloor = cboGrp.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try

        Try
            If cboGrp.SelectedItem.Value = "--All--" Then
                strAstGrp = ""
            Else
                strAstGrp = cboGrp.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try

        Try
            If cboGrpType.SelectedItem.Value = "--All--" Then
                strAstGrpType = ""
            Else
                strAstGrpType = cboGrpType.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try


        Try
            If cboBrd.SelectedItem.Value = "--All--" Then
                strAstBrand = ""
            Else
                strAstBrand = cboBrd.SelectedItem.Value
            End If
        Catch ex As Exception

        End Try

        Try
            If cboMdl.SelectedItem.Value = "--All--" Then
                strAstMdl = ""
            Else
                strAstMdl = cboMdl.SelectedItem.Value
            End If

        Catch ex As Exception

        End Try

        Dim ds As New DataSet
        Dim param() As SqlParameter = New SqlParameter(4) {}
        param(0) = New SqlParameter("@BDG_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = strBuilding
        'param(1) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        'param(1).Value = strFloor
        param(1) = New SqlParameter("@AST_GRP", SqlDbType.NVarChar, 200)
        param(1).Value = strAstGrp
        param(2) = New SqlParameter("@AST_GRPTYPE", SqlDbType.NVarChar, 200)
        param(2).Value = strAstGrpType
        param(3) = New SqlParameter("@AST_BRND", SqlDbType.NVarChar, 200)
        param(3).Value = strAstBrand
        param(4) = New SqlParameter("@AST_MDL", SqlDbType.NVarChar, 200)
        param(4).Value = strAstMdl
        ds = ObjSubSonic.GetSubSonicDataSet("MN_GET_ASSETNOT_MAINT_REPORT", param)
        Dim rds As New ReportDataSource()
        rds.Name = "AssetNotUnderMaintenanceDS"
        rds.Value = ds.Tables(0)
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/AssetNotUnderMaintenanceReport.rdlc")

        'Setting Header Column value dynamically

        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        'gdAsset.DataSource = ds
        'gdAsset.DataBind()

        If (ds.Tables(0).Rows.Count <= 0) Then

            lblMsg.Visible = True
            'lblMsg.Text = "No Records Found With Selected Criteria"
        Else
            lblMsg.Visible = True
            lblMsg.Text = ""
            'gdAsset.Visible = True
        End If

    End Sub


    Protected Sub cboGrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGrp.SelectedIndexChanged
        ReportViewer1.Visible = False
        lblMsg.Visible = False
        'gdAsset.Visible = False
        cboGrpType.Enabled = True
        cboGrpType.Items.Clear()
        cboGrpType.Items.Insert("0", "--All--")
        If cboGrp.SelectedItem.Value <> "--All--" Then
            'If cboGrp.SelectedItem.Text = "--All--" Then
            '    BindGrid()
            '    gdAsset.Visible = False
            '    cboGrpType.Enabled = False
            '    cboBrd.Enabled = False
            '    cboMdl.Enabled = False
            'Else
            'Dim param() As SqlParameter = New SqlParameter(2) {}
            'param(1) = New SqlParameter("@FLRID", SqlDbType.NVarChar, 200)
            'param(1).Value = cboGrp.SelectedItem.Value
            'param(1) = New SqlParameter("@AGRPID", SqlDbType.NVarChar, 200)
            'ObjSubSonic.Binddropdown(cboGrpType, "PVM_ANUM_AGRP", "GPT", "AGT_CODE", param)

            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = cboGrp.SelectedItem.Value
            ObjSubSonic.Binddropdown(cboGrpType, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP_REPORT", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
            cboGrpType.Items.Insert("0", "--All--")
            cboGrpType.Items.RemoveAt(1)

            'End If
        End If

        cboBrd.Items.Clear()
        cboBrd.Items.Insert("0", "--All--")

        cboMdl.Items.Clear()
        cboMdl.Items.Insert("0", "--All--")
        lblNo.Visible = False
    End Sub

    Protected Sub cboBrd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBrd.SelectedIndexChanged
        ReportViewer1.Visible = False
        lblMsg.Visible = False
        'gdAsset.Visible = False
        cboMdl.Enabled = True
        cboMdl.Items.Clear()
        cboMdl.Items.Insert("0", "--All--")
        If cboBrd.SelectedItem.Value <> "--All--" Then
            'If cboBrd.SelectedItem.Text = "--All--" Then
            '    BindGrid()
            '    gdAsset.Visible = False
            '    cboMdl.Enabled = False
            'Else
            'BindGrid()

            '-----Replaced with SP PVM_ANUM_ABRND
            'Dim param() As SqlParameter = New SqlParameter(4) {}
            'param(1) = New SqlParameter("@FLRID", SqlDbType.NVarChar, 200)
            'param(1).Value = cboGrp.SelectedItem.Value
            'param(2) = New SqlParameter("@AGRPID", SqlDbType.NVarChar, 200)
            'param(2).Value = cboGrp.SelectedItem.Value
            'param(3) = New SqlParameter("@AGRPTYP", SqlDbType.NVarChar, 200)
            'param(3).Value = cboGrpType.SelectedItem.Value

            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = cboGrp.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = cboGrpType.SelectedItem.Value
            param(3) = New SqlParameter("@BRD_ID", SqlDbType.NVarChar, 200)
            param(3).Value = cboBrd.SelectedItem.Value
            'ObjSubSonic.Binddropdown(cboMdl, "PVM_ANUM_ABRND", "AAM_NAME", "AAM_CODE", param)
            ObjSubSonic.Binddropdown(cboMdl, "MN_GET_ASSET_MODEL_BY_LOCGRUPBRND_REPORT", "MODEL_NAME", "MODEL_CODE", param)
            cboMdl.Items.Insert("0", "--All--")
            cboMdl.Items.RemoveAt(1)
        End If
        'End If
        lblNo.Visible = False
    End Sub

    Protected Sub cboGrpType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGrpType.SelectedIndexChanged
        ReportViewer1.Visible = False
        lblMsg.Visible = False
        'gdAsset.Visible = False
        cboBrd.Enabled = True
        cboBrd.Items.Clear()
        cboBrd.Items.Insert("0", "--All--")
        If cboGrpType.SelectedItem.Value <> "--All--" Then
            'If cboGrpType.SelectedItem.Text = "--All--" Then
            '    BindGrid()
            '    gdAsset.Visible = False
            '    cboMdl.Enabled = False
            'Else

            'BindGrid()
            '------Replaced with SP PVM_ANUM_AGRPTYP
            'Dim param() As SqlParameter = New SqlParameter(3) {}
            'param(1) = New SqlParameter("@FLRID", SqlDbType.NVarChar, 200)
            'param(1).Value = cboGrp.SelectedItem.Value
            'param(2) = New SqlParameter("@AGRPID", SqlDbType.NVarChar, 200)
            'param(2).Value = cboGrp.SelectedItem.Value

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = cboGrp.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = cboGrpType.SelectedItem.Value
            'ObjSubSonic.Binddropdown(cboBrd, "PVM_ANUM_AGRPTYP", "AAB_NAME", "AAB_CODE", param)
            ObjSubSonic.Binddropdown(cboBrd, "MN_GET_ASSETBRAND_LOCGRUPTYPE_REPORT", "BRAND_NAME", "BRAND_ID", param)
            cboBrd.Items.Insert("0", "--All--")
            cboBrd.Items.RemoveAt(1)
        End If
        'End If
        lblNo.Visible = False
        cboMdl.Items.Clear()
        cboMdl.Items.Insert("0", "--All--")
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        lblNo.Visible = False
        BindGrid()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~/MaintenanceManagement/AMC/Reports/ViewAMCReports.aspx")
    End Sub


End Class
