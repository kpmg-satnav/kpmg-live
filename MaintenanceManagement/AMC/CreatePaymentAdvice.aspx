﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CreatePaymentAdvice.aspx.vb" Inherits="MaintenanceManagement_AMC_CreatePaymentAdvice" %>

<%--<%@ Register Src="Controls/PaymentAdvice.ascx" TagName="PaymentAdvice" TagPrefix="uc1" %>--%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>

<script type="text/ecmascript" defer>
    function refreshSelectpicker() {
        $("#<%=lstPendingMemo.ClientID%>").selectpicker();

    };
    refreshSelectpicker();
</script>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Payment Memo" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Create Payment Advice</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="Assetpanel" runat="server">
                            <ContentTemplate>
                                <%-- <uc1:PaymentAdvice ID="PaymentAdvice1" runat="server" />--%>
                                <div class="row" style="padding-top: 10px">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                                <asp:TextBox ID="txtListCount" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
                                                <asp:CompareValidator ID="cvList" runat="server" ErrorMessage="Please Select Payment Memos..."
                                                    Display="None" ControlToValidate="txtListCount" Operator="NotEqual" Type="Integer"
                                                    ValueToCompare="0"></asp:CompareValidator>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="Panel1" runat="server">

                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>PPM Plan Id <b>:</b></label>
                                                <asp:Label ID="lblReqId" Font-Bold="true" runat="server" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Location <b>:</b></label>
                                                <asp:Label ID="cboBuilding" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Asset Group <b>:</b></label>
                                                <asp:Label ID="ddlGroup" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Asset Group Type<b>:</b></label>
                                                <asp:Label ID="ddlgrouptype" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Asset Brand <b>:</b></label>
                                                <asp:Label ID="ddlbrand" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Vendor<b>:</b></label>
                                                <asp:Label ID="cmbVen" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Asset Name<span style="color: red;">*</span></label>
                                                <asp:ListBox ID="lstasset" runat="server" CssClass="form-control" SelectionMode="Multiple"></asp:ListBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Payment Memos <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvpaymem" runat="server" ErrorMessage="Please Select Payment Memo"
                                                    Display="None" ControlToValidate="lstPendingMemo" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:ListBox ID="lstPendingMemo" runat="server" CssClass="form-control" SelectionMode="Multiple" AutoPostBack="true"></asp:ListBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label>Payment Amount<span style="color: red;">*</span></label>
                                                    <asp:TextBox ID="txtPayAmt" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Mode Of Payment<span style="color: red;">*</span></label>
                                                <%--  <asp:CompareValidator ID="cfvcmbPayMode" runat="server" ValueToCompare="--Select--"
                                                    Operator="NotEqual" ControlToValidate="cmbPayMode" Display="None" ErrorMessage="Please Select Mode Of Payment"></asp:CompareValidator>--%>
                                                <asp:RequiredFieldValidator ID="rfvpaymod" runat="server" ErrorMessage="Please Select Mode Of Payment"
                                                    Display="None" ControlToValidate="cmbPayMode" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="cmbPayMode" runat="server" CssClass="form-control selectpicker" data-live-search="true" ToolTip="--Select--">
                                                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                    <asp:ListItem Value="1">Cheque</asp:ListItem>
                                                    <asp:ListItem Value="2">Demand Draft</asp:ListItem>
                                                    <asp:ListItem Value="3">Cash</asp:ListItem>
                                                    <asp:ListItem Value="NA">NA</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="box-footer text-right">
                                            <div class="form-group">
                                                <asp:Button ID="btnSub" CssClass="btn btn-default btn-primary" Text="Create Advice" runat="server" ValidationGroup="Val2"></asp:Button>
                                                <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="false" CssClass="btn btn-default btn-primary" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

