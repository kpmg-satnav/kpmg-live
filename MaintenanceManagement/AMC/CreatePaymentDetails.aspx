﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CreatePaymentDetails.aspx.vb" Inherits="MaintenanceManagement_AMC_CreatePaymentDetails" %>

<%--<%@ Register Src="Controls/PaymentDetails.ascx" TagName="PaymentDetails" TagPrefix="uc1" %>--%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
    <script type="text/ecmascript" defer>
        function refreshSelectpicker() {
            $("#<%=cmbPayment.ClientID%>").selectpicker();

        };
        refreshSelectpicker();
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Payment Memo" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Create Payment Details</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="Assetpanel" runat="server">
                            <ContentTemplate>
                                <%-- <uc1:PaymentDetails ID="PaymentDetails1" runat="server" />--%>
                                <div class="row" style="padding-top: 10px">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="Panel1" runat="server">

                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>PPM Plan Id <b>:</b></label>
                                                <asp:Label ID="lblReqId" Font-Bold="true" runat="server" CssClass="control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Location <b>:</b></label>
                                                <asp:Label ID="cboBuilding" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Asset Group <b>:</b></label>
                                                <asp:Label ID="ddlGroup" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Asset Group Type<b>:</b></label>
                                                <asp:Label ID="ddlgrouptype" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Asset Brand <b>:</b></label>
                                                <asp:Label ID="ddlbrand" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Vendor<b>:</b></label>
                                                <asp:Label ID="cmbVen" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Asset Name<span style="color: red;"></span></label>
                                                <asp:ListBox ID="lstasset" runat="server" CssClass="form-control" data-live-search="true"></asp:ListBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Payment Advice No<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvpaymem" runat="server" ErrorMessage="Please Select Payment Advice No"
                                                    Display="None" ControlToValidate="cmbPayment" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="cmbPayment" Display="None"
                                                    ErrorMessage="Please Select Payment Advice No" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                                                <asp:DropDownList ID="cmbPayment" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Payment Advice Generation Date<span style="color: red;"></span></label>
                                                <asp:TextBox ID="txtcheckdate" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>DD/Cheque/Acc No<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtChqNo" Display="None"
                                                    ErrorMessage="Please Enter Cheque/DD/Acc No" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtChqNo" CssClass="form-control" runat="server" MaxLength="15"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Dated/Credited On<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtChqDate" Display="None"
                                                    ErrorMessage="Please Select Dated/Credited On" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <div class='input-group date' id='fromdate'>
                                                    <asp:TextBox ID="txtChqDate" CssClass="form-control" runat="server"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Amount<span style="color: red;"></span></label>
                                                <asp:TextBox ID="txtChqAmt" CssClass="form-control" AutoPostBack="True" ReadOnly="True" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                            <div class="form-group">
                                                <asp:Button ID="btnSub" CssClass="btn btn-default btn-primary" runat="server" Text="Create" ValidationGroup="Val2"></asp:Button>
                                                <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="false" CssClass="btn btn-default btn-primary" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


