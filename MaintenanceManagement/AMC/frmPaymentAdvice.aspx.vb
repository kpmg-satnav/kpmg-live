Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class MaintenanceManagement_AMC_frmPaymentAdvice
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            BindData()
        End If
    End Sub

    Private Sub BindData()

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CREATE_PAYMENT_ADVICE")
        sp1.Command.AddParameter("@AURID", Session("uid"), DbType.String)
        sp1.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        grdViewAssets.DataSource = sp1.GetDataSet()
        grdViewAssets.DataBind()
        If (grdViewAssets.Items.Count = 0) Then
            lblmsg.Text = "No Records Found"
            grdViewAssets.Visible = False
            pnlbutton.Visible = False
        End If
    End Sub

    Sub grdViewAssets_Paged(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)

        grdViewAssets.CurrentPageIndex = e.NewPageIndex
        BindData()

    End Sub

    Private Sub grdViewAssets_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdViewAssets.PageIndexChanged

        grdViewAssets.CurrentPageIndex = e.NewPageIndex
        BindData()

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Response.Redirect("ViewAMCReports.aspx")
    End Sub

    Protected Sub cmdExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ExportToExcelEmployeeAllocationReport()
    End Sub

    Private Sub ExportToExcelEmployeeAllocationReport()

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CREATE_PAYMENT_ADVICE")
        sp1.Command.AddParameter("@AURID", Session("uid"), DbType.String)
        sp1.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        grdViewAssets.DataSource = sp1.GetDataSet()
        grdViewAssets.DataBind()

        Dim ds As New DataSet
        ds = sp1.GetDataSet()

        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        Export("PaymentAdvice.xls", gv)
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        lblmsg.Text = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEARCH_PAYMENT_BY_PLAN_ID_LOCATION_VENDOR")
        sp.Command.AddParameter("@PLAN_ID", txtReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        grdViewAssets.DataSource = sp.GetDataSet
        grdViewAssets.DataBind()
        If (grdViewAssets.Items.Count = 0) Then

            lblmsg.Text = "No Records Found"
        End If
    End Sub
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub

    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
End Class
