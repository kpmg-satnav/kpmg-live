﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAMCWorkOrder.aspx.vb" Inherits="MaintenanceManagement_AMC_Reports_frmAMCWorkOrder" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="View Work Order" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">View Work Order Details</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <%--<rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>--%>
                                    <div class="row form-inline">
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="row">
                                                    <asp:Label ID="lblMsg" runat="server" class="col-md-10" ForeColor="Red" Style="padding-bottom: 20px"></asp:Label>
                                                    <asp:Label ID="lblerror" runat="server" class="col-md-12" ForeColor="Red" Style="padding-bottom: 20px"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Label ID="Status" runat="server" class="col-md-10" Visible="false"></asp:Label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                    <asp:RangeValidator ID="rvAdvance" runat="server" MinimumValue="0" MaximumValue="999999999"
                                                        Type="Double" ControlToValidate="txtAdvance" Display="None" ErrorMessage="Advance Must be greater than or Equal to 0"></asp:RangeValidator>

                                                    <asp:TextBox ID="TxtAssetCtr" Visible="false" ReadOnly="True" CssClass="form-control"
                                                        runat="server" Text="0" EnableViewState="False">0</asp:TextBox>
                                                    <asp:TextBox ID="txtval" runat="server" CssClass="form-control" Visible="False"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" runat="server" visible="false">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="txtcode" for="txtcode">Status ID <span style="color: red;"></span></label>
                                                <div class="col-md-12">
                                                    <asp:TextBox ID="txtstaid" CssClass="form-control" EnableViewState="False" Visible="False"
                                                        runat="server" ReadOnly="True" MaxLength="50"></asp:TextBox>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-inline">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcode">Location <span style="color: red;">*</span></label>
                                                <asp:TextBox ID="cboBuilding" CssClass="form-control" EnableViewState="False"
                                                    runat="server" ReadOnly="True" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcode">Asset Group <span style="color: red;">*</span></label>
                                                <asp:TextBox ID="ddlGroup" CssClass="form-control" EnableViewState="False"
                                                    runat="server" ReadOnly="True" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcode">Asset Group Type <span style="color: red;">*</span></label>
                                                <asp:TextBox ID="ddlgrouptype" CssClass="form-control" EnableViewState="False"
                                                    runat="server" ReadOnly="True" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode">Asset Brand <span style="color: red;">*</span></label>
                                                <asp:TextBox ID="ddlbrand" CssClass="form-control" EnableViewState="False"
                                                    runat="server" ReadOnly="True" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row form-inline">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode" style="width: 90px">Vendor<span style="color: red;">*</span></label>
                                                <asp:TextBox ID="cboVendor" CssClass="form-control" EnableViewState="False"
                                                    runat="server" ReadOnly="True" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode">Maint. Contract ID<span style="color: red;">*</span></label>
                                                <asp:TextBox ID="DrpDwnAMCId" CssClass="form-control" EnableViewState="False"
                                                    runat="server" ReadOnly="True" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode">Asset Name<span style="color: red;">*</span></label>
                                                <asp:ListBox ID="lstasset" runat="server" CssClass="form-control" Rows="3" TextMode="MultiLine" Width="100%" ReadOnly="True"></asp:ListBox>

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcode">Maint. Contract Start Date<span style="color: red;">*</span></label>
                                                <asp:TextBox ID="txtAMCSDate" runat="server" CssClass="form-control" AutoPostBack="True" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row form-inline">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode">Maint. Contract End Date<span style="color: red;">*</span></label>
                                                <asp:TextBox ID="txtAMCEDate" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode">Work Order Start Date<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvTD" runat="server" ErrorMessage="Please Select Start Date"
                                                    ControlToValidate="txtDate" Display="None"></asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="cvfrmdate" runat="server" ControlToValidate="txtDate"
                                                    Display="None" ErrorMessage="Please Select Start Date" Operator="DataTypeCheck"
                                                    Type="Date"></asp:CompareValidator>
                                                <div class='input-group date' id='fromdate'>
                                                    <asp:TextBox ID="txtDate" runat="server" CssClass="form-control"
                                                        EnableViewState="False" MaxLength="10"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode" style="width: 90px">Total<span style="color: red;">*</span></label>
                                                <asp:RegularExpressionValidator ID="regXTot" runat="server" ErrorMessage="Please Enter A Valid Total"
                                                    Display="None" ControlToValidate="txtGrandTot" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                                                <asp:TextBox ID="txtGrandTot" CssClass="form-control" runat="server" TextMode="Number" min="0"
                                                    AutoPostBack="True" OnTextChanged="txtGrandTot_TextChanged" Text="0">0</asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode" style="width: 90px">Taxes<span style="color: red;">*</span></label>
                                                <asp:RegularExpressionValidator ID="regXTax" runat="server" ErrorMessage="Please Enter A Valid Tax Details "
                                                    Display="None" ControlToValidate="txtTax" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                                                <asp:TextBox ID="txtTax" CssClass="form-control" runat="server" AutoPostBack="True" TextMode="Number" min="0"
                                                    OnTextChanged="txtTax_TextChanged"></asp:TextBox>
                                                <asp:Button Style="z-index: 0" Visible="false" ID="btnCalc" CssClass="clsButton" runat="server" Text="Re-Calculate"></asp:Button>
                                            </div>
                                        </div>
                                    </div>

                                    <asp:Label ID="lblAssets" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>

                                    <%--  <div id="Panel1" visible="false" runat="server">--%>

                                    <div class="row form-inline">


                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode">Grand Total<span style="color: red;">*</span></label>
                                                <asp:RegularExpressionValidator ID="regXGT" runat="server" ErrorMessage="Please Enter A Valid Grand total"
                                                    Display="None" ControlToValidate="TxtTotCost" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                                                <asp:TextBox ID="TxtTotCost" CssClass="form-control" EnableViewState="False"
                                                    runat="server" ReadOnly="True" value="0"></asp:TextBox>

                                            </div>
                                        </div>


                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode">Vendor Name</label>
                                                <asp:TextBox ID="txtConName" CssClass="form-control" EnableViewState="False"
                                                    runat="server" ReadOnly="True" MaxLength="50"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode">Vendor Address</label>
                                                <asp:TextBox ID="txtConAdd" CssClass="form-control" EnableViewState="False" Width="100%" Height="30%"
                                                    runat="server" ReadOnly="True" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode">Phone No.</label>
                                                <asp:TextBox ID="txtConPh" CssClass="form-control" runat="server" ReadOnly="True"
                                                    MaxLength="15"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-inline">


                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode" style="width: 90px">Email</label>
                                                <asp:TextBox ID="txtConPer" CssClass="form-control" runat="server" ReadOnly="True"
                                                    Visible="True"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode" style="width: 90px">Advance</label>
                                                <asp:RegularExpressionValidator ID="regXAdv" runat="server" ErrorMessage="Please Enter A Valid Advance Amount !"
                                                    Display="None" ControlToValidate="txtAdvance" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="rfvtxtAdvance" runat="server" ControlToValidate="txtAdvance"
                                                    Display="None" ErrorMessage="Please Enter Advance Amount!"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtAdvance" CssClass="form-control" EnableViewState="False" TextMode="Number" min="0"
                                                    runat="server" MaxLength="10">0</asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode">Half Yearly</label>
                                                <asp:RegularExpressionValidator ID="regXHy" runat="server" ErrorMessage="Please Enter A Valid Half-Year Amount !"
                                                    Display="None" ControlToValidate="txtHalfYear" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                                                <asp:TextBox ID="txtHalfYear" CssClass="form-control" EnableViewState="False" TextMode="Number" min="0"
                                                    runat="server" MaxLength="10">0</asp:TextBox>

                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode" style="width: 90px">Monthly</label>
                                                <asp:RegularExpressionValidator ID="regXMnt" runat="server" ErrorMessage="Please Enter A Valid Monthly Advance Amount !"
                                                    Display="None" ControlToValidate="txtMonthly" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                                                <asp:TextBox ID="txtMonthly" CssClass="form-control" EnableViewState="False" TextMode="Number" min="0"
                                                    runat="server" MaxLength="10">0</asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row form-inline">

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode" style="width: 90px">Annual</label>
                                                <asp:RegularExpressionValidator ID="regXAnn" runat="server" ErrorMessage="Please Enter A Valid Annual Amount !"
                                                    Display="None" ControlToValidate="txtAnnual" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                                                <asp:TextBox ID="txtAnnual" CssClass="form-control" EnableViewState="False" TextMode="Number" min="0"
                                                    runat="server" MaxLength="10">0</asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode" style="width: 90px">Quarterly</label>
                                                <asp:RegularExpressionValidator ID="regxQtly" runat="server" ErrorMessage="Please Enter A Valid Quarter Amount !"
                                                    Display="None" ControlToValidate="txtQuarter" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                                                <asp:TextBox ID="txtQuarter" CssClass="form-control" EnableViewState="False" TextMode="Number" min="0"
                                                    runat="server" MaxLength="10">0</asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode" style="width: 90px">Others</label>
                                                <asp:RegularExpressionValidator ID="regXOth" runat="server" ErrorMessage="Please Enter A Valid Others Amount !"
                                                    Display="None" ControlToValidate="TxtOthers" ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                                                <asp:TextBox ID="TxtOthers" CssClass="form-control" EnableViewState="False" TextMode="Number" min="0"
                                                    runat="server" MaxLength="10">0</asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcode" style="width: 90px">Remarks</label>
                                                <asp:TextBox ID="txtRemarks" CssClass="form-control" Width="100%" Height="30%"
                                                    runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-inline">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcode">Terms & Conditions</label>
                                                <asp:TextBox ID="txtTerms" CssClass="form-control" Width="100%" Height="30%" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">

                                                <label for="txtcode">Do You Want To Send A Mail To Vendor?</label>
                                                <asp:CheckBox ID="chkmail" runat="server" Checked="True" />

                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12 text-right">

                                            <div class="col-md-12">
                                                <%--<asp:Button ID="btnVIew" runat="server" CssClass="btn btn-primary" OnClientClick="showPopWin()" Text="View"></asp:Button>&nbsp;&nbsp;--%>
                                                <%--<input type="submit" value="View" onclick="showPopWin()" class="btn btn-primary custom-button-color" />--%>
                                                <input type='button' value='View' class="btn btn-primary" onclick="showPopWin()" />
                                                <asp:Button ID="btnSub" CssClass="btn btn-primary" runat="server" Text="Update"></asp:Button>&nbsp;&nbsp;                                
                                                        <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary" Text="Back" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" runat="server" tabindex='-1' data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Maint. Contract - Work Order Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript" defer>

        var id = '<%= Session("req")%>';
        function showPopWin() {
            $("#modalcontentframe").attr("src", "../../AMC/frmAMCWorkOrder.aspx?rid=" + id);
            $("#myModal").modal().fadeIn();
            return false;
        }
    </script>
</body>
</html>
