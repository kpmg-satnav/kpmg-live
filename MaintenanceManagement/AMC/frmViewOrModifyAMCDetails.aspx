﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewOrModifyAMCDetails.aspx.vb" Inherits="MaintenanceManagement_AMC_frmViewOrModifyAMCDetails" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="View Or Modify Maintenance Contract" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">View Or Modify Maintenance Contract</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                        <div class="row" style="padding-top: 10px;">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-6 control-label">Search by Requisition ID / Location / Vendor<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                            Display="none" ErrorMessage="Please Enter Requisition ID/Location/Vendor" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-12">
                                    <asp:Button ID="btnSubmit" CssClass="btn btn-default btn-primary" runat="server" Text="Search" ValidationGroup="Val1"
                                        CausesValidation="true" TabIndex="2" />

                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <asp:GridView ID="gvItems" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                                EmptyDataText="No Records Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                <Columns>
                                    <asp:TemplateField HeaderText="Requisition ID">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AMN_PLAN_ID", "~/MaintenanceManagement/AMC/frmViewOrModifyAMC.aspx?RID={0}")%>'
                                                Text='<%#Eval("AMN_PLAN_ID")%>'></asp:HyperLink>
                                            <asp:Label ID="lblPlanid" runat="server" Text='<%#Eval("AMN_PLAN_ID")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLocation" Text='<%#Eval("LOCATION")%>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVendor" Text='<%#Eval("VENDORNAME")%>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Maint. Contract Years">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAMCYears" Text='<%#Eval("AMN_NO_OF_YEARS")%>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Maint. Contract Start Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAMNFromDate" Text='<%#Eval("AMN_FROM_DATE") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Maint. Contract End Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAMNToDate" Text='<%#Eval("AMN_TO_DATE") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Maint. Contract Created Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAMCCreateddate" Text='<%#Eval("CREATED_DATE")%>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
