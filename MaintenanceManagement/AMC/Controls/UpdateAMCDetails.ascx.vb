Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports System.Globalization

Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO
Imports System.Data.OleDb
Imports System.Collections.Generic

Partial Class MaintenanceManagement_AMC_Controls_UpdateAMCDetails
    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Request.QueryString("rid") <> "" Then
            Session("rid") = Request.QueryString("rid")
        End If
        If Session("uid") = "" Then
            Response.Redirect(Application("fmglogout"))
        End If
        lblmsg.Visible = False
        txtAMCDate.Attributes.Add("readonly", "readonly")
        txtSDate.Attributes.Add("readonly", "readonly")
        txtEDate.Attributes.Add("readonly", "readonly")
        'ddlGroup.Items.Insert(0, "--Select--")
        'ddlgrouptype.Items.Insert(0, "--Select--")
        'ddlbrand.Items.Insert(0, "--Select--")
        'ddlVendor.Items.Insert(0, "--Select--")
        If Not IsPostBack Then
            If Session("ViewOrModify") = "1" Then
                btnSubmit.Enabled = False
            End If
            txtAmcYears.Text = "1"
            lblmsg.Visible = False
            txtHidTodayDt.Text = getoffsetdate(Date.Today)
            FillBFT_Panel()
            Disable_All()

            'ddlBuilding.Focus()
            If ddlBuilding.Items.Count = 1 Then
                lblmsg.Visible = True
                lblmsg.Text = "Assets Not Available For AMC !"
                Exit Sub
            End If
            btnExport.Visible = False
        End If
    End Sub

    Sub FillBFT_Panel()
        Disable_All()
        'lblerror.Visible = False
        ddlBuilding.Items.Clear()
        BindBuilding()
    End Sub

    Private Sub Disable_All()
        PanSelAssets.Visible = False
    End Sub

    Private Sub BindBuilding()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID").ToString
        ObjSubSonic.Binddropdown(ddlBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Protected Sub ddlBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBuilding.SelectedIndexChanged
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
        param(0).Value = ddlBuilding.SelectedItem.Value
        clearitems()
        ObjSubSonic.Binddropdown(ddlGroup, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)
        'ddlGroup.Focus()

        Panel2.Visible = False
        PanSelAssets.Visible = False
    End Sub

    Public Sub clearitems()
        ddlbrand.Items.Clear()
        ddlGroup.Items.Clear()
        ddlgrouptype.Items.Clear()
        ddlVendor.Items.Clear()
        txtAMCDate.Text = ""
    End Sub

    

    Public Sub cleardata()
        ddlbrand.ClearSelection()
        ddlGroup.ClearSelection()
        ddlgrouptype.ClearSelection()
        ddlVendor.ClearSelection()
    End Sub

    Private Sub fillgrid()
        Dim dt As DateTime
        If Not txtAMCDate.Text = "" Then
            dt = DateTime.ParseExact(txtAMCDate.Text.ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture)
        Else
            dt = Now.ToShortDateString
        End If
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSETS_BY_EXPIRY_AMC_EXPIRY_WRNT_DATE")
        sp.Command.AddParameter("@BDG_ID", ddlBuilding.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAP_VENDOR", ddlVendor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAP_GROUP_ID", ddlGroup.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAP_GROUPTYPE_ID", ddlgrouptype.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AAP_BRAND_ID", ddlbrand.SelectedItem.Value, DbType.String)
        'CDate(dt.ToShortDateString)
        sp.Command.AddParameter("@AMC_DATE", CDate(dt.ToShortDateString), DbType.Date)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()

        If gvItems.Rows.Count = 0 Then
            lblmsg.Text = "No Assets To Create Maintenance Contract"
            lblmsg.Visible = True
            PanSelAssets.Visible = False
            Panel2.Visible = False
        Else
            lblmsg.Visible = False
            PanSelAssets.Visible = True
            Panel2.Visible = True
        End If

    End Sub

    Protected Sub ddlVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendor.SelectedIndexChanged
        Panel2.Visible = True
        PanSelAssets.Visible = True
    End Sub

    Protected Sub btnGetData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetData.Click
        fillgrid()
        PanSelAssets.Visible = True
        Dim dt As DateTime
        If txtAMCDate.Text = "" Then
            txtSDate.Text = Now.ToShortDateString
        Else
            txtSDate.Text = txtAMCDate.Text
        End If
        'txtSDate.Text = txtAMCDate.Text
        Dim sdt As DateTime = Convert.ToDateTime(txtSDate.Text)
        Dim toDate As DateTime
        Dim result As String = txtSDate.Text
        toDate = CDate(result).AddYears(txtAmcYears.Text)
        txtEDate.Text = toDate.ToString("MM/dd/yyyy")
    End Sub

    Dim plan As String ''This is not use full updated in final page

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'Page.Culture = "en-GB"
        Dim fdt As DateTime = txtSDate.Text
        Dim tdt As DateTime = txtEDate.Text
        Dim todaydt As DateTime = txtHidTodayDt.Text
        lblmsg.Visible = True
        If CDate(tdt.ToShortDateString) < CDate(fdt.ToShortDateString) Then
            lblmsg.Text = "Maint. Contract Start Date should be less than Maint. Contract End Date"
            Exit Sub
            'ElseIf CDate(fdt.ToShortDateString) < CDate(todaydt.ToShortDateString) Then
            '    lblmsg.Text = "Maint. Contract Start Date Cannot Be Less Than Today's Date"
            '    Exit Sub
        ElseIf CDate(tdt.ToShortDateString) < CDate(fdt.ToShortDateString) Then
            lblmsg.Text = "ToMaint. Contract End Date should be greater than Maint. Contract Start Date "
            Exit Sub
        End If
        If Not Page.IsValid Then
            Exit Sub
        End If
        Dim amnfdt As DateTime = txtSDate.Text
        Dim amntdt As DateTime = txtEDate.Text

        Dim amcdate As DateTime
        If Not txtAMCDate.Text = "" Then
            amcdate = txtAMCDate.Text
        Else
            amcdate = Now.ToShortDateString
        End If

        If amntdt.ToShortDateString < getoffsetdate(Date.Today) Then
            lblmsg.Visible = True
            lblmsg.Text = "Please Select AMC End Date Greater Than Todays Date"
            Exit Sub
        End If
        Dim arparam(), param(), param1() As SqlParameter
        If gvItems.Rows.Count > 0 Then
            plan = Session("uid") & "REQ" & Year(getoffsetdatetime(DateTime.Now)) & Month(getoffsetdatetime(DateTime.Now)) & Day(getoffsetdatetime(DateTime.Now)) & Hour(getoffsetdatetime(DateTime.Now)) & Minute(getoffsetdatetime(DateTime.Now)) & Second(getoffsetdatetime(DateTime.Now))
            ''''**Plan code updated in  final page using stored procedure UPDATE_AMC_MAIN_DETAILS
            lblmsg.Visible = False
            If Len(txtComments.Text) > 200 Then
                lblmsg.Visible = True
                lblmsg.Text = "Please Enter The Comments Less Than 200 Characters"
                Exit Sub
            ElseIf txtComments.Text = String.Empty Then
                txtComments.Text = "NA"
            Else
                lblmsg.Visible = False
            End If
            Dim Mainflag, Checkflag
            Mainflag = 1
            Checkflag = 1
            Dim i As Integer
            Dim icnt As Integer = 0
            param = New SqlParameter(19) {}
            param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
            param(0).Value = plan
            param(1) = New SqlParameter("@AMN_PLAN_FOR", SqlDbType.NVarChar, 50)
            param(1).Value = "Office"
            param(2) = New SqlParameter("@AMN_BDG_ID", SqlDbType.NVarChar, 50)
            param(2).Value = ddlBuilding.SelectedItem.Value
            param(3) = New SqlParameter("@AMN_TWR_ID", SqlDbType.Int)
            param(3).Value = 0
            param(4) = New SqlParameter("@AMN_TYPE_ID ", SqlDbType.NVarChar, 50)
            param(4).Value = ""
            param(5) = New SqlParameter("@AMN_CTM_ID", SqlDbType.NVarChar, 50)
            param(5).Value = ddlVendor.SelectedItem.Value
            param(6) = New SqlParameter("@AMN_FROM_DATE", SqlDbType.DateTime)
            param(6).Value = CDate(amnfdt.ToShortDateString)
            param(7) = New SqlParameter("@AMN_TO_DATE", SqlDbType.DateTime)
            param(7).Value = CDate(amntdt.ToShortDateString)
            param(8) = New SqlParameter("@AMN_COMMENTS", SqlDbType.NVarChar, 250)
            param(8).Value = Replace(Trim(txtComments.Text), "'", "''")
            param(9) = New SqlParameter("@AMN_UPD_BY", SqlDbType.NVarChar, 10)
            param(9).Value = Session("uid")
            param(10) = New SqlParameter("@AMN_UPD_DT", SqlDbType.DateTime)
            param(10).Value = getoffsetdatetime(DateTime.Now)
            param(11) = New SqlParameter("@AMN_CNTR_NAME", SqlDbType.NVarChar, 250)
            param(11).Value = ddlVendor.SelectedItem.Text
            param(12) = New SqlParameter("@AMN_AST_GROUP", SqlDbType.NVarChar, 250)
            param(12).Value = ddlGroup.SelectedItem.Value
            param(13) = New SqlParameter("@AMN_AST_GROUP_TYPE", SqlDbType.NVarChar, 250)
            param(13).Value = ddlgrouptype.SelectedItem.Value
            param(14) = New SqlParameter("@AMN_AST_BRAND", SqlDbType.NVarChar, 250)
            param(14).Value = ddlbrand.SelectedItem.Value
            param(15) = New SqlParameter("@AMN_EXPIRING_ON_OR_BEFORE", SqlDbType.DateTime)

            param(15).Value = CDate(amcdate.ToShortDateString)
            param(16) = New SqlParameter("@AMN_NO_OF_YEARS", SqlDbType.Int)
            param(16).Value = CInt(txtAmcYears.Text)
            param(17) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(17).Value = Session("COMPANYID")
            param(18) = New SqlParameter("@COMPREHENSIVE", SqlDbType.Int)
            param(18).Value = ddlType.SelectedItem.Value
            param(19) = New SqlParameter("@VEN_MAIL", SqlDbType.Bit)
            param(19).Value = chkVenMail.Checked
            ObjSubSonic.GetSubSonicExecute("AMC_INSRT_AMC_DTLS", param)
            Dim chkCount As Integer = 0
            For Each row As GridViewRow In gvItems.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim lblwarrantyDate As Label = DirectCast(row.FindControl("lblwarrantyDate"), Label)

                If chkSelect.Checked And Not lblwarrantyDate.Text = "" Then
                    If CDate(lblwarrantyDate.Text) > CDate(txtSDate.Text) Then
                        chkCount = chkCount + 1
                        lblmsg.Visible = True
                        lblmsg.Text = "Warranty Date should be less than Contract Start Date !"
                        Exit Sub
                    End If
                End If
            Next
            If chkCount = 0 Then
                For Each row As GridViewRow In gvItems.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                    Dim lbAAP_CODE As Label = DirectCast(row.FindControl("lbAAP_CODE"), Label)
                    Dim lblAssetName As Label = DirectCast(row.FindControl("lblAssetName"), Label)
                    Dim lblAAP_RUNNO As Label = DirectCast(row.FindControl("lblAAP_RUNNO"), Label)
                    Dim lblwarrantyDate As Label = DirectCast(row.FindControl("lblwarrantyDate"), Label)
                    If chkSelect.Checked Then
                        arparam = New SqlParameter(3) {}
                        arparam(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
                        arparam(0).Value = plan
                        arparam(1) = New SqlParameter("@AAT_CODE", SqlDbType.NVarChar, 200)
                        arparam(1).Value = lbAAP_CODE.Text
                        arparam(2) = New SqlParameter("@AAT_NAME", SqlDbType.NVarChar, 200)
                        arparam(2).Value = lblAssetName.Text
                        arparam(3) = New SqlParameter("@COMPANYID", SqlDbType.Int)
                        arparam(3).Value = Session("COMPANYID")
                        ObjSubSonic.GetSubSonicExecute("INSRT_AMC_DTLS", arparam)
                    End If
                Next
            End If

            ' Sending mail in final page
            'SendAMCMail()

            If Mainflag = 2 Then
                lblmsg.Visible = True
                lblmsg.Text = "AMC Already Submitted !"
                Exit Sub
            Else
                lblmsg.Visible = False
                If Len(Trim(txtSDate.Text)) = 0 And Len(Trim(txtEDate.Text)) = 0 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "Select Start Date And End Date !"
                    Exit Sub
                ElseIf Len(Trim(txtSDate.Text)) = 0 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "Select Start Date !"
                    Exit Sub
                ElseIf Len(Trim(txtEDate.Text)) = 0 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "Select End Date !"
                    Exit Sub
                End If
                If Checkflag = 2 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "AMC Already Submitted For This Dates."
                    Exit Sub
                Else
                    lblmsg.Visible = False
                    Dim strAst As String = String.Empty
                    strAst = Trim(ddlVendor.SelectedItem.Value)
                    ''''**Plan generated code updated in  final page using stored procedure UPDATE_AMC_MAIN_DETAILS
                    Response.Redirect("frmAMCfinalpage.aspx?rid=" & plan & "&asset=" & strAst & "&staid=submitted" & "&Ven_mail=" & chkVenMail.Checked & "&MAIL_ID=" & 1)
                End If
            End If
        Else
            lblmsg.Visible = True
            lblmsg.Text = "Select At Least one Asset"
        End If
    End Sub

    'Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim arparam(), param(), param1() As SqlParameter

    'If lstRight.Items.Count > 0 Then
    '    Dim aid, plan, MainSql
    '    Dim planid As String
    '    aid = Sanitize.SanitizeInput(Session("uid"))
    '    plan = Sanitize.SanitizeInput(Session("rid"))
    '    lblmsg.Visible = False

    '    If Len(txtComments.Text) > 100 Then
    '        lblmsg.Visible = True
    '        lblmsg.Text = "Please Enter The Comments Less Than 100 Characters"
    '        Exit Sub
    '    ElseIf txtComments.Text = String.Empty Then
    '        txtComments.Text = "NA"
    '    Else
    '        lblmsg.Visible = False
    '    End If


    '    MainSql = "exec AMC_MAIN_PLAN '" & plan & "'"
    '    Dim Mainflag, Checkflag
    '    Mainflag = 1
    '    Checkflag = 1
    '    strSQL = MainSql


    '    ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL)
    '    If ObjDR.Read() Then
    '        Mainflag = 2
    '        planid = ObjDR("amn_plan_id")
    '    End If
    '    ObjDR.Close()


    '    Dim i As Integer
    '    Dim icnt As Integer = 0
    '    lstRight.DataValueField = lstLeft.DataValueField
    '    If Mainflag = 1 Then
    '        For icnt = 0 To lstRight.Items.Count - 1
    '            '----------Replaced By SP ------------------
    '            param = New SqlParameter(11) {}
    '            param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
    '            param(0).Value = plan
    '            param(1) = New SqlParameter("@AMN_PLAN_FOR", SqlDbType.NVarChar, 50)
    '            param(1).Value = "Office"
    '            param(2) = New SqlParameter("@AMN_BDG_ID", SqlDbType.Int)
    '            param(2).Value = ddlBuilding.SelectedItem.Value
    '            param(3) = New SqlParameter("@AMN_TWR_ID", SqlDbType.Int)
    '            param(3).Value = 0
    '            param(4) = New SqlParameter("@AMN_TYPE_ID ", SqlDbType.NVarChar, 50)
    '            param(4).Value = lstRight.Items(icnt).Text
    '            param(5) = New SqlParameter("@AMN_CTM_ID", SqlDbType.Int)
    '            param(5).Value = ddlVendor.SelectedItem.Value
    '            param(6) = New SqlParameter("@AMN_FROM_DATE", SqlDbType.DateTime)
    '            param(6).Value = CDate(txtSDate.Text)
    '            param(7) = New SqlParameter("@AMN_TO_DATE", SqlDbType.DateTime)
    '            param(7).Value = CDate(txtEDate.Text)
    '            param(8) = New SqlParameter("@AMN_COMMENTS", SqlDbType.NVarChar, 250)
    '            param(8).Value = Replace(Trim(txtComments.Text), "'", "''")
    '            param(9) = New SqlParameter("@AMN_UPD_BY", SqlDbType.NVarChar, 10)
    '            param(9).Value = aid
    '            param(10) = New SqlParameter("@AMN_UPD_DT", SqlDbType.DateTime)
    '            param(10).Value = getoffsetdatetime(DateTime.Now)

    '            param(11) = New SqlParameter("@AMN_CNTR_NAME", SqlDbType.NVarChar, 250)
    '            param(11).Value = ddlVendor.SelectedItem.Text
    '            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "AMC_INSRT_AMC_DTLS", param)



    '            '----------replaced By SP ------------------
    '            param1 = New SqlParameter(5) {}
    '            param1(0) = New SqlParameter("@AAP_AMC_VENDOR", SqlDbType.Int)
    '            param1(0).Value = ddlVendor.SelectedItem.Value
    '            param1(1) = New SqlParameter("@AAP_AMC_FROMDT", SqlDbType.DateTime)
    '            param1(1).Value = CDate(txtSDate.Text)

    '            param1(2) = New SqlParameter("@AAP_AMC_TODT", SqlDbType.DateTime)
    '            param1(2).Value = CDate(txtEDate.Text)

    '            param1(3) = New SqlParameter("@AAP_AMC_STA", SqlDbType.Int)
    '            param1(3).Value = 1
    '            param1(4) = New SqlParameter("@AAP_PVM_STA", SqlDbType.Int)
    '            param1(4).Value = 0
    '            param1(5) = New SqlParameter("@AAP_SNO", SqlDbType.VarChar, 50)
    '            param1(5).Value = lstRight.Items(icnt).Value
    '            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "AMC_UPDATE_AMC_DTLS", param1)


    '        Next


    '    End If




    '    i = 0
    '    icnt = 0
    '    lstRight.DataValueField = lstLeft.DataValueField
    '    For icnt = 0 To lstRight.Items.Count - 1
    '        Dim temp As String
    '        arparam = New SqlParameter(2) {}
    '        arparam(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
    '        arparam(0).Value = plan
    '        arparam(1) = New SqlParameter("@AAT_CODE", SqlDbType.NVarChar, 35)
    '        arparam(1).Value = lstRight.Items(icnt).Value
    '        arparam(2) = New SqlParameter("@AAT_NAME", SqlDbType.NVarChar, 100)
    '        arparam(2).Value = lstRight.Items(icnt).Text
    '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "INSRT_AMC_DTLS", arparam)
    '    Next

    '    If Mainflag = 2 Then
    '        lblmsg.Visible = True
    '        lblmsg.Text = "AMC Already Submitted !"
    '        Exit Sub
    '    Else
    '        lblmsg.Visible = False
    '        If Len(Trim(txtSDate.Text)) = 0 And Len(Trim(txtEDate.Text)) = 0 Then
    '            lblmsg.Visible = True
    '            lblmsg.Text = "Select StartDate And EndDate !"
    '            Exit Sub
    '        ElseIf Len(Trim(txtSDate.Text)) = 0 Then
    '            lblmsg.Visible = True
    '            lblmsg.Text = "Select Start Date !"
    '            Exit Sub
    '        ElseIf Len(Trim(txtEDate.Text)) = 0 Then
    '            lblmsg.Visible = True
    '            lblmsg.Text = "Select End Date !"
    '            Exit Sub
    '        End If

    '        If Checkflag = 2 Then
    '            lblmsg.Visible = True
    '            lblmsg.Text = "AMC Already Submited For This Dates."
    '            Exit Sub
    '        Else
    '            lblmsg.Visible = False
    '            Dim strAst As String = String.Empty
    '            strAst = Trim(ddlVendor.SelectedItem.Text)
    '            Response.Redirect("frmAMCfinalpage.aspx?rid=" & plan & "&asset=" & strAst & "&staid=submitted")
    '        End If
    '    End If
    'Else
    '    lblmsg.Visible = True
    '    lblmsg.Text = "Select At Least one Asset"
    'End If
    'End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        If ddlGroup.SelectedIndex <> 0 Then
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = ddlBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            ddlbrand.Items.Clear()

            ddlVendor.Items.Clear()

            ddlgrouptype.Items.Clear()
            ObjSubSonic.Binddropdown(ddlgrouptype, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
        Else
            cleardata()
        End If
        Panel2.Visible = False
        PanSelAssets.Visible = False
    End Sub

    Protected Sub ddlgrouptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlgrouptype.SelectedIndexChanged
        If ddlgrouptype.SelectedIndex <> 0 Then
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = ddlBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            ddlVendor.Items.Clear()

            ddlbrand.Items.Clear()
            ObjSubSonic.Binddropdown(ddlbrand, "MN_GET_ASSETBRAND_LOCGRUPTYPE", "BRAND_NAME", "BRAND_ID", param)
        Else
            ddlbrand.ClearSelection()
            ddlVendor.ClearSelection()
        End If
        Panel2.Visible = False
        PanSelAssets.Visible = False
    End Sub

    Protected Sub SendAMCMail()
        Dim arparam() As SqlParameter
        arparam = New SqlParameter(3) {}
        arparam(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
        arparam(0).Value = plan
        arparam(1) = New SqlParameter("@MAIL_ID", SqlDbType.NVarChar, 50)
        arparam(1).Value = 1
        arparam(2) = New SqlParameter("@VEN_MAIL", SqlDbType.Bit)
        arparam(2).Value = chkVenMail.Checked
        arparam(3) = New SqlParameter("@COMPANYID", SqlDbType.Int)
        arparam(3).Value = Session("COMPANYID")
        ObjSubSonic.GetSubSonicExecute("SEND_MAIL_MN_CREATE_AMC", arparam)
    End Sub

    Protected Sub ddlbrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlbrand.SelectedIndexChanged
        lblmsg.Visible = False
        If ddlbrand.SelectedIndex <> 0 Then
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = ddlBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            param(3) = New SqlParameter("@VEND_ID", SqlDbType.NVarChar, 200)
            param(3).Value = ddlbrand.SelectedItem.Value
            ddlVendor.Items.Clear()
            ObjSubSonic.Binddropdown(ddlVendor, "MN_GET_ASSET_VENDOR_BY_LOCGRUPBRND", "VENDOR_NAME", "VENDOR_ID", param)
            If ddlVendor.Items.Count = 0 Then
                lblmsg.Visible = True
                lblmsg.Text = "No vendors for selected Asset Group."

            End If
        Else
            ddlVendor.ClearSelection()
        End If
        Panel2.Visible = False
        PanSelAssets.Visible = False
    End Sub

    Protected Sub txtAmcYears_TextChanged(sender As Object, e As EventArgs)
        If txtAMCDate.Text <> "" Then
            Dim dt As DateTime = txtAMCDate.Text
            txtSDate.Text = dt.ToString("MM/dd/yyyy")
            Dim sdt As DateTime = txtSDate.Text
            Dim toDate As DateTime
            toDate = CDate(sdt.ToString("MM/dd/yyyy")).AddYears(txtAmcYears.Text)
            txtEDate.Text = toDate.ToString("MM/dd/yyyy")
        Else
            Dim dt As DateTime = txtSDate.Text
            txtSDate.Text = dt.ToString("MM/dd/yyyy")
            Dim sdt As DateTime = txtSDate.Text
            Dim toDate As DateTime
            toDate = CDate(sdt.ToString("MM/dd/yyyy")).AddYears(txtAmcYears.Text)
            txtEDate.Text = toDate.ToString("MM/dd/yyyy")
        End If
    End Sub

    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile Then
                lblmsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = Path.GetFileNameWithoutExtension(fpBrowseDoc.FileName) & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Server.MapPath("~\UploadFiles\"), "\", "\\") & filename
                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(filepath)
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblmsg.Visible = True
                    lblmsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""

                msheet = listSheet(0).ToString()
                mfilename = msheet

                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"

                End If
                Dim snocnt As Integer = 1

                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If

                If Trim(LCase(ds.Tables(0).Columns(0).ToString)) <> "location name" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 1: Column name should be Location Name"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(1).ToString)) <> "asset group" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 2: Column name should be Asset Group"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(2).ToString)) <> "asset group type" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 3: Column name should be Asset Group Type"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(3).ToString)) <> "asset brand" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 4: Column name should be Asset Brand"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(4).ToString)) <> "vendor" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 5: Column name should be Vendor"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(5).ToString)) <> "asset name" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 6: Column name should be Asset Name"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(6).ToString)) <> "contract no of years" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 7: Column name should be Contract No Of Years "
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(7).ToString)) <> "contract start date (mm/dd/yyyy)" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 8: Column name should be Contract Start Date (MM/DD/YYYY)"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(8).ToString)) <> "contract end date (mm/dd/yyyy)" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 9: Column name should be Contract End Date (MM/DD/YYYY)"
                    Exit Sub
                ElseIf Trim(LCase(ds.Tables(0).Columns(9).ToString)) <> "comprehensive" Then
                    lblmsg.Visible = True
                    lblmsg.Text = "At Column 10: Column name should be Comprehensive"
                    Exit Sub
                
                End If

                Dim remarks As String = ""

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "location name" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Location NAme is null or empty, "
                            End If
                        End If

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset group" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Asset Group is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset group type" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Asset Group Type is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset brand" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Asset Brand is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "vendor" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Vendor is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "asset name" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Asset Name is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "contract no of years" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                'name_cnt = 0
                                remarks = remarks + " Contract No Of Years is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "contract start date (mm/dd/yyyy)" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Contract Start Date is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "contract end date (mm/dd/yyyy)" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                remarks = remarks + " Contract End Date is null or empty, "
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "comprehensive" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then
                                'name_cnt = 0
                                remarks = remarks + " Comprehensive is null or empty, "
                            End If
                        End If
                    Next

                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPLOAD_AMC_DATA")
                    sp.Command.AddParameter("@LOC_NAME", ds.Tables(0).Rows(i).Item(0).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_GRUP", ds.Tables(0).Rows(i).Item(1).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_GRUP_TYPE", ds.Tables(0).Rows(i).Item(2).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_BRAND", ds.Tables(0).Rows(i).Item(3).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_VENDOR", ds.Tables(0).Rows(i).Item(4).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_NAME", ds.Tables(0).Rows(i).Item(5).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_CNTR_NO_YEARS", ds.Tables(0).Rows(i).Item(6).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_CNTR_ST_DT", ds.Tables(0).Rows(i).Item(7).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_CNTR_END_DT", ds.Tables(0).Rows(i).Item(8).ToString, DbType.String)
                    sp.Command.AddParameter("@AST_COMPREHENSIVE", ds.Tables(0).Rows(i).Item(9).ToString, DbType.String)
                    sp.Command.AddParameter("@UID", Session("Uid"), DbType.String)
                    sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
                    sp.Command.AddParameter("@REMARKS", remarks, DbType.String)
                    sp.ExecuteScalar()
                    remarks = ""
                Next
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "INSERT_AMC_DATA")
                sp1.Command.AddParameter("@dummy", 1, DbType.Int32)
                Dim ds1 As New DataSet
                ds1 = sp1.GetDataSet()
                If ds1.Tables(0).Rows.Count > 0 Then
                    GvUpload.DataSource = ds1.Tables(0)
                    GvUpload.DataBind()
                    btnExport.Visible = True
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    If ds1.Tables(0).Rows(i).Item("Remarks").ToString() <> "Success" Then
                        lblmsg.Visible = True
                        lblmsg.Text = "Please Check the Remarks and Re-Upload Correct data..."
                    Else
                        lblmsg.Visible = True
                        lblmsg.Text = "Data uploaded successfully..."
                    End If
                Next
                'lblmsg.Visible = True
                'lblmsg.Text = "Data uploaded successfully..."

            Else
                lblmsg.Text = "Please Select File"
            End If
        Catch ex As Exception
            CommonModules.PopUpMessage(ex.Message, Page)
        End Try
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Export("UPLOAD_AMC_DATA.xls", GvUpload)
    End Sub
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            row.Cells(0).Style.Add("mso-number-format", "\@")
            row.Cells(5).Style.Add("mso-number-format", "\@")
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        'style to format numbers to string
        Dim style As String = "<style>.textmode{mso-number-format:\@;}</style>"
        'HttpContext.Current.Response.Write(style)
        HttpContext.Current.Response.Write(sw.ToString)
        'HttpContext.Current.Response.Write(style + sw.ToString())
        HttpContext.Current.Response.End()
    End Sub
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

 
End Class
