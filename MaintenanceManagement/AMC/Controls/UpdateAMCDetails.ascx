<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UpdateAMCDetails.ascx.vb" Inherits="MaintenanceManagement_AMC_Controls_UpdateAMCDetails" %>

<script type="text/javascript">
    function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
        re = new RegExp(aspCheckBoxID)
        for (i = 0; i < form1.elements.length; i++) {
            elm = document.forms[0].elements[i]
            if (elm.type == 'checkbox') {
                if (re.test(elm.name)) {
                    if (elm.disabled == false)
                        elm.checked = checkVal
                }
            }
        }
    }

    var TotalChkBx;
    var Counter;
    window.onload = function () {
        //Get total no. of CheckBoxes in side the GridView.
        TotalChkBx = parseInt('<%= Me.gvItems.Rows.Count%>');
        //Get total no. of checked CheckBoxes in side the GridView.
        Counter = 0;
    }

    function ChildClick(CheckBox) {
        //Get target base & child control.
        var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
        var TargetChildControl = "chkSelect";
        //Get all the control of the type INPUT in the base control.
        var Inputs = TargetBaseControl.getElementsByTagName("input");
        // check to see if all other checkboxes are checked
        for (var n = 0; n < Inputs.length; ++n)
            if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                // Whoops, there is an unchecked checkbox, make sure
                // that the header checkbox is unchecked
                if (!Inputs[n].checked) {
                    Inputs[0].checked = false;
                    return;
                }
            }
        // If we reach here, ALL GridView checkboxes are checked
        Inputs[0].checked = true;
    }

         <%--check box validation--%>
    function validateCheckBoxesMyReq() {
        var gridView = document.getElementById("<%=gvItems.ClientID%>");
        var checkBoxes = gridView.getElementsByTagName("input");
        for (var i = 0; i < checkBoxes.length; i++) {
            if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                return true;
            }
        }
        alert("Please select atleast one checkbox");
        return false;
    }


</script>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 10px">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblmsg" ForeColor="Red" CssClass="col-md-12 control-label" runat="server"></asp:Label>
            </div>
        </div>
    </div>
</div>

<asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
<asp:ValidationSummary ID="ValidationSummary2" runat="server" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />

<div class="box-body">
    <div class="row" style="padding-top: 10px">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <asp:HyperLink ID="HyperLink1" runat="server" Text=" Click To Download Template" NavigateUrl="~/Masters/Mas_Webfiles/Create_Maintenance_Contract_Template.xlsx"></asp:HyperLink>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 17px">
            <div class="form-group">
                <asp:UpdatePanel runat="server" ID="UpdatePanel8" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="row">
                            <label class="col-md-4 control-label">Upload to Create Maint.Contract (Only Excel) <span style="color: red;">*</span></label>
                            <asp:RequiredFieldValidator ID="rfvpayment" runat="server" Display="None" ErrorMessage="Please Choose Excel File To Upload"
                                ControlToValidate="fpBrowseDoc" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revfubrowse" Display="None" ValidationGroup="Val1" runat="Server" ErrorMessage="Invalid Document ! Please Choose Excel File"
                                ControlToValidate="fpBrowseDoc" ValidationExpression="^.+\.(([xX][lL][sS])|([xX][lL][sS][xX]))$"> 
                            </asp:RegularExpressionValidator>
                            <div class="col-md-4">
                                <div class="btn-default">
                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                    <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
                                </div>
                            </div>
                            <div class="col-md-4 control-label">
                                <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-default btn-primary" Text="Upload" ValidationGroup="Val1"></asp:Button>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnUpload" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-right">
        <div class="form-group">
            <asp:Button ID="btnExport" runat="server" Text="Export To Excel" CssClass="btn btn-primary custom-button-color" />
        </div>
    </div>
</div>
<div class="row" style="padding-bottom: 17px">
    <div class="col-md-12">
        <div class="form-group">
            <asp:GridView ID="GvUpload" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped">
                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>

        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Location<span style="color: red;">*</span></label>
            <asp:TextBox ID="txtHidTodayDt" runat="server" Visible="False"></asp:TextBox>
            <asp:CompareValidator ID="CompareValidator9" runat="server" ControlToValidate="ddlBuilding" Display="None" ErrorMessage="Please Select Location" Operator="NotEqual" ValueToCompare="--Select--" ValidationGroup="Val2"></asp:CompareValidator>
            <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Group<span style="color: red;">*</span></label>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlGroup" Display="None" ErrorMessage="Please Select Asset Group" Operator="NotEqual" ValueToCompare="--Select--" ValidationGroup="Val2"></asp:CompareValidator>
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Group Type<span style="color: red;">*</span></label>
            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlgrouptype" Display="None" ErrorMessage="Please Select Asset Group Type" Operator="NotEqual" ValueToCompare="--Select--" ValidationGroup="Val2"></asp:CompareValidator>
            <asp:DropDownList ID="ddlgrouptype" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Brand<span style="color: red;">*</span></label>
            <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlbrand" Display="None" ErrorMessage="Please Select Asset Brand" Operator="NotEqual" ValueToCompare="--Select--" ValidationGroup="Val2"></asp:CompareValidator>
            <asp:DropDownList ID="ddlbrand" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>

</div>
<div class="row">

    <%-- <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Vendor<span style="color: red;">*</span></label>
            <asp:CompareValidator ID="Comparevalidator10" runat="server" ControlToValidate="ddlVendor" Display="None" ErrorMessage="Please Select Vendor" Operator="NotEqual" ValueToCompare="--Select--" ValidationGroup="Val2"></asp:CompareValidator>
            <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>--%>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Expiring On Or Before<span style="color: red;"></span></label>
            <%-- <asp:RequiredFieldValidator ID="ReqStartAMCDate" runat="server" ControlToValidate="txtAMCDate"
                Display="None" ErrorMessage="Please Select  Expiring On Or Before Date" SetFocusOnError="True" ValidationGroup="Val2"></asp:RequiredFieldValidator>--%>
            <div class='input-group date' id='Div1'>
                <asp:TextBox ID="txtAMCDate" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                <span class="input-group-addon">
                    <span class="fa fa-calendar" onclick="setup('Div1')"></span>
                </span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
        <div class="form-group">
            <asp:Button ID="btnGetData" runat="server" Class="btn btn-default btn-primary" Text="Search" ValidationGroup="Val2"></asp:Button>
        </div>
    </div>

</div>

<div id="PanSelAssets" runat="server">
    <div class="row">
        <div class="col-md-12">
            <asp:GridView ID="gvItems" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                EmptyDataText="No Assets Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                <Columns>
                    <asp:TemplateField HeaderText="Asset Id">
                        <ItemTemplate>
                            <asp:Label ID="lblAssetName" Text='<%#Eval("AssetName")%>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Asset Name">
                        <ItemTemplate>
                            <asp:Label ID="lbAAP_CODE" Text='<%#Eval("AAP_CODE") %>' runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lbldesc" Text='<%#Eval("AAT_DESC")%>' runat="server"></asp:Label>
                            <asp:Label ID="lblAAP_RUNNO" Text='<%#Eval("AAP_RUNNO") %>' runat="server" Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Warranty/Maint.Contract End Date" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblwarrantyDate" Text='<%#Eval("AAT_WRNTY_DATE") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Select">
                        <HeaderTemplate>
                            <input id="chkAll" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkSelect', this.checked)">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:ChildClick(this);" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="pagination-ys" />
            </asp:GridView>
        </div>
    </div>
</div>

<div id="Panel2" runat="server" visible="false" style="padding-top: 17px">

    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Vendor<span style="color: red;">*</span></label>
                <asp:CompareValidator ID="Comparevalidator10" runat="server" ControlToValidate="ddlVendor" Display="None" ErrorMessage="Please Select Vendor" Operator="NotEqual" ValueToCompare="--Select--" ValidationGroup="Val2"></asp:CompareValidator>
                <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>No.of Years<span style="color: red;">*</span></label>
                <asp:TextBox ID="txtAmcYears" runat="server" CssClass="form-control" AutoPostBack="true" onkeypress="return validateText(event)" OnTextChanged="txtAmcYears_TextChanged"></asp:TextBox>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Maint. Contract Start Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="ReqStartDate" runat="server" ControlToValidate="txtSDate"
                    Display="None" ErrorMessage="Please Select Maint. Contract Start Date" SetFocusOnError="True" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                <div class='input-group date' id='fromdate'>
                    <asp:TextBox ID="txtSDate" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                    <span class="input-group-addon">
                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                    </span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Maint. Contract End Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="ReqEndDate" runat="server" ControlToValidate="txtEDate"
                    Display="None" ErrorMessage="Please Select Maint. Contract End Date" SetFocusOnError="True" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                <div class='input-group date' id='todate'>
                    <asp:TextBox ID="txtEDate" runat="server" CssClass="form-control"></asp:TextBox>
                    <span class="input-group-addon">
                        <span class="fa fa-calendar" onclick="setup('todate')"></span>
                    </span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Type<span style="color: red;">*</span></label>
                <asp:CompareValidator ID="Comparevalidator1" runat="server" ControlToValidate="ddlType" Display="None" ErrorMessage="Please Select Type" Operator="NotEqual" ValueToCompare="--Select--" ValidationGroup="Val2"></asp:CompareValidator>
                <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                    <asp:ListItem>--Select--</asp:ListItem>
                    <asp:ListItem Value="1">Comprehensive</asp:ListItem>
                    <asp:ListItem Value="0">Non-Comprehensive</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Remarks<span style="color: red;"></span></label>

                <asp:RegularExpressionValidator ID="regXAC" runat="server" Visible="True" ErrorMessage="Special characters (',-,.,+,<,>) not allowed in Admin Comments!"
                    Display="None" ControlToValidate="txtComments" ValidationExpression="^[a-zA-Z0-9\s-()]+"></asp:RegularExpressionValidator>
                <asp:TextBox ID="txtComments" runat="server" CssClass="form-control" MaxLength="500" Height="30%" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Do you wish to send a mail to Vendor</label>
                <asp:CheckBox ID="chkVenMail" Checked="true" runat="server" />
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
            <div class="form-group">
                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-default btn-primary" Text="Submit" OnClientClick="javascript:return validateCheckBoxesMyReq();" ValidationGroup="Val2"></asp:Button>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    function validateText(evt) {
        if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
            alert("Enter Only Numbers");
            return false;
        }
    }
</script>
<script type="text/ecmascript">
    function refreshSelectpicker() {
        $("#<%=ddlbrand.ClientID%>").selectpicker();
        $("#<%=ddlBuilding.ClientID%>").selectpicker();
        $("#<%=ddlGroup.ClientID%>").selectpicker();
        $("#<%=ddlgrouptype.ClientID%>").selectpicker();
        $("#<%=ddlType.ClientID%>").selectpicker();
        $("#<%=ddlVendor.ClientID%>").selectpicker();
    };
    refreshSelectpicker();
</script>

