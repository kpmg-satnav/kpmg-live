Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_AMC_Controls_AMC_Work_Order
    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim cmpny As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        'Put user code to initialize the page here
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        'RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        txtDate.Attributes.Add("onClick", "displayDatePicker('" + txtDate.ClientID + "')")
        txtDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        Dim uid As String = Session("uid")
        cmpny = Session("COMPANYID")
        txtDate.Attributes.Add("readonly", "readonly")
        If Not IsPostBack = True Then
            txtTax.Text = 0
            txtQuarter.Text = 0
            lblMsg.Visible = False
            Panel1.Visible = False
            DrpDwnAMCId.Items.Clear()
            BindBuilding()
        End If
       
    End Sub

    Private Sub BindBuilding()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID")
        ObjSubSonic.Binddropdown(cboBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub clearitems()
        cboVendor.Items.Clear()
        ddlGroup.Items.Clear()
        ddlgrouptype.Items.Clear()
        ddlbrand.Items.Clear()
        DrpDwnAMCId.Items.Clear()
        lstasset.Items.Clear()
        txtAMCSDate.Text = ""
        txtAMCEDate.Text = ""
        txtDate.Text = ""
        Panel1.Visible = False
    End Sub

    Public Sub clearitems2()

        cboVendor.Items.Clear()
        ddlgrouptype.Items.Clear()
        ddlbrand.Items.Clear()
        DrpDwnAMCId.Items.Clear()
        lstasset.Items.Clear()
        txtAMCSDate.Text = ""
        txtAMCEDate.Text = ""
        txtDate.Text = ""
        Panel1.Visible = False


    End Sub

    Public Sub clearitems3()

        cboVendor.Items.Clear()
        ddlbrand.Items.Clear()
        DrpDwnAMCId.Items.Clear()
        lstasset.Items.Clear()
        txtAMCSDate.Text = ""
        txtAMCEDate.Text = ""
        txtDate.Text = ""
        Panel1.Visible = False

    End Sub

    Public Sub clearitems4()

        cboVendor.Items.Clear()
        DrpDwnAMCId.Items.Clear()
        lstasset.Items.Clear()
        txtAMCSDate.Text = ""
        txtAMCEDate.Text = ""
        txtDate.Text = ""
        Panel1.Visible = False

    End Sub

    Public Sub clearitems5()

        DrpDwnAMCId.Items.Clear()
        lstasset.Items.Clear()
        txtAMCSDate.Text = ""
        txtAMCEDate.Text = ""
        txtDate.Text = ""
        Panel1.Visible = False

    End Sub

    Private Sub getPlanDetails()

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 200)
        param(0).Value = DrpDwnAMCId.SelectedItem.Value
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("AMC_GET_AMC_VENDOR", param)
        If ds.Tables(0).Rows.Count > 0 Then
            txtConName.Text = ds.Tables(0).Rows(0).Item("AVR_NAME").ToString
            txtConAdd.Text = ds.Tables(0).Rows(0).Item("AVR_ADDR").ToString
            txtConPh.Text = ds.Tables(0).Rows(0).Item("AVR_MOBILE_PHNO").ToString
            txtConPer.Text = ds.Tables(0).Rows(0).Item("AVR_EMAIL").ToString
            txtAMCSDate.Text = ds.Tables(0).Rows(0).Item("AMN_FROM_DATE")
            txtAMCEDate.Text = ds.Tables(0).Rows(0).Item("AMN_TO_DATE")
            Panel1.Visible = True
        Else
            Panel1.Visible = False
            Dim flag As Integer
            If flag = 0 Then
                TxtAssetCtr.Text = 0
                Panel1.Visible = False
                lblAssets.Text = "No Assets For This Requisition"
                Exit Sub
            Else
                lblAssets.Text = ""
                Panel1.Visible = True
            End If
        End If

    End Sub


    Protected Sub cboBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBuilding.SelectedIndexChanged

        If cboBuilding.SelectedItem.Value <> "--Select--" Then
            If cboBuilding.SelectedItem.Value <> "--All--" Then
                lblMsg.Text = ""
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
                param(0).Value = cboBuilding.SelectedItem.Value
                clearitems()
                ObjSubSonic.Binddropdown(ddlGroup, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)

            End If
        Else
            clearitems()
            lblMsg.Visible = False
        End If

    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged

        If ddlGroup.SelectedIndex <> 0 Then
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            lblMsg.Visible = False
            clearitems2()
            ObjSubSonic.Binddropdown(ddlgrouptype, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
        Else
            clearitems2()
        End If

    End Sub

    Protected Sub ddlgrouptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlgrouptype.SelectedIndexChanged

        If ddlgrouptype.SelectedIndex <> 0 Then
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            lblMsg.Visible = False
            clearitems3()
            ObjSubSonic.Binddropdown(ddlbrand, "MN_GET_ASSETBRAND_LOCGRUPTYPE", "BRAND_NAME", "BRAND_ID", param)
        Else
            clearitems3()
        End If

    End Sub

    Protected Sub ddlbrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlbrand.SelectedIndexChanged

        If ddlbrand.SelectedIndex <> 0 Then
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            param(3) = New SqlParameter("@VEND_ID", SqlDbType.NVarChar, 200)
            param(3).Value = ddlbrand.SelectedItem.Value
            lblMsg.Visible = False
            clearitems4()
            ObjSubSonic.Binddropdown(cboVendor, "MN_GET_ASSET_VENDOR_BY_LOCGRUPBRND", "VENDOR_NAME", "VENDOR_ID", param)
        Else
            clearitems4()

        End If

    End Sub

    Protected Sub cboVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboVendor.SelectedIndexChanged

        If cboVendor.SelectedIndex <> 0 Then
            clearitems5()
            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@AMN_BDG_ID", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@AMN_CTM_ID", SqlDbType.NVarChar, 200)
            param(1).Value = cboVendor.SelectedItem.Value
            param(2) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlGroup.SelectedItem.Value
            param(3) = New SqlParameter("@GRPTYPEID", SqlDbType.NVarChar, 200)
            param(3).Value = ddlgrouptype.SelectedItem.Value
            param(4) = New SqlParameter("@BRAND", SqlDbType.NVarChar, 200)
            param(4).Value = ddlbrand.SelectedItem.Value
            param(5) = New SqlParameter("@CMPNY", SqlDbType.Int)
            param(5).Value = cmpny
            ObjSubSonic.Binddropdown(DrpDwnAMCId, "GET_AMC_REQ_DETAILS_BYGROUPBRAND", "AMN_PLAN_ID", "AMN_PLAN_ID", param)
            If DrpDwnAMCId.Items.Count = 0 Then

                lblMsg.Text = "Please Create/Update AMC And Then Proceed..."
                lblMsg.Visible = True
                DrpDwnAMCId.Items.Insert(0, "--Select--")
            Else
                lblMsg.Visible = False
            End If
        Else
            clearitems5()

        End If

    End Sub

    Protected Sub DrpDwnAMCId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DrpDwnAMCId.SelectedIndexChanged

        If DrpDwnAMCId.SelectedItem.Value <> "--Select--" Then
            Dim param1(0) As SqlParameter
            param1(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 200)
            param1(0).Value = DrpDwnAMCId.SelectedItem.Value
            ObjSubSonic.BindListBox(lstasset, "GET_AMC_DETAILS", "ASSETNAME", "AMN_PLAN_ID", param1)
            getPlanDetails()
        Else
            lstasset.Items.Clear()
            txtAMCSDate.Text = ""
            txtAMCEDate.Text = ""
            txtDate.Text = ""
            Panel1.Visible = False
        End If

    End Sub

    Protected Sub btnCalc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalc.Click

        If IsNumeric(txtTax.Text) = False Or IsNumeric(txtGrandTot.Text) = False Then
            Response.Write("<script language=JavaScript>alert('Enter Only Numeric Values in Grand Total And Tax!!')</script>")
            Exit Sub
        End If
        If txtTax.Text = "" Or txtGrandTot.Text = "" Then
            txtTax.Text = 0
            txtGrandTot.Text = 0

        End If
        TxtTotCost.Text = CDbl(txtTax.Text) + CDbl(txtGrandTot.Text)
        getPlanDetails()

    End Sub

    Protected Sub txtGrandTot_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGrandTot.TextChanged

        'If IsNumeric(txtTax.Text) = False Or IsNumeric(txtGrandTot.Text) = False Then
        '    Response.Write("<script language=JavaScript>alert('Enter Only Numeric Values in Grand Total And Tax!!')</script>")
        '    Exit Sub
        'End If
        If txtTax.Text = "" Or txtGrandTot.Text = "" Then
            txtTax.Text = 0
            txtGrandTot.Text = 0
        End If
        TxtTotCost.Text = CDbl(txtTax.Text) + CDbl(txtGrandTot.Text)
        getPlanDetails()

    End Sub

    Protected Sub txtTax_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTax.TextChanged

        'If IsNumeric(txtTax.Text) = False Or IsNumeric(txtGrandTot.Text) = False Then
        '    Response.Write("<script language=JavaScript>alert('Enter Only Numeric Values in Grand Total And Tax!')</script>")
        '    Exit Sub
        'End If
        If txtTax.Text = "" Or txtGrandTot.Text = "" Then
            txtTax.Text = 0
            txtGrandTot.Text = 0
        End If
        TxtTotCost.Text = CDbl(txtTax.Text) + CDbl(txtGrandTot.Text)
        getPlanDetails()

    End Sub

    Protected Sub btnSub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSub.Click

        Dim AMN_TYPE_ID
        Dim Mainflag
        Dim varTot As Double
        If txtGrandTot.Text < 1 Then
            lblMsg.Visible = True
            lblMsg.Text = "Total Value Should Be Greater Than 0..."
            Exit Sub
        Else
            lblMsg.Visible = False
        End If
        Mainflag = 1
        If txtDate.Text = String.Empty Then
            lblerror.Visible = True
            lblerror.Text = "Please Enter Start Date"
            Exit Sub
        End If
        If CDate(txtDate.Text) >= CDate(txtAMCSDate.Text) And CDate(txtDate.Text) < CDate(txtAMCEDate.Text) Then
            lblerror.Visible = False
        Else
            lblerror.Visible = True
            lblerror.Text = "The Start Date Should Be Between AMC Start Date and AMC End Date..."
            Exit Sub
        End If

       
       

        varTot = (CInt(txtMonthly.Text) + CInt(txtQuarter.Text) + CInt(txtHalfYear.Text) + CInt(txtAnnual.Text) + CInt(TxtOthers.Text)) - CInt(txtAdvance.Text)
        TxtTotCost.Text = CDbl(txtTax.Text) + CDbl(txtGrandTot.Text)
        If varTot > CInt(TxtTotCost.Text) Then
            lblAssets.Text = "Total Sum Exceeds AMC Cost..."
            lblAssets.Visible = True
            Exit Sub
        Else
            lblAssets.Visible = False
        End If
        Dim par(0) As SqlParameter
        par(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
        par(0).Value = DrpDwnAMCId.SelectedItem.Value
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("CHK_WORKORDER", par)
        If ds.Tables(0).Rows.Count > 0 Then
            Mainflag = 2
        End If
        If Mainflag = 2 Then
            lblMsg.Visible = True
            lblMsg.Text = "Work Order Already Created For Selected AMC"
        Else
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
            param(0).Value = DrpDwnAMCId.SelectedItem.Value
            Dim ds1 As New DataSet
            ds1 = ObjSubSonic.GetSubSonicDataSet("GET_AMC_MAIN", param)
            If ds.Tables(0).Rows.Count > 0 Then
                AMN_TYPE_ID = ds1.Tables(0).Rows(0).Item("AMN_TYPE_ID")
            End If

            Dim cost
            cost = CInt(TxtTotCost.Text)

            '-------------REPLACED by sp------------
            param = New SqlParameter(18) {}
            param(0) = New SqlParameter("@AWO_MAMPLAN_ID", SqlDbType.NVarChar, 50)
            param(0).Value = DrpDwnAMCId.SelectedItem.Value
            param(1) = New SqlParameter("@AWO_DATE ", SqlDbType.DateTime)
            param(1).Value = Sanitize.SanitizeInput(txtDate.Text)
            param(2) = New SqlParameter("@AWO_MAMTYPE_ID", SqlDbType.VarChar, 100)
            param(2).Value = "Asset"
            param(3) = New SqlParameter("@AWO_AMC_COST", SqlDbType.Int)
            param(3).Value = cost
            param(4) = New SqlParameter("@AWO_ADVANCE ", SqlDbType.Int)
            param(4).Value = Replace(Trim(txtAdvance.Text), "'", "''")
            param(5) = New SqlParameter("@AWO_MONTHLY", SqlDbType.Int)
            param(5).Value = Replace(Trim(txtMonthly.Text), "'", "''")
            param(6) = New SqlParameter("@AWO_QUARTERLY", SqlDbType.Int)
            param(6).Value = Replace(Trim(txtQuarter.Text), "'", "''")
            param(7) = New SqlParameter("@AWO_HALFYEAR", SqlDbType.Int)
            param(7).Value = Replace(Trim(txtHalfYear.Text), "'", "''")
            param(8) = New SqlParameter("@AWO_ANNUAL", SqlDbType.Int)
            param(8).Value = Replace(Trim(txtAnnual.Text), "'", "''")
            param(9) = New SqlParameter("@AWO_OTHERS", SqlDbType.Int)
            param(9).Value = Replace(Trim(TxtOthers.Text), "'", "''")
            param(10) = New SqlParameter("@AWO_UPT_BY", SqlDbType.NVarChar, 50)
            param(10).Value = Session("uid")
            param(11) = New SqlParameter("@AWO_UPT_ON", SqlDbType.DateTime)
            param(11).Value = getoffsetdatetime(DateTime.Now)
            param(12) = New SqlParameter("@AWO_STA_ID", SqlDbType.Int, 4)
            param(12).Value = 1
            param(13) = New SqlParameter("@AWO_REMARKS", SqlDbType.NVarChar)
            param(13).Value = txtRemarks.Text
            param(14) = New SqlParameter("@COMAPNYID", SqlDbType.Int)
            param(14).Value = Session("COMPANYID")
            param(15) = New SqlParameter("@TAX", SqlDbType.Int)
            param(15).Value = txtTax.Text
            param(16) = New SqlParameter("@TOTAL", SqlDbType.Int)
            param(16).Value = txtGrandTot.Text
            param(17) = New SqlParameter("@AWO_TERMS_COND", SqlDbType.NVarChar)
            param(17).Value = txtTerms.Text
            param(18) = New SqlParameter("@AWO_MAIL", SqlDbType.Bit)
            param(18).Value = chkmail.Checked

            ObjSubSonic.GetSubSonicExecute("AMC_INSRT_AMC_WO", param)
            Dim strAst As String = String.Empty
            strAst = Trim(cboVendor.SelectedItem.Value)
            Response.Redirect("frmAMCfinalpage.aspx?rid=" & DrpDwnAMCId.SelectedItem.Value & "&staid=WorkOrder&asset=" & strAst)
        End If

    End Sub

End Class
