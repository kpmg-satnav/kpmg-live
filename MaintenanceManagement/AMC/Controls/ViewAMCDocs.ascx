<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewAMCDocs.ascx.vb" Inherits="MaintenanceManagement_AMC_Controls_ViewAMCDocs" %>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12" style="padding-bottom: 17px">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label" Visible="False">No Documents Available</asp:Label>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Location<span style="color: red;">*</span></label>
            <asp:CompareValidator ID="CompareValidator9" runat="server" ErrorMessage="Please Select Location"
                Display="None" ControlToValidate="cboBuilding" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
            <asp:DropDownList ID="cboBuilding" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Group<span style="color: red;">*</span></label>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlGroup" Display="None" ErrorMessage="Please Select Asset Group" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Group Type<span style="color: red;">*</span></label>
            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlgrouptype" Display="None" ErrorMessage="Please Select Asset Group Type" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
            <asp:DropDownList ID="ddlgrouptype" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Asset Brand<span style="color: red;">*</span></label>
            <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlbrand" Display="None" ErrorMessage="Please Select Asset Brand" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
            <asp:DropDownList ID="ddlbrand" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Select Vendor<span style="color: red;">*</span></label>
            <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="ddlbrand" Display="None" ErrorMessage="Please Select Vendor" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
            <asp:DropDownList ID="cmbVen" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Agreement Number<span style="color: red;">*</span></label>
            <asp:DropDownList ID="cmbAgreement" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Assets<span style="color: red;">*</span></label>
            <asp:ListBox ID="lstasset" runat="server" CssClass="form-control"></asp:ListBox>
        </div>
    </div>
</div>

<div id="tblDocs" runat="server">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <div class="row">
                    <strong>DOCUMENTS</strong>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <asp:DataGrid ID="grdDocs" runat="server" CssClass="table table-condensed table-bordered table-hover table-striped" EmptyDataText="No Maint. Contract Document Found."
                PageSize="15" AutoGenerateColumns="False" DataKeyField="AMD_ID">
                <Columns>
                    <asp:BoundColumn Visible="False" DataField="AMD_ID" HeaderText="ID"></asp:BoundColumn>
                    <asp:BoundColumn DataField="AMD_DOC_TYPE" HeaderText="Document Type">
                        <HeaderStyle></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="AMD_DOC_STATUS" HeaderText="Status">
                        <HeaderStyle></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="AMD_DOC_RECDT" HeaderText="Document Date">
                        <HeaderStyle></HeaderStyle>
                    </asp:BoundColumn>
                     <asp:BoundColumn DataField="AMD_REM" HeaderText="Remarks">
                        <HeaderStyle></HeaderStyle>
                           </asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="File Name">
                        <HeaderStyle></HeaderStyle>
                        <ItemTemplate>
                            <a href='' id="hrfDoc" runat="server" target="_blank">AtRunTime</a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <HeaderStyle ForeColor="white" BackColor="Black" />
                <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
            </asp:DataGrid>
        </div>
    </div>

</div>
<script  type="text/ecmascript">
    function refreshSelectpicker() {
        $("#<%=ddlbrand.ClientID%>").selectpicker();
        $("#<%=ddlGroup.ClientID%>").selectpicker();
        $("#<%=ddlgrouptype.ClientID%>").selectpicker();
        $("#<%=cmbVen.ClientID%>").selectpicker();
        $("#<%=cboBuilding.ClientID%>").selectpicker();
        $("#<%=cmbAgreement.ClientID%>").selectpicker();
       
    }; 
    refreshSelectpicker();
</script>