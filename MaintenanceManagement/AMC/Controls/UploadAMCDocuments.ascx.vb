Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_AMC_Controls_UploadAMCDocuments
    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Public param() As SqlParameter

    Public Sub clearitems()
        ddlGroup.Items.Clear()
        ddlgrouptype.Items.Clear()
        ddlbrand.Items.Clear()
        cmbVen.Items.Clear()
        DrpDwnAMCId.Items.Clear()
        lstasset.Items.Clear()
        txttype.Text = ""
        cmbstatus.SelectedIndex = 0
        txtDate.Text = ""
        fu1.Attributes.Clear()
        lblMsg.Visible = False
    End Sub

    Public Sub clearitems1()
        ddlGroup.Items.Clear()
        ddlgrouptype.Items.Clear()
        ddlbrand.Items.Clear()
        cmbVen.Items.Clear()
        DrpDwnAMCId.Items.Clear()
        lstasset.Items.Clear()
        txttype.Text = ""
        cmbstatus.SelectedIndex = 0
        txtDate.Text = ""
        fu1.Attributes.Clear()
        lblMsg.Visible = False
    End Sub

    Public Sub clearitems2()
        ddlbrand.Items.Clear()
        cmbVen.Items.Clear()
        DrpDwnAMCId.Items.Clear()
        lstasset.Items.Clear()
        txttype.Text = ""
        cmbstatus.SelectedIndex = 0
        txtDate.Text = ""
        fu1.Attributes.Clear()
        lblMsg.Visible = False
    End Sub

    Public Sub clearitems3()
        cmbVen.Items.Clear()
        DrpDwnAMCId.Items.Clear()
        lstasset.Items.Clear()
        txttype.Text = ""
        cmbstatus.SelectedIndex = 0
        txtDate.Text = ""
        fu1.Attributes.Clear()
        lblMsg.Visible = False
    End Sub

    Public Sub clearitems4()
        DrpDwnAMCId.Items.Clear()
        lstasset.Items.Clear()
        txttype.Text = ""
        cmbstatus.SelectedIndex = 0
        txtDate.Text = ""
        fu1.Attributes.Clear()
        lblMsg.Visible = False
    End Sub

    Public Sub clearitems5()
        lstasset.Items.Clear()
        txttype.Text = ""
        cmbstatus.SelectedIndex = 0
        txtDate.Text = ""
        fu1.Attributes.Clear()
        lblMsg.Visible = False
    End Sub

    Private Sub ClearControls()
        lblMsg.Text = ""
        txtDate.Text = ""
        txttype.Text = ""
        cmbstatus.SelectedIndex = 0
        If txtEditDocNo.Text.Trim <> "" Then grdDocs.Items(txtIndex.Text).BackColor = Drawing.Color.LightSteelBlue
        tblUploadFile.Visible = False
    End Sub

    Private Sub BindBuilding()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID").ToString
        ObjSubSonic.Binddropdown(cboBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindDocuments(ByVal Agrno As String)
        Dim dtDocs As New DataTable("Documents")
        param = New SqlParameter(1) {}
        param(0) = New SqlParameter("@AMD_AGR_NO", SqlDbType.NVarChar, 50)
        param(0).Value = DrpDwnAMCId.SelectedItem.Text
        param(1) = New SqlParameter("@COMPANYID", SqlDbType.Int)
        param(1).Value = Session("COMPANYID")
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("AMC_GET_DOCS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            'tblGridDocs.Visible = False
            'ObjSubSonic.BindDataGrid(grdDocs, "AMC_GET_DOCS", param)
            grdDocs.DataSource = ds
            grdDocs.DataBind()
            tblGridDocs.Visible = True
            lblMsg.Text = ""
        Else
            tblGridDocs.Visible = False
            lblMsg.Visible = True
            lblMsg.Text = "No Documents Available"
        End If
        dtDocs = Nothing
    End Sub

    Private Sub UploadDocs()
        lblErrorlink.Text = ""
        If cmbstatus.SelectedItem.Text = "Received" Then
            If fu1.PostedFile IsNot Nothing Then
                Dim intsize As Long = CInt(fu1.PostedFile.ContentLength)
                Dim strFileName As String
                Dim strFileExt As String
                If intsize <= 20971520 Then
                    strFileName = System.IO.Path.GetFileName(fu1.PostedFile.FileName)
                    If strFileName = "" Then
                        lblErrorlink.Text = "Please choose file"
                    Else
                        strFileExt = System.IO.Path.GetExtension(fu1.PostedFile.FileName)
                        Dim strpath As String = Request.PhysicalApplicationPath.ToString() & "MaintenanceManagement\AMC\DOCUMENTS\" & strFileName
                        fu1.PostedFile.SaveAs(strpath)
                        param = New SqlParameter(6) {}
                        param(0) = New SqlParameter("@AMD_AGR_NO", SqlDbType.NVarChar, 500)
                        param(0).Value = DrpDwnAMCId.SelectedItem.Text
                        param(1) = New SqlParameter("@AMD_DOC_TYPE", SqlDbType.NVarChar, 50)
                        param(1).Value = txttype.Text
                        param(2) = New SqlParameter("@AMD_DOC_STATUS", SqlDbType.NVarChar, 50)
                        param(2).Value = cmbstatus.SelectedItem.Text
                        param(3) = New SqlParameter("@AMD_DOC_RECDT", SqlDbType.NVarChar, 50)
                        param(3).Value = txtDate.Text
                        'param(4) = New SqlParameter("@AMD_STA_ID", SqlDbType.NVarChar, 50)
                        'param(4).Value = 1
                        param(4) = New SqlParameter("@AMD_UPD_BY", SqlDbType.NVarChar, 50)
                        param(4).Value = Session("uid")
                        param(5) = New SqlParameter("@AMD_UPD_DT", SqlDbType.NVarChar, 50)
                        param(5).Value = getoffsetdate(Date.Today) 'Now.Date
                        param(6) = New SqlParameter("@COMPANYID", SqlDbType.Int)
                        param(6).Value = Session("COMPANYID")
                        ObjSubSonic.GetSubSonicExecute("AMC_INSRT_DOCS", param)

                        Dim iDocNo As Integer
                        param = New SqlParameter(0) {}
                        param(0) = New SqlParameter("@AMD_AGR_NO", SqlDbType.NVarChar, 200)
                        param(0).Value = DrpDwnAMCId.SelectedItem.Text
                        Dim ds As New DataSet
                        ds = ObjSubSonic.GetSubSonicDataSet("GET_DOC_ID", param)
                        If ds.Tables(0).Rows.Count > 0 Then
                            iDocNo = ds.Tables(0).Rows(0).Item("AMD_ID")
                        End If
                        Dim fileName As String = "DocU" & iDocNo & strFileExt
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "MaintenanceManagement\AMC\Documents\" & fileName
                        fu1.PostedFile.SaveAs(filePath)
                        param = New SqlParameter(3) {}
                        param(0) = New SqlParameter("@AMD_DOC_FILE", SqlDbType.NVarChar, 500)
                        param(0).Value = fileName
                        param(1) = New SqlParameter("@AMD_DOC_LINK", SqlDbType.NVarChar, 500)
                        param(1).Value = "..\Documents\" & fileName
                        param(2) = New SqlParameter("@AMD_ID", SqlDbType.NVarChar, 200)
                        param(2).Value = iDocNo
                        param(3) = New SqlParameter("@COMPANYID", SqlDbType.Int)
                        param(3).Value = Session("COMPANYID")
                        ObjSubSonic.GetSubSonicExecute("AMC_UPDATE_DOCS", param)
                    End If
                Else
                    Response.Write("<script>")
                    Response.Write("alert(""" & "Upload File size should not be more than 20MB" & """)")
                    Response.Write("</script>")
                End If
            Else
                lblErrorlink.Text = "Please choose file"
            End If
        ElseIf cmbstatus.SelectedItem.Text = "Receivable" Then
            param = New SqlParameter(6) {}
            param(0) = New SqlParameter("@AMD_AGR_NO", SqlDbType.NVarChar, 500)
            param(0).Value = DrpDwnAMCId.SelectedItem.Text
            param(1) = New SqlParameter("@AMD_DOC_TYPE", SqlDbType.NVarChar, 50)
            param(1).Value = txttype.Text
            param(2) = New SqlParameter("@AMD_DOC_STATUS", SqlDbType.NVarChar, 50)
            param(2).Value = cmbstatus.SelectedItem.Text
            param(3) = New SqlParameter("@AMD_DOC_RECDT", SqlDbType.NVarChar, 50)
            param(3).Value = txtDate.Text
            'param(4) = New SqlParameter("@AMD_STA_ID", SqlDbType.NVarChar, 50)
            'param(4).Value = 1
            param(4) = New SqlParameter("@AMD_UPD_BY", SqlDbType.NVarChar, 50)
            param(4).Value = Session("uid")
            param(5) = New SqlParameter("@AMD_UPD_DT", SqlDbType.NVarChar, 50)
            param(5).Value = getoffsetdate(Date.Today) 'Now.Date
            param(6) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(6).Value = Session("COMPANYID")
            ObjSubSonic.GetSubSonicExecute("AMC_INSRT_DOCS", param)
            lblMsg.Text = "UpLoaded Successfully..."

            Dim iDocNo As Integer
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@AMD_AGR_NO", SqlDbType.NVarChar, 500)
            param(0).Value = DrpDwnAMCId.SelectedItem.Text
            Dim ds As New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("GET_DOC_ID", param)
            If ds.Tables(0).Rows.Count > 0 Then
                iDocNo = ds.Tables(0).Rows(0).Item("AMD_ID")
            End If
            param = New SqlParameter(3) {}
            param(0) = New SqlParameter("@AMD_DOC_FILE", SqlDbType.NVarChar, 500)
            param(0).Value = "NA"
            param(1) = New SqlParameter("@AMD_DOC_LINK", SqlDbType.NVarChar, 500)
            param(1).Value = "NA"
            param(2) = New SqlParameter("@AMD_ID", SqlDbType.NVarChar, 200)
            param(2).Value = iDocNo
            param(3) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(3).Value = Session("COMPANYID")
            ObjSubSonic.GetSubSonicExecute("AMC_UPDATE_DOCS", param)

        End If
    End Sub

    Private Sub UpdateDoc()
        lblErrorlink.Text = ""
        dtProcess = New DataTable
        If cmbstatus.SelectedItem.Text = "Received" Then
            'If Not fu1.PostedFile Is Nothing Then
            If fu1.PostedFile IsNot Nothing Then
                Dim strFileName As String = System.IO.Path.GetFileName(fu1.PostedFile.FileName)
                ' Dim strFileExt As String = UCase(Right(strFileName, 4))
                Dim strFileExt = System.IO.Path.GetExtension(fu1.PostedFile.FileName)
                Dim fileName As String = "DocU" & txtEditDocNo.Text & strFileExt
                If strFileName = "" Then
                    lblErrorlink.Text = "Please choose file"
                Else
                    Dim strpath As String = Request.PhysicalApplicationPath.ToString() & "MaintenanceManagement\amc\DOCUMENTS\" & strFileName
                    fu1.PostedFile.SaveAs(strpath)
                    'replaced by sp
                    param = New SqlParameter(8) {}
                    param(0) = New SqlParameter("@AMD_DOC_TYPE", SqlDbType.NVarChar, 50)
                    param(0).Value = txttype.Text
                    param(1) = New SqlParameter("@AMD_DOC_STATUS", SqlDbType.NVarChar, 50)
                    param(1).Value = cmbstatus.SelectedItem.Text
                    param(2) = New SqlParameter("@AMD_DOC_FILE", SqlDbType.NVarChar, 500)
                    param(2).Value = fileName
                    param(3) = New SqlParameter("@AMD_DOC_RECDT", SqlDbType.NVarChar, 50)
                    param(3).Value = txtDate.Text
                    param(4) = New SqlParameter("@AMD_DOC_LINK", SqlDbType.NVarChar, 500)
                    param(4).Value = "..\Documents\" & fileName
                    param(5) = New SqlParameter("@AMD_UPD_BY", SqlDbType.NVarChar, 50)
                    param(5).Value = Session("uid")
                    param(6) = New SqlParameter("@AMD_UPD_DT", SqlDbType.NVarChar, 50)
                    param(6).Value = getoffsetdate(Date.Today) 'Now.Date
                    param(7) = New SqlParameter("@AMD_ID", SqlDbType.Int)
                    param(7).Value = txtEditDocNo.Text
                    param(8) = New SqlParameter("@COMPANYID", SqlDbType.Int)
                    param(8).Value = Session("COMPANYID")
                    ObjSubSonic.GetSubSonicExecute("AMC_UPDATE_MODIFYDOCS", param)
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "MaintenanceManagement\AMC\Documents\" & fileName
                    fu1.PostedFile.SaveAs(filePath)
                    grdDocs.Items(txtIndex.Text).BackColor = Drawing.Color.Empty
                End If
            Else
                lblErrorlink.Text = "Select Uploadable file by clicking the Browse button"
            End If
        ElseIf cmbstatus.SelectedItem.Text = "Receivable" Then
            param = New SqlParameter(8) {}
            param(0) = New SqlParameter("@AMD_DOC_TYPE", SqlDbType.NVarChar, 50)
            param(0).Value = txttype.Text
            param(1) = New SqlParameter("@AMD_DOC_STATUS", SqlDbType.NVarChar, 50)
            param(1).Value = cmbstatus.SelectedItem.Text
            param(2) = New SqlParameter("@AMD_DOC_FILE", SqlDbType.NVarChar, 500)
            param(2).Value = "NA"
            param(3) = New SqlParameter("@AMD_DOC_RECDT", SqlDbType.NVarChar, 50)
            param(3).Value = txtDate.Text
            param(4) = New SqlParameter("@AMD_DOC_LINK", SqlDbType.NVarChar, 500)
            param(4).Value = "NA"
            param(5) = New SqlParameter("@AMD_UPD_BY", SqlDbType.NVarChar, 50)
            param(5).Value = Session("uid")
            param(6) = New SqlParameter("@AMD_UPD_DT", SqlDbType.NVarChar, 50)
            param(6).Value = getoffsetdate(Date.Today) 'Now.Date
            param(7) = New SqlParameter("@AMD_ID", SqlDbType.Int)
            param(7).Value = txtEditDocNo.Text
            param(8) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(8).Value = Session("COMPANYID")
            ObjSubSonic.GetSubSonicExecute("AMC_UPDATE_MODIFYDOCS", param)
            grdDocs.Items(txtIndex.Text).BackColor = Drawing.Color.Empty
        End If
        dtProcess = Nothing
        txtEditDocNo.Text = ""
        cmdsavedoc.Text = "Upload"
        btnCancel.Visible = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If
        Dim uid
        uid = Session("uid")
        Try
            txtDate.Attributes.Add("readonly", "readonly")
            If Not IsPostBack = True Then
                BindBuilding()
                lblMsg.Visible = False
                'tblPremise.Visible = True
                tblGridDocs.Visible = False
            End If
        Catch ex As Exception

        End Try
        'ddlGroup.Items.Insert(0, "--Select--")
        'ddlgrouptype.Items.Insert(0, "--Select--")
        'ddlbrand.Items.Insert(0, "--Select--")
        'cmbVen.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub cboBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBuilding.SelectedIndexChanged
        If cboBuilding.SelectedItem.Value <> "--Select--" Then
            If cboBuilding.SelectedItem.Value <> "--All--" Then
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
                param(0).Value = cboBuilding.SelectedItem.Value
                clearitems()
                ObjSubSonic.Binddropdown(ddlGroup, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)
            End If
        Else
            clearitems()
        End If
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        ddlgrouptype.Items.Clear()
        ddlbrand.Items.Clear()

        If ddlGroup.SelectedIndex <> 0 Then
            'ddlgrouptype.ClearSelection()
            'ddlbrand.ClearSelection()
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value

            ObjSubSonic.Binddropdown(ddlgrouptype, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)

        Else
            clearitems1()
        End If
    End Sub

    Protected Sub ddlgrouptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlgrouptype.SelectedIndexChanged
        If ddlgrouptype.SelectedIndex <> 0 Then
            clearitems2()
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            ObjSubSonic.Binddropdown(ddlbrand, "MN_GET_ASSETBRAND_LOCGRUPTYPE", "BRAND_NAME", "BRAND_ID", param)
        Else
            clearitems2()
        End If
    End Sub

    Protected Sub ddlbrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlbrand.SelectedIndexChanged
        If ddlbrand.SelectedIndex <> 0 Then
            clearitems3()
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            param(3) = New SqlParameter("@VEND_ID", SqlDbType.NVarChar, 200)
            param(3).Value = ddlbrand.SelectedItem.Value
            ObjSubSonic.Binddropdown(cmbVen, "MN_GET_ASSET_VENDOR_BY_LOCGRUPBRND", "VENDOR_NAME", "VENDOR_ID", param)
        Else
            clearitems3()
        End If
    End Sub

    Protected Sub cmbVen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbVen.SelectedIndexChanged
        If cmbVen.SelectedItem.Value <> "--Select--" Then
            'clearitems4()
            Dim param1(5) As SqlParameter
            param1(0) = New SqlParameter("@AMN_CTM_ID", SqlDbType.NVarChar, 200)
            param1(0).Value = cmbVen.SelectedItem.Value
            param1(1) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 200)
            param1(1).Value = cboBuilding.SelectedItem.Value
            param1(2) = New SqlParameter("@GROUP_ID", SqlDbType.NVarChar, 200)
            param1(2).Value = ddlGroup.SelectedItem.Value
            param1(3) = New SqlParameter("@GROUP_TYPE_ID", SqlDbType.NVarChar, 200)
            param1(3).Value = ddlgrouptype.SelectedItem.Value
            param1(4) = New SqlParameter("@BRAND", SqlDbType.NVarChar, 200)
            param1(4).Value = ddlbrand.SelectedItem.Value
            param1(5) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param1(5).Value = Session("COMPANYID")
            ObjSubSonic.Binddropdown(DrpDwnAMCId, "MN_AMC_GET_WORKORDER_BY_AMNSTA_ID", "AMN_PLAN_ID", "AMN_PLAN_ID", param1)
        Else
            clearitems4()
        End If
    End Sub

    Protected Sub DrpDwnAMCId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DrpDwnAMCId.SelectedIndexChanged
        clearitems5()
        Try
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
            param(0).Value = DrpDwnAMCId.SelectedItem.Text
            ObjSubSonic.BindListBox(lstasset, "GET_AMC_DETAILS", "ASSETNAME", "AMN_PLAN_ID", param)
            If DrpDwnAMCId.SelectedIndex > 0 Then
                BindDocuments(DrpDwnAMCId.SelectedItem.Text)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDocs.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            CType(e.Item.Cells(6).Controls(0), LinkButton).Attributes.Add("OnClick", "return confirm('This will remove " & e.Item.Cells(1).Text & " document. Click OK to continue.');")
        End If
    End Sub

    Private Sub grdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.ItemCommand
        If e.CommandName = "Select" Then
            txtEditDocNo.Text = grdDocs.DataKeys(e.Item.ItemIndex)
            e.Item.BackColor = Drawing.Color.LightSteelBlue
            txtIndex.Text = e.Item.ItemIndex
            txttype.Text = grdDocs.Items(e.Item.ItemIndex).Cells(1).Text
            cmbstatus.SelectedIndex = cmbstatus.Items.IndexOf(cmbstatus.Items.FindByText(grdDocs.Items(e.Item.ItemIndex).Cells(3).Text.Trim))
            txtDate.Text = grdDocs.Items(e.Item.ItemIndex).Cells(4).Text
            If grdDocs.Items(e.Item.ItemIndex).Cells(3).Text.Trim = "Receivable" Then
                FileUpload.Visible = False
            Else
                FileUpload.Visible = True
            End If
            cmdsavedoc.Text = "Update"
            btnCancel.Visible = True
            tblUploadFile.Visible = True
        End If
    End Sub

    Private Sub grdDocs_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.DeleteCommand
        If grdDocs.EditItemIndex = -1 Then
            Dim Bid As Integer
            Dim DocPath As String
            Bid = grdDocs.DataKeys(e.Item.ItemIndex)
            param = New SqlParameter(3) {}
            param(0) = New SqlParameter("@AMD_CANCEL_BY", SqlDbType.NVarChar, 50)
            param(0).Value = Session("uid")
            param(1) = New SqlParameter("@AMD_CANCEL_DT", SqlDbType.NVarChar, 50)
            param(1).Value = getoffsetdatetime(DateTime.Now)
            param(2) = New SqlParameter("@AMD_ID", SqlDbType.Int)
            param(2).Value = Bid
            param(3) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(3).Value = Session("COMPANYID")
            ObjSubSonic.GetSubSonicDataSet("AMC_CANCEL_DOCS", param)
            BindDocuments(DrpDwnAMCId.SelectedItem.Text)
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        cmdsavedoc.Text = "Upload"
        tblUploadFile.Visible = False
        btnAdd.Visible = True
        ClearControls()
        If DrpDwnAMCId.SelectedIndex > 0 Then
            BindDocuments(DrpDwnAMCId.SelectedItem.Text)
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        txtDate.Text = ""
        txttype.Text = ""
        cmbstatus.SelectedIndex = 0
        tblUploadFile.Visible = True
        btnAdd.Visible = False
        cmdsavedoc.Text = "Upload"
        lblMsg.Text = ""
        FileUpload.Visible = True
    End Sub

    Private Sub cmbstatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbstatus.SelectedIndexChanged
        If cmbstatus.SelectedItem.Text = "Receivable" Or cmbstatus.SelectedItem.Text = "--Select--" Then
            FileUpload.Visible = False
        Else
            FileUpload.Visible = True
        End If
    End Sub

    Protected Sub cmdsavedoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdsavedoc.Click
        If cmbstatus.SelectedItem.Text = "Received" And CDate(txtDate.Text) > getoffsetdatetime(DateTime.Now) Then
            Dim strScript As String
            lblMsg.Text = "Received Date Cannot Be Greater Than Todays Date..."
            lblMsg.Visible = True
            Return
        End If
        If cmdsavedoc.Text = "Upload" Then UploadDocs()
        If cmdsavedoc.Text = "Update" Then UpdateDoc()
        tblUploadFile.Visible = False
        btnAdd.Visible = True
        If DrpDwnAMCId.SelectedIndex > 0 Then
            BindDocuments(DrpDwnAMCId.SelectedItem.Text)

        End If

    End Sub

End Class
