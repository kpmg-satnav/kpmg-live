<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PaymentDetails.ascx.vb"
    Inherits="MaintenanceManagement_AMC_Controls_PaymentDetails" %>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>
            </div>
        </div>
    </div>
</div>

<div id="Panel1" runat="server">

    <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>PPM Plan Id <b>:</b></label>
                <asp:Label ID="lblReqId" Font-Bold="true" runat="server" CssClass="control-label"></asp:Label>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Location <b>:</b></label>
                <asp:Label ID="cboBuilding" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Group <b>:</b></label>
                <asp:Label ID="ddlGroup" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Group Type<b>:</b></label>
                <asp:Label ID="ddlgrouptype" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Brand <b>:</b></label>
                <asp:Label ID="ddlbrand" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Vendor<b>:</b></label>
                <asp:Label ID="cmbVen" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
            </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Name<span style="color: red;"></span></label>
                <asp:ListBox ID="lstasset" runat="server" CssClass="form-control" data-live-search="true"></asp:ListBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Payment Advice No<span style="color: red;">*</span></label>
                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="cmbPayment" Display="None"
                    ErrorMessage="Please Select Payment Advice No" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                <asp:DropDownList ID="cmbPayment" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Payment Advice Generation Date<span style="color: red;"></span></label>
                <asp:TextBox ID="txtcheckdate" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>DD/Cheque/Acc No<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtChqNo" Display="None"
                    ErrorMessage="Please Enter Cheque/DD/Acc No"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtChqNo" CssClass="form-control" runat="server" MaxLength="15"></asp:TextBox>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Dated/Credited On<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtChqDate" Display="None"
                    ErrorMessage="Please Enter Dated/Credited On"></asp:RequiredFieldValidator>
                <div class='input-group date' id='fromdate'>
                    <asp:TextBox ID="txtChqDate" CssClass="form-control" runat="server"></asp:TextBox>
                    <span class="input-group-addon">
                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Amount<span style="color: red;"></span></label>
                <asp:TextBox ID="txtChqAmt" CssClass="form-control" AutoPostBack="True" ReadOnly="True" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
            <div class="form-group">
                <asp:Button ID="btnSub" CssClass="btn btn-default btn-primary" runat="server" Text="Create"></asp:Button>
                <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="false" CssClass="btn btn-default btn-primary" />
            </div>
        </div>
    </div>

</div>
