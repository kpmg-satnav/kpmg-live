<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmAssetGroupsType.aspx.vb" Inherits="MaintenanceManagement_MaintenanceMaster_frmAssetGroupsType" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Asset Group Type Master 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ShowMessageBox="false" ShowSummary="True"
                            ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="LblFinalStatus" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            <asp:Label ID="LblPageStatus" runat="server" CssClass="col-md-12 control-label" Visible="False"></asp:Label>
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-2 btn btn-default pull-right">
                                        <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                            ToolTip="Please Select Add to add new Asset Group Type and Select Modify to modify the existing Asset Group Type" />
                                        Add
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="btn btn-default" style="margin-left: 25px">
                                        <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                            ToolTip="Please Select Add to add new Asset Group Type and Select Modify to modify the existing Asset Group Type" />
                                        Modify
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trgroup" runat="server" visible="False">
                                        <label class="col-md-5 control-label">Asset Group Types <span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cmpPkeys" runat="server" ErrorMessage="Please Select  Asset Group Type"
                                            ControlToValidate="cmbPKeys" Display="None" Operator="NotEqual" ValueToCompare="--Select--">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbPKeys" TabIndex="1" runat="server" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Asset Group Types">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Asset Group Type Code <span style="color: red;">*</span></label>

                                        <asp:TextBox ID="txtcd" runat="server" Visible="false" CssClass="form-control">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvcode" runat="server" Width="48px" ErrorMessage="Please Enter Asset Group Type Code"
                                            ControlToValidate="txtcode" Display="None">
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regXCode" runat="server" ErrorMessage="Enter Valid Code !"
                                            ControlToValidate="txtcode" Display="None" ValidationExpression="^[A-Za-z/0-9 -]+">
                                        </asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtcode" TabIndex="2" runat="server" CssClass="form-control"
                                                MaxLength="30">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Asset Group Type Name <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" Width="48px" ErrorMessage="Please Enter Asset Group Type Name"
                                            ControlToValidate="txtName" Display="None">
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegXName" runat="server" ErrorMessage="Enter Only Alphabets !"
                                            ControlToValidate="txtName" Display="None" ValidationExpression="^[a-z&amp;0-9/A-Z ]+">
                                        </asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtName" TabIndex="3" runat="server" CssClass="form-control"
                                                MaxLength="50">
                                            </asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Asset Category<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cvacat" runat="server" ErrorMessage="Please Select  Asset Category "
                                            ControlToValidate="cmbastype" Display="None" Operator="NotEqual" ValueToCompare="--Select--">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbastype" TabIndex="4" runat="server" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Asset Category" AutoPostBack="True">

                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="Movable">Movable</asp:ListItem>
                                                <asp:ListItem Value="Fixed">Fixed</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Is this Asset Group Type related to Spaces <span style="color: red;">*</span></label>

                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Please Select Asset Group Type Spaces "
                                            ControlToValidate="cmbspaces" Display="None" Operator="NotEqual" ValueToCompare="--Select--">
                                        </asp:CompareValidator>


                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbspaces" TabIndex="5" runat="server" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true">

                                                <asp:ListItem>--Select--</asp:ListItem>
                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                <asp:ListItem Value="No">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Asset Group <span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CmpFkeys" runat="server" ErrorMessage="Please Select  Asset Group "
                                            ControlToValidate="cmbFKeys" Display="None" Operator="NotEqual" ValueToCompare="--Select--">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbFKeys" TabIndex="6" runat="server" AutoPostBack="True"
                                                CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Asset Group">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Status<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                                            Display="none" ErrorMessage="Please Select Status" Operator="NotEqual" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Asset Status">
                                                <asp:ListItem>--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="0">InActive</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvremarks" runat="server" Width="48px" ErrorMessage="Please Enter Remarks"
                                            ControlToValidate="txtrem" Display="None">
                                        </asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtrem" TabIndex="7" runat="server" CssClass="form-control"
                                                MaxLength="500" TextMode="MultiLine">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" TabIndex="8" runat="server" CssClass="btn btn-primary custom-button-color" Text="Add"></asp:Button>
                                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary custom-button-color" CausesValidation="false" PostBackUrl="~/MaintenanceManagement/Masters/Mas_Webfiles/frmMaintenanceMasters.aspx"
                                        Text="Back" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvAssetGroupType" runat="server" AllowPaging="True" AllowSorting="False"
                                    RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                    PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Asset Group Type Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("AGT_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset Group Type Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLcmName" runat="server" CssClass="lblLcmName" Text='<%#Eval("AGT_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset Group Type Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("AGT_CODE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Asset Group">
                                            <ItemTemplate>
                                                <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("AGT_AGPM_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#Bind("AGT_STA_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
