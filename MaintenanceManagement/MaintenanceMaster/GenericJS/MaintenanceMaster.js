﻿// JScript File

function GetAssetGroup()
{   
    $("#UsersGrid").jqGrid(
   {    url: '../../MaintenanceManagement/MaintenanceMaster/Generic_Handlers/AssetGroup.ashx',
        datatype: 'json',
        height: 250,
        colNames: ['Asset Groups ','STATUS'],
        colModel: [
                           { name: 'AAG_NAME', width: 250, sortable: true },
                           { name: 'AAG_STA_ID', width: 250, sortable: true }
                   ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#UsersGridPager'),
        sortname: 'AAG_CODE',
        viewrecords: true,
        forceFit: true,
        sortorder: 'asc'
        
        //caption: 'Report'
        }).navGrid('#UsersGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
					
					// add custom button to export the data to excel
			jQuery("#UsersGrid").jqGrid('navButtonAdd','#UsersGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'exportexcel.aspx?Mcity='+ city +'&MLCM_CODE='+ brn + ''; jQuery("#UsersGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
  
function GetAssetGroupType()
{   
    $("#UsersGrid").jqGrid(
   {    url: '../../MaintenanceManagement/MaintenanceMaster/Generic_Handlers/AssetGroupType.ashx',
        datatype: 'json',
        height: 250,
        colNames: ['Asset Groups Type ','STATUS'],
        colModel: [
          
                           { name: 'AGT_NAME', width: 250, sortable: true },
                           { name: 'AGT_STA_ID', width: 250, sortable: true }
                   ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#UsersGridPager'),
        sortname: 'AGT_NAME',
        viewrecords: true,
        forceFit: true,
        sortorder: 'asc'
        
        //caption: 'Report'
        }).navGrid('#UsersGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
					
					// add custom button to export the data to excel
			jQuery("#UsersGrid").jqGrid('navButtonAdd','#UsersGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'exportexcel.aspx?Mcity='+ city +'&MLCM_CODE='+ brn + ''; jQuery("#UsersGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
  