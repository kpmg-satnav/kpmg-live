﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data;

public partial class MaintenanceManagement_Reports_PMPlanSheet : System.Web.UI.Page
{
    DataTable dt = new DataTable();

    clsSubSonicCommonFunctions ObjSubSonic = new clsSubSonicCommonFunctions();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["uid"] == null)
            Response.Redirect(Application["FMGLogout"].ToString());

        if (!IsPostBack)
        {
            LoadLocation();
            LoadSubCategory();
            BindPlanType(); 
            LoadYears();
        }
    }

    public void LoadYears()
    {
        ddlyear.Items.Clear();
        for (int i = DateTime.Now.AddYears(1).Year; i >= DateTime.Now.AddYears(-6).Year; i--)
        {

            ListItem ls = new ListItem(i.ToString(), i.ToString());
            if (i == DateTime.Now.Year)
            {
                ls.Selected = true;
                ddlyear.Items.Add(ls);
            }
            else
                ddlyear.Items.Add(ls);
        }
    }

    public void LoadLocation()
    {
        DataSet ds = new DataSet();

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@USER_ID", SqlDbType.VarChar);
        param[0].Value = Session["UID"];
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "MN_GET_ALL_LOCATIONS", param);
        ddllocation.DataSource = ds.Tables[0];
        ddllocation.DataValueField = "LCM_CODE";
        ddllocation.DataTextField = "LCM_NAME";
        ddllocation.DataBind();
        ddllocation.Items.Insert(0, new ListItem("--Select All--"));

        //int i = ObjSubSonic.Binddropdown(ref ddllocation, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param);
    }

    public void LoadSubCategory()
    {
        //ObjSubSonic.Binddropdown(ref ddlSubcategory, "MM_GET_SUB_CATEGORY", "AST_SUBCAT_NAME", "AST_SUBCAT_CODE");
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "MM_GET_SUB_CATEGORY");
        ddlSubcategory.DataSource = ds.Tables[0];
        ddlSubcategory.DataValueField = "AST_SUBCAT_CODE";
        ddlSubcategory.DataTextField = "AST_SUBCAT_NAME";
        ddlSubcategory.DataBind();
        ddlSubcategory.Items.Insert(0, new ListItem("--Select All--"));
    }

    public void BindPlanType()
    {
        //ObjSubSonic.Binddropdown(ref ddlplantype, "GET_DISTINCT_PLAN_TYPE", "PVM_PLAN_FREQ", "PVM_PLAN_FREQ");
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_DISTINCT_PLAN_TYPE");
        ddlplantype.DataSource = ds.Tables[0];
        ddlplantype.DataValueField = "PVM_PLAN_FREQ";
        ddlplantype.DataTextField = "PVM_PLAN_FREQ";
        ddlplantype.DataBind();
        ddlplantype.Items.Insert(0, new ListItem("--Select All--"));
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        var selected = ddlplantype.GetSelectedIndices().ToList();
        var selectedValues = (from c in selected
                              select ddlplantype.Items[c].Value).ToList();

        string plans = string.Join(",", selectedValues.ToArray());

        selected = ddllocation.GetSelectedIndices().ToList();
        selectedValues = (from c in selected
                          select ddllocation.Items[c].Value).ToList();

        string locations = string.Join(",", selectedValues.ToArray());

        selected = ddlSubcategory.GetSelectedIndices().ToList();
        selectedValues = (from c in selected
                          select ddlSubcategory.Items[c].Value).ToList();

        string subcategories = string.Join(",", selectedValues.ToArray());

        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@YEAR", ddlyear.SelectedValue);
        param[1] = new SqlParameter("@LCM_CODE", locations);
        param[2] = new SqlParameter("@SERVICE", subcategories);
        param[3] = new SqlParameter("@FREQ", plans);
        param[4] = new SqlParameter("@FLAG", 2);

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PM_GET_PLAN_SHEET", param);

        dt = ds.Tables[0];

        ReportViewer1.Reset();
        ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("PlanSheetDS", dt));
        //ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(localReport_SubreportProcessing);
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/PMPlanSheet.rdlc");
        ReportViewer1.LocalReport.Refresh();
        ReportViewer1.SizeToReportContent = true;
        ReportViewer1.PageCountMode = PageCountMode.Actual;
        ReportViewer1.Visible = true;

    }


    //All  we’ve done here is, we’re binding the data set to appropriate report data source**. 
    void localReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
    {
        string lcm_code = e.Parameters["PLCM_NAME"].Values[0].ToString();
        string service = e.Parameters["PSERVICE"].Values[0].ToString();
        string year = e.Parameters["PYEAR"].Values[0].ToString();

        var selected = ddlplantype.GetSelectedIndices().ToList();
        var selectedValues = (from c in selected
                              select ddlplantype.Items[c].Value).ToList();

        string plans = string.Join(",", selectedValues.ToArray());

        //string lcm_code = "";
        //string service = "";
        //string year = ddlyear.SelectedValue;


        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@YEAR", ddlyear.SelectedValue);
        param[1] = new SqlParameter("@lcm_Code", lcm_code);
        param[2] = new SqlParameter("@service", service);
        param[3] = new SqlParameter("@FLAG", 2);
        param[4] = new SqlParameter("@FREQ", plans);

        DataTable subrpt = new DataTable();
        subrpt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "PM_GET_PLAN_SHEET", param);

        e.DataSources.Add(new ReportDataSource("PlanSheetDS", subrpt));

    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlplantype.ClearSelection();
        ddllocation.ClearSelection();
        ddlSubcategory.ClearSelection();
        ddlyear.ClearSelection();
    }
    protected void ddlplantype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlplantype.SelectedValue == "--Select All--")
        {
            foreach (ListItem item in ddlplantype.Items)
            {
                if (item.Value != "--Select All--")
                    item.Selected = true;
                else
                    item.Selected = false;
            }
        }
    }
}