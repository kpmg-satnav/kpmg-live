﻿<%@ Page AutoEventWireup="false" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />


    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

      

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="MaintenanceCustomizedController" onload="setDateVals()" class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Create / Update AMC" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Maintenance Customizable Report</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" name="frmMaintCust" data-valid-submit="LoadData()">
                        <div class="clearfix">

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Quick Select</label>
                                    <br />
                                    <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                        <option value="TODAY">Today</option>
                                        <option value="YESTERDAY">Yesterday</option>
                                        <option value="7">Last 7 Days</option>
                                        <option value="30">Last 30 Days</option>
                                        <option value="THISMONTH">This Month</option>
                                        <option value="LASTMONTH">Last Month</option>
                                        <option value="THISYEAR">This Year</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">From Date</label>
                                    <div class="input-group date" id='FromDate'>
                                        <input type="text" class="form-control" data-ng-model="MaintCust.FromDate" id="Text1" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('FromDate')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="frmMaintCust.$submitted && frmMaintCust.FromDate.$invalid" style="color: red;">Please select From Date</span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">To Date</label>
                                    <div class="input-group date" id='ToDate'>
                                        <input type="text" class="form-control" data-ng-model="MaintCust.ToDate" id="Text2" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('ToDate')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="frmMaintCust.$submitted && frmMaintCust.ToDate.$invalid" style="color: red;">Please select To Date</span>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmMaintCust.$submitted && frmMaintCust.Request_Type.$invalid}">
                                    <label for="txtcode">Category Type</label>
                                    <select class="selectpicker" data-ng-model="MaintCust.Request_Type" id="Request_Type" name="Request_Type" required="">
                                        <option value="0">--ALL--</option>
                                        <option value="1">Asset Under AMC</option>
                                        <option value="2">Asset Under Non AMC</option>
                                        <option value="3">Asset Expired</option>
                                        <option value="4">Asset Breakdown</option>
                                    </select>
                                    <span class="error" data-ng-show="frmMaintCust.$submitted && frmMaintCust.Request_Type.$invalid" style="color: red;">Please Select Request Type</span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmMaintCust.$submitted && frmMaintCust.Columns.$invalid}">
                                    <label class="control-label">Columns<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Cols" data-output-model="COL" button-label="icon COL" item-label="icon COL" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="COL" name="Columns" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmMaintCust.$submitted && frmMaintCust.Columns.$invalid" style="color: red">Please Select Columns </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                <div class="form-group">
                                    <label class="control-label">Company</label>
                                    <div isteven-multi-select data-input-model="Company" data-output-model="MaintCust.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                        item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="MaintCust.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmMaintCust.$submitted && frmMaintCust.Company.$invalid" style="color: red">Please Select Company </span>
                                </div>
                            </div>
                            <br />
                            <div class="col-md-3 col-sm-6 col-xs-12 text-left" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>

                        <div class="row">
                            <%--<div class="col-md-6">
                                    <label>View In : </label>
                                    <input id="viewswitch" type="checkbox" checked data-size="small"
                                        data-on-text="<span class='fa fa-table'></span>"
                                        data-off-text="<span class='fa fa-bar-chart'></span>" />

                                </div>--%>
                            <div class="box-footer text-right" id="table2" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <br />
                                <a data-ng-click="GenReport(MaintCust,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(MaintCust,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(MaintCust,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                            </div>
                        </div>
                        <%-- </form>
                            <form id="form2">--%>
                        <div id="Tabular" data-ng-show="GridVisiblity">
                            <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">

                                <div class="input-group" style="width: 20%">
                                    <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary custom-button-color" type="submit">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>
                        <div id="Graphicaldiv" data-ng-show="GridVisiblity">
                            <div id="MaintGraph">&nbsp</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../Js/MaintenanceCustomized.min.js" defer></script>
    <script type="text/javascript" defer>
        function setDateVals() {
            $('#fromdate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#todate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#fromdate').datepicker('setDate', new Date(moment().startOf('year').format('MM/DD/YYYY')));
            $('#todate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));
        }
    </script>
    <script type="text/javascript" defer>
        $(document).ready(function () {
            setDateVals();
        });

        var CompanySession = '<%= Session("COMPANYID")%>';
    
    </script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
</body>
</html>
