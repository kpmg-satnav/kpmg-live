<%@ Page Language="VB" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />

    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };


    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body data-ng-controller="ViewPVMDatesController" class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="PPM Maintenance Status" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Preventive Maintenace Status Details</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="MaintContract" name="frmMaintCont" data-valid-submit="LoadData()">
                        <div class="clearfix" data-ng-show="CompanyVisible==0">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Company</label>
                                    <div isteven-multi-select data-input-model="Company" data-output-model="MaintCont.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                        item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="MaintCont.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmMaintCont.$submitted && frmMaintCont.Company.$invalid" style="color: red">Please Select Company </span>
                                </div>
                            </div>
                            <br />
                            <div class="box-footer text-left" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <br />
                            <div class="col-md-12" id="table2" data-ng-show="GridVisiblity">
                                <br />
                                <a data-ng-click="GenReport(MaintCont,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(MaintCont,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(MaintCont,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                            </div>
                        </div>
                        <div>
                            <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session("COMPANYID")%>';
    </script>
    <script src="../Js/AMCPaymentAdvice.js" defer></script>
    <script src="../../../SMViews/Utility.js" defer></script>
</body>
</html>
