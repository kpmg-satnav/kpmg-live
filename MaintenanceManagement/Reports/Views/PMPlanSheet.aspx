﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PMPlanSheet.aspx.cs" Inherits="MaintenanceManagement_Reports_PMPlanSheet" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::QuickFMS</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-fresh.min.css" rel="stylesheet" />

</head>
<body>


    <div class="container-fluid page-content-inner">
        <div class="bgc-gray p-20 m-b-25">
            <div class="panel-heading-qfms">
                <h3 class="panel-title panel-heading-qfms-title">PM Plan Sheet</h3>
            </div>
            <div class="panel-body" style="padding-right: 50px;">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server"
                        ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" Style="padding-bottom: 20px" />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <label>Year of Plan :</label>

                                <asp:DropDownList ID="ddlyear" runat="server" CssClass="form-control">
                                    <asp:ListItem>2017</asp:ListItem>
                                    <asp:ListItem>2018</asp:ListItem>
                                    <asp:ListItem>2019</asp:ListItem>
                                    <asp:ListItem>2020</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <label>Location :</label>
                                <asp:ListBox ID="ddllocation" SelectionMode="Multiple" runat="server" CssClass="form-control selectpicker" data-live-search="true"></asp:ListBox>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <label>Service:</label>
                                <asp:ListBox ID="ddlSubcategory" SelectionMode="Multiple" runat="server" CssClass="form-control selectpicker" data-live-search="true"></asp:ListBox>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <label>Maintenance Frequency :</label>

                                <asp:ListBox ID="ddlplantype" runat="server" SelectionMode="Multiple" CssClass="form-control selectpicker" data-live-search="true"></asp:ListBox>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                            </div>
                        </div>

                        <div class="col-xs-12 text-right" style="padding-left: 20px">
                            <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Show Plan" OnClick="btnsubmit_Click"></asp:Button>
                            <asp:Button ID="btnClear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" OnClick="btnClear_Click"></asp:Button>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                        </div>
                    </div>
                    <div class="col-md-12 table-responsive">
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server"></rsweb:ReportViewer>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>

        $(document).ready(function () {

            $(".selectpicker").on('change', function () {
                //console.log($(this).val().length);

                //console.log($("#ddlplantype").prop('selectedIndex'));

                if ($(this).val() == "--Select All--") {
                    $("option", this).prop("selected", true);
                }
                //else if ($(this).val() != "--Select All--" && $(this).val().length != 1) {
                //    $("#ddlplantype option").prop("selected", false);
                //}
                $('.selectpicker').selectpicker('refresh');

            });

            //$("#ddlplantype").on('change', function () {
            //    if ($(this).val() == "--Select All--") {
            //        $("#ddlplantype option").prop("selected", true);
            //    }
            //    $('.selectpicker').selectpicker('refresh');

            //});
        });
    </script>
</body>
</html>
