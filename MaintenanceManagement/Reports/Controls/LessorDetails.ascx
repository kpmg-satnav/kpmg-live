<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LessorDetails.ascx.vb"
    Inherits="PropertyManagement_Reports_Controls_LessorDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                            ForeColor="Black">Lessor Details
             <hr align="center" width="60%" /></asp:Label>
                        &nbsp;
                        <br />
                    </td>
                </tr>
            </table>
            <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_left_top_corner.gif")%>"
                            width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Lessor Details</strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"
                            width="16" /></td>
                </tr>
                <tr>
                    <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                        &nbsp;</td>
                    <td align="left">
                        &nbsp;<br />
                        <asp:Panel ID="panel1" runat="server" Width="100%">
                            <cc1:ExportPanel ID="exp_CityWise" runat="server">
                                <asp:GridView ID="gvitems" runat="Server" AllowPaging="true" PageSize="10" AllowSorting="true"
                                    AutoGenerateColumns="false" EmptyDataText="No Records Found" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Lessor Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPLR_NAME" runat="server" Text='<%#Eval("PLR_NAME" ) %>'></asp:Label>
                                                <asp:Label ID="lblPLR_ID" Visible="false" runat="server" Text='<%#Eval("PLR_ID" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPLR_ADDRESS" runat="server" Text='<%#Eval("PLR_ADDRESS" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phone">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPLR_OFFICE_PHNO" runat="server" Text='<%#Eval("PLR_OFFICE_PHNO" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Premise Owned">
                                            <ItemTemplate>
                                                <asp:GridView ID="dgPremises" EmptyDataText="Premise Owned details not found." runat="server"
                                                    Width="100%" AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="PRM_NAME" HeaderText="Premise">
                                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Width="35%"></HeaderStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="PRM_ADDRESS" HeaderText="Address">
                                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Width="65%"></HeaderStyle>
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </cc1:ExportPanel>
                        </asp:Panel>
			<asp:Button ID="btnExport" runat="server" Width="150px" CssClass="button" Text="Export">
                        </asp:Button>
                    </td>
                    <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif") %>">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <img height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif") %>"
                            width="9" /></td>
                    <td background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                        <img height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>" width="25" /></td>
                    <td>
                        <img height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                            width="16" /></td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExport" />
    </Triggers>
</asp:UpdatePanel>
