<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmMasVendor.aspx.vb" Inherits="Masters_Mas_Webfiles_frmMasVendor"
    Title="Vendor Master" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Vendor Master 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Val1" ShowMessageBox="false" ShowSummary="true" CssClass="alert alert-danger" ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="LblFinalStatus" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                           <%-- <asp:Label ID="LblPageStatus" runat="server" CssClass="col-md-12 control-label" Visible="False"></asp:Label>--%>
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-2 btn btn-default pull-right">
                                        <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                            ToolTip="Please Select Add to add new Vendor and Select Modify to modify the existing Vendor" />
                                        Add
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="btn btn-default" style="margin-left: 25px">
                                        <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                            ToolTip="Please Select Add to add new Vendor and Select Modify to modify the existing Vendor" />
                                        Modify
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="TRVENDOR" runat="server" visible="False">

                                        <asp:Label ID="lblvendor" class="col-md-5 control-label" runat="server">Select Vendor</asp:Label>
                                        <asp:CompareValidator ID="cmpPkeys" runat="server" ErrorMessage="Please Select Vendor"
                                            ControlToValidate="cmbPKeys" Display="None" ValidationGroup="Val1" Operator="NotEqual"
                                            ValueToCompare="--Select--">cmpPkeys</asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbPKeys" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select Vendor">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Vendor Code<span style="color: red;">*</span></label>

                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" Width="16px" ErrorMessage="Please Enter Vendor Code"
                                            ControlToValidate="txtcode" Display="None" ValidationGroup="Val1">rc</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="frvCode" runat="server" ErrorMessage="Enter Valid Vendor Code "
                                            ControlToValidate="txtcode" Display="None" ValidationGroup="Val1" ValidationExpression="^[a-zA-Z0-9 -]*"></asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Vendor Name <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" Width="21px" ErrorMessage="Please Enter Vendor Name"
                                            ControlToValidate="txtName" Display="None" ValidationGroup="Val1">rn</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter Valid Vendor Name"
                                            ControlToValidate="txtName" Display="None" ValidationGroup="Val1" ValidationExpression="^[a-zA-Z&amp;.-_\s]+"></asp:RegularExpressionValidator>&nbsp;
                               
                                         <div class="col-md-7">
                                             <asp:TextBox ID="txtName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Grade  <span style="color: red;">*</span></label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbgrade" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Grade">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="A">A</asp:ListItem>
                                                <asp:ListItem Value="B">B</asp:ListItem>
                                                <asp:ListItem Value="C">C</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Constitution <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="reqconst" runat="server" ControlToValidate="cmbconst" Display="None"
                                            ErrorMessage="Please Select Constitution" InitialValue="--Select--" ValidationGroup="Val1">
                                        </asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbconst" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Constitution">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="Proprietership Concern">proprietorship Concern</asp:ListItem>
                                                <asp:ListItem Value="Partnership Firm">Partnership Firm</asp:ListItem>
                                                <asp:ListItem Value="Pvt Ltd Co">Pvt Ltd Co</asp:ListItem>
                                                <asp:ListItem Value="Pub Ltd Co">Pub Ltd Co</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">City<span style="color: red;">*</span></label>

                                        <asp:CompareValidator ID="Comparevalidator1" runat="server" ErrorMessage="Please Select City"
                                            ControlToValidate="cmbloc" Display="None" ValidationGroup="Val1" Operator="NotEqual"
                                            ValueToCompare="--Select--">loc</asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbloc" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select City">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Pincode</label>
                                        <asp:CompareValidator ID="cmppin" runat="server" ErrorMessage="Enter Pincode In Numeric Values"
                                            ControlToValidate="txtpincode" Display="None" ValidationGroup="Val1" Operator="DataTypeCheck"
                                            Type="Integer">cmpPin</asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtpincode" runat="server" CssClass="form-control" MaxLength="7"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Office Ph. No.</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtoffphno" runat="server" CssClass="form-control"
                                                MaxLength="15"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Mobile Ph. No.<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Enter A Valid 10-Digit Mobile Ph. No."
                                            ControlToValidate="txtmobilephno" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtmobilephno" runat="server" CssClass="form-control"
                                                MaxLength="10"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Residence Ph. No.</label>

                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtresiphno" runat="server" CssClass="form-control"
                                                MaxLength="15"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Email<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter Email"
                                            ControlToValidate="txtemail" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtemail" runat="server" CssClass="form-control" MaxLength="75"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">PAN No.</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtPANno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">GIR No.</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtGIRno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">TAN No.</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtTANno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Bank A/C No.</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtICICIACno" runat="server" CssClass="form-control"
                                                MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">LST No.</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtlstno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">WCT No.</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtwctno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">CST No.</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtcstno" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">ST. Assessment Circle</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtstcircle" runat="server" CssClass="form-control"
                                                MaxLength="75"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Address <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="reqadd" runat="server" ErrorMessage="Please Enter Address"
                                            ControlToValidate="txtaddress" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">

                                            <asp:TextBox ID="txtaddress" runat="server" CssClass="form-control"
                                                MaxLength="500" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Remarks</label>

                                        <div class="col-md-7">

                                            <asp:TextBox ID="txtrem" runat="server" CssClass="form-control"
                                                TextMode="MultiLine" MaxLength="1000"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Status</label>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlstatus" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Status">
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="0">InActive</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Add" ValidationGroup="Val1"></asp:Button></td>
                                <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary custom-button-color" CausesValidation="false" PostBackUrl="~/MaintenanceManagement/Masters/Mas_Webfiles/frmMaintenanceMasters.aspx"
                                    Text="Back" />

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <asp:GridView ID="gvitems" runat="server" AllowPaging="true" AllowSorting="true"
                                    AutoGenerateColumns="false" EmptyDataText="No Vendor Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Vendor Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllcode" runat="server" Text='<%#Eval("AVR_CODE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Vendor Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblname" runat="server" Text='<%#Eval("AVR_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("AVR_STA_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
