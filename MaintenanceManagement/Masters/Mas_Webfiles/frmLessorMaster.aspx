<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmLessorMaster.aspx.vb" Inherits="Masters_Mas_Webfiles_frmLessorMaster"
    Title="Lessor Masters" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Lessor Masters
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0" align="center" border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                        width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">&nbsp;<strong>Lessor Masters</strong></td>
                <td style="width: 17px">
                    <img alt="" height="27" src="<%Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"
                        width="16" /></td>
            </tr>
            <tr>
                <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">&nbsp;</td>

<%--                <asp:Label ID="lblNote" runat="server" Width="100%" CssClass="clsNote" ToolTip="Provide information for (*) mandatory fields. "
                    ForeColor="Red">Provide information for mandatory (*) fields. </asp:Label>--%>
                 <asp:Label ID="Label2" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage" ShowSummary="true"
                            ForeColor="" ValidationGroup="Val1" />
                        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="1">

                            <asp:Label ID="lblmsg" runat="server" CssClass="clsMessage"></asp:Label>
                            <tr>
                                <td align="center" width="25%" colspan="4">
                                    <asp:RadioButtonList ID="rbActions" runat="server" Width="159px" RepeatDirection="Horizontal"
                                        Font-Bold="True" AutoPostBack="True" Height="16px">
                                        <asp:ListItem Value="Add">Add</asp:ListItem>
                                        <asp:ListItem Value="Modify">Modify</asp:ListItem>
                                    </asp:RadioButtonList></td>
                            </tr>
                            <tr id="trLessor" runat="server">
                                <td width="25%" colspan="1">&nbsp;Lessor
                                    <asp:CompareValidator ID="cmpPkeys" runat="server" ErrorMessage="Please Select the Vendor"
                                        ControlToValidate="cmbPKeys" Display="none" ValidationGroup="Val1" Operator="NotEqual" ValueToCompare="--Select--">cmpPkeys</asp:CompareValidator></td>
                                <td width="25%" colspan="3">
                                    <asp:DropDownList ID="cmbPKeys" runat="server" Width="100%" AutoPostBack="True" CssClass="clsComboBox">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td style="height: 18px" width="25%">
                                    <p>
                                        &nbsp;Lessor Code<font class="clsNote">*</font>

                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" Width="16px" ErrorMessage="Please Enter the Code"
                                            ControlToValidate="txtcode" Display="none" ValidationGroup="Val1">rc</asp:RequiredFieldValidator>
                                    </p>
                                </td>
                                <td style="height: 18px" width="25%">
                                    <asp:TextBox ID="txtcode" runat="server" Width="97%" CssClass="clsTextField" MaxLength="30"></asp:TextBox></td>
                                <td style="height: 18px" width="25%">&nbsp;Lessor Name<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" Width="21px" ErrorMessage="Please Enter the Name "
                                        ControlToValidate="txtName" Display="none" ValidationGroup="Val1">rn</asp:RequiredFieldValidator></td>
                                <td style="height: 18px" width="25%">
                                    <asp:TextBox ID="txtName" runat="server" Width="97%" CssClass="clsTextField" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td width="25%" style="height: 46px">&nbsp;Address<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvad" runat="server" Width="9px" ErrorMessage="Please Enter the Address "
                                        ControlToValidate="txtaddress" Display="none" ValidationGroup="Val1"></asp:RequiredFieldValidator></td>
                                <td width="75%" colspan="3" style="height: 46px">
                                    <asp:TextBox ID="txtaddress" runat="server" Width="97%" Height="36px" CssClass="clsTextField"
                                        MaxLength="500" TextMode="MultiLine"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="height: 28px" width="25%">&nbsp;City<font class="clsNote">*</font>
                                    <%--<asp:RequiredFieldValidator ID="reqcity" runat="server" ControlToValidate="cmbloc" Display="None" 
                                        ErrorMessage="Select City" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                      <asp:CompareValidator ID="cvcity" runat="server" ErrorMessage="Please Select City"
                                        ControlToValidate="cmbloc" Display="None" ValidationGroup="Val1" Operator="NotEqual" ValueToCompare="0"></asp:CompareValidator></td>
                            
                                 </td>
                                <td style="height: 28px" width="25%">
                                    <asp:DropDownList ID="cmbloc" runat="server" CssClass="clsComboBox">
                                    </asp:DropDownList>
                                </td>
                                <td style="height: 28px" width="25%">&nbsp;Pin Code<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvpin" runat="server" ErrorMessage="Please Enter Pin Code "
                                        ControlToValidate="txtpincode" Display="none" ValidationGroup="Val1">rfvpin</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cmppin" runat="server" ErrorMessage="Enter pincode in numeric values"
                                        ControlToValidate="txtpincode" Display="none" ValidationGroup="Val1" Operator="DataTypeCheck" Type="Integer">cmpPin</asp:CompareValidator></td>
                                <td style="height: 28px" width="25%">
                                    <asp:TextBox ID="txtpincode" runat="server" Width="97%" CssClass="clsTextField"
                                        MaxLength="7"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td width="25%">&nbsp;Office Ph. No.<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvOffno" runat="server" ErrorMessage="Please Enter Office Phone Number "
                                        ControlToValidate="txtoffphno" Display="none" ValidationGroup="Val1">rfvpin</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3"
                                        runat="server" Display="none" ValidationGroup="Val1" ControlToValidate="txtoffphno"
                                        ErrorMessage="Please Enter Valid Office Ph. No " ValidationExpression="^[0-9-+  ]*"></asp:RegularExpressionValidator>
                                </td>
                                <td width="25%">
                                    <asp:TextBox ID="txtoffphno" runat="server" Width="97%" CssClass="clsTextField"
                                        MaxLength="15"></asp:TextBox></td>
                                <td width="25%">&nbsp;Mobile Ph. No.&nbsp;
                                    <asp:RegularExpressionValidator ID="regMob"
                                        runat="server" Display="none" ValidationGroup="Val1" ControlToValidate="txtmobilephno"
                                        ErrorMessage="Please Enter A Valid 10-digit Mobile Ph. No." ValidationExpression="^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td width="25%">
                                    <asp:TextBox ID="txtmobilephno" runat="server" Width="97%" CssClass="clsTextField"
                                        MaxLength="10"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="height: 12px" width="25%">&nbsp;Residence Ph. No.&nbsp;
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                        runat="server" Display="none" ValidationGroup="Val1" ControlToValidate="txtresiphno"
                                        ErrorMessage="Please Enter Valid Residence Ph. No." ValidationExpression="^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td width="25%">
                                    <asp:TextBox ID="txtresiphno" runat="server" Width="97%" CssClass="clsTextField"
                                        MaxLength="15"></asp:TextBox></td>
                                <td style="height: 12px" width="25%">&nbsp;Email <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvemail" runat="server" Display="none" ValidationGroup="Val1" ErrorMessage="Please Enter Email" ControlToValidate="txtemail"></asp:RequiredFieldValidator>

                                    <asp:RegularExpressionValidator ID="regemail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ErrorMessage="Please enter a valid E-Mail Address" ControlToValidate="txtemail"
                                        Display="none" ValidationGroup="Val1">re</asp:RegularExpressionValidator></td>
                                <td width="25%">
                                    <asp:TextBox ID="txtemail" runat="server" Width="97%" CssClass="clsTextField" MaxLength="75"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td width="25%">&nbsp;PAN No.</td>
                                <td width="25%">
                                    <asp:TextBox ID="txtPANno" runat="server" Width="97%" CssClass="clsTextField" MaxLength="50"></asp:TextBox></td>
                                <td width="25%">&nbsp;GIR No.</td>
                                <td width="25%">
                                    <asp:TextBox ID="txtGIRno" runat="server" Width="97%" CssClass="clsTextField" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="height: 20px" width="25%">&nbsp;TAN No.</td>
                                <td style="height: 20px" width="25%">
                                    <asp:TextBox ID="txtTANno" runat="server" Width="97%" CssClass="clsTextField" MaxLength="50"></asp:TextBox></td>
                                <td style="height: 20px" width="25%">&nbsp;Bank A/C No.</td>
                                <td style="height: 20px" width="25%">
                                    <asp:TextBox ID="txtICICIACno" runat="server" Width="97%" CssClass="clsTextField"
                                        MaxLength="75"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td width="25%">&nbsp;LST No.</td>
                                <td width="25%">
                                    <asp:TextBox ID="txtlstno" runat="server" Width="97%" CssClass="clsTextField" MaxLength="50"></asp:TextBox></td>
                                <td width="25%">&nbsp;WCT No.</td>
                                <td width="25%">
                                    <asp:TextBox ID="txtwctno" runat="server" Width="97%" CssClass="clsTextField" MaxLength="50"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td width="25%">&nbsp;CST No.</td>
                                <td width="25%">
                                    <asp:TextBox ID="txtcstno" runat="server" Width="97%" CssClass="clsTextField" MaxLength="50"></asp:TextBox></td>


                                <td width="25%"style="height: 28px" width:"25%">&nbsp;Select Status<font class="clsNote">*</font> &nbsp; </td>
                                       <asp:CompareValidator ID="ddlstatusvalidator" runat="server" ErrorMessage="Please Select Status"
                                        ControlToValidate="ddlstatus" Display="None" ValidationGroup="Val1" Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                               
                                <td width="25%">
                                    <asp:DropDownList ID="ddlstatus" runat="server" CssClass="clsComboBox" Width="97%">
                                        <asp:ListItem Text="--Select--"></asp:ListItem>
                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                        <asp:ListItem Value="0">InActive</asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                            </tr>
                            <tr>
                                <td align="center" width="25%" colspan="4">
                                     <asp:Button ID="btnback" CssClass="button" runat="server" Text="Back" />    
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" ValidationGroup="Val1"></asp:Button></td>
                            </tr>
                        </table>
                        <asp:Panel ID="panel1" runat="server" Width="100%">
                            <asp:GridView ID="gvitems" runat="server" AllowPaging="true" AllowSorting="true"
                                AutoGenerateColumns="false" EmptyDataText="No Records Found" Width="100%">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsno" runat="server" Text='<%#Eval("PLR_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lessor Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lbllcode" runat="server" Text='<%#Eval("PLR_CODE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lessor Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblname" runat="server" Text='<%#Eval("PLR_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("STATS") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif") %>">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <img height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif") %>"
                        width="9" /></td>
                <td background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                    <img height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>" width="25" /></td>
                <td>
                    <img height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                        width="16" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
