<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmAssetMaster.aspx.vb" Inherits="Masters_Mas_Webfiles_frmAssetMaster"
    Title="Asset Master" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function setup(id) {
            $('.date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Asset Master 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">

                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="true" CssClass="alert alert-danger" ForeColor="Red"
                            ShowMessageBox="false"></asp:ValidationSummary>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="Lblmsg" runat="server" CssClass="col-md-12 control-label"></asp:Label>
                                        <asp:Label ID="LblFinalStatus" runat="server" CssClass="col-md-12 control-label"></asp:Label>
                                        <asp:Label ID="LblPageStatus" runat="server" CssClass="col-md-12 control-label" Visible="False">  </asp:Label>
                                        <asp:Label ID="lblMessage" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label class="col-md-2 btn btn-default pull-right">
                                        <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                            ToolTip="Please Select Add to add new Asset and Select Modify to modify the existing Asset" />
                                        Add
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="btn btn-default" style="margin-left: 25px">
                                        <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                            ToolTip="Please Select Add to add new Asset and Select Modify to modify the existing Asset" />
                                        Modify
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Description/Title:<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvAstdesc" runat="server" Width="48px" Display="None"
                                            ControlToValidate="txtAstTitle" ErrorMessage="Please Enter Description/Title ">
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtAstTitle" runat="server" CssClass="form-control" AutoPostBack="true"
                                                MaxLength="100">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trgroup" runat="server" visible="False">
                                        <label class="col-md-5 control-label">Asset <span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="cmbassets"
                                            ErrorMessage="Please Select Asset" ValueToCompare="--Select--"
                                            Operator="NotEqual">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbassets" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Asset"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Asset Group:<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="casgrp" runat="server" Display="None" ControlToValidate="cmbAstGroups"
                                            ErrorMessage="Please Select Asset Group" ValueToCompare="--Select--" Operator="NotEqual">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbAstGroups" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Vendor"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Asset Group Type:<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cgrpt" runat="server" Display="None" ControlToValidate="cmbAstGroupsTypes"
                                            ErrorMessage="Please Select Asset Group Type" ValueToCompare="--Select--"
                                            Operator="NotEqual">
                                        </asp:CompareValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbAstGroupsTypes" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Group Type"
                                                AutoPostBack="True">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Asset Brand:<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cbrnd" runat="server" Display="None" ControlToValidate="cmbAstBrands"
                                            ErrorMessage="Please Select Asset Brand" ValueToCompare="--Select--" Operator="NotEqual">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbAstBrands" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Brand"
                                                AutoPostBack="True">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Asset Model:<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cmdl" runat="server" Display="None" ControlToValidate="cmbAstModels"
                                            ErrorMessage="Please Select Asset Model" ValueToCompare="--Select--" Operator="NotEqual">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbAstModels" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Model"
                                                AutoPostBack="True">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Serial Number:</label>
                                        <asp:TextBox ID="txtHiddenSNO" runat="server" Visible="False">
                                        </asp:TextBox>
                                        <asp:TextBox ID="txtHidTodayDt" runat="server" Visible="False">
                                        </asp:TextBox>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtAstSNO" runat="server" CssClass="form-control" MaxLength="100">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">PO Number:</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtAstPO" runat="server" CssClass="form-control" MaxLength="40">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Invoice Number:</label>

                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtInvoice" runat="server" CssClass="form-control" MaxLength="20">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Vendor:<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="cmbAstVendor" Display="None"
                                            ErrorMessage="Please Select Vendor" Operator="NotEqual" ValueToCompare="--Select--">
                                        </asp:CompareValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbAstVendor" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Vendor">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Date of Purchase:<span style="color: red;">*</span></label>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                                            ControlToValidate="txtDOP" ErrorMessage="Please Select Date of Purchase">
                                        </asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cmpDOP" runat="server" Display="None" ControlToValidate="txtDOP"
                                            ErrorMessage="Date of Purchase Cannot Be Greater Than Today's Date" Operator="LessThanEqual"
                                            Type="Date" ControlToCompare="txtHidTodayDt">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='Div1'>
                                                <asp:TextBox ID="txtDOP" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Cost:</label>
                                        <asp:RegularExpressionValidator ID="revPropertyType" runat="server" ControlToValidate="txtAstCost"
                                            ErrorMessage="Please enter cost in numbers" Display="None" ValidationExpression="^[0-9]+"
                                            ></asp:RegularExpressionValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtAstCost" runat="server" CssClass="form-control"
                                                    MaxLength="10">
                                                </asp:TextBox>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Warranty Up To:<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Width="48px" Display="None"
                                            ControlToValidate="txtWaranty" ErrorMessage="Please Select Warranty Up To">
                                        </asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CompareValidator4" runat="server" Display="None" ControlToValidate="txtWaranty"
                                            ErrorMessage="Warranty Date Cannot Be Less Than Today's Date" Operator="GreaterThanEqual"
                                            Type="Date" ControlToCompare="txtHidTodayDt">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtWaranty" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%-- <asp:TextBox ID="txtWaranty" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>--%>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Country:<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cmpCountry" runat="server" ControlToValidate="cmbCountry" Display="None" ErrorMessage="Please Select Country" Operator="NotEqual" ValueToCompare="--Select--">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbCountry" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select Country" TabIndex="5">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">City:<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="cmbCity" Display="None" ErrorMessage="Please Select City" Operator="NotEqual" ValueToCompare="--Select--">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbCity" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select City" TabIndex="5">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location:<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cmpBldg" runat="server" ControlToValidate="cmbBldg" Display="None" ErrorMessage="Please Select Location" Operator="NotEqual" ValueToCompare="--Select--">
                                        </asp:CompareValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbBldg" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select Vendor" TabIndex="5">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Floor:<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cmpFloor" runat="server" ControlToValidate="cmbFloor" Display="None" ErrorMessage="Please Select Floor" Operator="NotEqual" ValueToCompare="--Select--">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbFloor" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select Floor" TabIndex="5">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Wing:<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cmpWing" runat="server" ControlToValidate="cmbWing" Display="None" ErrorMessage="Please Select Wing" Operator="NotEqual" ValueToCompare="--Select--">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbWing" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true" ToolTip="Select Wing" TabIndex="5">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Asset Status:<span style="color: red;">*</span></label>
                                        <asp:CompareValidator ID="cmpStatus" runat="server" ControlToValidate="cmbAstStatus" Display="None" ErrorMessage="Please Select Asset Status" Operator="NotEqual" ValueToCompare="--Select--">
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cmbAstStatus" runat="server" CssClass="selectpicker" data-live-search="true" ToolTip="Select Status" TabIndex="5">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="In Use">In Use</asp:ListItem>
                                                <asp:ListItem Value="In Store">In Store</asp:ListItem>
                                                <asp:ListItem Value="Under Repair">Under Repair</asp:ListItem>
                                                <asp:ListItem Value="Disposed">Disposed</asp:ListItem>
                                                <asp:ListItem Value="Others">Others</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Remarks:<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvRem" runat="server" ControlToValidate="txtrem" Display="None" ErrorMessage="Please Enter Remarks">
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div>
                                                <asp:TextBox ID="txtrem" runat="server" CssClass="form-control" Height="50px" MaxLength="100" TextMode="MultiLine">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Add"></asp:Button>
                                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary custom-button-color" CausesValidation="false" PostBackUrl="~/MaintenanceManagement/Masters/Mas_Webfiles/frmMaintenanceMasters.aspx"
                                        Text="Back" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvAsset" runat="server" AllowPaging="True" AllowSorting="False"
                                    RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                    PageSize="8" AutoGenerateColumns="false" EmptyDataText="No Asset Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("AAP_RUNNO")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLcmName" runat="server" CssClass="lblLcmName" Text='<%#Eval("AAP_CODE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset Group">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("AAP_GROUP_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset Group Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("AAP_GROUPTYPE_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Brand">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSpaceType" runat="server" CssClass="lblStatus" Text='<%#Bind("AAP_BRAND_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Model">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#Bind("AAP_MODEL_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

