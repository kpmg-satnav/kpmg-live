<%@ Application Language="VB" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Security.Principal" %>
<%@ Import Namespace="System.Web.SessionState" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Security" %>
<%@ Import Namespace="System.Security.SecurityManager" %>
<%@ Import Namespace="System.Configuration.ConfigurationManager" %>
<%@ Import Namespace="System.Data.DataRowView" %>
<%@ Import Namespace="System.Web.Http" %>


<script RunAt="server">


    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request

        If (Request.Cookies("userculture") Is Nothing) Then
            Response.Cookies.Add(New HttpCookie("userculture", "en-IN"))
        Else
            Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo(Request.Cookies("userculture").Value)
            Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = System.Globalization.DateTimeFormatInfo.InvariantInfo
        End If
        Context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.AppendHeader("Refresh", Convert.ToString((20 * 60) + 10) & "; URL=" & VirtualPathUtility.ToAbsolute("~/login.aspx"))
    End Sub
    'Public Sub Application_PostRequestHandlerExecute(ByVal sender As Object, ByVal e As EventArgs)
    '    'Dim app As HttpApplication = CType(sender, HttpApplication)
    '    'Dim length As Long = app.Context.Response.Filter.Length
    'End Sub
    'Public Sub Application_ReleaseRequestState(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim context As HttpContext = HttpContext.Current
    'End Sub

    'Public Sub Application_PostRequestHandlerExecute(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim context As HttpContext = HttpContext.Current
    '    If context.Session("Uid") is Nothing or context.Session("TENANT") is Nothing  Then
    '        Response.Redirect(AppSettings("FMGLogout"), True)
    '    End If
    'End Sub

    Public Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)

        RouteTable.Routes.MapHttpRoute(name:="DefaultApi", routeTemplate:="api/{controller}/{action}/{id}", defaults:=New With { _
          Key .id = System.Web.Http.RouteParameter.[Optional] _
        }).RouteHandler = New SessionStateRouteHandler()


        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12 Or SecurityProtocolType.Ssl3
        ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(Function()
                                                                                                                                  Return True
                                                                                                                              End Function)



        ' Code that runs on application startup 
        Application("AppURL") = "http://AmantraAxis/"
        Application("server_ip") = "mail.satnavtech.com"

        Application("FMGLogout") = "~/login.aspx"
        Application("Logout") = "~/login.aspx"

        Application("oldAppConStr") = ConfigurationManager.AppSettings("AmantraAxisFramework")
        Application("oldframeworkConstr") = ConfigurationManager.AppSettings("AmantraAxisFramework")
    End Sub

    Public Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown 
    End Sub

    Public Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim strErrorMsg As String = ""
            Dim ex As System.Exception = Context.[Error].GetBaseException()
            ' string strStack = Context.Error.StackTrace; 
            Dim strURL As String = HttpContext.Current.Request.RawUrl.ToString()
            If strURL.IndexOf("?") > 0 Then
                strURL = strURL.Substring(0, strURL.IndexOf("?"))
            Else
                strURL = strURL
            End If
            Dim Host As String = Environment.MachineName
            Dim UserID As Integer = 0
            Dim httpCtx As HttpContext = HttpContext.Current
            Dim objSession As System.Web.SessionState.HttpSessionState = httpCtx.Session
            Dim StatusCode As Integer = 0
            Dim ErrorText As String
            Dim httpException As New HttpException()
            If httpException IsNot Nothing Then
                StatusCode = 500
            End If
            ErrorText = ex.Message

            ' string stackMsg = ex.ToString(); 
            'strErrorMsg = (strErrorMsg & "<b>#Page Name: </b>") + strURL & "<BR>"
            strErrorMsg = ((strErrorMsg & "<b>#Error Message:</b> ") + ex.Message & "") + ex.StackTrace
            Dim exetype As String = ex.GetType().Name

            'Dim obj As New clsMasters
            'obj.Insert_Log(strErrorMsg, strURL, "12345")
            'Dim objsubsonic As New clsSubSonicCommonFunctions
            'Dim param(8) As SqlParameter
            'param(0) = New SqlParameter("@LOG_NAME", SqlDbType.NVarChar, 200)
            'param(0).Value = strErrorMsg
            'param(1) = New SqlParameter("@LOG_PAGE", SqlDbType.NVarChar, 200)
            'param(1).Value = strURL
            'param(2) = New SqlParameter("@LOG_USER", SqlDbType.NVarChar, 200)
            'param(2).Value = Session("Uid").ToString().Trim()

            '' swethan add additinal parameters
            'param(3) = New SqlParameter("@PROCEDURE", SqlDbType.NVarChar, 200)
            'param(7) = New SqlParameter("@LOG_LINE_NUMBER", SqlDbType.NVarChar, 200)

            'If (exetype = "SqlException") Then
            '    param(3).Value = DirectCast(ex, System.Data.SqlClient.SqlException).Procedure
            '    param(7).Value = DirectCast(ex, System.Data.SqlClient.SqlException).LineNumber
            'ElseIf (exetype = "NullReferenceException") Then
            '    param(3).Value = "NullReferenceException"
            '    param(7).Value = ""
            'Else
            '    param(3).Value = ""
            '    param(7).Value = ""
            'End If
            'param(4) = New SqlParameter("@COMPANY", SqlDbType.NVarChar, 200)
            'param(4).Value = Session("COMPANYID")
            'param(5) = New SqlParameter("@TENANT_ID", SqlDbType.NVarChar, 200)
            'param(5).Value = Session("TENANT")
            'param(6) = New SqlParameter("@LOG_MESSAGE", SqlDbType.NVarChar, 200)
            'param(6).Value = ErrorText
            'param(8) = New SqlParameter("@LOG_FROM", SqlDbType.NVarChar, 200)
            'If Context.Request.IsLocal Then
            '    param(8).Value = "Local"
            'Else
            '    param(8).Value = "Live"
            'End If
            'Dim ds As New DataSet
            'ds = objsubsonic.GetSubSonicDataSet("INSERT_LOG", param)

            Dim Procedure As String
            Dim LineNumber As String
            Dim LogFrom As String

            If (exetype = "SqlException") Then
                Procedure = DirectCast(ex, System.Data.SqlClient.SqlException).Procedure
                LineNumber = DirectCast(ex, System.Data.SqlClient.SqlException).LineNumber
            ElseIf (exetype = "NullReferenceException") Then
                Procedure = "NullReferenceException"
                LineNumber = ""
            Else
                Procedure = ""
                LineNumber = ""
            End If
            If Context.Request.IsLocal Then
                LogFrom = "Local"
            Else
                LogFrom = "Live"
            End If

            Dim db As String = AppSettings("FRMDB")

            Dim sp As New SubSonic.StoredProcedure(db & "." & "INSERT_LOG")
            sp.Command.AddParameter("@LOG_NAME", strErrorMsg, DbType.String)
            sp.Command.AddParameter("@LOG_PAGE", strURL, DbType.String)
            sp.Command.AddParameter("@LOG_USER", Session("Uid").ToString().Trim(), DbType.String)
            sp.Command.AddParameter("@PROCEDURE", Procedure, DbType.String)
            sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)
            sp.Command.AddParameter("@TENANT_ID", Session("TENANT"), DbType.String)
            sp.Command.AddParameter("@LOG_MESSAGE", ErrorText, DbType.String)
            sp.Command.AddParameter("@LOG_LINE_NUMBER", LineNumber, DbType.String)
            sp.Command.AddParameter("@LOG_FROM", LogFrom, DbType.String)

            sp.Execute()



            'Context.Items.Add("ErrorSession", strErrorMsg)
            'Server.Transfer("~/ErrorPage.aspx")

        Catch ex As Exception

        End Try
    End Sub

    Public Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started 
        Session("serverip") = "mail.satnavtech.com"
        Session("temp") = ""
        Session("Key") = ""
        Session("PageURL") = ""
        Session("trace") = ""
        Session("MSG") = ""
        Session("Entered") = "No"
        Session("TENANT") = ""
        ' To check whether the user entered his login or not .... 
    End Sub

    Public Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'Dim spUserId As SqlParameter = New SqlParameter("@VC_USERID", SqlDbType.NVarChar, 50)
            'spUserId.Value = Session("uid")
            'SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_UPDATE_LOGIN_INFO", spUserId)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_UPDATE_LOGIN_INFO")
            sp.Command.AddParameter("@VC_USERID", Session("uid"), DbType.String)
            sp.ExecuteScalar()
            'Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1))
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            'Response.Cache.SetNoStore()
            'Session.Abandon()
            Session.RemoveAll()
            'Session.Clear()
            Response.Redirect("../Default.aspx")
        Catch ex As Exception
            'Throw New AmantraAxis.Exception.DataException("This error has been occured while Updating data", "TopFrame", "Load", ex)
        End Try

        Session.RemoveAll()
    End Sub

    Public Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        '' Fires upon attempting to authenticate the user 
        'Dim Cookiename As String = FormsAuthentication.FormsCookieName
        '' To Create New Cookie 
        ''HttpCookie authCookie =new HttpCookie(Request.Cookies[Cookiename].Value) ; 
        ''HttpCookie authCookie = (HttpCookie)Request.Cookies[Cookiename].Value; 
        'Dim authCookie As New HttpCookie(Cookiename)
        'If authCookie Is Nothing Then
        '    Exit Sub
        'End If
        'Dim authTicket As FormsAuthenticationTicket = Nothing
        'Try
        '    authTicket = FormsAuthentication.Decrypt(authCookie.Value)
        'Catch ex As Exception
        '    Exit Sub
        'End Try
        'If authTicket Is Nothing Then
        '    Exit Sub
        'End If
        'Dim roles As String() = authTicket.UserData.Split(New Char() {"|"c})
        'Dim id As New FormsIdentity(authTicket)
        'Dim principal As New GenericPrincipal(id, roles)
        'Context.User = principal
        ''Context.User = New GenericPrincipal(Context.User.Identity , roles) 
    End Sub
    Protected Sub Application_AcquireRequestState(sender As Object, e As EventArgs)
        Dim contextVal As HttpContext = HttpContext.Current
        If contextVal IsNot Nothing AndAlso contextVal.Session IsNot Nothing Then
            Context.Session("UID") = Session("UID")
            Context.Session("LoginUniqueID") = Session("LoginUniqueID")
            '  Context.Session("usr_" + Session("UID")) = HttpContext.Current.Session.SessionID
            If Session("UID") IsNot Nothing AndAlso Session("LoginUniqueID") IsNot Nothing Then
                Dim UsrSta As Integer
                UsrSta = Global.clsMasters.GetUserSession(Session("UID"))

                If UsrSta = 0 Or UsrSta = -1 Then ' IF USRSTA IS 0 THEN SESSION IS EXPIRED
                    Global.clsMasters.doLogout()

                End If
            Else

            End If
        End If
    End Sub



</script>

