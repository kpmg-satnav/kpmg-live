﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System.Web;

public partial class Changepassword : System.Web.UI.Page
{
    SqlDataReader objdata;
    string strTpwd, strPwd = String.Empty;
    string strSql = String.Empty;
    string strADID = String.Empty;
    string strUsername = String.Empty;
    clsQueries strclass;
    string idrole;
    string strlogged = String.Empty;
    string strQuery;
    int iQuery;
    int iQuery1;
    int COUNT;
    protected void Page_Load(object sender, System.EventArgs e)
    {
        try
        {

            if (Session["uid"].ToString() == "")
            {
                Response.Redirect(Application["logout"].ToString());
            }

            if (!IsPostBack)
            {
                SqlParameter[] Params = new SqlParameter[1];
                Params[0] = new SqlParameter("@AURID", SqlDbType.VarChar);
                Params[0].Value = Session["uid"].ToString();
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_MENU_SCRIPT1_BOOTSTRAP_CPASS", Params);
                //SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure((Session["TENANT"] + ("." + "GET_MENU_SCRIPT1_BOOTSTRAP")));
                //sp.Command.AddParameter("@AURID", Session["uid"].ToString(), DbType.String);
                //litMenu.Text = sp.ExecuteScalar;
                litMenu.Text = ds.Tables[0].Rows[0]["Column1"].ToString();

                DataSet dsCustomerInfo = new DataSet();
                Params[0] = new SqlParameter("@VC_USERID", SqlDbType.NVarChar, 200);
                Params[0].Value = Session["uid"];
                dsCustomerInfo = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_SPACE_CHNGPWD_GETCHNGPWD", Params);
                if ((dsCustomerInfo.Tables[0].Rows.Count > 0))
                {
                    txtUsrId.Text = dsCustomerInfo.Tables[0].Rows[0]["AUR_ID"].ToString();
                    txtEmpName.Text = dsCustomerInfo.Tables[0].Rows[0]["AUR_KNOWN_AS"].ToString();
                    txtmailid.Text = dsCustomerInfo.Tables[0].Rows[0]["aur_email"].ToString();
                }

            }
            

        }
        catch (System.Exception exp)
        {
            throw new Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Default", "Load", exp);
        }

        //if ((strSql != String.Empty))
        //{
        //    Response.Redirect(strSql);
        //}

    }

    public DateTime getoffsetdatetime(DateTime date)
    {
        Object O = HttpContext.Current.Session["useroffset"];
        if (O != null)
        {
            string Temp = O.ToString().Trim();
            if (!Temp.Contains("+") && !Temp.Contains("-"))
                Temp = Temp.Insert(0, "+");
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            DateTime startTime = DateTime.Parse(DateTime.UtcNow.ToString());
            DateTime _now = DateTime.Parse(DateTime.UtcNow.ToString());
            foreach (TimeZoneInfo timeZoneInfo__1 in timeZones)
            {
                if (timeZoneInfo__1.ToString().Contains(Temp))
                {
                    // Response.Write(timeZoneInfo.ToString());
                    // string _timeZoneId = "Pacific Standard Time";
                    TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id);
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
                    break;
                }
            }
            return _now;
        }
        else
            return DateTime.Now;
    }

    protected void btnSubmit_Click(object sender, System.EventArgs e)
    {
        try
        {
            if ((txtpwd.Text.Trim().Length < 8))
            {
                lblMsg.Text = "Password should be minimum of 8 Chars.";
                return;
            }

            bool bol = clsSecurity.checkPassword(txtpwd.Text.Trim());
            if ((bol == false))
            {
                lblMsg.Text = "Password should be minimum of 8 Chars, 2 Numerics and 1 Special Char.";
                return;
            }

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@VC_USERID", SqlDbType.NVarChar, 200);
            param[0].Value = Session["uid"];
            DataSet dsGetOldPWD = new DataSet();
            dsGetOldPWD = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_SPACE_VIEW_GETVIEWBTN", param);
            if ((dsGetOldPWD.Tables[0].Rows.Count > 0))
            {
                strPwd = dsGetOldPWD.Tables[0].Rows[0]["USR_LOGIN_PASSWORD"].ToString();
            }

            if ((txtOldPwd.Text == ""))
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Please Enter Old Password ";
                return;
            }

            if ((txtpwd.Text == ""))
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Please Enter New Password ";
                return;
            }

            if ((txtOldPwd.Text.Trim() == strPwd.Trim()))
            {

            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.Text = "Please Enter Valid Old Password ";
                return;
            }

            if ((txtpwd.Text.Trim() == txtcpwd.Text.Trim()))
            {
                SqlParameter[] paramPWD = new SqlParameter[6];
                paramPWD[0] = new SqlParameter("@vc_LGNPWD", SqlDbType.NVarChar, 200);
                paramPWD[0].Value = txtpwd.Text.Trim();
                paramPWD[1] = new SqlParameter("@VC_ACTIVE", SqlDbType.NVarChar, 200);
                paramPWD[1].Value = "Y";
                paramPWD[2] = new SqlParameter("@VC_UPDTBY", SqlDbType.NVarChar, 200);
                paramPWD[2].Value = Session["uid"].ToString().Trim();
                paramPWD[3] = new SqlParameter("@VC_UPDTON", SqlDbType.DateTime);
                paramPWD[3].Value = getoffsetdatetime(DateTime.Now);
                paramPWD[4] = new SqlParameter("@VC_LOGGED", SqlDbType.NVarChar, 200);
                paramPWD[4].Value = "N";
                paramPWD[5] = new SqlParameter("@VC_USERID", SqlDbType.NVarChar, 200);
                paramPWD[5].Value = Session["uid"].ToString().Trim();
                SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_SPACE_CHNGPWD_UPDATEPWD", paramPWD);
                lblMsg.Text = "Password Has been Changed and Sent to Your Mail ID";
                lblPwd.Visible = false;
                btnSubmit.Enabled = true;
                string strMsg;
                strMsg = ("Dear "
                            + (txtEmpName.Text + ("," + ("\r\n" + ("\r\n" + ("<br><br>This is to inform you that your password has been changed to  \'"
                            + (txtpwd.Text + ("\'. " + ("\r\n" + ("\r\n" + ("<br><br>Thanking You," + ("\r\n" + "<br>Amantra Admin."))))))))))));
                Response.Redirect("frmFinalPage.aspx?staid=ChangePassword");
            }

        }
        catch (Exception ex)
        {
            throw new Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Change Password", "Load", ex);
        }

    }

}