﻿<%@ Page Language="VB" AutoEventWireup="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="google-signin-client_id" content="817305281174-8k1t7lombpfpo0gij169s90fcqo6imsq.apps.googleusercontent.com" />
    <%=ScriptCombiner.GetScriptTags("login_scripts", "css", 1)%>
</head>
<body>
    <main class="auth-main">
        <div class="auth-block">
            <form id="GoogleAuth" runat="server">
                <div style="margin-left: auto; margin-right: auto; text-align: center;">
                <label id="lblvalue" style="font-weight:bold; font-size:x-large"
                    class="StrongText">INVALID USER</label>
                </div>
                <br />
                <br />
                <br />
                <br />
                <div class="row">
                <div class="col-md-6 col-md-offset-4">
                <button id="btnNext" class="btn btn-primary" runat="server" value="Back To Login Page" validationgroup="Val1"
                    causesvalidation="true" onclick="signOut();">Back To Login Page</button>
                </div>
                    </div>
            </form>
        </div>
    </main>
</body>
<script src="BootStrapCSS/Scripts/jquery.min.js"></script>

<script src="BlurScripts/BlurJs/api.js"></script>

<script>
    gapi.load('auth2', function () {
        gapi.auth2.init();
    });

    $(document).ready(function () {
        var errval = GetParameterValues("redirect");
        if (errval == "domain")
            $('#lblvalue').text("Invalid Domain! please logout from the google account");
        else if (errval == "invalid")
            $('#lblvalue').text("User not exists! Please contact Adminstator");
        else
            $('#lblvalue').text("Error has occured please logout from the google account");
    });

    function signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        console.log(auth2);
        auth2.signOut().then(function () {
            console.log('User signed out.');
            window.location.href = "login.aspx";
        });
    }



    function GetParameterValues(param) {
        var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < url.length; i++) {
            var urlparam = url[i].split('=');
            if (urlparam[0] == param) {
                return urlparam[1];
            }
        }
    }

</script>
</html>
<%--  --%>