Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports System.io
Partial Class WorkSpace_SMS_Webfiles_frmAssignUserMaps
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        lblmsg.Text = ""
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                BindCity()
                'BindLocation()
            End If
        End If
    End Sub
    Private Sub BindCity()
        ObjSubsonic.Binddropdown(ddlCity, "GETALLCITIES", "CTY_NAME", "CTY_CODE")
    End Sub
    Private Sub BindLocation()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        ddlLocation.Items.Clear()
        BindLocation()
        If ddlLocation.Items.Count <= 1 Then
            lblMsg.Text = "No Locations Available"
            ddlLocation.Items.Insert(0, "--Select--")
            pnlAssignMapUser.Visible = False
        Else
            ddlLocation.Items.Insert(1, "--All--")
        End If

    End Sub
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        getUserFloors()
    End Sub

    Private Sub getUserFloors()

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@FLR_CTY_ID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@FLR_LOC_ID", SqlDbType.NVarChar, 200)
        param(1).Value = ddlLocation.SelectedItem.Value
        ObjSubsonic.BindGridView(gvFloors, "GET_FLOOR_BYLOCATION", param)

        If gvFloors.Rows.Count > 0 Then
            lblmsg.Text = ""
            pnlUserMaps.Visible = True
            pnlAssignMapUser.Visible = False
            gvFloors.Visible = True
        End If

    End Sub

    Protected Sub gvFloors_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvFloors.PageIndexChanging
        gvFloors.PageIndex = e.NewPageIndex
        getUserFloors()
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnkPortal As LinkButton = DirectCast(sender, LinkButton)
        Dim info As String = lnkPortal.CommandArgument
        Dim arg As String() = New String(1) {}
        Dim splitter As Char() = {";"c}
        arg = info.Split(splitter)

        Dim flr_id As String
        Dim aur_id As String

        flr_id = arg(0).ToString
        aur_id = arg(1).ToString

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@FLR_ID", SqlDbType.NVarChar)
        param(0).Value = flr_id
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar)
        param(1).Value = aur_id

        ObjSubsonic.GetSubSonicExecute("DELETE_USERFLOOR_BYFLOOR_ID", param)
        getUserFloors()
    End Sub

    Protected Sub btnAddUserMap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUserMap.Click
        pnlUserMaps.Visible = False
        pnlAssignMapUser.Visible = True
        txtEmployeeId.Text = ""
        BindFloors()
    End Sub

    Private Sub BindFloors()
        ddlMaps.Items.Clear()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@FLR_CTY_ID", SqlDbType.NVarChar)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@FLR_LOC_ID", SqlDbType.NVarChar)
        param(1).Value = ddlLocation.SelectedItem.Value
        ObjSubsonic.Binddropdown(ddlMaps, "GET_FLOORMAPS", "FLR_NAME", "FLR_ID", param)

        If ddlMaps.Items.Count <= 1 Then
            lblmsg.Text = "No Floors Available To Map"
            ddlMaps.Items.Insert(0, "--Select--")
            pnlAssignMapUser.Visible = False
        Else
            ddlMaps.Items.Insert(1, "--All--")
        End If
    End Sub

    Protected Sub btnAssignMap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssignMap.Click
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@LOC_ID", SqlDbType.NVarChar)
        param(0).Value = ddlLocation.SelectedItem.Value
        param(1) = New SqlParameter("@FLR_ID", SqlDbType.NVarChar)
        param(1).Value = ddlMaps.SelectedItem.Value
        param(2) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar)
        param(2).Value = Trim(txtEmployeeId.Text)
        param(3) = New SqlParameter("@CTY_ID", SqlDbType.NVarChar)
        param(3).Value = ddlCity.SelectedItem.Value
        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("ASSIGN_USERMAPS", param)

        If ds.Tables(0).Rows(0).Item("Empstatus").ToString = "0" Then
            lblmsg.Text = "Employee Id does not exist."
        Else
            lblMsg.Text = "Map Assigned Successfully."
            ddlCity.ClearSelection()
            ddlLocation.ClearSelection()
            ddlMaps.ClearSelection()
            txtEmployeeId.Text = ""
        End If

    End Sub
End Class
