﻿Imports Microsoft.VisualBasic
Imports System
Imports System.IO
Imports System.Net
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports cls_OLEDB_postgresSQL
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Partial Class WorkSpace_SMS_Webfiles_repPropertyInsurance
    Inherits System.Web.UI.Page
    Dim filepath As String
    Dim strtxt As String
    Dim Subsonic As clsSubSonicCommonFunctions = New clsSubSonicCommonFunctions
    Dim strResponse As String = ""
    Dim strpoints As String = ""
    Dim strResponse1 As String = ""
    Dim fdate As DateTime
    Dim tdate As DateTime

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Bindmap(" ", " ")

        End If
        txtFdate.Attributes.Add("readonly", "readonly")
        txtTdate.Attributes.Add("readonly", "readonly")

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click

        fdate = CDate(txtFdate.Text)
        tdate = CDate(txtTdate.Text)
        If fdate > tdate Then
            ScriptManager.RegisterStartupScript(Me, [GetType](), "AlertCode", "alert('From Date should be less than to date');", True)
        Else
            Bindmap(fdate, tdate)

        End If

    End Sub


    Private Sub Bindmap(ByVal fdate As String, ByVal tdate As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PROPERTY_INSURANCE_REPORT")
        sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.String)
        sp.Command.AddParameter("@TODATE", txtTdate.Text, DbType.String)
        Dim dr As SqlDataReader = sp.GetReader
        Dim ds As New DataSet
        ds = sp.GetDataSet
       
        Dim rds As New ReportDataSource()
        rds.Name = "PropertyInsuranceDS"

        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Property_Mgmt/PropertyInsuranceReport.rdlc")
        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p1 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())
        ReportViewer1.LocalReport.SetParameters(p1)
        ReportViewer1.LocalReport.Refresh()

        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True

        
    End Sub
End Class
