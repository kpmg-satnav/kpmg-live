﻿Imports System.Data
Imports cls_OLEDB_postgresSQL
Partial Class WorkSpace_SMS_Webfiles_Spacetypemaster
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim objpgsql As New cls_OLEDB_postgresSQL

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            pnlid.Visible = False
            objsubsonic.Binddropdown(ddlcity, "usp_getActiveCities", "CTY_NAME", "CTY_CODE")       
            objsubsonic.Binddropdown(ddlspctype, "GET_SPACE_TYPE", "SPACE_TYPE", "SNO")
            cleardata()
        End If
    End Sub

    Private Sub cleardata()
        ddlBDG.Items.Clear()
        ddlTWR.Items.Clear()
        ddlFLR.Items.Clear()
        ddlWNG.Items.Clear()
        pnlid.Visible = False
        btnupdate.Visible = False
        spctype.Visible = False
        lblMsg.Text = ""
    End Sub

    Private Sub Get_Building(ByVal city As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "EFM_SRQ_BUILDING_CITY")
        sp.Command.AddParameter("@city", city, DbType.String)
        ddlBDG.DataSource = sp.GetDataSet()
        ddlBDG.DataTextField = "BDG_NAME"
        ddlBDG.DataValueField = "BDG_ADM_CODE"
        ddlBDG.DataBind()
        ddlBDG.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Protected Sub ddlBDG_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBDG.SelectedIndexChanged
        ddlTWR.Items.Clear()
        ddlFLR.Items.Clear()
        ddlWNG.Items.Clear()
        pnlid.Visible = False
        btnupdate.Visible = False
        spctype.Visible = False     
        Get_Tower(ddlBDG.SelectedItem.Value)
    End Sub

    Private Sub Get_Tower(ByVal bdgtemp As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_SRQ_TOWER")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        ddlTWR.DataSource = sp.GetDataSet()
        ddlTWR.DataTextField = "TWR_NAME"
        ddlTWR.DataValueField = "TWR_CODE"
        ddlTWR.DataBind()
        ddlTWR.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Protected Sub ddlTWR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTWR.SelectedIndexChanged
        ddlFLR.Items.Clear()
        ddlWNG.Items.Clear()
        pnlid.Visible = False
        btnupdate.Visible = False
        spctype.Visible = False
        Get_Floor(ddlBDG.SelectedItem.Value, ddlTWR.SelectedItem.Value)      
    End Sub

    Private Sub Get_Floor(ByVal bdgtemp As String, ByVal twrtemp As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "EFM_SRQ_FLOOR")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", twrtemp, DbType.String)
        ddlFLR.DataSource = sp.GetDataSet()
        ddlFLR.DataTextField = "FLR_NAME"
        ddlFLR.DataValueField = "FLR_CODE"
        ddlFLR.DataBind()
        ddlFLR.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Protected Sub ddlFLR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFLR.SelectedIndexChanged
        ddlWNG.Items.Clear()
        pnlid.Visible = False
        btnupdate.Visible = False
        spctype.Visible = False
        Get_Wing(ddlBDG.SelectedItem.Value, ddlTWR.SelectedItem.Value, ddlFLR.SelectedItem.Value)    
    End Sub

    Private Sub Get_Wing(ByVal bdgtemp As String, ByVal twrtemp As String, ByVal flrtemp As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "EFM_SRQ_WING")
        sp.Command.AddParameter("@BDG_ADM_CODE", bdgtemp, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", twrtemp, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", flrtemp, DbType.String)
        ddlWNG.DataSource = sp.GetDataSet()
        ddlWNG.DataTextField = "WNG_NAME"
        ddlWNG.DataValueField = "WNG_CODE"
        ddlWNG.DataBind()
        ddlWNG.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Protected Sub ddlcity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlcity.SelectedIndexChanged
        cleardata()
        Get_Building(ddlcity.SelectedItem.Value)
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        fillgrid()
        pnlid.Visible = True
    End Sub

    Private Sub fillgrid()
        pnlid.Visible = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "EFM_SPACE_GRID")
        sp.Command.AddParameter("@SPC_BDG_ID", ddlBDG.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SPC_TWR_ID", ddlTWR.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SPC_FLR_ID", ddlFLR.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SPC_WNG_ID", ddlWNG.SelectedItem.Value, DbType.String)
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
        If gvitems.Rows.Count > 0 Then
            btnupdate.Visible = True
            spctype.Visible = True
        Else
            btnupdate.Visible = False
            spctype.Visible = False
        End If
    End Sub

    Protected Sub btnupdate_Click(sender As Object, e As EventArgs) Handles btnupdate.Click
        lblMsg.Text = ""
        Dim d As Integer = gvitems.PageCount
        Dim texts As String() = New String(gvitems.PageSize - 1) {}
        Dim count As Integer = 0
        For Each row As GridViewRow In gvitems.Rows
            Dim lblspcid As Label = DirectCast(row.FindControl("lblspcid"), Label)
            Dim lblspacename As Label = DirectCast(row.FindControl("lblspacename"), Label)
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblMsg.Text = ""
            If chkSelect.Checked = True Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDATE_SPACE_SPCTYPE")
                sp.Command.AddParameter("@SPC_ID", lblspcid.Text, DbType.String)
                sp.Command.AddParameter("@SPACE_TYPE", ddlspctype.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@AUR_ID", Session("Uid").ToString().Trim(), DbType.String)
                sp.ExecuteScalar()
                count = count + 1
                UpdateSpaceType(lblspcid.Text, ddlspctype.SelectedItem.Value)
            End If
        Next
        If count <> 0 Then
            lblMsg.Text = "Space Type Updated Successfully For Space"
        Else
            lblMsg.Text = "Please Select Any Space From the Grid"
        End If
        fillgrid()
    End Sub

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        For Each r As GridViewRow In gvitems.Rows
            If DirectCast(r.FindControl("chkSelect"), CheckBox).Checked = True Then
                DirectCast(r.FindControl("chkSelect"), CheckBox).Checked = False
            Else
                DirectCast(r.FindControl("chkSelect"), CheckBox).Checked = True
            End If
        Next
    End Sub

    Protected Sub gvitems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        fillgrid()
    End Sub

    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~/Masters/MAS_Webfiles/frmMasSpaceMasters.aspx")
    End Sub
End Class
