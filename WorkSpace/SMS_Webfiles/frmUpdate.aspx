<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmUpdate.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmUpdate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <script defer src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>
    <script defer src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>
 <div>
        <table id="table1" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center">  <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False" ForeColor="Black">Space Connect
             <hr align="center" width="60%" /></asp:Label></td>
                        </tr>
                        </table>
                         <asp:Panel ID="PNLCONTAINER"  runat="server" Width="95%" Height="100%">
      
                <table id="table3"  cellspacing="0" cellpadding="0" width="95%" align="center">
                <tr>
                        <td style="height: 27px; width: 10px;">
                            <img  alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9"/></td>
                        <td width="100%" class="tableHEADER" style="height: 27px" align="left">
                            <strong>&nbsp;Update</strong>
                        </td>
                        <td style="height: 27px">
                            <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16"/></td>
                    </tr>
                    
                    
                         <tr>
                        <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
                        </td>
                        <td align="left">
                        
                        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center" Width="100%">
                                <p>
                                    <asp:Label ID="Label2" runat="server" CssClass="clsMessage" Visible="False">No requisitions for Changes</asp:Label></p>
                                &nbsp;
                                <asp:GridView ID="grdReqList" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    PageSize="20" Width="95%">
                                    <RowStyle HorizontalAlign="Left" />
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Requisition ID" HeaderStyle-CssClass="clstblHead">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="Hyperlink1" runat="server" NavigateUrl='<%# Eval("SRN_REQ_ID", "frmSpcModifyReq.aspx?RID={0}") %>'
                                                    Text='<%# Eval("SRN_REQ_ID") %>'></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="SRN_REQ_DT" DataFormatString="{0:d}" HeaderText="Requisition Date">
                                            <HeaderStyle CssClass="clstblHead" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SRN_REM" HeaderText="Remarks1" >
                                            <HeaderStyle CssClass="clstblHead" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="BDG_DESC1" HeaderText="Location">
                                            <HeaderStyle CssClass="clstblHead" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="BDG_DESC2" HeaderText="Location2" >
                                            <HeaderStyle CssClass="clstblHead" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="STA_NAME" HeaderText="Status">
                                            <HeaderStyle CssClass="clstblHead" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Request Type">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SRN_TYPE_ID" HeaderText="SRN_TYPE_ID3" >
                                            <HeaderStyle CssClass="clstblHead" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                <p>
                                    &nbsp;</p>
                                <p>
                                    &nbsp;</p>
                            </asp:Panel>
                        </td>
                        
                         <td background="../../Images/table_right_mid_bg.gif" style="width: 10px;">
                        </td>
                    </tr>
                        <tr>
                        <td style="width: 10px; height: 17px;">
                            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                    
                </table>
             
            </asp:Panel>
           
        </div>
</asp:Content>

