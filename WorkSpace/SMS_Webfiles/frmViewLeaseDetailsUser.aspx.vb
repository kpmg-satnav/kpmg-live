Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmViewLeaseDetailsUser
    Inherits System.Web.UI.Page
    Dim maintper As Decimal = 0
    Dim SERVPER As Decimal = 0
   
    Private Sub BindExtension()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LEASE_EXTENSION_USER1")
        sp.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        txtextfromdate.Text = ds.Tables(0).Rows(0).Item("FROM_DATE")
        txtexttodate.Text = ds.Tables(0).Rows(0).Item("TO_DATE")
        txtrent.Text = ds.Tables(0).Rows(0).Item("RENT_AMOUNT")
        txtsdep.Text = ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")
        txtremarks.Text = ds.Tables(0).Rows(0).Item("REMARKS")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
           
            Label1.Visible = False
            'txtrmremarks.ReadOnly = True
            'txthrremarks.ReadOnly = True
            txtvphrremarks.ReadOnly = True
            panld1.Visible = False
            pnlld2.Visible = False
            pnlld3.Visible = False
            BindPropertyType()
            BindProperty()
            BindLeaseType()
            ddlLesse.Enabled = False
            lblMsg.Visible = False
            ddlMode.SelectedValue = "2"
            ddlMode.Enabled = False
            ddlproptype.SelectedValue = "1"
            ddlproptype.Enabled = False
            ddlLeaseType.SelectedValue = "2"
            ddlLeaseType.Enabled = False
            txtentitle.ReadOnly = True
            ddlCity.Enabled = False
            ddlproperty.Enabled = False
            txtBuilding.ReadOnly = True
            txtprop3.ReadOnly = True
            txtLnumber.ReadOnly = True
            ddlLesse.Enabled = False
            txtstore.ReadOnly = True
            txtEscalationDate.ReadOnly = True
            txtInvestedArea.ReadOnly = True
            txtOccupiedArea.ReadOnly = True

            'added by praveen 
            txtsduty.ReadOnly = True
            txtregcharges.ReadOnly = True
            txtpfees.ReadOnly = True
            txtbrokerage.ReadOnly = True
            txtdg.ReadOnly = True
            txtfurniture.ReadOnly = True
            txtofcequip.ReadOnly = True
            txtservicetax.ReadOnly = True
            txtproptax.ReadOnly = True
            txtrentrev.ReadOnly = True
            txtimp.ReadOnly = True
            txtld1addr2.ReadOnly = True
            txtld1addr3.ReadOnly = True
            txtld1Pin.ReadOnly = True
            txtbrnch.ReadOnly = True
            'ends here

            ddlStatus.Enabled = False
            txtpay.ReadOnly = True
            ddlesctype.Enabled = False
            txtrcramt1.ReadOnly = True
            txtrcramt2.ReadOnly = True
            txtrcrfrmdate1.ReadOnly = True
            txtrcrfrmdate2.ReadOnly = True
            txtrcrtodate1.ReadOnly = True
            txtrcrtodate2.ReadOnly = True
            txtEmpAccNo.ReadOnly = True
            txtEmpBankName.ReadOnly = True
            txtEmpBranch.ReadOnly = True
            txtrcryfromdate.ReadOnly = True
            txtrcrytodate.ReadOnly = True
            panafteresc1.Visible = False
            panafteresc2.Visible = False
            BindLandlordstate()
            txtentitle.ReadOnly = True
            txtComments.ReadOnly = True
            txtldname.ReadOnly = True
            txtldaddr.ReadOnly = True
            txtPAN.ReadOnly = True
            txtldemail.ReadOnly = True
            txtmob.ReadOnly = True
            txtpfromdate.ReadOnly = True
            txtptodate.ReadOnly = True
            txtpmonthrent.ReadOnly = True
            txtpsecdep.ReadOnly = True
            ddlpaymentmode.Enabled = False
            txtAccNo.ReadOnly = True
            txtIBankName.ReadOnly = True
            txtDeposited.ReadOnly = True
            txtIFSC.ReadOnly = True
            txtld1name.ReadOnly = True
            txtld2addr.ReadOnly = True
            txtld2pan.ReadOnly = True
            txtld2email.ReadOnly = True
            txtld2mob.ReadOnly = True
            txtld2frmdate.ReadOnly = True
            txtld2todate.ReadOnly = True
            txtld2rent.ReadOnly = True
            txtld2sd.ReadOnly = True
            ddlld2mode.Enabled = False
            txtld2accno.ReadOnly = True
            txtld2IBankName.ReadOnly = True
            txtld2Deposited.ReadOnly = True
            txtld2IFSC.ReadOnly = True
            txtld3name.ReadOnly = True
            txtld3addr.ReadOnly = True
            txtld3pan.ReadOnly = True
            txtld3email.ReadOnly = True
            txtld3mob.ReadOnly = True
            txtld3fromdate.ReadOnly = True
            txtld3todate.ReadOnly = True
            txtld3rent.ReadOnly = True
            txtld3sd.ReadOnly = True
            ddlld3mode.Enabled = False
            txtLd3acc.ReadOnly = True
            txtld3IBankName.ReadOnly = True
            txtld3Deposited.ReadOnly = True
            txtld3ifsc.ReadOnly = True
            txtAgreedate.ReadOnly = True
            ddlpoa.Enabled = False
            ddlleaseld.Enabled = False
            txtagreeamt.ReadOnly = True
            txtregamt.ReadOnly = True
            txtsdate.ReadOnly = True
            txtedate.ReadOnly = True
            ddlagreeres.Enabled = False
            txtagreeregdate.ReadOnly = True
            txtagreesub.ReadOnly = True
            txtnotice.ReadOnly = True
            txtlock.ReadOnly = True
            txtesctodate1.ReadOnly = True
            txtescfromdate2.ReadOnly = True
            txtesctodate2.ReadOnly = True
            txtbrkamount.ReadOnly = True
            txtbrkname.ReadOnly = True
            txtbrkaddr.ReadOnly = True
            txtbrkpan.ReadOnly = True
            txtbrkremail.ReadOnly = True
            txtbrkmob.ReadOnly = True
            ddlesc.Enabled = False
            txtfirstesc.ReadOnly = True
            txtsecondesc.ReadOnly = True
            txtEmpAccNo.ReadOnly = True
            txtEmpBankName.ReadOnly = True
            txtEmpBranch.ReadOnly = True
            txtEmpRcryAmt.ReadOnly = True
            txtrcryfromdate.ReadOnly = True
            txtrcrytodate.ReadOnly = True
            txtPOAName.ReadOnly = True
            txtPOAMobile.ReadOnly = True
            txtPOAEmail.ReadOnly = True
            txtPOAAddress.ReadOnly = True
            Dim id As String = Request.QueryString("id")
            BindLeaseDetails(id)
            GetName()
            GetEntitle(id)
            BindEscalationDetails(id)
            BindAgreementDetails(id)
            BindBrokerageDetails(id)
            BindRecoveryDetails()
            BindRecoveryDetails1()
            BindLandLordDetails(id)
            ValidateHR1()
            BindGrid()
            Dim leaseext As Integer
            leaseext = Validleaseextension()
            If leaseext = 0 Then
                tr_vp_hr1.Visible = True
                BindExtension()
            Else
                tr_vp_hr1.Visible = False
            End If
            If ddlleaseld.SelectedItem.Value = "1" Then
                panld1.Visible = True
                pnlld2.Visible = False
                pnlld3.Visible = False

            ElseIf ddlleaseld.SelectedItem.Value = "2" Then
                panld1.Visible = True
                pnlld2.Visible = True
                pnlld3.Visible = False

            ElseIf ddlleaseld.SelectedItem.Value = "3" Then
                panld1.Visible = True
                pnlld2.Visible = True
                pnlld3.Visible = True

            End If
            If ddlesc.SelectedItem.Value = "Yes" Then
                If txtescfromdate2.Text <> "" And txtEscalationDate.Text <> "" Then
                    pnlesc1.Visible = True
                    pnlesc2.Visible = True
                ElseIf txtescfromdate2.Text = "" And txtEscalationDate.Text <> "" Then
                    pnlesc1.Visible = True
                    pnlesc2.Visible = False
                End If
            Else
                pnlesc1.Visible = False
                pnlesc2.Visible = False
            End If
            onetimecost()
            maintenancecost()

            Dim rent As Decimal = 0
            Dim esc1 As Decimal = 0
            Dim esc2 As Decimal = 0
            If txtservicetax.Text <> "" Then
                rent = CDbl(txtservicetax.Text)
            End If
            If txtmain.Text <> "" Then
                esc1 = CDbl(txtmain.Text)
            End If
            If txtInvestedArea.Text <> "" Then
                esc2 = CDbl(txtInvestedArea.Text)
            End If

            txttotalrent.Text = rent + esc1 + esc2
            'btnprint.Attributes.Add("onclick", "printPartOfPage('Div1')")
        End If

    End Sub
    Private Sub BindProperty()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_PROPERTY_LEASE")
     sp.Command.AddParameter("@dummy", 0, DbType.String)
        ddlproperty.DataSource = sp.GetDataSet()
        ddlproperty.DataTextField = "PN_NAME"
        ddlproperty.DataValueField = "BDG_ID"
        ddlproperty.DataBind()
        ddlproperty.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub
    Private Function Validleaseextension()
        Dim valid As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_LEASE_EXT")
        sp.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
        valid = sp.ExecuteScalar()
        Return valid
    End Function
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GET_EMPDETAILS_RMAPPROVAL")
        sp.Command.AddParameter("@LESSE_ID", txtstore.Text, DbType.String)
        gvEmpDetails.DataSource = sp.GetDataSet()
        gvEmpDetails.DataBind()
    End Sub
    Private Function GetLeaseStatus()
        Dim getstatus As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LEASE_STATUS")
        sp.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
        getstatus = sp.ExecuteScalar()
        Return getstatus
    End Function
    Private Sub BindLandlordstate()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTCTY_STATE")

        ddlstate.DataSource = sp.GetDataSet()
        ddlstate.DataTextField = "STE_NAME"
        ddlstate.DataValueField = "STE_CODE"
        ddlstate.DataBind()
        ddlstate.Items.Insert(0, New ListItem("--Select--", "--Select--"))

        ddlld2state.DataSource = sp.GetDataSet()
        ddlld2state.DataTextField = "STE_NAME"
        ddlld2state.DataValueField = "STE_CODE"
        ddlld2state.DataBind()
        ddlld2state.Items.Insert(0, New ListItem("--Select--", "--Select--"))

        ddlld3state.DataSource = sp.GetDataSet()
        ddlld3state.DataTextField = "STE_NAME"
        ddlld3state.DataValueField = "STE_CODE"
        ddlld3state.DataBind()
        ddlld3state.Items.Insert(0, New ListItem("--Select--", "--Select--"))

    End Sub
    Private Sub GetName()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"LEASE_GET_NAME_HR")
        sp.Command.AddParameter("@AUR_ID", txtstore.Text, DbType.String)
        sp.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            'lblrm.Text = ds.Tables(0).Rows(0).Item("RM_NAME")
            'lblhr.Text = ds.Tables(0).Rows(0).Item("HR_NAME")
            lblRMname.Text = ds.Tables(0).Rows(0).Item("vp_HR_NAME")
            'lblrmdate.Text = ds.Tables(0).Rows(0).Item("RM_DATE")
            'lblhrdate.Text = ds.Tables(0).Rows(0).Item("HR_DATE")
            lblvphrdate.Text = ds.Tables(0).Rows(0).Item("VP_HR_DATE")
        End If
        
    End Sub
    Private Sub BindRecoveryDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"LEASE_EMPLOYEE_ACCOUNT_DETAILS")
            sp.Command.AddParameter("@AUR_ID", txtstore.Text, DbType.String)
            Dim DS As New DataSet()
            DS = sp.GetDataSet()
            If DS.Tables(0).Rows.Count > 0 Then
                txtEmpAccNo.Text = DS.Tables(0).Rows(0).Item("AUR_ACCOUNT_NUMBER")
                txtEmpBankName.Text = DS.Tables(0).Rows(0).Item("AUR_BANK_NAME")
                txtEmpBranch.Text = DS.Tables(0).Rows(0).Item("AUR_BRANCH_NAME")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Function ValidateLandlord()
        Dim landlord As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALIDATE_LANDLORD")
        sp.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
        landlord = sp.ExecuteScalar()
        Return landlord
    End Function
    Private Function ValidateBroker(ByVal id As String)
        Dim validbrk As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_VALID_BROKER_DETAILS")
        sp.Command.AddParameter("LEASE", id, DbType.String)
        validbrk = sp.ExecuteScalar()
        Return validbrk
    End Function
    Private Function BindLandlord2(ByVal id As String)
        Dim l2 As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_LANDLORD2")
        sp.Command.AddParameter("@LEASE", id, DbType.String)
        l2 = sp.ExecuteScalar()
        Return l2
    End Function
    Private Function BindLandlord3(ByVal id As String)
        Dim l3 As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_LANDLORD3")
        sp.Command.AddParameter("@LEASE", id, DbType.String)
        l3 = sp.ExecuteScalar()
        Return l3
    End Function
    Private Sub GetEntitle(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_GETENTITLE_USER_RM")
            sp.Command.AddParameter("@LEASE", id, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                lblmaxrent.Text = ds.Tables(0).Rows(0).Item("RENT_AMOUNT")
                lblmaxsd.Text = ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindPropertyType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTPROPTYPE")
            sp.Command.AddParameter("@dummy", Session("UID"), DbType.String)
            ddlproptype.DataSource = sp.GetDataSet()
            ddlproptype.DataTextField = "PN_PROPERTYTYPE"
            ddlproptype.DataValueField = "PN_TYPEID"
            ddlproptype.DataBind()
            ddlproptype.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLeaseType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GETACTIVE_LEASETYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlLeaseType.DataSource = sp3.GetDataSet()
        ddlLeaseType.DataTextField = "PN_LEASE_TYPE"
        ddlLeaseType.DataValueField = "PN_LEASE_ID"
        ddlLeaseType.DataBind()
        ddlLeaseType.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindLeaseDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_PN_LEASES_GETDETAILS")
            sp.Command.AddParameter("@LEASE_NAME", id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            ddlCity.Items.Clear()
            ddlCity.Items.Insert(0, New ListItem(ds.Tables(0).Rows(0).Item("CITY"), "0"))
            ddlproperty.ClearSelection()
            ddlproperty.Items.FindByValue(ds.Tables(0).Rows(0).Item("PROPERTY_ADDRESS1")).Selected = True
            txtBuilding.Text = ds.Tables(0).Rows(0).Item("PROPERTY_ADDRESS2")
            txtprop3.Text = ds.Tables(0).Rows(0).Item("STATE")
            txtregion.Text = ds.Tables(0).Rows(0).Item("REGION")
            txtsduty.Text = ds.Tables(0).Rows(0).Item("STAMP_DUTY")
            txtregcharges.Text = ds.Tables(0).Rows(0).Item("REGISTRATION_CHARGES")
            txtpfees.Text = ds.Tables(0).Rows(0).Item("PROFESSIONAL_FEES")
            txtbrokerage.Text = ds.Tables(0).Rows(0).Item("BROKERAGE_AMOUNT")
            txtbasic.Text = ds.Tables(0).Rows(0).Item("BASIC_RENT")
            
            txtdg.Text = ds.Tables(0).Rows(0).Item("DG_BACKUP_CHARGES")


            txtproptax.Text = ds.Tables(0).Rows(0).Item("PROPERTY_TAX")
            txtimp.Text = ds.Tables(0).Rows(0).Item("LEASEHOLDIMPROVEMENTS")
            txtrentrev.Text = ds.Tables(0).Rows(0).Item("RENT_REVISION")


            txttenure.Text = ds.Tables(0).Rows(0).Item("TENURE_AGREEMENT")
            txtfurniture.Text = ds.Tables(0).Rows(0).Item("FURNITURE")
            txtofcequip.Text = ds.Tables(0).Rows(0).Item("OFFICE_EQUIPMENTS")

            txtLnumber.Text = ds.Tables(0).Rows(0).Item("CTS_NUMBER")
            ddlLesse.Items.Clear()
            ddlLesse.Items.Insert(0, ds.Tables(0).Rows(0).Item("LESSE_ID"))
            txtstore.Text = ds.Tables(0).Rows(0).Item("LESSE")    
            txtInvestedArea.Text = ds.Tables(0).Rows(0).Item("LEASE_RENT")
            txtOccupiedArea.Text = ds.Tables(0).Rows(0).Item("BUILTUP_AREA")
            bindpropertytax()
            txtmain1.Text = maintper * CDbl(txtOccupiedArea.Text)
            txtservicetax.Text = SERVPER * CDbl(txtOccupiedArea.Text)
            ddlStatus.ClearSelection()
            ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("LEASE_STA_ID")).Selected = True
            txtpay.Text = ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")

            txtpincode.Text = ds.Tables(0).Rows(0).Item("PINCODE")
            ddlpoa.ClearSelection()
            ddlpoa.Items.FindByValue(ds.Tables(0).Rows(0).Item("POA")).Selected = True
            If ddlpoa.SelectedValue = "Yes" Then
                panPOA.Visible = True

            Else
                panPOA.Visible = False
            End If
            ddlleaseld.ClearSelection()
            ddlleaseld.Items.FindByValue(ds.Tables(0).Rows(0).Item("LANDLORDS")).Selected = True

            ddlesc.ClearSelection()
            ddlesc.Items.FindByValue(ds.Tables(0).Rows(0).Item("LEASE_ESCALATION")).Selected = True

            Dim stat1 As String = ds.Tables(0).Rows(0).Item("LEASE_ESCALATION")
            If stat1 = "Yes" Then
                pnlesc1.Visible = True
                pnlesc2.Visible = True
            Else
                pnlesc1.Visible = False
                pnlesc2.Visible = False
            End If
            ddlesctype.ClearSelection()
            ddlesctype.Items.FindByValue(ds.Tables(0).Rows(0).Item("ESCALATION")).Selected = True
            txtentitle.Text = ds.Tables(0).Rows(0).Item("ENTITLE_LEASE_AMOUNT")
            txtComments.Text = ds.Tables(0).Rows(0).Item("LEASE_COMMENTS")
            'txtrmremarks.Text = ds.Tables(0).Rows(0).Item("RM_REMARKS")
            'txthrremarks.Text = ds.Tables(0).Rows(0).Item("HR_REMARKS")
            txtvphrremarks.Text = ds.Tables(0).Rows(0).Item("VP_HR_REMARKS")

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub bindpropertytax()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_PROEPRTY_TAX")
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            maintper = ds.Tables(0).Rows(0).Item("MAINTENANCE_CHARGE")
            SERVPER = ds.Tables(0).Rows(0).Item("SERVICE_TAX")
        End If
    End Sub
    Private Sub BindEscalationDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_PN_ESCALATION_DETAILS_GETDETAILS")
            sp.Command.AddParameter("@LEASE", id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            txtEscalationDate.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATED_FROM_DATE1")
            txtesctodate1.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATED_TO_DATE1")
            txtescfromdate2.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATED_FROM_DATE2")
            txtesctodate2.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATED_TO_DATE2")
            txtfirstesc.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATED_AMOUNT1")
            txtsecondesc.Text = ds.Tables(0).Rows(0).Item("LEASE_ESCALATED_AMOUNT2")
            txtEmpAccNo.Text = ds.Tables(0).Rows(0).Item("EMPLOYEE_ACCOUNT_NUMBER")
            txtEmpBankName.Text = ds.Tables(0).Rows(0).Item("EMPLOYEE_BANK_NAME")
            txtEmpBranch.Text = ds.Tables(0).Rows(0).Item("EMPLOYEE_BRANCH_NAME")
            txtEmpRcryAmt.Text = ds.Tables(0).Rows(0).Item("RECOVERY_AMOUNT")
            txtrcrytodate.Text = ds.Tables(0).Rows(0).Item("RECOVERY_TODATE")
            txtrcryfromdate.Text = ds.Tables(0).Rows(0).Item("RECOVERY_FROMDATE")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLandLordDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_PN_LANDLORDS_GETDETAILS")
            sp.Command.AddParameter("@LEASE_NAME", id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtldname.Text = ds.Tables(0).Rows(0).Item("LANDLORD_NAME")
                txtldaddr.Text = ds.Tables(0).Rows(0).Item("LANDLORD_aDDRESS")
                txtPAN.Text = ds.Tables(0).Rows(0).Item("LANDLORD_PAN")
                txtldemail.Text = ds.Tables(0).Rows(0).Item("LANDLORD_EMAIL")
                txtmob.Text = ds.Tables(0).Rows(0).Item("LANDLORD_MOBILE")
                txtpfromdate.Text = ds.Tables(0).Rows(0).Item("FROM_DATE")
                txtptodate.Text = ds.Tables(0).Rows(0).Item("TO_DATE")
                txtpmonthrent.Text = ds.Tables(0).Rows(0).Item("RENT_AMOUNT")
                txtpsecdep.Text = ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT")
                txtld1addr2.Text = ds.Tables(0).Rows(0).Item("LANDLORD_ADDERSS2")
                txtld1addr3.Text = ds.Tables(0).Rows(0).Item("LANDLORD_ADDRESS3")
                txtld1Pin.Text = ds.Tables(0).Rows(0).Item("LANDLORD_PIN")
                ddlld1city.Text = ds.Tables(0).Rows(0).Item("LANDLORD_CITY")
                ddlstate.ClearSelection()
                ddlstate.Items.FindByValue(ds.Tables(0).Rows(0).Item("LANDLORD_STATE")).Selected = True
                ddlpaymentmode.ClearSelection()
                ddlpaymentmode.Items.FindByValue(ds.Tables(0).Rows(0).Item("PAYMENT_MODE")).Selected = True
                txtL12Accno.Text = ds.Tables(0).Rows(0).Item("ACCOUNT_NUMBER")
                txtAccNo.Text = ds.Tables(0).Rows(0).Item("ACCOUNT_NUMBER")
                txtIBankName.Text = ds.Tables(0).Rows(0).Item("ACCOUNT_NUMBER")
                txtBankName.Text = ds.Tables(0).Rows(0).Item("DEPOSITED_BANK")
                txtDeposited.Text = ds.Tables(0).Rows(0).Item("DEPOSITED_BANK")
                txtIFSC.Text = ds.Tables(0).Rows(0).Item("IFSC_CODE")
                txtbrnch.Text = ds.Tables(0).Rows(0).Item("BRANCH_NAME")
                Dim paymode As String = ds.Tables(0).Rows(0).Item("PAYMENT_MODE")
                If paymode = "1" Then
                    panel1.Visible = True
                    panel2.Visible = False
                    panell12.Visible = False

                ElseIf paymode = "2" Then
                    panel1.Visible = False
                    panel2.Visible = False
                    panell12.Visible = True
                Else
                    panel1.Visible = False
                    panel2.Visible = True
                    panell12.Visible = False
                End If

            End If
            If ds.Tables(1).Rows.Count > 0 Then
                txtld1name.Text = ds.Tables(1).Rows(0).Item("LANDLORD_NAME")
                txtld2addr.Text = ds.Tables(1).Rows(0).Item("LANDLORD_aDDRESS")
                txtld2pan.Text = ds.Tables(1).Rows(0).Item("LANDLORD_PAN")
                txtld2email.Text = ds.Tables(1).Rows(0).Item("LANDLORD_EMAIL")
                txtld2mob.Text = ds.Tables(1).Rows(0).Item("LANDLORD_MOBILE")
                txtld2frmdate.Text = ds.Tables(1).Rows(0).Item("FROM_DATE")
                txtld2todate.Text = ds.Tables(1).Rows(0).Item("TO_DATE")
                txtld2rent.Text = ds.Tables(1).Rows(0).Item("RENT_AMOUNT")
                txtld2sd.Text = ds.Tables(1).Rows(0).Item("SECURITY_DEPOSIT")
                ddlld2mode.ClearSelection()
                ddlld2mode.Items.FindByValue(ds.Tables(1).Rows(0).Item("PAYMENT_MODE")).Selected = True
               
                txtl22accno.Text = ds.Tables(1).Rows(0).Item("ACCOUNT_NUMBER")
                txtld2accno.Text = ds.Tables(1).Rows(0).Item("ACCOUNT_NUMBER")
                txtld2IBankName.Text = ds.Tables(1).Rows(0).Item("ACCOUNT_NUMBER")
                txtld2bankname.Text = ds.Tables(1).Rows(0).Item("DEPOSITED_BANK")
                txtld2Deposited.Text = ds.Tables(1).Rows(0).Item("DEPOSITED_BANK")
                txtld2IFSC.Text = ds.Tables(1).Rows(0).Item("IFSC_CODE")
                txtl2brnchname.Text = ds.Tables(1).Rows(0).Item("BRANCH_NAME")
                txtld2addr2.Text = ds.Tables(1).Rows(0).Item("LANDLORD_ADDERSS2")
                txtld2addr3.Text = ds.Tables(1).Rows(0).Item("LANDLORD_ADDRESS3")
                txtld2Pin.Text = ds.Tables(1).Rows(0).Item("LANDLORD_PIN")
                
                ddlld2city.Text = ds.Tables(1).Rows(0).Item("LANDLORD_CITY")
                ddlld2state.ClearSelection()
                ddlld2state.Items.FindByValue(ds.Tables(1).Rows(0).Item("LANDLORD_STATE")).Selected = True
                Dim paymode1 As String = ds.Tables(1).Rows(0).Item("PAYMENT_MODE")
                If paymode1 = "1" Then
                    panel3.Visible = False
                    panel4.Visible = False
                    pnll22.Visible = False
                ElseIf paymode1 = "2" Then
                    panel3.Visible = False
                    panel4.Visible = False
                    pnll22.Visible = True
                Else
                    panel3.Visible = False
                    panel4.Visible = True
                    pnll22.Visible = False
                End If
            End If
            If ds.Tables(2).Rows.Count > 0 Then

                txtld3name.Text = ds.Tables(2).Rows(0).Item("LANDLORD_NAME")
                txtld3addr.Text = ds.Tables(2).Rows(0).Item("LANDLORD_aDDRESS")
                txtld3pan.Text = ds.Tables(2).Rows(0).Item("LANDLORD_PAN")
                txtld3email.Text = ds.Tables(2).Rows(0).Item("LANDLORD_EMAIL")
                txtld3mob.Text = ds.Tables(2).Rows(0).Item("LANDLORD_MOBILE")
                txtld3fromdate.Text = ds.Tables(2).Rows(0).Item("FROM_DATE")
                txtld3todate.Text = ds.Tables(2).Rows(0).Item("TO_DATE")
                txtld3rent.Text = ds.Tables(2).Rows(0).Item("RENT_AMOUNT")
                txtld3sd.Text = ds.Tables(2).Rows(0).Item("SECURITY_DEPOSIT")
                ddlld3mode.ClearSelection()
                ddlld3mode.Items.FindByValue(ds.Tables(2).Rows(0).Item("PAYMENT_MODE")).Selected = True
               
                txtl32accno.Text = ds.Tables(1).Rows(0).Item("ACCOUNT_NUMBER")
                txtLd3acc.Text = ds.Tables(2).Rows(0).Item("ACCOUNT_NUMBER")
                txtld3IBankName.Text = ds.Tables(2).Rows(0).Item("ACCOUNT_NUMBER")
                txtld3bankname.Text = ds.Tables(1).Rows(0).Item("DEPOSITED_BANK")
                txtld3Deposited.Text = ds.Tables(2).Rows(0).Item("DEPOSITED_BANK")
                txtld3ifsc.Text = ds.Tables(2).Rows(0).Item("IFSC_CODE")
                txtl3brnch.Text = ds.Tables(2).Rows(0).Item("BRANCH_NAME")
                txtld3addr2.Text = ds.Tables(2).Rows(0).Item("LANDLORD_ADDERSS2")
                txtld3addr3.Text = ds.Tables(2).Rows(0).Item("LANDLORD_ADDRESS3")
                txtld3Pin.Text = ds.Tables(2).Rows(0).Item("LANDLORD_PIN")
               
                ddlld3city.Text = ds.Tables(2).Rows(0).Item("LANDLORD_CITY")
                ddlld3state.ClearSelection()
                ddlld3state.Items.FindByValue(ds.Tables(2).Rows(0).Item("LANDLORD_STATE")).Selected = True
                Dim paymode2 As String = ds.Tables(2).Rows(0).Item("PAYMENT_MODE")
                If paymode2 = "1" Then
                    panel5.Visible = False
                    panel6.Visible = False
                    pnll32.Visible = False
                ElseIf paymode2 = "2" Then
                    panel5.Visible = False
                    panel6.Visible = False
                    pnll32.Visible = True
                Else
                    panel5.Visible = False
                    panel6.Visible = True
                    pnll32.Visible = False
                End If


            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindAgreementDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_PN_AGREEMENT_DETAILS_GETDETAILS")
            sp.Command.AddParameter("@LEASE_NAME", id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtAgreedate.Text = ds.Tables(0).Rows(0).Item("AGREEMENT_EXECUTION_DATE")

                txtagreeamt.Text = ds.Tables(0).Rows(0).Item("AMOUNT_OF_STAMP_DUTY_PAID")
                txtregamt.Text = ds.Tables(0).Rows(0).Item("REGISTRATION_AMOUNT")
                txtsdate.Text = ds.Tables(0).Rows(0).Item("EFFECTIVE_AGREEMENT_DATE")
                txtedate.Text = ds.Tables(0).Rows(0).Item("EXPIRY_AGREEMENT_DATE")
                ddlagreeres.ClearSelection()
                ddlagreeres.Items.FindByValue(ds.Tables(0).Rows(0).Item("AGREEMENT_REGISTERED")).Selected = True
                txtagreeregdate.Text = ds.Tables(0).Rows(0).Item("REGISTRATION_DATE")
                txtagreesub.Text = ds.Tables(0).Rows(0).Item("SUB_REGISTARS_OFFICE_NAME")
                txtPOAName.Text = ds.Tables(0).Rows(0).Item("POA_NAME")
                txtPOAMobile.Text = ds.Tables(0).Rows(0).Item("POA_MOBILE")
                txtPOAEmail.Text = ds.Tables(0).Rows(0).Item("POA_EMAIL")
                txtPOAAddress.Text = ds.Tables(0).Rows(0).Item("POA_ADDRESS")
                Dim res As String = ds.Tables(0).Rows(0).Item("AGREEMENT_REGISTERED")
                If res = "Yes" Then
                    trregagree.Visible = True
                Else
                    trregagree.Visible = False
                End If
                txtnotice.Text = ds.Tables(0).Rows(0).Item("TERMINATION_NOTICE")
                txtlock.Text = ds.Tables(0).Rows(0).Item("LOCKIN_PERIOD_OF_AGREEMENT")
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindBrokerageDetails(ByVal id As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_PN_BROKERAGE_DETAILS_GETDETAILS")
            sp.Command.AddParameter("@LEASE_NAME", id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtbrkamount.Text = ds.Tables(0).Rows(0).Item("AMOUNT_OF_BROKERAGE_PAID")
                txtbrkname.Text = ds.Tables(0).Rows(0).Item("BROKER_NAME")
                txtbrkaddr.Text = ds.Tables(0).Rows(0).Item("BROKER_aDDRESS")
                txtbrkpan.Text = ds.Tables(0).Rows(0).Item("PAN_NO")
                txtbrkremail.Text = ds.Tables(0).Rows(0).Item("EMAIL")
                txtbrkmob.Text = ds.Tables(0).Rows(0).Item("MOBILE_NO")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Clear_Recovery_Details()
        txtEmpBankName.Text = ""
        txtEmpBranch.Text = ""
        txtEmpAccNo.Text = ""
        txtEmpRcryAmt.Text = ""
        txtrcrytodate.Text = Date.Today
        txtrcryfromdate.Text = Date.Today
    End Sub
    Private Sub BindRecoveryDetails1()
        txtpfromdate.Text = txtsdate.Text
        txtptodate.Text = txtedate.Text
        txtld2frmdate.Text = txtsdate.Text
        txtld2todate.Text = txtedate.Text
        txtld3fromdate.Text = txtsdate.Text
        txtld3todate.Text = txtedate.Text
        Dim dt As DateTime = Convert.ToDateTime(txtsdate.Text).ToString("dd-MMM-yyyy")
        txtrcryfromdate.Text = txtsdate.Text
        If ddlesc.SelectedItem.Value = "Yes" Then
            txtrcrytodate.Text = dt.AddYears(1).ToString("dd-MMM-yyyy")
        Else
            txtrcrytodate.Text = txtedate.Text
        End If
        txtrcrfrmdate1.Text = txtEscalationDate.Text
        txtrcrtodate1.Text = txtesctodate1.Text
        txtrcrfrmdate2.Text = txtescfromdate2.Text
        txtrcrtodate2.Text = txtesctodate2.Text
        Dim Interest As Double = ((CDbl(txtpay.Text) * 9) / 100) / 12
        txtstore1.Text = Interest

        If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
            txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
            txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
            panrecwiz.Visible = True
            panafteresc1.Visible = True

        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
            txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
            panrecwiz.Visible = True
            txtrcramt1.Text = ""
            panafteresc1.Visible = False
        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
            panrecwiz.Visible = False

            If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) > CDbl(txtentitle.Text) Then
                panafteresc1.Visible = True
                txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
            Else
                panafteresc1.Visible = False
                txtrcramt1.Text = ""
            End If
        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
            Clear_Recovery_Details()
            panrecwiz.Visible = False
            panafteresc1.Visible = False
        End If
    End Sub
    Private Sub ValidateHR1()
        Dim getstatus As Integer
        getstatus = GetLeaseStatus()

        If getstatus = "1001" Then
            'rm.Visible = False
            'tr_HR.Visible = False
            tr_VP_HR.Visible = False
        ElseIf getstatus = "1003" Then
            'rm.Visible = True
            'tr_HR.Visible = False
            tr_VP_HR.Visible = False

        ElseIf getstatus = "1007" Then
            'rm.Visible = True
            'tr_HR.Visible = True
            tr_VP_HR.Visible = False
        ElseIf getstatus = "1005" Then
            'rm.Visible = True
            'tr_HR.Visible = True
            tr_VP_HR.Visible = True
        End If
    End Sub
    Private Function ValidateLeaseExtension()
        Dim Valid As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_LEASE_EXTENSION")
        sp.Command.AddParameter("@LEASE", Request.QueryString("id"), DbType.String)
        Valid = sp.ExecuteScalar()
        Return Valid
    End Function
    Protected Sub btn3prev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn3prev.Click

        panbrk.Visible = True
        panPOA.Visible = False
        panld1.Visible = False
        pnlld2.Visible = False
        pnlld3.Visible = False
        panwiz1.Visible = False
        btn2Next.Visible = True
        btn2prev.Visible = True
        btn3prev.Visible = False
        'btn3finish.Visible = False
        btn1Next.Visible = False
        'rm.Visible = False
        'tr_HR.Visible = False
        tr_VP_HR.Visible = False
        panwiz3.Visible = True
        If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
            txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
            txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
            panrecwiz.Visible = True
            panafteresc1.Visible = True
        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) > CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "No" Then
            txtEmpRcryAmt.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) - CDbl(txtentitle.Text)
            panrecwiz.Visible = True
            txtrcramt1.Text = ""
            panafteresc1.Visible = False
        ElseIf (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text)) < CDbl(txtentitle.Text) And ddlesc.SelectedItem.Value = "Yes" Then
            panrecwiz.Visible = False
            If (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) > CDbl(txtentitle.Text) Then
                panafteresc1.Visible = True
                txtrcramt1.Text = (CDbl(txtstore1.Text) + CDbl(txtInvestedArea.Text) + CDbl(txtfirstesc.Text)) - CDbl(txtentitle.Text)
            Else
                panafteresc1.Visible = False
                txtrcramt1.Text = ""
            End If
        Else
            Clear_Recovery_Details()
            panrecwiz.Visible = False
            panafteresc1.Visible = False
        End If
    End Sub



    Private Sub onetimecost()

        Dim sduty As Decimal = 0
        Dim regcharges As Decimal = 0
        Dim professionalfees As Decimal = 0
        Dim brokeragefees As Decimal = 0

        If txtsduty.Text = "" Then
            sduty = 0
        Else
            sduty = CDbl(txtsduty.Text)
        End If

        If txtregcharges.Text = "" Then
            regcharges = 0
        Else
            regcharges = CDbl(txtregcharges.Text)
        End If

        If txtpfees.Text = "" Then
            professionalfees = 0
        Else
            professionalfees = CDbl(txtpfees.Text)
        End If

        If txtbrokerage.Text = "" Then
            brokeragefees = 0
        Else
            brokeragefees = CDbl(txtbrokerage.Text)
        End If

        txtbasic.Text = sduty + regcharges + professionalfees + brokeragefees




    End Sub
    Private Sub maintenancecost()
        Dim dgbackup As Decimal = 0
        Dim furniture As Decimal = 0
        Dim office As Decimal = 0
        Dim maintenance As Decimal = 0

        If txtdg.Text = "" Then
            dgbackup = 0
        Else
            dgbackup = CDbl(txtdg.Text)
        End If

        If txtfurniture.Text = "" Then
            furniture = 0
        Else
            furniture = CDbl(txtfurniture.Text)
        End If

        If txtofcequip.Text = "" Then
            office = 0
        Else
            office = CDbl(txtofcequip.Text)
        End If

        If txtmain1.Text = "" Then
            maintenance = 0
        Else
            maintenance = CDbl(txtmain1.Text)
        End If

        txtmain.Text = dgbackup + furniture + office + maintenance
    End Sub


  
End Class
