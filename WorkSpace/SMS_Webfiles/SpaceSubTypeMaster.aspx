﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script defer src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script defer src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>

    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/colorpicker.min.css" rel="stylesheet" />
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

    </script>


</head>
<body data-ng-controller="SpaceSubTypeController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Space Masters" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Space Sub Type Master</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form role="form" id="form1" name="frmSpaceSubType" data-valid-submit="Save()" novalidate>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Space Sub Type Code<span style="color: red;">*</span></label>
                                    <div class="col-md-12">
                                        <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()" data-ng-class="{'has-error': frmSpaceSubType.$submitted && frmSpaceSubType.SST_CODE.$invalid}">
                                            <input id="SST_CODE" name="SST_CODE" type="text" data-ng-model="SpaceSubType.SST_CODE" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" maxlength="15" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" data-ng-hide="" />
                                            <span class="error" style="color: red" data-ng-show="frmSpaceSubType.$submitted && frmSpaceSubType.SST_CODE.$invalid">Please enter valid sub type code </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Space Sub Type Name <span style="color: red;">*</span></label>
                                    <div class="col-md-12">
                                        <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()" data-ng-class="{'has-error': frmSpaceSubType.$submitted && frmSpaceSubType.SST_NAME.$invalid}">
                                            <input id="SST_NAME" name="SST_NAME" type="text" data-ng-model="SpaceSubType.SST_NAME" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" maxlength="50" class="form-control" autofocus required="" />
                                            <span class="error" style="color: red" data-ng-show="frmSpaceSubType.$submitted && frmSpaceSubType.SST_NAME.$invalid">Please enter valid space sub type name </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Space Type Name<span style="color: red;">*</span></label>
                                    <div class="col-md-12">
                                        <div data-ng-class="{'has-error': frmSpaceSubType.$submitted && frmSpaceSubType.SST_SPC_TYPE.$invalid}">
                                            <select id="SST_SPC_TYPE" name="SST_SPC_TYPE" data-ng-model="SpaceSubType.SST_SPC_TYPE" class="form-control" data-live-search="true" required>
                                                <option value="">--Select--</option>
                                                <option data-ng-repeat="Spc in SpaceTypeList" value="{{Spc.SPC_TYPE_CODE}}">{{Spc.SPC_TYPE_NAME}}</option>
                                            </select>
                                            <span class="error" data-ng-show="frmSpaceSubType.$submitted && frmSpaceSubType.SST_SPC_TYPE.$invalid" style="color: red">Please Select valid Space Type Name </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Select Color<span style="color: red;">*</span></label>
                                    <div class="col-md-12" data-ng-class="{'has-error': frmSpaceSubType.$submitted && frmSpaceSubType.SST_COLOR.$invalid}" onmouseover="Tip('Please Select Color')" onmouseout="UnTip()">
                                        <input colorpicker id="Text1" type="text" name="SST_COLOR" maxlength="50" data-ng-model="SpaceSubType.SST_COLOR" class="form-control" required />&nbsp;                                                               
                                            <span class="error" data-ng-show="frmSpaceSubType.$submitted && frmSpaceSubType.SST_COLOR.$invalid" style="color: red">Please Enter color </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Remarks</label>
                                    <div class="col-md-12">
                                        <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                            <textarea id="SST_REM" style="height: 30%" name="SST_REM" data-ng-model="SpaceSubType.SST_REM" class="form-control" maxlength="500"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div data-ng-show="ActionStatus==1">
                                        <label class="col-md-12 control-label">Status<span style="color: red;">*</span></label>
                                        <div class="col-md-12">
                                            <select id="SST_STA_ID" name="SST_STA_ID" data-ng-model="SpaceSubType.SST_STA_ID" class="form-control">
                                                <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                    <input type="button" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                    <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row" style="padding-left: 30px;">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 230px; width: 100%;" class="ag-blue"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer src="../../Scripts/bootstrap-colorpicker-module.min.js"></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "colorpicker.module"]);

        app.service('SpaceSubTypeService', function ($http, $q) {
            var deferred = $q.defer();

            //Bind Grid
            this.BindGrid = function () {
                deferred = $q.defer();
                return $http.get('../../api/SpaceSubType/BindGrid')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

            this.GetSpaceTypes = function () {
                deferred = $q.defer();

                return $http.get('../../api/SpaceSubType/GetSpaceTypes')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });

            }

            //SAVE
            this.saveSpaceSubType = function (repeat) {
                deferred = $q.defer();
                return $http.post('../../api/SpaceSubType/Save', repeat)
                    .then(function (response) {
                        deferred.resolve(response);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

            //UPDATE BY ID
            this.updateSpaceSubType = function (repeat) {
                console.log(repeat);
                deferred = $q.defer();
                return $http.post('../../api/SpaceSubType/Update', repeat)
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

        });

        app.controller('SpaceSubTypeController', function ($scope, $q, SpaceSubTypeService, $http, $timeout) {

            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.SpaceSubType = {};
            $scope.SpaceTypeList = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;

            //TO SAVE THE DATA
            $scope.Save = function () {
                if ($scope.IsInEdit) {
                    SpaceSubTypeService.updateSpaceSubType($scope.SpaceSubType).then(function (repeat) {
                        var updatedobj = {};
                        angular.copy($scope.SpaceSubType, updatedobj)
                        $scope.gridata.unshift(updatedobj);
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Updated Successfully";


                        SpaceSubTypeService.BindGrid().then(function (data) {
                            $scope.gridata = data;
                            //$scope.createNewDatasource();
                            $scope.gridOptions.api.setRowData(data);
                        }, function (error) {
                            console.log(error);
                        });

                        $scope.IsInEdit = false;
                        $scope.ClearData();
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        //setTimeout(function () {
                        //    $scope.$apply(function () {                      
                        //        showNotification('success', 8, 'bottom-right', $scope.Success);
                        //    });
                        //}, 700);
                    }, function (error) {
                    })
                }
                else {
                    $scope.SpaceSubType.SST_STA_ID = "1";
                    SpaceSubTypeService.saveSpaceSubType($scope.SpaceSubType).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Inserted Successfully";
                        var savedobj = {};
                        angular.copy($scope.SpaceSubType, savedobj)
                        $scope.gridata.unshift(savedobj);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                        $scope.SpaceSubType = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                        showNotification('error', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                    });
                }
            }

            //for GridView
            var columnDefs = [
                { headerName: "Space Sub Type Code", field: "SST_CODE", width: 25, cellClass: 'grid-align', suppressMenu: true },
                { headerName: "Space Sub Type Name", field: "SST_NAME", width: 30, cellClass: 'grid-align' },
                {
                    headerName: "Color", field: "SST_COLOR", width: 10, suppressMenu: true, cellClass: 'grid-align', cellStyle: function (params) {
                        return { color: params.value, 'background-color': params.value };

                    }
                },
                { headerName: "Status", template: "{{ShowStatus(data.SST_STA_ID)}}", width: 20, cellClass: 'grid-align', suppressMenu: true },
                { headerName: "Edit", width: 5, suppressMenu: true, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)" }
            ];

            // To display grid row data
            $scope.LoadData = function () {
                progress(0, 'Loading...', true);
                SpaceSubTypeService.GetSpaceTypes().then(function (data) {
                    $scope.SpaceTypeList = data;
                    SpaceSubTypeService.BindGrid().then(function (data) {
                        $scope.gridata = data;
                        //$scope.createNewDatasource();
                        $scope.gridOptions.api.setRowData(data);
                        setTimeout(function () {
                            progress(0, 'Loading...', false);
                        }, 700);
                    }, function (error) {
                        console.log(error);
                    });
                }, function (error) {
                });
            }

            $scope.onPageSizeChanged = function () {
                $scope.createNewDatasource();
            };

            //$scope.pageSize = '10';

            //$scope.createNewDatasource = function () {
            //    var dataSource = {
            //        pageSize: parseInt($scope.pageSize),
            //        getRows: function (params) {
            //            setTimeout(function () {
            //                var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
            //                var lastRow = -1;
            //                if ($scope.gridata.length <= params.endRow) {
            //                    lastRow = $scope.gridata.length;
            //                }
            //                params.successCallback(rowsThisPage, lastRow);
            //            }, 500);
            //        }
            //    };
            //    $scope.gridOptions.api.setDatasource(dataSource);
            //}

            $scope.gridOptions = {
                columnDefs: columnDefs,
                rowData: null,
                enableSorting: true,
                cellClass: 'grid-align',
                angularCompileRows: true,
                enableFilter: true,
                enableCellSelection: false,
                suppressHorizontalScroll: true,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }
            };


            $scope.EditFunction = function (data) {
                $scope.SpaceSubType = {};
                angular.copy(data, $scope.SpaceSubType);
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;

            }

            $scope.ClearData = function () {
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
                $scope.SpaceSubType = {};
                $scope.frmSpaceSubType.$submitted = false;
            }

            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }
            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })


            $timeout($scope.LoadData, 1000);
        });

    </script>
    <%--<script defer src="../../SMViews/Utility.js"></script>--%>
    <script defer src="../../SMViews/Utility.min.js"></script>
</body>
</html>
