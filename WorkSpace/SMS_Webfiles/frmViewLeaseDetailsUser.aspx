<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewLeaseDetailsUser.aspx.vb"
    Inherits="WorkSpace_SMS_Webfiles_frmViewLeaseDetailsUser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <script type="text/javascript">
            function printPartOfPage(elementId) {
                var printContent = document.getElementById(elementId);
                var windowUrl = 'about:blank';
                var uniqueName = new Date();
                var windowName = 'Print' + uniqueName.getTime();
                var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');
                printWindow.document.write(printContent.innerHTML);
                printWindow.document.close();
                printWindow.focus();
                printWindow.print();
                printWindow.close();
                return false;
            }
        </script>

        <div id="Div2">            
            <table cellspacing="0" cellpadding="0" width="85%" align="center" border="0">
                <tr>
                    <td align="left" width="100%" colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <asp:TextBox ID="txtstore1" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtstore" runat="server" Visible="false"></asp:TextBox>
                        <div id="Div1">
                            <table id="trprint" runat="server" cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr id="axisimg" runat="server">
                                    <td align="Center" width="75%">
                                        <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                                            ForeColor="Black"> Lease  Application form
             <hr align="center" width="60%" /></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table id="tab" runat="server" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="height: 20px">
                                        <asp:GridView ID="gvEmpDetails" runat="server" AllowPaging="true" AllowSorting="true"
                                            AutoGenerateColumns="false" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Employee No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmpno" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Employee Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblname" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_FIRST_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblemail" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_EMAIL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Department">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDept" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_DEP_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Designation">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldesg" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_DESGN_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="DOJ">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDoj" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_DOJ") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contact Details">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblext" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_EXTENSION") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mobile No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblresidence" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_RES_NUMBER") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr id="Tr9" runat="server">
                                    <td align="left">
                                        <table id="tr_vp_hr1" runat="Server" cellpadding="1" cellspacing="0" width="100%"
                                            border="1">
                                            <tr>
                                                <td align="left" style="height: 26px; width: 50%">Lease Extesnion Fromdate
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:TextBox ID="txtextfromdate" runat="server" CssClass="clsTextField" Width="97%"
                                                        ReadOnly="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="height: 26px; width: 50%">Lease Extesnion Todate
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:TextBox ID="txtexttodate" runat="server" CssClass="clsTextField" Width="97%"
                                                        ReadOnly="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="height: 26px; width: 50%">Rent Amount
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:TextBox ID="txtrent" runat="server" CssClass="clsTextField" Width="97%" TabIndex="2"
                                                        MaxLength="10" ReadOnly="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="height: 26px; width: 50%">Security Deposit
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:TextBox ID="txtsdep" runat="server" CssClass="clsTextField" Width="97%" TabIndex="3"
                                                        MaxLength="10" ReadOnly="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="height: 26px; width: 50%">Remarks
                                                </td>
                                                <td align="left" style="height: 26px; width: 50%">
                                                    <asp:TextBox ID="txtremarks" runat="server" CssClass="clsTextField" TabIndex="4"
                                                        Width="97%" Rows="5" TextMode="multiLine" MaxLength="1000" ReadOnly="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Panel ID="panwiz1" runat="server" Width="100%" GroupingText="Lease Details">
                                            <table id="TABWIZ1" cellspacing="0" cellpadding="1" width="100%" border="1">
                                                <tr id="Tr1" runat="server" visible="false">
                                                    <td align="left" style="width: 25%; height: 26px;">Select Property Type
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:DropDownList ID="ddlproptype" runat="server" CssClass="clsComboBox" Width="99%">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Select Lesse
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:DropDownList ID="ddlLesse" runat="server" CssClass="clsComboBox" Width="99%">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;" visible="false">Lease Type</td>
                                                    <td align="left" style="width: 25%; height: 26px;" visible="false">
                                                        <asp:DropDownList ID="ddlLeaseType" runat="server" CssClass="clsComboBox" AutoPostBack="True"
                                                            Width="99%">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="Tr2" runat="server" visible="false">
                                                    <td align="left" style="width: 25%; height: 26px;">Select City
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:DropDownList ID="ddlCity" runat="server" CssClass="clsComboBox" Width="99%"
                                                            AutoPostBack="True" TabIndex="1">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">PinCode
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtpincode" runat="server" CssClass="clsTextField" TabIndex="5"
                                                            Width="97%" MaxLength="10"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Select Property
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:DropDownList ID="ddlproperty" runat="server" CssClass="clsComboBox" Width="97%">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">CTS Number
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtLnumber" runat="server" CssClass="clsTextField" TabIndex="6"
                                                            Width="97%" MaxLength="50"></asp:TextBox>
                                                    </td>
                                                    <td id="Td1" align="left" style="width: 25%; height: 26px;" visible="false" runat="server">Select Status
                                                    </td>
                                                    <td id="Td2" align="center" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                        <asp:DropDownList ID="ddlStatus" runat="server" Width="99%" CssClass="clsComboBox">
                                                            <asp:ListItem Value="1">Active</asp:ListItem>
                                                            <asp:ListItem Value="0">Terminated</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="Tr3" runat="server">
                                                    <td align="left" style="width: 50%; height: 26px;" colspan="2">Complete Address
                                                    </td>
                                                    <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                                        <asp:TextBox ID="txtBuilding" runat="server" Width="97%" MaxLength="500" TabIndex="3"
                                                            TextMode="MultiLine" Rows="5">.</asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">State
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtprop3" runat="server" CssClass="clsTextField" Width="97%" MaxLength="50"
                                                            TabIndex="4">.</asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Region
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtregion" runat="server" CssClass="clsTextField" Width="97%" MaxLength="50"
                                                            TabIndex="5">.</asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Stamp duty :
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtsduty" runat="server" CssClass="clsTextField" Width="97%" MaxLength="12"
                                                            TabIndex="6"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Registration charges:
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtregcharges" runat="server" CssClass="clsTextField" Width="97%"
                                                            TabIndex="7"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Professional Fees :
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtpfees" runat="server" CssClass="clsTextField" Width="97%" MaxLength="12"
                                                            TabIndex="8"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Consultancy / Brokerage:
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtbrokerage" runat="server" CssClass="clsTextField" Width="97%"
                                                            TabIndex="9"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Entitled Lease Amount</td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtentitle" runat="server" CssClass="clsTextField" TabIndex="12"
                                                            Width="97%"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Basic Rent(Max.
                                                        <asp:Label ID="lblmaxrent" runat="server" CssClass="bodytext"></asp:Label>)
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtInvestedArea" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="15" TabIndex="13"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Builtup Area (sqft.)
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtOccupiedArea" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="15" TabIndex="14" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Security Deposit (Max.
                                                        <asp:Label ID="lblmaxsd" runat="server" CssClass="bodytext"></asp:Label>)
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtpay" runat="server" CssClass="clsTextField" TabIndex="15" Width="97%"
                                                                MaxLength="26"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="Tr4" runat="server">
                                                    <td align="left" style="width: 25%; height: 26px;">DG Back up charges
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtdg" runat="server" CssClass="clsTextField" Width="97%" MaxLength="15"
                                                            TabIndex="16"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Maintenance charges
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtmain1" runat="server" CssClass="clsTextField" Width="97%" MaxLength="15"
                                                                ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="backup2">
                                                    <td align="left" style="width: 25%; height: 26px;">Furniture & Fixtures:
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtfurniture" runat="server" Width="97%" TabIndex="20" MaxLength="17"
                                                            CssClass="clsTextField"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Office Equipments :
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtofcequip" runat="server" Width="97%" TabIndex="18" CssClass="clsTextField"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="backup1">
                                                    <td align="left" style="width: 25%; height: 26px;">Service Tax
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtservicetax" runat="server" CssClass="clsTextField" Width="97%"
                                                            MaxLength="15" TabIndex="21"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Property Tax
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtproptax" runat="server" CssClass="clsTextField" Width="97%" MaxLength="15"
                                                            TabIndex="22"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 50%; height: 30px;" colspan="2">One Time cost (Stamp duty+Registration charges+Professional fees+Brokergare Fees)
                                                    </td>
                                                    <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                                        <asp:TextBox ID="txtbasic" runat="server" CssClass="clsTextField" Width="97%" MaxLength="15"
                                                            ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 50%; height: 30px;" colspan="2">Maintenance Cost (Maintenance charge+DGBackup charges+Furniture&fixtures+Office
                                                        Equipments)
                                                    </td>
                                                    <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                                        <asp:TextBox ID="txtmain" runat="server" CssClass="clsTextField" Width="97%" MaxLength="15"
                                                            ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 50%; height: 30px;" colspan="2">Total Rent (Service tax+Maintenance cost+Basic Rent)
                                                    </td>
                                                    <td align="left" style="width: 50%; height: 26px;" colspan="2">
                                                        <asp:TextBox ID="txttotalrent" runat="server" CssClass="clsTextField" Width="97%"
                                                            ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="Td3" align="left" style="width: 25%; height: 26px;" runat="server">Effective Date of Agreement
                                                    </td>
                                                    <td id="Td4" align="left" style="width: 25%; height: 26px;" runat="server">
                                                        <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtsdate" runat="server" CssClass="clsTextField" Width="97%" TabIndex="25"></asp:TextBox>
                                                    </td>
                                                    <td id="Td5" align="left" style="width: 25%; height: 26px;" runat="server">Expiry Date of Agreement
                                                    </td>
                                                    <td id="Td6" align="left" style="width: 25%; height: 26px;" runat="server">
                                                        <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtedate" runat="server" CssClass="clsTextField" Width="97%" TabIndex="26"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Rent Revision
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtrentrev" runat="server" CssClass="clsTextField" Width="97%" MaxLength="15"
                                                            TabIndex="27"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">No. of Landlords
                                                    </td>
                                                    <td align="center" style="width: 25%; height: 26px;">
                                                        <asp:DropDownList ID="ddlleaseld" runat="server" Width="99%" CssClass="clsComboBox"
                                                            TabIndex="28" AutoPostBack="True">
                                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                            <asp:ListItem Value="1">1</asp:ListItem>
                                                            <asp:ListItem Value="2">2</asp:ListItem>
                                                            <asp:ListItem Value="3">3</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Agreement to be signed by POA
                                                    </td>
                                                    <td align="center" style="width: 25%; height: 26px;">
                                                        <asp:DropDownList ID="ddlpoa" runat="server" Width="99%" CssClass="clsComboBox" TabIndex="29">
                                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                            <asp:ListItem Value="No">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Do You wish to Enter Lease Escalation
                                                    </td>
                                                    <td id="tdlseesc1" runat="server" align="left" style="width: 25%; height: 26px;">
                                                        <asp:DropDownList ID="ddlesc" runat="server" CssClass="clsComboBox" TabIndex="30"
                                                            Width="99%" AutoPostBack="True">
                                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                            <asp:ListItem Value="No">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="Tr10" runat="server" visible="false">
                                                    <td align="left" style="width: 25%; height: 26px;">Lease Cost Per</td>
                                                    <td align="left" style="width: 25%; height: 26px">
                                                        <asp:DropDownList ID="ddlMode" runat="server" CssClass="clsComboBox" Width="99%"
                                                            TabIndex="29">
                                                            <asp:ListItem>--Select--</asp:ListItem>
                                                            <asp:ListItem Value="1">Weekly</asp:ListItem>
                                                            <asp:ListItem Value="2">Monthly</asp:ListItem>
                                                            <asp:ListItem Value="3">Half-Yearly</asp:ListItem>
                                                            <asp:ListItem Value="4">Yearly</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="Tr11" runat="server" visible="false">
                                                    <td id="Td7" align="left" style="width: 25%; height: 26px;" runat="server">Lease Escalation Type
                                                    </td>
                                                    <td id="Td9" align="center" style="width: 25%; height: 26px;" runat="server">
                                                        <asp:DropDownList ID="ddlesctype" runat="server" Width="99%" CssClass="clsComboBox"
                                                            AutoPostBack="True" TabIndex="33">
                                                            <asp:ListItem Value="PER">Percentage</asp:ListItem>
                                                            <asp:ListItem Value="FLT">Flat Amount</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2" width="100%">
                                                        <asp:Panel ID="pnlesc1" runat="server" Width="100%" GroupingText="Escalation1">
                                                            <table id="tbesc1" runat="Server" cellpadding="1" width="100%">
                                                                <tr>
                                                                    <td id="Td8" align="left" style="width: 50%; height: 26px;" runat="server">From Date
                                                                    </td>
                                                                    <td id="Td10" align="left" style="width: 50%; height: 26px;" runat="server">
                                                                        <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                                                            <asp:TextBox ID="txtEscalationDate" runat="server" TabIndex="34" CssClass="clsTextField"
                                                                                Width="97%"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="Td11" align="left" style="width: 50%; height: 26px;" runat="server">To Date
                                                                    </td>
                                                                    <td id="Td12" align="left" style="width: 50%; height: 26px;" runat="server">
                                                                        <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                                                            <asp:TextBox ID="txtesctodate1" runat="server" TabIndex="35" CssClass="clsTextField"
                                                                                Width="97%"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="width: 50%; height: 26px;">Rent Escalation
                                                                    </td>
                                                                    <td id="tdesc2" runat="server" align="left" style="width: 50%; height: 26px;">
                                                                        <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                            <asp:TextBox ID="txtfirstesc" runat="server" TabIndex="36" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                    <td align="left" colspan="2" width="100%">
                                                        <asp:Panel ID="pnlesc2" runat="server" Width="100%" GroupingText="Escalation2">
                                                            <table id="tbesc2" runat="Server" width="100%">
                                                                <tr>
                                                                    <td id="Td14" align="left" style="width: 50%; height: 26px;" runat="server">From Date</td>
                                                                    <td id="Td13" align="left" style="width: 50%; height: 26px;" runat="server">
                                                                        <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                                                            <asp:TextBox ID="txtescfromdate2" runat="server" TabIndex="37" CssClass="clsTextField"
                                                                                Width="97%"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="Td15" align="left" style="width: 50%; height: 26px;" runat="server">To Date</td>
                                                                    <td id="Td16" align="left" style="width: 50%; height: 26px;" runat="server">
                                                                        <div onmouseover="Tip('Click on the Textbox to select Date')" onmouseout="UnTip()">
                                                                            <asp:TextBox ID="txtesctodate2" runat="server" TabIndex="38" CssClass="clsTextField"
                                                                                Width="97%"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="width: 50%; height: 26px;">Rent Escalation
                                                                    </td>
                                                                    <td id="tdesc4" runat="server" align="left" style="width: 50%; height: 26px;">
                                                                        <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                            <asp:TextBox ID="txtsecondesc" runat="server" TabIndex="39" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="15"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr id="Tr12" runat="server" visible="false">
                                                    <td align="left" style="width: 25%; height: 26px;">Tenure of agreement
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txttenure" runat="server" CssClass="clsTextField" Width="97%" MaxLength="15"
                                                            TabIndex="40"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Leasehold Improvements :
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtimp" runat="server" Width="97%" Rows="5" TextMode="Multiline"
                                                            MaxLength="500" TabIndex="41"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Lease Comments
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <div onmouseover="Tip('Enter Comments with maximum 750 Characters')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtComments" runat="server" Width="97%" TextMode="MultiLine" Rows="5"
                                                                MaxLength="1000" TabIndex="42"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btn1Next" runat="server" Text="Next" CausesValidation="false" Visible="false"
                                            CssClass="button" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Panel ID="panwiz3" runat="server" Width="100%" GroupingText="Agreement Details">
                                            <table id="tabwiz3" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                                                <tr id="Tr5" runat="server">
                                                    <td id="Td17" align="left" style="width: 25%; height: 26px;" runat="server">Tentative Execution Date
                                                    </td>
                                                    <td id="Td18" align="left" style="width: 25%; height: 26px;" runat="server">
                                                        <asp:TextBox ID="txtAgreedate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                    </td>
                                                    <td id="Td19" align="left" style="width: 25%; height: 26px;" runat="server">Amount of stampDuty paid
                                                    </td>
                                                    <td id="Td20" align="left" style="width: 25%; height: 26px;" runat="server">
                                                        <asp:TextBox ID="txtagreeamt" runat="server" CssClass="clsTextField" Width="97%"
                                                            MaxLength="15"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="Td21" align="left" style="width: 25%; height: 26px;" runat="server">Amount paid towards Registration
                                                    </td>
                                                    <td id="Td22" align="left" style="width: 25%; height: 26px;" runat="server">
                                                        <asp:TextBox ID="txtregamt" runat="server" CssClass="clsTextField" Width="97%" MaxLength="15"></asp:TextBox>
                                                    </td>
                                                    <td id="Td23" align="left" style="width: 25%; height: 26px;" runat="server">Agreement Registered
                                                    </td>
                                                    <td id="Td24" align="left" style="width: 25%; height: 26px;" runat="server">
                                                        <asp:DropDownList ID="ddlagreeres" runat="server" CssClass="clsComboBox" Width="99%"
                                                            AutoPostBack="True">
                                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                            <asp:ListItem Value="No" Selected="True">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="trregagree" runat="server">
                                                    <td id="Td25" align="left" style="width: 25%; height: 26px;" runat="server">Date of Registration
                                                    </td>
                                                    <td id="Td26" align="left" style="width: 25%; height: 26px;" runat="server">
                                                        <asp:TextBox ID="txtagreeregdate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                    </td>
                                                    <td id="Td27" align="left" style="width: 25%; height: 26px;" runat="server">Sub-Regristrarís office name
                                                    </td>
                                                    <td id="Td28" align="left" style="width: 25%; height: 26px;" runat="server">
                                                        <asp:TextBox ID="txtagreesub" runat="server" CssClass="clsTextField" Width="97%"
                                                            MaxLength="50"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="Tr6" runat="server">
                                                    <td id="Td29" align="left" style="width: 25%; height: 26px;" runat="server">Termination Notice (in days)
                                                    </td>
                                                    <td id="Td30" align="left" style="width: 25%; height: 26px;" runat="server">
                                                        <asp:TextBox ID="txtnotice" runat="server" CssClass="clsTextField" Width="97%" MaxLength="10"></asp:TextBox>
                                                    </td>
                                                    <td id="Td31" align="left" style="width: 25%; height: 26px;" runat="server">Lock-In Period of Agreement(in months)
                                                    </td>
                                                    <td id="Td32" align="left" style="width: 25%; height: 26px;" runat="server">
                                                        <asp:TextBox ID="txtlock" runat="server" CssClass="clsTextField" Width="97%" MaxLength="10"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="panbrk" runat="server" Width="100%" GroupingText="Brokerage Details">
                                            <table cellpadding="1" cellspacing="0" width="100%" border="1">
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Amount of Brokerage Paid
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtbrkamount" runat="server" CssClass="clsTextField" Width="97%"
                                                            MaxLength="12" TabIndex="24"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Broker Name
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtbrkname" runat="server" CssClass="clsTextField" TabIndex="25"
                                                            Width="97%" MaxLength="50"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Broker Address
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtbrkaddr" runat="server" CssClass="clsTextField" Width="97%" MaxLength="1000"
                                                            TextMode="MultiLine" TabIndex="26" Rows="5"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Broker Pan Number
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtbrkpan" runat="server" TabIndex="27" CssClass="clsTextField"
                                                            Width="97%" MaxLength="10"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="Tr7" runat="server">
                                                    <td id="Td33" align="left" style="width: 25%; height: 26px;" runat="server">Broker Email
                                                    </td>
                                                    <td id="Td34" align="left" style="width: 25%; height: 26px;" runat="server">
                                                        <asp:TextBox ID="txtbrkremail" runat="server" CssClass="clsTextField" Width="97%"
                                                            MaxLength="50" TabIndex="28"></asp:TextBox>
                                                    </td>
                                                    <td id="Td35" align="left" style="width: 25%; height: 26px;" runat="server">Contact Details
                                                    </td>
                                                    <td id="Td36" align="left" style="width: 25%; height: 26px;" runat="server">
                                                        <asp:TextBox ID="txtbrkmob" runat="server" CssClass="clsTextField" TabIndex="29"
                                                            Width="97%" MaxLength="15"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="panrecwiz" runat="server" Width="100%" GroupingText="Recovery Details Before Escalation ">
                                            <table id="tabrecwiz" runat="Server" width="100%" cellpadding="1" cellspacing="0"
                                                border="1">
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Employee's Account Number
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtEmpAccNo" runat="server" CssClass="clsTextField" Width="97%"
                                                            MaxLength="50"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Employee's Bank Name
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtEmpBankName" runat="server" CssClass="clsTextField" Width="97%"
                                                            MaxLength="50"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Employee's Bank Branch Name
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtEmpBranch" runat="server" CssClass="clsTextField" Width="97%"
                                                            MaxLength="50"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Recovery Amount (Per Month)
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtEmpRcryAmt" runat="server" CssClass="clsTextField" Width="97%"
                                                            MaxLength="50"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="LEFT" style="height: 26px; width: 25%">Recovery From Date
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtrcryfromdate" runat="Server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Recovery To Date
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtrcrytodate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="panafteresc1" runat="server" Width="100%" GroupingText="First Escalation ">
                                            <table id="tabafteresc1" runat="Server" width="100%" cellpadding="1" cellspacing="0"
                                                border="1">
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Recovery Amount (Per Month)
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtrcramt1" runat="server" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="LEFT" style="height: 26px; width: 25%">Recovery From Date
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtrcrfrmdate1" runat="Server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Recovery To Date
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtrcrtodate1" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="panafteresc2" runat="server" Width="100%" GroupingText="Second Escalation ">
                                            <table id="tabafteresc2" runat="Server" width="100%" cellpadding="1" cellspacing="0"
                                                border="1">
                                                <tr>
                                                    <td align="left" style="width: 25%; height: 26px;">Recovery Amount (Per Month)
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtrcramt2" runat="server" CssClass="clsTextField" Width="97%" MaxLength="50"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="LEFT" style="height: 26px; width: 25%">Recovery From Date
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtrcrfrmdate2" runat="Server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">Recovery To Date
                                                    </td>
                                                    <td align="left" style="width: 25%; height: 26px;">
                                                        <asp:TextBox ID="txtrcrtodate2" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btn2prev" runat="server" Text="Previous" CssClass="button" Visible="false"
                                            CausesValidation="false" />
                                        <asp:Button ID="btn2Next" runat="server" Text="Next" CssClass="button" Visible="false"
                                            CausesValidation="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Panel ID="landlord" runat="server" Width="100%">
                                            <asp:Panel ID="panPOA" runat="Server" Width="100%" GroupingText="Power of Attorney Details">
                                                <table id="tabpoa" runat="Server" width="100%" cellpadding="1" cellspacing="0" border="1">
                                                    <tr>
                                                        <td align="left" style="width: 25%; height: 26px;">Name
                                                        </td>
                                                        <td align="left" style="width: 25%; height: 26px;">
                                                            <asp:TextBox ID="txtPOAName" runat="server" CssClass="clsTextField" TabIndex="32"
                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                        <td align="left" style="width: 25%; height: 26px;">Address
                                                        </td>
                                                        <td align="left" style="width: 25%; height: 26px;">
                                                            <asp:TextBox ID="txtPOAAddress" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="1000" TextMode="MultiLine" Rows="4" TabIndex="33"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="width: 25%; height: 26px;">Contact Details
                                                        </td>
                                                        <td align="left" style="width: 25%; height: 26px;">
                                                            <asp:TextBox ID="txtPOAMobile" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="12" TabIndex="34"></asp:TextBox>
                                                        </td>
                                                        <td align="left" style="width: 25%; height: 26px;">Email-ID
                                                        </td>
                                                        <td align="left" style="width: 25%; height: 26px;">
                                                            <asp:TextBox ID="txtPOAEmail" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="1000" TextMode="MultiLine" TabIndex="35" Rows="4"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Label ID="lbl1" runat="server" CssClass="clsMessage"></asp:Label>
                                            <asp:Panel ID="panld1" runat="server" Width="100%" GroupingText="Landlord1 Details">
                                                <table id="tabld1" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                                                    <tr id="Tr8" runat="server">
                                                        <td id="Td37" align="left" style="width: 25%; height: 26px;" runat="server">Name
                                                        </td>
                                                        <td id="Td38" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtldname" runat="server" TabIndex="36" CssClass="clsTextField"
                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                        <td id="Td39" align="left" style="width: 25%; height: 26px;" runat="server">Address 1
                                                        </td>
                                                        <td id="Td40" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtldaddr" runat="server" CssClass="clsTextField" Width="97%" MaxLength="500"
                                                                TextMode="MultiLine" TabIndex="37" Rows="5"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td41" align="left" style="width: 25%; height: 26px;" runat="server">Address2
                                                        </td>
                                                        <td id="Td42" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld1addr2" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="500" TextMode="MultiLine" TabIndex="38" Rows="5"></asp:TextBox>
                                                        </td>
                                                        <td id="Td43" align="left" style="width: 25%; height: 26px;" runat="server">Address3
                                                        </td>
                                                        <td id="Td44" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld1addr3" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="500" TextMode="MultiLine" TabIndex="38" Rows="5"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td45" align="left" style="width: 25%; height: 26px;" runat="server">State
                                                        </td>
                                                        <td id="Td46" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:DropDownList ID="ddlstate" runat="server" CssClass="clsTextField" AutoPostBack="true"
                                                                Width="99%" TabIndex="39" Enabled="false">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td id="Td47" align="left" style="width: 25%; height: 26px;" runat="server">City
                                                        </td>
                                                        <td id="Td48" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="ddlld1city" runat="server" CssClass="clsTextField" Width="99%" TabIndex="40"
                                                                ReadOnly="TRUE"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td49" align="left" style="width: 25%; height: 26px;" runat="server">PIN CODE
                                                        </td>
                                                        <td id="Td50" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld1Pin" runat="server" CssClass="clsTextField" Width="97%" TabIndex="41"
                                                                MaxLength="10"></asp:TextBox>
                                                        </td>
                                                        <td id="Td51" align="left" style="width: 25%; height: 26px;" runat="server">PAN No
                                                        </td>
                                                        <td id="Td52" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtPAN" runat="server" CssClass="clsTextField" Width="97%" TabIndex="42"
                                                                MaxLength="10"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr13" runat="server">
                                                        <td id="Td53" align="left" style="width: 25%; height: 26px;" runat="server">Email
                                                        </td>
                                                        <td id="Td54" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtldemail" runat="server" CssClass="clsTextField" Width="97%" TabIndex="43"
                                                                MaxLength="50"></asp:TextBox>
                                                        </td>
                                                        <td id="Td55" align="left" style="width: 25%; height: 26px;" runat="server">Contact Details
                                                        </td>
                                                        <td id="Td56" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtmob" runat="server" CssClass="clsTextField" Width="97%" TabIndex="44"
                                                                MaxLength="12"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td57" align="left" style="width: 25%; height: 26px;" runat="server">Monthly Rent Payable
                                                        </td>
                                                        <td id="Td58" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtpmonthrent" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="15" TabIndex="45"></asp:TextBox>
                                                        </td>
                                                        <td id="Td59" align="left" style="width: 25%; height: 26px;" runat="server">Security Deposit
                                                        </td>
                                                        <td id="Td60" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtpsecdep" runat="server" TabIndex="46" CssClass="clsTextField"
                                                                Width="97%" MaxLength="15"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr14" runat="server">
                                                        <td id="Td61" align="left" style="width: 25%; height: 26px;" runat="server" visible="false">From Date
                                                        </td>
                                                        <td id="Td62" align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                            <asp:TextBox ID="txtpfromdate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                        </td>
                                                        <td id="Td63" align="left" style="width: 25%; height: 26px;" runat="server" visible="False">To Date
                                                        </td>
                                                        <td id="Td64" align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                            <asp:TextBox ID="txtptodate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td65" align="left" style="height: 26px; width: 50%" colspan="2" runat="server">Select Payment Mode
                                                        </td>
                                                        <td id="Td66" align="left" style="height: 26px; width: 50%" colspan="2" runat="server">
                                                            <asp:DropDownList ID="ddlpaymentmode" runat="server" CssClass="clsComboBox" Width="99%"
                                                                AutoPostBack="True" TabIndex="47">
                                                                <asp:ListItem Value="--Select PaymentMode--">--Select PaymentMode--</asp:ListItem>
                                                                <asp:ListItem Value="1">DD</asp:ListItem>
                                                                <asp:ListItem Value="2">Axis Bank Account Credit</asp:ListItem>
                                                                <asp:ListItem Value="3">NEFT / RTGS</asp:ListItem>
                                                            </asp:DropDownList>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr15" runat="server">
                                                        <td id="Td67" colspan="4" width="100%" runat="server">
                                                            <asp:Panel ID="panel1" runat="server" Width="100%">
                                                                <table id="table4" runat="server" width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td id="Td68" align="left" style="height: 26px; width: 25%" runat="server">Bank Name
                                                                        </td>
                                                                        <td id="Td69" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtBankName" runat="server" TabIndex="48" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="Tr16" runat="server">
                                                                        <td id="Td70" align="left" style="height: 26px; width: 25%" runat="server">Account Number
                                                                        </td>
                                                                        <td id="Td71" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtAccNo" runat="server" TabIndex="49" CssClass="clsTextField" Width="97%"
                                                                                MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr17" runat="server">
                                                        <td id="Td72" colspan="4" width="100%" runat="server">
                                                            <asp:Panel ID="panelL12" runat="server" Width="100%">
                                                                <table id="Table1" runat="server" width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr id="Tr18" runat="server">
                                                                        <td id="Td73" align="left" style="height: 26px; width: 25%" runat="server">Account Number
                                                                        </td>
                                                                        <td id="Td74" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtL12Accno" runat="server" TabIndex="50" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr19" runat="server">
                                                        <td id="Td75" colspan="4" width="100%" runat="server">
                                                            <asp:Panel ID="panel2" runat="server" Width="100%">
                                                                <table id="table5" runat="server" width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr id="Tr20" runat="server">
                                                                        <td id="Td76" align="left" style="height: 26px; width: 25%" runat="server">LandLords Account Number
                                                                        </td>
                                                                        <td id="Td77" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtIBankName" runat="server" TabIndex="51" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                        <td id="Td78" align="left" style="height: 26px; width: 25%" runat="server">Bank Name
                                                                        </td>
                                                                        <td id="Td79" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtDeposited" runat="server" TabIndex="52" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="Tr21" runat="server">
                                                                        <td id="Td80" align="left" style="height: 26px; width: 25%" runat="server">Branch Name
                                                                        </td>
                                                                        <td id="Td81" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtbrnch" runat="server" TabIndex="53" CssClass="clsTextField" Width="97%"
                                                                                MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                        <td id="Td82" align="left" style="height: 26px; width: 25%" runat="server">IFSC Code
                                                                        </td>
                                                                        <td id="Td83" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtIFSC" runat="server" TabIndex="54" CssClass="clsTextField" Width="97%"
                                                                                MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:Button ID="btnAddlandlord1" runat="server" CssClass="button" Text="Add Landlord1"
                                                                Visible="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlld2" runat="server" Width="100%" GroupingText="Landlord2 Details">
                                                <table id="tabld2" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                                                    <tr id="Tr22" runat="server">
                                                        <td id="Td84" align="left" style="width: 25%; height: 26px;" runat="server">Name
                                                        </td>
                                                        <td id="Td85" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld1name" runat="server" TabIndex="55" CssClass="clsTextField"
                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                        <td id="Td86" align="left" style="width: 25%; height: 26px;" runat="server">Address 1
                                                        </td>
                                                        <td id="Td87" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld2addr" runat="server" TabIndex="56" CssClass="clsTextField"
                                                                Width="97%" MaxLength="500" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td88" align="left" style="width: 25%; height: 26px;" runat="server">Address2
                                                        </td>
                                                        <td id="Td89" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld2addr2" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="500" TextMode="MultiLine" TabIndex="57" Rows="5"></asp:TextBox>
                                                        </td>
                                                        <td id="Td90" align="left" style="width: 25%; height: 26px;" runat="server">Address3
                                                        </td>
                                                        <td id="Td91" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld2addr3" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="500" TextMode="MultiLine" TabIndex="58" Rows="5"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td92" align="left" style="width: 25%; height: 26px;" runat="server">State
                                                        </td>
                                                        <td id="Td93" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:DropDownList ID="ddlld2state" runat="server" CssClass="clsTextField" AutoPostBack="true"
                                                                Width="99%" TabIndex="59" Enabled="false">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td id="Td94" align="left" style="width: 25%; height: 26px;" runat="server">City
                                                        </td>
                                                        <td id="Td95" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="ddlld2city" runat="server" CssClass="clsTextField" Width="99%" TabIndex="60"
                                                                ReadOnly="TRUE"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr23" runat="server">
                                                        <td id="Td96" align="left" style="width: 25%; height: 26px;" runat="server">PIN CODE
                                                        </td>
                                                        <td id="Td97" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld2Pin" runat="server" CssClass="clsTextField" Width="97%" TabIndex="61"
                                                                MaxLength="10"></asp:TextBox>
                                                        </td>
                                                        <td id="Td98" align="left" style="width: 25%; height: 26px;" runat="server">PAN No
                                                        </td>
                                                        <td id="Td99" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld2pan" runat="server" TabIndex="62" CssClass="clsTextField"
                                                                Width="97%" MaxLength="10"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td100" align="left" style="width: 25%; height: 26px;" runat="server">Email
                                                        </td>
                                                        <td id="Td101" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld2email" runat="server" TabIndex="63" CssClass="clsTextField"
                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                        <td id="Td102" align="left" style="width: 25%; height: 26px;" runat="server">Contact Details
                                                        </td>
                                                        <td id="Td103" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld2mob" runat="server" TabIndex="64" CssClass="clsTextField"
                                                                Width="97%" MaxLength="15"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr24" runat="server">
                                                        <td id="Td104" align="left" style="width: 25%; height: 26px;" runat="server" visible="false">From Date
                                                        </td>
                                                        <td id="Td105" align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                            <asp:TextBox ID="txtld2frmdate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                        </td>
                                                        <td id="Td106" align="left" style="width: 25%; height: 26px;" runat="server" visible="false">To Date
                                                        </td>
                                                        <td id="Td107" align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                            <asp:TextBox ID="txtld2todate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td108" align="left" style="width: 25%; height: 26px;" runat="server">Monthly Rent Payable
                                                        </td>
                                                        <td id="Td109" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld2rent" runat="server" CssClass="clsTextField" TabIndex="65"
                                                                Width="97%" MaxLength="15"></asp:TextBox>
                                                        </td>
                                                        <td id="Td110" align="left" style="width: 25%; height: 26px;" runat="server">Security Deposit
                                                        </td>
                                                        <td id="Td111" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld2sd" runat="server" TabIndex="66" CssClass="clsTextField" Width="97%"
                                                                MaxLength="15"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td112" align="left" style="height: 26px; width: 50%" colspan="2" runat="server">Select Payment Mode
                                                        </td>
                                                        <td id="Td113" align="left" style="height: 26px; width: 50%" colspan="2" runat="server">
                                                            <asp:DropDownList ID="ddlld2mode" runat="server" CssClass="clsComboBox" Width="99%"
                                                                AutoPostBack="True" TabIndex="67">
                                                                <asp:ListItem Value="--Select--">--Select PaymentMode--</asp:ListItem>
                                                                <asp:ListItem Value="1">DD</asp:ListItem>
                                                                <asp:ListItem Value="2">Axis Bank Account Credit</asp:ListItem>
                                                                <asp:ListItem Value="3">NEFT / RTGS</asp:ListItem>
                                                            </asp:DropDownList>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr25" runat="server">
                                                        <td id="Td114" colspan="4" width="100%" runat="server">
                                                            <asp:Panel ID="panel3" runat="server" Width="100%">
                                                                <table id="table3" runat="server" width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td id="Td115" align="left" style="height: 26px; width: 25%" runat="server">Bank Name
                                                                        </td>
                                                                        <td id="Td116" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtld2bankname" runat="server" TabIndex="68" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="Tr26" runat="server">
                                                                        <td id="Td117" align="left" style="height: 26px; width: 25%" runat="server">Account Number
                                                                        </td>
                                                                        <td id="Td118" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtld2accno" runat="server" TabIndex="69" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td119" colspan="4" width="100%" runat="Server">
                                                            <asp:Panel ID="pnll22" runat="server" Width="100%">
                                                                <table id="Table6" runat="server" width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr id="Tr27" runat="server">
                                                                        <td id="Td120" align="left" style="height: 26px; width: 25%" runat="server">Account Number
                                                                        </td>
                                                                        <td id="Td121" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtl22accno" runat="server" TabIndex="70" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr28" runat="server">
                                                        <td id="Td122" colspan="4" width="100%" runat="server">
                                                            <asp:Panel ID="panel4" runat="server" Width="100%">
                                                                <table id="table7" runat="server" width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr id="Tr29" runat="server">
                                                                        <td id="Td123" align="left" style="height: 26px; width: 25%" runat="server">LandLords Account Number
                                                                        </td>
                                                                        <td id="Td124" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtld2IBankName" runat="server" TabIndex="71" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                        <td id="Td125" align="left" style="height: 26px; width: 25%" runat="server">Bank Name
                                                                        </td>
                                                                        <td id="Td126" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtld2Deposited" runat="server" TabIndex="72" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="Tr30" runat="server">
                                                                        <td id="Td127" align="left" style="height: 26px; width: 25%" runat="server">Branch Name
                                                                        </td>
                                                                        <td id="Td128" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtl2brnchname" runat="server" TabIndex="73" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                        <td id="Td129" align="left" style="height: 26px; width: 25%" runat="server">IFSC Code
                                                                        </td>
                                                                        <td id="Td130" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtld2IFSC" runat="server" TabIndex="74" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:Button ID="btnAddLandLord2" runat="server" CssClass="button" Text="Add LandLord2"
                                                                Visible="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlld3" runat="server" Width="100%" GroupingText="Landlord3 Details">
                                                <table id="tabld3" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                                                    <tr id="Tr31" runat="server">
                                                        <td id="Td131" align="left" style="width: 25%; height: 26px;" runat="server">Name
                                                        </td>
                                                        <td id="Td132" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld3name" runat="server" TabIndex="75" CssClass="clsTextField"
                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                        <td id="Td133" align="left" style="width: 25%; height: 26px;" runat="server">Address1
                                                        </td>
                                                        <td id="Td134" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld3addr" runat="server" TabIndex="76" CssClass="clsTextField"
                                                                Width="97%" MaxLength="500" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td135" align="left" style="width: 25%; height: 26px;" runat="server">Address2
                                                        </td>
                                                        <td id="Td136" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld3addr2" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="500" TextMode="MultiLine" TabIndex="57" Rows="5"></asp:TextBox>
                                                        </td>
                                                        <td id="Td137" align="left" style="width: 25%; height: 26px;" runat="server">Address3
                                                        </td>
                                                        <td id="Td138" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld3addr3" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="500" TextMode="MultiLine" TabIndex="78" Rows="5"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td139" align="left" style="width: 25%; height: 26px;" runat="server">State
                                                        </td>
                                                        <td id="Td140" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:DropDownList ID="ddlld3state" runat="server" CssClass="clsTextField" AutoPostBack="true"
                                                                Width="99%" TabIndex="79" Enabled="false">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td id="Td141" align="left" style="width: 25%; height: 26px;" runat="server">City
                                                        </td>
                                                        <td id="Td142" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="ddlld3city" runat="server" CssClass="clsTextField" Width="99%" TabIndex="80"
                                                                ReadOnly="TRUE"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr32" runat="server">
                                                        <td id="Td143" align="left" style="width: 25%; height: 26px;" runat="server">PIN CODE
                                                        </td>
                                                        <td id="Td144" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld3Pin" runat="server" CssClass="clsTextField" Width="97%" TabIndex="81"
                                                                MaxLength="10"></asp:TextBox>
                                                        </td>
                                                        <td id="Td145" align="left" style="width: 25%; height: 26px;" runat="server">PAN No
                                                        </td>
                                                        <td id="Td146" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld3pan" runat="server" CssClass="clsTextField" TabIndex="82"
                                                                Width="97%" MaxLength="10"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td147" align="left" style="width: 25%; height: 26px;" runat="server">Email
                                                        </td>
                                                        <td id="Td148" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld3email" runat="server" CssClass="clsTextField" Width="97%"
                                                                MaxLength="50" TabIndex="83"></asp:TextBox>
                                                        </td>
                                                        <td id="Td149" align="left" style="width: 25%; height: 26px;" runat="server">Contact Details
                                                        </td>
                                                        <td id="Td150" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld3mob" runat="server" CssClass="clsTextField" TabIndex="84"
                                                                Width="97%" MaxLength="15"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr33" runat="server" visible="False">
                                                        <td id="Td151" align="left" style="width: 25%; height: 26px;" runat="server" visible="false">From Date
                                                        </td>
                                                        <td id="Td152" align="left" style="width: 25%; height: 26px;" runat="server" visible="False">
                                                            <asp:TextBox ID="txtld3fromdate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                        </td>
                                                        <td id="Td153" align="left" style="width: 25%; height: 26px;" runat="server" visible="false">To Date
                                                        </td>
                                                        <td id="Td154" align="left" style="width: 25%; height: 26px;" runat="server" visible="false">
                                                            <asp:TextBox ID="txtld3todate" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td155" align="left" style="width: 25%; height: 26px;" runat="server">Monthly Rent Payable
                                                        </td>
                                                        <td id="Td156" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld3rent" runat="server" TabIndex="85" CssClass="clsTextField"
                                                                Width="97%" MaxLength="15"></asp:TextBox>
                                                        </td>
                                                        <td id="Td157" align="left" style="width: 25%; height: 26px;" runat="server">Security Deposit
                                                        </td>
                                                        <td id="Td158" align="left" style="width: 25%; height: 26px;" runat="server">
                                                            <asp:TextBox ID="txtld3sd" runat="server" CssClass="clsTextField" TabIndex="86" Width="97%"
                                                                MaxLength="15"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td159" align="left" style="height: 26px; width: 50%" colspan="2" runat="server">Select Payment Mode
                                                        </td>
                                                        <td id="Td160" align="left" style="height: 26px; width: 50%" colspan="2" runat="server">
                                                            <asp:DropDownList ID="ddlld3mode" runat="server" TabIndex="87" CssClass="clsComboBox"
                                                                Width="99%" AutoPostBack="True">
                                                                <asp:ListItem Value="--Select--">--Select PaymentMode--</asp:ListItem>
                                                                <asp:ListItem Value="1">DD</asp:ListItem>
                                                                <asp:ListItem Value="2">Axis Bank Account Credit</asp:ListItem>
                                                                <asp:ListItem Value="3"> NEFT / RTGS </asp:ListItem>
                                                            </asp:DropDownList>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr34" runat="server">
                                                        <td id="Td161" colspan="4" width="100%" runat="server">
                                                            <asp:Panel ID="panel5" runat="server" Width="100%">
                                                                <table id="table8" runat="server" width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td id="Td162" align="left" style="height: 26px; width: 25%" runat="server">Bank Name
                                                                        </td>
                                                                        <td id="Td163" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtld3bankname" runat="server" CssClass="clsTextField" Width="97%"
                                                                                MaxLength="50" TabIndex="88"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="Tr35" runat="server">
                                                                        <td id="Td164" align="left" style="height: 26px; width: 25%" runat="server">Account Number
                                                                        </td>
                                                                        <td id="Td165" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtLd3acc" runat="server" TabIndex="89" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="Td166" colspan="4" width="100%" runat="Server">
                                                            <asp:Panel ID="pnll32" runat="server" Width="100%">
                                                                <table id="Table9" runat="server" width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr id="Tr36" runat="server">
                                                                        <td id="Td167" align="left" style="height: 26px; width: 25%" runat="server">Account Number
                                                                        </td>
                                                                        <td id="Td168" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtl32accno" runat="server" TabIndex="90" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr id="Tr37" runat="server">
                                                        <td id="Td169" colspan="4" width="100%" runat="server">
                                                            <asp:Panel ID="panel6" runat="server" Width="100%">
                                                                <table id="table10" runat="server" width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr id="Tr38" runat="server">
                                                                        <td id="Td170" align="left" style="height: 26px; width: 25%" runat="server">LandLords Account Number
                                                                        </td>
                                                                        <td id="Td171" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtld3IBankName" runat="server" TabIndex="91" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                        <td id="Td172" align="left" style="height: 26px; width: 25%" runat="server">Bank Name
                                                                        </td>
                                                                        <td id="Td173" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtld3Deposited" runat="server" TabIndex="92" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="Tr39" runat="server">
                                                                        <td id="Td174" align="left" style="height: 26px; width: 25%" runat="server">Branch Name
                                                                        </td>
                                                                        <td id="Td175" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtl3brnch" runat="server" TabIndex="93" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                        <td id="Td176" align="left" style="height: 26px; width: 25%" runat="server">IFSC Code
                                                                        </td>
                                                                        <td id="Td177" align="left" style="height: 26px; width: 25%" runat="server">
                                                                            <asp:TextBox ID="txtld3ifsc" runat="server" TabIndex="94" CssClass="clsTextField"
                                                                                Width="97%" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:Button ID="btnAddlandlord3" runat="server" CssClass="button" Text="Add LandLord3"
                                                                Visible="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="tr_RM" runat="Server">
                                    <td align="left">
                                        <asp:Panel ID="pnlremarks" runat="server" Width="100%">
                                            <table id="t" runat="Server" cellpadding="1" cellspacing="0" width="100%" border="1">
                                                <tr id="tr_VP_HR" runat="Server">
                                                    <td align="left" style="height: 26px; width: 50%">Competent Authority (HR) Remarks <strong>(<asp:Label ID="lblRMname" runat="server"
                                                        CssClass="clsLabel"></asp:Label>) (<asp:Label ID="lblvphrdate" runat="Server"></asp:Label>)
                                                    </strong>
                                                    </td>
                                                    <td align="left" style="height: 26px; width: 50%">
                                                        <asp:TextBox ID="txtvphrremarks" runat="server" CssClass="clsTextField" Width="97%"
                                                            TextMode="MultiLine" Rows="5" MaxLength="1000"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btn3prev" runat="Server" Text="Previous" CssClass="button" Visible="false"
                                            CausesValidation="false" TabIndex="95" />
                                        <%--<asp:Button ID="btn3finish" runat="server" Text="Close" CssClass="button" TabIndex="96"
                                            OnClientClick="return parent.hidePopWin(true);" CausesValidation="false" />--%>
                                        <asp:Button ID="btnprint" runat="server" CssClass="button" Text="Print" CausesValidation="false" OnClientClick="return printPartOfPage('Div1')" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>

                </tr>
            </table>
        </div>
    </form>
</body>
</html>
