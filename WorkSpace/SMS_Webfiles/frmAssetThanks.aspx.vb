﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common
Imports System.Data.SqlClient
Partial Class WorkSpace_SMS_Webfiles_frmAssetThanks
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim ReqId As String = GetRequestId()
            Dim Message As String = ""
            If String.IsNullOrEmpty(ReqId) AndAlso String.IsNullOrEmpty(Request("PO")) Then
                Message = "Sorry! Unknown problem occurred while raising Asset Requisition."
                lblMsg.Text = Message
            ElseIf String.IsNullOrEmpty(ReqId) Then
                Message = "PO (" + Request("PO") + ") Approved."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "assetlabels" Then
                Message = "Asset labels generated successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "intramovement" Then
                Message = "Intra movement requests submitted successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "intermovement" Then
                Message = "Inter movement requests submitted successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "astreject" Then
                Dim strReq_id As String = ""
                strReq_id = Request.QueryString("Req_id")
                Message = "Asset Rejection request (<b>" & strReq_id & "</b>) submitted successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "maintsc" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "<b><center><br><br> Requisition ID : " & MaintSc_ReqId & "<br>Has Been " & Request.QueryString("staid") & "updated.<br><br>Thank You For Using The System.</center>"
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "editmaintsc" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "<b><center><br><br> Requisition ID : " & MaintSc_ReqId & "<br>Has Been " & Request.QueryString("staid") & "updated.<br><br>Thank You For Using The System.</center>"
                lblMsg.Text = Message
                '*****************
            ElseIf LCase(ReqId) = "eintmvmt" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Inter movement requests submitted successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "intermvmtit" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Inter movement IT request submitted successfully."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "intermvmtitrej" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Inter movement IT request rejected."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "intermvmtitdetails" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Request successfully approved."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "intramvmtitrej" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Intra movement IT request rejected."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "outwardapp" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Request successfully approved."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "outwardrej" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Request rejected."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "inwardapp" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Request successfully approved."
                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "gengatepass" Then
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("MReqId")
                Message = "Gate pass generated successfully."
                lblMsg.Text = Message

            ElseIf LCase(ReqId) = "itemreq" Then
                lblHead.Text = "Item Requisition Status"
                lblSubHead.Text = "Item Requisition Status"
                pnlMain.GroupingText = "Item Requisition Status"
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("ReqId")
                Dim StatusIdItem As Integer = GetRequestStatusItem(MaintSc_ReqId)

                Select Case StatusIdItem
                    Case 1027
                        Message = "Item Requisition (" + MaintSc_ReqId + ") raised successfully."

                End Select

                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "itemreqmodified" Then
                lblHead.Text = "Item Requisition Status"
                lblSubHead.Text = "Item Requisition Status"
                pnlMain.GroupingText = "Item Requisition Status"
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("ReqId")
                Dim StatusIdItem As Integer = GetRequestStatusItem(MaintSc_ReqId)

                Select Case StatusIdItem
                    Case 1028
                        Message = "Item Requisition (" + MaintSc_ReqId + ") modified successfully."
                    Case 1029
                        Message = "Item Requisition (" + MaintSc_ReqId + ") canceled successfully."
                End Select

                lblMsg.Text = Message

            ElseIf LCase(ReqId) = "itemreqrmapproved" Then
                lblHead.Text = "Item Requisition Status"
                lblSubHead.Text = "Item Requisition Status"
                pnlMain.GroupingText = "Item Requisition Status"
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("ReqId")
                Dim StatusIdItem As Integer = GetRequestStatusItem(MaintSc_ReqId)

                Select Case StatusIdItem
                    Case 1030
                        Message = "Item Requisition (" + MaintSc_ReqId + ") Approved by RM successfully."
                    Case 1031
                        Message = "Item Requisition (" + MaintSc_ReqId + ") Canceled by RM successfully."
                End Select

                lblMsg.Text = Message
            ElseIf LCase(ReqId) = "addstock" Then
                Message = "Stock added successfully."
                lblMsg.Text = Message
                '*********************
            ElseIf LCase(ReqId) = "itemreqrmapprovedbysm" Then
                lblHead.Text = "Item Requisition Status"
                lblSubHead.Text = "Item Requisition Status"
                pnlMain.GroupingText = "Item Requisition Status"
                Dim MaintSc_ReqId As String
                MaintSc_ReqId = Request.QueryString("ReqId")
                Dim StatusIdItem As Integer = GetRequestStatusItem(MaintSc_ReqId)

                Select Case StatusIdItem
                    Case 1032
                        Message = "Item Requisition (" + MaintSc_ReqId + ") Approved by SM successfully."
                    Case 1033
                        Message = "Item Requisition (" + MaintSc_ReqId + ") Canceled by SM successfully."
                    Case 1034
                        Message = "Item Requisition (" + MaintSc_ReqId + ") Pending by SM successfully."
                End Select

                lblMsg.Text = Message

                '*********************
            Else

                Dim StatusId As Integer = GetRequestStatus(ReqId)
                Select Case StatusId
                    Case 1001
                        Message = "Asset Requisition (" + ReqId + ") raised successfully."
                    Case 1002
                        Message = "Asset Requisition (" + ReqId + ") updated successfully."
                    Case 1003
                        Message = "Asset Requisition (" + ReqId + ") canceled successfully."
                    Case 1004
                        Message = "Asset Requisition (" + ReqId + ") approved by RM."
                    Case 1005
                        Message = "Asset Requisition (" + ReqId + ") rejected by RM."
                    Case 1006
                        Message = "Asset Requisition (" + ReqId + ") approved by Admin."
                    Case 1007
                        Message = "Asset Requisition (" + ReqId + ") rejected by Admin."
                    Case 1008
                        Message = "Asset Requisition (" + ReqId + ") approved by Budget."
                    Case 1009
                        Message = "Asset Requisition (" + ReqId + ") rejected by Budget."
                    Case 1010
                        Message = "Asset Requisition (" + ReqId + ") approved by Coordinator Check."
                    Case 1011
                        Message = "Asset Requisition (" + ReqId + ") rejected by Coordinator Check."
                    Case 1012
                        Message = "Asset Requisition (" + ReqId + ") approved for procurement."
                    Case 1013
                        Message = "Asset Requisition (" + ReqId + ") rejected for procurement."
                    Case 1014
                        Message = "PO (" + Request("PO") + ")Generated For Asset Requisition (" + ReqId + ")."
                    Case 1015
                        Message = "Asset Requisition (" + ReqId + ") updated successfully."
                    Case 1016
                        Message = "Asset Requisition (" + ReqId + ") updated successfully."
                End Select

                lblMsg.Text = Message
                SendMailTuUser()
                SendMailToRM()
            End If
        End If
    End Sub
    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function
    Private Function GetRequestStatus(ByVal ReqId As String) As String
        Dim StatusId As Integer = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_REQUISITION_GetByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, Data.DbType.String)
        Dim dr As sqlDatareader = sp.GetReader
        If dr.Read() Then
            Integer.TryParse(dr("AIR_STA_ID"), StatusId)
        End If
        Return StatusId
    End Function
    Private Function GetRequestStatusItem(ByVal ReqId As String) As String
        Dim StatusId As Integer = 0
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_CONSBLE_ITEM_REQUISITION_GetByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, Data.DbType.String)
        Dim dr As SqlDataReader = sp.GetReader
        If dr.Read() Then
            Integer.TryParse(dr("AIR_STA_ID"), StatusId)
        End If
        Return StatusId
    End Function
    Private Sub SendMailTuUser()

    End Sub
    Private Sub SendMailToRM()

    End Sub

End Class
