﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager

Partial Class WorkSpace_SMS_Webfiles_Conferencreate
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters
    Dim Objsubsonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            lblMsg.Visible = False
            If Not Page.IsPostBack Then
                'obj.Block_LoadGridConf(gvItem)
                trLName.Visible = False
                rbActions.Checked = True
                'obj.BindConf(ddlCName)
                'obj.BindCFloor2(ddlFloor)
                'obj.BindTower(ddlTower)
                'obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindConfType(ddlConftype)
                BindGrid()
                'obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASBlock", "Page_Load", exp)
        End Try
    End Sub

    Private Sub cleardata()

        txtConfrencecode.Text = String.Empty
        txtConferenceName.Text = String.Empty
        ' txtRemarks.Text = String.Empty
        txtconfcapacity.Text = String.Empty
        ddlCName.SelectedIndex = 0
        ddlFloor.SelectedIndex = 0
        ddlTower.SelectedIndex = 0
        ddlLocation.SelectedIndex = 0
        ddlCity.SelectedIndex = 0
        ddlConftype.SelectedIndex = 0
        'ddlCountry.SelectedIndex = 0
    End Sub

    Private Sub Insertdata()
        obj.getcode = txtConfrencecode.Text
        obj.getname = txtConferenceName.Text
        obj.getCapacity = CInt(txtconfcapacity.Text)
        Dim iStatus As Integer = obj.insertConf(ddlConftype, ddlFloor, ddlTower, ddlLocation, ddlCity, Me)
        If iStatus = 1 Then
            lblMsg.Text = "Conference Code Already Exists"
            lblMsg.Visible = True
        ElseIf iStatus = 2 Then
            lblMsg.Text = "Conference Inserted Successfully"
            lblMsg.Visible = True
            cleardata()
        ElseIf iStatus = 0 Then
            lblMsg.Text = "Conference Inserted Successfully"
            lblMsg.Visible = True
            cleardata()
        End If

        'obj.BindConf(ddlCName)
        'obj.BindCFloor2(ddlFloor)
        ' obj.BindTower(ddlTower)
        'obj.Bindlocation(ddlLocation)
        obj.BindCity(ddlCity)
        obj.BindConfType(ddlConftype)
        BindGrid()
        'obj.BindCountry(ddlCountry)
        'obj.Block_LoadGrid(gvItem)
    End Sub

    Private Sub Modifydata()
        obj.getcode = txtConfrencecode.Text
        obj.getname = txtConferenceName.Text
        obj.getCapacity = CInt(txtconfcapacity.Text)
        'obj.getRemarks = txtRemarks.Text
        If (obj.ModifyConf(ddlConftype, ddlFloor, ddlTower, ddlLocation, ddlCity, Me) > 0) Then
            lblMsg.Text = "Conference Updated Successfully"
            lblMsg.Visible = True
            cleardata()
        End If

        'obj.BindConf(ddlCName)
        'obj.BindCFloor2(ddlFloor)
        'obj.BindTower(ddlTower)
        'obj.Bindlocation(ddlLocation)
        obj.BindCity(ddlCity)
        obj.BindConfType(ddlConftype)
        BindGrid()
        'obj.BindCountry(ddlCountry)
        'obj.Block_LoadGrid(gvItem)
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        Try

            If rbActions.Checked = True Then
                trLName.Visible = False
                txtConfrencecode.ReadOnly = False
                ddlCity.Enabled = True
                ddlLocation.Enabled = True
                ddlTower.Enabled = True
                ddlFloor.Enabled = True
                btnSubmit.Text = "Submit"
                cleardata()
                BindGrid()
            Else
                trLName.Visible = True
                txtConfrencecode.ReadOnly = True
                ddlCity.Enabled = False
                ddlLocation.Enabled = False
                ddlTower.Enabled = False
                ddlFloor.Enabled = False

                btnSubmit.Text = "Modify"
                obj.BindConf(ddlCName)
                obj.BindCFloor2(ddlFloor)
                obj.BindTower(ddlTower)
                obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindConfType(ddlConftype)
                'obj.BindCountry(ddlCountry)
                cleardata()
                BindGrid()
            End If

        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "ConferenceMasterr", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                If txtConfrencecode.Text = String.Empty Or txtConferenceName.Text = String.Empty Or ddlFloor.SelectedItem.Text = "--Select--" Or ddlTower.SelectedItem.Text = "--Select--" Or ddlLocation.SelectedItem.Text = "--Select--" Or ddlCity.SelectedItem.Text = "--Select--" Or ddlConftype.SelectedItem.Text = "--Select--" Then
                    ' PopUpMessage("Please Enter Mandatory fields", Me)
                    lblMsg.Text = "Please Enter Mandatory fields"
                    lblMsg.Visible = True
                    'ElseIf txtRemarks.Text.Length > 500 Then
                    '    'PopUpMessage("Please Enter remarks in less than or equal to 500 characters", Me.Page)
                    '    lblMsg.Text = "Please Enter remarks in less than or equal to 500 characters"
                    '    lblMsg.Visible = True
                Else
                    Insertdata()
                End If
            Else
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If txtConfrencecode.Text = String.Empty Or txtConferenceName.Text = String.Empty Or ddlFloor.SelectedItem.Text = "--Select--" Or ddlTower.SelectedItem.Text = "--Select--" Or ddlLocation.SelectedItem.Text = "--Select--" Or ddlCity.SelectedItem.Text = "--Select--" Or ddlCName.SelectedItem.Text = "--Select--" Or ddlConftype.SelectedItem.Text = "--Select--" Then
                    'PopUpMessage("Please Enter Mandatory fields", Me)
                    lblMsg.Text = "Please Enter Mandatory fields"
                    lblMsg.Visible = True
                    'ElseIf txtRemarks.Text.Length > 500 Then
                    '    'PopUpMessage("Please Enter remarks in less than or equal to 500 characters", Me.Page)
                    '    lblMsg.Text = "Please Enter remarks in less than or equal to 500 characters"
                    '    lblMsg.Visible = True
                Else
                    Modifydata()
                End If
            End If
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException(strEroorMsg, "ConferenceMasterr", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlBName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCName.SelectedIndexChanged
        Try
            If ddlCName.SelectedItem.Value <> "--Select--" Then

                'obj.BindCFloor2(ddlFloor)
                'obj.BindTower(ddlTower)
                'obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindConfType(ddlConftype)
                ' obj.BindCountry(ddlCountry)
                'obj.Block_SelectedIndex_Changed(ddlBName, ddlFloor, ddlTower, ddlLocation, ddlCity, ddlCountry)
                obj.Block_SelectedIndex_ChangedConf(ddlCName, ddlConftype, ddlFloor, ddlTower, ddlLocation, ddlCity)
                txtConfrencecode.Text = obj.getcode
                txtConferenceName.Text = obj.getname
                txtconfcapacity.Text = obj.getCapacity
                'txtRemarks.Text = obj.getRemarks
            Else
                cleardata()
                'obj.BindConf(ddlCName)
                'obj.BindCFloor2(ddlFloor)
                'obj.BindTower(ddlTower)
                'obj.Bindlocation(ddlLocation)
                obj.BindCity(ddlCity)
                obj.BindConfType(ddlConftype)
                'obj.BindCountry(ddlCountry)
            End If
            ddlConftype.Focus()
        Catch exp As System.Exception
            Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASBlock", "ddlBName_SelectedIndexChanged", exp)
        End Try
    End Sub

    'Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
    '    Try
    '        gvItem.PageIndex = e.NewPageIndex
    '        'gvItem.Columns(2).Visible = True
    '        'gvItem.Columns(3).Visible = True
    '        obj.Block_LoadGrid(gvItem)
    '    Catch exp As System.Exception
    '        Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASBlock", "gvItem_PageIndexChanging", exp)
    '    End Try
    'End Sub

    'Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand

    '    Try
    '        If e.CommandName = "Status" Then
    '            Dim index As Integer = CType(e.CommandArgument, Integer)
    '            Dim iStatus As Integer = obj.Block_Rowcommand(gvItem, index)
    '            If iStatus = 0 Then
    '                gvItem.HeaderRow.Cells(4).Visible = False
    '                gvItem.HeaderRow.Cells(5).Visible = False
    '                For i As Integer = 0 To gvItem.Rows.Count - 1
    '                    gvItem.Rows(i).Cells(4).Visible = False
    '                    gvItem.Rows(i).Cells(5).Visible = False
    '                Next
    '                'PopUpMessage("First inactivate all the seats under this Block !", Me)
    '                lblMsg.Text = "First inactivate all the seats under this Block !"
    '                lblMsg.Visible = True
    '                Exit Sub
    '            End If
    '            obj.Block_LoadGrid(gvItem)
    '        End If
    '        cleardata()
    '        obj.BindBlock(ddlCName)
    '        obj.BindFloor2(ddlFloor)
    '        obj.BindTower(ddlTower)
    '        obj.Bindlocation(ddlLocation)
    '        obj.BindCity(ddlCity)
    '        ' obj.BindCountry(ddlCountry)
    '    Catch exp As System.Exception
    '        Throw New Amantra.Exception.DataException("Error has been occured while Updating data", "frmMASBlock", "gvItem_RowCommand", exp)
    '    End Try

    'End Sub

    'Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFloor.SelectedIndexChanged
    '    Try
    '        If (ddlFloor.SelectedIndex <> 0) Then
    '            Dim str As String = ddlFloor.SelectedValue.ToString().Trim()
    '            Dim strFlrCode As String = ddlFloor.SelectedValue.ToString().Trim().Remove(ddlFloor.SelectedValue.ToString().Trim().IndexOf("/"))
    '            Dim strTwrCode As String = str.Substring(str.IndexOf("/") + 1)

    '            Dim dr As SqlDataReader = obj.block_getflr_detls(strFlrCode, strTwrCode)
    '            If (dr.Read) Then
    '                'ddlTower.SelectedIndex = ddlTower.Items.IndexOf(ddlTower.Items.FindByValue)
    '                ddlTower.SelectedValue = dr("flr_twr_id").ToString().Trim()
    '                ddlLocation.SelectedValue = dr("flr_loc_id").ToString().Trim()
    '                ddlCity.SelectedValue = dr("flr_cty_id").ToString().Trim()
    '                '  ddlCountry.SelectedValue = dr("flr_cny_id").ToString().Trim()
    '            End If
    '        Else
    '            ddlTower.SelectedIndex = 0
    '            ddlLocation.SelectedIndex = 0
    '            ddlCity.SelectedIndex = 0
    '            ' ddlCountry.SelectedIndex = 0

    '        End If
    '    Catch exp As System.Exception
    '        Throw New Amantra.Exception.DataException("Error has been occured while retrieving data from database", "frmMASBlock", "ddlFloor_SelectedIndexChanged", exp)
    '    End Try
    'End Sub



    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex = 0 Then
            'ddlTWR.SelectedIndex = 0
            ddlFloor.SelectedIndex = 0
            ddlLocation.SelectedIndex = 0
        End If
        obj.Get_Location(ddlCity.SelectedItem.Value, ddlLocation)
        ddlLocation.Focus()
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex = 0 Then
            'ddlTWR.SelectedIndex = 0
            ddlFloor.SelectedIndex = 0
        End If
        obj.Get_Tower(ddlLocation.SelectedItem.Value, ddlTower)
        ddlTower.Focus()
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        obj.Get_Floor(ddlLocation.SelectedValue, ddlTower.SelectedValue, ddlFloor)
        ddlFloor.Focus()
    End Sub

    Public Sub BindGrid()
        Objsubsonic.BindGridView(gvItem, "GET_CONFERENCE_DETAILS_GRIDVIEW")
    End Sub

End Class
