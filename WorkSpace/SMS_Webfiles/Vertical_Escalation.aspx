<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Vertical_Escalation.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Vertical_Escalation" Title="Escalation Matrix for Vertical" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script defer type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        function refreshSelectpicker() {
            $("#<%=ddlCompany.ClientID%>").selectpicker();
            $("#<%=ddlVerticalBlockConfirmation.ClientID%>").selectpicker();
            $("#<%=ddlVerticalBlockRequest.ClientID%>").selectpicker();
            $("#<%=ddlVerticalRelease.ClientID%>").selectpicker();
        }
        refreshSelectpicker();
    </script>
    <style>
        body {
            color: black !important;
        }
    </style>

</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Country Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">
                        <asp:Label ID="lblmatrix" for="txtcode" runat="server"> Vertical Matrix </asp:Label>
                    </h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="CityPanel1" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="Radio-btn-s">
                                            <label class="btn">
                                                <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                                    ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                                                Add</label>
                                            <label class="btn">
                                                <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                                    ToolTip="Please Select Add to add new Country and Select Modify to modify the existing Country" />
                                                Modify
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblVertical" for="txtcode" runat="server">Vertical Block Requisition By<span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvrm" runat="server" ControlToValidate="ddlVerticalBlockRequest" InitialValue="--Select--" Display="None" ErrorMessage="Please Select Vertical Block Requisition By"
                                                ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlVerticalBlockRequest" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblVerticalconfirm" for="txtcode" runat="server">Vertical Block Confirmation By<span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvhr" runat="server" ControlToValidate="ddlVerticalBlockConfirmation" InitialValue="--Select--" Display="None" ErrorMessage="Please Select Vertical Block Confirmation By"
                                                ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlVerticalBlockConfirmation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblVerticalrelse" runat="server">Vertical Release By<span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="rfvre" runat="server" ControlToValidate="ddlVerticalRelease" InitialValue="--Select--"
                                                Display="None" ErrorMessage="Please Select Vertical Release By"
                                                ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlVerticalRelease" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server">Status <span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="--Select--" ControlToValidate="ddlStatus"
                                                Display="None" ErrorMessage="Please Select Status" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="0">InActive</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="Label3" runat="server">Select Company <span style="color: red;">*</span></asp:Label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlVerticalRelease" InitialValue="--Select--" Display="None"
                                                ErrorMessage="Please Select Company"
                                                ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="Label2" runat="server">Remarks </asp:Label>
                                            <asp:TextBox ID="txtRemarks" Height="30%" runat="server" CssClass="form-control" TextMode="MultiLine" MaxLength="15"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px; padding-left: 29px">
                                        <div class="form-group">
                                            <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" CausesValidation="true" ValidationGroup="Val1" />
                                            <asp:Button ID="btnModify" runat="server" CssClass="btn btn-primary custom-button-color" Text="Modify" CausesValidation="true" ValidationGroup="Val1" />
                                            <asp:Button ID="btnClear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" CausesValidation="true" />
                                            <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" CausesValidation="false" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div style="overflow: scroll">
                                            <asp:GridView ID="gvitems" runat="server" AllowSorting="true" AllowPaging="true"
                                                AutoGenerateColumns="false" CssClass="table table-condensed table-bordered table-hover table-striped">
                                                <Columns>
                                                    <asp:BoundField DataField="VM_SPC_BLK_REQ" HeaderText="Vertical" ItemStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="VM_APP_SPC_BLK_REQ" HeaderText="Vertical" ItemStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="VM_APP_VER_REL" HeaderText="Vertical" ItemStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="STA_ID" HeaderText="Vertical" ItemStyle-HorizontalAlign="Left" />
                                                </Columns>
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
