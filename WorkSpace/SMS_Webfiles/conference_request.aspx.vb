Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports Google.Apis.Calendar.v3.Data

Partial Class WorkSpace_SMS_Webfiles_conference_request
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Addusers As New AddUsers
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty
    Dim Conf_REQ_ID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtFrmDate.Attributes.Add("onClick", "displayDatePicker('" + txtFrmDate.ClientID + "')")
        txtFrmDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        lblmsg.Text = " "

        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If

        If Not Page.IsPostBack Then
            txtInternal.Text = ""
            Textinternal.Text = ""
            If Not Request.QueryString("FROM_DATE") Is Nothing Then
                txtFrmDate.Text = Request.QueryString("FROM_DATE")
                BindConfTimes()
                LoadCity()
                txtEmpId.Text = Request.QueryString("EMP_ID")
                GetUserData()
                'txtDepartment.Text = Request.QueryString("DESG_ID")
                'txtName.Text = Request.QueryString("EMP_NAME")
                'txtSpaceID.Text = Request.QueryString("SPACE_ID")
                ddlCity.ClearSelection()
                ddlCity.Items.FindByValue(Request.QueryString("CTY_CODE")).Selected = True
                Bindlocation(ddlCity.SelectedItem.Value)
                ddlSelectLocation.ClearSelection()
                ddlSelectLocation.Items.FindByValue(Request.QueryString("LCM_CODE")).Selected = True
                BindTower(ddlSelectLocation.SelectedItem.Value)
                ddlTower.ClearSelection()
                ddlTower.Items.FindByValue(Request.QueryString("TWR_CODE")).Selected = True
                BindFloor(ddlTower.SelectedItem.Value, ddlSelectLocation.SelectedItem.Value.ToString().Trim())
                ddlFloor.ClearSelection()
                ddlFloor.Items.FindByValue(Request.QueryString("FLR_CODE")).Selected = True
                BindCapacity()
                ddlCapacity.ClearSelection()
                ddlCapacity.Items.FindByValue(Request.QueryString("CAPACITY")).Selected = True
                BindConference()
                ddlConf.ClearSelection()
                ddlConf.Items.FindByValue(Request.QueryString("CONF_CODE")).Selected = True
                'BindMail()
                ddlCity.Enabled = False
                ddlSelectLocation.Enabled = False
                ddlTower.Enabled = False
                ddlFloor.Enabled = False
                ddlCapacity.Enabled = False
                ddlConf.Enabled = False
                txtFrmDate.Enabled = False
                starttimehr.Enabled = False
                endtimehr.Enabled = False

                If Not Request.QueryString("FROM_TIME") Is Nothing Then
                    starttimehr.SelectedValue = (CInt(Request.QueryString("FROM_TIME"))).ToString("d2")
                    If starttimehr.SelectedValue = 23 Then
                        endtimehr.SelectedValue = "00"
                    Else
                        endtimehr.SelectedValue = (CInt(Request.QueryString("FROM_TIME")) + 1).ToString("d2")
                    End If
                Else
                    starttimehr.Enabled = True
                    endtimehr.Enabled = True
                End If
            Else
                LoadCity()
            End If
        End If
    End Sub

    Public Sub GetUserData()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HRDGET_USER_DETAILS")
        sp.Command.AddParameter("@AUR_ID", Request.QueryString("EMP_ID"), DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            txtName.Text = ds.Tables(0).Rows(0).Item("NAME")
            txtDepartment.Text = ds.Tables(0).Rows(0).Item("DEPARTMENT")
            txtSpaceID.Text = ds.Tables(0).Rows(0).Item("SPACEID")
            ''txtMobile.Text = ds.Tables(0).Rows(0).Item("PHONE_NUBER")
            ViewState("DSN_CODE") = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
        End If
        'ddlCity.SelectedItem.Text = ds.Tables(0).Rows(0).Item("CTY_NAME")
        'ddlSelectLocation.SelectedItem.Text = ds.Tables(0).Rows(0).Item("CTY_NAME")
        'ddlTower.SelectedItem.Text = ds.Tables(0).Rows(0).Item("CTY_NAME")
        'ddlFloor.SelectedItem.Text = ds.Tables(0).Rows(0).Item("CTY_NAME")


    End Sub
    Public Sub Textinternal_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim a As String = Textinternal.Text
        If ddlCapacity.SelectedItem.Value <= txtInternal.Items.Count Then
            lblmsg.Text = "Maximum capacity reached"

        Else
            Dim item As ListItem = txtInternal.Items.FindByText(a)

            If item IsNot Nothing Then
                lblmsg.Text = "The Selected mail already there"
            Else
                txtInternal.Items.Add(a)
            End If

        End If
        Textinternal.Text = ""
    End Sub
    Public Sub Remove_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim deletedItems As New List(Of ListItem)()

        If txtInternal.SelectedItem Is Nothing Then
            'Dim message As String = "select atleast one mail"
            'Dim sb As New System.Text.StringBuilder()
            'sb.Append("<script type = 'text/javascript'>")
            'sb.Append("window.onload=function(){")
            'sb.Append("alert('")
            'sb.Append(message)
            'sb.Append("')};")
            'sb.Append("</script>")
            'ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
            lblmsg.Text = "select atleast one mail"
        Else
            For Each item As ListItem In txtInternal.Items
                If item.Selected Then
                    deletedItems.Add(item)
                End If
            Next
            For Each item As ListItem In deletedItems
                txtInternal.Items.Remove(item)
            Next
        End If

    End Sub

    'Private Sub BindMail()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CM_GET_ALL_MAIL_IDS")
    '    sp.Command.AddParameter("@CITY", ddlCity.SelectedValue)
    '    sp.Command.AddParameter("@LOCATION", ddlSelectLocation.SelectedValue)
    '    sp.Command.AddParameter("@TOWER", ddlTower.SelectedValue)
    '    sp.Command.AddParameter("@FLOOR", ddlFloor.SelectedValue)
    '    sp.Command.AddParameter("@COMPANY", Session("COMPANYID"))
    '    lstInternal.DataSource = sp.GetDataSet()
    '    lstInternal.DataTextField = "AUR_ID"
    '    lstInternal.DataValueField = "AUR_EMAIL"
    '    lstInternal.DataBind()
    'End Sub

    Public Sub LoadCity()
        Dim param As SqlParameter() = New SqlParameter(0) {}
        param(0) = New SqlParameter("@USER_ID", SqlDbType.VarChar)
        param(0).Value = Session("uid")
        ObjSubSonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE", param)
    End Sub

    Public Sub Bindlocation(ByVal strcity As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = strcity
        param(1) = New SqlParameter("@USER_ID", SqlDbType.VarChar)
        param(1).Value = Session("uid")
        ObjSubSonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Public Sub BindTower(ByVal strLoc As String)
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlCity.SelectedItem.Value
        param(1) = New SqlParameter("@usr_id", SqlDbType.VarChar)
        param(1).Value = Session("uid")
        param(2) = New SqlParameter("@VC_LOC", SqlDbType.NVarChar, 200)
        param(2).Value = ddlSelectLocation.SelectedItem.Value
        ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)

    End Sub

    Private Sub BindFloor(ByVal tower As String, ByVal loc As String)
        Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
        verDTO = verBll.GetDataforFloors(tower, loc.Trim())
        ddlFloor.DataSource = verDTO
        ddlFloor.DataTextField = "Floorname"
        ddlFloor.DataValueField = "Floorcode"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, "-- Select --")
    End Sub

    Private Sub BindCapacity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

        ddlCapacity.DataSource = sp.GetDataSet
        ddlCapacity.DataTextField = "capacity"
        ddlCapacity.DataValueField = "capacity"
        ddlCapacity.DataBind()
        ddlCapacity.Items.Insert(0, "--Select--")

    End Sub

    Public Sub BindConfTimes()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONF_TIMES")
        sp.Command.AddParameter("@DATE", txtFrmDate.Text, DbType.Date)
        Dim ds As DataSet
        ds = sp.GetDataSet
        starttimehr.DataSource = ds
        starttimehr.DataTextField = "TIMINGS"
        starttimehr.DataValueField = "TIMINGS"
        starttimehr.DataBind()
        starttimehr.Items.Insert(0, "HH")
        endtimehr.DataSource = ds
        endtimehr.DataTextField = "TIMINGS"
        endtimehr.DataValueField = "TIMINGS"
        endtimehr.DataBind()
        endtimehr.Items.Insert(0, "HH")

    End Sub
    Public Sub BindConference()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAPACITY", ddlCapacity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlConf.DataSource = ds
        ddlConf.DataTextField = "CONF_NAME"
        ddlConf.DataValueField = "CONF_CODE"
        ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONFERENCE_BOOKED_SLOTS")
        sp.Command.AddParameter("@REQ_ID", "", DbType.String)
        sp.Command.AddParameter("@SPC_ID", ddlConf.SelectedValue, DbType.String)
        sp.Command.AddParameter("@FROM_DATE", txtFrmDate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_DATE", txtFrmDate.Text, DbType.Date)
        sp.Command.AddParameter("@FROM_TIME", starttimehr.SelectedValue, DbType.String)
        sp.Command.AddParameter("@TO_TIME", endtimehr.SelectedValue, DbType.String)

        Dim ds As New DataSet
        ds = sp.GetDataSet()
        gvItem.DataSource = sp.GetDataSet()
        gvItem.DataBind()
        If endtimehr.SelectedItem.Value < starttimehr.SelectedItem.Value Then
            lblmsg.Text = "To Time should be grater than from Time"
            'btnsubmit.Visible = False
            Exit Sub
        End If

        If (ds.Tables(0).Rows.Count > 0) Then
            gvItem.Visible = True
            lblconfalert.Visible = True
            lblconfalert.Text = "Note: selected time interval is already booked, please select other available slots."
            Exit Sub
        Else
            SubmitAllocationOccupied("Conference", Session("Uid").ToString(), 2)
            lblmsg.Text = ddlConf.SelectedItem.Text & " is booked successfully ."
            cleardata()
        End If
    End Sub

    Private Sub cleardata()
        ddlCity.SelectedIndex = 0
        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlCapacity.Items.Clear()
        ddlConf.Items.Clear()
        txtFrmDate.Text = String.Empty
        starttimehr.ClearSelection()
        endtimehr.ClearSelection()
        btnsubmit.Visible = False
    End Sub

    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function

    Private Sub SubmitAllocationOccupied(ByVal strVerticalCode As String, ByVal strAurId As String, ByVal status As Integer)

        REQID = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "SSA_ID", Session("TENANT") & ".", "CONFERENCE_BOOKING")
        Dim verticalreqid As String = REQID
        Dim cntWst As Integer = 0
        Dim cntHCB As Integer = 0
        Dim cntFCB As Integer = 0
        Dim twr As String = ""
        Dim flr As String = ""
        Dim wng As String = ""
        Dim intCount As Int16 = 0
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        Dim remarks As String = ""

        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If

        If endtimehr.SelectedItem.Value <> "Hr" Then
            If endtimehr.SelectedItem.Value = "24" Then
                EndHr = "00"
            Else
                EndHr = endtimehr.SelectedItem.Text
            End If
        End If



        ftime = StartHr + ":" + StartMM
        ttime = EndHr + ":" + EndMM

        Dim sta As Integer = status

        RIDDS = RIDGENARATION("VerticalReq")

        Dim selectedValues As String = String.Empty
        For i As Integer = 0 To txtInternal.Items.Count - 1
            selectedValues = selectedValues & txtInternal.Items(i).Value + Convert.ToString(",")

        Next
        If selectedValues <> "" Then
            selectedValues = selectedValues.Remove(selectedValues.Length - 1)
        End If


        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50)
        param(0).Value = RIDDS

        If ObjSubSonic.GetSubSonicExecuteScalar("USP_CHECK_VERTICAL_REQID", param) Then
            lblmsg.Text = "Request is already raised "
            Exit Sub
        Else
            cntWst = 1

            Dim cnt As Int32 = 0
            ' For Each row As GridViewRow In gdavail.Rows
            Dim lblspcid As String   ' DirectCast(row.FindControl("lblspcid"), Label)
            lblspcid = ddlConf.SelectedItem.Value
            'Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            lblmsg.Text = ""
            ' If chkSelect.Checked = True Then
            verticalreqid = ObjSubSonic.REQGENARATION_REQ(strVerticalCode, "ssa_id", Session("TENANT") & ".", "SMS_space_allocation")
            Conf_REQ_ID = verticalreqid
            Dim param3(15) As SqlParameter

            param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
            param3(0).Value = REQID
            param3(1) = New SqlParameter("@VERTICALREQID", SqlDbType.NVarChar, 200)
            param3(1).Value = verticalreqid
            param3(2) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
            param3(2).Value = strVerticalCode
            param3(3) = New SqlParameter("@SPCID", SqlDbType.NVarChar, 200)
            param3(3).Value = lblspcid
            param3(4) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 200)
            param3(4).Value = txtEmpId.Text
            param3(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            param3(5).Value = txtFrmDate.Text
            param3(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
            param3(6).Value = txtFrmDate.Text
            param3(7) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
            param3(7).Value = "2" ' GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
            param3(8) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
            param3(8).Value = ftime
            param3(9) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
            param3(9).Value = ttime
            param3(10) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
            param3(10).Value = sta
            param3(11) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
            param3(11).Value = Session("COMPANYID")
            ' Dim replacement As String = Regex.Replace(Regex.Replace(txtInternal.Text.TrimEnd(), "\t|\n|\r", ","), ",,+", ",")
            param3(12) = New SqlParameter("@INT_ATND", SqlDbType.NVarChar, 1000)
            param3(12).Value = selectedValues 'txtInternal.Text.TrimEnd()
            param3(13) = New SqlParameter("@EXT_ATND", SqlDbType.NVarChar, 1000)
            param3(13).Value = Regex.Replace(Regex.Replace(txtAttendees.Text.TrimEnd(), "\t|\n|\r", ","), ",,+", ",") 'txtAttendees.Text.TrimEnd()
            param3(14) = New SqlParameter("@DESC", SqlDbType.NVarChar, 200)
            param3(14).Value = txtDescription.Text
            param3(15) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
            param3(15).Value = Session("Uid").ToString()
            ObjSubSonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART3", param3)

            '-------------------SEND MAIL-----------------

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_CONF_BOOKING_REQUEST")
            sp.Command.AddParameter("@REQID", REQID, DbType.String)
            sp.ExecuteScalar()
        End If

        strRedirect = "frmConference_finalpage.aspx?sta=" & clsSecurity.Encrypt("2") & "&rid=" & clsSecurity.Encrypt(REQID)
GVColor:

        'Try
        '    BookEventOnGoogleCalendar()
        'Catch ex As Exception
        '    PopUpMessage(ex.Message, Me)
        'End Try

        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect, False)
        End If

    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        'GET_SPACE_CONFERENCE
        If ddlFloor.SelectedIndex <> 0 Then
            BindConference()
        End If
    End Sub

    Private Function BookEventOnGoogleCalendar() As Boolean
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            If endtimehr.SelectedItem.Value = "24" Then
                EndHr = "00"
            Else
                EndHr = endtimehr.SelectedItem.Text
            End If
        End If

        If EndHr = "00" And StartHr = "23" Then
            EndHr = "23:59"
        End If

        ftime = txtFrmDate.Text + " " + StartHr + ":" + StartMM
        ttime = txtFrmDate.Text + " " + EndHr + ":" + EndMM

        Dim calendarservice As New clsCalendarService
        With calendarservice
            .Summary = txtDescription.Text
            .Location = ddlSelectLocation.SelectedItem.Text + "," + ddlTower.SelectedItem.Text + "," + ddlFloor.SelectedItem.Text + "," + ddlConf.SelectedItem.Text
            .Description = txtDescription.Text
            .Start = New EventDateTime
            With calendarservice.Start
                .DateTime = ftime
                '.TimeZone = "Asia/Kolkata"
                .TimeZone = Session("useroffset")
            End With

            .End = New EventDateTime
            With .End
                .DateTime = ttime
                .TimeZone = Session("useroffset")
            End With

            Dim ndx As Integer = 0
            Dim ndx1 As Integer = 0
            Dim attendees() = txtAttendees.Text.Split(",")
            .Attendees = New EventAttendee(attendees.Length - 1) {}
            For i = 0 To attendees.Length - 1
                .Attendees(ndx) = New EventAttendee
                With .Attendees(ndx)
                    .Email = (attendees(ndx)).Trim()
                End With
                ndx = ndx + 1
            Next
            Dim intattendees() = txtInternal.Text.Split(",")
            .Attendees = New EventAttendee(intattendees.Length - 1) {}
            For j = 0 To intattendees.Length - 1
                .Attendees(ndx1) = New EventAttendee
                With .Attendees(ndx1)
                    .Email = (intattendees(ndx1)).Trim()
                End With
                ndx1 = ndx1 + 1
            Next
            For i = 0 To txtInternal.Items.Count - 1
                If txtInternal.Items(i).Selected Then
                    ReDim Preserve .Attendees(ndx + 1)
                    .Attendees(ndx) = New EventAttendee
                    With .Attendees(ndx)
                        .Email = txtInternal.Items(i).Value
                    End With
                    ndx = ndx + 1
                End If
            Next
            .Recurrence = New String() {"RRULE:FREQ=DAILY;COUNT=1"}
            .Reminders = New Google.Apis.Calendar.v3.Data.Event.RemindersData()
            With .Reminders
                .UseDefault = False
                .Overrides = New EventReminder() {
                    New EventReminder() With {.Method = "email", .Minutes = 24 * 60},
                    New EventReminder() With {.Method = "popup", .Minutes = 10}
                }
            End With
        End With

        Dim Event_ID As String = calendarservice.PushEvent()
        Dim InternalAttendees As String = ""
        For Each li As ListItem In txtInternal.Items
            If li.Selected = True Then
                InternalAttendees = InternalAttendees + li.Value + ","
            End If
        Next

        InternalAttendees = InternalAttendees.TrimEnd(",")

        Dim param_Event(6) As SqlParameter
        param_Event(0) = New SqlParameter("@CONF_REQ_ID", SqlDbType.NVarChar, 1000)
        param_Event(0).Value = Conf_REQ_ID
        param_Event(1) = New SqlParameter("@CONF_EVENT_ID", SqlDbType.NVarChar, 1000)
        param_Event(1).Value = Event_ID
        param_Event(2) = New SqlParameter("@CONF_EVENT_STATUS", SqlDbType.Int)
        param_Event(2).Value = 1
        param_Event(3) = New SqlParameter("@CONF_DESCRIPTION", SqlDbType.NVarChar, 1000)
        param_Event(3).Value = txtDescription.Text
        param_Event(4) = New SqlParameter("@CREATED_BY", SqlDbType.NVarChar, 100)
        param_Event(4).Value = Session("UID")
        param_Event(5) = New SqlParameter("@EXTERNAL_ATTENDEES", txtAttendees.Text)
        'param_Event(5).Value = txtAttendees.Text
        param_Event(6) = New SqlParameter("@INTERNAL_ATTENDEES", InternalAttendees)
        ObjSubSonic.GetSubSonicExecute("CONF_GOOGLE_NEW_EVENT_CREATE", param_Event)

    End Function
End Class
