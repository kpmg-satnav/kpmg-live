﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmLeaseSummaryReport.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmLeaseSummaryReport" %>

<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>

    <table id="table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">Lease Summary Detail Report<hr
                        align="center" width="60%" /> </asp:Label>
            </td>
        </tr>
        <tr>
            <td width="100%" align="center"></td>
        </tr>
    </table>
    <table id="table3" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
        <tr>
            <td>
                <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                <strong>&nbsp;Lease Summary Detail Report</strong>
            </td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
            <td align="left">
                <asp:Button ID="btnexporttoexcel" runat="Server" Text="Export to Excel" CssClass="button" Style="clear: both" />
                <br />
                <table cellpadding="1" cellspacing="0" width="100%" border="1">
                    <tr>
                        <td align="center" colspan="2">
                            <cc1:ExportPanel ID="exportpanel1" runat="server">
                                <asp:GridView ID="gvLeaseDetails" runat="server" AllowPaging="True" AllowSorting="False"
                                    RowStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" Width="100%"
                                    PageSize="20" AutoGenerateColumns="false" EmptyDataText="No Records Found" OnRowDataBound="gvLeaseDetails_RowDataBound" ShowFooter="true">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Property Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpropertyName" runat="server" Text='<%#Eval("PN_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Land Lord">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLandLard" runat="server" Text='<%#Bind("LANDLORD_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLocation" runat="server" Text='<%#Bind("CTY_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Basic Rent(INR)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBasicRent" runat="server" Text='<%#Eval("BASIC_RENT")%>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Maintenance Cost(INR)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMaintenanceCost" runat="server" Text='<%#Eval("MAINTENANCE_COST")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Rent(INR)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTotalRent" runat="server" Text='<%#Eval("TOTAL_RENT")%>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal" runat="server" />
                                            </FooterTemplate>
                                             <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </cc1:ExportPanel>
                        </td>
                    </tr>
                </table>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>

</asp:Content>

