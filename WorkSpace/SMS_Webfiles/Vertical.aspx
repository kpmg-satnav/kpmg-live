<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="Vertical.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Vertical" Title="Check Vacant spaces" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="center" width="100%">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                        ForeColor="Black">
             <hr align="center" width="60%" />Vacant Space for verticals</asp:Label></td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
            <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td align="left" width="100%" colspan="3">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Vacant Space for verticals </strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val2" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <table id="tab1" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                            <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                    Select Vertical<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlvertical"
                                        Display="None" ErrorMessage="Please Select Vertical" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                    <asp:DropDownList ID="ddlvertical" runat="server" CssClass="clsComboBox" Width="90%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                               <tr>
                                <td align="left" style="height: 26px; width: 50%">
                                   Enter Count<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcount"
                                        Display="None" ErrorMessage="Please Enter Count" ValidationGroup="Val1" ></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revpincode" runat="server" ControlToValidate="txtcount"
                                                ErrorMessage="Please enter Count in Numerics" Display="None" ValidationGroup="Val1"
                                                ValidationExpression="^[0-9 ]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" style="height: 26px; width: 50%">
                                   <asp:TextBox ID="txtcount" runat="server" CssClass="clsTextField" Width="97%"></asp:TextBox>
                                </td>
                            </tr>
                           <tr>
                           <td align="center" colspan="2" style="height: 28px">
                           <asp:Button ID="btnsubmit" runat="server" CssClass="button"  Text="Submit" CausesValidation="true" ValidationGroup="Val1"/>
                           </td>
                           </tr>
                        <tr>
                        <td align="left" colspan="2">
                        <asp:GridView ID="gvitems" runat="server" AutoGenerateColumns="False" AllowPaging="true" Width="100%" EmptyDataText="No Records Found" PageSize="20">
                        <Columns>
                        <asp:TemplateField HeaderText="Location code">
                        <ItemTemplate>
                        <asp:Label ID="lblloccode" runat="server" Text=<%#Eval("LCM_CODE") %>></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Location name">
                        <ItemTemplate>
                        <asp:Label ID="lbllocname" runat="server" Text=<%#Eval("LCM_NAME") %>></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Tower code">
                        <ItemTemplate>
                        <asp:Label ID="lbltcode" runat="server" Text=<%#Eval("TWR_CODE") %>></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Tower name">
                        <ItemTemplate>
                        <asp:Label ID="lbltname" runat="server" Text=<%#Eval("TWR_NAME") %>></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Floor code">
                        <ItemTemplate>
                        <asp:Label ID="lblfcode" runat="server" Text=<%#Eval("FLR_CODE") %>></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Floor name">
                        <ItemTemplate>
                        <asp:Label ID="lblfname" runat="server" Text=<%#Eval("FLR_NAME") %>></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                        <ItemTemplate>
                        <asp:Label ID="lblvert" runat="server" Text=<%#Eval("SSA_VERTICAL") %>></asp:Label>
                        </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Count">
                        <ItemTemplate>
                      
  <a href="#" onclick="showPopWin('Verticalvacantseats.aspx?BDG_ID=<%#Eval("LCM_CODE") %>&TWR_ID=<%#Eval("TWR_CODE") %>&FLR_ID=<%#Eval("FLR_CODE") %>&VERT_CODE=<%#Eval("SSA_VERTICAL") %>',820,500,null)"><%#Eval("vancant_seat_count") %></a>
                        
                                      
                        </ItemTemplate>
                        </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
                        </td>
                        </tr>
                        </table>
                        
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
