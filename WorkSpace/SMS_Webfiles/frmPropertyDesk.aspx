<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPropertyDesk.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmPropertyDesk" Title="Property Desk" %>

<%@ Register Src="../../Controls/PropertyDesk.ascx" TagName="PropertyDesk" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="View Work Order" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Tenant Search</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" class="form-horizontal well" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                                <uc1:PropertyDesk ID="PropertyDesk1" runat="server" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block --%>

    <%--<div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View Tenant Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer"> 
                    
                </div>
            </div>
        </div>
    </div>--%>
    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View Tenant Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <iframe id="modalcontentframe" src="#" width="100%" height="450px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>


    <script defer>
        function showPopWin(id) {
            $("#modalcontentframe").attr("src", "frmTenantDetails.aspx?tenant=" + id);
            $("#myModal").modal().fadeIn();
        }
    </script>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

