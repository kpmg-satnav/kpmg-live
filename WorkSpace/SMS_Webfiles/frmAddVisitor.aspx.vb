Imports System.Data
Imports System.Data.SqlClient
Imports System.Security
Imports System.web.Security
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System
Imports System.Text.RegularExpressions
Imports System.Data.OleDb




Partial Class WorkSpace_SMS_Webfiles_frmAddVisitor
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Public strSQL As String = ""
    Public strSQL1 As String = ""
    Public str As String = ""
    Public strSelImg As String = ""
    Public strRem As String = ""
    Dim intFileNameLength As Integer
    Dim strFileNamePath As String
    Dim strFileNameOnly As String

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    ' ------------------- Json ------------------------------------
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/WorkSpace/Reports/Source/themes/flick/jquery-ui.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/WorkSpace/Reports/Source/css/ui.jqgrid.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js/jquery.min.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js/i18n/grid.locale-en.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js/jquery.jqGrid.min.js")))
    '    'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js_source/TowerwiseUtilization.js")))
    '    '--------------------------------------------------------------
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))

    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = String.Empty Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            Try



            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub radvis_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radvis.SelectedIndexChanged
        btnSubmit.Visible = False
        dgMain.Visible = False
        If radvis.SelectedItem.Value = 1 Then
            pnlexcel.Visible = True
            pnlform.Visible = False
            'ExPanel.Visible = False
            lblMessage.Visible = False
        Else
            pnlexcel.Visible = False
            pnlform.Visible = True
            'ExPanel.Visible = True
            lblMessage.Visible = False
        End If
    End Sub


    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            'pnlEx.Visible = True
            If txtAdd.Text = "" Then

                lblMessage.Text = "Please Enter Number of Visitors"
                lblMessage.Visible = True
                Exit Sub
            End If
            If txtAdd.Text < 0 Then
                lblMessage.Text = "Please Enter Positive Numbers only"
                lblMessage.Visible = True
                Exit Sub
            End If
            If txtAdd.Text = 0 Then
                dgMain.Visible = False
                btnSubmit.Visible = False
                Exit Sub
            End If

            Dim i As Integer
            Dim str As Integer = CInt(txtAdd.Text)
            Dim dt As New DataTable
            dt.Columns.Add("sno")
            For i = 0 To str - 1
                Dim dr As DataRow = dt.NewRow
                dr(0) = i + 1
                dt.Rows.Add(dr)
            Next
            dgMain.HeaderStyle.CssClass = "clslabel"
            dgMain.DataSource = dt
            dgMain.DataBind()
            dgMain.Visible = True
            btnSubmit.Visible = True
            'ExPanel.Visible = False
        Catch ex As Exception
            lblMessage.Text = "Please Do Not Enter Characters and Special Characters"
            lblMessage.Visible = True
            Exit Sub
        End Try
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim i, row As Integer
        Dim column As String
        'pnlEx.Visible = False
        Try
            Dim ex As Exception
            Dim ex1 As Exception
            
            For i = 0 To dgMain.Items.Count - 1
                row = i
                Dim txt As TextBox = CType(dgMain.Items(i).Cells(1).FindControl("txtName"), TextBox)
                If txt.Text = "" Then
                    column = dgMain.Columns(1).HeaderText
                    Throw ex
                End If
                Dim txt2 As TextBox = CType(dgMain.Items(i).Cells(2).FindControl("txtEmail"), TextBox)
                If txt2.Text = "" Then
                    column = dgMain.Columns(2).HeaderText
                    Throw ex
                End If
                Dim reg As New Regex("\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
                Dim mat As Match = reg.Match(txt2.Text)
                If mat.Captures.Count = 0 Then
                    lblMessage.Text = "Excel Format is not correct in Row:" & i + 1 & "Column E-Mail"
                    Exit Sub
                End If
                Dim txt3 As TextBox = CType(dgMain.Items(i).Cells(3).FindControl("txtDomain"), TextBox)
                If txt3.Text = "" Then
                    column = dgMain.Columns(3).HeaderText
                    Throw ex
                End If
                Dim txt4 As TextBox = CType(dgMain.Items(i).Cells(4).FindControl("txtGroup"), TextBox)
                If txt4.Text = "" Then
                    column = dgMain.Columns(4).HeaderText
                    Throw ex
                End If
                Dim txt5 As TextBox = CType(dgMain.Items(i).Cells(5).FindControl("txtRelation"), TextBox)
                If txt5.Text = "" Then
                    column = dgMain.Columns(5).HeaderText
                    Throw ex
                End If
                Dim txt6 As TextBox = CType(dgMain.Items(i).Cells(6).FindControl("txtRemarks"), TextBox)
                If txt6.Text = "" Then
                    column = dgMain.Columns(6).HeaderText
                    Throw ex
                End If



                Dim param(6) As SqlParameter
                param(0) = New SqlParameter("@VIS_NAME", SqlDbType.NVarChar, 200)
                param(0).Value = Replace(Trim(txt.Text), "'", "''")
                param(1) = New SqlParameter("@VIS_EMAIL", SqlDbType.NVarChar, 200)
                param(1).Value = Replace(Trim(txt2.Text), "'", "''")
                param(2) = New SqlParameter("@VIS_DOMAIN", SqlDbType.NVarChar, 200)
                param(2).Value = Replace(Trim(txt3.Text), "'", "''")
                param(3) = New SqlParameter("@VIS_GROUP", SqlDbType.NVarChar, 200)
                param(3).Value = Replace(Trim(txt4.Text), "'", "''")
                param(4) = New SqlParameter("@VIS_RELATION", SqlDbType.NVarChar, 200)
                param(4).Value = Replace(Trim(txt5.Text), "'", "''")
                param(5) = New SqlParameter("@VIS_REQ_ID", SqlDbType.NVarChar, 200)
                param(5).Value = Request.QueryString("req_id")
                param(6) = New SqlParameter("@VIS_REMARKS", SqlDbType.NVarChar, 200)
                param(6).Value = Replace(Trim(txt6.Text), "'", "''")
                objsubsonic.GetSubSonicExecute("VISITORS_ADD", param)

                'This IF condition checks whether you enetring the Visitors into DB while Request first time or Modyfying existing Request.
                'If Request.QueryString("strFromChanges") = "Modify" Then
                '    strSQL = "INSERT INTO VISITORS (VIS_NAME,VIS_EMAIL,VIS_DOMAIN,VIS_GROUP,VIS_RELATION,VIS_REQ_ID,VIS_REMARKS) VALUES ('" & Replace(Trim(txt.Text), "'", "''") & "','" & Replace(Trim(txt2.Text), "'", "''") & "','" & Replace(Trim(txt3.Text), "'", "''") & "','" & Replace(Trim(txt4.Text), "'", "''") & "','" & Replace(Trim(txt5.Text), "'", "''") & "','" & Request.QueryString("strReqID") & "','" & Replace(Trim(txt6.Text), "'", "''") & "')"


                'Else
                '    strSQL = "INSERT INTO VISITORS (VIS_NAME,VIS_EMAIL,VIS_DOMAIN,VIS_GROUP,VIS_RELATION,VIS_REQ_ID,VIS_REMARKS) VALUES ('" & Replace(Trim(txt.Text), "'", "''") & "','" & Replace(Trim(txt2.Text), "'", "''") & "','" & Replace(Trim(txt3.Text), "'", "''") & "','" & Replace(Trim(txt4.Text), "'", "''") & "','" & Replace(Trim(txt5.Text), "'", "''") & "','" & Session("rid") & "','" & Replace(Trim(txt6.Text), "'", "''") & "')"
                'End If


                'ExecuteQry(strSQL)
                dgMain.Visible = False
                'Session("List") = "visitors"
                Dim strScript As String
                strScript = "@SCRIPT>window.opener.document.forms(0).submit();" & _
                "self.close()@/SCRIPT>"
                Page.RegisterClientScriptBlock("CloseChild", strScript.Replace("@", "<"))
            Next
        Catch ex As Exception
            lblMessage.Text = "There is an Unfilled text box at Row:" & row + 1 & " and Column:" & column
        End Try

    End Sub


    Private Sub btnupload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupload.Click
       
        If Not (uploadexcel.PostedFile Is Nothing) Then
            strFileNamePath = uploadexcel.PostedFile.FileName
            intFileNameLength = InStr(1, StrReverse(strFileNamePath), "\")
            strFileNameOnly = strFileNamePath 'Mid(strFileNamePath, (Len(strFileNamePath) - intFileNameLength) + 2)
            Dim paths = Server.MapPath("UploadFiles")
            'paths = paths & "..\..\UploadFiles\"
            If File.Exists(paths & strFileNameOnly) Then
                lblMessage.Text = "File of Similar name already Exist,Choose other name"
                Exit Sub
            ElseIf strFileNameOnly = "" Then
                Exit Sub
            Else
                strFileNameOnly = Session("UID") & "-" & Session("Acountry") & "-" & Format(getoffsetdate(Date.Today), "mm-dd-yyyy").Replace("/", "-") & ".xls"
                uploadexcel.PostedFile.SaveAs(paths & "\" & strFileNameOnly)
                'lblMessage.Text = "File Upload Success."
                validateData()
                Session("Img") = strFileNameOnly
            End If
        End If
    End Sub

    Private Sub validateData()
        Dim myDataset As New DataSet
        Dim strConn As String = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "Data Source=" & Server.MapPath(".") & "/UploadFiles/" & strFileNameOnly & ";" & _
        "Extended Properties=Excel 8.0;"
        ''You must use the $ after the object you reference in the spreadsheet
        strSQL = "SELECT * FROM [Visitors$]"
        Dim myData As New OleDbDataAdapter(strSQL, strConn)
        myData.TableMappings.Add("Table", "ExcelTest")
        myData.Fill(myDataset)

        dgexcel.DataSource = myDataset.Tables(0).DefaultView
        dgexcel.DataBind()
        Dim j As Integer
        Dim str1 As Integer = dgexcel.Items.Count
        Dim dt1 As New DataTable
        dt1.Columns.Add("sno")
        For j = 0 To str1 - 1
            Dim dr1 As DataRow = dt1.NewRow
            dr1(0) = j + 1
            dt1.Rows.Add(dr1)
        Next
        dgMain.HeaderStyle.CssClass = "clslabel"
        dgMain.DataSource = dt1
        dgMain.DataBind()
        dgMain.Visible = True
        For j = 0 To dgMain.Items.Count - 1
            Dim txt As TextBox = CType(dgMain.Items(j).Cells(1).FindControl("txtName"), TextBox)
            Dim txt2 As TextBox = CType(dgMain.Items(j).Cells(2).FindControl("txtEmail"), TextBox)
            Dim txt3 As TextBox = CType(dgMain.Items(j).Cells(3).FindControl("txtDomain"), TextBox)
            Dim txt4 As TextBox = CType(dgMain.Items(j).Cells(4).FindControl("txtGroup"), TextBox)
            Dim txt5 As TextBox = CType(dgMain.Items(j).Cells(5).FindControl("txtRelation"), TextBox)
            Dim txt6 As TextBox = CType(dgMain.Items(j).Cells(6).FindControl("txtRemarks"), TextBox)
            txt.Text = Replace(dgexcel.Items(j).Cells(0).Text, "&nbsp;", "")
            txt2.Text = Replace(dgexcel.Items(j).Cells(1).Text, "&nbsp;", "")
            txt3.Text = Replace(dgexcel.Items(j).Cells(2).Text, "&nbsp;", "")
            txt4.Text = Replace(dgexcel.Items(j).Cells(3).Text, "&nbsp;", "")
            txt5.Text = Replace(dgexcel.Items(j).Cells(4).Text, "&nbsp;", "")
            txt6.Text = Replace(dgexcel.Items(j).Cells(5).Text, "&nbsp;", "")
        Next
        btnSubmit.Visible = True
    End Sub
End Class
