<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmLocationReport.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmLocationReport"
    Title="Location Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script defer src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <script defer language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>

    <script defer language="javascript" type="text/javascript" src="../../Scripts/Cal.js"></script>

    <div>
        <table id="table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%">Location Report<hr
                        align="center" width="60%" /> </asp:Label>
                </td>
            </tr>
            <tr>
                <td width="100%" align="center">
                </td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="85%" Height="100%">
            <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
                border="0">
                <tr>
                    <td colspan="3" align="left">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10px">
                        <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp; Location Report </strong>
                    </td>
                    <td style="width: 17px">
                        <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif" style="width: 10px">
                        &nbsp;</td>
                    <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <table id="tab" runat="server" cellpadding="2" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td align="left" style="height: 26px; width: 25%">
                                    From Date(MM/DD/YYYY)
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox ID="txtFdate" runat="server" CssClass="clsTextField" Width="97%">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 25%">
                                    To Date(MM/DD/YYYY)
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:TextBox ID="txtTdate" runat="server" CssClass="clsTextField" Width="97%">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 26px; width: 25%">
                                    Select Location
                                </td>
                                <td align="left" style="height: 26px; width: 25%">
                                    <asp:DropDownList ID="ddllocation" runat="server" CssClass="clsComboBox" Width="97%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnSubmit" runat="Server" CssClass="button" Text="Submit" />
                                </td>
                            </tr>
                        </table>
                        <table id="t1" runat="Server" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                        <td align="left">
                        <asp:Label ID="lblawt1" runat="server"></asp:Label>
                        </td>
                        </tr>
                        <tr>
                        <td align="left">
                        <asp:Label ID="lblowt1" runat="server"></asp:Label>
                        </td>
                        </tr>
                        <tr>
                        <td align="left">
                        <asp:Label ID="lblAwi1" runat="server"></asp:Label>
                        </td>
                        </tr>
                        <tr>
                        <td align="left">
                        <asp:Label ID="lblowi1" runat="server"></asp:Label>
                        </td>
                        </tr>
                        <tr>
                        <td align="left">
                        <asp:Label ID="lblawbpo1" runat="server"></asp:Label>
                        </td>
                        </tr>
                        <tr>
                        <td align="left">
                        <asp:Label ID="lblowbpo1" runat="server"></asp:Label>
                        </td>
                        </tr>
                        <tr>
                        <td align="left">
                        <asp:Label ID="lblawstl1" runat="server"></asp:Label>
                        </td>
                        </tr>
                        <tr>
                        <td align="left">
                        <asp:Label ID="lblowstl1" runat="server"></asp:Label>
                        </td>
                        </tr>
                        </table>
                        
                        <asp:Button ID="btnexport" runat="server" CssClass="button" Text="Export to Excel" />
                        <asp:Panel ID="pnlgrid" runat="Server" Width="100%">
                            <asp:GridView ID="gvitems" runat="server" AllowPaging="True" AllowSorting="True"
                                AutoGenerateColumns="False" Width="100%" ShowFooter="true">
                                <Columns>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblloc" runat="server" Text='<%#Eval("LOCATION_CODE") %>'></asp:Label>
                                            />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="LOCATION_NAME" HeaderText="LOCATION" SortExpression="LOCATION_NAME"
                                        FooterText="Total" >
                                          <FooterStyle Font-Bold="True" />
                                            </asp:BoundField>
                                    <asp:TemplateField HeaderText="Available WT" SortExpression="AVAILABLE_WT">
                                        <ItemTemplate>
                                            <asp:Label ID="lblawt" runat="Server"  Text='<%#Eval("AVAILABLE_WT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTawt" runat="server" CssClass="label"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Occupied WT" SortExpression="OCCUPIED_WT">
                                        <ItemTemplate>
                                            <asp:Label ID="lblowt" runat="Server"  Text='<%#Eval("OCCUPIED_WT") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTowt" runat="server" CssClass="label"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Available WI" SortExpression="AVAILABLE_WI">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAwi" runat="Server"  Text='<%#Eval("AVAILABLE_WI") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTawi" runat="server" CssClass="label"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Occupied WI" SortExpression="OCCUPIED_WI">
                                        <ItemTemplate>
                                            <asp:Label ID="lblowi" runat="Server" Text='<%#Eval("OCCUPIED_WI") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTowi" runat="server" CssClass="label"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Available WBPO" SortExpression="AVAILABLE_WBPO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblawbpo" runat="Server"  Text='<%#Eval("AVAILABLE_WBPO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTawbpo" runat="server" CssClass="label"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Occupied WBPO" SortExpression="OCCUPIED_WBPO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblowbpo" runat="Server"  Text='<%#Eval("OCCUPIED_WBPO") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTowbpo" runat="server" CssClass="label"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Available WSTL" SortExpression="AVAILABLE_WSTL">
                                        <ItemTemplate>
                                            <asp:Label ID="lblawstl" runat="Server"  Text='<%#Eval("AVAILABLE_WSTL") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTawstl" runat="server" CssClass="label"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Occupied WSTL" SortExpression="OCCUPIED_WSTL">
                                        <ItemTemplate>
                                            <asp:Label ID="lblowstl" runat="Server"  Text='<%#Eval("OCCUPIED_WSTL") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTowstl" runat="server" CssClass="label"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="MODIFIED_DATE" HeaderText="Modified Date" SortExpression="MODIFIED_DATE" />
                                    <%--    <asp:ButtonField Text="EDIT" CommandName="EDIT" />--%>
                                </Columns>
                                <PagerStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
