Imports System.Configuration.ConfigurationManager
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Net.Mail
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions

Partial Class SpaceManagement_frmSpaceVerticalAllocations
    Inherits System.Web.UI.Page
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty
    Dim strRedirect As String = String.Empty

    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("uid") = "" Then
            Response.Redirect(Application("logout"))
        End If
       


        If Not Page.IsPostBack Then

            Try
                BindReqidtoDropDown()
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforTowers("Amp")

                BindVertical()
                'objMaster.BindVerticalAllocationData(ddlVertical)


                ddlTower.DataSource = verDTO
                ddlTower.DataTextField = "Towername"
                ddlTower.DataValueField = "Towercode"
                ddlTower.DataBind()
                ddlTower.Items.Insert(0, "-- Select --")
                ' txtVertical.Text = "Vertical"
                tblDetails.Visible = False
                '  ddlMonth.Items.Insert(0, "-- Select --")
                'ddlYear.Items.Insert(0, "-- Select --")
                ddlFloor.Items.Insert(0, "-- Select --")
                ddlWing.Items.Insert(0, "-- Select --")
            Catch ex As Exception
                ' Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Allocation", "Load", ex)
            End Try


        End If
    End Sub
    Private Sub BindVertical()

        Dim UID As String = ""
        UID = Session("uid")

        Dim param(1) As SqlParameter

        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = UID
        param(1) = New SqlParameter("@MODE", SqlDbType.Int)
        param(1).Value = 1

        ObjSubSonic.Binddropdown(ddlVertical, "GET_COSTCENTER_AURID_ALLLOCATE", "COST_CENTER_NAME", "COST_CENTER_CODE", param)


    End Sub


    Public Sub BindReqidtoDropDown()
        Dim reqDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
        Dim dt As DataTable = verBll.GetRequIdsForVertical()
        ddlReqid.DataSource = dt
        ddlReqid.DataTextField = "RequestID"
        ddlReqid.DataValueField = "RequestID"
        ddlReqid.DataBind()
        ddlReqid.Items.Insert(0, "-- Select --")
    End Sub

    Protected Sub ddlReqid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReqid.SelectedIndexChanged
        Try
            If ddlReqid.SelectedIndex <> -1 And ddlReqid.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDatesforreqID(ddlReqid.SelectedValue)
                txtMonth.Text = Convert.ToDateTime(verDTO(0).MonthName).ToShortDateString
                txtYear.Text = Convert.ToDateTime(verDTO(0).MonthID).ToShortDateString
                tblDetails.Visible = False
                clearFields()
                getData()
                Dim reqDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                reqDTO = verBll.GetPartialAllocVerticalData(ddlReqid.SelectedValue, Convert.ToDateTime(txtMonth.Text), Convert.ToDateTime(txtYear.Text))
                txtWSRequired.Text = Convert.ToInt32(txtWSRequired.Text) - Convert.ToInt32(reqDTO(0).WorkStationsAllocated)
                txtFullCabin.Text = Convert.ToInt32(txtFullCabin.Text) - Convert.ToInt32(reqDTO(0).FullCabinsAllocated)
                txtHalfCabin.Text = Convert.ToInt32(txtHalfCabin.Text) - Convert.ToInt32(reqDTO(0).HalfCabinsAllocated)
                tblDetails.Visible = True
                ddlTower.SelectedIndex = 0
            Else
                tblDetails.Visible = False
            End If
        Catch ex As Exception
            '  Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Allocation", "Load", ex)
        End Try


    End Sub

    Protected Sub btnGet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGet.Click
        Try

            'If ddlReqid.SelectedIndex = -1 Or ddlReqid.SelectedIndex = 0 Or txtMonth.Text = String.Empty Or txtYear.Text = String.Empty Then
            '    PopUpMessage("Please select all mandatory fields!", Me)
            '    Exit Sub
            'End If

            'getData()
            'Dim reqDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            'reqDTO = verBll.GetPartialAllocVerticalData(ddlReqid.SelectedValue, Convert.ToDateTime(txtMonth.Text), Convert.ToDateTime(txtYear.Text))
            'txtWSRequired.Text = Convert.ToInt32(txtWSRequired.Text) - Convert.ToInt32(reqDTO(0).WorkStationsAllocated)
            'txtFullCabin.Text = Convert.ToInt32(txtFullCabin.Text) - Convert.ToInt32(reqDTO(0).FullCabinsAllocated)
            'txtHalfCabin.Text = Convert.ToInt32(txtHalfCabin.Text) - Convert.ToInt32(reqDTO(0).HalfCabinsAllocated)
            'tblDetails.Visible = True
        Catch ex As Exception
            '  Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Allocation", "Load", ex)
        End Try


    End Sub

    Sub clearFields()
        txtFullCabin.Text = ""
        txtHalfCabin.Text = ""
        txtWSRequired.Text = ""
        txtRequesterName.Text = ""
        txtRequestorRemarks.Text = ""
        txtLocation.Text = ""
        txtVertical.Text = ""
        Try
            ddlTower.SelectedIndex = 0
        Catch ex As Exception

        End Try
        Try
            ddlFloor.SelectedIndex = 0
        Catch ex As Exception

        End Try
        Try
            ddlWing.SelectedIndex = 0
        Catch ex As Exception

        End Try

        'lstfullcabins.Items.Clear()
        'lstHalfCabins.Items.Clear()
        'lstwrkStations.Items.Clear()
        gvWs.DataSource = Nothing
        gvWs.DataBind()
        gvHC.DataSource = Nothing
        gvHC.DataBind()
        gvFC.DataSource = Nothing
        gvFC.DataBind()
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, txtHidLocCode.Text.Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "-- Select --")
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFloor.SelectedIndexChanged
        Try
            If ddlFloor.SelectedIndex <> -1 And ddlFloor.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforWings(ddlFloor.SelectedValue, ddlTower.SelectedValue.ToString().Trim(), txtHidLocCode.Text.Trim())
                ddlWing.DataSource = verDTO
                ddlWing.DataTextField = "WingName"
                ddlWing.DataValueField = "WingCode"
                ddlWing.DataBind()
                ddlWing.Items.Insert(0, "-- Select --")
            Else
                ddlWing.Items.Clear()
                'lstwrkStations.Items.Clear()
                'lstfullcabins.Items.Clear()
                'lstHalfCabins.Items.Clear()
            End If
            gvWs.DataSource = Nothing
            gvWs.DataBind()
            gvHC.DataSource = Nothing
            gvHC.DataBind()
            gvFC.DataSource = Nothing
            gvFC.DataBind()
        Catch ex As Exception
            ' Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Allocation", "Load", ex)
        End Try


    End Sub

    Protected Sub getData()
        Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
        verDTO = verBll.GetDataforreqID(ddlReqid.SelectedValue, Convert.ToDateTime(txtMonth.Text), Convert.ToDateTime(txtYear.Text))
        txtFullCabin.Text = verDTO(0).FullCabins
        txtHalfCabin.Text = verDTO(0).HalfCabins
        txtWSRequired.Text = verDTO(0).WorkStations
        txtRequesterName.Text = verDTO(0).RequestorName
        txtRequestorRemarks.Text = verDTO(0).Remarks
        txtLocation.Text = verDTO(0).LocationName
        txtVertical.Text = verDTO(0).getvertical
        TxtCity.Text = verDTO(0).CityCode
        txtHidLocCode.Text = verDTO(0).LocationCode
        txtLabSpace.Text = verDTO(0).LabSapce
        txtFromDate.Text = verDTO(0).FromDate
        txtToDate.Text = verDTO(0).ToDate
        verDTO = verBll.GetDataforTowers(verDTO(0).LocationCode)
        ddlTower.DataSource = verDTO
        ddlTower.DataTextField = "Towername"
        ddlTower.DataValueField = "Towercode"
        ddlTower.DataBind()
        ddlTower.Items.Insert(0, "-- Select --")

        'verDTO = verBll.GetDataforSpace("HALF CABIN", frmDate, toDate, ddlTower.SelectedValue, ddlFloor.SelectedValue, ddlWing.SelectedValue)
        'lstHalfCabins.DataSource = verDTO
        'lstHalfCabins.DataTextField = "SpaceID"
        'lstHalfCabins.DataValueField = "SpaceID"
        'lstHalfCabins.DataBind()
        'verDTO = verBll.GetDataforSpace("FULL CABIN", frmDate, toDate, ddlTower.SelectedValue, ddlFloor.SelectedValue, ddlWing.SelectedValue)
        'lstfullcabins.DataSource = verDTO
        'lstfullcabins.DataTextField = "SpaceID"
        'lstfullcabins.DataValueField = "SpaceID"
        'lstfullcabins.DataBind()
        'verDTO = verBll.GetDataforSpace("WORK STATION", frmDate, toDate, ddlTower.SelectedValue, ddlFloor.SelectedValue, ddlWing.SelectedValue)
        'lstwrkStations.DataSource = verDTO
        'lstwrkStations.DataTextField = "SpaceID"
        'lstwrkStations.DataValueField = "SpaceID"
        'lstwrkStations.DataBind()
        'txtFromDate.Text = frmDate
        'txtToDate.Text = toDate
        txtVerticalName.Text = verBll.getVerticalName(txtVertical.Text)
    End Sub

    Protected Sub btnAllocate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllocate.Click
        Try
            If txtAdminRemarks.Text.Length > 500 Then
                lblMsg.Text = "Remarks Shoud be less than 500 characters !"
                Exit Sub
            End If
            If Convert.ToInt32(txtWSAllocated.Text) <= 0 And Convert.ToInt32(txtHCAllocated.Text) <= 0 And Convert.ToInt32(txtFCAllocated.Text) <= 0 Then
                lblMsg.Text = "Any one of the space should be more than zero"
                lblMsg.Visible = True
                Exit Sub
            End If
            Dim reqDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            reqDTO = verBll.GetPartialAllocVerticalData(ddlReqid.SelectedValue, Convert.ToDateTime(txtMonth.Text), Convert.ToDateTime(txtYear.Text))
            Dim iWSCountP, iFCCountP, iHCCountP As Integer
            iWSCountP = Convert.ToInt32(reqDTO(0).WorkStationsAllocated)
            iFCCountP = Convert.ToInt32(reqDTO(0).FullCabinsAllocated)
            iHCCountP = Convert.ToInt32(reqDTO(0).HalfCabinsAllocated)
            Dim iWSCount As Integer = Convert.ToInt32(txtWSAllocated.Text)
            Dim iFCCount As Integer = Convert.ToInt32(txtFCAllocated.Text)
            Dim iHCCount As Integer = Convert.ToInt32(txtHCAllocated.Text)
            Dim iWSCountL, iFCCountL, iHCCountL As Integer
            iWSCountL = 0
            iFCCountL = 0
            iHCCountL = 0
            Dim iWSCountT As Integer = Convert.ToInt32(txtWSRequired.Text)
            If iWSCount > iWSCountT Then
                lblMsg.Text = "You can Allocate maximum of " & iWSCountT & " Work Stations of " & txtWSAllocated.Text & " because remaining " & iWSCountP & "allocated already !"
                Exit Sub
            End If
            Dim iFCCountT As Integer = Convert.ToInt32(txtFullCabin.Text)
            If iFCCount > iFCCountT Then
                lblMsg.Text = "You can Allocate maximum of " & iFCCountT & " Full Cabins of " & txtFullCabin.Text & " because remaining " & iFCCountP & "allocated already !"
                Exit Sub
            End If
            Dim iHCCountT As Integer = Convert.ToInt32(txtHalfCabin.Text)
            If iHCCount > iHCCountT Then
                lblMsg.Text = "You can Allocate maximum of " & iHCCountT & " Half Cabins of" & txtHalfCabin.Text & " because remaining " & iHCCountP & "allocated already !"
                Exit Sub
            End If
            For iWs As Integer = 0 To gvWs.Rows.Count - 1
                Dim chk As HtmlInputCheckBox = CType(gvWs.Rows(iWs).FindControl("chk"), HtmlInputCheckBox)
                If chk.Checked Then
                    iWSCountL += 1
                End If

            Next
            For iWs As Integer = 0 To gvFC.Rows.Count - 1
                Dim chk As HtmlInputCheckBox = CType(gvFC.Rows(iWs).FindControl("chkFC"), HtmlInputCheckBox)
                If chk.Checked Then
                    iFCCountL += 1
                End If

            Next
            For iWs As Integer = 0 To gvHC.Rows.Count - 1
                Dim chk As HtmlInputCheckBox = CType(gvHC.Rows(iWs).FindControl("chkHC"), HtmlInputCheckBox)
                If chk.Checked Then
                    iHCCountL += 1
                End If
            Next
            If iWSCount <> iWSCountL Then
                lblMsg.Text = "Please select Exact no of Work Stations as Allocated "
                GoTo GVColor
                Exit Sub
            End If
            If iFCCount <> iFCCountL Then
                lblMsg.Text = "Please select Exact no of Full Cabins as Allocated "
                GoTo GVColor
                Exit Sub
            End If
            If iHCCount <> iHCCountL Then
                lblMsg.Text = "Please select Exact no of Half Cabins as Allocated "
                GoTo GVColor
                Exit Sub
            End If
            Dim iCount As Integer = 0
            Dim verDTO As New VerticalDTO()
            verDTO.RequestID = ddlReqid.SelectedValue
            verDTO.LocationCode = txtHidLocCode.Text
            verDTO.Towercode = ddlTower.SelectedValue
            verDTO.FloorCode = ddlFloor.SelectedValue
            verDTO.WingCode = ddlWing.SelectedValue
            verDTO.UserID = Session("uid").ToString()
            verDTO.Status = 167
            verDTO.FromDate = txtFromDate.Text
            verDTO.ToDate = txtToDate.Text
            verDTO.SvrFromDate = Convert.ToDateTime(txtMonth.Text)
            verDTO.SvrToDate = Convert.ToDateTime(txtYear.Text)
            For iWs As Integer = 0 To gvWs.Rows.Count - 1
                Dim chk As HtmlInputCheckBox = CType(gvWs.Rows(iWs).FindControl("chk"), HtmlInputCheckBox)
                If chk.Checked Then
                    verDTO.SpaceID = chk.Value
                    verDTO.SpaceType = "WORK STATION"
                    iCount = verBll.insertSpaceDetails(verDTO, txtVertical.Text)
                End If
            Next
            For iWs As Integer = 0 To gvFC.Rows.Count - 1
                Dim chk As HtmlInputCheckBox = CType(gvFC.Rows(iWs).FindControl("chkFC"), HtmlInputCheckBox)
                If chk.Checked Then
                    verDTO.SpaceID = chk.Value
                    verDTO.SpaceType = "FULL CABIN"
                    iCount = verBll.insertSpaceDetails(verDTO, txtVertical.Text)
                End If
            Next
            For iWs As Integer = 0 To gvHC.Rows.Count - 1
                Dim chk As HtmlInputCheckBox = CType(gvHC.Rows(iWs).FindControl("chkHC"), HtmlInputCheckBox)
                If chk.Checked Then
                    verDTO.SpaceID = chk.Value
                    verDTO.SpaceType = "HALF CABIN"
                    iCount = verBll.insertSpaceDetails(verDTO, txtVertical.Text)
                End If
            Next
            verDTO.WorkStations = txtWSRequired.Text
            verDTO.FullCabins = txtFullCabin.Text
            verDTO.HalfCabins = txtHalfCabin.Text
            verDTO.Status = 167
            strStatus = 167
            verDTO.Remarks = txtAdminRemarks.Text
            verDTO.WorkStationsAllocated = txtWSAllocated.Text
            verDTO.HalfCabinsAllocated = txtHCAllocated.Text
            verDTO.FullCabinsAllocated = txtFCAllocated.Text
            iCount = verBll.insertVerAllocDetails(verDTO)
            iCount = verBll.insertVerAllocChildDetails(verDTO)
            Dim intStatus As Int16 = 0
            Dim spStatus As New SqlParameter("@SVD_STA_ID", SqlDbType.NVarChar, 50)
            Dim spReqID As New SqlParameter("@SVD_REQID", SqlDbType.NVarChar, 100)
            Dim spMon As New SqlParameter("@SVD_MON", SqlDbType.DateTime)
            Dim spYear As New SqlParameter("@SVD_YEAR", SqlDbType.DateTime)
            Dim spLabSpace As New SqlParameter("@SVR_LABSPACE", SqlDbType.NVarChar, 20)
            Dim svrReqID As New SqlParameter("@SVR_REQID", SqlDbType.NVarChar, 100)
            If txtWSRequired.Text = txtWSAllocated.Text And txtHalfCabin.Text = txtHCAllocated.Text And txtFullCabin.Text = txtFCAllocated.Text Then
                intStatus = 167
                spStatus.Value = 167
                strStatus = 167
                spReqID.Value = ddlReqid.SelectedValue
                spMon.Value = Convert.ToDateTime(txtMonth.Text)
                spYear.Value = Convert.ToDateTime(txtYear.Text)
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure,  "USP_UPDATE_VER_REQ_DTLS", spStatus, spReqID, spMon, spYear)
                'To update LabSpace Value
                spLabSpace.Value = txtLabSpace.Text
                svrReqID.Value = ddlReqid.SelectedValue
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure,  "USP_UPDATE_LAB_SPACE_ALLOCATION", spLabSpace, svrReqID, spStatus)
            Else
                intStatus = 166
                If chkClsRequest.Checked = True Then
                    intStatus = 6
                    strStatus = 6
                    ' intStatus = 166
                    iCount = verBll.updateVertStatusID(ddlReqid.SelectedValue, intStatus)
                    'To update LabSpace Value
                    spLabSpace.Value = txtLabSpace.Text
                    spReqID.Value = ddlReqid.SelectedValue
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure,  "USP_UPDATE_LAB_SPACE_ALLOCATION", spLabSpace, svrReqID, spStatus)

                Else
                    iCount = verBll.updateVertStatusID(ddlReqid.SelectedValue, intStatus)
                    spStatus.Value = 166
                    strStatus = 166
                    spReqID.Value = ddlReqid.SelectedValue
                    spMon.Value = Convert.ToDateTime(txtMonth.Text)
                    spYear.Value = Convert.ToDateTime(txtYear.Text)
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure,  "USP_UPDATE_VER_REQ_DTLS", spStatus, spReqID, spMon, spYear)
                    'To update LabSpace Value
                    spLabSpace.Value = txtLabSpace.Text
                    svrReqID.Value = ddlReqid.SelectedValue
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure,  "USP_UPDATE_LAB_SPACE_ALLOCATION", spLabSpace, svrReqID, spStatus)
                End If
            End If
            Dim strFdate As String = txtMonth.Text
            Dim strTdate As String = txtYear.Text
            Dim spReq As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 100)
            spReq.Value = ddlReqid.SelectedValue
            Dim strEMail As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "USP_GET_EMAIL", spReq).ToString().Trim()
            'sendMail(ddlReqid.SelectedItem.Text, strFdate, strTdate, strEMail, txtVertical.Text.Trim(), txtLocation.Text, txtWSRequired.Text, txtWSAllocated.Text, txtHalfCabin.Text, txtHCAllocated.Text, txtFullCabin.Text, txtFCAllocated.Text)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CREATE_VER_ALLOC_MAIL")
            sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
            sp.Command.AddParameter("@REQID", ddlReqid.SelectedValue, DbType.String)
            sp.Command.AddParameter("@MAILSTATUS", 4, DbType.Int32)
            sp.ExecuteScalar()

            strRedirect = "finalpage.aspx?sta=" & clsSecurity.Encrypt("6") & "&rid=" & clsSecurity.Encrypt(ddlReqid.SelectedValue)
GVColor:
            For iWs As Integer = 0 To gvFC.Rows.Count - 1
                Dim chk As HtmlInputCheckBox = CType(gvFC.Rows(iWs).FindControl("chkFC"), HtmlInputCheckBox)
                If chk.Checked Then
                    gvFC.Rows(iWs).BackColor = Drawing.Color.LightGray
                Else
                    gvFC.Rows(iWs).BackColor = Drawing.Color.White
                End If
            Next
            lblSelecetedFC.Text = "Selected Full Cabin count is : " & iFCCountL

            For iWs As Integer = 0 To gvWs.Rows.Count - 1
                Dim chk As HtmlInputCheckBox = CType(gvWs.Rows(iWs).FindControl("chk"), HtmlInputCheckBox)
                If chk.Checked Then
                    gvWs.Rows(iWs).BackColor = Drawing.Color.LightGray
                Else
                    gvWs.Rows(iWs).BackColor = Drawing.Color.White
                End If
            Next
            lblSelectedWs.Text = "Selected Work Station count is : " & iWSCountL

            For iWs As Integer = 0 To gvHC.Rows.Count - 1
                Dim chk As HtmlInputCheckBox = CType(gvHC.Rows(iWs).FindControl("chkHC"), HtmlInputCheckBox)
                If chk.Checked Then
                    gvHC.Rows(iWs).BackColor = Drawing.Color.LightGray
                Else
                    gvHC.Rows(iWs).BackColor = Drawing.Color.White
                End If
            Next
            lblSelectedHC.Text = "Selected Half Cabin count is : " & iHCCountL
        Catch ex As Exception
            ' Throw New Amantra.Exception.DataException("This error has been occured while Updating data to Allocate Spaces", "Space Vertical Allocation", "Load", ex)
        End Try
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect)
        End If
    End Sub

    Protected Sub ddlWing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWing.SelectedIndexChanged
        Try
            If ddlWing.SelectedIndex <> -1 And ddlWing.SelectedIndex <> 0 Then
                Dim frmDate As String = txtFromDate.Text
                Dim toDate As String = txtToDate.Text
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforSpace("HALF CABIN", frmDate, toDate, ddlTower.SelectedValue, ddlFloor.SelectedValue, ddlWing.SelectedValue, txtHidLocCode.Text.Trim())
                gvHC.DataSource = verDTO
                gvHC.DataBind()
                verDTO = verBll.GetDataforSpace("FULL CABIN", frmDate, toDate, ddlTower.SelectedValue, ddlFloor.SelectedValue, ddlWing.SelectedValue, txtHidLocCode.Text.Trim())
                gvFC.DataSource = verDTO
                gvFC.DataBind()
                verDTO = verBll.GetDataforSpace("WORK SPACE", frmDate, toDate, ddlTower.SelectedValue, ddlFloor.SelectedValue, ddlWing.SelectedValue, txtHidLocCode.Text.Trim())
                gvWs.DataSource = verDTO
                gvWs.DataBind()

                lblSelectedWs.Text = "Selected Work Station count is : 0"
                lblSelectedHC.Text = "Selected Half Cabin count is : 0"
                lblSelecetedFC.Text = "Selected Full Cabin count is : 0"

                lblWSCount.Text = "Of  " & gvWs.Rows.Count & ""
                lblHCCount.Text = "Of  " & gvHC.Rows.Count & ""
                lblFCCount.Text = "Of  " & gvFC.Rows.Count & ""



                lblAvail_Occp_Count_WST.Text = getAllocated_Occupied(Trim(txtLocation.Text), ddlTower.SelectedItem.Value, ddlFloor.SelectedItem.Value, ddlWing.SelectedItem.Value, "WS")

                lblAvail_Occp_Count_HCB.Text = getAllocated_Occupied(Trim(txtLocation.Text), ddlTower.SelectedItem.Value, ddlFloor.SelectedItem.Value, ddlWing.SelectedItem.Value, "HC")

                lblAvail_Occp_Count_FCB.Text = getAllocated_Occupied(Trim(txtLocation.Text), ddlTower.SelectedItem.Value, ddlFloor.SelectedItem.Value, ddlWing.SelectedItem.Value, "FC")

            Else
                gvWs.DataSource = Nothing
                gvWs.DataBind()
                gvHC.DataSource = Nothing
                gvHC.DataBind()
                gvFC.DataSource = Nothing
                gvFC.DataBind()
            End If
        Catch ex As Exception
            ' Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Allocation", "Load", ex)
        End Try

    End Sub

    Public Function getAllocated_Occupied(ByVal BDG_ID As String, ByVal SSA_TWR_ID As String, ByVal SSA_FLR_ID As String, ByVal SSA_WNG_ID As String, ByVal SSA_SPCTYPE As String) As String
        Dim strAVAILABLE_OCCUPIED_COUNT As String = ""
        Try


            Dim parms As SqlParameter() = {New SqlParameter("@BDG_ID", SqlDbType.NVarChar, 200), _
                                      New SqlParameter("@SSA_TWR_ID", SqlDbType.NVarChar, 200), _
                                      New SqlParameter("@SSA_FLR_ID", SqlDbType.NVarChar, 200), _
                                       New SqlParameter("@SSA_WNG_ID", SqlDbType.NVarChar, 200), _
                                        New SqlParameter("@SSA_SPCTYPE", SqlDbType.NVarChar, 200)}

            parms(0).Value = BDG_ID
            parms(1).Value = SSA_TWR_ID
            parms(2).Value = SSA_FLR_ID
            parms(3).Value = SSA_WNG_ID
            parms(4).Value = SSA_SPCTYPE

            Dim dr As SqlDataReader
            dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_AVAILABLE_OCCUPIED_COUNT", parms)

            While dr.Read()
                strAVAILABLE_OCCUPIED_COUNT = dr.Item("AVAILABLE_OCCUPIED_COUNT").ToString
            End While
            If dr.IsClosed = False Then
                dr.Close()
            End If

        Catch ex As Exception

        End Try
        Return strAVAILABLE_OCCUPIED_COUNT
    End Function


    'Private Sub sendMail(ByVal strReqId As String, ByVal strMonth As String, ByVal strYear As String, ByVal toMail As String, ByVal strVertical As String, ByVal strLocation As String, ByVal reqWs As Int16, ByVal AllocWs As Int16, ByVal reqHC As Int16, ByVal AllocHC As Int16, ByVal reqFC As Int16, ByVal AllocFC As Int16)
    '    Try
    '        Dim to_mail As String = Session("uemail")
    '        Dim cc_mail As String = Session("uemail")
    '        Dim strFMG As String = String.Empty
    '        Dim strBUHead As String = String.Empty
    '        Dim body As String = String.Empty
    '        Dim strKnownas As String = String.Empty
    '        Dim strStat As String = String.Empty
    '        Dim strCC As String = String.Empty
    '        Dim strEmail As String = String.Empty
    '        Dim strRM As String = String.Empty
    '        Dim strRR As String = String.Empty
    '        Dim objData As SqlDataReader
    '        'strCC = "select aur_known_as,aur_reporting_to from amantra_user where aur_id= '" & Session("uid") & "'"
    '        'objData = SqlHelper.ExecuteReader(CommandType.Text, strCC)
    '        'While objData.Read
    '        '    strRR = objData("aur_reporting_to")
    '        '    strKnownas = objData("aur_known_as")
    '        'End While
    '        'strEmail = " select aur_email from amantra_user where aur_id='" & strRR & "'"
    '        'objData = SqlHelper.ExecuteReader(CommandType.Text, strEmail)
    '        'While objData.Read
    '        '    strRM = objData("aur_email")
    '        'End While

    '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_RM_DETAILS")
    '        sp1.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
    '        objData = sp1.GetReader()
    '        While objData.Read
    '            strRR = objData("aur_reporting_to").ToString
    '            strRM = objData("aur_reporting_email").ToString
    '            strKnownas = objData("aur_known_as").ToString
    '            to_mail = objData("aur_email").ToString
    '            'BCC = objData("BCC").ToString
    '        End While

    '        If strStatus = 166 Then
    '            strStat = "Partially Allocated"
    '        ElseIf strStatus = 167 Then
    '            strStat = "Allocated but not Approved"
    '        Else
    '            strStat = "Allocated"
    '        End If

    '        body = "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'>***********THIS IS AN AUTOGENERATED MAIL***********</td></tr></table><br /><br /> "

    '        body = body & "<table align='center' width='50%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B' align='left'>Dear <b> Sir/Madam </b> , </td></tr> </b><tr><td  style='font-family:Bookman Old Style;color:#00008B;font-size:10.5pt;padding-left:5px'><br />Requested Vertical spaces has been allotted by " & strKnownas & " . The details are as follows </td></tr></table>&nbsp;<br />"
    '        body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
    '        body = body & "<table align='center'>"
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Vertical Requisition Id</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strReqId & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strLocation & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Vertical</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlVertical.SelectedItem.Text & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Requested Date</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtMonth.Text & "</td></tr>"
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlTower.SelectedItem.Text & "</td></tr>"
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Floor</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlFloor.SelectedItem.Text & "</td></tr>"
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Wing</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & ddlWing.SelectedItem.Text & "</td></tr>"

    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Requested Status</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & strStat & "</td></tr>"
    '        body = body & "</table>"
    '        body = body & "</td></tr></table><br/><br/>"

    '        body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'><tr><td>"
    '        body = body & "<table align='center'>"
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Work Stations</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqWs & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Allocated Work Stations</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocWs & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Half Cabins</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqHC & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Allocated Half Cabins</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocHC & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Required Full Cabins</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & reqFC & "</td></tr> "
    '        body = body & "<tr><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Allocated Full Cabins</td><td style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & AllocFC & "</td></tr>"
    '        body = body & "</table>"
    '        body = body & "</td></tr></table>"


    '        body = body & "<br /> <br /><table align='center' width='50%'><tr><td colspan='3' align='left' style='font-family:Bookman Old Style;font-size:10.5pt;color:#00008B;'>Regards, <br />myAmantraAxis Team</td></tr></table><br/>"


    '        body = body & "<table width='50%' style='border-style:solid;border-width:thin;border-color:#000000' align='center'>"

    '        body = body & "<tr><td  style='background-color:white;color:#FF6600;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>For any queries, Please log a Service Ticket in Service Connect<br /><a style='color:#0000FF;' href='http://spaceconnect.wipro.com' target='_blank'>[http://spaceconnect.wipro.com]</a><br /><br />ServiceTicket is the fastest means of resolving your queries </td></tr> "

    '        body = body & "</table>"

    '        'If to_mail = String.Empty Then
    '        '    to_mail = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        'End If
    '        'Dim mail As New MailMessage
    '        'mail.From = New MailAddress(ConfigurationManager.AppSettings("AmantraEmailId").ToString)
    '        'mail.To.Add(to_mail)
    '        ''mail.CC.Add(cc_mail)
    '        'mail.Subject = "Vertical Allocation Details"
    '        'mail.IsBodyHtml = True
    '        'mail.Body = body

    '        'Dim strVRM As String = String.Empty
    '        'Dim parms As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '        'Dim parms2 As New SqlParameter("@VC_MSG", SqlDbType.Text, 200000)
    '        'Dim parms3 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '        'Dim parms4 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '        'Dim parms5 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '        'Dim parms6 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '        'Dim parms7 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '        'Dim parms8 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)


    '        'parms.Value = "Abcd"
    '        'parms2.Value = body
    '        'parms3.Value = to_mail
    '        'parms4.Value = "Vertical Allocation Details"
    '        'parms5.Value = getoffsetdatetime(DateTime.Now)
    '        'parms6.Value = "Request Submitted"
    '        'parms7.Value = "Normal Mail"
    '        'parms8.Value = strRM

    '        'SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms, parms2, parms3, parms4, parms5, parms6, parms7, parms8)


    '        'Dim strsql As String = String.Empty
    '        'Dim stVrm As String = String.Empty
    '        'Dim dr As SqlDataReader
    '        'Dim dr1 As SqlDataReader
    '        'Dim iQry As Integer = 0

    '        'strSQL1 = "Select count(VER_VRM) from VERTICAL where VER_CODE='" & ddlVertical.SelectedItem.Value & "'"
    '        'iQry = Convert.ToInt32(SqlHelper.ExecuteScalar(CommandType.Text, strSQL1))
    '        'If iQry > 0 Then
    '        '    strSQL1 = "Select VER_VRM from VERTICAL where VER_CODE='" & ddlVertical.SelectedItem.Value & "'"
    '        '    dr1 = SqlHelper.ExecuteReader(CommandType.Text, strSQL1)
    '        '    While dr1.Read
    '        '        stVrm = dr1("VER_VRM").ToString()
    '        '    End While
    '        '    strsql = "Select aur_email from amantra_user where aur_id='" & stVrm & "'"
    '        '    dr = SqlHelper.ExecuteReader(CommandType.Text, strsql)
    '        '    While dr.Read
    '        '        strVRM = dr("aur_email").ToString()
    '        '    End While
    '        'Else
    '        '    strVRM = ConfigurationManager.AppSettings("AmantraEmailId").ToString
    '        'End If

    '        Dim parms10 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '        Dim parms11 As New SqlParameter("@VC_MSG", SqlDbType.VarChar)
    '        Dim parms12 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '        Dim parms13 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '        Dim parms14 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '        Dim parms15 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '        Dim parms16 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '        Dim parms17 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '        parms10.Value = "Abcd"
    '        parms11.Value = body
    '        parms12.Value = to_mail
    '        parms13.Value = "Vertical Allocation Details"
    '        parms14.Value = getoffsetdatetime(DateTime.Now)
    '        parms15.Value = "Request Submitted"
    '        parms16.Value = "Normal Mail"
    '        parms17.Value = strRM
    '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms10, parms11, parms12, parms13, parms14, parms15, parms16, parms17)


    '        'Dim parms18 As SqlParameter() = {New SqlParameter("@vc_Status", SqlDbType.VarChar, 10)}
    '        'parms18(0).Value = 3
    '        'objData = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_get_Role_Email_ids", parms18)
    '        'If objData.HasRows Then
    '        '    While objData.Read
    '        '        strBUHead = objData("aur_email")

    '        '        Dim parms19 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '        '        Dim parms20 As New SqlParameter("@VC_MSG", SqlDbType.VarChar)
    '        '        Dim parms21 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '        '        Dim parms22 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '        '        Dim parms23 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '        '        Dim parms24 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '        '        Dim parms25 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '        '        Dim parms26 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '        '        parms19.Value = "Abcd"
    '        '        parms20.Value = body
    '        '        parms21.Value = strBUHead
    '        '        parms22.Value = "Vertical Allocation Details"
    '        '        parms23.Value = getoffsetdatetime(DateTime.Now)
    '        '        parms24.Value = "Request Submitted"
    '        '        parms25.Value = "Normal Mail"
    '        '        parms26.Value = strFMG
    '        '        strVRM = strBUHead + "," + strBUHead
    '        '        '    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms19, parms20, parms21, parms22, parms23, parms24, parms25, parms26)


    '        '    End While
    '        'End If
    '        'Dim parms9 As SqlParameter() = {New SqlParameter("@vc_Status", SqlDbType.VarChar, 10)}
    '        'parms9(0).Value = 1
    '        'objData = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "usp_get_Role_Email_ids", parms9)
    '        'If objData.HasRows Then
    '        '    While objData.Read
    '        '        strFMG = objData("aur_email")

    '        '        Dim parms10 As New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
    '        '        Dim parms11 As New SqlParameter("@VC_MSG", SqlDbType.VarChar)
    '        '        Dim parms12 As New SqlParameter("@vc_mail", SqlDbType.NVarChar, 4000)
    '        '        Dim parms13 As New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 1000)
    '        '        Dim parms14 As New SqlParameter("@DT_MAILTIME", SqlDbType.DateTime)
    '        '        Dim parms15 As New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
    '        '        Dim parms16 As New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
    '        '        Dim parms17 As New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 4000)

    '        '        parms10.Value = "Abcd"
    '        '        parms11.Value = body
    '        '        parms12.Value = strBUHead
    '        '        parms13.Value = "Vertical Allocation Details"
    '        '        parms14.Value = getoffsetdatetime(DateTime.Now)
    '        '        parms15.Value = "Request Submitted"
    '        '        parms16.Value = "Normal Mail"
    '        '        parms17.Value = strFMG
    '        '        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "USP_SPACE_INSERT_AMTMAIL", parms10, parms11, parms12, parms13, parms14, parms15, parms16, parms17)



    '        '    End While
    '        'End If


    '    Catch ex As Exception
    '        ' Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Space Vertical Allocation", "Load", ex)
    '    End Try

    'End Sub

    'Protected Sub lstwrkStations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim intCount As Integer = 0
    '    For i As Integer = 0 To lstwrkStations.Items.Count - 1
    '        If lstwrkStations.Items(i).Selected Then
    '            intCount += 1
    '        End If
    '    Next
    '    lblSelectedWs.Text = "Selcted Workstation count is : <b>" & intCount & "<b>"
    'End Sub

    'Protected Sub lstHalfCabins_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim intCount As Integer = 0
    '    For i As Integer = 0 To lstHalfCabins.Items.Count - 1
    '        If lstHalfCabins.Items(i).Selected Then
    '            intCount += 1
    '        End If
    '    Next
    '    lblSelectedHC.Text = "Selcted Half Cabin count is : <b>" & intCount & "<b>"
    'End Sub

    'Protected Sub lstfullcabins_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim intCount As Integer = 0
    '    For i As Integer = 0 To lstfullcabins.Items.Count - 1
    '        If lstfullcabins.Items(i).Selected Then
    '            intCount += 1
    '        End If
    '    Next
    '    lblSelecetedFC.Text = "Selcted Full Cabin count is : <b>" & intCount & "<b>"
    'End Sub

    Protected Sub gvWs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim chkSelect As HtmlInputCheckBox = CType(e.Row.FindControl("chk"), HtmlInputCheckBox)
            chkSelect.Attributes.Add("OnClick", "javascript:colorRow(event)")
        End If
    End Sub

    Protected Sub gvHC_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim chkSelect As HtmlInputCheckBox = CType(e.Row.FindControl("chkHC"), HtmlInputCheckBox)

            chkSelect.Attributes.Add("OnClick", "javascript:colorRowHC(event)")

            ''If e.Row.RowState = DataControlRowState.Alternate Then
            'e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            'e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#B4CDCD';")
            ''Else
            ''    e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            ''    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#eefef0';")
            ''End If
        End If
    End Sub

    Protected Sub gvFC_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim chkSelect As HtmlInputCheckBox = CType(e.Row.FindControl("chkFC"), HtmlInputCheckBox)

            chkSelect.Attributes.Add("OnClick", "javascript:colorRowFC(event)")

            ''If e.Row.RowState = DataControlRowState.Alternate Then
            'e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            'e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#B4CDCD';")
            ''Else
            ''    e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFFE1';")
            ''    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#eefef0';")
            ''End If
        End If
    End Sub

    Protected Sub ddlVertical_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVertical.SelectedIndexChanged
        If (ddlVertical.SelectedIndex > 0) Then
            Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
            verDTO = verBll.GetSelectedVerticalReqIDs(ddlVertical.SelectedValue)
            ddlReqid.DataSource = verDTO
            ddlReqid.DataTextField = "RequestID"
            ddlReqid.DataValueField = "RequestID"
            ddlReqid.DataBind()
            ddlReqid.Items.Insert(0, "-- Select --")
        Else
            ddlReqid.SelectedIndex = 0
            ddlReqid.Items.Clear()
            ddlReqid.Items.Insert(0, "-- Select --")
            tblDetails.Visible = False
        End If
    End Sub
End Class
