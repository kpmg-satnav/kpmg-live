﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="Conferencetype.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Conferencetype" %>

<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>    
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Conference Type Master 
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label class="col-md-2 btn btn-default pull-right">
                                        <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true"
                                            ToolTip="Please Select Add to add new Conference Type and Select Modify to modify the existing Conference Type" />
                                        Add
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="btn btn-default" style="margin-left: 25px">
                                        <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsModify" GroupName="rbActions" AutoPostBack="true"
                                            ToolTip="Please Select Add to add new Conference Type and Select Modify to modify the existing Conference Type" />
                                        Modify
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div id="trLName" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="LblConftype" class="col-md-5 control-label" runat="server">Select Conference Type<span style="color: red;">*</span></asp:Label>

                                            <asp:RequiredFieldValidator ID="rfvCType" runat="server" ControlToValidate="ddlConftype" Display="None" ErrorMessage="Please Select Conference Type"
                                                InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                            <div class="col-md-7">
                                                <asp:DropDownList ID="ddlConftype" runat="server" CssClass="selectpicker" Tabindex="1" data-live-search="true" ToolTip="Select Conference Type" AutoPostBack="true">

                                                    <asp:ListItem Value="1">--Select--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" id="trPropertyType" runat="server">
                                        <label class="col-md-5 control-label">Conference Type Code <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvCCode" runat="server"
                                            ErrorMessage="Please Enter Conference Type Code " ControlToValidate="txtConfTypeCode" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtConfTypeCode"
                                            Display="None" ErrorMessage="Please Enter Conference Code In Alphanumerics" ValidationExpression="^[0-9a-zA-Z]+" ValidationGroup="Val1"></asp:RegularExpressionValidator><%--</font></td>--%>
                                              <div class="col-md-7">
                                                  <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                                      onmouseout="UnTip()">
                                                      <asp:TextBox ID="txtConfTypeCode" MaxLength="15" runat="server" Tabindex="2" CssClass="form-control"></asp:TextBox>

                                                  </div>
                                              </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Conference Type Name<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                            runat="server" ControlToValidate="txtConfTyepName" Display="None" ErrorMessage="Please Enter Conference Type Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtConfTyepName"
                                            Display="None" ErrorMessage="Please Enter Conference Name in alphanumerics and (space,-,_ ,(,),/,\,, allowed)"
                                            ValidationExpression="^[0-9a-zA-Z-_\/(), ]+" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/,, allowed) and upto 50 characters allowed')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtConfTyepName" MaxLength="50" runat="server" Tabindex="3" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Tabindex="4" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                    <asp:Button ID="btnback" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" Tabindex="5" PostBackUrl="~/WorkSpace/SMS_Webfiles/ConferenceMasterr.aspx" CausesValidation="False"></asp:Button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

</body>
</html>

