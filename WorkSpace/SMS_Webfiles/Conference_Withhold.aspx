﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="Conference_Withhold.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Conference_Withhold" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script defer type="text/javascript">
        function setup(id) {
            var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = (month < 10 ? '0' : '') + month + '/' +
                (day < 10 ? '0' : '') + day + '/' + d.getFullYear();
            $('#' + id).datepicker('setStartDate', output);
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            }).on('change', function () {
                $('.datepicker').hide();
            });
        };
        function refreshSelectpicker() {
            $("#<%=ddlCity.ClientID%>").selectpicker();
            $("#<%=ddlSelectLocation.ClientID%>").selectpicker();
            $("#<%=ddlTower.ClientID%>").selectpicker();
            $("#<%=ddlFloor.ClientID%>").selectpicker();
            $("#<%=ddlCapacity.ClientID%>").selectpicker();
            $("#<%=ddlConf.ClientID%>").selectpicker();
            $("#<%=starttimehr.ClientID%>").selectpicker();
            $("#<%=endtimehr.ClientID%>").selectpicker();
        }
        refreshSelectpicker();
    </script>
</head>
<body class="amantra">


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Reservation Type Master" ba-panel-class="with-scroll" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Reservation Room Withhold</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>City<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCity"
                                                Display="None" ErrorMessage="Please Select City" InitialValue="--Select--">
                                            </asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Location<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvSL" runat="server" ControlToValidate="ddlSelectLocation"
                                                Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--">
                                            </asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlSelectLocation" AutoPostBack="true" runat="server"
                                                CssClass="form-control selectpicker with-search" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Tower<span style="color: red;">*</span></label>
                                            <asp:CompareValidator ID="cmpflr" runat="server" ControlToValidate="ddlTower" Display="None"
                                                ErrorMessage="Please Select Tower " ValueToCompare="--Select--" Operator="NotEqual">cmpflr</asp:CompareValidator>
                                            <asp:DropDownList ID="ddlTower" TabIndex="5" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Floor<span style="color: red;">*</span></label>
                                            <asp:CompareValidator ID="cmptwr" runat="server" ControlToValidate="ddlFloor" Display="None"
                                                ErrorMessage="Please Select Floor " ValueToCompare="--Select--" Operator="NotEqual">cmptwr</asp:CompareValidator>
                                            <asp:DropDownList ID="ddlFloor" TabIndex="5" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Capacity<span style="color: red;">*</span></label>
                                            <asp:CompareValidator ID="cmpcap" runat="server" ControlToValidate="ddlCapacity" Display="None"
                                                ErrorMessage="Please Select Capacity " ValueToCompare="--Select--" Operator="NotEqual">cmpcap</asp:CompareValidator>
                                            <asp:DropDownList ID="ddlCapacity" TabIndex="5" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Reservation Room<span style="color: red;">*</span></label>
                                            <asp:CompareValidator ID="cmpconf" runat="server" ControlToValidate="ddlConf" Display="None"
                                                ErrorMessage="Please Select Reservation Room " ValueToCompare="--Select--" Operator="NotEqual">cmpconf</asp:CompareValidator>
                                            <asp:DropDownList ID="ddlConf" TabIndex="5" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>From Date<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvfrmdate" runat="server" ControlToValidate="txtFrmDate"
                                                Display="None" ErrorMessage="Please Select From Date ">
                                            </asp:RequiredFieldValidator>
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtFrmDate" OnTextChanged="txtFrmDate_TextChanged" AutoPostBack="true" runat="server" MaxLength="10" CssClass="form-control">
                                                </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span id="dtFrmDt" class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>To Date<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                                Display="None" ErrorMessage="Please Select To Date ">
                                            </asp:RequiredFieldValidator>
                                            <div class='input-group date' id='todate'>
                                                <asp:TextBox ID="txtToDate" OnTextChanged="txtToDate_TextChanged" AutoPostBack="true" runat="server" MaxLength="10" CssClass="form-control">
                                                </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>From Time (HH):<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="starttimehr" Display="None" ErrorMessage="Please Select From Time " InitialValue="HH"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="starttimehr" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                <asp:ListItem Text="HH" Value="HH"></asp:ListItem>
                                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>To Time (HH):<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="endtimehr" Display="None" ErrorMessage="Please Select To Time " InitialValue="HH"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="endtimehr" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                <asp:ListItem Text="HH" Value="HH"></asp:ListItem>
                                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                                <%--<asp:ListItem Text="24" Value="24"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
