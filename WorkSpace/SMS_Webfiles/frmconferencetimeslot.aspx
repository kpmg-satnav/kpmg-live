﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmconferencetimeslot.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmconferencetimeslot" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>

    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Reservation Booking" ba-panel-class="with-scroll" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Booking Request</h3>
                </div>
                <div class="panel-content">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>

                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" CssClass="alert alert-danger" />
                        <%--<asp:Label ID="lblmsg" ForeColor="RED" class="col-md-12 control-label" runat="server"></asp:Label>--%><br />
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblmsg" ForeColor="RED" Visible="true" class="col-md-12 control-label" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div id="Panel1" runat="server" visible="true">
                                    <div id="divOnbehalfSts" class="row" runat="server" style="padding-bottom: 20px">
                                        <div class="col-md-12 text-center">
                                            <label class="btn btn-default">
                                                <asp:RadioButton value="0" runat="server" name="rbActionsself" ID="rbActionsself" GroupName="rbActionsself" AutoPostBack="true" Checked="true" />
                                                Self
                                            </label>
                                            <label class="btn btn-default">
                                                <asp:RadioButton value="1" runat="server" name="rbActionsself" ID="rbActionsModify" GroupName="rbActionsself" AutoPostBack="true" />
                                                On behalf of
                                            </label>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtEmpId"
                                                    Display="none" ErrorMessage="Please Enter Employee Id" ValidationGroup="Val1">
                                                </asp:RequiredFieldValidator>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i id="id" class="fa fa-tag"></i></div>
                                                    <asp:TextBox ID="txtEmpId" runat="server" CssClass="form-control" placeholder="Enter Employee Id" onKeyPress="GetValueOnKeyPress()" AutoComplete="on" AutoPostBack="True"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i id="user" class="fa fa-user"></i></div>
                                                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control" AutoPostBack="True" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i id="dept" class="fa fa-bookmark"></i>
                                                    </div>
                                                    <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <img src="../../images/Chair_Blue.gif" />
                                                    </div>
                                                    <asp:TextBox ID="txtSpaceID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>City<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlCity" ValidationGroup="Val1"
                                                    Display="None" ErrorMessage="Please Select City" InitialValue="--Select--">
                                                </asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Location<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvSL" runat="server" ControlToValidate="ddlSelectLocation" ValidationGroup="Val1"
                                                    Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--">
                                                </asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlSelectLocation" AutoPostBack="true" runat="server"
                                                    CssClass="selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Tower<span style="color: red;">*</span></label>
                                                <asp:CompareValidator ID="cmpflr" runat="server" ControlToValidate="ddlTower" Display="None" ValidationGroup="Val1"
                                                    ErrorMessage="Please Select Tower " ValueToCompare="--Select--" Operator="NotEqual">cmpflr</asp:CompareValidator>
                                                <asp:DropDownList ID="ddlTower" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Floor<span style="color: red;">*</span></label>
                                                <asp:CompareValidator ID="cmptwr" runat="server" ControlToValidate="ddlFloor" Display="None" ValidationGroup="Val1"
                                                    ErrorMessage="Please Select Floor " ValueToCompare="--Select--" Operator="NotEqual">cmptwr</asp:CompareValidator>
                                                <asp:DropDownList ID="ddlFloor" TabIndex="5" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                    AutoPostBack="True" OnSelectedIndexChanged="ddlFloor_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Capacity<span style="color: red;">*</span></label>
                                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlCapacity" Display="None" ValidationGroup="Val1"
                                                    ErrorMessage="Please Select Capacity" ValueToCompare="--Select--" Operator="NotEqual">cmptwr</asp:CompareValidator>

                                                <asp:DropDownList ID="ddlCapacity" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true"
                                                    AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Reservation Room<span style="color: red;">*</span></label>
                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlConf" Display="None" ValidationGroup="Val1"
                                                    ErrorMessage="Please Select Reservation Room " ValueToCompare="--Select--" Operator="NotEqual">cmptwr</asp:CompareValidator>

                                                <asp:DropDownList ID="ddlConf" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix" style="padding-bottom: 20px">
                                        <div class="col-md-12 text-center">
                                            <label class="btn btn-default">
                                                <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true" />
                                                OneTime
                                            </label>
                                            <label class="btn btn-default">
                                                <asp:RadioButton value="1" runat="server" name="Recurring" ID="rbActionsrecurring" GroupName="rbActions" AutoPostBack="true" />
                                                Recurring
                                            </label>
                                        </div>
                                    </div>

                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>From Date<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select From Date" Display="None" ControlToValidate="txtFdate" __designer:wfdid="w49">
                                                </asp:RequiredFieldValidator>
                                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFdate" ValidationExpression="^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$"
                                                    ErrorMessage="Not Valid Date" ValidationGroup="Val1" Display="None"></asp:RegularExpressionValidator>--%>
                                                <div class='input-group date' id='Fromdate'>
                                                    <asp:TextBox ID="txtFdate" OnTextChanged="txtFrmDate_TextChanged" AutoPostBack="true" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('Fromdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div id="Tr1" runat="server" visible="false">
                                                    <label>To Date<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select To Date" Display="None" ControlToValidate="txtTdate" __designer:wfdid="w49">
                                                    </asp:RequiredFieldValidator>
                                                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtTdate" ValidationExpression="^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$"
                                                        ErrorMessage="Not Valid Date" ValidationGroup="Val1" Display="None"></asp:RegularExpressionValidator>--%>

                                                    <div class='input-group date' id='Todate'>
                                                        <asp:TextBox ID="txtTdate" OnTextChanged="txtToDate_TextChanged" AutoPostBack="true" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('Todate')"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="rectimes" runat="server" class="clearfix" visible="false">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>From Time (HH):<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlstarttime"
                                                        Display="None" ErrorMessage="Please Enter From Time" InitialValue="HH" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                                    <%--  <asp:DropDownList ID="ddlstarttime" runat="server" CssClass="selectpicker" data-live-search="true" />--%>
                                                    <asp:DropDownList ID="ddlstarttime" runat="server" CssClass="selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>

                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label>To Time (HH):<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlendtime"
                                                        Display="None" ErrorMessage="Please Enter To Time " InitialValue="HH" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                                    <asp:DropDownList ID="ddlendtime" runat="server" CssClass="selectpicker" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" Visible="False"
                                        BorderWidth="0px"></asp:TextBox>


                                    <div id="AttendeesMail" runat="server" class="clearfix" visible="false">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-md-5 control-label">
                                                        External Attendees <a title="Please Enter Mail IDs With Comma (,) Separation">Help</a></label>
                                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtAttendees" ValidationGroup="Val1"
                                                        Display="None" ErrorMessage="Please Enter Valid Email ID" ValidationExpression="^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[,]{0,1}\s*)+$">
                                                    </asp:RegularExpressionValidator>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="txtAttendees" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" ToolTip="Please Enter Mail IDs With Comma (,) Separtion"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <%-- <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-md-5 control-label">
                                                        Internal Attendees Mail Id <a title="Please Enter Mail IDs With Comma (,) Separation"> Help</a></label>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtInternal" ValidationGroup="Val1"
                                                        Display="None" ErrorMessage="Please Enter Valid Email ID" ValidationExpression="^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[,]{0,1}\s*)+$">
                                                    </asp:RegularExpressionValidator>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="txtInternal" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" ToolTip="Please Enter Mail IDs With Comma (,) Separtion"></asp:TextBox>
                                                        <asp:ListBox ID="lstInternal" runat="server" SelectionMode="Multiple" CssClass="form-control" Rows="5"></asp:ListBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>--%>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Select Internal Attendees  </label>
                                                <asp:TextBox ID="Textinternal" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="Textinternal_SelectedIndexChanged" data-live-search="true"></asp:TextBox>
                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="Textinternal" MinimumPrefixLength="2" EnableCaching="false"
                                                    CompletionSetCount="10" CompletionInterval="10" ServiceMethod="GetintAttendees" ServicePath="~/Autocompletetype.asmx">
                                                </asp:AutoCompleteExtender>
                                                <%--<asp:ListBox ID="lstInternal" runat="server" SelectionMode="Multiple" Height="50px" CssClass="form-control" Rows="5"></asp:ListBox>--%>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>
                                                    Internal Attendees
                                                            <asp:LinkButton ID="btnLogout" Text="Remove" OnClick="Remove_Click" runat="server" /></label>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtInternal" ValidationGroup="Val1"
                                                    Display="None" ErrorMessage="Please Enter Valid Email ID" ValidationExpression="^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[,]{0,1}\s*)+$">
                                                </asp:RegularExpressionValidator>
                                                <%--<asp:TextBox ID="txtInternal" runat="server" Height="50px" CssClass="form-control" TextMode="MultiLine" Rows="5" ToolTip="Please Enter Mail IDs With Comma (,) Separtion"></asp:TextBox>--%>
                                                <asp:ListBox ID="txtInternal" runat="server" SelectionMode="Multiple" Height="50px" CssClass="form-control" Rows="5"></asp:ListBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="InternalAttendees" runat="server" class="clearfix" visible="false">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-md-5 control-label">
                                                        Description<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription"
                                                        Display="None" ErrorMessage="Please Enter Description" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">

                                            <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Check Availability" CausesValidation="true" ValidationGroup="Val1" />
                                            <asp:Button ID="btnView" runat="server" CssClass="btn btn-primary custom-button-color" Text="View Search" />
                                            <asp:Button ID="btncheck" runat="server" CssClass="btn btn-primary custom-button-color" Text="Book" CausesValidation="true" ValidationGroup="Val1" />

                                        </div>
                                    </div>
                                </div>

                                <div id="gvPanel" runat="server">
                                    <div class="col-md-9 pull-right">
                                        <fieldset>
                                            <legend>
                                                <asp:Label runat="server" ID="lblHeader"></asp:Label>
                                            </legend>
                                        </fieldset>
                                    </div>

                                    <div class="form-group"></div>
                                    <div class="row">
                                        <div class="col-md-11 pull-right">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-md-5 control-label">Location Name<span style="color: red;">*</span></label>
                                                    <div class="col-md-7">
                                                        <asp:Label runat="server" ID="lblBuilding1">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-11 pull-right">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-md-5 control-label">
                                                        Reservation Room Name<span style="color: red;">*</span></label>
                                                    <div class="col-md-7">
                                                        <asp:Label runat="server" ID="lblConfName1">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-11 pull-right">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-md-5 control-label">Click on date for multiple slot booking <span style="color: red;">*</span></label>
                                                    <div class="col-md-7">
                                                        <asp:Label runat="server" Text="Click on green slot for 1Hr slot booking" ID="Label5">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-11 pull-right">
                                        <div class="row">
                                            <div class="form-group">
                                                <asp:Image ID="Image1" runat="server" ImageUrl="../../images/available.gif" />
                                                <asp:Image ID="Image3" runat="server" ImageUrl="../../images/Booked.gif" />
                                                <asp:Image ID="Image2" runat="server" ImageUrl="../../images/Withhold.gif" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvspacereport" runat="server" EmptyDataText="No Reservation Booking Found." GridLines="None" TabIndex="5"
                                            CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                            <RowStyle BackColor="#DEDFDE" ForeColor="Black" Wrap="False" />
                                            <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                            <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                            <SortedDescendingHeaderStyle BackColor="#33276A" />
                                        </asp:GridView>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="row">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblconfalert" ForeColor="RED" class="col-md-12 control-label" runat="server" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="true" PageSize="10" EmptyDataText="No Records Found."
                                            CssClass="table table-condensed table-bordered table-hover table-striped" Visible="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Booked From Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBookedfromdate" runat="server" CssClass="bodyText" Text='<%#Eval("BOOKED_FROM_DATE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Booked To Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBookedtodate" runat="server" CssClass="bodyText" Text='<%#Eval("BOOKED_TO_DATE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Time">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblfromtime" runat="server" CssClass="bodyText" Text='<%#((Eval("FTIME")))%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Time">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltotime" runat="server" CssClass="bodyText" Text='<%#((Eval("TTIME")))%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstatus" runat="server" CssClass="bodyText" Text='<%#Eval("ConfStatus")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <style>
        .ui-autocomplete-loading {
            background: white url("images/ui-anim_basic_16x16.gif") right center no-repeat;
        }
    </style>

    <%--    <link href="../../BootStrapCSS/jquery-ui.min.css" rel="stylesheet" />
    <script defer src="../../BootStrapCSS/Scripts/jquery-ui.min.js"></script>--%>

    <script defer type="text/javascript">
       
        $("#txtEmpId").autocomplete({
            minLength: 1,
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "frmconferencetimeslot.aspx/SearchCustomers",
                    data: "{'prefixText':'" + $('#txtEmpId').val() + "'}",
                    dataType: "json",
                    success: function (data) {
                        response(data.d);
                    },
                    error: function (result) {
                        alert(result.data);
                    }
                });
            },
            select: function (event, ui) {
                var selectedempid = ui.item.value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "frmconferencetimeslot.aspx/GetUserDataClient",
                    data: "{'aur_id':'" + selectedempid + "'}",
                    dataType: "json",
                    success: function (data) {
                        $("#txtName").val(data.d.Name);
                        $("#txtDepartment").val(data.d.DEPARTMENT);
                        $("#txtSpaceID").val(data.d.SPACEID);
                    },
                    error: function (result) {
                        alert(result.data);
                    }
                });
            }
        });


        //function setup(id) {
        //    $('#' + id).datepicker({
        //        format: 'mm/dd/yyyy',
        //        autoclose: true
        //    });
        //};
        function setup(id) {
            var sta =<%=Session("STA")%>;
            $("#datepicker").datepicker("destroy");

            if (id == "Todate") {
                // $("#Todate").datepicker("destroy");

                var fromdate = $('#txtFdate').val();
                //    var tt = e.target.value
                //   //-- var f = $('#txtTdate').val();
                //    console.log(tt);
                if (sta == 0) {
                    var fromdate = $('#txtFdate').val();
                    var date = new Date(fromdate);
                    var newdate = new Date(date);

                    newdate.setDate(newdate.getDate() + 14);
                    var dd = fixDigit(newdate.getDate());
                    var mm = fixDigit(newdate.getMonth() + 1);
                    var y = newdate.getFullYear();
                    var fdd = fixDigit(date.getDate());
                    var fmm = fixDigit(date.getMonth() + 1);
                    var fyy = date.getFullYear();
                    var fromdate1 = fmm + '/' + fdd + '/' + fyy;
                    var todate = mm + '/' + dd + '/' + y;
                    console.log(fromdate1);
                    console.log(todate);

                    //first day which can be selected in dt2 is selected date in dt1

                    $('#' + id).datepicker();
                    $('#' + id).datepicker('remove');

                    $('#' + id).datepicker({
                        format: "mm/dd/yyyy",
                        startDate: new Date(fromdate1),
                        endDate: new Date(todate)

                        //maxDate: '10/19/2019',

                    })
                }

                else {
                    $('#' + id).datepicker({
                        format: 'mm/dd/yyyy',
                        autoclose: true,

                    })
                }
            }
            else {
                $('#' + id).datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,

                })
            }


            function fixDigit(val) {
                return val.toString().length === 1 ? "0" + val : val;
            }
            $("#datepicker").datepicker("refresh");
        };
    </script>

    <style>
        #txtEmpId {
            overflow: scroll;
        }
    </style>
    <script defer>


        function GetValueOnKeyPress() {
            var edValue = document.getElementById("txtEmpId");
            var s = edValue.value;
            var lblValue = document.getElementById("lblValue");
            lblValue.innerText = "The text box contains: " + s;
        }

        $(function () {
            var maybe = true;
            var text = $('#txtSpaceID').val();
            if (maybe) {
                $('#txtSpaceID').attr('title', text);
            }
        });
        function refreshSelectpicker() {
            $("#<%=ddlCity.ClientID%>").selectpicker();
            $("#<%=ddlSelectLocation.ClientID%>").selectpicker();
            $("#<%=ddlTower.ClientID%>").selectpicker();
            $("#<%=ddlFloor.ClientID%>").selectpicker();
            $("#<%=ddlCapacity.ClientID%>").selectpicker();
            $("#<%=ddlConf.ClientID%>").selectpicker();

        }
        refreshSelectpicker();


    </script>

</body>
</html>



