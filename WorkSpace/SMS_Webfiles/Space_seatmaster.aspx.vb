Imports System.Data
Imports System.Data.SqlClient
Partial Class WorkSpace_SMS_Webfiles_Space_seatmaster
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim obj As clsMasters = New clsMasters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            cleardata()
            lblMsg.Text = ""
            ddlcity.Items.Clear()
            obj.BindCity(ddlcity)
            objsubsonic.Binddropdown(ddlseattype, "EFM_SEAT_MASTER", "seat_type", "seat_type_code")
            btnsubmit.Visible = True
        End If
    End Sub
   
    Protected Sub ddlBDG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBDG.SelectedIndexChanged
        If ddlBDG.SelectedIndex = 0 Then
            ddlTWR.SelectedIndex = 0
            ddlFLR.SelectedIndex = 0
            ddlWNG.SelectedIndex = 0
        Else
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@BDG_ADM_CODE", SqlDbType.NVarChar, 200)
            param(0).Value = ddlBDG.SelectedItem.Value
            objsubsonic.Binddropdown(ddlTWR, "EFM_SRQ_TOWER", "TWR_NAME", "TWR_CODE", param)
        End If

        
        
    End Sub

    Protected Sub ddlTWR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTWR.SelectedIndexChanged
        'ddlWNG.SelectedIndex = 0
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@BDG_ADM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlBDG.SelectedItem.Value
        param(1) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlTWR.SelectedItem.Value
        objsubsonic.Binddropdown(ddlFLR, "EFM_SRQ_FLOOR", "FLR_NAME", "FLR_CODE", param)
    End Sub

    Protected Sub ddlFLR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFLR.SelectedIndexChanged
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@BDG_ADM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlBDG.SelectedItem.Value
        param(1) = New SqlParameter("@TWR_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlTWR.SelectedItem.Value
        param(2) = New SqlParameter("@FLR_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = ddlFLR.SelectedItem.Value
        objsubsonic.Binddropdown(ddlWNG, "EFM_SRQ_WING", "WNG_NAME", "WNG_CODE", param)
    End Sub
    Private Sub cleardata()
        ddlBDG.Items.Clear()
        ddlTWR.Items.Clear()
        ddlFLR.Items.Clear()
        ddlWNG.Items.Clear()
        ddlseattype.Items.Clear()
        ddlspace.Items.Clear()
    End Sub

    Protected Sub ddlcity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcity.SelectedIndexChanged
        If ddlcity.SelectedIndex = 0 Then
            cleardata()
        Else

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@CITY", SqlDbType.NVarChar, 200)
            param(0).Value = ddlcity.SelectedItem.Value
            objsubsonic.Binddropdown(ddlBDG, "EFM_SRQ_BUILDING_CITY", "BDG_NAME", "BDG_ADM_CODE", param)
        End If
    End Sub

    Protected Sub ddlWNG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWNG.SelectedIndexChanged
        If ddlWNG.SelectedIndex = 0 Then
            cleardata()
        Else
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@SPC_BDG_ID", SqlDbType.NVarChar, 200)
            param(0).Value = ddlBDG.SelectedItem.Value
            param(1) = New SqlParameter("@SPC_TWR_ID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlTWR.SelectedItem.Value
            param(2) = New SqlParameter("@SPC_WNG_ID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlWNG.SelectedItem.Value
            param(3) = New SqlParameter("@SPC_FLR_ID", SqlDbType.NVarChar, 200)
            param(3).Value = ddlFLR.SelectedItem.Value
            
            objsubsonic.Binddropdown(ddlspace, "EFM_SPACE_GRID", "SPC_VIEW_NAME", "SPC_ID", param)


        End If
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@SPACE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlspace.SelectedItem.Value
        param(1) = New SqlParameter("@SEAT_TYPE", SqlDbType.NVarChar, 200)
        param(1).Value = ddlseattype.SelectedItem.Value
        Dim obj As Object = objsubsonic.GetSubSonicExecuteScalar("UPDATE_SEAT_SPACE", param)
        lblMsg.Text = "Record Updated Succesfully"
        cleardata()
        ddlcity.SelectedIndex = 0

    End Sub

    Protected Sub ddlspace_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlspace.SelectedIndexChanged
        If ddlspace.SelectedIndex = 0 Then
            ddlseattype.Items.Clear()
        Else

            Dim valid As Integer
            valid = ValidateSpace()
            If valid = 1 Then
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@SPACE", SqlDbType.NVarChar, 200)
                param(0).Value = ddlspace.SelectedItem.Value
                Dim ds As DataSet = objsubsonic.GetSubSonicDataSet("GET_SEAT_MASTER", param)
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlseattype.ClearSelection()
                    ddlseattype.Items.FindByValue(ds.Tables(0).Rows(0).Item("SEAT_TYPE_CODE")).Selected = True
                Else
                    ddlseattype.Items.Clear()
                    objsubsonic.Binddropdown(ddlseattype, "EFM_SEAT_MASTER", "seat_type", "seat_type_code")
                End If
            Else
                ddlseattype.Items.Clear()
                objsubsonic.Binddropdown(ddlseattype, "EFM_SEAT_MASTER", "seat_type", "seat_type_code")
            End If
        End If
    End Sub
    Private Function ValidateSpace()
        Dim valid As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_SPACE_SEAT")
        sp.Command.AddParameter("@SPACE", ddlspace.SelectedItem.Value, DbType.String)
        valid = sp.ExecuteScalar()
        Return valid
    End Function

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("~/Masters/Mas_Webfiles/frmMASMasters.aspx")
    End Sub
End Class
