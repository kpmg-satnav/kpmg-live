<%@ Page Language="VB" AutoEventWireup="false" CodeFile="finalpage.aspx.vb" Inherits="AssetManagement_SW_WebFiles_finalpage" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script defer src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script defer src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->

    <script defer type="text/javascript">
        function noway(go) {
            if (document.all) {
                if (event.button == 2) {
                    alert('For Security reasons Right click option was disabled');
                    return false;
                }
            }
            if (document.layers) {
                if (go.which == 3) {
                    alert('For Security reasons Right click option was disabled');
                    return false;
                }
            }
        }
        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);
        }
        document.onmousedown = noway;
    </script>
</head>
<body>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend></legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:Label ID="lbl1" runat="server"></asp:Label>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
