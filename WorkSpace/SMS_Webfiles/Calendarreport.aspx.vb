﻿
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Partial Class WorkSpace_SMS_Webfiles_Calendarreport
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim obj As New clsReports

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If

        txtfromdate.Attributes.Add("onClick", "displayDatePicker('" + txtfromdate.ClientID + "')")
        txtfromdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        txttodate.Attributes.Add("onClick", "displayDatePicker('" + txttodate.ClientID + "')")
        txttodate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        RequiredFieldValidator3.ErrorMessage = "Please select " + Session("Parent")
        If Not IsPostBack Then
            obj.bindVertical(ddlvertical)
            Calendar1.Visible = False

        End If
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        If txtfromdate.Text = "" Then
            lblMsg.Text = "Please select From Date"
            lblMsg.Visible = True
            Calendar1.Visible = False
        ElseIf txttodate.Text = "" Then
            lblMsg.Text = "Please select To Date"
            lblMsg.Visible = True
            Calendar1.Visible = False
        ElseIf CDate(txttodate.Text) < CDate(txtfromdate.Text) Then
            lblMsg.Text = "To Date Should be greater than From Date "
            lblMsg.Visible = True
            Calendar1.Visible = False
            Exit Sub
        ElseIf ddlvertical.SelectedIndex = 0 Then
            lblMsg.Text = "Please Select Vertical"
            lblMsg.Visible = True
            Calendar1.Visible = False
            Exit Sub
        End If
        If ddlvertical.SelectedIndex > 0 And txtfromdate.Text <> "" And txttodate.Text <> "" Then

            Calendar1.Visible = True
            lblMsg.Visible = False
        Else
            lblMsg.Text = "Please Enter Mandatory Fields"
        End If
    End Sub
    Private Function GetTable() As DataTable

       





        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"CALENDAR_REPORT")
        If txtfromdate.Text = "" Then
            sp.Command.AddParameter("@FROMDATE", DBNull.Value, DbType.DateTime)
        Else
            sp.Command.AddParameter("@FROMDATE", txtfromdate.Text, DbType.DateTime)

        End If

        If txttodate.Text = "" Then
            sp.Command.AddParameter("@TODATE", DBNull.Value, DbType.DateTime)
        Else
            sp.Command.AddParameter("@TODATE", txttodate.Text, DbType.DateTime)
        End If

        If ddlvertical.SelectedIndex > 0 Then
            sp.Command.AddParameter("@VERTICAL", ddlvertical.SelectedItem.Value, DbType.String)
        Else
            sp.Command.AddParameter("@VERTICAL", 0, DbType.String)
        End If

        Dim ds As DataSet = sp.GetDataSet()

        Dim dt As DataTable = ds.Tables(0)
        Return dt

    End Function

    Protected Sub Calendar1_DayRender(sender As Object, e As DayRenderEventArgs) Handles Calendar1.DayRender

        Dim dt As DataTable = GetTable()

        For i As Integer = 0 To dt.Rows.Count - 1


            If e.Day.Date.ToShortDateString() = Convert.ToDateTime(dt.Rows(i)(0).ToString()).ToShortDateString() Then

                If dt.Rows(i)(2).ToString() = "6" Then
                    e.Cell.BackColor = System.Drawing.Color.Yellow
                    e.Cell.Text = dt.Rows(i)(3).ToString() & " , " & dt.Rows(i)(4).ToString()
                    e.Cell.ToolTip = "Allocated - " & e.Day.Date()
                Else
                    e.Cell.BackColor = System.Drawing.Color.Brown
                    e.Cell.Text = dt.Rows(i)(3).ToString() & " , " & dt.Rows(i)(4).ToString()
                    e.Cell.ToolTip = "Occupied - " & e.Day.Date()
                End If


            End If

        Next

    End Sub
End Class
