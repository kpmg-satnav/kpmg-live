﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmEnergyMasters.aspx.cs" Inherits="WorkSpace_SMS_Webfiles_frmEnergyMasters" Title="Energy Management Masters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-5b02d1ea3b.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>

<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Asset Masters" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Energy Management Masters</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form2" runat="server">
                        <div class="box-body">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <asp:HyperLink ID="hpl" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/EM/Masters/Views/Threshold.aspx">Energy Budget Master</asp:HyperLink>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
