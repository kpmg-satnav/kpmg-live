﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ConferenceRoomMapping.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_ConferenceRoomMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript"></script>

    <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td align="center" width="100%">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                    ForeColor="Black">  
             <hr align="center" width="60%" />Conference Room Mapping</asp:Label></td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="90%" align="center" border="0">
        <tr>
            <td align="left" width="100%" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">&nbsp; <strong>Conference Room Mapping</strong>
            </td>
            <td style="width: 16px">
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">&nbsp;</td>
            <td align="left">
              <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
				
				
				
				<asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="clsMessage"
                    ForeColor="" />
                <br />
                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                <br />
                <br />
                <table id="table4" cellspacing="1" cellpadding="1" style="width: 100%; border-collapse: collapse;"
                    border="1">
                     <tr>
                        <td align="left" class="clslabel" style="height: 26px; width: 50%" colspan="2">Select City<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCity"
                                Display="None" ErrorMessage="Please Select City" InitialValue="--Select--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td style="height: 26px; width: 50%" colspan="2">
                            <asp:DropDownList ID="ddlCity" runat="server" Width="98%" CssClass="clsComboBox"
                                AutoPostBack="True" >
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%" colspan="2">Select Location<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvSL" runat="server" ControlToValidate="ddlSelectLocation"
                                Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td style="height: 26px; width: 50%" colspan="2">
                            <asp:DropDownList ID="ddlSelectLocation" AutoPostBack="true" runat="server" Width="98%"
                                CssClass="clsComboBox" TabIndex="1">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 26px; width: 50%" colspan="2">Select Tower<font class="clsNote">*</font>

                            <asp:RequiredFieldValidator ID="rfv2" runat="server" ControlToValidate="ddlTower"
                                Display="None" ErrorMessage="Please Select Tower" InitialValue="--Select--">
                            </asp:RequiredFieldValidator>

                        </td>
                        <td align="left" style="height: 26px; width: 50%" colspan="2">
                            <asp:DropDownList ID="ddlTower" TabIndex="2" runat="server" CssClass="clsComboBox"
                                Width="98%" AutoPostBack="true">
                            </asp:DropDownList></td>
                    </tr>


                    <tr>
                        <td align="left" style="height: 26px; width: 50%" colspan="2">Floor<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RFV4" runat="server" ControlToValidate="ddlFloor"
                                Display="None" ErrorMessage="Please Select Floor" InitialValue="--Select--">
                            </asp:RequiredFieldValidator>

                        </td>
                        <td style="height: 26px; width: 50%" colspan="2">
                            <asp:DropDownList ID="ddlFloor" TabIndex="3" runat="server" CssClass="clsComboBox"
                                Width="98%" AutoPostBack="true" >
                            </asp:DropDownList></td>
                    </tr>

                     <tr>
                        <td align="left" style="height: 26px; width: 50%" colspan="2">Mapping Date <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfvfrmdate" runat="server" ControlToValidate="txtFrmDate"
                                Display="None" ErrorMessage="Please Enter Mapping Date ">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td style="height: 26px; width: 50%" colspan="2">
                            <asp:TextBox ID="txtFrmDate" runat="server" Width="95%" MaxLength="10" CssClass="clsTextField">
                            </asp:TextBox>
                        </td>
                    </tr>

                     <tr runat="server" visible="false" >
                        <td align="left" style="height: 26px; width: 50%" colspan="2">To Date <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txttodate"
                                Display="None" ErrorMessage="Please Enter To Date ">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td style="height: 26px; width: 50%" colspan="2">
                            <asp:TextBox ID="txttodate" runat="server" Width="95%" MaxLength="10" CssClass="clsTextField">
                            </asp:TextBox>
                        </td>
                    </tr>

                     <tr runat="server" visible="false" >
                        <td align="left" style="height: 26px; width: 25%">From:<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="starttimehr"
                                Display="None" ErrorMessage="Please Enter From Time " InitialValue="Hr">
                            </asp:RequiredFieldValidator>
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="starttimemin"
                                Display="None" ErrorMessage="Please Enter From Minutes" InitialValue="Min">
                            </asp:RequiredFieldValidator>
                        </td>
                        <td align="left" style="height: 26px; width: 25%">
                            <asp:DropDownList ID="starttimehr" runat="server" Width="60">
                                <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                <asp:ListItem Text="23" Value="23"></asp:ListItem>
                            </asp:DropDownList>&nbsp;<asp:DropDownList ID="starttimemin" runat="server" Width="60">
                                <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                <asp:ListItem Text="30" Value="30"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" style="height: 26px; width: 25%">To:<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="endtimehr"
                                Display="None" ErrorMessage="Please Enter To Time " InitialValue="Hr">
                            </asp:RequiredFieldValidator>
                            <font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="endtimemin"
                                Display="None" ErrorMessage="Please Enter To Minutes" InitialValue="Min">
                            </asp:RequiredFieldValidator></td>
                        <td align="left" style="height: 26px; width: 25%">
                            <asp:DropDownList ID="endtimehr" runat="server" Width="60">
                                <asp:ListItem Text="Hr" Value="Hr"></asp:ListItem>
                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                <asp:ListItem Text="23" Value="23"></asp:ListItem>
                            </asp:DropDownList>&nbsp;<asp:DropDownList ID="endtimemin" runat="server" Width="60">
                                <asp:ListItem Text="Min" Value="Min"></asp:ListItem>
                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                <asp:ListItem Text="30" Value="30"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                      

                    <tr runat="server" id="confroom">
                        <td align="left" style="height: 26px; width: 50%" colspan="2">Select Conference<font class="clsNote">*</font>
                            <asp:RequiredFieldValidator ID="rfv3" runat="server" ControlToValidate="ddlConference"
                                Display="None" ErrorMessage="Please Select Conference" InitialValue="--Select--">
                            </asp:RequiredFieldValidator>

                        </td>
                        <td style="height: 26px; width: 50%" colspan="2">
                            <asp:DropDownList ID="ddlConference" TabIndex="4" runat="server" CssClass="clsComboBox"
                                Width="98%" AutoPostBack="true" >
                            </asp:DropDownList></td>
                    </tr>

                    <tr runat="server" id="assets">
                        <td align="left" style="height: 26px; width: 50%" colspan="2">Select Assets<font class="clsNote">*</font>
                        </td>

                        <td style="height: 26px; width: 50%" colspan="2">
                            <asp:ListBox ID="lstassets" runat="server" SelectionMode="Multiple" Width="95%" Height="120px"></asp:ListBox>
                        </td>

                    </tr>


                      
                   

                   <tr>

                       <td align="center" colspan="4">
                           <asp:button id="btnsubmit" runat="server" Text="Submit" causesvalidation="true" CssClass="button"></asp:button>
                       </td>
                   </tr>





                </table>

				 </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 16px; height: 100%;">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 16px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>

</asp:Content>

