Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_Vertical
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindVerticals()
            gvitems.Visible = False
        End If
    End Sub
    Private Sub BindVerticals()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_COSTCENTER_VERTICAL")

        ddlvertical.DataSource = sp.GetDataSet()
        ddlvertical.DataTextField = "VER_NAME"
        ddlvertical.DataValueField = "VER_CODE"
        ddlvertical.DataBind()
        ddlvertical.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        gvitems.Visible = True
        BindGrid()
      
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_VACANT_SEATS_BYVERTICALS")
        sp.Command.AddParameter("@AUR_VERT_CODE", ddlvertical.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@NEEDSEATS", txtcount.Text, DbType.Int32)
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

End Class
