﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="ConferenceMasterr.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_ConferenceMasterr" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/bootstrap.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/bootstrap-select.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/amantra.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/font-awesome/css/font-awesome.min.css")%>' rel="stylesheet" />
    <link rel="stylesheet" href='<%= Page.ResolveUrl("~/BootStrapCSS/datepicker.css")%>' />--%>


    <script defer type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
    <style>
        .btn {
            border-radius: 4px;
            background-color: #209e91;
        }
    </style>
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Primary Masters" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Reservation Masters</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <div class="row">
                            <div class="box-body">
                                <div class="clearfix">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <asp:HyperLink ID="hplreqtype" role="button" class="btn btn-block btn-primary" runat="server" NavigateUrl="~/ConferenceViews/Views/ConferenceTypeMaster.aspx">Reservation Type Master</asp:HyperLink>
                                    </div>
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <asp:HyperLink ID="hypreqedit" role="button" class="btn btn-block btn-primary" runat="server" NavigateUrl="~/ConferenceViews/Views/ConferenceRoomMaster.aspx">Reservation Room Master</asp:HyperLink>
                                    </div>

                                    <div class="clearfix">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <asp:HyperLink ID="HyperLink1" role="button" class="btn btn-block btn-primary" runat="server" NavigateUrl="~/ConferenceViews/Views/FacilityMaster.aspx">Reservation Facility Master</asp:HyperLink>
                                        </div>
                                        <br />
                                        <br />
                                        <br />
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <asp:HyperLink ID="HyperLink2" role="button" class="btn btn-block btn-primary" runat="server" NavigateUrl="~/ConferenceViews/Views/ConferenceAssetMaster.aspx">Reservation Asset Master</asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script defer src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/jquery.min.js")%>' type="text/javascript"></script>
    <script defer src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap.min.js")%>' type="text/javascript"></script>
    <script defer src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-select.min.js")%>' type="text/javascript"></script>
    <script defer src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-datepicker.js") %>' type="text/javascript"></script>
    <script defer src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/ObjectKeys.js")%>'></script>
    <script defer src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/wz_tooltip.js")%>' type="text/javascript"></script>
    <script defer type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js" temp_src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <!--[if lt IE 9]>
        <script defer src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" temp_src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script defer src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" temp_src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</body>
</html>


