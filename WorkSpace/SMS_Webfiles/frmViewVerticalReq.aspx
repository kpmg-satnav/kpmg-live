<%@ Page Language="VB" MasterPageFile="~/MasterPage.master"  AutoEventWireup="false" CodeFile="frmViewVerticalReq.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmViewVerticalReq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script defer language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"></script>
    <script defer language="javascript" type="text/javascript"> 
    function noway(go)
    {
        if(document.all)
        {
            if (event.button == 2)
            {
                alert('For Security reasons Right click option was disabled,Inconvenience regreted!');
                return false;
            }
        }
        if (document.layers) 
        {
            if (go.which == 3)
            {
                alert('For Security reasons Right click option was disabled,,Inconvenience regreted!');
                return false;
            }
        }
    } 
     if (document.layers) 
     {
        document.captureEvents(Event.MOUSEDOWN); 
     }
     document.onmousedown=noway;
    </script>


    <script defer src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

 
        <div>
        
        <table id="table1" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center">   <asp:Label ID="lblHead" runat="server" CssClass="clsHead"  Width="95%" Font-Underline="False" ForeColor="Black">Space Connect
             <hr align="center" width="60%" /></asp:Label></td>
                        </tr>
                        </table>
                        
        
      
           
            
          
                
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%">
                <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                        <td  colspan="3" align="left">
           <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
       
          </td>
                        </tr>
                       
                    <tr>
                        <td>
                            <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                        <td width="100%" class="tableHEADER" align="left">
                            &nbsp;<strong> Vertical Wise View/Change Requisition</strong></td>
                        <td>
                            <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                    </tr>
                     <tr>
                        <td  colspan="3" align="left">
              <asp:ValidationSummary ID="VerticalValidations" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
          </td>
                        </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">
                        </td>
                        <td align="center">
                            <table id="table2" cellspacing="1" cellpadding="1" style="width: 100%;" border="1">
                                <tr>
                                    <td align="left" colspan="2"  class="2">
                                    Vertical Requisition
                                        </td>
                                        <td align="left" colspan="2"  class="2">
                                        <asp:DropDownList ID="ddlReqID" runat="server" CssClass="clsComboBox" Width="100%"></asp:DropDownList>
                                        </td>
                                        
                                </tr>
                                <tr id="trLName" runat="server">
                                    <td align="left" class="clslabel" style="width: 25%; height: 26px;">
                                        Preferred Location<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvSL" runat="server" ControlToValidate="ddlSelectLocation"
                                            Display="None" ErrorMessage="Please Select location" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="width: 25%; height: 26px;">
                                        <asp:DropDownList ID="ddlSelectLocation" runat="server" Width="98%" CssClass="clsComboBox">
                                        </asp:DropDownList></td>
                                    <td align="left" style="width: 25%; height: 26px;">
                                        Vertical<font class="clsNote">*</font><asp:RequiredFieldValidator ID="rfvVTl" runat="server"
                                            ControlToValidate="ddlVertical" Display="None" ErrorMessage="Please Select Vertical"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="height: 26px">
                                        <asp:DropDownList ID="ddlVertical" runat="server" Width="98%" CssClass="clsComboBox">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr id="Tr1" runat="server" visible="false">
                                    <td align="left" class="clslabel" style="width: 25%; height: 17px">
                                        From Date<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvfrmdate" runat="server" ControlToValidate="txtFrmDate"
                                            Display="None" ErrorMessage="Please enter from date !"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvfrmdate" runat="server" ControlToValidate="txtFrmDate"
                                            Display="None" ErrorMessage="Please enter valid from date !" Operator="DataTypeCheck"
                                            Type="Date"></asp:CompareValidator></td>
                                    <td style="width: 25%; height: 17px">
                                        <asp:TextBox ID="txtFrmDate" runat="server" Width="95%" MaxLength="10" CssClass="clsTextBox"></asp:TextBox>
                                      
                                    </td>
                                    <td align="left" style="width: 25%; height: 17px">
                                        To Date<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvtodate" runat="server" ControlToValidate="txtToDate"
                                            Display="None" ErrorMessage="Please enter to date !"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cvtodate" runat="server" ControlToValidate="txtToDate"
                                            Display="None" ErrorMessage="Please enter valid to date !" Operator="DataTypeCheck"
                                            Type="Date"></asp:CompareValidator></td>
                                    <td style="height: 17px">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="95%" MaxLength="10" CssClass="clsTextBox"></asp:TextBox>
                                      
                                    </td>
                                </tr>
                            </table>
                            <table class="table" width="100%" border="1">
                                <tr>
                                    <td colspan="4">
                                        <asp:GridView ID="gvEnter" runat="server" AutoGenerateColumns="False" Width="100%"
                                            PageSize="1">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Month">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="clsComboBox" Width="100%">
                                                            <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                                            <asp:ListItem Value="1" Text="January"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="February"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="March"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="April"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="May"></asp:ListItem>
                                                            <asp:ListItem Value="6" Text="June"></asp:ListItem>
                                                            <asp:ListItem Value="7" Text="July"></asp:ListItem>
                                                            <asp:ListItem Value="8" Text="August"></asp:ListItem>
                                                            <asp:ListItem Value="9" Text="September"></asp:ListItem>
                                                            <asp:ListItem Value="10" Text="October"></asp:ListItem>
                                                            <asp:ListItem Value="11" Text="November"></asp:ListItem>
                                                            <asp:ListItem Value="12" Text="December"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvMonth" runat="server" ControlToValidate="ddlMonth"
                                                            Display="None" ErrorMessage="Please select Month" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Year">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="clsComboBox" Width="100%">
                                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvYear" runat="server" ControlToValidate="ddlYear"
                                                            Display="None" ErrorMessage="Please select Year !" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Work Stations Required">
                                                    <ItemTemplate>
                                                        <div onmouseover="Tip('Enter number of  workstations required')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtWorkstations" CssClass="clsTextBox" runat="server" MaxLength="5"></asp:TextBox></div>
                                                        <asp:CompareValidator ID="cvwrkstations" runat="server" ControlToValidate="txtWorkstations"
                                                            Display="None" ErrorMessage="Please enter numerics in Work stations !" Operator="DataTypeCheck"
                                                            Type="Integer"></asp:CompareValidator>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Half Cabins Required">
                                                    <ItemTemplate>
                                                        <div onmouseover="Tip('Enter number of  Halfcabins required')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtHalfcabinsrequired" CssClass="clsTextBox" runat="server" MaxLength="5"></asp:TextBox></div>
                                                        <asp:CompareValidator ID="cvhc" runat="server" ControlToValidate="txtHalfcabinsrequired"
                                                            Display="None" ErrorMessage="Please enter numerics in half cabins !" Operator="DataTypeCheck"
                                                            Type="Integer"></asp:CompareValidator>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Full Cabins Required">
                                                    <ItemTemplate>
                                                        <div onmouseover="Tip('Enter number of  full cabins required')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtFullCabinsRequired" CssClass="clsTextBox" runat="server" MaxLength="5"></asp:TextBox></div>
                                                        <asp:CompareValidator ID="cvfc" runat="server" ControlToValidate="txtFullCabinsRequired"
                                                            Display="None" ErrorMessage="Please enter numerics in full cabins !" Operator="DataTypeCheck"
                                                            Type="Integer"></asp:CompareValidator>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="right" style="height: 19px">
                                    <div onmouseover="Tip('Click for more Requistions')" onmouseout="UnTip()" >
                                    <asp:LinkButton ID="lnkmore" runat="server" Text="Add More" CausesValidation="False"></asp:LinkButton></div></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="table10" style="width: 100%">
                                            <tr>
                                                <td style="width: 25%; height: 26px;" align="left">
                                                    Reporting Manager<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvRM" runat="server" ControlToValidate="ddlRM" Display="None"
                                                        ErrorMessage="Please Select Reporting Manager !" InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                                <td style="width: 25%; height: 26px;">
                                                    <asp:DropDownList ID="ddlRM" CssClass="clsComboBox" runat="server" Width="100%" Height="39px">
                                                    </asp:DropDownList></td>
                                                <td style="height: 26px; width: 25%;" align="left">
                                                    Remarks<font class="clsNote">*</font>
                                                    <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRemarks"
                                                        Display="None" ErrorMessage="Please  enter Remarks !"></asp:RequiredFieldValidator></td>
                                                <td style="height: 26px; width: 25%">
                                                    <div onmouseover="Tip('Enter remarks up to 500 characters')" onmouseout="UnTip()">
                                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="clsTextBox" TextMode="MultiLine"
                                                            MaxLength="500" Width="100%"></asp:TextBox></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 17px;" colspan="2">
                                        <asp:Button ID="btnUpdate" runat="server" Width="76px" CssClass="button" Text="Update" />
                                        <asp:Button ID="btnCancel" runat="server" Width="76px" CssClass="button" Text="Cancel" /></td>
                                </tr>
                            </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px;">
                            &nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </asp:Panel>
            
        </div>
   </asp:Content>