﻿
Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL
Imports Google.Apis.Calendar.v3.Data

Partial Class WorkSpace_SMS_Webfiles_modifyconference
    Inherits System.Web.UI.Page

    Dim Addusers As New AddUsers
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty
    Dim aur_id As String = String.Empty
    Dim verticalreqid As String = String.Empty
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim CONF_EVENT_ID As String


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        lblMsg.Text = ""
        lblconfalert.Text = ""
        txtFdate.Attributes.Add("onClick", "displayDatePicker('" + txtFdate.ClientID + "')")
        txtFdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        txtFdate.Attributes.Add("readonly", "readonly")
        If Not Page.IsPostBack Then
            LoadCity()
            LoadLocation()
            LoadTower()
            LoadFloor()
            txtDate.Text = getoffsetdatetime(DateTime.Now).Date
            Dim id As String = Request.QueryString("id")
            REQID = id

            Binddetails(id)
            BindConfTimes()
            'BindMail()
            'confroom.Visible = False
            'btnsubmit.Visible = False
            'emp.Visible = False
            'gvassets.Visible = False
            'gvDaily.Visible = False



        End If
    End Sub

    Protected Sub txtTdate_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        BindConfTimes()
    End Sub

    Public Sub BindConfTimes()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONF_TIMES")
        sp.Command.AddParameter("@DATE", txtFdate.Text, DbType.Date)
        Dim ds As DataSet
        ds = sp.GetDataSet
        For Each item As ListItem In starttimehr.Items
            item.Attributes.Add("disabled", "disabled")
        Next
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            starttimehr.Items.FindByValue(ds.Tables(0).Rows(i).Item("Timings")).Attributes.Remove("disabled")
        Next
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONF_TIMES")
        sp1.Command.AddParameter("@DATE", txtTdate.Text, DbType.Date)
        Dim ds1 As DataSet
        ds1 = sp1.GetDataSet
        For Each item As ListItem In endtimehr.Items
            item.Attributes.Add("disabled", "disabled")
        Next
        For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
            endtimehr.Items.FindByValue(ds1.Tables(0).Rows(i).Item("Timings")).Attributes.Remove("disabled")
        Next
    End Sub

    'Private Sub BindMail()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CM_GET_ALL_MAIL_IDS")
    '    sp.Command.AddParameter("@CITY", ddlCity.SelectedValue, DbType.String)
    '    sp.Command.AddParameter("@LOCATION", ddlSelectLocation.SelectedValue, DbType.String)
    '    sp.Command.AddParameter("@TOWER", ddlTower.SelectedValue, DbType.String)
    '    sp.Command.AddParameter("@FLOOR", ddlFloor.SelectedValue, DbType.String)
    '    sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)
    '    lstInternal.DataSource = sp.GetDataSet()
    '    lstInternal.DataTextField = "AUR_ID"
    '    lstInternal.DataValueField = "AUR_EMAIL"
    '    lstInternal.DataBind()
    'End Sub

    Private Sub Binddetails(ByVal id As String)
        'Dim Status As String = ""
        Dim sp2 As New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") & "." & "GET_ROLE_OF_THE_USER")
        sp2.Command.AddParameter("@USR_ID", Session("uid"), DbType.String)
        Dim ds2 As New DataSet()
        ds2 = sp2.GetDataSet()
        Session("STA") = ds2.Tables(0).Rows(0).Item("ROL_ID")


        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VIEW_CONFERENCE")
        sp.Command.AddParameter("@REQ_ID", id, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("status") = 4 Then
                btnCancel.Visible = False
                btnsubmit.Visible = False
            End If
            ddlCity.ClearSelection()
            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONF_CITY")).Selected = True
            'BindLocation(ddlCity.SelectedItem.Value)
            ddlSelectLocation.ClearSelection()
            ddlSelectLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONF_LOC")).Selected = True
            'BindTower(ddlSelectLocation.SelectedItem.Value)
            ddlTower.ClearSelection()
            ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONF_TOWER")).Selected = True
            ' BindFloor(ddlTower.SelectedItem.Value, ddlSelectLocation.SelectedItem.Value.ToString().Trim())
            ddlFloor.ClearSelection()
            ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONF_FLOOR")).Selected = True
            BindCapacity()
            ddlCapacity.ClearSelection()
            ddlCapacity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CAPACITY")).Selected = True
            txtFdate.Text = ds.Tables(0).Rows(0).Item("FROM_DATE")
            txtTdate.Text = ds.Tables(0).Rows(0).Item("TO_DATE")
            'If txtFdate.Text >= getoffsetdate(Date.Today) And ds.Tables(0).Rows(0).Item("status") <> 4 Then
            If txtTdate.Text > txtFdate.Text And ds.Tables(0).Rows(0).Item("status") <> 4 Then
                rbActionsrecurring.Checked = True
                rbActions.Enabled = False
                Tr1.Visible = True
                btnsubmit.Visible = True
                btnCancel.Visible = True
                txtFdate.Enabled = False
                frmdate.Visible = False
            ElseIf txtFdate.Text >= getoffsetdate(Date.Today) And ds.Tables(0).Rows(0).Item("status") <> 4 Then
                rbActions.Checked = True
                rbActionsrecurring.Enabled = False
                Tr1.Visible = True
                btnsubmit.Visible = True
                btnCancel.Visible = True
                txtFdate.Enabled = False
                frmdate.Visible = False
                txtTdate.Enabled = False
                frmtodate.Visible = False
            Else
                btnsubmit.Visible = False
                btnCancel.Visible = False
                starttimehr.Enabled = False
                endtimehr.Enabled = False
                txtFdate.Enabled = False
                frmdate.Visible = False
                txtTdate.Enabled = False
                frmtodate.Visible = False
                rbActionsrecurring.Enabled = False
            End If
            'If txtTdate.Text > txtFdate.Text Then
            '    btnsubmit.Visible = False
            '    btnCancel.Visible = False
            'End If
            'starttimehr.ClearSelection()
            starttimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_HOUR")).Selected = True
            'starttimemin.ClearSelection()
            'starttimemin.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_MINUTE")).Selected = True
            'endtimehr.ClearSelection()
            endtimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_HOUR")).Selected = True
            'If ((ds.Tables(0).Rows(0).Item("TO_HOUR")) <= (ds.Tables(0).Rows(0).Item("FROM_HOUR")) And (ds.Tables(0).Rows(0).Item("TO_DATE")) > (ds.Tables(0).Rows(0).Item("FROM_DATE"))) Then
            '    btnsubmit.Visible = False
            '    btnCancel.Visible = False
            '    starttimehr.Enabled = False
            '    endtimehr.Enabled = False
            '    txtFdate.Enabled = False
            '    txtTdate.Enabled = False
            '    rbActions.Enabled = False
            'End If


            'endtimemin.ClearSelection()
            'endtimemin.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_MINUTE")).Selected = True
            'BindConference(ddlSelectLocation.SelectedItem.Value.ToString().Trim(), ddlTower.SelectedItem.Value, ddlFloor.SelectedItem.Value)
            BindConference_ddl()
            ddlConf.ClearSelection()
            ddlConf.Items.FindByValue(ds.Tables(0).Rows(0).Item("SSA_SPC_ID")).Selected = True
            ' txtEmployeeCode.Text = ds.Tables(0).Rows(0).Item("SSA_EMP_MAP")
            ' BindassetGrid()
            'GetEmployeeSearchDetails(txtEmployeeCode.Text)
            'loadgrid()

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONF_EVENT_DETAILS_BY_REQ_ID")
            sp1.Command.AddParameter("@CONF_REQ_ID", Request.QueryString("id"), DbType.String)
            Dim ds1 As DataSet = sp1.GetDataSet()
            Dim InternalAttendees As Array
            If ds1.Tables(0).Rows.Count > 0 Then
                ' BindMail()
                txtDescription.Text = ds1.Tables(0).Rows(0).Item("CONF_DESCRIPTION")
                txtAttendees.Text = ds1.Tables(0).Rows(0).Item("EXTERNAL_ATTENDEES")
                InternalAttendees = ds1.Tables(0).Rows(0).Item("INTERNAL_ATTENDEES").Split(",")
                txtInternal.ClearSelection()
                For i = 0 To InternalAttendees.Length - 1
                    Dim k As String = InternalAttendees(i)
                    txtInternal.Items.Add(k)

                Next
            End If
        End If
    End Sub

    ' Private Sub BindassetGrid()

    'Dim param(3) As SqlParameter
    '    param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
    '    param(0).Value = ddlSelectLocation.SelectedItem.Value
    '    param(1) = New SqlParameter("@TWR", SqlDbType.NVarChar, 200)
    '    param(1).Value = ddlTower.SelectedItem.Value
    '    param(2) = New SqlParameter("@FLR", SqlDbType.NVarChar, 200)
    '    param(2).Value = ddlFloor.SelectedItem.Value
    '    param(3) = New SqlParameter("@CONF_ID", SqlDbType.NVarChar, 200)
    '    param(3).Value = ddlConference.SelectedItem.Value

    '    objsubsonic.BindGridView(gvassets, "GETCONFERENCE_ASSETS", param)

    'End Sub
    'Private Sub loadgrid()
    '    Dim param(2) As SqlParameter
    '    param(0) = New SqlParameter("@SPACE_ID", SqlDbType.NVarChar, 200)
    '    param(0).Value = ddlConference.SelectedItem.Value
    '    param(1) = New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
    '    param(1).Value = txtFdate.Text
    '    param(2) = New SqlParameter("@Mode", SqlDbType.Int)
    '    param(2).Value = "1"
    '    objsubsonic.BindGridView(gvDaily, "SPACE_CONFERENCE_CHART", param)

    'End Sub
    'Protected Sub gvassets_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvassets.PageIndexChanging
    '    gvassets.PageIndex = e.NewPageIndex()
    '    BindassetGrid()
    'End Sub

    Public Sub LoadCity()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = HttpContext.Current.Session("UID")
        objsubsonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE", param)
    End Sub

    Public Sub LoadLocation()
        objsubsonic.Binddropdown(ddlSelectLocation, "usp_getLocationDetails", "LCM_NAME", "LCM_CODE")
    End Sub

    Public Sub LoadTower()
        objsubsonic.Binddropdown(ddlTower, "usp_getTowerDetails", "twr_name", "twr_code")
    End Sub

    Public Sub LoadFloor()
        objsubsonic.Binddropdown(ddlFloor, "usp_getFloorDetails", "FLR_NAME", "FLR_CODE")
    End Sub


    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged

        ddlSelectLocation.Items.Clear()
        Dim city As String = ddlCity.SelectedItem.Value
        BindLocation(city)


    End Sub

    Protected Sub ddlSelectLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSelectLocation.SelectedIndexChanged

        Try
            'usp_getActiveTower_LOC

            If ddlSelectLocation.SelectedIndex <> -1 And ddlSelectLocation.SelectedIndex <> 0 Then
                BindTower(ddlSelectLocation.SelectedItem.Value)

            Else
                ddlTower.Items.Clear()
                ddlFloor.Items.Clear()
                ddlTower.Items.Add("--Select--")
                ddlFloor.Items.Add("--Select--")
            End If
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try

            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                BindFloor(ddlSelectLocation.SelectedItem.Value.ToString().Trim(), ddlTower.SelectedItem.Value)
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Add("--Select--")

            End If

        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlCapacity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCapacity.SelectedIndexChanged
        'GET_SPACE_CONFERENCE

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_SPACE_CONFERENCE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAPACITY", ddlCapacity.SelectedItem.Value, DbType.String)

        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlConf.DataSource = ds
        ddlConf.DataTextField = "CONF_NAME"
        ddlConf.DataValueField = "CONF_CODE"
        ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")
    End Sub

    Private Sub BindFloor(ByVal tower As String, ByVal loc As String)
        Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
        verDTO = verBll.GetDataforFloors(tower, loc.Trim())
        ddlFloor.DataSource = verDTO
        ddlFloor.DataTextField = "Floorname"
        ddlFloor.DataValueField = "Floorcode"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, "-- Select --")
    End Sub

    Private Sub BindTower(ByVal loc As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@vc_LOC", SqlDbType.NVarChar, 200)
        param(0).Value = loc
        objsubsonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)
    End Sub

    Private Sub BindLocation(ByVal city As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = city
        param(1) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("COMPANYID")
        objsubsonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Private Sub BindCapacity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlCapacity.DataSource = sp.GetDataSet
        ddlCapacity.DataTextField = "capacity"
        ddlCapacity.DataValueField = "capacity"
        ddlCapacity.DataBind()
        ddlCapacity.Items.Insert(0, "--Select--")

    End Sub

    Public Sub BindConference_ddl()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAPACITY", ddlCapacity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlConf.DataSource = ds
        ddlConf.DataTextField = "CONF_NAME"
        ddlConf.DataValueField = "CONF_CODE"
        ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")
    End Sub

    Private Sub cleardata()

        ddlCity.SelectedIndex = 0
        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlConf.Items.Clear()
        btnsubmit.Visible = False
    End Sub

    Private Sub Cancelrequest(ByVal id As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CANCEL_CONFERENCE")
        sp.Command.AddParameter("@REQ_ID", id, DbType.String)
        Dim dr As SqlDataReader = sp.GetReader()
        If dr.Read() Then
            aur_id = dr("AUR_ID").ToString()
            verticalreqid = dr("VER_REQID").ToString()
        End If
    End Sub

    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function

    Private Sub SubmitAllocationOccupied(ByVal strVerticalCode As String, ByVal strAurId As String, ByVal status As Integer)

        'REQID = objsubsonic.REQGENARATION_REQ(strVerticalCode, "SVR_ID", Session("TENANT") & "." , "SMS_VERTICAL_REQUISITION")
        REQID = Request.QueryString("id")
        Dim cntWst As Integer = 0
        Dim cntHCB As Integer = 0
        Dim cntFCB As Integer = 0
        Dim twr As String = ""
        Dim flr As String = ""
        Dim wng As String = ""
        Dim intCount As Int16 = 0
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "24"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        Dim remarks As String = ""

        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        'If starttimemin.SelectedItem.Value <> "Min" Then
        '    StartMM = starttimemin.SelectedItem.Text
        'End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            If endtimehr.SelectedItem.Value = "24" Then
                EndHr = "00"
            Else
                EndHr = endtimehr.SelectedItem.Text
            End If
        End If
        'If endtimemin.SelectedItem.Value <> "Min" Then
        '    EndMM = endtimemin.SelectedItem.Text
        'End If




        ftime = StartHr + ":" + StartMM
        ttime = EndHr + ":" + EndMM


        If CDate(txtFdate.Text).Month < getoffsetdatetime(DateTime.Now).Month Then ' And CDate(txtToDate.Text).Year < getoffsetdatetime(DateTime.Now).Year 
            lblMsg.Text = "You can't request for the past month"
            Exit Sub

        End If

        Dim selectedValues As String = String.Empty
        For i As Integer = 0 To txtInternal.Items.Count - 1
            selectedValues = selectedValues & txtInternal.Items(i).Value + Convert.ToString(",")

        Next
        If selectedValues <> "" Then
            selectedValues = selectedValues.Remove(selectedValues.Length - 1)
        End If

        Dim sta As Integer = status
        Dim param3(15) As SqlParameter

        param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
        param3(0).Value = REQID
        param3(1) = New SqlParameter("@VERTICALREQID", SqlDbType.NVarChar, 200)
        param3(1).Value = verticalreqid
        param3(2) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
        param3(2).Value = strVerticalCode

        param3(3) = New SqlParameter("@SPCID", SqlDbType.NVarChar, 200)
        param3(3).Value = ddlConf.SelectedValue
        param3(4) = New SqlParameter("@EMPID", SqlDbType.NVarChar, 200)
        param3(4).Value = aur_id
        param3(5) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        param3(5).Value = txtFdate.Text
        param3(6) = New SqlParameter("@TODATE", SqlDbType.DateTime)
        param3(6).Value = txtFdate.Text
        param3(7) = New SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50)
        param3(7).Value = "2" ' GETSPACETYPE(Request.QueryString("id")).Tables(0).Rows(0).Item("SPACE_TYPE")
        param3(8) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
        param3(8).Value = ftime
        param3(9) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
        param3(9).Value = ttime
        param3(10) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
        param3(10).Value = sta
        param3(11) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 10)
        param3(11).Value = Session("COMPANYID")
        param3(12) = New SqlParameter("@INT_ATND", SqlDbType.NVarChar, 1000)
        param3(12).Value = selectedValues
        param3(13) = New SqlParameter("@EXT_ATND", SqlDbType.NVarChar, 1000)
        param3(13).Value = txtAttendees.Text
        param3(14) = New SqlParameter("@DESC", SqlDbType.NVarChar, 200)
        param3(14).Value = txtDescription.Text
        param3(15) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param3(15).Value = Session("Uid").ToString()
        objsubsonic.GetSubSonicExecute("BLOCK_SEATS_REQUEST_PART3", param3)

        'Dim param3(5) As SqlParameter

        'param3(0) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
        'param3(0).Value = REQID
        'param3(1) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        'param3(1).Value = txtFdate.Text
        'param3(2) = New SqlParameter("@TODATE", SqlDbType.DateTime)
        'param3(2).Value = txtFdate.Text
        'param3(3) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
        'param3(3).Value = ftime
        'param3(4) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
        'param3(4).Value = ttime
        'param3(5) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
        'param3(5).Value = sta
        'objsubsonic.GetSubSonicExecute("CONF_UPDATE_CONFSTAT", param3)


    End Sub

    Protected Sub rbActions_CheckedChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActions.CheckedChanged
        If rbActionsrecurring.Checked = True Then
            Tr1.Visible = True
            btnsubmit.Visible = True
            gvItem.Visible = False
        Else
            Tr1.Visible = False
            btnsubmit.Visible = True
            gvItem.Visible = False
            starttimehr.ClearSelection()
            endtimehr.ClearSelection()
        End If
    End Sub
    Public Sub Textinternal_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim a As String = Textinternal.Text
        If ddlCapacity.SelectedItem.Value <= txtInternal.Items.Count Then
            lblMsg.Text = "Maximum capacity reached"
        Else
            Dim item As ListItem = txtInternal.Items.FindByText(a)
            If item IsNot Nothing Then
                lblMsg.Text = "The Selected mail already there"
            Else
                txtInternal.Items.Add(a)

            End If

        End If

        Textinternal.Text = ""



    End Sub
    Public Sub Remove_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim deletedItems As New List(Of ListItem)()

        If txtInternal.SelectedItem Is Nothing Then
            lblMsg.Text = "select atleast one mail"

        Else
            For Each item As ListItem In txtInternal.Items
                If item.Selected Then
                    deletedItems.Add(item)
                End If
            Next
            For Each item As ListItem In deletedItems
                txtInternal.Items.Remove(item)
            Next
        End If

    End Sub


    Private Sub BindGrid()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONFERENCE_BOOKED_SLOTS")
        sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        sp.Command.AddParameter("@SPC_ID", ddlConf.SelectedValue, DbType.String)
        sp.Command.AddParameter("@FROM_DATE", txtFdate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_DATE", txtTdate.Text, DbType.Date)
        sp.Command.AddParameter("@FROM_TIME", starttimehr.SelectedValue, DbType.String)
        sp.Command.AddParameter("@TO_TIME", endtimehr.SelectedValue, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        gvItem.DataSource = sp.GetDataSet()
        gvItem.DataBind()

        If (ds.Tables(0).Rows.Count > 0) Then
            gvItem.Visible = True
            lblconfalert.Visible = True
            lblconfalert.Text = "Note: selected time interval is already booked, please select other available slots."

        ElseIf (txtFdate.Text >= getoffsetdate(Date.Today)) Then
            'If starttimehr.SelectedValue < endtimehr.SelectedValue Then
            Dim StrSearchQry As String = ""
            'If String.IsNullOrEmpty(txtEmployeeCode.Text) = False Then
            '    lblMsg.Text = ""
            '    StrSearchQry = txtEmployeeCode.Text

            '    GetEmployeeSearchDetails(StrSearchQry)
            REQID = Request.QueryString("id")
            Dim cntWst As Integer = 0
            Dim cntHCB As Integer = 0
            Dim cntFCB As Integer = 0
            Dim twr As String = ""
            Dim flr As String = ""
            Dim wng As String = ""
            Dim intCount As Int16 = 0
            Dim ftime As String = ""
            Dim ttime As String = ""
            Dim StartHr As String = "00"
            Dim EndHr As String = "24"
            Dim StartMM As String = "00"
            Dim EndMM As String = "00"

            Dim selectedValues As String = String.Empty
            For i As Integer = 0 To txtInternal.Items.Count - 1

                selectedValues = selectedValues & txtInternal.Items(i).Value + Convert.ToString(",")

            Next
            If selectedValues <> "" Then
                selectedValues = selectedValues.Remove(selectedValues.Length - 1)
            End If
            If starttimehr.SelectedItem.Value <> "HH" Then
                StartHr = starttimehr.SelectedItem.Text
            End If
            'If starttimemin.SelectedItem.Value <> "Min" Then
            '    StartMM = starttimemin.SelectedItem.Text
            'End If
            If endtimehr.SelectedItem.Value <> "HH" Then
                If endtimehr.SelectedItem.Value = "24" Then
                    EndHr = "00"
                Else
                    EndHr = endtimehr.SelectedItem.Text
                End If
            End If
            'If endtimemin.SelectedItem.Value <> "Min" Then
            '    EndMM = endtimemin.SelectedItem.Text
            'End If

            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
            Dim param(17) As SqlParameter
            param(0) = New SqlParameter("@LOC", SqlDbType.NVarChar, 200)
            param(0).Value = ddlSelectLocation.SelectedItem.Value
            param(1) = New SqlParameter("@TOWER", SqlDbType.NVarChar, 200)
            param(1).Value = ddlTower.SelectedItem.Value
            param(2) = New SqlParameter("@FLOOR", SqlDbType.NVarChar, 200)
            param(2).Value = ddlFloor.SelectedItem.Value
            param(3) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
            param(3).Value = Convert.ToDateTime(txtFdate.Text)
            param(4) = New SqlParameter("@TODATE", SqlDbType.DateTime)
            param(4).Value = Convert.ToDateTime(txtTdate.Text)
            param(5) = New SqlParameter("@FROMTIME", SqlDbType.DateTime)
            param(5).Value = Convert.ToDateTime(ftime)
            param(6) = New SqlParameter("@TOTIME", SqlDbType.DateTime)
            param(6).Value = Convert.ToDateTime(ttime)
            param(7) = New SqlParameter("@CONF_CODE", SqlDbType.VarChar)
            param(7).Value = ddlConf.SelectedValue
            param(8) = New SqlParameter("@REQID", SqlDbType.NVarChar, 200)
            param(8).Value = REQID
            param(9) = New SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200)
            param(9).Value = "Conference"
            param(10) = New SqlParameter("@SPCID", SqlDbType.NVarChar, 200)
            param(10).Value = ddlConf.SelectedValue
            param(11) = New SqlParameter("@SPACETYPE", SqlDbType.Int, 50)
            param(11).Value = 2
            Dim sta As Integer = check_user_role()
            If sta = 0 Then
                param(12) = New SqlParameter("@STATUSID", SqlDbType.Int, 200)
                param(12).Value = 3
                strStatus = "3"
            Else
                param(12) = New SqlParameter("@STATUSID", SqlDbType.Int, 200)
                param(12).Value = 5
                strStatus = "5"
            End If
            param(13) = New SqlParameter("@COMPANY", SqlDbType.Int)
            param(13).Value = Session("COMPANYID")
            param(14) = New SqlParameter("@EMPID", SqlDbType.VarChar, 100)
            param(14).Value = Session("UID")
            param(15) = New SqlParameter("@INT_ATND", SqlDbType.NVarChar, 1000)
            param(15).Value = selectedValues
            param(16) = New SqlParameter("@EXT_ATND", SqlDbType.NVarChar, 1000)
            param(16).Value = txtAttendees.Text
            param(17) = New SqlParameter("@DESC", SqlDbType.NVarChar, 200)
            param(17).Value = txtDescription.Text

            'ObjSubSonic.Binddropdown(ddlConference, "usp_get_CONFERENCE_Seats_PART1", "spc_name", "spc_id", param)
            'confroom.Visible = True

            'If Request.QueryString("mode") = 2 Then
            '    emp.Visible = True
            'Else
            '    emp.Visible = False
            'End If

            'added by praveen on 13/02/2014
            Dim flag As Integer
            flag = objsubsonic.GetSubSonicExecuteScalar("USP_CONF_MODIFY_SEAT", param)

            If flag = 1 Then 'added a new flag case.
                ' SubmitAllocationOccupied("Conference", Session("Uid").ToString(), 3)
                lblMsg.Text = ddlConf.SelectedItem.Text & " is booked successfully ."
                'BindEventDetails()
                'BookEventOnGoogleCalendar()
                cleardata()
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_CONF_MODIFY_REQUEST")
                sp1.Command.AddParameter("@REQID", REQID, DbType.String)
                sp1.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
                sp1.ExecuteScalar()
                ' Start here
                strRedirect = "frmConference_finalpage.aspx?sta=" & clsSecurity.Encrypt(strStatus) & "&rid=" & clsSecurity.Encrypt(REQID)
                'Else
                '    PopUpMessage("Already conference is alloted for this date.Please select another date ", Me)
            End If


GVColor:


            If strRedirect <> String.Empty Then
                Response.Redirect(strRedirect, False)
            End If
        Else
            lblMsg.Text = "Please Select From Time and To Time"
        End If
        'Else
        '    lblMsg.Text = "Date Must Be Greater than Today's date"
        'End If
        ' Response.Redirect("editconference.aspx")

        'Else
        'lblMsg.Text = "Please enter employee code."
        'End If
    End Sub

    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        gvItem.Visible = False
        Try
            Dim intFromDate As Date = CType(txtFdate.Text, Date)
        Catch ex As Exception
            lblMsg.Text = "Please enter vaild From Date "
            lblMsg.Visible = True
            Exit Sub
        End Try

        Try
            Dim intToDate As Date = CType(txtTdate.Text, Date)
        Catch ex As Exception
            lblMsg.Text = "Please enter vaild To Date "
            lblMsg.Visible = True
            Exit Sub
        End Try
        If CDate(txtTdate.Text) <= CDate(txtFdate.Text) Then
            'lblMsg.Text = "To date should be greater than from date "
            'lblMsg.Visible = True
            'Exit Sub
        ElseIf CDate(txtFdate.Text) < CDate(txtDate.Text) Then
            lblMsg.Text = " Please enter valid from date that has to be greater than today's date "
            lblMsg.Visible = True
            Exit Sub
        End If

        If endtimehr.SelectedItem.Value < starttimehr.SelectedItem.Value Then
            lblMsg.Text = "To Time should be grater than from Time"
            'btnsubmit.Visible = False
            Exit Sub
        End If
        Dim InternalCount As Integer = 0
        Dim InternalAttendees As String = ""
        'For Each li As ListItem In lstInternal.Items
        '    If li.Selected = True Then
        '        InternalCount = 1
        '    End If
        'Next

        'If txtAttendees.Text = "" And txtInternal.Text = "" Then
        '    lblMsg.Text = "Please enter or select at least one Attendees Email"
        '    lblMsg.Visible = True
        '    Exit Sub
        'End If

        BindGrid()
    End Sub

    Public Function check_user_role() As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CONF_CHECK_EMP_ROLE")
        sp.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
        Return sp.ExecuteScalar()
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindEventDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CancelConferenceRoom")

        sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
        sp.ExecuteScalar()
        Dim rtnurl As String = ""
        rtnurl = Request.QueryString("rtnurl")
        If rtnurl = "" Then
            strRedirect = "frmConference_finalpage.aspx?sta=" & clsSecurity.Encrypt("4") & "&rid=" & clsSecurity.Encrypt(Request.QueryString("id"))
        Else
            strRedirect = rtnurl
        End If
        If strRedirect <> String.Empty Then
            Response.Redirect(strRedirect, False)
        End If

    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        'GET_SPACE_CONFERENCE
        If ddlFloor.SelectedIndex <> 0 Then
            BindConference_ddl()
        End If

    End Sub


    Private Function BindEventDetails() As Boolean

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONF_EVENT_DETAILS_BY_REQ_ID")
        sp.Command.AddParameter("@CONF_REQ_ID", Request.QueryString("id"), DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("CONF_EVENT_ID") <> "" Then

                Dim param1(1) As SqlParameter
                param1(0) = New SqlParameter("@CONF_EVENT_ID", SqlDbType.NVarChar, 1000)
                param1(0).Value = ds.Tables(0).Rows(0).Item("CONF_EVENT_ID")
                param1(1) = New SqlParameter("@DELETED_BY", SqlDbType.NVarChar, 100)
                param1(1).Value = Session("UID")
                objsubsonic.GetSubSonicExecuteScalar("UPDATE_CALENDAR_EVENT_BY_EVENT_ID", param1)

                'Dim calendarservice As New clsCalendarService
                'With calendarservice
                '    .DelEventID = ds.Tables(0).Rows(0).Item("CONF_EVENT_ID")
                'End With
                'Return calendarservice.DeleteEvents()

            End If
        End If

    End Function

    Private Function BookEventOnGoogleCalendar() As Boolean
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "00"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        If starttimehr.SelectedItem.Value <> "Hr" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
        If endtimehr.SelectedItem.Value <> "Hr" Then
            If endtimehr.SelectedItem.Value = "24" Then
                EndHr = "00"
            Else
                EndHr = endtimehr.SelectedItem.Text
            End If
        End If

        If EndHr = "00" And StartHr = "23" Then
            EndHr = "23:59"
        End If

        ftime = txtFdate.Text + " " + StartHr + ":" + StartMM
        ttime = txtFdate.Text + " " + EndHr + ":" + EndMM

        Dim RecCount As Integer
        Dim FromDate As DateTime = txtFdate.Text
        Dim ToDate As DateTime = txtTdate.Text
        RecCount = (ToDate - FromDate).TotalDays
        RecCount = RecCount + 1

        Dim calendarservice As New clsCalendarService
        With calendarservice
            .Summary = txtDescription.Text
            .Location = ddlSelectLocation.SelectedItem.Text + "," + ddlTower.SelectedItem.Text + "," + ddlFloor.SelectedItem.Text + "," + ddlConf.SelectedItem.Text
            .Description = txtDescription.Text
            .Start = New EventDateTime
            With calendarservice.Start
                .DateTime = ftime
                '.TimeZone = "Asia/Kolkata"
                .TimeZone = Session("useroffset")
            End With

            .End = New EventDateTime
            With .End
                .DateTime = ttime
                .TimeZone = Session("useroffset")
            End With

            Dim ndx As Integer = 0
            Dim ndx1 As Integer = 0
            If txtAttendees.Text <> "" Then
                Dim attendees() = txtAttendees.Text.Split(",")
                .Attendees = New EventAttendee(attendees.Length - 1) {}
                For i = 0 To attendees.Length - 1
                    .Attendees(ndx) = New EventAttendee
                    With .Attendees(ndx)
                        .Email = (attendees(ndx)).Trim()
                    End With
                    ndx = ndx + 1
                Next
            End If
            Dim intattendees() = txtInternal.Text.Split(",")
            .Attendees = New EventAttendee(intattendees.Length - 1) {}
            For j = 0 To intattendees.Length - 1
                .Attendees(ndx1) = New EventAttendee
                With .Attendees(ndx1)
                    .Email = (intattendees(ndx1)).Trim()
                End With
                ndx1 = ndx1 + 1
            Next
            For i = 0 To txtInternal.Items.Count - 1
                If txtInternal.Items(i).Selected Then
                    ReDim Preserve .Attendees(ndx + 1)
                    .Attendees(ndx) = New EventAttendee
                    With .Attendees(ndx)
                        .Email = txtInternal.Items(i).Value
                    End With
                    ndx = ndx + 1
                End If
            Next
            .Recurrence = New String() {"RRULE:FREQ=DAILY;COUNT=" + RecCount.ToString()}
            .Reminders = New Google.Apis.Calendar.v3.Data.Event.RemindersData()
            With .Reminders
                .UseDefault = False
                .Overrides = New EventReminder() {
                    New EventReminder() With {.Method = "email", .Minutes = 24 * 60},
                    New EventReminder() With {.Method = "popup", .Minutes = 10}
                }
            End With
        End With

        Dim Event_ID As String = calendarservice.PushEvent()

        Dim InternalAttendees As String = ""
        For Each li As ListItem In txtInternal.Items
            If li.Selected = True Then
                InternalAttendees = InternalAttendees + li.Value + ","
            End If
        Next

        'InternalAttendees = InternalAttendees.TrimEnd(",")

        Dim param_Event(6) As SqlParameter
        param_Event(0) = New SqlParameter("@CONF_REQ_ID", SqlDbType.NVarChar, 1000)
        param_Event(0).Value = Request.QueryString("id")
        param_Event(1) = New SqlParameter("@CONF_EVENT_ID", SqlDbType.NVarChar, 1000)
        param_Event(1).Value = Event_ID
        param_Event(2) = New SqlParameter("@CONF_EVENT_STATUS", SqlDbType.Int)
        param_Event(2).Value = 1
        param_Event(3) = New SqlParameter("@CONF_DESCRIPTION", SqlDbType.NVarChar, 1000)
        param_Event(3).Value = txtDescription.Text
        param_Event(4) = New SqlParameter("@CREATED_BY", SqlDbType.NVarChar, 100)
        param_Event(4).Value = Session("UID")
        param_Event(5) = New SqlParameter("@EXTERNAL_ATTENDEES", txtAttendees.Text)
        param_Event(6) = New SqlParameter("@INTERNAL_ATTENDEES", InternalAttendees)
        objsubsonic.GetSubSonicExecute("CONF_GOOGLE_NEW_EVENT_CREATE", param_Event)

    End Function

End Class
