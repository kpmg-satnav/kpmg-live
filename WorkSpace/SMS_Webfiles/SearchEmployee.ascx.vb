Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions

Partial Class WorkSpace_SMS_Webfiles_Controls_SearchEmployee
    Inherits System.Web.UI.UserControl
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvEmp.Visible = False
            pnlEmailid.Visible = False
            pnlempid.Visible = True
            lblMsg.Visible = False
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'If String.IsNullOrEmpty(txtEmpId.Text.Trim()) Then
        '    lblMsg.Visible = True
        '    lblMsg.Text = "ok"
        'End If

        Dim empstatus As String
        lblMsg.Visible = False
        If radSearch.SelectedItem.Value = "0" Then
            lblMsg.Visible = False
            If String.IsNullOrEmpty(txtEmpId.Text.Trim()) Then
                gvEmp.Visible = False
                lblMsg.Visible = True
                lblMsg.Text = "Please Enter Employee Id to Search..."
            Else
                empstatus = checkaurid()
                gvEmp.Visible = False
                If empstatus = "0" Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Employee Id does not exist..."
                Else
                    gvEmp.Visible = True
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SPC_EMP_ID_SEARCH")
                    sp.Command.AddParameter("@AUR_ID", txtEmpId.Text, DbType.String)
                    gvEmp.DataSource = sp.GetDataSet()
                    gvEmp.DataBind()
                End If
            End If
        Else
            If String.IsNullOrEmpty(txtEmpName.Text.Trim()) Then
                gvEmp.Visible = False
                lblMsg.Visible = True
                lblMsg.Text = "Please Enter Email Id to Search..."
            Else

                lblMsg.Visible = False
                gvEmp.Visible = True
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SPC_EMP_NAME_SEARCH")
                sp.Command.AddParameter("@aur_name", txtEmpName.Text, DbType.String)

                Dim ds As New DataSet
                ds = sp.GetDataSet()
                gvEmp.DataSource = ds
                gvEmp.DataBind()
            End If

        End If
    End Sub

    Private Function checkaurid()
        Dim empstatus As String
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SPC_EMPID_CHECK")
        sp.Command.AddParameter("@AUR_ID", txtEmpId.Text, DbType.String)
        empstatus = sp.ExecuteScalar()
        Return empstatus
    End Function

    Protected Sub radSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radSearch.SelectedIndexChanged
        If radSearch.SelectedItem.Value = "0" Then
            pnlempid.Visible = True
            pnlEmailid.Visible = False
            gvEmp.Visible = False
            lblMsg.Visible = False
            txtEmpId.Text = ""
            lblMsg.Visible = False

        Else
            pnlEmailid.Visible = True
            pnlempid.Visible = False
            gvEmp.Visible = False
            lblMsg.Visible = False
            txtEmpName.Text = ""
            lblMsg.Visible = False

        End If
    End Sub
End Class
