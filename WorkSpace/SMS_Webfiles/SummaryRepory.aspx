<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SummaryRepory.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_SummaryRepory" %>

<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

 
        <div>
        <table id="table5" cellspacing="0" cellpadding="0"  width="100%"
                    align="center" border="0">
                    <tr>
                        <td width="100%" align="center"> <asp:Label ID="lblHead" runat="server" CssClass="clsHead"  Width="95%" Font-Underline="False" ForeColor="Black">Space Connect
             <hr align="center" width="60%" /></asp:Label></td>
                        </tr>
                        </table>
          
                 
            
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%">
                <table id="table3" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                        <td  colspan="3" align="left">
           <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
          </td>
                        </tr>
                    <tr>
                        <td style="width: 36px">
                            <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                        <td width="100%" class="tableHEADER" align="left">
                            &nbsp;<strong>City/Location Wise Summary Report</strong>&nbsp;</td>
                        <td>
                            <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif" style="width: 36px">
                        </td>
                        <td align="left">
                            <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="clsMessage"
                                ForeColor="" />
                            <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                            <table id="table2" cellspacing="1" cellpadding="1" style="width: 100%; border-collapse: collapse;"
                                border="1">
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="rblstSummary" runat="server" AutoPostBack="True">
                                            <asp:ListItem Value="1">City Wise Summary Report</asp:ListItem>
                                            <asp:ListItem Value="2">Location Wise Summary Report</asp:ListItem>
                                            <asp:ListItem Value="3">Tower Wise Summary Report</asp:ListItem>
                                            <asp:ListItem Value="4">Vertical Wise Summary Report</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px;">
                            &nbsp;</td>
                    </tr>
                    <tr id="trCityCriteria" runat="server">
                        <td background="../../Images/table_left_mid_bg.gif" style="width: 36px">
                        </td>
                        <td align="left">
                            <table id="table1" cellspacing="1" cellpadding="1" style="width: 100%; border-collapse: collapse;"
                                border="1">
                                <tr>
                                    <td style="width: 238px" class="clslabel">
                                        Select City :</td>
                                    <td>
                                        <asp:DropDownList ID="ddlCity" runat="server" Width="159px" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px;">
                            &nbsp;</td>
                    </tr>
                    <tr id="trLocCity" runat="server">
                        <td background="../../Images/table_left_mid_bg.gif" style="width: 36px">
                        </td>
                        <td align="left">
                            <table id="table4" cellspacing="1" cellpadding="1" style="width: 100%; border-collapse: collapse;"
                                border="1">
                                <tr>
                                    <td class="clslabel">
                                        Select City :</td>
                                    <td>
                                        <asp:DropDownList ID="ddlCty" runat="server" Width="159px" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            <%--    <tr>
                                    <td class="clslabel">
                                        Select Location Type :</td>
                                    <td>
                                        <asp:DropDownList ID="ddlLocType" runat="server" Width="159px" AutoPostBack="True">
                                            <asp:ListItem Text="--Select--" Value="--Select--"></asp:ListItem>
                                            <asp:ListItem Text="--All--" Value="--All--"></asp:ListItem>
                                           <%-- <asp:ListItem Text="STPI" Value="STPI"></asp:ListItem>
                                            <asp:ListItem Text="SEZ" Value="SEZ"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td class="clslabel">
                                        Select Location :</td>
                                    <td>
                                        <asp:DropDownList ID="ddlLocation" runat="server" Width="159px">
                                        </asp:DropDownList>
                                        &nbsp; &nbsp; &nbsp;
                                        <asp:Button ID="Button1" runat="server" Text="View" /></td>
                                </tr>
                            </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px;">
                            &nbsp;</td>
                    </tr>
                    <%-- <tr id="trCityCriteria" runat="server">
                            <td style="width: 36px">
                                Select City : 
                            </td>
                            <td>
                            <asp:DropDownList ID="ddlCity" runat="server"></asp:DropDownList>
                            </td>                            
                            </tr>--%>
                    <br />
                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" CssClass="clsButton"
                        Width="136px" />
                    <br />
                    <br />
                    <cc1:ExportPanel ID="ExportPanel1" runat="server">
                        <tr runat="server" id="trCity">
                            <td background="../../Images/table_left_mid_bg.gif" style="width: 36px">
                            </td>
                            <td valign="top" align="left">
                                <br />
                              <%--  <h2>
                                    Location Type: STPI</h2>--%>
                                <br />
                                <asp:GridView ID="gdavail" ShowFooter="true" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="City" HeaderText="City Name" FooterText="Total">
                                            <FooterStyle Font-Bold="True" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Location" HeaderText="Location Name"></asp:BoundField>
                                        <asp:BoundField DataField="Tower" HeaderText="Tower Name"></asp:BoundField>
                                        <asp:BoundField DataField="Type" HeaderText="Location Type"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Available Seats">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Available Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Allotted Seats">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount1" runat="server" Text='<%# Eval("Allotted Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblAllotted" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Occupied Seats">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount2" runat="server" Text='<%# Eval("Occupied Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblOccupied" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Alloted but not Occupied">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount4" runat="server" Text='<%# Eval("Allocated but not Occupied").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblAllocNotOcc" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Available for Allocation">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount3" runat="server" Text='<%# Eval("Available for Allocation").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblAvailAlloc" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <br />
                             <%--   <h2>
                                    Location Type: SEZ</h2>--%>
                                <br />
                                <asp:GridView ID="gdavail_SEZ" ShowFooter="true" runat="server" AutoGenerateColumns="False"
                                    Width="100%" Visible="false">
                                    <Columns>
                                        <asp:BoundField DataField="City" HeaderText="City Name" FooterText="Total">
                                            <FooterStyle Font-Bold="True" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Location" HeaderText="Location Name" />
                                        <asp:BoundField DataField="Tower" HeaderText="Tower Name" />
                                        <asp:BoundField DataField="Type" HeaderText="Location Type" />
                                        <asp:TemplateField HeaderText="Available Seats">
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Available Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Allotted Seats">
                                            <FooterTemplate>
                                                <asp:Label ID="lblAllotted" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount1" runat="server" Text='<%# Eval("Allotted Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Occupied Seats">
                                            <FooterTemplate>
                                                <asp:Label ID="lblOccupied" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount2" runat="server" Text='<%# Eval("Occupied Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Alloted but not Occupied">
                                            <FooterTemplate>
                                                <asp:Label ID="lblAllocNotOcc" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount4" runat="server" Text='<%# Eval("Allocated but not Occupied").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Available for Allocation">
                                            <FooterTemplate>
                                                <asp:Label ID="lblAvailAlloc" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount3" runat="server" Text='<%# Eval("Available for Allocation").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%">
                            </td>
                        </tr>
                        
                        <tr id="trTowerCity" runat="server">
                            <td background="../../Images/table_left_mid_bg.gif" style="width: 36px">
                            </td>
                            <td>
                                Select City:
                                <asp:DropDownList ID="ddlTowerCity" AutoPostBack="true" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%">
                            </td>
                        </tr>
                        <tr runat="server" id="trTowerWise">
                            <td background="../../Images/table_left_mid_bg.gif" style="width: 36px">
                            </td>
                            <td valign="top" align="left">
                                <br />
                                <%--<h2>
                                    Location Type: STPI</h2>--%>
                                <br />
                                <asp:GridView ID="gvTower_STPI" EmptyDataText="There is no records available." ShowFooter="true"
                                    runat="server" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="City" HeaderText="City Name" FooterText="Total">
                                            <FooterStyle Font-Bold="True" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Location" HeaderText="Location Name"></asp:BoundField>
                                        <asp:BoundField DataField="Tower" HeaderText="Tower Name"></asp:BoundField>
                                        <asp:BoundField DataField="Type" HeaderText="Location Type"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Available Seats">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Available Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Allotted Seats">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount1" runat="server" Text='<%# Eval("Allotted Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblAllotted" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Occupied Seats">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount2" runat="server" Text='<%# Eval("Occupied Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblOccupied" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Alloted but not Occupied">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount4" runat="server" Text='<%# Eval("Allocated but not Occupied").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblAllocNotOcc" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Available for Allocation">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount3" runat="server" Text='<%# Eval("Available for Allocation").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblAvailAlloc" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <br />
                               <%-- <h2>
                                    Location Type: SEZ</h2>--%>
                                <br />
                                <asp:GridView ID="gvTower_SEZ" EmptyDataText="There is no records available." ShowFooter="true"
                                    runat="server" AutoGenerateColumns="False" Width="100%" Visible="false">
                                    <Columns>
                                        <asp:BoundField DataField="City" HeaderText="City Name" FooterText="Total">
                                            <FooterStyle Font-Bold="True" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Location" HeaderText="Location Name" />
                                        <asp:BoundField DataField="Tower" HeaderText="Tower Name" />
                                        <asp:BoundField DataField="Type" HeaderText="Location Type" />
                                        <asp:TemplateField HeaderText="Available Seats">
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Available Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Allotted Seats">
                                            <FooterTemplate>
                                                <asp:Label ID="lblAllotted" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount1" runat="server" Text='<%# Eval("Allotted Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Occupied Seats">
                                            <FooterTemplate>
                                                <asp:Label ID="lblOccupied" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount2" runat="server" Text='<%# Eval("Occupied Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Alloted but not Occupied">
                                            <FooterTemplate>
                                                <asp:Label ID="lblAllocNotOcc" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount4" runat="server" Text='<%# Eval("Allocated but not Occupied").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Available for Allocation">
                                            <FooterTemplate>
                                                <asp:Label ID="lblAvailAlloc" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount3" runat="server" Text='<%# Eval("Available for Allocation").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%">
                            </td>
                        </tr>
                        <tr id="trBusinessUnitWise" visible="false" runat="server">
                            <td colspan="6">
                                <table width="100%" cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td class="clslabel">
                                            Select Vertical :</td>
                                        <td>
                                            <asp:DropDownList ID="ddlBusinessUnit" runat="server" Width="159px">
                                            </asp:DropDownList>
                                            &nbsp; &nbsp; &nbsp;
                                            <asp:Button ID="btnBusinessUnit" runat="server" Text="View" /></td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                                <asp:GridView ID="gvBusinessUnit" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    ForeColor="#333333" ShowFooter="true" GridLines="None">
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    <Columns>
                                        <asp:BoundField DataField="VER_NAME" HeaderText="VERTICAL" SortExpression="VER_NAME" />
                                        <asp:BoundField DataField="TWR_ID" Visible="true" HeaderText="TOWER" SortExpression="TWR_ID" />
                                        <asp:BoundField DataField="FLR_ID" Visible="true" HeaderText="FLOOR" SortExpression="FLR_ID" />
                                        <%-- <asp:BoundField DataField="WNG_NAME" Visible="false" HeaderText="WING" SortExpression="WNG_NAME" />
                                 <asp:BoundField DataField="SSA_PROJECT" HeaderText="SSA_PROJECT" SortExpression="SSA_PROJECT" />--%>
                                        <asp:TemplateField HeaderText="AVL WS">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTOTAL_WS" runat="server" Text='<%# Eval("TOTAL_WS").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalTOTAL_WS" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AVL FC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTOTAL_FC" runat="server" Text='<%# Eval("TOTAL_FC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalTOTAL_FC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AVL HC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTOTAL_HC" runat="server" Text='<%# Eval("TOTAL_HC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalTOTAL_HC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AVL TOTAL">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTOTAL_TOTAL" runat="server" Text='<%# Eval("TOTAL_TOTAL").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalTOTAL_TOTAL" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ALC WS">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCAP_WS" runat="server" Text='<%# Eval("CAP_WS").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalCAP_WS" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ALC FC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCAP_FC" runat="server" Text='<%# Eval("CAP_FC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalCAP_FC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ALC HC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCAP_HC" runat="server" Text='<%# Eval("CAP_HC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalCAP_HC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ALC TOTAL">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCAP_TOTAL" runat="server" Text='<%# Eval("CAP_TOTAL").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalCAP_TOTAL" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OCC WS">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOCC_WS" runat="server" Text='<%# Eval("OCC_WS").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalOCC_WS" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OCC FC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOCC_FC" runat="server" Text='<%# Eval("OCC_FC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalOCC_FC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OCC HC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOCC_HC" runat="server" Text='<%# Eval("OCC_HC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalOCC_HC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OCC TOTAL">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOCC_TOTAL" runat="server" Text='<%# Eval("OCC_TOTAL").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalOCC_TOTAL" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VACANT WS">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVACANT_WS" runat="server" Text='<%# Eval("VACANT_WS").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalVACANT_WS" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VACANT FC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVACANT_FC" runat="server" Text='<%# Eval("VACANT_FC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalVACANT_FC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VACANT HC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVACANT_HC" runat="server" Text='<%# Eval("VACANT_HC").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalVACANT_HC" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VACANT TOTAL">
                                            <ItemTemplate>
                                                <asp:Label ID="lblVACANT_TOTAL" runat="server" Text='<%# Eval("VACANT_TOTAL").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalVACANT_TOTAL" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left"
                                        VerticalAlign="Top" />
                                    <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <asp:Label ID="lblTot" runat="server"></asp:Label><tr runat="server" id="trLocation">
                            <td background="../../Images/table_left_mid_bg.gif" style="width: 36px">
                            </td>
                            <td>
                                <asp:GridView ID="GridView1" ShowFooter="true" runat="server" AutoGenerateColumns="False"
                                    Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="City" HeaderText="City Name" FooterText="Total">
                                            <FooterStyle Font-Bold="True" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Location" HeaderText="Location Name"></asp:BoundField>
                                        <asp:BoundField DataField="Tower" HeaderText="Tower Name"></asp:BoundField>
                                        <asp:BoundField DataField="Type" HeaderText="Location Type"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Available Seats">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Available Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Allotted Seats">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount1" runat="server" Text='<%# Eval("Allotted Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblAllotted" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Occupied Seats">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount2" runat="server" Text='<%# Eval("Occupied Seats").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblOccupied" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Alloted but not Occupied">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount4" runat="server" Text='<%# Eval("Allocated but not Occupied").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblAllocNotOcc" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Available for Allocation">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount3" runat="server" Text='<%# Eval("Available for Allocation").ToString()%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblAvailAlloc" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                &nbsp;
                            </td>
                            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%">
                            </td>
                        </tr>
                    </cc1:ExportPanel>
                    <tr>
                        <td style="width: 36px; height: 17px;">
                            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </asp:Panel>
          
        </div>
   </asp:Content>