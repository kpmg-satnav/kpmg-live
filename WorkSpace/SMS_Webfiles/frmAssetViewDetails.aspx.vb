Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmAssetViewDetails
    Inherits System.Web.UI.Page

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))


    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))


    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))

    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtasstcode.ReadOnly = True
            txtasset.ReadOnly = True
            txtemp.ReadOnly = True
            txtasstflr.ReadOnly = True
            txtassttower.ReadOnly = True
            txtasstloc.ReadOnly = True
            txtastcity.ReadOnly = True
            Dim Reqid As String = Request.QueryString("id")
            BindDetails(Reqid)
        End If
    End Sub
    Private Sub BindDetails(ByVal Reqid As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_GET_ASSET_DETAILS")
            sp.Command.AddParameter("@ASST_CODE", Reqid, DbType.String)
            Dim ds As New dataset()
            ds = sp.GetDataSet()
            If ds.tables(0).rows.count > 0 Then
                txtasstcode.Text = ds.Tables(0).Rows(0).Item("AAS_AAT_CODE")
                txtasset.Text = ds.Tables(0).Rows(0).Item("AAT_NAME")
                txtemp.Text = ds.Tables(0).Rows(0).Item("EMP")
                txtasstflr.Text = ds.Tables(0).Rows(0).Item("FLOOR")
                txtassttower.Text = ds.Tables(0).Rows(0).Item("TOWER")
                txtasstloc.Text = ds.Tables(0).Rows(0).Item("LOCATION")
                txtastcity.Text = ds.Tables(0).Rows(0).Item("CITY")

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
