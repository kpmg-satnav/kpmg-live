﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script defer src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script defer src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->


    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }
    </style>
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>

</head>
<body data-ng-controller="EntityController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Parent Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Sub Function1 Master</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form role="form" id="form1" name="frmEntity" data-valid-submit="Save()" novalidate>
                        <%-- <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" />--%>
                        <div class="row form-inline">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Sub Function1 Code <span style="color: red;">*</span></label>
                                    <%--<asp:RequiredFieldValidator ID="rfvchild" runat="server" ErrorMessage="Please Enter Valid Sub Function1 Code.... "
                                                         ControlToValidate="CH_ENTY_CODE" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                    <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()" data-ng-class="{'has-error': frmEntity.$submitted && frmEntity.CH_ENTY_CODE.$invalid}">
                                        <input id="CH_ENTY_CODE" name="CH_ENTY_CODE" type="text" data-ng-model="ChildEntity.CH_ENTY_CODE" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" maxlength="15" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" data-ng-hide="" />
                                        <span class="error" style="color: red" data-ng-show="frmEntity.$submitted && frmEntity.CH_ENTY_CODE.$invalid">Please enter valid Sub Function1 code </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Sub Function1 Name <span style="color: red;">*</span></label>
                                    <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()" data-ng-class="{'has-error': frmEntity.$submitted && frmEntity.CH_ENTY_NAME.$invalid}">
                                        <input id="CH_ENTY_NAME" name="CH_ENTY_NAME" type="text" data-ng-model="ChildEntity.CH_ENTY_NAME" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" maxlength="50" class="form-control" autofocus required="" />
                                        <span class="error" style="color: red" data-ng-show="frmEntity.$submitted && frmEntity.CH_ENTY_NAME.$invalid">Please enter valid Sub Function1 name </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Sub Function Name<span style="color: red;">*</span></label>
                                    <div data-ng-class="{'has-error': frmEntity.$submitted && frmEntity.CH_ENTY_PRNT_CODE.$invalid}">
                                        <select id="CH_ENTY_PRNT_CODE" name="CH_ENTY_PRNT_CODE" data-ng-model="ChildEntity.CH_ENTY_PRNT_CODE" class="selectpicker" required>
                                            <option value="">--Select--</option>
                                            <option data-ng-repeat="Enty in ParentEntityList" value="{{Enty.PRNT_CODE}}">{{Enty.PRNT_NAME}}</option>
                                        </select>
                                        <span class="error" data-ng-show="frmEntity.$submitted && frmEntity.CH_ENTY_PRNT_CODE.$invalid" style="color: red">Please select Sub Function name </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                        <textarea id="CH_ENTY_REM" style="height: 60px" name="CH_ENTY_REM" data-ng-model="ChildEntity.CH_ENTY_REM" class="form-control" maxlength="500"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row form-inline">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-show="ActionStatus==1">
                                    <label>Status<span style="color: red;">*</span></label>
                                    <select id="CH_ENTY_STA_ID" name="CH_ENTY_STA_ID" data-ng-model="ChildEntity.CH_ENTY_STA_ID" class="form-control">
                                        <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 text-right" style="padding-top: 17px">
                                <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                <input type="button" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                <a class='btn btn-primary custom-button-color' href="../../Masters/Mas_Webfiles/frmMASMasters.aspx">Back</a>
                            </div>
                        </div>
                        <div class="row" style="padding-left: 30px;">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
        app.service('EntityService', function ($http, $q) {

            var deferred = $q.defer();

            this.GetParentEntity = function () {
                deferred = $q.defer();
                return $http.get('../../api/Entity/GetParentEntity')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            }
            //SAVE
            this.saveChildEntity = function (Insert) {
                deferred = $q.defer();
                return $http.post('../../api/Entity/Insert', Insert)
                  .then(function (response) {
                      deferred.resolve(response);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
            //Bind Grid
            this.BindGridChEntity = function () {
                deferred = $q.defer();
                return $http.get('../../api/Entity/BindGridEntity')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
            //UPDATE BY ID
            this.updateChildEntity = function (repeat) {
                deferred = $q.defer();
                return $http.post('../../api/Entity/UpdateChildEntity/', repeat)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
        });
        app.controller('EntityController', function ($scope, $q, EntityService, $timeout) {

            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.ChildEntity = {};
            $scope.ParentEntityList = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;

            //TO SAVE THE DATA
            $scope.Save = function () {

                if ($scope.IsInEdit) {
                    EntityService.updateChildEntity($scope.ChildEntity).then(function (repeat) {
                        var updatedobj = {};
                        angular.copy($scope.ChildEntity, updatedobj)
                        $scope.gridata.unshift(updatedobj);
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Updated Successfully";
                        EntityService.BindGridChEntity().then(function (data) {
                            $scope.gridata = data;
                            //$scope.createNewDatasource();
                            $scope.gridOptions.api.setRowData(data);
                        }, function (error) {
                        });
                        $scope.IsInEdit = false;
                        $scope.ClearData();
                        showNotification('success', 8, 'bottom-right', $scope.Success);

                        //setTimeout(function () {
                        //    $scope.$apply(function () {
                        //        showNotification('success', 8, 'bottom-right', $scope.Success);
                        //        $scope.ShowMessage = false;
                        //    });
                        //}, 700);
                    }, function (error) {
                    })
                }
                else {
                    $scope.ChildEntity.CH_ENTY_STA_ID = "1";
                    EntityService.saveChildEntity($scope.ChildEntity).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Inserted Successfully";
                        var savedobj = {};
                        angular.copy($scope.ChildEntity, savedobj)
                        $scope.gridata.unshift(savedobj);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                        $scope.ChildEntity = {};
                        $scope.ClearData();
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                        showNotification('error', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                    });
                }
            }

            //for GridView 
            var columnDefs = [
               { headerName: "Sub Function1 Code", field: "CH_ENTY_CODE", width: 150, cellClass: 'grid-align' },
               { headerName: "Sub Function1 Name", field: "CH_ENTY_NAME", width: 180, cellClass: 'grid-align' },
               { headerName: "Sub Function Name", field: "CH_ENTY_PRNT_CODE", width: 180, cellClass: 'grid-align' },
               { headerName: "Status", template: "{{ShowStatus(data.CH_ENTY_STA_ID)}}", width: 100, cellClass: 'grid-align' },
               { headerName: "Edit", width: 70, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true }
            ];

            // To display grid row data
            $scope.LoadData = function () {
                progress(0, 'Loading...', true);
                EntityService.GetParentEntity().then(function (data) {
                    $scope.ParentEntityList = data;
                    //

                    setTimeout(function () {
                        $('#CH_ENTY_PRNT_CODE').selectpicker('refresh');
                    }, 200);
                    EntityService.BindGridChEntity().then(function (data) {
                        $scope.gridata = data;
                        //$scope.createNewDatasource();
                        $scope.gridOptions.api.setRowData(data);

                        setTimeout(function () {
                            progress(0, 'Loading...', false);
                        }, 700);
                    }, function (error) {
                    });
                }, function (error) {
                });
            }

            $scope.onPageSizeChanged = function () {
                $scope.createNewDatasource();
            };

            //$scope.pageSize = '10';

            //$scope.createNewDatasource = function () {
            //    var dataSource = {
            //        pageSize: parseInt($scope.pageSize),
            //        getRows: function (params) {
            //            setTimeout(function () {
            //                var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
            //                var lastRow = -1;
            //                if ($scope.gridata.length <= params.endRow) {
            //                    lastRow = $scope.gridata.length;
            //                }
            //                params.successCallback(rowsThisPage, lastRow);
            //            }, 500);
            //        }
            //    };
            //    $scope.gridOptions.api.setDatasource(dataSource);
            //}

            $scope.gridOptions = {
                columnDefs: columnDefs,
                rowData: null,
                enableSorting: true,
                cellClass: 'grid-align',
                angularCompileRows: true,
                enableFilter: true,
                //enableColResize: true,
                suppressHorizontalScroll: true,
                enableCellSelection: false,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }
            };
            $timeout($scope.LoadData, 1000);

            $scope.EditFunction = function (data) {
                $scope.ChildEntity = {};
                angular.copy(data, $scope.ChildEntity);
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;
                setTimeout(function () {
                    $('#CH_ENTY_PRNT_CODE').selectpicker('refresh');
                }, 200);
            }

            $scope.ClearData = function () {
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
                $scope.ChildEntity = {};
                $scope.frmEntity.$submitted = false;
                setTimeout(function () {
                    $('#CH_ENTY_PRNT_CODE').selectpicker('refresh');
                }, 200);
            }

            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }
            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })


        });

    </script>
    <%--<script defer src="../../SMViews/Utility.js"></script>--%>
    <script defer src="../../SMViews/Utility.min.js"></script>
</body>
</html>
