Imports System.Data
Imports System.Data.SqlClient
Imports System.Security
Imports System.web.Security
Imports System.Configuration.ConfigurationManager
Imports System.IO
Imports System
Partial Class WorkSpace_SMS_Webfiles_frmERBSelectConsumableList
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    ' ------------------- Json ------------------------------------
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/WorkSpace/Reports/Source/themes/flick/jquery-ui.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/WorkSpace/Reports/Source/css/ui.jqgrid.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js/jquery.min.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js/i18n/grid.locale-en.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js/jquery.jqGrid.min.js")))
    '    'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js_source/TowerwiseUtilization.js")))
    '    '--------------------------------------------------------------
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))

    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = String.Empty Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            Try
                BindConsumAssets()
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Sub BindConsumAssets()
        objsubsonic.BindDataGrid(dgListGrid, "GETCONS_ASSETS")

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        For i As Integer = 0 To dgListGrid.Items.Count - 1
            Dim chkSelect As CheckBox = CType(dgListGrid.Items(i).FindControl("chkChoice"), CheckBox)
            Dim txtRemarks As TextBox = CType(dgListGrid.Items(i).FindControl("txtRemarks"), TextBox)
            Dim txtQty As TextBox = CType(dgListGrid.Items(i).FindControl("txtQty"), TextBox)
            If chkSelect.Checked = True Then

                Try
                    Dim it As Integer = CInt(txtQty.Text)
                Catch ex As Exception
                    lblmsg.Visible = True
                    lblmsg.Text = "Please Enter Quantity in Numbers Only"
                    Exit Sub
                End Try


                Dim param(5) As SqlParameter
                param(0) = New SqlParameter("@ReqId", SqlDbType.NVarChar, 200)
                param(0).Value = Request.QueryString("req_id")
                param(1) = New SqlParameter("@ConAstCode", SqlDbType.NVarChar, 200)
                param(1).Value = dgListGrid.Items(i).Cells(0).Text
                param(2) = New SqlParameter("@ConAstName", SqlDbType.NVarChar, 200)
                param(2).Value = dgListGrid.Items(i).Cells(1).Text
                param(3) = New SqlParameter("@Qty", SqlDbType.NVarChar, 200)
                param(3).Value = txtQty.Text
                param(4) = New SqlParameter("@Remark", SqlDbType.NVarChar, 200)
                param(4).Value = txtRemarks.Text
                param(5) = New SqlParameter("@ReqType", SqlDbType.NVarChar, 200)
                param(5).Value = radType.SelectedItem.Value
                
                objsubsonic.GetSubSonicExecute("ConferenceConsumbleAstList_INSERT", param)


            End If
        Next
    End Sub
End Class
