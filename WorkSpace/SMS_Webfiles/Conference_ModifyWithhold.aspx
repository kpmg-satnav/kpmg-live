﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="Conference_ModifyWithhold.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_Conference_ModifyWithhold" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>




    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script defer type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker('setStartDate', $('#txtFrmDate').val());
            $('#' + id).datepicker('setEndDate', $('#txtToDate').val());
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
            });

            //$('#' + id).datepicker("option", "minDate", -1);
            //$('#' + id).datepicker("option", "maxDate", -2);
        };
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Conference Type Master" ba-panel-class="with-scroll" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Modify Withold Request</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1"
                            ForeColor="Red" />
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>City<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCity"
                                        Display="None" ErrorMessage="Please Select City" InitialValue="--Select--">
                                    </asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                        AutoPostBack="True" Enabled="False">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Location<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="rfvSL" runat="server" ControlToValidate="ddlSelectLocation"
                                        Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--">
                                    </asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="ddlSelectLocation" AutoPostBack="true" runat="server"
                                        CssClass="form-control selectpicker with-search" data-live-search="true" Enabled="False">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Tower<span style="color: red;">*</span></label>
                                    <asp:CompareValidator ID="cmptwr" runat="server" ControlToValidate="ddlTower" Display="None"
                                        ErrorMessage="Please Select Tower " ValueToCompare="--Select--" Operator="NotEqual">cmptwr</asp:CompareValidator>
                                    <asp:DropDownList ID="ddlTower" TabIndex="5" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                        AutoPostBack="true" Enabled="False">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Floor<span style="color: red;">*</span></label>
                                    <asp:CompareValidator ID="cmpflr" runat="server" ControlToValidate="ddlFloor" Display="None"
                                        ErrorMessage="Please Select Floor " ValueToCompare="--Select--" Operator="NotEqual">cmpflr</asp:CompareValidator>
                                    <asp:DropDownList ID="ddlFloor" TabIndex="5" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                        AutoPostBack="True" Enabled="False">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Capacity<span style="color: red;">*</span></label>
                                    <asp:CompareValidator ID="cmpcap" runat="server" ControlToValidate="ddlCapacity" Display="None"
                                        ErrorMessage="Please Select Capacity " ValueToCompare="--Select--" Operator="NotEqual">cmpcap</asp:CompareValidator>

                                    <asp:DropDownList ID="ddlCapacity" TabIndex="5" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                        AutoPostBack="True" Enabled="False">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Conference Room<span style="color: red;">*</span></label>
                                    <asp:CompareValidator ID="cmpconf" runat="server" ControlToValidate="ddlConf" Display="None"
                                        ErrorMessage="Please Select Conference Room " ValueToCompare="--Select--" Operator="NotEqual">cmpconf</asp:CompareValidator>
                                    <asp:DropDownList ID="ddlConf" TabIndex="5" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                        AutoPostBack="True" Enabled="False">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>From Date<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="rfvfrmdate" runat="server" ControlToValidate="txtFrmDate"
                                        Display="None" ErrorMessage="Please Enter From Date ">
                                    </asp:RequiredFieldValidator>
                                    <div class='input-group date' id='fromdate'>
                                        <asp:TextBox ID="txtFrmDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>To Date<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtToDate"
                                        Display="None" ErrorMessage="Please Enter To Date ">
                                    </asp:RequiredFieldValidator>
                                    <div class='input-group date' id='todate'>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>From Time (HH):<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="rffromtime" runat="server" InitialValue="HH" ErrorMessage="Please Select From Time "
                                        Display="None" ControlToValidate="starttimehr" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="starttimehr" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                        <asp:ListItem Text="HH" Value="HH"></asp:ListItem>
                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>To Time (HH):<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="rftotime" runat="server" InitialValue="HH" ErrorMessage="Please Select To Time "
                                        Display="None" ControlToValidate="endtimehr" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <asp:DropDownList ID="endtimehr" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                        <asp:ListItem Text="HH" Value="HH"></asp:ListItem>
                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1" />
                                    <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" PostBackUrl="~/WorkSpace/SMS_Webfiles/Conference_UpdateWithhold.aspx" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
