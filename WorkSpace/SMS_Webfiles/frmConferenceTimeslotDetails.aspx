﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmConferenceTimeslotDetails.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmConferenceTimeslotDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"> </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

            <script language="javascript" type="text/javascript" src="../../Scripts/DateTimePicker.js"> </script>

            <div>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tbody>
                        <tr>
                            <td align="center" width="100%">
                                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" ForeColor="Black" Font-Underline="False" Width="86%" __designer:wfdid="w44">Conference Booking Availability
             <hr align="center" width="60%" /></asp:Label>
                                &nbsp;
                                <br />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="TABLE1" onclick="return TABLE1_onclick()" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                    <tbody>
                        <tr>
                            <td>
                                <img height="27" alt="" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                            <td class="tableHEADER" align="left" width="100%">&nbsp;<strong>Conference Booking Availability</strong></td>
                            <td style="width: 17px">
                                <img height="27" alt="" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                        </tr>
                        <tr>
                            <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                            <td align="left">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage" ForeColor="" ValidationGroup="Val1" __designer:wfdid="w45"></asp:ValidationSummary>
                                <br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage" __designer:wfdid="w46"></asp:Label><br />

                                <asp:GridView ID="gvspacereport" runat="server" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1" EmptyDataText="No Data Found" GridLines="None" TabIndex="5" Width="100%">
                                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" Wrap="False" />
                                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#594B9C" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#33276A" />
                                </asp:GridView>
                                <tr>
                                    <td background="../../Images/table_right_mid_bg.gif">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" />
                                    </td>
                                    <td background="../../images/table_bot_mid_bg.gif">
                                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" />
                                    </td>
                                    <td>
                                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" />
                                    </td>
                                </tr>
                        </tr>

                        </tr>
                    </tbody>
                </table>
            </div>
        </ContentTemplate>

        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnsubmit" EventName="Click"></asp:AsyncPostBackTrigger>
            <%--    <asp:PostBackTrigger ControlID="btnExport" />--%>
        </Triggers>

    </asp:UpdatePanel>
</asp:Content>

