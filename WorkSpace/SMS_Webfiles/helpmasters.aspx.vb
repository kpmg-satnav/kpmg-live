
Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.IO
Imports System.Web
Imports System.Web.Services
Imports System.Web.Script.Services
Partial Class WorkSpace_SMS_Webfiles_helpmasters
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack() Then
            ' To check designation mapping enabled or not
            Dim sp6 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
            sp6.Command.AddParameter("@TYPE ", 6, DbType.Int32)
            ViewState("DSN_MAPPING_STA") = sp6.ExecuteScalar()
            If ViewState("DSN_MAPPING_STA") = 1 Then
                Designation.Visible = True
            Else
                Designation.Visible = False
            End If

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_CHECK_SYS_PREFERENCES")
            sp.Command.AddParameter("@TYPE ", 22, DbType.Int32)
            ViewState("CHD_CAT_LOC_STA") = sp.ExecuteScalar()
            If ViewState("CHD_CAT_LOC_STA") = 1 Then
                ChildCatLoc.Visible = True
            Else
                ChildCatLoc.Visible = False
            End If

        End If
    End Sub

End Class
