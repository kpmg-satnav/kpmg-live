﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_addutilities
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            gvPropType.PageIndex = 0

            fillgrid()
            gvPropType.Visible = True
            btnmodify.Visible = False
            btnSubmit.Visible = True
        End If
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_UTILITY_GRID")
        sp.Command.AddParameter("@PN_PROPERTYTYPE", "", DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvPropType.DataSource = ds
        gvPropType.DataBind()

        For i As Integer = 0 To gvPropType.Rows.Count - 1
            Dim lblstatus As Label = CType(gvPropType.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "InActive"
            End If
        Next


    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_UTILITIES")
        sp.Command.AddParameter("@UTILITY", txtutility.Text, DbType.String)
        sp.Command.AddParameter("@STATUS", ddlStatus.SelectedItem.Value, DbType.Int32)
        sp.Command.AddParameter("@MODE", "ADD", DbType.String)
        sp.ExecuteScalar()
        lblMsg.Text = "Utility Added Succesfully"
        fillgrid()
        cleardata()
    End Sub

   
    Protected Sub gvPropType_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvPropType.PageIndexChanging
        gvPropType.PageIndex = e.NewPageIndex()
        fillgrid()
    End Sub

    Protected Sub gvPropType_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvPropType.RowCommand
        If e.CommandName = "EDIT" Then

            lblMsg.Text = ""
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblID As Label = DirectCast(gvPropType.Rows(rowIndex).FindControl("lblID"), Label)
            txtstore.text = lblID.Text
            GetRequestDetails(lblID.Text)
            btnSubmit.Visible = False
            btnModify.Visible = True
        End If
    End Sub
    Private Sub GetRequestDetails(ByVal id As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_UTILITY_DETAILS")
        sp.Command.AddParameter("@ID", id, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            txtutility.Text = ds.Tables(0).Rows(0).Item("PN_UTILITY")
            ddlStatus.ClearSelection()
            ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("PN_STA_ID")).Selected = True
        End If

    End Sub

    Protected Sub btnmodify_Click(sender As Object, e As EventArgs) Handles btnmodify.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_UTILITIES")
        sp.Command.AddParameter("@UTILITY", txtutility.Text, DbType.String)
        sp.Command.AddParameter("@STATUS", ddlStatus.SelectedItem.Value, DbType.Int32)
        sp.Command.AddParameter("@MODE", txtstore.Text, DbType.String)
        sp.ExecuteScalar()
        lblMsg.Text = "Utility Modified Succesfully"
        fillgrid()
        cleardata()
    End Sub
    Private Sub cleardata()
        txtutility.Text = ""
        ddlStatus.SelectedIndex = -1
    End Sub

    Protected Sub gvPropType_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvPropType.RowEditing

    End Sub
End Class
