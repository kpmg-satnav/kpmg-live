﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>


    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script defer src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script defer src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/colorpicker.min.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

    </script>

</head>
<body data-ng-controller="SpaceTypeController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Space Masters" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Space Type Master</h3>
                </div>
                 <div class="panel-body" style="padding-right: 50px;">
                    <form role="form" id="form1" name="frmSpaceType" data-valid-submit="Save()" novalidate>

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Space Type Code<span style="color: red;">*</span></label>
                                    <div class="col-md-12">
                                        <div data-ng-class="{'has-error': frmSpaceType.$submitted && frmSpaceType.SPC_Code.$invalid}" onmouseover="Tip('Enter code in alphabets and numbers')" onmouseout="UnTip()">
                                            <input id="SPC_Code" type="text" name="SPC_Code" data-ng-readonly="ActionStatus==1" maxlength="15" autofocus data-ng-pattern="/^[a-zA-Z0-9. ]*$/" data-ng-model="SpaceType.SPC_Code" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmSpaceType.$submitted && frmSpaceType.SPC_Code.$invalid" style="color: red">Please enter valid space type code </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Space Type Name<span style="color: red;">*</span></label>
                                    <div class="col-md-12">
                                        <div data-ng-class="{'has-error': frmSpaceType.$submitted && frmSpaceType.SPC_Name.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers')" onmouseout="UnTip()">
                                            <input id="SPC_Name" type="text" name="SPC_Name" maxlength="50" data-ng-model="SpaceType.SPC_Name" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmSpaceType.$submitted && frmSpaceType.SPC_Name.$invalid" style="color: red">Please enter valid space type name </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Select Color<span style="color: red;">*</span></label>
                                    <div class="col-md-12" data-ng-class="{'has-error': frmSpaceType.$submitted && frmSpaceType.SPC_COLOR.$invalid}" onmouseover="Tip('Please Select Color')" onmouseout="UnTip()">
                                        <div onmouseover="Tip('Select Color')"
                                            onmouseout="UnTip()">
                                            <input id="Text1" colorpicker type="text" name="SPC_COLOR" maxlength="50" data-ng-model="SpaceType.SPC_COLOR" class="form-control" required="required" />&nbsp;                                           
                                                <span class="error" data-ng-show="frmSpaceType.$submitted && frmSpaceType.SPC_COLOR.$invalid" style="color: red">Please select color </span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Remarks</label>
                                    <div class="col-md-12">
                                        <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                            <textarea id="Textarea1" style="height: 30%" runat="server" data-ng-model="SpaceType.SPC_REM" class="form-control" maxlength="500"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12" style="padding-left: 15px">
                                <div class="form-group">
                                    <div data-ng-show="ActionStatus==1">
                                        <label class="col-md-12 control-label">Status<span style="color: red;">*</span></label>
                                        <div class="col-md-12">
                                            <select id="Select1" name="SPC_Status_Id" data-ng-model="SpaceType.SPC_Status_Id" class="form-control">
                                                <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row text-right" style="padding-right: 15px; padding-top: 15px">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                    <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                    <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>

                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row" style="padding-left: 30px;">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer src="../../Scripts/bootstrap-colorpicker-module.min.js"></script>
    <script defer>

        var app = angular.module('QuickFMS', ["agGrid", "colorpicker.module"]);


        app.service("SpaceTypeService", function ($http, $q) {
            var deferred = $q.defer();
            this.getSpaceType = function () {
                deferred = $q.defer();
                return $http.get('../../api/SpaceType')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            //SAVE
            this.saveSpaceType = function (spcType) {
                deferred = $q.defer();
                return $http.post('../../api/SpaceType/Create', spcType)
                  .then(function (response) {
                      deferred.resolve(response);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            //UPDATE BY ID
            this.updateSpaceType = function (spcUpdate) {
                deferred = $q.defer();
                return $http.post('../../api/SpaceType/UpdateSpaceTypeData', spcUpdate)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
            //Bind Grid
            this.GetSpaceTypeBindGrid = function () {
                deferred = $q.defer();
                return $http.get('../../api/SpaceType/BindGridData')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
        });

        app.controller('SpaceTypeController', function ($scope, $q, SpaceTypeService, $http, $timeout) {
            $scope.StaDet = [{ Id: 1, Name: 'Active-General' }, { Id: 2, Name: 'Active-Space' }, { Id: 3, Name: 'Active-Asset' }, { Id: 4, Name: 'Active' }];
            $scope.SpaceType = {};
            $scope.repeatCategorylist = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;

            // To display grid row data
            $scope.LoadData = function () {
                progress(0, 'Loading...', true);
                SpaceTypeService.GetSpaceTypeBindGrid().then(function (data) {
                    $scope.gridata = data;
                    //$scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 700);
                }, function (error) {
                    console.log(error);
                });
            }
            //to Save the data
            $scope.Save = function () {
                if ($scope.IsInEdit) {
                    SpaceTypeService.updateSpaceType($scope.SpaceType).then(function (repeat) {

                        var updatedobj = {};
                        angular.copy($scope.SpaceType, updatedobj)
                        $scope.gridata.unshift(updatedobj);
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Updated Successfully";

                        SpaceTypeService.GetSpaceTypeBindGrid().then(function (data) {
                            $scope.gridata = data;
                            $scope.gridOptions.api.setRowData(data);
                        }, function (error) {
                            console.log(error);
                        });

                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        $scope.IsInEdit = false;
                        $scope.ClearData();
                        setTimeout(function () {
                            $scope.$apply(function () {
                                showNotification('success', 8, 'bottom-right', $scope.Success);
                            });
                        }, 700);
                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.SpaceType.SPC_Status_Id = "1";
                    SpaceTypeService.saveSpaceType($scope.SpaceType).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Inserted Successfully";
                        var savedobj = {};
                        angular.copy($scope.SpaceType, savedobj)
                        $scope.gridata.unshift(savedobj);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                        $scope.SpaceType = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                        showNotification('error', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                        console.log(error);
                    });
                }
            }
            //for GridView
            var columnDefs = [
               //{ headerName: "Space Type", template: "{{data.SPC_Code}} / {{data.SPC_Name}}", width: 200, cellClass: 'grid-align' },
               { headerName: "Space Type Code", field: "SPC_Code", width: 25, cellClass: 'grid-align' },
               { headerName: "Space Type Name", field: "SPC_Name", width: 30, cellClass: 'grid-align' },
               {
                   headerName: "Color", field: "SPC_COLOR", width: 10, cellClass: 'grid-align', suppressMenu: true, cellClass: 'grid-align', cellStyle: function (params) {
                       return { color: params.value, 'background-color': params.value };

                   }
               },
                { headerName: "Status", template: "{{ShowStatus(data.SPC_Status_Id)}}", width: 20, cellClass: 'grid-align', suppressMenu: true },
               { headerName: "Edit", width: 5, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true }
            ];


            //$scope.pageSize = '10';

            //$scope.createNewDatasource = function () {
            //    var dataSource = {
            //        pageSize: parseInt($scope.pageSize),
            //        getRows: function (params) {
            //            setTimeout(function () {
            //                var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
            //                var lastRow = -1;
            //                if ($scope.gridata.length <= params.endRow) {
            //                    lastRow = $scope.gridata.length;
            //                }
            //                params.successCallback(rowsThisPage, lastRow);
            //            }, 500);
            //        }
            //    };
            //    $scope.gridOptions.api.setDatasource(dataSource);
            //}

            $scope.gridOptions = {
                columnDefs: columnDefs,
                rowData: null,
                enableSorting: true,
                cellClass: 'grid-align',
                angularCompileRows: true,
                enableFilter: true,
                suppressMovable: false,
                suppressHorizontalScroll: true,
                enableCellSelection: false,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }
            };
            $timeout($scope.LoadData, 1000);
            $scope.EditFunction = function (data) {
                $scope.SpaceType = {};
                angular.copy(data, $scope.SpaceType);
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;
                $scope.Show = 1;
            }
            $scope.ClearData = function () {
                $scope.SpaceType = {};
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
                $scope.Show = 0;
                $scope.frmSpaceType.$submitted = false;
            }
            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 1 ? 0 : [value == 2 ? 1 : [value == 3 ? 2 : 3]]].Name;
            }
            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })

        });

    </script>
    <%--<script defer src="../../SMViews/Utility.js"></script>--%>
    <script defer src="../../SMViews/Utility.min.js"></script>
</body>
</html>
