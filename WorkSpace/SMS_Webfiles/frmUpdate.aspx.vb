
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class WorkSpace_SMS_Webfiles_frmUpdate
    Inherits System.Web.UI.Page
    Dim strSQL As String
    Dim varUID As String
    Dim trole As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(AppSettings("logout"))
            End If
            visibleTF(1)
            BindData()
            visibleTF(0)
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Private Sub BindData()

        varUID = Session("uid")

        Dim objdata As SqlDataReader

        Label2.Visible = False

        strSQL = "SELECT URL_USR_ID,ROL_ACRONYM,URL_SCOPEMAP_ID " & _
          "FROM  " & Session("TENANT") & "."  & "USER_ROLE, " & Session("TENANT") & "."  & "ROLE " & _
          "WHERE URL_ROL_ID=ROL_ID AND ROL_ACRONYM IN ('OSSI','OSSA','GADMIIN','All') AND URL_USR_ID = '" & Session("uid") & "'"

        objdata = SqlHelper.ExecuteReader(CommandType.Text, strSQL)

        Dim locs As String
        Dim rol As String
        Dim userid As String
        While objdata.Read
            locs = locs & objdata("URL_SCOPEMAP_ID").ToString & ","
            userid = objdata("URL_USR_ID").ToString
            rol = objdata("ROL_ACRONYM").ToString
        End While

        If rol <> "GADMIIN" Then
            strSQL = "SELECT DISTINCT SRN_REQ_ID,SRN_REQ_DT,SRN_REM," & _
                     " (SELECT DISTINCT  LCM_NAME from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_ONE ) BDG_DESC1," & _
                     " (select DISTINCT STA_TITLE from  " & Session("TENANT") & "."  & "Status where STA_id=SRN_sta_id) as STA_NAME, " & _
                     " (SELECT DISTINCT LCM_NCME from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_TWO ) BDG_DESC2,srn_type_id FROM " & _
                     "  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "BUILDING PRM WHERE  SRN_AUR_ID ='" & varUID & "' " & _
                     " and SRN_STA_ID in (5,10) order by SRN_REQ_DT DESC"
        End If


        If rol <> "OSSA" Then
            strSQL = "SELECT DISTINCT SRN_REQ_ID,SRN_REQ_DT,SRN_REM," & _
                     " (SELECT DISTINCT  LCM_NAME from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_ONE ) BDG_DESC1," & _
                     " (select DISTINCT  STA_TITLE from  " & Session("TENANT") & "."  & "Status where STA_id=SRN_sta_id) as STA_NAME, " & _
                     " (SELECT  DISTINCT LCM_NAME from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_TWO ) BDG_DESC2,srn_type_id FROM " & _
                     "  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "BUILDING PRM WHERE  SRN_AUR_ID ='" & varUID & "' " & _
                     " and SRN_STA_ID in (5,10) order by SRN_REQ_DT DESC"
        Else

            strSQL = "SELECT DISTINCT SRN_REQ_ID,SRN_REQ_DT,SRN_REM," & _
                     " (SELECT DISTINCT LCM_NAME from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_ONE ) BDG_DESC1," & _
                     " (select DISTINCT STA_TITLE from  " & Session("TENANT") & "."  & "Status where STA_id=SRN_sta_id) as STA_NAME, " & _
                     " (SELECT  DISTINCT LCM_NAME from  " & Session("TENANT") & "."  & "LOCATION WHERE LCM_CODE = SRM.SRN_BDG_TWO ) BDG_DESC2,srn_type_id " & _
                     " FROM  " & AppSettings("APPDB") & "SMS_SPACEREQUISITION SRM, " & Session("TENANT") & "."  & "BUILDING PRM WHERE   SRN_STA_ID in (5,10) " & _
                     "  order by SRN_REQ_DT DESC"
        End If

        Dim capDS As New DataSet

        BindGridSet(strSQL, grdReqList)

        If grdReqList.Rows.Count > 0 Then
            grdReqList.Visible = True
            Label2.Visible = False
            Dim i As Integer
            For i = 0 To grdReqList.Rows.Count - 1
                If grdReqList.Rows(i).Cells(7).Text = "1" Then
                    grdReqList.Rows(i).Cells(6).Text = "Employee(s)"
                ElseIf grdReqList.Rows(i).Cells(7).Text = "2" Then
                    grdReqList.Rows(i).Cells(6).Text = "Non Employee(s)"
                Else
                    grdReqList.Rows(i).Cells(6).Text = "Na"
                End If
            Next
        Else
            grdReqList.Visible = False
            Label2.Visible = True
        End If
    End Sub

    Protected Sub grdReqList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdReqList.PageIndexChanging
        Try
            grdReqList.PageIndex = e.NewPageIndex
            BindData()
        Catch ex As SqlException
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", ex)
            'fn_ErrorLog(1, ex.Message, "Sql Exception", ex.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        Catch em As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "View Requisitions", "Load", em)
            'fn_ErrorLog(1, em.Message, "General Exception", em.StackTrace, Me.Form.TemplateControl.AppRelativeVirtualPath, Session("UID"))
        End Try
    End Sub
    Private Sub visibleTF(ByVal status As Integer)
        If status = 1 Then
            grdReqList.Columns(2).Visible = True
            grdReqList.Columns(4).Visible = True
            grdReqList.Columns(7).Visible = True
        Else
            grdReqList.Columns(2).Visible = False
            grdReqList.Columns(4).Visible = False
            grdReqList.Columns(7).Visible = False
        End If


    End Sub

End Class
