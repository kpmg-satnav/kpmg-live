﻿<%@ Page Title="" Language="VB" AutoEventWireup="false"
    CodeFile="modifyconference.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_modifyconference" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script defer type="text/javascript">
        var sta =<%=Session("STA")%>;

        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker('setStartDate', $('#txtFdate').val());
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            }).on('change', function () {
                $('.datepicker').hide();
            });
        };

        function refreshSelectpicker() {
            $("#<%=ddlCity.ClientID%>").selectpicker();
            $("#<%=ddlSelectLocation.ClientID%>").selectpicker();
            $("#<%=ddlTower.ClientID%>").selectpicker();
            $("#<%=ddlFloor.ClientID%>").selectpicker();
            $("#<%=ddlCapacity.ClientID%>").selectpicker();
            $("#<%=ddlConf.ClientID%>").selectpicker();
            $("#<%=starttimehr.ClientID%>").selectpicker();
            $("#<%=endtimehr.ClientID%>").selectpicker();
        }
        refreshSelectpicker();

        //function setup(id) {
        //    $("#datepicker").datepicker("destroy");

        //    if (id == "Todate") {
        //       // $("#Todate").datepicker("destroy");

        //        var fromdate = $('#txtFdate').val();
        //        //    var tt = e.target.value
        //    //   //-- var f = $('#txtTdate').val();
        //    //    console.log(tt);
        //        if (sta == 0) {
        //            var fromdate = $('#txtFdate').val();
        //            var date = new Date(fromdate);
        //            var newdate = new Date(date);

        //            newdate.setDate(newdate.getDate() + 14);
        //            var dd = fixDigit(newdate.getDate());
        //            var mm = fixDigit(newdate.getMonth() + 1);
        //            var y = newdate.getFullYear();
        //            var fdd = fixDigit(date.getDate());
        //            var fmm = fixDigit(date.getMonth()+1);
        //            var fyy = date.getFullYear();
        //            var fromdate1 = fmm + '/' + fdd + '/' + fyy;
        //            var todate = mm + '/' + dd + '/' + y;
        //            console.log(fromdate1);
        //            console.log(todate);

        //            //first day which can be selected in dt2 is selected date in dt1

        //            $('#' + id).datepicker();
        //            $('#' + id).datepicker('remove');

        //            $('#' + id).datepicker({
        //                format: "mm/dd/yyyy",
        //                startDate: new Date(fromdate1),
        //                endDate: new Date(todate)                    

        //                //maxDate: '10/19/2019',

        //            })
        //        }

        //        else {
        //            $('#' + id).datepicker({
        //                format: 'mm/dd/yyyy',
        //                autoclose: true,

        //            })
        //        }
        //    }
        //    else {
        //        $('#' + id).datepicker({
        //            format: 'mm/dd/yyyy',
        //            autoclose: true,

        //        })
        //    }


        //    function fixDigit(val) {
        //        return val.toString().length === 1 ? "0" + val : val;
        //    }
        //    $("#datepicker").datepicker("refresh");
        //        };
    </script>
</head>
<body>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Conference Type Master" ba-panel-class="with-scroll" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Modify Request</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1"
                            ForeColor="Red" />
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>City<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="--Select--" ErrorMessage="Please Select City" Display="None" ControlToValidate="ddlCity" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" Enabled="False">
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <label>Location<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvSL" runat="server" InitialValue="--Select--" ErrorMessage="Please Select Location" Display="None" ControlToValidate="ddlSelectLocation" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                            <asp:DropDownList ID="ddlSelectLocation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true" Enabled="False">
                                            </asp:DropDownList>


                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <label>Tower<span style="color: red;">*</span></label>
                                            <asp:CompareValidator ID="cmpflr" runat="server" ErrorMessage="Please Select Tower " Display="None" ControlToValidate="ddlTower" Operator="NotEqual" ValueToCompare="--Select--" ValidationGroup="Val1">cmpflr</asp:CompareValidator>

                                            <asp:DropDownList ID="ddlTower" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true" Enabled="False">
                                            </asp:DropDownList>


                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <label>Floor<span style="color: red;">*</span></label>
                                            <asp:CompareValidator ID="cmptwr" runat="server" ErrorMessage="Please Select Floor " Display="None" ControlToValidate="ddlFloor" Operator="NotEqual" ValueToCompare="--Select--" ValidationGroup="Val1">cmptwr</asp:CompareValidator>

                                            <asp:DropDownList ID="ddlFloor" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" Enabled="False">
                                            </asp:DropDownList>


                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <label>Capacity<span style="color: red;">*</span></label>

                                            <asp:CompareValidator ID="cmpcap" runat="server" ControlToValidate="ddlCapacity" Display="None"
                                                ErrorMessage="Please Select Capacity " ValueToCompare="--Select--" Operator="NotEqual">cmpcap</asp:CompareValidator>

                                            <asp:DropDownList ID="ddlCapacity" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True" Enabled="False">
                                            </asp:DropDownList>


                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <label>Reservation Room<span style="color: red;">*</span></label>

                                            <asp:CompareValidator ID="cmpconf" runat="server" ControlToValidate="ddlConf" Display="None"
                                                ErrorMessage="Please Select Reservation Room " ValueToCompare="--Select--" Operator="NotEqual">cmpconf</asp:CompareValidator>

                                            <asp:DropDownList ID="ddlConf" TabIndex="5" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True" Enabled="False">
                                            </asp:DropDownList>


                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row" style="padding-left: 350px">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <label class="btn btn-default pull-right">
                                            <asp:RadioButton value="0" runat="server" name="rbActions" ID="rbActions" GroupName="rbActions" AutoPostBack="true" Checked="true" />
                                            OneTime
                                        </label>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <label class="btn btn-default" style="margin-left: 25px">
                                            <asp:RadioButton value="1" runat="server" name="rbActions" ID="rbActionsrecurring" GroupName="rbActions" AutoPostBack="true" />
                                            Recurring
                                        </label>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>From Date<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select From Date" Display="None" ControlToValidate="txtFdate" __designer:wfdid="w49">
                                            </asp:RequiredFieldValidator>
                                           <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFdate" ValidationExpression="^(([1-9])|(0[1-9])|(1[0-2]))\/((0[1-9])|([1-31]))\/((19|20)\d\d)$"
                                                ErrorMessage="Not Valid Date" ValidationGroup="Val1" Display="None"></asp:RegularExpressionValidator>--%>

                                            <div class='input-group date' id='Fromdate' runat="server">
                                                <asp:TextBox ID="txtFdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span id="frmdate" runat="server" class="fa fa-calendar" onclick="setup('Fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <div id="Tr1" runat="server" visible="false">
                                                <label>To Date<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select To Date" Display="None" ControlToValidate="txtTdate" __designer:wfdid="w49">
                                                </asp:RequiredFieldValidator>
                                             <%--   <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtTdate" ValidationExpression="^(([1-9])|(0[1-9])|(1[0-2]))\/((0[1-9])|([1-31]))\/((19|20)\d\d)$"
                                                    ErrorMessage="Not Valid Date" ValidationGroup="Val1" Display="None"></asp:RegularExpressionValidator>--%>

                                                <div class='input-group date' id='Todate'>
                                                    <asp:TextBox ID="txtTdate" OnTextChanged="txtTdate_TextChanged" AutoPostBack="true" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span id="frmtodate" runat="server" class="fa fa-calendar" onclick="setup('Todate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <label>From Time (HH):<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="HH" ErrorMessage="Please Enter From Time " Display="None" ControlToValidate="starttimehr" ValidationGroup="Val1"></asp:RequiredFieldValidator>


                                            <asp:DropDownList ID="starttimehr" runat="server" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Text="HH" Value="HH"></asp:ListItem>
                                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                            </asp:DropDownList>


                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <label>To Time (HH):<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" InitialValue="HH" ErrorMessage="Please Enter To Time " Display="None" ControlToValidate="endtimehr" ValidationGroup="Val1"></asp:RequiredFieldValidator>


                                            <asp:DropDownList ID="endtimehr" runat="server" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Text="HH" Value="HH"></asp:ListItem>
                                                <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                                <%--<asp:ListItem Value="24">24</asp:ListItem>--%>
                                            </asp:DropDownList>


                                        </div>
                                    </div>
                                </div>

                                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" Visible="False"
                                    BorderWidth="0px"></asp:TextBox>
                                <div id="AttendeesMail" runat="server" class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>External Attendees <a title="Please Enter Mail IDs With Comma (,) Separation">Help</a></label>
                                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtAttendees" ValidationGroup="Val1"
                                                Display="None" ErrorMessage="Please Enter Valid Email ID" ValidationExpression="^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[,]{0,1}\s*)+$">
                                            </asp:RegularExpressionValidator>
                                            <asp:TextBox ID="txtAttendees" Height="50px" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" ToolTip="Please Enter Mail IDs With Comma (,) Separtion"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">

                                        <div class="form-group">
                                            <label>Select Internal Attendees  </label>
                                            <asp:TextBox ID="Textinternal" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="Textinternal_SelectedIndexChanged" data-live-search="true"></asp:TextBox>
                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="Textinternal" MinimumPrefixLength="2" EnableCaching="false"
                                                CompletionSetCount="10" CompletionInterval="10" ServiceMethod="GetintAttendees" ServicePath="~/Autocompletetype.asmx">
                                            </asp:AutoCompleteExtender>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>
                                                Internal Attendees
                                                            <asp:LinkButton ID="btnLogout" Text="Remove" OnClick="Remove_Click" runat="server" /></label>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtInternal" ValidationGroup="Val1"
                                                Display="None" ErrorMessage="Please Enter Valid Email ID" ValidationExpression="^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[,]{0,1}\s*)+$">
                                            </asp:RegularExpressionValidator>
                                            <asp:ListBox ID="txtInternal" runat="server" SelectionMode="Multiple" Height="50px" CssClass="form-control" Rows="5"></asp:ListBox>
                                        </div>

                                    </div>

                                </div>
                                <div id="InternalAttendees" runat="server" class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Description<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription"
                                                Display="None" ErrorMessage="Please Enter Description" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtDescription" Height="50px" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">

                                            <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" ValidationGroup="Val1"></asp:Button>
                                            <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary custom-button-color" Text="Cancel" CausesValidation="False"></asp:Button>
                                            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" PostBackUrl="~/WorkSpace/SMS_Webfiles/editconference.aspx" CausesValidation="False"></asp:Button>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="row">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblconfalert" ForeColor="RED" class="col-md-12 control-label" runat="server" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <asp:GridView ID="gvItem" runat="server" AutoGenerateColumns="False" AllowPaging="true" PageSize="10" EmptyDataText="No Records Found."
                                            CssClass="table table-condensed table-bordered table-hover table-striped" Visible="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Booked From Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBookedfromdate" runat="server" CssClass="bodyText" Text='<%#Eval("BOOKED_FROM_DATE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Booked To Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBookedtodate" runat="server" CssClass="bodyText" Text='<%#Eval("BOOKED_TO_DATE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Time">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblfromtime" runat="server" CssClass="bodyText" Text='<%#((Eval("FTIME")))%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Time">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltotime" runat="server" CssClass="bodyText" Text='<%#((Eval("TTIME")))%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstatus" runat="server" CssClass="bodyText" Text='<%#Eval("ConfStatus")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
