Imports System.Data.SqlClient
Imports System.Data
Imports System.io
Imports ControlFreak
Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Web.Configuration
Imports clsSubSonicCommonFunctions
Partial Class WorkSpace_SMS_Webfiles_frmLocationReport_date
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@dummy", 1, DbType.Int32)
            param(0).Value = 1
            objsubsonic.Binddropdown(ddllocation, "GET_BDG_LOCATION", "LCM_NAME", "lcm_code", param)
            btnexport.Visible = False
            txtFdate.Attributes.Add("onClick", "displayDatePicker('" + txtFdate.ClientID + "')")
            txtFdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtTdate.Attributes.Add("onClick", "displayDatePicker('" + txtTdate.ClientID + "')")
            txtTdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        End If

    End Sub
   
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim fdate As Date
        Dim tdate As Date
        fdate = CDate(txtFdate.Text)
        tdate = CDate(txtTdate.Text)
        If fdate > tdate Then
            lblMsg.Text = "To date should be more than From Date"
        Else
            lblMsg.Text = ""
            BindGrid()
            If gvlocation.Rows.Count > 0 Then
                btnexport.Visible = True
            Else
                btnexport.Visible = False
            End If
        End If
    End Sub
    Private Sub BindGrid()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
        param(0).Value = ddllocation.SelectedItem.Value
        param(1) = New SqlParameter("@FROM_DATE", SqlDbType.Date)
        param(1).Value = txtFdate.Text
        param(2) = New SqlParameter("@TO_DATE", SqlDbType.Date)
        param(2).Value = txtTdate.Text
        Dim ds As DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_USP_LOCATION_TABLE", param)
        gvlocation.DataSource = ds
        gvlocation.DataBind()
    End Sub

    Protected Sub gvlocation_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvlocation.PageIndexChanging
        gvlocation.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
#Region "Export"
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < Control.Controls.Count)
            Dim current As Control = Control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                Control.Controls.Remove(current)
                Control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                Control.Controls.Remove(current)
                Control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                Control.Controls.Remove(current)
                Control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                Control.Controls.Remove(current)
                Control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                Control.Controls.Remove(current)
                Control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                Control.Controls.Remove(current)
                Control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
#End Region

    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        Exporttoexcel()
    End Sub
    Private Sub Exporttoexcel()
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@LOCATION", SqlDbType.NVarChar, 200)
        param(0).Value = ddllocation.SelectedItem.Value
        param(1) = New SqlParameter("@FROM_DATE", SqlDbType.Date)
        param(1).Value = txtFdate.Text
        param(2) = New SqlParameter("@TO_DATE", SqlDbType.Date)
        param(2).Value = txtTdate.Text
        Dim ds As DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_USP_LOCATION_TABLE", param)
        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        Export("Location_Wise_Report" & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") & ".xls", gv)
    End Sub
End Class
