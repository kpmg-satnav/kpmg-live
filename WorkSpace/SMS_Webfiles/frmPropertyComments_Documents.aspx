<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmPropertyComments_Documents.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmPropertyComments_Documents"
    Title="Property Comments & Document" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <contenttemplate>
  
  
  
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Property Comments & Document
                </asp:Label>
                <hr align="center" width="60%" />
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Property Comments & Document </strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="left">
                <table width="100%" style="vertical-align: top;" cellpadding="3" cellspacing="3"
                    align="center" border="0">
                    <tr>
                        <td align="left" valign="top" colspan="2">
                            <asp:Label ID="lblmsg" runat="server" CssClass="error"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="Label3" runat="server" Text="Property Type"></asp:Label>
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlPropertyType" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="Label4" runat="server" Text="Property"></asp:Label>
                        </td>
                        <td align="left" valign="top">
                            <asp:DropDownList ID="ddlProperty" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="2">
                            <fieldset>
                                <legend>Property Remarks</legend>
                                <asp:GridView ID="gvLevel" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                    AllowPaging="True" PageSize="5" EmptyDataText="No Remarks Found" Width="100%">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Comments & Documents">
                                            <ItemTemplate>
                                            <fieldset>
                                                <legend> Level 1 Approval Coments & Documents  </legend>
                                                <asp:Label ID="lblAUR_ID" visible="false" Text='<%# Eval("AUR_ID") %>' runat="server"></asp:Label>
                                                      <asp:GridView ID="gvRemarks" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                               onRowDeleting="gvRemarks_RowDeleting"     OnRowCommand="gvRemarks_RowCommand1"   OnPageIndexChanging="gvRemarks_PageIndexChanging"   AllowPaging="True" PageSize="5" EmptyDataText="No Remarks Found" Width="100%">
                                                        <PagerSettings Mode="NumericFirstLast" />
                                                        <Columns>
                                                        <asp:TemplateField HeaderText="Commented by">
                                                                <ItemTemplate>
                                                                 <asp:Label ID="lblID" Text='<%# Eval("ID") %>' visible="false" runat="server"></asp:Label>
                                                                    <asp:Label ID="lblAUR_FIRST_NAME" Text='<%# Eval("AUR_FIRST_NAME") %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Comments">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCOMMENTS" Text='<%# Eval("COMMENTS") %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Commented Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label2" Text='<%# Eval("COMMENT_DATE") %>' runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Uploaded Document">
                                                                <ItemTemplate>
                                                                  <a href='<%=Page.ResolveUrl("~/UploadFiles") %>/<%# Eval("PROPERTY_DOC") %>' target="_blank" ><%# Eval("PROPERTY_DOC") %></a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" id="lnkDelete" text="Delete" CommandArgument='<%# Eval("id") %>' commandname="Delete"      ></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                    </fieldset>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            Remarks 
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Height="150" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            Document
                        </td>
                        <td align="left" valign="top">
                           
                             <input name="fpBrowseDoc" type="file" id="xlfile" runat="server" class="button">
                            </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                        </td>
                        <td align="left" valign="top">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="clsButton" />
                        </td>
                    </tr>
                </table>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
    </contenttemplate>
        <triggers>
        <asp:PostBackTrigger ControlID="btnSubmit" />
    </triggers>
    </asp:UpdatePanel>
</asp:Content>
