﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="IntraMovementAssets.aspx.vb" Inherits="FAM_FAM_Webfiles_IntraMovementAssets" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script defer type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Asset Movements
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">

                            <%--<div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Movement Assets</legend>
                                        <asp:GridView ID="gvgriditems" runat="server" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="false"
                                            CssClass="table table-condensed table-bordered table-hover table-striped" EmptyDataText="No Records Found">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Requisition">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblasset" runat="server" Text='<%#Eval("MMR_REQ_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Raised by">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblreqraised" runat="server" Text='<%#Eval("MMR_RAISEDBY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldate" runat="server" Text='<%#Eval("MMR_MVMT_DATE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtn5" runat="server" Text="Asset Transfer Note" CommandName="Transfer"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </fieldset>
                                </div>
                            </div>--%>

                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Inter Movement Assets</legend>
                                        <asp:GridView ID="gvintra" runat="server" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="false"
                                            CssClass="table table-condensed table-bordered table-hover table-striped" EmptyDataText="No Inter Movement Asset Found">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Requisition">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblasset" runat="server" Text='<%#Eval("MMR_REQ_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Raised by">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblreqraised" runat="server" Text='<%#Eval("MMR_RAISEDBY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldate" runat="server" Text='<%#Eval("MMR_MVMT_DATE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtn5" runat="server" Text="Asset Transfer Note" CommandName="Transfer"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </fieldset>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Disposed Assets</legend>
                                        <asp:GridView ID="gvdispose" runat="server" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="false"
                                            CssClass="table table-condensed table-bordered table-hover table-striped" EmptyDataText="No Disposed Asset Found">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Requisition">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblasset" runat="server" Text='<%#Eval("DREQ_TS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Asset">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblasset1" runat="server" Text='<%#Eval("AST_CODE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Raised by">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblraisedby" runat="server" Text='<%#Eval("DREQ_REQUESTED_BY") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkbtn5" runat="server" Text="Asset Disposal Note" CommandName="Dispose"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </fieldset>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
