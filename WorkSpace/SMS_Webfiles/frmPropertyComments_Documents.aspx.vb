Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmPropertyComments_Documents
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If

            Try
                BindPropType()

            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub BindPropType()

        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select Property Type--", "--Select Property Type--"))

    End Sub

    Private Sub BindRemarks()
 
        ObjSubSonic.BindGridView(gvLevel, "GETLEVELS")

    End Sub

   

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If ddlPropertyType.SelectedItem.Value = "--Select Property Type--" Then
            lblmsg.Text = "Please select Property Type."
            Exit Sub
        End If
        If ddlProperty.SelectedItem.Value = "--Select--" Then
            lblmsg.Text = "Please select Property."
            Exit Sub
        End If

        AddRemarks()
        BindRemarks()


    End Sub

    Private Sub AddRemarks()

        Dim UploadFilesName As String
        UploadFilesName = UploadFiles()

        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@PROPERTY_ID", SqlDbType.NVarChar, 200)
        param(0).Value = ddlProperty.SelectedItem.Value
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("uid")
        param(2) = New SqlParameter("@COMMENTS", SqlDbType.NVarChar, 200)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@FilesName", SqlDbType.NVarChar, 200)
        param(3).Value = UploadFilesName
        ObjSubSonic.GetSubSonicExecuteScalar("ADD_PROPERTY_COMMENTS_AUR_ID", param)
       
    End Sub
 
    Protected Sub ddlPropertyType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPropertyType.SelectedIndexChanged
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@PROPERTYTYPE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlPropertyType.SelectedItem.Text
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("uid")
        ObjSubSonic.Binddropdown(ddlProperty, "GET_PROPERTIES", "PN_NAME", "BDG_ID", param)
    End Sub

    Protected Sub ddlProperty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProperty.SelectedIndexChanged
        BindRemarks()
    End Sub

    Private Function UploadFiles() As String
        Dim strext As String
        Dim strFileName As String = ""
        Dim Strpath As String = ""
        Dim str As String = ""
        If xlfile.PostedFile.FileName <> "" Then
            strext = xlfile.PostedFile.FileName
            strFileName = xlfile.PostedFile.FileName
            strext = Right(strext, strext.Length - (strext.Length - 4))
            Strpath = Server.MapPath("~/UploadFiles/")
            str = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & strext
            Strpath &= str
            xlfile.PostedFile.SaveAs(Strpath)
        End If
        Return str
    End Function 

    Protected Sub gvLevel_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLevel.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblAUR_ID As Label = TryCast(e.Row.FindControl("lblAUR_ID"), Label)
            Dim gvRemarks As GridView = TryCast(e.Row.FindControl("gvRemarks"), GridView)
            BindInnerGridView(gvRemarks, lblAUR_ID.Text)
        End If
    End Sub

    Private Sub BindInnerGridView(ByVal gv As GridView, ByVal aur_id As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@PROPERTY", SqlDbType.NVarChar, 200)
        param(0).Value = ddlProperty.SelectedItem.Value
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = aur_id
        ObjSubSonic.BindGridView(gv, "GET_PROPERTY_COMMENTS_AUR_ID", param)

        For i As Integer = 0 To gv.Rows.Count - 1
            Dim lnkDelete As LinkButton = CType(gv.Rows(i).FindControl("lnkDelete"), LinkButton)
            If aur_id = Session("uid") Then
                lnkDelete.Visible = True
            Else
                lnkDelete.Visible = False
            End If
        Next

    End Sub
 
    Protected Sub gvRemarks_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        Dim gvwChild As GridView = DirectCast(sender, GridView)
        Dim gvRowParent As GridViewRow = TryCast(DirectCast(sender, GridView).Parent.Parent, GridViewRow)
        gvwChild.PageIndex = e.NewPageIndex
        Dim lblaurid As Label = CType(gvRowParent.FindControl("lblAUR_ID"), Label)
        BindInnerGridView(gvwChild, lblaurid.Text)
    End Sub
 
    Protected Sub gvRemarks_RowCommand1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        If e.CommandName = "Delete" Then
            Dim gvwChild As GridView = DirectCast(sender, GridView)
             
            Dim gvRowParent As GridViewRow = TryCast(DirectCast(sender, GridView).Parent.Parent, GridViewRow)

            Dim lblaurid As Label = CType(gvRowParent.FindControl("lblAUR_ID"), Label)
            Dim lblID As Label = CType(gvwChild.FindControl("lblID"), Label)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@COM_ID", SqlDbType.NVarChar, 200)
            param(0).Value = CInt(e.CommandArgument.ToString())

            ObjSubSonic.GetSubSonicExecuteScalar("DELETE_COMMENT", param)
            BindInnerGridView(gvwChild, lblaurid.Text)
 
        End If
    End Sub

    Protected Sub gvRemarks_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)

    End Sub
End Class
