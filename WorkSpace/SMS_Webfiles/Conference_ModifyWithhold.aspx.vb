﻿Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports clsMasters
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Imports Amantra.VerticalDTON
Imports Amantra.VerticalDALN
Imports Amantra.VerticalBLLN
Imports System.Collections.Generic
Imports Amantra.SmartReader
Imports clsSubSonicCommonFunctions
Imports cls_OLEDB_postgresSQL

Partial Class WorkSpace_SMS_Webfiles_Conference_ModifyWithhold
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Addusers As New AddUsers
    Dim REQID As String
    Dim RIDDS As String
    Dim strRedirect As String = String.Empty
    Dim verBll As New VerticalBLL()
    Dim objMaster As New clsMasters()
    Dim rDTO As New VerticalDTO()
    Dim objVerticalDAL As VerticalDAL
    Dim strStatus As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtFrmDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
        lblMsg.Text = String.Empty
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("logout"))
        End If
        If Not Page.IsPostBack Then
            LoadCity()
            Dim id As String = Request.QueryString("id")
            Binddetails(id)
            BindConfTimes()
        End If
    End Sub

    Public Sub BindConfTimes()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONF_TIMES")
        sp.Command.AddParameter("@DATE", txtFrmDate.Text, DbType.Date)
        Dim ds As DataSet
        ds = sp.GetDataSet
        For Each item As ListItem In starttimehr.Items
            item.Attributes.Add("disabled", "disabled")
        Next
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            starttimehr.Items.FindByValue(ds.Tables(0).Rows(i).Item("Timings")).Attributes.Remove("disabled")
        Next
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CONF_TIMES")
        sp1.Command.AddParameter("@DATE", txtToDate.Text, DbType.Date)
        Dim ds1 As DataSet
        ds1 = sp1.GetDataSet
        For Each item As ListItem In endtimehr.Items
            item.Attributes.Add("disabled", "disabled")
        Next
        For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
            endtimehr.Items.FindByValue(ds1.Tables(0).Rows(i).Item("Timings")).Attributes.Remove("disabled")
        Next
    End Sub

    Private Sub Binddetails(ByVal id As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VIEW_CONFERENCE_WITHHOLD")
        sp.Command.AddParameter("@REQ_ID", id, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("status") = 8 Then
                txtFrmDate.Enabled = False
                txtToDate.Enabled = False
                starttimehr.Enabled = False
                endtimehr.Enabled = False
                btnsubmit.Visible = False
            End If
            ddlCity.ClearSelection()
            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONF_CITY")).Selected = True
            BindLocation(ddlCity.SelectedItem.Value)
            ddlSelectLocation.ClearSelection()
            ddlSelectLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONF_LOC")).Selected = True
            BindTower(ddlCity.SelectedItem.Value, ddlSelectLocation.SelectedItem.Value)
            ddlTower.ClearSelection()
            ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONF_TOWER")).Selected = True
            BindFloor(ddlTower.SelectedItem.Value, ddlSelectLocation.SelectedItem.Value.ToString().Trim())
            ddlFloor.ClearSelection()
            ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONF_FLOOR")).Selected = True
            BindCapacity()
            ddlCapacity.ClearSelection()
            ddlCapacity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CAPACITY")).Selected = True
            txtFrmDate.Text = ds.Tables(0).Rows(0).Item("FROM_DATE")
            txtToDate.Text = ds.Tables(0).Rows(0).Item("TO_DATE")

            If txtToDate.Text >= getoffsetdate(Date.Today) And ds.Tables(0).Rows(0).Item("status") <> 8 Then

                txtFrmDate.Enabled = True
                txtToDate.Enabled = True
                starttimehr.Enabled = True
                endtimehr.Enabled = True
                btnsubmit.Visible = True
            Else
                txtFrmDate.Enabled = False
                txtToDate.Enabled = False
                starttimehr.Enabled = False
                endtimehr.Enabled = False
                btnsubmit.Visible = False
            End If

            starttimehr.ClearSelection()
            starttimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_HOUR")).Selected = True
            endtimehr.ClearSelection()

            If ds.Tables(0).Rows(0).Item("ETIME") = "23:59:00" Then
                endtimehr.Items.FindByValue("24").Selected = True
            Else
                endtimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_HOUR")).Selected = True
            End If
            BindConference_ddl()
            ddlConf.ClearSelection()
            ddlConf.Items.FindByValue(ds.Tables(0).Rows(0).Item("SSA_SPC_ID")).Selected = True
        End If
    End Sub

    Public Sub LoadCity()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = HttpContext.Current.Session("UID")
        ObjSubSonic.Binddropdown(ddlCity, "USP_GETCITY", "CTY_NAME", "CTY_CODE", param)
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCity.SelectedIndexChanged
        BindLocation(ddlCity.SelectedItem.Value)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlConf.DataSource = sp.GetDataSet
        ddlConf.DataTextField = "CONF_NAME"
        ddlConf.DataValueField = "CONF_CODE"
        ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")
    End Sub

    Private Sub BindFloor(ByVal tower As String, ByVal loc As String)
        Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
        verDTO = verBll.GetDataforFloors(tower, loc.Trim())
        ddlFloor.DataSource = verDTO
        ddlFloor.DataTextField = "Floorname"
        ddlFloor.DataValueField = "Floorcode"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, "-- Select --")
    End Sub

    Private Sub BindTower(ByVal city As String, ByVal loc As String)
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = city
        param(1) = New SqlParameter("@vc_LOC", SqlDbType.NVarChar, 200)
        param(1).Value = loc
        param(2) = New SqlParameter("@usr_id", SqlDbType.NVarChar, 200)
        param(2).Value = HttpContext.Current.Session("UID")
        ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)
    End Sub

    Private Sub BindLocation(ByVal city As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
        param(0).Value = city
        param(1) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(1).Value = HttpContext.Current.Session("UID")
        ObjSubSonic.Binddropdown(ddlSelectLocation, "GET_LOCATIONSbyCTYID", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Private Sub BindCapacity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CAPACITY")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

        ddlCapacity.DataSource = sp.GetDataSet
        ddlCapacity.DataTextField = "capacity"
        ddlCapacity.DataValueField = "capacity"
        ddlCapacity.DataBind()
        ddlCapacity.Items.Insert(0, "--Select--")

    End Sub

    Public Sub BindConference_ddl()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAPACITY", ddlCapacity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)

        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlConf.DataSource = ds
        ddlConf.DataTextField = "CONF_NAME"
        ddlConf.DataValueField = "CONF_CODE"
        ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlSelectLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSelectLocation.SelectedIndexChanged
        Try
            If ddlSelectLocation.SelectedIndex <> -1 And ddlSelectLocation.SelectedIndex <> 0 Then
                Dim param(2) As SqlParameter
                param(0) = New SqlParameter("@CTYID", SqlDbType.NVarChar, 200)
                param(0).Value = ddlCity.SelectedItem.Value
                param(1) = New SqlParameter("@vc_LOC", SqlDbType.NVarChar, 200)
                param(1).Value = ddlSelectLocation.SelectedItem.Value
                param(2) = New SqlParameter("@usr_id", SqlDbType.NVarChar, 200)
                param(2).Value = HttpContext.Current.Session("UID")
                ObjSubSonic.Binddropdown(ddlTower, "usp_getActiveTower_LOC", "twr_name", "twr_code", param)
            Else
                ddlTower.Items.Clear()
                ddlFloor.Items.Clear()
                ddlTower.Items.Add("--Select--")
                ddlFloor.Items.Add("--Select--")
            End If
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            Dim ds As DataSet
            ds = sp.GetDataSet
            ddlConf.DataSource = sp.GetDataSet
            ddlConf.DataTextField = "CONF_NAME"
            ddlConf.DataValueField = "CONF_CODE"
            ddlConf.DataBind()
            ddlConf.Items.Insert(0, "--Select--")
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTower.SelectedIndexChanged
        Try
            If ddlTower.SelectedIndex <> -1 And ddlTower.SelectedIndex <> 0 Then
                Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
                verDTO = verBll.GetDataforFloors(ddlTower.SelectedValue, ddlSelectLocation.SelectedValue.ToString().Trim())
                ddlFloor.DataSource = verDTO
                ddlFloor.DataTextField = "Floorname"
                ddlFloor.DataValueField = "Floorcode"
                ddlFloor.DataBind()
                ddlFloor.Items.Insert(0, "-- Select --")
            Else
                ddlFloor.Items.Clear()
                ddlFloor.Items.Add("--Select--")

            End If
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_SPACE_CONFERENCE_BY_ANY_ONE")
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
            Dim ds As DataSet
            ds = sp.GetDataSet
            ddlConf.DataSource = sp.GetDataSet
            ddlConf.DataTextField = "CONF_NAME"
            ddlConf.DataValueField = "CONF_CODE"
            ddlConf.DataBind()
            ddlConf.Items.Insert(0, "--Select--")
        Catch ex As Exception
            PopUpMessage(ex.Message, Me)
        End Try
    End Sub



    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If (txtFrmDate.Text >= getoffsetdate(Date.Today)) Then
            If txtToDate.Text >= txtFrmDate.Text Then
                If starttimehr.SelectedItem.Text < endtimehr.SelectedItem.Text Then
                    SubmitAllocationOccupied("Conference", Session("Uid").ToString(), 7)
                    lblMsg.Text = ddlConf.SelectedItem.Text & " is booked successfully ."
                    cleardata()
                    strRedirect = "frmConference_finalpage.aspx?sta=" & clsSecurity.Encrypt("7") & "&rid=" & clsSecurity.Encrypt(REQID)

GVColor:

                    If strRedirect <> String.Empty Then
                        Response.Redirect(strRedirect, False)
                    End If
                Else
                    lblMsg.Text = "To Time should be grater than from Time"
                End If
            Else
            End If
        Else
            lblMsg.Text = "To date must be greater than from date"
        End If
    End Sub

    Private Sub cleardata()
        ddlCity.SelectedIndex = 0
        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlConf.Items.Clear()
        btnsubmit.Visible = False
    End Sub

    Public Function RIDGENARATION(ByVal field1 As String) As String
        Dim RID As String
        RID = Replace(getoffsetdatetime(DateTime.Now), "/", "")
        RID = Replace(RID, "AM", "")
        RID = Replace(RID, "PM", "")
        RID = Replace(RID, ":", "")
        RID = Replace(RID, " ", "")
        RID = Replace(RID, "#", "")
        RID = field1 & "REQ" & RID
        Return RID
    End Function

    Private Sub SubmitAllocationOccupied(ByVal strVerticalCode As String, ByVal strAurId As String, ByVal status As Integer)
        Dim cntWst As Integer = 0
        Dim cntHCB As Integer = 0
        Dim cntFCB As Integer = 0
        Dim twr As String = ""
        Dim flr As String = ""
        Dim wng As String = ""
        Dim intCount As Int16 = 0
        Dim ftime As String = ""
        Dim ttime As String = ""
        Dim StartHr As String = "00"
        Dim EndHr As String = "24"
        Dim StartMM As String = "00"
        Dim EndMM As String = "00"
        Dim remarks As String = ""

        If starttimehr.SelectedItem.Value <> "HH" Then
            StartHr = starttimehr.SelectedItem.Text
        End If
       
        If endtimehr.SelectedItem.Value <> "HH" Then
            If endtimehr.SelectedItem.Value = "24" Then
                EndHr = "00"
            Else
                EndHr = endtimehr.SelectedItem.Text
            End If
        End If
      
        ftime = StartHr + ":" + StartMM
        ttime = EndHr + ":" + EndMM

        Dim sta As Integer = status
        Dim param3(5) As SqlParameter

        param3(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param3(0).Value = Request.QueryString("ID")
        param3(1) = New SqlParameter("@FROM_DATE", SqlDbType.DateTime)
        param3(1).Value = txtFrmDate.Text
        param3(2) = New SqlParameter("@TO_DATE", SqlDbType.DateTime)
        param3(2).Value = txtToDate.Text
        param3(3) = New SqlParameter("@FROM_TIME", SqlDbType.DateTime)
        param3(3).Value = ftime
        param3(4) = New SqlParameter("@TO_TIME", SqlDbType.DateTime)
        param3(4).Value = ttime
        param3(5) = New SqlParameter("@STATUSID", SqlDbType.NVarChar, 200)
        param3(5).Value = sta
        ObjSubSonic.GetSubSonicExecute("CONF_UPDATE_WITHHOLD", param3)
    End Sub

    Protected Sub ddlFloor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFloor.SelectedIndexChanged
        'GET_SPACE_CONFERENCE
        If ddlFloor.SelectedIndex <> 0 Then
            BindConference()
        End If
    End Sub
    Protected Sub ddlConf_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlConf.SelectedIndexChanged
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SP_GET_CONF_DETAILS")
        sp.Command.AddParameter("@CONF_CODE", ddlConf.SelectedItem.Value, DbType.String)
        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlSelectLocation.Items.Clear()
        ddlTower.Items.Clear()
        ddlFloor.Items.Clear()
        ddlCapacity.Items.Clear()
        BindLocation(ddlCity.SelectedItem.Value)
        ddlSelectLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONFDET_LOCATION")).Selected = True
        BindTower(ddlCity.SelectedItem.Value, ddlSelectLocation.SelectedItem.Value)
        ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONFDET_TOWER")).Selected = True
        BindFloor(ddlTower.SelectedItem.Value, ddlSelectLocation.SelectedItem.Value.ToString().Trim())
        ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONFDET_FLOOR")).Selected = True
        BindCapacity()
        ddlCapacity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONFERENCE_CAPACITY")).Selected = True
    End Sub
    Public Sub BindConference()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_CONFERENCE")
        sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LCM_CODE", ddlSelectLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", ddlTower.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", ddlFloor.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CAPACITY", ddlCapacity.SelectedItem.Value, DbType.String)

        Dim ds As DataSet
        ds = sp.GetDataSet
        ddlConf.DataSource = ds
        ddlConf.DataTextField = "CONF_NAME"
        ddlConf.DataValueField = "CONF_CODE"
        ddlConf.DataBind()
        ddlConf.Items.Insert(0, "--Select--")
    End Sub

End Class
