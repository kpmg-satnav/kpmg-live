using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using OneLogin.Saml;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AccountSettings accountSettings = new AccountSettings();
        if (Request.Form["SAMLResponse"] == "" || Request.Form["SAMLResponse"] == null)
        {
            OneLogin.Saml.AuthRequest req = new AuthRequest(new AppSettings2(), accountSettings);
            string st = accountSettings.idp_sso_target_url + "?SAMLRequest=" + Server.UrlEncode(req.GetRequest(AuthRequest.AuthRequestFormat.Base64));
            Response.Redirect(st);
        }
        else
        {
            OneLogin.Saml.Response samlResponse = new Response(accountSettings);
            samlResponse.LoadXmlFromBase64(Request.Form["SAMLResponse"]);
            //Response.Write(Request.Form["SAMLResponse"]);
            //string redirect_url = "";
            string email_id = "";
            string emp_id = "";
            string filename = "";
            string filepath = "";
            string str1 = "~/LogFlip_kartin.aspx/?company=";
            string redirect_url = "";
            try
            {

                filename = "Log_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                filepath = HttpContext.Current.Server.MapPath(Convert.ToString("~/ErrorLogFiles/") + filename);
                using (StreamWriter stwriter = File.CreateText(filepath))
                { 
                    stwriter.Write(Request.Form["SAMLResponse"]);
                }
                //string emp_id = samlResponse.GetEmpID();
                //email_id = samlResponse.GetNameID();
                //redirect_url = str1 + "Flipkart" + "&empid=" + emp_id + "&email=" + email_id;
            }
            catch (Exception Ex)
            {
                //Response.Write(Request.Form["SAMLResponse"]);
                Response.Write("Failed");
                throw Ex;
            }
            if (samlResponse.IsValid())
            {
                //string str1 = "~/login.aspx/?company=";
                //string emp_id = samlResponse.GetEmpID();
                //string email_id = samlResponse.GetNameID();
                //string redirect_url = str1 + "Flipkart" + "&empid=" + emp_id + "&email=" + email_id;
                emp_id = samlResponse.GetEmpID();
                email_id = samlResponse.GetNameID();
                redirect_url = str1 + "Flipkart" + "&empid=" + emp_id + "&email=" + email_id;
                //Response.Redirect(redirect_url);
                Server.TransferRequest(redirect_url);
                //Response.Write(email_id);
                //Response.Write("OK!");
                //Response.Write(emp_id);
                //Response.Write(email_id);
            }
            else
            {
                //Response.Write(Request.Form["SAMLResponse"]);
                Response.Write("Failed");
            }
        }
    }
}
