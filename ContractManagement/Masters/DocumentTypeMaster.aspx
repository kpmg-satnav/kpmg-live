﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/ContractManagement/Masters/DocumentTypeMaster.cs" Inherits="ContractManagement_Masters_DocumentTypeMaster" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <%----%>

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->


    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }
    </style>
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>

</head>
<body data-ng-controller="DocumentTypeMasterController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Spend Category Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Document Type Master</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form role="form" id="form1" name="frmDocMas" data-valid-submit="Save()" novalidate>
                        <div class="row form-inline">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmDocMas.$submitted && frmDocMas.DDT_CODE.$invalid}">
                                    <label for="txtcode">Document Type Code<span style="color: red;">*</span></label>
                                    <input type="text" name="DDT_CODE" class="form-control" data-ng-pattern="codepattern"
                                        data-ng-model="DocumentType.DDT_CODE" autofocus required="" />
                                    <span class="error" data-ng-show="frmDocMas.$submitted && frmDocMas.DDT_CODE.$invalid">Please enter valid code </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmDocMas.$submitted && frmDocMas.DDT_NAME.$invalid}">
                                    <label for="Text1">Document Type Name<span style="color: red;">*</span></label>
                                    <input type="text" name="DDT_NAME" class="form-control" data-ng-pattern="namepattern"
                                        data-ng-model="DocumentType.DDT_NAME" required="" />
                                    <span class="error" data-ng-show="frmDocMas.$submitted && frmDocMas.DDT_NAME.$invalid">Please enter valid name </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmDocMas.$submitted && frmDocMas.DDT_STA_ID.$invalid}">
                                    <label for="txtcode">Status<span style="color: red;">*</span></label>
                                    <select data-ng-model="DocumentType.DDT_STA_ID" name="DDT_STA_ID" class="selectpicker"
                                        data-live-serach="true" required="">
                                        <option value="" selected>--Select--</option>
                                        <option data-ng-repeat="obj in StaDet" value="{{obj.Id}}">{{obj.Name}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'error': frmDocMas.$submitted && frmDocMas.DDT_REM.$invalid}">
                                    <label for="txtcode">Remarks</label>
                                    <textarea class="form-control" style="height: 60px" name="DDT_REM" data-ng-pattern="codepattern"
                                        data-ng-model="DocumentType.DDT_REM"></textarea>
                                    <span class="error" data-ng-show="frmDocMas.$submitted && frmDocMas.DDT_REM.$invalid">Please enter valid text </span>
                                </div>
                            </div>
                        </div>

                        <div class="row form-inline">
                            <div class="col-md-12 text-right" style="padding-top: 17px">
                                <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                <input type="button" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                <a class='btn btn-primary custom-button-color' href="../../ContractManagement/Masters/MasterScreens.aspx">Back</a>
                            </div>
                        </div>
                        <div class="row" style="padding-left: 30px;">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <%--<script src="../JS/DocumentTypeMaster.js"></script>--%>
    <script src="../JS/DocumentTypeMaster.min.js" defer></script>
    <%--<script src="../../SMViews/Utility.js"></script>--%>
    <script src="../../SMViews/Utility.min.js" defer></script>
</body>
</html>
