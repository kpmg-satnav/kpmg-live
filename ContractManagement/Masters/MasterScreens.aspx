﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MasterScreens.aspx.cs" Inherits="ContractManagement_Masters_MasterScreens" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-5b02d1ea3b.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <%----%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Primary Masters" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Contract Masters</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <div class="box-body">
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12" id="ContractHLdiv" runat="server">
                                    <asp:HyperLink ID="ContractHL" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/ContractManagement/Masters/ContractMaster.aspx"> Contract Master </asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="Div1" runat="server">
                                    <asp:HyperLink ID="HyperLink1" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/ContractManagement/Masters/SupplierMaster.aspx"> Supplier Master </asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="Div2" runat="server">
                                    <asp:HyperLink ID="HyperLink2" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/ContractManagement/Masters/SpendCategoryMaster.aspx"> Spend Category </asp:HyperLink>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12" id="Div3" runat="server">
                                    <asp:HyperLink ID="HyperLink3" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/ContractManagement/Masters/BusinessGroupMaster.aspx"> Business Group </asp:HyperLink>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="Div4" runat="server">
                                    <asp:HyperLink ID="HyperLink4" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/ContractManagement/Masters/DocumentTypeMaster.aspx"> Document Type </asp:HyperLink>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
