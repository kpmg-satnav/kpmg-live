﻿app.service("ModifyFactSheetService", function ($http, $q) {
    this.GetDetails = function () {
        deferred = $q.defer();
        return $http.get('../../api/ModifyFactSheet/GetFactSheetDetails')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetFSDetails = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/ModifyFactSheet/Get_Factsheet_Details', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Update = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/ModifyFactSheet/Update', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.DRMExcelDownload = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/ModifyFactSheet/DRMExcelDownload', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

});

app.controller('ModifyFactSheetController', function ($scope, ModifyFactSheetService, UtilityService, FactSheetService, $filter) {

    $scope.Viewstatus = 0;
    $scope.ViewUploption = true;
    $scope.PODates = true;
    $scope.PORemainder = true;
    $scope.AgreeDates = true;
    $scope.UploadDoc = true;
    $scope.TermsClause = true;
    $scope.Stakehol = true;
    $scope.Values = true;
    $scope.Remainder = true;
    $scope.FactSheet = {};
    $scope.FactSheetDate = {};
    $scope.FactSheetRenewal = {};
    $scope.FactSheetValues = {};
    $scope.FactSheetDetails = {};
    $scope.document = {};
    $scope.DocumentDetails = [];
    $scope.Select5 = true;
    $scope.PODates = true;
    $scope.PORemainder = true;
    $scope.KBPDetails = [
     {
         'KPH_NAME': '',
         'IS_ERROR': false,
     }
    ]

    $scope.AddNew = function () {
        $scope.KBPDetails.push({
            'KPH_NAME': '',
            'IS_ERROR': false,
        });
    }

    $scope.AddNew = function () {
        $scope.KBPDetails.push({
            'KPH_NAME': '',
            'IS_ERROR': false,
        });
    }

    $scope.Delete = function (item) {
        $scope.KBPDetails = _.without($scope.KBPDetails, item);
    }


    //for GridView 
    var columnDefs = [
       { headerName: "Request Id", field: "DFM_REQ_ID", width: 150, cellClass: 'grid-align', template: '<a ng-click="GetDetailsOnSelection(data)">{{data.DFM_REQ_ID}}</a>' },
       { headerName: "Contract Name", field: "DCT_NAME", width: 180, cellClass: 'grid-align' },
       { headerName: "Supplier Name", field: "DST_NAME", width: 180, cellClass: 'grid-align' },
       { headerName: "Spend Category", field: "DSD_NAME", width: 180, cellClass: 'grid-align' },
       { headerName: "Business Group", field: "DBT_NAME", width: 180, cellClass: 'grid-align' },
       { headerName: "Reference No", field: "DFM_REF_NO", width: 180, cellClass: 'grid-align' },
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        //enableColResize: true,
        suppressHorizontalScroll: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.Loaddata = function () {
        FactSheetService.Contract().then(function (data) {
            $scope.Contracts = data;
            FactSheetService.Spend().then(function (data1) {
                $scope.SpendCats = data1;
                FactSheetService.Business().then(function (data2) {
                    $scope.BusinessGrp = data2;
                    FactSheetService.Supplier().then(function (data3) {
                        $scope.Suppliers = data3;
                        FactSheetService.Remainder().then(function (data4) {
                            $scope.RemainderDates = data4;
                            FactSheetService.PORemainder().then(function (data9) {
                                $scope.RemainderPODates = data9
                                FactSheetService.Notification().then(function (data5) {
                                    $scope.RepeatNoft = data5;
                                    FactSheetService.PONotification().then(function (data10) {
                                        $scope.RepeatPONoft = data10;
                                      
                                        FactSheetService.Notisperiod().then(function (data6) {
                                            $scope.NoticePrd = data6;
                                            FactSheetService.NotisPOperiod().then(function (data11) {
                                                $scope.NoticePOPrd = data11;
                                                FactSheetService.Doctype().then(function (data7) {
                                                    $scope.TypeOfDoc = data7;
                                                    FactSheetService.GetRefNum().then(function (data8) {
                                                        $scope.FactSheet.DFM_REF_NO = data8;
                                                        ModifyFactSheetService.GetDetails().then(function (data) {
                                                            $scope.gridata = data;
                                                            $scope.gridOptions.api.setRowData(data);
                                                        }, function (error) {
                                                        })
                                                    }, function (error) {
                                                    })
                                                }, function (error) {
                                                })
                                            }, function (error) {
                                            })
                                        }, function (error) {
                                        })
                                    }, function (error) {
                                    })
                                }, function (error) {
                                });
                            }, function (error) {
                            });
                        }, function (error) {
                        });
                    }, function (error) {
                    });
                }, function (error) {

                });
            }, function (error) {

            });
        });
    }

    $scope.Loaddata();

    $scope.Parties = {};
    $scope.GetParties = function (obj) {
        for (var i = 0; i < $scope.Suppliers.length; i += 1) {
            var result = $scope.Suppliers[i];
            if (result.DSD_CODE == obj) {
                $scope.Parties = " Contract between " + result.DSD_NAME + " and QuickFMS";
            }
        }
    }
    var value;
    var EFRM, TRMN;
    $scope.EffDatechange = function () {
        var EFRM1 = moment($scope.FactSheetDate.DFDT_EFRM_DATE, "DD/MM/YYYY")
        var TRMN1 = moment($scope.FactSheetDate.DFDT_TRMN_DATE, "DD/MM/YYYY")
        if (EFRM1 > TRMN1) {
            showNotification('error', 8, 'bottom-right', 'Invalid date! Expiry Date should be greater than Effective Date .');
        }
        else {
            var EFRM = moment($scope.FactSheetDate.DFDT_EFRM_DATE, "MM/DD/YYYY").year()
            var TRMN = moment($scope.FactSheetDate.DFDT_TRMN_DATE, "MM/DD/YYYY").year()
            value = parseInt(TRMN) - parseInt(EFRM);
        }
    }

    $scope.POEffDatechange = function () {
        var POEFRM1 = moment($scope.FactSheetPODate.DFDT_PO_EFRM_DATE, "DD/MM/YYYY")
        var POTRMN1 = moment($scope.FactSheetPODate.DFDT_PO_TRMN_DATE, "DD/MM/YYYY")
        if (POEFRM1 > POTRMN1) {
            showNotification('error', 9, 'bottom-right', 'Invalid date! PO Expiry Date should be greater than PO Effective Date .')
        }
        else {
            var POEFRM = moment($scope.FactSheetPODate.DFDT_PO_EFRM_DATE, "DD/MM/YYYY").year()
            var POTRMN = moment($scope.FactSheetPODate.DFDT_PO_TRMN_DATE, "DD/MM/YYYY").year()
            value = parseInt(POTRMN) - parseInt(POEFRM);
        }
    }

    $scope.back = function () {
        $scope.ViewUploption = true;
        $scope.Viewstatus = 0;
    }
    $scope.Requestid = {};

    $scope.GetDetailsOnSelection = function (data) {
        $scope.Requestid = data.DFM_REQ_ID;
        $scope.ViewUploption = false;
        $scope.Viewstatus = 1;
        $scope.EnableStatus = 1;
        ModifyFactSheetService.GetFSDetails(data).then(function (data) {
            $scope.FactSheet = data.MainSheet;
            $scope.FactSheetDate = data.AgreeMentSheet;
            $scope.FactSheetDate.DFDT_SIGN_DATE = $filter('date')($scope.FactSheetDate.DFDT_SIGN_DATE, "dd/MM/yyyy");
            $scope.FactSheetDate.DFDT_EFRM_DATE = $filter('date')($scope.FactSheetDate.DFDT_EFRM_DATE, "dd/MM/yyyy");
            $scope.FactSheetDate.DFDT_TRMN_DATE = $filter('date')($scope.FactSheetDate.DFDT_TRMN_DATE, "dd/MM/yyyy");
            $scope.FactSheetDate.DFDT_REN_DATE = $filter('date')($scope.FactSheetDate.DFDT_REN_DATE, "dd/MM/yyyy");
            $scope.FactSheetPODate = data.POSheet;
            $scope.FactSheetPODate.DFDT_PO_EFRM_DATE = $filter('date')($scope.FactSheetPODate.DFDT_PO_EFRM_DATE, "dd/MM/yyyy");
            $scope.FactSheetPODate.DFDT_PO_TRMN_DATE = $filter('date')($scope.FactSheetPODate.DFDT_PO_TRMN_DATE, "dd/MM/yyyy");
            $scope.FactSheetPODate.DFDT_PO_REN_DATE = $filter('date')($scope.FactSheetPODate.DFDT_PO_REN_DATE, "dd/MM/yyyy");
            $scope.RepeatNoft.DRN_SNO = data.DRD_SNO;
            $scope.RemainderDates.DRD_SNO = data.DRN_SNO;
            $scope.RepeatPONoft.DRN_SNO = data.DRD_SNO;
            $scope.RemainderPODates.DRD_SNO = data.DRN_SNO;
            $scope.FactSheetDetails = data.StakeholderSheet;
            $scope.KBPDetails = data.BusinessStakehol;
            $scope.FactSheetValues = data.ValuesSheet;
            $scope.GetParties($scope.FactSheet.DFM_DSD_CODE);
            $scope.DocumentDetails = data.DocumentDetails;
            $scope.gridOptions1.api.setRowData($scope.DocumentDetails);
        }, function (error) {
            console.log(error);
        })
    }

    $scope.UploadOutsideSubmit = function () {
        //console.log($scope.FactSheetDate)
        $scope.frm.$submitted = true;
        if ($scope.frm.$valid) {
            $scope.ModifyFactSheet();
        }
    }

    $scope.ModifyFactSheet = function () {
        var RTRM = moment.utc($scope.FactSheetDate.DFDT_REN_DATE, "DD/MM/YYYY")
        var EFRM = moment.utc($scope.FactSheetDate.DFDT_EFRM_DATE, "DD/MM/YYYY")
        var TRMN = moment.utc($scope.FactSheetDate.DFDT_TRMN_DATE, "DD/MM/YYYY")
        var SGMN = moment.utc($scope.FactSheetDate.DFDT_SIGN_DATE, "DD/MM/YYYY")
        //var SGMN = moment.utc($scope.FactSheetDate.DFDT_SIGN_DATE).format("DD/MM/YYYY")
        var POEFRM = moment.utc($scope.FactSheetPODate.DFDT_PO_EFRM_DATE, "DD/MM/YYYY")
        var POTRMN = moment.utc($scope.FactSheetPODate.DFDT_PO_TRMN_DATE, "DD/MM/YYYY")
        var PORTMN = moment.utc($scope.FactSheetPODate.DFDT_PO_REN_DATE, "DD/MM/YYYY")
        console.log(SGMN);
        console.log($scope.FactSheetDate.DFDT_TRMN_DATE);
        //if ($scope.RemainderDates.DRD_SNO != undefined && $scope.RemainderPODates.DRD_SNO != undefined)
        //{
        //    if ($scope.RepeatNoft.DRN_SNO != undefined && $scope.RepeatPONoft.DRN_SNO != undefined)
        //    {
        //        if (EFRM < TRMN && POEFRM < POTRMN)
        //        {
                    $scope.FactSheet.DFDT_EFRM_DATE = EFRM,
                    $scope.FactSheet.DFDT_TRMN_DATE = TRMN,
                    $scope.FactSheet.DFDT_SIGN_DATE = SGMN,
                    $scope.FactSheet.DFDT_REN_DATE = RTRM,
                    $scope.FactSheetPODate.DFDT_PO_EFRM_DATE = POEFRM,
                    $scope.FactSheetPODate.DFDT_PO_TRMN_DATE = POTRMN,
                    $scope.FactSheetPODate.DFDT_PO_REN_DATE = PORTMN,
                    //console.log($scope.FactSheetDate.DFDT_EFRM_DATE);
                    //console.log($scope.FactSheetDate.DFDT_TRMN_DATE);
                    $scope.FactSheet.DFM_REQ_ID = $scope.Requestid;
                    $scope.dataobj = {
                        MainSheet: $scope.FactSheet,
                        AgreeMentSheet: $scope.FactSheet,
                        DRN_SNO: $scope.RepeatNoft.DRN_SNO,
                        DRD_SNO: $scope.RemainderDates.DRD_SNO,
                        POSheet: $scope.FactSheetPODate,
                        DRN_SNO: $scope.RepeatPONoft.DRN_SNO,
                        DRD_SNO: $scope.RemainderPODates.DRD_SNO,
                        ValuesSheet: $scope.FactSheetValues,
                        StakeholderSheet: $scope.FactSheetDetails,
                        BusinessStakehol: $scope.KBPDetails,
                        DocumentDetails: $scope.DocumentDetails
                    };
                    console.log($scope.dataobj);
                    ModifyFactSheetService.Update($scope.dataobj).then(function (data) {
                        if (data != "ERROR") {
                            $scope.FactSheet = {};
                            $scope.FactSheetDate = {};
                            $scope.FactSheetPODate = {};
                            $scope.FactSheetRenewal = {};
                            $scope.FactSheetValues = {};
                            $scope.FactSheetDetails = {};
                            $scope.KBPDetails = [
      {
          'KPH_NAME': '',
      }
                            ]
                            angular.forEach($scope.RemainderDates, function (val) {
                                if (val.DRD_ISCHECKED == true) {
                                    val.DRD_ISCHECKED = false;
                                }
                            });
                            $scope.RepeatNoft.DRN_SNO = {};
                            $scope.RemainderDates.DRD_SNO = {};
                            $scope.RepeatPONoft.DRN_SNO = {};
                            $scope.RemainderPODates.DRD_SNO = {};
                            $scope.frm.$submitted = false;
                            $scope.DocumentDetails = [];
                            $scope.document = {};
                            showNotification('success', 8, 'bottom-right', "Modified Succesfully..");
                        }

                    }, function (error) {
                    });
        //        }
        //        else if (EFRM > TRMN) {
        //            showNotification('error', 8, 'bottom-right', 'Invalid date! Expiry Date should be greater than Effective Date.');

        //        }
        //        else {
        //            showNotification('error', 9, 'bottom-right', 'Invalid date ! PO Expiry Date should be greater than PO Effective Date.');
        //        }
        //    }
        //    else if (error = 4) {
        //        showNotification('error', 4, 'bottom-right', "Please Select Atleast One Repeat Notification");

        //    }
        //    else {
        //        showNotification('error', 10, 'bottom-right', "Please Select Atleast One PO Repeat Notification");
        //    }
        //}
        //else if (error = 4) {
        //    showNotification('error', 4, 'bottom-right', "Please Select Atleast One Renewal Remainder Date");
        //}
        //else {
        //    showNotification('error', 10, 'bottom-right', "Please Select Atleast One PO Renewal Remainder Date");
        //}
    }

    var columnDefs1 = [
     { headerName: "Type of document", field: "DFDC_DDT_CODE", width: 150, cellClass: 'grid-align' },
     { headerName: "Document Name", field: "DFDC_ACT_NAME", width: 180, cellClass: 'grid-align' },
     { headerName: "Delete", width: 70, template: '<a ng-click = "DeleteDoc(data)"> <i class="fa fa-times class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true },
    { headerName: "Download", width: 70, template: '<a ng-click = "Download(data)"> <i class="fa fa-download class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true }
    ];

    $scope.gridOptions1 = {
        columnDefs: columnDefs1,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        suppressHorizontalScroll: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions1.api.sizeColumnsToFit()
        }
    };

    $scope.DeleteDoc = function (dataobj) {
        $scope.DocumentDetails = _.without($scope.DocumentDetails, dataobj);
        $scope.gridOptions1.api.setRowData($scope.DocumentDetails);
    }

    $scope.Download = function (dataobj) {

        window.location.assign(UtilityService.path + "/api/ModifyFactSheet/DRMExcelDownload?id=" + dataobj.DFDC_ID);

    }

    $scope.UploadFile = function () {
        if ($scope.document.DFDC_DDT_CODE != undefined) {
            var Ext = $('#FileUpl').val().split('.').pop().toLowerCase();
            var fname = $('#FileUpl').val().split('\\');
            var str = fname[fname.length - 1];
            var ExtCount = str.split('.');
            if (ExtCount.length <= 2) {
                if ($('#FileUpl').val()) {
                    if (/^[a-zA-Z0-9-_. ]*$/.test(str) == true) {
                        if (Ext == "xlsx" || Ext == "xls" || Ext == "doc" || Ext == "docx" || Ext == "PDF" || Ext == "ppt" || Ext == "pptx" || Ext == "pdf" || Ext == "txt" || Ext == "Txt" || Ext == "msg" || Ext == "jpeg" || Ext == "jpg" || Ext == "png") {
                            if (!window.FormData) {
                                redirect(); // if IE8   
                            }
                            else {
                                var formData = new FormData();
                                var UplFile = $('#FileUpl')[0];
                                formData.append("UplFile", UplFile.files[0]);
                                formData.append("CurrObj", $scope.document.DFDC_DDT_CODE);
                                $.ajax({
                                    url: UtilityService.path + '/api/FactSheet/UploadTemplate',
                                    type: "POST",
                                    data: formData,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function (data) {
                                        var respdata = JSON.parse(data);
                                        if (respdata.data != null) {
                                            console.log(respdata.data);
                                            angular.forEach(respdata.data, function (data) {
                                                debugger;
                                                console.log(data);
                                                $scope.DocumentDetails.push(data);
                                                //$scope.gridata = data;
                                            });
                                            $scope.gridOptions1.api.setRowData($scope.DocumentDetails);
                                            $scope.document = {};
                                            showNotification('success', 8, 'bottom-right', "Uploaded Successfully");
                                        }
                                        else {
                                            showNotification('error', 8, 'bottom-right', respdata.Message);
                                        }
                                    }
                                });
                            }
                        }
                        else {
                            showNotification('error', 8, 'bottom-right', 'Please upload only .xls, .xlsx, PDF, PSD and doc files');
                        }
                    }
                    else {
                        showNotification('error', 8, 'bottom-right', 'Your file Name contains special charaters');
                    }
                }
                else {
                    showNotification('error', 8, 'bottom-right', 'Please Select File To Upload');
                }
            }
            else {
                showNotification('error', 8, 'bottom-right', 'Please upload valid file.');
            }
        }

        else {
            showNotification('error', 8, 'bottom-right', 'Please Select document type');
        }
    }
});