﻿app.service("AgreementReportService", function ($http, $q, UtilityService) {
   
    this.GetAgreementReportData = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/AgreementReport/GetAgreementReportData', dataobj)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    }
});

app.controller('AgreementReportController', function ($scope, $q, $location, AgreementReportService, UtilityService, $filter) {

    $scope.AgreementReport = {};
    $scope.MainCategory = [];
    $scope.SubCategories = [];
    $scope.Status = [];
 
    UtilityService.GetContractCategories(1).then(function (response) {
        if (response.data != null) {
            $scope.MainCategory = response.data;
        }
    });
 
    $scope.CatChanged = function (subcat, AgreementReport) {        
        UtilityService.GetContractSubcategories(subcat).then(function (response) {         
            if (response.data != null) {
                $scope.SubCategories = response.data;
            }
        });
    }
    UtilityService.GetContractStatus().then(function (response) {
        if (response.data != null) {
            $scope.Status = response.data;
        }
    });
    $scope.columDefs = [
          { headerName: "Contract Title", field: "DCT_NAME", cellClass: 'grid-align', filter: 'set' },
             { headerName: "Contract Ref No.", field: "DFM_REF_NO", cellClass: 'grid-align', filter: 'set' },
          { headerName: "Created Dates", field: "DFM_CREATED_DT", template: '<span>{{data.DFM_CREATED_DT | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align" },
           { headerName: "Total Contract Value", field: "DFV_CNRT_VAL", cellClass: "grid-align" },
            { headerName: "Payment Terms.", field: "DFD_PT", cellClass: "grid-align" },
               { headerName: "Status", field: "STATUS", cellClass: "grid-align" },
                  { headerName: "Expiry Dates", field: "DFDT_TRMN_DATE", template: '<span>{{data.DFDT_TRMN_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align" }
                   
    ];

    $scope.gridOptions = {
        columnDefs: $scope.columDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple',
    };

    $scope.GetAgreementReportData = function () {
        AgreementReportService.GetAgreementReportData($scope.AgreementReport).then(function (response) {          
            if (response.data.length > 0) {
                $scope.gridOptions.api.setRowData(response.data);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "No Data Found");
            }
        });
    }
    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "AgreementReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.GenReport = function (Allocdata, Type) {
        $scope.GenerateFilterExcel();

    };
});