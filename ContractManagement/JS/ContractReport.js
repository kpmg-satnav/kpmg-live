﻿app.service("ContractReportService", function ($http, $q) {
    this.Contract = function () {
        deferred = $q.defer();
        return $http.get('../../api/ContractMaster/BindContractData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Spend = function () {
        deferred = $q.defer();
        return $http.get('../../api/SpendCategoryMaster/BindCategoryData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Business = function () {
        deferred = $q.defer();
        return $http.get('../../api/BusinessGroupMaster/BindBusinessData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Supplier = function () {
        deferred = $q.defer();
        return $http.get('../../api/SupplierMaster/BindSupplierData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.GetData = function (dataobj) {        
        deferred = $q.defer();
        return $http.post('../../api/ContractReport/GetData', dataobj)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    }
});

app.controller('ContractReportController', function ($scope, $q, $location, ContractReportService, UtilityService, $filter) {

    $scope.ContractReport = {};
    $scope.Contracts = {};
    $scope.Loaddata = function () {
        ContractReportService.Contract().then(function (data) {
            $scope.Contracts = data;          
            ContractReportService.Spend().then(function (data1) {
                $scope.SpendCats = data1;               
                ContractReportService.Business().then(function (data2) {
                    $scope.BusinessGrp = data2;                  
                    ContractReportService.Supplier().then(function (data3) {
                        $scope.Suppliers = data3;      
                    }, function (error) {
                    });
                }, function (error) {
                });
            }, function (error) {
            });
        }, function (error) {
        });
    }
   
        $scope.Loaddata();


    $scope.columDefs = [     
          { headerName: "Contract Type", field: "DCT_NAME", cellClass: 'grid-align', filter: 'set' },
             { headerName: "licence Model", field: "DST_NAME", cellClass: 'grid-align', filter: 'set' },
          { headerName: "Client Vertical", field: "DBT_NAME", cellClass: "grid-align" },
           { headerName: "Client Name", field: "DSD_NAME", cellClass: "grid-align" },
            { headerName: "Contract Ref No.", field: "DFM_REF_NO", cellClass: "grid-align" },
               { headerName: "Created Dates", field: "DFM_CREATED_DT", template: '<span>{{data.DFM_CREATED_DT | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align" },
                  { headerName: "Expiry Date", field: "DFDT_TRMN_DATE", template: '<span>{{data.DFDT_TRMN_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align" },
                     { headerName: "Contract Value", field: "DFV_CNRT_VAL", cellClass: "grid-align" },
                       { headerName: "Scope of contract", field: "DFV_CNRT_SCP", cellClass: "grid-align" },
                        { headerName: "Payment Terms", field: "DFD_PT", cellClass: "grid-align" },
                           { headerName: "Effective Date", template: '<span>{{data.DFDT_EFRM_DATE | date:"dd MMM, yyyy"}}</span>', field: "DFDT_EFRM_DATE", cellClass: "grid-align" },
                             { headerName: "Termination Clauses", field: "DFD_TC", cellClass: "grid-align" },
                              { headerName: "Clause Deviation", field: "DFD_CD", cellClass: "grid-align" },
                              { headerName: "Confidentiality", field: "DFD_CONFD", cellClass: "grid-align" },
                              { headerName: "Commercial Terms", field: "DFD_CT", cellClass: "grid-align" },
                              { headerName: "Ancillary Service", field: "DFD_AC", cellClass: "grid-align" }

    ];

    $scope.gridOptions = {
        columnDefs: $scope.columDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple',
    };
   
    $scope.GetData = function () {
        ContractReportService.GetData($scope.ContractReport).then(function (response) {
            console.log(response.data);
            if (response.data.length>0) {
                $scope.gridOptions.api.setRowData(response.data);              
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "No Data Found");
            }
        });
    }
    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "ContractReport.csv"
        };        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.GenReport = function (Allocdata, Type) {
        $scope.GenerateFilterExcel();        
     
    };
});