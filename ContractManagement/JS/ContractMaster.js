﻿app.service("ContractMasterService", ['$http', '$q', function ($http, $q) {

    this.BindGrid = function () {
        deferred = $q.defer();
        return $http.get('../../api/ContractMaster/BindContractData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Insertdata = function (dataobj) {
        console.log(dataobj);
        deferred = $q.defer();
        return $http.post('../../api/ContractMaster/Insert', dataobj)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    }

    this.Updatedata = function (dataobj) {
        console.log(dataobj);
        deferred = $q.defer();
        return $http.post('../../api/ContractMaster/Update', dataobj)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    }

}]);

app.controller('ContractMasterController', ['$scope', '$q', 'ContractMasterService', function ($scope, $q, ContractMasterService) {

    $scope.ActionStatus = 0;
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.CurrentContract = {};
    $scope.IsInEdit = false;
    $scope.codepattern = /^[a-zA-Z0-9. ]*$/;
    $scope.namepattern = /^[a-zA-Z0-9-_ /():. ]*$/;

    //for GridView 
    var columnDefs = [
       { headerName: "Contract Code", field: "DCT_CODE", width: 150, cellClass: 'grid-align' },
       { headerName: "Contract Name", field: "DCT_NAME", width: 180, cellClass: 'grid-align' },
       { headerName: "Status", template: "{{ShowStatus(data.DCT_STA_ID)}}", width: 100, cellClass: 'grid-align' },
       { headerName: "Edit", width: 70, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        //enableColResize: true,
        suppressHorizontalScroll: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })


    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

    $scope.Loaddata = function () {
        ContractMasterService.BindGrid().then(function (data) {
            $scope.gridata = data;
            $scope.gridOptions.api.setRowData(data);
        }, function (error) {
        });
    }
    $scope.Loaddata();

    $scope.EditFunction = function (data) {
        console.log(data);
        $scope.CurrentContract = {};
        angular.copy(data, $scope.CurrentContract);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
     $scope.CurrentContract.DCT_STA_ID = data.DCT_STA_ID;
        $scope.StaDet.Id = data.DCT_STA_ID;
    }

    $scope.ClearData = function () {
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.CurrentContract = {};
        $scope.frmConMas.$submitted = false;
    }

    $scope.Save = function () {
        if ($scope.IsInEdit) {
            ContractMasterService.Updatedata($scope.CurrentContract).then(function () {
                var updatedobj = {};
                angular.copy($scope.CurrentContract, updatedobj)
                $scope.gridata.unshift(updatedobj);
                $scope.ShowMessage = true;
                $scope.Success = "Data Updated Successfully";
                ContractMasterService.BindGrid().then(function (data) {
                    $scope.gridata = data;
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                });
                $scope.IsInEdit = false;
                $scope.ClearData();
                showNotification('success', 8, 'bottom-right', $scope.Success);
            }, function (error) {
            })
        }
        else {
            ContractMasterService.Insertdata($scope.CurrentContract).then(function (data) {
                $scope.ShowMessage = true;
                $scope.Success = "Data Inserted Successfully";
                var savedobj = {};
                angular.copy($scope.CurrentContract, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
                $scope.CurrentContract = {};
                $scope.ClearData();
            }
            , function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
            });
        }
    }

}]);