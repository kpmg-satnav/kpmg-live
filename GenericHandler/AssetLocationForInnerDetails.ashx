<%@ WebHandler Language="VB" Class="AssetLocationForInnerDetails" %>
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System

Public Class AssetLocationForInnerDetails : Implements IHttpHandler, IRequiresSessionState 
    
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim id As String = request("ct")
      
        Dim totalRecords As Integer
        Dim clsAssetLocationForInnerDetails As Collection(Of clsAssetLocationForInnerDetails) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords, id)
       
       
        Dim output As String = BuildJQGridResults(clsAssetLocationForInnerDetails, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        'response.Write(strcallback & "(" & output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}") & ")")
        response.Write(output)
       
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer, ByVal id As String) As Collection(Of clsAssetLocationForInnerDetails)
        Dim users As New Collection(Of clsAssetLocationForInnerDetails)()
        Dim ds As New DataSet
       
        Dim param(0) As SqlParameter
     
        param(0) = New SqlParameter("@categoryID", SqlDbType.NVarChar, 200)
        param(0).Value = id
      
        
        ds = ObjSubsonic.GetSubSonicDataSet("GET_ASTCOUNT_BYCATEGORY", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New clsAssetLocationForInnerDetails
                ' user.AssetCount = ds.Tables(0).Rows(i).Item("AAS_AAT_CODE").ToString
                'user.AssetName = ds.Tables(0).Rows(i).Item("PRODUCTNAME").ToString
                users.Add(user)
            Next
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal clsAssetLocationForInnerDetails As Collection(Of clsAssetLocationForInnerDetails), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Summary As clsAssetLocationForInnerDetails In clsAssetLocationForInnerDetails
            Dim row As New JQGridRow()
            row.cell = New String(1) {}
            row.id = Summary.AssetCount.ToString()
            row.cell(1) = Summary.AssetCount.ToString()
            row.cell(0) = Summary.AssetName.ToString()
            rows.Add(row)
        Next
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords
        result.records = totalRecords
        Return New JavaScriptSerializer().Serialize(result)
    End Function
End Class