<%@ WebHandler Language="VB" Class="ViewAssetREQreport2" %>
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports System

Public Class ViewAssetREQreport2 : Implements IHttpHandler
    
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response
        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        Dim nd As String = request("nd")
        Dim id As String = request("ct")
        
        Dim totalRecords As Integer
        Dim cslAssetReqreport As Collection(Of cslAssetReqreport) = GetStoresForUserDefinedRoutes(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords, id)
       
       
        Dim output As String = BuildJQGridResults(cslAssetReqreport, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        ' response.Write(output.Replace("\", "").Replace(""":[""", """:{""").Replace("""]}", """}}"))
        response.Write(output)
    End Sub
 
    Private Function GetStoresForUserDefinedRoutes(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer, ByVal id As String) As Collection(Of cslAssetReqreport)
        Dim users As New Collection(Of cslAssetReqreport)()
        Dim ds As New DataSet
       
        Dim param(0) As SqlParameter
     
        param(0) = New SqlParameter("@status", SqlDbType.NVarChar, 200)
        param(0).Value = id
       
        ds = ObjSubsonic.GetSubSonicDataSet("GET_STATDTLS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim user As New cslAssetReqreport
                
                user.AIR_REQ_ID = ds.Tables(0).Rows(i).Item("Requisition Id").ToString
                user.sta = ds.Tables(0).Rows(i).Item("Status").ToString
                users.Add(user)
            Next
            totalRecords = CInt(ds.Tables(0).Rows.Count)
        End If
        Return users
    End Function
    
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Private Function BuildJQGridResults(ByVal cslAssetReqreport As Collection(Of cslAssetReqreport), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String
        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each Summary As cslAssetReqreport In cslAssetReqreport
            Dim row As New JQGridRow()
            row.cell = New String(1) {}
            row.id = Summary.AIR_REQ_ID.ToString()
            row.cell(0) = Summary.AIR_REQ_ID.ToString()
            row.cell(1) = Summary.sta.ToString()
            rows.Add(row)
        Next
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords
        result.records = totalRecords
        Return New JavaScriptSerializer().Serialize(result)
    End Function

End Class
