Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager

Partial Class AdminFunctions_ADF_WebFiles_frmadfprivileges
    Inherits System.Web.UI.Page
#Region "Variables"
    Dim ObjDR As SqlDataReader
#End Region
    Private Sub page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            strSQL = "AMT_GetRoleDtls_SP"

            ObjDR = SqlHelper.ExecuteReader(CommandType.StoredProcedure, strSQL)

            lblPrevileges.Text = lblPrevileges.Text & "<table style=""BORDER-COLLApSE: collapse;"" width=95% align=center border=1 cellpading=0 cellspacing=0><tr><td class=clsTblhead>Sl.No.</td><td class=clsTblhead>Role Acronym</td><td class=clsTblhead>Role Description</td></tr>"
            Dim iSno As Integer = 1
            While ObjDR.Read()
                lblPrevileges.Text = lblPrevileges.Text & "<tr><td >" & iSno & "</td><td >" & ObjDR.GetValue(2) & "</td><td ><a href=frmADFaddprivileges.aspx?rid=" & ObjDR.GetValue(0) & "&act=N>" & ObjDR.GetValue(1).ToString() & "</a></td></tr>"
                iSno = iSno + 1
            End While
            lblPrevileges.Text = lblPrevileges.Text & "<td><br></td></table>"
            ObjDR.Close()
        Catch ex As System.Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Provillages", "Load", ex)
        End Try

    End Sub

End Class
