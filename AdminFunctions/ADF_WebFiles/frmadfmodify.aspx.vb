Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Data.SqlClient
Partial Class AdminFunctions_ADF_WebFiles_frmadfmodify
    Inherits System.Web.UI.Page

#Region "Variables Declarations"
    Dim strsql As String
    Dim stastr As String
    Dim tcctm As String
    Dim uprm_id As Integer
    Dim ObjDR As SqlDataReader

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If

        If Not Page.IsPostBack Then
            ClearPnls(pnlEmpDtls)
            fillcombos()
            bindlist()
        End If

    End Sub
    Public Sub fillcombos()
        Try
            'Locations Load
            strsql = "select LCM_ID,LCM_CODE+'-'+LCM_Name LCM_NAME  from " & Session("TENANT") & "." & "Tbl_Location_Master  Where LCM_STA_ID=1 Order by LCM_NAME "
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            cmbLoc.DataSource = ObjDR
            cmbLoc.DataTextField = "LCM_Name"
            cmbLoc.DataValueField = "LCM_ID"
            cmbLoc.DataBind()
            ' cmbLoc.Items.Insert(0, "--All--")
            ObjDR.Close()
            'selecting Employee Location
            strsql = "select LCM_ID,LCM_CODE from  " & Session("TENANT") & "." & "AmantraUsers, " & Session("TENANT") & "." & "BUILDING," & Session("TENANT") & "." & "Tbl_Location_Master" & _
                    " where  LCM_ID=PRM_LCM_ID and PRM_ID=AUR_PRM_ID and AUR_ID='" & Session("uid") & "'"
            Dim lid As Integer
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            While ObjDR.Read
                lid = ObjDR("LCM_ID")
            End While
            For i As Integer = 1 To cmbLoc.Items.Count - 1
                If cmbLoc.Items(i).Value = lid Then
                    cmbLoc.SelectedIndex = i
                    Exit For
                End If
            Next
            'Response.Write(cmbLoc.SelectedItem.Value)
            'Response.End()
            ObjDR.Close()
            'Cost center Load
            strsql = "select CCTM_CODE from " & Session("TENANT") & "." & "Tbl_Cost_Center_Master Where CCTM_STA_ID=1 order by CCTM_CODE "
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            cmbCenter.DataSource = ObjDR
            cmbCenter.DataTextField = "CCTM_CODE"
            cmbCenter.DataValueField = "CCTM_CODE"
            cmbCenter.DataBind()
            cmbCenter.Items.Insert(0, "--All--")
            ObjDR.Close()
            strsql = "select  CCTM_CODE from " & Session("TENANT") & "." & "Tbl_Cost_Center_Master," & Session("TENANT") & "." & "AmantraUsers where CCTM_CODE=AUR_CCTM_CODE " & _
                     " and AUR_ID='" & Session("uid") & "'"
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            ' selecting Department
            Dim icnt As Integer
            icnt = 0
            Dim cid As String
            While ObjDR.Read
                cid = ObjDR("CCTM_CODE")
                icnt += 1
            End While
            If icnt = 0 Then
                cid = "FMG-MUM"
            End If
            For i As Integer = 1 To cmbCenter.Items.Count - 1
                If cmbCenter.Items(i).Value = cid Then
                    cmbCenter.SelectedIndex = i
                    Exit For
                End If
            Next
            ObjDR.Close()
            'Status Load
            strsql = "select PRM_ID,PRM_NAME from " & Session("TENANT") & "." & "Tbl_Premise_master," & Session("TENANT") & "." & "Tbl_Location_Master Where PRM_STA_ID=1 and PRM_LCM_ID=LCM_ID and PRM_LCM_ID=" & cmbLoc.SelectedItem.Value & " order by PRM_NAME"
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            cmbPremise.DataSource = ObjDR
            cmbPremise.DataTextField = "PRM_NAME"
            cmbPremise.DataValueField = "PRM_ID"
            cmbPremise.DataBind()
            cmbPremise.Items.Insert(0, "--All--")
            ObjDR.Close()
            strsql = "select PRM_ID from  " & Session("TENANT") & "." & "BUILDING," & Session("TENANT") & "." & "AmantraUsers where AUR_PRM_ID=PRM_ID and AUR_ID='" & Session("uid") & "'"
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            Dim pid As Integer
            '        Dim i As Integer
            While ObjDR.Read
                pid = ObjDR("PRM_ID")
            End While
            For i As Integer = 1 To cmbPremise.Items.Count - 1
                If cmbPremise.Items(i).Value = pid Then
                    cmbPremise.SelectedIndex = i
                    Exit For
                End If
            Next
            ObjDR.Close()
            'getdetails()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Public Sub fillmodifycombos()
        Try
            'Locations Load
            'strsql = "select LCM_ID,LCM_CODE+'-'+LCM_Name LCM_NAME  from Tbl_Location_Master  Where LCM_STA_ID=1 Order by LCM_NAME"
            strsql = "select distinct LCM_ID,LCM_CODE+'-'+LCM_Name LCM_NAME from " & Session("TENANT") & "." & "Tbl_Location_Master," & Session("TENANT") & "." & "BUILDING Where LCM_ID=PRM_LCM_ID and LCM_STA_ID=1"
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            cboLocation.DataSource = ObjDR
            cboLocation.DataTextField = "LCM_Name"
            cboLocation.DataValueField = "LCM_ID"
            cboLocation.DataBind()

         
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub loaduserdefaults()
        Try
            strsql = "select distinct AUR_PRM_ID,Ltrim(Rtrim(AUR_CCTM_CODE)) AUR_CCTM_CODE " & _
            " from  " & Session("TENANT") & "."  & "AMT_AMANtrAUSER_VW where AUR_ID='" & Trim(lstEMP.SelectedItem.Value) & "'"
            'Dim dtNow As DateTime = getoffsetdate(Date.Today)
            'Response.Write(strsql)
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            Dim i As Integer

            While ObjDR.Read
                uprm_id = ObjDR("AUR_PRM_ID")
                tcctm = ObjDR("AUR_CCTM_CODE")
            End While
            ObjDR.Close()

            strsql = "select PRM_ID,PRM_NAME from " & Session("TENANT") & "." & "Tbl_Premise_master Where PRM_STA_ID=1 Order by PRM_NAME"
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            cmbPrm.DataSource = ObjDR
            cmbPrm.DataTextField = "PRM_NAME"
            cmbPrm.DataValueField = "PRM_ID"
            cmbPrm.DataBind()
            ObjDR.Close()

            strsql = "select PRM_ID from  " & Session("TENANT") & "." & "BUILDING," & Session("TENANT") & "." & "AmantraUsers where AUR_PRM_ID=PRM_ID and AUR_PRM_ID=" & uprm_id
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            Dim pid As Integer
            While ObjDR.Read
                pid = ObjDR("PRM_ID")
            End While
            ObjDR.Close()
            For i = 0 To cmbPrm.Items.Count - 1
                If cmbPrm.Items(i).Value = uprm_id Then
                    cmbPrm.SelectedIndex = i
                    Exit For
                End If
            Next

            strsql = "select CCTM_CODE from " & Session("TENANT") & "." & "Tbl_Cost_Center_Master Where " & _
                     " CCTM_STA_ID=1 order by CCTM_CODE "
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            cmbCCenter.DataSource = ObjDR
            cmbCCenter.DataTextField = "CCTM_CODE"
            cmbCCenter.DataValueField = "CCTM_CODE"
            cmbCCenter.DataBind()
            ObjDR.Close()

            strsql = "select  CCTM_CODE from " & Session("TENANT") & "." & "Tbl_Cost_Center_Master," & Session("TENANT") & "." & "AmantraUsers where CCTM_CODE=AUR_CCTM_CODE " & _
                     " and AUR_PRM_ID=" & uprm_id

            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            ' selecting Department

            Dim icnt As Integer = 0
            Dim cid As String
            While ObjDR.Read
                cid = ObjDR("CCTM_CODE")
            End While
            ObjDR.Close()
            For i = 0 To cmbCCenter.Items.Count - 1
                If cmbCCenter.Items(i).Value = tcctm Then
                    cmbCCenter.SelectedIndex = i
                    Exit For
                End If
            Next
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Public Sub bindlist()
        Try
            If cmbCenter.SelectedItem.Text = "--All--" And cmbPremise.SelectedItem.Text = "--All--" Then
                strsql = " select AUR_ID,AUR_ID+'/'+AUR_FIRST_NAME as EMP from " & Session("TENANT") & "." & "AmantraUsers "
            ElseIf cmbCenter.SelectedItem.Text <> "--All--" And cmbPremise.SelectedItem.Text = "--All--" Then
                strsql = " select AUR_ID,AUR_ID+'/'+AUR_FIRST_NAME as EMP from " & Session("TENANT") & "." & "AmantraUsers " & _
                         " where AUR_cctm_code='" & cmbCenter.SelectedItem.Value & "'"
            ElseIf cmbCenter.SelectedItem.Text = "--All--" And cmbPremise.SelectedItem.Text <> "--All--" Then
                strsql = " select AUR_ID,AUR_ID+'/'+AUR_FIRST_NAME as EMP from " & Session("TENANT") & "." & "AmantraUsers " & _
                         " where AUR_PRM_ID=" & cmbPremise.SelectedItem.Value
            Else
                strsql = " select AUR_ID,AUR_ID+'/'+AUR_FIRST_NAME as EMP from " & Session("TENANT") & "." & "AmantraUsers " & _
                         " where AUR_PRM_ID=" & cmbPremise.SelectedItem.Value & _
                         " and AUR_cctm_code='" & cmbCenter.SelectedItem.Value & "'"
            End If
            'Response.Write(strsql)

            'Response.End()
            lstEMP.Items.Clear()
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            'Objda = New SqlDataAdapter(ObjComm)
            'Objda.Fill(ds, "emp")
            'lstEMP.DataTextField = ds
            lstEMP.DataSource = ObjDR
            lstEMP.DataTextField = "EMP"
            lstEMP.DataValueField = "AUR_ID"
            lstEMP.DataBind()
            ObjDR.Close()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            strsql = "Update " & HttpContext.Current.Session("TENANT") & "." & "AmantraUsers set AUR_FIRST_NAME='" & Replace(Trim(txtfname.Text), "'", "''") & "',AUR_LAST_NAME='" & _
                     Replace(Trim(txtlname.Text), "'", "''") & "',AUR_MIDDLE_NAME='" & Replace(Trim(txtmname.Text), "'", "''") & "',AUR_Known_as='" & _
                     Replace(Trim(txtknown.Text), "'", "''") & "',AUR_EMAIL='" & Replace(Trim(txtemail.Text), "'", "''") & "',AUR_REPORTING_TO='" & _
                     Replace(Trim(txtreport.Text), "'", "''") & "',AUR_BASIC=" & Val(txtbsal.Text) & ", AUR_Direct_Line='" & _
                     Replace(Trim(txtcontact.Text), "'", "''") & "',AUR_EXTENSION='" & Replace(Trim(txtextNo.Text), "'", "''") & "',AUR_ACTIVE='" & cmbstatus.SelectedItem.Value & "'," & _
                     " AUR_PRM_ID=" & cmbPrm.SelectedItem.Value & _
                     ",AUR_CCTM_CODE='" & cmbCCenter.SelectedItem.Value & "',AUR_RES_NUMBER='" & _
                     Replace(Trim(txtMobile.Text), "'", "''") & "' where AUR_ID='" & Replace(Trim(txtid.Text), "'", "''") & "'"
            'Response.Write(strsql)
            SqlHelper.ExecuteNonQuery(CommandType.Text, strsql)
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub cmbLoc_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLoc.SelectedIndexChanged
        Try
            GetPremise()
            ClearPnls(pnlEmpDtls, pnllist)
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub GetPremise()
        Try
            strsql = "select PRM_ID,PRM_NAME from  " & Session("TENANT") & "."  & "BUILDING where PRM_STA_ID=1 and PRM_LCM_ID=" & cmbLoc.SelectedItem.Value
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            cmbPremise.DataSource = ObjDR
            cmbPremise.DataTextField = "PRM_Name"
            cmbPremise.DataValueField = "PRM_ID"
            cmbPremise.DataBind()
            cmbPremise.Items.Insert(0, "--All--")
            ObjDR.Close()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub getdetails()
        Try
            strsql = "SELECT AUR_ID, isnull(AUR_FIRST_NAME,'None')AUR_FIRST_NAME," & _
                   " isnull(AUR_MIDDLE_NAME,'None') AUR_MIDDLE_NAME, isnull(AUR_LAST_NAME ,'None') AUR_LAST_NAME," & _
                   " AUR_Known_as, isnull(AUR_EMAIL,'None') AUR_EMAIL," & _
                   " isnull(AUR_REPORTING_TO,'None') as AUR_REPORTING_TO, AUR_BASIC,AUR_ACTIVE," & _
                   " (select PRM_NAME from  " & Session("TENANT") & "."  & "BUILDING where AUR_PRM_ID=PRM_ID)as Building, " & _
                   " (select LCM_NAME from  " & Session("TENANT") & "."  & "BUILDING,Tbl_Location_Master where" & _
                   " PRM_LCM_ID= LCm_ID and AUR_PRM_ID=PRM_ID)as Loc, isnull(AUR_EXTENSION,'None')AUR_EXTENSION," & _
                   " isnull(AUR_DIRECT_LINE,'None')AUR_DIRECT_LINE,isnull(AUR_RES_NUMBER,'None')AUR_RES_NUMBER, isnull(AUR_CGM_GROUP,'None')AUR_CGM_GROUP," & _
                   " isnull(AUR_CGM_DESC,'None')AUR_CGM_DESC,AUR_ACTIVE," & _
                   " isnull(AUR_CCTM_CODE,'None') AUR_CCTM_CODE,AUR_prm_id,AUR_cctm_code FROM AmantraUsers " & _
                   " where AUR_ID='" & lstEMP.SelectedItem.Value & "'"
            'Response.Write(strsql)
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            While ObjDR.Read
                txtid.Text = ObjDR("AUR_ID")
                txtfname.Text = ObjDR("AUR_FIRST_NAME")
                txtlname.Text = ObjDR("AUR_LAST_NAME")
                txtmname.Text = ObjDR("AUR_MIDDLE_NAME")
                txtknown.Text = ObjDR("AUR_Known_as")
                txtemail.Text = ObjDR("AUR_EMAIL")
                txtreport.Text = ObjDR("AUR_REPORTING_TO")
                txtbsal.Text = ObjDR("AUR_BASIC")
                txtcontact.Text = ObjDR("AUR_DIRECT_LINE")
                txtMobile.Text = ObjDR("AUR_RES_NUMBER")
                txtextNo.Text = ObjDR("AUR_EXTENSION")
                stastr = ObjDR("AUR_ACTIVE")
            End While
            ObjDR.Close()
            For i As Integer = 0 To cmbstatus.Items.Count - 1
                If cmbstatus.Items(i).Value = stastr Then
                    cmbstatus.SelectedIndex = i
                    Exit For
                End If
            Next
            loaduserdefaults()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub lstEMP_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstEMP.SelectedIndexChanged
        Try
            getdetails()
            ShowPnls(pnlEmpDtls)
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub cmbPremise_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPremise.SelectedIndexChanged
        ' bindlist()
        Try
            ClearPnls(pnlEmpDtls, pnllist)
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub cmbCenter_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCenter.SelectedIndexChanged
        ' bindlist()
        Try
            ClearPnls(pnlEmpDtls, pnllist)
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        'strsql = " select AUR_ID,(AUR_ID+'/'+AUR_FIRST_NAME+'/'+ PRM_NAME)as EMP from AmantraUsers, " & Session("TENANT") & "."  & "BUILDING " & _
        ' " where  PRM_ID=AUR_PRM_ID and AUR_cctm_code='" & cmbCenter.SelectedItem.Value & "' and AUR_PRM_ID=" & cmbPremise.SelectedItem.Value
        ''Response.Write(strsql)

        'ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSql)
        'lstEMP.DataSource = ObjDR
        'lstEMP.DataTextField = "EMP"
        'lstEMP.DataValueField = "AUR_ID"
        'lstEMP.DataBind()
        'ObjDR.Close()
        Try
            ShowPnls(pnllist)
            ClearPnls(pnlEmpDtls)
            bindlist()
            txtSearchId.Text = ""
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub ShowPnls(ByVal ParamArray pnlsrc() As Panel)
        Try
            Dim i As Panel
            For Each i In pnlsrc
                i.Visible = True
            Next
            i.Dispose()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub ClearPnls(ByVal ParamArray pnlsrc() As Panel)
        Try
            Dim i As Panel
            For Each i In pnlsrc
                i.Visible = False
            Next
            i.Dispose()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Private Sub cboLocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLocation.SelectedIndexChanged
        'strsql = "select PRM_ID,PRM_NAME from  " & Session("TENANT") & "."  & "BUILDING where PRM_STA_ID=1 and PRM_LCM_ID=" & cboLocation.SelectedItem.Value
        'ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSql)
        'cmbPrm.DataSource = ObjDR
        'cmbPrm.DataTextField = "PRM_Name"
        'cmbPrm.DataValueField = "PRM_ID"
        'cmbPrm.DataBind()
        ' cmbPremise.Items.Insert(0, "--All--")
        ObjDR.Close()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Searchlist()
            ClearPnls(pnlEmpDtls)
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Public Sub Searchlist()
        Try
            If cmbCenter.SelectedItem.Text = "--All--" And cmbPremise.SelectedItem.Text = "--All--" Then
                strsql = " select AUR_ID,AUR_ID+'/'+AUR_FIRST_NAME as EMP from " & Session("TENANT") & "." & "AmantraUsers " & _
                         " where AUR_ID='" & Replace(Trim(txtSearchId.Text), "'", "''") & "'"
            ElseIf cmbCenter.SelectedItem.Value = "--All--" And cmbPremise.SelectedItem.Text <> "--All--" Then
                strsql = " select AUR_ID,AUR_ID+'/'+AUR_FIRST_NAME as EMP from " & Session("TENANT") & "." & "AmantraUsers " & _
                         " where AUR_PRM_ID='" & cmbPremise.SelectedItem.Value & "'" & _
                         " and AUR_ID='" & Replace(Trim(txtSearchId.Text), "'", "''") & "'"
            ElseIf cmbCenter.SelectedItem.Text <> "--All--" And cmbPremise.SelectedItem.Text = "--All--" Then
                strsql = " select AUR_ID,AUR_ID+'/'+AUR_FIRST_NAME as EMP from " & Session("TENANT") & "." & "AmantraUsers " & _
                         " where AUR_cctm_code='" & cmbCenter.SelectedItem.Value & "'" & _
                         " and AUR_ID='" & Replace(Trim(txtSearchId.Text), "'", "''") & "'"
            Else
                strsql = " select AUR_ID,AUR_ID+'/'+AUR_FIRST_NAME as EMP from " & Session("TENANT") & "." & "AmantraUsers " & _
                         " where AUR_PRM_ID='" & cmbPremise.SelectedItem.Value & "'" & _
                         " and AUR_cctm_code='" & cmbCenter.SelectedItem.Value & "'" & _
                         " and AUR_ID='" & Replace(Trim(txtSearchId.Text), "'", "''") & "'"
            End If
            'Response.Write(strsql)
            'Response.End()
            lstEMP.Items.Clear()
            ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strsql)
            'Objda = New SqlDataAdapter(ObjComm)
            'Objda.Fill(ds, "emp")
            'lstEMP.DataTextField = ds
            lstEMP.DataSource = ObjDR
            lstEMP.DataTextField = "EMP"
            lstEMP.DataValueField = "AUR_ID"
            lstEMP.DataBind()
            ObjDR.Close()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub

End Class

