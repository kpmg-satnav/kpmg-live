Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class AdminFunctions_ADF_WebFiles_frmadfrolemain
    Inherits System.Web.UI.Page

#Region "General Variables"
    Dim strSql As String
    Dim daRole As New SqlDataAdapter()
    Dim dsRole As New DataSet()
    Dim drRole As SqlDataReader
    Dim iPro As Integer
    Dim strActive As String
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Put user code to initialize the page here

            If Not IsPostBack Then
                lblmsg.Text = Request.QueryString("strSql")
                BindDataList()
            Else
                lblmsg.Text = ""
            End If
        Catch ex As Exception

            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try


    End Sub

     Public Sub BindDataList()
        Try
            Dim Ds As New DataSet()
            strSql = "SELECT ROL_ID,ROL_ACRONYM, ROL_DESCRIPTION,ROL_ACTIVE,SCP_ID,SCP_DESCRIPTION " & _
                     " FROM  " & Session("TENANT") & "."  & "AMT_ROLESCOPE_VW " & _
                     " ORDER BY ROL_ACRONYM"


            Ds = SqlHelper.ExecuteDataset(CommandType.Text, strSql)
            grdProcess.DataSource = Ds
            grdProcess.DataBind()

            grdProcess.Visible = True
        Catch ex As Exception
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    
   

    Protected Sub grdProcess_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdProcess.PageIndexChanging
        grdProcess.PageIndex = e.NewPageIndex
        BindDataList()
    End Sub

    Protected Sub grdProcess_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdProcess.RowCommand
        Try
            Dim index As Integer
            Dim lbtnStatus As LinkButton = grdProcess.Rows(index).Cells(1).Controls(0)
            Dim str As String
            str = lbtnStatus.Text
            If str = "Y" Or str = "N" Then
            Else
                Exit Sub
            End If

            iPro = grdProcess.Rows(index).Cells(0).Text

            Select Case Trim(lbtnStatus.Text)
                Case "Y"
                    lbtnStatus.Text = "N"

                    strSql = "UPDATE  " & Session("TENANT") & "."  & "ROLE SET ROL_ACTIVE='N' WHERE ROL_ID=" & iPro


                    SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)
                    BindDataList()

                Case "N"
                    lbtnStatus.Text = "Y"

                    strSql = "UPDATE  " & Session("TENANT") & "."  & "ROLE SET ROL_ACTIVE='Y' WHERE ROL_ID=" & iPro
                    SqlHelper.ExecuteNonQuery(CommandType.Text, strSql)
                    BindDataList()
                Case Else
                    Exit Sub
            End Select
            BindDataList()
        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 15) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
End Class

