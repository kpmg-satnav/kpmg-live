Imports System.Data
Imports System.Data.SqlClient

Partial Class AdminFunctions_ADF_WebFiles_frmADFAddNewRole
    Inherits System.Web.UI.Page
    Dim obj As New clsMasters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            lblError.Text = ""
            If Not Page.IsPostBack Then
                obj.Role_LoadGrid(gvItem)
                trCName.Visible = False
                rbActions.Checked = True
                obj.BindRole(ddlRole)
                lblError.Visible = True
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Role", "Load", ex)

        End Try

    End Sub

    Private Sub Cleardata()
        txtDescription.Text = String.Empty
        txtRoleAcronym.Text = String.Empty
        txtRemarks.Text = String.Empty
        ddlRole.SelectedIndex = 0
    End Sub

    Private Sub insertdata()
        Try
            obj.getcode = txtRoleAcronym.Text
            obj.getname = txtDescription.Text
            obj.getRemarks = txtRemarks.Text
            intResult = obj.InsertRole(Me)
            If intResult = 1 Then
                lblError.Text = "The Role Code Already Exists, Please Modify Code."
            ElseIf intResult = 2 Then
                lblError.Text = "The Role Name Already Exists, Please Modify Name."
            ElseIf intResult = 3 Then
                Cleardata()
                lblError.Text = "Record has been inserted"
            End If
            Cleardata()
            obj.Role_LoadGrid(gvItem)
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while inserting data to database", "Role", "Load", ex)
        End Try


    End Sub

    Private Sub Modifydata()
        obj.getname = txtDescription.Text
        obj.getRemarks = txtRemarks.Text
        intResult = obj.ModifyRole(ddlRole, Me)
        If intResult = 4 Then
            lblError.Text = "Record has been modified"
        Else
            lblError.Text = "failed to modify"
        End If
        Cleardata()
        obj.Role_LoadGrid(gvItem)
    End Sub
    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged

        If rbActions.Checked = True Then

            trCName.Visible = False
            txtRoleAcronym.ReadOnly = False
            btnSubmit.Text = "Submit"
            Cleardata()
        Else
            Cleardata()
            trCName.Visible = True
            txtRoleAcronym.ReadOnly = True
            btnSubmit.Text = "Modify"
            obj.BindRole(ddlRole)
        End If

    End Sub
    Dim intResult As Integer
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strErrorMsg As String = String.Empty
        Try
            If rbActions.Checked = True Then
                strErrorMsg = "This error has been occured while Inserting data"
                btnSubmit.Text = "Submit"
                If txtRoleAcronym.Text = String.Empty Or txtDescription.Text = String.Empty Or txtRemarks.Text = String.Empty Then

                    lblError.Text = "Enter mandatory fields"
                    Exit Sub
                ElseIf txtRemarks.Text.Length > 500 Then

                    lblError.Text = "Enter Remarks in less than or equal to 500 characters"
                    Exit Sub
                Else
                    insertdata()
                End If
            Else
                strErrorMsg = "This error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If ddlRole.SelectedItem.Value = "--Select--" Or txtRoleAcronym.Text = String.Empty Or txtDescription.Text = String.Empty Or txtRemarks.Text = String.Empty Then
                    lblError.Text = "Enter mandatory fields"
                    Exit Sub
                ElseIf txtRemarks.Text.Length > 500 Then
                    lblError.Text = "Enter Remarks in less than or equal to 500 characters"
                    Exit Sub
                Else
                    Modifydata()
                    obj.BindRole(ddlRole)
                End If
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException(strErrorMsg, "Role", "Load", ex)
        End Try


    End Sub

    Protected Sub ddlRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRole.SelectedIndexChanged
        Try
            If ddlRole.SelectedValue <> "--Select--" Then
                obj.role_SelectedIndex_Changed(ddlRole)
                txtRoleAcronym.Text = obj.getcode
                txtDescription.Text = obj.getname
                txtRemarks.Text = obj.getRemarks
            Else
                Cleardata()
                obj.BindRole(ddlRole)
            End If
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Role", "Load", ex)
        End Try
    End Sub

    Protected Sub gvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        Try
            gvItem.PageIndex = e.NewPageIndex
            'gvItem.Columns(2).Visible = True
            'gvItem.Columns(3).Visible = True
            obj.Role_LoadGrid(gvItem)
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Role", "Load", ex)
        End Try


    End Sub

    Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItem.RowCommand
        Try
            If e.CommandName = "Status" Then
                Dim index As Integer = CType(e.CommandArgument, Integer)
                obj.Role_Rowcommand(gvItem, index)
            End If
            obj.BindRole(ddlRole)
            obj.Role_LoadGrid(gvItem)
            Cleardata()
        Catch ex As Exception
            Throw New Amantra.Exception.DataException("This error has been occured while retrieving data from database", "Role", "Load", ex)
        End Try
    End Sub

End Class
