﻿<%@ Page Language="VB" AutoEventWireup="false" %>

<!DOCTYPE html>
<script runat="server">


</script>

<html data-ng-app="QuickFMS" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Messenger/messenger.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/qtip/qtip.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/UserRoleMapping.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/Scripts/angular-confirm.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <link href="../../BootStrapCSS/colorpicker.min.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function setup(id) {
            $('.date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                orientation: 'bottom'
            });
        };
    </script>
</head>
<body data-ng-controller="UserRoleMappingController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="User & Role Mapping" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">User & Role Mapping</h3>
                </div>
                <div class="clearfix" style="padding-left: 30px; padding-top: 15px" data-ng-show="ViewUploption">
                    <form role="form" id="FileUsrUpl" name="FileUsrUpl" data-valid-submit="UploadFile()" novalidate>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <a href="../../Masters/Mas_Webfiles/User_Role_Mapping_Template.xlsx">Click here to download template</a>
                                </div>
                                <div class="form-group">
                                    <div class="btn-default">
                                        <label>Upload File </label>
                                        <input type="file" name="UPLFILE" id="FileUpl" required="" />
                                    </div>
                                </div>
                            </div>

                            <br />
                            <br />
                            <%--  <div class="col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="Label1" runat="server" ng-model="name" Text="Total Employee Count:" Font-Bold="true"></asp:Label><span ng-bind="name"></span>

                                     </div>--%>


                            <div class="col-md-5 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div>
                                        <input type="submit" id="btnUpload" class="btn btn-primary custom-button-color" value="Upload" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <a id="HyperLink11" href="../../Masters/Mas_Webfiles/frmNewUser.aspx" onmouseover="Tip('Add New User')" onmouseout="UnTip()"><i class="fa fa-user fa-2x"></i></a>
                            </div>

                        </div>
                        <br />

                        <div>
                        </div>
                    </form>
                </div>

                <div class="clearfix" style="padding-left: 10px;">

                    <div style="height: 530px" data-ng-show="Viewstatus==0">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <input id="filtertxt" class="form-control" placeholder="Filter by any..." type="text" style="width: 50%" />
                            </div>
                            <div class="col-sm-5">
                                <div>
                                    <label><b>Total Employee Count: </b></label>
                                    <a href=""><b><span ng-bind="name"></span></b></a>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="pull-right">
                                    <a data-ng-click="GenReport(CurrentProfileEmp,'xlsx')" data-ng-show="Viewstatus==0"><i id="I2" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right" style="padding-right: 45px"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row"></div>
                        <div data-ag-grid="gridOptions" style="height: 500px;" class="ag-blue"></div>
                    </div>
                    <div style="height: 530px" data-ng-show="Viewstatus==2">
                        <input id="Text1" class="form-control" placeholder="Filter by any..." type="text" style="width: 25%" />
                        <div data-ag-grid="gridOptions1" style="height: 100%;" class="ag-blue"></div>
                    </div>
                </div>

                <div data-ng-show="Viewstatus==1" class="container-fluid">
                    <div class="panel-body" style="padding-right: 10px;">
                        <div class="clearfix">
                            <div class="box-footer text-right">
                                <span style="color: red;">&nbsp; &nbsp;  <span style="color: red;">**</span> Select to auto fill the data
                            </div>
                        </div>
                        <div class="container" style="padding-top: 10px;">
                            <div class="stepwizard">
                                <div class="stepwizard-row setup-panel">
                                    <div class="stepwizard-step" data-ng-show="EmployeeDetails">
                                        <a href="#step-1" id="stp1" type="button" class="btn btn-primary btn-circle">Employee Details</a>
                                    </div>
                                    <div class="stepwizard-step" data-ng-show="RoleAssignment">
                                        <a href="#step-2" id="stp2" type="button" ng-click='LoadRMdetails_Step2()' class="btn btn-default btn-circle">Role Assignment</a>
                                    </div>
                                    <div class="stepwizard-step" data-ng-show="Mapping">
                                        <a href="#step-3" id="stp3" type="button" ng-click="LoadMappingDtls_Step3()" class="btn btn-default btn-circle">Mapping</a>
                                    </div>
                                </div>
                            </div>
                            <form role="form" id="EmpDetails" name="frmEmpDetails">
                                <input type="hidden" id="AUR_ID_NEW" class="form-control">
                                <div class="row setup-content" id="step-1">
                                    <br />
                                    <div class="row">
                                        <%--<div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label><b>Employee ID</b></label>
                                                        <div>{{CurrentProfileEmp.AUR_ID}}</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label><b>Name</b> </label>
                                                        <br />
                                                        {{CurrentProfileEmp.AUR_KNOWN_AS}}                                          
                                                    </div>
                                                </div>--%>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>User ID</b><span style="color: red;">*</span></label>
                                                <input id="UserId" type="text" class="form-control" data-ng-model="CurrentProfileEmp.NEW_AUR_ID" name="NEW_AUR_ID" required />
                                                <span class="error" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.NEW_AUR_ID.$invalid" style="color: red">Please enter Employee ID</span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Name</b> </label>
                                                <br />
                                                <input type="text" id="Username" class="form-control" data-ng-model="CurrentProfileEmp.AUR_KNOWN_AS" />
                                            </div>
                                        </div>


                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Email</b></label>
                                                <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.AUR_EMAIL.$invalid}">
                                                    <input type="text" class="form-control" data-ng-model="CurrentProfileEmp.AUR_EMAIL" name="AUR_EMAIL" ng-pattern="/^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z-.]+\.[a-zA-Z.]{2,5}$/" />
                                                    <span class="error" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.AUR_EMAIL.$invalid" style="color: red">Please enter valid Email Id</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Phone No</b> </label>

                                                <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.AUR_RES_NUMBER.$invalid}">
                                                    <input type="text" class="form-control" data-ng-model="CurrentProfileEmp.AUR_RES_NUMBER" maxlength="11" minlength="10" name="AUR_RES_NUMBER" ng-pattern="/^[0-9]+$/" />
                                                    <span class="error" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.AUR_RES_NUMBER.$invalid" style="color: red">Please Enter 10 digit Mobile Number</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Date Of Birth</b></label>
                                                <div class="input-group date" id='frmdate'>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('frmdate')"></span>
                                                    </span>
                                                    <%-- <div>
                                                                <input type="text" class="form-control" id="startDate" ng-model="frmdate" />
                                                                   </div>--%>
                                                    <input class="form-control" id="txtreqfrmdate" data-ng-model="CurrentProfileEmp.AUR_DOB" name="AUR_DOB" type="text" />
                                                    <%--  <asp:CompareValidator ID="cmpcalender" runat="server" Type="Date" ControlToCompare="txtdoj" ControlToValidate="txtreqfrmdate" Operator="GreaterThan" ErrorMessage="Date Of Birth must be greater than Date of Joining"></asp:CompareValidator>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Date Of Joining</b></label>
                                                <%-- <div>
                                                          <input type="text" class="form-control" 
                                                                 id="endDate" ng-model="doj" 
                                                                    ng-change='checkErr(frmdate,doj)' />

                                                                            </div>--%>
                                                <div class="input-group date" id='doj'>
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar" onclick="setup('doj')"></i>
                                                    </div>
                                                    <input class="form-control" id="txtdoj" data-ng-model="CurrentProfileEmp.AUR_DOJ" name="AUR_DOJ" type="text" />

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Password</b></label>
                                                <select ng-show="false" data-ng-model="CurrentProfileEmp.AUR_COUNTRY" id="ddlcountry" class="form-control" name="AUR_COUNTRY" data-live-search="true">
                                                    <option value="">--Select--</option>
                                                    <option data-ng-repeat="cnt in Countrylst" value="{{cnt.CNY_CODE}}">{{cnt.CNY_NAME}}</option>
                                                </select>
                                                <input type="password" class="form-control" data-ng-model="CurrentProfileEmp.AUR_PASSWORD" name="AUR_PASSWORD" />
                                                <span class="error" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.AUR_PASSWORD.$invalid" style="color: red">Please enter Password</span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <%-- <div class="form-group">
                                                        <label><b>Reporting To</b></label>
                                                        <select data-ng-model="CurrentProfileEmp.AUR_REPORTING_TO" id="ddlrepTo"
                                                            name="AUR_REPORTING_TO" class="form-control selectpicker" data-live-search="true">
                                                            <option value="">--Select--</option>
                                                            <option data-ng-repeat="usr in allUsers" value="{{usr.AUR_ID}}">{{usr.NAME}}</option>
                                                        </select>
                                                    </div>--%>

                                            <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.AUR_REPORTING_TO.$invalid}">
                                                <div class="form-group">
                                                    <label>Reporting To</label>
                                                    <%--  <input type="text" name="name" class="form-control"
                                                            ng-model="main.name"
                                                           
                                                            required >--%>
                                                </div>
                                                <div angucomplete-alt
                                                    id="ex7"
                                                    placeholder="Search Employee"
                                                    pause="500"
                                                    selected-object="selectedEmp"
                                                    remote-url="../../../api/Utility/GetEmployeeNameAndID"
                                                    remote-url-request-formatter="remoteUrlRequestFn"
                                                    remote-url-data-field="items"
                                                    title-field="NAME"
                                                    minlength="2"
                                                    input-class="form-control"
                                                    match-class="highlight"
                                                    required>
                                                </div>
                                                <input type="text" id="txtrptTo" data-ng-model="CurrentProfileEmp.AUR_REPORTING_TO" style="display: none" name="AUR_REPORTING_TO" />

                                                <span class="error" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.AUR_REPORTING_TO.$invalid" style="color: red">Please Enter Reporting To</span>
                                            </div>

                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Extension No</b></label>
                                                <input type="text" class="form-control" data-ng-model="CurrentProfileEmp.AUR_EXTENSION" />
                                                <%--<asp:RequiredFieldValidator ID="rfvext" runat="server" ControlToValidate="text" ErrorMessage="Please Enter Extension No" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Designation</b></label><br />
                                                <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.DSG_CODE.$invalid}">
                                                    <select data-ng-model="CurrentProfileEmp.DSG_CODE" class="selectpicker" id="DSG"
                                                        name="DSG_CODE" cssclass="form-control selectpicker" data-live-search="true">
                                                        <option value="">--Select--</option>
                                                        <option data-ng-repeat="dsg in designationddl" value="{{dsg.DSG_CODE}}">{{dsg.DSG_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.DSG.$invalid" style="color: red">Please select designation</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>{{BsmDet.Parent}}</b></label><br />
                                                <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.VERTICAL.$invalid}">
                                                    <select data-ng-model="CurrentProfileEmp.VERTICAL" class="selectpicker" id="VERTICAL"
                                                        name="VER_CODE" cssclass="form-control selectpicker" data-ng-change="VerChange(CurrentProfileEmp.VERTICAL)" data-live-search="true">
                                                        <option value="">--Select--</option>
                                                        <option data-ng-repeat="ver in verticalddl" value="{{ver.VER_CODE}}">{{ver.VER_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.VERTICAL.$invalid" style="color: red">Please select Vertical</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>{{BsmDet.Child}}</b></label><br />
                                                <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.COSTCENTER.$invalid}">
                                                    <select data-ng-model="CurrentProfileEmp.COSTCENTER" class="selectpicker" id="COSTCENTER"
                                                        name="Cost_Center_Code" cssclass="form-control selectpicker" data-live-search="true">
                                                        <option value="">--Select--</option>
                                                        <option data-ng-repeat="cos in costcenterddl" value="{{cos.Cost_Center_Code}}">{{cos.Cost_Center_Name}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.COSTCENTER.$invalid" style="color: red">Please select CostCenter</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Status</b></label><br />
                                                <select data-ng-model="CurrentProfileEmp.AUR_STA_ID" class="selectpicker" id="ddlusrStatus" data-ng-change="Alert()"
                                                    name="AUR_STA_ID" cssclass="form-control selectpicker" data-live-search="true">
                                                    <option value="">--Select--</option>
                                                    <option data-ng-repeat="sts in usrStatus" value="{{sts.value}}">{{sts.Name}}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Location</b></label><br />
                                                <div data-ng-class="{'has-error': frmEmpDetails.$submitted && frmEmpDetails.Location.$invalid}">
                                                    <select data-ng-model="CurrentProfileEmp.LOCATION" class="selectpicker" id="LOCATION"
                                                        name="LCM_CODE" cssclass="form-control selectpicker" data-live-search="true">
                                                        <option value="">--Select--</option>
                                                        <option data-ng-repeat="lcm in locationddl" value="{{lcm.LCM_CODE}}">{{lcm.LCM_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmEmpDetails.$submitted && frmEmpDetails.Location.$invalid" style="color: red">Please select Location</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Remarks</b> </label>
                                                <br />
                                                <textarea rows="1" cols="10" id="UserRemarks" class="form-control" data-ng-model="CurrentProfileEmp.AUR_EMP_REMARKS"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                    <input type="button" value="Back" class="btn btn-primary pull-right" data-ng-click="back()">
                                    <input type="submit" class="btn btn-primary nextBtn pull-right" value="Save & Continue" ng-show="SAVE_Continue" data-ng-click="SaveEmpDetails()">
                                    <input type="button" class="btn btn-primary pull-right" value="Save" ng-show="SAVE" data-ng-click="SaveEmpDetailsonly()" />
                                </div>
                            </form>
                            <form role="form" id="frmMRdetails" name="frmMREmp" data-valid-submit="SaveRMDetls()" novalidate>
                                <div class="row setup-content" id="step-2">
                                    <br />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <label><b>Module</b> <span style="color: red;">*</span></label>
                                            <select id="module" data-ng-model="Currentmodule.MOD_NAME" data-ng-change="accessbymodule(Currentmodule.MOD_NAME)"
                                                class="form-control" name="MOD_NAME" data-live-search="true">
                                                <option value="ALL" selected>ALL</option>
                                                <option data-ng-repeat="mod in modules" value="{{mod.MOD_NAME}}">{{mod.MOD_NAME}}</option>
                                            </select>
                                        </div>
                                        <br />
                                        <br />
                                    </div>
                                    <br />
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12" data-ng-disabled="!RolStatus">
                                            <div class="form-group">
                                                <label><b>Access Level</b> <span style="color: red;">*</span></label>
                                                <div data-ng-class="{'has-error': frmMREmp.$submitted && frmMREmp.AUR_RES_NUMBER.$invalid}">
                                                    <div data-ng-repeat="obj in acceslevel">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <label>
                                                                <input type="checkbox" data-ng-model="obj.isChecked" ng-value="{{obj.isChecked}}" ng-change="Rolechk()" />
                                                                {{obj.ROL_DESCRIPTION}}                                                                                 
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--{{(acceslevel | filter: {isChecked :  true}).length}}--%>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                    <div class="row" data-ng-show="SpaceVisible == true">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label><b>Map Level Access</b></label>
                                                <br />
                                                <input type="radio" name="userdetails" value="Y" ng-model="Currentmodule.MapAccess" />&nbsp View & Edit &nbsp&nbsp&nbsp&nbsp
                                                        <input type="radio" name="userdetails" value="N" ng-model="Currentmodule.MapAccess" />&nbsp Only View
                                                        <br />
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                    <span class="error" data-ng-show="RolValidation == false" style="color: red">Please Select Atleast One Role to Assign</span>
                                    <%--<input type="submit" class="btn btn-primary nextBtn btn-lg pull-right" data-ng-disabled="RolValidation == 'true'"  value="Save & Continue" />--%>
                                    <input type="button" value="Back" class="btn btn-primary pull-right" data-ng-click="back()">
                                    <button type="submit" id="btnsave" class="btn btn-primary nextBtn pull-right" data-ng-disabled="RolValidation == false" value="Save & Continue">Save & Continue</button>
                                </div>
                            </form>
                            <form role="form" id="frmMappingDetails" name="frmMappingEmp" data-valid-submit="SaveMappingDetls()" novalidate>
                                <div class="row setup-content" id="step-3">
                                    <br />
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.CNY_NAME.$invalid}">
                                                <label for="txtcode">Country <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Countrylst" data-output-model="UserRole.selectedCountries" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME maker"
                                                    data-on-item-click="CountryChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="cnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedCountries" name="CNY_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.CTY_NAME.$invalid}">
                                                <label for="txtcode">City <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Citylst" data-output-model="UserRole.selectedCities" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                                    data-on-item-click="CityChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="ctySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedCities" name="CTY_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.CTY_NAME.$invalid " style="color: red">Please select city </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.LCM_NAME.$invalid}">
                                                <label for="txtcode">Location <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Locationlst" data-output-model="UserRole.selectedLocations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                                    data-on-item-click="LocChange()" data-on-select-all="LCMChangeAll()" data-on-select-none="lcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedLocations" name="LCM_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.TWR_NAME.$invalid}">
                                                <label for="txtcode">Tower <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Towerlist" data-output-model="UserRole.selectedTowers" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME maker"
                                                    data-on-item-click="TwrChange()" data-on-select-all="TwrChangeAll()" data-on-select-none="twrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedTowers" name="TWR_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.TWR_NAME.$invalid" style="color: red">Please select tower </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.FLR_NAME.$invalid}">
                                                <label for="txtcode">Floor <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Floorlist" data-output-model="UserRole.selectedFloors" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME maker"
                                                    data-on-item-click="FloorChange()" data-on-select-all="FloorChangeAll()" data-on-select-none="FloorSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedFloors" name="FLR_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.FLR_NAME.$invalid" style="color: red">Please select floor </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12" data-ng-show="Currentmodule.PCENTITY">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.PE_NAME.$invalid}">
                                                <label for="txtcode">Parent Entity  <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Parentlist" data-output-model="UserRole.selectedParentEntity" data-button-label="icon PE_NAME" data-item-label="icon PE_NAME maker"
                                                    data-on-item-click="ParentEntityChange()" data-on-select-all="ParentEntityChangeAll()" data-on-select-none="ParentEntitySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedParentEntity" name="PE_NAME" style="display: none" ng-required="Currentmodule.PCENTITY" />
                                                <span class="error" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.PE_NAME.$invalid" style="color: red">Please select parent entity </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12" data-ng-show="Currentmodule.PCENTITY">
                                            <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.CHE_NAME.$invalid}">
                                                <label for="txtcode">Child Entity  <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Childlist" data-output-model="UserRole.selectedChildEntity" data-button-label="icon CHE_NAME" data-item-label="icon CHE_NAME maker"
                                                    data-on-item-click="ChildEntityChange()" data-on-select-all="ChildEntityChangeAll()" data-on-select-none="ChildEntitySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="UserRole.selectedChildEntity" name="CHE_NAME" style="display: none" ng-required="Currentmodule.PCENTITY" />
                                                <span class="error" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.CHE_NAME.$invalid" style="color: red">Please select child entity </span>
                                            </div>
                                        </div>
                                        <div id="showhide" data-ng-show="Currentmodule.VERT_COST">
                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.VER_NAME.$invalid}">
                                                    <label for="txtcode">{{BsmDet.Parent}} <span style="color: red;">*</span></label>
                                                    <div isteven-multi-select data-input-model="Verticallist" data-output-model="UserRole.selectedVerticals" data-button-label="icon VER_NAME" data-item-label="icon VER_NAME maker"
                                                        data-on-item-click="VerticalChange()" data-on-select-all="VerticalChangeAll()" data-on-select-none="verticalSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                    </div>
                                                    <input type="text" data-ng-model="UserRole.selectedVerticals" name="VER_NAME" style="display: none" ng-required="Currentmodule.VERT_COST" />
                                                    <span class="error" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.VER_NAME.$invalid" style="color: red">Please select  {{BsmDet.Parent |lowercase}}  </span>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frmMappingEmp.$submitted && frmMappingEmp.Cost_Center_Name.$invalid}">
                                                    <label for="txtcode">{{BsmDet.Child}}  <span style="color: red;">**</span></label>
                                                    <div isteven-multi-select data-input-model="CostCenterlist" data-output-model="UserRole.selectedCostcenter" data-button-label="icon Cost_Center_Name" data-item-label="icon Cost_Center_Name maker"
                                                        data-on-item-click="CostCenterChange()" data-on-select-all="CostCenterChangeAll()" data-on-select-none="CostCenterSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                    </div>
                                                    <input type="text" data-ng-model="UserRole.selectedCostcenter" name="Cost_Center_Name" style="display: none" ng-required="Currentmodule.VERT_COST" />
                                                    <span class="error" data-ng-show="frmMappingEmp.$submitted && frmMappingEmp.Cost_Center_Name.$invalid" style="color: red">Please select {{BsmDet.Child |lowercase}} </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="button" value="Back" class="btn btn-primary pull-right" data-ng-click="back()">
                                    <input type="submit" class="btn btn-primary pull-right" value="Finish" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../BootStrapCSS/Scripts/angular-confirm.js" defer></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js" defer></script>
    <script src="../../BootStrapCSS/qtip/qtip.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "angucomplete-alt", "cp.ngConfirm"]);
        var UserEdit = '<%=Session("USER_EDIT")%>'
        FormWizard();


        //FORM WIZARD END JS

        function FormWizard() {
            var navListItems = $('div.setup-panel div a'),
                    allWells = $('.setup-content'),
                    allNextBtn = $('.nextBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                        $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-default');
                    $item.addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allNextBtn.click(function () {

                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for (var i = 0; i < curInputs.length; i++) {
                    if (!curInputs[i].validity.valid) {
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-primary').trigger('click');
        };
    </script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../System%20Preferences/Js/UserRoleMapping.js"></script>
    <script src="../../BlurScripts/BlurJs/moment.js" defer></script>
    <%--<script src="../System%20Preferences/Js/UserRoleMapping.min.js" defer></script>--%>
</body>
</html>


