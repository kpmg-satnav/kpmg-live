<%@ page Language="vb" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" Inherits="frmADFemployeesearch" CodeFile="frmADFemployeesearch.aspx.vb" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div>
    <table id="table1" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                <td align="left" width="100%">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" >Admin Functions
                <hr align="center" width="75%" /></asp:Label>
  
                </td>
                </tr>
                <tr>
                <td align="left" width="100%">
                <asp:label id="Label1" runat="server" Class="clsRptCaption" Height="14px">Note: To search for multiple  users , enter the user id with comma  seperator.</asp:label>
                  </td>
                </tr>
                 <tr>
                <td align="left" width="100%">
                <asp:textbox id="txtCtrlSelectVal" runat="server" Visible="False" Width="19px">--Select--</asp:textbox>

                  </td>
                </tr>
                <tr>
                <td align="left" width="100%">
                <asp:validationsummary id="vsSearch" runat="server" ShowSummary="False" ShowMessageBox="true"></asp:validationsummary>

                  </td>
                </tr>
                
                </table>
      
   
  			 <asp:RegularExpressionValidator id="rev3" runat="server" Display="None" ControlToValidate="TxtLname" ErrorMessage="Enter Valid Name" ValidationExpression="(\D+)*"></asp:RegularExpressionValidator>
			<asp:RegularExpressionValidator id="rev2" runat="server" Display="None" ControlToValidate="txtMname" ErrorMessage="Enter Valid Name" ValidationExpression="(\D+)*"></asp:RegularExpressionValidator>
			<asp:RegularExpressionValidator id="rev1" runat="server" Display="None" ControlToValidate="txtFname" ErrorMessage="Enter Valid Name" ValidationExpression="(\D+)*"></asp:RegularExpressionValidator>
                       
                       <asp:label id="lblchkemp" runat="server" Visible="False" Width="372px" CssClass="clsMessage" ForeColor="Red">Label</asp:label>
                       
                       
     
   
        <asp:Panel ID="PNLCONTAINER" runat="server" Height="96px" Width="95%">
            <br />
            <table id="table4" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="width: 63px">
                        <img alt="" id="IMG1" height="40" onclick="return IMG1_onclick()" src="../../images/table_left_top_corner.gif"
                            width="63" /></td>
                    <td background="../../images/table_mid_top_bg.gif" class="tableheader" width="100%" align="left">
                        <strong><font face="Arial" size="2" >Advanced Search</font></strong></td>
                    <td align="right" valign="top" width="37">
                        <img alt="" height="40" src="../../images/table_right_top_corner.gif" width="37" /></td>
                </tr>
                <tr align="center" valign="top">
                    <td colspan="3">
                       
                      <asp:panel id="panel1" runat="server" Width="95%" >
								<p align="center"/><br />
			        <table id="tblprv" cellspacing="0" cellpadding="0" width="100%" border="1">
					<tr>
						<td style="WIDTH: 404px" width="404">
							<asp:label id="lblFname" runat="server">First Name</asp:label></td>
						<td width="70%">
							<asp:textbox id="txtFname" tabIndex="6" runat="server" Width="100%" CssClass="clsTextField"></asp:textbox></td>
					</tr>
					<tr>
						<td style="WIDTH: 404px" width="404">
							<asp:label id="lblMname" runat="server" Width="106px">Middle Name</asp:label></td>
						<td width="50%">
							<asp:textbox id="txtMname" tabIndex="6" runat="server" Width="100%" CssClass="clsTextField"></asp:textbox></td>
					</tr>
					<tr>
						<td style="WIDTH: 404px" width="404">
							<asp:label id="lblLname" runat="server" Width="81px">Last Name</asp:label></td>
						<td width="50%">
							<asp:textbox id="TxtLname" tabIndex="6" runat="server" Width="100%" CssClass="clsTextField"></asp:textbox></td>
					</tr>
					<tr>
						<td style="WIDTH: 404px" width="404">
							<asp:label id="lblEid" runat="server" Width="104px">Employee ID(s)</asp:label></td>
						<td width="50%">
							<asp:textbox id="TxtEid" tabIndex="6" runat="server" Width="100%" CssClass="clsTextField"></asp:textbox></td>
					</tr>
					<tr>
						<td style="WIDTH: 404px" width="404">
							<asp:label id="Lblext" runat="server" Width="81px">Extension</asp:label></td>
						<td width="50%">
							<asp:textbox id="Txtext" tabIndex="6" runat="server" Width="100%" CssClass="clsTextField"></asp:textbox></td>
					</tr>
					<tr>
						<td style="WIDTH: 404px" width="404">
							<asp:label id="lblDept" runat="server">Department</asp:label></td>
						<td width="50%">
							<asp:dropdownlist id="cboDept" tabIndex="3" runat="server" Width="100%" CssClass="clsComboBox" AutopostBack="true"></asp:dropdownlist></td>
					</tr>
					<tr>
						<td style="WIDTH: 404px; HEIGHT: 21px" width="404">
							<asp:label id="lblEmail" runat="server" Width="81px">Email</asp:label></td>
						<td style="HEIGHT: 21px" width="50%">
							<asp:textbox id="txtEmail" tabIndex="6" runat="server" Width="100%" CssClass="clsTextField"></asp:textbox></td>
					</tr>
					<tr>
						<td align="center" colspan="2">
							<asp:button id="btnSearch" CssClass="button" Runat="server" Text="Search"></asp:button></td>
					</tr>
					</table>
					</asp:panel>
                      
                     </td>
                </tr>
                <tr>
                    <td style="height: 18px">
                        <img alt="" height="16" src="../../images/table_left_bot_corner.gif" width="63" /></td>
                    <td height="18" style="height: 18px">
                        <img alt="" height="16" src="../../images/table_mid_bot_bg.gif" width="25" /></td>
                    <td style="height: 18px">
                        <img alt="" height="16" src="../../images/table_right_bot_corner.gif" width="37" /></td>
                </tr>
            </table>
        </asp:Panel>
        
   
   
   
    </div>
    
    </asp:Content>
    