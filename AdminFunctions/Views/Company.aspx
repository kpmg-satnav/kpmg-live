﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />


    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        span.error {
            color: red;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>

<body data-ng-controller="CompanyController" class="amantra">
    <div class="container-fluid page-content-inner">
        <%--<div class="clearfix">
                    <div class="box-footer text-right" style="padding-right: 50px">
                        <span style="color: red;">*</span>  Mandatory field
                    </div>
                </div>--%>
        <br />
        <div ba-panel ba-panel-title="Add Company" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Add Company</h3>
                </div>
                <div class="panel-body" style="overflow:scroll">
                    <form id="Form1" name="frmCompany" novalidate data-valid-submit="SaveOrModify()">
                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-body">
                                    <table data-ng-table="tableParams" class="table ng-table-responsive table-condensed table-bordered table-hover table-striped">
                                        <tr style="background-color: #366599; color: white">
                                            <th>Company Name <span style="color: red;">*</span> </th>
                                            <th data-ng-show="CNPDetails.length !=0">Modules </th>
                                            <th>Address </th>
                                            <th>Remarks </th>
                                            <th>Action</th>
                                        </tr>
                                        <tr data-ng-show="CNPDetails.length ==0 || ActionStatus == 1" data-ng-form="innerForm">
                                            <td>
                                                <div class="form-group" data-ng-class="{'has-error': frmCompany.$submitted && innerForm.CNP_NAME.$invalid}">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i id="I1" class="fa fa-building"></i></div>
                                                        <input class="form-control input-sm" placeholder="Company Name" data-ng-model="Company.CNP_NAME" name="CNP_NAME" required="" data-ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" type="text">
                                                    </div>
                                                </div>
                                                <span class="error" data-ng-show="frmCompany.$submitted && innerForm.CNP_NAME.$invalid">Enter Company Name </span>
                                            </td>
                                            <td data-ng-show="CNPDetails.length !=0">{{Company.CNP_MODULE}}
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i id="id" class="fa fa-map-marker"></i></div>
                                                    <input class="form-control input-sm" placeholder="Address" data-ng-model="Company.CNP_ADDRESS" name="CNP_ADDRESS" type="text">
                                                </div>
                                            </td>

                                            <td>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i id="i3" class="fa fa-comments"></i></div>
                                                    <input class="form-control input-sm" placeholder="Remarks" data-ng-model="Company.CNP_REMARKS" name="CNP_REMARKS" type="text">
                                                </div>
                                            </td>
                                            <td>
                                                <button type="submit" title="Add" data-ng-show="ActionStatus==0"><i class="fa fa-plus-square"></i></button>
                                                <button type="submit" title="Modify" data-ng-show="ActionStatus==1"><i class="fa fa-exchange"></i></button>
                                                <a data-ng-click="Clear()" title="Clear"><i class="fa fa-refresh "></i></a>
                                            </td>
                                        </tr>
                                        <tr data-ng-repeat="CNPD in CNPDetails" data-ng-show="CNPDetails.length !=0 && ActionStatus == 0">
                                            <td>
                                                <label class="control-label">{{CNPD.CNP_NAME}}</label>
                                            </td>
                                            <td>
                                                <label class="control-label">{{CNPD.CNP_MODULE}}</label>
                                            </td>
                                            <td>
                                                <label class="control-label">{{CNPD.CNP_ADDRESS}}</label>
                                            </td>
                                            <td>
                                                <label class="control-label">{{CNPD.CNP_REMARKS}}</label>
                                            </td>
                                            <td>
                                                <a data-ng-click="Edit(CNPD)" title="Edit"><i class="fa fa-pencil"></i></a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Add Group Company" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Add Group Company</h3>
                </div>
                <div class="panel-body" style="padding-right: 41px; overflow:scroll">
                    <div data-ng-show="CNPDetails.length !=0">
                        <form id="Form2" name="frmGRPCompany" novalidate data-valid-submit="GRPSaveOrModify()">
                            <div class="col-md-12">
                                <div class="box">
                                    <div class="box-body">
                                        <table data-ng-table="tableParams" class="table ng-table-responsive table-condensed table-bordered table-hover table-striped">
                                            <tr style="background-color: #366599; color: white">
                                                <th>Group Company Name <span style="color: red;">*</span> </th>
                                                <th>Modules <span style="color: red;">*</span></th>
                                                <th>Address </th>
                                                <th>Remarks </th>
                                                <th>Action</th>
                                            </tr>
                                            <tr data-ng-form="innerGRPForm">
                                                <td>
                                                    <div class="form-group" data-ng-class="{'has-error': frmGRPCompany.$submitted && innerGRPForm.CNP_NAME.$invalid}">
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i id="user" class="fa fa-building"></i></div>
                                                            <input class="form-control input-sm" placeholder="Group Company Name" data-ng-model="GRPCompany.CNP_NAME" name="CNP_NAME" required="" data-ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" type="text">
                                                        </div>
                                                    </div>
                                                    <span class="error" data-ng-show="frmGRPCompany.$submitted && innerGRPForm.CNP_NAME.$invalid">Enter Group Company Name </span>
                                                </td>
                                                <td>
                                                    <div class="form-group" data-ng-class="{'has-error': frmGRPCompany.$submitted && innerGRPForm.MOD_NAME.$invalid}">
                                                        <div isteven-multi-select data-input-model="MODlst" data-output-model="GRPCompany.MODlst" data-button-label="icon MOD_NAME"
                                                            data-item-label="icon MOD_NAME maker" data-tick-property="ticked" data-max-labels="1">
                                                        </div>
                                                        <input type="text" data-ng-model="GRPCompany.MODlst[0]" name="MOD_NAME" style="display: none" required="" />
                                                        <span class="error" data-ng-show="frmGRPCompany.$submitted && innerGRPForm.MOD_NAME.$invalid">Please select Module </span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i id="i2" class="fa fa-map-marker"></i></div>
                                                        <input class="form-control input-sm" placeholder="Address" data-ng-model="GRPCompany.CNP_ADDRESS" name="CNP_ADDRESS" type="text">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i id="i4" class="fa fa-comments"></i></div>
                                                        <input class="form-control input-sm" placeholder="Remarks" data-ng-model="GRPCompany.CNP_REMARKS" name="CNP_REMARKS" type="text">
                                                    </div>
                                                </td>
                                                <td>
                                                    <button type="submit" title="Add" data-ng-show="GRPActionStatus==0"><i class="fa fa-plus-square"></i></button>
                                                    <button type="submit" title="Modify" data-ng-show="GRPActionStatus==1"><i class="fa fa-exchange"></i></button>
                                                    <a data-ng-click="GRPClear()" title="Clear"><i class="fa fa-refresh"></i></a>
                                                </td>
                                            </tr>
                                            <tr data-ng-repeat="GRPCNPD in GRPCNPDetails">
                                                <td>
                                                    <label class="control-label">{{GRPCNPD.CNP_NAME}}</label>
                                                </td>
                                                <td>
                                                    <label class="control-label">{{GRPCNPD.CNP_MODULE}}</label>
                                                </td>
                                                <td>
                                                    <label class="control-label">{{GRPCNPD.CNP_ADDRESS}}</label>
                                                </td>
                                                <td>
                                                    <label class="control-label">{{GRPCNPD.CNP_REMARKS}}</label>
                                                </td>

                                                <td>
                                                    <a data-ng-click="GRPEdit(GRPCNPD)" title="Edit"><i class="fa fa-pencil"></i></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script>
        var app = angular.module('QuickFMS', ["isteven-multi-select"]);
    </script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <%--<script src="../JS/Company.js"></script>--%>
    <script src="../JS/Company.min.js" defer></script>
    <script src="../../SMViews/Utility.min.js" defer></script>
    <%--<script src="../../SMViews/Utility.js"></script>--%>
</body>
</html>
