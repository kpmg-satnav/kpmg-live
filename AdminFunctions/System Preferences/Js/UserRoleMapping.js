﻿app.service("UserRoleMappingService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    //For Grid
    this.GetUserRoleList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/UserRoleMapping/GetUserRoleDetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.DeleteSeat = function (Id) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMapping/DeleteSeat', Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.InactiveEmployee = function (Id) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMapping/InactiveSpaces', Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };
    //for Module
    this.Getmodules = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/UserRoleMapping/Getmodules')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //accessBy Module
    this.accessbymodule = function (dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMapping/GetAccessByModule', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //save emp details step1
    this.SaveEmployeeDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMapping/SaveEmpDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    //save Role/Module details step2
    this.SaveRMDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMapping/SaveRoleModDetls', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //save Mapping details step3
    this.SaveMappingDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/UserRoleMapping/SaveMappingDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //emp details step1
    this.GetUserEmpDetails = function (Id) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/UserRoleMapping/GetUserEmpDetails/' + Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //Get Role-module details step2
    this.GetRMDetails_Step2 = function (Id) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/UserRoleMapping/GetRMDetails_Step2/' + Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //Get Mapping details step3
    this.LoadMappingDtls_Step3 = function (Id) {
        var deferred = $q.defer();

        ShowProgress();
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }

        return $http.get(UtilityService.path + '/api/UserRoleMapping/GetMappingDetails_Step3/' + Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('UserRoleMappingController', ['$scope', '$q', 'UserRoleMappingService', 'UtilityService', '$timeout', '$filter', '$http', '$ngConfirm', function ($scope, $q, UserRoleMappingService, UtilityService, $timeout, $filter, $http, $ngConfirm) {
    $scope.sta_id = [{ "value": "1", "Name": "Active" }, { "value": "2", "Name": "In-Active" }]

    $scope.Viewstatus = 0;
    $scope.UserRole = {};
    $scope.CurrentProfile = {};
    $scope.CurrentProfileEmp = {};
    $scope.Currentmodule = {};
    $scope.modules = [];
    $scope.acceslevel = [];
    $scope.Countrylst = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.Verticallist = [];
    $scope.CostCenterlist = [];
    $scope.Parentlist = [];
    $scope.Childlist = [];
    $scope.allUsers = [];
    $scope.Currentmodule.MapAccess = "Y";

    $scope.ViewUploption = true;
    $scope.UserStatus = true;
    var EmployeeId = '';
    $scope.Currentmodule.VERT_COST = true;      //FOR ALL MODULE, SHOW VERT/CC
    $scope.Currentmodule.PCENTITY = true;
    $scope.reqdate = "MM/dd/yyyy"; //mm / dd / yyyy
    $scope.RolValidation = false;
    $scope.SpaceVisible = true;
    //For Grid column Headers
    var columnDefs = [



        { headerName: "User ID", field: "AUR_ID", cellClass: "grid-align" },
        { headerName: "User Name", field: "AUR_KNOWN_AS", cellClass: "grid-align", filter: 'set', template: '<a ng-click="onRowSelectedFunc(data)">{{data.AUR_KNOWN_AS}}</a>' },
        { headerName: "Location", field: "AUR_LOCATION", cellClass: "grid-align" },
        { headerName: "Role Name", field: "ROL_DESCRIPTION", cellClass: "grid-align" },
        { headerName: "Map Level Access", field: "MAP_ACCESS", cellClass: "grid-align" },
        { headerName: "Email ID", field: "AUR_EMAIL", cellClass: "grid-align" },
        { headerName: "Designation", field: "AUR_DESGN_ID", cellClass: "grid-align" },
        { headerName: "Employee type", field: "AUR_GRADE", cellClass: "grid-align" },
        { headerName: "Vertical", field: "AUR_VERTICAL_NAME", cellClass: "grid-align" },
        { headerName: "Costcenter", field: "AUR_COSTCENTER_NAME", cellClass: "grid-align" },
        { headerName: '<i class="fa fa-times" aria-hidden="true"></i>', cellclass: "grid-align", filter: 'set', template: '<a ng-click="Delete(data)"><i class="fa fa-times" aria-hidden="true"></i></a>' }
    ];


    var columnDefs1 = [
        { headerName: "Country Name", field: "HST_CNY_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "City Name", field: "HST_CTY_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Location", field: "HST_LCM_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Tower", field: "HST_TWR_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Floor", field: "HST_FLR_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Vertical Name", field: "HST_VERT_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "Cost Center Name", field: "HST_CST_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "User Role", field: "HST_USR_ROLE_NAME", cellClass: "grid-align", width: 100 },
        { headerName: "User Id", field: "HST_USR_ID", cellClass: "grid-align", width: 100 },
        { headerName: "Remarks", field: "HST_REM", cellClass: "grid-align", width: 450 }
    ];

    function setRowData(allOfTheData) {
        //console.log(allOfTheData);
        var dataSource = {
            rowCount: null, // behave as infinite scroll
            getRows: function (params) {
                //params.startRow = 0;
                //params.endRow = 10000;
                console.log('asking for ' + params.startRow + ' to ' + params.endRow);
                // At this point in your code, you would call the server, using $http if in AngularJS.
                // To make the demo look real, wait for 500ms before returning
                setTimeout(function () {
                    // take a slice of the total rows
                    var rowsThisPage = allOfTheData.slice(params.startRow, params.endRow);
                    // if on or after the last page, work out the last row.
                    var lastRow = -1;
                    if (allOfTheData.length <= params.endRow) {
                        lastRow = allOfTheData.length;
                    }
                    // call the success callback
                    params.successCallback(rowsThisPage, lastRow);
                }, 500);
            }
        };

        $scope.gridOptions.api.setDatasource(dataSource);
    }

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);

        $scope.dataobject = { Pagesize: $scope.pageSize, PageNumber: $scope.pageNumber };
        UserRoleMappingService.GetUserRoleList().then(function (data) {
            if (data.data != null) {
                //console.log(data.data);
                $scope.gridata = data.data;
                $scope.name = data.data[0].EMP_COUNT;
                //$scope.createNewDatasource();
                
                        if (window.innerWidth <= 480) {
                            $scope.gridOptions.api.setColumnDefs(columnDefs);
                            $scope.gridOptions.api.setRowData($filter('filter')($scope.gridata, { EMP_COUNT: 0 }));
                           // params.api.sizeColumnsToFit();
                        }
                        else {
                            $scope.gridOptions.api.setRowData($filter('filter')($scope.gridata, { EMP_COUNT: 0 }));
                        }


               // $scope.gridOptions.api.setRowData($filter('filter')($scope.gridata, { EMP_COUNT: 0 }));
                progress(0, '', false);
            }
            else {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, data.Message);
            }
        }, function (error) {
            progress(0, '', false);
        });


    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        debug: true,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        rowSelection: 'single',
        rowModelType: 'virtual',
        paginationOverflowSize: 2,
        maxConcurrentDatasourceRequests: 2,
        //paginationPageSize: 30,
        paginationInitialRowCount: 1,
        maxPagesInCache: 2,
        //  onRowSelected: onRowSelectedFunc,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };
    //$scope.pageSize = '10';
    //$scope.pageNumber = '1';

    $scope.createNewDatasource = function () {
        var dataSource = {
            pageSize: parseInt($scope.pageSize),
            getRows: function (params) {
                setTimeout(function () {
                    var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                    var lastRow = -1;
                    if ($scope.gridata.length <= params.endRow) {
                        lastRow = $scope.gridata.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
                }, 500);
            }
        };
        $scope.gridOptions.api.setDatasource(dataSource);
    }

    setTimeout(function () {
        $scope.LoadData();
    }, 700);

    $scope.gridOptions1 = {
        columnDefs: columnDefs1,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        rowSelection: 'single',
        onReady: function () {
            $scope.gridOptions1.api.sizeColumnsToFit()
        }
    };

    $scope.Delete = function (data) {
        var encoding = encodeURIComponent(data.AUR_ID);
        //var Id = encoding.replace(/%2F/i, "_");
        var Id = encoding.split(/%2F/i).join("__");
        var obj = { AurId: Id };
        progress(0, 'Loading...', true);
        $ngConfirm({
            icon: 'fa fa-warning',
            columnClass: 'col-md-6 col-md-offset-3',
            title: 'Confirm!',
            content: 'Are you sure want to Inactive / Delete  - ' + '(' + data.AUR_ID + ') ' + data.AUR_KNOWN_AS,
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            type: 'red',
            autoClose: 'cancel|15000',
            buttons: {
                Confirm: {
                    text: 'Delete',
                    btnClass: 'btn-red',
                    action: function (button) {
                        UserRoleMappingService.DeleteSeat(obj).then(function (response) {
                            if (response.data != null) {
                                $ngConfirm({
                                    icon: 'fa fa-success',
                                    title: 'Status',
                                    content: 'Employee Deleted Successfully',
                                    closeIcon: true,
                                    closeIconClass: 'fa fa-close'
                                });
                            }
                            setTimeout(function () {
                                $scope.LoadData();
                            }, 800);
                        });
                    }
                },
                Inactive: {
                    text: 'Inactive',
                    btnClass: 'btn-blue',
                    action: function (button) {
                        UserRoleMappingService.InactiveEmployee(obj).then(function (response) {
                            if (response.data != null) {
                                $ngConfirm({
                                    icon: 'fa fa-success',
                                    title: 'Status',
                                    content: 'Selected Employee Is Inactive',
                                    closeIcon: true,
                                    closeIconClass: 'fa fa-close'
                                });
                            }
                            setTimeout(function () {
                                $scope.LoadData();
                            }, 800);
                        });
                    }
                },
                cancel: function () {
                    $ngConfirm({
                        title: 'Status!',
                        content: 'Cancelled',
                    });
                }
            }
        });
        progress(0, 'Loading...', false);
    }


    $scope.onRowSelectedFunc = function (data) {
        progress(0, 'Loading...', true);

        //FormWizard();        
        $('div.setup-panel div a').removeClass('btn-primary').addClass('btn-default');
        $('.setup-content').hide();
        $target = $($('#stp1').attr('href'));
        $('#stp1').addClass('btn-primary');
        $target.show();
        $target.find('input:eq(0)').focus();
        $scope.SelectdEmp = data;
        $scope.CurrentProfile = {};
        $scope.CurrentProfileEmp = {};
        //var Id = data.AUR_ID;
        $scope.ViewUploption = false;
        EmployeeId = data.AUR_ID;
        $scope.Currentmodule.MapAccess = "Y";

        //var Id = data.AUR_ID.replace("/", "__");
        var Id = data.AUR_ID.split("/").join("__");

        //vert
        UtilityService.getVerticals(1).then(function (Vedata) {
            $scope.verticalddl = Vedata.data;
        });

        //cost cent
        UtilityService.getCostCenters(1).then(function (Csdata) {
            $scope.costcenterddl = Csdata.data;
        });

        UtilityService.getDesignation().then(function (response) {
            $scope.designationddl = response.data;
        });

        UserRoleMappingService.GetUserEmpDetails(Id).then(function (response) {
            $scope.selectedEmpDB = {
                selected: {
                    data: { AUR_ID: response.AUR_REPORTING_TO, NAME: response.AUR_REPORTING_TO_NAME, ticked: false, VERTICAL: response.VERTICAL, COSTCENTER: response.COSTCENTER, AUR_EMAIL: response.AUR_EMAIL, AUR_RES_NUMBER: response.AUR_RES_NUMBER, AUR_PASSWORD: response.AUR_PASSWORD, selectedEmp: response.AUR_REPORTING_TO, AUR_EXTENSION: response.AUR_EXTENSION, AUR_DESGN_ID: response.AUR_DESGN_ID, LOCATION: response.LOCATION },
                    title: ""
                }
            }
            setTimeout(function () {
                $scope.CurrentProfileEmp.AUR_ID_OLD = Id;
            }, 200);

            $scope.usrStatus = [{ value: 0, Name: "In-Active" }, { value: 1, Name: "Active" }];

            $scope.CurrentProfileEmp = response;
            $scope.reselectedEmp($scope.selectedEmpDB);
            $scope.CurrentProfileEmp.AUR_OLD_PASSWORD = response.AUR_PASSWORD;
            $scope.CurrentProfileEmp.NEW_AUR_ID = response.AUR_ID;
            $scope.CurrentProfileEmp.DSG_CODE = response.AUR_DESGN_ID;
            $scope.CurrentProfileEmp.AUR_DOJ = $filter('date')(response.AUR_DOJ, $scope.reqdate);
            $scope.CurrentProfileEmp.AUR_DOB = $filter('date')(response.AUR_DOB, $scope.reqdate);
            $scope.CurrentProfileEmp.AUR_EMP_REMARKS = response.AUR_EMP_REMARKS;
            $scope.CurrentProfileEmp.AUR_STA_ID = response.AUR_STA_ID.toString();
            setTimeout(function () { $('#ddlusrStatus').selectpicker('refresh'); }, 200);
            $scope.Viewstatus = 1;
            $scope.Currentmodule.MapAccess = "Y";

            setTimeout(function () {
                $('#DSG').selectpicker('refresh');
                //$("#COSTCENTER option[Cost_Center_Code=" + response.COSTCENTER + "]").attr('selected', 'selected');
            }, 100);
            setTimeout(function () {
                $('#COSTCENTER').selectpicker('refresh');
                //$("#COSTCENTER option[Cost_Center_Code=" + response.COSTCENTER + "]").attr('selected', 'selected');
            }, 100);
            setTimeout(function () {
                $('#VERTICAL').selectpicker('refresh');
                //$("#VERTICAL option[VER_CODE=" + response.VERTICAL + "]").attr('selected', 'selected');
            }, 200);
            setTimeout(function () {
                $('#LOCATION').selectpicker('refresh');
                //$("#VERTICAL option[VER_CODE=" + response.VERTICAL + "]").attr('selected', 'selected');
            }, 200);


            progress(0, '', false);
        }, function (error) {
            progress(0, '', false);
        });
    }

    //for Module DropDown
    UserRoleMappingService.Getmodules().then(function (data) {
        $scope.modules = data;
        //UtilityService.GetRoles(1).then(function (rol) {
        $scope.dataobject = { AurId: EmployeeId, ModuleId: $scope.Currentmodule.MOD_NAME };
        UserRoleMappingService.accessbymodule($scope.dataobject).then(function (rol) {

            $scope.acceslevel = rol.data;
        });
    }, function (error) {
        console.log(error);
    });


    //For Access  by Module change
    $scope.accessbymodule = function () {
        $scope.dataobject = { AurId: EmployeeId, ModuleId: $scope.Currentmodule.MOD_NAME };
        UserRoleMappingService.accessbymodule($scope.dataobject).then(function (data) {
            $scope.acceslevel = data;
        }, function (error) {
            console.log(error);
        });
        //other than All, Space hide vert, costcenter
        if ($scope.Currentmodule.MOD_NAME == "ALL" || $scope.Currentmodule.MOD_NAME == "Space") {
            $scope.Currentmodule.VERT_COST = true;
            $scope.Currentmodule.PCENTITY = true;
            $scope.SpaceVisible = true;
            $scope.Currentmodule.MapAccess = "Y";
        }
        else if ($scope.Currentmodule.MOD_NAME == "Maintenance") {
            $scope.Currentmodule.VERT_COST = true;
            $scope.Currentmodule.PCENTITY = false;
            $scope.SpaceVisible = false;
        }
        else {
            $scope.Currentmodule.VERT_COST = false;
            $scope.Currentmodule.PCENTITY = false;
            $scope.SpaceVisible = false;
        }
        $scope.RolValidation = false;
        $scope.Currentmodule.MapAccess = "Y";
        // $scope.IsVisible = $scope.IsVisible ? false : true;
    }

    //For Status Name
    $scope.ShowstatusName = function (sta) {
        for (var i = 0; i < $scope.sta_id.length; i += 1) {
            var result = $scope.sta_id[i];
            if (result.value == sta) {
                return result.Name;
            }
        }
    }

    $scope.CountryChanged = function () {
        UtilityService.getCitiesbyCny($scope.UserRole.selectedCountries, 1).then(function (response) {
            if (response.data != null) {
                $scope.Citylst = response.data
            } else { $scope.Citylst = [] }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.CnyChangeAll = function () {
        $scope.UserRole.selectedCountries = $scope.Countrylst;
        $scope.CountryChanged();
    }

    $scope.cnySelectNone = function () {
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    $scope.CityChanged = function () {
        $scope.UserRole.selectedCountries = [];

        UtilityService.getLocationsByCity($scope.UserRole.selectedCities, 1).then(function (data) {
            if (data.data == null) { $scope.Locationlst = [] } else {
                $scope.Locationlst = data.data;
            }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.UserRole.selectedCountries.push(cny);
            }
        });
    }
    $scope.CtyChangeAll = function () {
        $scope.UserRole.selectedCities = $scope.Citylst;
        $scope.CityChanged();
    }
    $scope.ctySelectNone = function () {
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    $scope.LocChange = function () {
        $scope.UserRole.selectedCountries = [];
        $scope.UserRole.selectedCities = [];

        UtilityService.getTowerByLocation($scope.UserRole.selectedLocations, 1).then(function (data) {
            if (data.data != null) { $scope.Towerlist = data.data } else { $scope.Towerlist = [] }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.UserRole.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Locationlst, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.UserRole.selectedCities.push(cty);
            }
        });
    }
    $scope.LCMChangeAll = function () {
        $scope.UserRole.selectedLocations = $scope.Locationlst;
        $scope.LocChange();
    }
    $scope.lcmSelectNone = function () {
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    $scope.TwrChange = function () {
        $scope.UserRole.selectedCountries = [];
        $scope.UserRole.selectedCities = [];
        $scope.UserRole.selectedLocations = [];

        UtilityService.getFloorByTower($scope.UserRole.selectedTowers, 1).then(function (data) {
            if (data.data != null) { $scope.Floorlist = data.data } else { $scope.Floorlist = [] }
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towerlist, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.UserRole.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.UserRole.selectedCities.push(cty);
            }
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                lcm.ticked = true;
                $scope.UserRole.selectedLocations.push(lcm);
            }
        });
    }
    $scope.TwrChangeAll = function () {
        $scope.UserRole.selectedTowers = $scope.Towerlist;
        $scope.TwrChange();
    }
    $scope.twrSelectNone = function () {
        $scope.Floorlist = [];
    }

    $scope.FloorChange = function () {
        $scope.UserRole.selectedCountries = [];
        $scope.UserRole.selectedCities = [];
        $scope.UserRole.selectedLocations = [];
        $scope.UserRole.selectedTowers = [];

        angular.forEach($scope.Countrylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylst, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locationlst, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Towerlist, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var cny = _.find($scope.Countrylst, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.UserRole.selectedCountries.push(cny);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var cty = _.find($scope.Citylst, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.UserRole.selectedCities.push(cty);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var lcm = _.find($scope.Locationlst, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                lcm.ticked = true;
                $scope.UserRole.selectedLocations.push(lcm);
            }
        });

        angular.forEach($scope.Floorlist, function (value, key) {
            var twr = _.find($scope.Towerlist, { TWR_CODE: value.TWR_CODE, LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (twr != undefined && value.ticked == true && twr.ticked == false) {
                twr.ticked = true;
                $scope.UserRole.selectedTowers.push(twr);
            }
        });
    }

    $scope.FloorChangeAll = function () {
        $scope.UserRole.selectedFloors = $scope.Floorlist;
        $scope.FloorChange();
    }

    $scope.ParentEntityChange = function () {
        UtilityService.getchildbyparent($scope.UserRole.selectedParentEntity, 1).then(function (response) {
            $scope.Childlist = [];
            if (response.data != null) { $scope.Childlist = response.data } else { $scope.Childlist = [] }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.ParentEntityChangeAll = function () {
        $scope.UserRole.selectedParentEntity = $scope.Parentlist;
        $scope.ParentEntityChange();
    }

    $scope.ParentEntitySelectNone = function () {
        $scope.Childlist = [];
        $scope.Verticallist = [];
        $scope.CostCenterlist = [];
    }

    $scope.ChildEntityChange = function () {
        UtilityService.getverticalbychild($scope.UserRole.selectedChildEntity, 1).then(function (response) {
            if (response.data != null) {
                $scope.Verticallist = response.data
                UtilityService.getCostcenterByVertical($scope.Verticallist, 1).then(function (response) {
                    if (response.data != null) { $scope.CostCenterlist = response.data } else { $scope.CostCenterlist = [] }
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                $scope.Verticallist = []
            }
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.Parentlist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Childlist, function (value, key) {
            
            var par = _.find($scope.Parentlist, { PE_CODE: value.PE_CODE });
            if (par != undefined && value.ticked == true) {
                par.ticked = true;
                $scope.UserRole.selectedParentEntity.push(par);
            }
        });
    }

    $scope.ChildEntityChangeAll = function () {
        $scope.UserRole.selectedChildEntity = $scope.Childlist;
        $scope.ChildEntityChange();
    }
    $scope.ChildEntitySelectNone = function () {
        $scope.Verticallist = [];
        $scope.CostCenterlist = [];
    }

    $scope.VerticalChange = function () {
        UtilityService.getCostcenterByVertical($scope.UserRole.selectedVerticals, 1).then(function (data) {
            if (data.data != null) { $scope.CostCenterlist = data.data } else { $scope.CostCenterlist = [] }
        }, function (error) {
            console.log(error);
        });
    }
    $scope.VerticalChangeAll = function () {
        $scope.UserRole.selectedVerticals = $scope.Verticallist;
        $scope.VerticalChange();
    }
    $scope.verticalSelectNone = function () {
        $scope.CostCenterlist = [];
    }

    $scope.CostCenterChange = function () {
        angular.forEach($scope.Verticallist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.CostCenterlist, function (value, key) {
            var ver = _.find($scope.Verticallist, { VER_CODE: value.Vertical_Code });
            if (ver != undefined && value.ticked == true) {
                ver.ticked = true;
                $scope.UserRole.selectedVerticals.push(ver);
            }
        });
    }

    $scope.CostCenterChangeAll = function () {
        $scope.UserRole.selectedCostcenter = $scope.CostCenterlist;
        $scope.CostCenterChange();
    }
    //For Back Button
    $scope.back = function () {
        $scope.Viewstatus = 0;
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
        $scope.Countrylst = [];
        $scope.acceslevel = [];
        $scope.Verticallist = [];
        $scope.Parentlist = [];
        $scope.Childlist = [];
        $scope.CostCenterlist = [];
        //$scope.SAVE_Continue = true;
        //$scope.SAVE = false;

        //$scope.frmMREmpDe = true;
        //$scope.frmMapping = true;
        $scope.RoleAssignment = true;
        $scope.Mapping = true;
        $scope.Currentmodule.MOD_NAME = "ALL";

        $scope.Currentmodule.MapAccess = "Y";
        $scope.ViewUploption = true;
        setTimeout(function () {
            $scope.LoadData();
        }, 700);
    }
    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });

    // bnp user role mapping by role id
    UtilityService.getSysPreferences().then(function (response) {

        if (response.data != null) {
            $scope.SysPref = response.data;
        }
        return UtilityService.GetRoleID();
    }).then(function (response) {
        if (response.data != null) {
            $scope.Role = response.data;
        }
        var prefval = _.find($scope.SysPref, { SYSP_CODE: "User role mapping access by RoleID" });
        if (prefval.SYSP_VAL1 == 1) {
            var role = _.find($scope.Role, { ROL_ID: "1" });
            if (role != undefined) {
                $scope.SAVE_Continue = true;
                $scope.SAVE = false;
                $scope.EmployeeDetails = true;
                $scope.RoleAssignment = true;
                $scope.Mapping = true;
                //$scope.frmMREmpDe = true;
                //$scope.frmMapping = true;
            }
            else {
                $scope.SAVE_Continue = false;
                $scope.SAVE = true;
                $scope.EmployeeDetails = true;
                $scope.RoleAssignment = false;
                $scope.Mapping = false;
                //$scope.frmMREmpDe = false;
                //$scope.frmMapping = false;

            }
        }

        else {
            $scope.SAVE_Continue = true;
            $scope.SAVE = false;
            $scope.EmployeeDetails = true;
            $scope.RoleAssignment = true;
            $scope.Mapping = true;
        }
    });



    // For save only
    $scope.SaveEmpDetailsonly = function () {
        var fromdate = moment($scope.CurrentProfileEmp.AUR_DOB);
        var todate = moment($scope.CurrentProfileEmp.AUR_DOJ);
        var encoding;
        var Id;

     
        var OldUserID = $scope.CurrentProfileEmp.AUR_ID;
        AUR_ID_NEW = $scope.CurrentProfileEmp.NEW_AUR_ID;
        console.log(AUR_ID_NEW);

        if (fromdate > todate) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            if ($scope.CurrentProfileEmp.COSTCENTER != "") {
                progress(0, 'Loading...', true);
                encoding = encodeURIComponent(EmployeeId);
                //var Id = encoding.replace(/%2F/i, "_");
                Id = encoding.split(/%2F/i).join("_");
                $scope.CurrentProfileEmp.AUR_ID = Id;
                if ($scope.CurrentProfileEmp.AUR_ID != $scope.CurrentProfileEmp.NEW_AUR_ID) {
                    $ngConfirm({
                        icon: 'fa fa-warning',
                        title: 'Confirm!',
                        content: 'Please wait for 5 mins to update all existing transaction details with new empid. Are you Sure want to Update?',
                        closeIcon: true,
                        closeIconClass: 'fa fa-close',
                        buttons: {
                            Confirm: {
                                text: 'Sure',
                                btnClass: 'btn-blue',
                                action: function (button) {
                                    UserRoleMappingService.SaveEmployeeDetails($scope.CurrentProfileEmp).then(function (data) {
                                        progress(0, 'Loading...', false);
                                        // nxtbtnclick();
                                        if (data.data == undefined) {
                                            showNotification('error', 8, 'bottom-right', data.Message);
                                        }
                                        else {
                                            showNotification('success', 8, 'bottom-right', 'Employee Details Saved Successfully');


                                        }
                                    });

                                }
                            },
                            close: function ($scope, button) {
                            }
                        }
                    });
                }
                else {
                    UserRoleMappingService.SaveEmployeeDetails($scope.CurrentProfileEmp).then(function (data) {
                        progress(0, 'Loading...', false);
                        // nxtbtnclick();
                        if (data.data == undefined) {
                            showNotification('error', 8, 'bottom-right', data.Message);
                        }
                        else {
                            showNotification('success', 8, 'bottom-right', 'Employee Details Saved Successfully');


                        }
                    });
                }
                

               
            }
            else {
                //showNotification('success', 8, 'bottom-right', 'Please select costcenter.');
                alert("Please select LOB/Costcenter.");
            }
        }



    };

    //For Filter
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.Currentmodule.MOD_NAME = "ALL";
    $scope.Currentmodule.MapAccess = "Y";

    //For Save Continu
    $scope.SaveEmpDetails = function () {

        var fromdate = moment($scope.CurrentProfileEmp.AUR_DOB);
        var todate = moment($scope.CurrentProfileEmp.AUR_DOJ);


        var OldUserID = $scope.CurrentProfileEmp.AUR_ID;
        AUR_ID_NEW = $scope.CurrentProfileEmp.NEW_AUR_ID;
        console.log(AUR_ID_NEW);

        if (fromdate > todate) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            if ($scope.CurrentProfileEmp.COSTCENTER != "") {
                progress(0, 'Loading...', true);
                var encoding = encodeURIComponent(EmployeeId);
                //var Id = encoding.replace(/%2F/i, "_");
                var Id = encoding.split(/%2F/i).join("_");
                $scope.CurrentProfileEmp.AUR_ID = Id;
                if ($scope.CurrentProfileEmp.AUR_ID != $scope.CurrentProfileEmp.NEW_AUR_ID) {
                    $ngConfirm({
                        icon: 'fa fa-warning',
                        title: 'Confirm!',
                        content: 'After Employee Id Updation, All Old Transcations will be Transfered to new employee Id in 5 minutes.Are you Sure want to Update?',
                        closeIcon: true,
                        closeIconClass: 'fa fa-close',
                        buttons: {
                            Confirm: {
                                text: 'Sure',
                                btnClass: 'btn-blue',
                                action: function (button) {
                                    UserRoleMappingService.SaveEmployeeDetails($scope.CurrentProfileEmp).then(function (data) {
                                        progress(0, 'Loading...', false);
                                        nxtbtnclick();
                                        if (data.data == undefined) {
                                            showNotification('error', 8, 'bottom-right', data.Message);
                                        }
                                        else {


                                            if (data.data.AUR_PASSWORD != null) {
                                                $scope.CurrentProfileEmp.AUR_OLD_PASSWORD = data.data.AUR_PASSWORD;
                                            }
                                            $scope.modules = data.data.MRD.modulelist;
                                            //$scope.UserRole = response.USERMAPPINGDETAILS;

                                            if (data.data.MRD.UserRoleVM.length > 0) {
                                                $scope.dataobject = { AurId: EmployeeId, ModuleId: $scope.Currentmodule.MOD_NAME };
                                                UserRoleMappingService.accessbymodule($scope.dataobject).then(function (rol) {
                                                    $scope.acceslevel = rol;
                                                    for (i = 0; i < data.data.MRD.UserRoleVM.length; i++) {
                                                        var a = _.find($scope.acceslevel, { ROL_ID: data.data.MRD.UserRoleVM[i].ROL_ID });
                                                        a.isChecked = true;
                                                    }
                                                });
                                            }
                                            else {
                                                // UtilityService.GetRoles(1).then(function (rol) {
                                                $scope.dataobject = { AurId: EmployeeId, ModuleId: $scope.Currentmodule.MOD_NAME };
                                                UserRoleMappingService.accessbymodule($scope.dataobject).then(function (rol) {
                                                    $scope.acceslevel = rol;
                                                    for (i = 0; i < $scope.acceslevel.length; i++) {
                                                        // var a = _.find($scope.acceslevel, { ROL_ID: 6 });
                                                        if ($scope.acceslevel[i].ROL_ID == "6") {
                                                            $scope.acceslevel[i].isChecked = true;
                                                        }
                                                    }
                                                });
                                            }
                                            $scope.SelectdEmp.AUR_EMAIL = $scope.CurrentProfileEmp.AUR_EMAIL;
                                            $scope.gridOptions.api.refreshView();
                                            showNotification('success', 8, 'bottom-right', 'Employee Details Saved Successfully');
                                        }
                                    });

                                }
                            },
                            close: function ($scope, button) {
                            }
                        }
                    });
                }
                else {
                    UserRoleMappingService.SaveEmployeeDetails($scope.CurrentProfileEmp).then(function (data) {
                        progress(0, 'Loading...', false);
                        nxtbtnclick();
                        if (data.data == undefined) {
                            showNotification('error', 8, 'bottom-right', data.Message);
                        }
                        else {


                            if (data.data.AUR_PASSWORD != null) {
                                $scope.CurrentProfileEmp.AUR_OLD_PASSWORD = data.data.AUR_PASSWORD;
                            }
                            $scope.modules = data.data.MRD.modulelist;
                            //$scope.UserRole = response.USERMAPPINGDETAILS;

                            if (data.data.MRD.UserRoleVM.length > 0) {
                                $scope.dataobject = { AurId: EmployeeId, ModuleId: $scope.Currentmodule.MOD_NAME };
                                UserRoleMappingService.accessbymodule($scope.dataobject).then(function (rol) {
                                    $scope.acceslevel = rol;
                                    for (i = 0; i < data.data.MRD.UserRoleVM.length; i++) {
                                        var a = _.find($scope.acceslevel, { ROL_ID: data.data.MRD.UserRoleVM[i].ROL_ID });
                                        a.isChecked = true;
                                    }
                                });
                            }
                            else {
                                // UtilityService.GetRoles(1).then(function (rol) {
                                $scope.dataobject = { AurId: EmployeeId, ModuleId: $scope.Currentmodule.MOD_NAME };
                                UserRoleMappingService.accessbymodule($scope.dataobject).then(function (rol) {
                                    $scope.acceslevel = rol;
                                    for (i = 0; i < $scope.acceslevel.length; i++) {
                                        // var a = _.find($scope.acceslevel, { ROL_ID: 6 });
                                        if ($scope.acceslevel[i].ROL_ID == "6") {
                                            $scope.acceslevel[i].isChecked = true;
                                        }
                                    }
                                });
                            }
                            $scope.SelectdEmp.AUR_EMAIL = $scope.CurrentProfileEmp.AUR_EMAIL;
                            $scope.gridOptions.api.refreshView();
                            showNotification('success', 8, 'bottom-right', 'Employee Details Saved Successfully');
                        }
                    });
                }
                

               
            }
            else {
                //showNotification('success', 8, 'bottom-right', 'Please select costcenter.');
                alert("Please select LOB/Costcenter.");
            }
        }
    };

    //step2 click -> Getting Role/Module details
    $scope.LoadRMdetails_Step2 = function () {
        progress(0, 'Loading...', true);
        var encoding = encodeURIComponent(EmployeeId);
        //var Id = encoding.replace(/%2F/i, "_");
        var Id = encoding.split(/%2F/i).join("_");

        UserRoleMappingService.GetRMDetails_Step2(Id).then(function (response) {
            $scope.RolValidation = true;
            $scope.modules = response.data.modulelist;

            if (response.data.UserRoleVM.length > 0) {
                var id = $scope.Currentmodule.MOD_NAME;
                $scope.dataobject = { AurId: EmployeeId, ModuleId: $scope.Currentmodule.MOD_NAME };
                UserRoleMappingService.accessbymodule($scope.dataobject).then(function (rol) {
                    $scope.acceslevel = rol;
                    $scope.Currentmodule.MapAccess = response.data.UserRoleVM[0].ROL_ACRONYM;
                    for (i = 0; i < response.data.UserRoleVM.length; i++) {
                        var a = _.find($scope.acceslevel, { ROL_ID: response.data.UserRoleVM[i].ROL_ID });
                        if (a)
                            a.isChecked = true;
                    }
                    // ChkLength                     
                    var ChkLength = $filter('filter')($scope.acceslevel, { isChecked: true }).length;
                    if (ChkLength == 0) {
                        var a = _.find($scope.acceslevel, { ROL_ID: 6 });
                        a.isChecked = true;
                        //for (i = 0; i < $scope.acceslevel.length ; i++) {                      
                        //    if ($scope.acceslevel[i].ROL_ID == "6") {
                        //        $scope.acceslevel[i].isChecked = true;
                        //    }
                        //}
                    }
                });
            }
            else {
                //UtilityService.GetRoles(1).then(function (rol) {
                $scope.dataobject = { AurId: EmployeeId, ModuleId: $scope.Currentmodule.MOD_NAME };
                UserRoleMappingService.accessbymodule($scope.dataobject).then(function (rol) {
                    $scope.acceslevel = rol;
                    for (i = 0; i < $scope.acceslevel.length; i++) {
                        // var a = _.find($scope.acceslevel, { ROL_ID: 6 });                        
                        if ($scope.acceslevel[i].ROL_ID == "6") {
                            $scope.acceslevel[i].isChecked = true;
                        }
                    }
                });
            }

            progress(0, 'Loading...', false);
        }, function (error) {
            progress(0, '', false);
        });
    };

    $scope.LoadMappingDtls_Step3 = function () {
        progress(0, 'Loading...', true);
        var encoding = encodeURIComponent(EmployeeId);
        //var Id = encoding.replace(/%2F/i, "_");
        var Id = encoding.split(/%2F/i).join("__");

        UserRoleMappingService.LoadMappingDtls_Step3(Id).then(function (response) {
            //response
            $scope.UsrMappingDtls(response);
            progress(0, 'Loading...', false);
        }, function (error) {
            progress(0, '', false);
        });
    };

    function nxtbtnclick() {

        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    }

    //save role module details / step2
    $scope.SaveRMDetls = function () {
        //EmployeeId        
        progress(0, 'Loading...', true);
        $scope.selectedRoles = [];
        $scope.selectedRoles = $filter('filter')($scope.acceslevel, { isChecked: true });
        if ($scope.selectedRoles.length > 0) { //selectedRoles        
            $scope.RolValidation = true;
            $scope.dataobject = { UserRoleMapping: { AUR_ID: EmployeeId }, UserRoleVM: $scope.selectedRoles, MapLevelAccess: $scope.Currentmodule.MapAccess };
            UserRoleMappingService.SaveRMDetails($scope.dataobject).then(function (response) {
                $scope.UsrMappingDtls(response);

                progress(0, 'Loading...', false);
                nxtbtnclick();
                showNotification('success', 8, 'bottom-right', 'Role Assigned Successfully');
            }, function (error) {
                progress(0, '', false);
            });

            var RoleLst = '';

            for (i = 0; i < $scope.selectedRoles.length; i++) {
                if ($scope.selectedRoles[i].isChecked == true) {
                    RoleLst = RoleLst + ',' + $scope.selectedRoles[i].ROL_DESCRIPTION;
                }
            }
            RoleLst = RoleLst.slice(1);

            $scope.SelectdEmp.ROL_DESCRIPTION = RoleLst;
            $scope.gridOptions.api.refreshView();

        } else {
            progress(0, 'Loading...', false);
            $scope.RolValidation = false;
            showNotification('error', 8, 'bottom-right', 'Please Select Atleast One Role to Assign');
        }
    };
    //$scope.selectedRoles = [];
    //$scope.$watch('acceslevel', function () {
    //    $scope.selectedRoles = $filter('filter')($scope.acceslevel, { isChecked: true });
    //    if ($scope.selectedRoles.length > 0)
    //        $scope.nextBtnCss = 'nextBtn';
    //    else
    //        $scope.nextBtnCss = '';
    //}, true);


    $scope.SaveMappingDetls = function () {
        progress(0, 'Loading...', true);
        $scope.dataobject = { UserRoleMapping: { AUR_ID: EmployeeId }, USERMAPPINGDETAILS: $scope.UserRole, MapLevelAccess: 'Y' };
        UserRoleMappingService.SaveMappingDetails($scope.dataobject).then(function (data) {

            nxtbtnclick();
            showNotification('success', 8, 'bottom-right', 'Mapped Successfully');
        }, function (error) {
            progress(0, '', false);
        });
        progress(0, 'Loading...', false);
    };

    $scope.UsrMappingDtls = function (response) {
        //countries
        UtilityService.getCountires(1).then(function (Condata) {
            $scope.Countrylst = Condata.data;
            if (response.data.selectedCountries != null) {
                for (i = 0; i < response.data.selectedCountries.length; i++) {
                    var a = _.find($scope.Countrylst, { CNY_CODE: response.data.selectedCountries[i].CNY_CODE });
                    if (a)
                        a.ticked = true;
                }
            }

            //cities
            UtilityService.getCities(1).then(function (Cnydata) {
                $scope.Citylst = Cnydata.data;
                if (response.data.selectedCities != null) {
                    for (i = 0; i < response.data.selectedCities.length; i++) {
                        var a = _.find($scope.Citylst, { CTY_CODE: response.data.selectedCities[i].CTY_CODE });
                        if (a)
                            a.ticked = true;
                    }
                }
                else {
                    $scope.Citylst = Cnydata.data;
                }

                //locs
                UtilityService.getLocations(1).then(function (Ctydata) {
                    $scope.Locationlst = Ctydata.data;
                    if (response.data.selectedLocations != null) {
                        for (i = 0; i < response.data.selectedLocations.length; i++) {
                            var a = _.find($scope.Locationlst, { LCM_CODE: response.data.selectedLocations[i].LCM_CODE });
                            if (a)
                                a.ticked = true;
                        }
                    }
                    else {
                        $scope.Locationlst = Ctydata.data;
                    }

                    //towers
                    UtilityService.getTowers(1).then(function (Locdata) {
                        $scope.Towerlist = Locdata.data;
                        if (response.data.selectedTowers != null) {
                            for (i = 0; i < response.data.selectedTowers.length; i++) {
                                var a = _.find($scope.Towerlist, { TWR_CODE: response.data.selectedTowers[i].TWR_CODE });
                                if (a)
                                    a.ticked = true;
                            }
                        }
                        else {
                            $scope.Towerlist = Locdata.data;
                        }

                        //floors
                        UtilityService.getFloors(1).then(function (Flrdata) {
                            $scope.Floorlist = Flrdata.data;
                            if (response.data.selectedFloors != null) {
                                for (i = 0; i < response.data.selectedFloors.length; i++) {
                                    var a = _.find($scope.Floorlist, { FLR_CODE: response.data.selectedFloors[i].FLR_CODE });
                                    if (a)
                                        a.ticked = true;
                                }
                            }
                            else {
                                $scope.Floorlist = Flrdata.data;
                            }

                            //parent entity
                            UtilityService.getParentEntity(1).then(function (parent) {
                                $scope.Parentlist = parent.data;
                                if (response.data.selectedParentEntity != null) {
                                    for (i = 0; i < response.data.selectedParentEntity.length; i++) {
                                        var a = _.find($scope.Parentlist, { PE_CODE: response.data.selectedParentEntity[i].PE_CODE });
                                        if (a)
                                            a.ticked = true;
                                    }
                                }

                                //child
                                UtilityService.getChildEntity(1).then(function (chddata) {
                                    $scope.Childlist = chddata.data;
                                    if (response.data.selectedChildEntity != null) {
                                        for (i = 0; i < response.data.selectedChildEntity.length; i++) {
                                            var a = _.find($scope.Childlist, { CHE_CODE: response.data.selectedChildEntity[i].CHE_CODE });
                                            if (a)
                                                a.ticked = true;
                                        }
                                    }

                                    //vert
                                    UtilityService.getVerticals(1).then(function (Vedata) {
                                        $scope.Verticallist = Vedata.data;
                                        if (response.data.selectedVerticals != null) {
                                            for (i = 0; i < response.data.selectedVerticals.length; i++) {
                                                var a = _.find($scope.Verticallist, { VER_CODE: response.data.selectedVerticals[i].VER_CODE });
                                                if (a)
                                                    a.ticked = true;
                                            }
                                        }

                                        //cost cent
                                        UtilityService.getCostCenters(1).then(function (Csdata) {
                                            $scope.CostCenterlist = Csdata.data;
                                            if (response.data.selectedCostcenter != null) {
                                                for (i = 0; i < response.data.selectedCostcenter.length; i++) {
                                                    var a = _.find($scope.CostCenterlist, { Cost_Center_Code: response.data.selectedCostcenter[i].Cost_Center_Code });
                                                    if (a)
                                                        a.ticked = true;
                                                }
                                            } else { $scope.CostCenterlist = Csdata.data; }

                                            progress(0, '', false);
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    };

    //excel upload
    $scope.UploadFile = function () {
        if ($('#FileUpl', $('#FileUsrUpl')).val()) {
            var filetype = $('#FileUpl', $('#FileUsrUpl')).val().substring($('#FileUpl', $('#FileUsrUpl')).val().lastIndexOf(".") + 1);
            if (filetype == "xlsx" || filetype == "xls") {
                var formData = new FormData();
                // $scope.currentSLA.SLA_CREATED_BY = login;
                var UplFile = $('#FileUpl')[0];

                formData.append("UplFile", UplFile.files[0]);
                formData.append("CurrObj", "");
                $.ajax({
                    url: UtilityService.path + '/api/UserRoleMapping/UploadTemplate',
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        var respdata = JSON.parse(data);
                        if (respdata.data != null)
                            $scope.LoadUploadedData(respdata.data);
                        else
                            showNotification('error', 8, 'bottom-right', respdata.Message);
                    }
                });
            }
            else
                showNotification('error', 8, 'bottom-right', 'Please Upload only Excel (.xlsx) File(s)');
        }
        else
            showNotification('error', 8, 'bottom-right', 'Please Select File To Upload');
    }

    $scope.Rolechk = function () {
        var ChkLength = $filter('filter')($scope.acceslevel, { isChecked: true }).length;

        if (ChkLength > 0) {
            $scope.RolValidation = true;
        }
        else {
            $scope.RolValidation = false;
        }
    }

    function getDataAsCsvCustom(params) {

        var csvString = $scope.gridOptions.api.getDataAsCsv(params);
        var fileNamePresent = params && params.fileName && params.fileName.length !== 0;
        var fileName = fileNamePresent ? params.fileName : 'export.csv';

        var downloadLink = document.createElement("a");
        downloadLink.href = 'data:text/csv;charset=utf-8,%EF%BB%BF' + encodeURIComponent(csvString);
        downloadLink.download = fileName;
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);

    }







    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "UserRoleMapping.csv"
        };

        getDataAsCsvCustom(Filterparams)

        //$scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.GenReport = function (Allocdata, Type) {
        progress(0, 'Loading...', true);
        Allocdata.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Allocdata.Type == "pdf") {
                progress(0, 'Loading...', true);
                $scope.GenerateFilterPdf();
                progress(0, '', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GenerateFilterExcel();
                progress(0, '', false);
            }

        }
        else {
            $scope.DocTypeVisible = 0
            progress(0, 'Loading...', true);
            $http({
                url: UtilityService.path + '/api/UserRoleMapping/GetEmployeeData',
                method: 'POST',
                data: Allocdata,
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'EmployeeData.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {
            });
        }
    };

    $scope.LoadUploadedData = function (data) {
        $scope.$apply(function () {
            $scope.Viewstatus = 2;
            $scope.gridata1 = data.Table;
            $scope.gridOptions1.api.setRowData($scope.gridata1);
        });
    }

    $scope.selectedEmp = function (selected) {
        if (selected) {
            $scope.selectedEmployee = selected.originalObject;
            $scope.$broadcast('angucomplete-alt:changeInput', 'ex7', $scope.selectedEmployee.NAME);
            $scope.CurrentProfileEmp.AUR_REPORTING_TO = $scope.selectedEmployee.AUR_ID;
            $scope.CurrentProfileEmp.VERTICAL = $scope.selectedEmployee.Vertical;
            $scope.CurrentProfileEmp.COSTCENTER = $scope.selectedEmployee.Costcenter;
            //setTimeout(function () { $('.selectpicker').selectpicker('refresh'); }, 400);
        }
    };

    $scope.remoteUrlRequestFn = function (str) {
        return { q: str };
    };




    $scope.reselectedEmp = function (obj) {
        if (obj) {
            $scope.selectedEmployee = obj.selected.data;
            $scope.$broadcast('angucomplete-alt:changeInput', 'ex7', $scope.selectedEmployee.NAME);
            $scope.CurrentProfileEmp.AUR_REPORTING_TO = $scope.selectedEmployee.AUR_ID;
        } else {

        }

    };

    $scope.Currentmodule.MapAccess = "Y";

    $scope.radioItems = [{ Id: 1, Value: 'Y' }, { Id: 2, Value: 'N' }];



    UtilityService.getDesignation().then(function (response) {
        //setTimeout(function () { $('#DSG').selectpicker('refresh'); }, 400);
        $scope.designationddl = response.data;
        return UtilityService.getLocations(1);
    }).then(function (response) {
        //setTimeout(function () { $('#LOCATION').selectpicker('refresh'); }, 500);
        $scope.locationddl = response.data;
        return UtilityService.getVerticals(1);
    }).then(function (response) {
        //setTimeout(function () { $('#VERTICAL').selectpicker('refresh'); }, 400);
        $scope.verticalddl = response.data;
        return UtilityService.getCostCenters(1)
    }).then(function (response) {
        //setTimeout(function () { $('#COSTCENTER').selectpicker('refresh'); }, 500);
        $scope.costcenterddl = response.data;
    }).catch(function (error) {
        console.log(error);
    });


    $scope.Alert = function () {
        if (UserEdit == "1") {
            if ($('#ddlusrStatus').val() == "0") {
                alert('When You Inactive Current Employee Then It Will Be Released From The Allocated Space');
            }
        }
    }

    if (UserEdit == "1") { $("#UserId").attr("readonly", false); $("#Username").attr("readonly", false); }
    else { $("#UserId").attr("readonly", false); $("#Username").attr("readonly", false); }

    $scope.VerChange = function (data) {
        $scope.Data = { VER_CODE: data };
        $scope.CurrentProfileEmp.COSTCENTER = "";
        UtilityService.getCostcenterByVerticalcode($scope.Data, 1).then(function (response) {
            setTimeout(function () { $('#COSTCENTER').selectpicker('refresh'); }, 500);
            $scope.costcenterddl = response.data;
        }, function (error) {
            console.log(error);
        });
    }

}]);



