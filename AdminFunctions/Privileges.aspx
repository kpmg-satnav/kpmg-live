﻿<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="Privileges.aspx.vb" Inherits="Privileges" Title="Privileges" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
  <%--  <link href="../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />--%>
    <link href="../BlurScripts/BlurCss/app-5b02d1ea3b.css" rel="stylesheet" />

    <script type="text/javascript" defer>
        function setup(id) {
            $('.date').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>

</head>
<body>
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Privileges" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel" style="padding-right: 45px;">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Privileges</h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" class="form-horizontal" runat="server">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#General" aria-controls="General" role="tab" data-toggle="tab">General Roles</a></li>
                            <li><a href="#Property" aria-controls="Property" role="tab" data-toggle="tab">Property Roles</a></li>
                            <li><a href="#Space" aria-controls="Space" role="tab" data-toggle="tab">Space Roles</a></li>
                            <li><a href="#Asset" aria-controls="Asset" role="tab" data-toggle="tab">Asset Roles</a></li>
                            <li><a href="#Maintenance" aria-controls="Maintenance" role="tab" data-toggle="tab">Maintenance Roles</a></li>
                            <li><a href="#Helpdesk" aria-controls="Helpdesk" role="tab" data-toggle="tab">Helpdesk Roles</a></li>
                            <li><a href="#Conference" aria-controls="Conference" role="tab" data-toggle="tab">Reservation Roles</a></li>
                            <li><a href="#GuestHouse" id="GuestHouseTitle" aria-controls="Guest House" role="tab" data-toggle="tab"></a></li>
                            <li><a href="#BusinessCard" aria-controls="Guest House" role="tab" data-toggle="tab">Business Card Roles</a></li>
                        </ul>

                        <!-- Tab Panes -->
                        <div id="privilegesList">
                            <div class="tab-content" style="padding-top: 20px">
                                <div role="tabpanel" class="tab-pane active" align="center" id="General">
                                </div>

                                <div role="tabpanel" class="tab-pane" align="center" id="Property"></div>

                                <div role="tabpanel" class="tab-pane" align="center" id="Space"></div>

                                <div role="tabpanel" class="tab-pane" align="center" id="Asset"></div>

                                <div role="tabpanel" class="tab-pane" align="center" id="Maintenance"></div>

                                <div role="tabpanel" class="tab-pane" align="center" id="Helpdesk"></div>

                                <div role="tabpanel" class="tab-pane" align="center" id="Conference"></div>

                                <div role="tabpanel" class="tab-pane" align="center" id="GuestHouse"></div>

                                <div role="tabpanel" class="tab-pane" align="center" id="BusinessCard"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


<script type="text/javascript" defer>
    $(document).ready(function () {
        $.getJSON("../api/AdminfunctionsAPI/GetRoledata", function (resultdata) {
            $.each(resultdata, function (key, value) {
                var table = $("#" + value.ROL_MODULE);
                table.append("<tr>" +
                                    "<td style='text-align:left;fit-position:left'><a href='ADF_WebFiles/frmPrivilegesV2.aspx?_id=" + value.ROL_ID + "&Value=" + value.ROL_ACRONYM + "'>" + value.ROL_ACRONYM + "</a></td>" +
                                    "<td>" + '&nbsp&nbsp&nbsp' + "</td>" +
                                    "<td>" + '➜' + "</td>" +
                                    "<td>" + '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' + "</td>" +
                                    "<td style='text-align:left;'>" + value.ROL_DESCRIPTION + "</td>" +
                              "</tr>");
            });
        });
    });
</script>
<script type="text/javascript" defer>
    var GHT = '<%= Session("GHT")%>';
    $(document).ready(function () {
        $("#GuestHouseTitle").append(GHT).append(" Roles");
    });

</script>
