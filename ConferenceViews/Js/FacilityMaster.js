﻿app.service("FacilityMasterService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.SaveFacility = function (response) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/FacilityMaster/SaveFacilityDetails', response)
         .then(function (response) {
             console.log(response);
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/FacilityMaster/GetGridData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.ModifyFacility = function (response) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/FacilityMaster/ModifyFacilityDetails', response)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

}]);

app.controller('FacilityMasterController', ['$scope', '$q', 'FacilityMasterService', 'UtilityService', '$filter', function ($scope, $q, FacilityMasterService, UtilityService, $filter) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.Facility = {};
    $scope.Locations = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.LocationArray = [];
    $scope.ShowMessage = false;

    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Locations = response.data;
        }
    });

    $scope.SaveData = function () {
        progress(0, 'Loading...', true);
        if ($scope.IsInEdit) {
            var savedobj = { LocatinoList: $scope.Facility.Locations, FMastr: $scope.Facility };
            FacilityMasterService.ModifyFacility(savedobj).then(function (resposne) {
                $scope.IsInEdit = false;

                $scope.gridOptions.api.setRowData([]);
                $scope.LoadData();
                $scope.Clear();
                $scope.ActionStatus = 0;
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', resposne.Message);
            }, function (error) {
                console.log(error);
            });
        }
        else {
            $scope.Facility.FACILITY_STATUS = "1";
            var savedobj = { LocatinoList: $scope.Facility.Locations, FMastr: $scope.Facility };
            FacilityMasterService.SaveFacility(savedobj).then(function (resposne) {              
                $scope.gridOptions.api.setRowData([]);
                $scope.LoadData();
                $scope.Clear();
                $scope.ActionStatus = 0;
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', resposne.Message);
            }, function (error) {
                console.log(error);
            });
        }
    }

    var columnDefs = [
               { headerName: "Facility Code", field: "FACILITY_CODE", width: 190, cellClass: 'grid-align' },
               { headerName: "Facility Name", field: "FACILITY_NAME", width: 280, cellClass: 'grid-align' },
              { headerName: "Location", field: "LCM_NAME", width: 280, cellClass: 'grid-align' },
               { headerName: "Unit Cost", field: "FACILITY_UNIT_COST", width: 280, cellClass: 'grid-align', suppressMenu: true, },
               { headerName: "Status", template: "{{ShowStatus(data.FACILITY_STATUS)}}", width: 170, cellClass: 'grid-align' },
               { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110, suppressMenu: true, }];

    $scope.LoadData = function () {
        FacilityMasterService.GetGriddata().then(function (data) {
            $scope.gridata = data;
            $scope.gridOptions.api.setRowData(data);
        }, function (error) {
            console.log(error);
        });
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
     $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableColResize: true,
        enableFilter: true,
        enableSorting: true,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    $scope.LoadData();

    $scope.EditData = function (data) {
        console.log(data);
        $scope.ActionStatus = 1;
        angular.copy(data, $scope.Facility);
        $scope.LocationArray = data.LCM_CODE.split(',');
        _($scope.Locations).forEach(function (value) {
            value.ticked = false;
        });

        _($scope.LocationArray).forEach(function (value) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value });
            lcm.ticked = true;
        });
        $scope.IsInEdit = true;        
    }

    $scope.Clear = function () {
        angular.forEach($scope.Locations, function (Lcm) {
            Lcm.ticked = false;
        });
        $scope.Facility.Locations = [];
        $scope.Facility = {};
        $scope.ActionStatus = 0;
        $scope.frm.$submitted = false;
    }
    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

}]);