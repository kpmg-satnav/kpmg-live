﻿app.service("ConferenceCustomizableReportService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (ConferenceUtilizationReport) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceCustomizableReport/GetConferenceReportBindGrid', ConferenceUtilizationReport)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);
app.controller('ConferenceCustomizableReport', ['$scope', '$q', '$http', 'ConferenceCustomizableReportService', 'UtilityService', '$timeout', '$filter', function ($scope, $q, $http, ConferenceCustomizableReportService, UtilityService, $timeout, $filter) {
    $scope.ConferenceUtilizationReport = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.BsmDet = { Parent: "", Child: "" };
    $scope.Company = [];
    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.ConferenceUtilizationReport.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });
    }, 100);

    $scope.Pageload = function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.ConferenceUtilizationReport.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Location = response.data;
                angular.forEach($scope.Location, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getTowers(2).then(function (response) {
            if (response.data != null) {
                $scope.Tower = response.data;
                angular.forEach($scope.Tower, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getFloors(2).then(function (response) {
            if (response.data != null) {
                $scope.Floor = response.data;
                angular.forEach($scope.Floor, function (value, key) {
                    value.ticked = true;
                })
            }
        });
    }

    $scope.cnySelectAll = function () {
        $scope.ConferenceUtilizationReport.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }
    $scope.CnySelectNone = function () {
        $scope.ConferenceUtilizationReport.Country = [];
        $scope.getCitiesbyCny();
    }


    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.ConferenceUtilizationReport.Country, 1).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.ctySelectAll = function () {
        $scope.ConferenceUtilizationReport.City = $scope.City;
        $scope.getLocationsByCity();
    }
    $scope.CtySelectNone = function () {
        $scope.ConferenceUtilizationReport.City = [];
        $scope.getLocationsByCity();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.ConferenceUtilizationReport.City, 1).then(function (response) {
            $scope.Location = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ConferenceUtilizationReport.Country[0] = cny;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.ConferenceUtilizationReport.Location = $scope.Location;
        $scope.getTowerByLocation();
    }
    $scope.LcmSelectNone = function () {
        $scope.ConferenceUtilizationReport.Location = [];
        $scope.getTowerByLocation();
    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.ConferenceUtilizationReport.Location, 2).then(function (response) {
            $scope.Tower = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ConferenceUtilizationReport.Country[0] = cny;
            }
        });

        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ConferenceUtilizationReport.City[0] = cty;
            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.ConferenceUtilizationReport.Tower = $scope.Tower;
        $scope.getFloorByTower();
    }
    $scope.TwrSelectNone = function () {
        $scope.ConferenceUtilizationReport.Tower = [];
        $scope.getFloorByTower();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.ConferenceUtilizationReport.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ConferenceUtilizationReport.Country[0] = cny;
            }
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ConferenceUtilizationReport.City[0] = cty;
            }
        });

        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ConferenceUtilizationReport.Location[0] = lcm;
            }
        });
    }

    $scope.floorChangeAll = function () {
        $scope.ConferenceUtilizationReport.Floor = $scope.Floor;
        $scope.FloorChange();
    }
    $scope.FloorSelectNone = function () {
        $scope.Floorlist = [];
        $scope.FloorChange();
    }

    $scope.FloorChange = function () {

        $scope.ConferenceUtilizationReport.Country = [];
        $scope.ConferenceUtilizationReport.City = [];
        $scope.ConferenceUtilizationReport.Location = [];
        $scope.ConferenceUtilizationReport.Tower = [];

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ConferenceUtilizationReport.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ConferenceUtilizationReport.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ConferenceUtilizationReport.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.ConferenceUtilizationReport.Tower.push(twr);
            }
        });

    }

    $scope.Cols = [
        { COL: "Request Id", value: "Request_Id", ticked: false },
        { COL: "Country", value: "COUNTRY", ticked: false },
        { COL: "City", value: "CITY", ticked: false },
        { COL: "Location", value: "LOCATION", ticked: false },
        { COL: "TOWER", value: "TOWER", ticked: false },
        { COL: "FLOOR", value: "FLOOR", ticked: false },
        { COL: "Reservation Room Name", value: "RoomName", ticked: false },
        { COL: "Employee Id", value: "EMP_ID", ticked: false },
        { COL: "Employee Name", value: "EMP_NAME", ticked: false },
        { COL: "Department", value: "DEP_NAME", ticked: false },
        { COL: "From Date", value: "From_Date", ticked: false },
        { COL: "To Date", value: "To_Date", ticked: false },
        { COL: "From Time", value: "From_Time", ticked: false },
        { COL: "To Time", value: "To_Time", ticked: false },
        { COL: "Total Hours", value: "TotalHours", ticked: false },
        { COL: "Status", value: "Status", ticked: false },
        { COL: "Description", value: "Description", ticked: false }
    ];

    $scope.columnDefs = [
        { headerName: "Request Id", field: "Request_Id", width: 100, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Country", field: "COUNTRY", width: 100, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "City", field: "CITY", cellClass: 'grid-align', width: 100 },
        { headerName: "Location", field: "LOCATION", cellClass: 'grid-align', width: 150 },
        { headerName: "Tower", field: "TOWER", cellClass: 'grid-align', width: 100 },
        { headerName: "Floor", field: "FLOOR", cellClass: 'grid-align', width: 100 },
        { headerName: "Reservation Room Name", field: "RoomName", cellClass: 'grid-align', width: 150 },
        { headerName: "Employee Id", field: "EMP_ID", cellClass: 'grid-align', width: 150 },
        { headerName: "Employee Name", field: "EMP_NAME", cellClass: 'grid-align', width: 150 },
        { headerName: "Department", field: "DEP_NAME", cellClass: 'grid-align', width: 150 },
        { headerName: "From Date", field: "From_Date", template: '<span>{{data.From_Date | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "To Date", field: "To_Date", template: '<span>{{data.To_Date | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "From Time", field: "From_Time", cellClass: 'grid-align', width: 100 },
        { headerName: "To Time", field: "To_Time", cellClass: 'grid-align', width: 100 },
        { headerName: "Total Hours", field: "TotalHours", cellClass: 'grid-align', width: 100 },
        { headerName: "Status", field: "Status", cellClass: 'grid-align', width: 100 },
        { headerName: "Description", field: "Description", cellClass: 'grid-align', width: 100 }
    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: false,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function groupAggFunction(rows) {
        var sums = {
            TotalHours: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.TotalHours += parseFloat((data.TotalHours).toFixed(2));
        });
        return sums;
    }
    var OVERALL_COUNT;
    $scope.LoadData = function (stat, ReqType) {
        
        if (ReqType == 1) {
            ReqType = $scope.ConferenceUtilizationReport.Request_Type;
        }
        var searchval;
        if (stat == 0) {
            searchval = "";
            $("#filtertxt").val("");
        }
        else {
            searchval = $("#filtertxt").val();
        }
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
            flrlst: $scope.ConferenceUtilizationReport.Floor,
            Request_Type: ReqType,
            STAT: stat,
            FromDate: $scope.ConferenceUtilizationReport.FromDate,
            ToDate: $scope.ConferenceUtilizationReport.ToDate,
            CNP_NAME: $scope.ConferenceUtilizationReport.CNP_NAME[0].CNP_ID
        };
        ConferenceCustomizableReportService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                $("#btNext").attr("disabled", true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData([]);
                $scope.gridOptions.api.refreshView();
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                OVERALL_COUNT=$scope.gridata[0].OVERALL_COUNT;
                $scope.gridOptions.api.setRowData($scope.gridata);
                var cols = [];
               // console.log($scope.Cols);
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });
                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                //console.log(cols);
                $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
                cols = [];
                for (i = 0; i < ticked.length; i++) {
                    cols[i] = ticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
            }
            progress(0, '', false);
        });

    
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };
    $scope.CustmPageLoad = function () {


    }, function (error) {
        console.log(error);
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [
            { title: "Country", key: "COUNTRY" }, { title: "City", key: "CITY" },
            { title: "Location", key: "LOCATION" }, { title: "Tower", key: "TOWER" },
            { title: "Floor", key: "FLOOR" },
            { title: "Employee", key: "EMP_NAME" },
            { title: "Room Name", key: "RoomName" },

            { title: "From Date", key: "From_Date" }, { title: "To Date", key: "To_Date" },
             { title: "From Time", key: "From_Time" },
            { title: "To Time", key: "To_Time" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("Conference Room Booking Report.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Conference Room Booking Report.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.ConferenceUtilizationReport.Request_Type = "All";
    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.selVal = "30";
    //$scope.ConferenceUtilizationReport.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
    //$scope.ConferenceUtilizationReport.ToDate = moment().format('MM/DD/YYYY');
    $scope.rptDateRanges = function () {
        //console.log($scope.selVal);
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.ConferenceUtilizationReport.FromDate = moment().format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.ConferenceUtilizationReport.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.ConferenceUtilizationReport.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':                
                $scope.ConferenceUtilizationReport.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.ConferenceUtilizationReport.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.ConferenceUtilizationReport.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }

    $scope.GenReport = function (ConferenceUtilizationReport, Type) {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        var dataobj = {
            SearchValue: searchval,
            PageNumber: 1,
            PageSize: OVERALL_COUNT,
            Request_Type: $scope.ConferenceUtilizationReport.Request_Type,
            flrlst: $scope.ConferenceUtilizationReport.Floor,
            FromDate: $scope.ConferenceUtilizationReport.FromDate,
            ToDate: $scope.ConferenceUtilizationReport.ToDate,
            CNP_NAME: $scope.ConferenceUtilizationReport.CNP_NAME[0].CNP_ID,
            Type: Type
        };
      
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type === "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/ConferenceCustomizableReport/GetCustomizedData',
                method: 'POST',
                data: dataobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Conference Room Booking Report.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    $scope.Pageload();
    setTimeout(function () {
        $scope.LoadData(1, 'All');
    }, 2000);
    //setTimeout(function () {
    //    $scope.Pageload();
    //}, 500);

}]);
