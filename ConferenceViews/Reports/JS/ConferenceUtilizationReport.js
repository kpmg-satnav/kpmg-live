﻿app.service("ConferenceUtilizationReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetGridData = function (AreaRpt) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceUtilizationReport/GetConferenceReportBindGrid', AreaRpt)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('ConferenceUtilizationReportController', ['$scope', '$q', 'ConferenceUtilizationReportService', 'UtilityService', '$timeout', '$http', function ($scope, $q, ConferenceUtilizationReportService, UtilityService, $timeout, $http) {
    $scope.ConferenceUtilizationReport = {};
    $scope.Viewstatus = 0;
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.EnableStatus = 0;
    $scope.CompanyVisible = 0;
    $scope.GridVisiblity = 0;
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];

    $scope.SelRowData = [];

    $scope.ConferenceUtilizationReport.Country = [];
    $scope.ConferenceUtilizationReport.City = [];
    $scope.ConferenceUtilizationReport.Location = [];
    $scope.ConferenceUtilizationReport.Tower = [];
    $scope.ConferenceUtilizationReport.Floor = [];

    UtilityService.getCountires(2).then(function (Countries) {
        if (Countries.data != null) {
            $scope.Country = Countries.data;
        }
    });
    UtilityService.getBussHeirarchy().then(function (response) {
        if (response.data != null) {
            $scope.BsmDet = response.data;
        }
    });


    UtilityService.getCountires(2).then(function (response) {
        if (response.data != null) {
            $scope.Country = response.data;
            angular.forEach($scope.Country, function (value, key) {
                value.ticked = true;
            })

            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.City = response.data;
                    angular.forEach($scope.City, function (value, key) {
                        value.ticked = true;
                    })

                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Location = response.data;
                            angular.forEach($scope.Location, function (value, key) {
                                value.ticked = true;
                            })

                            UtilityService.getTowers(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Tower = response.data;
                                    angular.forEach($scope.Tower, function (value, key) {
                                        value.ticked = true;
                                    })

                                    UtilityService.getFloors(2).then(function (response) {
                                        if (response.data != null) {
                                            $scope.Floor = response.data;
                                            angular.forEach($scope.Floor, function (value, key) {
                                                value.ticked = true;
                                            })

                                        }
                                    });

                                }
                            });
                        }
                    });
                }

            });

        }
    });


    //// Country Events
    $scope.CnyChangeAll = function () {
        $scope.ConferenceUtilizationReport.Country = $scope.Country;
        $scope.CnyChanged();
    }

    $scope.CnySelectNone = function () {
        $scope.ConferenceUtilizationReport.Country = [];
        $scope.CnyChanged();
    }

    $scope.CnyChanged = function () {
        if ($scope.ConferenceUtilizationReport.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.ConferenceUtilizationReport.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.ConferenceUtilizationReport.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.ConferenceUtilizationReport.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.ConferenceUtilizationReport.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ConferenceUtilizationReport.Country.push(cny);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.ConferenceUtilizationReport.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.ConferenceUtilizationReport.Location = [];
        $scope.LcmChanged();
    }

    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.ConferenceUtilizationReport.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ConferenceUtilizationReport.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ConferenceUtilizationReport.City.push(cty);
            }
        });


    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.ConferenceUtilizationReport.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    $scope.TwrSelectNone = function () {
        $scope.ConferenceUtilizationReport.Tower = [];
        $scope.TwrChanged();
    }

    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.ConferenceUtilizationReport.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ConferenceUtilizationReport.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ConferenceUtilizationReport.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ConferenceUtilizationReport.Location[0].push(lcm);
            }
        });



    }

    //// floor events
    $scope.FlrChangeAll = function () {
        $scope.ConferenceUtilizationReport.Floor = $scope.Floor;
        $scope.FlrChanged();
    }

    $scope.FlrSelectNone = function () {
        $scope.ConferenceUtilizationReport.Floor = [];
        $scope.FlrChanged();
    }

    $scope.FlrChanged = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ConferenceUtilizationReport.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ConferenceUtilizationReport.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ConferenceUtilizationReport.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.ConferenceUtilizationReport.Tower.push(twr);
            }
        });

    }



    UtilityService.GetCompanies().then(function (response) {
        if (response.data != null) {
            $scope.Company = response.data;
            $scope.ConferenceUtilizationReport.CNP_NAME = [];
            //$scope.AreaReport.CNP_NAME = parseInt(CompanySession);
            //console.log($scope.AreaReport.CNP_NAME);
            angular.forEach($scope.Company, function (value, key) {
                var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                if (a) {
                    a.ticked = true;
                    $scope.ConferenceUtilizationReport.CNP_NAME.push(a);
                }
            });
            if (CompanySession == "1") { $scope.EnableStatus = 1; }
            else { $scope.EnableStatus = 0; }
            setTimeout(function () {
                $scope.LoadData();
            }, 1000);
        }
    });

    var OVERALL_COUNT;
    $scope.LoadData = function (std) {
        var searchval; 
        if (std == 0) {
            searchval = "";
            $("#filtertxt").val("");
        }
        else {
             searchval = $("#filtertxt").val();
        }
       
        $("#btLast").hide();
        $("#btFirst").hide();
        //console.log($scope.ConferenceUtilizationReport.CNP_NAME[0]);
        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    CNP_NAME: $scope.ConferenceUtilizationReport.CNP_NAME[0].CNP_ID,
                    flrlst: $scope.ConferenceUtilizationReport.Floor,
                    CNF_FROM_DATE: $scope.ConferenceUtilizationReport.CNF_FROM_DATE,
                    CNF_TO_DATE: $scope.ConferenceUtilizationReport.CNF_TO_DATE

                };
                var fromdate = $scope.ConferenceUtilizationReport.CNF_FROM_DATE
                var todate = $scope.ConferenceUtilizationReport.CNF_TO_DATE

                if (fromdate > todate) {
                    $scope.GridVis = false;
                    showNotification('error', 8, 'bottom-right', "Fromdate Should Be Less Than Todate");
                }
                else {
                    ConferenceUtilizationReportService.GetGridData(params).then(function (response) {
                        progress(0, 'Loading...', true);
                        $scope.gridata = response.data;
                        if (response.data == null) {
                            $("#btNext").attr("disabled", true);
                            $scope.gridOptions.api.setRowData([]);
                            $scope.GridVisiblity = false;
                            progress(0, 'Loading...', false);
                            showNotification('error', 8, 'bottom-right', response.Message);

                        }
                        else {
                            $scope.GridVisiblity = true;
                            progress(0, 'Loading...', true);
                            OVERALL_COUNT = $scope.gridata[0].OVERALL_COUNT;
                            $scope.gridOptions.api.setRowData($scope.gridata);
                            progress(0, 'Loading...', false);
                        }
                        $scope.AreaChartDetais(params);
                    }, function (error) {
                        console.log(error);
                    });
                }

            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    //setTimeout(function () {
    //    $scope.LoadData();
    //}, 1000);
    var columnDefs = [
        { headerName: "Country", field: "CNY_NAME", width: 150, cellClass: 'grid-align', filter: 'set' },
        { headerName: "City", field: "CTY_NAME", width: 200, cellClass: 'grid-align' },
        { headerName: "Location", field: "LCM_NAME", width: 150, cellClass: 'grid-align', },
        { headerName: "Tower", field: "TWR_NAME", width: 150, cellClass: 'grid-align' },
        { headerName: "Floor", field: "FLR_NAME", width: 150, cellClass: 'grid-align', },
        { headerName: "Room Name", field: "CONFERENCE_ROOM_NAME", width: 200, cellClass: 'grid-align' },
        { headerName: "Total Hours", field: "TOTAL_HOURS", width: 80, cellClass: 'grid-align' }

    ];

    $scope.pageSize = '10';

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        rowData: null,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    //$scope.LoadData();

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
        } else {
            $scope.DocTypeVisible = 0;
        }
    }

    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    $("#Tabular").fadeIn();
    $("#table2").fadeIn();
    $("#Graphicaldiv").fadeOut();
    var chart;
    chart = c3.generate({
        data: {
            //x: 'x',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Location'],
                height: 130,
                show: true,
                //label: {
                //    text: 'Locations',
                //    position: 'outer-center'
                //}
            },
            y: {
                show: true,
                label: {
                    text: 'Area in Sq.Ft',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });


    $scope.AreaChartDetais = function (utilization) {
        $http({
            url: UtilityService.path + '/api/ConferenceUtilizationReport/GetConferenceReportChartData/',
            method: 'POST',
            data: utilization
        }).
            success(function (result) {
                chart.unload();
                chart.load({ columns: result });
            });
        setTimeout(function () {
            $("#SpcGraph").append(chart.element);
        }, 700);
    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
            });
        }
    });

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" },
        { title: "Room Name", key: "CONFERENCE_ROOM_NAME" }, { title: "Total Hours", key: "TOTAL_HOURS" }
        ];
        progress(0, 'Loading...', true);
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("Reservation Utilization Report.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ",",
            fileName: "Reservation Utilization Report.csv"
        }; $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (AreaReport, Type) {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        eaobj = {};
        eaobj.SearchValue = searchval;
        eaobj.PageNumber = 1;
        eaobj.PageSize = OVERALL_COUNT;
        eaobj.CNP_NAME = $scope.ConferenceUtilizationReport.CNP_NAME[0].CNP_ID;

        eaobj.flrlst = $scope.ConferenceUtilizationReport.Floor,
            eaobj.CNF_FROM_DATE = $scope.ConferenceUtilizationReport.CNF_FROM_DATE,
            eaobj.CNF_TO_DATE = $scope.ConferenceUtilizationReport.CNF_TO_DATE

        eaobj.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/ConferenceUtilizationReport/ExportConferenceUtilizationReportData',
                method: 'POST',
                data: eaobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Reservation Utilization Report.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }


    $scope.selVal = "30";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.ConferenceUtilizationReport.FromDate = moment().format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.ConferenceUtilizationReport.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.ConferenceUtilizationReport.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.ConferenceUtilizationReport.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.ConferenceUtilizationReport.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.ConferenceUtilizationReport.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'NEXTMONTH':
                $scope.ConferenceUtilizationReport.FromDate = moment().add(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.ConferenceUtilizationReport.ToDate = moment().add(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }

}]);