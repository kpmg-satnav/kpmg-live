﻿<%@ Page Language="VB" AutoEventWireup="false" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="ConferenceAssetMasterController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Conference Type Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Reservation Asset Master</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form role="form" id="form1" name="frm" data-valid-submit="Save()" novalidate>
                        <div class="clearfix">
                            <div class="box-footer text-right">
                                <span style="color: red;">*</span>  Mandatory field 
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.AST_CODE.$invalid}">
                                    <label>Asset Code<span style="color: red;">*</span></label>
                                    <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                        <input id="txtcode" name="AST_CODE" type="text" data-ng-model="AssetType.AST_CODE" data-ng-pattern="codepattern" maxlength="15" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                        <span class="error" data-ng-show="frm.$submitted && frm.AST_CODE.$invalid" style="color: red">Please Enter Valid Code</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.AST_NAME.$invalid}">
                                    <label>Asset Name<span style="color: red;">*</span></label>
                                    <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                        onmouseout="UnTip()">
                                        <input id="txtCName" name="AST_NAME" type="text" data-ng-model="AssetType.AST_NAME" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" />
                                        <span class="error" data-ng-show="frm.$submitted && frm.AST_NAME.$invalid" style="color: red">Please Enter Valid Name</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                        <textarea id="txtremarks" style="height: 30%" name="AST_REMARKS" data-ng-model="AssetType.AST_REMARKS" class="form-control" maxlength="500"></textarea>
                                        <%--<span class="error" data-ng-show="frm.$submitted && frm.AST_REMARKS.$invalid" style="color: red">Please Enter Remarks</span>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frm.$submitted && frm.AST_STA_ID.$invalid}">
                                        <label>Status<span style="color: red;">*</span></label>
                                        <select id="ddlsta" name="Status" data-ng-model="AssetType.AST_STA_ID" class="form-control">
                                            <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                        </select>
                                        <span class="error" data-ng-show="frm.$submitted && frm.AST_STA_ID.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please Select Status</span>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                    <input type="reset" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="EraseData()" />
                                    <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <input id="filtertxt" placeholder="Filter..." type="text" class="form-control" style="width: 25%;" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 300px; width: 100%"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <%--<script src="../Js/ConferenceAssetMaster.js"></script>--%>
    <script src="../Js/ConferenceAssetMaster.min.js" defer></script>
    <script src="../../SMViews/Utility.min.js" defer></script>
    <%--<script src="../../SMViews/Utility.js"></script>--%>
</body>
</html>
