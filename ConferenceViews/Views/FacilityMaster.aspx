﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="FacilityMasterController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Conference Type Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Reservation Facility Master</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form role="form" id="form1" name="frm" data-valid-submit="SaveData()" novalidate>
                        <div class="clearfix">
                            <div class="box-footer text-right">
                                <span style="color: red;">*</span>  Mandatory field 
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="clearfix">
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.FACILITY_CODE.$invalid}">
                                        <label>Facility Code<span style="color: red;">*</span></label>
                                        <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                            <input id="txtcode" name="FACILITY_CODE" type="text" data-ng-model="Facility.FACILITY_CODE" data-ng-pattern="codepattern" maxlength="15" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                            <span class="error" data-ng-show="frm.$submitted && frm.FACILITY_CODE.$invalid" style="color: red">Please Enter Valid Code</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.FACILITY_NAME.$invalid}">
                                        <label>Facility Name<span style="color: red;">*</span></label>
                                        <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                            onmouseout="UnTip()">
                                            <input id="txtCName" name="FACILITY_NAME" type="text" data-ng-model="Facility.FACILITY_NAME" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" />
                                            <span class="error" data-ng-show="frm.$submitted && frm.FACILITY_NAME.$invalid" style="color: red">Please Enter Valid Name</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.FACILITY_LCM.$invalid}">
                                        <label>Location <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Locations" data-output-model="Facility.Locations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Facility.Locations" name="FACILITY_LCM" style="display: none" required="" />
                                        <span class="error" data-ng-show="frm.$submitted && frm.FACILITY_LCM.$invalid" style="color: red">Please select location </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.FACILITY_UNIT_COST.$invalid}">
                                        <label>Unit Cost<span style="color: red;">*</span></label>
                                        <div onmouseover="Tip('Enter only numbers')" onmouseout="UnTip()">
                                            <input id="Text1" name="FACILITY_UNIT_COST" type="number" min="0" data-ng-model="Facility.FACILITY_UNIT_COST" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" />
                                            <span class="error" data-ng-show="frm.$submitted && frm.FACILITY_UNIT_COST.$invalid" style="color: red">Please Enter Unit Cost</span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label runat="server">Remarks</asp:Label>
                                        <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                            <textarea id="txtremarks" name="FACILITY_REMARKS" data-ng-model="Facility.FACILITY_REMARKS" class="form-control" style="height: 30%" maxlength="500" autofocus></textarea>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frm.$submitted && frm.CONF_STA_ID.$invalid}" style="width: 262px">
                                        <label>Status<span style="color: red;">*</span></label>
                                        <select id="ddlsta" name="Status" data-ng-model="Facility.FACILITY_STATUS" class="form-control">
                                            <option value="">--Select--</option>
                                            <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                        </select>
                                        <span class="error" data-ng-show="frm.$submitted && frm.FACILITY_STATUS.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please Select Status</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                    <input type="reset" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="Clear()" />
                                    <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <input id="filtertxt" placeholder="Filter..." type="text" class="form-control" style="width: 25%;" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 300px; width: 100%"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <%--<script src="../../SMViews/Utility.js"></script>--%>
    <%--<script src="../Js/FacilityMaster.js"></script>--%>
    <script src="../../SMViews/Utility.min.js" defer></script>
    <script src="../Js/FacilityMaster.min.js" defer></script>
</body>
</html>
