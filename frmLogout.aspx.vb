
Partial Class frmLogout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session.RemoveAll()
        Response.Write("<script language=""javascript"" type=""text/javascript"">window.top.location.href=""frmAMTDefault.aspx""</script>")
    End Sub
End Class
