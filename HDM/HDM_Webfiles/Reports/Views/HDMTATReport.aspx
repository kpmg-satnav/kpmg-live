﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
   

    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <%--  <link href="../../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        #pageRowSummaryPanel {
            display: none;
        }

        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMTATReportController" onload="setDateVals()" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="TAT Report" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">TAT Report</h3>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <form id="form1" name="frmHDMTAT" data-valid-submit="LoadData()">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Select Range</label>
                                        <br />
                                        <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                            <option value="TODAY">Today</option>
                                            <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="LASTMONTH">Last Month</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">From Date</label>
                                        <div class="input-group date" id='fromdate'>
                                            <input type="text" class="form-control" data-ng-model="HDMTAT.FromDate" id="FromDate" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmHDMTAT.$submitted && frmHDMTAT.FromDate.$invalid" style="color: red;">Please Select From Date</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">To Date</label>
                                        <div class="input-group date" id='todate'>
                                            <input type="text" class="form-control" data-ng-model="HDMTAT.ToDate" id="ToDate" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmHDMTAT.$submitted && frmHDMTAT.ToDate.$invalid" style="color: red;">Please Select To Date</span>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmHDMTAT.$submitted && frmHDMTAT.Request_Type.$invalid}">
                                        <label for="txtcode">Request Type</label>
                                        <select class="form-control" data-ng-model="HDMTAT.Request_Type" id="Request_Type" name="Request_Type" required="">
                                            <option value="ALL" selected>--ALL--</option>
                                            <option data-ng-repeat="ReqType in RequestTypes" value="{{ReqType.CODE}}">{{ReqType.NAME}}</option>
                                        </select>
                                        <span class="error" data-ng-show="frmHDMTAT.$submitted && frmHDMTAT.Request_Type.$invalid" style="color: red;">Please Select Request Type</span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                    <div class="form-group">
                                        <label class="control-label">Company</label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="HDMTAT.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                            item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="HDMTAT.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmHDMTAT.$submitted && frmHDMTAT.Company.$invalid" style="color: red">Please Select Company </span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <div class="box-footer text-right">
                                        <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                            </div>

                            <br />
                            <div class="row" style="padding-left: 18px">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6" id="table2">
                                    <br />
                                    <a data-ng-click="GenReport(HDMTAT,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(HDMTAT,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(HDMTAT,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                            </div>
                        </form>
                        <form id="form2">
                            <div id="Tabular" data-ng-show="GridVisiblity">
                                <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                    <div class="input-group" style="width: 20%">
                                        <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary custom-button-color" type="submit" data-ng-click="GetTAT()">
                                                <i class="glyphicon glyphicon-search"></i>
                                            </button>
                                        </div>
                                    </div>

                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                </div>
                            </div>
                            <div id="Graphicaldiv" data-ng-show="GridVisiblity">
                                <div id="SpcGraph">&nbsp</div>
                            </div>
                        </form>
                        <div class="modal fade bs-example-modal-lg col-md-12 " id="historymodal">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="panel-group box box-primary" id="Div2">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <div class="panel-heading ">
                                                <h4 class="panel-title modal-header-primary" style="padding-left: 17px" data-target="#collapseTwo">History Details</h4>
                                                <form role="form" name="form2" id="form3">
                                                    <div class="clearfix">
                                                        <div class="col-md-12">
                                                            <div class="box">
                                                                <div class="box-danger table-responsive">
                                                                    <div data-ag-grid="PopOptions" class="ag-blue" style="height: 305px; width: 100%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../../Dashboard/C3/c3.min.js" defer></script>
    <script src="../../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../../Scripts/moment.min.js" defer></script>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../Js/HDMTATReport.min.js" defer></script>
    <%--<script src="../Js/HDMTATReport.js" defer></script>--%>
    <script type="text/javascript" defer>
        var CompanySession = '<%= Session["COMPANYID"]%>';
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            $('#FromDate').datepicker('setDate', new Date(moment().subtract(1, 'days').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));
        }
    </script>
    <%--<script src="../../Utility.js"></script>--%>
    <script src="../../../../SMViews/Utility.min.js" defer></script>

</body>
</html>
