﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.css" rel="stylesheet" />
    <link href="../../../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <link href="../../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <script src="../../../../jszip.js"></script>

    <script src="../../../../Scripts/FileSaver.js"></script>
    <script src="../../../../jsutilis.js"></script>
    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script src="../../../../Scripts/FileSaver.js"></script>
    <script src="../../../../jszip.js"></script>
    <script src="../../../../jsutilis.js"></script>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }



        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMISFCReportByUserController" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Report by User" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">Report by User</h3>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <form role="form" id="HDMRptByUserDetails" name="frmHDMISFCRptByUserDetails" data-valid-submit="getHDMISFCReportByUser()">
                            <div class="clearfix">
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">Select Range<span style="color: red;">*</span></label>
                                            <br />
                                            <select id="ddlRange" class="selectpicker" ng-model="RptByUsr.selVal" data-ng-change="rptDateRanges()">
                                                <option value="">Select Range</option>
                                                <option value="TODAY">Today</option>
                                                <option value="YESTERDAY">Yesterday</option>
                                                <option value="7">Last 7 Days</option>
                                                <option value="30">Last 30 Days</option>
                                                <option value="THISMONTH">This Month</option>
                                                <option value="LASTMONTH">Last Month</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">From Date <span style="color: red;">*</span></label>
                                            <div class="input-group date" id='fromdate'>
                                                <input type="text" class="form-control" data-ng-model="RptByUsr.FromDate" id="FROM_DATE" name="FROM_DATE" required="" placeholder="mm/dd/yyyy" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                                </span>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">To Date <span style="color: red;">*</span></label>
                                            <div class="input-group date" id='todate'>
                                                <input type="text" class="form-control" data-ng-model="RptByUsr.ToDate" id="TO_DATE" name="TO_DATE" required="" placeholder="mm/dd/yyyy" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                                </span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmHDMISFCRptByUserDetails.$submitted && frmHDMISFCRptByUserDetails.Request_Type.$invalid}">
                                            <%--<label for="txtcode">Select Status</label>--%>
                                            <label class="control-label">Select Status<span style="color: red;"></span></label>
                                            <%--<select class="form-control" data-ng-model="RptByUsr.Status" id="Status" name="Status" selection-mode="multiple">
                                               <option value="3">--ALL--</option>
                                                <option data-ng-repeat="ReqType in StatusTypes" value="{{ReqType.SCODE}}">{{ReqType.SNAME}}</option>
                                            </select>--%>

                                            <div isteven-multi-select data-input-model="Status" data-output-model="RptByUsr.Status" data-button-label="icon SNAME"
                                                data-item-label="icon SNAME maker" data-on-item-click=""
                                                data-tick-property="ticked" data-max-labels="1" selection-mode="multiple">
                                            </div>
                                            <input type="text" data-ng-model="RptByUsr.Status[0]" name="SNAME" style="display: none" />
                                            <span class="error" data-ng-show="frmHDMISFCRptByUserDetails.$submitted && frmHDMISFCRptByUserDetails.SNAME.$invalid" style="color: red"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                        <div class="form-group">
                                            <div class="box-footer text-left">
                                                <label class="control-label">Company</label>
                                                <div isteven-multi-select data-input-model="Company" data-output-model="RptByUsr.CNP_NAME" button-label="icon CNP_NAME"
                                                    item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                                </div>
                                            </div>
                                            <input type="text" data-ng-model="RptByUsr.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmHDMISFCRptByUserDetails.$submitted && frmHDMISFCRptByUserDetails.Company.$invalid" style="color: red">Please Select Company </span>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="box-footer text-left" style="margin-top: 15px">
                                                <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color">Search</button>
                                                <button id="btndnload" class="btn btn-primary custom-button-color" data-ng-click="btndnload()">Download Images</button>
                                            </div>
                                        </div>
                                    </div>

                                    <%--<div class="col-sm-12">--%>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="margin-top: 15px">
                                        <label>View In : </label>
                                        <input id="viewswitch" type="checkbox" checked data-size="small"
                                            data-on-text="<span class='fa fa-table'></span>"
                                            data-off-text="<span class='fa fa-bar-chart'></span>" />
                                    </div>
                                    <div data-ng-show="RptByUsrGrid">
                                        <button type="button" class="btn btn-success" data-toggle="collapse" title="Click here to view column definitions" data-target="#demo" style="margin-top: 15px">
                                            <span class="glyphicon glyphicon-expand"></span>
                                        </button>
                                    </div>
                                    <%-- </div>--%>
                                </div>
                                <br />
                                <div id="demo" class="collapse">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <span class="glyphicon glyphicon-time"></span><b>Defined TAT :</b> Sum of SLA times defined for all the status in SLA master for HD Incharge.<br />
                                                    <span class="glyphicon glyphicon-repeat"></span><b>TAT: </b>Time difference between request created and till date(If closed, closed date).<br />
                                                    <span class="glyphicon glyphicon-ok"></span><b>SLA: </b>SLA time defined for current status.<br />
                                                    <span class="glyphicon glyphicon-time"></span><b>TAT_SLA:</b> Total time in the current status.<br />
                                                    <span class="glyphicon glyphicon-minus-sign"></span><b>Delayed Time:</b> (TAT) - (Defined TAT)
                                                    <br />
                                                    <span class="glyphicon glyphicon-minus-sign"></span><b>Delayed SLA:</b>  (TAT_SLA) - SLA<br />
                                                    <span class="glyphicon glyphicon-check"></span><b>Response Time: </b>Time difference between request created and its frist response.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="Tabular" data-ng-show="RptByUsrGrid">
                                <div class="col-sm-12">
                                </div>
                                <div class="row" style="padding-right: 15px;">
                                    <a data-ng-click="GenReport(RptByUsr,'doc')"><i id="word" data-ng-show="DocTypeVisible==0" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(RptByUsr,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(RptByUsr,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                                <div class="row" style="padding-left: 17px;">
                                    <div class="col-md-12" style="height: 320px">
                                        <input id="UpdteFilter" placeholder="Filter..." type="text" style="width: 25%;" />

                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 305px; width: auto"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div id="Graphicaldiv">
                                    <div id="LocationGraph">&nbsp</div>
                                    <div id="SubCatGraph">&nbsp</div>
                                </div>
                            </div>
                        </form>
                        <div class="modal fade bs-example-modal-lg col-md-12 " id="historymodal">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="panel-group box box-primary" id="Div2">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <div class="panel-heading ">
                                                <h4 class="panel-title modal-header-primary" style="padding-left: 17px" data-target="#collapseTwo">History Details</h4>
                                                <br />
                                                <form role="form" name="form2" id="form3">
                                                    <div class="clearfix">
                                                        <div class="col-md-12">
                                                            <div class="box">
                                                                <div class="box-danger table-responsive">
                                                                    <div data-ag-grid="PopOptions" class="ag-blue" style="height: 305px; width: auto"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>


    <script src="../../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js" defer></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/highlight.js" defer></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/main.js" defer></script>

    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/moment.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../../../SMViews/Utility.js" defer></script>
    <script src="../../js/HDMUtility.js" defer></script>
    <script src="../Js/HDMISFCReportByUser.js" defer></script>


    <script type="text/javascript" defer>
        var CompanySession = '<%= Session["COMPANYID"]%>';
        $(document).ready(function () {
            setDateVals();
        });

        function setDateVals() {
            $('#FROM_DATE').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#TO_DATE').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FROM_DATE').datepicker('setDate', new Date(moment().subtract(0, 'month').startOf('month').format('DD-MMM-YYYY')));
            $('#TO_DATE').datepicker('setDate', new Date(moment().subtract(0, 'month').endOf('month').format('DD-MMM-YYYY')));
        }
    </script>
</body>
</html>

