﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.css" rel="stylesheet" />
    <link href="../../../../BootStrapCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <%-- <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
    <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>--%>
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
            });
        };
    </script>
    <style>
        #pageRowSummaryPanel {
            display: none;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMConsolidatedReportController" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Consolidated Report" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">Consolidated Report</h3>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <form role="form" id="HDMConsReport" name="frmHDMConsReport">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">Select Range<span style="color: red;">*</span></label>
                                        <br />
                                        <select id="ddlRange" class="selectpicker" ng-model="ConsReport.selVal" data-ng-change="rptDateRanges()">
                                            <option value="">Select Range</option>
                                            <option value="TODAY">Today</option>
                                            <option value="YESTERDAY">Yesterday</option>
                                            <option value="7">Last 7 Days</option>
                                            <option value="30">Last 30 Days</option>
                                            <option value="THISMONTH">This Month</option>
                                            <option value="LASTMONTH">Last Month</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">From Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" id='fromdate'>
                                            <input type="text" class="form-control" data-ng-model="ConsReport.FromDate" data-output-model="ConsReport.FDate" id="FROM_DATE" name="FROM_DATE" required="" placeholder="mm/dd/yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">To Date <span style="color: red;">*</span></label>
                                        <div class="input-group date" id='todate'>
                                            <input type="text" class="form-control" data-ng-model="ConsReport.ToDate" data-output-model="ConsReport.TDate" id="TO_DATE" name="TO_DATE" required="" placeholder="mm/dd/yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                    <div class="form-group">
                                        <label class="control-label">Company</label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="ConsReport.CNP_NAME" button-label="icon CNP_NAME"
                                            item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="ConsReport.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmHDMConsReport.$submitted && frmHDMConsReport.Company.$invalid" style="color: red">Please Select Company </span>
                                    </div>
                                </div>


                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="clearfix">
                                        <div class="box-footer text-right">
                                            <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color" data-ng-click="GetConsReport()">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="padding-left: 18px">
                                <div class="col-md-6">
                                </div>

                                <div class="col-md-6" id="table2">
                                    <br />
                                    <a data-ng-click="GenReport(ConsReport,'doc')"><i id="word" data-ng-show="DocTypeVisible==0" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(ConsReport,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(ConsReport,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                            </div>
                            <div id="Tabular" data-ng-show="ConsReportGrid">
                                <div class="row" style="padding-right: 15px;">

                                    <div class="row" style="padding-left: 17px;">
                                        <div class="col-md-12" style="height: 320px">
                                            <div class="input-group" style="width: 20%">
                                                <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-primary custom-button-color" type="submit" data-ng-click="GetConsReport()">
                                                        <i class="glyphicon glyphicon-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 305px; width: auto"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>



                    </div>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js"></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>

    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../../Scripts/moment.min.js"></script>
    <script src="../../../../Scripts/jspdf.min.js"></script>
    <script src="../../../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../js/HDMUtility.min.js"></script>
    <script src="../../../../SMViews/Utility.min.js"></script>
    <%--<script src="../Js/HDMConsolidatedReport.min.js"></script>--%>
    <script src="../Js/HDMConsolidatedReport.js"></script>
    <script type="text/javascript">
        var CompanySession = '<%= Session["COMPANYID"]%>';
        //$(document).ready(function () {
        //    setDateVals();
        //});

        //function setDateVals() {
        //    $('#FROM_DATE').datepicker({
        //        format: 'dd-M-yyyy',
        //        autoclose: true,
        //        todayHighlight: true
        //    });
        //    $('#TO_DATE').datepicker({
        //        format: 'dd-M-yyyy',
        //        autoclose: true,
        //        todayHighlight: true
        //    });
        //    $('#FROM_DATE').datepicker('setDate', new Date(moment().subtract(29, 'days').startOf('month').format('DD-MMM-YYYY')));
        //    $('#TO_DATE').datepicker('setDate', new Date(moment().format('DD-MMM-YYYY')));
        //}
    </script>
</body>
</html>
