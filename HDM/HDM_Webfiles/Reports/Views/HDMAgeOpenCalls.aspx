﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    

    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />

    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMAgeOpenCallsController" onload="setDateVals()" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Age of Open Calls Report" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">Age of Open Calls Report</h3>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <form id="form1" name="frmopencalls" data-valid-submit="LoadData()">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">From Date</label>
                                        <div class="input-group date" id='fromdate'>
                                            <input type="text" class="form-control" data-ng-model="OpenCallsRpt.FromDate" id="FROM_DATE" name="FROM_DATE" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmopencalls.$submitted && frmopencalls.FromDate.$invalid" style="color: red;">Please Select From Date</span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">To Date</label>
                                        <div class="input-group date" id='todate'>
                                            <input type="text" class="form-control" data-ng-model="OpenCallsRpt.ToDate" id="TO_DATE" name="TO_DATE" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                            </span>
                                        </div>
                                        <span class="error" data-ng-show="frmopencalls.$submitted && frmopencalls.ToDate.$invalid" style="color: red;">Please Select To Date</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                    <div class="form-group">
                                        <label class="control-label">Company</label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="OpenCallsRpt.CNP_NAME" button-label="icon CNP_NAME"
                                            item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="3" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="OpenCallsRpt.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmopencalls.$submitted && frmopencalls.Company.$invalid" style="color: red">Please Select Company </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="clearfix">
                                        <div class="box-footer text-right">
                                            <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row" style="padding-left: 18px">
                                <div class="col-md-6">
                                    <label>View In : </label>
                                    <input id="viewswitch" type="checkbox" checked data-size="small"
                                        data-on-text="<span class='fa fa-table'></span>"
                                        data-off-text="<span class='fa fa-bar-chart'></span>" />
                                </div>

                                <div class="col-md-6" id="table2">
                                    <br />
                                    <a data-ng-click="GenReport(OpenCallsRpt,'doc')"><i id="word" data-toggle="tooltip" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(OpenCallsRpt,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport(OpenCallsRpt,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                            </div>

                        </form>

                        <form id="form2" data-ng-show="GridVisiblity">
                            <div id="Tabular">
                                <div class="row" style="padding-left: 17px;">
                                    <div class="col-md-12" style="height: 320px">
                                        <input id="UpdteFilter" placeholder="Filter..." type="text" style="width: 25%;" />
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 305px; width: auto"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div id="Graphicaldiv" data-ng-show="GridVisiblity">
                                    <div id="OccupGraph">&nbsp</div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js" defer></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/highlight.js" defer></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/main.js" defer></script>

    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/moment.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../Js/HDMAgeOpenCalls.min.js" defer></script>
    <script src="../../../../SMViews/Utility.min.js" defer></script>
    <script type="text/javascript" defer>
        var CompanySession = '<%= Session["COMPANYID"]%>';
        $(document).ready(function () {
            setDateVals();
        });
        function setDateVals() {
            $('#FROM_DATE').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#TO_DATE').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FROM_DATE').datepicker('setDate', new Date(moment().subtract(29, 'days').format('DD-MMM-YYYY')));
            $('#TO_DATE').datepicker('setDate', new Date(moment().format('DD-MMM-YYYY')));
        }
    </script>
</body>

</html>
