﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />   
    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMReqStatusconsolidatedReportController" class="amantra">
    <div class="animsition" ng-cloak>

        <div class="container-fluid page-content-inner">
            <div ba-panel ba-panel-title="Age of Open Calls Report" ba-panel-class="with-scroll">
                <div class="bgc-gray p-20 m-b-25">
                    <div class="panel-heading-qfms">
                        <h3 class="panel-title panel-heading-qfms-title">Cons. Request Status Report</h3>
                    </div>
                    <div class="panel-body" style="padding-right: 10px;">
                        <form id="form1" name="frmCons" data-valid-submit="BindGrid()">
                            <div class="clearfix">
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <label>View In : </label>
                                    <input id="viewswitch" type="checkbox" checked data-size="small"
                                        data-on-text="<span class='fa fa-table'></span>"
                                        data-off-text="<span class='fa fa-bar-chart'></span>" />
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                    <div class="form-group">
                                        <label class="control-label">Company</label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="ConsRpt.CNP_NAME" button-label="icon CNP_NAME"
                                            item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="ConsRpt.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmCons.$submitted && frmCons.Company.$invalid" style="color: red">Please Select Company </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="txtcode">History Status<span style="color: red;"></span></label>
                                        <br />
                                        <select id="ddlstatus" class="selectpicker" ng-model="ConsRpt.selstatus" data-ng-change="STATUSCHANGE()">
                                            <option value="1">Active</option>
                                            <option value="2">In-Active</option>
                                            <option value="3">All</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="clearfix">
                                        <div class="box-footer text-right">
                                            <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="Search()" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6 col-xs-12 text-right" id="table2" data-ng-show="GridVisiblity">
                                    <br />
                                    <a data-ng-click="GenReport('doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport('xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReport('pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12 text-right" id="table3" data-ng-show="GridVisiblityZone">
                                    <br />
                                    <a data-ng-click="GenReportZone('doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReportZone('xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <a data-ng-click="GenReportZone('pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                </div>
                            </div>
                        </form>
                        <br />
                        <form id="form2">

                            <div id="Tabular">
                                <div class="col-md-12">
                                    <div class="panel-heading ">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tabSummary" data-ng-click="BindGrid()" data-toggle="tab">Summary</a></li>
                                            <li><a href="#tabZone" data-ng-click="BindGrid_Zone()" data-toggle="tab">Zone Wise</a></li>
                                        </ul>
                                    </div>


                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="tabSummary">
                                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                            </div>
                                            <div class="tab-pane fade" id="tabZone">
                                                <input type="text" class="form-control" id="filteredzone" placeholder="Filter by any..." style="width: 25%" />
                                                <div data-ag-grid="gridzone" class="ag-blue" style="height: 310px; width: auto"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div id="Graphicaldiv">
                                <div id="HDMconsolidatedGraph">&nbsp</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js" defer></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/highlight.js" defer></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/main.js" defer></script>


    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>

    <script defer>
        //agGrid.initialiseAgGridWithAngular1(angular);
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../Js/HDMReqStatusconsolidatedReport.js" defer></script>
    <script src="../../../../SMViews/Utility.js" defer></script>
    <script src="../../../../Scripts/moment.min.js" defer></script>
</body>

<script type="text/javascript" defer>
    var CompanySession = '<%= Session["COMPANYID"]%>';
</script>




</html>
