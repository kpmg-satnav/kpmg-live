﻿app.service("HDMFeedBackReportService", function ($http, $q, UtilityService) {
    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMFeedBackReport/GetGriddata')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.GetGriddataBySearch = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMFeedBackReport/GetGriddataBySearch', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});


app.controller('HDMFeedBackReportController', function ($scope, $q, $http, HDMFeedBackReportService, HDMViewFeedBackService, UtilityService, HDMFeedBackService, $timeout, $filter, $window) {
    $scope.FeedBackReport = {};
    $scope.Request_Type = [];
    $scope.Columns = [];
    $scope.Cols = [];
    $scope.CompanyVisible = 0;
    $scope.columnDefs = [];

    var RequestId;
    $scope.ReqId = true;




    // fill company ddls

    var ROLID; var b;
    $scope.PageLoad = function () {

        UtilityService.GetLocationsall(1).then(function (response) {
            if (response.data != null) {
                $scope.Clinic = response.data;
                $scope.FeedBackReport.Clinic = $scope.Clinic;
            }
        });

        HDMFeedBackService.GetMainCat().then(function (response) {
            if (response.data != null) {
                $scope.Main_Category = response.data;
            }
        });
        HDMFeedBackService.Getsubcat().then(function (response) {
            if (response.data != null) {
                $scope.Sub_Category = response.data;
            }
        });
        HDMFeedBackService.GetStatusList().then(function (response) {
            if (response.data != null) {
                $scope.Status = response.data;
            }
        });

    }
    $scope.PageLoad();


    // }

    var columnDefs = [
        { headerName: "Requisition Id", field: "REQUEST_ID", width: 130, cellClass: 'grid-align', },
        { headerName: "Clinic", field: "LOCATION", width: 140, cellClass: 'grid-align', },
        { headerName: "Client Name", field: "CLNT_NAME", width: 150, cellClass: 'grid-align', },
        { headerName: "Contact", field: "CONTACT_NO", width: 100, cellClass: 'grid-align', },
        { headerName: "Main Category", field: "MAIN_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Sub Category", field: "SUB_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Clinic Manager", field: "CLNC_MGR", width: 150, cellClass: 'grid-align', },
        { headerName: "AOM", field: "AOM", width: 130, cellClass: 'grid-align', },
        { headerName: "OTHERS", field: "OTHERS", width: 130, cellClass: 'grid-align', },
        { headerName: "Complaint Received Date", field: "CMPLT_REV_DATE", width: 150, cellClass: 'grid-align', },
        { headerName: "Main Status", field: "MAIN_STATUS", width: 150, cellClass: 'grid-align', },
        { headerName: "Mode Name", field: "MODE_NAME", width: 130, cellClass: 'grid-align', },
        { headerName: "Mode Comments", field: "MODE_CMTS", width: 130, cellClass: 'grid-align', },
        { headerName: "Sub Status", field: "SUB_STA_NAME", width: 100, cellClass: 'grid-align', },
        { headerName: "Status Comments", field: "SUB_STA_CMTS", width: 100, cellClass: 'grid-align', },
        { headerName: "Requested By", field: "REQUESTED_BY", width: 150, cellClass: 'grid-align', },
        //{ headerName: "Labour Cost", field: "LABOUR_COST", width: 100, cellClass: 'grid-align', },
        //{ headerName: "Spare Cost", field: "SPARE_COST", width: 100, cellClass: 'grid-align', },
        //{ headerName: "Additional Cost", field: "ADDITIONAL_COST", width: 120, cellClass: 'grid-align', },
        //{ headerName: "Closed Time", field: "CLOSED_TIME", width: 130, cellClass: 'grid-align', },
        //{ headerName: "Defined TAT", field: "DEFINED_TAT", width: 100, cellClass: 'grid-align', },
        //{ headerName: "Response TAT", field: "RESPONSE_TAT", width: 140, cellClass: 'grid-align', },
        //{ headerName: "Delayed TAT", field: "DELAYED_TAT", width: 140, cellClass: 'grid-align', },
    ];


    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        //showToolPanel: true,
        //groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Main Category", field: "Main_Category",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    HDMFeedBackReportService.GetGriddata().then(function (response) {
      
        $scope.gridata = response.data;
        if (response.data == null) {
            $scope.gridOptions.api.setRowData([]);
            progress(0, 'Loading...', false);
        }
        else {
            progress(0, 'Loading...', true);
            $scope.GridVisiblity = true;
            $scope.gridOptions.api.setRowData(response.data);
            progress(0, 'Loading...', false);
        }
    });


    $scope.FeedBackReport.selstatus = "1";
    $scope.STATUSCHANGE = function () {
        switch ($scope.FeedBackReport.selstatus) {
            case '1':
                $scope.FeedBackReport.selstatus = "1";
                break;
            case '2':
                $scope.FeedBackReport.selstatus = "2";
                break;
            case '3':
                $scope.FeedBackReport.selstatus = "3";
                break
        }
    }
    $scope.FeedBackReport.selVal = "30";
    $scope.FeedBackReport.FromDate = moment().subtract(29, 'days').startOf('month').format('DD-MMM-YYYY');
    $scope.FeedBackReport.ToDate = moment().format('DD-MMM-YYYY');
    //$scope.FeedBackReport.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
    //$scope.FeedBackReport.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
    $scope.rptDateRanges = function () {
        switch ($scope.FeedBackReport.selVal) {
            case 'TODAY':
                $scope.FeedBackReport.FromDate = moment().format('DD-MMM-YYYY');
                $scope.FeedBackReport.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'YESTERDAY':
                $scope.FeedBackReport.FromDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                $scope.FeedBackReport.ToDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                break
            case '7':
                $scope.FeedBackReport.FromDate = moment().subtract(6, 'days').format('DD-MMM-YYYY');
                $scope.FeedBackReport.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case '30':
                $scope.FeedBackReport.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
                $scope.FeedBackReport.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'THISMONTH':
                $scope.FeedBackReport.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
                $scope.FeedBackReport.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
                break;
            case 'LASTMONTH':
                $scope.FeedBackReport.FromDate = moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY');
                $scope.FeedBackReport.ToDate = moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY');
                break;
        }
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    var clk = true;
    var ExportColumns;
    $scope.LoadData = function () {
        var unticked = _.filter($scope.Cols, function (item) {
            return item.ticked == false;
        });
        var ticked = _.filter($scope.Cols, function (item) {
            return item.ticked == true;
        });

        if ($scope.FeedBackReport.FromDate == undefined && $scope.FeedBackReport.ToDate == undefined) {
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var firstDayWithSlashes = ((firstDay.getMonth() + 1)) + '/' + firstDay.getDate() + '/' + firstDay.getFullYear();
            var lastDayWithSlashes = ((lastDay.getMonth() + 1)) + '/' + lastDay.getDate() + '/' + lastDay.getFullYear();

            $scope.FeedBackReport.FromDate = firstDayWithSlashes;
            $scope.FeedBackReport.ToDate = lastDayWithSlashes;
        }
        //var cp = _.find($scope.Company, { ticked: true })
        var params = {
            RequestID: $scope.FeedBackReport.RequestID,
            LCMlst: $scope.FeedBackReport.Clinic,
            MAINlst: $scope.FeedBackReport.Main_Category,
            SUBlst: $scope.FeedBackReport.Sub_Category,
            STATUSlst: $scope.FeedBackReport.Status,
            HSTSTATUS: $scope.FeedBackReport.selstatus,
            //Request_Type: $scope.HDMViewSubRequisitionsForm.Request_Type,
            FromDate: $scope.FeedBackReport.FromDate,
            ToDate: $scope.FeedBackReport.ToDate,
            //CompanyId: cp.CNP_ID
        };

        HDMFeedBackReportService.GetGriddataBySearch(params).then(function (response) {
            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData(response.data);
                progress(0, 'Loading...', false);
            }
        });

    }, function (error) {
        console.log(error);
    }

    $scope.CustmPageLoad = function () {
    }, function (error) {
        console.log(error);
    }

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "REQUEST ID", key: "REQUEST_ID" }, { title: "CLINIC", key: "LOCATION" }, { title: "CLIENT NAME", key: "CLNT_NAME" }, { title: "CONTACT", key: "CONTACT_NO" }, { title: "MAIN CATEGORY", key: "MAIN_CATEGORY" }, { title: "SUB CATEGORY", key: "SUB_CATEGORY" }, { title: "CLINIC MANAGER", key: "CLNC_MGR" }, { title: "AOM", key: "AOM" }, { title: "COMPLIANT RECEIVED DATE", key: "CMPLT_REV_DATE" }, { title: "MAIN STATUS", key: "MAIN_STATUS" }, { title: "MODE NAME", key: "MODE_NAME" }, { title: "MODE COMMENTS", key: "MODE_CMTS" }, { title: "SUB STATUS", key: "SUB_STA_NAME" }, { title: "SUB STATUS COMMENTS", key: "SUB_STA_CMTS" }, { title: "REQUESTED BY", key: "REQUESTED_BY" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("FeedBackReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "FeedBackReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Customized, Type) {
        progress(0, 'Loading...', true);
        Customized.Type = Type;

        //Customized.loclst = $scope.LeaseRep.Locations;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Customized.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/HDMFeedBackReport/GetGrid',
                method: 'POST',
                data: Customized,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'FeedBackReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }

    $scope.getsubbymain = function () {
        UtilityService.getsubbymain($scope.FeedBackReport.Main_Category).then(function (response) {
            $scope.Sub_Category = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.getmainbysub = function () {
        HDMFeedBackService.getmainbysub($scope.FeedBackReport.Sub_Category[0].SUBC_CODE).then(function (response) {
            angular.forEach($scope.Main_Category, function (Main_Category) {
                Main_Category.ticked = false;
            });
            var MainLst = response.data;
            angular.forEach(MainLst, function (value, key) {
                var main = _.find($scope.Main_Category, { MNC_CODE: value.MNC_CODE });
                if (main != undefined && main.ticked == false) {
                    main.ticked = true;
                }
            });
        }, function (error) {
            console.log(error);
        });
    }


});
