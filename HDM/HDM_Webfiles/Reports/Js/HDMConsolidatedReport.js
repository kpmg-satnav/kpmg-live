﻿app.service("HDMConsolidatedReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetConsReport = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMConsolidatedReport/GetHDMConsolidateDetails', searchObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SearchAllData = function (SearchSpaces) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMConsolidatedReport/SearchAllData', SearchSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);


app.controller('HDMConsolidatedReportController', ['$scope', '$q', 'UtilityService', 'HDMConsolidatedReportService', '$timeout', '$http', function ($scope, $q, UtilityService, HDMConsolidatedReportService, $timeout, $http) {
    $scope.ConsReport = {};
    $scope.ConsReportGrid = true;
    $scope.ActionStatus = 0;
    $scope.DocTypeVisible = 0;
    $scope.CompanyVisible = 0;
    $scope.Company = [];

    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data !== null) {
                console.log(CompanySession);
                $scope.Company = response.data;
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.ConsReport.CNP_NAME.push(a);
                });

                if (CompanySession === "1") { $scope.CompanyVisible = 0; }
                else { $scope.CompanyVisible = 1; }
            }
            $timeout(getLastMonthData, 200);
        });
    }, 500);



    //$scope.GetConsReport = function () {
    //    $scope.rptDateRanges();
    //    $scope.HDMConsReport = {
    //        FromDate: $scope.ConsReport.FromDate,
    //        ToDate: $scope.ConsReport.ToDate,
    //        CompanyId: $scope.ConsReport.CNP_NAME[0].CNP_ID,

    //    };
    //    var fromdate = moment($scope.ConsReport.FromDate);
    //    var todate = moment($scope.ConsReport.ToDate);
    //    if (fromdate > todate) {
    //        $scope.GridVisiblity = false;
    //        showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
    //    }
    //    else {
    //        progress(0, 'Loading...', true);
    //        HDMConsolidatedReportService.GetConsReport($scope.HDMConsReport).then(function (response) {
    //            $scope.ConsReportGrid = true;
    //            $scope.gridata = response;
    //            if (response.data == null) {
    //                $scope.gridOptions.api.setRowData([]);
    //                progress(0, 'Loading...', false);
    //            }
    //            else {
    //                progress(0, 'Loading...', true);
    //                $scope.gridOptions.api.setRowData(response.data);
    //                progress(0, 'Loading...', false);
    //            }

    //        }, function (error) {
    //            console.log(error);
    //        });
    //    };
    //}

    $scope.GetConsReport = function () {
        //$("#btLast").hide();
        //$("#btFirst").hide();
        ////$scope.rptDateRanges();
        //var dataSource = {
        //    rowCount: null,
        //    getRows: function (params) {
        //        var searchval = $("#filtertxt").val();
        //        console.log(searchval);
        //        setTimeout(function () {
        //            var lastRow = -1;
        //            var dataObj = {
        //                SearchValue: searchval,
        //                FromDate: $('#FROM_DATE').val(),
        //                ToDate: $('#TO_DATE').val(),
        //                CompanyId: $scope.ConsReport.CNP_NAME[0].CNP_ID,
        //                PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
        //                PageSize: 10,
        //                Type: $scope.flag
        //            };

        //            var fromdate = moment($scope.ConsReport.FromDate);
        //            var todate = moment($scope.ConsReport.ToDate);
        //            if (fromdate > todate) {
        //                $scope.GridVisiblity = false;
        //                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        //            }
        //            else {
        //                if (searchval == "" || searchval == undefined) {
        //                    HDMConsolidatedReportService.GetConsReport(dataObj).then(function (response) {
        //                        console.log(response );
        //                        console.table(response.data);
        //                        if (response.data != null) {
        //                            $scope.ConsReportGrid = true;
        //                            $scope.gridata = response;
        //                            params.successCallback(response.data, lastRow);
        //                        }
        //                        else {
        //                            params.failCallback();
        //                            $("#btNext").attr("disabled", true);
        //                           // $scope.ConsReportGrid = false;
        //                        }
        //                        //function bindLocChart() {
        //                        //    $scope.LocWiseBarChart($scope.dataObj);
        //                        //}
        //                        //function bindSubCatGraph() {
        //                        //    $scope.subCatGraph($scope.dataObj);
        //                        //}
        //                        //$timeout(bindLocChart, 1000);
        //                        //$timeout(bindSubCatGraph, 1000);
        //                        $scope.gridOptions.api.setRowData(response.data);


        //                    });
        //                }

        //                else {
        //                    HDMConsolidatedReportService.SearchAllData(dataObj).then(function (response) {
        //                        if (response != null) {
        //                            $scope.ConsReportGrid = true;
        //                            params.successCallback(response, lastRow);
        //                        }
        //                        else {
        //                            params.failCallback();
        //                            $("#btNext").attr("disabled", true);
        //                            $scope.ConsReportGrid = false;
        //                            params.successCallback('', lastRow);
        //                        }
        //                        $scope.gridOptions.api.setRowData(response);
        //                    });
        //                }
        //            }
        //        }, 1);

        //    }
        //}
        //console.log(dataSource);
        //$scope.gridOptions.api.setDatasource(dataSource);
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        var dataObj = {
            SearchValue: searchval,
            FromDate: $scope.ConsReport.FromDate,//('#FROM_DATE').val(),
            ToDate: $scope.ConsReport.ToDate,//$('#TO_DATE').val(),
            CompanyId: $scope.ConsReport.CNP_NAME[0].CNP_ID,
            PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
            PageSize: 10,
            Type: $scope.flag
        };

        var fromdate = moment($scope.ConsReport.FromDate);
        var todate = moment($scope.ConsReport.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else
        {
            console.log(dataObj);
            HDMConsolidatedReportService.GetConsReport(dataObj).then(function (response) {
                console.log(response);
                if (response.data !== null) {
                    $scope.gridOptions.api.setRowData(response.data);
                    progress(0, 'Loading...', false);
                    //$scope.ConsReportGrid = true;
                    //params.successCallback(response, lastRow);
                }
                else {
                    $scope.gridOptions.api.setRowData([]);
                    progress(0, 'Loading...', false);
                    //params.failCallback();
                    //$("#btNext").attr("disabled", true);
                    //$scope.ConsReportGrid = false;
                    //params.successCallback('', lastRow);
                }
                
            });
        }

    };

    var columnDefs = [
        { headerName: "Requisition Id", field: "SER_REQ_ID", width: 130, cellClass: 'grid-align', sno: 1 },
        { headerName: "Main Category", field: "MNC_NAME", width: 130, cellClass: 'grid-align', sno: 2 },
        { headerName: "Sub Category", field: "SUBC_NAME", width: 140, cellClass: 'grid-align', sno: 3 },
        { headerName: "Child Category", field: "CHC_TYPE_NAME", width: 150, cellClass: 'grid-align', sno: 4 },
        { headerName: "Location", field: "LCM_NAME", width: 120, cellClass: 'grid-align', sno: 5 },
        { headerName: "Problem Description", field: "SER_PROB_DESC", width: 150, cellClass: 'grid-align', sno: 6 },
        { headerName: "Requested By ID", field: "REQUESTED_BY_ID", width: 150, cellClass: 'grid-align', sno: 7 },
        { headerName: "Requested By Name", field: "REQUESTED_BY_NAME", width: 150, cellClass: 'grid-align', sno: 8 },
        { headerName: "Requested Date", field: "SER_CREATED_DT", template: '<span>{{ data.SER_CREATED_DT | date: "dd-MM-yyyy - HH:mm:ss"}}</span >', width: 130, cellClass: 'grid-align', sno: 9 },
        { headerName: "Updated Date", field: "SER_UPDATED_DT", template: '<span>{{data.SER_UPDATED_DT | date:"dd-MM-yyyy - HH:mm:ss"}}</span>', width: 130, cellClass: 'grid-align', sno: 10 },
        //{ headerName: "Assigned Date", field: "ASSIGNED_DATE", template: '<span>{{data.ASSIGNED_DATE | date:"dd-MM-yyyy - HH:mm:ss"}}</span>', width: 130, cellClass: 'grid-align', },
        { headerName: "Assigned To ID", field: "SERH_ASSIGN_TO", width: 150, cellClass: 'grid-align', sno: 11 },
        { headerName: "Assigned To Name", field: "ASSIGN_TO_NAME", width: 150, cellClass: 'grid-align', sno: 12 },
        { headerName: "Contact No", field: "CONTACT_NO", width: 150, cellClass: 'grid-align', sno: 13 },
        { headerName: "Status", field: "STA_TITLE", width: 100, cellClass: 'grid-align', sno: 14 },
        //{ headerName: "FeedBack", field: "FEEBACK", width: 100, cellClass: 'grid-align', sno: 15 },
        { headerName: "Remarks", field: "COMMENTS", width: 100, cellClass: 'grid-align', sno: 16 },
         { headerName: "Claim Amount", field: "ser_claim_amt", width: 100, cellClass: 'grid-align' },
        { headerName: "Labour Cost", field: "LABOUR_COST", width: 100, cellClass: 'grid-align', sno: 17 },
        { headerName: "Spare Cost", field: "SPARE_COST", width: 100, cellClass: 'grid-align', sno: 18 },
        { headerName: "Additional/Approved Cost", field: "ADDITIONAL_COST", width: 120, cellClass: 'grid-align', sno: 19 },
        { headerName: "Closed Time", field: "CLOSED_TIME", template: '<span>{{data.CLOSED_TIME | date:"dd-MM-yyyy - HH:mm:ss"}}</span>', width: 130, cellClass: 'grid-align', sno: 20 },
        { headerName: "Defined TAT", field: "DEFINED_TAT", width: 100, cellClass: 'grid-align', sno: 21 },
        { headerName: "Response TAT", field: "RESPONSE_TAT", width: 140, cellClass: 'grid-align', sno: 22 },
        { headerName: "Delayed TAT", field: "DELAYED_TAT", width: 140, cellClass: 'grid-align', sno: 23 },
        { headerName: "TAT", field: "TOTAL_TIME", width: 140, cellClass: 'grid-align', sno: 24 },
        { headerName: "HELPDESK", field: "HELPDESK", width: 130, cellClass: 'grid-align', },
        { headerName: "L1Manager", field: "L1Manager", width: 100, cellClass: 'grid-align', },
        { headerName: "L2Manager", field: "L2Manager", width: 140, cellClass: 'grid-align', },
        { headerName: "L3Manager", field: "L3Manager", width: 140, cellClass: 'grid-align', },
        { headerName: "Helpdesk Incharge ESC DT", field: "Helpdesk_Incharge", width: 130, cellClass: 'grid-align', },
        { headerName: "L1 Manager ESC DT", field: "L1_Manager", template: '<span>{{data.L1_Manager | date:"dd-MM-yyyy - HH:mm:ss"}}</span>', width: 140, cellClass: 'grid-align', },
        { headerName: "L2 Manager ESC DT", field: "L2_Manager", template: '<span>{{data.L2_Manager | date:"dd-MM-yyyy - HH:mm:ss"}}</span>', width: 140, cellClass: 'grid-align', },
        { headerName: "L3 Manager ESC DT", field: "L3_Manager", template: '<span>{{data.L3_Manager | date:"dd-MM-yyyy - HH:mm:ss"}}</span>', width: 140, cellClass: 'grid-align', },
        { headerName: "ZONE", field: "ZONE", width: 100, cellClass: 'grid-align', },
        { headerName: "FeedBack Deatils", field: "FEEBACK", width: 100, cellClass: 'grid-align', },
    ];

    $scope.pageSize = '10';

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },
        floatingFilter: false,
        debug: true,
        rowModelType: 'infinite',
        pagination: true,
        paginationPageSize: 10,
        cacheOverflowSize: 2,
        maxConcurrentDatasourceRequests: 2,
        infiniteInitialRowCount: 1,
        maxBlocksInCache: 2
    };

    function onUpdateFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
        } else {
            $scope.DocTypeVisible = 0;
        }
    }

    $("#UpdteFilter").change(function () {
        onUpdateFilterChanged($(this).val());
    }).keydown(function () {
        onUpdateFilterChanged($(this).val());
    }).keyup(function () {
        onUpdateFilterChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFilterChanged($(this).val());
    })

    $scope.ConsReport.selVal = "30";

    $scope.ConsReport.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
    $scope.ConsReport.ToDate = moment().format('DD-MMM-YYYY');
    $scope.rptDateRanges = function () {
        switch ($scope.ConsReport.selVal) {
            case 'TODAY':
                $scope.ConsReport.FromDate = moment().format('DD-MMM-YYYY');
                $scope.ConsReport.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'YESTERDAY':
                $scope.ConsReport.FromDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                $scope.ConsReport.ToDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                break;
            case '7':
                $scope.ConsReport.FromDate = moment().subtract(6, 'days').format('DD-MMM-YYYY');
                $scope.ConsReport.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case '30':
                $scope.ConsReport.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
                $scope.ConsReport.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'THISMONTH':
                $scope.ConsReport.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
                $scope.ConsReport.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
                break;
            case 'LASTMONTH':
                $scope.ConsReport.FromDate = moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY');
                $scope.ConsReport.ToDate = moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY');
                break;
        }
    }

    function getLastMonthData() {
        $scope.GetConsReport();
    }

   

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [
            { title: "Requisition Id", key: "SER_REQ_ID" }, { title: "Main Category", key: "MNC_NAME" }, { title: "Sub Category", key: "SUBC_NAME" },
            { title: "Child Category", key: "CHC_TYPE_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Prbolem Description", key: "SER_PROB_DESC" },
            { title: "Requested By ID", key: "REQUESTED_BY_ID" }, { title: "Requested By Name", key: "REQUESTED_BY_NAME" }, { title: "Requested Date", key: "SER_CREATED_DT" },
            { title: "Status", key: "STA_TITLE" }, { title: "Closed Date", key: "CLOSED_TIME" }, { title: "Updated Date", key: "SER_UPDATED_DT" },
            { title: "Assingned Date", key: "ASSIGNED_DATE" }, { title: "Contact No", key: "CONTACT_NO" }, { title: "Assigned To ID", key: "SERH_ASSIGN_TO" }, { title: "Assigned To Name", key: "ASSIGN_TO_NAME" }, { title: "Closed Time", key: "CLOSED_TIME" },
            { title: "Defined TAT", key: "DEFINED_TAT" }, { title: "Response TAT(in Min)", key: "RESPONSE_TAT" }, { title: "Delayed TAT(in Min)", key: "DELAYED_TAT" }, { title: "TAT", key: "TOTAL_TIME" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("ConsolidatedReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    //$scope.GenerateFilterExcel = function () {
    //    progress(0, 'Loading...', true);
    //    var Filterparams = {
    //        skipHeader: false,
    //        skipFooters: false,
    //        skipGroups: false,
    //        allColumns: false,
    //        onlySelected: false,
    //        columnSeparator: ",",
    //        fileName: "ReportByCategory.csv"
    //    }; $scope.gridOptions.api.exportDataAsCsv(Filterparams);
    //    setTimeout(function () {
    //        progress(0, 'Loading...', false);
    //    }, 1000);
    //}
    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);

        var mapvalues = [];
        var columns = "";
        _.forEach($scope.gridOptions.api.columnController.allColumns, function (value) {

            columns = columns + value.colId + " as [" + value.colDef.headerName + "],"

        });

        mapvalues.push(_.map($scope.gridOptions.api.inMemoryRowController.rowsAfterFilter, 'data'));
        console.log(mapvalues);
        alasql('SELECT ' + columns.slice(0, -1) + ' INTO XLSX("ConsolidatedReport.xlsx",{headers:true}) \
                    FROM ?', JSON.parse(JSON.stringify(mapvalues)));
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.gridOptions.onColumnVisible = function (event) {
        if (event.visible) {
            $scope.HideColumns = 0;
            //console.log(event.column.colId + ' was made visible');     

            $scope.gridOptions.columnDefs.push(
                {
                    headerName: event.column.colDef.headerName,
                    field: event.column.colId,
                    cellClass: "grid-align",
                    width: 110,
                    sno: event.column.colDef.sno,
                    suppressMenu: true
                }
            );
            $scope.gridOptions.columnDefs.visible = true;
            _.sortBy($scope.gridOptions.columnDefs, [function (o) { return o.sno; }]);
        } else {
            //console.log($scope.gridOptions.columnDefs.length);
            $scope.HideColumns = 1;
            for (var i = 0; i < $scope.gridOptions.columnDefs.length; i++) {
                if ($scope.gridOptions.columnDefs[i].field === event.column.colId) {
                    $scope.gridOptions.columnDefs[i].visible = false;
                    $scope.gridOptions.columnDefs.splice(i, 1);
                }
            }
        }
    };
    setTimeout(function () {
        $scope.GenReport = function (ConsReport, Type) {
            progress(0, 'Loading...', true);
            woobj = {};
            woobj.CompanyId = ConsReport.CNP_NAME[0].CNP_ID;
            woobj.FromDate = ConsReport.FromDate;
            woobj.ToDate = ConsReport.ToDate;
            woobj.Type = Type;
            //if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            //    if (ConsReport.Type == "pdf") {
            //        $scope.GenerateFilterPdf();
            //    }
            //    else {
            //        $scope.GenerateFilterExcel();
            //    }
            //} else {
            $http({
                url: UtilityService.path + '/api/HDMConsolidatedReport/GetConsolidatedReportDoc',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                //if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                //    window.navigator.msSaveOrOpenBlob(file, 'Consolidated Report.' + Type);
                //}
                //else if ((navigator.userAgent.indexOf("Firefox") != -1)) {
                //    window.navigator.msSaveOrOpenBlob(file, 'Consolidated Report.' + Type);
                //}
                //   else {
                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Consolidated Report.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, 'Loading...', false);
                // }
            }).error(function (data, status, headers, config) {

            });
            //  };
        };
    }, 2500);
   // $timeout($scope.GetConsReport, 1500);


}]);

