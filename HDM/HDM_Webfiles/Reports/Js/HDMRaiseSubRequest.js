﻿app.service("HDMRaiseSubRequestService", function ($http, $q, UtilityService) {
    this.GetAssertLocations = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMRaiseSubRequest/GetAssertLocations')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSubCategoriesByMain = function (MNC_CODE) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMRaiseSubRequest/GetSubCategories', MNC_CODE)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetChildCategoriessBySub = function (MNC_SUB_CODE) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMRaiseSubRequest/GetChildCategories', MNC_SUB_CODE)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetMainCat = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMRaiseSubRequest/GetMainCat')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.Getsubcat = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMRaiseSubRequest/Getsubcat')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.Getchildcat = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMRaiseSubRequest/Getchildcat')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.GetApprovals = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMRaiseSubRequest/GetApprovals')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SaveEmployeeDetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMRaiseSubRequest/SaveEmpDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

    this.getClientPrefixId = function (LcmCode) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMRaiseSubRequest/getClientPrefixId?LcmCode=' + LcmCode + '')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

});


app.controller('HDMRaiseSubRequestController', function ($scope, $q, $http, HDMRaiseSubRequestService, UtilityService, $timeout, $filter) {
    $scope.HDMRaiseSubRequest = {};

    $scope.Clear = function () {

        angular.forEach($scope.Locations, function (Locations) {
            Locations.ticked = false;
        });
        angular.forEach($scope.Main_Category, function (Main_Category) {
            Main_Category.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (Sub_Category) {
            Sub_Category.ticked = false;
        });
        angular.forEach($scope.child_Category, function (child_Category) {
            child_Category.ticked = false;
        });
        //angular.forEach($scope.AssetLocation, function (AssetLocation) {
        //    AssetLocation.ticked = false;
        //});
        //angular.forEach($scope.Approvals, function (Approvals) {
        //    Approvals.ticked = false;
        //});
        angular.element("input[type='file']").val(null);
        $scope.HDMRaiseSubRequest = {};
        $scope.HDMRaiseSubReq.$submitted = false;
        rowData = [];
        gridOptions.api.setRowData(rowData);
    }

    /////////////////////////////
    ///////////////////////////

    $scope.SaveEmpDetails = function () {
        var params = {

            LCMlst: $scope.HDMRaiseSubRequest.Locations,
            CHILDlst: $scope.HDMRaiseSubRequest.child_Category,
            //ASSERTlst: $scope.HDMRaiseSubRequest.AssetLocation,
            Mobile: $scope.HDMRaiseSubRequest.Mobile,
            InvoiceNo: $scope.HDMRaiseSubRequest.InvoiceNo,
            CustID: $scope.HDMRaiseSubRequest.CustID,
            CustName: $scope.HDMRaiseSubRequest.CustName,
            ProbDesc: $scope.HDMRaiseSubRequest.ProbDesc,
            BilledBy: $scope.HDMRaiseSubRequest.BilledBy,
            //BillDate: $scope.HDMRaiseSubRequest.Billdate,
            //BillNo: $scope.HDMRaiseSubRequest.BillNo,
            InvoiceDate: $scope.HDMRaiseSubRequest.InvoiceDate,
            PostedFiles: rowData,
            Amount: $scope.HDMRaiseSubRequest.Amount,
            Data: data
            //Approvals: $scope.HDMRaiseSubRequest.Approvals[0].SER_APP_STA_ID,
            //CompanyId: cp.CNP_ID
        };

        if ($scope.HDMRaiseSubRequest.Locations.length != 0 && $scope.HDMRaiseSubRequest.child_Category.length != 0 && $scope.HDMRaiseSubRequest.InvoiceNo != undefined && $scope.HDMRaiseSubRequest.CustID != undefined && $scope.HDMRaiseSubRequest.CustName != undefined && $scope.HDMRaiseSubRequest.ProbDesc != undefined && $scope.HDMRaiseSubRequest.InvoiceDate != undefined) {
            HDMRaiseSubRequestService.SaveEmployeeDetails(params).then(function (response) {
                $.ajax({
                    type: "POST",
                    url: 'https://live.quickfms.com/api/HDMRaiseSubRequest/UploadFiles',    // CALL WEB API TO SAVE THE FILES.
                    //enctype: 'multipart/form-data',
                    contentType: false,
                    processData: false,         // PREVENT AUTOMATIC DATA PROCESSING.
                    cache: false,
                    data: params.Data, 		        // DATA OR FILES IN THIS CONTEXT.
                    success: function (data, textStatus, xhr) {
                        showNotification('success', 8, 'bottom-right', response.Message);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(textStatus + ': ' + errorThrown);
                    }
                });
                //showNotification('success', 8, 'bottom-right', response.Message);
                $scope.Clear();
            }, function (error) {
                console.log(error);
            });


        }
    }

    $scope.getClientPrefixId = function () {
        console.log($scope.HDMRaiseSubRequest.Locations[0].LCM_CODE);
        HDMRaiseSubRequestService.getClientPrefixId($scope.HDMRaiseSubRequest.Locations[0].LCM_CODE).then(function (response) {
            console.log(response);
            $scope.HDMRaiseSubRequest.CustID = response.data[0].LCM_PREFIX_ID;
        }, function (error) {
            console.log(error);
        });
    };

    $scope.getsubbymain = function () {
        UtilityService.getsubbymain($scope.HDMRaiseSubRequest.Main_Category).then(function (response) {
            $scope.Sub_Category = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getchildbysub = function () {
        UtilityService.getchildbysub($scope.HDMRaiseSubRequest.Sub_Category).then(function (response) {
            $scope.child_Category = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Main_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (value, key) {
            var main = _.find($scope.Main_Category, { MNC_CODE: value.MNC_CODE });
            if (main != undefined && value.ticked == true) {
                main.ticked = true;
                $scope.HDMRaiseSubRequest.Main_Category[0] = main;
            }
        });
    }
    $scope.childchange = function () {

        angular.forEach($scope.Main_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.child_Category, function (value, key) {
            var main = _.find($scope.Main_Category, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
            if (main != undefined && value.ticked == true) {
                main.ticked = true;
                $scope.HDMRaiseSubRequest.Main_Category[0] = main;
            }
        });

        angular.forEach($scope.child_Category, function (value, key) {
            var sub = _.find($scope.Sub_Category, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sub != undefined && value.ticked == true) {
                sub.ticked = true;
                $scope.HDMRaiseSubRequest.Sub_Category[0] = sub;
            }
        });

    }


    //$scope.getchildbysub = function (MNC_SUB_CODE) {
    //    console.log(MNC_SUB_CODE);
    //    HDMRaiseSubRequestService.GetChildCategoriessBySub(MNC_SUB_CODE).then(function (response) {
    //        $scope.child_Category = response.data;
    //    }, function (error) {
    //        console.log(error);
    //    });
    //}
    //$scope.getsubbymain = function () {
    //    UtilityService.getsubbymain($scope.HDMcustomized.Main_Category).then(function (response) {
    //        $scope.Sub_Category = response.data;
    //    }, function (error) {
    //        console.log(error);
    //    });
    //}
    //$scope.getchildbysub = function () {
    //    UtilityService.getchildbysub($scope.HDMcustomized.Sub_Category).then(function (response) {
    //        $scope.child_Category = response.data;
    //    }, function (error) {
    //        console.log(error);
    //    });
    //    angular.forEach($scope.Main_Category, function (value, key) {
    //        value.ticked = false;
    //    });
    //    angular.forEach($scope.Sub_Category, function (value, key) {
    //        var main = _.find($scope.Main_Category, { MNC_CODE: value.MNC_CODE });
    //        if (main != undefined && value.ticked == true) {
    //            main.ticked = true;
    //            $scope.HDMcustomized.Main_Category[0] = main;
    //        }
    //    });
    //}
    $scope.childchange = function () {

        angular.forEach($scope.Main_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.child_Category, function (value, key) {
            var main = _.find($scope.Main_Category, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
            if (main != undefined && value.ticked == true) {
                main.ticked = true;
                $scope.HDMRaiseSubRequest.Main_Category[0] = main;
            }
        });

        angular.forEach($scope.child_Category, function (value, key) {
            var sub = _.find($scope.Sub_Category, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sub != undefined && value.ticked == true) {
                sub.ticked = true;
                $scope.HDMRaiseSubRequest.Sub_Category[0] = sub;
            }
        });

    }

    $scope.PageLoad = function () {

        $scope.HDMRaiseSubRequest.Main_Category = '--select--';
        UtilityService.GetLocationsall(3).then(function (response) {
            if (response.data != null) {
                $scope.Locations = response.data;
            }
        });

        //UtilityService.Getmaincat().then(function (response) {
        HDMRaiseSubRequestService.GetMainCat().then(function (response) {
            if (response.data != null) {
                $scope.Main_Category = response.data;
                $scope.HDMRaiseSubRequest.Main_Category = $scope.Main_Category;
                //console.log($scope.HDMRaiseSubRequest);
                //console.log($scope.HDMRaiseSubRequest[0].MNC_CODE);
            }
        });
        HDMRaiseSubRequestService.Getsubcat().then(function (response) {
            if (response.data != null) {
                $scope.Sub_Category = response.data;
                $scope.HDMRaiseSubRequest.Sub_Category = $scope.Sub_Category;
                //console.log($scope.HDMRaiseSubRequest[0].Sub_Category);
            }
        });
        HDMRaiseSubRequestService.Getchildcat().then(function (response) {
            if (response.data != null) {
                $scope.child_Category = response.data;
            }
        });
        //HDMRaiseSubRequestService.GetApprovals().then(function (response) {
        //    $scope.Approvals = response.data;
        //    $scope.HDMRaiseSubRequest.Approvals = $scope.Approvals;
        //}, function (error) {
        //    console.log(error);
        //});
        //HDMRaiseSubRequestService.GetAssertLocations().then(function (response) {
        //    $scope.AssetLocation = response.data;
        //    $scope.HDMRaiseSubRequest.AssetLocation = $scope.AssetLocation;
        //}, function (error) {
        //    console.log(error);
        //});

    }
    $scope.PageLoad();

});
