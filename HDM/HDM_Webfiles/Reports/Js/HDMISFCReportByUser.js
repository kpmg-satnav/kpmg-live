﻿app.service("HDMISFCReportByUserService", function ($http, $q) {
    this.getHDMISFCReportByUser = function (searchObj) {
        var deferred = $q.defer();
        return $http.post('../../../../api/HDMISFCReportByUser/GetHDMReportByUser', searchObj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetHistory = function (data) {
        
        deferred = $q.defer();
        return $http.post('../../../../api/HDMISFCReportByUser/GetHistory/', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    

    this.DloadFiles = function (data) {
        deferred = $q.defer();
        return $http.post('../../../../api/HDMISFCReportByUser/DloadFiles/',data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetStatusTypes = function () {
        deferred = $q.defer();
        return $http.get('../../../../api/HDMISFCReportByUser/GetStatusTypes')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});

app.controller('HDMISFCReportByUserController', function ($scope, $q, HDMISFCReportByUserService, UtilityService, HDMUtilityService, $timeout, $http) {
    $scope.RptByUsr = {};
    $scope.Getcountry = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];

    $scope.maincategorylist = [];
    $scope.SubCategorylist = [];
    $scope.ChildCategorylist = [];
    $scope.ReqBylist = [];
    $scope.ReqSTAlist = [];

    $scope.RptByUsrGrid = true;
    sendCheckedValsObj = [];
    $scope.tempspace = {};
    $scope.DocTypeVisible = 0;
    //$scope.StatusTypes = [];
    $scope.Status = [];
    $scope.RptByUsr.CNP_NAME = [];
    //$scope.RptByUsr.Status = "3";
    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                //console.log(CompanySession);
                $scope.Company = response.data;
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.RptByUsr.CNP_NAME.push(a);
                });

                if (CompanySession == "1") { $scope.CompanyVisible = 0; }
                else { $scope.CompanyVisible = 1; }
            }
        });
    }, 300);

    UtilityService.getCountires(2).then(function (response) {
        $scope.Getcountry = response.data;
    }, function (error) {
        console.log(error);
    });

    $scope.CountryChanged = function () {
        UtilityService.getCitiesbyCny($scope.RptByUsr.selectedCountries, 2).then(function (response) {
            $scope.Citylst = response.data
        }, function (error) {
            console.log(error);
        });
    }

    $scope.CnyChangeAll = function () {
        UtilityService.getCitiesbyCny($scope.Getcountry, 2).then(function (response) {
            $scope.Citylst = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectNone = function () {
        $scope.Citylst = [];
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    $scope.CityChanged = function () {
        UtilityService.getLocationsByCity($scope.RptByUsr.selectedCities, 2).then(function (response) {
            $scope.Locationlst = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.CtyChangeAll = function () {
        UtilityService.getLocationsByCity($scope.Citylst, 2).then(function (response) {
            $scope.Locationlst = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.ctySelectNone = function () {
        $scope.Locationlst = [];
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    $scope.LocChange = function () {
        UtilityService.getTowerByLocation($scope.RptByUsr.selectedLocations, 2).then(function (response) {
            $scope.Towerlist = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.LCMChangeAll = function () {
        UtilityService.getTowerByLocation($scope.Locationlst, 2).then(function (response) {
            $scope.Towerlist = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.lcmSelectNone = function () {
        $scope.Towerlist = [];
        $scope.Floorlist = [];
    }

    HDMUtilityService.getMainCategories().then(function (data) {
        $scope.maincategorylist = data.data;
    }, function (error) {
        console.log(error);
    });

    $scope.MCChange = function () {
        HDMUtilityService.getSubCatbyMainCategories($scope.RptByUsr.selectedMC).then(function (data) {
            $scope.SubCategorylist = data.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.MCChangeAll = function () {
        HDMUtilityService.getSubCatbyMainCategories($scope.maincategorylist).then(function (data) {
            $scope.SubCategorylist = data.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.MCSelectNone = function () {
        $scope.SubCategorylist = [];
    }

    $scope.SCChange = function () {
        HDMUtilityService.getChildCatBySubCategories($scope.RptByUsr.selectedSC).then(function (data) {
            $scope.ChildCategorylist = data.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.SCChangeAlll = function () {
        HDMUtilityService.getChildCatBySubCategories($scope.SubCategorylist).then(function (data) {
            $scope.ChildCategorylist = data.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.SCSelectNone = function () {
        $scope.maincategorylist = [];
    }

    HDMUtilityService.getRequestedUsers().then(function (response) {
        $scope.ReqBylist = response.data;
    }, function (error) {
        console.log(error);
    });

    HDMUtilityService.getStatus().then(function (response) {
        $scope.ReqSTAlist = response.data;
    }, function (error) {
        console.log(error);
    });

    HDMISFCReportByUserService.GetStatusTypes().then(function (response) {
        //console.log(response.data);
        $scope.Status = response.data;
    }, function (error) {
        console.log(error);
    });

    $scope.getHDMISFCReportByUser = function () {
        var tarray = [];

        var ticked = _.filter($scope.Status, function (o) {
            return o.ticked == true;
        });
        tarray.push(_.map(ticked, function (item) { return item.SCODE; }));

        $scope.rptDateRanges();
        $scope.HDMRptByUser = {
            FromDate: $scope.RptByUsr.FromDate,
            ToDate: $scope.RptByUsr.ToDate,
            CompanyId: $scope.RptByUsr.CNP_NAME[0].CNP_ID,
            Status: tarray.toString()
        };
        var fromdate = ($scope.RptByUsr.FromDate);
        var todate = ($scope.RptByUsr.ToDate);
        if (fromdate > todate) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
        }
        else {
            progress(0, 'Loading...', true);
            HDMISFCReportByUserService.getHDMISFCReportByUser($scope.HDMRptByUser).then(function (response) {
                $scope.RptByUsrGrid = true;
                $scope.gridata = response;
                if (response == null) {
                    $scope.gridOptions.api.setRowData([]);
                    progress(0, 'Loading...', false);
                }
                else {
                    progress(0, 'Loading...', true);
                    $scope.gridOptions.api.setRowData(response);
                    progress(0, 'Loading...', false);
                }

                function bindLocChart() {
                    $scope.LocWiseBarChart($scope.HDMRptByUser);
                }
                function bindSubCatGraph() {
                    $scope.subCatGraph($scope.HDMRptByUser);
                }
                $timeout(bindLocChart, 1000);
                $timeout(bindSubCatGraph, 1000);
            }, function (error) {
                console.log(error);
            });
        }
    };

    var columnDefs = [
        { headerName: "Req ID", field: "SER_REQ_ID", width: 120, cellClass: 'grid-align', filter: 'set', template: '<a ng-click="ShowPopup(data)">{{data.SER_REQ_ID}}</a>', pinned: 'left' },
        { headerName: "Requested By", field: "AUR_KNOWN_AS", width: 150, cellClass: 'grid-align', },
        { headerName: "Location", field: "LCM_NAME", width: 150, cellClass: 'grid-align', },
        { headerName: "Main Category ", field: "MNC_NAME", width: 150, cellClass: 'grid-align', },
        { headerName: "Sub Category", field: "SUBC_NAME", width: 150, cellClass: 'grid-align', },
        { headerName: "Child Category", field: "CHC_TYPE_NAME", width: 200, cellClass: 'grid-align', },
        { headerName: "Status", field: "STATUS", width: 120, cellClass: 'grid-align', },
        { headerName: "Total Amount", field: "TOTAL_AMOUNT", width: 120, cellClass: 'grid-align', },
        { headerName: "No.Of.Approvals", field: "NO_OF_APPROVALS", width: 120, cellClass: 'grid-align', },
        { headerName: "L1 Approved", field: "L1_Approved", width: 120, cellClass: 'grid-align', },
        { headerName: "L1 Approved Date", field: "L1_APPR_DT", width: 120, cellClass: 'grid-align', },
        { headerName: "L1 Remarks", field: "L1_COMMENTS", width: 120, cellClass: 'grid-align', },
        { headerName: "L2 Approved", field: "L2_Approved", width: 120, cellClass: 'grid-align', },
        { headerName: "L2 Approved Date", field: "L2_APPR_DT", width: 120, cellClass: 'grid-align', },
        { headerName: "L2 Remarks", field: "L2_COMMENTS", width: 120, cellClass: 'grid-align', },
        { headerName: "NoOfEscalations", field: "NoOfEscalations", width: 150, cellClass: 'grid-align', },
        { headerName: "SLA", field: "SLA", width: 80, cellClass: 'grid-align', },
        { headerName: "DEFINED TAT", field: "DEFINED_TAT", width: 150, cellClass: 'grid-align', },
        { headerName: "TAT", field: "TAT", width: 80, cellClass: 'grid-align', },
        { headerName: "TAT_SLA", field: "TAT_SLA", width: 80, cellClass: 'grid-align', },
        { headerName: "DELAYED_TIME", field: "DELAYED_TIME", width: 130, cellClass: 'grid-align', },
        { headerName: "DELAYED_SLA", field: "DELAYED_SLA", width: 130, cellClass: 'grid-align', },
        { headerName: "TOTAL", field: "TOTAL", width: 80, cellClass: 'grid-align', },
        { headerName: "Approved Date", field: "SER_UPDATED_DT", width: 130, cellClass: 'grid-align', },
        { headerName: "Remarks", field: "SER_PROB_DESC", width: 130, cellClass: 'grid-align', },
        { headerName: "Claim Amount", field: "SER_CLAIM_AMT", width: 130, cellClass: 'grid-align', },
        { headerName: "Approved Amount", field: "SER_APPR_AMOUNT", width: 130, cellClass: 'grid-align', },
        { headerName: "L3 Approved", field: "L3_Approved", width: 120, cellClass: 'grid-align', },
        { headerName: "L3 Approved Date", field: "L3_APPR_DT", width: 120, cellClass: 'grid-align', },
        { headerName: "L3 Remarks", field: "L3_COMMENTS", width: 120, cellClass: 'grid-align', },
        { headerName: "L4 Approved", field: "L4_Approved", width: 120, cellClass: 'grid-align', },
        { headerName: "L4 Approved Date", field: "L4_APPR_DT", width: 120, cellClass: 'grid-align', },
        { headerName: "L4 Remarks", field: "L4_COMMENTS", width: 120, cellClass: 'grid-align', },
        { headerName: "L5 Approved", field: "L5_Approved", width: 120, cellClass: 'grid-align', },
        { headerName: "L5 Approved Date", field: "L5_APPR_DT", width: 120, cellClass: 'grid-align', },
        { headerName: "L5 Remarks", field: "L5_COMMENTS", width: 120, cellClass: 'grid-align', },
        { headerName: "L6 Approved", field: "L6_Approved", width: 120, cellClass: 'grid-align', },
        { headerName: "L6 Approved Date", field: "L6_APPR_DT", width: 120, cellClass: 'grid-align', },
        { headerName: "L6 Remarks", field: "L6_COMMENTS", width: 120, cellClass: 'grid-align', },
        { headerName: "L7 Approved", field: "L7_Approved", width: 120, cellClass: 'grid-align', },
        { headerName: "L7 Approved Date", field: "L7_APPR_DT", width: 120, cellClass: 'grid-align', },
        { headerName: "L7 Remarks", field: "L7_COMMENTS", width: 120, cellClass: 'grid-align', },
        { headerName: "L8 Approved", field: "L8_Approved", width: 120, cellClass: 'grid-align', },
        { headerName: "L8 Approved Date", field: "L8_APPR_DT", width: 120, cellClass: 'grid-align', },
        { headerName: "L8 Remarks", field: "L8_COMMENTS", width: 120, cellClass: 'grid-align', },
        { headerName: "L9 Approved", field: "L9_Approved", width: 120, cellClass: 'grid-align', },
        { headerName: "L9 Approved Date", field: "L9_APPR_DT", width: 120, cellClass: 'grid-align', },
        { headerName: "L9 Remarks", field: "L9_COMMENTS", width: 120, cellClass: 'grid-align', },
        { headerName: "L10 Approved", field: "L10_Approved", width: 120, cellClass: 'grid-align', },
        { headerName: "L10 Approved Date", field: "L10_APPR_DT", width: 120, cellClass: 'grid-align', },
        { headerName: "L10 Remarks", field: "L10_COMMENTS", width: 120, cellClass: 'grid-align', },
        //{ headerName: "Downlaod", field: "HRU_UPL_PATH", width: 120, cellClass: 'grid-align', filter: 'set', template: '<a ng-click="downloadfile(data.HRU_UPL_PATH)">{{data.HRU_UPL_PATH}</a>' },
        {
            headerName: "", field: "HRU_UPL_PATH", width: 250, cellRenderer: function (params) {
                return '<a download="' + params.data.HRU_UPL_PATH + '" href="' + 'https://live.quickfms.com/UploadFiles/[ISFC].dbo/' + params.data.HRU_UPL_PATH + '"><span class="glyphicon glyphicon-download"></span></a>';
            }, width: 30, pinned: 'right', suppressMenu: true, cellStyle: { 'text-align': "center" }
        }
    ];
    //$scope.downloadfile = function (params) {
    //    var a = document.createElement("a");
    //    a.href = 'https://live.quickfms.com/UploadFiles/[ISFC].dbo/' + params;
    //    a.setAttribute("download", params);
    //    a.click();
    //};
   


    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Req ID", key: "SER_REQ_ID" }, { title: "Requested By", key: "AUR_KNOWN_AS" }, { title: "Location", key: "LCM_NAME" },
        { title: "Main Category ", key: "MNC_NAME" }, { title: "Sub Category", key: "SUBC_NAME" }, { title: "Child Category", key: "CHC_TYPE_NAME" },
        { title: "Status", key: "STATUS" }, { title: "No.Of.Approvals", key: "NO_OF_APPROVALS" }, { title: "L1 Approved", key: "L1_Approved" },
        { title: "L2 Approved", key: "L2_Approved" }, { title: "L3 Approved", key: "L3_Approved" }, { title: "L4 Approved", key: "L4_Approved" },
        { title: "L5 Approved", key: "L5_Approved" }, { title: "L6 Approved", key: "L6_Approved" }, { title: "L7 Approved", key: "L7_Approved" },
        { title: "L8 Approved", key: "L8_Approved" }, { title: "L9 Approved", key: "L9_Approved" }, { title: "L10 Approved", key: "L10_Approved" },

        { title: "NoOfEscalations", key: "NoOfEscalations" }, { title: "SLA", key: "SLA" },
        { title: "DEFINED TAT", key: "DEFINED_TAT" }, { title: "TAT", key: "TAT" }, { title: "TAT_SLA", key: "TAT_SLA" },
        { title: "Delayed Time(in Minutes)", key: "DELAYED_TIME" }, { title: "DELAYED_SLA", key: "DELAYED_SLA" }, { title: "TOTAL", key: "TOTAL" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("ReportByUser.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ",",
            fileName: "ReportByUser.csv"
        }; $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function onUpdateFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
        } else {
            $scope.DocTypeVisible = 0;
        }
    }
    $("#UpdteFilter").change(function () {
        onUpdateFilterChanged($(this).val());
    }).keydown(function () {
        onUpdateFilterChanged($(this).val());
    }).keyup(function () {
        onUpdateFilterChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFilterChanged($(this).val());
    })

    //$scope.RptByUsr.selVal = "THISMONTH";

    $scope.rptDateRanges = function () {
        switch ($scope.RptByUsr.selVal) {
            case 'TODAY':
                $scope.RptByUsr.FromDate = moment().format('DD-MMM-YYYY');
                $scope.RptByUsr.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'YESTERDAY':
                $scope.RptByUsr.FromDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                $scope.RptByUsr.ToDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                break;
            case '7':
                $scope.RptByUsr.FromDate = moment().subtract(6, 'days').format('DD-MMM-YYYY');
                $scope.RptByUsr.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case '30':
                $scope.RptByUsr.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
                $scope.RptByUsr.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'THISMONTH':
                $scope.RptByUsr.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
                $scope.RptByUsr.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
                break;
            case 'LASTMONTH':
                $scope.RptByUsr.FromDate = moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY');
                $scope.RptByUsr.ToDate = moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY');
                break;
        }
    }


    function getLastMonthData() {
        $scope.getHDMISFCReportByUser();
    }

    function getLastMonthChartData() {
        $scope.LocWiseBarChart($scope.HDMRptByUser);
    }

    //$timeout(getLastMonthData, 500);

    var PopDefs = [
        { headerName: "Req Id", suppressMenu: true, field: "SERH_SER_ID", width: 120, cellClass: 'grid-align', pinned: 'left' },
        { headerName: "Updated On", suppressMenu: true, field: "CREATEDDATE", template: '<span>{{data.CREATEDDATE | date:"MMM dd yyyy - HH:mm"}}</span>', width: 190, cellClass: 'grid-align' },
        { headerName: "Updated By", suppressMenu: true, field: "CREATEDBY", width: 190, cellClass: 'grid-align' },
        { headerName: "Status", suppressMenu: true, field: "SERH_STA_TITLE", width: 190, cellClass: 'grid-align' },
        { headerName: "Remarks", suppressMenu: true, field: "SERH_COMMENTS", width: 190, cellClass: 'grid-align' },
    ];

    $scope.PopOptions = {
        columnDefs: PopDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
    }

    $scope.ShowPopup = function (data) {
        $scope.SelValue = data;
        $("#historymodal").modal('show');
    }
    $('#historymodal').on('shown.bs.modal', function () {
        HDMISFCReportByUserService.GetHistory($scope.SelValue).then(function (response) {
            $scope.popdata = response.data;
            //console.log($scope.popdata);
            //$scope.PopOptions.api.setDatasource($scope.createNewDatasource($scope.popdata, $scope.ApprvlPageSize));
            $scope.PopOptions.api.setRowData($scope.popdata);
        });
    });

    $scope.GenReport = function (rptByUser, Type) {
        var tarray = [];

        var ticked = _.filter($scope.Status, function (o) {
            return o.ticked == true;
        });
        tarray.push(_.map(ticked, function (item) { return item.SCODE; }));
        progress(0, 'Loading...', true);
        rptByUser.DocType = Type;
        woobj = {};
        woobj.CompanyId = 0;
        woobj.DocType = Type;
        woobj.FromDate = rptByUser.FromDate;
        woobj.ToDate = rptByUser.ToDate;
        woobj.Status = tarray.toString();
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (rptByUser.DocType == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        } else {
            $http({
                url: '../../../../api/HDMISFCReportByUser/GetReportByUser',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });

                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    window.navigator.msSaveOrOpenBlob(file, 'ReportByUser.' + Type);
                }
                else if ((navigator.userAgent.indexOf("Firefox") != -1)) {
                    window.navigator.msSaveOrOpenBlob(file, 'ReportByUser.' + Type);
                }
                else {
                    //trick to download store a file having its URL
                    var fileURL = URL.createObjectURL(file);
                    //var popupwin = window.open(fileURL, "SpaceRequisitionReport", "toolbar=no,width=500,height=500");
                    //setTimeout(function () {
                    //    popupwin.focus();
                    //}, 500);
                    $("#reportcontainer").attr("src", fileURL);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = 'ReportByUser.' + Type;
                    document.body.appendChild(a);
                    a.click();
                    progress(0, 'Loading...', false);
                }
            }).error(function (data, status, headers, config) {

            });
        };
    }

    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    var chart;
    var chartSubCat;
    chart = c3.generate({
        data: {
            //x: 'x',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Location'],
                height: 130,
                show: true,
                //label: {
                //    text: 'Locations',
                //    position: 'outer-center'
                //}
            },
            y: {
                show: true,
                label: {
                    text: 'Requisition Count',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });


    chartSubCat = c3.generate({
        data: {
            //x: 'x',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Sub Category'],
                height: 130,
                show: true,
                //label: {
                //    text: 'Locations',
                //    position: 'outer-center'
                //}
            },
            y: {
                show: true,
                label: {
                    text: 'Requisition Count',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });

    $scope.LocWiseBarChart = function (data) {
        $http({
            url: '../../../../api/HDMISFCReportByUser/GetLocationWiseCount',
            method: 'POST',
            data: data
        }).success(function (result) {
            chart.unload({
                ids: ['LocationGraph']
            });
            //console.log(result);
            chart.load({ columns: result });
        });
        setTimeout(function () {
            $("#LocationGraph").append(chart.element);
        }, 700);
    }


    $scope.subCatGraph = function (spcData) {
        $http({
            url: '../../../../api/HDMISFCReportByUser/GetSubCatWiseCount',
            method: 'POST',
            data: spcData
        }).success(function (result) {
            //console.log(result);
            //chartSubCat.unload();
            chartSubCat.unload({
                ids: ['SubCatGraph']
            });
            chartSubCat.load({ columns: result });
        });

        setTimeout(function () {
            $("#SubCatGraph").empty();
            $("#SubCatGraph").append(chartSubCat.element);
            $("#SubCatGraph").load();
            //$("#SubCatGraph").append(chartSubCat.element);
        }, 1500);

    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
            });
        }
    });
    $timeout($scope.getHDMISFCReportByUser, 500);

    $scope.btndnload = function () {
        var params = {
            FromDate: $scope.RptByUsr.FromDate,
            ToDate: $scope.RptByUsr.ToDate
        }
        var add = params.FromDate + '' + 'to' + params.ToDate +'.zip';
        progress(0, 'Loading...', true);
        HDMISFCReportByUserService.DloadFiles(params).then(function (response) {
            var zip = new JSZip();
            var count = 0;
            var zipFilename = add;
            var urls = response;
            
            urls.forEach(function (url) {
                var filename = url;
                // loading a file and add it in a zip file
                JSZipUtils.getBinaryContent('http://localhost:55730/UploadFiles/[ISFC].dbo/'+url, function (err, data) {
                    
                    if (err) {
                        throw err; // or handle the error
                    }
                    zip.file(filename, data, { binary: true });
                    count++;
                    if (count == urls.length) {
                        var zipFile = zip.generate({ type: "blob" });
                        saveAs(zipFile, zipFilename);
                        progress(0, 'Loading...', false);
                    }
                });
            });
        })

    }


});

