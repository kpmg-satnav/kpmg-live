﻿#Region "NameSpaces"

Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO

#End Region

Partial Class HDM_HDM_Webfiles_Reports_frmHDMStatusDetailsReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack() Then
            Dim Status As String = Request("RID")
            If Status <> 0 Then
                '    BindGrid()
                'Else
                BindGrid(Status)
            End If
        End If

    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmHDMReportByUser.aspx")
    End Sub

    Private Sub BindGrid(ByVal Status As Integer)
        Dim Stat As String = Request("RID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GET_REPORT_BY_STATUS")
        'Dim dummy As String = sp.Command.CommandSql
        sp.Command.AddParameter("@STATUS", Stat, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If Request("RID") = 2 Then
            gvViewRequisitions.Columns(7).Visible = True
        End If
        If Request("RID") = 3 Then
            gvViewRequisitions.Columns(6).Visible = True
        End If
        gvViewRequisitions.DataSource = ds
        gvViewRequisitions.DataBind()
        For i As Integer = 0 To gvViewRequisitions.Rows.Count - 1
            Dim lblstatusId As Label = CType(gvViewRequisitions.Rows(i).FindControl("lblStatusId"), Label)
            Dim lblStatus As Label = CType(gvViewRequisitions.Rows(i).FindControl("lblStatus"), Label)
            If lblstatusId.Text = "1" Then
                lblStatus.Text = "Request Submitted"
            ElseIf lblstatusId.Text = "2" Then
                lblStatus.Text = "Request Modified"
            ElseIf lblstatusId.Text = "3" Then
                lblStatus.Text = "Request Cancelled"
            ElseIf lblstatusId.Text = "4" Then
                lblStatus.Text = "RM Approved"
            ElseIf lblstatusId.Text = "5" Then
                lblStatus.Text = "RM Rejected"
            ElseIf lblstatusId.Text = "6" Then
                lblStatus.Text = "On Hold"
            ElseIf lblstatusId.Text = "7" Then
                lblStatus.Text = "In-Progress"
            ElseIf lblstatusId.Text = "8" Then
                lblStatus.Text = "Closed"
            ElseIf lblstatusId.Text = "9" Then
                lblStatus.Text = "Assigned"
            End If
        Next

    End Sub

    'Private Sub BindGrid()
    '    Dim Stat As String = Request("RID")
    '    Dim FROMDATE As String = Request("FROMDATE")
    '    Dim TODATE As String = Request("TODATE")
    '    Dim EMPID As String = Request("EMPID")
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GET_REPORT_BY_STATUS_ALL")
    '    'Dim dummy As String = sp.Command.CommandSql
    '    sp.Command.AddParameter("@STATUS", Stat, DbType.String)
    '    sp.Command.AddParameter("@FROMDATE", FROMDATE, DbType.String)
    '    sp.Command.AddParameter("@TODATE", TODATE, DbType.String)
    '    sp.Command.AddParameter("@EMPID", EMPID, DbType.String)
    '    Dim ds As New DataSet
    '    ds = sp.GetDataSet()
    '    If Request("RID") = 0 Then
    '        gvViewRequisitions.Columns(7).Visible = True
    '    End If
    '    gvViewRequisitions.DataSource = ds
    '    gvViewRequisitions.DataBind()
    '    For i As Integer = 0 To gvViewRequisitions.Rows.Count - 1
    '        Dim lblstatusId As Label = CType(gvViewRequisitions.Rows(i).FindControl("lblStatusId"), Label)
    '        Dim lblStatus As Label = CType(gvViewRequisitions.Rows(i).FindControl("lblStatus"), Label)
    '        If lblstatusId.Text = "1" Then
    '            lblStatus.Text = "Request Submitted"
    '        ElseIf lblstatusId.Text = "2" Then
    '            lblStatus.Text = "Request Modified"
    '        ElseIf lblstatusId.Text = "3" Then
    '            lblStatus.Text = "Request Cancelled"
    '        ElseIf lblstatusId.Text = "4" Then
    '            lblStatus.Text = "RM Approved"
    '        ElseIf lblstatusId.Text = "5" Then
    '            lblStatus.Text = "RM Rejected"
    '        ElseIf lblstatusId.Text = "6" Then
    '            lblStatus.Text = "On Hold"
    '        ElseIf lblstatusId.Text = "7" Then
    '            lblStatus.Text = "In-Progress"
    '        ElseIf lblstatusId.Text = "8" Then
    '            lblStatus.Text = "Closed"
    '        ElseIf lblstatusId.Text = "9" Then
    '            lblStatus.Text = "Assigned"
    '        End If
    '    Next

    'End Sub

    Protected Sub gvViewRequisitions_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvViewRequisitions.PageIndexChanging
        gvViewRequisitions.PageIndex = e.NewPageIndex
        Dim Status As String = Request("RID")
        If Status <> 0 Then
            '    BindGrid()
            'Else
            BindGrid(Status)
        End If
        'BindGrid(Status)
    End Sub


    
    'Protected Sub gvViewRequisitions_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvViewRequisitions.RowDataBound
    '    If Request("RID") = 2 Then
    '        if (e.Row.RowType == DataControlRowType.Header) then    
    '            e.Row.Cells(6).Text = "Updated Time"
    '        End If
    '    End If
    'End Sub
End Class
