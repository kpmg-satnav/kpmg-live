﻿#Region "NameSpaces"

Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions

#End Region

Partial Class HDM_HDM_Webfiles_frmHDMAdminRequisitionDetails
    Inherits System.Web.UI.Page


#Region "Variable Declarations"

    Dim ObjSubsonic As New clsSubSonicCommonFunctions


#End Region

#Region "User Defined Methods"

    Private Function GetCurrentUser() As Integer
        If String.IsNullOrEmpty(Session("UID")) Then
            Return 0
        Else
            Return CInt(Session("UID"))
        End If
    End Function

    Private Sub BindRequisition()

        Dim ReqId As String = Request("RID")
        Dim RaisedBy As Integer = 0
        If String.IsNullOrEmpty(ReqId) Then
            ' lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_AMG_ITEM_REQUISITION_GetBy_adminReqId")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            'sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()

            lblReqId.Text = ReqId

            BindLocations()
            ddlLocation.ClearSelection()
            ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_BDG_ID")).Selected = True

            BindCategories()
            ddlServiceCategory.ClearSelection()
            ddlServiceCategory.Items.FindByValue(ds.Tables(0).Rows(0).Item("REQ_TYPE")).Selected = True

            BindServiceType()
            ddlServiceType.ClearSelection()
            ddlServiceType.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_REQ_FOR")).Selected = True
            ' ddlEmp.Items.FindByValue(ds.Tables(0).Rows(0).Item("ASSIGNED_TO")).Selected = True
            ddlEmp.Items.Insert(0, New ListItem(ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS"), ds.Tables(0).Rows(0).Item("ASSIGNED_TO")))
            txtStatus.Text = ds.Tables(0).Rows(0).Item("REQ_STATUS")

            txtRemarks.Text = ds.Tables(0).Rows(0).Item("SER_DESCRIPTION")
            'txtconvdate.Text = ds.Tables(0).Rows(0).Item("CONVINIENT_DATE")
            txtEmployee.Text = ds.Tables(0).Rows(0).Item("SER_AUR_ID")
            txtAddress.Text = ds.Tables(0).Rows(0).Item("ADDRESS")
            txtSIRemarks.Text = ds.Tables(0).Rows(0).Item("SER_RM_REMARKS")
            txtadmin2.Text = ds.Tables(0).Rows(0).Item("SER_ADM_REMARKS")
            'txtConvTime.Text = ds.Tables(0).Rows(0).Item("CONVINIENT_TIME")
            'txtL1Time.Text = ""
            'txtL1Time.Text = ds.Tables(0).Rows(0).Item("L1_TIME")
            'txtL2Time.Text = ""
            'txtL2Time.Text = ds.Tables(0).Rows(0).Item("L2_TIME")
            'txtL3Time.Text = ""
            'txtL3Time.Text = ds.Tables(0).Rows(0).Item("L3_TIME")
            '    Dim Status As Integer
            '    Status = ds.Tables(0).Rows(0).Item("SER_STATUS")
            '    If Status > 1 Then
            '        'btnCancel.Visible = False
            '    Else
            '        'btnCancel.Visible = True
            '    End If
            Dim Status As Integer
            Status = ds.Tables(0).Rows(0).Item("SER_STATUS")
            If Status > 2 Then
                ddlAssign.Visible = False
                txtAdminRemarks.Visible = False
                btnAssign.Visible = False
                trassignedto.Visible = False
                tradminremarks.Visible = False
                trSIRemarks.Visible = True
                tradmin2.Visible = True
                'btnCancel.Visible = False
                'btnReload.Visible = False
                'btnModify.Visible = False
            Else
                ddlAssign.Visible = True
                'txtAdminRemarks.Visible = True
                btnAssign.Visible = True
                trassignedto.Visible = True
                tradminremarks.Visible = True
                'txtSIRemarks.Visible = True
                trSIRemarks.Visible = False
                tradmin2.Visible = False
            End If
        End If
    End Sub

    Private Sub BindUsers(ByVal AUR_ID As String)

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = AUR_ID
        ObjSubsonic.Binddropdown(ddlEmp, "HDM_BINDUSERS_NAME", "NAME", "AUR_ID", param)
        Dim li As ListItem = ddlEmp.Items.FindByValue(AUR_ID)
        If Not li Is Nothing Then
            li.Selected = True
        End If
    End Sub

    Private Sub BindCategories()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GETACTIVESERREQUESTTYPE")
        sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
        ddlServiceCategory.DataSource = sp.GetDataSet()
        ddlServiceCategory.DataTextField = "SER_NAME"
        ddlServiceCategory.DataValueField = "SER_CODE"
        ddlServiceCategory.DataBind()
        ddlServiceCategory.Items.Insert(0, "--Select--")

    End Sub

    Private Sub BindServiceType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_SERTYPEBYCATLOCATION")
        sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SER_CODE", ddlServiceCategory.SelectedItem.Value, DbType.String)
        ddlServiceType.DataSource = sp.GetDataSet()
        ddlServiceType.DataTextField = "SER_TYPE_NAME"
        ddlServiceType.DataValueField = "SER_TYPE_CODE"
        ddlServiceType.DataBind()
        ddlServiceType.Items.Insert(0, "--Select--")
    End Sub

    Private Sub BindLocations()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Location_GetAll")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, "--Select--")

    End Sub

    Private Sub BindAssignedTo()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_ADMIN_ASSIGN_DETAILS")
        sp.Command.AddParameter("@SRQ_MAP_AURID", ddlEmp.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SRQ_MAP_REQID", ddlServiceCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SRQ_LCM_ID", ddlLocation.SelectedItem.Value, DbType.String)
        ddlAssign.DataSource = sp.GetDataSet()
        ddlAssign.DataTextField = "AUR_KNOWN_AS"
        ddlAssign.DataValueField = "AUR_ID"
        ddlAssign.DataBind()
        ddlAssign.Items.Insert(0, "--Select--")
    End Sub

    'Private Sub BindAssignUpdate()
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_UPDATE_ADMIN_ASSIGN_DETAILS")
    '    ddlEmp.DataSource = sp.GetDataSet()
    '    ddlEmp.DataTextField = "EMPLOYEE"
    '    ddlEmp.DataValueField = "SRQ_MAP_AUR_ID"
    '    ddlEmp.DataBind()
    '    ddlEmp.Items.Insert(0, "--Select--")
    'End Sub

    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function

    Private Sub DeleteRequistionItems(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_DeleteByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As Integer, ByVal Qty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_update")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

#End Region

#Region "Page Event Handlers"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                Dim UID As String = Session("uid")
                'BindUsers(UID)
                BindRequisition()
                BindAssignedTo()
                delUser()
            End If
        End If
    End Sub
    Public Sub delUser()
        If ddlAssign.Items.Count <> 0 Then
            Dim removeItem As ListItem = ddlAssign.Items.FindByValue(ddlEmp.SelectedValue)
            ddlAssign.Items.Remove(removeItem)
        End If
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("frmHDMAdminViewRequisitions.aspx")
    End Sub

#End Region

    Protected Sub btnAssign_Click(sender As Object, e As EventArgs) Handles btnAssign.Click
        'Dim ReqId As String = Request("RID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_UPDATE_ADMIN_ASSIGN_DETAILS")
        sp.Command.AddParameter("@REQ_ID", lblReqId.Text, DbType.String)
        sp.Command.AddParameter("@ASSIGNED_TO", ddlAssign.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SER_ADM_REMARKS", txtAdminRemarks.Text, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        'sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.ExecuteScalar()
        'BindRequisition()
        'ddlEmp.Visible = True
        'lblmsg.Visible = True
        'lblmsg.Text = "Employee Assigned Successfully"
        Response.Redirect("frmhelpthanks.aspx?id=9&Reqid=" & lblReqId.Text)
    End Sub
End Class
