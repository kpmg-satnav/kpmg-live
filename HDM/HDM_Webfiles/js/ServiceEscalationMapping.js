﻿app.directive('convertToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return parseInt(val, 10);
            });
            ngModel.$formatters.push(function (val) {
                return '' + val;
            });
        }
    };
});

app.service("ServiceEscalationService", ['$http', '$q', 'UtilityService',function ($http, $q, UtilityService) {
    var deferred = $q.defer();
    //to save
    this.SaveSEMDetails = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ServiceEscalationMapping/SaveDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //update
    this.UpdateSEMDetails = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ServiceEscalationMapping/UpdateDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //edit
    this.EditSEMDetails = function (dt) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ServiceEscalationMapping/EditSEMDetails/' + dt)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetMainCategory = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ServiceEscalationMapping/GetMaincategory')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetCountries = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ServiceEscalationMapping/GetCountries')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    ///Amantra_Multi_UI/api/SLA/GetSubCategoryByMain/4444
    this.GetSubCategoryByMain = function (SEM_MNC_CODE) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ServiceEscalationMapping/GetSubCategoryByMain/' + SEM_MNC_CODE)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetCityByCountry = function (SEM_CNY_CODE) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ServiceEscalationMapping/GetCityByCountry/' + SEM_CNY_CODE)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetLocationByCity = function (SEM_CTY_CODE) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ServiceEscalationMapping/GetLocationByCity/' + SEM_CTY_CODE)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetTowerByLocation = function (SEM_LOC_CODE) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ServiceEscalationMapping/GetTowerByLocation/' + SEM_LOC_CODE)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //to get child by sub category
    this.GetChildCategoryBySub = function (SEM_SUBC_CODE) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ServiceEscalationMapping/GetChildCategoryBySubCat/' + SEM_SUBC_CODE)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //For Grid
    this.GetSEMList = function (id) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ServiceEscalationMapping/GetSEMDetails/' + id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetRoleUserList = function (id) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ServiceEscalationMapping/GetRoleUserDetails/' + id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetRoleUserListByModule = function (id) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ServiceEscalationMapping/GetRoleUserListByModule/' + id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.downloadTemplate = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ServiceEscalationMapping/DownloadTemplate', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('ServiceController', ['$scope', '$q', 'ServiceEscalationService', 'ChildCatApprovalLocMatrixService', 'MainCategoryService', 'HDMUtilityService', 'UtilityService', 'SubCategoryService', 'ChildCategoryService', '$filter', '$http', function ($scope, $q, ServiceEscalationService, ChildCatApprovalLocMatrixService, MainCategoryService, HDMUtilityService, UtilityService, SubCategoryService, ChildCategoryService, $filter, $http) {
    $scope.SEMMaster = {};
    $scope.ActionStatus = 0;
    $scope.EnableDD = 0;
    $scope.Country = [];
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.maincategorylist = [];
    $scope.SubCategorylist = [];
    $scope.ChildCategorylist = [];
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;
    $scope.roleuserspageSize = '10';
    $scope.gridOptionsEnabled = true;
    $scope.genreportEnabled = true;
    //$scope.semdetailspageSize = '10';

    //main category drop down bind
    //ServiceEscalationService.GetMainCategory().then(function (data) {
    //    $scope.maincategorylist = data;
    //    //country
    //    ServiceEscalationService.GetCountries().then(function (data) {
    //        $scope.Country = data;
    //    }, function (error) {
    //        console.log(error);
    //    });
    //}, function (error) {
    //    console.log(error);
    //});
    setTimeout(function () {
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;

                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Location = response.data;

                                UtilityService.getTowers(2).then(function (response) {
                                    if (response.data != null) {
                                        $scope.Tower = response.data;
                                    }
                                });
                            }
                        });
                    }
                });

            }
        });
    }, 1000)


    UtilityService.GetCompanies().then(function (response) {
        if (response.data != null) {
            $scope.Company = response.data;
            $scope.SEMMaster.CNP_NAME = parseInt(CompanySession);
            if (CompanySession == "1") {
                $scope.EnableStatus = 1;
            }
            else { $scope.EnableStatus = 0; }
        }
    });

    $scope.CnyChangeAll = function () {
        $scope.SEMMaster.Country = $scope.Country;
        $scope.CnyChanged();
    }

    $scope.CnySelectNone = function () {
        $scope.SEMMaster.Country = [];
        $scope.CnyChanged();
    }

    $scope.CnyChanged = function () {
        if ($scope.SEMMaster.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.SEMMaster.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.SEMMaster.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.SEMMaster.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        $scope.SEMMaster.Country = [];

        UtilityService.getLocationsByCity($scope.SEMMaster.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.SEMMaster.Country.push(cny);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.SEMMaster.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.SEMMaster.Location = [];
        $scope.LcmChanged();
    }

    $scope.LcmChanged = function () {

        $scope.SEMMaster.Country = [];
        $scope.SEMMaster.City = [];
        UtilityService.getTowerByLocation($scope.SEMMaster.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.SEMMaster.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.SEMMaster.City.push(cty);
            }
        });


    }

    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.SEMMaster.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    $scope.TwrSelectNone = function () {
        $scope.SEMMaster.Tower = [];
        $scope.TwrChanged();
    }

    $scope.TwrChanged = function () {
        $scope.SEMMaster.Country = [];
        $scope.SEMMaster.City = [];
        $scope.SEMMaster.Location = [];

        UtilityService.getFloorByTower($scope.SEMMaster.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.SEMMaster.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.SEMMaster.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE, CTY_CODE: value.CTY_CODE });
            if (lcm != undefined && value.ticked == true && lcm.ticked == false) {
                lcm.ticked = true;
                $scope.SEMMaster.Location.push(lcm);
            }
        });
    }

    HDMUtilityService.getMainCategories().then(function (response) {
        if (response.data != null) {
            $scope.Main = response.data;

            HDMUtilityService.getAllSubCategories().then(function (response) {
                if (response.data != null) {
                    $scope.Sub = response.data;

                    HDMUtilityService.getAllChildCategories().then(function (response) {
                        if (response.data != null) {
                            $scope.Child = response.data;
                        }
                    });
                }
            });

        }
    });

    //// Main Category Events
    $scope.MainChangeAll = function () {
        $scope.SEMMaster.Main = $scope.Main;
        $scope.MainChanged();
    }

    $scope.MainSelectNone = function () {
        $scope.SEMMaster.Main = [];
        $scope.MainChanged();
    }

    $scope.MainChanged = function () {
        if ($scope.SEMMaster.Main.length != 0) {
            HDMUtilityService.getSubCatbyMainCategories($scope.SEMMaster.Main, 2).then(function (response) {
                if (response.data != null)
                    $scope.Sub = response.data;
                else
                    $scope.Sub = [];
            });
        }
        else
            $scope.Sub = [];
    }

    //// Sub Category Events
    $scope.SubChangeAll = function () {
        $scope.SEMMaster.Sub = $scope.Sub;
        $scope.SubChanged();
    }
    $scope.SubSelectNone = function () {
        $scope.SEMMaster.Sub = [];
        $scope.SubChanged();
    }

    $scope.SubChanged = function () {
        $scope.SEMMaster.Main = [];

        HDMUtilityService.getChildCatBySubCategories($scope.SEMMaster.Sub, 2).then(function (response) {
            if (response.data != null)
                $scope.Child = response.data;
            else
                $scope.Child = [];
        });

        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Sub, function (value, key) {
            var mn = _.find($scope.Main, { MNC_CODE: value.SUBC_MNC_CODE });
            if (mn != undefined && value.ticked == true && mn.ticked == false) {
                mn.ticked = true;
                $scope.SEMMaster.Main.push(mn);
            }
        });
    }

    ///// Child Category Events
    $scope.ChildChangeAll = function () {
        $scope.SEMMaster.Child = $scope.Child;
        $scope.ChildChanged();
    }

    $scope.ChildSelectNone = function () {
        $scope.SEMMaster.Child = [];
        $scope.ChildChanged();
    }

    $scope.ChildChanged = function () {
        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Child, function (value, key) {
            var mn = _.find($scope.Main, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
            if (mn != undefined && value.ticked == true && mn.ticked == false) {
                mn.ticked = true;
                $scope.SEMMaster.Main.push(mn);
            }
        });

        angular.forEach($scope.Child, function (value, key) {
            var sb = _.find($scope.Sub, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sb != undefined && value.ticked == true && sb.ticked == false) {
                sb.ticked = true;
                $scope.SEMMaster.Sub.push(sb);
            }
        });

    }
    //to bind subcategory on main category change
    //$scope.MainCategoryChanged = function () {
    //    ServiceEscalationService.GetSubCategoryByMain($scope.SEMMaster.SEM_MNC_CODE).then(function (data) {
    //        $scope.Sub = data;
    //        $scope.SubCategoryChanged(data.SEM_SUBC_CODE);
    //    }, function (error) {
    //        console.log(error);
    //    });
    //}
    ////to bind childcategory on sub category change

    //$scope.SubCategoryChanged = function () {
    //    ServiceEscalationService.GetChildCategoryBySub($scope.SEMMaster.SEM_SUBC_CODE).then(function (data) {
    //        $scope.Child = data;
    //        $scope.CountryChanged(data.SEM_CNY_CODE);
    //    }, function (error) {
    //        console.log(error);
    //    });
    //}

    $scope.createNewDatasource = function (data, pagesize) {
        var dataSource = {
            pageSize: parseInt(pagesize),
            getRows: function (params) {
                setTimeout(function () {
                    var rowsThisPage = data.slice(params.startRow, params.endRow);
                    var lastRow = -1;
                    if (data.length <= params.endRow) {
                        lastRow = data.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
                }, 500);
            }
        };
        return dataSource;
    }

    //For Grid column Headers
    $scope.roleusersoptionscolumnDefs = [
        { headerName: "Role ", field: "ROL_ACRONYM", cellClass: "grid-align", rowGroupIndex: 0 },
    ];

    $scope.roleusersoptions = {
        columnDefs: $scope.roleusersoptionscolumnDefs,
        rowData: null,
        angularCompileRows: true,
        enableScrollbars: false,
        rowSelection: 'multiple',
        groupSelectsChildren: true,
        width: 50,
        suppressRowClickSelection: true,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Role", field: "AUR_KNOWN_AS",
            cellRenderer: {
                renderer: "group",
                checkbox: true
            }
        },
        onReady: function () {
            $scope.roleusersoptions.api.sizeColumnsToFit()
        },
    };

    //To save a record
    $scope.SaveDetails = function () {
        progress(0, 'Loading...', true);
        var count = 0;
        var keep = true;
        if ($scope.IsInEdit) {
            console.log($scope.IsInEdit);
            if (keep) {
                $scope.roleusersoptions.api.getSelectedRows().forEach(function (node) {
                    console.log(node);
                    if ($scope.SEMMaster.Module != undefined) {
                        if ($scope.SEMMaster.Module[0].HDM_MAIN_MOD_ID != undefined) {
                            count++;
                        }
                    }
                    else {
                        if (node.ROL_ORDER == 1) {
                            count++;
                        }
                    }
                });
                if (count == 0) {
                    keep = false;
                    progress(0, '', false);
                    showNotification('error', 8, 'bottom-right', 'Please Select One Value For Role HD INCHARGE');
                }
            }
            if (keep) {
                $scope.data = { SEM_ID: $scope.SEMMaster.SEM_ID, TWRLST: $scope.SEMMaster.Tower, SEM: $scope.SEMMaster.Child, SEMD: $scope.roleusersoptions.api.getSelectedRows() };
                //$scope.dataobject = { SEM: $scope.SEMMaster, SEMD: $scope.roleusersoptions.api.getSelectedRows() };
                ServiceEscalationService.UpdateSEMDetails($scope.data).then(function (dataobj) {
                    $scope.LoadSemDetails(CompanySession);
                    $scope.clear();
                    setTimeout(function () {
                        $scope.$apply(function () {
                            showNotification('success', 8, 'bottom-right', dataobj.Message);
                        });
                    }, 700);
                }, function (response) {
                    progress(0, '', false);
                });
            }
        }
        else {
            if (keep) {
                $scope.roleusersoptions.api.getSelectedRows().forEach(function (node) {
                    console.log(node);
                    if ($scope.SEMMaster.Module != undefined) {
                        if ($scope.SEMMaster.Module[0].HDM_MAIN_MOD_ID != undefined) {
                            count++;
                        }
                    }
                    else {
                        if (node.ROL_ORDER == 1) {
                            count++;
                        }
                        else {
                            if (count == 0) {
                                keep = false;
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', 'Please Select One Value For Role HD INCHARGE');
                            }
                        }
                    }
                });

            }
            if (keep) {
                $scope.data = { TWRLST: $scope.SEMMaster.Tower, SEM: $scope.SEMMaster.Child, SEMD: $scope.roleusersoptions.api.getSelectedRows() };
                ServiceEscalationService.SaveSEMDetails($scope.data).then(function (dataobj) {
                    if (dataobj.data.sem_id == 0) {
                        setTimeout(function () {
                            $scope.$apply(function () {
                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', dataobj.Message);
                                setTimeout(function () { $scope.LoadRoleUsers(CompanySession); }, 400);
                            });
                        }, 700);
                    }
                    else {
                        $scope.LoadSemDetails($scope.SEMMaster.CNP_NAME);
                        $scope.clear();
                        setTimeout(function () {
                            $scope.$apply(function () {
                                progress(0, '', false);
                                showNotification('success', 8, 'bottom-right', dataobj.Message);
                                setTimeout(function () { $scope.LoadRoleUsers(CompanySession); }, 400);
                            });
                        }, 700);
                    }
                }, function (response) {
                    progress(0, '', false);
                });
            }
        }
    }

    //clear 
    $scope.clear = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Child, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Designation, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Module, function (value, key) {
            value.ticked = false;
        });
        $scope.SEMMaster = {};
        $scope.ActionStatus = 0;
        $scope.EnableDD = 0;
        $scope.IsInEdit = false;
        $scope.roleusersoptions.api.deselectAll();
        $scope.frmService.$submitted = false;
    }

    //// Main Category Events
    $scope.MainChangeAll = function () {
        $scope.SEMMaster.Main = $scope.Main;
        $scope.MainChanged();
    }

    $scope.MainSelectNone = function () {
        $scope.SEMMaster.Main = [];
        $scope.MainChanged();
    }

    $scope.MainChanged = function () {
        if ($scope.SEMMaster.Main.length != 0) {
            HDMUtilityService.getSubCatbyMainCategories($scope.SEMMaster.Main, 2).then(function (response) {
                if (response.data != null) {
                    $scope.Sub = response.data;
                }
                else
                    $scope.Sub = [];
            });
        }
        else
            $scope.Sub = [];
    }

    //// Sub Category Events
    $scope.SubChangeAll = function () {
        $scope.SEMMaster.Sub = $scope.Sub;
        $scope.SubChanged();
    }
    $scope.SubSelectNone = function () {
        $scope.SEMMaster.Sub = [];
        $scope.SubChanged();
    }

    $scope.SubChanged = function () {
        $scope.SEMMaster.Main = [];

        HDMUtilityService.getChildCatBySubCategories($scope.SEMMaster.Sub, 2).then(function (response) {
            if (response.data != null)
                $scope.Child = response.data;
            else
                $scope.Child = [];
        });

        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Sub, function (value, key) {
            var mn = _.find($scope.Main, { MNC_CODE: value.SUBC_MNC_CODE });
            if (mn != undefined && value.ticked == true && mn.ticked == false) {
                mn.ticked = true;
                $scope.SEMMaster.Main.push(mn);
            }
        });
    }

    ///// Child Category Events
    $scope.ChildChangeAll = function () {
        $scope.SEMMaster.Child = $scope.Child;
        $scope.ChildChanged();
    }

    $scope.ChildSelectNone = function () {
        $scope.SEMMaster.Child = [];
        $scope.ChildChanged();
    }

    $scope.ChildChanged = function () {
        $scope.SEMMaster.Main = [];
        $scope.SEMMaster.Sub = [];


        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Child, function (value, key) {
            var mn = _.find($scope.Main, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
            if (mn != undefined && value.ticked == true && mn.ticked == false) {
                mn.ticked = true;
                $scope.SEMMaster.Main.push(mn);
            }
        });

        angular.forEach($scope.Child, function (value, key) {
            var sb = _.find($scope.Sub, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sb != undefined && value.ticked == true && sb.ticked == false) {
                sb.ticked = true;
                $scope.SEMMaster.Sub.push(sb);
            }
        });

    }
    //Edit
    $scope.EditSEMDetails = function (data) {
        console.log(data);
        progress(0, 'Loading...', true);
        $scope.ActionStatus = 1;
        $scope.EnableDD = 1;
        $scope.IsInEdit = true;
        var dt = data.SEM_ID;

        $scope.SEMMaster.Country = [];

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Child, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Module, function (value, key) {
            value.ticked = false;
        });

        var Mod = _.find($scope.Module, { HDM_MAIN_MOD_ID: data.HDM_MAIN_MOD_ID });
        if (Mod != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    Mod.ticked = true;
                    $scope.SEMMaster.Module.push(Mod);
                });
            }, 550)
        }

        var cny = _.find($scope.Country, { CNY_CODE: data.SEM_CNY_CODE });
        if (cny != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    cny.ticked = true;
                    $scope.SEMMaster.Country.push(cny);
                });
            }, 100)
        }

        var cty = _.find($scope.City, { CTY_CODE: data.SEM_CTY_CODE });
        if (cty != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    cty.ticked = true;
                    $scope.SEMMaster.City.push(cty);
                });
            }, 100)
        }
        var lcm = _.find($scope.Location, { LCM_CODE: data.SEM_LOC_CODE });
        if (lcm != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    lcm.ticked = true;
                    $scope.SEMMaster.Location.push(lcm);
                });
            }, 100)
        }
        var twr = _.find($scope.Tower, { TWR_CODE: data.SEM_TWR_CODE });
        if (twr != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    twr.ticked = true;
                    $scope.SEMMaster.Tower.push(twr);
                });
            }, 100)
        }
        var main = _.find($scope.Main, { MNC_CODE: data.SEM_MNC_CODE });
        if (main != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    main.ticked = true;
                    $scope.SEMMaster.Main.push(main);
                });
            }, 100)
        }
        var sub = _.find($scope.Sub, { SUBC_CODE: data.SEM_SUBC_CODE });
        if (sub != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    sub.ticked = true;
                    $scope.SEMMaster.Sub.push(sub);
                });
            }, 100)
        }
        var child = _.find($scope.Child, { CHC_TYPE_CODE: data.SEM_CHC_CODE });
        if (child != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    child.ticked = true;
                    $scope.SEMMaster.Child.push(child);
                });
            }, 100)
        }

        ServiceEscalationService.EditSEMDetails(dt).then(function (response) {
            $scope.SEMMaster = data;
            $scope.roleusersoptions.api.deselectAll();
            for (i = 0; i < response.length; i++) {
                var user = response[i];
                $scope.roleusersoptions.api.forEachNode(function (node) {
                    if (node.data != null) {
                        if (user.AD_ID == node.data.AD_ID & user.ROL_ID == node.data.ROL_ID) {
                            console.log(user.AD_ID + '/' + user.ROL_ID + ' - ' + node.data.AD_ID + '/' + node.data.ROL_ID);
                            $scope.roleusersoptions.api.selectNode(node, true)
                        }
                    }
                });
            }
            progress(0, 'Loading...', false);
        }, function (response) {
            progress(0, '', false);
        });
    }

    ////////////////////////////////
    $scope.DownloadExcel = function () {
        $scope.dataobj = { Twrlst: $scope.SEMMaster.Tower, SEM: $scope.SEMMaster.Child };
        console.log($scope.dataobj)
        progress(0, 'Loading...', true);
        $http({
            url: UtilityService.path + '/api/ServiceEscalationMapping/DownloadTemplate',
            method: 'POST',
            responseType: 'arraybuffer',
            data: $scope.dataobj, //this is your json data string
            headers: {
                'Content-type': 'application/json',
                'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            }
        }).success(function (data) {
            if (data.byteLength > 27) {
                var blob = new Blob([data], {
                    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                });
                save(blob, 'HelpDesk_Service_Escalation_Template.xlsx');
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', "No data Found");

            }
        }).error(function () {
            progress(0, '', false);
            //Some error log
        });

        //UploadSpacesService.downloadTemplate($scope.dataobj).then(function (result) {
        //    var uriString = parseReturn(result);
        //    location.href = "uriString"

        //});
    }

    function save(blob, fileName) {
        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(blob, fileName);
            progress(0, '', false);
        } else {
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.click();
            window.URL.revokeObjectURL(link.href);
            $scope.ToggleDiv = !$scope.ToggleDiv;
            progress(0, '', false);
        }
    }

    $scope.UploadFile = function () {
        if ($('#FileUpl', $('#form2')).val()) {
            progress(0, 'Loading...', true);
            var selectedoptions = $filter('filter')($scope.UplOptions, { isChecked: true });
            var selopt = _.map(selectedoptions, 'CODE').join(",");
            var filetype = $('#FileUpl', $('#form2')).val().substring($('#FileUpl', $('#form2')).val().lastIndexOf(".") + 1);
            if (filetype == "xlsx" || filetype == "xls") {
                if (!window.FormData) {
                    redirect(); // if IE8
                }
                else {
                    var formData = new FormData();
                    var UplFile = $('#FileUpl')[0];
                    //var CurrObj = { UplOptions: selopt, UplAllocType: $scope.UploadSpaces.UplAllocType };
                    //console.log(CurrObj);
                    formData.append("UplFile", UplFile.files[0]);
                    //formData.append("CurrObj", JSON.stringify(CurrObj));
                    $.ajax({
                        url: UtilityService.path + "/api/ServiceEscalationMapping/UploadTemplate",
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {

                            if (response.data != null) {
                                $scope.$apply(function () {
                                    $scope.semdetailsOptionsEnabled = true;
                                    $scope.gridOptionsEnabled = false;
                                    $scope.genreportEnabled = false;
                                    $scope.gridOptions.api.setRowData(response.data);
                                    progress(0, '', false);
                                });
                                $scope.DispDiv = true;

                                progress(0, '', false);
                                showNotification('success', 8, 'bottom-right', response.Message);
                            }
                            else {

                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', response.Message);
                            }
                        }
                    });
                }

            }
            else
                progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', 'Please Upload only Excel File(s)');
        }
        else
            showNotification('error', 8, 'bottom-right', 'Please Select File To Upload');

    }

    $scope.semdetailscolumnDefs = [
        { headerName: "Country", field: "CNY_NAME", cellClass: "grid-align" },
        { headerName: "City", field: "CTY_NAME", cellClass: "grid-align" },
        { headerName: "Location", field: "LCM_NAME", cellClass: "grid-align" },
        { headerName: "Tower", field: "TWR_NAME", cellClass: "grid-align" },
        { headerName: "Main Category", field: "MNC_NAME", cellClass: "grid-align" },
        { headerName: "Sub Category", field: "SUBC_NAME", cellClass: "grid-align" },
        { headerName: "Child Category", field: "CHC_TYPE_NAME", cellClass: "grid-align" },
        { headerName: "Edit", template: '<a data-ng-click ="EditSEMDetails(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true, suppressSorting: true }
    ];

    $scope.semdetailsOptions = {
        columnDefs: $scope.semdetailscolumnDefs,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        onReady: function () {
            $scope.semdetailsOptions.api.sizeColumnsToFit()
        },
    };

    var columnDefs = [
        { headerName: "Country", field: "TEMP_HDMSER_COUNTRY", cellClass: "grid-align" },
        { headerName: "City", field: "TEMP_HDMSER_CITY", cellClass: "grid-align" },
        { headerName: "Location", field: "TEMP_HDMSER_LOCATION", cellClass: "grid-align" },
        { headerName: "Tower", field: "TEMP_HDMSER_TOWER", cellClass: "grid-align" },
        { headerName: "Main Category", field: "TEMP_HDMSER_MNC", cellClass: "grid-align" },
        { headerName: "Sub Category", field: "TEMP_HDMSER_SUBC", cellClass: "grid-align" },
        { headerName: "Child Category", field: "TEMP_HDMSER_CHC", cellClass: "grid-align" },
        { headerName: "HD Incharge", field: "TEMP_HDMSER_HDINCHARGE", cellClass: "grid-align" },
        { headerName: "L1 Manager", field: "TEMP_HDMSER_HDL1", cellClass: "grid-align" },
        { headerName: "L2 Manager", field: "TEMP_HDMSER_HDL2", cellClass: "grid-align" },
        { headerName: "L3 Manager", field: "TEMP_HDMSER_HDL3", cellClass: "grid-align" },
        { headerName: "L4 Manager", field: "TEMP_HDMSER_HDL4", cellClass: "grid-align" },
        { headerName: "L5 Manager", field: "TEMP_HDMSER_HDL5", cellClass: "grid-align" },
        { headerName: "L6 Manager", field: "TEMP_HDMSER_HDL6", cellClass: "grid-align" },
        { headerName: "Remarks", field: "TEMP_HDMSER_REMARKS", cellClass: "grid-align" }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableFilter: true,
        enableCellSelection: false,
        enableSorting: true,
    };

    function onReqFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "HDCustomizationReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Customized, Type) {

        progress(0, 'Loading...', true);


        $scope.GenerateFilterExcel();


    }


    $scope.LoadRoleUsers = function (id) {
        progress(0, 'Loading...', true);
        ServiceEscalationService.GetRoleUserList(id).then(function (data) {
            $scope.gridata = data;
            // $scope.roleusersoptions.api.setDatasource($scope.createNewDatasource($scope.gridata, $scope.roleuserspageSize));
            $scope.roleusersoptions.api.setRowData($scope.gridata);
            setTimeout(function () { $scope.LoadSemDetails(CompanySession) }, 1000);
            progress(0, '', false);
        }, function (response) {
            progress(0, '', false);
        });
    }


    // To display grid row data
    $scope.LoadSemDetails = function (id) {
        progress(0, 'Loading...', true);
        ServiceEscalationService.GetSEMList(id).then(function (data) {
            $scope.gridata = data;
            //$scope.semdetailsOptions.api.setDatasource($scope.createNewDatasource($scope.gridata, $scope.semdetailspageSize));
            $scope.semdetailsOptions.api.setRowData($scope.gridata);
            progress(0, '', false);
        }, function (response) {
            progress(0, '', false);
        });
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.semdetailsOptions.api.setQuickFilter(value);
    }

    setTimeout(function () { $scope.LoadRoleUsers(CompanySession); }, 400);

    $scope.PageLoad = function () {
        MainCategoryService.HelpDeskModuleHide().then(function (response) {
            var result = response.data;
            if (result == "1") {
                MainCategoryService.GetCompanyModules().then(function (response) {
                    $scope.Module = response.data.data;
                    $scope.SEMMaster.Module = $scope.Module;
                });
            } else {
                $scope.HelpDeskModule = true;
                $scope.SEMMaster.Module = "1";
            }

        });
    }

    $scope.PageLoad();
    $scope.getMainCategories = function () {
        var params = {
            CompanyId: $scope.SEMMaster.Module[0].HDM_MAIN_MOD_ID
        }
        SubCategoryService.GetMainCategoryByModule(params).then(function (response) {
            $scope.Main = response.data;
            $scope.SEMMaster.Main = $scope.Main;
            angular.forEach($scope.Sub, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.Child, function (value, key) {
                value.ticked = false;
            });
        }, function (error) {
            console.log(error);
        });

        ChildCatApprovalLocMatrixService.GetChildCategoryByModule(params).then(function (response) {
            $scope.Child = response.data;
            //$scope.ChildCatLocApp.Child = $scope.Child;
        });

        ChildCategoryService.GetSubCategoryByModule(params).then(function (response) {
            $scope.Sub = response;
        });

        ServiceEscalationService.GetRoleUserListByModule($scope.SEMMaster.Module[0].HDM_MAIN_MOD_ID).then(function (data) {
            $scope.gridata = data;
            // $scope.roleusersoptions.api.setDatasource($scope.createNewDatasource($scope.gridata, $scope.roleuserspageSize));
            $scope.roleusersoptions.api.setRowData($scope.gridata);
            setTimeout(function () { $scope.LoadSemDetails(CompanySession) }, 1000);
            progress(0, '', false);
        }, function (response) {
            progress(0, '', false);
        });

    }

}]);
