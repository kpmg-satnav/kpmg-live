﻿app.service("ChildCatApprovalLocMatrixService", function ($http, $q, UtilityService) {

    this.GetGridDetails = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ChildCatApprovalLocMatrix/GetGridDetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SaveData = function (Dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ChildCatApprovalLocMatrix/InsertDetails', Dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.UpdateData = function (Dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ChildCatApprovalLocMatrix/UpdateDetails', Dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //For Edit
    this.editData = function (obj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ChildCatApprovalLocMatrix/editData/', obj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetChildCategoryByModule = function (dataObject) {
        deferred = $q.defer();
        return $http.post('../../../api/ChildCatApprovalLocMatrix/GetChildCategoryByModule', dataObject)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.DeleteSeat = function (Id) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ChildCatApprovalLocMatrix/DeleteSeat', Id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

});

app.controller('ChildCatApprovalLocMatrixController',function ($scope, $q, ChildCatApprovalLocMatrixService, MainCategoryService, HDMUtilityService, UtilityService, ChildCatApprovalMatService, SubCategoryService, ChildCategoryService, $filter, $http, $ngConfirm) {
    $scope.ChildCatLocApp = {};
    $scope.ActionStatus = 0;
    $scope.ShowMessage = false;
    $scope.sta_id = [{ "value": "1", "Name": "Active" }, { "value": "2", "Name": "In-Active" }]

    $scope.ClearData = function () {
        $scope.ChildCatLocApp = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frmMapping.$submitted = false;

        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Child, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Designation, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Company, function (value, key) {
            value.ticked = false;
        });
        $scope.Apprlst = [];
        $scope.Numbers.Nums = '';
    }


    HDMUtilityService.getMainCategories().then(function (response) {
        if (response.data != null) {
            $scope.Main = response.data;

            HDMUtilityService.getAllSubCategories().then(function (response) {
                if (response.data != null) {
                    $scope.Sub = response.data;

                    HDMUtilityService.getAllChildCategories().then(function (response) {
                        if (response.data != null) {
                            $scope.Child = response.data;
                        }
                    });
                }
            });
        }
    });

    ChildCatApprovalMatService.GetHeldeskList().then(function (response) {
        if (response != null) {
            $scope.HDMList = response;
        }
    });

    $scope.PageLoad = function () {
        MainCategoryService.GetCompanyModules().then(function (response) {
            $scope.Company = response.data.data;
            $scope.ChildCatLocApp.Company = $scope.Company;
        });
    }

    $scope.PageLoad();

    $scope.getMainCategories = function () {
        var params = {
            CompanyId: $scope.ChildCatLocApp.Company[0].HDM_MAIN_MOD_ID
        }
        SubCategoryService.GetMainCategoryByModule(params).then(function (response) {
            $scope.Main = response.data;
            $scope.ChildCatLocApp.Main = $scope.Main;
            angular.forEach($scope.Sub, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.Child, function (value, key) {
                value.ticked = false;
            });
        }, function (error) {
            console.log(error);
        });

        ChildCatApprovalLocMatrixService.GetChildCategoryByModule(params).then(function (response) {
            $scope.Child = response.data;
            //$scope.ChildCatLocApp.Child = $scope.Child;
        });

        ChildCategoryService.GetSubCategoryByModule(params).then(function (response) {
            $scope.Sub = response;
        });

    }

    $scope.GetEmpOnRole = function (data) {
        var SelectedRole = { Role: data.ROL_ID };
        ChildCatApprovalMatService.GetEmpOnRole(SelectedRole).then(function (response) {
            if (response != null) {
                data.Emplst = response.data;
            }
        });
    }


    var columnDefs = [
        { headerName: "Child Category", field: "CHC_TYPE_NAME", width: 290, cellClass: 'grid-align' },
        { headerName: "Location", field: "LCM_NAME", width: 290, cellClass: 'grid-align' },
        //{ headerName: "Designation", field: "DSN_AMT_TITLE", width: 290, cellClass: 'grid-align' },
        { headerName: "No. of Approval Levels", field: "COUNT", width: 380, cellClass: 'grid-align' },
        { headerName: "Edit", width: 114, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true, suppressSorting: true, onmouseover: "cursor: hand (a pointing hand)" },
        { headerName: 'Delete', cellclass: "grid-align", filter: 'set', template: '<a ng-click="Delete(data)"><i class="fa fa-times" aria-hidden="true"></i></a>' }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        ChildCatApprovalLocMatrixService.GetGridDetails().then(function (response) {
            if (response.data != null) {
                $scope.gridata = response.data;
                $scope.gridOptions.api.setRowData(response.data);
                console.log(response.data);
                progress(0, '', false);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Data Found');
            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $scope.ChildCatAppMat = function () {
        progress(0, 'Loading...', true);
        if ($scope.IsInEdit) {
            $scope.Dataobj = { ChildCatlst: $scope.ChildCatLocApp.Child, Apprlst: $scope.Apprlst, HDM_NUM: $scope.Numbers.Nums, lcmlst: $scope.ChildCatLocApp.Locations };
            ChildCatApprovalLocMatrixService.UpdateData($scope.Dataobj).then(function (response) {
                var updatedobj = {};
                angular.copy($scope.ChildCatLocApp, updatedobj)
                $scope.gridata.unshift(updatedobj);
                $scope.Success = "Data Updated Successfully";
                //$scope.LoadData();
                $scope.IsInEdit = false;
                $scope.ClearData();
                setTimeout(function () {
                    $scope.$apply(function () {
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
            }, function (error) {
                console.log(error);
            })
        }
        else {
            $scope.Dataobj = { ChildCatlst: $scope.ChildCatLocApp.Child, Apprlst: $scope.Apprlst, HDM_NUM: $scope.Numbers.Nums, lcmlst: $scope.ChildCatLocApp.Locations };
            ChildCatApprovalLocMatrixService.SaveData($scope.Dataobj).then(function (response) {
                $scope.ShowMessage = true;
                ChildCatApprovalLocMatrixService.GetGridDetails().then(function (response) {
                    if (response.data != null) {
                        $scope.gridata = response.data;
                        $scope.gridOptions.api.setRowData(response.data);
                        progress(0, '', false);
                    }
                });
                //$scope.LoadData();
                showNotification('success', 8, 'bottom-right', "Data Inserted Successfully");
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                $scope.ClearData();
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
        progress(0, '', false);
    }

    setTimeout(function () {
        $scope.LoadData();
    }, 700);


    //// Main Category Events
    $scope.MainChangeAll = function () {
        $scope.ChildCatLocApp.Main = $scope.Main;
        $scope.MainChanged();
    }

    $scope.MainSelectNone = function () {
        $scope.ChildCatLocApp.Main = [];
        $scope.MainChanged();
    }

    $scope.MainChanged = function () {
        if ($scope.ChildCatLocApp.Main.length != 0) {
            HDMUtilityService.getSubCatbyMainCategories($scope.ChildCatLocApp.Main, 2).then(function (response) {
                if (response.data != null) {
                    $scope.Sub = response.data;
                }
                else
                    $scope.Sub = [];
            });
        }
        else
            $scope.Sub = [];
    }

    //// Sub Category Events
    $scope.SubChangeAll = function () {
        $scope.ChildCatLocApp.Sub = $scope.Sub;
        $scope.SubChanged();
    }
    $scope.SubSelectNone = function () {
        $scope.ChildCatLocApp.Sub = [];
        $scope.SubChanged();
    }

    $scope.SubChanged = function () {
        $scope.ChildCatLocApp.Main = [];

        HDMUtilityService.getChildCatBySubCategories($scope.ChildCatLocApp.Sub, 2).then(function (response) {
            if (response.data != null)
                $scope.Child = response.data;
            else
                $scope.Child = [];
        });

        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Sub, function (value, key) {
            var mn = _.find($scope.Main, { MNC_CODE: value.SUBC_MNC_CODE });
            if (mn != undefined && value.ticked == true && mn.ticked == false) {
                mn.ticked = true;
                $scope.ChildCatLocApp.Main.push(mn);
            }
        });
    }

    ///// Child Category Events
    $scope.ChildChangeAll = function () {
        $scope.ChildCatLocApp.Child = $scope.Child;
        $scope.ChildChanged();
    }

    $scope.ChildSelectNone = function () {
        $scope.ChildCatLocApp.Child = [];
        $scope.ChildChanged();
    }

    $scope.ChildChanged = function () {
        $scope.ChildCatLocApp.Main = [];
        $scope.ChildCatLocApp.Sub = [];


        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Child, function (value, key) {
            var mn = _.find($scope.Main, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
            if (mn != undefined && value.ticked == true && mn.ticked == false) {
                mn.ticked = true;
                $scope.ChildCatLocApp.Main.push(mn);
            }
        });

        angular.forEach($scope.Child, function (value, key) {
            var sb = _.find($scope.Sub, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sb != undefined && value.ticked == true && sb.ticked == false) {
                sb.ticked = true;
                $scope.ChildCatLocApp.Sub.push(sb);
            }
        });

        //$scope.Dataobj = { ChildCatlst: $scope.ChildCatApp.Child };
        //ChildCatApprovalMatService.CheckMappedDet($scope.Dataobj).then(function (response) {
        //    console.log(response);
        //    if (response != "Ok")
        //        showNotification('success', 8, 'bottom-right', response);
        //});

    }

    $scope.Numbers = [{ Value: '1', Key: 1 }, { Value: '2', Key: 2 }, { Value: '3', Key: 3 }, { Value: '4', Key: 4 }, { Value: '5', Key: 5 },
    { Value: '6', Key: 6 }, { Value: '7', Key: 7 }, { Value: '8', Key: 8 }, { Value: '9', Key: 9 }, { Value: '10', Key: 10 }];


    $scope.NumberChanged = function () {
        var num = $scope.Numbers.Nums;
        loadtable(num);
    }

    function loadtable(val) {
        $scope.Apprlst = [];
        var obj = {};
        for (var i = 1; i <= val; i++) {
            obj = {};
            obj.ROL_ID = "";
            obj.ROL_LEVEL = i;
            $scope.Apprlst.push(obj);
        }
    }


    $scope.Numbers.Nums = '2';
    loadtable($scope.Numbers.Nums);

    $scope.EditFunction = function (data) {
        console.log(data);
        var obj = { CHILD_CODE: data.CAM_CHC_CODE, LCM_CODE: data.LCM_CODE };
        ChildCatApprovalLocMatrixService.editData(obj).then(function (response) {
            $scope.Numbers.Nums = response.data.HDM_NUM;
            $scope.Apprlst = response.data.Apprlst;
            angular.forEach($scope.Child, function (value, key) {
                value.ticked = false;
            });

            angular.forEach($scope.Locations, function (value, key) {
                value.ticked = false;
            });
            angular.forEach(response.data.lcmlst, function (value, key) {
                var sb = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });

                if (sb != undefined && sb.ticked == false) {
                    sb.ticked = true;

                }
            });

            //angular.forEach($scope.Designation, function (value, key) {
            //    value.ticked = false;
            //});

            //angular.forEach(response.data.dsglst, function (value, key) {
            //    var sb = _.find($scope.Designation, { DSG_CODE: value.DSG_CODE });

            //    if (sb != undefined && sb.ticked == false) {
            //        sb.ticked = true;

            //    }
            //});

            angular.forEach(response.data.ChildCatlst, function (value, key) {
                var sb = _.find($scope.Child, { CHC_TYPE_CODE: value.CHC_TYPE_CODE });
                if (sb != undefined && sb.ticked == false) {
                    sb.ticked = true;
                    $scope.ChildCatLocApp.Child.push(sb);
                }
            });
            angular.forEach($scope.Sub, function (value, key) {
                value.ticked = false;
            });
            angular.forEach(response.data.ChildCatlst, function (value, key) {
                var cc = _.find($scope.Sub, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
                if (cc != undefined && cc.ticked == false) {
                    cc.ticked = true;
                    $scope.ChildCatLocApp.Sub.push(cc);
                }
            });
            angular.forEach($scope.Main, function (value, key) {
                value.ticked = false;
            });

            angular.forEach(response.data.ChildCatlst, function (value, key) {
                var mm = _.find($scope.Main, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
                if (mm != undefined && mm.ticked == false) {
                    mm.ticked = true;
                    $scope.ChildCatLocApp.Main.push(mm);
                }
            });

            angular.forEach($scope.Company, function (val) {
                if (val.HDM_MAIN_MOD_NAME == response.data.HDM_MAIN_MOD_NAME) {
                    val.ticked = true;
                }
                else { val.ticked = false; }

            });


        });

        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;

    }

    $scope.column = {};




    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Locations = response.data;

        }
    });

    //UtilityService.getDesignation().then(function (response) {
    //    if (response.data != null) {
    //        $scope.Designation = response.data;
    //    }
    //});

    $scope.locSelectAll = function () {

    }

    $scope.LocationChange = function () {


    }

    $scope.LocationSelectNone = function () {
        $scope.Locationlst = [];

    }

    $scope.DesignationSelectAll = function () {

    }

    $scope.DesignationChange = function () {


    }

    $scope.DesignationSelectNone = function () {
        $scope.Designation = [];

    }


    $scope.Delete = function (data) {
        console.log(data);
        var encoding = encodeURIComponent(data.CAM_ID);
        //var Id = encoding.replace(/%2F/i, "_");
        var Id = encoding.split(/%2F/i).join("__");
        var obj = { CAM_ID: Id };
        console.log(obj);
        progress(0, 'Loading...', true);
        $ngConfirm({
            icon: 'fa fa-warning',
            columnClass: 'col-md-6 col-md-offset-3',
            title: 'Confirm!',
            content: 'Are you sure want to Inactive / Delete  - ' + '(' + data.CHC_TYPE_NAME + ') ',   //+ '(' + data.CHC_TYPE_NAME + ') ' + data.AUR_KNOWN_AS
            closeIcon: true,
            closeIconClass: 'fa fa-close',
            type: 'red',
            autoClose: 'cancel|15000',
            buttons: {
                Confirm: {
                    text: 'Delete',
                    btnClass: 'btn-red',
                    action: function (button) {
                        ChildCatApprovalLocMatrixService.DeleteSeat(obj).then(function (response) {
                            if (response != null) {
                                $ngConfirm({
                                    icon: 'fa fa-success',
                                    title: 'Status',
                                    content: 'Employee Deleted Successfully',
                                    closeIcon: true,
                                    closeIconClass: 'fa fa-close'
                                });
                            }
                            setTimeout(function () {
                                $scope.LoadData();
                            }, 800);
                        });
                    }
                },
                cancel: function () {
                    $ngConfirm({
                        title: 'Status!',
                        content: 'Cancelled',
                    });
                }
            }
        });
        progress(0, 'Loading...', false);
    }

});