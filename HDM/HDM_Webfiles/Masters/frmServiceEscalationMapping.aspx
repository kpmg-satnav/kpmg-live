﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />      
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/colorpicker.min.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>
</head>

<body data-ng-controller="ServiceController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Service Escalation Mapping Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Service Escalation Mapping Master</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form role="form" id="form2" name="frmService" novalidate>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label id="Label1" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">

                            <div class="col-md-3 col-sm-6 col-xs-12" ng-hide="HelpDeskModule">
                                <div class="form-group">
                                    <%--data-ng-class="{'has-error': frmService.$submitted && frmService.HDM_MAIN_MOD_NAME.$invalid}"--%>
                                    <label class="control-label">Module <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Module" data-output-model="SEMMaster.Module" data-button-label="icon HDM_MAIN_MOD_NAME"
                                        data-on-item-click="getMainCategories()" data-item-label="icon HDM_MAIN_MOD_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="SEMMaster.Company[0]" name="HDM_MAIN_MOD_NAME" style="display: none" required="" />
                                    <%--<span class="error" data-ng-show="frmService.$submitted && frmService.HDM_MAIN_MOD_NAME.$invalid" style="color: red;">Please Select Module</span>--%>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmService.$submitted && frmService.CNY_NAME.$invalid}">
                                    <label class="control-label">Country <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Country" data-output-model="SEMMaster.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                        data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableDD==1">
                                    </div>
                                    <input type="text" data-ng-model="SEMMaster.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmService.$submitted && frmService.CNY_NAME.$invalid" style="color: red;">Please select country </span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmService.$submitted && frmService.CTY_NAME.$invalid}">
                                    <label class="control-label">City <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="City" data-output-model="SEMMaster.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                        data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableDD==1">
                                    </div>
                                    <input type="text" data-ng-model="SEMMaster.City[0]" name="CTY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmService.$submitted && frmService.CTY_NAME.$invalid" style="color: red;">Please select city </span>

                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmService.$submitted && frmService.LCM_NAME.$invalid}">
                                    <label class="control-label">Location <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Location" data-output-model="SEMMaster.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                        data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableDD==1">
                                    </div>
                                    <input type="text" data-ng-model="SEMMaster.Location[0]" name="LCM_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmService.$submitted && frmService.LCM_NAME.$invalid" style="color: red;">Please select location </span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmService.$submitted && frmService.TWR_NAME.$invalid}">
                                    <label class="control-label">Tower <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Tower" data-output-model="SEMMaster.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                        data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableDD==1">
                                    </div>
                                    <input type="text" data-ng-model="SEMMaster.Tower[0]" name="TWR_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmService.$submitted && frmService.TWR_NAME.$invalid" style="color: red;">Please select tower </span>
                                </div>
                            </div>

                        </div>

                        <div class="clearfix">

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmService.$submitted && frmService.MNC_NAME.$invalid}">
                                    <label class="control-label">Main Category <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Main" data-output-model="SEMMaster.Main" data-button-label="icon MNC_NAME" data-item-label="icon MNC_NAME"
                                        data-on-item-click="MainChanged()" data-on-select-all="MainChangeAll()" data-on-select-none="MainSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableDD==1">
                                    </div>
                                    <input type="text" data-ng-model="SEMMaster.Main[0]" name="MNC_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmService.$submitted && frmService.MNC_NAME.$invalid" style="color: red">Please select Main Category </span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmService.$submitted && frmService.SUBC_NAME.$invalid}">
                                    <label class="control-label">Sub Category <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Sub" data-output-model="SEMMaster.Sub" data-button-label="icon SUBC_NAME" data-item-label="icon SUBC_NAME"
                                        data-on-item-click="SubChanged()" data-on-select-all="SubChangeAll()" data-on-select-none="SubSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableDD==1">
                                    </div>
                                    <input type="text" data-ng-model="SEMMaster.Sub[0]" name="SUBC_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmService.$submitted && frmService.SUBC_NAME.$invalid" style="color: red">Please select Sub Category </span>

                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmService.$submitted && frmService.CHC_TYPE_NAME.$invalid}">
                                    <label class="control-label">Child Category <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Child" data-output-model="SEMMaster.Child" data-button-label="icon CHC_TYPE_NAME" data-item-label="icon CHC_TYPE_NAME"
                                        data-on-item-click="ChildChanged()" data-on-select-all="ChildChangeAll()" data-on-select-none="ChildSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableDD==1">
                                    </div>
                                    <input type="text" data-ng-model="SEMMaster.Child[0]" name="CHC_TYPE_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmService.$submitted && frmService.CHC_TYPE_NAME.$invalid" style="color: red">Please select Child Category </span>
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-6 col-xs-12" data-ng-hide="EnableStatus==0">
                                <div class="form-group" data-ng-class="{'has-error': frmService.$submitted && frmService.CNP_NAME.$invalid}">
                                    <label class="control-label">Company</label>
                                    <div data-ng-class="{'has-error': frmService.$submitted && frmService.CNP_NAME.$invalid}">
                                        <select id="ddlCompany" name="CNP_NAME" class="form-control" data-ng-model="SEMMaster.CNP_NAME" convert-to-number
                                            required data-live-search="true" ng-change="LoadRoleUsers(SEMMaster.CNP_NAME)" data-ng-disabled="IsInEdit" data-is-disabled="EnableDD==1">
                                            <option value="0">--Select-- </option>
                                            <option data-ng-repeat="cmpy in Company" value="{{cmpy.CNP_ID}}">{{cmpy.CNP_NAME}}</option>
                                        </select>
                                        <span class="error" data-ng-show="frmService.$submitted && frmService.CNP_NAME.$invalid" style="color: red">Please Select Company </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div style="height: 250px; width: 200px;">
                            <div data-ag-grid="roleusersoptions" style="height: 250px;" class="ag-blue"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right" style="padding-top: 5px">
                                <input type="submit" value="Submit" id="btnSubmit" class="btn btn-primary custom-button-color" data-ng-click="SaveDetails()" data-ng-show="ActionStatus==0" />
                                <input type="submit" value="Modify" id="btnmodify" class="btn btn-primary custom-button-color" data-ng-click="SaveDetails()" data-ng-show="ActionStatus==1" />
                                <input id="btnClear" type="button" class='btn btn-primary custom-button-color' value="Clear" data-ng-click="clear()" />
                                <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 25px">
                            <div class="col-md-12 text-right" style="padding-top: 5px">

                                <input type="file" name="UPLFILE" id="FileUpl" required="" accept=".xls,.xlsx" class="custom-file-input" style="padding-right: 100px; margin-left: 543px;">
                                <span class="custom-file-control"></span>
                                <input type="submit" id="btnsubmit" value="Download Template" class="btn btn-primary custom-button-color" data-ng-click="DownloadExcel()" style="margin-top: -43px;" />
                                <input type="button" id="btnUpload" class="btn btn-primary custom-button-color" value="Upload Excel" data-ng-click="UploadFile()" style="margin-top: -43px;" />
                                <div class="box-footer text-right" id="table2">
                                    <a data-ng-click="GenReport('xls')"><i data-ng-hide="genreportEnabled" id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                </div>
                            </div>
                        </div>

                        <div style="height: 250px" data-ng-hide="semdetailsOptionsEnabled">
                            <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="semdetailsOptions" style="height: 100%;" class="ag-blue"></div>
                        </div>
                        <div style="height: 250px" data-ng-hide="gridOptionsEnabled">
                            <input type="text" class="selectpicker" id="filtertxt1" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 100%;" class="ag-blue"></div>
                        </div>
                        <div></div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../..<%--/../SMViews/Utility.js"></script>
    <script src="../js/ServiceEscalat--%>ionMapping.js"
        defer></script>
    <%--   <script src="../../../HelpdeskManagement/JS/MainCategoryMaster.js"></script>
    <script src="../../../HelpdeskManagement/JS/SubCategoryMaster.js"></script>
    <script src="../../../HelpdeskManagement/JS/ChildCategoryMaster.js"></script>
    <script src="../js/ChildCatApprovalLocMatrix.js"></script>
    <script src="../js/ChildCatApprovalMat.js"></script>
    <script src="../js/HDMUtility.js"></script>--%>


    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../js/HDMUtility.min.js" defer></script>
    <script src="../../../HelpdeskManagement/JS/MainCategoryMaster.min.js" defer></script>
    <script src="../../../HelpdeskManagement/JS/SubCategoryMaster.min.js" defer></script>
    <script src="../../../HelpdeskManagement/JS/ChildCategoryMaster.min.js" defer></script>
    <script src="../js/ChildcatApprovalLocMatrix.min.js" defer></script>
    <script src="../js/ChildCatApprovalMat.min.js" defer></script>
    <script src="../js/ServiceEscalationMapping.min.js" defer></script>

</body>
</html>



