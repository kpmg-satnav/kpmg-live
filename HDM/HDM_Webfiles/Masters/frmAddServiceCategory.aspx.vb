﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Imports System.Configuration.ConfigurationManager
Imports System.Web.Services
Imports System.Collections.Generic
Imports System.Data.OleDb
Imports System.IO
Partial Class HDM_HDM_Webfiles_Masters_frmAddServiceCategory
    Inherits System.Web.UI.Page

    Protected Sub rbActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
        'Dim iLoop As Integer
        'For iLoop = 0 To rbActions.Items.Count - 1
        '    If rbActions.Items(iLoop).Selected = True Then
        '        If rbActions.Items(iLoop).Text = "Add" Then
        If rbActions.Checked = True Then
            ddlSerCat.Visible = False
            trLName.Visible = False
            lblSerCatSelect.Visible = False
            cleardata()
            btnSubmit.Text = "Add"
            lblMsg.Visible = False
            txtSerCatCode.Enabled = True
            fillgrid()
            ddlLocation.Enabled = True
        Else
            getlocation()
            getservicecategory()
            trLName.Visible = True
            ddlSerCat.Visible = True
            ddlLocation.Enabled = False
            lblSerCatSelect.Visible = True
            cleardata()
            btnSubmit.Text = "Modify"
            lblMsg.Visible = False
            txtSerCatCode.Enabled = False
            fillgrid()
        End If

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            getlocation()
            btnSubmit.Text = "Add"
            fillgrid()
            trLName.Visible = False
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If Page.IsValid Then
            If btnSubmit.Text = "Add" Then
                Dim ValidateCode As Integer
                ValidateCode = ValidateCategory(txtSerCatCode.Text, ddlLocation.SelectedItem.Value)
                If ValidateCode = 0 Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Service Category already exist please enter another Service Category..."
                ElseIf ValidateCode = 1 Then
                    insertnewrecord()
                End If
            Else

                If ddlSerCat.SelectedIndex <> 0 Then
                    modifydata()
                    cleardata()
                    ddlSerCat.SelectedIndex = 0
                End If
                'getlocation()
            End If
            ' Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=1")
            ' insertnewrecord()
            'cleardata()
        End If
    End Sub

    Public Function ValidateCategory(ByVal sercatcode As String, lcm_code As String) As Integer
        Dim ValidateCode As Integer
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_VALIDATE_SERCATLOCATIONCODE")
            sp1.Command.AddParameter("@SER_CODE", sercatcode, DbType.String)
            sp1.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
            ValidateCode = sp1.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Return ValidateCode
    End Function

    Public Sub cleardata()
        ddlLocation.SelectedIndex = 0
        txtSerCatCode.Text = ""
        txtSerCatName.Text = ""
        ddlStatus.SelectedIndex = 0
        txtRemarks.Text = ""
    End Sub

    Public Sub getlocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Location_GetAll")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, "--Select--")
    End Sub

    Public Sub insertnewrecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_INSERT_SERCATEGORYLOCATION")
            '@VT_CODE,@VT_TYPE,@VT_STATUS,@VT_CREATED_BY,@VT_CREATED_DT,@VT_REM
            sp1.Command.AddParameter("@SER_LCM_ID", ddlLocation.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SER_CODE", txtSerCatCode.Text, DbType.String)
            sp1.Command.AddParameter("@SER_NAME", txtSerCatName.Text, DbType.String)
            sp1.Command.AddParameter("@SER_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@SER_UPT_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@SER_REM", txtRemarks.Text, DbType.String)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "New Service Category Added Successfully..."
            cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GET_ALLSERCATLOCATION")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvLocation.DataSource = ds
        gvLocation.DataBind()
        For i As Integer = 0 To gvLocation.Rows.Count - 1
            Dim lblstatus As Label = CType(gvLocation.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "InActive"
            End If
        Next
    End Sub

    Public Sub getservicecategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_GETALLSERCAT")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlSerCat.DataSource = sp.GetDataSet()
        ddlSerCat.DataTextField = "SER_NAME"
        ddlSerCat.DataValueField = "SER_ID"
        ddlSerCat.DataBind()
        ddlSerCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlSerCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSerCat.SelectedIndexChanged
        Try
            If ddlSerCat.SelectedIndex <> 0 Then
                lblMsg.Visible = False
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_GETSERCATELOCATIONDATAMODIFY")
                sp.Command.AddParameter("@SER_ID", ddlSerCat.SelectedItem.Value, DbType.Int32)
                Dim ds As New DataSet
                ds = sp.GetDataSet
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlLocation.ClearSelection()
                    ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_LCM_ID")).Selected = True
                    txtSerCatCode.Text = ds.Tables(0).Rows(0).Item("SER_CODE")
                    txtSerCatName.Text = ds.Tables(0).Rows(0).Item("SER_NAME")
                    txtRemarks.Text = ds.Tables(0).Rows(0).Item("SER_REM")
                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_STA_ID")).Selected = True
                End If
            Else
                cleardata()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub modifydata()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_UPDATE_SERCATLOCATION")
            sp1.Command.AddParameter("@SER_ID", ddlSerCat.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SER_NAME", txtSerCatName.Text, DbType.String)
            sp1.Command.AddParameter("@SER_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@SER_UPT_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@SER_REM", txtRemarks.Text, DbType.String)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "Service Category Modified Successfully..."

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnbrowse_Click(sender As Object, e As EventArgs) Handles btnbrowse.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                'Connection String to Excel Workbook
                'If strFileType.Trim() = ".xls" Then
                '    connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                'ElseIf strFileType.Trim() = ".xlsx" Then
                '    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                ' msheet = s(count - 1)
                msheet = listSheet(0).ToString()
                mfilename = msheet
                ' MessageBox.Show(s(count - 1))
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                    'str = "Select * from [sheet1$]"
                End If
                Dim snocnt As Integer = 1
                Dim locationid_cnt As Integer = 1
                Dim category_codecnt As Integer = 1
                Dim category_namecnt As Integer = 1
                Dim status_cnt As Integer = 1
                Dim remarks_cnt As Integer = 1
                'Dim dmn_idcnt As Integer = 1

                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If LCase(ds.Tables(0).Columns(0).ToString) <> "sno" Then
                            snocnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be SNO"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "location_id" Then
                            locationid_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be LOCATION_ID"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(2).ToString) <> "category_code" Then
                            category_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be CATEGORY_CODE"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(3).ToString) <> "category_name" Then
                            category_namecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be CATEGORY_NAME"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "status" Then
                            status_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be STATUS"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(5).ToString) <> "remarks" Then
                            remarks_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be REMARKS"
                            Exit Sub
                            'ElseIf LCase(ds.Tables(0).Columns(6).ToString) <> "domain_head_id" Then
                            '    dmn_idcnt = 0
                            '    lblMsg.Visible = True
                            '    lblMsg.Text = "Column name should be DOMAIN_HEAD_ID"
                            '    Exit Sub
                        End If
                    Next
                Next

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1

                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "location_id" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Location Code is null or empty in uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "category_code" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Category Code is null or empty in uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "category_name" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Category Name is null or empty in uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                        If Trim(LCase(ds.Tables(0).Columns(j).ToString)) = "status" Then
                            If IsDBNull(ds.Tables(0).Rows(i).Item(j)) = True Then

                                lblMsg.Text = "Status is null or empty in uploaded excel"
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                    Next
                Next

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'Start inseting excel data
                    Dim ValidateData As Integer
                    ValidateData = ValidateCategory(ds.Tables(0).Rows(i).Item("CATEGORY_CODE").ToString, ds.Tables(0).Rows(i).Item("LOCATION_ID").ToString)
                    If ValidateData = 0 Then
                        'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_UPDATE_SERCATLOCATION")
                        'sp1.Command.AddParameter("@SER_ID", ddlSerCat.SelectedItem.Value, DbType.String)
                        'sp1.Command.AddParameter("@SER_NAME", ds.Tables(0).Rows(i).Item("CATEGORY_NAME").ToString, DbType.String)
                        'sp1.Command.AddParameter("@SER_STA_ID", ds.Tables(0).Rows(i).Item("STATUS").ToString, DbType.Int32)
                        'sp1.Command.AddParameter("@SER_UPT_BY", Session("Uid"), DbType.String)
                        'sp1.Command.AddParameter("@SER_REM", ds.Tables(0).Rows(i).Item("REMARKS").ToString, DbType.String)
                        'sp1.ExecuteScalar()
                    ElseIf ValidateData = 1 Then
                        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_INSERT_SERCATEGORYLOCATION")
                        sp1.Command.AddParameter("@SER_LCM_ID", ds.Tables(0).Rows(i).Item("LOCATION_ID").ToString, DbType.String)
                        sp1.Command.AddParameter("@SER_CODE", ds.Tables(0).Rows(i).Item("CATEGORY_CODE").ToString, DbType.String)
                        sp1.Command.AddParameter("@SER_NAME", ds.Tables(0).Rows(i).Item("CATEGORY_NAME").ToString, DbType.String)
                        sp1.Command.AddParameter("@SER_STA_ID", ds.Tables(0).Rows(i).Item("STATUS").ToString, DbType.Int32)
                        sp1.Command.AddParameter("@SER_UPT_BY", Session("Uid"), DbType.String)
                        sp1.Command.AddParameter("@SER_REM", ds.Tables(0).Rows(i).Item("REMARKS").ToString, DbType.String)
                        sp1.ExecuteScalar()
                    End If
                Next
                lblMsg.Visible = True
                lblMsg.Text = "Data uploaded successfully..."
                fillgrid()
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Please choose file..."
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvLocation_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvLocation.PageIndexChanging
        gvLocation.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        lblMsg.Text = ""
    End Sub

End Class
