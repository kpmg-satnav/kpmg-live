﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/HDM/HDM_Webfiles/Masters/frmHolidayMaster.cs" Inherits="HDM_HDM_Webfiles_Masters_frmHolidayMaster" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'>   
    <![endif]-->

    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="HolidayController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Holiday Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Holiday Master</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">

                    <div class="clearfix">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <a href="../../../Masters/Mas_Webfiles/HolidayMaster.xlsx">Click here to download template</a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmHolidayUpload.$submitted && frmHolidayUpload.UPLFILE.$invalid}">
                                <label for="txtcode">Upload File <span style="color: red;">*</span></label>
                                <input type="file" name="UPLFILE" id="FileUpl" required="" accept=".xls,.xlsx" />
                                <input type="hidden" value="{{login}}" name="login" />
                            </div>
                        </div>
                        <div class="box-footer text-right">
                            <input type="button" id="btnUpload" data-ng-click="UploadFile()" class="btn btn-primary custom-button-color" value="Upload Excel" />
                        </div>
                    </div>
                    <br />
                    <form role="form" id="form1" name="frm" data-valid-submit="Save()" novalidate>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Holiday Name<span style="color: red;">*</span></label>
                                    <div data-ng-class="{'has-error': frm.$submitted && frm.HOL_REASON.$invalid}" onmouseover="Tip('Enter Holiday Name')" onmouseout="UnTip()">
                                        <input id="txtcode" name="HOL_REASON" type="text" data-ng-model="Holiday.HOL_REASON" data-ng-pattern="RegExpName.REGEXP" maxlength="40" class="form-control" autofocus required="" />
                                        <span class="error" data-ng-show="frm.$submitted && frm.HOL_REASON.$invalid" style="color: red" data-ng-bind="RegExpName.REGMSG"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Holiday Date<span style="color: red;">*</span></label>
                                    <div class="input-group date" id='fromdate' data-ng-class="{'has-error': frm.$submitted && frm.HOL_DATE.$invalid}">
                                        <div class="input-group-addon">
                                            <i class="btn btn-mini dropdown-toggle" onclick="setup('fromdate')"></i>
                                        </div>
                                        <div onmouseover="Tip('Enter Holiday date')"
                                            onmouseout="UnTip()">
                                            <input class="form-control input-sm" type="text" id="Text2" name="HOL_DATE" data-ng-model="Holiday.HOL_DATE" data-ng-pattern="RegExpDate.REGEXP" required="" data-ng-disabled="Date==0" placeholder="mm/dd/yyyy" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask>
                                        </div>
                                    </div>
                                    <span class="error" data-ng-show="frm.$submitted && frm.HOL_DATE.$invalid" style="color: red" data-ng-bind="RegExpDate.REGMSG"></span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CTY_NAME.$invalid}">
                                    <label for="txtcode">City <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="City" data-output-model="Holiday.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                        data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Holiday.City[0]" name="CTY_NAME" required="" style="display: none" />
                                    <span class="error" data-ng-show="frm.$submitted && frm.CTY_NAME.$invalid" style="color: red">Please select City</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.LCM_NAME.$invalid}">
                                    <label for="txtcode">Location <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Location" data-output-model="Holiday.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                        data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Holiday.Location[0]" name="LCM_NAME" required="" style="display: none" />
                                    <span class="error" data-ng-show="frm.$submitted && frm.LCM_NAME.$invalid" style="color: red">Please select City</span>
                                </div>
                            </div>

                            <%--<div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>City<span style="color: red;">*</span></label>
                                            <div data-ng-class="{'has-error': frm.$submitted && frm.HOL_CITY_CODE.$invalid}">
                                                <select id="ddlMain" name="HOL_CITY_CODE" class="form-control" data-ng-model="Holiday.HOL_CITY_CODE" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" required data-ng-readonly="ActionStatus==1" ng-change="onchange(Holiday.HOL_CITY_COD,Holiday)">
                                                    <option id="ddlsub" value=''>--Select--</option>
                                                    <option data-ng-repeat="city in CityList" value="{{city.CTY_CODE}}">{{city.CTY_NAME}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frm.$submitted && frm.HOL_CITY_CODE.$invalid" style="color: red">Please select City </span>
                                            </div>
                                        </div>
                                    </div>--%>
                            <%--<div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Location<span style="color: red;">*</span></label>
                                            <div data-ng-class="{'has-error': frm.$submitted && frm.HOL_LCM_CODE.$invalid}">
                                                <select id="ddlloc" name="HOL_LCM_CODE" class="form-control" data-ng-model="Holiday.HOL_LCM_CODE" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" required data-ng-readonly="ActionStatus==1">
                                                    <option id="ddladd" value=''>--Select--</option>
                                                    <option data-ng-repeat="Location in locationList" value="{{Location.LCM_CODE}}">{{Location.LCM_NAME}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frm.$submitted && frm.HOL_LCM_CODE.$invalid" style="color: red">Please select Location </span>
                                            </div>
                                        </div>
                                    </div>--%>
                            <div class="col-md-3 col-sm-12 col-xs-12">

                                <div class="form-group" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frm.$submitted && frm.HOL_STA_ID.$invalid}">
                                    <label>Status<span style="color: red;">*</span></label>

                                    <select id="ddlsta" name="Status" data-ng-model="Holiday.HOL_STA_ID" class="form-control">
                                        <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                    <span class="error" data-ng-show="frm.$submitted && frm.HOL_STA_ID.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please select status </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <div data-ng-class="{'has-error': frm.$submitted && frm.HOL_REM.$invalid}">
                                        <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                            <textarea id="txtremarks" data-ng-model="Holiday.HOL_REM" name="HOL_REM" data-ng-pattern="RegExpRemarks.REGEXP" class="form-control" maxlength="500"></textarea>
                                        </div>
                                    </div>
                                    <span class="error" data-ng-show="frm.$submitted && frm.HOL_REM.$invalid" style="color: red" data-ng-bind="RegExpRemarks.REGMSG"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                    <input type="reset" value="Clear" class='btn btn-primary custom-button-color' data-ng-click="EraseData()" />
                                    <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 329px; width: 100%"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
        app.service("HolidayService", function ($http, $q) {
            var deferred = $q.defer();
            this.getHoliday = function () {
                return $http.get('../../../api/Holiday')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

            this.CreateHoliday = function (dataobject) {
                deferred = $q.defer();
                return $http.post('../../../api/Holiday/Create', dataobject)
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

            this.UpdateHoliday = function (upd) {
                deferred = $q.defer();
                return $http.post('../../../api/Holiday/UpdateHolidays', upd)
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };
            this.GetGriddata = function () {
                deferred = $q.defer();
                return $http.get('../../../api/Holiday/GetGridData')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

            this.GetCities = function () {
                deferred = $q.defer();
                return $http.get('../../../api/Holiday/GetCities')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            }
            this.Getlocations = function () {
                deferred = $q.defer();
                return $http.get('../../../api/Holiday/Getlocations')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            }
            this.GetLocationsByCity = function (Holiday) {
                deferred = $q.defer();
                return $http.get('../../../api/Holiday/GetLocations', Holiday)
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            }


        });
        app.controller('HolidayController', function ($scope, $q, HolidayService, $http, $timeout, $filter, UtilityService) {
            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.Holiday = {};
            $scope.categorydata = [];
            $scope.City = [];
            $scope.Location = [];
            $scope.ActionStatus = 0;
            $scope.Date = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;
            $scope.RegExpName = UtilityService.RegExpName;
            $scope.RegExpDate = UtilityService.RegExpDate;
            $scope.RegExpRemarks = UtilityService.RegExpRemarks;
            $scope.hol_id = [];


            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.City = response.data;

                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Location = response.data;

                        }
                    });
                }
            });
            $scope.CtyChangeAll = function () {
                $scope.Holiday.City = $scope.City;
                $scope.CtyChanged();
            }
            $scope.CtySelectNone = function () {
                $scope.Holiday.City = [];
                $scope.CtyChanged();
            }

            $scope.CtyChanged = function () {

                UtilityService.getLocationsByCity($scope.Holiday.City, 1).then(function (response) {
                    if (response.data != null)
                        $scope.Location = response.data;
                    else
                        $scope.Location = [];
                });


            }

            $scope.LcmChangedAll = function () {
                $scope.Holiday.Location = $scope.Location;

            }
            $scope.lcmSelectNone = function () {
                $scope.Holiday.Location = [];

            }

            $scope.Save = function () {
                if ($scope.IsInEdit) {
                    console.log($scope.Holiday);
                    HolidayService.UpdateHoliday($scope.Holiday).then(function (category) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Updated Successfully";
                        var savedobj = {};
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        $scope.IsInEdit = false;
                        setTimeout(function () {
                            $scope.$apply(function () {
                                showNotification('success', 8, 'bottom-right', $scope.Success);
                            });
                        }, 700);
                        $scope.LoadData();
                        $scope.Holiday = {};
                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.data = $scope.Holiday;
                    console.log($scope.data)
                    $scope.Holiday.HOL_STA_ID = "1";
                    HolidayService.CreateHoliday($scope.data).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Inserted Successfully";
                        $scope.LoadData();
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                        $scope.Holiday = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;

                        setTimeout(function () {
                            $scope.$apply(function () {

                                showNotification('error', 8, 'bottom-right', $scope.Success);
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                        console.log(error);
                    });
                }
            }
            var columnDefs = [
                //{ headerName: "HOLID", field: "HOL_ID", width: 200, cellClass: 'grid-align',visible:false},
                { headerName: "Holiday", field: "HOL_REASON", width: 200, cellClass: 'grid-align' },
                { headerName: "Holiday Date", template: '<span>{{data.HOL_DATE | date:"dd MMM, yyyy"}}</span>', width: 150, cellClass: 'grid-align' },
                { headerName: "City", field: "CITY_NAME", width: 180, cellClass: 'grid-align' },
                { headerName: "Location", field: "LCM_NAME", width: 180, cellClass: 'grid-align' },
                { headerName: "Status", suppressMenu: true, template: "{{ShowStatus(data.HOL_STA_ID)}}", width: 100, cellClass: 'grid-align' },
                { headerName: "Edit", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', suppressMenu: true, width: 100 }];

            $scope.LoadData = function () {
                progress(0, 'Loading...', true);
                HolidayService.GetGriddata().then(function (gddata) {
                    $scope.gridata = gddata;
                    // $scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(gddata);
                    progress(0, '', false);
                }, function (error) {
                    console.log(error);
                });
            }

            //$scope.pageSize = '10';

            $scope.createNewDatasource = function () {
                var dataSource = {
                    pageSize: parseInt($scope.pageSize),
                    getRows: function (params) {
                        setTimeout(function () {
                            var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                            var lastRow = -1;
                            if ($scope.gridata.length <= params.endRow) {
                                lastRow = $scope.gridata.length;
                            }
                            params.successCallback(rowsThisPage, lastRow);
                        }, 500);
                    }
                };
                $scope.gridOptions.api.setDatasource(dataSource);
            }

            $scope.gridOptions = {
                columnDefs: columnDefs,
                enableCellSelection: false,
                rowData: null,
                enableSorting: true,
                enableFilter: true,
                angularCompileRows: true,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }
            };
            $timeout($scope.LoadData, 1000);

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })

            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
            $scope.EditData = function (data) {
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;
                angular.copy(data, $scope.Holiday);
                $scope.Holiday.HOL_PREVDATE = $filter('date')($scope.Holiday.HOL_DATE, 'MM/dd/yyyy');
                $scope.Holiday.HOL_DATE = $filter('date')($scope.Holiday.HOL_DATE, 'MM/dd/yyyy');
            }
            //$scope.EditData = function (data) {
            //    $scope.ActionStatus = 1;
            //    $scope.IsInEdit = true;
            //    angular.copy(data, $scope.Holiday);
            //    $scope.Holiday.HOL_PREVDATE = $filter('date')($scope.Holiday.HOL_DATE, 'MM/dd/yyyy');
            //    $scope.Holiday.HOL_DATE = $filter('date')($scope.Holiday.HOL_DATE, 'MM/dd/yyyy');

            //    $scope.hol_id.push(data.HOL_ID);
            //    $scope.Holiday.HOL_ID= $scope.hol_id.toString();
            //    console.log($scope.Holiday.HOL_ID);
            //    var cty = _.find($scope.City, { CTY_CODE: data.CITY_CODE });
            //    if (cty != undefined) {
            //        setTimeout(function () {
            //            $scope.$apply(function () {
            //                cty.ticked = true;
            //                $scope.Holiday.City.push(cty);
            //            });
            //        }, 100)
            //    }
            //    var lcm = _.find($scope.Location, { LCM_CODE: data.LCM_CODE });
            //    if (lcm != undefined) {
            //        setTimeout(function () {
            //            $scope.$apply(function () {
            //                lcm.ticked = true;
            //                $scope.Holiday.Location.push(lcm);
            //            });
            //        }, 100)
            //    }
            //}

            $scope.EraseData = function () {
                $("#FileUpl").val("");
                $scope.Holiday = {};
                $scope.ActionStatus = 0;
                $scope.City = [];
                $scope.Location = [];
                $scope.IsInEdit = false;
                $scope.frm.$submitted = false;
            }

            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }

            //get city cat name to display in grid after submit
            $scope.GetCityName = function (ctyCode) {
                for (var i = 0; $scope.CityList != null && i < $scope.CityList.length; i += 1) {
                    var result = $scope.CityList[i];
                    if (result.CTY_CODE === ctyCode) {
                        return result.CTY_NAME;
                    }
                }
            }
            $scope.LcmChanged = function () {
                $scope.Holiday.City = [];

                angular.forEach($scope.City, function (value, key) {
                    value.ticked = false;
                });

                angular.forEach($scope.Location, function (value, key) {
                    var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
                    if (cty != undefined && value.ticked == true && cty.ticked == false) {
                        cty.ticked = true;
                        $scope.Holiday.City.push(cty);
                    }
                });

            }
            $scope.LcmChangeAll = function () {
                $scope.Holiday.Location = $scope.Location;
                $scope.LcmChanged();
            }

            $scope.LcmSelectNone = function () {
                $scope.Holiday.Location = [];
                $scope.LcmChanged();
            }

            $scope.UploadFile = function () {

                if ($('#FileUpl').val()) {
                    progress(0, 'Loading...', true);
                    var filetype = $('#FileUpl').val().substring($('#FileUpl').val().lastIndexOf(".") + 1);
                    if (filetype == "xlsx" || filetype == "xls") {
                        if (!window.FormData) {
                            redirect(); // if IE8
                        }
                        else {
                            var formData = new FormData();
                            var UplFile = $('#FileUpl')[0];
                            formData.append("UplFile", UplFile.files[0]);
                            $.ajax({
                                url: "../../../api/Holiday/UploadTemplate",
                                type: "POST",
                                data: formData,
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function (response) {
                                    console.log(response);

                                    if (response.data != null) {
                                        $scope.$apply(function () {
                                            console.log(response);
                                            $scope.gridOptions.api.setRowData(response.data);
                                            progress(0, '', false);
                                        });
                                        $scope.DispDiv = true;
                                        showNotification('success', 8, 'bottom-right', response.Message);
                                    }
                                    else {
                                        progress(0, '', false);
                                        showNotification('error', 8, 'bottom-right', response.Message);
                                    }
                                }
                            });
                        }

                    }
                    else
                        showNotification('error', 8, 'bottom-right', 'Please Upload only Excel File(s)');
                }
                else
                    showNotification('error', 8, 'bottom-right', 'Please Select File To Upload');

            }


        });



    </script>
    <%--<script src="../../../SMViews/Utility.js"></script>--%>
    <script src="../../../SMViews/Utility.min.js" defer></script>
</body>
</html>
