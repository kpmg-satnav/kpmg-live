﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmChildCategory.aspx.cs" Inherits="HDM_HDM_Webfiles_Masters_frmChildCategory" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
</head>
<body data-ng-controller="ChildCategoryController">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="HD Main Category" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">HD Child Category </h3>
                </div>

                <div class="panel-body" style="padding-right: 50px;">
                    <form role="form" id="form1" name="frm" data-valid-submit="Save()" novalidate>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12" ng-hide="HelpDeskModule">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.HDM_MAIN_MOD_NAME.$invalid}">
                                    <label class="control-label">Module <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Company" data-output-model="ChildCategory.Company" data-button-label="icon HDM_MAIN_MOD_NAME" data-on-item-click="getSubCategoriesByModule()"
                                        data-item-label="icon HDM_MAIN_MOD_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="ChildCategory.Company[0]" name="HDM_MAIN_MOD_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frm.$submitted && frm.HDM_MAIN_MOD_NAME.$invalid" style="color: red;">Please Select Module</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.Code.$invalid}">
                                    <label>Child Category Code<span style="color: red;">*</span></label>
                                    <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                        <input id="txtcode" name="Code" type="text" required data-ng-model="ChildCategory.Code" data-ng-pattern="codepattern" maxlength="30" class="form-control" data-ng-readonly="ActionStatus==1" />
                                        <span class="error" data-ng-show="frm.$submitted && frm.Code.$invalid" style="color: red">Please enter valid code </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.Name.$invalid}">
                                    <label>Child Category Name<span style="color: red;">*</span></label>
                                    <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                        onmouseout="UnTip()">
                                        <input id="txtCName" name="Name" type="text" data-ng-model="ChildCategory.Name" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" />
                                        <span class="error" data-ng-show="frm.$submitted && frm.Name.$invalid" style="color: red">Please enter valid name</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.SubCode.$invalid}">
                                    <label>Sub Category<span style="color: red;">*</span></label>
                                    <div data-ng-class="{'has-error': frm.$submitted && frm.SubCode.$invalid}">
                                        <select id="ddlSub" name="SubCode" class="form-control" data-ng-model="ChildCategory.SubCode" required data-live-search="true" data-ng-change="SubCategoryChanged()">
                                            <option id="ddlsub" value=''>--Select--</option>
                                            <option data-ng-repeat="subcategory in subcategorylist" value="{{subcategory.SUBC_CODE}}">{{subcategory.SUBC_NAME}}</option>
                                        </select>
                                        <span class="error" data-ng-show="frm.$submitted && frm.SubCode.$invalid" style="color: red">Please select sub category </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Main Category<span style="color: red;"></span></label>
                                    <select id="ddlMain" name="MainCode" class="form-control" required data-ng-model="ChildCategory.MainCode" disabled>
                                        <option data-ng-repeat="maincategory in maincategorylist" value="{{maincategory.MNC_CODE}}">{{maincategory.MNC_NAME}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                        <textarea id="txtremarks" data-ng-model="ChildCategory.Remarks" class="form-control" maxlength="500"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frm.$submitted && frm.Status.$invalid}">
                                    <label>Status<span style="color: red;">*</span></label>
                                    <select id="ddlsta" name="Status" data-ng-model="ChildCategory.Status" class="form-control">
                                        <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                    <span class="error" data-ng-show="frm.$submitted && frm.Status.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please select status </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                    <input type="reset" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="EraseData()" />
                                    <%--<input type="button" value="Back" class='btn btn-primary' onclick="window.location = 'frmMASMasters.aspx'" />--%>
                                    <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                                    <%-- <a data-ng-click="GenReport(ChildCategory,'xlsx')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" id="table2">


                            <a data-ng-click="onBtExport()"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>

                        </div>

                        <div class="row" style="padding-left: 30px;">
                            <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <%--<script src="../../../Scripts/aggrid/ag-grid.min.js"></script>--%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

        //app.service("ChildCategoryService", function ($http, $q) {
        //    var deferred = $q.defer();
        //    this.getChildCategory = function () {
        //        return $http.get('../../../api/ChildCategory')
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };
        //    this.CreateSubCategory = function (category) {
        //        deferred = $q.defer();
        //        return $http.post('../../../api/ChildCategory/Create', category)
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };
        //    this.UpdateSubCategory = function (category) {
        //        deferred = $q.defer();
        //        return $http.post('../../../api/ChildCategory/UpdateChildcatData', category)
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };
        //    //sub
        //    this.GetSubCategory = function () {
        //        deferred = $q.defer();
        //        return $http.get('../../../api/ChildCategory/GetSubCategory')
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });

        //    }
        //    //main
        //    this.GetMainCategory = function () {
        //        deferred = $q.defer();
        //        return $http.get('../../../api/ChildCategory/GetMaincategory')
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });

        //    }
        //    //main by sub
        //    this.GetChildCategoryBySub = function (SubCode) {
        //        deferred = $q.defer();
        //        return $http.get('../../../api/ChildCategory/GetMaincategoryBySub/' + SubCode)
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    }

        //    this.GetGriddata = function () {
        //        deferred = $q.defer();
        //        return $http.get('../../../api/ChildCategory/GetGridData')
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };

        //    this.GetSubCategoryByModule = function (dataObject) {
        //        deferred = $q.defer();
        //        return $http.post('../../../api/ChildCategory/GetSubCategoryByModule', dataObject)
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };
        //});
        //app.controller('ChildCategoryController', function ($scope, $q, ChildCategoryService, MainCategoryService) {
        //    $scope.StaDet = [{ Id: 'Active', Name: 'Active' }, { Id: 'Inactive', Name: 'Inactive' }];
        //    $scope.ChildCategory = {};
        //    $scope.categorydata = [];
        //    $scope.maincategorylist = [];
        //    $scope.subcategorylist = [];
        //    $scope.changedlist = [];
        //    $scope.ActionStatus = 0;
        //    $scope.IsInEdit = false;
        //    $scope.ShowMessage = false;

        //    ChildCategoryService.GetSubCategory().then(function (data) {
        //        $scope.subcategorylist = data;
        //        $scope.LoadData();
        //    }, function (error) {
        //        console.log(error);
        //    });

        //    $scope.getSubCategoriesByModule = function () {
        //        var params = {
        //            CompanyId: $scope.ChildCategory.Company[0].HDM_MAIN_MOD_ID
        //        }
        //        ChildCategoryService.GetSubCategoryByModule(params).then(function (response) {
        //            $scope.subcategorylist = response;
        //        }, function (error) {
        //            console.log(error);
        //        });
        //    }

        //    $scope.SubCategoryChanged = function () {
        //        ChildCategoryService.GetChildCategoryBySub($scope.ChildCategory.SubCode).then(function (data) {
        //            $scope.maincategorylist = data;
        //            $scope.ChildCategory.MainCode = data[0].MNC_CODE;
        //        }, function (error) {
        //            console.log(error);
        //        });
        //    }

        //    $scope.PageLoad = function () {
        //        MainCategoryService.HelpDeskModuleHide().then(function (response) {
        //            var result = response.data;
        //            if (result == "1") {
        //                MainCategoryService.GetCompanyModules().then(function (response) {

        //                    $scope.Company = response.data.data;
        //                    $scope.ChildCategory.Company = $scope.Company;
        //                });
        //            } else {
        //                $scope.HelpDeskModule = true;
        //                $scope.ChildCategory.Company = "1";
        //            }

        //        });
        //    }
        //    $scope.PageLoad();

        //    $scope.Save = function () {
        //        if ($scope.IsInEdit) {
        //            $scope.ChildCategory.Company = $scope.ChildCategory.Company[0].HDM_MAIN_MOD_ID;
        //            ChildCategoryService.UpdateSubCategory($scope.ChildCategory).then(function (category) {
        //                $scope.ShowMessage = true;
        //                $scope.Success = "Data Updated Successfully";
        //                $scope.LoadData();
        //                var savedobj = {};
        //                $scope.gridOptions.api.setRowData($scope.gridata);
        //                $scope.IsInEdit = false;
        //                $scope.EraseData();
        //                setTimeout(function () {
        //                    $scope.$apply(function () {
        //                        $scope.ShowMessage = false;
        //                        showNotification('success', 8, 'bottom-right', $scope.Success);
        //                    });
        //                }, 700);
        //                $scope.LoadData();
        //                $scope.ChildCategory = {};
        //                $scope.ActionStatus = 0;
        //            }, function (error) {
        //                console.log(error);
        //            })
        //        }
        //        else {
        //            $scope.ChildCategory.Status = "1";
        //            $scope.ChildCategory.Company = $scope.ChildCategory.Company[0].HDM_MAIN_MOD_ID;
        //            ChildCategoryService.CreateSubCategory($scope.ChildCategory).then(function (response) {
        //                $scope.ShowMessage = true;
        //                $scope.Success = "Data Inserted Successfully";
        //                var savedobj = {};
        //                angular.copy($scope.ChildCategory, savedobj)
        //                $scope.gridata.unshift(savedobj);
        //                $scope.gridOptions.api.setRowData($scope.gridata);
        //                $scope.EraseData();
        //                setTimeout(function () {
        //                    $scope.$apply(function () {
        //                        $scope.ShowMessage = false;
        //                        showNotification('success', 8, 'bottom-right', $scope.Success);
        //                    });
        //                }, 700);
        //                $scope.LoadData();
        //                $scope.ChildCategory = {};
        //            }, function (error) {
        //                $scope.ShowMessage = true;
        //                $scope.Success = error.data;
        //                showNotification('error', 8, 'bottom-right', $scope.Success);
        //                setTimeout(function () {
        //                    $scope.$apply(function () {
        //                        $scope.ShowMessage = false;
        //                    });
        //                }, 1000);
        //                console.log(error);
        //            });
        //        }
        //    }
        //    var columnDefs = [
        //        { headerName: "Child Category Code", field: "Code", width: 190, cellClass: 'grid-align' },
        //        { headerName: "Child Category Name", field: "Name", width: 180, cellClass: 'grid-align' },
        //        //{ headerName: "Sub Category Code", field: "SubCode", width: 190, cellClass: 'grid-align' },
        //        { headerName: "Sub Category Name", field: "SubName", width: 180, cellClass: 'grid-align' },
        //        { headerName: "Main Category", field: "MainName", width: 180, cellClass: 'grid-align' },
        //        { headerName: "Module", field: "Company", width: 180, cellClass: 'grid-align' },
        //        //{ headerName: "Status", suppressMenu: true, template: "{{ShowStatus(data.Status)}}", width: 270, cellClass: 'grid-align' },
        //        { headerName: "Status", field: "Status", suppressMenu: true, width: 270, cellClass: 'grid-align' },
        //        { headerName: "Edit", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', suppressMenu: true, width: 110 }];

        //    $scope.LoadData = function () {
        //        ChildCategoryService.GetGriddata().then(function (data) {
        //            $scope.gridata = data;
        //            //$scope.createNewDatasource();
        //            $scope.gridOptions.api.setRowData(data);
        //        }, function (error) {
        //            console.log(error);
        //        });
        //    }
        //    //$scope.pageSize = '10';

        //    $scope.createNewDatasource = function () {
        //        var dataSource = {
        //            pageSize: parseInt($scope.pageSize),
        //            getRows: function (params) {
        //                setTimeout(function () {
        //                    var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
        //                    var lastRow = -1;
        //                    if ($scope.gridata.length <= params.endRow) {
        //                        lastRow = $scope.gridata.length;
        //                    }
        //                    params.successCallback(rowsThisPage, lastRow);
        //                }, 500);
        //            }
        //        };
        //        $scope.gridOptions.api.setDatasource(dataSource);
        //    }

        //    $scope.gridOptions = {
        //        columnDefs: columnDefs,
        //        enableCellSelection: false,
        //        rowData: null,
        //        enableSorting: true,
        //        enableFilter: true,
        //        showToolPanel: true,
        //        angularCompileRows: true,
        //        onReady: function () {
        //            $scope.gridOptions.api.sizeColumnsToFit()
        //        }
        //    };
        //    $scope.LoadData();

        //    $scope.EditData = function (data) {
        //        $scope.ActionStatus = 1;
        //        $scope.IsInEdit = true;

        //        angular.forEach($scope.Company, function (val) {
        //            if (val.HDM_MAIN_MOD_NAME == data.Company) {
        //                val.ticked = true;
        //            }
        //            else { val.ticked = false; }

        //        });

        //        ChildCategoryService.GetSubCategoryByModule(data).then(function (response) {
        //            $scope.subcategorylist = response;
        //            ChildCategoryService.GetChildCategoryBySub(data.SubCode).then(function (responses) {
        //                $scope.maincategorylist = responses;
        //                $scope.ChildCategory.MainCode = responses[0].MNC_CODE;
        //            })
        //        })
        //        //$scope.ChildCategory.MainCode = data.MainCode;


        //        //$scope.ChildCategory.SubCode = data.SubCode
        //        //$scope.SubCategoryChanged();
        //        //$scope.SubCategoryChanged(data.SubCode);

        //        angular.copy(data, $scope.ChildCategory);
        //    }

        //    //$scope.GenReport = function (ChildMasterData, Type) {
        //    //    progress(0, 'Loading...', true);
        //    //    soobj = {};
        //    //    angular.copy(ChildMasterData, soobj);
        //    //    //soobj.CNP_NAME = Occupdata.CNP_NAME[0].CNP_ID;
        //    //    soobj.Type = Type;
        //    //    //soobj.FromDate = Occupdata.FromDate;
        //    //    //soobj.ToDate = Occupdata.ToDate;
        //    //    //soobj.Costcenterlst = Occupdata.Cost;
        //    //    //Occupdata.Type = Type;
        //    //    if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
        //    //        if (Occupdata.Type == "pdf") {
        //    //            $scope.GenerateFilterPdf();
        //    //        }
        //    //        else {
        //    //            $scope.GenerateFilterExcel();
        //    //        }
        //    //    }
        //    //    else {
        //    //        $http({
        //    //            url: UtilityService.path + '/api/ChildCategory/GetGridData',
        //    //            method: 'POST',
        //    //            data: soobj,
        //    //            responseType: 'arraybuffer'

        //    //        }).success(function (data, status, headers, config) {
        //    //            var file = new Blob([data], {
        //    //                type: 'application/' + Type
        //    //            });
        //    //            var fileURL = URL.createObjectURL(file);
        //    //            $("#reportcontainer").attr("src", fileURL);
        //    //            var a = document.createElement('a');
        //    //            a.href = fileURL;
        //    //            a.target = '_blank';
        //    //            a.download = 'HD Child Category Report.' + Type;
        //    //            document.body.appendChild(a);
        //    //            a.click();
        //    //            progress(0, '', false);
        //    //        }).error(function (data, status, headers, config) {
        //    //        });
        //    //    };
        //    //}

        //    $scope.EraseData = function () {
        //        $scope.ChildCategory = {};
        //        $scope.ActionStatus = 0;
        //        $scope.IsInEdit = false;
        //        $scope.frm.$submitted = false;
        //        angular.forEach($scope.Company, function (Company) {
        //            Company.ticked = false;
        //        });
        //    }

        //    $scope.onBtExport = function () {

        //        var Filterparams = {
        //            skipHeader: false,
        //            skipFooters: false,
        //            skipGroups: false,
        //            allColumns: false,
        //            onlySelected: true,
        //            columnSeparator: ",",
        //            columnKeys: ['Child Category Code'],
        //            fileName: "HD Child Category Master.csv"

        //        };


        //        $scope.gridOptions.api.exportDataAsCsv(Filterparams);


        //    }


        //    $scope.ShowStatus = function (value) {
        //        return $scope.StaDet[value == 0 ? 1 : 0].Name;
        //    }

        //    $("#filtertxt").change(function () {
        //        onFilterChanged($(this).val());
        //    }).keydown(function () {
        //        onFilterChanged($(this).val());
        //    }).keyup(function () {
        //        onFilterChanged($(this).val());
        //    }).bind('paste', function () {
        //        onFilterChanged($(this).val());
        //    })

        //    function onFilterChanged(value) {
        //        $scope.gridOptions.api.setQuickFilter(value);
        //    }
        //});
    </script>
    <%--    <script src="../../../SMViews/Utility.js"></script>
    <script src="../../../HelpdeskManagement/JS/MainCategoryMaster.js"></script>
    <script src="../../../HelpdeskManagement/JS/ChildCategoryMaster.js"></script>--%>


    <script src="../../../SMViews/Utility.min.js" defer></script>

    <script src="../../../HelpdeskManagement/JS/MainCategoryMaster.min.js" defer></script>

    <script src="../../../HelpdeskManagement/JS/ChildCategoryMaster.min.js" defer></script>
</body>
</html>
