﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
           </style>
</head>
<body data-ng-controller="CatDsgController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Category Designation Mapping</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                                </div>
                            </div>
                            <br />
                            <form id="Form1" name="frmMapping" data-valid-submit="CatDsgMaster1()" novalidate>

                                <div class="row">
                                    <div class="clearfix">

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.MNC_NAME.$invalid}">
                                                <label class="control-label">Main Category <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Main" data-output-model="CatDsgMaster.Main" data-button-label="icon MNC_NAME" data-item-label="icon MNC_NAME"
                                                    data-on-item-click="MainChanged()" data-on-select-all="MainChangeAll()" data-on-select-none="MainSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="CatDsgMaster.Main" name="MNC_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmMapping.$submitted && frmMapping.MNC_NAME.$invalid" style="color: red">Please select Main Category </span>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.SUBC_NAME.$invalid}">
                                                <label class="control-label">Sub Category <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Sub" data-output-model="CatDsgMaster.Sub" data-button-label="icon SUBC_NAME" data-item-label="icon SUBC_NAME"
                                                    data-on-item-click="SubChanged()" data-on-select-all="SubChangeAll()" data-on-select-none="SubSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="CatDsgMaster.Sub" name="SUBC_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmMapping.$submitted && frmMapping.SUBC_NAME.$invalid" style="color: red">Please select Sub Category </span>

                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.CHC_TYPE_NAME.$invalid}">
                                                <label class="control-label">Child Category <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Child" data-output-model="CatDsgMaster.Child" data-button-label="icon CHC_TYPE_NAME" data-item-label="icon CHC_TYPE_NAME"
                                                    data-on-item-click="ChildChanged()" data-on-select-all="ChildChangeAll()" data-on-select-none="ChildSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="CatDsgMaster.Child" name="CHC_TYPE_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmMapping.$submitted && frmMapping.CHC_TYPE_NAME.$invalid" style="color: red">Please select Child Category </span>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.DSN_AMT_TITLE.$invalid}">
                                                <label class="control-label">Designation <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Designation" data-output-model="CatDsgMaster.Designation" data-button-label="icon DSN_AMT_TITLE" data-item-label="icon DSN_AMT_TITLE"
                                                    data-on-item-click="DsgChanged()" data-on-select-all="DsgChangeAll()" data-on-select-none="DsgSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="CatDsgMaster.Designation" name="DSN_AMT_TITLE" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmMapping.$submitted && frmMapping.DSN_AMT_TITLE.$invalid" style="color: red">Please select Designation </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                    <textarea id="Textarea1" runat="server" data-ng-model="CatDsgMaster.HDM_REM" class="form-control" maxlength="500"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                            <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                            <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                            <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                        </div>
                                    </div>
                                </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                        <div data-ag-grid="gridOptions" style="height: 329px; width: 100%;" class="ag-blue"></div>
                                    </div>

                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>

    <script src="../../../SMViews/Utility.js" defer></script>
    <script src="../js/CatDsg.js" defer></script>

    <script src="../js/HDMUtility.js" defer></script>

</body>


</html>





