﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/HDM/HDM_Webfiles/Masters/frmFeedBackMaster.cs" Inherits="HDM_HDM_Webfiles_Masters_frmFeedBackMaster" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="FeedBackController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="HD Main Category" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">FeedBack Master</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form role="form" id="form1" name="frmFeedBack" data-valid-submit="Save()" novalidate>
                        <div>

                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>FeedBack Code<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmFeedBack.$submitted && frmFeedBack.FBD_CODE.$invalid}" onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                            <input id="FBD_CODE" type="text" name="FBD_CODE" data-ng-readonly="ActionStatus==1" maxlength="15" data-ng-pattern="RegExpCode.REGEXP" data-ng-model="FeedBack.FBD_CODE" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmFeedBack.$submitted && frmFeedBack.FBD_CODE.$invalid" style="color: red" data-ng-bind="RegExpCode.REGMSG"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>FeedBack Name<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmFeedBack.$submitted && frmFeedBack.FBD_NAME.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')" onmouseout="UnTip()">
                                            <input id="FBD_NAME" type="text" name="FBD_NAME" maxlength="50" data-ng-model="FeedBack.FBD_NAME" data-ng-pattern="RegExpCode.REGEXP" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmFeedBack.$submitted && frmFeedBack.FBD_NAME.$invalid" style="color: red" data-ng-bind="RegExpCode.REGMSG"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Remarks</label>
                                        <div data-ng-class="{'has-error': frmFeedBack.$submitted && frmFeedBack.FBD_REM.$invalid}" onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                            <textarea id="FBD_REM" name="FBD_REM" runat="server" data-ng-model="FeedBack.FBD_REM" class="form-control" maxlength="500" data-ng-pattern="RegExpCode.REGEXP"></textarea>
                                            <span class="error" data-ng-show="frmFeedBack.$submitted && frmFeedBack.FBD_REM.$invalid" style="color: red" data-ng-bind="RegExpCode.REGMSG"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-show="ActionStatus==1">
                                        <label>Status<span style="color: red;">*</span></label>
                                        <select id="FBD_STA_ID" name="FBD_STA_ID" data-ng-model="FeedBack.FBD_STA_ID" class="form-control">
                                            <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                        <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                        <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                        <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" style="height: 329px; width: 100%;" class="ag-blue"></div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>

        var app = angular.module('QuickFMS', ["agGrid"]);

        app.service("FeedBackService", function ($http, $q) {
            var deferred = $q.defer();


            //SAVE
            this.saveFeedBack = function (assetLoc) {
                deferred = $q.defer();
                return $http.post('../../../api/FeedBack/Create', assetLoc)
                  .then(function (response) {
                      deferred.resolve(response);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
            //UPDATE BY ID
            this.updateFeedBack = function (asset) {
                deferred = $q.defer();
                return $http.post('../../../api/FeedBack/UpdateFeedBackData', asset)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
            //Bind Grid
            this.GetFeedBackBindGrid = function () {
                deferred = $q.defer();
                return $http.get('../../../api/FeedBack/GetFeedBackBindGrid')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
        });

        app.controller('FeedBackController', function ($scope, $q, FeedBackService, $http, $timeout, UtilityService) {
            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.FeedBack = {};
            $scope.repeatCategorylist = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;
            $scope.RegExpName = UtilityService.RegExpName;
            $scope.RegExpRemarks = UtilityService.RegExpRemarks;
            $scope.RegExpCode = UtilityService.RegExpCode;

            //to Save the data
            $scope.Save = function () {
                if ($scope.IsInEdit) {

                    FeedBackService.updateFeedBack($scope.FeedBack).then(function (repeat) {
                        var updatedobj = {};
                        angular.copy($scope.FeedBack, updatedobj)
                        $scope.gridata.unshift(updatedobj);
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Updated Successfully";
                        $scope.LoadData();
                        $scope.IsInEdit = false;
                        $scope.ClearData();

                        setTimeout(function () {
                            $scope.$apply(function () {
                                showNotification('success', 8, 'bottom-right', $scope.Success);
                                //$scope.ShowMessage = false;
                            });
                        }, 700);
                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.FeedBack.FBD_STA_ID = "1";
                    FeedBackService.saveFeedBack($scope.FeedBack).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data Inserted Successfully";
                        var savedobj = {};
                        angular.copy($scope.FeedBack, savedobj)
                        $scope.gridata.unshift(savedobj);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                        $scope.FeedBack = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                        showNotification('error', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                        console.log(error);
                    });
                }
            }
            //for GridView
            var columnDefs = [
               { headerName: "FeedBack Code", field: "FBD_CODE", width: 290, cellClass: 'grid-align' },
               { headerName: "FeedBack Name", field: "FBD_NAME", width: 380, cellClass: 'grid-align' },
               { headerName: "Status", suppressMenu: true, template: "{{ShowStatus(data.FBD_STA_ID)}}", width: 270, cellClass: 'grid-align' },
               { headerName: "Edit", width: 114, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true, onmouseover: "cursor: hand (a pointing hand)" }
            ];

            // To display grid row data
            $scope.LoadData = function () {
                progress(0, 'Loading...', true);
                FeedBackService.GetFeedBackBindGrid().then(function (data) {
                    $scope.gridata = data;
                    // $scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);
                    progress(0, '', false);
                }, function (error) {
                    console.log(error);
                });
            }

            $scope.onPageSizeChanged = function () {
                $scope.createNewDatasource();
            };


            // $scope.pageSize = '10';

            $scope.createNewDatasource = function () {
                var dataSource = {
                    pageSize: parseInt($scope.pageSize),
                    getRows: function (params) {
                        setTimeout(function () {
                            var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                            var lastRow = -1;
                            if ($scope.gridata.length <= params.endRow) {
                                lastRow = $scope.gridata.length;
                            }
                            params.successCallback(rowsThisPage, lastRow);
                        }, 500);
                    }
                };
                $scope.gridOptions.api.setDatasource(dataSource);
            }

            $scope.gridOptions = {
                columnDefs: columnDefs,
                rowData: null,
                enableSorting: true,
                cellClass: 'grid-align',
                angularCompileRows: true,
                enableCellSelection: false,
                enableFilter: true,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }
            };

            $timeout($scope.LoadData, 1000);

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })

            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }


            $scope.EditFunction = function (data) {
                $scope.FeedBack = {};
                angular.copy(data, $scope.FeedBack);
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;

            }
            $scope.ClearData = function () {
                $scope.FeedBack = {};
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
                $scope.frmFeedBack.$submitted = false;
            }
            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }
        });

    </script>
    <%--<script src="../../../SMViews/Utility.js"></script>--%>
    <script src="../../../SMViews/Utility.min.js" defer></script>
</body>
</html>
