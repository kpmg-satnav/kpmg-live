﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Imports System.IO
Imports System.Web.UI

Partial Class HDM_HDM_Webfiles_Masters_frmAddSLAMaster
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lblMsg.Text = ""
        If Not IsPostBack Then
            'bindrequests()
            bindlocation()
            fillgrid()
            gvSer.Visible = True
            ' btnSubmit.Text = "Add"
        End If
    End Sub

    Public Sub bindlocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Location_GetAll")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, "--Select--")
    End Sub

    Public Sub bindrequests(lcm_code As String)

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GETACTIVESERREQUESTTYPE")
        sp.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
        ddlServiceCat.DataSource = sp.GetDataSet()
        ddlServiceCat.DataTextField = "SER_NAME"
        ddlServiceCat.DataValueField = "SER_CODE"
        ddlServiceCat.DataBind()
        ddlServiceCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            lblMsg.Text = ""
            If ddlLocation.SelectedIndex <> 0 Then
                bindrequests(ddlLocation.SelectedItem.Value)
            Else
                ddlServiceCat.Items.Clear()
                ddlSerType.Items.Clear()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub cleardata()
        ddlLocation.SelectedIndex = 0
        ddlServiceCat.SelectedIndex = 0
        ddlSerType.SelectedIndex = 0
        txtL1.Text = ""
        txtL2.Text = ""
        txtL3.Text = ""
        txtRemarks.Text = ""
        ddlStatus.SelectedIndex = 0
    End Sub

    Protected Sub ddlServiceCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlServiceCat.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex <> 0 And ddlServiceCat.SelectedIndex <> 0 Then
                bindretypes(ddlLocation.SelectedItem.Value, ddlServiceCat.SelectedItem.Value)
            Else
                ddlSerType.Items.Clear()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub bindretypes(lcm_code As String, ser_cat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_SERTYPEBYCATLOCATION")
        sp.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
        sp.Command.AddParameter("@SER_CODE", ser_cat, DbType.String)
        ddlSerType.DataSource = sp.GetDataSet()
        ddlSerType.DataTextField = "SER_TYPE_NAME"
        ddlSerType.DataValueField = "SER_TYPE_CODE"
        ddlSerType.DataBind()
        ddlSerType.Items.Insert(0, "--Select--")
    End Sub

    Public Sub AddRequest()
        Try
            Dim req As String = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") + "/" + Session("uid")

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_ADD_REQUEST_MATRIX")
            sp.Command.AddParameter("@REQ_ID", req, DbType.String)
            sp.Command.AddParameter("@REQ_LOC", ddlLocation.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@REQ_TYPE", ddlSerType.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@REQ_CAT", ddlServiceCat.SelectedItem.Value, DbType.String)
            'sp.Command.AddParameter("@REQ_NAME", ddlRequests.SelectedItem.Value, DbType.String)
            'sp.Command.AddParameter("@LEVEL1_USER", txtUser1Contact.Text, DbType.String)
            'sp.Command.AddParameter("@LEVEL2_USER", txtuser2.Text, DbType.String)
            'sp.Command.AddParameter("@LEVEL3_USER", txtuser3.Text, DbType.String)
            sp.Command.AddParameter("@L1_HR", txtL1.Text, DbType.String)
            sp.Command.AddParameter("@L2_HR", txtL2.Text, DbType.String)
            sp.Command.AddParameter("@L3_HR", txtL3.Text, DbType.String)
            sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
            sp.Command.AddParameter("@UPDATED_BY", Session("UID"), DbType.String)

            'sp.Command.AddParameter("@LOCATION", loc, DbType.String)
            sp.Command.AddParameter("@STA_ID", ddlStatus.SelectedItem.Value, DbType.String)
            'sp.Command.AddParameter("@REQ_CITY", ddlcity.SelectedItem.Value, DbType.String)


            Dim validatecode As String
            validatecode = sp.ExecuteScalar()
            If validatecode = "1" Then
                cleardata()
                lblMsg.Visible = True
                lblMsg.Text = "SLA added Successfully"
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Request type SLA already exists..."
            End If

            'Dim strEmpId As String = ""
            'Dim strEmpId2 As String = ""
            'Dim strEmpId3 As String = ""
            'If Trim(txtUser1Contact.Text) <> "" Then
            '    Dim arrTEmployees As Array = Split(txtUser1Contact.Text, ",")
            '    strEmpId = strEmpId & arrTEmployees(0)
            'End If
            'If Trim(txtuser2.Text) <> "" Then
            '    Dim arrTEmployees2 As Array = Split(txtuser2.Text, ",")
            '    strEmpId2 = strEmpId2 & arrTEmployees2(0)
            'End If
            'If Trim(txtuser3.Text) <> "" Then
            '    Dim arrTEmployees3 As Array = Split(txtuser3.Text, ",")
            '    strEmpId3 = strEmpId3 & arrTEmployees3(0)
            'End If


            ''----------------- USER ID AND EMAILS -----------------------------------------
            'If Trim(txtUser1Contact.Text) <> "" Then
            '    Dim strArray As String() = Trim(txtUser1Contact.Text).Split(",")
            '    Dim i As Integer
            '    For i = 0 To strArray.Length - 1
            '        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"INSERT_EMAIL_REQUEST")
            '        sp2.Command.AddParameter("@REQ_TYPE", req, DbType.String)
            '        sp2.Command.AddParameter("@AUR_ID", strArray(i), DbType.String)
            '        sp2.Command.AddParameter("@ESC_LEVEL", 0, DbType.Int32)
            '        sp2.ExecuteScalar()
            '    Next i
            'End If

            'If Trim(txtuser2.Text) <> "" Then
            '    Dim strArray As String() = Trim(txtuser2.Text).Split(",")
            '    Dim i As Integer
            '    For i = 0 To strArray.Length - 1
            '        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"INSERT_EMAIL_REQUEST")
            '        sp2.Command.AddParameter("@REQ_TYPE", req, DbType.String)
            '        sp2.Command.AddParameter("@AUR_ID", strArray(i), DbType.String)
            '        sp2.Command.AddParameter("@ESC_LEVEL", 1, DbType.Int32)
            '        sp2.ExecuteScalar()
            '    Next i
            'End If

            'If Trim(txtuser3.Text) <> "" Then
            '    Dim strArray As String() = Trim(txtuser3.Text).Split(",")
            '    Dim i As Integer
            '    For i = 0 To strArray.Length - 1
            '        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"INSERT_EMAIL_REQUEST")
            '        sp2.Command.AddParameter("@REQ_TYPE", req, DbType.String)
            '        sp2.Command.AddParameter("@AUR_ID", strArray(i), DbType.String)
            '        sp2.Command.AddParameter("@ESC_LEVEL", 2, DbType.Int32)
            '        sp2.ExecuteScalar()
            '    Next i
            'End If

            ''--------------------------------------------------------------



            'If txtmob1.Text <> "" Then
            '    Dim strArray As String() = Trim(txtmob1.Text).Split(",")
            '    Dim i As Integer
            '    For i = 0 To strArray.Length - 1
            '        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_SMS_REQ_0")
            '        sp2.Command.AddParameter("@SMS_REQ_TYPE", req, DbType.String)
            '        sp2.Command.AddParameter("@SMS_PHONE_NUMBER", strArray(i), DbType.String)
            '        sp2.ExecuteScalar()
            '    Next i
            'End If

            'If txtmob2.Text <> "" Then
            '    Dim strArray1 As String() = Trim(txtmob2.Text).Split(",")
            '    Dim J As Integer
            '    For J = 0 To strArray1.Length - 1
            '        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_SMS_REQ_1")
            '        sp3.Command.AddParameter("@SMS_REQ_TYPE", req, DbType.String)
            '        sp3.Command.AddParameter("@SMS_PHONE_NUMBER", strArray1(J), DbType.String)
            '        sp3.ExecuteScalar()
            '    Next J
            'End If

            'If txtmob3.Text <> "" Then
            '    Dim strArray2 As String() = Trim(txtmob3.Text).Split(",")
            '    Dim K As Integer
            '    For K = 0 To strArray2.Length - 1
            '        Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_SMS_REQ_2")
            '        sp4.Command.AddParameter("@SMS_REQ_TYPE", req, DbType.String)
            '        sp4.Command.AddParameter("@SMS_PHONE_NUMBER", strArray2(K), DbType.String)
            '        sp4.ExecuteScalar()
            '    Next K
            'End If

            'Dim validassigned As Integer
            'validassigned = ValidateAssigned(strEmpId)
            'If validassigned = 1 Then
            '    UpdateAssignedRole(strEmpId)

            'End If
            'Dim validassigned1 As Integer
            'validassigned1 = ValidateAssigned1(strEmpId2)
            'If validassigned = 1 Then
            '    UpdateAssignedRole1(strEmpId2)

            'End If
            'Dim validassigned2 As Integer
            'validassigned2 = ValidateAssigned2(strEmpId3)
            'If validassigned = 1 Then
            '    UpdateAssignedRole2(strEmpId3)

            'End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If Not IsValid Then
            Exit Sub
        End If
        If btnSubmit.Text = "Add" Then
            AddRequest()

            'fillgrid()
        Else
            updaterecord()
        End If
        fillgrid()
    End Sub

    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GET_ALLSLA")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvSer.DataSource = ds
        gvSer.DataBind()
        For i As Integer = 0 To gvSer.Rows.Count - 1
            Dim lblstatus As Label = CType(gvSer.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "InActive"
            End If
        Next
    End Sub

    Public Sub updaterecord()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_UPDATE_REQUEST_MATRIX")
        sp.Command.AddParameter("@SNO", CInt(lblTemp.Text), DbType.String)
        sp.Command.AddParameter("@REQ_LOC", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQ_TYPE", ddlSerType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQ_CAT", ddlServiceCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@L1_HR", txtL1.Text, DbType.String)
        sp.Command.AddParameter("@L2_HR", txtL2.Text, DbType.String)
        sp.Command.AddParameter("@L3_HR", txtL3.Text, DbType.String)
        sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@UPDATED_BY", Session("UID"), DbType.String)
        'sp.Command.AddParameter("@LOCATION", loc, DbType.String)
        sp.Command.AddParameter("@STA_ID", ddlStatus.SelectedItem.Value, DbType.String)
        'sp.Command.AddParameter("@REQ_CITY", ddlcity.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
        cleardata()
        lblMsg.Visible = True
        lblMsg.Text = "SLA modified successfully..."
        btnSubmit.Text = "Add"
        ddlServiceCat.Enabled = True
        ddlLocation.Enabled = True
        ddlSerType.Enabled = True

    End Sub

    Protected Sub gvSer_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvSer.PageIndexChanging
        gvSer.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub gvSer_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvSer.RowCommand
        If e.CommandName = "EDIT" Then
            btnSubmit.Text = "Modify"
            If gvSer.Rows.Count > 0 Then
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
                Dim lblID As Label = DirectCast(gvSer.Rows(rowIndex).FindControl("lblID"), Label)
                Dim id As Integer = CInt(lblID.Text)
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GET_ALLSLABIND")
                sp.Command.AddParameter("@SNO", id, DbType.Int32)
                lblTemp.Text = id
                Dim ds As DataSet
                ds = sp.GetDataSet()
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim lcm_code As String
                    Dim ser_code As String
                    ddlLocation.Enabled = False
                    ddlServiceCat.Enabled = False
                    ddlSerType.Enabled = False
                    ddlLocation.ClearSelection()
                    ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("LCM_CODE")).Selected = True
                    lcm_code = ds.Tables(0).Rows(0).Item("LCM_CODE")
                    bindrequests(lcm_code)
                    ddlServiceCat.ClearSelection()
                    ddlServiceCat.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_CODE")).Selected = True
                    ser_code = ds.Tables(0).Rows(0).Item("SER_CODE")
                    bindretypes(lcm_code, ser_code)
                    ddlSerType.ClearSelection()
                    ddlSerType.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_TYPE_CODE")).Selected = True
                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("STA_ID")).Selected = True
                    txtL1.Text = ds.Tables(0).Rows(0).Item("L1_HR")
                    txtL2.Text = ds.Tables(0).Rows(0).Item("L2_HR")
                    txtL3.Text = ds.Tables(0).Rows(0).Item("L3_HR")
                    txtRemarks.Text = ds.Tables(0).Rows(0).Item("REMARKS")
                    'txtEmployeeCode.Text = ds.Tables(0).Rows(0).Item("SRQ_MAP_AUR_ID")


                End If


            End If

        End If
    End Sub

    Protected Sub gvSer_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles gvSer.RowEditing

    End Sub

End Class
