﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/HDM/HDM_Webfiles/Masters/frmFacilityMaster.cs" Inherits="HDM_HDM_Webfiles_Masters_frmFacilityMaster" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="FacilityController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="HD Facility Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">HD Facility Master </h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <form role="form" id="form1" name="frmFacility" data-valid-submit="Save()" novalidate>
                        <div>
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Facility Code<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmFacility.$submitted && frmFacility.FD_CODE.$invalid}" onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                            <input id="FD_CODE" type="text" name="FD_CODE" data-ng-readonly="ActionStatus==1" maxlength="15" data-ng-pattern="RegExpCode.REGEXP" data-ng-model="HDFacility.FD_CODE" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmFacility.$submitted && frmFacility.FD_CODE.$invalid" style="color: red" data-ng-bind="RegExpCode.REGMSG"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Facility Name<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmFacility.$submitted && frmFacility.FD_Name.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')" onmouseout="UnTip()">
                                            <input id="FD_Name" type="text" name="FD_Name" maxlength="50" data-ng-model="HDFacility.FD_Name" data-ng-pattern="RegExpName.REGEXP" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmFacility.$submitted && frmFacility.FD_Name.$invalid" style="color: red" data-ng-bind="RegExpName.REGMSG"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Location<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmFacility.$submitted && frmFacility.FD_LOCATION.$invalid}">
                                            <select id="ddlLocation" name="FD_LOCATION" class="form-control" data-ng-model="HDFacility.FD_LOCATION" required>
                                                <option id="ddlLoc" value=''>--Select--</option>
                                                <option data-ng-repeat="location in locationlist" value="{{location.LCM_CODE}}">{{location.LCM_NAME}}</option>
                                            </select>
                                            <span class="error" data-ng-show="frmFacility.$submitted && frmFacility.FD_LOCATION.$invalid" style="color: red">Please select location. </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Cost<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmFacility.$submitted && frmFacility.COST.$invalid}" onmouseover="Tip('Enter Cost in numbers')" onmouseout="UnTip()">
                                            <input id="txtCost" type="text" name="COST" maxlength="50" data-ng-model="HDFacility.COST" data-ng-pattern="/^[0-9]*$/" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmFacility.$submitted && frmFacility.COST.$invalid" style="color: red"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Remarks</label>
                                        <div data-ng-class="{'has-error': frmFacility.$submitted && frmFacility.Remarks.$invalid}" onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                            <textarea id="Remarks" name="Remarks" runat="server" data-ng-model="HDFacility.Remarks" class="form-control" maxlength="500" data-ng-pattern="RegExpRemarks.REGEXP"></textarea>
                                            <span class="error" data-ng-show="frmFacility.$submitted && frmFacility.Remarks.$invalid" style="color: red" data-ng-bind="RegExpRemarks.REGMSG"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-show="ActionStatus==1">
                                        <label>Status<span style="color: red;">*</span></label>
                                        <select id="FD_STA_ID" name="FD_STA_ID" data-ng-model="HDFacility.FD_STA_ID" class="form-control">
                                            <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                        <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                        <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                        <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="height: 320px">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 90%;" class="ag-blue"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js" defer></script>
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/highlight.js" defer></script>
    <script src="../../../BootStrapCSS/Bootstrapswitch/js/main.js" defer></script>
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
        var CompanySessionId = '<%= Session["COMPANYID"]%>';
    </script>
    <%--<script src="../../../HelpdeskManagement/JS/FacilityMaster.js"></script>--%>
    <script src="../../../HelpdeskManagement/JS/FacilityMaster.min.js" defer></script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <%--<script src="../../../SMViews/Utility.js"></script>--%>
</body>
</html>

