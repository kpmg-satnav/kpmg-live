﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class HDM_HDM_Webfiles_Masters_frmAddLocationMaster
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'getassetbrand()
            btnSubmit.Text = "Add"
            fillgrid()
            trLName.Visible = False
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If Page.IsValid Then
            If btnSubmit.Text = "Add" Then
                Dim ValidateCode As Integer
                ValidateCode = ValidateBrand(txtLcmCode.Text)
                If ValidateCode = 0 Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Location Code already exist please enter another Location Code..."
                ElseIf ValidateCode = 1 Then
                    insertnewrecord()
                    ' fillgrid()

                End If
            Else
                'txtLcmCode.Enabled = False
                If ddlLocation.SelectedIndex <> 0 Then
                    modifydata()
                End If
                getlocation()
            End If
            ' Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=1")
            ' insertnewrecord()
            cleardata()
        End If
    End Sub
    Public Sub insertnewrecord()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_INSERT_LOCATION")
            '@VT_CODE,@VT_TYPE,@VT_STATUS,@VT_CREATED_BY,@VT_CREATED_DT,@VT_REM
            sp1.Command.AddParameter("@LCM_CODE", txtLcmCode.Text, DbType.String)
            sp1.Command.AddParameter("@LCM_NAME", txtLcmName.Text, DbType.String)
            sp1.Command.AddParameter("@LCM_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@LCM_UPT_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@LCM_REM", txtRemarks.Text, DbType.String)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "New Location Added Successfully..."
            cleardata()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Function ValidateBrand(ByVal brandcode As String) As Integer

        Dim ValidateCode As Integer
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_VALIDATE_LOCATIONCODE")
            sp1.Command.AddParameter("@LCM_CODE", brandcode, DbType.String)
            ValidateCode = sp1.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Return ValidateCode


    End Function
    Public Sub cleardata()
        txtLcmCode.Text = ""
        txtLcmName.Text = ""
        ddlStatus.SelectedIndex = 0
        txtRemarks.Text = ""
    End Sub
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GET_ALLLOCATION")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvLocation.DataSource = ds
        gvLocation.DataBind()
        For i As Integer = 0 To gvLocation.Rows.Count - 1
            Dim lblstatus As Label = CType(gvLocation.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "InActive"
            End If
        Next
    End Sub
    Protected Sub rbActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActions.SelectedIndexChanged
        Dim iLoop As Integer
        For iLoop = 0 To rbActions.Items.Count - 1
            If rbActions.Items(iLoop).Selected = True Then
                If rbActions.Items(iLoop).Text = "Add" Then
                    ddlLocation.Visible = False
                    trLName.Visible = False
                    lblLocationSelect.Visible = False
                    cleardata()
                    btnSubmit.Text = "Add"
                    lblMsg.Visible = False
                    txtLcmCode.Enabled = True
                    fillgrid()
                ElseIf rbActions.Items(iLoop).Text = "Modify" Then
                    getlocation()
                    trLName.Visible = True
                    ddlLocation.Visible = True
                    lblLocationSelect.Visible = True
                    cleardata()
                    btnSubmit.Text = "Modify"
                    lblMsg.Visible = False
                    txtLcmCode.Enabled = False

                    fillgrid()
                End If
            End If
        Next
    End Sub
    Public Sub getlocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_GETLOCATIONCODE")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        Try
            If ddlLocation.SelectedIndex <> 0 Then
                lblMsg.Visible = False
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_GETLOCATIONDATAMODIFY")
                sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String)
                Dim ds As New DataSet
                ds = sp.GetDataSet
                If ds.Tables(0).Rows.Count > 0 Then '
                    txtLcmCode.Text = ds.Tables(0).Rows(0).Item("LCM_CODE")
                    txtLcmName.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
                    txtRemarks.Text = ds.Tables(0).Rows(0).Item("LCM_REM")
                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("LCM_STA_ID")).Selected = True

                End If
            Else
                cleardata()
            End If

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub modifydata()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_UPDATE_LOCATION")
            sp1.Command.AddParameter("@LCM_CODE", txtLcmCode.Text, DbType.String)
            sp1.Command.AddParameter("@LCM_NAME", txtLcmName.Text, DbType.String)
            sp1.Command.AddParameter("@LCM_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@LCM_UPT_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@LCM_REM", txtRemarks.Text, DbType.String)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "Location Modified Successfully..."
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvLocation_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvLocation.PageIndexChanging
        gvLocation.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
End Class
