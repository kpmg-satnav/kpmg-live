﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="MainCategoryController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="HD Main Category" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">HD Main Category </h3>
                </div>
                <div class="panel-body" style="padding-right: 50px;">
                    <form role="form" id="form1" name="frmMainCategory" data-valid-submit="Save()" novalidate>

                        <div class="row">

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Main Category Code <span style="color: red;">*</span></label>
                                    <div data-ng-class="{'has-error': frmMainCategory.$submitted && frmMainCategory.MNC_Code.$invalid}" onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                        <input id="MNC_Code" type="text" name="MNC_Code" data-ng-readonly="ActionStatus==1" maxlength="15" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" data-ng-model="MainCategory.MNC_Code" class="form-control" required="required" />&nbsp;
                                              <span class="error" data-ng-show="frmMainCategory.$submitted && frmMainCategory.MNC_Code.$invalid" style="color: red">Please enter valid code </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Main Category Name<span style="color: red;">*</span></label>
                                    <div data-ng-class="{'has-error': frmMainCategory.$submitted && frmMainCategory.MNC_Name.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')" onmouseout="UnTip()">
                                        <input id="MNC_Name" type="text" name="MNC_Name" maxlength="50" data-ng-model="MainCategory.MNC_Name" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmMainCategory.$submitted && frmMainCategory.MNC_Name.$invalid" style="color: red">Please enter valid Name </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Remarks <span style="color: red;"></span></label>
                                    <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                        <textarea id="txtrem" data-ng-model="MainCategory.MNC_REM" class="form-control" maxlength="500"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="row" data-ng-show="ActionStatus==1">
                                        <label class="control-label">Status <span style="color: red;">*</span></label>
                                        <div>
                                            <select id="Select1" name="MNC_Status_Id" data-ng-model="MainCategory.MNC_Status_Id" class="form-control">
                                                <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12" ng-hide="HelpDeskModule">
                                <div class="form-group" data-ng-class="{'has-error': frmMainCategory.$submitted && frmMainCategory.HDM_MAIN_MOD_NAME.$invalid}">
                                    <label class="control-label">Module <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Company" data-output-model="MainCategory.Company" data-button-label="icon HDM_MAIN_MOD_NAME"
                                        data-item-label="icon HDM_MAIN_MOD_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="MainCategory.Company[0]" name="HDM_MAIN_MOD_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmMainCategory.$submitted && frmMainCategory.HDM_MAIN_MOD_NAME.$invalid" style="color: red;">Please Select COMPANY</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                    <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                    <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-left: 30px;">
                            <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>

    <script defer>

        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

        //app.service("MainCategoryService", function ($http, $q) {
        //    var deferred = $q.defer();
        //    this.getMainCategory = function () {
        //        deferred = $q.defer();
        //        return $http.get('../../../api/MainCategory')
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };

        //    //SAVE
        //    this.saveMainCategory = function (mncCat) {
        //        deferred = $q.defer();
        //        return $http.post('../../../api/MainCategory/Create', mncCat)
        //            .then(function (response) {
        //                deferred.resolve(response);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };


        //    this.GetCompanyModules = function () {
        //        deferred = $q.defer();
        //        return $http.get('../../../api/MainCategory/GetCompanyModules')
        //            .then(function (response) {
        //                deferred.resolve(response);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };

        //    this.HelpDeskModuleHide = function () {
        //        deferred = $q.defer();
        //        return $http.get('../../../api/MainCategory/HelpDeskModuleHide')
        //            .then(function (response) {
        //                deferred.resolve(response);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };
        //    //UPDATE BY ID
        //    this.updateMainCategory = function (repeat) {
        //        deferred = $q.defer();
        //        return $http.post('../../../api/MainCategory/UpdateMainCategoryData/', repeat)
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };
        //    //Bind Grid
        //    this.GetMainCategoryBindGrid = function () {
        //        deferred = $q.defer();
        //        return $http.get('../../../api/MainCategory/GetMainCategoryBindGrid')
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };
        //});

        //app.controller('MainCategoryController', function ($scope, $q, MainCategoryService, $timeout, $http) {
        //    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
        //    $scope.MainCategory = {};
        //    $scope.repeatCategorylist = [];
        //    $scope.ActionStatus = 0;
        //    $scope.IsInEdit = false;
        //    $scope.ShowMessage = false;
        //    $scope.Company = [];
        //    //$scope.MainCategory.Company = {}

        //    //to Save the data
        //    $scope.Save = function () {
        //        if ($scope.IsInEdit) {
        //            $scope.MainCategory.Company = $scope.MainCategory.Company[0].HDM_MAIN_MOD_ID;
        //            console.log($scope.MainCategory);
        //            MainCategoryService.updateMainCategory($scope.MainCategory).then(function (repeat) {
        //                $scope.ShowMessage = true;
        //                $scope.Success = "Data Updated Successfully";
        //                $scope.LoadData();

        //                var savedobj = {};
        //                angular.copy($scope.MainCategory, savedobj)
        //                $scope.gridata.unshift(savedobj);
        //                $scope.gridOptions.api.setRowData($scope.gridata);
        //                $scope.ClearData();
        //                $scope.IsInEdit = false;
        //                setTimeout(function () {
        //                    $scope.$apply(function () {
        //                        $scope.ShowMessage = false;
        //                        showNotification('success', 8, 'bottom-right', $scope.Success);
        //                    });
        //                }, 700);
        //                $scope.MainCategory = {};
        //            }, function (error) {
        //                console.log(error);
        //            })
        //        }
        //        else {
        //            $scope.MainCategory.MNC_Status_Id = "1";
        //            $scope.MainCategory.Company = $scope.MainCategory.Company[0].HDM_MAIN_MOD_ID;
        //            console.log($scope.MainCategory);
        //            MainCategoryService.saveMainCategory($scope.MainCategory).then(function (response) {
        //                $scope.ShowMessage = true;
        //                $scope.Success = "Data Inserted Successfully";
        //                var savedobj = {};
        //                //$scope.EditCategory.MNC_UPDATED_DT = response.
        //                angular.copy($scope.MainCategory, savedobj)
        //                $scope.gridata.unshift(savedobj);
        //                $scope.gridOptions.api.setRowData($scope.gridata);
        //                showNotification('success', 8, 'bottom-right', $scope.Success);
        //                setTimeout(function () {
        //                    $scope.$apply(function () {
        //                        $scope.ShowMessage = false;
        //                    });
        //                }, 700);
        //                $scope.MainCategory = {};
        //            }, function (error) {
        //                $scope.ShowMessage = true;
        //                $scope.Success = error.data;
        //                showNotification('error', 8, 'bottom-right', $scope.Success);
        //                setTimeout(function () {
        //                    $scope.$apply(function () {
        //                        $scope.ShowMessage = false;
        //                    });
        //                }, 1000);
        //                console.log(error);
        //            });
        //        }
        //    }
        //    //for GridView
        //    var columnDefs = [
        //        { headerName: "Main Category Code", field: "MNC_Code", width: 120, cellClass: 'grid-align' },
        //        { headerName: "Main Category Name", field: "MNC_Name", width: 160, cellClass: 'grid-align' },
        //        { headerName: "Module", field: "Company", width: 160, cellClass: 'grid-align' },
        //        { headerName: "Status", template: "{{ShowStatus(data.MNC_Status_Id)}}", suppressMenu: true, width: 100, cellClass: 'grid-align' },
        //        { headerName: "Edit", width: 70, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true, suppressSorting: true }
        //    ];

        //    $scope.PageLoad = function () {
        //        MainCategoryService.HelpDeskModuleHide().then(function (response) {
        //            var result = response.data;
        //            if (result == "1") {
        //                MainCategoryService.GetCompanyModules().then(function (response) {
        //                    $scope.Company = response.data.data;
        //                    $scope.MainCategory.Company = $scope.Company;
        //                });
        //            } else {
        //                $scope.HelpDeskModule = true;
        //                $scope.MainCategory.Company = "1";
        //            }

        //        });
        //        //MainCategoryService.GetCompanyModules().then(function (response) {
        //        //    $scope.Company = response.data.data;                   
        //        //    $scope.MainCategory.Company = $scope.Company;
        //        //});
        //    }
        //    $scope.PageLoad();

        //    // To display grid row data
        //    $scope.LoadData = function () {
        //        progress(0, 'Loading...', true);
        //        MainCategoryService.GetMainCategoryBindGrid().then(function (data) {
        //            $scope.gridata = data;
        //            console.log(data);
        //            //$scope.createNewDatasource();
        //            $scope.gridOptions.api.setRowData(data);
        //            progress(0, '', false);
        //        }, function (error) {
        //            console.log(error);
        //        });
        //    }
        //    //$scope.pageSize = '10';

        //    $scope.createNewDatasource = function () {
        //        var dataSource = {
        //            pageSize: parseInt($scope.pageSize),
        //            getRows: function (params) {
        //                setTimeout(function () {
        //                    var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
        //                    var lastRow = -1;
        //                    if ($scope.gridata.length <= params.endRow) {
        //                        lastRow = $scope.gridata.length;
        //                    }
        //                    params.successCallback(rowsThisPage, lastRow);
        //                }, 500);
        //            }
        //        };
        //        $scope.gridOptions.api.setDatasource(dataSource);
        //    }

        //    $scope.gridOptions = {
        //        columnDefs: columnDefs,
        //        rowData: null,
        //        enableSorting: true,
        //        cellClass: 'grid-align',
        //        angularCompileRows: true,
        //        enableFilter: true,
        //        enableCellSelection: false,
        //        onReady: function () {
        //            $scope.gridOptions.api.sizeColumnsToFit()
        //        }
        //    };
        //    $timeout($scope.LoadData, 1000);
        //    $scope.EditFunction = function (data) {
        //        //var f = _.find($scope.Company, function (o) { return o.HDM_MAIN_MOD_NAME != data.Company }); 
        //        // f.ticked = false;
        //        $scope.MainCategory = {};
        //        $scope.EditCategory = data;
        //        angular.forEach($scope.Company, function (val) {
        //            if (val.HDM_MAIN_MOD_NAME == data.Company) {
        //                val.ticked = true;
        //            }
        //            else { val.ticked = false; }

        //        });

        //        //var t = _.find($scope.Company, function (o) { return o.HDM_MAIN_MOD_NAME == data.Company });                
        //        //t.ticked = true;   

        //        angular.copy(data, $scope.MainCategory);
        //        $scope.ActionStatus = 1;
        //        $scope.IsInEdit = true;
        //    }
        //    $scope.ClearData = function () {
        //        $scope.MainCategory = {};
        //        $scope.ActionStatus = 0;
        //        $scope.IsInEdit = false;
        //        $scope.frmMainCategory.$submitted = false;
        //        angular.forEach($scope.Company, function (Company) {
        //            Company.ticked = false;
        //        });
        //    }
        //    $scope.ShowStatus = function (value) {
        //        return $scope.StaDet[value == 0 ? 1 : 0].Name;
        //    }

        //    $("#filtertxt").change(function () {
        //        onFilterChanged($(this).val());
        //    }).keydown(function () {
        //        onFilterChanged($(this).val());
        //    }).keyup(function () {
        //        onFilterChanged($(this).val());
        //    }).bind('paste', function () {
        //        onFilterChanged($(this).val());
        //    })

        //    function onFilterChanged(value) {
        //        $scope.gridOptions.api.setQuickFilter(value);
        //    }

        //});

    </script>
    <%--   <script src="../../../SMViews/Utility.js"></script>
    <script src="../../../HelpdeskManagement/JS/MainCategoryMaster.js"></script>--%>

    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../../../HelpdeskManagement/JS/MainCategoryMaster.min.js" defer></script>
</body>
</html>
