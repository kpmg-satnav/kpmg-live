﻿<%@ Page Language="C#" AutoEventWireup="false" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Scripts/angular-confirm.css" rel="stylesheet" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="ChildCatApprovalLocMatrixController" class="amantra">
    <div class="container-fluid page-content-inner">
        <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading-qfms">
                    <h3 class="panel-title panel-heading-qfms-title">Child Category Location Approval Matrix</h3>
                </div>
                <div class="panel-body" style="padding-right: 10px;">
                    <div class="clearfix">
                        <div class="box-footer text-right">
                            <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                        </div>
                    </div>
                    <br />
                    <form id="Form1" name="frmMapping" data-valid-submit="ChildCatAppMat()" novalidate>

                        <div class="row">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-6 col-xs-12" ng-hide="HelpDeskModule">
                                    <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.HDM_MAIN_MOD_NAME.$invalid}">
                                        <label class="control-label">Module <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Company" data-output-model="ChildCatLocApp.Company" data-button-label="icon HDM_MAIN_MOD_NAME"
                                            data-on-item-click="getMainCategories()" data-item-label="icon HDM_MAIN_MOD_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="ChildCatLocApp.Company[0]" name="HDM_MAIN_MOD_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmMapping.$submitted && frmMapping.HDM_MAIN_MOD_NAME.$invalid" style="color: red;">Please Select Module</span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.MNC_NAME.$invalid}">
                                        <label class="control-label">Main Category <span style="color: red;">*</span></label>
                                        <div isteven-multi-select data-input-model="Main" data-output-model="ChildCatLocApp.Main" data-button-label="icon MNC_NAME" data-item-label="icon MNC_NAME"
                                            data-on-item-click="MainChanged()" data-on-select-all="MainChangeAll()" data-on-select-none="MainSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="ChildCatLocApp.Main" name="MNC_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmMapping.$submitted && frmMapping.MNC_NAME.$invalid" style="color: red">Please select Main Category </span>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.SUBC_NAME.$invalid}">
                                        <label class="control-label">Sub Category <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Sub" data-output-model="ChildCatLocApp.Sub" data-button-label="icon SUBC_NAME" data-item-label="icon SUBC_NAME"
                                            data-on-item-click="SubChanged()" data-on-select-all="SubChangeAll()" data-on-select-none="SubSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="ChildCatLocApp.Sub" name="SUBC_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmMapping.$submitted && frmMapping.SUBC_NAME.$invalid" style="color: red">Please select Sub Category </span>

                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.CHC_TYPE_NAME.$invalid}">
                                        <label class="control-label">Child Category <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Child" data-output-model="ChildCatLocApp.Child" data-button-label="icon CHC_TYPE_NAME" data-item-label="icon CHC_TYPE_NAME"
                                            data-on-item-click="ChildChanged()" data-on-select-all="ChildChangeAll()" data-on-select-none="ChildSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="ChildCatLocApp.Child" name="CHC_TYPE_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmMapping.$submitted && frmMapping.CHC_TYPE_NAME.$invalid" style="color: red">Please select Child Category </span>
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix">
                                <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.DSG_NAME.$invalid}">
                                                <label class="control-label">Designation <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Designation" data-output-model="ChildCatApp.Designation" data-button-label="icon DSG_NAME"
                                                    data-item-label="icon DSG_NAME maker" data-on-item-click="DesignationChange()" data-on-select-all="DesignationSelectAll()" data-on-select-none="DesignationSelectNone()"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="ChildCatApp.Designation" name="DSG_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="frmMapping.$submitted && frmMapping.DSG_NAME.$invalid" style="color: red">Please select Designation </span>
                                            </div>
                                        </div>--%>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.LCM_NAME.$invalid}">
                                        <label class="control-label">Location <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Locations" data-output-model="ChildCatLocApp.Locations" data-button-label="icon LCM_NAME"
                                            data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="LocationSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="ChildCatLocApp.Locations" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmMapping.$submitted && frmMapping.LCM_NAME.$invalid" style="color: red">Please select Location </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.Nums.$invalid}">
                                        <label>No. of Level to Approve<span style="color: red;">*</span></label>
                                        <div data-ng-class="{'has-error': frmMapping.$submitted && frmMapping.Nums.$invalid}">
                                            <select id="ddlNum" name="Nums" class="form-control" data-ng-model="Numbers.Nums" required="" data-live-search="true" data-ng-change="NumberChanged(Nums)">
                                                <option id="Option1" value=''>--Select--</option>
                                                <option data-ng-repeat="num in Numbers" value="{{num.Key}}">{{num.Value}}</option>
                                            </select>
                                            <span class="error" data-ng-show="frmMapping.$submitted && frmMapping.Nums.$invalid" style="color: red">Please select No. Of Levels Approve </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="clearfix ">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>Approver Level</td>
                                            <td>Select Role </td>
                                            <td>Select Employees</td>
                                        </tr>
                                        <tr data-ng-repeat="column in Apprlst" data-ng-form="innerForm">
                                            <td>{{ $index + 1}}</td>
                                            <td>
                                                <div data-ng-class="{'has-error': frmSLA.$submitted && innerForm.column.ROL_ID.$invalid}">
                                                    <select class="form-control input-sm" data-ng-model="column.ROL_ID" name="ROL_ID" required="" data-ng-change="GetEmpOnRole(column)">
                                                        <option id="Option2" value="">--Select--</option>
                                                        <option data-ng-repeat="Lst in HDMList" data-ng-selected="column.ROL_ID==Lst.ROL_ID" value="{{Lst.ROL_ID}}">{{Lst.ROL_DESCRIPTION}}</option>
                                                    </select>
                                                </div>
                                                <span class="error" data-ng-show="innerForm.column.ROL_ID.$error.pattern">Select All Levels</span>
                                            </td>
                                            <td>
                                                <div isteven-multi-select data-input-model="column.Emplst" data-output-model="column.OpEmplst" data-button-label="icon AUR_KNOWN_AS" data-item-label="icon AUR_KNOWN_AS"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="column.OpEmplst" name="AUR_KNOWN_AS" style="display: none" required="" />
                                                <span class="error" data-ng-show="innerForm.$submitted && innerForm.AUR_KNOWN_AS.$invalid" style="color: red">Select An Employee</span>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="clearfix">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                    <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                    <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>

                        <div style="height: 320px">
                            <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 80%;" class="ag-blue"></div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../BootStrapCSS/Scripts/angular-confirm.js" defer></script>

    <%--  <script src="../../BootStrapCSS/Scripts/angular-confirm.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js"></script>--%>
    <%--<script src="../../BootStrapCSS/qtip/qtip.js"></script>--%>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "cp.ngConfirm"]);
        //var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "cp.ngConfirm"]);
    </script>


    <script src="../../../SMViews/Utility.js" defer></script>
    <script src="../js/ChildCatApprovalLocMatrix.js" defer></script>
    <script src="../js/ChildCatApprovalMat.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../HelpdeskManagement/JS/MainCategoryMaster.js" defer></script>
    <script src="../../../HelpdeskManagement/JS/SubCategoryMaster.js" defer></script>
    <script src="../../../HelpdeskManagement/JS/ChildCategoryMaster.js" defer></script>
    <script src="../js/HDMUtility.js" defer></script>

</body>


</html>






